import os
import math
import maya.cmds as Cmds
import maya.mel as Mel
import xml.dom.minidom as Dom

rootNode = u"|Scene"
directory = "Export" #os.path.dirname(filePath)
children = Cmds.listRelatives([rootNode], children=True)

for i in range(0, len(children)):
  child = children[i]
  childName = child.split("|")[-1]
  Cmds.select(childName)
  ogreExport = 'ogreExport -sel -obj -lu pref -scale 1'
  ogreExport += ' -mesh "' + directory + '\\\\' + childName + '.mesh" -shared -v -n -c -t -edges -tangents -TEXCOORD'
  ogreExport += ' -mat "' + directory + '\\\\' + childName + '.material" -matPrefix "' + childName + '" -copyTex "' + directory + '\\\\";'
  Mel.eval(ogreExport)

'''
command = 'ogreExport -sel -obj -lu pref -scale 1 -mesh "Export\\\MeshName.mesh" -shared -v -n -c -t -edges -tangents -TEXCOORD;'
Mel.eval(command)
command = 'ogreExport -sel -obj -lu pref -scale 1 -mat "Export\\\MeshName.material" -matPrefix "MeshNamePrefix" -copyTex "Export\\\";'
Mel.eval(command)
'''

'''  
select -cl;
select "structure*";
pickWalk -d up;
string $sel[] = `ls -sl`;
for($each in $sel)
{
  eval ("move -y 0 " + $each + ".scalePivot " + $each + ".rotatePivot; \n");
	move -y 0 $each.scalePivot $each.rotatePivot ;
  //	ogreExport -all -world -lu pref -scale 1 -mesh "C:\\Documents and Settings\\Scott\\Desktop\\City_maya\\Exporty\\" + $each + ".mesh" -shared -v -n -t -mat "C:\\Documents and Settings\\Scott\\Desktop\\City_maya\\Exporty\\" + $each + ".material";
}

ogreExport -sel -obj -lu pref -scale 1 -mesh "Export\\MeshName.mesh" -shared -v -n -c -t -edges -tangents -TEXCOORD -mat "Export\\MatName.material" -matPrefix "MatPrefix" -copyTex "Export\\";
'''