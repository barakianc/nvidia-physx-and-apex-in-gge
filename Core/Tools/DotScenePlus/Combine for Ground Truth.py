'''
Scene Transformation Tool for Maya

(c) 2008
    Balakrishnan Ranganathan (balki_live_com)
All Rights Reserved.
'''

import os
import math
import maya.cmds as Cmds
import maya.mel as Mel
import xml.dom.minidom as Dom

rootNode = u"|polySurface360"
children = Cmds.listRelatives([rootNode], children=True)

for i in range(0, len(children), 2):
  childA = children[i]
  childB = children[i + 1]
  childAName = childA.split("|")[-1]
  childBName = childB.split("|")[-1]
  buildingIndex = i / 2 + 1
  outputName = 'structure_city_building%03d' % (buildingIndex, )
  Cmds.polyUnite(childAName, childBName, n=outputName, ch=False)
  Cmds.xform(outputName, cp=True)
  Cmds.move(0, 0, 0, '%s.scalePivot' % (outputName, ), '%s.rotatePivot' % (outputName, ), y=True)