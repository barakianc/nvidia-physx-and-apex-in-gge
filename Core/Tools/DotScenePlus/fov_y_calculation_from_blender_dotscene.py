		# fov calculation
		render = sceneExporter.scene.getRenderingContext()
		aspX = render.aspectRatioX()
		aspY = render.aspectRatioY()
		sizeX = render.imageSizeX()
		sizeY = render.imageSizeY()
		if (camera.getType()==0):
			# perspective projection
			# FOV calculation
			#    .
			#   /|
			#  / |v = 16
			# /a |
			#.---'
			#  u = lens
			#
			# => FOV = 2*a = 2*arctan(v/u)
			#
			# Blender's v is in direction of the larger image dimension.
			# Ogre's FOV is definied in y direction. Therefore if 
			# SizeX*AspX > SizeY*AspY the corresponding v in y direction is
			# v_y = v*(SizeY*AspY)/(SizeX*AspX).
			fovY = 0.0
			if (sizeX*aspX > sizeY*aspY):
				fovY = 2*math.atan(sizeY*aspY*16.0/(camera.getLens()*sizeX*aspX))
			else:
				fovY = 2*math.atan(16.0/camera.getLens())
			# fov in degree
			fovY = fovY*180.0/math.pi
			# write fov
			fileObject.write("fov=\"%f\"" % fovY)
			fileObject.write(" projectionType=\"perspective\">\n")
		else:
			# orthographic projection
			# FOV calculation 
			# 
			# In Blender, the displayed area is scale wide in the larger image direction,
			# where scale is a ortho camera attribute.
			#
			# Ogre calculates the with and height of the displayed area from the
			# FOVy and the near clipping plane.
			#    .
			#   /|
			#  / |y
			# /a |
			#.---'
			#  n
			# Here n = near clipping plane and a = FOVy/2.
			#
			# Hence y = scale/2 and FOVy = 2*atan(scale/(2*n)).
			fovY = 0.0
			if (sizeX*aspX > sizeY*aspY):
				# tan(FOVx/2.0) = x/n = y*a/n,
				# where a is the aspect ratio.
				# It follows that s/(n*a) = y/n = tan(FOVy/2.0).
				fovY = 2.0*math.atan(sizeY*aspY*camera.getScale()/(2.0*camera.getClipStart()*sizeX*aspX))
			else:
				fovY = 2.0*math.atan(camera.getScale()/(2.0*camera.getClipStart()))
			# fov in degree
			fovY = fovY*180.0/math.pi
			# write fov
			fileObject.write("fov=\"%f\"" % fovY)
			fileObject.write(" projectionType=\"orthographic\">\n")