'''
Scene Exporter for Maya

(c) 2008
  Balakrishnan Ranganathan (balki_live_com)
  Rahul Singh (euclidmenot_gmail_com)
All Rights Reserved.
'''

import os
import re
import math
import maya.cmds as Cmds
import maya.mel as Mel
import xml.dom.minidom as Dom

global xmlDocument
global exportPath

def ParseVector3(xmlNode, nodeName, attributeString):
  '''
  Parses the vector3 xyz components and adds it to the xmlNode
  '''
  xmlNode.setAttribute("x", CleanAttribute(Cmds.getAttr(nodeName + "." + attributeString + "X")))
  xmlNode.setAttribute("y", CleanAttribute(Cmds.getAttr(nodeName + "." + attributeString + "Y")))
  xmlNode.setAttribute("z", CleanAttribute(Cmds.getAttr(nodeName + "."  + attributeString + "Z")))

def ParseQuaternion(xmlNode, nodeName, attributeString):
  '''
  Parses the vector3 xyz components and adds it to the xmlNode
  '''
  xmlNode.setAttribute("x", str(Cmds.getAttr(nodeName + "." + attributeString + "X")))
  xmlNode.setAttribute("y", str(Cmds.getAttr(nodeName + "." + attributeString + "Y")))
  xmlNode.setAttribute("z", str(Cmds.getAttr(nodeName + "."  + attributeString + "Z")))
  xmlNode.setAttribute("w", str(Cmds.getAttr(nodeName + "."  + attributeString + "W")))

def ParseColor(xmlNode, nodeName, attributeString = "color"):
  '''
  Parses the color rgba components and adds it to the xmlNode
  '''
  xmlNode.setAttribute("r", str(Cmds.getAttr(nodeName + "." + attributeString + "R")))
  xmlNode.setAttribute("g", str(Cmds.getAttr(nodeName + "." + attributeString + "G")))
  xmlNode.setAttribute("b", str(Cmds.getAttr(nodeName + "."  + attributeString + "B")))
  xmlNode.setAttribute("a", str(Cmds.getAttr(nodeName + ".intensity")))

def ParseMatrix(xmlNode, nodeName, attributeString):
  '''
  Parses the matrix and adds it to the xmlNode
  '''
  matrix = Cmds.getAttr(nodeName + "." + attributeString)
  for i in range(0, 16):
    xmlNode.setAttribute("v" + str(i), str(matrix[i]))

def ParseBoolean(booleanResult):
  '''
  Returns True for 1 and False for 0
  '''
  if (str(booleanResult) == "1"):
    return "true"
  else:
    return "false"

def CleanAttribute(attribute):
  '''
  Cleans up the random erroneous Maya attribute which comes in as a list
  and also makes all values a string to write to the XML file
  '''
  if (type(attribute) == type([])):
    attribute = attribute[-1]
  return str(attribute)

def GetParentName(fullNodePath):
  '''
  Returns the parent's name
  '''
  return fullNodePath.split("|")[-2]

def Traverse(node, parentXmlNode):
  '''
  Traverse the maya full-path node and attach it to the parentXmlNode
  '''
  global xmlDocument
  children = Cmds.listRelatives([node], children=True)

  '''
  print "\nNode: " + str(node)
  print "Children: " + str(children)
  print "Node type: " + Cmds.nodeType(node)
  print "Attributes: " + str(Cmds.listAttr(node))
  '''

  nodeName = node.split("|")[-1]
  nodeType = Cmds.nodeType(node)

  # xmlNode.setAttribute("", Cmds.getAttr(nodeName + "."))

  if nodeType in ["node", "transform"]:
    xmlNode = xmlDocument.createElement("node")
    xmlNode.setAttribute("name", nodeName)
  elif nodeType in ["pointLight", "directionalLight", "spotLight"]:
    xmlNode = xmlDocument.createElement("light")
    xmlNode.setAttribute("name", GetParentName(node))
    xmlNode.setAttribute("type", nodeType.replace("Light", ""))

    xmlNode.setAttribute("visible", ParseBoolean(Cmds.getAttr(GetParentName(node) + ".visibility")))
    xmlNode.setAttribute("castShadows", ParseBoolean(Cmds.getAttr(nodeName + ".useDepthMapShadows")))

    if (Cmds.getAttr(nodeName + ".emitDiffuse") == 1):
      xmlColourDiffuseNode = xmlDocument.createElement("colourDiffuse")
      xmlNode.appendChild(xmlColourDiffuseNode)
      ParseColor(xmlColourDiffuseNode, nodeName, "color") # Might be lightIntensity

    if (Cmds.getAttr(nodeName + ".emitSpecular") == 1):
      xmlColourSpecularNode = xmlDocument.createElement("colourSpecular")
      xmlNode.appendChild(xmlColourSpecularNode)
      ParseColor(xmlColourSpecularNode, nodeName, "color") # Might be lightIntensity

    if nodeType in ["directionalLight", "spotLight"]:
      xmlNormalNode = xmlDocument.createElement("normal")
      xmlNode.appendChild(xmlNormalNode)
      ParseVector3(xmlNormalNode, nodeName, "lightDirection")

    if nodeType == "spotLight":
      xmlLightRangeNode = xmlDocument.createElement("lightRange")
      xmlLightRangeNode.setAttribute("inner", str(Cmds.getAttr(nodeName + ".coneAngle")))
      xmlLightRangeNode.setAttribute("outer", str(Cmds.getAttr(nodeName + ".penumbraAngle")))
      xmlLightRangeNode.setAttribute("falloff", str(Cmds.getAttr(nodeName + ".dropoff")))
      xmlNode.appendChild(xmlLightRangeNode)

    '''
    xmlLightAttenuationNode = xmlDocument.createElement("lightAttenuation")
    xmlLightAttenuationNode.appendChild(xmlLightAttenuationNode)
    xmlLightAttenuationNode.setAttribute("range", str(10000))
    if Cmds.getAttr(nodeName + ".decayRate") == 0:
      xmlLightAttenuationNode.setAttribute("constant", str(1))
    else:
      xmlLightAttenuationNode.setAttribute("constant", str(0))
      if Cmds.getAttr(nodeName + ".decayRate") == 1:
        xmlLightAttenuationNode.setAttribute("linear", str(1))
      else:
        xmlLightAttenuationNode.setAttribute("linear", str(0))
        if Cmds.getAttr(nodeName + ".decayRate") == 2:
          xmlLightAttenuationNode.setAttribute("quadratic", str(1))
        else:
          xmlLightAttenuationNode.setAttribute("quadratic", str(0))
    '''

  elif nodeType in ["camera"]:
    xmlNode = xmlDocument.createElement("camera")
    xmlNode.setAttribute("name", GetParentName(node))

    focalLength = Cmds.getAttr(nodeName + ".focalLength");
    aspectX = Cmds.getAttr(nodeName + ".horizontalFilmAperture")
    aspectY = Cmds.getAttr(nodeName + ".verticalFilmAperture")

    fovY = 0.0

    '''
    fovY = 2 * math.atan((aspectX/aspectY) / 2 * focalLength)
    #37.8 = 2 * math.atan(d / 2 * focalLength)
    #tan(37.8 / 2) * 2f = d
    #fovY = 2 * focalLength * math.tan((37.8 * math.pi / 180) / 2) # 23.96 for 37.8 (v) and 35.97 for 54.4 (h)
    fovY = fovY * 180.0 / math.pi
    '''

    fovY = 37.8

    xmlNode.setAttribute("fov", str(fovY))
    xmlNode.setAttribute("aspectRatio", str(aspectX / aspectY))

    if (Cmds.getAttr(nodeName + ".orthographic") == 0):
      xmlNode.setAttribute("projectionType", "perspective")
    else:
      xmlNode.setAttribute("projectionType", "orthographic")

    xmlClippingNode = xmlDocument.createElement("clipping")
    xmlNode.appendChild(xmlClippingNode)
    xmlClippingNode.setAttribute("nearPlaneDist", str(Cmds.getAttr(nodeName + ".nearClipPlane")))
    xmlClippingNode.setAttribute("farPlaneDist", str(Cmds.getAttr(nodeName + ".farClipPlane")))

    '''
    xmlLookTargetNode = xmlDocument.createElement("lookTarget")
    xmlNode.appendChild(xmlLookTargetNode)

    xmlTrackTargetNode = xmlDocument.createElement("trackTarget")
    xmlNode.appendChild(xmlTrackTargetNode)
    '''
  elif nodeType in ["joint"]:
    xmlNode = xmlDocument.createElement("joint")
    xmlNode.setAttribute("name", nodeName)
  elif nodeType in ["mesh"]:
    parentName = GetParentName(node)
    
    '''
    Cmds.select(parentName)
    Cmds.makeIdentity(s=True, r=True)
    '''
    
    xmlNode = xmlDocument.createElement("entity")
    xmlNode.setAttribute("name", nodeName)
    
    if parentName.find(":") != -1:
      meshFileName = os.path.basename(Cmds.referenceQuery(parentName, filename = True))
      meshFileName = os.path.splitext(meshFileName)[0]
    else:
      meshFileName = parentName
      meshFileName += "_" + nodeName

    xmlNode.setAttribute("meshFile", meshFileName + ".mesh")
    xmlNode.setAttribute("castShadows", "true")
    
    Cmds.select(nodeName)
    meshFilePath = exportPath + '\\\\' + meshFileName + '.mesh'
    materialFilePath = exportPath + '\\\\' + meshFileName + '.material'
    ogreExport = 'ogreExport -sel -obj -lu pref -scale 1'
    ogreExport += ' -mesh "' + meshFilePath + '" -shared -v -n -c -t -edges -tangents -TEXCOORD'
    ogreExport += ' -mat "' + materialFilePath + '" -matPrefix "' + meshFileName + '" -copyTex "' + exportPath + '\\\\";'
    
    #print "Executing: " + ogreExport
    Mel.eval(ogreExport)
    
    if os.path.exists(materialFilePath):
      textureRE = re.compile(r'( *texture )(.*)')
      materialFile = open(materialFilePath, "r")
      lines = materialFile.readlines()
      materialFile.close()
      
      materialFile = open(materialFilePath, "w")
      for line in lines:
        line = textureRE.sub('\g<1>"\g<2>"', line)
        materialFile.write(line)
      materialFile.close()
  else:
    xmlNode = xmlDocument.createElement("node")
    xmlNode.setAttribute("name", nodeName)

  xmlNode.setAttribute("maya-type", nodeType)
  parentXmlNode.appendChild(xmlNode)

  if "xformMatrix" in Cmds.listAttr(node):
    xformNode = xmlDocument.createElement("xform")
    xmlNode.appendChild(xformNode)
    ParseMatrix(xformNode, nodeName, "xformMatrix")

  if "translate" in Cmds.listAttr(node):
    positionNode = xmlDocument.createElement("position")
    xmlNode.appendChild(positionNode)
    ParseVector3(positionNode, nodeName, "translate")

  if "rotate" in Cmds.listAttr(node):
    '''
    rotationNode =  xmlDocument.createElement("rotation")
    xmlNode.appendChild(rotationNode)
    ParseVector3(rotationNode, nodeName, "rotate")
    '''

  if "rotateQuaternion" in Cmds.listAttr(node):
    '''
    quaternionNode = xmlDocument.createElement("quaternion")
    xmlNode.appendChild(quaternionNode)
    ParseQuaternion(quaternionNode, nodeName, "rotateQuaternion")
    '''

    '''
    heading = Cmds.getAttr(nodeName + ".rotateX")
    attitude = Cmds.getAttr(nodeName + ".rotateY")
    bank = Cmds.getAttr(nodeName + ".rotateZ")

    c1 = math.cos(heading / 2);
    s1 = math.sin(heading / 2);
    c2 = math.cos(attitude / 2);
    s2 = math.sin(attitude / 2);
    c3 = math.cos(bank / 2);
    s3 = math.sin(bank / 2);
    c1c2 = c1 * c2;
    s1s2 = s1 * s2;
    w = c1c2 * c3 - s1s2 * s3;
    x = c1c2 * s3 + s1s2 * c3;
    y = s1 * c2 * c3 + c1 * s2 * s3;
    z = c1 * s2 * c3 - s1 * c2 * s3;

    quaternionNode = xmlDocument.createElement("quaternion")
    xmlNode.appendChild(quaternionNode)
    quaternionNode.setAttribute("x", str(x))
    quaternionNode.setAttribute("y", str(y))
    quaternionNode.setAttribute("z", str(z))
    quaternionNode.setAttribute("w", str(w))
    '''

  if "scale" in Cmds.listAttr(node):
    scaleNode = xmlDocument.createElement("scale")
    xmlNode.appendChild(scaleNode)
    ParseVector3(scaleNode, nodeName, "scale")

  if children != None:
    for child in children:
      Traverse(node + "|" + child, xmlNode)

def Main(filePath):
  global xmlDocument
  global exportPath
  
  xmlDocument = Dom.Document()
  rootXmlElement = xmlDocument.createElement("scene")
  rootXmlElement.setAttribute("formatVersion", "3.0")
  xmlDocument.appendChild(rootXmlElement)

  xmlNodesElement = xmlDocument.createElement("nodes")
  rootXmlElement.appendChild(xmlNodesElement)

  rootNode = u"|Scene"
  if not Cmds.objExists(rootNode):
    rootNode = u"|scene"

  if not Cmds.objExists(rootNode):
    print "\n\nError: Please provide a root node (group) in maya named 'Scene' so the exporter can know what objects to export."
    Cmds.confirmDialog(title="DotScene Exporter for Maya", message="Please provide a root node (group) in maya named 'Scene' so the exporter can know what objects to export.", button=["Ooh!"])
  else:
    exportPath = os.path.dirname(filePath) + "\\OgreExport"
    if not os.path.exists(exportPath):
      os.mkdir(exportPath)
    exportPath = exportPath.replace("\\", "\\\\")
    
    Traverse(rootNode, xmlNodesElement)
    rootXmlElement.writexml(open(filePath, "w"), "", "\t", "\n")
    print "\n\n\nExport complete."
    Cmds.confirmDialog(title="DotScene Exporter for Maya", message="Export complete!", button=["Yay!"])

def CreateDotScene(*args):
  '''
  Pass the desired output path to Main
  '''
  Main(Cmds.textField(outputFilePathTextField, query=True, text=True))

'''
Create UI Window
'''
window = Cmds.window(title="DotScene Exporter for Maya", widthHeight=(500, 100))
Cmds.rowColumnLayout( numberOfColumns=2, columnAttach=(1, "left", 1), columnWidth=[(1, 125), (2, 350)] )
Cmds.text(label="Output file path")
outputFilePathTextField = Cmds.textField()
Cmds.textField(outputFilePathTextField, edit=True, text="Output.scene")
Cmds.button(label="Create DotScene file", command=CreateDotScene)
Cmds.text(label="Warning: Remember to freeze scale and rotation.")
Cmds.showWindow(window)