// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef APEX_EFFECT_PACKAGE_EDITOR_H

#define APEX_EFFECT_PACKAGE_EDITOR_H

#define USE_EFFECT_PACKAGE 0

#if USE_EFFECT_PACKAGE

#include "Foundation/Px.h"

namespace physx
{
	class PxTransform;
	namespace general_renderdebug4
	{
		class RenderDebug;
	};


namespace apex
{

class NxApexSDK;
class NxApexScene;
class NxApexActor;
class NxApexEmitterAsset;
class NxApexAsset;
class NxApexRenderVolume;

};
};

namespace physx
{

class PxEditorWidgetManager;

class ApexEffectPackageEditorInterface
{
public:
	virtual void apexEffectPackageEditorLog(const char *fmt,...) = 0;
};

class ApexEffectPackageEditor
{
public:
	enum FieldSamplerType
	{
		FST_TURBULENCE,
		FST_JET,
		FST_ATTRACTOR,
		FST_LAST
	};
	//! release this object.
	virtual void release(void) = 0;
	virtual physx::apex::NxApexActor* pump(bool& exitApp) = 0;  // if pump returns false, then exit the application (close window)
	virtual void editApexAssets(physx::apex::NxApexSDK* sdk, physx::apex::NxApexScene* scene) = 0;
	virtual void visualize(physx::general_renderdebug4::RenderDebug* callback,bool solid) = 0;
	virtual void setSaveResourcesPath(const char *pathName) = 0;
	virtual void setPublishResourcesPath(const char *pathName) = 0;
	virtual void preResetScene(void) = 0;
	virtual void postResetScene(physx::apex::NxApexScene *scene) = 0;

	virtual void setWorldTransform(const physx::PxTransform &xform) = 0;
	virtual const PxTransform &getWorldTransform(void) const = 0;

protected:
	virtual ~ApexEffectPackageEditor(void) {}

};

}; // namespace physx


#if defined(PX_VC)
#if defined(PX_EDITOR_EXPORT_DLL)
#define PX_EDITOR_DLL_API __declspec(dllexport)
#else
#define PX_EDITOR_DLL_API __declspec(dllimport)
#endif
#endif

typedef physx::ApexEffectPackageEditor* (*CreateApexEffectPackageEditorPROC)(const char *rootPath,const char *savePath,physx::ApexEffectPackageEditorInterface *callback);
PX_C_EXPORT PX_EDITOR_DLL_API physx::ApexEffectPackageEditor* CreateApexEffectPackageEditor(const char *rootPath,const char *savePath,physx::ApexEffectPackageEditorInterface *callback);

#endif

#endif
