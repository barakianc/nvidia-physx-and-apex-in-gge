// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

// ===============================================================================
//
//							NVIDIA APEX SDK Samples
//
// This file contains settings used by all the sample applications, for connecting
// to the Visual Remote Debugger (VRD). Edit this file to make the samples connect
// to the computer running the VRD application.
//
// ===============================================================================

#ifndef SAMPLES_VRD_SETTINGS
#define SAMPLES_VRD_SETTINGS

#include "NxApexSDK.h"

#include "PVDBinding.h"

//Set this to 0 to stop the samples from trying to connect to the VRD
#define SAMPLES_USE_VRD 1

//Change this setting to the IP number or DNS name of the computer that is running the VRD
#define SAMPLES_VRD_HOST "localhost"

/**
 *	PVD can interpret basically three types of information.  Debug information like objects and properties,
 *	profiling information memory events.  Normally, due to the fairly small amount they take up,
 *	profiling and memory is always included.  Debug information can easily swamp the connection and
 *	slow down the simulation.
 */
static inline void togglePvdConnection(const char* pvd_host, physx::debugger::TConnectionFlagsType flags)
{
	using namespace physx::apex;
	using namespace PVD;
	if (NxGetApexSDK())
	{
		const char default_pvd_host[] = SAMPLES_VRD_HOST;
		if (pvd_host == NULL || pvd_host[0] == '\0')
		{
			pvd_host = default_pvd_host;
		}

		PvdBinding* theBinding = NxGetApexSDK()->getPvdBinding();
		if (theBinding != NULL)
		{
			if (theBinding->isConnected())
			{
				theBinding->disconnect();
			}
			else
			{
				theBinding->connect(pvd_host, DEFAULT_PVD_BINDING_PORT, DEFAULT_PVD_BINDING_TIMEOUT_MS, flags);
			}
		}
	}
}



#endif //SAMPLES_VRD_SETTINGS
