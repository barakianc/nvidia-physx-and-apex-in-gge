// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_CLOTHING_ACTOR_H
#define SAMPLE_CLOTHING_ACTOR_H

#include <SampleActor.h>
#include "SampleRandomWind.h"
#include "PsShare.h"

#include <vector>

#include <NxParameterized.h>

namespace SampleRenderer
{
class Renderer;
}

namespace physx
{
namespace apex
{
class NxApexScene;
class NxClothingActor;
class NxClothingAsset;
class NxUserRenderer;
}
}

class SampleAnimatedMeshActor;

class SampleClothingActor : public SampleFramework::SampleActor
{
public:
	SampleClothingActor(physx::apex::NxApexScene& apexScene, physx::apex::NxClothingAsset& asset, physx::apex::NxUserRenderer* apexRenderer, const physx::PxMat44& globalPose, bool useGPU = true, bool useLocalSpace = true);
	SampleClothingActor(physx::apex::NxApexScene& apexScene, physx::apex::NxClothingAsset& asset, physx::apex::NxUserRenderer* apexRenderer, SampleAnimatedMeshActor* actor, bool useGPU = true, bool useLocalSpace = true, const physx::PxVec3* morphDisplacements = NULL, physx::PxU32 numMorphDisplacements = 0);

	virtual ~SampleClothingActor(void);

	virtual void tick(float dtime, bool rewriteBuffers);
	virtual void render(bool rewriteBuffers);
	virtual int getType()
	{
		return TYPE;
	}

	void setGlobalPose(const physx::PxMat44& pose)
	{
		mGlobalPose = pose;
	}
	const physx::PxMat44& getGlobalPose() const
	{
		return mGlobalPose;
	}

	void setParent(SampleActor* parent)
	{
		mParent = parent;
	}

	void setGlobalPoseMotion(bool on)
	{
		mShouldMoveGlobalPose = on;
	}

	void updateState(const physx::PxMat44& globalPose, const physx::PxMat44* boneMatrices, int numBoneMatrices, bool isContinuous);

	int changeRenderLOD();

	void forcePhysicalLOD(physx::PxF32 lod);

	void setWind(physx::PxF32 windAdaption, physx::PxVec3 windVelocity);
	void setWindNoise(physx::PxF32 adaptionChange, physx::PxF32 velocityConeAngleDegree);

	void setLocalSpace(bool on);
	bool isLocalSpace();

	physx::apex::NxClothingActor* getClothingActor()
	{
		return mClothingActor;
	}

	enum { TYPE = 9}; // could be larger, just to be safe
private:
	void init(physx::apex::NxApexScene& apexScene, const physx::PxVec3* morphDisplacement, physx::PxU32 numMorphDisplacement);

	void fillSkinningMatrices();

	std::vector<physx::PxU32>				mBoneRemapTable;
	std::vector<physx::PxMat44>				mSkinningMatrices;


	SampleActor*							mParent;
	physx::apex::NxClothingAsset&			mAsset;
	physx::apex::NxClothingActor*			mClothingActor;
	physx::apex::NxUserRenderer*			mApexRenderer;
	physx::PxF32							mTime;
	physx::PxMat44							mGlobalPose;
	physx::PxF32							mScale;
	bool									mShouldMoveGlobalPose;

	bool									mIsContinuous;

	bool									mUseGPU;
	bool									mUseLocalSpace;

	physx::PxI32							mNumGraphicalLods;
	physx::PxU32							mLodIndex;
	std::vector<physx::PxU32>				mLods;
	bool									mIncreaseLod;

	SampleRandomWind*						mRandomWind;
};

#endif // SAMPLE_CLOTHING_ACTOR_H
