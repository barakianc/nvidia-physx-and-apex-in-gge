// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "NxApexDefs.h"
#include <SampleActor.h>
#include <RendererMeshContext.h>

class SimpleTerrainData;

namespace physx
{
namespace apex
{
class NxApexScene;
class NxApexSDK;
}
}

#if NX_SDK_VERSION_MAJOR == 2
class NxActor;
class NxCookingInterface;
class NxTriangleMesh;
#elif NX_SDK_VERSION_MAJOR == 3
namespace physx
{
class PxActor;
class PxCooking;
class PxTriangleMesh;
class PxMaterial;
}
#endif

namespace SampleRenderer
{
class Renderer;
class RendererMaterial;
class RendererMaterialInstance;
class RendererTerrainShape;
}

namespace SampleFramework
{
class SampleAssetManager;
class SampleMaterialAsset;
}

class SampleTerrainActor : public SampleFramework::SampleActor
{
public:
	SampleTerrainActor(SimpleTerrainData* terrainData,
	                   SampleRenderer::Renderer& renderer,
	                   SampleFramework::SampleAssetManager* assetManager,
	                   physx::apex::NxApexScene& apexScene,
	                   physx::apex::NxApexSDK* apexSDK,
	                   physx::PxU16 materialIndex,
	                   physx::PxU16 collisionGroup);

	virtual ~SampleTerrainActor(void);

	virtual void render(bool rewriteBuffers);

	virtual int getType()
	{
		return TerrainActor;
	}

	enum { TerrainActor = 4 };

private:
	physx::apex::NxApexSDK*						m_apexSDK;
	physx::apex::NxApexScene*					m_apexScene;
	SampleRenderer::Renderer&					m_renderer;
	SampleFramework::SampleMaterialAsset*		groundMat;
	SampleRenderer::RendererMaterial*			m_rendererMaterial;
	SampleRenderer::RendererMaterialInstance*	m_rendererMaterialInstance;
	SampleRenderer::RendererMeshContext			m_renderMeshContext;
	SimpleTerrainData*							m_terrainData;
	SampleRenderer::RendererTerrainShape*		m_renderShape;
	SampleFramework::SampleAssetManager*		m_assetManager;
#if NX_SDK_VERSION_MAJOR == 2
	NxActor*									m_actor;
	NxCookingInterface*							m_physxCooking;
	NxTriangleMesh*								m_terrainMesh;
#elif NX_SDK_VERSION_MAJOR == 3
	physx::PxActor*								m_actor;
	physx::PxCooking*							m_physxCooking;
	physx::PxTriangleMesh*						m_terrainMesh;
	physx::PxMaterial*							m_material;
#endif
};
