// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_ANIMATED_MESH_ASSET_H
#define SAMPLE_ANIMATED_MESH_ASSET_H

#include <UserAllocator.h>
#include <string>

#include "TriangleMesh.h"

namespace physx
{
namespace apex
{
class NxClothingAsset;
class NxResourceCallback;
}
}

namespace Samples
{
class SkeletalAnim;
class TriangleMesh;
}

class SampleAnimatedMeshAsset
{
public:
	SampleAnimatedMeshAsset(const std::string& filename);
	virtual ~SampleAnimatedMeshAsset();

	Samples::TriangleMesh* getTriangleMesh()
	{
		return mTriangleMesh;
	}
	Samples::SkeletalAnim* getSkeletalAnim()
	{
		return mSkeletalAnim;
	}

	void hideByMaterialName(const char* materialName);
	bool loadAnimation(const char* animationFile);
	void loadMaterials(physx::apex::NxResourceCallback* resourceCallback, physx::apex::NxUserRenderResourceManager* rrm,
	                   const char* materialPrefix, const char* materialSuffix);

	static void setPrefixPath(const char* prefix)
	{
		mPrefixPath = prefix;
	}

private:
	bool loadMeshImport(std::string realName, bool appendAnimation);

	static const char* mPrefixPath;

	Samples::TriangleMesh* mTriangleMesh;
	Samples::SkeletalAnim* mSkeletalAnim;
};

#endif
