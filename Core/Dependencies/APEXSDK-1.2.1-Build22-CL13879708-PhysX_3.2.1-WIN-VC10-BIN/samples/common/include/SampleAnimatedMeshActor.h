// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_ANIMATED_MESH_ACTOR_H
#define SAMPLE_ANIMATED_MESH_ACTOR_H

#include <SampleActor.h>

#include "PxMat34Legacy.h"
#include "TriangleMesh.h"

namespace physx
{
namespace apex
{
class NxUserRenderResourceManager;
}
}

class SampleAnimatedMeshAsset;
class SampleClothingActor;

namespace Samples
{
class SkeletalAnim;
class TriangleMesh;
}


class SampleAnimatedMeshActor : public SampleFramework::SampleActor
{
public:
	SampleAnimatedMeshActor(const physx::PxMat44& globalPose, SampleAnimatedMeshAsset* meshAsset,
	                        physx::apex::NxUserRenderResourceManager* renderResourceManager,
	                        physx::apex::NxUserRenderer* apexRenderer, SampleRenderer::Renderer* renderer);
	virtual ~SampleAnimatedMeshActor();

	virtual void tick(float dtime, bool rewriteBuffers);
	virtual void render(bool rewriteBuffers);
	virtual int getType()
	{
		return TYPE;
	}
	virtual void addFollower(SampleClothingActor* follower);

	void setAnimationTimeScale(float s)
	{
		mAnimationTimeScale = s;
	}
	void setSecondsUntilRestart(float s)
	{
		mSecondsUntilRestart = s;
	}
	void setContinuousAnimation(bool on)
	{
		mContinuousAnimation = on;
	}

	const Samples::SkeletalAnim* getSkeletalAnim()
	{
		return mSkeletalAnim;
	}

	void applyMorphTarget(const std::vector<physx::PxVec3>& displacements);

	enum { TYPE = 100 };
private:

	physx::PxMat44 mGlobalPose;

	physx::apex::NxUserRenderResourceManager* mRenderResourceManager;
	physx::apex::NxUserRenderer* mApexRenderer;

	SampleAnimatedMeshAsset* mParent;
	Samples::TriangleMesh* mTriangleMesh;
	Samples::SkeletalAnim* mSkeletalAnim;

	std::vector<SampleClothingActor*> mFollowers;

	float mTimePassed;

	bool mContinuousAnimation;
	float mSecondsUntilRestart;
	float mAnimationTimeScale;
};


#endif
