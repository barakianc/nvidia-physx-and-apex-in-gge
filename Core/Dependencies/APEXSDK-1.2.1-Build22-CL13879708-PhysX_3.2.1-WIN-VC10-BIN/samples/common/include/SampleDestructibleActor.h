// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#ifndef SAMPLE_DESTRUCTIBLE_ACTOR_H
#define SAMPLE_DESTRUCTIBLE_ACTOR_H

#include <SampleActor.h>

namespace physx
{
namespace apex
{
class NxApexScene;

class NxDestructibleAsset;
class NxDestructibleActor;
}
}
namespace NxParameterized
{
class Interface;
};

namespace SampleRenderer
{
class Renderer;
}

class SampleDestructibleActor : public SampleFramework::SampleActor
{
public:
    SampleDestructibleActor(SampleRenderer::Renderer& renderer, physx::apex::NxApexScene& apexScene,
        physx::apex::NxDestructibleAsset& asset, NxParameterized::Interface* descParams, bool ownsParams = false,
        const physx::PxVec3& vel = physx::PxVec3(0.0f), const physx::PxVec3& angVel = physx::PxVec3(0.0f));

    static void initWithProjectileParams(NxParameterized::Interface* descParams);

    virtual ~SampleDestructibleActor(void);

	virtual void render(bool rewriteBuffers);

	void breakIt();

	physx::PxVec3 getPosition();
	const physx::apex::NxDestructibleActor* getActor() const
	{
		return m_actor;
	}
	physx::apex::NxDestructibleActor* getActor()
	{
		return m_actor;
	}

	virtual int getType()
	{
		return DestructibleActor;
	}

	enum { DestructibleActor = 2 };

private:
	SampleRenderer::Renderer&			m_renderer;
	physx::apex::NxDestructibleAsset&	m_asset;
	physx::apex::NxDestructibleActor*	m_actor;
};

#endif // SAMPLE_DESTRUCTIBLE_ACTOR_H
