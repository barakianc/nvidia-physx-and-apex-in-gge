// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#ifndef COMM_TOOL_H
#define COMM_TOOL_H

#include "CommunicationsTool.h"

#ifdef COMM_TOOL_VALID

#include <assert.h>

#include "PsFoundation.h"
#include "PsIPC.h"
#include "PsThread.h"
#include "PsSocket.h"
#include "PsRingBuffer.h"

#define DECLARE_DISABLE_COPY_AND_ASSIGN(Class) private: Class(const Class &); Class & operator = (const Class &);

namespace CommTool
{
class CommLayer : public CommInterface
{
public:
	CommLayer(const ConnectionProperty * connectionProperties, unsigned int connectionCount);
    void release();
    bool send(const void * bufferStart, physx::PxU32 bufferSize, physx::PxU32 connectionIndex);
    bool receive(void *& bufferStart, physx::PxU32 & bufferSize, physx::PxU32 connectionIndex);
private:
    CommLayer();
	~CommLayer();
	DECLARE_DISABLE_COPY_AND_ASSIGN(CommLayer);
private:
	const physx::PxU32		connectionCount;
    physx::shdfnd::PxIPC **	mIPC;
};

namespace
{
	CommInterface * instance = NULL;
}; //namespace nameless

CommInterface * createCommInstance(const ConnectionProperty * connectionProperties, unsigned int connectionCount)
{
	return (NULL == instance ? static_cast<CommInterface*>(::new CommLayer(connectionProperties, connectionCount)) : NULL);
}
}; // namespace CommTool

// all these used to be in John's PsSocketIPC.h, which got removed!
namespace physx
{
namespace shdfnd
{
class ReadThread : public Thread
{
public:
	ReadThread();
	~ReadThread();
	void init(Socket *socket,PxRingBuffer *reader,PxU32 maxReadSize);
	void execute();
private:
	DECLARE_DISABLE_COPY_AND_ASSIGN(ReadThread);
	bool					mExit;
	PxU32					mReadSize;
	PxU8					*mReadBuffer;
	PxRingBuffer			*mRead;				// The ring buffer we use to read data
	Socket					*mSocket;
};

class SendThread : public Thread
{
public:
	SendThread();
	~SendThread();
	void init(Socket *socket,PxRingBuffer *writer,PxU32 maxSendSize);
	void execute();
private:
	DECLARE_DISABLE_COPY_AND_ASSIGN(SendThread);
	bool					mExit;
	PxU32					mSendSize;
	PxU8					*mSendBuffer;
	PxRingBuffer			*mWrite;				// The ring buffer we use to read data
	Socket					*mSocket;
};

class SocketIPC : public PxIPC, public UserAllocated
{
public:
	SocketIPC(PxIPC::ErrorCode &errorCode,
		PxIPC::ConnectionType connectionType,
		const char *socketAddress,
		PxU16	portNumber,
		PxU32 serverRingBufferSize,
		PxU32 clientRingBufferSize,
		bool allowLongMessages,
		bool bufferSends,
		PxU32 maxBufferSendSize);
	~SocketIPC();
	void release();
	PxIPC::ErrorCode sendData(const void *data,PxU32 data_len,bool bufferIfFull);
	const void * receiveData(PxU32 &data_len);
	bool isServer() const;
	PxIPC::ErrorCode receiveAcknowledge();
	bool pumpPendingSends();
	bool haveConnection() const;
	bool canSend(PxU32 len);
private:
	DECLARE_DISABLE_COPY_AND_ASSIGN(SocketIPC);
	Socket					mSocket;
	bool					mIsServer;
	ReadThread			mReadThread;
	SendThread				mSendThread;
	PxRingBuffer			mServerRingBuffer;
	PxRingBuffer			mClientRingBuffer;
	PxRingBuffer			*mWrite;
	PxRingBuffer			*mRead;
};
}; // namespace shdfnd
}; // namespace physx
#endif // COMM_TOOL_VALID
#endif // COMM_TOOL_H
