// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_APEX_APPLICATION_H
#define SAMPLE_APEX_APPLICATION_H

#include "NxApex.h"

#include "SampleApplication.h"
#include "SampleAssetManager.h"
#include "SampleMaterialAsset.h"
#include "SampleApexResourceCallback.h"
#include "SampleCommandLine.h"
#include "SampleActor.h"
#include "SampleShapeActor.h"
#include "SampleAverage.h"
#include "SampleCommandLineInputs.h"
#include "RendererShape.h"
#include "RendererProjection.h"
#include "OpenAutomateInterface.h"

#include "UserAllocator.h"
#include "UserErrorCallback.h"
#include "PsString.h"
#include "PsTime.h"

#include "physxvisualdebuggersdk/PvdConnectionFlags.h"
#include "NxApexScene.h"

#if NX_SDK_VERSION_MAJOR == 3 && defined(PX_PS3)
#include "SampleUserInput.h"
class PxDefaultSpuDispatcher;
#endif

namespace physx
{
namespace pxtask
{
class CpuDispatcher;
class CudaContextManager;
}
}

enum SampleShapeType
{
	SphereShapeType,
	CapsuleShapeType,
	BoxShapeType,
	HalfSpaceShapeType,
	ConvexShapeType,
	TriMeshShapeType
};

PX_INLINE bool empty(const char* buffer) 
{
	return NULL == buffer || buffer[0] == '\0';
}
PX_INLINE bool newline(const char* buffer) 
{
	return NULL != buffer && buffer[0] == '\n';
}

class SampleApexApplication : public SampleFramework::SampleApplication
#if NX_SDK_VERSION_MAJOR == 3
							, public physx::NxApexPhysX3Interface
#endif
{
public:
	SampleApexApplication(const SampleFramework::SampleCommandLine& cmdline, physx::PxI32 camMoveButton = -1);
	virtual ~SampleApexApplication();

	virtual void onInit() {}
	virtual void onOpen();
	virtual void onTickPreRender(float dtime);
	virtual void onRender() {}
	virtual void onTickPostRender(float dtime);
	virtual float tweakElapsedTime(float dtime);

	void registerInputEvents();
	void unregisterInputEvents();

	virtual void collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents);
	virtual void collectInputDescriptions(std::vector<const char*>& inputDescriptions);
	virtual void onAnalogInputEvent(const SampleFramework::InputEvent& , float val);
	virtual bool onDigitalInputEvent(const SampleFramework::InputEvent& , bool val);
	virtual void onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy);
	virtual void onShutdown();

	// Samples should implement these methods
	virtual void updateCollisionObjectSceneInfo() {}
	virtual void updateScalableAndLodInfo() {}
	virtual void collisionObjectAdded(physx::apex::NxApexScene& /*scene*/, SampleShapeActor& /*shapeActor*/) {}
#if NX_SDK_VERSION_MAJOR == 2
	virtual void onMouseClickShapeHit(const physx::PxVec3& /*pickDir*/, NxRaycastHit& /*hit*/, NxShape*) {}
#elif NX_SDK_VERSION_MAJOR == 3
	virtual void onMouseClickShapeHit(const physx::PxVec3& /*pickDir*/, physx::PxRaycastHit& /*hit*/, physx::PxShape*) {}
#endif

	void sampleInit(const char* sampleName, bool useCuda, bool useCudaInterop, size_t gpuTotalMemLimit = 0);
	physx::apex::NxModule*    loadModule(const char* moduleName);
	physx::apex::NxApexAsset* loadApexAsset(const char* nameSpace, const char* name, SampleAssetFileType assetType = ANY_ASSET);
	void                      releaseApexAsset(physx::apex::NxApexAsset*);

	// must be called after the SampleRenderer::Renderer::render() call!!
	void printSceneInfo(const char* sceneInfo = "", 
	                    const char* additionalInfo = "",
                        bool bPrintFPS = true, bool bPrintDebug = true);
	void printToScreen(const char* output, const physx::apex::NxApexScene* sceneForStats, bool recomputeAverages);

	void openConsole(const char* windowTitle, const char* outputFile);

	void updateApexSceneMatrices();

#if NX_SDK_VERSION_MAJOR == 3
	// NxApexPhysX3Interface
	virtual void setContactReportFlags(physx::PxShape* shape, physx::PxPairFlags flags);
	virtual physx::PxPairFlags getContactReportFlags(const physx::PxShape* shape) const;
#endif
	/* Debug Rendering methods */
	struct DebugRenderFlag
	{
#if NX_SDK_VERSION_MAJOR == 2
		DebugRenderFlag(const char* name, NxParameter param, float v = 0.0f)
			: nxParameter(param)
			, fullName(name)
			, apexParameter(NULL)
			, defaultValue(v)
		{
		}
		NxParameter	nxParameter;
#elif NX_SDK_VERSION_MAJOR == 3
		DebugRenderFlag(const char* name, physx::PxVisualizationParameter::Enum param, float v = 0.0f)
			: pxParameter(param)
			, fullName(name)
			, apexParameter(NULL)
			, defaultValue(v)
		{
		}
		physx::PxVisualizationParameter::Enum pxParameter;
#endif

		DebugRenderFlag(const char* name, const char* apexName, float v = 0.0f)
			:
#if NX_SDK_VERSION_MAJOR == 2
			nxParameter(NX_PARAMS_NUM_VALUES),
#elif NX_SDK_VERSION_MAJOR == 3
			pxParameter(physx::PxVisualizationParameter::eNUM_VALUES),
#endif
			fullName(name),
			apexParameter(apexName),
			defaultValue(v)
		{
		}

		const char*	fullName;
		const char*	apexParameter;
		float		defaultValue;
	};
	struct DebugRenderConfiguration
	{
		std::vector<DebugRenderFlag> flags;
	};

	void addDebugRenderConfig(const DebugRenderConfiguration& config);
	void applyDebugRenderConfig(size_t index, physx::apex::NxApexScene* apexScene, float scale);
	int applyApexDebugRenderFlag(physx::apex::NxApexScene* apexScene, const char* name, float value);
	void nextDebugRenderConfig(int change);
	void clearDebugRenderConfig();
	std::string getDebugRenderConfigName();

	/* OpenAutomate methods */
	virtual displayResType::Enum processDisplayOption(const char* str);
	virtual void processEarlyCmdLineArgs(const char* ProgramVersion = openAutomateInterface::defaultProgramVersionString, oa_OptionsOverrideDesc* desc = NULL);
	virtual void reportOaResults();
	virtual void reportOaResultsValues();
	virtual int getDisplayWidth();
	virtual int getDisplayHeight();
	virtual bool rendererOverride();
	virtual void initBenchmarks(void (*m_BenchmarkStart)(const char*));

	float getPhysicalLod() const
	{
		return m_physicalLOD;
	}

	void setPVDConnectionFlags(physx::debugger::TConnectionFlagsType flags)
	{
		m_pvdConnectionFlags = flags;
	}

	physx::debugger::TConnectionFlagsType getPVDConnectionFlags()
	{
		return m_pvdConnectionFlags;
	}

	void setCpuThreadCount(unsigned int cpuThreadCount)
	{
		m_cpuThreadCount = cpuThreadCount;
	}

	unsigned int getCpuThreadCount()
	{
		return m_cpuThreadCount;
	}
	bool useApplicationProfiler(void) const
	{
		return false;
	}
	void setApexStatDisplay(bool enabled)
	{
		m_displayApexStats = enabled;
	}

	bool getApexStatDisplay()
	{
		return m_displayApexStats;
	}

	void setAssetPreference(SampleAssetFileType pref)
	{
		m_assetPreference = pref;
	}

	void setPaused(bool pause)
	{
		m_pause = pause;
	}

	const std::vector<SampleFramework::SampleActor*>& getActors() const 
	{
		return m_actors;
	}

	std::vector<SampleFramework::SampleActor*>& getActors() 
	{
		return m_actors;
	}

protected:
	virtual unsigned int getNumScenes() const = 0;
	virtual unsigned int getSelectedScene() const = 0;
	virtual const char* getSceneName(unsigned int sceneNumber) const = 0;
	virtual void selectScene(unsigned int sceneNumber) = 0;

	bool isInputEventActive(physx::PxU16 inputEventId);
	
	void showHelpPopup(const std::string& helpText, physx::PxF32 duration = 3.0f);
	void printHelp(physx::PxU32 x, physx::PxU32 y);
	void printSceneSelection(physx::PxU32 x, physx::PxU32 y);

	const char* inputInfoMsg(physx::PxI32 inputEventId1, physx::PxI32 inputEventId2 = -1);
	bool hasInputCommand(unsigned id) const          { return m_commandLineInput.hasInputCommand(id); }
	const char* inputCommandValue(unsigned id) const { return m_commandLineInput.inputCommandValue(id); }
	void print(physx::PxU32 x, physx::PxU32 y,
	           const char* text,
	           physx::PxReal scale, physx::PxReal shadowOffset,
	           SampleRenderer::RendererColor textColor,
	           bool forceFixWidthNumbers) const;
	void printCommandHelp() const;
	void printUnusedCommands() const;

	bool cudaSupported() const;
	bool cudaEnabled() const;
	bool interopSupported() const;
	bool interopEnabled() const;
	void setBenchmarkEnable(bool enabled);

	/* Application Singletons */
	physx::apex::NxApexSDK*				m_apexSDK;
	physx::apex::NxApexRenderDebug*		m_apexRenderDebug;
	SampleApexResourceCallback*			m_resourceCallback;
	physx::apex::NxUserRenderResourceManager*	m_renderResourceManager;
	physx::apex::NxUserRenderer*		m_apexRenderer;
	std::string							m_recordRrmFile;
	UserPxAllocator*					m_allocator;
	UserErrorCallback*					m_errorCallback;
	physx::pxtask::CpuDispatcher*		m_cpuDispatcher;
	physx::pxtask::CudaContextManager*  m_cudaContext;
# if defined(PX_PS3) && NX_SDK_VERSION_MAJOR == 3
	PxDefaultSpuDispatcher*				m_spuDispatcher;	
# endif
	SampleRenderer::RendererShape*      m_unitBox;
	SampleFramework::SampleMaterialAsset* m_simpleLitMaterial;
	SampleFramework::SampleMaterialAsset* m_simpleUnlitMaterial;
	SampleFramework::SampleMaterialAsset* m_simpleLitColorMaterial;
	SampleFramework::SampleMaterialAsset* m_simpleTransparentMaterial;
#if defined(RENDERER_TABLET)	
	SampleFramework::SampleMaterialAsset* m_controlMaterialAsset;
	SampleFramework::SampleMaterialAsset* m_buttonMaterialAsset;
#endif

	SampleRenderer::RendererProjection	m_projection;
	bool								m_pause;
	bool								m_step;
	std::string							m_sampleName;
	std::string							m_resourceDir;
	std::string							m_pvdHostName;
	physx::debugger::TConnectionFlagsType		m_pvdConnectionFlags;
	unsigned int						m_cpuThreadCount;
	bool								m_enableGpuPhysX;
	bool								m_simulateRigidBodiesOnGPU;
	physx::apex::NxApexScene*			m_apexScene;
	float								m_collisionObjRadius;
	float          						m_simulationBudget;
	SampleAssetFileType					m_assetPreference;
	std::vector<physx::apex::NxModule*>			m_apexModuleList;
	std::vector<SampleRenderer::RendererLight*>	m_lights;
	std::vector<SampleFramework::SampleActor*>	m_actors;
	std::vector<DebugRenderConfiguration>		m_debugRenderConfigs;
	size_t										m_currentDebugRenderConfig;
	std::vector<SampleShapeActor*>				m_shape_actors;

	/* Open Automate Variables */
	openAutomateInterface*				m_OaInterface;
	displayResType::Enum	    		m_displayResolution;
	unsigned int						m_benchmarkNumFrames;
	unsigned int						m_benchmarkNumActors;
	unsigned int						m_benchmarkMaxHitCount;
	unsigned int						m_FrameNumber;
	float								m_BenchmarkElapsedTime;
	physx::Time						    m_BenchMarkTimer;
	float                				m_physicalLOD;
	void (*m_BenchmarkStart)(const char*);
	bool								m_shuttingDown;
	bool								m_BenchmarkMode;		// an OA or a cmd line benchmark is running
	bool								m_CmdLineBenchmarkMode;	// a cmd line benchmark is running
	bool								m_dataCollectionStarted;
	float           					m_dataCollectionStartTime;
	std::vector<const char*>			m_benchmarkNames;

	/* PhysX version specific APIs, found in SampleApexApplicationPhysX?.cpp */
	virtual bool						initSDKs(bool useApplicationProfiler);
	virtual void						shutdownSDKs();
	virtual void						createThreadPool(unsigned int numThreads);
	virtual void                        createCudaContext(bool useInterop, size_t gpuTotalMemLimit);
	virtual physx::apex::NxApexScene*   createSampleScene(const physx::PxVec3& gravity);
	virtual void						shutdownScene(physx::apex::NxApexScene&);
	virtual SampleShapeActor*			addCollisionShape(SampleShapeType type, const physx::PxVec3& pos, const physx::PxVec3& vel, const physx::PxVec3& extents);
	virtual void						updateCollisionShapes(float dtime);
	virtual void						doMouseClickRaycast(const physx::PxVec3& eyePos, const physx::PxVec3& pickDir);

#if NX_SDK_VERSION_MAJOR == 2
	NxPhysicsSDK*						m_physxSDK;
	NxCookingInterface*					m_physxCooking;
	NxGroupsMask						m_shapeGroupMask;
	physx::apex::NxApexScene*           createSampleScene(const NxSceneDesc& desc);
#elif NX_SDK_VERSION_MAJOR == 3
	physx::PxProfileZoneManager*		m_ProfileZoneManager;
	physx::PxFoundation*				m_Foundation;
	physx::PxPhysics*					m_physxSDK;
	physx::PxCooking*					m_physxCooking;
	physx::PxMaterial*					m_material;


#endif

	typedef SampleAverage<physx::PxF32, 100> SampleAvg;
	std::vector<SampleAvg*>				m_statAverages;
	bool								m_displayApexStats;

	bool								computeFps(float& fps);
	void								setBudget(float newBudget);
	physx::apex::NxApexAsset*			tryLoadApexAsset(const char* nameSpace, const char* name, const char* ext);
	void								forceLoadAssets();
	void								clearAverageStats();

	/* File Export */
	FILE*						        m_outFile;
	void						        dumpApexStats(const physx::apex::NxApexScene* sceneForStats);
	void						        closeApexStatsFile(void);

	/* Stdout redirect */
	FILE*								m_stdOutReplacement;
	bool								m_dumpPerfData;
	bool								m_vsync;
	bool								m_allowCreateObject;
	
	unsigned int						m_sceneNumberSelection;

	enum DisplayState
	{
		DISPLAY_NONE,
		DISPLAY_HELP_POPUP,
		DISPLAY_HELP_MESSAGE,
		DISPLAY_SCENE_SELECTION,
	};
	DisplayState						m_displayState;
	bool								m_fadeOutDisplay;
	std::string							m_helpPopupText;
	physx::PxU16						m_helpPage;
	physx::PxReal						m_textAlpha;

	SampleCommandLineInput				m_commandLineInput;
};

#endif
