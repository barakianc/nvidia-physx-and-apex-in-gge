// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_INPUT_EVENT_IDS_H
#define SAMPLE_INPUT_EVENT_IDS_H

#include <SampleFrameworkInputEventIds.h>

// InputEvents used by SampleApexApplication
enum SampleInputEventIds
{
	SAMPLE_BASE_FIRST = NUM_SAMPLE_FRAMEWORK_INPUT_EVENT_IDS,

	CREATE_OBJECT = SAMPLE_BASE_FIRST,

	PAUSE_SAMPLE ,
	STEP_ONE_FRAME ,
	NEXT_VISUALIZATION,
	PREV_VISUALIZATION,
	DECREASE_COLLISION_RADIUS,
	INCREASE_COLLISION_RADIUS,
	DECREASE_SIM_BUDGET,
	INCREASE_SIM_BUDGET,
	TOGGLE_WIREFRAME,
	TOGGLE_PVD_CONNECTION,
	SHOW_SCENE_STATS,

	SCENE_SELECT,
	SCENE_MENU,
	SAMPLE_ESCAPE,

	SHOW_HELP,

	MOUSE_SELECTION_BUTTON,

	TOGGLE_CONSOLE,
	CONSOLE_ESCAPE,
	CONSOLE_SCROLL_UP,
	CONSOLE_SCROLL_DOWN,
	CONSOLE_LIST_COMMAND_UP,
	CONSOLE_LIST_COMMAND_DOWN,

	GAMEPAD_BUTTON_START,
	GAMEPAD_BUTTON_MENU_SELECT,
	GAMEPAD_TRIGGER_LEFT,
	GAMEPAD_TRIGGER_RIGHT,

	NEXT_PAGE,
	PREVIOUS_PAGE,

	NEXT_SCENE,
	PREVIOUS_SCENE,

	DUMP_ALLOCATED_MEMORY,

	MODIFIER_SHIFT,
	MODIFIER_CONTROL,

	EDIT_PARTICLE_SYSTEM,
	SAVE_PARTICLE_SYSTEM,
	SHOW_SOFT_INPUT,

	NUM_SAMPLE_BASE_INPUT_EVENT_IDS,
	NUM_EXCLUSIVE_SAMPLE_BASE_INPUT_EVENT_IDS = NUM_SAMPLE_BASE_INPUT_EVENT_IDS - SAMPLE_BASE_FIRST
};

// InputEvent descriptions used by SampleApexApplication
const char* const SampleInputEventDescriptions[] =
{
	"create an object (+shift for sphere, +control for capsule)",

	"pause the sample",
	"step one frame", 
	"next debug visualization",
	"prev debug visualization",
	"decrease the actor collision radius (hold down (shift) first)",
	"increase the actor collision radius (hold down (shift) first)",
	"decrease the simulation LOD budget",
	"increase the simulation LOD budget",

	"toggle wireframe mode",
	"toggle the PVD connection" ,
	"show scene stats" , 

	"change the scene",
	"show scene selection menu",
	"quit the sample",

	"show the help menu",

	"activate the target selection",

	"toggle console display",
	"close the console",
	"scroll up in the console",
	"scroll down in the console",
	"move up the console command list",
	"move down the console command list",

	"change the scene",
	"quit the sample",
	"", //"enable looking around with the camera",
	"", //"activate the target selection",

	"go to the next help page",
	"go the previous help page",

	"", // select the next scene
	"", // select the previous scene
	"dump report of currently allocated memory (available only in debug builds)",
	"", //"activate the shift modifier",
	"", //"activate the control modifier",
	"edit particle system",
	"save particle system",
	"keyboard"
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleInputEventDescriptions) == NUM_EXCLUSIVE_SAMPLE_BASE_INPUT_EVENT_IDS);

#endif
