// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#ifndef NX_COMM_TOOL_H
#define NX_COMM_TOOL_H

#if defined WIN32
#define COMM_TOOL_VALID
#endif // WIN32

#ifdef COMM_TOOL_VALID
namespace CommTool
{
class CommInterface
{
public:
    virtual void release() = 0;
    virtual bool send(const void * bufferStart, unsigned int bufferSize, unsigned int connectionIndex = 0) = 0;
    virtual bool receive(void *& bufferStart, unsigned int & bufferSize, unsigned int connectionIndex = 0) = 0;
protected:
    virtual ~CommInterface() {}
};

struct ConnectionType
{
    enum Enum
    {
        CREATE_AS_SERVER = 0,
        CREATE_AS_CLIENT,
		COUNT,
    };
};

struct ConnectionProperty
{
    ConnectionType::Enum	connectionType;
    const char *			socketAddress;
    unsigned short			portNumber;
	unsigned int			bufferSize;
};

//extern "C" __declspec(dllexport) CommInterface * createCommInstance(const ConnectionProperty * connectionProperties, unsigned int connectionCount = 1);
CommInterface * createCommInstance(const ConnectionProperty * connectionProperties, unsigned int connectionCount = 1);

}; // namespace CommTool
#endif // COMM_TOOL_VALID
#endif // NX_COMM_TOOL_H