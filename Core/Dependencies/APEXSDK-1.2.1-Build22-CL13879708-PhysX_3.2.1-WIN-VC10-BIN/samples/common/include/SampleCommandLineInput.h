// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_COMMANDLINE_INPUT_H
#define SAMPLE_COMMANDLINE_INPUT_H

#include <vector>
#include <string>
#include <sstream>
#include <map>

namespace SampleFramework
{
	class SampleCommandLine;
}

template<typename T>
inline bool convert(const char* str, T& strValue);

template<typename T>
inline T convertWithDefault(const char* str, T defaultValue);

struct CommandLineInput
{
	unsigned    mID;
	const char* mShortName;
	const char* mLongName;
	const char* mDesc;
};
typedef std::vector<CommandLineInput> CommandLineInputs;

struct ProcessedCommandLineInput 
{
	unsigned id() const       { return mInput.mID; }
	const char* value() const { return mValue; }

	CommandLineInput mInput;
	const char*      mValue;
};
typedef std::vector<ProcessedCommandLineInput> ProcessedCommandLineInputs;

class SampleCommandLineInput
{
public:
	SampleCommandLineInput(const SampleFramework::SampleCommandLine&);

	// Register valid commands for command line input
	//    This will add the valid command, and query the command line to see if that command was actually input
	void registerCommands( const CommandLineInput* inputs, unsigned count );
	void registerCommand( const CommandLineInput& input );

	// Retrieve a list of valid (possible) commands
	CommandLineInputs::const_iterator validCommandsBegin() const { return mValidCommands.begin(); }
	CommandLineInputs::const_iterator validCommandsEnd() const   { return mValidCommands.end(); }

	// Retrieve a list of commands that are both valid and were input from the command line
	ProcessedCommandLineInputs::const_iterator inputCommandsBegin() const { return mInputCommands.begin(); }
	ProcessedCommandLineInputs::const_iterator inputCommandsEnd() const   { return mInputCommands.end(); }

	// Returns whether the command is both a) valid and b) input from the command line
	bool        hasInputCommand( unsigned id ) const;

	// Returns the value of the input command, NULL if invalid or nonexistent
	const char* inputCommandValue( unsigned id ) const;

	// Converts the value of the input command to type T
	//   Returns true if the input command is present
	//   Otherwise, returns false and makes no assignment
	template<typename T>
	bool inputCommandValue( unsigned id, T& value ) const 
	{
		return convert( inputCommandValue(id), value );
	}

	// Converts the value of the input command to type T
	//  Returns the converted value if the command is present
	//  Otherwise, returns the specified default value and makes no assignment
	template<typename T>
	T inputCommandValueWithDefault( unsigned id, const T& defaultValue ) const 
	{
		return convertWithDefault( inputCommandValue(id), defaultValue );
	}

	// Retrieve a formatted string of all possible commands
	std::string validCommands( bool bReverseOrder = true ) const;

	// Retrieve a formatted string of valid commands that were input from the command line
	std::string inputCommands( bool bReverseOrder = true ) const;

private:
	void processCommand( const CommandLineInput& command );
	void addInputCommand( const CommandLineInput& command, const char* value );
	void processCommandLineArgs();

	typedef std::map<unsigned, size_t>        IDToIndexMap;
	
	CommandLineInputs                         mValidCommands;  // All valid commands
	ProcessedCommandLineInputs                mInputCommands;  // All input commands that are valid
	IDToIndexMap                              mCommandIDToProcessedCommandIndex; // Used in looking up input commands by ID
	const SampleFramework::SampleCommandLine& mCommandLine;    // A handle to the command line input
};


template<typename T>
bool convert(const char* str, T& strValue)
{
	if (NULL == str || str[0] == '\0')
		return false;

	std::stringstream ss(str);
	ss >> strValue;
	return true;
}

template<typename T>
T convertWithDefault(const char* str, T defaultValue)
{
	// If the conversion is successful, defaultValue is overridden
	convert(str, defaultValue);
	return defaultValue;
}

#endif
