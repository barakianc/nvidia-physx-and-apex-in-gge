// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_COMMANDLINE_INPUTS_H
#define SAMPLE_COMMANDLINE_INPUTS_H

#include <PsUtilities.h>
#include <SampleCommandLineInput.h>

#define ENABLE_COMMAND_LINE_INPUTS 1

// InputEvents used by SampleApexApplication
struct SampleCommandLineInputIds
{
	enum Enum
	{
		PREFER_APB = 0,
		PREFER_APX,
		D3D9,
		D3D11,
		INSTALL,
		UNINSTALL,
		CREATE_OA_FILE,
		OPEN_AUTOMATE,
		HELP,
		PVD_DEBUG,
		DISABLE_RENDERING,
		PHYSICAL_LOD,
		BUDGET,
		WINDOW,
		NUM_FRAMES,
		NUM_ACTORS,
		MAX_HIT_COUNT,
		CPU_THREAD_COUNT,
		NO_GPU_PHYSX,
		RECORD_RRM,
		DUMP_PERF_DATA,
		DISABLE_VSYNC,
		PROFILER,
		PVD_HOST,
		NO_MEM_TRACKER,
		NO_APEX_CUDA,
		NO_INTEROP,

		COUNT,
		COMMON_IDS_START = 0,
		COMMON_IDS_END   = NO_INTEROP,
		NUM_COMMON_IDS   = COMMON_IDS_END - COMMON_IDS_START + 1
	};
};
typedef SampleCommandLineInputIds SCLIDS;

// InputEvent descriptions used by SampleApexApplication
const CommandLineInput SampleCommandLineInputs[] =
{
	{ SCLIDS::PREFER_APB,          "prefer-apb",               "", "prefer apb(binary) assets to apx assets on name collisions" },
	{ SCLIDS::PREFER_APX,          "prefer-apx",               "", "prefer apx(xml) assets to apb assets on name collisions" },
	{ SCLIDS::D3D9,                "d3d9",                     "", "initializes the D3D9 renderer" },
	{ SCLIDS::D3D11,               "d3d11",                    "", "initializes the D3D11 renderer" },
	{ SCLIDS::INSTALL,             "install",                  "", "register the application with OA" },
	{ SCLIDS::UNINSTALL,           "uninstall",                "", "un-register the application with OA" },
	{ SCLIDS::CREATE_OA_FILE,      "createOaStatsOptionsFile", "", "create a template PhysX stats2 file" },
	{ SCLIDS::OPEN_AUTOMATE,       "openautomate",             "", "<dll_path>: run in OA mode" },
	{ SCLIDS::HELP,                "help",                     "", "print the help command on program startup" },
	{ SCLIDS::PVD_DEBUG,           "pvdDebug",                 "", "enable PVD debugging" },
	{ SCLIDS::DISABLE_RENDERING,   "disableRendering",         "", "disable sample renderer" },
	{ SCLIDS::PHYSICAL_LOD,        "physicalLod",              "", "set the physical LOD of the simulation" },
	{ SCLIDS::BUDGET,              "budget",                   "", "<res_budget>: set the APEX resource budget" },
	{ SCLIDS::WINDOW,              "window",                   "", "<window_size>: 640x480, 800x600, ... 1280x1024" },
	{ SCLIDS::NUM_FRAMES,          "numFrames",                "", "<num_frames>: specify the number of frames that a benchmark should be run" },
	{ SCLIDS::NUM_ACTORS,          "numActors",                "", "<num_actors>: specifies the number of actors for the benchmarks.  Must be between 1 and 81." },
	{ SCLIDS::MAX_HIT_COUNT,       "maxHitCount",              "", "<max_hit_count>: specify the maximum number of destruction events in hallmark" },
	{ SCLIDS::CPU_THREAD_COUNT,    "cpuThreadCount",           "", "<thread_count>: specifies the simulation thread count" },
	{ SCLIDS::NO_GPU_PHYSX,        "noGpuPhysX",               "", "disable GPU physics (otherwise will be enabled if a cuda-capable card is installed with a new enough driver)" },
	{ SCLIDS::RECORD_RRM,          "recordRRM",                "", "records the RRM file for offline replay" },
	{ SCLIDS::DUMP_PERF_DATA,      "dumpPerfData",             "", "write performance data from the simulation to a file" },
	{ SCLIDS::DISABLE_VSYNC,       "disableVsync",             "", "disables vsync for sample rendering" },
	{ SCLIDS::PROFILER,            "profiler",                 "", "(not yet supported)" },
	{ SCLIDS::PVD_HOST,            "pvdhost",                  "", "<pvd_hostname>: specifies the PVD host name" },
	{ SCLIDS::NO_MEM_TRACKER,      "noMemTracker",             "", "disable memory allocation tracking in supported builds (debug/checked)" },
	{ SCLIDS::NO_APEX_CUDA,        "noApexCuda",               "", "disable Cuda use within APEX" },
	{ SCLIDS::NO_INTEROP,          "noInterop",                "", "disable Cuda graphics interop" }
};

PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleCommandLineInputs) == SCLIDS::COUNT);

#endif
