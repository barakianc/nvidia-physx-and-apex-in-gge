// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef OPEN_AUTOMATE_INTERFACE_H
#define OPEN_AUTOMATE_INTERFACE_H

#include "NxApexDefs.h"
#if NX_SDK_VERSION_MAJOR == 2
#include "NxScene.h"
#include "NxSceneStats2.h"
#else
#include "PxScene.h"
#endif

#include "foundation/PxSimpleTypes.h"
#include "foundation/PxErrorCallback.h"
#include "OpenAutomate.h"

#include "PsAllocator.h"
#include "PsFile.h"
#include "PsTime.h"
#include "UserAllocator.h"
#include "PsUserAllocated.h"

#include "foundation/PxAssert.h"

#include "NxApexScene.h"
#include "OpenAutomate.h"
#include "SampleCommandLine.h"

#include <string>
#include <fstream>
#include <vector>
#include <map>

#define NOT_FOUND				(0xFFFFFFFF)
#define WINDOW_SIZE_STR			"Window Size"
#define RESOURCE_BUDGET_STR		"Resource Budget"
#define BMARK_NUM_FRAMES_STR	"Benchmark Frames"
#define NUM_ACTORS_STR			"Number of Actors"
#define NUM_CPU_THREADS_STR		"Number of PxTask CPU Threads"
#define STATS2_STR              "Stats2 Options"
#define GRB_ENABLE_STR			"Simulate rigid bodies on GPU"

#if defined PX_WINDOWS
#define	OA_OPTIONS_FILE_PREFIX ".\\"
#elif defined PX_X360
#define	OA_OPTIONS_FILE_PREFIX "DEVKIT:\\"
#elif defined PX_PS3
#define OA_OPTIONS_FILE_PREFIX "/app_home/"
#elif defined PX_ANDROID
#define OA_OPTIONS_FILE_PREFIX "/sdcard/"
/* TODO: just used the same value as in WinDef.h. Is it right? */
#define MAX_PATH 260
#endif

struct OAState
{
	/**
	\brief Enum of gamepad button
	*/
	enum Enum
	{
		NONE,		// OA is not specified on the command line
		IDLE,
		BENCHMARKING,
		RUNNING,
		EXIT,
	};
};

typedef struct
{
	physx::PxI32	width;
	physx::PxI32	height;
	const char* 	resolutionName;
} displayType;

struct displayResType
{
	/**
	\brief Enum of display resolutions
	*/
	enum Enum
	{
		res_640x480,
		res_800x600,
		res_1024x768,
		res_1152x864,
		res_1280x720,
		res_1280x768,
		res_1280x800,
		res_1280x960,
		res_1280x1024,
		res_1366x768,
		res_1440x900,
		res_1600x1200,
		res_1680x1050,
		res_1920x1080,
		res_1920x1200,
		NUM_DISPLAY_RESOLUTIONS,
	};
};

class oa_OptionsOverrideDesc
{
private:
	physx::PxU32 maxBudget;
	physx::PxU32 maxFrames;
	physx::PxU32 maxActors;
public:
	// default constructor to set the 
	oa_OptionsOverrideDesc(void)
	{
		maxBudget = 0;
		maxFrames = 0;
		maxActors = 0;
	}
	void setMaxBudget(physx::PxU32 budget)
	{
		maxBudget = budget;
	}
	void setMaxFrames(physx::PxU32 frames)
	{
		maxFrames = frames;
	}
	void setMaxActors(physx::PxU32 actors)
	{
		maxActors = actors;
	}
	physx::PxU32 getMaxBudget(void)
	{
		return(maxBudget);
	}
	physx::PxU32 getMaxFrames(void)
	{
		return(maxFrames);
	}
	physx::PxU32 getMaxActors(void)
	{
		return(maxActors);
	}
};

typedef std::map<std::string, bool> optionsMapType;

class openAutomateInterface : public physx::UserAllocated
{
public:
	// rather than repeating this string everywhere, provide it for the poor people that need something 
	// other than default arguments
	static const char * defaultProgramVersionString;

	openAutomateInterface(const char* openAutomateLibPath,
	                      const char* programPathname,
	                      void (*runNormalModePtr)(void),
	                      void (*benchmarkStartPtr)(const char*),
	                      std::vector<const char*>,
	                      const SampleFramework::SampleCommandLine* cmdline,
	                      const char* programVersionString = openAutomateInterface::defaultProgramVersionString,
						  oa_OptionsOverrideDesc* desc = NULL);
	openAutomateInterface(const char* programVersionString = openAutomateInterface::defaultProgramVersionString,
	                      const SampleFramework::SampleCommandLine* cmdline = NULL);
	~openAutomateInterface(void);

	OAState::Enum						getOaState(void);
	void								setOaState(OAState::Enum state);
	void								OAHandler(void);
	bool								RegisterAppItself(void);
	bool								UnRegisterAppItself(void);
	displayResType::Enum				getDisplayResolution(void);
	physx::PxU32						getSimulationBudget(void);
	static displayType					DisplayResolutions[displayResType::NUM_DISPLAY_RESOLUTIONS];
	physx::PxU32						getBenchmarkNumFrames(void);
	physx::PxU32						getBenchmarkNumActors(void);
	physx::PxU32						getBenchmarkNumCpuThreads(void);
	bool								getGRBenabled(void);
	char*                               getStats2Str(void);

	void								reportOaStatFloat(const char* name, float val);
	void								reportOaStatsOptions(void);
	void								reportOaResultsValues(void);
	void								readOaStatsOptionsFile(physx::apex::NxApexScene* apexScene);
	static void							setAllocatorForTracking(UserPxAllocator* allocator);
	void								endBenchmark(void);

#if NX_SDK_VERSION_MAJOR == 2
	bool								createOaStatsOptionsFile(NxScene* physxScene);
#endif

private:
	physx::Time							mOaTimer;
	physx::PxF64						mCumulativeTime;
	physx::PxU32						mCumulativeTimeIgnoreCount;
	static std::map<std::string, bool>	mOaStats2Options;
	static physx::apex::NxApexScene*	mOpenAutomateInterfaceApexScene;
#if NX_SDK_VERSION_MAJOR == 2
	static NxScene*						mOpenAutomateInterfacePhysxScene;
#endif
	static UserPxAllocator*				mAllocator;
	static physx::PxU32					StatName2Index(char* name);

	void								initMemberVars(void);
	void	(*mRunNormalModePtr)(void);
	void	(*mBenchmarkStartPtr)(const char* benchMark);
	OAState::Enum						mOAState;
	displayResType::Enum				mDispResolution;
	physx::PxU32						mSimulationBudget;			// the total APEX simulation budget
	const char*							mProgramVersionString;
	physx::PxU32						mBenchmarkNumFrames;		// number of frames to run the benchmark
	physx::PxU32						mNumActors;					// number of actors in the benchmark
	physx::PxU32						mNumCpuThreads;				// number of pxtask CPU threads
	oaBool								mEnableGRB;					// whether to simulate rigid bodies on the GPU
	std::vector<oaNamedOption>			openAutomateOptions;
	std::vector<const char*>			mBenchMarkNames;
	const SampleFramework::SampleCommandLine* mCmdline;				// the command line
	char*                               mStats2BufPtr;              // pointer to the stats2 options buffer

	void								ConvertOAValue(oaOptionDataType value_type, const oaValue* value, char* result);
	const char*							FormatCompareOp(oaComparisonOpType op_value);
	const char*							FormatDataType(oaOptionDataType value_type);
	void								GetAllOptions(void);
	void								GetCurrentOptions(void);
	void								InitOptions(oa_OptionsOverrideDesc* desc);
	void								PrintOptions(const oaNamedOption* option);
	displayResType::Enum				processDisplayOption(char* str);
	void								deleteOptionsFile(void);
	void								readOptionsFile(void);
	void								SetOptions(void);
	void								UseOptions(void);
	void								writeOptionsFile(void);
	char*								getKeyStr(char* keyStr);
	char*								getOptionFileName(char* optionFileName);
	char*								getBaseExeName(char* baseExeName);
	char*								getExeName(char* baseExeName);
	char*								getKeyRoot(char* baseExeName);
	char*								openFilePrefix;
};
#endif // OPEN_AUTOMATE_INTERFACE_H
