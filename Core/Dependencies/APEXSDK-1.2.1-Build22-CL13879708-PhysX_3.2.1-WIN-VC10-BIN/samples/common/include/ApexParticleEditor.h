// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef APEX_PARTICLE_EDITOR_H

#define APEX_PARTICLE_EDITOR_H

// This header file is provided as a demonstration to the developer on how to 
// incorporate the APEX parameter editor DLL plugins into their game engine.
//
// You should be able to use this source directly inside your own application as
// a starting point.
//
// Besides APEX it has one additional header file dependency and that is 'ApexParamEditor.h'
// This header file is located in:  .\tools\EditorWidgets\ApexParamEditor\include\ApexParamEditor.h
//
// This header file defines the API to access the DLL 'libapexparameditor_??_????.dll'
//
// The DLL plugin in 'libapexparameditor' manages the process of loading the editor widgets DLL
// and populating it with the data from the current APEX Emitter that you wish to edit.
//
// The DLL plugin 'libapexeditorwidgets' is a wxWidgets based editor that supports editing
// the contents of a collection of NxParameterized data objects; as well as a curve editor
// which makes it easy to edit a set of data points.


namespace physx
{
	namespace apex
	{
		class NxApexActor;
		class NxApexSDK;
		class NxApexScene;
		class NxApexEmitterAsset;
	};
};

// This is the wrapper class which provides helper support for editing APEX emitters
class ApexParticleEditor
{
public:
	// Your application must call this once each frame to allow the editor to perform it's work.
	// The pointer returned is the current NxApexActor that is being edited.
	// Many edit commands can cause the actor to be deleted and re-created, so your application
	// should not hold a persistent pointer to the original actor passed in; but rather update it
	// from the results of the 'pump' operation.
	// If the bool reference 'exitApp' is true, this indicates that the user has closed the editor
	// and you should exit 'editing' mode for this particle.
	virtual physx::apex::NxApexActor* pump(bool& exitApp) = 0;  // if pump returns false, then exit the application (close window)

	// This method spawns the editor.  If successful it will return true.
	// You should pass in the 'actor' which must be an apex emitter actor.  Also a pointer to the APEX SDK and the current APEX scene.
	virtual bool editApexActor(physx::apex::NxApexActor* actor, physx::apex::NxApexSDK* sdk, physx::apex::NxApexScene* scene) = 0;

	// Returns the current asset which is associated with this actor.
	virtual physx::apex::NxApexEmitterAsset* getApexEmitterAsset(void) const = 0;

	// releases the editor.
	virtual void releaseEditApexActor(void) = 0;

	// This method demonstrates how the contents of the edited assets can
	// be serialized and saved to disk as a series of XML format APX files.
	virtual bool saveApexAssets(void) = 0;

	// Call this method to release the editor interface.
	virtual void release(void) = 0;
protected:
	virtual ~ApexParticleEditor(void)
	{
	}
};

// This method will create the APEX Particle editor.  This feature only works on Windows.
ApexParticleEditor *createApexParticleEditor(void);

#endif
