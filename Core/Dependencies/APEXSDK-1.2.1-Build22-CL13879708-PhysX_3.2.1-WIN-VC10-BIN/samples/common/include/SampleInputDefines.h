// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_INPUT_DEFINES_H
#define SAMPLE_INPUT_DEFINES_H

#include "SamplePlatform.h"

PX_INLINE void inputEventDefImpl(std::vector<const SampleFramework::InputEvent*>& inputEvents, physx::PxU16 id, const char* name, physx::PxU16 key, bool analog)
{
#if defined(STRICT_INPUT_DEFINITIONS)
	const std::vector<size_t>* events = SampleFramework::SamplePlatform::platform()->getSampleUserInput()->getInputEvents(key);
	if (NULL == events)
	{
		const SampleFramework::InputEvent* retVal = 
			SampleFramework::SamplePlatform::platform()->getSampleUserInput()->registerInputEvent(SampleFramework::InputEvent(id, name, analog), key);
		if(retVal) inputEvents.push_back(retVal);
	}
	else
	{
		char msg[256];
		const std::vector<SampleFramework::UserInput>& eventList = SampleFramework::SamplePlatform::platform()->getSampleUserInput()->getUserInputList();
		sprintf(msg, "Key %s has already been used.", eventList[(*events)[0]].m_Name);
		RENDERER_ASSERT(0, msg);
	}
#else
	const SampleFramework::InputEvent* retVal = 
		SampleFramework::SamplePlatform::platform()->getSampleUserInput()->registerInputEvent(SampleFramework::InputEvent(id, name, analog), key);
	if(retVal) inputEvents.push_back(retVal);
#endif
}
PX_INLINE void touchInputEventDefImpl(std::vector<const SampleFramework::InputEvent*>& inputEvents, physx::PxU16 id, const char* name, physx::PxU16 key, const char* caption)
{
	const SampleFramework::InputEvent* retVal = 
		SampleFramework::SamplePlatform::platform()->getSampleUserInput()->registerTouchInputEvent(SampleFramework::InputEvent(id, name, false), key, caption);
	if(retVal) inputEvents.push_back(retVal);
}

#if defined(RENDERER_WINDOWS)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) winKey
#elif defined(RENDERER_XBOX360)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) xbox360key
#elif defined(RENDERER_PSP2)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) psp2Key
#elif defined(RENDERER_PS3)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) ps3Key
#elif defined(RENDERER_ANDROID)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) andrKey
#define SELECT_TOUCH_KEY(andrKey, iosKey) andrKey
#elif defined(RENDERER_MACOSX)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) osxKey
#elif defined(RENDERER_IOS)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) iosKey
#define SELECT_TOUCH_KEY(andrKey, iosKey) iosKey
#elif defined(RENDERER_LINUX)
#define SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) linuxKey
#endif

#define DIGITAL_INPUT_EVENT_DEF_IMPL(var, desc, key) inputEventDefImpl(inputEvents, var, desc, key, false)
#define ANALOG_INPUT_EVENT_DEF_IMPL(var, desc, key)  inputEventDefImpl(inputEvents, var, desc, key, true)

#if defined(RENDERER_ANDROID) || defined(RENDERER_IOS)
#define TOUCH_INPUT_EVENT_DEF_IMPL(var, caption, andrKey, iosKey) touchInputEventDefImpl(inputEvents, var, #var, SELECT_TOUCH_KEY(andrKey, iosKey), caption)
#else
#define TOUCH_INPUT_EVENT_DEF_IMPL(var, caption, andrKey, iosKey) 
#endif

#define DIGITAL_INPUT_EVENT_DEF_DESC(var, desc, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) \
	DIGITAL_INPUT_EVENT_DEF_IMPL(var, desc, SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey))
#define ANALOG_INPUT_EVENT_DEF_DESC(var, desc, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)  \
	ANALOG_INPUT_EVENT_DEF_IMPL(var, desc, SELECT_KEY(winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey))
#define TOUCH_INPUT_EVENT_DEF_DESC(var, caption, andrKey, iosKey) \
	TOUCH_INPUT_EVENT_DEF_IMPL(var, caption, andrKey, iosKey)

#define DIGITAL_INPUT_EVENT_DEF(var, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) \
	DIGITAL_INPUT_EVENT_DEF_DESC(var, #var, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)
#define ANALOG_INPUT_EVENT_DEF(var, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)  \
	ANALOG_INPUT_EVENT_DEF_DESC(var, #var, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)
#define TOUCH_INPUT_EVENT_DEF(var, caption, andrKey, iosKey) \
	TOUCH_INPUT_EVENT_DEF_DESC(var, caption, andrKey, iosKey)

#define DIGITAL_INPUT_EVENT_DEF_2(var, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey) \
	DIGITAL_INPUT_EVENT_DEF_DESC(var, inputDescriptions[var], winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)
#define ANALOG_INPUT_EVENT_DEF_2(var, winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)  \
	ANALOG_INPUT_EVENT_DEF_DESC(var, inputDescriptions[var], winKey, xbox360key, ps3Key, andrKey, osxKey, psp2Key, iosKey, linuxKey)
#define TOUCH_INPUT_EVENT_DEF_2(var, andrKey, iosKey) \
	TOUCH_INPUT_EVENT_DEF_DESC(var, inputDescriptions[var], andrKey, iosKey)

#endif
