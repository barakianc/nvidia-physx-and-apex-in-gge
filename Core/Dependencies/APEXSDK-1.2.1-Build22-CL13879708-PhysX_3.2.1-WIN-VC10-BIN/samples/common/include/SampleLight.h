// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#ifndef SAMPLE_LIGHT_H
#define SAMPLE_LIGHT_H

#if defined(PX_WINDOWS)
#define ENABLE_SHADOWS 1
#else
#define ENABLE_SHADOWS 0
#endif

#define SHADOW_RESOLUTION 1024

#include <PxMat34Legacy.h>
#include <RendererProjection.h>

namespace SampleRenderer
{
class Renderer;
class RendererLight;
class RendererTarget;
class RendererDirectionalLightDesc;
class RendererSpotLightDesc;
}


class SampleLight
{
public:
	SampleLight(SampleRenderer::Renderer& renderer, const SampleRenderer::RendererDirectionalLightDesc& lightDesc);
	SampleLight(SampleRenderer::Renderer& renderer, const SampleRenderer::RendererSpotLightDesc& lightDesc);
	virtual ~SampleLight();

	bool										supportsShadows(void)     const
	{
		return mShadowTarget && mShadowTexture ? true : false;
	}
	const physx::PxMat44&						getShadowTransform(void)  const
	{
		return mShadowTransform;
	}
	const SampleRenderer::RendererProjection&	getShadowProjection(void) const
	{
		return mShadowProjection;
	}
	SampleRenderer::RendererTarget*				getShadowTarget(void)     const
	{
		return mShadowTarget;
	}

public:
	SampleRenderer::RendererLight* getLight(void) const
	{
		return mLight;
	}

protected:
	SampleRenderer::RendererLight*		mLight;
	physx::PxMat44						mShadowTransform;
	SampleRenderer::RendererProjection	mShadowProjection;
	SampleRenderer::RendererTarget*		mShadowTarget;
	SampleRenderer::RendererTexture2D*	mShadowTexture;
	SampleRenderer::RendererTexture2D*	mShadowDepthTexture;
};



#endif // SAMPLE_LIGHT_H
