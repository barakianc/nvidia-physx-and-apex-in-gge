// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#ifndef SAMPLE_MORPH_TARGETS_H
#define SAMPLE_MORPH_TARGETS_H


#include <vector>
#include "foundation/PxVec3.h"

#include "Find.h"


class SampleApexResourceCallback;


class SampleMorphTargets : public physx::apex::FileHandler
{
public:
	SampleMorphTargets(SampleApexResourceCallback& resourceCallback) : mResourceCallback(resourceCallback), mTempPrefix(NULL), mTempDir(NULL) {}
	virtual ~SampleMorphTargets() {}

	bool loadDefault(const char* filename);
	bool loadMorph(const char* filename);

	unsigned int loadAll(const char* dir, const char* filePrefix);

	virtual void handle(const char* filename);

	void prepareMorphTargetMapping(const std::vector<physx::PxVec3>& mapOriginalPositions);

	unsigned int getNumMorphs() { return (unsigned int)mMorphedPositions.size(); }
	void clearDisplacement() { mDisplacements.clear(); }
	void computeDisplacement(const std::vector<float>& weights);

	const std::vector<physx::PxVec3>& getOriginalPositions() { return mOriginalPositions; }
	const std::vector<physx::PxVec3>& getDisplacements() { return mDisplacements; }

private:
	void loadVertices(const char* filename, std::vector<physx::PxVec3>& output);
	SampleApexResourceCallback& mResourceCallback;

	std::vector<physx::PxVec3> mOriginalPositions;
	std::vector<std::vector<physx::PxVec3> > mMorphedPositions;

	std::vector<int> mMorphTargetMap;
	std::vector<physx::PxVec3> mDisplacements;

	const char* mTempPrefix;
	const char* mTempDir;
};


#endif // SAMPLE_MORPH_TARGETS_H
