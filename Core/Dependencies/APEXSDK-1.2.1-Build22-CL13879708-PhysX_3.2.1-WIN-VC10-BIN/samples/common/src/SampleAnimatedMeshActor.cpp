// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#include "SampleAnimatedMeshActor.h"

#include "SampleAnimatedMeshAsset.h"
#include "SampleClothingActor.h"
#include <MeshImport.h>
#include <SkeletalAnim.h>
#include <TriangleMesh.h>


SampleAnimatedMeshActor::SampleAnimatedMeshActor(const physx::PxMat44& globalPose,
        SampleAnimatedMeshAsset* meshAsset,
        physx::apex::NxUserRenderResourceManager* renderResourceManager,
        physx::apex::NxUserRenderer* apexRenderer, SampleRenderer::Renderer* renderer) :
	mGlobalPose(globalPose),
	mRenderResourceManager(renderResourceManager),
	mApexRenderer(apexRenderer),
	mParent(meshAsset),
	mTriangleMesh(NULL),
	mSkeletalAnim(NULL),
	mTimePassed(0.0f),
	mContinuousAnimation(false),
	mSecondsUntilRestart(0.0f),
	mAnimationTimeScale(1.0f)
{
	if (mParent->getTriangleMesh() != NULL)
	{
		mTriangleMesh = new Samples::TriangleMesh(PX_MAX_U32, renderer);
		mTriangleMesh->loadFromParent(mParent->getTriangleMesh());

		if (mParent->getSkeletalAnim() != NULL)
		{
			mSkeletalAnim = new Samples::SkeletalAnim;
			mSkeletalAnim->loadFromParent(mParent->getSkeletalAnim());

			mSkeletalAnim->setAnimPose(0, 0.0f);
			mTriangleMesh->skin(*mSkeletalAnim);
		}
	}
}


SampleAnimatedMeshActor::~SampleAnimatedMeshActor()
{
	if (mTriangleMesh != NULL)
	{
		mTriangleMesh->clear(mRenderResourceManager, NULL);
		delete mTriangleMesh;
		mTriangleMesh = NULL;
	}

	delete mSkeletalAnim;
	mSkeletalAnim = NULL;
}



void SampleAnimatedMeshActor::tick(float dtime, bool rewriteBuffers)
{
	bool isContinuous = true;

	if (mSkeletalAnim != NULL)
	{
		// counter frame delay, skin before updating the animation
		mTriangleMesh->skin(*mSkeletalAnim);

		mTimePassed += dtime * mAnimationTimeScale;
		if (mSkeletalAnim->getAnimations().size() > 0)
		{
			if (mTimePassed > mSkeletalAnim->getAnimations()[0]->maxTime + mSecondsUntilRestart)
			{
				mTimePassed = mSkeletalAnim->getAnimations()[0]->minTime;
				mSkeletalAnim->setAnimPose(0, mTimePassed);
				isContinuous = mContinuousAnimation;
			}
			else if (mTimePassed < mSkeletalAnim->getAnimations()[0]->maxTime)
			{
				mSkeletalAnim->setAnimPose(0, mTimePassed);
			}
		}

		for (size_t i = 0; i < mFollowers.size(); i++)
		{
			//mFollower->setGlobalPose(mGlobalPose);
			mFollowers[i]->updateState(mGlobalPose, &mSkeletalAnim->getSkinningMatrices()[0], (int)(mSkeletalAnim->getSkinningMatrices().size()), isContinuous);
		}
	}
	else
	{
		for (size_t i = 0; i < mFollowers.size(); i++)
		{
			mFollowers[i]->updateState(mGlobalPose, NULL, 0, isContinuous);
		}
	}

	if (mTriangleMesh != NULL)
	{
		mTriangleMesh->updateRenderResources(rewriteBuffers, *mRenderResourceManager, NULL);
		//mTriangleMesh->updateRenderer(rewriteBuffers);
	}
}



void SampleAnimatedMeshActor::render(bool /*rewriteBuffers*/)
{
	if (mTriangleMesh != NULL)
	{
		mTriangleMesh->dispatchRenderResources(*mApexRenderer, mGlobalPose);
		//mTriangleMesh->queueForRendering(mGlobalPose);
	}
}



void SampleAnimatedMeshActor::addFollower(SampleClothingActor* follower)
{
	if (follower != NULL)
	{
		mFollowers.push_back(follower);

		follower->setParent(this);
	}
}


void SampleAnimatedMeshActor::applyMorphTarget(const std::vector<physx::PxVec3>& displacements)
{
	if (mTriangleMesh != NULL)
	{
		//if (mTriangleMesh->getVertices().size() == displacements.size())
		{
			mTriangleMesh->applyMorphDisplacements(displacements);
		}
	}
}