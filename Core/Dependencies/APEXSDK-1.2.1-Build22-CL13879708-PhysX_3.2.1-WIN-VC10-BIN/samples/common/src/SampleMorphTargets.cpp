// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "SampleMorphTargets.h"

#include "SampleApexResourceCallback.h"

#include "PxFileBuf.h"

#include <cstdio>
#include <assert.h>


bool SampleMorphTargets::loadDefault(const char* filename)
{
	loadVertices(filename, mOriginalPositions);

	return !mOriginalPositions.empty();
}


bool SampleMorphTargets::loadMorph(const char* filename)
{
	std::vector<physx::PxVec3> temp;
	loadVertices(filename, temp);

	if (!temp.empty())
	{
		mMorphedPositions.push_back(temp);
	}

	return !temp.empty();
}



unsigned int SampleMorphTargets::loadAll(const char* dir, const char* filePrefix)
{
	mTempDir = dir;
	mTempPrefix = filePrefix;
	mResourceCallback.findFiles(dir, *this);
	mTempDir = mTempPrefix = NULL;

	bool ok = true;
	for (size_t i = 0; i < mMorphedPositions.size(); i++)
	{
		ok &= mMorphedPositions[i].size() == mOriginalPositions.size();
	}

	if (!ok)
	{
		for (size_t i = 0; i < mMorphedPositions.size(); i++)
		{
			mMorphedPositions[i].clear();
		}
		mMorphedPositions.clear();
		mOriginalPositions.clear();
	}

	return (unsigned int)mMorphedPositions.size();
}



void SampleMorphTargets::handle(const char* filename)
{
	const char* rightDir = strstr(filename, mTempDir);
	if (rightDir != NULL)
	{
		const char* realName = rightDir + strlen(mTempDir) + 1;
		if (strncmp(realName, mTempPrefix, strlen(mTempPrefix)) == 0 && strstr(realName, ".obj") != NULL)
		{
			// valid file, now I just need to know if it's the default (original position) or a morph
			bool isDefault = strncmp(realName + strlen(mTempPrefix), "default", 7) == 0;
			bool isBase = strncmp(realName + strlen(mTempPrefix), "Base", 4) == 0;
			if (isDefault || isBase)
			{
				assert(mOriginalPositions.empty());
				loadDefault(rightDir);
			}
			else
			{
				loadMorph(rightDir);
			}
		}
	}
}



void SampleMorphTargets::prepareMorphTargetMapping(const std::vector<physx::PxVec3>& mapOriginalPositions)
{
	mMorphTargetMap.clear();
	mMorphTargetMap.resize(mapOriginalPositions.size());

	for (size_t i = 0; i < mMorphTargetMap.size(); i++)
	{
		float bestDistSquared = FLT_MAX;
		size_t bestJ = 0;
		for (size_t j = 0; j < mOriginalPositions.size(); j++)
		{
			const float distSquared = (mOriginalPositions[j] - mapOriginalPositions[i]).magnitudeSquared();
			if (distSquared < bestDistSquared)
			{
				bestDistSquared = distSquared;
				bestJ = j;
			}
		}

		mMorphTargetMap[i] = static_cast<int>(bestJ);
	}
}



void SampleMorphTargets::computeDisplacement(const std::vector<float>& weights)
{
	// clear the displacements
	mDisplacements.clear();
	mDisplacements.resize(mOriginalPositions.size(), physx::PxVec3(0.0f));

	// compute the displacements
	for (size_t w = 0; w < weights.size(); w++)
	{
		const float weight = weights[w];
		if (weight != 0.0f)
		{
			for (size_t i = 0; i < mOriginalPositions.size(); i++)
			{
				const physx::PxVec3 disp = mMorphedPositions[w][i] - mOriginalPositions[i];
				mDisplacements[i] += disp * weight;
			}
		}
	}

	if (!mMorphTargetMap.empty())
	{
		std::vector<physx::PxVec3> newDisplacements(mMorphTargetMap.size());

		for (size_t i = 0; i < mMorphTargetMap.size(); i++)
		{
			newDisplacements[i] = mDisplacements[mMorphTargetMap[i]];
		}

		mDisplacements.swap(newDisplacements);
	}
}



void SampleMorphTargets::loadVertices(const char* filename, std::vector<physx::PxVec3>& output)
{
	physx::PxFileBuf* objFile = mResourceCallback.findApexAsset(filename);

	if (objFile != NULL)
	{
		char line[128];

		while (true)
		{
			line[127] = 0;
			for (unsigned int i = 0; i < 126; i++)
			{
				line[i] = objFile->readByte();
				if (line[i] == 10 || line[i] == 13 || line[i] == 0)
				{
					line[i + 1] = 0;
					break;
				}
			}

			if (line[0] == 'v' && line[1] == ' ')
			{
				physx::PxVec3 temp;
				sscanf(line + 2, "%f %f %f", &temp.x, &temp.y, &temp.z);
				output.push_back(temp);
			}
			else if (line[0] == 0)
			{
				break;
			}
		}

		objFile->release();
	}
}