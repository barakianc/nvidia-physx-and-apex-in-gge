// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "CommunicationsToolInternal.h"

#ifdef COMM_TOOL_VALID

using namespace physx; //urgh. remove this someday
using namespace shdfnd; //urgh. remove this someday

namespace
{
void operationOK(const physx::shdfnd::PxIPC::ErrorCode & errorCode)
{
	PX_FORCE_PARAMETER_REFERENCE(errorCode);
	assert(physx::shdfnd::PxIPC::EC_FAIL != errorCode);
}

PxIPC * createSocketIPC(PxIPC::ErrorCode &errorCode,
		PxIPC::ConnectionType connectionType,
		const char *socketAddress,
		PxU16	portNumber,
		PxU32 serverRingBufferSize,
		PxU32 clientRingBufferSize,
		bool allowLongMessages,
		bool bufferSends,
		PxU32 maxBufferSendSize)
{
	SocketIPC *ret = PX_NEW(SocketIPC)(errorCode,
		connectionType,
		socketAddress,
		portNumber,
		serverRingBufferSize,
		clientRingBufferSize,
		allowLongMessages,
		bufferSends,
		maxBufferSendSize);
	
	if ( errorCode != PxIPC::EC_OK )
	{
		PX_FREE(ret);
		ret = NULL;
	}
	return static_cast< PxIPC * >(ret);
}
}; // namespace nameless

namespace CommTool
{
CommLayer::CommLayer(const ConnectionProperty * connectionProperties, unsigned int connectionCount_)
:connectionCount(connectionCount_)
,mIPC(NULL)
{
	assert(connectionCount < 5); // arbitrary
	if(connectionCount > 0)
	{
		mIPC = ::new physx::shdfnd::PxIPC * [connectionCount];
	}
	for(physx::PxU32 connectionIndex = 0; connectionIndex < connectionCount; ++connectionIndex)
	{
		const ConnectionProperty & currentConnection = *connectionProperties++;

		// set connection type
		physx::shdfnd::PxIPC::ConnectionType connectionType	= physx::shdfnd::PxIPC::CT_LAST;
		switch(currentConnection.connectionType)
		{
		case ConnectionType::CREATE_AS_CLIENT :
			connectionType = physx::shdfnd::PxIPC::CT_CLIENT;
			break;
		case ConnectionType::CREATE_AS_SERVER :
			connectionType = physx::shdfnd::PxIPC::CT_SERVER;
			break;
		default:
			assert(!"invalid connectionType!");
		}
		assert(physx::shdfnd::PxIPC::CT_LAST != connectionType);

		// create socket
		physx::shdfnd::PxIPC::ErrorCode errorCode = physx::shdfnd::PxIPC::EC_FAIL;
		mIPC[connectionIndex] = createSocketIPC(errorCode, connectionType, currentConnection.socketAddress, currentConnection.portNumber, currentConnection.bufferSize, currentConnection.bufferSize, false, false, 2 * currentConnection.bufferSize);
		assert(NULL != mIPC[connectionIndex]);
		assert(physx::shdfnd::PxIPC::EC_OK == errorCode);
	}
	assert(connectionCount > 0 ? (NULL != mIPC) : true);
}

CommLayer::~CommLayer()
{
    assert(connectionCount > 0 ? (NULL != mIPC) : true);
    if(NULL != mIPC)
    {
		for(physx::PxU32 connectionIndex = 0; connectionIndex < connectionCount; ++connectionIndex)
		{
			mIPC[connectionIndex]->release();
			mIPC[connectionIndex] = NULL;
		}
		::delete [] mIPC;
		mIPC = NULL;
    }
}

void CommLayer::release()
{
    ::delete this;
}

bool CommLayer::send(const void * bufferStart, physx::PxU32 bufferSize, physx::PxU32 connectionIndex)
{
	bool validOperation = false;
	if((NULL != bufferStart) && (0 != bufferSize) && (connectionIndex < connectionCount))
	{
		validOperation = true;
		assert(NULL != &mIPC);
		physx::shdfnd::PxIPC & currentIPC = *(mIPC[connectionIndex]);
		assert(NULL != &currentIPC);
		assert(currentIPC.canSend(bufferSize));
		//assert(!currentIPC.pumpPendingSends());
		operationOK(currentIPC.sendData(bufferStart, bufferSize, true));
	}
	return validOperation;
}

bool CommLayer::receive(void *& bufferStart, physx::PxU32 & bufferSize, physx::PxU32 connectionIndex)
{
	bool validOperation = false;
	bufferStart = NULL;
	bufferSize = 0;
	if(connectionIndex < connectionCount)
	{
		validOperation = true;
		assert(NULL != &mIPC);
		physx::shdfnd::PxIPC & currentIPC = *(mIPC[connectionIndex]);
		assert(NULL != &currentIPC);
		//assert(currentIPC.haveConnection());
		bufferStart = const_cast<void*>(currentIPC.receiveData(bufferSize));
		if((NULL != bufferStart) && (0 != bufferSize))
		{
			operationOK(currentIPC.receiveAcknowledge());
		}
	}
	return validOperation;
}
}; // namespace CommTool


// all these used to be in John's PsSocketIPC.cpp, which got removed!
namespace physx
{
namespace shdfnd
{
ReadThread::ReadThread()
{
	mReadBuffer = NULL;
	mReadSize = 0;
	mSocket = NULL;
	mRead = NULL;
	mExit = false;
}

ReadThread::~ReadThread()
{
	PX_FREE(mReadBuffer);
}

void ReadThread::init(Socket *socket,PxRingBuffer *reader,PxU32 maxReadSize)
{
	mSocket = socket;
	mRead = reader;
	mReadSize = maxReadSize;
	mReadBuffer = (PxU8 *)PX_ALLOC(maxReadSize, PX_DEBUG_EXP("PsSocketIPC_ReadBuffer"));
	Thread::start(Thread::getDefaultStackSize());
}

void ReadThread::execute()
{
	// all data coming in from the socket is stored into the ring buffer 
	while ( !mExit )
	{
		PxU32 readSize = mSocket->read(mReadBuffer,mReadSize);
		if ( readSize )
		{
			//mReadBuffer[readSize] = 0;
			//readSize++;
			mRead->sendData(PxRingBuffer::MT_APP,mReadBuffer,readSize,true);
		}

		// Provide an interrupt
		sleep(0);
	}
}

SendThread::SendThread()
{
	mSendBuffer = NULL;
	mSendSize = 0;
	mSocket = NULL;
	mWrite = NULL;
	mExit = false;
}

SendThread::~SendThread()
{
	PX_FREE(mSendBuffer);
}

void SendThread::init(Socket *socket,PxRingBuffer *writer,PxU32 maxSendSize)
{
	mSocket = socket;
	mWrite = writer;
	mSendSize = maxSendSize;
	mSendBuffer = (PxU8 *)PX_ALLOC(maxSendSize, PX_DEBUG_EXP("PsSocketIPC_SendBuffer"));
	Thread::start(Thread::getDefaultStackSize());
}

void SendThread::execute()
{
	// all data coming in from the socket is stored into the ring buffer 
	while ( !mExit )
	{
		//PxRingBuffer::MessageType receiveData(const void *&data,PxU32 &data_len)
		const void *data=NULL;
		PxU32 data_len = 0;
		PxRingBuffer::MessageType mt = mWrite->receiveData(data,data_len);
		if ( mt == PxRingBuffer::MT_APP )
		{
			PxU32 sent = mSocket->write((const PxU8*)data,data_len);
			if ( sent == data_len )
			{
				printf("Successfully sent message: %s\r\n", (const char*)data );
				mWrite->receiveAcknowledge(); // acknowledge that we consumed (sent) this data.
			}
			else
			{
				PX_ALWAYS_ASSERT(); // not expecting this to fail...
			}
			mSocket->flush();
		}

		// Provide an interrupt
		sleep(0);
	}
}

SocketIPC::SocketIPC(PxIPC::ErrorCode &errorCode,
			PxIPC::ConnectionType connectionType,
			const char *socketAddress,
			PxU16	portNumber,
			PxU32 serverRingBufferSize,
			PxU32 clientRingBufferSize,
			bool allowLongMessages,
			bool bufferSends,
			PxU32 maxBufferSendSize) : mSocket(false)
{
	errorCode = PxIPC::EC_OK;

	switch ( connectionType )
	{
	case PxIPC::CT_CLIENT_REQUIRE_SERVER:
		if ( !mSocket.connect(socketAddress,portNumber))
		{
			errorCode = PxIPC::EC_SERVER_NOT_FOUND;
		}
		break;
	case PxIPC::CT_CLIENT:
		if ( !mSocket.connect(socketAddress,portNumber))
		{
			errorCode = PxIPC::EC_SERVER_NOT_FOUND;
		}
		break;
	case PxIPC::CT_SERVER:
		if ( !mSocket.listen(portNumber,NULL))
		{
			errorCode = PxIPC::EC_SERVER_ALREADY_EXISTS;
		}
		break;
	case PxIPC::CT_CLIENT_OR_SERVER:
		if ( !mSocket.connect(socketAddress,portNumber))
		{
			if ( !mSocket.listen(portNumber,NULL))
			{
				errorCode = PxIPC::EC_SERVER_ALREADY_EXISTS;
			}
		}
		break;

	case PxIPC::CT_LAST:
		// Make compiler happy
		break;
	}

	if ( errorCode == PxIPC::EC_OK  )
	{
		mServerRingBuffer.init(NULL,
			serverRingBufferSize,
			allowLongMessages,
			bufferSends,
			maxBufferSendSize,
			NULL,
			NULL);

		mClientRingBuffer.init(NULL,
			clientRingBufferSize,
			allowLongMessages,
			bufferSends,
			maxBufferSendSize,
			NULL,
			NULL);

		mRead = &mServerRingBuffer;
		mWrite = &mClientRingBuffer;

		mReadThread.init(&mSocket,mRead,serverRingBufferSize);
		mSendThread.init(&mSocket,mWrite,clientRingBufferSize);

	}
}

SocketIPC::~SocketIPC()
{
	mSocket.disconnect();
}

PxIPC::ErrorCode SocketIPC::sendData(const void *data,PxU32 data_len,bool bufferIfFull)
{
	ErrorCode ret = EC_OK;
	PX_ASSERT(mWrite);
	if ( mWrite )
	{
		PxRingBuffer::ErrorCode rec = mWrite->sendData(PxRingBuffer::MT_APP,data,data_len,bufferIfFull);
		if ( rec != PxRingBuffer::EC_OK )
		{
			ret = EC_FAIL;
		}
	}
	else
	{
		ret = EC_FAIL;
	}
	return ret;
}

const void * SocketIPC::receiveData(PxU32 &data_len)
{
	const void *ret = NULL;
	data_len = 0;
	PX_ASSERT(mRead);
	if ( mRead )
	{
		mRead->receiveData(ret,data_len);
	}
	return ret;
}

bool SocketIPC::isServer() const
{
	return mIsServer;
}

PxIPC::ErrorCode SocketIPC::receiveAcknowledge()
{
	ErrorCode ret = EC_OK;
	PX_ASSERT(mRead);
	if ( mRead )
	{
		PxRingBuffer::ErrorCode rec = mRead->receiveAcknowledge();
		if ( rec != PxRingBuffer::EC_OK )
		{
			ret = EC_FAIL;
		}
	}
	else
	{
		ret = EC_FAIL;
	}
	return ret;
}

bool SocketIPC::pumpPendingSends()
{
	bool ret = false;
	PX_ASSERT(mWrite);
	if ( mWrite )
	{
		ret = mWrite->pumpPendingSends();
	}
	return ret;
}

bool SocketIPC::haveConnection() const
{
	bool ret = false;
	return ret;
}

bool SocketIPC::canSend(PxU32 len)
{
	bool ret = false;
	if ( mWrite )
	{
		if ( mWrite->canSend(len) == PxRingBuffer::EC_OK )
		{
			ret = true;
		}
		else
		{
			bool pendingSend = mWrite->pumpPendingSends();
			if ( !pendingSend )
			{
				ret = true;
			}
		}
	}
	return ret;
}

void SocketIPC::release()
{
	delete this;
}
}; // namespace shdfnd
}; // namespace physx
#endif // COMM_TOOL_VALID
