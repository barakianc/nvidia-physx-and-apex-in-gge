// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#include "SampleClothingActor.h"

#include "SampleAnimatedMeshActor.h"
#include <SkeletalAnim.h>

#include <NxClothingActor.h>
#include <NxClothingAsset.h>
#include <NxParameterized.h>
#include <NxParamUtils.h>


SampleClothingActor::SampleClothingActor(physx::apex::NxApexScene& apexScene, physx::apex::NxClothingAsset& asset,
										 physx::apex::NxUserRenderer* apexRenderer, const physx::PxMat44& globalPose,
										 bool useGPU, bool useLocalSpace) :
	mParent(NULL),
	mAsset(asset),
	mClothingActor(NULL),
	mApexRenderer(apexRenderer),
	mTime(0.0f),
	mGlobalPose(globalPose),
	mScale(1.0f),
	mShouldMoveGlobalPose(true),
	mIsContinuous(true),
	mUseGPU(useGPU),
	mUseLocalSpace(useLocalSpace),
	mRandomWind(NULL)
{
	init(apexScene, NULL, 0);
}



SampleClothingActor::SampleClothingActor(physx::apex::NxApexScene& apexScene, physx::apex::NxClothingAsset& asset,
										 physx::apex::NxUserRenderer* apexRenderer, SampleAnimatedMeshActor* actor,
										 bool useGPU, bool useLocalSpace,
										 const physx::PxVec3* morphDisplacement, physx::PxU32 numMorphDisplacement) :
	mParent(NULL),
	mAsset(asset),
	mClothingActor(NULL),
	mApexRenderer(apexRenderer),
	mTime(0.0f),
	mGlobalPose(physx::PxMat44::createIdentity()),
	mScale(1.0f),
	mShouldMoveGlobalPose(true),
	mIsContinuous(true),
	mUseGPU(useGPU),
	mUseLocalSpace(useLocalSpace),
	mRandomWind(NULL)
{
	PX_ASSERT(actor);
	actor->addFollower(this);

	if (actor->getSkeletalAnim() != NULL)
	{
		const std::vector<Samples::SkeletalBone>& bones = actor->getSkeletalAnim()->getBones();
		mBoneRemapTable.resize(asset.getNumUsedBones());

		for (physx::PxU32 i = 0; i < asset.getNumUsedBones(); i++)
		{
			const char* boneName = asset.getBoneName(i);
			unsigned int count = 0;
			for (physx::PxU32 j = 0; j < bones.size(); j++)
			{
				if (bones[j].name == boneName)
				{
					PX_ASSERT(count == 0);
					count++;
					mBoneRemapTable[i] = j;
				}
			}
			PX_ASSERT(count == 1);
		}
	}

	actor->tick(false, 0.0f);

	init(apexScene, morphDisplacement, numMorphDisplacement);
}



SampleClothingActor::~SampleClothingActor()
{
	if (mClothingActor)
	{
		mClothingActor->release();
	}

	if (mRandomWind != NULL)
	{
		delete mRandomWind;
		mRandomWind = NULL;
	}
}



void SampleClothingActor::tick(float dtime, bool /*rewriteBuffers*/)
{
	physx::PxMat34Legacy pose = mGlobalPose;
	
	if (mShouldMoveGlobalPose && mParent == NULL)
	{
		// here we need to update its animation and global pose, and priority
		pose.t.x += sin(mTime);
	}

	mClothingActor->lockRenderResources();
	mClothingActor->updateRenderResources();
	mClothingActor->unlockRenderResources();

	using namespace physx::apex;
	ClothingTeleportMode::Enum tm = mIsContinuous ? ClothingTeleportMode::Continuous : ClothingTeleportMode::TeleportAndReset;
	if (mSkinningMatrices.empty())
	{
		mClothingActor->updateState(pose, NULL, 0, 0, tm);
	}
	else
	{
		mClothingActor->updateState(pose, &mSkinningMatrices[0], sizeof(physx::PxMat44), (physx::PxU32)mSkinningMatrices.size(), tm);
	}

	if (mRandomWind != NULL)
	{
		mRandomWind->tick(dtime);
		setWind(mRandomWind->getAdaption(), mRandomWind->getVelocity());
	}

	mTime += dtime;
}



void SampleClothingActor::render(bool /*rewriteBuffers*/)
{
	if (mClothingActor)
	{
		mClothingActor->dispatchRenderResources(*mApexRenderer);
	}
}



void SampleClothingActor::updateState(const physx::PxMat44& globalPose, const physx::PxMat44* boneMatrices, int numBoneMatrices, bool isContinuous)
{
	mGlobalPose = globalPose;
	mIsContinuous = isContinuous;

	if (mSkinningMatrices.size() != mBoneRemapTable.size())
	{
		mSkinningMatrices.resize(mBoneRemapTable.size());
	}

	for (size_t i = 0; i < mBoneRemapTable.size(); i++)
	{
		if ((int)mBoneRemapTable[i] < numBoneMatrices)
		{
			mSkinningMatrices[i] = boneMatrices[mBoneRemapTable[i]];
		}
	}
}



int SampleClothingActor::changeRenderLOD()
{
	if (mClothingActor && mNumGraphicalLods > 1)
	{
		if ((physx::PxI32)mLodIndex == mNumGraphicalLods - 1)
		{
			mIncreaseLod = false;
		}
		if (mLodIndex == 0)
		{
			mIncreaseLod = true;
		}

		if (mIncreaseLod)
		{
			++mLodIndex;
		}
		else
		{
			--mLodIndex;
		}
		mClothingActor->setGraphicalLOD(mLods[mLodIndex]);

		return mLods[mLodIndex];
	}

	return -1;
}



void SampleClothingActor::setWind(physx::PxF32 windAdaption, physx::PxVec3 windVelocity)
{
	if (mClothingActor != NULL)
	{
		NxParameterized::Interface* actorDesc = mClothingActor->getActorDesc();

		PX_ASSERT(actorDesc != NULL);
		if (actorDesc != NULL)
		{
			NxParameterized::setParamF32(*actorDesc, "windParams.Adaption", windAdaption);
			NxParameterized::setParamVec3(*actorDesc, "windParams.Velocity", windVelocity);
		}
	}
}



void SampleClothingActor::setWindNoise(physx::PxF32 adaptionChange, physx::PxF32 velocityConeAngleDegree)
{
	physx::PxF32 windAdaption = 0.0f;
	physx::PxVec3 windVelocity(0.0f);

	if (mClothingActor != NULL)
	{
		NxParameterized::Interface* actorDesc = mClothingActor->getActorDesc();

		PX_ASSERT(actorDesc != NULL);
		if (actorDesc != NULL)
		{
			NxParameterized::getParamF32(*actorDesc, "windParams.Adaption", windAdaption);
			NxParameterized::getParamVec3(*actorDesc, "windParams.Velocity", windVelocity);
		}
	}

	if (mRandomWind != NULL)
	{
		delete mRandomWind;
	}
	mRandomWind = new SampleRandomWind(windAdaption, windVelocity, adaptionChange, velocityConeAngleDegree);
}



#ifdef _DEBUG
#define VERIFY_PARAM(_A) { NxParameterized::ErrorType error = _A; PX_ASSERT(error == NxParameterized::ERROR_NONE); }
#else
#define VERIFY_PARAM(_A) _A
#endif

void SampleClothingActor::init(physx::apex::NxApexScene& apexScene, const physx::PxVec3* morphDisplacement, physx::PxU32 numMorphDisplacement)
{
	NxParameterized::Interface* actorDesc = mAsset.getDefaultActorDesc();
	PX_ASSERT(actorDesc != NULL);
	if (actorDesc != NULL)
	{
		NxParameterized::Handle handle(actorDesc);

		NxParameterized::setParamMat44(*actorDesc, "globalPose", mGlobalPose);

		mScale = mGlobalPose.column0.magnitude();
		NxParameterized::setParamF32(*actorDesc, "actorScale", mScale);

		VERIFY_PARAM(handle.getParameter("boneMatrices"));
		VERIFY_PARAM(handle.resizeArray((physx::PxU32)mSkinningMatrices.size()));
		if (!mSkinningMatrices.empty())
		{
			VERIFY_PARAM(handle.setParamMat44Array(&mSkinningMatrices[0], (physx::PxU32)mSkinningMatrices.size()));
		}

		NxParameterized::setParamBool(*actorDesc, "useHardwareCloth", mUseGPU);

		NxParameterized::setParamBool(*actorDesc, "flags.RecomputeNormals", false);
		//NxParameterized::setParamBool(*actorDesc, "fallbackSkinning", true);

		NxParameterized::setParamBool(*actorDesc, "flags.CorrectSimulationNormals", true);


		// Use the external ordering
		NxParameterized::setParamBool(*actorDesc, "useInternalBoneOrder", true);

		// Set up the groupsmask
		NxParameterized::setParamU32(*actorDesc, "clothDescTemplate.groupsMask.bits0", 0);
		NxParameterized::setParamU32(*actorDesc, "clothDescTemplate.groupsMask.bits1", 1);
		NxParameterized::setParamU32(*actorDesc, "clothDescTemplate.groupsMask.bits2", 0);
		NxParameterized::setParamU32(*actorDesc, "clothDescTemplate.groupsMask.bits3", 1);

#if NX_SDK_VERSION_MAJOR == 2
		NxParameterized::setParamU32(*actorDesc, "shapeDescTemplate.groupsMask.bits0", 0);
		NxParameterized::setParamU32(*actorDesc, "shapeDescTemplate.groupsMask.bits1", 1);
		NxParameterized::setParamU32(*actorDesc, "shapeDescTemplate.groupsMask.bits2", 0);
		NxParameterized::setParamU32(*actorDesc, "shapeDescTemplate.groupsMask.bits3", 1);
#elif NX_SDK_VERSION_MAJOR == 3
		// apan, TODO, need use simulationFilterData: word0, word2, and reserve word3 for contact report flags
		//NxParameterized::setParamU32(*actorDesc, "p3ShapeDescTemplate.simulationFilterData.word1", 1);
		//NxParameterized::setParamU32(*actorDesc, "p3ShapeDescTemplate.simulationFilterData.word3", 1);
#endif
		// Setup LoD values
		NxParameterized::setParamF32(*actorDesc, "lodWeights.maxDistance", 2000.0f);
		NxParameterized::setParamF32(*actorDesc, "lodWeights.distanceWeight", 1.0f);
		NxParameterized::setParamF32(*actorDesc, "lodWeights.bias", 0.0f);
		NxParameterized::setParamF32(*actorDesc, "lodWeights.benefitsBias", 0.0f);

		// Setup Morph Targets
		if (numMorphDisplacement > 0)
		{
			NxParameterized::Handle md(*actorDesc, "morphDisplacements");
			md.resizeArray(numMorphDisplacement);
			md.setParamVec3Array(morphDisplacement, numMorphDisplacement);
		}

#ifdef PX_PS3
		NxParameterized::setParamBool(*actorDesc, "flags.ParallelCpuSkinning", false);
#else
		NxParameterized::setParamBool(*actorDesc, "flags.ParallelCpuSkinning", true);
#endif

		physx::apex::ClothSolverMode::Enum solverMode = mAsset.getClothSolverMode();
		if (solverMode == physx::apex::ClothSolverMode::v2)
		{
			mUseLocalSpace = false;
		}
		NxParameterized::setParamBool(*actorDesc, "localSpaceSim", mUseLocalSpace);
		
		physx::NxApexActor* actor = mAsset.createApexActor(*actorDesc, apexScene);
		mClothingActor = static_cast<physx::NxClothingActor*>(actor);

		if (mClothingActor->getClothSolverMode() == physx::apex::ClothSolverMode::v2)
		{
			setLocalSpace(false);
		}

		const NxParameterized::Interface* assetParams = mAsset.getAssetNxParameterized();
		if (assetParams)
		{
			NxParameterized::Handle graphicalLodsHandle(*assetParams);
			VERIFY_PARAM(graphicalLodsHandle.getParameter("graphicalLods"));
			VERIFY_PARAM(graphicalLodsHandle.getArraySize(mNumGraphicalLods));

			mLods.resize(mNumGraphicalLods);
			for (int i = 0; i < mNumGraphicalLods; ++i)
			{
				// select child i
				graphicalLodsHandle.set(i);

				NxParameterized::Interface* lodParams;
				VERIFY_PARAM(graphicalLodsHandle.getParamRef(lodParams));

				// go back to parent element
				graphicalLodsHandle.popIndex();

				mLods[i] = PX_MAX_U32;
				NxParameterized::getParamU32(*lodParams, "lod", mLods[i]);
				PX_ASSERT(mLods[i] != PX_MAX_U32);
			}
		}
		mLodIndex = mNumGraphicalLods - 1;
		mIncreaseLod = false;
		mClothingActor->setGraphicalLOD(mLods[mLodIndex]);
	}
}

void SampleClothingActor::forcePhysicalLOD(physx::PxF32 lod)
{
	if (mClothingActor != NULL)
	{
		mClothingActor->forcePhysicalLod(lod);
	}
}

void SampleClothingActor::setLocalSpace(bool on)
{
	mUseLocalSpace = on;
	if (mClothingActor != NULL)
	{
		NxParameterized::Interface* actorDesc = mClothingActor->getActorDesc();

		PX_ASSERT(actorDesc != NULL);
		if (actorDesc != NULL)
		{
			NxParameterized::setParamBool(*actorDesc, "localSpaceSim", mUseLocalSpace);
		}
		
	}
}

bool SampleClothingActor::isLocalSpace()
{
	return mUseLocalSpace;
}

#undef VERIFY_PARAM
