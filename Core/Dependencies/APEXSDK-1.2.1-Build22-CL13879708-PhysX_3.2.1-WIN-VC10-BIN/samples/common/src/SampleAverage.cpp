// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "SampleAverage.h"

#include <algorithm>
#include <math.h>

#include <PsSort.h>


template <>
float SampleAverageBase<float>::updateAverage(float* list, unsigned int size)
{
	float averageValue = 0.0f;

	for (unsigned int i = 0; i < size; i++)
	{
		averageValue += list[i];
	}

	averageValue /= (float)size;
	return averageValue;
}


template <>
float SampleAverageBase<float>::updateAverageWeighted(float* list, unsigned int size, unsigned int start)
{
#if 0
	return updateAverage(list, size);
#else
	float averageValue = 0.0f;
	float sumWeight = 0.0f;

	for (unsigned int i = 0; i < size; i++)
	{
		unsigned int startIndex = (i - start + size) % size;
		float normalizedPosition = (float)startIndex / size;
		float weight = ::pow(2.0f, normalizedPosition * 6 - 3);


		averageValue += weight * list[i];
		sumWeight += weight;
	}

	return averageValue / sumWeight;
#endif
}

template<typename T, int medianSize>
class Median
{
public:
	Median() : mFillState(0), mTimestamp(0) {}
	void update(T newVal)
	{
		if (mFillState < medianSize)
		{
			mMedians[mFillState].element = newVal;
			mMedians[mFillState].timestamp = mTimestamp++;

			mFillState++;

			if (mFillState == medianSize)
			{
				// sort them
				sort();
			}
		}
		else
		{
			for (unsigned int i = 0; i < medianSize; i++)
			{
				if (mMedians[i].timestamp == mTimestamp - medianSize)
				{
					mMedians[i].element = newVal;
					mMedians[i].timestamp = mTimestamp++;
					// now bubble up or down
					sort();
					break;
				}
			}
		}
	}

	void sort()
	{
		physx::shdfnd::sort(mMedians, medianSize, MedianElement());
	}

	bool getMedian(T& median)
	{
		if (mFillState == medianSize)
		{
			median = mMedians[medianSize / 2].element;
			return true;
		}
		return false;
	}

private:
	unsigned int mFillState;
	unsigned int mTimestamp;

	struct MedianElement
	{
		T element;
		unsigned int timestamp;
		bool operator()(const MedianElement& e1, const MedianElement& e2) const
		{
			return e1.element < e2.element;
		}
	};
	MedianElement mMedians[medianSize];
};



template <>
float SampleAverageBase<float>::updateAverageMedian(float* list, unsigned int size, unsigned int start)
{
#if 0
	//return updateAverage(list, size);
	return updateAverageWeighted(list, size, start);
#else
	float averageValue = 0.0f;
	float sumWeight = 0.0f;

	Median<float, 5> median;

	for (unsigned int i = 0; i < size; i++)
	{
		unsigned int startIndex = (i + start) % size;
		median.update(list[startIndex]);

		float m = 0.0f;
		if (median.getMedian(m))
		{
			averageValue += m;
			sumWeight += 1.0f;
		}
	}

	if (sumWeight > 0.0f)
	{
		averageValue /= sumWeight;
	}
	return averageValue;
#endif
}
