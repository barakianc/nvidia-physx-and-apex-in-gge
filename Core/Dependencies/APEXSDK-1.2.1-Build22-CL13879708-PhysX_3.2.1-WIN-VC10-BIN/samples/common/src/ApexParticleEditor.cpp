// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#include "ApexParticleEditor.h"
#include "NxApexDefs.h"

// particle editing only available in a debug or checked build on windows.
#if defined(PX_CHECKED) || defined(_DEBUG) || defined(PHYSX_PROFILE_SDK)
#if defined(PX_WINDOWS)
#define USE_APEX_PARAM_EDITOR
#endif
#endif

#ifdef USE_APEX_PARAM_EDITOR


#include "NxApexActor.h"
#include "NxApexAsset.h"
#include "NxParameterized.h"
#include "NxParamUtils.h"
#include "NxIofxAsset.h"
#include <stdlib.h>
#include <stdio.h>

#include "ApexParamEditor.h"
#include <Windows.h>

#pragma warning(disable:4996)


class _ApexParticleEditor : public ApexParticleEditor
{
public:
	_ApexParticleEditor(void)
	{
		mEditActor = NULL;
		mApexSDK = NULL;
		mApexParamEditor = NULL;
		loadApexParamEditor();
	}

	virtual ~_ApexParticleEditor(void)
	{
		releaseApexParamEditor();
	}

	virtual void release(void)
	{
		delete this;
	}

	virtual bool isDllLoaded()
	{
		return mApexParamEditor != NULL;
	}

	// This method loades the 'libapexparameditor' plugin which will, in turn,
	// load the 'libapexeditorwidgets' plugin.  It uses conditional compilation to
	// load the correctly named DLL (i.e. _x86 / _x64, _checked, _debug, _profile)
	void loadApexParamEditor(void)
	{
		UINT errorMode = SEM_FAILCRITICALERRORS;
		UINT oldErrorMode = SetErrorMode(errorMode);
		const char* dll=NULL;
#ifdef PX_X64
#ifdef _DEBUG
		dll = "libapexparameditor_x64_debug.dll";
#elif defined(PX_CHECKED)
		dll = "libapexparameditor_x64_checked.dll";
#elif defined(PHYSX_PROFILE_SDK)
		dll = "libapexparameditor_x64_profile.dll";
#endif // end _DEBUG
#else
#ifdef _DEBUG
		dll = "libapexparameditor_x86_debug.dll";
#elif defined(PX_CHECKED)
		dll = "libapexparameditor_x86_checked.dll";
#elif defined(PHYSX_PROFILE_SDK)
		dll = "libapexparameditor_x86_profile.dll";
#endif // end _DEBUG
#endif // end PX_X64
		if ( dll )
		{
			HMODULE module = LoadLibraryA(dll);
			SetErrorMode(oldErrorMode);
			if (module)
			{
				void* proc = GetProcAddress(module, "CreateApexParamEditor");
				if (proc)
				{
					typedef physx::ApexParamEditor * (__cdecl * PLUGIN_INTERFACE_LIST_FUNC)(void);
					mApexParamEditor = ((PLUGIN_INTERFACE_LIST_FUNC)proc)();
				}
			}
		}
	}


	void releaseApexParamEditor(void)
	{
		if ( mApexParamEditor )
		{
			mApexParamEditor->release();
			mApexParamEditor = NULL;
		}
	}

	virtual physx::apex::NxApexActor* pump(bool& exitApp)  // if pump returns false, then exit the application (close window)
	{
		exitApp = false;
		physx::apex::NxApexActor *ret = NULL;
		if ( mApexParamEditor )
		{
			ret = mApexParamEditor->pump(exitApp);
			if ( ret )
			{
				mEditActor = ret;
			}
		}
		return ret;
	}

	virtual void releaseEditApexActor(void)
	{
		if ( mApexParamEditor )
		{
			mApexParamEditor->releaseEditApexActor();
		}
		mEditActor = NULL;
		mApexSDK = NULL;
	}

	virtual bool editApexActor(physx::apex::NxApexActor* actor, physx::apex::NxApexSDK* sdk, physx::apex::NxApexScene* scene)
	{
		bool ret = false;

		if ( !actor ) return false;
		if ( !sdk ) return false;
		if ( !scene ) return false;

		{
			physx::apex::NxApexAsset *a = actor->getOwner();
			const NxParameterized::Interface *iface = a->getAssetNxParameterized();
			const char *className = iface->className();
			if ( strcmp(className,"ApexEmitterAssetParameters") != 0 )
			{
				return false;
			}
		}

		if ( mApexParamEditor )
		{
			releaseEditApexActor();
			ret = mApexParamEditor->editApexActor(actor,sdk,scene);
			if ( ret )
			{
				mEditActor = actor;
				mApexSDK = sdk;
			}
		}
		return ret;
	}
	virtual physx::apex::NxApexEmitterAsset* getApexEmitterAsset(void) const
	{
		physx::apex::NxApexEmitterAsset *ret = NULL;
		if ( mApexParamEditor )
		{
			ret = mApexParamEditor->getApexEmitterAsset();
		}
		return ret;
	}

	bool saveParams(const NxParameterized::Interface *params,const char *fname)
	{
		bool ret = false;

		char scratch[512];
		strncpy(scratch,fname,512);
		char *dot = strchr(scratch,'.');
		if ( dot )
		{
			*dot = 0;
		}
		strcat(scratch,".apx");
		fname = scratch;

		NxParameterized::Serializer *ser = mApexSDK->createSerializer(NxParameterized::Serializer::NST_XML);
		if ( ser )
		{
			physx::PxFileBuf *fb = mApexSDK->createMemoryWriteStream();
			if ( fb )
			{
				const NxParameterized::Interface *obj = params;
				NxParameterized::Serializer::ErrorType err = ser->serialize(*fb,&obj,1);
				if ( err == NxParameterized::Serializer::ERROR_NONE )
				{
					physx::PxU32 len;
					const void *mem = mApexSDK->getMemoryWriteBuffer(*fb,len);
					if ( mem )
					{
						FILE *fph = fopen(fname,"wb");
						if ( fph )
						{
							fwrite(mem,len,1,fph);
							fclose(fph);
							ret = true;
						}
					}
					mApexSDK->releaseMemoryWriteStream(*fb);
				}
			}
			ser->release();
		}
		return ret;
	}

	virtual bool saveApexAssets(void)
	{
		bool ret = false;
		if ( mEditActor && mApexSDK )
		{
			// Save the Emitter APX
			physx::apex::NxApexAsset *asset = mEditActor->getOwner();
			const NxParameterized::Interface *iface = asset->getAssetNxParameterized();
			saveParams(iface,asset->getName());

			// Save the IOFX APX
			{
				// Now we look up the named reference for the IOFX
				NxParameterized::Interface* iofxAssetNamedRef = NULL;
				NxParameterized::getParamRef(const_cast<NxParameterized::Interface&>(*iface), "iofxAssetName", iofxAssetNamedRef);
				if (iofxAssetNamedRef)
				{
					void* tmp = mApexSDK->getNamedResourceProvider()->getResource(NX_IOFX_AUTHORING_TYPE_NAME, iofxAssetNamedRef->name());
					if (tmp)
					{
						physx::apex::NxIofxAsset* iofxAsset = (physx::apex::NxIofxAsset*)tmp;
						const NxParameterized::Interface* params = iofxAsset->getAssetNxParameterized();
						saveParams(params,iofxAsset->getName());
					}
				}
			}
			// Save the IOS
			{
				// Now we look up the named reference for the IOS
				NxParameterized::Interface* iosAssetNamedRef = NULL;
				NxParameterized::getParamRef(const_cast<NxParameterized::Interface&>(*iface), "iosAssetName", iosAssetNamedRef);
				if (iosAssetNamedRef)
				{
					const char* className = iosAssetNamedRef->className();
					void* tmp = mApexSDK->getNamedResourceProvider()->getResource(iosAssetNamedRef->className(), iosAssetNamedRef->name());
					if (tmp)
					{
						physx::apex::NxIofxAsset* iosAsset = (physx::apex::NxIofxAsset*)tmp;
						const NxParameterized::Interface* params = iosAsset->getAssetNxParameterized();
						saveParams(params,iosAsset->getName());
					}
				}
			}

		}
		return ret;
	}

	physx::apex::NxApexSDK	*mApexSDK;
	physx::apex::NxApexActor *mEditActor;
	physx::ApexParamEditor	*mApexParamEditor;
};

ApexParticleEditor *createApexParticleEditor(void)
{
	_ApexParticleEditor *ret = new _ApexParticleEditor;
	if (!ret->isDllLoaded())
	{
		delete ret;
		return NULL;
	}
	else
	{
		return static_cast< ApexParticleEditor *>(ret);
	}
}


#else

ApexParticleEditor *createApexParticleEditor(void)
{
	return 0;
}

#endif