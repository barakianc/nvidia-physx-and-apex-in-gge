// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

/* PhysX version agnostic SampleApexApplication routines */

#include <stdio.h>

#include "NxApexDefs.h"
#include "SampleApexApplication.h"
#include "OpenAutomateInterface.h"
#include "SampleCommandLine.h"
#include "SampleApexRenderer.h"
#include "SamplePlatform.h"
#include "SampleFileExport.h"
#include "SampleInputEventIds.h"
#include "SampleUserInputIds.h"
#include "SampleInputDefines.h"
#include "SamplesVRDSettings.h"
#include "Renderer.h"
#include "RendererWindow.h"
#include "RendererBoxShape.h"
#include "RendererLight.h"

#include "RecordingRenderResourceManager.h"

#include <NxParamUtils.h>
#include <NxApexSDK.h>
#include <sstream>

#if defined(PX_WINDOWS)
#include "pxtask/PxCudaContextManager.h"
#include "pxtask/PxGpuDispatcher.h"
#endif

#include <PsUtilities.h> // just for PX_STRINGIZE

const char* ASSET_PATH_UNDER_MEDIA		= "/"PX_STRINGIZE(MEDIA_APEX);
const char* RENDERER_PATH_UNDER_MEDIA	= "/"PX_STRINGIZE(MEDIA_SAMPLERENDERER);

#ifndef PX_DISABLE_USER_PROFILER_CALLBACK

#if defined(RENDERER_ANDROID)
#define RENDERER_TABLET
#define LOGI(...) LOG_INFO("SampleApexApplication", __VA_ARGS__)
#include "SamplePlatform.h"
#include "SampleMaterialAsset.h"
const char* controlMaterialPath			= "materials/tablet_sticks.xml";
const char* buttonMaterialPath			= "materials/tablet_buttons.xml";

const char* RENDERER_MEDIA_ON_TABLET	= "/"PX_STRINGIZE(MEDIA_ON_TABLET);
#endif

#endif

SampleApexApplication::SampleApexApplication(const SampleFramework::SampleCommandLine& cmdline, physx::PxI32 camMoveButton)
	: SampleApplication(cmdline,
                    /*
                    	Android devices can't search for 'media' directory upwards with regard to executable's path. So Renderer inside SampleApplication needs exact path.
                    */
#if defined(PX_ANDROID)
	                    RENDERER_MEDIA_ON_TABLET,
#else
	                    "media",
#endif
	                    camMoveButton)

	, m_apexSDK(NULL)
	, m_apexRenderDebug(NULL)
	, m_resourceCallback(NULL)
	, m_renderResourceManager(NULL)
	, m_apexRenderer(NULL)
	, m_allocator(NULL)
	, m_errorCallback(NULL)
	, m_cpuDispatcher(NULL)
	, m_cudaContext(NULL)
#if defined(PX_PS3) && NX_SDK_VERSION_MAJOR == 3
	, m_spuDispatcher(NULL)
#endif
	, m_unitBox(NULL)
	, m_simpleLitMaterial(NULL)
	, m_simpleUnlitMaterial(NULL)
	, m_simpleLitColorMaterial(NULL)
	, m_simpleTransparentMaterial(NULL)
	, m_projection(45.0f, 1.0f, 0.1f, 10000.0f)
	, m_pause(false)
	, m_step(false)
	, m_pvdConnectionFlags(physx::debugger::PvdConnectionType::Profile | physx::debugger::PvdConnectionType::Memory)
	, m_cpuThreadCount(0) // 0 signifies the default configuration
	, m_enableGpuPhysX(true)
	, m_simulateRigidBodiesOnGPU(false)
	, m_apexScene(NULL)
	, m_collisionObjRadius(0.3f)
	, m_simulationBudget(50000.0f)
	, m_assetPreference(ANY_ASSET)
	, m_currentDebugRenderConfig(0)
	, m_OaInterface(NULL)
	, m_displayResolution(displayResType::res_800x600)
	, m_benchmarkNumFrames(1000)
	, m_benchmarkNumActors(5)
	, m_benchmarkMaxHitCount(1000000)
	, m_FrameNumber(0)
	, m_BenchmarkElapsedTime(0.0f)
	, m_physicalLOD(-1.0f)
	, m_BenchmarkStart(NULL)
	, m_shuttingDown(false)
	, m_BenchmarkMode(false)
	, m_CmdLineBenchmarkMode(false)
	, m_dataCollectionStarted(false)
	, m_dataCollectionStartTime(0.0f)
	, m_physxSDK(NULL)
	, m_physxCooking(NULL)
#if NX_SDK_VERSION_MAJOR == 3
	, m_material(NULL)
#endif
	, m_displayApexStats(false)
	, m_outFile(NULL)
	, m_stdOutReplacement(NULL)
	, m_dumpPerfData(false)
	, m_vsync(true)
	, m_allowCreateObject(true)
	, m_sceneNumberSelection(0)
	, m_displayState(DISPLAY_HELP_POPUP)
	, m_fadeOutDisplay(true)
	, m_helpPage(0)
	, m_textAlpha(3.0f)
	, m_commandLineInput(cmdline)
{
#if defined(PX_ANDROID)
	m_resourceDir = RENDERER_MEDIA_ON_TABLET;
#else
	m_resourceDir = m_assetPathPrefix;
#endif
	m_resourceDir.append(ASSET_PATH_UNDER_MEDIA);

	if (cmdline.hasSwitch("prefer-apb"))
	{
		m_assetPreference = BIN_ASSET;
	}

	if (cmdline.hasSwitch("prefer-apx"))
	{
		m_assetPreference = XML_ASSET;
	}

	setRightStickRotate(true); // APEX samples want the right stick to control the camera rotation, left stick move/strafe

	m_commandLineInput.registerCommands(&SampleCommandLineInputs[SampleCommandLineInputIds::COMMON_IDS_START], SampleCommandLineInputIds::NUM_COMMON_IDS);
}

void SampleApexApplication::onOpen()
{
	m_renDesc.vsync = m_vsync;

	SampleApplication::onOpen();

	// parse the command line arguments
	for (ProcessedCommandLineInputs::const_iterator it = m_commandLineInput.inputCommandsBegin();
	        it != m_commandLineInput.inputCommandsEnd();
	        ++it)
	{
		switch (it->id())
		{
		case SCLIDS::HELP:
			printCommandHelp();
			break;
		default:
			break;
		};
	}

	printUnusedCommands();
}

void SampleApexApplication::sampleInit(const char* sampleName, bool useCuda, bool useCudaInterop, size_t gpuTotalMemLimit)
{
	m_sampleName = sampleName;

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();

	if (!renderer || !assetManager)
	{
		return;
	}

#if defined(PX_ANDROID)
	std::string rendererMedia = RENDERER_MEDIA_ON_TABLET;
#else
	std::string rendererMedia = m_assetPathPrefix;
#endif

	rendererMedia += RENDERER_PATH_UNDER_MEDIA;
	renderer->setAssetDir(rendererMedia.c_str());

	if (m_recordRrmFile.empty())
	{
		m_renderResourceManager = new SampleApexRenderResourceManager(*renderer);
		m_apexRenderer = new SampleApexRenderer();
	}
	else
	{
		SampleApexRenderer* realRenderer = NULL;
		SampleApexRenderResourceManager* realRrm = NULL;

		realRenderer = new SampleApexRenderer();
		realRrm = new SampleApexRenderResourceManager(*renderer);

		//FileRecorder* recorder = new FileRecorder(NULL);
		FileRecorder* recorder = new FileRecorder(m_recordRrmFile.c_str());

		m_renderResourceManager = new RecordingRenderResourceManager(realRrm, true, recorder);
		m_apexRenderer = new RecordingRenderer(realRenderer, recorder);
	}

	m_resourceCallback = new SampleApexResourceCallback(*renderer, *assetManager);
	m_resourceCallback->setAssetPreference(m_assetPreference);

	m_unitBox = new SampleRenderer::RendererBoxShape(*renderer, physx::PxVec3(1, 1, 1));

	std::string sampleMedia = m_resourceDir + "/" + sampleName + "/";
	std::string commonMedia = m_resourceDir + "/common/";

	m_resourceCallback->addResourceSearchPath("./");
	m_resourceCallback->addResourceSearchPath(sampleMedia.c_str());
	m_resourceCallback->addResourceSearchPath(commonMedia.c_str());

	SampleFramework::addSearchPath(rendererMedia.c_str());
	SampleFramework::addSearchPath(sampleMedia.c_str());
	SampleFramework::addSearchPath(commonMedia.c_str());

#if defined(PX_WINDOWS)
	std::string loc = m_resourceDir +  "/common/bin/PxUserProfilerCallback";
#if defined(PX_X86)
	loc += "_x86";
#elif defined(PX_X64)
	loc += "_x64";
#endif
	loc += ".dll";
	m_allocator = ::new UserPxAllocator("APEX", loc.c_str(), !m_commandLineInput.hasInputCommand(SCLIDS::NO_MEM_TRACKER));

	std::string errorFile = "$" + m_sampleName + "_errors.txt";
	m_errorCallback = ::new UserErrorCallback(errorFile.c_str(), "a", true, m_OaInterface == NULL);
#else
	m_allocator = ::new UserPxAllocator("APEX", NULL, !m_commandLineInput.hasInputCommand(SCLIDS::NO_MEM_TRACKER));
	// APEX requires an error callback, so create an innocuous on consoles

#if defined(PX_ANDROID)
	std::string errorFile = "/sdcard/media/" + m_sampleName + "_errors.txt";
	LOGI("m_errorCallback: writing to '%s'", errorFile.c_str());
	m_errorCallback = ::new UserErrorCallback(errorFile.c_str(), "a", false, false);
#else
	m_errorCallback = ::new UserErrorCallback(NULL /*errorFile.c_str()*/, "a", false, false);
#endif

#endif

	openAutomateInterface::setAllocatorForTracking(m_allocator);

	// ******* Start of PhysX version specific virtual functions

	if (!initSDKs(useApplicationProfiler()))
	{
		return;
	}

	createThreadPool(m_cpuThreadCount);

	if (useCuda)
	{
		createCudaContext(useCudaInterop, gpuTotalMemLimit);
	}

	// ******* End of PhysX version specific virtual functions

	renderer->initTexter();

	m_simpleLitMaterial = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset("materials/simple_lit.xml", SampleFramework::SampleAsset::ASSET_MATERIAL));
	m_simpleLitColorMaterial = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset("materials/simple_lit_color.xml", SampleFramework::SampleAsset::ASSET_MATERIAL));
	m_simpleUnlitMaterial = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset("materials/simple_unlit.xml", SampleFramework::SampleAsset::ASSET_MATERIAL));
	m_simpleTransparentMaterial = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset("materials/simple_lit_alpha.xml", SampleFramework::SampleAsset::ASSET_MATERIAL));

	renderer->initScreenquad();

#if defined(RENDERER_TABLET)
	// load touchscreen control material
	SampleFramework::SampleMaterialAsset* m_controlMaterialAsset = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset(controlMaterialPath, SampleFramework::SampleAsset::ASSET_MATERIAL));
	PX_ASSERT(m_controlMaterialAsset);
	PX_ASSERT(m_controlMaterialAsset->getType() == SampleFramework::SampleAsset::ASSET_MATERIAL);
	if (m_controlMaterialAsset)
	{
		renderer->initControls(m_controlMaterialAsset->getMaterial(0),
		                       m_controlMaterialAsset->getMaterialInstance(0));
	}
	else
	{
#if defined(PX_ANDROID)
		LOGI("Unable to load tablet controls material!");
#endif
	}
	SampleFramework::SampleMaterialAsset* m_buttonMaterialAsset = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset(buttonMaterialPath, SampleFramework::SampleAsset::ASSET_MATERIAL));
	PX_ASSERT(m_buttonMaterialAsset);
	PX_ASSERT(m_buttonMaterialAsset->getType() == SampleFramework::SampleAsset::ASSET_MATERIAL);
	if (!m_buttonMaterialAsset)
	{
#if defined(PX_ANDROID)
		LOGI("Unable to load tablet buttons material!");
#endif
	}
	else
	{
		// add some buttons
		physx::PxReal yInc = -0.09, xInc = 0.05;
		physx::PxVec2 leftBottom(0.75f, 0.90f);
		physx::PxVec2 rightTop(0.99f, 0.82f);
		renderer->addButton(leftBottom, rightTop, NULL, m_buttonMaterialAsset->getMaterial(0), m_buttonMaterialAsset->getMaterialInstance(0));
		leftBottom.y += yInc;
		rightTop.y += yInc;
		renderer->addButton(leftBottom, rightTop, NULL, m_buttonMaterialAsset->getMaterial(0), m_buttonMaterialAsset->getMaterialInstance(0));
		leftBottom.y += yInc;
		rightTop.y += yInc;
		renderer->addButton(leftBottom, rightTop, NULL, m_buttonMaterialAsset->getMaterial(0), m_buttonMaterialAsset->getMaterialInstance(0));
		leftBottom.y += yInc;
		rightTop.y += yInc;
		renderer->addButton(leftBottom, rightTop, NULL, m_buttonMaterialAsset->getMaterial(0), m_buttonMaterialAsset->getMaterialInstance(0));
		leftBottom.y += yInc;
		rightTop.y += yInc;
		renderer->addButton(leftBottom, rightTop, NULL, m_buttonMaterialAsset->getMaterial(0), m_buttonMaterialAsset->getMaterialInstance(0));
	}
#endif

	m_apexRenderDebug = m_apexSDK->createApexRenderDebug();

	// look for PVD connection
	if (const char* PVD_host = inputCommandValue(SCLIDS::PVD_HOST))
	{
		m_pvdHostName = PVD_host;
	}
#if SAMPLES_USE_VRD
	togglePvdConnection(m_pvdHostName.c_str(), m_pvdConnectionFlags);
#endif

	getPlatform()->getSampleUserInput()->registerInputEventListerner(this);

	registerInputEvents();
}

void SampleApexApplication::onShutdown()
{
	getPlatform()->getSampleUserInput()->registerInputEventListerner(NULL);

	SampleRenderer::Renderer* renderer = getRenderer();
	if (renderer)
	{
		renderer->closeTexter();
		renderer->closeScreenquad();
	}

	for (physx::PxU32 i = 0; i < m_lights.size(); i++)
	{
		m_lights[i]->release();
	}
	m_lights.clear();

	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		m_actors[i]->release();
	}
	m_actors.clear();

	for (physx::PxU32 i = 0; i < m_shape_actors.size(); i++)
	{
		if (m_shape_actors[i])
		{
			m_shape_actors[i]->release();
		}
	}
	m_shape_actors.clear();

	if (m_unitBox)
	{
		delete m_unitBox;
		m_unitBox = 0;
	}

	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
#if defined(RENDERER_TABLET)
	if (m_controlMaterialAsset && assetManager)
	{
		assetManager->returnAsset(*m_controlMaterialAsset);
		m_controlMaterialAsset = 0;
	}
	if (m_buttonMaterialAsset && assetManager)
	{
		assetManager->returnAsset(*m_buttonMaterialAsset);
		m_buttonMaterialAsset = 0;
	}
#endif
	if (m_simpleLitMaterial && assetManager)
	{
		assetManager->returnAsset(*m_simpleLitMaterial);
		m_simpleLitMaterial = 0;
	}
	if (m_simpleLitColorMaterial && assetManager)
	{
		assetManager->returnAsset(*m_simpleLitColorMaterial);
		m_simpleLitColorMaterial = 0;
	}
	if (m_simpleUnlitMaterial && assetManager)
	{
		assetManager->returnAsset(*m_simpleUnlitMaterial);
		m_simpleUnlitMaterial = 0;
	}
	if (m_simpleTransparentMaterial && assetManager)
	{
		assetManager->returnAsset(*m_simpleTransparentMaterial);
		m_simpleTransparentMaterial = 0;
	}

	if (m_apexModuleList.size())
	{
		for (physx::PxU32 i = 0; i < m_apexModuleList.size(); i++)
		{
			if (m_apexModuleList[i])
			{
				m_apexSDK->releaseModule(m_apexModuleList[i]);
				m_apexModuleList[i] = 0;
			}
		}
		m_apexModuleList.clear();
	}

	if (m_apexScene)
	{
		shutdownScene(*m_apexScene);
		m_apexSDK->releaseScene(m_apexScene);
		m_apexScene = NULL;
	}

	if (m_apexRenderDebug)
	{
		m_apexSDK->releaseApexRenderDebug(*m_apexRenderDebug);
		m_apexRenderDebug = NULL;
	}

#if NX_SDK_VERSION_MAJOR == 2
	if (m_cpuDispatcher)
	{
		m_apexSDK->releaseCpuDispatcher(*m_cpuDispatcher);
		m_cpuDispatcher = NULL;
	}
#endif

#if defined(PX_WINDOWS)
	if (m_cudaContext)
	{
		m_cudaContext->release();
		m_cudaContext = 0;
	}
#endif
#if defined(PX_WINDOWS)
#endif

	if (m_apexSDK)
	{
		m_apexSDK->release();
		m_apexSDK = NULL;
	}


	shutdownSDKs();

	if (m_allocator)
	{
		delete m_allocator;
		m_allocator = NULL;
	}

	if (m_resourceCallback)
	{
		delete m_resourceCallback;
		m_resourceCallback = NULL;
	}

	if (m_apexRenderer)
	{
		delete m_apexRenderer;
		m_apexRenderer	= NULL;
	}

	if (m_renderResourceManager)
	{
		delete m_renderResourceManager;
		m_renderResourceManager = NULL;
	}

	if (m_errorCallback != NULL)
	{
		delete m_errorCallback;
		m_errorCallback = NULL;
	}
	if (!m_statAverages.empty())
	{
		for (size_t i = 0; i < m_statAverages.size(); i++)
		{
			delete m_statAverages[i];
		}
		m_statAverages.clear();
	}

	m_shuttingDown = true;
}

SampleApexApplication::~SampleApexApplication()
{
	// Hitting this assert means the application did not call SampleApexApplication::onShutdown
	PX_ASSERT(m_shuttingDown);

	std::string leakFile = "$" + m_sampleName + "_leaks.html";
	bool leaked = UserPxAllocator::dumpMemoryLeaks(leakFile.c_str());
	PX_FORCE_PARAMETER_REFERENCE(leaked);
#if defined(PX_WINDOWS)
	PX_ASSERT(!leaked);
#endif

	closeApexStatsFile();

	if (m_stdOutReplacement != NULL)
	{
		fclose(m_stdOutReplacement);
		m_stdOutReplacement = NULL;
	}
}

void SampleApexApplication::printCommandHelp() const
{
	std::string commandHelp = m_commandLineInput.validCommands();
#if defined(PX_WINDOWS)
	MessageBoxA(NULL, commandHelp.c_str(), "APEX Sample Help", MB_OK);
#endif
	while (commandHelp.size() > 0)
	{
		size_t tempSize = std::min((size_t)2047, commandHelp.size());
		std::string tmpCommandHelp = commandHelp.substr(0, tempSize);
		ERRORSTREAM_DEBUG_INFO(tmpCommandHelp.c_str());
		commandHelp = commandHelp.substr(tempSize);
	}
}

void SampleApexApplication::printUnusedCommands() const
{
	physx::PxU32 unusedSize = m_cmdline.unusedArgsBufSize();
	if (unusedSize)
	{
		std::vector<char> tmpBuf(unusedSize);
		char* tmpBufPtr = &tmpBuf[0];
		{
			m_cmdline.getUnusedArgs(tmpBufPtr, unusedSize);
#if defined(PX_WINDOWS)
			MessageBoxA(NULL, tmpBufPtr, "Error: unknown command line arguments", MB_OK);
#endif
			ERRORSTREAM_DEBUG_INFO("Error: unknown command line arguments\n");
			ERRORSTREAM_DEBUG_INFO(tmpBufPtr);
			ERRORSTREAM_DEBUG_INFO("\n");
		}
	}
}

using SampleFramework::SampleUserInput;

bool SampleApexApplication::isInputEventActive(physx::PxU16 inputEventId)
{
	return getPlatform()->getSampleUserInput()->getDigitalInputEventState(inputEventId);
}

const char* SampleApexApplication::inputInfoMsg(physx::PxI32 inputEventId1, physx::PxI32 inputEventId2)
{
	using namespace physx;

	enum { sMsgSize = 1024 };
	static char sMsg[sMsgSize];

	SampleUserInput* userInput = m_platform->getSampleUserInput();
	if (NULL == userInput)
		return NULL;

	std::stringstream ss;

	PxU32 inputTypeMask = SampleFramework::KEYBOARD_INPUT |
	                      SampleFramework::GAMEPAD_INPUT |
	                      SampleFramework::TOUCH_BUTTON_INPUT |
	                      SampleFramework::TOUCH_PAD_INPUT |
	                      SampleFramework::MOUSE_INPUT;

	const char* keyNames1[5];
	PxU16 numNames1 = userInput->getUserInputKeys(inputEventId1, keyNames1, 5, inputTypeMask);

	const char* keyNames2[5];
	PxU16 numNames2 = userInput->getUserInputKeys(inputEventId2, keyNames2, 5, inputTypeMask);

	if (!numNames1 && !numNames2)
	{
		return NULL;
	}

	ss << "Press ";
	// Input key names
	for (PxU16 i = 0; i < numNames1; ++i)
	{
		ss << keyNames1[i];
		if (numNames2 || (i < (numNames1-1)))
			ss << " or ";
	}

	for (PxU16 i = 0; i < numNames2; ++i)
	{
		ss << keyNames2[i];
		if (i < (numNames2-1))
		{
			ss << " or ";
		}
	}

	ss << " to ";

	// Input event names

	const SampleFramework::InputEvent* inputEvents[2] =
	  { inputEventId1 >= 0 ? userInput->getInputEventSlow(inputEventId1) : NULL,
	    inputEventId2 >= 0 ? userInput->getInputEventSlow(inputEventId2) : NULL };
	bool inputEventNamePrinted = false;
	for (PxU32 i = 0; i < PX_ARRAY_SIZE(inputEvents); ++i) 
	{
		if (NULL == inputEvents[i] || empty(inputEvents[i]->m_Name))
			continue;

		if (inputEventNamePrinted)
			ss << "/";

		inputEventNamePrinted = true;
		ss << inputEvents[i]->m_Name;
	}

	PxStrcpy(sMsg, sMsgSize-1, ss.str().c_str());

	return sMsg;
}

void SampleApexApplication::printHelp(physx::PxU32 x, physx::PxU32 y)
{
	using namespace physx;
	using namespace SampleFramework;
	using namespace SampleRenderer;

	Renderer* renderer = getRenderer();
	if (m_textAlpha <= 0.0f || NULL == renderer)
	{
		return;
	}

	const PxReal scale        = 0.5f;
	const PxReal shadowOffset = 6.0f;
	const RendererColor textColor(220, 220, 220, (PxU32)(PxMin(m_textAlpha, 1.f) * 255.0f));

	if (m_displayState == DISPLAY_HELP_POPUP)
	{
		if (m_helpPopupText.empty())
		{
			m_helpPopupText = inputInfoMsg(SHOW_HELP);
		}
		print(x, y, m_helpPopupText.c_str(), scale, shadowOffset, textColor, true);
		return;
	}

	const PxU32 yInc = 18;
	y += yInc;

	PxU32 width, height;
	renderer->getWindowSize(width, height);
	static const PxI16 linesToSkip = 4;
	// Skip a few lines, and leave room for each input event
	PxI16 inputEventsPerPage = (height - y) / yInc - linesToSkip;
	if (inputEventsPerPage <= 0)
		return;

	const std::vector<InputEvent> inputEventList = getPlatform()->getSampleUserInput()->getInputEventList();
	const PxU16 maxHelpPage = (PxU16)inputEventList.size() / inputEventsPerPage;
	m_helpPage = PxClamp(m_helpPage, (PxU16)0, maxHelpPage);

	PxU16 eventsPrinted   = 0;
	PxU16 eventsProcessed = 0;

	for (PxU32 i = 0; i < inputEventList.size(); ++i)
	{
		const InputEvent& ie = inputEventList[i];

		// Skip inputs with no name
		if (empty(ie.m_Name))
			continue;

		// Skip inputs with no info
		const char* msg = inputInfoMsg(ie.m_Id);
		if (empty(msg))
			continue;

		// Skip events prior to the current page
		if (++eventsProcessed <= inputEventsPerPage * m_helpPage)
			continue;

		// Skip touch events, because they are self-explanatory
		SampleFramework::UserInput ui;
		// convert input id to key id
		ui.m_Id = (PxU16)(*getPlatform()->getSampleUserInput()->getUserInputs(ie.m_Id))[0];
		if(getPlatform()->getSampleUserInput()->getInputType(ui) == SampleFramework::TOUCH_BUTTON_INPUT)
			continue;
		// Skip gamepad inputs
		else if(getPlatform()->getSampleUserInput()->getInputType(ui) == SampleFramework::GAMEPAD_INPUT)
			continue;

		print(x, y, msg, scale, shadowOffset, textColor, true);
		y += yInc;

		if (++eventsPrinted >= inputEventsPerPage)
		{
			break;
		}
	}

	if (0 == eventsPrinted && m_helpPage > 0)
	{
		--m_helpPage;
	}

	y += yInc;
	print(x, y, inputInfoMsg(NEXT_PAGE, PREVIOUS_PAGE), scale, shadowOffset, textColor, true);
}



void SampleApexApplication::printSceneSelection(physx::PxU32 x, physx::PxU32 y)
{
	const unsigned int numScenes = getNumScenes();

	const unsigned int bufLen = 128;
	char buf[bufLen];

	unsigned int yInc = 18;
	unsigned int yIncDir = 22;
	y += yInc;

	const float scale        = 0.5f;
	const float scaleDir     = 0.6f;
	const float shadowOffset = 6.0f;

	unsigned int selectedScene = getSelectedScene();

	physx::string::strcpy_s(buf, bufLen, getSceneName(selectedScene));
	char* group = strrchr(buf, '/');
	if (group != NULL)
	{
		*group = 0;
	}
	std::string selectedGroup = buf;
	physx::string::strcpy_s(buf, bufLen, getSceneName(m_sceneNumberSelection));
	group = strrchr(buf, '/');
	if (group != NULL)
	{
		*group = 0;
	}
	std::string selectionGroup = buf;

	std::string lastGroup;

	for (unsigned int i = 1; i < numScenes; i++)
	{
		const char* sceneName = getSceneName(i);

		if (sceneName != NULL)
		{
			physx::PxU8 textAlpha = (physx::PxU8)(physx::PxClamp(m_textAlpha, 0.0f, 1.0f) * 255.0f);
			SampleRenderer::RendererColor textColor(128, 128, 128, textAlpha);

			buf[0] = buf[1] = ' ';
			physx::string::strcpy_s(buf + 2, bufLen - 2, sceneName);
			group = strrchr(buf, '/');
			if (group != NULL)
			{
				*group = 0;
			}
			else
			{
				buf[0] = 0;
			}

			const char* shortName = group != NULL ? sceneName + (group - (buf + 2) + 1) : sceneName;

			bool openGroup = /*selectedGroup == buf + 2 || */selectionGroup == buf + 2;

			if (group != NULL && lastGroup != buf + 2)
			{
				buf[0] = openGroup ? '-' : '+';
				print(x, y, buf, scaleDir, shadowOffset, textColor, true);
				y += yIncDir;

				lastGroup = buf + 2;
			}

			if (group == NULL || openGroup)
			{
				physx::string::sprintf_s(buf, bufLen, "%s%s (%d)", group != NULL ? "    " : "  ", shortName, i);

				if (i == m_sceneNumberSelection)
				{
					textColor = SampleRenderer::RendererColor(128, 255, 128, textAlpha);
				}
				else if (i == selectedScene)
				{
					textColor = SampleRenderer::RendererColor(192, 192, 192, textAlpha);
				}

				print(x, y, buf, scale, shadowOffset, textColor, true);
				y += yInc;
			}
		}
	}
}



void SampleApexApplication::setBudget(float newBudget)
{
	m_simulationBudget = newBudget;
	// TODO: screen print
	printf("resource budget: %f !\n", m_simulationBudget);
	m_apexScene->setLODResourceBudget(m_simulationBudget);
	updateScalableAndLodInfo();
}

void SampleApexApplication::forceLoadAssets()
{
	physx::PxU32 loadedAssets = m_apexSDK->forceLoadAssets();
	while (loadedAssets > 0)
	{
		loadedAssets = m_apexSDK->forceLoadAssets();
	}
}

physx::apex::NxModule* SampleApexApplication::loadModule(const char* moduleName)
{
	physx::apex::NxModule* module = NULL;
	PX_ASSERT(m_apexSDK);
	if (m_apexSDK)
	{
		module = m_apexSDK->createModule(moduleName);
		PX_ASSERT(module);
		if (module)
		{
			module->init(*module->getDefaultModuleDesc());
			m_apexModuleList.push_back(module);
		}
	}

	return module;
}


void SampleApexApplication::reportOaResultsValues()
{
	if (m_OaInterface)
	{
		m_OaInterface->reportOaResultsValues();
	}
}

void SampleApexApplication::reportOaResults()
{
	if (m_OaInterface)
	{
		m_OaInterface->reportOaStatsOptions();
	}
}

void SampleApexApplication::addDebugRenderConfig(const DebugRenderConfiguration& config)
{
#ifndef WITHOUT_DEBUG_VISUALIZE
	m_debugRenderConfigs.push_back(config);
	m_currentDebugRenderConfig = m_debugRenderConfigs.size();
#endif
}

int SampleApexApplication::applyApexDebugRenderFlag(physx::apex::NxApexScene* apexScene, const char* name, float value)
{
#ifndef WITHOUT_DEBUG_VISUALIZE
	const char* slash = strchr(name, '/');

	NxParameterized::Interface* debugRenderInterface = apexScene->getDebugRenderParams();

	if (slash)
	{
		char prefix[64];
		strncpy(prefix, name, slash - name);
		prefix[slash - name] = 0;
		debugRenderInterface = apexScene->getModuleDebugRenderParams(prefix);
	}

	if (debugRenderInterface)
	{
		const char* realName = (slash != NULL) ? slash + 1 : name;

		NxParameterized::Handle handle(*debugRenderInterface, realName);
		PX_ASSERT(handle.isValid());
		if (handle.parameterDefinition()->type() == NxParameterized::TYPE_F32)
		{
			handle.setParamF32(value);
		}
		else if (handle.parameterDefinition()->type() == NxParameterized::TYPE_U32)
		{
			handle.setParamU32(physx::PxU32(value));
		}
		else if (handle.parameterDefinition()->type() == NxParameterized::TYPE_BOOL)
		{
			handle.setParamBool(value > 0.0f);
		}
		else
		{
			PX_ALWAYS_ASSERT();
		}
		return value > 0.0f ? 1 : 0;
	}
#endif

	return 0;
}

void SampleApexApplication::nextDebugRenderConfig(int change)
{
#ifndef WITHOUT_DEBUG_VISUALIZE
	m_currentDebugRenderConfig += change;

	if (m_currentDebugRenderConfig == m_debugRenderConfigs.size() + 1)
	{
		m_currentDebugRenderConfig = 0;
	}
	else if (m_currentDebugRenderConfig > m_debugRenderConfigs.size())
	{
		m_currentDebugRenderConfig = m_debugRenderConfigs.size();
	}

	applyDebugRenderConfig(m_currentDebugRenderConfig, m_apexScene, m_sceneSize);
#endif
}

void SampleApexApplication::clearDebugRenderConfig()
{
#ifndef WITHOUT_DEBUG_VISUALIZE
	m_currentDebugRenderConfig = m_debugRenderConfigs.size();
	applyDebugRenderConfig(m_currentDebugRenderConfig, m_apexScene, m_sceneSize);
	m_debugRenderConfigs.clear();
#endif
}

std::string SampleApexApplication::getDebugRenderConfigName()
{
	std::string result;

#ifndef WITHOUT_DEBUG_VISUALIZE
	if (m_currentDebugRenderConfig < m_debugRenderConfigs.size())
	{
		char temp[32];
		physx::string::sprintf_s(temp, 32, "Debug Rendering (%d/%d):", (int)m_currentDebugRenderConfig + 1, (int)m_debugRenderConfigs.size());
		result.append(temp);

		const DebugRenderConfiguration& config = m_debugRenderConfigs[m_currentDebugRenderConfig];

		for (size_t i = 0; i < config.flags.size(); i++)
		{
			result.append("\n");
			result.append(config.flags[i].fullName);
		}
	}
#endif

	return result;
}

void SampleApexApplication::initBenchmarks(void (*benchmarkStart)(const char*))
{
	PX_ASSERT(!m_benchmarkNames.empty());
	m_BenchmarkStart = benchmarkStart;
	setBenchmarkEnable(false);
	m_CmdLineBenchmarkMode = false;
}


displayResType::Enum SampleApexApplication::processDisplayOption(const char* str)
{
	if (str == NULL)
	{
		ERRORSTREAM_DEBUG_ERROR(WINDOW_SIZE_STR " argument missing value!  Assumed %s",
		                        openAutomateInterface::DisplayResolutions[displayResType::res_640x480].resolutionName);
		return displayResType::res_640x480;
	}
	for (unsigned int j = 0; j < displayResType::NUM_DISPLAY_RESOLUTIONS; j++)
	{
		if (strcmp(openAutomateInterface::DisplayResolutions[j].resolutionName, str) == 0)
		{
			return((displayResType::Enum) j);
		}
	}
	ERRORSTREAM_DEBUG_ERROR("ERROR: invalid display resolution!\n");
	return(displayResType::res_640x480);
}

int	SampleApexApplication::getDisplayWidth()
{
	return(openAutomateInterface::DisplayResolutions[m_displayResolution].width);
}

int	SampleApexApplication::getDisplayHeight()
{
	return(openAutomateInterface::DisplayResolutions[m_displayResolution].height);
}

bool SampleApexApplication::rendererOverride()
{
	bool overriden =	
#if defined(RENDERER_ENABLE_DIRECT3D9)
	                hasInputCommand(SCLIDS::D3D9)  ||
#endif
#if defined(RENDERER_ENABLE_DIRECT3D11)
	                hasInputCommand(SCLIDS::D3D11) ||
#endif
	                false;

	return overriden;
}

static void runNormalMode(void) {}

// Process the command line args that need to be processed before the window is open
void SampleApexApplication::processEarlyCmdLineArgs(const char* ProgramVersion, oa_OptionsOverrideDesc* desc)
{
	for (ProcessedCommandLineInputs::const_iterator it = m_commandLineInput.inputCommandsBegin();
	        it != m_commandLineInput.inputCommandsEnd();
	        ++it)
	{
		const char* argPtr = it->value();
		switch (it->id())
		{
		case SCLIDS::INSTALL:
		{
			m_OaInterface = ::new openAutomateInterface(ProgramVersion, &m_cmdline);
			m_OaInterface->RegisterAppItself();
			// Don't delete m_OaInterface; since no foundation class has been created.
			exit(0);
		}
		break;
		case SCLIDS::UNINSTALL:
		{
			m_OaInterface = ::new openAutomateInterface(ProgramVersion, &m_cmdline);
			m_OaInterface->UnRegisterAppItself();
			// Don't delete m_OaInterface; since no foundation class has been created.
			exit(0);
		}
		break;
		case SCLIDS::CREATE_OA_FILE:
		{
#if NX_SDK_VERSION_MAJOR == 2
			// create a stats file
			sampleInit("createOaStatsOptionsFile", false, false);
			physx::apex::NxApexScene* scene = createSampleScene(physx::PxVec3(0, -9.8f, 0));
			if (scene)
			{
				m_OaInterface->createOaStatsOptionsFile(scene->getPhysXScene());
			}
			onShutdown();
#endif
			exit(0);
		}
		break;
		case SCLIDS::PVD_DEBUG:
		{
			// use the exhaustive connection flags (debug, memory, and profile)
			// APEX Sample Framework disables "debug" by default
			//  Note: Simultaneous debug, memory and profile is not currently supported with 284
#if NX_SDK_VERSION_MAJOR == 2	
			m_pvdConnectionFlags = physx::debugger::PvdConnectionType::Debug;
#else
			m_pvdConnectionFlags = physx::debugger::defaultConnectionFlags();
#endif
		}
		break;
		case SCLIDS::PHYSICAL_LOD:
			convert(argPtr, m_physicalLOD);
			break;
		case SCLIDS::BUDGET:
			convert(argPtr, m_simulationBudget);
			break;
		case SCLIDS::WINDOW:
			if (argPtr)
			{
				m_displayResolution = processDisplayOption(argPtr);
			}
			break;
		case SCLIDS::NUM_FRAMES:
			convert(argPtr, m_benchmarkNumFrames);
			break;
		case SCLIDS::NUM_ACTORS:
			convert(argPtr, m_benchmarkNumActors);
			break;
		case SCLIDS::MAX_HIT_COUNT:
			convert(argPtr, m_benchmarkMaxHitCount);
			break;
		case SCLIDS::CPU_THREAD_COUNT:
			convert(argPtr, m_cpuThreadCount);
			break;
		case SCLIDS::NO_GPU_PHYSX:
			m_enableGpuPhysX = false;
		break;
		case SCLIDS::RECORD_RRM:
			if (argPtr)
			{
				m_recordRrmFile = argPtr;
			}
			break;
		case SCLIDS::OPEN_AUTOMATE:
			if (argPtr)
			{
				if (m_BenchmarkStart == NULL)
				{
					ERRORSTREAM_DEBUG_ERROR("ERROR: No benchmark defined!\n");
					exit(1);
				}
				if (m_benchmarkNames.empty())
				{
					ERRORSTREAM_DEBUG_ERROR("ERROR: Benchmark name list not defined!\n");
					exit(1);
				}

				m_OaInterface = ::new(openAutomateInterface)(argPtr,
				                m_cmdline.getProgramName(),
				                &runNormalMode,
				                m_BenchmarkStart,
				                m_benchmarkNames,
				                &m_cmdline,
				                ProgramVersion,
								desc);
				m_BenchmarkMode = true;
				m_vsync = false;
				m_simulationBudget = (float)m_OaInterface->getSimulationBudget();
				m_displayResolution = m_OaInterface->getDisplayResolution();
				m_cpuThreadCount = m_OaInterface->getBenchmarkNumCpuThreads();
				m_simulateRigidBodiesOnGPU = m_OaInterface->getGRBenabled();
			}
			break;
		case SCLIDS::DISABLE_RENDERING:
		{
			m_disableRendering = true;
		}
		break;
		case SCLIDS::DUMP_PERF_DATA:
		{
			m_dumpPerfData = true;
		}
		break;
		case SCLIDS::DISABLE_VSYNC:
		{
			m_vsync = false;
		}
		break;
		case SCLIDS::PROFILER:
		{
		}
		break;
		default:
			break;
		};
	}
}

void SampleApexApplication::releaseApexAsset(physx::apex::NxApexAsset* asset)
{
	if (asset)
	{
		m_resourceCallback->releaseResource(asset->getObjTypeName(), asset->getName(), asset);
	}
}

physx::apex::NxApexAsset* SampleApexApplication::loadApexAsset(const char* nameSpace, const char* name, SampleAssetFileType assetType)
{
	if (strrchr(name, '.') != NULL)
	{
		// a '.' is in the path, let's see if it's in the path or marks the extension
		const char* lastSlash = std::max(strrchr(name, '/'), strrchr(name, '\\'));
		const char* lastDot = strrchr(name, '.');
		if (lastDot > lastSlash)
		{
			physx::apex::NxApexAsset* specificAsset = tryLoadApexAsset(nameSpace, name, "");
			PX_ASSERT(specificAsset);
#if defined(PX_WINDOWS)
			if (specificAsset == NULL)
			{
				std::string message("Missing APEX Asset: ");
				message += name;
				MessageBoxA(NULL, message.c_str(), "Missing APEX Asset", MB_OK);
			}
#endif
			return specificAsset;
		}
	}

	bool apx = XML_ASSET == assetType || ANY_ASSET == assetType,
	     apb = BIN_ASSET == assetType || ANY_ASSET == assetType;

	physx::apex::NxApexAsset* asset = NULL;

	if (XML_ASSET == m_assetPreference)
	{
		if (apx && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apx");
		}

		if (apb && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apb");
		}
	}
	else if (BIN_ASSET == m_assetPreference)
	{
		if (apb && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apb");
		}

		if (apx && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apx");
		}
	}
	else
	{
		// We prefer binary files in shipping builds

#		ifdef APEX_SHIPPING
		if (apb && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apb");
		}
#		endif

		if (apx && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apx");
		}

#		ifndef APEX_SHIPPING
		if (apb && !asset)
		{
			asset = tryLoadApexAsset(nameSpace, name, ".apb");
		}
#		endif
	}

	PX_ASSERT(asset);

#if defined(PX_WINDOWS)
	if (asset == NULL)
	{
		std::string message("Missing APEX Asset: ");
		message += name;
		MessageBoxA(NULL, message.c_str(), "Missing APEX Asset", MB_OK);
	}
#endif
	return asset;
}



void SampleApexApplication::clearAverageStats()
{
	for (size_t i = 0; i < m_statAverages.size(); i++)
	{
		m_statAverages[i]->clearAverage();
	}
}


void SampleApexApplication::printSceneInfo(const char* sceneInfo,
										   const char* additionalInfo,
										   bool bPrintFPS, bool bPrintDebug)
{
	const char* delim = " - ";

	std::stringstream ss;
	ss << m_sampleName << delim << getSelectedScene() << delim << getSceneName(getSelectedScene());
	if (!empty(sceneInfo))
	{
		if (!newline(sceneInfo))
			ss << delim;
		ss << sceneInfo;
	}

	if (bPrintFPS)
	{
		float fps = 0.0f;
		computeFps(fps);

		ss << std::endl;
		if (m_BenchmarkMode)
		{
			physx::PxU32 framesLeft;
			if(m_FrameNumber < m_benchmarkNumFrames)
			{
				framesLeft = m_benchmarkNumFrames - m_FrameNumber;
			}
			else
			{
				framesLeft = 0;
			}
			ss << "Benchmarking!  FramesLeft = " << framesLeft << delim;
		}
		ss << "FPS = " << (int)fps;
	}

	if (bPrintDebug)
	{
		const std::string& debugRenderName = getDebugRenderConfigName();
		if (!debugRenderName.empty())
		{
			ss << std::endl << std::endl << debugRenderName;
		}
	}

	if (!empty(additionalInfo))
		ss << std::endl << std::endl << additionalInfo << std::endl;

	printToScreen(ss.str().c_str(), m_apexScene, !m_pause);
}

void SampleApexApplication::printToScreen(const char* output, const physx::apex::NxApexScene* sceneForStats, bool recomputeAverages)
{
	std::string tempString = output;

	if (sceneForStats !=  NULL && m_displayApexStats)
	{
		const physx::NxApexSceneStats* stats = sceneForStats->getStats();
		if (stats && stats->ApexStatsInfoPtr)
		{
			if (m_statAverages.empty())
			{
				for (physx::PxU32 i = 0; i < stats->numApexStats; i++)
				{
					m_statAverages.push_back(new SampleAverage<physx::PxF32, 100>());
				}
			}

			for (physx::PxU32 i = 0; i < stats->numApexStats; i++)
			{
				tempString.append("\n");
				tempString.append(stats->ApexStatsInfoPtr[i].StatName);
				tempString.append(": ");

				char buf[128] = {0};

				switch (stats->ApexStatsInfoPtr[i].StatType)
				{
				case physx::ApexStatDataType::INT:
					physx::string::sprintf_s(buf, 128, "%d", stats->ApexStatsInfoPtr[i].StatCurrentValue.Int);
					break;
				case physx::ApexStatDataType::FLOAT:
				{
					// work out if the stat is a time by looking for the string "Time" at the end of the name
					bool isTime = false;
					size_t len = strlen(stats->ApexStatsInfoPtr[i].StatName);

					if (4 <= len)
					{
						const char* lastFour = stats->ApexStatsInfoPtr[i].StatName + len - 4;
						isTime = (strcmp(lastFour, "Time") == 0);
					}

					physx::PxF32 val = stats->ApexStatsInfoPtr[i].StatCurrentValue.Float;

					if (recomputeAverages)
					{
						m_statAverages[i]->addNewValue(val);
					}

					//If stat is a time, add "ms" to the end of the string
					if (isTime)
					{
						physx::string::sprintf_s(buf, 128, "%4.3f ms", m_statAverages[i]->getAverage());
					}
					else
					{
						physx::string::sprintf_s(buf, 128, "%4.3f", m_statAverages[i]->getAverage());
					}
				}
				break;
				default:
					break;
				}

				tempString.append(buf);
			}

#ifdef PX_WINDOWS  // The GPU Heap only exists on Windows
			physx::pxtask::GpuDispatcher* gd = sceneForStats->getTaskManager()->getGpuDispatcher();
			if (gd)
			{
				physx::pxtask::CudaContextManager* ctx = gd->getCudaContextManager();
				if (ctx)
				{
					physx::pxtask::CudaMemoryManager* cmm = ctx->getMemoryManager();
					if (cmm)
					{
						struct physx::pxtask::CudaMemoryManagerStats cmmStats;
						// Clear these stats in case the memory manager is not initialized.  Currently
						// this is needed for clothing since it does not use the CMM GPU memory heap.
						memset(&cmmStats, 0, sizeof(cmmStats));
						physx::pxtask::CudaBufferType t(physx::pxtask::CudaBufferMemorySpace::T_GPU,
						                                physx::pxtask::CudaBufferFlags::F_READ_WRITE);
						cmm->getStats(t, cmmStats);
						//if any GPU heap has ever been allocated then...
						if (cmmStats.maxAllocated != 0)
						{
							char buf[128] = {0};
							//report the value to OA
							//oaStatValue.Int = (oaInt)cmmStats.heapSize;
							//oaAddFrameValue("APEX_GPU_heapSize", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = (oaInt)cmmStats.totalAllocated;
							//oaAddFrameValue("APEX_GPU_totalAllocated", OA_TYPE_INT, &oaStatValue);

							physx::string::sprintf_s(buf, 128, "\nCUDA heap size: %d\n", cmmStats.heapSize / (1024 * 1024));
							tempString.append(buf);
							physx::string::sprintf_s(buf, 128, "CUDA heap allocated: %d\n", cmmStats.totalAllocated / (1024 * 1024));
							tempString.append(buf);

							//unfortunately at the present time the following values are always 0.
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::APEX].size;
							//oaAddFrameValue("APEX_GPU_Heap", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::FLUID].size;
							//oaAddFrameValue("APEX_FLUID_GPU_Heap", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::DEFORMABLE].size;
							//oaAddFrameValue("APEX_DEFORMABLE_GPU_Heap", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::GPU_UTIL].size;
							//oaAddFrameValue("APEX_GPU_UTIL_GPU_Heap", OA_TYPE_INT, &oaStatValue);
						}
					}
				}
			}
#endif

		}
	}

	physx::PxU32 xText = 10;
	physx::PxU32 yText = 10;
	const physx::PxU32 yTextInc = 22;

	print(xText, yText, tempString.c_str(), 0.6f, 6.0f, SampleRenderer::RendererColor(255, 255, 255), true);

	physx::PxU32 lineCount = (physx::PxU32)std::count(tempString.begin(), tempString.end(), '\n');
	yText += yTextInc * (lineCount + 1);

	if (m_displayState == DISPLAY_SCENE_SELECTION)
	{
		printSceneSelection(xText, yText);
	}
	else if (m_displayState != DISPLAY_NONE)
	{
		printHelp(xText, yText);
	}
}

void SampleApexApplication::openConsole(const char* windowTitle, const char* outputFile)
{
	PX_FORCE_PARAMETER_REFERENCE(windowTitle);

	if (outputFile != NULL)
	{
#if defined(PX_PS3) || defined(PX_ANDROID)
		m_stdOutReplacement = freopen(outputFile, "w", stdout);
#else
		freopen_s(&m_stdOutReplacement, outputFile, "w", stdout);
#endif
		PX_ASSERT(m_stdOutReplacement);

		printf("%s: stdout rerouted to file\n\n", outputFile);
	}
	else
	{
#ifdef PX_WINDOWS
		//open a console for printf:
		if (AllocConsole())
		{
			FILE* stream;
			freopen_s(&stream, "CONOUT$", "wb", stdout);
			freopen_s(&stream, "CONOUT$", "wb", stderr);

			PX_ASSERT(windowTitle != NULL);
			SetConsoleTitle(windowTitle);
			//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);

			CONSOLE_SCREEN_BUFFER_INFO coninfo;
			GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
			coninfo.dwSize.Y = 1000;
			SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);
		}
#endif
	}
}

void SampleApexApplication::updateApexSceneMatrices()
{
	SampleRenderer::Renderer* renderer = getRenderer();
	if (!renderer || !m_apexScene)
	{
		return;
	}

	physx::PxU32 windowWidth  = 0;
	physx::PxU32 windowHeight = 0;
	renderer->getWindowSize(windowWidth, windowHeight);
	if (windowWidth > 0 && windowHeight > 0)
	{
		physx::PxF32 p[16];
		m_projection.getColumnMajor44(p);
		physx::PxMat44 projMatrix;
		projMatrix.column0 = physx::PxVec4(p + 0);
		projMatrix.column1 = physx::PxVec4(p + 4);
		projMatrix.column2 = physx::PxVec4(p + 8);
		projMatrix.column3 = physx::PxVec4(p + 12);
		m_apexScene->setProjMatrix(projMatrix, 0);
		m_apexScene->setProjParams(0.1f, 10000.0f, 45.0f, windowWidth, windowHeight);
		m_apexScene->setViewMatrix(getViewTransform(), 0);
		m_apexScene->setUseViewProjMatrix(0, 0);
	
		if (m_apexRenderDebug)
		{
			m_apexRenderDebug->setProjectionMatrix(p);
			m_apexRenderDebug->setViewMatrix(getViewTransform().front());
		}	
	}
}

bool SampleApexApplication::computeFps(float& Fps)
{
	static physx::Time timer;
	static physx::PxF32 cumulatedFrameTime = 0;
	static physx::PxU32 frameCounter = 0;
	static physx::PxF32 curfps = 0.0f;
	bool updated = false;
	physx::PxF32 frameTime;

	frameTime = (physx::PxF32) timer.getElapsedSeconds();
	m_BenchmarkElapsedTime += (physx::PxF32) frameTime;
	cumulatedFrameTime += frameTime;
	frameCounter++;
	if (cumulatedFrameTime > 0.5f && frameCounter > 0)
	{
		// update every half second
		curfps = (physx::PxF32)frameCounter / cumulatedFrameTime;

		cumulatedFrameTime = 0;
		frameCounter = 0;
		updated = true;
	}

	Fps = curfps;
	return updated;
}


physx::apex::NxApexAsset* SampleApexApplication::tryLoadApexAsset(const char* nameSpace, const char* name, const char* ext)
{
	if (!m_apexSDK)
	{
		return 0;
	}

	if (!m_resourceCallback->doesFileExist(name, ext))
	{
		return 0;
	}

	physx::apex::NxApexAsset* asset = reinterpret_cast<physx::apex::NxApexAsset*>(m_apexSDK->getNamedResourceProvider()->getResource(nameSpace, name));
	if (asset)
	{
		m_apexSDK->getNamedResourceProvider()->setResource(nameSpace, name, asset, true);
	}

	return asset;
}


void SampleApexApplication::dumpApexStats(const physx::apex::NxApexScene* sceneForStats)
{
	if (m_dumpPerfData)
	{
		const physx::NxApexSceneStats* stats = sceneForStats->getStats();
		if (stats && stats->ApexStatsInfoPtr)
		{
			if (m_outFile == NULL)
			{
				physx::fopen_s(&m_outFile, FILE_EXPORT_FILENAME, "w");
				if (m_outFile != NULL)
				{
					fprintf(m_outFile, "Frame");
					for (physx::PxU32 i = 0; i < stats->numApexStats; i++)
					{
						fprintf(m_outFile, "; %s", stats->ApexStatsInfoPtr[i].StatName);
					}
					fprintf(m_outFile, "\n");
				}
			}
			if (m_outFile != NULL)
			{
				static physx::PxU32 frameNumber = 0;
				fprintf(m_outFile, "%u", frameNumber);
				frameNumber++;

				for (physx::PxU32 i = 0; i < stats->numApexStats; i++)
				{
					if (m_outFile != NULL)
					{
						switch (stats->ApexStatsInfoPtr[i].StatType)
						{
						case physx::ApexStatDataType::INT:
							fprintf(m_outFile, "; %d", stats->ApexStatsInfoPtr[i].StatCurrentValue.Int);
							break;
						case physx::ApexStatDataType::FLOAT:
							fprintf(m_outFile, "; %4.3f", stats->ApexStatsInfoPtr[i].StatCurrentValue.Float);
						break;
						default:
							break;
						}
					}
				}
				fprintf(m_outFile, "\n");
				fflush(m_outFile);
			}
		}
	}
}

void SampleApexApplication::closeApexStatsFile(void)
{
	if (m_dumpPerfData)
	{
		if (m_outFile != NULL)
		{
			fclose(m_outFile);
			m_outFile = NULL;
		}
	}
}

void SampleApexApplication::registerInputEvents()
{
	//saveUserInputs();
	std::vector<const SampleFramework::InputEvent*> inputEvents;
	collectInputEvents(inputEvents);
	//saveInputEvents(inputEvents);
}

void SampleApexApplication::unregisterInputEvents()
{
	getPlatform()->getSampleUserInput()->shutdown();
}

void SampleApexApplication::collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
{
	using namespace SampleFramework;

	std::vector<const char*> inputDescriptions;
	SampleApexApplication::collectInputDescriptions(inputDescriptions);

	//digital keyboard events
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_FORWARD,		SCAN_CODE_FORWARD,		XKEY_W,			PS3KEY_W,		AKEY_UNKNOWN,	SCAN_CODE_FORWARD,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	SCAN_CODE_FORWARD);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_BACKWARD,		SCAN_CODE_BACKWARD,		XKEY_S,			PS3KEY_S,		AKEY_UNKNOWN,	SCAN_CODE_BACKWARD,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	SCAN_CODE_BACKWARD);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_UP,			SCAN_CODE_UP,			XKEY_E,			PS3KEY_E,		AKEY_UNKNOWN,	SCAN_CODE_UP,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	SCAN_CODE_UP);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_DOWN,			SCAN_CODE_DOWN,			XKEY_C,			PS3KEY_C,		AKEY_UNKNOWN,	SCAN_CODE_DOWN,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	SCAN_CODE_DOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_LEFT,			SCAN_CODE_LEFT,			XKEY_A,			PS3KEY_A,		AKEY_UNKNOWN,	SCAN_CODE_LEFT,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	SCAN_CODE_LEFT);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_RIGHT,		SCAN_CODE_RIGHT,		XKEY_D,			PS3KEY_D,		AKEY_UNKNOWN,	SCAN_CODE_RIGHT,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	SCAN_CODE_RIGHT);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_SHIFT_SPEED,		SCAN_CODE_LEFT_SHIFT,	XKEY_SHIFT,		PS3KEY_SHIFT,	AKEY_UNKNOWN,	OSXKEY_SHIFT,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SHIFT);
	DIGITAL_INPUT_EVENT_DEF_2(MODIFIER_SHIFT,			SCAN_CODE_LEFT_SHIFT,	XKEY_SHIFT,		PS3KEY_SHIFT,	AKEY_UNKNOWN,	OSXKEY_SHIFT,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SHIFT);
	DIGITAL_INPUT_EVENT_DEF_2(SAMPLE_ESCAPE,			WKEY_ESCAPE,			XKEY_ESCAPE,	PS3KEY_ESCAPE,	AKEY_UNKNOWN,	OSXKEY_ESCAPE,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_ESCAPE);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_MENU,				WKEY_RETURN,			XKEY_RETURN,	PS3KEY_RETURN,	AKEY_RETURN,	OSXKEY_RETURN,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_RETURN);
	DIGITAL_INPUT_EVENT_DEF_2(NEXT_SCENE,				WKEY_DOWN,				XKEY_DOWN,		PS3KEY_DOWN,	AKEY_DOWN,		OSXKEY_DOWN,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_DOWN);
	DIGITAL_INPUT_EVENT_DEF_2(PREVIOUS_SCENE,			WKEY_UP,				XKEY_UP,		PS3KEY_UP,		AKEY_UP,		OSXKEY_UP,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_UP);
	DIGITAL_INPUT_EVENT_DEF_2(MODIFIER_SHIFT,			WKEY_SHIFT,				XKEY_SHIFT,		PS3KEY_SHIFT,	AKEY_UNKNOWN,	OSXKEY_SHIFT,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SHIFT);
	DIGITAL_INPUT_EVENT_DEF_2(MODIFIER_CONTROL,			WKEY_CONTROL,			XKEY_CONTROL,	PS3KEY_SHIFT,	AKEY_UNKNOWN,	OSXKEY_CONTROL,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_CONTROL);
	DIGITAL_INPUT_EVENT_DEF_2(SHOW_HELP,				WKEY_F1,				XKEY_F1,		PS3KEY_F1,		AKEY_H,			OSXKEY_F1,			PSP2KEY_UNKNOWN, 	IKEY_UNKNOWN,	LINUXKEY_F1);
	if (m_allowCreateObject)
	{
		DIGITAL_INPUT_EVENT_DEF_2(CREATE_OBJECT,			WKEY_SPACE,				XKEY_SPACE,		PS3KEY_SPACE,	AKEY_SPACE,		OSXKEY_SPACE,		PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_SPACE);
	}
	DIGITAL_INPUT_EVENT_DEF_2(PAUSE_SAMPLE,				WKEY_P,					XKEY_P,			PS3KEY_P,		AKEY_P,			OSXKEY_P,			PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_P);
	DIGITAL_INPUT_EVENT_DEF_2(STEP_ONE_FRAME,			WKEY_O,					XKEY_O,			PS3KEY_O,		AKEY_O,			OSXKEY_O,			PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_O);
	DIGITAL_INPUT_EVENT_DEF_2(NEXT_VISUALIZATION,		WKEY_V,					XKEY_V,			PS3KEY_V,		AKEY_V,			OSXKEY_V,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_V);
	//DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_WIREFRAME,			WKEY_N,					XKEY_N,			PS3KEY_N,		AKEY_UNKNOWN,	OSXKEY_N,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_N		);
#ifdef USE_MEM_TRACKER
	DIGITAL_INPUT_EVENT_DEF_2(DUMP_ALLOCATED_MEMORY,		WKEY_M,					XKEY_M,			PS3KEY_M,		AKEY_M,			OSXKEY_M,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_M);
#endif
	//DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_PVD_CONNECTION,	WKEY_F5,				XKEY_F5,		PS3KEY_F5,		AKEY_UNKNOWN,	OSXKEY_F5,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_F5		);
	DIGITAL_INPUT_EVENT_DEF_2(SHOW_SCENE_STATS,			WKEY_F,					XKEY_F,			PS3KEY_F,		AKEY_F,			OSXKEY_F,			PSP2KEY_UNKNOWN,	IQUICK_BUTTON_1, LINUXKEY_F);
	DIGITAL_INPUT_EVENT_DEF_2(NEXT_PAGE,				WKEY_NEXT,				XKEY_NEXT,		PS3KEY_NEXT,	AKEY_UNKNOWN,	OSXKEY_NEXT,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_NEXT);
	DIGITAL_INPUT_EVENT_DEF_2(PREVIOUS_PAGE,			WKEY_PRIOR,				XKEY_PRIOR,		PS3KEY_PRIOR,	AKEY_UNKNOWN,	OSXKEY_PRIOR,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_PRIOR);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_COLLISION_RADIUS, WKEY_ADD,				XKEY_ADD,		PS3KEY_ADD,		AKEY_UNKNOWN,	OSXKEY_ADD,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SUBTRACT);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_SIM_BUDGET,		WKEY_ADD,				XKEY_ADD,		PS3KEY_ADD,		AKEY_UNKNOWN,	OSXKEY_ADD,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SUBTRACT);
	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_COLLISION_RADIUS, WKEY_SUBTRACT,			XKEY_SUBTRACT,	PS3KEY_SUBTRACT, AKEY_UNKNOWN,	OSXKEY_SUBTRACT,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SUBTRACT);
	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_SIM_BUDGET,		WKEY_SUBTRACT,			XKEY_SUBTRACT,	PS3KEY_SUBTRACT, AKEY_UNKNOWN,	OSXKEY_SUBTRACT,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_SUBTRACT);

	//digital mouse events
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_BUTTON, 		MOUSE_BUTTON_LEFT_DOWN, XKEY_UNKNOWN,	PS3KEY_UNKNOWN, AKEY_UNKNOWN, 	MOUSE_BUTTON_LEFT_DOWN, PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	MOUSE_BUTTON_LEFT_DOWN);
	DIGITAL_INPUT_EVENT_DEF_2(MOUSE_SELECTION_BUTTON,	MOUSE_BUTTON_RIGHT_DOWN, XKEY_UNKNOWN,	PS3KEY_UNKNOWN,	AKEY_UNKNOWN,	MOUSE_BUTTON_RIGHT_DOWN, PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	MOUSE_BUTTON_RIGHT_DOWN);

	//digital gamepad events
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_SPEED_INCREASE,	GAMEPAD_LEFT_STICK,		GAMEPAD_LEFT_STICK,	GAMEPAD_LEFT_STICK,		AKEY_UNKNOWN,	OSXKEY_UNKNOWN,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_SPEED_DECREASE,	GAMEPAD_RIGHT_STICK,	GAMEPAD_RIGHT_STICK, GAMEPAD_RIGHT_STICK,	AKEY_UNKNOWN,	OSXKEY_UNKNOWN,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_FORWARD,		GAMEPAD_DIGI_UP,		GAMEPAD_DIGI_UP,	GAMEPAD_DIGI_UP,		AKEY_UNKNOWN,	GAMEPAD_DIGI_UP,	GAMEPAD_DIGI_UP,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_BACKWARD,		GAMEPAD_DIGI_DOWN,		GAMEPAD_DIGI_DOWN,	GAMEPAD_DIGI_DOWN,		AKEY_UNKNOWN,	GAMEPAD_DIGI_DOWN,	GAMEPAD_DIGI_DOWN,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_LEFT,			GAMEPAD_DIGI_LEFT,		GAMEPAD_DIGI_LEFT,	GAMEPAD_DIGI_LEFT,		AKEY_UNKNOWN,	GAMEPAD_DIGI_LEFT,	GAMEPAD_DIGI_LEFT,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_RIGHT,		GAMEPAD_DIGI_RIGHT,		GAMEPAD_DIGI_RIGHT,	GAMEPAD_DIGI_RIGHT,		AKEY_UNKNOWN,	GAMEPAD_DIGI_RIGHT,	GAMEPAD_DIGI_RIGHT,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_MENU,				GAMEPAD_START,			GAMEPAD_START,		GAMEPAD_START,			AKEY_UNKNOWN,	GAMEPAD_START,		GAMEPAD_START,		IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(SAMPLE_ESCAPE,			GAMEPAD_SELECT,			GAMEPAD_SELECT,		GAMEPAD_SELECT,			AKEY_UNKNOWN,	GAMEPAD_SELECT,		GAMEPAD_SELECT,		IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(NEXT_SCENE,				GAMEPAD_DIGI_DOWN,		GAMEPAD_DIGI_DOWN,	GAMEPAD_DIGI_DOWN,		AKEY_UNKNOWN,	GAMEPAD_DIGI_DOWN,	GAMEPAD_DIGI_DOWN,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(PREVIOUS_SCENE,			GAMEPAD_DIGI_UP,		GAMEPAD_DIGI_UP,	GAMEPAD_DIGI_UP,		AKEY_UNKNOWN,	GAMEPAD_DIGI_UP,	GAMEPAD_DIGI_UP,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);

	//analog mouse events
	ANALOG_INPUT_EVENT_DEF_2(CAMERA_MOUSE_LOOK, 		MOUSE_MOVE, 			XKEY_UNKNOWN, 			PS3KEY_UNKNOWN, 		AKEY_UNKNOWN, 			MOUSE_MOVE, 			PSP2KEY_UNKNOWN, 		IKEY_UNKNOWN, 			MOUSE_MOVE);

	//analog gamepad events
	ANALOG_INPUT_EVENT_DEF_2(CAMERA_GAMEPAD_ROTATE_LEFT_RIGHT,	GAMEPAD_RIGHT_STICK_X,	GAMEPAD_RIGHT_STICK_X,	GAMEPAD_RIGHT_STICK_X,	GAMEPAD_RIGHT_STICK_X,	GAMEPAD_RIGHT_STICK_X,	GAMEPAD_RIGHT_STICK_X,	GAMEPAD_RIGHT_STICK_X,	LINUXKEY_UNKNOWN);
	ANALOG_INPUT_EVENT_DEF_2(CAMERA_GAMEPAD_ROTATE_UP_DOWN,		GAMEPAD_RIGHT_STICK_Y,	GAMEPAD_RIGHT_STICK_Y,	GAMEPAD_RIGHT_STICK_Y,	GAMEPAD_RIGHT_STICK_Y,	GAMEPAD_RIGHT_STICK_Y,	GAMEPAD_RIGHT_STICK_Y,	GAMEPAD_RIGHT_STICK_Y,	LINUXKEY_UNKNOWN);
	ANALOG_INPUT_EVENT_DEF_2(CAMERA_GAMEPAD_MOVE_LEFT_RIGHT,	GAMEPAD_LEFT_STICK_X,	GAMEPAD_LEFT_STICK_X,	GAMEPAD_LEFT_STICK_X,	GAMEPAD_LEFT_STICK_X,	GAMEPAD_LEFT_STICK_X,	GAMEPAD_LEFT_STICK_X,	GAMEPAD_LEFT_STICK_X,	LINUXKEY_UNKNOWN);
	ANALOG_INPUT_EVENT_DEF_2(CAMERA_GAMEPAD_MOVE_FORWARD_BACK,	GAMEPAD_LEFT_STICK_Y,	GAMEPAD_LEFT_STICK_Y,	GAMEPAD_LEFT_STICK_Y,	GAMEPAD_LEFT_STICK_Y,	GAMEPAD_LEFT_STICK_Y,	GAMEPAD_LEFT_STICK_Y,	GAMEPAD_LEFT_STICK_Y,	LINUXKEY_UNKNOWN);

	//digital gamepad events
	DIGITAL_INPUT_EVENT_DEF_2(NEXT_VISUALIZATION,	GAMEPAD_RIGHT_SHOULDER_TOP, GAMEPAD_RIGHT_SHOULDER_TOP, GAMEPAD_RIGHT_SHOULDER_TOP, AKEY_UNKNOWN, GAMEPAD_RIGHT_SHOULDER_TOP, GAMEPAD_RIGHT_SHOULDER_TOP, IKEY_UNKNOWN, LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_SHIFT_SPEED,	GAMEPAD_LEFT_SHOULDER_TOP, GAMEPAD_LEFT_SHOULDER_TOP, GAMEPAD_LEFT_SHOULDER_TOP, AKEY_UNKNOWN, GAMEPAD_LEFT_SHOULDER_TOP, GAMEPAD_LEFT_SHOULDER_TOP, IKEY_UNKNOWN, LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(GAMEPAD_TRIGGER_LEFT,	GAMEPAD_LEFT_SHOULDER_BOT, GAMEPAD_LEFT_SHOULDER_BOT, GAMEPAD_LEFT_SHOULDER_BOT, AKEY_UNKNOWN, GAMEPAD_LEFT_SHOULDER_BOT, GAMEPAD_LEFT_SHOULDER_BOT, IKEY_UNKNOWN, LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(GAMEPAD_TRIGGER_RIGHT, GAMEPAD_RIGHT_SHOULDER_BOT, GAMEPAD_RIGHT_SHOULDER_BOT, GAMEPAD_RIGHT_SHOULDER_BOT, AKEY_UNKNOWN, GAMEPAD_RIGHT_SHOULDER_BOT, GAMEPAD_RIGHT_SHOULDER_BOT, IKEY_UNKNOWN, LINUXKEY_UNKNOWN);

	// software input (on-screen keyboard)
	TOUCH_INPUT_EVENT_DEF_2(SHOW_SOFT_INPUT, ABUTTON_1, IBUTTON_1);

	if (m_allowCreateObject)
	{
		DIGITAL_INPUT_EVENT_DEF_2(CREATE_OBJECT,		GAMEPAD_SOUTH,	GAMEPAD_SOUTH,	GAMEPAD_SOUTH,	AKEY_UNKNOWN,	GAMEPAD_SOUTH,	GAMEPAD_SOUTH,	IKEY_UNKNOWN, LINUXKEY_UNKNOWN);
	}
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_BACKWARD,	GAMEPAD_WEST,	GAMEPAD_WEST,	GAMEPAD_WEST,	AKEY_UNKNOWN,	GAMEPAD_WEST,	GAMEPAD_WEST,	IKEY_UNKNOWN, LINUXKEY_UNKNOWN);
	DIGITAL_INPUT_EVENT_DEF_2(CAMERA_MOVE_FORWARD,	GAMEPAD_NORTH,	GAMEPAD_NORTH,	GAMEPAD_NORTH,	AKEY_UNKNOWN,	GAMEPAD_NORTH,	GAMEPAD_NORTH,	IKEY_UNKNOWN, LINUXKEY_UNKNOWN);
}

void SampleApexApplication::collectInputDescriptions(std::vector<const char*>& inputDescriptions)
{
	inputDescriptions.insert(inputDescriptions.end(), SampleFrameworkInputEventDescriptions, SampleFrameworkInputEventDescriptions + PX_ARRAY_SIZE(SampleFrameworkInputEventDescriptions));
	inputDescriptions.insert(inputDescriptions.end(), SampleInputEventDescriptions,          SampleInputEventDescriptions          + PX_ARRAY_SIZE(SampleInputEventDescriptions));
}

void SampleApexApplication::onAnalogInputEvent(const SampleFramework::InputEvent& ie, float val)
{
	SampleApplication::onAnalogInputEvent(ie, val);
}

bool SampleApexApplication::onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
{
	//PX_USER_PROFILER_PERF_SCOPE("SampleApexApplication/onDigitalInputEvent");
	SampleApplication::onDigitalInputEvent(ie, val);

	if (val)
	{
		switch (ie.m_Id)
		{
		default:
			break;

		case SHOW_HELP:
			if (m_displayState != DISPLAY_HELP_MESSAGE)
			{
				m_displayState = DISPLAY_HELP_MESSAGE;
				m_fadeOutDisplay = false;
				m_helpPage = 0;
			}
			else
			{
				m_fadeOutDisplay = true;
			}
			break;

		case NEXT_PAGE:
			if (m_displayState == DISPLAY_HELP_MESSAGE)
			{
				m_helpPage++;
			}
			break;
		case PREVIOUS_PAGE:
			if (m_displayState == DISPLAY_HELP_MESSAGE)
			{
				if (m_helpPage)
				{
					m_helpPage--;
				}
			}
			break;

		case SCENE_MENU:
			if (m_displayState != DISPLAY_SCENE_SELECTION)
			{
				m_displayState = DISPLAY_SCENE_SELECTION;
				m_fadeOutDisplay = false;
				m_sceneNumberSelection = getSelectedScene();
			}
			else
			{
				selectScene(m_sceneNumberSelection);
				// select scene
				m_displayState = DISPLAY_NONE;
				m_fadeOutDisplay = true;
			}
			break;

		case NEXT_SCENE:
			if (m_displayState == DISPLAY_SCENE_SELECTION)
			{
				m_sceneNumberSelection = std::min(getNumScenes() - 1, m_sceneNumberSelection + 1);
			}
			break;
		case PREVIOUS_SCENE:
			if (m_displayState == DISPLAY_SCENE_SELECTION && m_sceneNumberSelection > 1)
			{
				m_sceneNumberSelection--;
			}
			break;

		case PAUSE_SAMPLE:
			m_pause = !m_pause;
			break;

		case STEP_ONE_FRAME:
			m_step = true;
			m_pause = false;
			break;

		case NEXT_VISUALIZATION:
			nextDebugRenderConfig(isInputEventActive(MODIFIER_SHIFT) ? -1 : 1);
			break;

		case PREV_VISUALIZATION:
			nextDebugRenderConfig(-1);
			break;

		case CREATE_OBJECT:
		{
			const physx::PxMat44& eyeT = getEyeTransform();
			physx::PxVec3 pos =  eyeT.getPosition();
			physx::PxVec3 vel = -eyeT.column2.getXYZ() * 10.0f;
			physx::PxVec3 extents(m_collisionObjRadius, m_collisionObjRadius, m_collisionObjRadius);

			SampleShapeType type =
			    isInputEventActive(MODIFIER_SHIFT) && isInputEventActive(MODIFIER_CONTROL) ? HalfSpaceShapeType
			    : isInputEventActive(MODIFIER_SHIFT) ? SphereShapeType //TriMeshShapeType
			    : isInputEventActive(MODIFIER_CONTROL) ? ConvexShapeType //CapsuleShapeType
			    : BoxShapeType; //ConvexShapeType;

			addCollisionShape(type, pos, vel, extents);
		}
		break;

		case INCREASE_COLLISION_RADIUS:
		case INCREASE_SIM_BUDGET:
			if (isInputEventActive(MODIFIER_SHIFT))
			{
				m_collisionObjRadius += 0.1f;
				updateCollisionObjectSceneInfo();
			}
			else
			{
				setBudget(m_simulationBudget + 100);
			}
			break;

		case DECREASE_COLLISION_RADIUS:
		case DECREASE_SIM_BUDGET:
			if (isInputEventActive(MODIFIER_SHIFT))
			{
				m_collisionObjRadius -= 0.1f;
				if (m_collisionObjRadius < 0.0)
				{
					m_collisionObjRadius = 0.0f;
				}
				updateCollisionObjectSceneInfo();
			}
			else if (m_simulationBudget >= 100)
			{
				setBudget(m_simulationBudget - 100);
			}
			break;
		case SHOW_SOFT_INPUT:
			SampleFramework::SamplePlatform::platform()->showSoftInput();
			break;
#if SAMPLES_USE_VRD
		case TOGGLE_PVD_CONNECTION:
			togglePvdConnection(m_pvdHostName.c_str(), m_pvdConnectionFlags);
			break;
#endif
		case DUMP_ALLOCATED_MEMORY:
		{
			std::string allocatedMemoryFile = "$" + m_sampleName + "_allocated.html";
			UserPxAllocator::dumpMemoryLeaks(allocatedMemoryFile.c_str());
			break;
		}
		case SHOW_SCENE_STATS:
		{
			setApexStatDisplay(!getApexStatDisplay());
			break;
		}
		case SAMPLE_ESCAPE:
		{
			if (m_displayState == DISPLAY_SCENE_SELECTION && !m_fadeOutDisplay)
			{
				m_fadeOutDisplay = true;
			}
			else
			{
				RendererWindow::close();
			}
			break;
		}

		};
	}

	if (getRenderer() && val && (ie.m_Id == GAMEPAD_TRIGGER_LEFT || ie.m_Id == GAMEPAD_TRIGGER_RIGHT))
	{
		physx::PxU32 width  = 0;
		physx::PxU32 height = 0;
		getRenderer()->getWindowSize(width, height);
		width /= 2;
		height /= 2;
		SampleFramework::InputEvent ieTemp(ie.m_Id == GAMEPAD_TRIGGER_LEFT ? (physx::PxU16)CAMERA_MOVE_BUTTON : (physx::PxU16)MOUSE_SELECTION_BUTTON, "MOUSE", val);
		onPointerInputEvent(ieTemp, width, height, 0, 0);
	}

	return true;
}

void SampleApexApplication::onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy)
{
	//PX_USER_PROFILER_PERF_SCOPE("SampleApexApplication/onPointerInputEvent");
	SampleApplication::onPointerInputEvent(ie, x, y, dx, dy);

	SampleRenderer::Renderer* renderer = getRenderer();

	if (!m_apexScene || !renderer)
	{
		return;
	}

	switch (ie.m_Id)
	{
	case MOUSE_SELECTION_BUTTON:
	{
		physx::PxU32 width  = 0;
		physx::PxU32 height = 0;
		renderer->getWindowSize(width, height);

		physx::PxMat44 eyeTransform = getEyeTransform();
		physx::PxTransform view(eyeTransform.inverseRT());
		physx::PxVec3 eyePos        = eyeTransform.getPosition();
		physx::PxVec3 nearPos       = unproject(m_projection, view, (x / (physx::PxF32)width) * 2 - 1, (y / (physx::PxF32)height) * 2 - 1);
		physx::PxVec3 pickDir       = nearPos - eyePos;

		if (pickDir.normalize() > 0)
		{
			doMouseClickRaycast(eyePos, pickDir);
		}
	}
	}
}

void SampleApexApplication::onTickPreRender(float dtime)
{
	if (!m_fadeOutDisplay)
	{
		m_textAlpha = std::min(1.0f, m_textAlpha + dtime);
	}
	else
	{
		m_textAlpha = std::max(0.0f, m_textAlpha - dtime);
		if (m_textAlpha <= 0.0f && m_displayState != DISPLAY_NONE)
		{
			m_displayState = DISPLAY_NONE;
		}
	}
}

void SampleApexApplication::onTickPostRender(float /*dtime*/)
{
	if (m_apexScene && !m_pause)
	{
		if (m_step)
		{
			m_step = false;
			m_pause = true;
		}
	}
}

float SampleApexApplication::tweakElapsedTime(float dtime)
{
	static const float maxSimulateInterval = 1 / 30.0f;

	// To avoid "Large elapsedTime values can lead to instabilities"
	return (dtime < maxSimulateInterval) ? dtime : maxSimulateInterval;
}

bool SampleApexApplication::cudaSupported() const 
{
#if defined(PX_WINDOWS)
	return true;
#else
	return false;
#endif
}

bool SampleApexApplication::cudaEnabled() const
{
	return NULL != m_cudaContext;
}

bool SampleApexApplication::interopSupported() const
{
#if defined(PX_WINDOWS)
	// DX11 CUDA interop is not supported
	return (m_renDesc.driver != SampleRenderer::Renderer::DRIVER_DIRECT3D11);
#else
	return false;
#endif
}

bool SampleApexApplication::interopEnabled() const
{
#if defined(PX_WINDOWS)
	return cudaEnabled() && (physx::pxtask::CudaInteropMode::NO_INTEROP != m_cudaContext->getInteropMode());
#else
	return false;
#endif
}

void SampleApexApplication::setBenchmarkEnable(bool enabled)
{
	m_BenchmarkMode = enabled;

	if (enabled)
	{
		m_errorCallback->reportErrors(false);
	}
}

void SampleApexApplication::showHelpPopup(const std::string& helpText, physx::PxF32 duration)
{
	m_helpPopupText = helpText;
	m_displayState  = DISPLAY_HELP_POPUP;
	m_textAlpha     = duration;
}

void SampleApexApplication::print(physx::PxU32 x, physx::PxU32 y, 
								  const char* text, 
								  physx::PxReal scale, physx::PxReal shadowOffset, 
								  SampleRenderer::RendererColor textColor, 
								  bool forceFixWidthNumbers) const
{
	// TODO: Eventually, we may want to simply cache the provided text info,
	//       and defer rendering to some final text flush command
	PX_ASSERT(m_renderer);
	if (m_renderer && !empty(text))
		m_renderer->print(x, y, text, scale, shadowOffset, textColor, forceFixWidthNumbers);
}
