// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#include "SampleAnimatedMeshAsset.h"
#include "PsShare.h"
#include <MeshImport.h>
#include <SkeletalAnim.h>
#include <TriangleMesh.h>

#include <NxClothingAsset.h>
#include <foundation/PxErrorCallback.h>

#define REPORT_ERROR(_MESSAGE) physx::apex::NxGetApexSDK()->getErrorCallback()->reportError(physx::PxErrorCode::eINVALID_OPERATION, _MESSAGE, __FILE__, __LINE__);

const char* SampleAnimatedMeshAsset::mPrefixPath;

SampleAnimatedMeshAsset::SampleAnimatedMeshAsset(const std::string& filename) :
	mTriangleMesh(NULL),
	mSkeletalAnim(NULL)
{
	std::string realName(mPrefixPath != NULL ? mPrefixPath : "");
	realName.append(filename);

	loadMeshImport(realName.c_str(), false);
}



SampleAnimatedMeshAsset::~SampleAnimatedMeshAsset()
{
	delete mTriangleMesh;
	mTriangleMesh = NULL;

	delete mSkeletalAnim;
	mSkeletalAnim = NULL;
}



void SampleAnimatedMeshAsset::hideByMaterialName(const char* materialName)
{
	if (mTriangleMesh != NULL)
	{
		for (physx::PxU32 i = 0; i < mTriangleMesh->getNumSubmeshes(); i++)
		{
			if (mTriangleMesh->getSubMesh(i)->materialName == materialName)
			{
				mTriangleMesh->showSubmesh(i, false);
			}
		}
	}
}



bool SampleAnimatedMeshAsset::loadAnimation(const char* animationFile)
{
	std::string realName;
	if (mPrefixPath != NULL)
	{
		realName = mPrefixPath;
	}
	realName.append(animationFile);
	return loadMeshImport(realName.c_str(), true);
}



void SampleAnimatedMeshAsset::loadMaterials(physx::apex::NxResourceCallback* resourceCallback, physx::apex::NxUserRenderResourceManager* rrm,
        const char* materialPrefix, const char* materialSuffix)
{
	if (mTriangleMesh != NULL)
	{
		mTriangleMesh->loadMaterials(resourceCallback, rrm, false, materialPrefix, materialSuffix, true);
	}
}



bool SampleAnimatedMeshAsset::loadMeshImport(std::string realName, bool appendAnimation)
{
	if (mimp::gMeshImport == NULL)
	{
		return false;
	}


#ifdef PX_X360
	const size_t realNameLength = realName.length();
	// switch the '/' with '\'
	for (size_t i = 0; i < realNameLength; i++)
	{
		if (realName[i] == '/')
		{
			realName[i] = '\\';
		}
	}
#endif

	bool success = true;

	FILE* file = NULL;
	file = fopen(realName.c_str(), "rb");
	if (file != NULL)
	{
		fseek(file, 0, SEEK_END);
		physx::PxU32 len = ftell(file);
		if (len > 0)
		{
			fseek(file, 0, SEEK_SET);
			unsigned char* data = new unsigned char[len];
			fread(data, len, 1, file);
			mimp::MeshSystemContainer* msc = mimp::gMeshImport->createMeshSystemContainer(realName.c_str(), data, len, NULL);

			if (msc != NULL)
			{
				if (appendAnimation)
				{
					if (mSkeletalAnim != NULL)
					{
						std::string error;
						if (!mSkeletalAnim->loadFromMeshImport(msc, error, true))
						{
							// didn't work, oh well
							success = false;
							REPORT_ERROR(error.c_str());
						}
					}
				}
				else
				{
					PX_ASSERT(mTriangleMesh == NULL);
					mTriangleMesh = new Samples::TriangleMesh(PX_MAX_U32);
					if (mTriangleMesh->loadFromMeshImport(msc, false))
					{
						PX_ASSERT(mSkeletalAnim == NULL);
						mSkeletalAnim = new Samples::SkeletalAnim;

						std::string error;
						if (!mSkeletalAnim->loadFromMeshImport(msc, error, false))
						{
							REPORT_ERROR(error.c_str());
							delete mSkeletalAnim;
							mSkeletalAnim = NULL;
							delete mTriangleMesh;
							mTriangleMesh = NULL;
						}
					}
					else
					{
						delete mTriangleMesh;
						mTriangleMesh = NULL;
					}
				}

				mimp::gMeshImport->releaseMeshSystemContainer(msc);
				msc = NULL;
			}

			delete [] data;
			data = NULL;
		}
		fclose(file);
		file = NULL;
	}

	return success && mTriangleMesh != NULL && mSkeletalAnim != NULL;
}