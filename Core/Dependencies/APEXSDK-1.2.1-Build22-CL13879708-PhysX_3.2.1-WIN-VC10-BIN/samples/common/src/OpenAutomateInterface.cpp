// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//		   NVIDIA sample Open Automate NxParamInterface for SimpleDestruction sample.
//
// Title: Level of Detail Sample
// Description: This module provides the Open Automate interface for the
//			    SimpleDestruction APEX sample
// ===============================================================================

#include "UserErrorCallback.h"
#include "OpenAutomate.h"
#include "OpenAutomateInterface.h"
#include "NxApexScene.h"
#include "SampleCommandLine.h"
#include "PsShare.h"
#include "pxtask/PxTaskManager.h"
#if defined(PX_WINDOWS)
#include "pxtask/PxGpuDispatcher.h"
#include "pxtask/PxCudaContextManager.h"
#define NOMINMAX
#include <windows.h>
#include "limits.h"
#elif defined(PX_PS3)
#define MAX_PATH 260
#endif
#if defined(PX_X360)
#define MAX_PATH 260
#endif
#if defined(PX_APPLE)
#define MAX_PATH 260
#endif

#define PRINTF_DEBUGGING    0
#if PRINTF_DEBUGGING
#include <stdio.h>
static FILE* logFilePtr = NULL;
#define DBG_FILE_NAME "c:/temp/SimpleDestructionOA_STUFF.txt"
#define DBGPRINT(fmt, ...) (::fopen_s(&logFilePtr,DBG_FILE_NAME,"a+")==0) ? ::fprintf(logFilePtr, fmt, __VA_ARGS__),fclose(logFilePtr) : 0
#pragma warning(disable:4555)
#else
#define DBGPRINT(fmt, ...)
#pragma warning(disable:4555)
#endif

#define OaStatsOptionFileName	"OaStatsOptionFile.txt"
#define MAX_STATS2_BUF_SIZE     (8192)  // the maximum options buffer string
#define MAX_STATS2_OPTION_LEN   (200)   // the maximum lenght for 1 option!

const char * openAutomateInterface::defaultProgramVersionString = "\\v1.2\\";

displayType openAutomateInterface::DisplayResolutions[displayResType::NUM_DISPLAY_RESOLUTIONS] =
{
	{640,	480,	"640x480"},
	{800,	600,	"800x600"},
	{1024,	768,	"1024x768"},
	{1152,	864,	"1152x864"},
	{1280,	720 ,	"1280x720"},
	{1280,	768 ,	"1280x768"},
	{1280,	800 ,	"1280x800"},
	{1280,	960 ,	"1280x960"},
	{1280,	1024,	"1280x1024"},
	{1366,	768 ,	"1366x768"},
	{1440,	900 ,	"1440x900"},
	{1600,	1200,	"1600x1200"},
	{1680,	1050,	"1680x1050"},
	{1920,	1080,	"1920x1080"},
	{1920,	1200,	"1920x1200"}
};

void openAutomateInterface::initMemberVars(void)
{
	mStats2BufPtr           = new char[MAX_STATS2_BUF_SIZE + 1]; // +1 for '\0' just in case...
	mStats2BufPtr[0]        = '\0';
	mOAState				= OAState::IDLE;
	mRunNormalModePtr		= NULL;
	mBenchmarkStartPtr		= NULL;
	mProgramVersionString	= NULL;
	mDispResolution			= displayResType::res_640x480;
	mSimulationBudget		= 1;
	mBenchmarkNumFrames		= 1000;
	mNumActors				= 4;
	mNumCpuThreads			= 0;
	mEnableGRB				= OA_FALSE;
	mCumulativeTime			= 0.0;
	mCumulativeTimeIgnoreCount = 2;
}

#if NX_SDK_VERSION_MAJOR == 2
NxScene*						openAutomateInterface::mOpenAutomateInterfacePhysxScene = NULL;
#endif
physx::apex::NxApexScene*		openAutomateInterface::mOpenAutomateInterfaceApexScene = NULL;
UserPxAllocator*				openAutomateInterface::mAllocator = NULL;
std::map<std::string, bool>		openAutomateInterface::mOaStats2Options;


openAutomateInterface::openAutomateInterface(const char* openAutomateLibPath,
        const char* programPathname,
        void (*runNormalModePtr)(void),
        void (*benchmarkStartPtr)(const char*),
        std::vector<const char*> benchMarkNames,
        const SampleFramework::SampleCommandLine* cmdline,
        const char* programVersionString,
		oa_OptionsOverrideDesc* desc)
{
	initMemberVars();
	mAllocator				= NULL;
	mRunNormalModePtr		= runNormalModePtr;
	mBenchmarkStartPtr		= benchmarkStartPtr;
	mBenchMarkNames			= benchMarkNames;
	mCmdline				= cmdline;

	//OA startup
	oaVersion Version;
	if (!oaInit(openAutomateLibPath, &Version))
	{
		ERRORSTREAM_DEBUG_ERROR("OA did not initialize properly.");
		PX_ASSERT(0);
		exit(1);
	}
	InitOptions(desc);
}

openAutomateInterface::openAutomateInterface(const char* programVersionString, const SampleFramework::SampleCommandLine* cmdline)
{
	mAllocator				= NULL;
	initMemberVars();
	mProgramVersionString	= programVersionString;
	mCmdline				= cmdline;
}

openAutomateInterface::~openAutomateInterface(void)
{
	if (mStats2BufPtr)
	{
		delete mStats2BufPtr;
	}
	while (!openAutomateOptions.empty())
	{
		openAutomateOptions.clear();
	}
	// empty the map if anything is in it
	mOaStats2Options.erase(mOaStats2Options.begin(), mOaStats2Options.end());
}

#if defined(PX_WINDOWS)
static bool GetExePath(TCHAR* exe_path)
{
#if WIN32
	TCHAR delim = '\\';
#else
	TCHAR delim = '/';
#endif

	DWORD dwRet;
	dwRet = GetModuleFileName(NULL, exe_path, MAX_PATH);
	if (dwRet == 0)
	{
		exe_path[0] = '\0';
		return false;
	}

	int i;
	for (i = dwRet; i > 0; i--)
	{
		if (exe_path[i - 1] == delim)
		{
			break;
		}
	}
	exe_path[i] = '\0';

	return true;
}

char* openAutomateInterface::getKeyRoot(char* baseExeName)
{
	char buf[1024] = {0};
	strcpy(baseExeName, "Software\\OpenAutomate\\RegisteredApps\\NVIDIA\\");
	strcat(baseExeName, getBaseExeName(buf));
	return(baseExeName);
}

char* openAutomateInterface::getKeyStr(char* keyStr)
{
	getKeyRoot(keyStr);
	strcat(keyStr, mProgramVersionString);
	return(keyStr);
}
#endif

char* openAutomateInterface::getExeName(char* baseExeName)
{
	char* ptr;
	size_t n;
	bool found;
	strcpy(baseExeName, mCmdline->getProgramName());
	// remove all but the program name
	n = strlen(baseExeName);
	ptr = baseExeName + n - 1;
	found = false;
	while (ptr != baseExeName)
	{
		if ((*ptr == '/') || (*ptr == '\\') || (*ptr == ':'))
		{
			found = true;
			break;
		}
		ptr--;
	}
	if (found)
	{
		ptr++;	// move 1 character past the directory terminator
		memmove(baseExeName, ptr, n - (ptr - baseExeName) + 1); // copy the exe name and the NULL
	}
	return(baseExeName);
}

char* openAutomateInterface::getBaseExeName(char* baseExeName)
{
	char* ptr;
	char* returnVal = baseExeName;

	getExeName(baseExeName);

	ptr = baseExeName;
	if (*ptr == '"')	// skip over leading quote character if present.
	{
		ptr++;
	}
	while (*ptr && (*ptr != '.'))
	{
		*baseExeName = *ptr;
		ptr++;
		baseExeName++;
	}
	// add the null char at the end.
	*baseExeName = '\0';
	return(returnVal);
}

char* openAutomateInterface::getOptionFileName(char* optionFileName)
{
	char buf[1024] = {0};

	strcpy(optionFileName, OA_OPTIONS_FILE_PREFIX);
	strcat(optionFileName, getBaseExeName(buf));
	strcat(optionFileName, "_Options.bin");
	return(optionFileName);
}

bool openAutomateInterface::UnRegisterAppItself(void)
{
	deleteOptionsFile();
#if defined(PX_WINDOWS)
	DWORD dwRes1, dwRes2 = ERROR_SUCCESS;
	char keyStr[1024] = {0};
	char keyRoot[1024] = {0};

	dwRes1 = RegDeleteKey(HKEY_LOCAL_MACHINE, getKeyStr(keyStr));
	if (ERROR_SUCCESS == dwRes1)
	{
		dwRes2 = RegDeleteKey(HKEY_LOCAL_MACHINE, getKeyRoot(keyRoot));
	}
	if ((ERROR_SUCCESS == dwRes1) || (ERROR_SUCCESS == dwRes2))
	{
		return true;
	}
	else
	{
		ERRORSTREAM_DEBUG_ERROR("Deletetion of registry key failed!\r\n");
		return false;
	}
#else
	return false;
#endif
}

bool openAutomateInterface::RegisterAppItself(void)
{
#if defined(PX_WINDOWS)
	TCHAR exePath[MAX_PATH];
	HKEY hKey;
	DWORD dwRes;
	DWORD dwSize = MAX_PATH;
	TCHAR buf[MAX_PATH];
	TCHAR baseExeName[MAX_PATH];
	TCHAR keyRoot[MAX_PATH];
	TCHAR exeFileName[MAX_PATH];
	TCHAR keyStr[MAX_PATH];

	getBaseExeName(baseExeName);
	getKeyRoot(keyRoot);
	getExeName(exeFileName);
	getKeyStr(keyStr);

	if (!GetExePath(exePath))
	{
		ERRORSTREAM_DEBUG_ERROR("Cannot get exe path\r\n");
		return false;
	}

	dwRes = RegCreateKeyExA(HKEY_LOCAL_MACHINE,
	                        keyStr,
	                        0,
	                        NULL,
	                        REG_OPTION_NON_VOLATILE,
	                        KEY_WRITE, NULL,
	                        &hKey,
	                        &dwSize);
	if (ERROR_SUCCESS != dwRes)
	{
		RegCloseKey(hKey);
		return false;
	}

	bool bRet = false;
	do
	{
		dwRes = RegSetValueExA(hKey,
		                       TEXT("INSTALL_ROOT_PATH"),
		                       0,
		                       REG_SZ,
		                       (BYTE*)exePath,
		                       (DWORD)strlen(exePath));
		if (ERROR_SUCCESS != dwRes)
		{
			ERRORSTREAM_DEBUG_ERROR("Write INSTALL_ROOT_PATH Failed\r\n");
			break;
		}
		strcat(exePath, exeFileName);
		dwRes = RegSetValueExA(hKey,
		                       TEXT("ENTRY_EXE"),
		                       0,
		                       REG_SZ,
		                       (BYTE*)exePath,
		                       (DWORD)strlen(exePath));
		if (ERROR_SUCCESS != dwRes)
		{
			ERRORSTREAM_DEBUG_ERROR("Write ENTRY_EXE Failed\r\n");
			break;
		}

		ZeroMemory(buf, sizeof(buf));
		SYSTEMTIME  tm;
		GetLocalTime(&tm);
		sprintf_s(buf,
		          sizeof(buf),
		          "%04d-%02d-%02d %02d:%02d:%02d",
		          tm.wYear,
		          tm.wMonth,
		          tm.wDay,
		          tm.wHour,
		          tm.wMinute,
		          tm.wSecond);
		dwRes = RegSetValueExA(hKey,
		                       TEXT("INSTALL_DATETIME"),
		                       0,
		                       REG_SZ,
		                       (BYTE*)buf,
		                       (DWORD)strlen(buf));
		if (ERROR_SUCCESS != dwRes)
		{
			ERRORSTREAM_DEBUG_ERROR("Write INSTALL_DATETIME Failed\r\n");
			break;
		}

		dwRes = RegSetValueExA(hKey,
		                       TEXT("REGION"),
		                       0,
		                       REG_SZ,
		                       (BYTE*)TEXT("en_US"),
		                       (DWORD)strlen("en_US"));
		if (ERROR_SUCCESS != dwRes)
		{
			ERRORSTREAM_DEBUG_ERROR("Write REGION Failed\r\n");
			break;
		}

		bRet = true;

	}
	while (0);

	RegCloseKey(hKey);
	return bRet;
#else
	return false;
#endif
}

const char* openAutomateInterface::FormatDataType(oaOptionDataType value_type)
{
	switch (value_type)
	{
	case OA_TYPE_STRING:
		return "String";
	case OA_TYPE_INT:
		return "Int";
	case OA_TYPE_FLOAT:
		return "Float";
	case OA_TYPE_ENUM:
		return "Enum";
	case OA_TYPE_BOOL:
		return "Bool";
	default:
		return "Unknown";
	}
}

const char* openAutomateInterface::FormatCompareOp(oaComparisonOpType op_value)
{
	switch (op_value)
	{
	case OA_COMP_OP_EQUAL:
		return "Equal";
	case OA_COMP_OP_NOT_EQUAL:
		return "NotEqual";
	case OA_COMP_OP_GREATER:
		return "Greater";
	case OA_COMP_OP_LESS:
		return "Less";
	case OA_COMP_OP_GREATER_OR_EQUAL:
		return "GreaterOrEqual";
	case OA_COMP_OP_LESS_OR_EQUAL:
		return "LessOrEqual";
	default:
		return "Unknown";
	}
}

void openAutomateInterface::ConvertOAValue(oaOptionDataType value_type,
        const oaValue* value,
        char* result)
{
	switch (value_type)
	{
	case OA_TYPE_STRING:
	case OA_TYPE_ENUM:
		if (value->String != NULL)
		{
			strcpy(result, value->String);
		}
		else
		{
			strcpy(result, "");
		}
		break;
	case OA_TYPE_INT:
		physx::string::sprintf_s(result, MAX_PATH, "%d", value->Int);
		break;
	case OA_TYPE_FLOAT:
		physx::string::sprintf_s(result, MAX_PATH, "%f", value->Float);
		break;
	case OA_TYPE_BOOL:
		physx::string::sprintf_s(result, MAX_PATH, "%1d", value->Bool);
		break;
	default:
		return;
	}
}


void openAutomateInterface::PrintOptions(const oaNamedOption* option)
{
	char buf[MAX_PATH];
	ConvertOAValue(option->DataType, &option->Value, buf);

	//ERRORSTREAM_DEBUG_INFO("name=%s,type=%s,value=%s\r\n", option->Name, FormatDataType(option->DataType), buf);
	if ((option->MaxValue.Int && option->MinValue.Int) ||
	        (option->MaxValue.Float && option->MinValue.Float))
	{
		ConvertOAValue(option->DataType, &option->MinValue, buf);
		//ERRORSTREAM_DEBUG_INFO("minvalue=%s ", buf);
		ConvertOAValue(option->DataType, &option->MaxValue, buf);
		//ERRORSTREAM_DEBUG_INFO("maxvalue=%s\r\n", buf);
	}

	if (option->Dependency.ParentName)
	{
		ConvertOAValue(option->Dependency.ComparisonValType, &option->Dependency.ComparisonVal, buf);
		//ERRORSTREAM_DEBUG_INFO("  parentname=%s,comparisionop=%s,comparisiontype=%s,comparisionvalue=%s\r\n",
		//		option->Dependency.ParentName,
		//		FormatCompareOp(option->Dependency.ComparisonOp),
		//		FormatDataType(option->Dependency.ComparisonValType),
		//		buf);
	}
}

void openAutomateInterface::InitOptions(oa_OptionsOverrideDesc* desc)
{
	oaNamedOption windowResolutionOption;
	const char* argPtr;
	oaInt MaxBudget = INT_MAX;
	oaInt MinFrames = 100;
	oaInt MaxFrames = 10000;
	oaInt MaxActors = 20;
	oaInt MinCpuThreads = 0;
	oaInt MaxCpuThreads = 32;

	MaxActors = (desc != NULL && desc->getMaxActors()) ? (desc->getMaxActors()) : MaxActors;
	MaxBudget = (desc != NULL && desc->getMaxBudget()) ? (desc->getMaxBudget()) : MaxBudget;
	MaxFrames = (desc != NULL && desc->getMaxFrames()) ? (desc->getMaxFrames()) : MaxFrames;

	if ((argPtr = mCmdline->getValue("MaxBudget")))
	{
		MaxBudget = atoi(argPtr);
	}
	if ((argPtr = mCmdline->getValue("MaxFrames")))
	{
		MaxFrames = atoi(argPtr);
	}
	if ((argPtr = mCmdline->getValue("MaxActors")))
	{
		MaxActors = atoi(argPtr);
	}

	oaNamedOption resourceBudgetOption;
	oaInitOption(&resourceBudgetOption);
	resourceBudgetOption.Name = RESOURCE_BUDGET_STR;
	resourceBudgetOption.DataType = OA_TYPE_INT;
	resourceBudgetOption.MinValue.Int = 0;
	resourceBudgetOption.MaxValue.Int = MaxBudget;
	resourceBudgetOption.NumSteps = 0;
	openAutomateOptions.push_back(resourceBudgetOption);

	oaNamedOption bmarkNumFramesOption;
	oaInitOption(&bmarkNumFramesOption);
	bmarkNumFramesOption.Name = BMARK_NUM_FRAMES_STR;
	bmarkNumFramesOption.DataType = OA_TYPE_INT;
	bmarkNumFramesOption.MinValue.Int = MinFrames;
	bmarkNumFramesOption.MaxValue.Int = MaxFrames;
	bmarkNumFramesOption.NumSteps = 0;
	openAutomateOptions.push_back(bmarkNumFramesOption);

	oaNamedOption NumActorsOption;
	oaInitOption(&NumActorsOption);
	NumActorsOption.Name = NUM_ACTORS_STR;
	NumActorsOption.DataType = OA_TYPE_INT;
	NumActorsOption.MinValue.Int = 1;
	NumActorsOption.MaxValue.Int = MaxActors;
	NumActorsOption.NumSteps = 0;
	openAutomateOptions.push_back(NumActorsOption);

	oaNamedOption Stats2Option;
	oaInitOption(&Stats2Option);
	Stats2Option.Name = STATS2_STR;
	Stats2Option.DataType = OA_TYPE_STRING;
	Stats2Option.NumSteps = -1;
	openAutomateOptions.push_back(Stats2Option);

	oaNamedOption NumCpuThreadsOption;
	oaInitOption(&NumCpuThreadsOption);
	NumCpuThreadsOption.Name = NUM_CPU_THREADS_STR;
	NumCpuThreadsOption.DataType = OA_TYPE_INT;
	NumCpuThreadsOption.MinValue.Int = MinCpuThreads;
	NumCpuThreadsOption.MaxValue.Int = MaxCpuThreads;
	NumCpuThreadsOption.NumSteps = 0;
	openAutomateOptions.push_back(NumCpuThreadsOption);

	oaNamedOption GRBOption;
	oaInitOption(&GRBOption);
	GRBOption.Name = GRB_ENABLE_STR;
	GRBOption.DataType = OA_TYPE_BOOL;
	openAutomateOptions.push_back(GRBOption);

	oaInitOption(&windowResolutionOption);
	windowResolutionOption.Name = WINDOW_SIZE_STR;
	windowResolutionOption.DataType = OA_TYPE_ENUM;
	windowResolutionOption.NumSteps = -1;
	for (physx::PxU32 i = 0; i < displayResType::NUM_DISPLAY_RESOLUTIONS; i++)
	{
		// The oaString in the oaValueStruct should be a const char * (or const oaString)
		windowResolutionOption.Value.Enum = const_cast<oaString>(DisplayResolutions[i].resolutionName);
		//ERRORSTREAM_DEBUG_INFO("         *** Value.Enum = %s ***\n", windowResolutionOption.Value.Enum);
		openAutomateOptions.push_back(windowResolutionOption);
	}

	//read persisted OA values:
	readOptionsFile();
}

void openAutomateInterface::UseOptions(void)
{
	//write options to app vars
	//ERRORSTREAM_DEBUG_INFO("running bechmark @%s with: %d budget.\n",
	//				DisplayResolutions[mDispResolution].resolutionName,
	//				mSimulationBudget);

	// Transfer the saved stats2BufPtr data if any to the mOaStats2Options so that the
	// stats2 data can be collected
	if (mStats2BufPtr)
	{
		physx::PxU32 optionIndex = 0;
		physx::PxU32 optionEnd = 0;
		char optionBuf[MAX_STATS2_OPTION_LEN];
		while (mStats2BufPtr[optionIndex] != '\0')
		{
			optionEnd = (physx::PxU32) strcspn(&mStats2BufPtr[optionIndex], ",");
			if (optionEnd != optionIndex)
			{
				strncpy(optionBuf, &mStats2BufPtr[optionIndex], optionIndex + optionEnd);
				optionBuf[optionEnd] = '\0';
				std::string* myString;
				myString = new std::string(optionBuf);
				mOaStats2Options[*myString] = true;
			}
			optionIndex = optionIndex + optionEnd;
			if (mStats2BufPtr[optionIndex] != '\0') // if this is not the end of the string
			{
				optionIndex = optionIndex + 1;      // move the index past the ,
			}
		}
	}
}

void openAutomateInterface::GetAllOptions(void)
{
	for (unsigned i = 0; i < openAutomateOptions.size(); ++i)
	{
		PrintOptions(&openAutomateOptions[i]);
		oaAddOption(&openAutomateOptions[i]);
	}
}

void openAutomateInterface::GetCurrentOptions(void)
{

	openAutomateOptions[0].Value.Int = mSimulationBudget;
	PrintOptions(&openAutomateOptions[0]);
	oaAddOptionValue(openAutomateOptions[0].Name,
	                 openAutomateOptions[0].DataType,
	                 (const oaValue*)&openAutomateOptions[0].Value);

	openAutomateOptions[1].Value.Int = mBenchmarkNumFrames;
	PrintOptions(&openAutomateOptions[1]);
	oaAddOptionValue(openAutomateOptions[1].Name,
	                 openAutomateOptions[1].DataType,
	                 (const oaValue*)&openAutomateOptions[1].Value);

	//numActors
	openAutomateOptions[2].Value.Int = mNumActors;
	PrintOptions(&openAutomateOptions[2]);
	oaAddOptionValue(openAutomateOptions[2].Name,
	                 openAutomateOptions[2].DataType,
	                 (const oaValue*)&openAutomateOptions[2].Value);

	//stats2
	openAutomateOptions[3].Value.String = mStats2BufPtr;
	PrintOptions(&openAutomateOptions[3]);
	oaAddOptionValue(openAutomateOptions[3].Name,
	                 openAutomateOptions[3].DataType,
	                 (const oaValue*)&openAutomateOptions[3].Value);

	//numCpuThreads
	openAutomateOptions[4].Value.Int = mNumCpuThreads;
	PrintOptions(&openAutomateOptions[4]);
	oaAddOptionValue(openAutomateOptions[4].Name,
	                 openAutomateOptions[4].DataType,
	                 (const oaValue*)&openAutomateOptions[4].Value);

	//enableGrb
	openAutomateOptions[5].Value.Bool = mEnableGRB;
	PrintOptions(&openAutomateOptions[5]);
	oaAddOptionValue(openAutomateOptions[5].Name,
	                 openAutomateOptions[5].DataType,
	                 (const oaValue*)&openAutomateOptions[5].Value);

	//add new items here


	//increment this number if you add to the options list
	const unsigned resolutionStartIdx = 6;

	//window - this must be last
	unsigned openAutomateVideoIndex = mDispResolution + resolutionStartIdx;
	PrintOptions(&openAutomateOptions[openAutomateVideoIndex]);
	oaAddOptionValue(openAutomateOptions[openAutomateVideoIndex].Name,
	                 openAutomateOptions[openAutomateVideoIndex].DataType,
	                 (const oaValue*)&openAutomateOptions[openAutomateVideoIndex].Value);
}

void openAutomateInterface::SetOptions(void)
{
	oaNamedOption* o;

	while ((o = oaGetNextOption()) != NULL)
	{
		//ERRORSTREAM_DEBUG_INFO("option = '%s'\n", o);
		if (strcmp(WINDOW_SIZE_STR, o->Name) == 0)
		{
			mDispResolution = processDisplayOption(o->Value.String);
			continue;
		}
		else if (strcmp(RESOURCE_BUDGET_STR, o->Name) == 0)
		{
			mSimulationBudget = o->Value.Int;
			continue;
		}
		else if (strcmp(BMARK_NUM_FRAMES_STR, o->Name) == 0)
		{
			mBenchmarkNumFrames = o->Value.Int;
			continue;
		}
		else if (strcmp(NUM_ACTORS_STR, o->Name) == 0)
		{
			mNumActors = o->Value.Int;
			continue;
		}
		else if (strcmp(NUM_CPU_THREADS_STR, o->Name) == 0)
		{
			mNumCpuThreads = o->Value.Int;
			continue;
		}
		else if (strcmp(GRB_ENABLE_STR, o->Name) == 0)
		{
			mEnableGRB = o->Value.Bool;
			continue;
		}
		else if (strcmp(STATS2_STR, o->Name) == 0)
		{
			if (mStats2BufPtr)
			{
				strncpy(mStats2BufPtr, o->Value.String, MAX_STATS2_BUF_SIZE);
			}
			continue;
		}
		PX_ASSERT(0);   // this should NEVER happen since the options came from this program!
	}
	writeOptionsFile();
}

void openAutomateInterface::deleteOptionsFile(void)
{
	char optionFileName[1024] = {0};

	DBGPRINT("In readOptionsFile\n");
	getOptionFileName(optionFileName);
	unlink(optionFileName);
}

void openAutomateInterface::readOptionsFile(void)
{
	char optionFileName[1024] = {0};

	DBGPRINT("In readOptionsFile\n");
	//OA requires us to persist these:
	//so if the options file exists read it
	FILE* fp = 0;
	getOptionFileName(optionFileName);
	physx::fopen_s(&fp, optionFileName, "rb");
	if (fp)
	{
		physx::PxU32 DisplayResolution;

		fread(&DisplayResolution, sizeof(physx::PxU32), 1, fp);
		mDispResolution = (displayResType::Enum) DisplayResolution;
		fread(&mSimulationBudget, sizeof(physx::PxU32), 1, fp);
		fread(&mBenchmarkNumFrames, sizeof(physx::PxU32), 1, fp);
		fread(&mNumActors, sizeof(physx::PxU32), 1, fp);
		fread(&mNumCpuThreads, sizeof(physx::PxU32), 1, fp);
		fread(&mEnableGRB, sizeof(bool), 1, fp);
		// now read the stats2 string.  This MUST be last since it is a variable size
		if (mStats2BufPtr)
		{
			physx::PxU32 numChars = (physx::PxU32) fread(mStats2BufPtr, sizeof(char), MAX_STATS2_BUF_SIZE, fp);
			mStats2BufPtr[numChars] = '\0';
		}
		fclose(fp);

		const char* argPtr;
		if ((argPtr = mCmdline->getValue("MaxBudget")))
		{
			physx::PxU32 MaxBudget = atoi(argPtr);
			if (mSimulationBudget > MaxBudget)
			{
				mSimulationBudget = MaxBudget;
			}
		}
		if ((argPtr = mCmdline->getValue("MaxFrames")))
		{
			physx::PxU32 MaxFrames = atoi(argPtr);
			if (mBenchmarkNumFrames > MaxFrames)
			{
				mBenchmarkNumFrames = MaxFrames;
			}
		}
		if ((argPtr = mCmdline->getValue("MaxActors")))
		{
			physx::PxU32 MaxActors = atoi(argPtr);
			if (mNumActors > MaxActors)
			{
				mNumActors = MaxActors;
			}
		}
	}
}

void openAutomateInterface::writeOptionsFile(void)
{
	char optionFileName[1024] = {0};
	DBGPRINT("In writeOptionsFile\n");
	//OA requires us to persist these:
	//so write the options to an options file.
	//Options persistence could also be done using registry variables.
	FILE* fp = 0;
	getOptionFileName(optionFileName);
	physx::fopen_s(&fp, optionFileName, "wb");
	if (fp)
	{
		physx::PxU32 DisplayResolution;

		DisplayResolution = (physx::PxU32) mDispResolution;
		fwrite(&DisplayResolution,	sizeof(physx::PxU32), 1, fp);
		fwrite(&mSimulationBudget,	sizeof(physx::PxU32), 1, fp);
		fwrite(&mBenchmarkNumFrames, sizeof(physx::PxU32), 1, fp);
		fwrite(&mNumActors,			sizeof(physx::PxU32), 1, fp);
		fwrite(&mNumCpuThreads,		sizeof(physx::PxU32), 1, fp);
		fwrite(&mEnableGRB,			sizeof(bool), 1, fp);

		// this stats2 element must be last
		physx::PxU32 numChars = (physx::PxU32) strlen(mStats2BufPtr) + 1;
		if (mStats2BufPtr)
		{
			fwrite(mStats2BufPtr, sizeof(char), numChars, fp);
		}
		fclose(fp);
	}
	else
	{
		ERRORSTREAM_DEBUG_ERROR("Error: can't open/create options file %s!\n", optionFileName);
		PX_ASSERT(0);
		exit(1);
	}
}

OAState::Enum openAutomateInterface::getOaState(void)
{
	return(mOAState);
}

void openAutomateInterface::setOaState(OAState::Enum state)
{
	mOAState = state;
}

void openAutomateInterface::OAHandler(void)
{
	oaCommand command;
	oaInitCommand(&command);

	switch (oaGetNextCommand(&command))
	{
		/* Send all in-game options */
	case OA_CMD_GET_ALL_OPTIONS:
		DBGPRINT("starting OA_CMD_GET_ALL_OPTIONS\n");
		GetAllOptions();
		break;

		/* Send the option values currently set */
	case OA_CMD_GET_CURRENT_OPTIONS:
		DBGPRINT("starting OA_CMD_GET_CURRENT_OPTIONS\n");
		GetCurrentOptions();
		break;

		/* Ask OA for in-game options to use */
	case OA_CMD_SET_OPTIONS:
		DBGPRINT("starting OA_CMD_SET_OPTIONS\n");
		SetOptions();
		break;

		/* Send all known benchmarks */
	case OA_CMD_GET_BENCHMARKS:
		DBGPRINT("starting OA_CMD_GET_BENCHMARKS\n");
		for (physx::PxU32 i = 0; i < mBenchMarkNames.size(); i++)
		{
			DBGPRINT("   adding benchmark '%s'\n", mBenchMarkNames[i]);
			oaAddBenchmark(mBenchMarkNames[i]);
		}
		mBenchMarkNames.erase(mBenchMarkNames.begin(), mBenchMarkNames.end());
		break;

		/* Run benchmark */
	case OA_CMD_RUN_BENCHMARK:
		DBGPRINT("starting OA_CMD_RUN_BENCHMARK\n");
		if (mBenchmarkStartPtr != NULL)
		{
			mBenchmarkStartPtr(command.BenchmarkName);
			(void) mOaTimer.getElapsedSeconds();	// start the timer
			mCumulativeTime = 0.0;
			mCumulativeTimeIgnoreCount = 2;
			mOAState = OAState::BENCHMARKING;
			UseOptions();				//write OA options to demo vars
		}
		break;

		/* No more commands, exit program */
	case OA_CMD_EXIT:
		DBGPRINT("starting OA_CMD_EXIT\n");
		setOaState(OAState::EXIT);
		break;

		/* Run as normal */
	case OA_CMD_RUN:
		DBGPRINT("starting OA_CMD_RUN\n");
		if (mRunNormalModePtr != NULL)
		{
			mRunNormalModePtr();
			mOAState = OAState::RUNNING;
		}
		break;
	}
}

void openAutomateInterface::endBenchmark(void)
{
	oaEndBenchmark();
}

displayResType::Enum openAutomateInterface::processDisplayOption(char* str)
{
	if (str == NULL)
	{
		ERRORSTREAM_DEBUG_ERROR(WINDOW_SIZE_STR " argument missing value!  Assumed %s", DisplayResolutions[displayResType::res_640x480].resolutionName);
	}
	for (physx::PxU32 j = 0; j < displayResType::NUM_DISPLAY_RESOLUTIONS; j++)
	{
		if (strcmp(DisplayResolutions[j].resolutionName, str) == 0)
		{
			return((displayResType::Enum) j);
		}
	}
	ERRORSTREAM_DEBUG_ERROR("ERROR: invalid display resolution!\n");
	return(displayResType::res_640x480);
}

displayResType::Enum openAutomateInterface::getDisplayResolution(void)
{
	return(mDispResolution);
}

physx::PxU32 openAutomateInterface::getSimulationBudget(void)
{
	return(mSimulationBudget);
}

physx::PxU32 openAutomateInterface::getBenchmarkNumFrames(void)
{
	return(mBenchmarkNumFrames);
}

physx::PxU32 openAutomateInterface::getBenchmarkNumActors(void)
{
	return(mNumActors);
}

char* openAutomateInterface::getStats2Str(void)
{
	return(mStats2BufPtr);
}

physx::PxU32 openAutomateInterface::getBenchmarkNumCpuThreads(void)
{
	return(mNumCpuThreads);
}

bool openAutomateInterface::getGRBenabled(void)
{
	return(mEnableGRB == OA_TRUE);
}

static oaOptionDataType ApexStatDataType2oaOptionDataType(physx::apex::ApexStatDataType::Enum dt)
{
	return((oaOptionDataType) dt);
}
static void ApexStatValue2oaValue(physx::apex::ApexStatValue val, physx::apex::ApexStatDataType::Enum dt, oaValue* oaVal)
{
	switch (dt)
	{
	case physx::apex::ApexStatDataType::STRING:
		oaVal->String = val.String;
		break;
	case physx::apex::ApexStatDataType::INT:
		oaVal->Int = val.Int;
		break;
	case physx::apex::ApexStatDataType::FLOAT:
		oaVal->Float = val.Float;
		break;
	case physx::apex::ApexStatDataType::ENUM:
		oaVal->Enum = val.Enum;
		break;
	case physx::apex::ApexStatDataType::BOOL:
		oaVal->Bool = (oaBool)val.Bool;
		break;
	case physx::apex::ApexStatDataType::INVALID: // Make compiler happy
		break;
	}
}

void openAutomateInterface::setAllocatorForTracking(UserPxAllocator* allocator)
{
	mAllocator = allocator;
}

void openAutomateInterface::reportOaResultsValues(void)
{
	oaValue v;

	v.Int = mSimulationBudget;
	oaAddResultValue("SimulationBudget", OA_TYPE_INT, &v);

	v.Int = mBenchmarkNumFrames;
	oaAddResultValue("BenchmarkFrames", OA_TYPE_INT, &v);

	v.Int = mNumActors;
	oaAddResultValue("NumberOfActors", OA_TYPE_INT, &v);

	v.Int = mNumCpuThreads;
	oaAddResultValue("NumberOfCpuThreads", OA_TYPE_INT, &v);

	v.Bool = mEnableGRB;
	oaAddResultValue("SimulateRigidBodiesOnGPU", OA_TYPE_BOOL, &v);

	// The oaString in the oaValueStruct should be a const char * (or const oaString)
	v.String = const_cast<oaString>(DisplayResolutions[mDispResolution].resolutionName);
	oaAddResultValue("DisplayResolution", OA_TYPE_STRING, &v);

	v.String = const_cast<oaString>(UserErrorCallback::instance()->getFirstEror());
	oaAddResultValue("First error: ", OA_TYPE_STRING, &v);

	//clear the error counter in case another benchmark is run.
	UserErrorCallback::instance()->clearErrorCounter();
}



void openAutomateInterface::reportOaStatFloat(const char* name, float val)
{
	if (getOaState() == OAState::BENCHMARKING)
	{
		oaOptionDataType type = OA_TYPE_FLOAT;
		oaValue oaStatValue;
		oaStatValue.Float = val;
		oaAddFrameValue(name,
			type,
			&oaStatValue);
	}
}



void openAutomateInterface::reportOaStatsOptions(void)
{
	oaValue oaStatValue;
	const physx::apex::NxApexSceneStats* myApexStats;

	if (getOaState() == OAState::BENCHMARKING)
	{
		// keep track if the benchmark is in warmUp since
		// !! All calls to oaAddFrameValue() for a given frame should be done
		// !! before the oaDisplayFrame() call for that frame.
		if (mCumulativeTimeIgnoreCount > 0)
		{
			mCumulativeTimeIgnoreCount--;
			mOaTimer.getElapsedSeconds();
		}
		else
		{
#ifdef _DEBUG // the memory tracker is only available in the debug build
			if (mAllocator != NULL)
			{
				oaStatValue.Int = (oaInt) mAllocator->getAllocatedMemoryBytes();
				oaAddFrameValue("AllocatedMemoryBytes", OA_TYPE_INT, &oaStatValue);
			}
#endif

#ifdef PX_WINDOWS  // The GPU Heap only exists on Windows
			physx::pxtask::GpuDispatcher* gd = mOpenAutomateInterfaceApexScene->getTaskManager()->getGpuDispatcher();
			if (gd)
			{
				physx::pxtask::CudaContextManager* ctx = gd->getCudaContextManager();
				if (ctx)
				{
					physx::pxtask::CudaMemoryManager* cmm = ctx->getMemoryManager();
					if (cmm)
					{
						struct physx::pxtask::CudaMemoryManagerStats cmmStats;
						physx::pxtask::CudaBufferType t(physx::pxtask::CudaBufferMemorySpace::T_GPU,
						                                physx::pxtask::CudaBufferFlags::F_READ_WRITE);
						cmm->getStats(t, cmmStats);
						//if any GPU heap has ever been allocated then...
						if (cmmStats.maxAllocated != 0)
						{
							//report the value to OA
							oaStatValue.Int = (oaInt)cmmStats.heapSize;
							oaAddFrameValue("APEX_GPU_heapSize", OA_TYPE_INT, &oaStatValue);
							oaStatValue.Int = (oaInt)cmmStats.totalAllocated;
							oaAddFrameValue("APEX_GPU_totalAllocated", OA_TYPE_INT, &oaStatValue);
							//unfortunately at the present time the following values are always 0.
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::APEX].size;
							//oaAddFrameValue("APEX_GPU_Heap", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::FLUID].size;
							//oaAddFrameValue("APEX_FLUID_GPU_Heap", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::DEFORMABLE].size;
							//oaAddFrameValue("APEX_DEFORMABLE_GPU_Heap", OA_TYPE_INT, &oaStatValue);
							//oaStatValue.Int = cmmStats.allocIdStats[physx::pxtask::AllocId::GPU_UTIL].size;
							//oaAddFrameValue("APEX_GPU_UTIL_GPU_Heap", OA_TYPE_INT, &oaStatValue);
						}
					}
				}
			}
#endif

#if NX_SDK_VERSION_MAJOR == 2
			if (mOpenAutomateInterfacePhysxScene != NULL)
			{
				NxSceneStatistic stat;
				const NxSceneStats2* stats2Ptr = mOpenAutomateInterfacePhysxScene->getStats2();
				if (stats2Ptr != NULL)
				{
					for (physx::PxU32 i = 0; i < stats2Ptr->numStats; i++)
					{
						if (mOaStats2Options.find(stats2Ptr->stats[i].name) != mOaStats2Options.end())
						{
							// get the value
							stat = stats2Ptr->stats[i];
							// report the value to OA
							oaStatValue.Int = stat.curValue;
							oaAddFrameValue(stat.name, OA_TYPE_INT, &oaStatValue);
						}
					}
				}
			}
#endif

			if (mOpenAutomateInterfaceApexScene != NULL)
			{
				myApexStats = mOpenAutomateInterfaceApexScene->getStats();
				if ((myApexStats != NULL) && (myApexStats->ApexStatsInfoPtr != NULL))
				{
					for (physx::PxU32 i = 0; i < myApexStats->numApexStats; i++)
					{
						physx::apex::ApexStatDataType::Enum dType = myApexStats->ApexStatsInfoPtr[i].StatType;
						ApexStatValue2oaValue(myApexStats->ApexStatsInfoPtr[i].StatCurrentValue, dType, &oaStatValue);
						oaOptionDataType oadType = ApexStatDataType2oaOptionDataType(dType);

						oaAddFrameValue(myApexStats->ApexStatsInfoPtr[i].StatName,
						                oadType,
						                &oaStatValue);
					}
				}
			}
			mCumulativeTime += mOaTimer.getElapsedSeconds();
			oaDisplayFrame(mCumulativeTime);
		}
	}
}

void openAutomateInterface::readOaStatsOptionsFile(physx::apex::NxApexScene* apexScene)
{
	std::fstream fs;
	char lineBuf[256];

	// save the scene pointers for fetching the APEX scene results latter...
	mOpenAutomateInterfaceApexScene = apexScene;

#if NX_SDK_VERSION_MAJOR == 2
	mOpenAutomateInterfacePhysxScene = apexScene->getPhysXScene();
#endif

	// do not clear the map since options can also be sent via
	// the OA args now
	//mOaStats2Options.erase(mOaStats2Options.begin(), mOaStats2Options.end());

	fs.open(OaStatsOptionFileName, std::fstream::in);
	if (fs != NULL)
	{
		while (fs.getline(lineBuf, sizeof(lineBuf), '\n'))
		{
			if (lineBuf[0] != '#')
			{
				std::string* myString;
				myString = new std::string(lineBuf);
				mOaStats2Options[*myString] = true;
			}
		}
		fs.close();
	}
}

#if NX_SDK_VERSION_MAJOR == 2
bool openAutomateInterface::createOaStatsOptionsFile(NxScene* physxScene)
{
	std::fstream fs;
	bool status = true;
	fs.open(OaStatsOptionFileName, std::fstream::out);
	if (fs != NULL)
	{
		const NxSceneStats2* theStats = physxScene->getStats2();
		if (theStats != NULL)
		{
			NxSceneStatistic stat;
			physx::PxU32 numStats = theStats->numStats;
			for (physx::PxU32 i = 0; i < numStats; ++i)
			{
				stat = theStats->stats[i];
				fs << '#' << stat.name << std::endl;
			}
		}
		fs.close();
	}
	else
	{
		status = false;
	}
	return(status);
}
#endif
