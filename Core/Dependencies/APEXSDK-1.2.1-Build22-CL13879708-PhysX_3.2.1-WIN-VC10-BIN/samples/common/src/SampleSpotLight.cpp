// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#include "PsShare.h"
#include "SampleSpotLight.h"

#include <Renderer.h>
#include <RendererTargetDesc.h>
#include <RendererTexture2DDesc.h>
#include <RendererSpotLightDesc.h>


SampleSpotLight::SampleSpotLight(SampleRenderer::Renderer& renderer, const SampleRenderer::RendererSpotLightDesc& lightdesc) :
	SampleLight(renderer, lightdesc)
{
	// TODO/HACK: for now only support shadows in D3D9.
	if (mLight && renderer.getDriverType() == SampleRenderer::Renderer::DRIVER_DIRECT3D9)
	{
		physx::PxVec3 x;
		physx::PxVec3 y;
		physx::PxVec3 z = -lightdesc.direction;
		if (physx::PxAbs(z.y > 0.5f))
		{
			x = physx::PxVec3(1, 0, 0).cross(z);
		}
		else
		{
			x = physx::PxVec3(0, 1, 0).cross(z);
		}
		x.normalize();
		y = z.cross(x);
		y.normalize();
		mShadowTransform = physx::PxMat44(x, y, z, lightdesc.position);
		mShadowProjection = SampleRenderer::RendererProjection(physx::PxAcos(lightdesc.outerCone) * 2, 1.0f, physx::PxMax(0.1f, lightdesc.outerRadius / 1000.0f), lightdesc.outerRadius);

#if ENABLE_SHADOWS
		SampleRenderer::RendererTexture2DDesc texdesc;
		texdesc.filter       = SampleRenderer::RendererTexture2D::FILTER_LINEAR;
		texdesc.addressingU  = SampleRenderer::RendererTexture2D::ADDRESSING_CLAMP;
		texdesc.addressingV  = SampleRenderer::RendererTexture2D::ADDRESSING_CLAMP;
		texdesc.width        = SHADOW_RESOLUTION;
		texdesc.height       = SHADOW_RESOLUTION;
		texdesc.numLevels    = 1;
		texdesc.renderTarget = true;

		texdesc.format       = SampleRenderer::RendererTexture2D::FORMAT_R32F;
		mShadowTexture       = renderer.createTexture2D(texdesc);
		PX_ASSERT(mShadowTexture);

		texdesc.format       = SampleRenderer::RendererTexture2D::FORMAT_D16;
		mShadowDepthTexture  = renderer.createTexture2D(texdesc);
		PX_ASSERT(mShadowDepthTexture);

		if (mShadowTexture)
		{
			SampleRenderer::RendererTargetDesc targetdesc;
			targetdesc.textures            = &mShadowTexture;
			targetdesc.numTextures         = 1;
			targetdesc.depthStencilSurface = mShadowDepthTexture;
			mShadowTarget = renderer.createTarget(targetdesc);
#if defined(PX_WINDOWS)
			PX_ASSERT(mShadowTarget);
#endif
			mLight->setShadowMap(mShadowDepthTexture);
			mLight->setShadowTransform(physx::PxTransform(mShadowTransform));
			mLight->setShadowProjection(mShadowProjection);
		}
#endif
	}
}


SampleSpotLight::~SampleSpotLight()
{
}
