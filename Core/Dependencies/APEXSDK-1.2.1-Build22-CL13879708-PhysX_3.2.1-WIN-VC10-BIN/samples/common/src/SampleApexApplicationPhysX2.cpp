// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "NxApexDefs.h"
#if NX_SDK_VERSION_MAJOR == 2

#include <NxApex.h>
#include <NxApexSDK.h>
#include <NxParamUtils.h>
#include <PxCpuDispatcher.h>
#if defined(PX_WINDOWS)
#include <PxCudaContextManager.h>
#endif

#include "SampleApexApplication.h"
#include "SampleCommandLine.h"
#include "Renderer.h"

#include "SampleShapeActor.h"
#include "SampleBoxActor.h"
#include "SampleSphereActor.h"
#include "SampleCapsuleActor.h"
#include "SamplePlaneActor.h"
#include "SampleConvexMeshActor.h"
#include "SampleTriMeshActor.h"

#define NOMINMAX
#include <NxPhysicsSDK.h>
#include <NxCooking.h>
#include <NxSceneDesc.h>
#include <NxMaterial.h>
#include <PhysXLoader.h>

#if NX_SDK_VERSION_NUMBER >= 285 && NX_SDK_VERSION_NUMBER < 300
#include "NxUserProfilerCallback.h"
#endif


using namespace physx;
using namespace physx::apex;

bool SampleApexApplication::initSDKs(bool useApplicationProfiler)
{
	NxPhysicsSDKDesc sdkDesc;
	if (m_enableGpuPhysX)
	{
		sdkDesc.flags &= ~NX_SDKF_NO_HARDWARE;  // enable GPU acceleration for PhysX 2.8.4, redundant for 2.8.3 and earlier
		sdkDesc.gpuHeapSize = 256;
	}
	NxSDKCreateError sdkError = NXCE_NO_ERROR;

	NxUserOutputStream* errorCallback = m_errorCallback ? m_errorCallback->get28PhysXOutputStream() : NULL;
	m_physxSDK = NxCreatePhysicsSDK(NX_PHYSICS_SDK_VERSION, m_allocator->get28PhysXAllocator(), errorCallback, sdkDesc, &sdkError);
	PX_ASSERT(m_physxSDK);
	if (!m_physxSDK)
	{
#if defined(PX_WINDOWS)
		char buf[128];
		sprintf_s(buf, 128, "PhysXCore.dll was not found (ErrorCode %d), aborting", sdkError);
		MessageBoxA(NULL, buf, "Loading Error", MB_OK);
#endif
		close();
		return false;
	}

	m_physxSDK->setParameter(NX_VISUALIZATION_SCALE,        0);

#if ENABLE_SDK_VISUALIZATIONS
	m_physxSDK->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_POSITION,   1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_PACKETS,    1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_BOUNDS,    1);
	m_physxSDK->setParameter(NX_VISUALIZE_FORCE_FIELDS,    1);
#endif

	m_physxCooking = NxGetCookingLib(NX_PHYSICS_SDK_VERSION);
	PX_ASSERT(m_physxCooking);
	if (m_physxCooking)
	{
		m_physxCooking->NxInitCooking();
	}

	NxApexSDKDesc apexDesc;
	apexDesc.physXSDK              = m_physxSDK;
	apexDesc.cooking               = m_physxCooking;
	apexDesc.outputStream		   = m_errorCallback;
	apexDesc.renderResourceManager = m_renderResourceManager;
	apexDesc.resourceCallback      = m_resourceCallback;
	apexDesc.allocator			   = m_allocator;
	apexDesc.wireframeMaterial	   = "materials/simple_unlit.xml";
	apexDesc.solidShadedMaterial   = "materials/simple_lit_color.xml";
	NxApexCreateError errorCode;
	m_apexSDK = NxCreateApexSDK(apexDesc, &errorCode);
	PX_ASSERT(m_apexSDK);
	if (!m_apexSDK && m_errorCallback)
	{
		char buf[256];
		sprintf(buf, "Failed to create Apex SDK: errorCode = %d, cooking = %p\n", errorCode, m_physxCooking);
		m_errorCallback->reportError(physx::PxErrorCode::eDEBUG_WARNING, buf, __FILE__, __LINE__);
	}
	return m_apexSDK != NULL;
}

void SampleApexApplication::createCudaContext(bool useInterop, size_t gpuTotalMemLimit)
{
	PX_FORCE_PARAMETER_REFERENCE(useInterop);

#if defined(PX_WINDOWS)
	
	if (hasInputCommand(SCLIDS::NO_APEX_CUDA))
	{
		return;
	}

	physx::pxtask::CudaContextManagerDesc ctxMgrDesc;

	if (useInterop && !hasInputCommand(SCLIDS::NO_INTEROP))
	{
		SampleRenderer::Renderer* renderer = getRenderer();
		SampleRenderer::Renderer::DriverType driverType = renderer->getDriverType();
		ctxMgrDesc.graphicsDevice = renderer->getDevice();

		if (driverType == SampleRenderer::Renderer::DRIVER_DIRECT3D9)
		{
			ctxMgrDesc.interopMode = physx::pxtask::CudaInteropMode::D3D9_INTEROP;
		}
		else if (driverType == SampleRenderer::Renderer::DRIVER_DIRECT3D11)
		{
			ctxMgrDesc.interopMode = physx::pxtask::CudaInteropMode::D3D11_INTEROP;
		}
		else if (driverType == SampleRenderer::Renderer::DRIVER_OPENGL)
		{
			ctxMgrDesc.interopMode = physx::pxtask::CudaInteropMode::OGL_INTEROP;
		}
	}

	// this call simply returns NULL on platforms and configurations that don't support CUDA
	m_cudaContext = m_apexSDK->createCudaContextManager(ctxMgrDesc);
	if (m_cudaContext)
	{
		bool isValidContext = m_cudaContext->contextIsValid();
		if (isValidContext)
		{
			size_t gpuTotalMemSize = m_cudaContext->getDeviceTotalMemBytes();
			if (gpuTotalMemSize < gpuTotalMemLimit)
			{
				isValidContext = false;
				//not enough gpu memory
				char buf[256];
				sprintf( buf, "Non enough GPU memory: total = %d Mb, required = %d Mb\n", gpuTotalMemSize / (1024*1024), gpuTotalMemLimit / (1024*1024) );
				m_errorCallback->reportError(physx::PxErrorCode::eOUT_OF_MEMORY, buf, __FILE__, __LINE__);
			}
		}

		if (!isValidContext)
		{
			m_cudaContext->release();
			m_cudaContext = 0;
		}
	}
#endif
}

void SampleApexApplication::shutdownSDKs()
{
	if (m_physxCooking)
	{
		m_physxCooking->NxCloseCooking();
		m_physxCooking = 0;
	}

	if (m_physxSDK)
	{
		NxReleasePhysicsSDK(m_physxSDK);
		m_physxSDK = 0;
	}
}

void SampleApexApplication::shutdownScene(physx::apex::NxApexScene& apexScene)
{
	NxScene* scene = apexScene.getPhysXScene();
	if (scene)
	{
		m_physxSDK->releaseScene(*scene);
		apexScene.setPhysXScene(NULL);
	}
}

physx::apex::NxApexScene* SampleApexApplication::createSampleScene(const NxSceneDesc& desc)
{
	NxScene* scene = m_physxSDK->createScene(desc);
	PX_ASSERT(scene);
	if (!scene)
	{
		return NULL;
	}

	scene->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_AND);
	scene->setFilterBool(true);
	scene->setTiming(1.0f / 60.0f, 1);

	// setup default material...
	NxMaterial* material = scene->getMaterialFromIndex(0);
	PX_ASSERT(material);
	if (material)
	{
		material->setDynamicFriction(0.5f);
		material->setStaticFriction(0.5f);
		material->setRestitution(0.1f);
	}

	NxApexSceneDesc apexSceneDesc;
	apexSceneDesc.scene = scene;
	apexSceneDesc.debugVisualizeLocally = true;
	apexSceneDesc.debugVisualizeRemotely = true;
	apexSceneDesc.cpuDispatcher = m_cpuDispatcher;
#if defined(PX_WINDOWS)
	if (m_cudaContext && m_cudaContext->contextIsValid())
	{
		apexSceneDesc.gpuDispatcher = m_cudaContext->getGpuDispatcher();
	}
#endif

	NxApexScene* apexScene = m_apexSDK->createScene(apexSceneDesc);
	PX_ASSERT(apexScene);
	if (!apexScene)
	{
		m_physxSDK->releaseScene(*scene);
		return NULL;
	}

	apexScene->setLODResourceBudget(m_simulationBudget);

	if (m_OaInterface)
	{
		// read the options file if it exists
		m_OaInterface->readOaStatsOptionsFile(apexScene);
	}

	return apexScene;
}

physx::apex::NxApexScene* SampleApexApplication::createSampleScene(const physx::PxVec3& gravity)
{
	NxSceneDesc sceneDesc;
	sceneDesc.backgroundThreadCount = 1;
	sceneDesc.gravity = NXFROMPXVEC3(gravity);
	return createSampleScene(sceneDesc);
}

void SampleApexApplication::applyDebugRenderConfig(size_t index, physx::apex::NxApexScene* apexScene, float scale)
{
	int sumPhysx = 0;
	NxPhysicsSDK* nxSdk = physx::apex::NxGetApexSDK()->getPhysXSDK();
	int sumApex = 0;

	// turn off all
	for (size_t c = 0; c < m_debugRenderConfigs.size(); c++)
	{
		const DebugRenderConfiguration& config = m_debugRenderConfigs[c];
		for (size_t f = 0; f < config.flags.size(); f++)
		{
			DebugRenderFlag flag = config.flags[f];

			if (flag.nxParameter < NX_PARAMS_NUM_VALUES)
			{
				nxSdk->setParameter(flag.nxParameter, 0.0f);
			}

			if (flag.apexParameter != NULL)
			{
				applyApexDebugRenderFlag(apexScene, flag.apexParameter, 0.0f);
			}
		}
	}

	// enable active one
	if ((index < m_debugRenderConfigs.size()) && (m_OaInterface == NULL))
	{
		const DebugRenderConfiguration& config = m_debugRenderConfigs[index];
		for (size_t f = 0; f < config.flags.size(); f++)
		{
			DebugRenderFlag flag = config.flags[f];

			float value = flag.defaultValue != 0.0f ? flag.defaultValue : scale;

			if (flag.nxParameter < NX_PARAMS_NUM_VALUES)
			{
				sumPhysx += nxSdk->setParameter(flag.nxParameter, value) ? 1 : 0;
			}

			if (flag.apexParameter != NULL)
			{
				sumApex += applyApexDebugRenderFlag(apexScene, flag.apexParameter, value);
			}
		}
	}

	NxParameterized::Interface* debugRenderInterface = apexScene->getDebugRenderParams();

	NxParameterized::setParamF32(*debugRenderInterface, "VISUALIZATION_SCALE", sumApex > 0 ? 1.0f : 0.0f);
	NxParameterized::setParamBool(*debugRenderInterface, "VISUALIZATION_ENABLE", sumApex > 0);

	nxSdk->setParameter(NX_VISUALIZATION_SCALE, sumPhysx > 0 ? 1.0f : 0.0f);
}

SampleShapeActor* SampleApexApplication::addCollisionShape(SampleShapeType type, const physx::PxVec3& pos, const physx::PxVec3& vel, const physx::PxVec3& extents)
{
	SampleRenderer::Renderer* r = getRenderer();
	NxScene* scene = m_apexScene->getPhysXScene();

	SampleShapeActor* shapeActor = NULL;
	switch (type)
	{
	case HalfSpaceShapeType:
	{
		physx::PxVec3 n = vel;
		n.normalize();

		shapeActor = new SamplePlaneActor(r, *m_simpleLitMaterial, *scene, pos, n, NULL, true);
		break;
	}
	case SphereShapeType:
	{
		shapeActor = new SampleSphereActor(r, *m_simpleLitMaterial, *scene, pos, vel, extents, 100, NULL, true);
		break;
	}
	case CapsuleShapeType:
	{
		shapeActor = new SampleCapsuleActor(r, *m_simpleLitMaterial, *scene, pos, vel, extents.x, extents.y, 100, NULL, true);
		break;
	}
	case BoxShapeType:
	{
		shapeActor = new SampleBoxActor(r, *m_simpleLitMaterial, *scene, pos, vel, extents, 100, NULL, true);
		break;
	}
	case ConvexShapeType:
	{
		const int sides = 10;

		PxU32 nbVerts = 2*sides;
		PxVec3* verts = new PxVec3[sides*2];

		for(int i = 0; i < sides; i++)
		{
			verts[i].x = 0.5f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			verts[i].z = 0.5f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			verts[i].y = 1.0f*extents.y;
			verts[i+sides].x = 2.0f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			verts[i+sides].z = 2.0f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			verts[i+sides].y = -1.0f*extents.y;
		}

		shapeActor = new SampleConvexMeshActor(r, *m_simpleLitMaterial, *scene, verts, nbVerts, pos, vel, 100, NULL, true);
		
		delete [] verts;
		break;
	}
	case TriMeshShapeType:
	{
		PxF32	density = 100.0;
		#define AHR_TERRAIN 1

		#if AHR_TERRAIN==1
	
		struct _ahrTerrain
		{
			~_ahrTerrain()
			{
				delete [] indices;
				delete [] verts;
			}
			_ahrTerrain(PxVec3 _extents)
			: w(20)
			, h(20)
			, nbVerts(w*h)
			, nbIndices(6*w*h)
			, extents(_extents)
			, indices(NULL)
			, verts(NULL)
			{
				indices = new PxU32[nbIndices];
				verts = new PxVec3[h*w];

				// produce a triangle mesh from a 2d array of points
				for(int j = 0; j < h; j++)
				for(int i = 0; i < w; i++)
				{
					verts[j*w+i].x = (PxF32)(i-w/2);
					verts[j*w+i].z = (PxF32)(j-h/2);
					verts[j*w+i].y = 1.0f*cosf( physx::PxPi*(float)(i-w/2)/6.0f )+1.0f*cosf( physx::PxPi*(float)(j-h/2)/6.0f );
					verts[j*w+i]   = verts[j*w+i].multiply(extents);
				}
				//generate indices;
				for(int j = 0; j < h-1; j++)
				for(int i = 0; i < w-1; i++)
				{
					indices[6*(j*w+i)+0] = j*w+i;
					indices[6*(j*w+i)+1] = (j+1)*w+i;
					indices[6*(j*w+i)+2] = (j+1)*w+i+1;
					indices[6*(j*w+i)+3] = j*w+i;
					indices[6*(j*w+i)+4] = (j+1)*w+i+1;
					indices[6*(j*w+i)+5] = j*w+i+1;
				}

			}
			const int w,h;
			const int nbVerts;
			const int nbIndices;
			const PxVec3 extents;
			PxU32* indices;
			PxVec3* verts;
		};

		_ahrTerrain ahrTerrain(extents);
		PxVec3* verts = ahrTerrain.verts;
		PxU32* indices = ahrTerrain.indices;
		const int nbVerts = ahrTerrain.nbVerts;
		const int nbIndices = ahrTerrain.nbIndices;

		#elif AHR_TERRAIN==2
		const int sides = 20;
		PxVec3* verts = new PxVec3[6*sides];
		PxU32* indices = new PxU32[6*sides];
		for(int i = 0; i < sides; i++)
		{
			verts[3*i+0].x = 0.0f;
			verts[3*i+0].z = 0.0f;
			verts[3*i+0].y = -0.4f*extents.y;
			verts[3*i+2].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			verts[3*i+2].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			verts[3*i+2].y = 0.0f*extents.y;
			verts[3*i+1].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i+1)/sides );
			verts[3*i+1].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i+1)/sides );
			verts[3*i+1].y = 0.0f*extents.y;
			//verts[6*i+3].x = 0.0f;
			//verts[6*i+3].z = 0.0f;
			//verts[6*i+3].y = -2.0f*extents.y;
			//verts[6*i+4].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			//verts[6*i+4].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			//verts[6*i+4].y = 0.0f*extents.y;
			//verts[6*i+5].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i+1)/sides );
			//verts[6*i+5].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i+1)/sides );
			//verts[6*i+5].y = 0.0f*extents.y;
		}
		for(int i = 0; i < 6*sides; i++)
		{
			indices[i] = i;
		}

		PxU32 nbVerts = 6*sides;
		PxU32 nbIndices = nbVerts;
		#elif AHR_TERRAIN==3
		PxVec3* verts = new PxVec3[3];
		PxU32* indices = new PxU32[3];

		verts[0] = extents.multiply(PxVec3(0.0f,0.0f,0.0f));
		verts[1] = extents.multiply(PxVec3(1.0f,0.0f,0.0f));
		verts[2] = extents.multiply(PxVec3(0.0f,0.0f,1.0f));
		indices[0] = 0;
		indices[1] = 2;
		indices[2] = 1;

		PxU32 nbVerts = 3;
		PxU32 nbIndices = 3; 
		#endif
		
		shapeActor = new SampleTriMeshActor(getRenderer(), *m_simpleLitMaterial, *scene, verts, nbVerts, indices, nbIndices, pos, vel, density, NULL, true); //, indices, nbIndices);
		
		#if AHR_TERRAIN!=1
		delete [] verts;
		delete [] indices;
		#endif
		break;
	}
	default:
		PX_ALWAYS_ASSERT();
		break;
	}

	if (shapeActor)
	{
		shapeActor->setGroupsMask(m_shapeGroupMask);

		m_shape_actors.push_back(shapeActor);
		collisionObjectAdded(*m_apexScene, *shapeActor);
	}
	return shapeActor;
}

void SampleApexApplication::updateCollisionShapes(float /*dtime*/)
{
}

void SampleApexApplication::doMouseClickRaycast(const physx::PxVec3& eyePos, const physx::PxVec3& pickDir)
{
	NxVec3 neyePos, npickDir;
	NxFromPxVec3(neyePos, eyePos);
	NxFromPxVec3(npickDir, pickDir);

	NxRaycastHit hit;
	NxRay worldRay(neyePos, npickDir);
	NxShape* hitShape = m_apexScene->getPhysXScene()->raycastClosestShape(worldRay, NX_ALL_SHAPES, hit);
	if (hitShape)
	{
		onMouseClickShapeHit(pickDir, hit, hitShape);
	}
}

void SampleApexApplication::createThreadPool(unsigned int numThreads)
{
	m_cpuDispatcher = m_apexSDK->createCpuDispatcher(numThreads);
}

#endif
