// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "SampleRandomWind.h"

#include "foundation/PxMath.h"
#include "foundation/PxQuat.h"


SampleRandomWind::SampleRandomWind(float adaption, const physx::PxVec3& velocity, float adaptionChange, float velocityChange)
	: mAdaptionStart(adaption)
	, mVelocityStart(velocity)
	, mAdaptionCurrent(adaption)
	, mVelocityCurrent(velocity)
	, mAdaptionChange(adaptionChange)
	, mAdaptionFrequency(2.0f)
	, mVelocityChange(velocityChange / 180.0f * physx::PxPi)
	, mVelocityFrequence(1.3f)
	, mAccumulatedTime(0.0f)
{

}



void SampleRandomWind::tick(float deltaT)
{
	const float goldenRatio = 1.6180339887f;
	static int iterations = 5;

	float adaptionChange = 0.0f;
	float velocityCurrentAngle = 0.0f;
	float frequencyMultiplier = 1.0f;
	float changeMultiplier = 0.5f;
	for (int i = 0; i < iterations; i++)
	{
		adaptionChange += physx::PxSin(mAccumulatedTime * mAdaptionFrequency * frequencyMultiplier) * mAdaptionChange * changeMultiplier;
		velocityCurrentAngle += physx::PxSin(mAccumulatedTime * mVelocityFrequence * frequencyMultiplier) * mVelocityChange * changeMultiplier;

		frequencyMultiplier *= goldenRatio;
		changeMultiplier *= 0.5f;
	}

	physx::PxQuat rot(velocityCurrentAngle, physx::PxVec3(0.0f, 1.0f, 0.0f));

	mAdaptionCurrent = mAdaptionStart;
	mVelocityCurrent = rot.rotate(mVelocityStart) * (1.0f + adaptionChange);
	
	mAccumulatedTime += deltaT;
}
