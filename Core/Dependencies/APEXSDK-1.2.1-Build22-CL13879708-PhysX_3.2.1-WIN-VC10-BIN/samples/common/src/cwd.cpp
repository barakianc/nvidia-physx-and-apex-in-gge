// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
#include "PsShare.h"

#if defined(PX_WINDOWS) || defined(PX_APPLE)

#if defined(PX_APPLE)
#include <Carbon/Carbon.h>
#endif

#if defined(PX_WINDOWS)
#include <PsWindowsInclude.h>
#include <direct.h>
#endif

void EnableCrashingOnCrashes()
{
#ifdef PX_WINDOWS
	// PH: awesomeness from http://www.altdevblogaday.com/2012/07/06/when-even-crashing-doesnt-work/
	// In short, if a crash occurs but on the stack are some kernel mode functions, the crash is just skipped on win7.
	// This prevents this behavior.
	typedef BOOL (WINAPI *tGetPolicy)(LPDWORD lpFlags);
	typedef BOOL (WINAPI *tSetPolicy)(DWORD dwFlags);
	const DWORD EXCEPTION_SWALLOWING = 0x1;

	HMODULE kernel32 = LoadLibraryA("kernel32.dll");
	tGetPolicy pGetPolicy = (tGetPolicy)GetProcAddress(kernel32,
		"GetProcessUserModeExceptionPolicy");
	tSetPolicy pSetPolicy = (tSetPolicy)GetProcAddress(kernel32,
		"SetProcessUserModeExceptionPolicy");
	if (pGetPolicy && pSetPolicy)
	{
		DWORD dwFlags;
		if (pGetPolicy(&dwFlags))
		{
			// Turn off the filter
			pSetPolicy(dwFlags & ~EXCEPTION_SWALLOWING);
		}
	}
#endif
}


// Removes the last directory or file from a path spec.
static void popPathSpec(char* path)
{
	char* ls = 0;
	while (*path)
	{
		if (*path == '\\' || *path == '/')
		{
			ls = path;
		}
		path++;
	}
	if (ls)
	{
		*ls = 0;
	}
}

// Sets the Current Working directory to the executable.
void setCWDToEXE(void)
{
	char exepath[1024] = {0};

#if defined(PX_WINDOWS)

	GetModuleFileNameA(0, exepath, sizeof(exepath));

#elif defined(PX_APPLE)

	ProcessSerialNumber proc;
	GetCurrentProcess(&proc);
	FSRef location;
	GetProcessBundleLocation(&proc, &location);
	FSRefMakePath(&location, (UInt8*)exepath, 1024);

#endif

	if (exepath[0])
	{
		popPathSpec(exepath);
#if defined(PX_APPLE)
		chdir(exepath);
#else
		_chdir(exepath);
#endif
	}

	// Since this method is called in all our executables, this just makes them a whole lot safer.
	EnableCrashingOnCrashes();
}

#endif
