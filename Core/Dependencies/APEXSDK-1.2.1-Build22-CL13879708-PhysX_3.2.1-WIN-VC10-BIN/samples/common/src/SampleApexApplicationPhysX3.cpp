// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "NxApexDefs.h"
#if NX_SDK_VERSION_MAJOR == 3

#include <NxApex.h>
#include <NxApexSDK.h>
#include <NxParamUtils.h>

#include "pxtask/PxCpuDispatcher.h"

#if defined(PX_WINDOWS)
#include "pxtask/PxCudaContextManager.h"
#endif

#include "SampleApexApplication.h"
#include "SampleCommandLine.h"
#include "Renderer.h"
#include "SampleShapeActor.h"
#include "SampleBoxActor.h"
#include "SampleSphereActor.h"
#include "SampleCapsuleActor.h"
#include "SamplePlaneActor.h"
#include "SampleConvexMeshActor.h"
#include "SampleTriMeshActor.h"

#include <PxPhysics.h>
#include "cooking/PxCooking.h"
#include <PxScene.h>
#include "extensions/PxExtensionsAPI.h"
#include "extensions/PxDefaultCpuDispatcher.h"
#include "extensions/PxDefaultSimulationFilterShader.h"
#include <PxFiltering.h>
#include <PxMaterial.h>
#include "physxprofilesdk/PxProfileZoneManager.h"

#define GRB_DBG_USE_PHYSX 1

#if !GRB_DBG_USE_PHYSX
#include <GrbSceneDesc3.h>
#include <GrbPhysics3.h>
#endif

#if defined(PX_PS3)
#include "extensions/ps3/PxPS3Extension.h"
#include "extensions/ps3/PxDefaultSpuDispatcher.h"
#endif

using namespace physx;
using namespace physx::apex;

bool SampleApexApplication::initSDKs(bool useApplicationProfiler)
{
	m_Foundation = PxCreateFoundation(PX_PHYSICS_VERSION, *m_allocator, *m_errorCallback);
	if (!m_Foundation)
	{
#if defined(PX_WINDOWS)
		char buf[128];
		sprintf_s(buf, 128, "Unable to create PhysX Foundation");
		MessageBoxA(NULL, buf, "Loading Error", MB_OK);
#endif
		close();
		return false;
	}

	m_ProfileZoneManager = &PxProfileZoneManager::createProfileZoneManager(m_Foundation);
	if (!m_ProfileZoneManager)
	{
#if defined(PX_WINDOWS)
		char buf[128];
		sprintf_s(buf, 128, "Unable to create Profile Zone Manager");
		MessageBoxA(NULL, buf, "Loading Error", MB_OK);
#endif
		close();
		return false;
	}


	PxTolerancesScale scale;

#if (GRB_DBG_USE_PHYSX == 1)

	m_physxSDK = PxCreatePhysics(PX_PHYSICS_VERSION, *m_Foundation, scale, true, m_ProfileZoneManager);

#else

	int cudaDeviceId = pxtask::getSuggestedCudaDeviceOrdinal(m_Foundation->getErrorCallback());
	m_physxSDK = GrbCreatePhysics(PX_PHYSICS_VERSION, *m_Foundation, scale, true, m_ProfileZoneManager, cudaDeviceId);

#endif

	PX_ASSERT(m_physxSDK);
	if (!m_physxSDK)
	{
#if defined(PX_WINDOWS)
		char buf[128];
		sprintf_s(buf, 128, "Unable to create PhysX SDK");
		MessageBoxA(NULL, buf, "Loading Error", MB_OK);
#endif
		close();
		return false;
	}

	if (!PxInitExtensions(*m_physxSDK))
	{
#if defined(PX_WINDOWS)
		char buf[128];
		sprintf_s(buf, 128, "Unable to initialize PhysX Extensions");
		MessageBoxA(NULL, buf, "Loading Error", MB_OK);
#endif
		close();
		return false;
	}

	m_material = m_physxSDK->createMaterial(0.5f, 0.5f, 0.1f);
	PX_ASSERT(m_material);

	PxCookingParams params;
	m_physxCooking = PxCreateCooking(PX_PHYSICS_VERSION, m_physxSDK->getFoundation(), params);
	PX_ASSERT(m_physxCooking);
	if (!m_material || !m_physxCooking)
	{
		return false;
	}

#if defined(PX_PS3)

#if defined(_DEBUG)
	bool printfEnabled = true;
#else
	bool printfEnabled = false;
#endif

	if (!physx::PxGetPS3Extension().initSDK(PX_PS3_DEFAULT_SPU_COUNT, printfEnabled))
	{
		PX_ALWAYS_ASSERT();
		close();
		return false;
	}

	m_spuDispatcher = PxDefaultSpuDispatcherCreate(physx::PxGetPS3Extension().getSimulationTaskSet(), physx::PxGetPS3Extension().getQueryTaskSet());
	if (m_spuDispatcher == NULL)
	{
		PX_ALWAYS_ASSERT();
		close();
		return false;
	}
#endif

	NxApexSDKDesc apexDesc;
	apexDesc.physXSDK              = m_physxSDK;
	apexDesc.cooking               = m_physxCooking;
	apexDesc.outputStream		   = m_errorCallback;
	apexDesc.renderResourceManager = m_renderResourceManager;
	apexDesc.resourceCallback      = m_resourceCallback;
	apexDesc.wireframeMaterial	   = "materials/simple_unlit.xml";
	apexDesc.solidShadedMaterial   = "materials/simple_lit_color.xml";
	NxApexCreateError errorCode;
	m_apexSDK = NxCreateApexSDK(apexDesc, &errorCode);
	PX_ASSERT(m_apexSDK);
	if (!m_apexSDK && m_errorCallback)
	{
		char buf[256];
		sprintf(buf, "Failed to create Apex SDK: errorCode = %d, cooking = %p\n", errorCode, m_physxCooking);
		m_errorCallback->reportError(physx::PxErrorCode::eDEBUG_WARNING, buf, __FILE__, __LINE__);
	}

	return m_apexSDK != NULL;
}

void SampleApexApplication::shutdownSDKs()
{
	m_material->release();

#if defined(PX_PS3)
	if (m_spuDispatcher != NULL)
	{
		m_spuDispatcher->release();
		m_spuDispatcher = NULL;
	}
	physx::PxGetPS3Extension().terminateSDK();
#endif

	if (m_cpuDispatcher != NULL)
	{
		static_cast<PxDefaultCpuDispatcher*>(m_cpuDispatcher)->release();
		m_cpuDispatcher = NULL;
	}
	if (m_physxCooking)
	{
		m_physxCooking->release();
		m_physxCooking = 0;
	}

	PxCloseExtensions();

	if (m_physxSDK)
	{
		m_physxSDK->release();
		m_physxSDK = 0;
	}
	if (m_ProfileZoneManager)
	{
		m_ProfileZoneManager->release();
		m_ProfileZoneManager = 0;
	}
	if (m_Foundation)
	{
		m_Foundation->release();
		m_Foundation = 0;
	}
}

void SampleApexApplication::createCudaContext(bool useInterop, size_t gpuTotalMemLimit)
{
	if (hasInputCommand(SCLIDS::NO_APEX_CUDA))
	{
		return;
	}

#if defined(PX_WINDOWS)
	physx::pxtask::CudaContextManagerDesc ctxMgrDesc;

	if (useInterop && !hasInputCommand(SCLIDS::NO_INTEROP))
	{
		SampleRenderer::Renderer* renderer = getRenderer();
		SampleRenderer::Renderer::DriverType driverType = renderer->getDriverType();
		ctxMgrDesc.graphicsDevice = renderer->getDevice();

		if (driverType == SampleRenderer::Renderer::DRIVER_DIRECT3D9)
		{
			ctxMgrDesc.interopMode = physx::pxtask::CudaInteropMode::D3D9_INTEROP;
		}
		else if (driverType == SampleRenderer::Renderer::DRIVER_DIRECT3D11)
		{
			ctxMgrDesc.interopMode = physx::pxtask::CudaInteropMode::D3D11_INTEROP;
		}
		else if (driverType == SampleRenderer::Renderer::DRIVER_OPENGL)
		{
			ctxMgrDesc.interopMode = physx::pxtask::CudaInteropMode::OGL_INTEROP;
		}
	}

	// this call simply returns NULL on platforms and configurations that don't support CUDA
	m_cudaContext = pxtask::createCudaContextManager(m_physxSDK->getFoundation(), ctxMgrDesc, m_physxSDK->getProfileZoneManager());
	if (m_cudaContext)
	{
		bool isValidContext = m_cudaContext->contextIsValid();
		if (isValidContext)
		{
			size_t gpuTotalMemSize = m_cudaContext->getDeviceTotalMemBytes();
			if (gpuTotalMemSize < gpuTotalMemLimit)
			{
				isValidContext = false;
				//not enough gpu memory
				char buf[256];
				sprintf( buf, "Non enough GPU memory: total = %d Mb, required = %d Mb\n", gpuTotalMemSize / (1024*1024), gpuTotalMemLimit / (1024*1024) );
				m_errorCallback->reportError(physx::PxErrorCode::eOUT_OF_MEMORY, buf, __FILE__, __LINE__);
			}
		}

		if (!isValidContext)
		{
			m_cudaContext->release();
			m_cudaContext = 0;
		}
	}
#endif
}


PxFilterFlags SimulationFilterShader(
    PxFilterObjectAttributes attributes0,
    PxFilterData filterData0,
    PxFilterObjectAttributes attributes1,
    PxFilterData filterData1,
    PxPairFlags& pairFlags,
    const void* constantBlock,
    PxU32 constantBlockSize)
{
	// let triggers through
	if (PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
	{
		pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		return PxFilterFlags();
	}

	// use a group-based mechanism if the first two filter data words are not 0
	// For 284 here's (G0 op0 K0) op2 (G1 op1 K1) == b
	//	and it's G0 SWAP_AND G1 == true in fact, here's the code:
	//		scene->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_SWAP_AND);
	//		scene->setFilterBool(true);

	//		NxGroupsMask zeroMask;
	//		zeroMask.bits0 = zeroMask.bits1 = zeroMask.bits2 = zeroMask.bits3 = 0;
	//		scene->setFilterConstant0(zeroMask);
	//		scene->setFilterConstant1(zeroMask);
	// calc value for NX_FILTEROP_SWAP_AND
	// Note: word3 is reserved for pair flags, so only check 0 and 2 now
	PxU32 f0 = filterData0.word0 & filterData1.word2;
	PxU32 f2 = filterData0.word2 & filterData1.word0;
	if (f0 || f2)
	{
		pairFlags	= PxPairFlags(filterData0.word3 | filterData1.word3 | PxPairFlag::eRESOLVE_CONTACTS);
		//pairFlags	|= PxPairFlag::eSWEPT_CONTACT_GENERATION | PxPairFlag::eSWEPT_INTEGRATION_LINEAR;	// enable CCD

		return PxFilterFlags();
	}

	return PxFilterFlag::eKILL;
}
void SampleApexApplication::setContactReportFlags(physx::PxShape* shape, physx::PxPairFlags flags)
{
	PxFilterData	fd	= shape->getSimulationFilterData();
	fd.word3	= flags;
	shape->setSimulationFilterData(fd);
}
physx::PxPairFlags SampleApexApplication::getContactReportFlags(const physx::PxShape* shape) const
{
	PxFilterData	fd	= shape->getSimulationFilterData();
	return (physx::PxPairFlags)fd.word3;
}
PxSimulationFilterShader	gDefaultFilterShader = SimulationFilterShader;

physx::apex::NxApexScene* SampleApexApplication::createSampleScene(const physx::PxVec3& gravity)
{
	PxSceneDesc sceneDesc(m_physxSDK->getTolerancesScale());
	sceneDesc.gravity = gravity;
	sceneDesc.cpuDispatcher = m_cpuDispatcher;
#if defined(PX_PS3)
	if (m_spuDispatcher != NULL)
	{
		sceneDesc.spuDispatcher = m_spuDispatcher;
	}
#endif
	sceneDesc.filterShader = gDefaultFilterShader;
	sceneDesc.flags			|= PxSceneFlag::eENABLE_SWEPT_INTEGRATION;			// Prevent tunneling
#if !defined(_DEBUG)
	sceneDesc.flags			|= PxSceneFlag::eENABLE_TWO_DIRECTIONAL_FRICTION;	// More accurate friction
	sceneDesc.flags			|= PxSceneFlag::eADAPTIVE_FORCE;					// Accelerate solver convergence
#endif
#if defined(PX_WINDOWS)
	if (m_cudaContext)
	{
		sceneDesc.gpuDispatcher = m_cudaContext->getGpuDispatcher();
	}
#endif

#if (GRB_DBG_USE_PHYSX == 1)

	physx::PxScene* scene = m_physxSDK->createScene(sceneDesc);

#else

	GrbSceneDesc3 grbSceneDesc(m_physxSDK->getTolerancesScale());
	static_cast<PxSceneDesc &>(grbSceneDesc) = sceneDesc;
	grbSceneDesc.meshCellSize = 12.0f;
	grbSceneDesc.gpuMemSceneSize = 128;
	grbSceneDesc.gpuMemTempDataSize = 256;
	grbSceneDesc.nonPenSolverPosIterCount	= 9;
	grbSceneDesc.frictionSolverPosIterCount	= 3;
	grbSceneDesc.frictionSolverVelIterCount	= 3;
	physx::PxScene* scene = static_cast<GrbPhysics3 *>(m_physxSDK)->createScene(grbSceneDesc, 0);

#endif

	PX_ASSERT(scene);
	if (!scene)
	{
		return NULL;
	}

	NxApexSceneDesc apexSceneDesc;
	apexSceneDesc.scene = scene;
	apexSceneDesc.physX3Interface = this;
	NxApexScene* apexScene = m_apexSDK->createScene(apexSceneDesc);
	PX_ASSERT(apexScene);
	if (!apexScene)
	{
		scene->release();
		return NULL;
	}

	apexScene->setLODResourceBudget(m_simulationBudget);

	if (m_OaInterface)
	{
		// read the options file if it exists
		m_OaInterface->readOaStatsOptionsFile(apexScene);
	}

	return apexScene;
}

void SampleApexApplication::shutdownScene(physx::apex::NxApexScene& apexScene)
{
	physx::PxScene* scene = apexScene.getPhysXScene();
	if (scene)
	{
		scene->release();
		apexScene.setPhysXScene(NULL);
	}
}

void SampleApexApplication::applyDebugRenderConfig(size_t index, physx::apex::NxApexScene* apexScene, float scale)
{
	int sumPhysx = 0;
	int sumApex = 0;

	physx::PxScene* physxScene = apexScene->getPhysXScene();

	// turn off all
	for (size_t c = 0; c < m_debugRenderConfigs.size(); c++)
	{
		const DebugRenderConfiguration& config = m_debugRenderConfigs[c];
		for (size_t f = 0; f < config.flags.size(); f++)
		{
			DebugRenderFlag flag = config.flags[f];

			if (flag.pxParameter < PxVisualizationParameter::eNUM_VALUES)
			{
				physxScene->setVisualizationParameter(flag.pxParameter, 0.0f);
			}

			if (flag.apexParameter != NULL)
			{
				applyApexDebugRenderFlag(apexScene, flag.apexParameter, 0.0f);
			}
		}
	}

	// enable active one
	if ((index < m_debugRenderConfigs.size()) && (m_OaInterface == NULL))
	{
		const DebugRenderConfiguration& config = m_debugRenderConfigs[index];
		for (size_t f = 0; f < config.flags.size(); f++)
		{
			DebugRenderFlag flag = config.flags[f];

			float value = flag.defaultValue != 0.0f ? flag.defaultValue : scale;

			if (flag.pxParameter < PxVisualizationParameter::eNUM_VALUES)
			{
				sumPhysx += physxScene->setVisualizationParameter(flag.pxParameter, value) ? 1 : 0;
			}

			if (flag.apexParameter != NULL)
			{
				sumApex += applyApexDebugRenderFlag(apexScene, flag.apexParameter, value);
			}
		}
	}

	NxParameterized::Interface* debugRenderInterface = apexScene->getDebugRenderParams();
	NxParameterized::setParamF32(*debugRenderInterface, "VISUALIZATION_SCALE", sumApex > 0 ? 1.0f : 0.0f);
	NxParameterized::setParamBool(*debugRenderInterface, "VISUALIZATION_ENABLE", sumApex > 0);

	physxScene->setVisualizationParameter(PxVisualizationParameter::eSCALE, sumPhysx > 0 ? 1.0f : 0.0f);
}

SampleShapeActor* SampleApexApplication::addCollisionShape(SampleShapeType type, const physx::PxVec3& pos, const physx::PxVec3& vel, const physx::PxVec3& extents)
{
	SampleRenderer::Renderer* r = getRenderer();
	physx::PxScene* scene = m_apexScene->getPhysXScene();

	SampleShapeActor* shapeActor = NULL;
	switch (type)
	{
	case HalfSpaceShapeType:
	{
		physx::PxVec3 n = vel;
		n.normalize();

		shapeActor = new SamplePlaneActor(r, *m_simpleLitMaterial, *scene, pos, n, m_material, true);
		break;
	}
	case SphereShapeType:
	{
		shapeActor = new SampleSphereActor(r, *m_simpleLitMaterial, *scene, pos, vel, extents, 100, m_material, true);
		break;
	}
	case CapsuleShapeType:
	{
		shapeActor = new SampleCapsuleActor(r, *m_simpleLitMaterial, *scene, pos, vel, extents.x, extents.y, 100, m_material, true);
		break;
	}
	case BoxShapeType:
	{
		shapeActor = new SampleBoxActor(r, *m_simpleLitMaterial, *scene, pos, vel, extents, 100, m_material, true);
		break;
	}
	case ConvexShapeType:
	{
	    PxF32	density = 100.0;
		const int sides = 10;
		PxVec3* verts = new PxVec3[sides*2];
		for(int i = 0; i < sides; i++)
		{
			verts[i].x = 0.5f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			verts[i].z = 0.5f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			verts[i].y = 1.0f*extents.y;
			verts[i+sides].x = 2.0f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			verts[i+sides].z = 2.0f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			verts[i+sides].y = -1.0f*extents.y;
		}

		PxU32 nbVerts = 2*sides;
		shapeActor = new SampleConvexMeshActor(getRenderer(), *m_simpleLitMaterial, *scene, verts, nbVerts, pos, vel, density, m_material, true); //, indices, nbIndices);
		delete [] verts;
		break;
	}
	case TriMeshShapeType:
	{
		PxF32	density = 100.0;
		#define AHR_TERRAIN 1

		#if AHR_TERRAIN==1
	
		struct _ahrTerrain
		{
			~_ahrTerrain()
			{
				delete [] indices;
				delete [] verts;
			}
			_ahrTerrain(PxVec3 _extents)
			: w(20)
			, h(20)
			, nbVerts(w*h)
			, nbIndices(6*w*h)
			, extents(_extents)
			, indices(NULL)
			, verts(NULL)
			{
				indices = new PxU32[nbIndices];
				verts = new PxVec3[h*w];

				// produce a triangle mesh from a 2d array of points
				for(int j = 0; j < h; j++)
				for(int i = 0; i < w; i++)
				{
					verts[j*w+i].x = (PxF32)(i-w/2);
					verts[j*w+i].z = (PxF32)(j-h/2);
					verts[j*w+i].y = 1.0f*cosf( physx::PxPi*(float)(i-w/2)/6.0f )+1.0f*cosf( physx::PxPi*(float)(j-h/2)/6.0f );
					verts[j*w+i]   = verts[j*w+i].multiply(extents);
				}
				//generate indices;
				for(int j = 0; j < h-1; j++)
				for(int i = 0; i < w-1; i++)
				{
					indices[6*(j*w+i)+0] = j*w+i;
					indices[6*(j*w+i)+1] = (j+1)*w+i;
					indices[6*(j*w+i)+2] = (j+1)*w+i+1;
					indices[6*(j*w+i)+3] = j*w+i;
					indices[6*(j*w+i)+4] = (j+1)*w+i+1;
					indices[6*(j*w+i)+5] = j*w+i+1;
				}

			}
			const int w,h;
			const int nbVerts;
			const int nbIndices;
			const PxVec3 extents;
			PxU32* indices;
			PxVec3* verts;
		};

		_ahrTerrain ahrTerrain(extents);
		PxVec3* verts = ahrTerrain.verts;
		PxU32* indices = ahrTerrain.indices;
		const int nbVerts = ahrTerrain.nbVerts;
		const int nbIndices = ahrTerrain.nbIndices;

		#elif AHR_TERRAIN==2
		const int sides = 20;
		PxVec3* verts = new PxVec3[6*sides];
		PxU32* indices = new PxU32[6*sides];
		for(int i = 0; i < sides; i++)
		{
			verts[3*i+0].x = 0.0f;
			verts[3*i+0].z = 0.0f;
			verts[3*i+0].y = -0.4f*extents.y;
			verts[3*i+2].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			verts[3*i+2].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			verts[3*i+2].y = 0.0f*extents.y;
			verts[3*i+1].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i+1)/sides );
			verts[3*i+1].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i+1)/sides );
			verts[3*i+1].y = 0.0f*extents.y;
			//verts[6*i+3].x = 0.0f;
			//verts[6*i+3].z = 0.0f;
			//verts[6*i+3].y = -2.0f*extents.y;
			//verts[6*i+4].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i)/sides );
			//verts[6*i+4].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i)/sides );
			//verts[6*i+4].y = 0.0f*extents.y;
			//verts[6*i+5].x = 1.0f*extents.x*cos( 2*physx::PxPi*((float)i+1)/sides );
			//verts[6*i+5].z = 1.0f*extents.z*sin( 2*physx::PxPi*((float)i+1)/sides );
			//verts[6*i+5].y = 0.0f*extents.y;
		}
		for(int i = 0; i < 6*sides; i++)
		{
			indices[i] = i;
		}

		PxU32 nbVerts = 6*sides;
		PxU32 nbIndices = nbVerts;
		#elif AHR_TERRAIN==3
		PxVec3* verts = new PxVec3[3];
		PxU32* indices = new PxU32[3];

		verts[0] = extents.multiply(PxVec3(0.0f,0.0f,0.0f));
		verts[1] = extents.multiply(PxVec3(1.0f,0.0f,0.0f));
		verts[2] = extents.multiply(PxVec3(0.0f,0.0f,1.0f));
		indices[0] = 0;
		indices[1] = 2;
		indices[2] = 1;

		PxU32 nbVerts = 3;
		PxU32 nbIndices = 3; 
		#endif
		
		shapeActor = new SampleTriMeshActor(getRenderer(), *m_simpleLitMaterial, *scene, verts, nbVerts, indices, nbIndices, pos, vel, density, m_material, true); //, indices, nbIndices);
		
		#if AHR_TERRAIN!=1
		delete [] verts;
		delete [] indices;
		#endif
		break;
	}
	default:
		PX_ALWAYS_ASSERT();
		break;
	}

	if (shapeActor)
	{
		//shapeActor->setGroupsMask(m_shapeGroupMask);

		m_shape_actors.push_back(shapeActor);
		collisionObjectAdded(*m_apexScene, *shapeActor);
	}
	return shapeActor;
}

void SampleApexApplication::updateCollisionShapes(float dtime)
{
}

void SampleApexApplication::doMouseClickRaycast(const physx::PxVec3& eyePos, const physx::PxVec3& pickDir)
{
	PxRaycastHit hit;
	PxSceneQueryFlags outputFlags;
	outputFlags |= PxSceneQueryFlag::eIMPACT;
	outputFlags |= PxSceneQueryFlag::eDISTANCE;
	if (m_apexScene->getPhysXScene()->raycastSingle(eyePos, pickDir, 1e6, outputFlags, hit))
	{
		PxShape* hitShape = hit.shape;
		onMouseClickShapeHit(pickDir, hit, hitShape);
	}
}

void SampleApexApplication::createThreadPool(unsigned int numThreads)
{
	if (numThreads == 0)
	{
#if defined(PX_WINDOWS)
		numThreads = 4;
#elif defined(PX_X360)
		numThreads = 3;
#elif defined(PX_PS3)
		numThreads = 2;
#elif defined(PX_APPLE)
		numThreads = 2;
#elif defined(PX_ANDROID)
		numThreads = 4;
#endif
	}
	m_cpuDispatcher = PxDefaultCpuDispatcherCreate(numThreads);
}

#endif
