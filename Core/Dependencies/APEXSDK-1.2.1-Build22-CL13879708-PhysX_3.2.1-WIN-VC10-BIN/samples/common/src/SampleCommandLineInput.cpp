// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "SampleCommandLineInput.h"

#include <SampleCommandLine.h>

#include <PsString.h>
#include <sstream>
#include <iomanip>

SampleCommandLineInput::SampleCommandLineInput( const SampleFramework::SampleCommandLine& commandLine )
	: mCommandLine( commandLine ) { }

void SampleCommandLineInput::registerCommands( const CommandLineInput* inputs, unsigned count )
{
	for (unsigned i = 0; i < count; ++i) 
		registerCommand( inputs[i] );
}

void SampleCommandLineInput::registerCommand( const CommandLineInput& input )
{
	mValidCommands.push_back( input );
	processCommand( mValidCommands.back() );
}

void SampleCommandLineInput::processCommand( const CommandLineInput& command )
{
	for (unsigned int argNum = 1; argNum < mCommandLine.getNumArgs(); argNum++)
	{
		if (command.mShortName && physx::string::stricmp(command.mShortName, "") && mCommandLine.hasSwitch(command.mShortName, argNum))
		{
			addInputCommand(command, mCommandLine.getValue(command.mShortName, argNum));
			break;
		}
		if (command.mLongName && physx::string::stricmp(command.mLongName, "") && mCommandLine.hasSwitch(command.mLongName, argNum))
		{
			addInputCommand(command, mCommandLine.getValue(command.mLongName, argNum));
			break;
		}
	}
}

void SampleCommandLineInput::addInputCommand( const CommandLineInput& command, const char* value )
{
	ProcessedCommandLineInput processedCommand = {command, value};
	mCommandIDToProcessedCommandIndex[ processedCommand.id() ] = mInputCommands.size();
	mInputCommands.push_back( processedCommand );
}

template<typename InputIterator>
inline void commandsToString(InputIterator begin, InputIterator end, std::stringstream& ss)
{
	using namespace std;
	for ( InputIterator it = begin; it != end; ++it )
	{
		ss << "  " << "--" << setw(15)  << left << it->mShortName << "\t";
		//ss <<         "--" << setw(20)  << left << mValidCommands[i].mLongName  << "\t";
		ss <<                              left << it->mDesc      << endl;
	}
}

std::string SampleCommandLineInput::validCommands( bool bReverseOrder ) const
{
	std::stringstream ss;
	ss << "Usage: " << mCommandLine.getProgramName() << " [OPTION]" << std::endl << std::endl;

	if (bReverseOrder)
		commandsToString(mValidCommands.rbegin(), mValidCommands.rend(), ss);
	else
		commandsToString(mValidCommands.begin(),  mValidCommands.end(),  ss);

	return ss.str();
}

template<typename InputIterator>
inline void processedCommandsToString(InputIterator begin, InputIterator end, std::stringstream& ss)
{
	using namespace std;
	for ( InputIterator it = begin; it != end; ++it )
	{
		ss << "  " << "--" << setw(15)  << left << it->mInput.mShortName << "\t";
		ss <<                              left << it->value()           << std::endl;
	}
}

std::string SampleCommandLineInput::inputCommands( bool bReverseOrder ) const
{
	std::stringstream ss;

	if (bReverseOrder)
		processedCommandsToString(mInputCommands.rbegin(), mInputCommands.rend(), ss);
	else
		processedCommandsToString(mInputCommands.begin(),  mInputCommands.end(),  ss);
	
	return ss.str();
}

bool SampleCommandLineInput::hasInputCommand( unsigned ID ) const
{
	return mCommandIDToProcessedCommandIndex.find( ID ) != mCommandIDToProcessedCommandIndex.end();
}

const char* SampleCommandLineInput::inputCommandValue( unsigned ID ) const
{
	IDToIndexMap::const_iterator it = mCommandIDToProcessedCommandIndex.find( ID );
	return (it != mCommandIDToProcessedCommandIndex.end() && it->second < mInputCommands.size()) 
		? mInputCommands[ it->second ].value() : NULL;
}

