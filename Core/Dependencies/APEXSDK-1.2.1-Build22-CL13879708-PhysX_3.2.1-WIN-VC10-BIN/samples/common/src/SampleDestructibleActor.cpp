// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "NxFromPx.h"
#include <SampleDestructibleActor.h>
#include <NxParameterized.h>
#include <NxParamUtils.h>

#include <NxDestructibleActor.h>
#include <NxDestructibleAsset.h>

#include "SampleApexRenderer.h"
#include "PsString.h"

SampleDestructibleActor::SampleDestructibleActor(SampleRenderer::Renderer& renderer, physx::apex::NxApexScene& apexScene,
        physx::apex::NxDestructibleAsset& asset, NxParameterized::Interface* descParams, bool ownsParams,
        const physx::PxVec3& vel, const physx::PxVec3& angVel)
	: m_renderer(renderer)
	, m_asset(asset)
	, m_actor(NULL)
{
	m_actor = ownsParams ? (m_asset.createDestructibleActor(descParams, apexScene)) :
	                       static_cast<physx::apex::NxDestructibleActor*>(m_asset.createApexActor(*descParams, apexScene));
	PX_ASSERT(m_actor != NULL);
	if (m_actor != NULL)
	{
		m_actor->cacheModuleData();
		if (vel != physx::PxVec3(0.0f))
		{
			m_actor->setLinearVelocity(vel);
		}
		if (angVel != physx::PxVec3(0.0f))
		{
			m_actor->setAngularVelocity(angVel);
		}
	}
};

void SampleDestructibleActor::initWithProjectileParams(NxParameterized::Interface* descParams)
{
#if defined(PX_X360) || defined(PX_PS3)
	NxParameterized::setParamBool(*descParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", false);
#else
	NxParameterized::setParamBool(*descParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", true);
#endif
	NxParameterized::setParamString(*descParams, "crumbleEmitterName", "crumbleEmitter.apb");
	NxParameterized::setParamF32(*descParams, "destructibleParameters.damageCap", 10.0f);
	NxParameterized::setParamF32(*descParams, "destructibleParameters.damageToRadius", 0.0f);
	NxParameterized::setParamBool(*descParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", true);

	NxParameterized::setParamBool(*descParams, "formExtendedStructures", true);

	int dpCount = 0;
	NxParameterized::getParamArraySize(*descParams, "depthParameters", dpCount);
	NxParameterized::setParamI32(*descParams, "destructibleParameters.impactDamageDefaultDepth", dpCount-1);
	for (int i = 0; i < dpCount; i++)
	{
		char tmpStr[128];
		physx::string::sprintf_s(tmpStr, 128, "depthParameters[%d].OVERRIDE_IMPACT_DAMAGE", i);
		NxParameterized::setParamBool(*descParams, tmpStr, false);
	}

#if NX_SDK_VERSION_MAJOR == 2
	NxParameterized::setParamBool(*descParams, "shapeDescTemplate.groupsMask.useGroupsMask", true);
	NxParameterized::setParamU32(*descParams, "shapeDescTemplate.groupsMask.bits0", 2);
	NxParameterized::setParamU32(*descParams, "shapeDescTemplate.groupsMask.bits2", ~0);
	NxParameterized::setParamU32(*descParams, "shapeDescTemplate.collisionGroup", 0);
	NxParameterized::setParamF32(*descParams, "actorDescTemplate.density", 1.0f);
#elif NX_SDK_VERSION_MAJOR == 3
	NxParameterized::setParamU32(*descParams, "p3ShapeDescTemplate.simulationFilterData.word0", 2);
	NxParameterized::setParamU32(*descParams, "p3ShapeDescTemplate.simulationFilterData.word2", ~0);
	NxParameterized::setParamU32(*descParams, "p3ShapeDescTemplate.queryFilterData.word0", 2);
	NxParameterized::setParamU32(*descParams, "p3ShapeDescTemplate.queryFilterData.word2", ~0);
	NxParameterized::setParamF32(*descParams, "p3BodyDescTemplate.density", 1.0f);
#endif
	NxParameterized::setParamVec3(*descParams, "scale", physx::PxVec3(0.0025f));
	NxParameterized::setParamF32(*descParams, "destructibleParameters.forceToDamage", 100.0f);
	NxParameterized::setParamF32(*descParams, "destructibleParameters.damageThreshold", 5.0f);
};

SampleDestructibleActor::~SampleDestructibleActor(void)
{
	if (m_actor)
	{
		m_actor->release();
	}
}

void SampleDestructibleActor::render(bool rewriteBuffers)
{
	if (m_actor)
	{
		SampleApexRenderer apexRenderer;
		m_actor->lockRenderResources();
		m_actor->updateRenderResources(rewriteBuffers);
		m_actor->dispatchRenderResources(apexRenderer);
		m_actor->unlockRenderResources();
	}
}

void SampleDestructibleActor::breakIt()
{
	if (m_actor)
	{
		// only do this if it's not already broken
		physx::PxU8 visList[1024];
		m_actor->getChunkVisibilities(visList, 1024);
		if (visList[0])
		{
			m_actor->applyRadiusDamage(500.0f, 20.0f, m_actor->getChunkPose(0).getPosition(), 1.0f, false);
		}
	}
}

physx::PxVec3 SampleDestructibleActor::getPosition()
{
	if (m_actor)
	{
		// only do this if it's not already broken
		physx::PxU8 visList[1024];
		m_actor->getChunkVisibilities(visList, 1024);
		if (visList[0])
		{
			return m_actor->getChunkPose(0).getPosition();
		}
		else
		{
			return physx::PxVec3(0.0f);
		}
	}
	else
	{
		return physx::PxVec3(0.0f);
	}

}
