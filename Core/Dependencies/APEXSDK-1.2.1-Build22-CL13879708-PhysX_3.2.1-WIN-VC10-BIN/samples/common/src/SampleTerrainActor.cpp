// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include "SampleTerrainActor.h"

#include <SampleAssetManager.h>
#include <SampleMaterialAsset.h>
#include <SimpleTerrainData.h>

#include <NxApexSDK.h>
#include <NxApexScene.h>

#if NX_SDK_VERSION_MAJOR == 2
#include <NxArray.h>
#include <NxActor.h>
#include <NxActorDesc.h>
#include <NxCooking.h>
#include <NxPhysicsSDK.h>
#include <NxScene.h>
#include <NxTriangleMeshDesc.h>
#include <NxTriangleMeshShapeDesc.h>
#include <NxFromPx.h>
#elif NX_SDK_VERSION_MAJOR == 3
#include <PxScene.h>
#include <PxActor.h>
#include <PxShape.h>
#include <NxFromPx.h>
#include "cooking/PxCooking.h"
#include <PxRigidStatic.h>
#include "geometry/PxTriangleMesh.h"
#include "cooking/PxTriangleMeshDesc.h"
#include "geometry/PxTriangleMeshGeometry.h"
#endif

#include <Renderer.h>
#include <RendererTerrainShape.h>

SampleTerrainActor::SampleTerrainActor(SimpleTerrainData* terrainData,
                                       SampleRenderer::Renderer& renderer,
                                       SampleFramework::SampleAssetManager* assetManager,
                                       physx::apex::NxApexScene& apexScene,
                                       physx::apex::NxApexSDK* apexSDK,
                                       physx::PxU16 materialIndex,
                                       physx::PxU16 collisionGroup) :
	m_apexSDK(apexSDK),
	m_apexScene(&apexScene),
	m_renderer(renderer),
	m_renderShape(0),
	m_assetManager(assetManager),
	m_physxCooking(apexSDK->getCookingInterface())
{
	groundMat = static_cast<SampleFramework::SampleMaterialAsset*>(assetManager->getAsset("ground.xml", SampleFramework::SampleAsset::ASSET_MATERIAL));
	m_rendererMaterial = groundMat->getMaterial();
	m_rendererMaterialInstance = groundMat->getMaterialInstance();

	m_terrainData = terrainData;

#if NX_SDK_VERSION_MAJOR == 2
	// Build physical model
	NxTriangleMeshDesc terrainDesc;
	terrainDesc.numVertices					= m_terrainData->nbVerts;
	terrainDesc.numTriangles				= m_terrainData->nbFaces;
	terrainDesc.pointStrideBytes			= sizeof(physx::PxVec3);
	terrainDesc.triangleStrideBytes			= 3 * sizeof(physx::PxU16);
	terrainDesc.points						= m_terrainData->verts;
	terrainDesc.triangles					= m_terrainData->faces;
	terrainDesc.flags						= NX_MF_16_BIT_INDICES;

	terrainDesc.heightFieldVerticalAxis		= NX_Y;
	terrainDesc.heightFieldVerticalExtent	= -1000.0f;

	physx::PxFileBuf* writeStream = m_apexSDK->createMemoryWriteStream();
	writeStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxws(*writeStream);

	bool status = m_physxCooking->NxCookTriangleMesh(terrainDesc, nxws);
	if (!status)
	{
		printf("Unable to cook a triangle mesh.");
	}

	physx::PxU32 bufLen = 0;
	const void* buf = m_apexSDK->getMemoryWriteBuffer(*writeStream, bufLen);
	physx::PxFileBuf* readStream = m_apexSDK->createMemoryReadStream(buf, bufLen);
	readStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxrs(*readStream);
	m_terrainMesh = m_apexSDK->getPhysXSDK()->createTriangleMesh(nxrs);

	m_apexSDK->releaseMemoryWriteStream(*writeStream);
	m_apexSDK->releaseMemoryReadStream(*readStream);

	//
	// Please note about the created Triangle Mesh, user needs to release it when no one uses it to save memory. It can be detected
	// by API "meshData->getReferenceCount() == 0". And, the release API is "gPhysicsSDK->releaseTriangleMesh(*meshData);"
	//

	NxTriangleMeshShapeDesc terrainShapeDesc;
	terrainShapeDesc.meshData				= m_terrainMesh;
	terrainShapeDesc.shapeFlags				= NX_SF_FEATURE_INDICES | NX_SF_VISUALIZATION;
	terrainShapeDesc.materialIndex			= materialIndex;
	terrainShapeDesc.group					= collisionGroup;

	NxActorDesc ActorDesc;
	ActorDesc.shapes.pushBack(&terrainShapeDesc);
	ActorDesc.group = collisionGroup;
	m_actor = m_apexScene->getPhysXScene()->createActor(ActorDesc);
	m_actor->userData = (void*)0;
#elif NX_SDK_VERSION_MAJOR == 3
	// Build physical model
	physx::PxTriangleMeshDesc terrainDesc;
	terrainDesc.points.count				= m_terrainData->nbVerts;
	terrainDesc.triangles.count				= m_terrainData->nbFaces;
	terrainDesc.points.stride				= sizeof(physx::PxVec3);
	terrainDesc.triangles.stride			= 3 * sizeof(physx::PxU16);
	terrainDesc.points.data					= m_terrainData->verts;
	terrainDesc.triangles.data				= m_terrainData->faces;
	terrainDesc.flags						= physx::PxMeshFlag::e16_BIT_INDICES;

	//terrainDesc.heightFieldVerticalAxis		= NX_Y;
	//terrainDesc.heightFieldVerticalExtent	= -1000.0f;

	physx::PxFileBuf* writeStream = m_apexSDK->createMemoryWriteStream();
	writeStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxws(*writeStream);

	bool status = m_physxCooking->cookTriangleMesh(terrainDesc, nxws);
	if (!status)
	{
		printf("Unable to cook a triangle mesh.");
	}

	physx::PxU32 bufLen = 0;
	const void* buf = m_apexSDK->getMemoryWriteBuffer(*writeStream, bufLen);
	physx::PxFileBuf* readStream = m_apexSDK->createMemoryReadStream(buf, bufLen);
	readStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxrs(*readStream);
	m_terrainMesh = m_apexSDK->getPhysXSDK()->createTriangleMesh(nxrs);

	m_apexSDK->releaseMemoryWriteStream(*writeStream);
	m_apexSDK->releaseMemoryReadStream(*readStream);

	//
	// Please note about the created Triangle Mesh, user needs to release it when no one uses it to save memory. It can be detected
	// by API "meshData->getReferenceCount() == 0". And, the release API is "gPhysicsSDK->releaseTriangleMesh(*meshData);"
	//

	physx::PxRigidStatic* rigidStatic = m_apexSDK->getPhysXSDK()->createRigidStatic(physx::PxTransform(physx::PxVec3(0, 0, 0)));
    PX_ASSERT(rigidStatic && "creating trimesh terrain actor failed");

	physx::PxTriangleMeshGeometry geom(m_terrainMesh);
	m_material = m_apexSDK->getPhysXSDK()->createMaterial(0.5f, 0.5f, 0.1f);
	PX_ASSERT(m_material);
    physx::PxShape* shape = rigidStatic->createShape(geom, *m_material);
    shape->setSimulationFilterData(physx::PxFilterData(1, 1, 1, 0));	// word3 is reserved for contact report flags.
    PX_ASSERT(shape && "creating trimesh terrain shape failed");
    m_apexScene->getPhysXScene()->addActor(*rigidStatic);

	m_actor = rigidStatic;
	m_actor->userData = (void*)0;

#endif

	//Build render model
	m_renderShape = new SampleRenderer::RendererTerrainShape(m_renderer,
	        (physx::PxVec3*) m_terrainData->verts, m_terrainData->nbVerts,
	        (physx::PxVec3*) m_terrainData->normals, m_terrainData->nbVerts,
	        (physx::PxU16*) m_terrainData->faces, m_terrainData->nbFaces,
			0.01f);

	//Create a render mesh context
	m_renderMeshContext.mesh     = m_renderShape->getMesh();
	m_renderMeshContext.material = m_rendererMaterial;
	m_renderMeshContext.materialInstance = m_rendererMaterialInstance;
	m_renderMeshContext.cullMode = SampleRenderer::RendererMeshContext::COUNTER_CLOCKWISE;

}

SampleTerrainActor::~SampleTerrainActor(void)
{
	m_assetManager->returnAsset(*groundMat);
	if (m_actor)
	{
#if NX_SDK_VERSION_MAJOR == 2
		m_apexScene->getPhysXScene()->releaseActor(*m_actor);
#elif NX_SDK_VERSION_MAJOR == 3
		m_actor->release();
#endif
		if (m_terrainData)
		{
			delete m_terrainData;
			m_terrainData = 0;
		}
		if (m_renderShape)
		{
			delete m_renderShape;
			m_renderShape = 0;
		}
	}
}

void SampleTerrainActor::render(bool /*rewriteBuffers*/)
{
	if (m_actor && m_renderShape)
	{
		if (m_renderMeshContext.isValid())
		{
			m_renderer.queueMeshForRender(m_renderMeshContext);
		}
	}
}

