// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Turbulence Sample
// Description: This sample program shows how to create APEX TurbulenceFS actors.
//
// ===============================================================================


#ifndef SIMPLE_TURBULENCE_H
#define SIMPLE_TURBULENCE_H

#include <SampleApplication.h>
#include <SampleApexApplication.h>
#include <SampleAssetManager.h>
#include <SampleMaterialAsset.h>
#include <SampleActor.h>
#include <SamplesVRDSettings.h>
#include <SampleCommandLine.h>
#include <SamplePlatform.h>
#include <SampleUserInput.h>
#include <SampleUserInputIds.h>
#include <SampleInputEventIds.h>
#include <SampleInputDefines.h>

#include <Renderer.h>

#include <RendererBoxShape.h>
#include <RendererCapsuleShape.h>
#include <RendererMeshShape.h>
#include <RendererMeshContext.h>
#include <RendererMaterialDesc.h>
#include <RendererMaterialInstance.h>
#include <RendererDirectionalLightDesc.h>
#include <RendererProjection.h>

#include <SampleApexRenderer.h>

#include <NxApex.h>
#include <NxApexSDK.h>

#include <NxModuleEmitter.h>
#include <NxModuleEmitterLegacy.h>
#include <NxModuleIofx.h>
#include <NxModuleIofxLegacy.h>
#include <NxModuleFrameworkLegacy.h>

#if NX_SDK_VERSION_MAJOR == 2
#include <NxModuleWind.h>
#include <NxModuleExplosion.h>
#include <NxModuleFieldBoundary.h>
#include <NxModuleFieldBoundaryLegacy.h>
#include <NxFieldBoundaryAsset.h>
#include <NxWindAsset.h>
#include <NxWindActor.h>
#include <NxExplosionAsset.h>
#include <NxExplosionActor.h>
#endif

#include <NxModuleCommonLegacy.h>

#include <NxParamUtils.h>
#include <NxApexEmitterAsset.h>
#include <NxApexEmitterActor.h>
#include <NxApexEmitterPreview.h>
#include <NxEmitterLodParamDesc.h>
#include <NxIofxAsset.h>

#include <NxModuleBasicFS.h>
#include <NxModuleFieldSampler.h>
#include <NxModuleTurbulenceFS.h>
#include <NxTurbulenceFSAsset.h>
#include <NxTurbulenceFSActor.h>
#include <NxJetFSActor.h>
#include <NxAttractorFSActor.h>

#include <SampleBoxActor.h>
#include <SampleSphereActor.h>
#include <SampleCapsuleActor.h>
#include <SampleConvexMeshActor.h>
#include <SamplePlaneActor.h>

#include <PsString.h>
#include <PsShare.h>
#include <PsUtilities.h>
#include <cmath>

#include "PsTime.h"
#include "carDriving.h"
#include "simpleMovableBody.h"

enum SceneSelection
{
	NopScene = 0,
	TurbulenceCollisionObjectScene = 1,
	TurbulenceJetFSScene = 2,
	TurbulenceAttractorFSScene = 3,
	TurbulenceWindScene = 4,
	TurbulenceHeatScene = 5,
	TurbulenceTrailingSmokeScene = 6,
	AssetPreviewScene = 7,
#if NX_SDK_VERSION_MAJOR == 2
	TurbulenceWindFSScene = 8,
	TurbulenceExplosionFSScene = 9,
#endif
	NumScenes
};

using namespace physx::apex;
using namespace physx;

///////////////////////////////////////////////////////////////////////////

struct SampleTurbulenceCommandLineInputIds
{
	enum Enum
	{
		///////////////////////////////////////////////////////////////////////////
		// Benchmarks
		ASSET_PREVIEW_MARK = SampleCommandLineInputIds::COUNT,
		FIRST = ASSET_PREVIEW_MARK,
		FIRST_BENCHMARK = ASSET_PREVIEW_MARK,

		JET_FS,
		ATTRACTOR_FS,
		BASIC_TURBULENCE,
		COLLISION_OBJECT,
		HEAT,
#if NX_SDK_VERSION_MAJOR == 2
		WIND_FS,
		LAST  = WIND_FS,
		LAST_BENCHMARK  = WIND_FS,
#else
		LAST  = HEAT,
		LAST_BENCHMARK  = HEAT,
#endif
		COUNT = LAST - FIRST + 1,
	};
};

typedef SampleTurbulenceCommandLineInputIds STCLIDS;
const CommandLineInput SampleTurbulenceCommandLineInputs[] =
{
	{ STCLIDS::ASSET_PREVIEW_MARK, "assetPreviewMark",    "", "run the specified benchmark" },
	{ STCLIDS::JET_FS,			   "jetFS",               "", "run the specified benchmark" },
	{ STCLIDS::ATTRACTOR_FS,	   "attractorFS",         "", "run the specified benchmark" },
	{ STCLIDS::BASIC_TURBULENCE,   "basicTurbulence",     "", "run the specified benchmark" },
	{ STCLIDS::COLLISION_OBJECT,   "collisionObject",     "", "run the specified benchmark" },
	{ STCLIDS::HEAT,               "heat",                "", "run the specified benchmark" },
#if NX_SDK_VERSION_MAJOR == 2
	{ STCLIDS::WIND_FS,            "windFS",              "", "run the specified benchmark" },
#endif
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleTurbulenceCommandLineInputs) == STCLIDS::COUNT);

#define ENABLE_VISUALIZE_TURBULENCE_FS_FIELD	1

#define GROUND_PLANE_COL_GROUP					18

#define MAX_ACTORS		(4)

#define FLUID_IOS 1
#define BASIC_IOS 2

//change here!!!
#define SELECTED_IOS BASIC_IOS

#if SELECTED_IOS == FLUID_IOS

#include <NxModuleFluidIos.h>
#include <NxFluidIosAsset.h>

#define IOS_STRING "FluidIos"
#define IOS_MODULE_NAME "Particles"
#define IOS_MODULE_CLASS NxModuleFluidIos
#define IOS_TYPE_NAME NX_FLUID_IOS_AUTHORING_TYPE_NAME

#elif SELECTED_IOS == BASIC_IOS

#include <NxModuleBasicIos.h>
#include <NxModuleBasicIosLegacy.h>
#include <NxBasicIosAsset.h>

#define IOS_STRING "BasicIos"
#define IOS_MODULE_NAME "BasicIOS"
#define IOS_LEGACY_MODULE_NAME "BasicIOS_Legacy"
#define IOS_MODULE_CLASS NxModuleBasicIos
#define IOS_TYPE_NAME NX_BASIC_IOS_AUTHORING_TYPE_NAME

#endif

#define TEST_CONVEX 1

enum SampleTurbulenceInputEventIds
{
	SAMPLE_TURBULENCE_FIRST = NUM_SAMPLE_BASE_INPUT_EVENT_IDS,

	CONTROL_BACK = SAMPLE_TURBULENCE_FIRST,
	CONTROL_FORWARD,
	CONTROL_LEFT,
	CONTROL_RIGHT,
	CONTROL_UP,
	CONTROL_DOWN,

	SCENE_0,
	SCENE_1,
	SCENE_2,
	SCENE_3,
	SCENE_4,
	SCENE_5,
	SCENE_6,
	SCENE_7,
	SCENE_8,
	SCENE_9,

	INCREASE_SIM_VEL,
	DECREASE_SIM_VEL,

	INCREASE_SIM_SCALE,
	DECREASE_SIM_SCALE,

	INCREASE_TEMPERATURE,
	DECREASE_TEMPERATURE,

	INCREASE_SIM_LOD,
	DECREASE_SIM_LOD,

	RESET_SIM,

	TOGGLE_WIND_HEAT_COL,
	TOGGLE_HEAT_VIZ,
	TOGGLE_PARTICLES_VIZ,
	TOGGLE_PLANE_VIZ,
	TOGGLE_GRID_VIZ,
	TOGGLE_LOD_VIZ,
	TOGGLE_FIELD_VIZ,

	NUM_SAMPLE_TURBULENCE_INPUT_EVENT_IDS,
	NUM_EXCLUSIVE_SAMPLE_TURBULENCE_INPUT_EVENT_IDS = NUM_SAMPLE_TURBULENCE_INPUT_EVENT_IDS - SAMPLE_TURBULENCE_FIRST
};

//------------------------------------------------------------------------------------------

class SampleApexEmitterActor : public SampleFramework::SampleActor
{
public:
	SampleApexEmitterActor(NxApexEmitterAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n = 1, SceneSelection scene = TurbulenceHeatScene);
	void setCurrentPosition(const physx::PxVec3& pos);
	void setCurrentPose(const physx::PxMat44& pose);
	virtual ~SampleApexEmitterActor(void);
	void updateEyePosition(physx::PxVec3& /*position*/);
	virtual void render(bool /*rewriteBuffers*/);
	virtual int getType();

	enum { ApexEmitterActor = 5 };

	NxApexEmitterActor* mActor;
};

class SampleJetFieldSamplerActor : public SampleFramework::SampleActor
{
public:
	SampleJetFieldSamplerActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n = 1);
	virtual ~SampleJetFieldSamplerActor(void);
	void updateEyePosition(physx::PxVec3& /*position*/);
	virtual void render(bool /*rewriteBuffers*/);
	void setPose(const PxMat44& pose);
	void setFieldStrength(PxF32 value);
	virtual int getType();

	enum { JetFieldSamplerActor = 6 };

	NxJetFSActor* mActor;
};

class SampleAttractorFieldSamplerActor : public SampleFramework::SampleActor
{
public:
	SampleAttractorFieldSamplerActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n = 1);
	virtual ~SampleAttractorFieldSamplerActor(void);
	void updateEyePosition(physx::PxVec3& /*position*/);
	virtual void render(bool /*rewriteBuffers*/);
	void setPosition(const physx::PxVec3& position);
	void setFieldRadius(PxF32 radius);
	void setConstFieldStrength(PxF32 value);
	void setVariableFieldStrength(PxF32 value);
	virtual int getType();

	enum { AttractorFieldSamplerActor = 7 };

	NxAttractorFSActor* mActor;
};

#if NX_SDK_VERSION_MAJOR == 2
class SampleFieldBoundaryActor : public SampleFramework::SampleActor
{
public:
	SampleFieldBoundaryActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32);
	virtual ~SampleFieldBoundaryActor(void);
	void updateEyePosition(physx::PxVec3& /*position*/);
	virtual void render(bool /*rewriteBuffers*/);
	virtual int getType();

	enum { FieldBoundaryActor = 7 };

	NxApexActor* mActor;
};
#endif


class SampleTurbulenceFSActor : public SampleFramework::SampleActor
{
public:
	SampleTurbulenceFSActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 num = 1, SceneSelection scene = TurbulenceHeatScene);
	virtual ~SampleTurbulenceFSActor(void);
	void updateEyePosition(physx::PxVec3& /*position*/);
	virtual void render(bool /*rewriteBuffers*/);
	virtual int getType();

	enum { TurbulenceFSActor = 8 };

	NxTurbulenceFSActor* mActor;
};

#if NX_SDK_VERSION_MAJOR == 2
class SampleWindActor : public SampleFramework::SampleActor
{
public:
	SampleWindActor(NxWindAsset& asset, NxApexScene& apexScene, const physx::PxMat44& boxWorldPose, const physx::PxMat33& windToWorld, const physx::PxVec3& scale);
	virtual ~SampleWindActor(void);

	NxWindActor* mActor;
	NxWindAsset& mAsset;
};

class SampleExplosionActor : public SampleFramework::SampleActor
{
public:
	SampleExplosionActor(NxExplosionAsset& asset, NxApexScene& apexScene, const physx::PxF32& scale, const physx::PxMat44 pos);
	virtual ~SampleExplosionActor(void);

	NxExplosionActor* mActor;
	NxExplosionAsset& mAsset;
};
#endif

class SimpleTurbulenceApplication : public SampleApexApplication
{
public:
	SimpleTurbulenceApplication(const SampleFramework::SampleCommandLine& cmdline);
	void initializeText();
	void updateCollisionObjectSceneInfo();
	void updateHeatSceneInfo();
	void updateScalableAndLodInfo();
	void processEarlyCmdLineArgs();

	// Determine world impact position to create explosion
#if NX_SDK_VERSION_MAJOR == 2
	virtual void onMouseClickShapeHit(const physx::PxVec3& /*pickDir*/, NxRaycastHit& hit, NxShape*);
#endif

	virtual ~SimpleTurbulenceApplication();

private:
	static void benchmarkStart(const char* benchmarkName);
	void unloadCurrentAssetsAndActors();
	void loadAssetsAndActors(SceneSelection sceneNumber);
	virtual void onInit();
	virtual void onShutdown();
	virtual void onTickPreRender(float dtime);
	virtual void onRender();
	virtual void onTickPostRender(float /*dtime*/);
	virtual void collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents);
	virtual void collectInputDescriptions(std::vector<const char*>& inputDescriptions);
	void onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy);
	virtual bool onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val);
	void initializeApexCollisionNames();
	void initCommonDebugRenderConfigs();
	void setSceneEyePos();

	void createCollisionObjectScene();
	void createJetFSPWScene();
	void createAttractorFSPWScene();
	void createHeatScene();
	void createWindScene();
	void createWindFSScene();
	void createExplosionFSScene();
	void createTrailingSmokeScene();

	void updateTurbulenceCollisionScene(float dtime);
	void updateTurbulenceJetFSPWScene(float dtime);
	void updateTurbulenceAttractorFSPWScene(float dtime);
	void updateTurbulenceHeatScene(float /*dtime*/);
	void updateTurbulenceWindScene(float /*dtime*/);
	void updateTurbulenceWindFSScene(float /*dtime*/);
	void updateTurbulenceExplosionFSScene(float /*dtime*/);
	void updateTurbulenceTrailingSmokeScene(float dtime);

protected:
	virtual unsigned int getNumScenes() const;
	virtual unsigned int getSelectedScene() const;
	virtual const char* getSceneName(unsigned int sceneNumber) const;
	virtual void selectScene(unsigned int sceneNumber);

private:
	static SimpleTurbulenceApplication* m_AppPtr;

	enum {
		WINDOW_SCENE_NAME_INDEX = 0,
		WINDOW_SCENE_INFO_INDEX = 1,
		NUM_WINDOW_STRINGS      = 6,
		WINDOW_STRING_LENGTH    = 512
	};

	char									mWindowString[NUM_WINDOW_STRINGS][WINDOW_STRING_LENGTH];
	bool									mWindPreviewRenderingDone;
	bool									mRenderParticles;
	SceneSelection							mSceneNumber;
	float									mCapsuleHeight;
	float									mVelocityClamp;
	PxF32									mTemperature;
	PxU32									mRotationType;

	IOS_MODULE_CLASS*						mApexModuleIOS;
	NxModuleEmitter*						mApexEmitterModule;
	PxU32									mNbEmitterModuleScalables;
	NxApexParameter** 						mEmitterModuleScalables;
	NxModuleIofx*							mApexIofxModule;

	NxModuleTurbulenceFS*					mTurbulenceFSModule;
	PxU32									mNbTurbulenceModuleScalables;
	NxApexParameter** 						mTurbulenceModuleScalables;

	std::vector<NxApexRenderVolume*>		mRenderVolumeList;
	std::vector<NxApexEmitterAsset*>		mApexEmitterAssetList;
	std::vector<NxTurbulenceFSAsset*>		mApexTurbulenceFSAssetList;
	std::vector<NxRenderMeshAsset*>			mApexRenderMeshAssetList;
	std::vector<NxApexAsset*>				mApexFieldSamplerAssetList;

#if NX_SDK_VERSION_MAJOR == 2
	std::vector<NxApexAsset*>				mApexFieldBoundaryAssetList;
	NxExplosionAsset*						mApexExplosionAsset;
	NxWindAsset*							mApexWindAsset;
#endif

	std::vector<SampleTurbulenceFSActor*>	mApexTurbulenceActors;

	NxApexAsset*							mApexTurbulenceAsset;
	NxApexAsset*							mApexIosAsset;
	NxApexAsset*							mApexIofxAsset;
	NxApexAssetPreview*						mApexTurbulencePreview;

	SampleRenderer::RendererBoxShape*		mRendererBoxShape;
	SampleRenderer::RendererCapsuleShape*	mRendererCapsuleShape;

	std::vector<NxTurbulenceSphereHeatSource*>	mSphereHeatSource;

	SampleBoxActor*							mApexBoxCollisionShape[5];
	SampleSphereActor*						mApexSphereCollisionShape;
	SampleCapsuleActor*						mApexCapsuleCollisionShape;
	SampleConvexMeshActor*					mApexMeshCollisionShape;
	SamplePlaneActor*						mApexHalfSpaceCollisionShape;
	PxF32									mApexCollisionShapeSpeed;

	NxParameterized::Interface*				mTurbulenceFSDebugRenderParams;

	SampleRenderer::RendererMeshContext		mRenderMeshContext;
	physx::PxMat44							mRenderMeshTransform;

	Vector3D								mVelocity;
	PxF32									mPlaneOffset;
	PxReal									mPlaneX;
	PxReal									mPlaneY;
	PxReal									mPlaneZ;
	PxU16									mPlaneSwitch;
};

#endif //SIMPLE_TURBULENCE_H






