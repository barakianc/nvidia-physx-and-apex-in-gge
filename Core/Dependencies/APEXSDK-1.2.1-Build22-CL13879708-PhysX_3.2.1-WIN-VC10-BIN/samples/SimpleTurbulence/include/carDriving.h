//code from http://www.gamedev.net/community/forums/topic.asp?topic_id=470497


#include <algorithm>
#include <math.h>
#define PI 3.14159265


//mini 2d vector
class Vector
{
public:
	float X, Y;

	Vector()
	{
		X = 0;
		Y = 0;
	}
	Vector(float x, float y)
	{
		X = x;
		Y = y;
	}

	//length property
	float Length()
	{
		return (float)sqrt((double)(X * X + Y * Y));
	}

	//addition
	Vector operator +(Vector R)
	{
		return Vector(X + R.X, Y + R.Y);
	}

	//addition
	Vector operator +=(Vector R)
	{
		X += R.X;
		Y += R.Y;
		return (*this);
	}

	//subtraction
	Vector operator -(Vector R)
	{
		return Vector(X - R.X, Y - R.Y);
	}

	//subtraction
	Vector operator -=(Vector R)
	{
		X -= R.X;
		Y -= R.Y;
		return (*this);
	}

	//negative
	Vector operator -()
	{
		return Vector(-X, -Y);
	}

	//scalar multiply
	Vector operator *(float R)
	{
		return Vector(X * R, Y * R);
	}

	//divide multiply
	Vector operator /(float R)
	{
		return Vector(X / R, Y / R);
	}

	//dot product
	float operator *(Vector R)
	{
		return (X * R.X + Y * R.Y);
	}

	//cross product, in 2d this is a scalar since
	//we know it points in the Z direction
	float operator %(Vector R)
	{
		return (X * R.Y - Y * R.X);
	}

	//normalize the vector
	void normalize()
	{
		float mag = Length();

		X /= mag;
		Y /= mag;
	}

	//project this vector on to v
	Vector Project(Vector v)
	{
		//projected vector = (this dot v) * v;
		float thisDotV = (*this) * v;
		return v * thisDotV;
	}

	//project this vector on to v, return signed magnatude
	Vector Project(Vector v, float& mag)
	{
		//projected vector = (this dot v) * v;
		float thisDotV = (*this) * v;
		mag = thisDotV;
		return v * thisDotV;
	}

	Vector rotateVector(float theta)//angle given in radians
	{
		float cosTheta = cos(theta);
		float sinTheta = sin(theta);
		return Vector(X * cosTheta - Y * sinTheta, X * sinTheta + Y * cosTheta);
	}
};


//our simulation object
class RigidBody
{
protected:
	//linear properties
	Vector m_position;
	Vector m_velocity;
	Vector m_forces;
	float m_mass;

	//angular properties
	float m_angle;
	float m_angularVelocity;
	float m_torque;
	float m_inertia;

public:
	RigidBody()
	{
		//set these defaults so we dont get divide by zeros
		m_mass = 1.0f;
		m_angle = 0.0f;
		m_inertia = 1.0f;
		m_angularVelocity = 0.0f;
		m_torque = 0.0f;
		m_position = Vector(0, 0);
		m_velocity = Vector(0, 0);
		m_forces = Vector(0, 0);
	}

	//intialize out parameters
	virtual void Setup(Vector halfSize, float mass)
	{
		//store physical parameters
		m_mass = mass;
		m_inertia = (1.0f / 12.0f) * (halfSize.X * halfSize.X) *
		            (halfSize.Y * halfSize.Y) * mass;
	}

	void SetLocation(Vector position, float angle)
	{
		m_position = position;
		m_angle = angle;
	}

	Vector GetPosition()
	{
		return m_position;
	}
	Vector GetVelocity()
	{
		return m_velocity;
	}


	virtual void Update(float timeStep)
	{
		//integrate physics
		//linear
		Vector acceleration = m_forces / m_mass;
		m_velocity += acceleration * timeStep;
		m_position += m_velocity * timeStep;
		m_forces = Vector(0, 0); //clear forces

		//angular
		float angAcc = m_torque / m_inertia;
		m_angularVelocity += angAcc * timeStep;
		m_angle += m_angularVelocity * timeStep;
		m_torque = 0; //clear torque
	}

	//take a relative vector and make it a world vector
	//do we need a translation here?
	Vector RelativeToWorld(Vector relative)
	{
		float angle = m_angle;
		return relative.rotateVector(angle);
	}

	//take a world vector and make it a relative vector
	Vector WorldToRelative(Vector world)
	{
		float angle = -m_angle;
		return world.rotateVector(angle);
	}

	//velocity of a point on body
	Vector PointVel(Vector worldOffset)
	{
		Vector tangent = Vector(-worldOffset.Y, worldOffset.X);
		return tangent * m_angularVelocity + m_velocity;
	}

	void AddForce(Vector worldForce, Vector worldOffset)
	{
		//add linar force
		m_forces += worldForce;
		//and its associated torque
		m_torque += worldOffset % worldForce; //% is the cross product
	}
};


struct wheelProperties
{
	Vector orientation;
	float speed;
	float theta;
};

//our vehicle object
class Vehicle : public RigidBody
{
private:

	class Wheel
	{
	private:
		Vector m_forwardAxis, m_sideAxis;
		float m_wheelTorque, m_wheelSpeed;
		float m_wheelInertia, m_wheelRadius;
		Vector m_Position;
		float m_friction;
		float m_baseFriction;
		float m_renderingTheta;

	public:
		Wheel() {};
		Wheel(Vector position, float radius)
		{
			m_Position = position;
			m_wheelTorque = 0;
			SetSteeringAngle(0);
			m_wheelSpeed = 0;
			m_wheelRadius = radius;
			m_wheelInertia = radius * radius; //fake value
			m_baseFriction = 1;
			m_friction = 1;
			m_renderingTheta = 0.0;
		}

		void SetSteeringAngle(float newAngle)
		{
			/*
			Matrix mat = new Matrix();
			PointF[] vectors = new PointF[2];

			//foward vector
			vectors[0].X = 0;
			vectors[0].Y = 1;
			//side vector
			vectors[1].X = -1;
			vectors[1].Y = 0;

			mat.Rotate(newAngle / (float)Math.PI * 180.0f);
			mat.TransformVectors(vectors);

			m_forwardAxis = new Vector(vectors[0].X, vectors[0].Y);
			m_sideAxis = new Vector(vectors[1].X, vectors[1].Y);*/
			//float angle =  newAngle / (float)PI * 180.0f;
			float angle =  newAngle * ((float)PI / 180.0f);
			m_forwardAxis = Vector(0, 1).rotateVector(angle);
			m_sideAxis = Vector(-1, 0).rotateVector(angle);
		}

		void SetBrakingOn(float brakeFriction)
		{
			m_friction = brakeFriction;
		}

		void SetBrakingOff()
		{
			m_friction = m_baseFriction;
		}

		void AddTransmissionTorque(float newValue)
		{
			m_wheelTorque += newValue;
			//if(m_wheelTorque > 40.0) m_wheelTorque=40.0; //clamp the wheel torque
		}

		Vector GetForwardAxis()
		{
			return m_forwardAxis;
		}

		float GetWheelSpeed()
		{
			return m_wheelSpeed;
		}

		float GetRenderingTheta()
		{
			return m_renderingTheta;
		}

		Vector GetAttachPoint()
		{
			return m_Position;
		}

		Vector CalculateForce(Vector relativeGroundSpeed, float timeStep)
		{
			// cheesy friction forces
			m_wheelSpeed *= .95f;

			//calculate speed of tire patch at ground
			Vector patchSpeed = -m_forwardAxis * m_wheelSpeed * m_wheelRadius;

			//get velocity difference between ground and patch
			Vector velDifference = relativeGroundSpeed + patchSpeed;

			//project ground speed onto side axis
			float forwardMag = 0;
			Vector sideVel = velDifference.Project(m_sideAxis);
			Vector forwardVel = velDifference.Project(m_forwardAxis, forwardMag);

			//calculate super fake friction forces
			//calculate response force
			Vector responseForce = -sideVel * 2.0f * m_friction;
			responseForce -= forwardVel * m_friction;

			//calculate torque on wheel
			m_wheelTorque += forwardMag * m_wheelRadius;

			//integrate total torque into wheel
			m_wheelSpeed += m_wheelTorque / m_wheelInertia * timeStep;

			//clear our transmission torque accumulator
			m_wheelTorque = 0;

			//return force acting on body
			return responseForce;
		}

		void updateRenderingTheta()
		{
			//these hardcoded variables still need to be tweaked, and partly depend on the framerate of the demo
			float thetaInc = (m_wheelSpeed / 20.0f);
			if (thetaInc > 0.2f)
			{
				thetaInc = 0.2f;
			}
			m_renderingTheta += thetaInc;
		}

	};

	Wheel wheels[4];

public:

	Vehicle() {};
	void getWheelPositionAndOrientationAndSpeed(int w, wheelProperties& wP)
	{
		wP.orientation = wheels[w].GetForwardAxis();
		wP.speed = wheels[w].GetWheelSpeed();
		wP.theta = wheels[w].GetRenderingTheta();
	}

	void Setup(Vector halfSize, float mass)
	{
		//front wheels
		float wheelRadius = 0.5f;
		wheels[0] = Wheel(Vector(halfSize.X, halfSize.Y), wheelRadius);
		wheels[1] = Wheel(Vector(-halfSize.X, halfSize.Y), wheelRadius);

		//rear wheels
		wheels[2] = Wheel(Vector(halfSize.X, -halfSize.Y), wheelRadius);
		wheels[3] = Wheel(Vector(-halfSize.X, -halfSize.Y), wheelRadius);

		RigidBody::Setup(halfSize, mass);
	}

	void SetSteering(float steering)
	{
		const float steeringLock = 70.0f; //5.0f; //0.75f;

		//apply steering angle to front wheels
		wheels[0].SetSteeringAngle(steering * steeringLock);
		wheels[1].SetSteeringAngle(steering * steeringLock);
	}

	void SetThrottle(float throttle, bool allWheel)
	{
		const float torque = 10.0f;

		//apply transmission torque to back wheels
		if (allWheel)
		{
			wheels[0].AddTransmissionTorque(throttle * torque);
			wheels[1].AddTransmissionTorque(throttle * torque);
		}

		wheels[2].AddTransmissionTorque(throttle * torque);
		wheels[3].AddTransmissionTorque(throttle * torque);


	}

	void SetBrakes(float brakes)
	{
		const float brakeTorque = 2.0f;

		//apply brake torque apposing wheel vel
		for (int i = 0; i < 4; i++)
		{
			float wheelVel = wheels[i].GetWheelSpeed();
			wheels[i].AddTransmissionTorque(-wheelVel * brakeTorque * brakes);
			if (brakes != 0)
			{
				wheels[i].SetBrakingOn(4.0f);
			}
			else
			{
				wheels[i].SetBrakingOff();
			}
		}
	}

	void Update(float timeStep)
	{
		for (int i = 0; i < 4; i++)
		{
			Vector worldWheelOffset = RigidBody::RelativeToWorld(wheels[i].GetAttachPoint());
			Vector worldGroundVel = RigidBody::PointVel(worldWheelOffset);
			Vector relativeGroundSpeed = RigidBody::WorldToRelative(worldGroundVel);
			Vector relativeResponseForce = wheels[i].CalculateForce(relativeGroundSpeed, timeStep);
			Vector worldResponseForce = RigidBody::RelativeToWorld(relativeResponseForce);
			RigidBody::AddForce(worldResponseForce, worldWheelOffset);
			wheels[i].updateRenderingTheta();
		}

		RigidBody::Update(timeStep);
	}
};
