// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.



#include <algorithm>
#include <math.h>
#define PI 3.14159265


//mini 3d vector
class Vector3D
{
public:
	float X, Y, Z;

	Vector3D()
	{
		X = 0;
		Y = 0;
		Z = 0;
	}
	Vector3D(float x, float y, float z)
	{
		X = x;
		Y = y;
		Z = z;
	}

	//length property
	float Length()
	{
		return (float)sqrt((double)(X * X + Y * Y + Z * Z));
	}

	//addition
	Vector3D operator +(Vector3D R)
	{
		return Vector3D(X + R.X, Y + R.Y, Z + R.Z);
	}

	//addition
	Vector3D operator +=(Vector3D R)
	{
		X += R.X;
		Y += R.Y;
		Z += R.Z;
		return (*this);
	}

	//subtraction
	Vector3D operator -(Vector3D R)
	{
		return Vector3D(X - R.X, Y - R.Y, Z - R.Z);
	}

	//subtraction
	Vector3D operator -=(Vector3D R)
	{
		X -= R.X;
		Y -= R.Y;
		Z -= R.Z;
		return (*this);
	}

	//negative
	Vector3D operator -()
	{
		return Vector3D(-X, -Y, -Z);
	}

	//scalar multiply
	Vector3D operator *(float R)
	{
		return Vector3D(X * R, Y * R, Z * R);
	}

	//scalar multiply
	Vector3D operator *=(float R)
	{
		X *= R;
		Y *= R;
		Z *= R;
		return (*this);
	}

	//divide multiply
	Vector3D operator /(float R)
	{
		return Vector3D(X / R, Y / R, Z / R);
	}

	//dot product
	float operator *(Vector3D R)
	{
		return (X * R.X + Y * R.Y + Z * R.Z);
	}

	//normalize the vector
	void normalize()
	{
		float mag = Length();

		X /= mag;
		Y /= mag;
		Z /= mag;
	}
};

//simple movable object without orientation or angular velocity
class simpleMovableBody
{
private:
	Vector3D m_position;
	Vector3D m_velocity;
	Vector3D m_forces;
	Vector3D m_planeNormal;
	Vector3D m_planeOrigin;
	bool m_enableCollisionPlane;
	float m_mass;

	Vector3D ProjectOntoPlane(Vector3D position, Vector3D planeNormal, Vector3D planeOrigin)
	{
		planeNormal.normalize();
		float dot = planeNormal * (position - planeOrigin);
		return position - planeNormal * dot;
	}

public:

	void reset()
	{
		m_position = Vector3D(0, 0, 0);
		m_velocity = Vector3D(0, 0, 0);
		m_forces = Vector3D(0, 0, 0);
		m_mass = 0.25f;
		m_enableCollisionPlane = false;
		m_planeNormal = Vector3D(0, 1, 0);
		m_planeOrigin = Vector3D(0, 0, 0);
	}
	simpleMovableBody()
	{
		reset();
	}

	void setPosition(Vector3D position)
	{
		m_position = position;
	}

	void setVelocity(Vector3D velocity)
	{
		m_velocity = velocity;
	}

	void setMass(float mass)
	{
		m_mass = mass;
	}

	void AddForce(Vector3D force)
	{
		m_forces += force;
	}
	//void AddForce(float forceX, float forceY, float forceZ){  m_forces += Vector3D(forceX, forceY, forceZ); }

	Vector3D GetPosition()
	{
		return m_position;
	}
	Vector3D GetVelocity()
	{
		return m_velocity;
	}

	void setCollisionPlane(Vector3D normal, Vector3D origin, bool enable = true)
	{
		m_planeNormal = normal;
		m_planeOrigin = origin;
		m_enableCollisionPlane = enable;
	}

	void update(float timeStep)
	{
		Vector3D acceleration = m_forces / m_mass;
		m_velocity += acceleration * timeStep;
		m_velocity *= 0.99f;
		Vector3D newPos = m_position + m_velocity * timeStep;

		if (m_enableCollisionPlane)
		{
			//project the position onto the surface of the plane, if the position lies inside the plane
			Vector3D vecToPlane = newPos - m_planeOrigin;
			if (vecToPlane * m_planeNormal < 0)
			{
				newPos = ProjectOntoPlane(newPos, m_planeNormal, m_planeOrigin);    //project the point onto the plane
			}
		}

		m_position = newPos; //set the position
		m_forces = Vector3D(0, 0, 0); //clear forces

	}
};




