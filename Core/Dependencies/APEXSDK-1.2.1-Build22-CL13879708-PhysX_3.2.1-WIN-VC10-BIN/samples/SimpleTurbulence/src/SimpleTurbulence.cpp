// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Turbulence Sample
// Description: This sample program shows how to create APEX TurbulenceFS actors.
//
// ===============================================================================


#include <SimpleTurbulence.h>

PxReal normalA = 1.0f;
PxReal normalB = 3.0f;
PxReal normalC = 7.0f;
PxReal default_planeOffset = 0.5f;

//general data for sample scenes----------------------------
bool g_visualizeHeat = true;
bool g_applyWind = false;
bool g_visualizePlane = false;
bool g_visualizeGrid = false;
bool g_visualizeLOD = false;
bool g_visualizeField = true;

//data and functions specific to the user controlled car and movable object---------------------------------------------

simpleMovableBody g_simpleMovableBody;

Vehicle* drivableCar = 0;
float g_steering = 0; //-1 is left, 0 is center, 1 is right
float g_brakes = 0;   //0 is no brakes, 1 is full brakes
float g_throttle = 0; //0 is coasting, 1 is full throttle
#ifndef TEST_PW_FORCES
bool g_velBasedEmission = false;//true; //set this to false to test initial particle forces
#else
bool g_velBasedEmission = false;
#endif
//key entries
bool g_leftHeld = false;
bool g_rightHeld = false;
bool g_forwardHeld = false;
bool g_backHeld = false;
bool g_upHeld = false;
bool g_downHeld = false;
bool g_userLead = false;

float g_jetStrength = 7.0f;
float g_carTransformation[16];
float g_maxParticleRate = 0.15f;

bool g_renderEnable = true;
bool g_applyForce = false;
int g_applyForceFrame = 0;


bool g_useSphereForHeat = true;


const char* const SampleTurbulenceInputEventDescriptions[] =
{
	"move the scene's actor backward",
	"move the scene's actor forward",
	"move the scene's actor left",
	"move the scene's actor right",
	"move the scene's actor down",
	"move the scene's actor up",

	"switch to scene 0",
	"switch to scene 1",
	"switch to scene 2",
	"switch to scene 3",
	"switch to scene 4",
	"switch to scene 5",
	"switch to scene 6",
	"switch to scene 7",
	"switch to scene 8",
	"switch to scene 9",

	"(+ shift) increase simulation velocity (LOD and collision object scenes only)",
	"(+ shift) decrease simulation velocity (LOD and collision object scenes only)",

	"increase simulation scale",
	"decrease simulation scale",

	"increase temperature of the heat source (heat scene only)",
	"decrease temperature of the heat source (heat scene only)",

	"increase simulation LOD (LOD scene (0) only) / collision object radius (other scenes)",
	"decrease simulation LOD (LOD scene (0) only) / collision object radius (other scenes)",

	"reset the simulation",

	"toggle wind and heat visualization and rotation of collision sphere",
	"toggle heat visualization",
	"toggle particle visualization",
	"toggle ground plane visualization",
	"toggle grid visualization",
	"toggle LOD visualization",
	"toggle field visualization",
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleTurbulenceInputEventDescriptions) == NUM_EXCLUSIVE_SAMPLE_TURBULENCE_INPUT_EVENT_IDS);


static const char* benchmarkName(unsigned id)
{
	if (id < STCLIDS::FIRST_BENCHMARK || id > STCLIDS::LAST_BENCHMARK)
	{
		return NULL;
	}
	return SampleTurbulenceCommandLineInputs[id - STCLIDS::FIRST].mShortName;
}

///////////////////////////////////////////////////////////////////////////

void updateCarInputs()
{
	if (g_leftHeld && g_rightHeld)
	{
		g_steering = 0;
		g_userLead = true;
	}
	else if (g_leftHeld)
	{
		g_steering = -1;
		g_userLead = true;
	}
	else if (g_rightHeld)
	{
		g_steering = 1;
		g_userLead = true;
	}
	else
	{
		g_steering = 0;
	}

	if (g_forwardHeld)
	{
		g_throttle = 1;
		g_userLead = true;
	}
	else
	{
		g_throttle = 0;
	}

	if (g_backHeld)
	{
		g_brakes = 1;
		g_userLead = true;
	}
	else
	{
		g_brakes = 0;
	}
}

void updateSimpleMovableBodyInputs(simpleMovableBody& body)
{
	if (g_backHeld)
	{
		body.AddForce(Vector3D(0, 0, 1));
		g_userLead = true;
	}
	if (g_forwardHeld)
	{
		body.AddForce(Vector3D(0, 0, -1));
		g_userLead = true;
	}
	if (g_rightHeld)
	{
		body.AddForce(Vector3D(1, 0, 0));
		g_userLead = true;
	}
	if (g_leftHeld)
	{
		body.AddForce(Vector3D(-1, 0, 0));
		g_userLead = true;
	}
	if (g_upHeld)
	{
		body.AddForce(Vector3D(0, -1, 0));
		g_userLead = true;
	}
	if (g_downHeld)
	{
		body.AddForce(Vector3D(0, 1, 0));
		g_userLead = true;
	}
}

void updateInputsForMovement(const SampleFramework::InputEvent& ie, bool down)
{
	switch (ie.m_Id)
	{
	case CONTROL_BACK:
		g_backHeld = down;
		break;
	case CONTROL_FORWARD:
		g_forwardHeld = down;
		break;
	case CONTROL_RIGHT:
		g_rightHeld = down;
		break;
	case CONTROL_LEFT:
		g_leftHeld = down;
		break;
	case CONTROL_UP:
		g_upHeld = down;
		break;
	case CONTROL_DOWN:
		g_downHeld = down;
		break;
	default:
		break;
	}
}

void advanceCar(float timestep)
{
	//apply vehicle controls
	updateCarInputs();

	// don't look at the user's input yet - for the purpose of debugging
	if(g_userLead == false)
	{
		g_steering = -1;
		g_throttle = 1;
	}

	drivableCar->SetSteering(g_steering);
	drivableCar->SetThrottle(g_throttle, true);
	drivableCar->SetBrakes(g_brakes);
	//integrate vehicle physics
	drivableCar->Update(timestep);

	//update the global orientation matrix for the car
}

void initCar()
{
	drivableCar = new Vehicle();
	drivableCar->Setup(Vector(1.5, 3), 5);
	drivableCar->SetLocation(Vector(0, 0), 0);
}

// Function to return the acquired component of the position of the actors used by the corresponding scene benchmark.
static physx::PxF32 actorPosition(physx::PxU32 actorNum, physx::PxU32 numActors, SceneSelection scene, char component)
{
	physx::PxF32 spacing = 15.0f;
	if(component == 'X' || component == 'x')
	{
		switch(scene)
		{
		case TurbulenceCollisionObjectScene:
			{
				spacing = 8.0f;
			}
			break;
		case TurbulenceHeatScene:
		case TurbulenceAttractorFSScene:
			{
				spacing = 6.0f;
			}
			break;
		case TurbulenceJetFSScene:
			{
				spacing = 12.0f;
			}
			break;
#if NX_SDK_VERSION_MAJOR == 2
		case TurbulenceWindFSScene:
			{
				spacing = 45.0f;
			}
			break;
#endif
		case TurbulenceWindScene:
			{
				spacing = -7.0f;
			}
			break;
		default:
			break;
		};
	}
	else if(component == 'Z' || component == 'z')
	{
		switch(scene)
		{
		case TurbulenceWindScene:
			{
				spacing = 12.0f;
			}
			break;
		default:
			spacing = 0.0f;
			break;
		};
	}


	physx::PxF32 returnVal;
	returnVal = ((-(physx::PxF32)(numActors-1) * (spacing / 2.0f)) + ((physx::PxF32)actorNum * spacing));
	return(returnVal);
}

SampleApexEmitterActor::SampleApexEmitterActor(NxApexEmitterAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n, SceneSelection scene)
{
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	mActor = static_cast<NxApexEmitterActor*>(asset.createApexActor(*descParams, apexScene));
	PX_ASSERT(mActor);
	PxF32 ycomp = 0.0f;
	if(scene == TurbulenceCollisionObjectScene) ycomp = -3.0f; 
	else if(scene == TurbulenceTrailingSmokeScene) ycomp = 6.0f; 
	else ycomp = 0.0f; 
	if (mActor)
	{
		mActor->setCurrentPosition(physx::PxVec3(actorPosition(i, n, scene, 'x'), ycomp, actorPosition(i, n, scene, 'z')));
		mActor->startEmit(true);
	}
}

void SampleApexEmitterActor::setCurrentPosition(const physx::PxVec3& pos)
{
	mActor->setCurrentPosition(pos);
}

void SampleApexEmitterActor::setCurrentPose(const physx::PxMat44& pose)
{
	mActor->setCurrentPose(pose);
}


SampleApexEmitterActor::~SampleApexEmitterActor(void)
{
	if (mActor)
	{
		mActor->release();
	}
}

void SampleApexEmitterActor::updateEyePosition(physx::PxVec3& /*position*/)
{
}

void SampleApexEmitterActor::render(bool /*rewriteBuffers*/)
{
}

int SampleApexEmitterActor::getType()
{
	return ApexEmitterActor;
}



SampleJetFieldSamplerActor::SampleJetFieldSamplerActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n)
{
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	NxParameterized::Handle handle(descParams);

	if (NxParameterized::ERROR_NONE == handle.getParameter("initialPose"))
	{
		physx::PxMat44 actorPose = physx::PxMat44::createIdentity();
		actorPose.setPosition(physx::PxVec3(actorPosition(i, n, TurbulenceJetFSScene, 'x'), 0.0f, 0.0f));
		handle.setParamMat34(actorPose);
	}

	mActor = static_cast<NxJetFSActor*>(asset.createApexActor(*descParams, apexScene));
	PX_ASSERT(mActor);
}

SampleJetFieldSamplerActor::~SampleJetFieldSamplerActor(void)
{
	if (mActor)
	{
		mActor->release();
	}
}

void SampleJetFieldSamplerActor::updateEyePosition(physx::PxVec3& /*position*/)
{
}

void SampleJetFieldSamplerActor::render(bool /*rewriteBuffers*/)
{
}

void SampleJetFieldSamplerActor::setPose(const PxMat44& pose)
{
	mActor->setCurrentPose(pose);
}

void SampleJetFieldSamplerActor::setFieldStrength(PxF32 value)
{
	mActor->setFieldStrength(value);
}

int SampleJetFieldSamplerActor::getType()
{
	return JetFieldSamplerActor;
}


SampleAttractorFieldSamplerActor::SampleAttractorFieldSamplerActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n)
{
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	NxParameterized::Handle handle(descParams);

	if (NxParameterized::ERROR_NONE == handle.getParameter("initialPosition"))
	{
		physx::PxVec3 actorPosition(actorPosition(i, n, TurbulenceAttractorFSScene, 'x'), 0.0f, 0.0f);
		handle.setParamVec3(actorPosition);
	}

	mActor = static_cast<NxAttractorFSActor*>(asset.createApexActor(*descParams, apexScene));
	PX_ASSERT(mActor);
}

SampleAttractorFieldSamplerActor::~SampleAttractorFieldSamplerActor(void)
{
	if (mActor)
	{
		mActor->release();
	}
}

void SampleAttractorFieldSamplerActor::updateEyePosition(physx::PxVec3& /*position*/)
{
}

void SampleAttractorFieldSamplerActor::render(bool /*rewriteBuffers*/)
{
}

void SampleAttractorFieldSamplerActor::setPosition(const PxVec3& position)
{
	mActor->setCurrentPosition(position);
}

void SampleAttractorFieldSamplerActor::setFieldRadius(PxF32 radius)
{
	mActor->setFieldRadius(radius);
}

void SampleAttractorFieldSamplerActor::setConstFieldStrength(PxF32 value)
{
	mActor->setConstFieldStrength(value);
}

void SampleAttractorFieldSamplerActor::setVariableFieldStrength(PxF32 value)
{
	mActor->setVariableFieldStrength(value);
}

int SampleAttractorFieldSamplerActor::getType()
{
	return AttractorFieldSamplerActor;
}

#if NX_SDK_VERSION_MAJOR == 2
SampleFieldBoundaryActor::SampleFieldBoundaryActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32)
{
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	mActor = asset.createApexActor(*descParams, apexScene);
	PX_ASSERT(mActor);
}

SampleFieldBoundaryActor::~SampleFieldBoundaryActor(void)
{
	if (mActor)
	{
		mActor->release();
	}
}

void SampleFieldBoundaryActor::updateEyePosition(physx::PxVec3& /*position*/)
{
}

void SampleFieldBoundaryActor::render(bool /*rewriteBuffers*/)
{
}

int SampleFieldBoundaryActor::getType()
{
	return FieldBoundaryActor;
}
#endif


SampleTurbulenceFSActor::SampleTurbulenceFSActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i, physx::PxU32 n, SceneSelection scene)
{
	PxF32 ypos;
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	mActor = static_cast<NxTurbulenceFSActor*>(asset.createApexActor(*descParams, apexScene));
	PX_ASSERT(mActor);
	if(scene == TurbulenceCollisionObjectScene) ypos = 4.0f; else ypos = 0.0f;
	if (mActor)
	{
		physx::PxMat44 turbPose;
		physx::PxVec3  turbPosition(actorPosition(i, n, scene, 'x'), ypos, actorPosition(i, n, scene, 'z'));
		turbPose = turbPose.createIdentity();
		turbPose.setPosition(turbPosition);
		mActor->setPose(turbPose);

		//set the LOD for the actor (very important for turbulence!)

		//the function is setLODWeights(NxReal maxDistance, NxReal distanceWeight, NxReal bias, NxReal benefitBias, NxReal benefitWeight)

		//the first 4 parameters are used to compute the benefit of the given actor, using this formula:
		//(m_LODdistanceWeight * physx::PxMax(0.0f , m_LODmaxDistance - distanceFromEye) + m_LODbias) * m_simulationCost + m_LODbenefitBias
		//note that part of the LOD calculation is based on the distance from the eye, and if you use a non-zero m_LODdistanceWeight then
		//the distance from the eye will be used to actually affect the LOD of the system. You can also choose to bypass the distance from the eye,
		//and just base the LOD on the simulation cost (use non-zero m_LODbias) just set a constant benefit using m_LODbenefitBias, or use a combination of all.

		//the last parameter (called benefitWeight) is used to influence how many resources the actor will use *if* it is given more resources than it thinks it needs.
		//for example, say you have only one turbulence actor in the scene and it is very far away from the eye - this actor calculates his benefit to be very low
		//but since the system has no other actors to give resources to, it gives all alvailable resources to this actor. If you would like that this actor should not
		//use all the available resources (instead letting the game run at a higher frame rate) then you should set this weight to 1. On the other hand, setting
		//this weight to a very large number will make the actor use the given resources (and values progressively higher than 1 let you tweak how late you want
		//the actor to start using less than the given amount of resources).
		//the basic formula that the actor uses for this decision is
		//neededResources = MaxDesiredResources * (Benefit * benefitWeight) / MaxBenefit

		mActor->setLODWeights(400.0f, 1.0f, 0.0f, 0.0f, 3.0f);

		//if you dont want the LOD system to affect turbulence then use these calls below to set it explicitly. 1.0f corresponds to full simulation fidelity
		//mActor->enableCustomLOD(true);
		//mActor->setCustomLOD(1.0f);

	}
}

SampleTurbulenceFSActor::~SampleTurbulenceFSActor(void)
{
	if (mActor)
	{
		mActor->release();
	}
}

void SampleTurbulenceFSActor::updateEyePosition(physx::PxVec3& /*position*/)
{
}

void SampleTurbulenceFSActor::render(bool /*rewriteBuffers*/)	
{
}

int SampleTurbulenceFSActor::getType()
{
	return TurbulenceFSActor;
}

#if NX_SDK_VERSION_MAJOR == 2
SampleWindActor::SampleWindActor(NxWindAsset& asset, NxApexScene& apexScene, const physx::PxMat44& boxWorldPose, const physx::PxMat33& windToWorld, const physx::PxVec3& scale) :
mAsset(asset)
{
	mActor = NULL;
	NxParameterized::Interface* defaultParams = mAsset.getDefaultActorDesc();
	PX_ASSERT(defaultParams);
	NxParameterized::setParamMat44(*defaultParams, "boxWorldPose", boxWorldPose);
	NxParameterized::setParamMat33(*defaultParams, "windToWorld", windToWorld);
	NxParameterized::setParamVec3(*defaultParams, "halfLengthDimensions", scale);
	mActor =  static_cast<NxWindActor*>(mAsset.createApexActor(*defaultParams, apexScene));
	PX_ASSERT(mActor);
}

SampleWindActor::~SampleWindActor(void)
{
	if (mActor)
	{
		mAsset.releaseWindActor(*mActor);
	}
}

SampleExplosionActor::SampleExplosionActor(NxExplosionAsset& asset, NxApexScene& apexScene, const physx::PxF32& scale, const physx::PxMat44 pos) :
mAsset(asset)
{
	mActor = NULL;
	NxParameterized::Interface* defaultParams = mAsset.getDefaultActorDesc();
	PX_ASSERT(defaultParams);
	NxParameterized::setParamF32(*defaultParams, "scale", scale);
	NxParameterized::setParamMat44(*defaultParams, "initialPose", pos);
	mActor =  static_cast<NxExplosionActor*>(mAsset.createApexActor(*defaultParams, apexScene));
	PX_ASSERT(mActor);
}

SampleExplosionActor::~SampleExplosionActor(void)
{
	if (mActor)
	{
		mAsset.releaseExplosionActor(*mActor);
	}
}
#endif

SimpleTurbulenceApplication::SimpleTurbulenceApplication(const SampleFramework::SampleCommandLine& cmdline)	: SampleApexApplication(cmdline)
{
	m_simulationBudget				= 0xffffff00;
	mRenderParticles				= true;
	mVelocityClamp					= 2.5f;
	mTemperature					= 3.0f;
	mRotationType					= 0;
	mSceneNumber					= TurbulenceCollisionObjectScene;
	m_benchmarkNumActors			= 1;

	mApexModuleIOS					= NULL;
	mApexEmitterModule				= NULL;
	mNbEmitterModuleScalables		= NULL;
	mEmitterModuleScalables		= NULL;
	mApexIofxModule					= NULL;
	mTurbulenceFSModule			= NULL;
	mNbTurbulenceModuleScalables	= NULL;
	mTurbulenceModuleScalables		= NULL;

	mApexIosAsset					= NULL;
	mApexIofxAsset					= NULL;
#if NX_SDK_VERSION_MAJOR == 2
	mApexExplosionAsset				= NULL;
	mApexWindAsset					= NULL;
#endif
	mApexTurbulenceAsset			= NULL;
	mApexTurbulencePreview			= NULL;

	for(PxU32 i = 0; i < 5; ++i)
	{
		mApexBoxCollisionShape[i]	= NULL;
	}
	mApexSphereCollisionShape		= NULL;
	mApexCapsuleCollisionShape		= NULL;
	mApexMeshCollisionShape			= NULL;
	mApexHalfSpaceCollisionShape	= NULL;
	mRendererBoxShape				= NULL;
	mRendererCapsuleShape			= NULL;
	mPlaneOffset					= 0.f;
	mPlaneX							= 0.f;
	mPlaneY							= 0.f;
	mPlaneZ							= 0.f;
	mPlaneSwitch					= NULL;

	m_commandLineInput.registerCommands(&SampleTurbulenceCommandLineInputs[0], STCLIDS::COUNT);
	m_commandLineInput.registerCommand(SampleCommandLineInputs[SCLIDS::NO_APEX_CUDA]);
	m_commandLineInput.registerCommand(SampleCommandLineInputs[SCLIDS::NO_INTEROP]);
	for (unsigned benchmarkID = STCLIDS::FIRST_BENCHMARK; benchmarkID <= STCLIDS::LAST_BENCHMARK; ++benchmarkID)
	{
		m_benchmarkNames.push_back(benchmarkName(benchmarkID));
	}

	m_AppPtr = this;

#if NX_SDK_VERSION_MAJOR == 2
	m_shapeGroupMask.bits0 = 1;
	m_shapeGroupMask.bits2 = ~0;
	m_shapeGroupMask.bits1 = m_shapeGroupMask.bits3 = 0;
#endif
	mVelocity = Vector3D(0.f, 0.f, 0.f);
}

void SimpleTurbulenceApplication::initCommonDebugRenderConfigs()
{	
	DebugRenderConfiguration config;

	config.flags.push_back(DebugRenderFlag("TurbulenceFS velocities - press m.", "TurbulenceFS/VISUALIZE_TURBULENCE_FS_VELOCITY"));
	config.flags.push_back(DebugRenderFlag("TurbulenceFS BBOX.", "TurbulenceFS/VISUALIZE_TURBULENCE_FS_BBOX"));
	config.flags.push_back(DebugRenderFlag("TurbulenceFS actor name.", "TurbulenceFS/VISUALIZE_TURBULENCE_FS_ACTOR_NAME"));
	config.flags.push_back(DebugRenderFlag("TurbulenceFS field.", "TurbulenceFS/VISUALIZE_TURBULENCE_FS_FIELD"));
	config.flags.push_back(DebugRenderFlag("TurbulenceFS temperature - press t.", "TurbulenceFS/VISUALIZE_TURBULENCE_FS_TEMPERATURE"));
	config.flags.push_back(DebugRenderFlag("TurbulenceFS streamlines.", "TurbulenceFS/VISUALIZE_TURBULENCE_FS_STREAMLINES"));
	addDebugRenderConfig(config);
	config.flags.clear();

	config.flags.push_back(DebugRenderFlag("LOD benefits.", "VISUALIZE_LOD_BENEFITS"));
	addDebugRenderConfig(config);
	config.flags.clear();

	config.flags.push_back(DebugRenderFlag("IOFX actor.", "Iofx/VISUALIZE_IOFX_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	config.flags.push_back(DebugRenderFlag("Emitter actor.", "Emitter/apexEmitterParameters.VISUALIZE_APEX_EMITTER_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

#if NX_SDK_VERSION_MAJOR == 2
	config.flags.push_back(DebugRenderFlag("Collision Shapes", NX_VISUALIZE_COLLISION_SHAPES, 1.0f));
#elif NX_SDK_VERSION_MAJOR == 3
	config.flags.push_back(DebugRenderFlag("Collision Shapes", physx::PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f));
#endif
	addDebugRenderConfig(config);
	config.flags.clear();
}


void SimpleTurbulenceApplication::setSceneEyePos()
{
	PxMat44 eyeTransform = PxMat44::createIdentity(); //= m_AppPtr->getEyeTransform();
	PxF32 coeff;
	eyeTransform.column3.y = 2;
	if(mSceneNumber == TurbulenceCollisionObjectScene)
	{
		if(m_benchmarkNumActors == 1)
		{
			coeff = 13.0f;
		}
		else coeff = 7.0f * m_benchmarkNumActors;
	}
	else if(mSceneNumber == TurbulenceJetFSScene)
	{
		eyeTransform.column3.y = 7;
		if(m_benchmarkNumActors == 1)
		{
			eyeTransform.column3.y = 2.6;
			coeff = 13.7;
		}
		else coeff = 10.0f * m_benchmarkNumActors;
	}
	else if(mSceneNumber == TurbulenceAttractorFSScene)
	{
		eyeTransform.column3.y = 3.3f;
		if(m_benchmarkNumActors == 1)
		{
			coeff = 10.1f;
		}
		else coeff = 5.0f * m_benchmarkNumActors;
	}
	else if(mSceneNumber == TurbulenceWindScene)
	{
		eyeTransform.column3.y = 1.2f * m_benchmarkNumActors;
		coeff = 10.0f * m_benchmarkNumActors;
	}
	else if(mSceneNumber == TurbulenceHeatScene)
	{
		eyeTransform.column3.y = 7;
		if(m_benchmarkNumActors == 1)
		{
			coeff = 20;
		}
		else coeff = 8.0f * m_benchmarkNumActors;
	}
#if NX_SDK_VERSION_MAJOR == 2
	else if(mSceneNumber == TurbulenceWindFSScene) coeff = 30.0f * m_benchmarkNumActors;
	else if(mSceneNumber == TurbulenceExplosionFSScene) 
	{
		coeff = 20.0f;
		eyeTransform.column3.y = 5; 
	}
#endif
	else if(mSceneNumber == TurbulenceTrailingSmokeScene)
	{	
		coeff = 10.0f;
		eyeTransform.column3.y = 4; 
		eyeTransform.column3.x = 7; 
	}
	else coeff = 20.0f;
	eyeTransform.column3.z = 1.0f * coeff; 	// set the eye position based on the number of actors there will be
	m_AppPtr->setEyeTransform(eyeTransform);
}


void SimpleTurbulenceApplication::initializeText()
{
	if (mSceneNumber == TurbulenceTrailingSmokeScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Trailing Smoke Scene, use keys I,J,K,L to move the sphere");
		char str[MAX_DEBUG_TEXT_STRING];
		sprintf(str, "Scene LOD budget (+- to adjust, press \"V\" then \"N\" to visualize): %d", (physx::PxU32)(m_simulationBudget));
		strncpy(mWindowString[2], str, MAX_DEBUG_TEXT_STRING);
		sprintf(str, "Emitter Module scalable parameters (*/ to adjust)");
		strncpy(mWindowString[3], str, MAX_DEBUG_TEXT_STRING);
		sprintf(str, "Turbulence per Actor LOD");
		strncpy(mWindowString[4], str, MAX_DEBUG_TEXT_STRING);
		sprintf(str, "Turbulence Module scalable parameters ( ^& to adjust)");
		strncpy(mWindowString[5], str, MAX_DEBUG_TEXT_STRING);
	}
	else if (mSceneNumber == TurbulenceCollisionObjectScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Turbulence with external collision objects");
		strcpy(mWindowString[2], "Use space bar to create collision box (shift + space for spheres, ctrl + space for convexes, ctrl + shift + space for halfspaces)");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
		updateCollisionObjectSceneInfo();
	}
	else if (mSceneNumber == TurbulenceJetFSScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Turbulence with JetFS (jets)");
		strcpy(mWindowString[2], "Use keys U,Y,I,J,K,L to move the jet");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}
	else if (mSceneNumber == TurbulenceAttractorFSScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Turbulence with AttractorFS");
		strcpy(mWindowString[2], "Use keys U,Y,I,J,K,L to move the attractor");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}
	else if (mSceneNumber == TurbulenceHeatScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Turbulence with Thermodynamics");
		strcpy(mWindowString[2], "Use space bar to create collision box (shift + space for spheres, ctrl + space for convexes, ctrl + shift + space for halfspaces)");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "Use key 't' to toggle visualizing temperature (after choosing TurbulenceFS actor debug render mode - press 'v')");
		strcpy(mWindowString[5], "");
		updateHeatSceneInfo();
	}
	else if (mSceneNumber == TurbulenceWindScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Turbulence with external wind");
		strcpy(mWindowString[2], "Use key 'b' to toggle applying the wind");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}
#if NX_SDK_VERSION_MAJOR == 2
	else if (mSceneNumber == TurbulenceWindFSScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "BasicIOS with wind field sampler");
		strcpy(mWindowString[2], "");
		//disabled due to errors
		//strcpy(mWindowString[2], "Use key 'v' to turn on wind visualization");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}
	else if (mSceneNumber == TurbulenceExplosionFSScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "BasicIOS with explosion field sampler");
		strcpy(mWindowString[2], "Use right mouse click to activate explosions");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}
#endif
	else if (mSceneNumber == AssetPreviewScene)
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "Turbulence Asset Visualization");
		strcpy(mWindowString[2], "");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}
	else // NopScene
	{
		strcpy(mWindowString[0], "");
		strcpy(mWindowString[1], "");
		strcpy(mWindowString[2], "");
		strcpy(mWindowString[3], "");
		strcpy(mWindowString[4], "");
		strcpy(mWindowString[5], "");
	}

	if (cudaEnabled())
	{
		strcat(mWindowString[WINDOW_SCENE_NAME_INDEX], "CUDA IOFX");
		if (interopEnabled())
			strcat(mWindowString[WINDOW_SCENE_NAME_INDEX], " + Interop");
	}
	else
	{
		strcat(mWindowString[WINDOW_SCENE_NAME_INDEX], "CPU IOFX");
	}
}

void SimpleTurbulenceApplication::updateCollisionObjectSceneInfo()
{
	if (mSceneNumber == TurbulenceCollisionObjectScene)
	{
		char str[MAX_DEBUG_TEXT_STRING];
		sprintf(str, "Collision object radius (+- to adjust): %0.1f", m_collisionObjRadius);
		strncpy(mWindowString[3], str, MAX_DEBUG_TEXT_STRING);

		sprintf(str, "Collision object velocity clamp (^& to adjust): %0.1f", mVelocityClamp);
		strncpy(mWindowString[4], str, MAX_DEBUG_TEXT_STRING);

		switch (mRotationType)
		{
		case 0:
			sprintf(str, "X Collision sphere is rotating around X axis.");
			break;
		case 1:
			sprintf(str, "Z Collision sphere is rotating around Z axis.");
			break;
		case 2:
			sprintf(str, "Y Collision sphere is rotating around Y axis.");
			break;
		case 3:
			sprintf(str, "Collision sphere is not rotating.");
			break;
		}
		strncpy(mWindowString[5], str, MAX_DEBUG_TEXT_STRING);
	}
}

void SimpleTurbulenceApplication::updateHeatSceneInfo()
{
	if (mSceneNumber == TurbulenceHeatScene)
	{
		char str[MAX_DEBUG_TEXT_STRING];
		sprintf(str, "Heat source average temperature ('shift' + '*' (or 'shift' + '/') to adjust): %0.1f", mTemperature);
		strncpy(mWindowString[3], str, MAX_DEBUG_TEXT_STRING);

		switch (mRotationType)
		{
		case 0:
			sprintf(str, "X Collision sphere is rotating around X axis.");
			break;
		case 1:
			sprintf(str, "Z Collision sphere is rotating around Z axis.");
			break;
		case 2:
			sprintf(str, "Y Collision sphere is rotating around Y axis.");
			break;
		case 3:
			sprintf(str, "Collision sphere is not rotating.");
			break;
		}
		strncpy(mWindowString[5], str, MAX_DEBUG_TEXT_STRING);
	}
}

void SimpleTurbulenceApplication::updateScalableAndLodInfo()
{
	if (mSceneNumber == TurbulenceTrailingSmokeScene)
	{
		char str[MAX_DEBUG_TEXT_STRING];
		sprintf(str, "Scene LOD budget (+- to adjust, press \"V\" then \"C\" to visualize): %d", (physx::PxU32)(m_simulationBudget));
		strncpy(mWindowString[2], str, MAX_DEBUG_TEXT_STRING);

		if (mApexEmitterModule)
		{
			char str[MAX_DEBUG_TEXT_STRING];
			sprintf(str, "Emitter Module scalable parameters (*/ to adjust): ");

			for (physx::PxU32 i = 0; i < mNbEmitterModuleScalables; i++)
			{
				char pStr[MAX_DEBUG_TEXT_STRING];
				NxApexParameter& p = *mEmitterModuleScalables[i];
				sprintf(pStr, "%s:%d ", p.name, p.current);
				strcat(str, pStr);
			}
			strncpy(mWindowString[3], str, MAX_DEBUG_TEXT_STRING);
		}
		if (mTurbulenceFSModule)
		{
			std::string tmpString("Turbulence current LOD: ");
			for (unsigned int i = 0; i < mApexTurbulenceActors.size(); i++)
			{
				char str[MAX_DEBUG_TEXT_STRING];
				sprintf(str, " Actor %d LOD %.4f ,", i, mApexTurbulenceActors[i]->mActor->getCurrentLOD());
				tmpString.append(str);
			}
			strncpy(mWindowString[4], tmpString.c_str(), MAX_DEBUG_TEXT_STRING);


			tmpString = "Turbulence Module scalable parameters ( ^& to adjust): ";
			// scalable parameters, dont change these while running a real game!
			for (physx::PxU32 i = 0; i < mNbTurbulenceModuleScalables; i++)
			{
				char str[MAX_DEBUG_TEXT_STRING];
				NxApexParameter& p = *mTurbulenceModuleScalables[i];
				sprintf(str, "%s:%d ", p.name, p.current);
				tmpString.append(str);
			}
			strncpy(mWindowString[5], tmpString.c_str(), MAX_DEBUG_TEXT_STRING);
		}
	}
}

// Process the command line args that need to be processed before the window is open
void SimpleTurbulenceApplication::processEarlyCmdLineArgs(void)
{
	SampleApexApplication::initBenchmarks(&benchmarkStart);

	oa_OptionsOverrideDesc* desc = new oa_OptionsOverrideDesc();
	desc->setMaxActors(4);
	SampleApexApplication::processEarlyCmdLineArgs(openAutomateInterface::defaultProgramVersionString, desc);
}

// Determine world impact position to create explosion
#if NX_SDK_VERSION_MAJOR == 2
void SimpleTurbulenceApplication::onMouseClickShapeHit(const physx::PxVec3& /*pickDir*/, NxRaycastHit& hit, NxShape*)
{
	if (mSceneNumber == TurbulenceExplosionFSScene)
	{
		physx::PxF32 scale = 1.0f;
		physx::PxMat44 pos = physx::PxMat44::createIdentity();
		pos.setPosition(PXFROMNXVEC3(hit.worldImpact));
		m_actors.push_back(new SampleExplosionActor(*mApexExplosionAsset, *m_apexScene, scale, pos));
	}
}
#endif


SimpleTurbulenceApplication::~SimpleTurbulenceApplication(void)
{
	PX_ASSERT(!mApexModuleIOS);
	PX_ASSERT(!mApexEmitterModule);
	PX_ASSERT(!mApexIofxModule);
	PX_ASSERT(!mTurbulenceFSModule);
	PX_ASSERT(!m_lights.size());
	PX_ASSERT(!m_actors.size());
	PX_ASSERT(!mApexTurbulenceActors.size());
	PX_ASSERT(!mApexTurbulencePreview);
}

void SimpleTurbulenceApplication::benchmarkStart(const char* benchmarkName)
{
	PxU32 benchmarkNum;

	m_AppPtr->setBenchmarkEnable(true);
	if (m_AppPtr->m_OaInterface != NULL)
	{
		// get the benchmark parameters from openAutomate
		m_AppPtr->m_benchmarkNumFrames = m_AppPtr->m_OaInterface->getBenchmarkNumFrames();
		m_AppPtr->m_benchmarkNumActors = m_AppPtr->m_OaInterface->getBenchmarkNumActors();
		m_AppPtr->m_CmdLineBenchmarkMode = false;
	}
	else
	{
		m_AppPtr->m_CmdLineBenchmarkMode = true;
	}

	if (m_AppPtr->m_benchmarkNumActors > MAX_ACTORS)
	{
		m_AppPtr->m_benchmarkNumActors = MAX_ACTORS;
	}
	if (m_AppPtr->m_benchmarkNumActors < 1)
	{
		m_AppPtr->m_benchmarkNumActors = 1;
	}

	// get the benchmark duration from openAutomate
	if (m_AppPtr->m_OaInterface != NULL)
	{
		m_AppPtr->m_benchmarkNumFrames = m_AppPtr->m_OaInterface->getBenchmarkNumFrames();
	}
	m_AppPtr->m_FrameNumber = 0;
	m_AppPtr->m_BenchMarkTimer.getElapsedSeconds();
	m_AppPtr->m_BenchmarkElapsedTime = 0.0;
	m_AppPtr->m_dataCollectionStarted = true;
	for (physx::PxU32 i = 0; i < m_AppPtr->m_benchmarkNames.size(); i++)
	{
		if (physx::string::stricmp(m_AppPtr->m_benchmarkNames[i], benchmarkName) == 0)
		{
			benchmarkNum = i;
			m_AppPtr->loadAssetsAndActors((SceneSelection)benchmarkNum);
			break;
		}
	}
	oaStartBenchmark();
}

void SimpleTurbulenceApplication::unloadCurrentAssetsAndActors()
{
	clearDebugRenderConfig();

	for (physx::PxU32 i = 0; i < m_shape_actors.size(); i++)
	{
		if (m_shape_actors[i])
		{
			m_shape_actors[i]->release();
		}
	}
	m_shape_actors.clear();

	if (mRendererBoxShape != NULL)
	{
		delete mRendererBoxShape;
		mRendererBoxShape = NULL;
	}

	if (mRendererCapsuleShape != NULL)
	{
		delete mRendererCapsuleShape;
		mRendererCapsuleShape = NULL;
	}

	if (mSceneNumber == TurbulenceCollisionObjectScene || mSceneNumber == TurbulenceTrailingSmokeScene ||
		mSceneNumber == TurbulenceJetFSScene || mSceneNumber == TurbulenceAttractorFSScene ||
		mSceneNumber == TurbulenceHeatScene ||	mSceneNumber == TurbulenceWindScene 
#if NX_SDK_VERSION_MAJOR == 2
		|| mSceneNumber == TurbulenceWindFSScene || mSceneNumber == TurbulenceExplosionFSScene
#endif
		)
	{
		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i])
			{
				m_actors[i]->release();
			}
		}
		m_actors.clear();

		for (physx::PxU32 i = 0; i < mApexTurbulenceActors.size(); i++)
		{
			if (mApexTurbulenceActors[i])
			{
				mApexTurbulenceActors[i]->release();
			}
		}
		mApexTurbulenceActors.clear();

		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			releaseApexAsset(mApexEmitterAssetList[i]);
		}
		mApexEmitterAssetList.clear();

		for (physx::PxU32 i = 0; i < mApexTurbulenceFSAssetList.size(); i++)
		{
			releaseApexAsset(mApexTurbulenceFSAssetList[i]);
		}
		mApexTurbulenceFSAssetList.clear();

		for (physx::PxU32 i = 0; i < mApexFieldSamplerAssetList.size(); i++)
		{
			releaseApexAsset(mApexFieldSamplerAssetList[i]);
		}
		mApexFieldSamplerAssetList.clear();

#if NX_SDK_VERSION_MAJOR == 2
		for (physx::PxU32 i = 0; i < mApexFieldBoundaryAssetList.size(); i++)
		{
			releaseApexAsset(mApexFieldBoundaryAssetList[i]);
		}
		mApexFieldBoundaryAssetList.clear();

		if (mApexExplosionAsset)
		{
			releaseApexAsset(mApexExplosionAsset);
		}
		mApexExplosionAsset = 0;

		if (mApexWindAsset)
		{
			releaseApexAsset(mApexWindAsset);
		}
		mApexWindAsset = 0;
#endif	

		if (mApexTurbulenceAsset)
		{
			releaseApexAsset(mApexTurbulenceAsset);
		}
		mApexTurbulenceAsset = 0;

		while (mSphereHeatSource.size())
		{
			physx::PxU32 last = (physx::PxU32) mSphereHeatSource.size() - 1;
			mTurbulenceFSModule->removeHeatSource(*m_apexScene, mSphereHeatSource[last]);
			mSphereHeatSource.pop_back();
		}

		if (drivableCar)
		{
			delete drivableCar;
			drivableCar = 0;
		}
	}
	else if (mSceneNumber == AssetPreviewScene)
	{
		if (mApexTurbulencePreview)
		{
			mApexTurbulencePreview->release();
			mApexTurbulencePreview = 0;
		}

		if (mApexTurbulenceAsset)
		{
			releaseApexAsset(mApexTurbulenceAsset);
			mApexTurbulenceAsset = 0;
		}
	}
#if NX_SDK_VERSION_MAJOR == 2
	if (mSceneNumber == TurbulenceExplosionFSScene)
	{
		m_apexScene->getPhysXScene()->setGravity(NxVec3(0.0f));
	}
#endif
}

void SimpleTurbulenceApplication::loadAssetsAndActors(SceneSelection sceneNumber)
{
	mTemperature = 3.0f;
	g_userLead = false;
	if (sceneNumber >= NumScenes)
	{
		return;
	}

	mSceneNumber = sceneNumber;
	setSceneEyePos(); 	// set the eye position based on the number of actors there will be

	if (!m_apexScene || !m_apexSDK || !mApexEmitterModule)
	{
		return;
	}

	if (mSceneNumber != AssetPreviewScene
#if NX_SDK_VERSION_MAJOR == 2		
		&& mSceneNumber != TurbulenceWindFSScene && mSceneNumber != TurbulenceExplosionFSScene
#endif
		)
	{
		initCommonDebugRenderConfigs();
	}

	if (mSceneNumber == TurbulenceTrailingSmokeScene)
	{
		createTrailingSmokeScene();
	}
	else if (mSceneNumber == TurbulenceJetFSScene)
	{
		createJetFSPWScene();
	}
	else if (mSceneNumber == TurbulenceAttractorFSScene)
	{
		createAttractorFSPWScene();
	}
	else if (mSceneNumber == TurbulenceCollisionObjectScene)
	{
		createCollisionObjectScene();
		updateCollisionObjectSceneInfo();
	}
	else if (mSceneNumber == TurbulenceHeatScene)
	{
		createHeatScene();
		updateHeatSceneInfo();
	}
	else if (mSceneNumber == TurbulenceWindScene)
	{
		createWindScene();
	}
	else if (mSceneNumber == AssetPreviewScene)
	{
		mApexTurbulenceAsset = loadApexAsset("TurbulenceFSAsset", "defaultTurbulenceFS");
		if (mApexTurbulenceAsset)
		{
			NxParameterized::Interface* params = mApexTurbulenceAsset->getDefaultAssetPreviewDesc();

			physx::PxMat44 pose(physx::PxVec4(physx::PxVec3(0.7), 1)); //Scaling
			pose.setPosition(physx::PxVec3(-0.5, +2, 0.0));    //Shift
			//				pose[0][1] += 0.25; We do not need skewing

			NxParameterized::setParamMat44(*params, "globalPose", pose);
			NxParameterized::setParamBool(*params, "drawBox", true);
			NxParameterized::setParamBool(*params, "drawGrid", true);
			NxParameterized::setParamBool(*params, "drawAssetInfo", true);

			mApexTurbulencePreview = mApexTurbulenceAsset->createApexAssetPreview(*params);
		}
	}
#if NX_SDK_VERSION_MAJOR == 2
	else if (mSceneNumber == TurbulenceWindFSScene)
	{
		createWindFSScene();
	}
	else if (mSceneNumber == TurbulenceExplosionFSScene)
	{
		createExplosionFSScene();
	}
#endif
}

// called just AFTER the window opens.
void SimpleTurbulenceApplication::onInit(void)
{
	mApexCollisionShapeSpeed		= 0;
	sampleInit("SimpleTurbulence", 
		cudaSupported()    && !hasInputCommand(SCLIDS::NO_APEX_CUDA), 
		interopSupported() && !hasInputCommand(SCLIDS::NO_INTEROP),
		1024*1024*768  //gpuTotalMemLimit = 768 Mb
		);
	m_apexScene = createSampleScene(PxVec3(0, -9.81f, 0));

#if NX_SDK_VERSION_MAJOR == 2
	// For now don't try to use the 128 bit collision group

	// Set collision filter
	NxScene* scene = m_apexScene->getPhysXScene();
	scene->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_SWAP_AND);
	scene->setFilterBool(true);

	NxGroupsMask zeroMask;
	zeroMask.bits0 = zeroMask.bits1 = zeroMask.bits2 = zeroMask.bits3 = 0;
	scene->setFilterConstant0(zeroMask);
	scene->setFilterConstant1(zeroMask);

	// get rid of gravity too
	scene->setGravity(NxVec3(0.0f));
#elif NX_SDK_VERSION_MAJOR == 3
	m_apexScene->getPhysXScene()->setGravity(PxVec3(0.0f));
#endif

#if APEX_MODULES_STATIC_LINK
	instantiateModuleBasicFS();
	instantiateModuleBasicIos();
	instantiateModuleBasicIOSLegacy();
	instantiateModuleEmitter();
	instantiateModuleEmitterLegacy();
	instantiateModuleFieldSampler();
	instantiateModuleCommonLegacy();
	instantiateModuleFrameworkLegacy();
	instantiateModuleIofx();
	instantiateModuleIOFXLegacy();
	instantiateModuleTurbulenceFS();
	instantiateModuleTurbulenceFSLegacy();
# if NX_SDK_VERSION_MAJOR == 2
	instantiateModuleExplosion();
	instantiateModuleFieldBoundary();
	instantiateModuleFieldBoundaryLegacy();
	instantiateModuleWind();
# endif
#endif

#if NX_SDK_VERSION_MAJOR == 2 && ENABLE_SDK_VISUALIZATIONS
	m_physxSDK->setParameter(NX_VISUALIZATION_SCALE,        0);
	m_physxSDK->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_POSITION,   1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_PACKETS,    1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_BOUNDS,     1);
	m_physxSDK->setParameter(NX_VISUALIZE_FORCE_FIELDS,     1);
#endif

	mApexModuleIOS = static_cast<IOS_MODULE_CLASS*>(m_apexSDK->createModule(IOS_MODULE_NAME));
	PX_ASSERT(mApexModuleIOS);
	if (mApexModuleIOS)
	{
		NxParameterized::Interface* params = mApexModuleIOS->getDefaultModuleDesc();
		mApexModuleIOS->init(*params);
		mApexModuleIOS->setLODBenefitValue(1000.0f);
	}

	mApexEmitterModule = static_cast<NxModuleEmitter*>(m_apexSDK->createModule("Emitter"));
	PX_ASSERT(mApexEmitterModule);
	if (mApexEmitterModule)
	{
		NxParameterized::Interface* params = mApexEmitterModule->getDefaultModuleDesc();
		mApexEmitterModule->init(*params);
		mNbEmitterModuleScalables = mApexEmitterModule->getNbParameters();
		mEmitterModuleScalables = mApexEmitterModule->getParameters();
		for (physx::PxU32 i = 0; i < mNbEmitterModuleScalables; i++)
		{
			NxApexParameter& p = *mEmitterModuleScalables[i];
			mApexEmitterModule->setIntValue(i, p.range.maximum);
		}
	}

	mApexIofxModule = static_cast<NxModuleIofx*>(m_apexSDK->createModule("IOFX"));
	PX_ASSERT(mApexIofxModule);
	if (mApexIofxModule)
	{
		NxParameterized::Interface* params = mApexIofxModule->getDefaultModuleDesc();
		mApexIofxModule->init(*params);
	}

	mTurbulenceFSModule = static_cast<NxModuleTurbulenceFS*>(m_apexSDK->createModule("TurbulenceFS"));
	if (mTurbulenceFSModule)
	{
		NxParameterized::Interface* params = mTurbulenceFSModule->getDefaultModuleDesc();
		mTurbulenceFSModule->init(*params);

		mNbTurbulenceModuleScalables = mTurbulenceFSModule->getNbParameters();
		mTurbulenceModuleScalables = mTurbulenceFSModule->getParameters();
		for (physx::PxU32 i = 0; i < mNbTurbulenceModuleScalables; i++)
		{
			NxApexParameter& p = *mTurbulenceModuleScalables[i];
			mTurbulenceFSModule->setIntValue(i, p.range.maximum);
		}
	}

	loadModule("BasicFS");
	loadModule("BasicIOS_Legacy");
	loadModule("Emitter_Legacy");
	loadModule("Common_Legacy");
	loadModule("Framework_Legacy");
	loadModule("FieldSampler");
	loadModule("IOFX_Legacy");
	loadModule("TurbulenceFS_Legacy");
#if NX_SDK_VERSION_MAJOR == 2
	loadModule("Wind");
	loadModule("Explosion");
	loadModule("FieldBoundary");
	loadModule("FieldBoundary_Legacy");
#endif
#ifdef IOS_LEGACY_MODULE_NAME
	loadModule(IOS_LEGACY_MODULE_NAME);
#endif

	if (!mApexModuleIOS && !mApexIofxModule)
	{
		return;
	}

	if (interopSupported())
	{
		if (!m_cudaContext &&
			m_errorCallback &&
			!hasInputCommand(SCLIDS::NO_APEX_CUDA) &&
			!hasInputCommand(SCLIDS::NO_INTEROP))
		{
			char buf[256];
			sprintf(buf, "No GPU Dispatcher created, CUDA will not be used within APEX\n");
			m_errorCallback->reportError(physx::PxErrorCode::eDEBUG_INFO, buf, __FILE__, __LINE__);
		}
	}

	initializeText();

	/* Initialize TurbulenceFS Debug Rendering parameters */
	mTurbulenceFSDebugRenderParams = m_apexScene->getModuleDebugRenderParams("TurbulenceFS");
	PX_ASSERT(mTurbulenceFSDebugRenderParams);

	NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_FIELD_SCALE",
		0.2f);

	NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
		"VISUALIZE_TURBULENCE_FS_PLANE",
		false);

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_PLANE.normal",
		PxVec3(normalA, normalB, normalC));

	NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_PLANE.offset",
		default_planeOffset);

	m_resourceCallback->setApexSupport(*m_apexSDK);

	initializeApexCollisionNames();

	if (mApexIofxModule)
	{
		physx::PxBounds3 b;
		b.setInfinite();
		mRenderVolumeList.push_back(mApexIofxModule->createRenderVolume(*m_apexScene, b, 0, true));
		//mApexIofxModule->disableCudaModifiers();
	}

	mRenderMeshContext.material         = m_simpleLitMaterial->getMaterial();
	mRenderMeshContext.materialInstance = m_simpleLitMaterial->getMaterialInstance();

	// NOTE: Only one light here for now, the second light halved the framerate.
	SampleRenderer::RendererDirectionalLightDesc lightdesc;

	lightdesc.color     = SampleRenderer::RendererColor(255, 255, 255, 255);
	lightdesc.intensity = 0.5;
	lightdesc.direction = physx::PxVec3(0, -0.707f, -0.707f);
	m_lights.push_back(getRenderer()->createLight(lightdesc));

	getRenderer()->setClearColor(SampleRenderer::RendererColor(150, 150, 150, 255));
	getRenderer()->setAmbientColor(SampleRenderer::RendererColor(100, 100, 100, 255));

	std::string benchmarkName;
	for (physx::PxU32 i = 0; i < m_benchmarkNames.size(); i++)
	{
		if (m_cmdline.hasSwitch(m_benchmarkNames[i]))
		{
			if (m_CmdLineBenchmarkMode == false)
			{
				// only 1 benchmark can be run from the command line.
				// OA mode can command sequential bench marks...
				benchmarkName = m_benchmarkNames[i];
			}
		}
	}

	if (benchmarkName.size())
	{
		benchmarkStart(benchmarkName.c_str());
	}
	else
	{
		loadAssetsAndActors(mSceneNumber);
	}

	m_apexScene->allocViewMatrix(ViewMatrixType::LOOK_AT_RH);
	m_apexScene->allocProjMatrix(ProjMatrixType::USER_CUSTOMIZED);
}

// called just BEFORE the window closes. return 'true' to confirm the window closure.
void SimpleTurbulenceApplication::onShutdown(void)
{
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
	PX_ASSERT(assetManager);
	PX_FORCE_PARAMETER_REFERENCE(assetManager);

	for (physx::PxU32 i = 0; i < m_lights.size(); i++)
	{
		m_lights[i]->release();
	}
	m_lights.clear();

	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i])
		{
			m_actors[i]->release();
		}
	}
	m_actors.clear();

	unloadCurrentAssetsAndActors();

	if (mApexTurbulencePreview)
	{
		mApexTurbulencePreview->release();
		mApexTurbulencePreview = 0;
	}

	for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
	{
		releaseApexAsset(mApexEmitterAssetList[i]);
	}
	mApexEmitterAssetList.clear();

	for (physx::PxU32 i = 0; i < mApexTurbulenceFSAssetList.size(); i++)
	{
		releaseApexAsset(mApexTurbulenceFSAssetList[i]);
	}
	mApexTurbulenceFSAssetList.clear();

	for (physx::PxU32 i = 0; i < mApexFieldSamplerAssetList.size(); i++)
	{
		releaseApexAsset(mApexFieldSamplerAssetList[i]);
	}
	mApexFieldSamplerAssetList.clear();

#if NX_SDK_VERSION_MAJOR == 2
	for (physx::PxU32 i = 0; i < mApexFieldBoundaryAssetList.size(); i++)
	{
		releaseApexAsset(mApexFieldBoundaryAssetList[i]);
	}
	mApexFieldBoundaryAssetList.clear();

	if (mApexExplosionAsset)
	{
		releaseApexAsset(mApexExplosionAsset);
		mApexExplosionAsset = 0;
	}	
	if (mApexWindAsset)
	{
		releaseApexAsset(mApexWindAsset);
		mApexWindAsset = 0;
	}
#endif

	if (mApexTurbulenceAsset)
	{
		releaseApexAsset(mApexTurbulenceAsset);
		mApexTurbulenceAsset = 0;
	}

	if (mApexEmitterModule)
	{
		m_apexSDK->releaseModule(mApexEmitterModule);
		mApexEmitterModule = 0;
	}

	if (mRenderVolumeList.size())
	{
		for (physx::PxU32 i = 0; i < mRenderVolumeList.size(); i++)
		{
			if (mRenderVolumeList[i])
			{
				mApexIofxModule->releaseRenderVolume(*mRenderVolumeList[i]);
				mRenderVolumeList[i] = 0;
			}
		}
		mRenderVolumeList.clear();
	}

	while (mSphereHeatSource.size())
	{
		physx::PxU32 last = (physx::PxU32) mSphereHeatSource.size() - 1;
		mTurbulenceFSModule->removeHeatSource(*m_apexScene, mSphereHeatSource[last]);
		mSphereHeatSource.pop_back();
	}

	if (mApexIofxModule)
	{
		m_apexSDK->releaseModule(mApexIofxModule);
		mApexIofxModule = 0;
	}
	if (mApexModuleIOS)
	{
		m_apexSDK->releaseModule(mApexModuleIOS);
		mApexModuleIOS = 0;
	}

	if (mTurbulenceFSModule)
	{
		m_apexSDK->releaseModule(mTurbulenceFSModule);
		mTurbulenceFSModule = 0;
	}

	SampleApexApplication::onShutdown();
}

void SimpleTurbulenceApplication::onTickPreRender(float dtime)
{
	if (m_apexScene && !m_pause && ((m_OaInterface == NULL)||m_OaInterface->getOaState()!=OAState::IDLE))
	{
		if (mSceneNumber == TurbulenceTrailingSmokeScene)
		{
			updateTurbulenceTrailingSmokeScene(dtime);
		}
		else if (mSceneNumber == TurbulenceJetFSScene)
		{
			updateTurbulenceJetFSPWScene(dtime);
		}
		else if (mSceneNumber == TurbulenceAttractorFSScene)
		{
			updateTurbulenceAttractorFSPWScene(dtime);
		}
		else if (mSceneNumber == TurbulenceCollisionObjectScene)
		{
			updateTurbulenceCollisionScene(dtime);
		}
		else if (mSceneNumber == TurbulenceHeatScene)
		{
			updateTurbulenceHeatScene(dtime);
		}
		else if (mSceneNumber == TurbulenceWindScene)
		{
			updateTurbulenceWindScene(dtime);
		}
#if NX_SDK_VERSION_MAJOR == 2
		else if (mSceneNumber == TurbulenceWindFSScene)
		{
			updateTurbulenceWindFSScene(dtime);
		}
		else if (mSceneNumber == TurbulenceExplosionFSScene)
		{
			updateTurbulenceExplosionFSScene(dtime);
		}
#endif
		for (PxU32 i = 0; i < m_shape_actors.size(); i++)
		{
			m_shape_actors[i]->tick(dtime);
		}

		//m_apexScene->simulate(1.0f/60.0f);
		m_apexScene->simulate(dtime);
	}

	SampleApexApplication::onTickPreRender(dtime);
}

// called when the window's contents needs to be redrawn.
void SimpleTurbulenceApplication::onRender(void)
{
	updateScalableAndLodInfo();

	SampleRenderer::Renderer* renderer = getRenderer();
	if (renderer)
	{
		physx::PxU32 windowWidth  = 0;
		physx::PxU32 windowHeight = 0;
		renderer->getWindowSize(windowWidth, windowHeight);
		if (windowWidth > 0 && windowHeight > 0)
		{
			renderer->clearBuffers();

			m_projection = SampleRenderer::RendererProjection(45.0f, windowWidth / (float)windowHeight, 0.1f, 10000.0f);

			// The APEX scene needs the view and projection matrices for LOD, IOFX modifiers, and other things
			updateApexSceneMatrices();

			for (physx::PxU32 i = 0; i < m_lights.size(); i++)
			{
				renderer->queueLightForRender(*m_lights[i]);
			}

			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i])
				{
					m_actors[i]->render();
				}
			}

			for (PxU32 i = 0; i < m_shape_actors.size(); i++)
			{
				if (m_shape_actors[i])
				{
					m_shape_actors[i]->render();
				}
			}

			if (mSceneNumber == TurbulenceJetFSScene || mSceneNumber == TurbulenceAttractorFSScene)
			{
				//Weimin: Do wind and explosion scenes have to be included here?
				renderer->queueMeshForRender(mRenderMeshContext);
			}

			if (mApexTurbulencePreview)
			{
				SampleApexRenderer apexRenderer;
				mApexTurbulencePreview->lockRenderResources();
				mApexTurbulencePreview->updateRenderResources();
				mApexTurbulencePreview->dispatchRenderResources(apexRenderer);
				mApexTurbulencePreview->unlockRenderResources();
			}

			if (m_apexScene)
			{
				SampleApexRenderer apexRenderer;

				if (m_apexRenderDebug)
				{
					m_apexRenderDebug->lockRenderResources();
					m_apexRenderDebug->updateRenderResources(0);
					m_apexRenderDebug->unlockRenderResources();
					m_apexRenderDebug->dispatchRenderResources(apexRenderer);
				}

				m_apexScene->lockRenderResources();
				m_apexScene->updateRenderResources();
				m_apexScene->dispatchRenderResources(apexRenderer);
				m_apexScene->unlockRenderResources();
			}

			if (mApexIofxModule && m_apexScene && mRenderParticles)
			{
				m_apexScene->prepareRenderResourceContexts();

				SampleApexRenderer apexRenderer;
				NxApexRenderableIterator* iter = mApexIofxModule->createRenderableIterator(*m_apexScene);
				for (NxApexRenderable* r = iter->getFirst(); r ; r = iter->getNext())
				{
					r->updateRenderResources(NULL);
					r->dispatchRenderResources(apexRenderer);
				}
				iter->release();
			}

			renderer->render(getEyeTransform(), m_projection);

			///////////////////////////////////////////////////////////////////////////

			std::string sceneInfo;
			for (int i = WINDOW_SCENE_INFO_INDEX; i < NUM_WINDOW_STRINGS; ++i)
			{
				if (!empty(mWindowString[i]))
				{
					sceneInfo.append(mWindowString[i]);
					sceneInfo.append("\n");
				}
			}
			printSceneInfo(mWindowString[WINDOW_SCENE_NAME_INDEX], sceneInfo.c_str());
		}
	}
}

void SimpleTurbulenceApplication::onTickPostRender(float dtime)
{
	dumpApexStats(m_apexScene);
	if (m_apexScene && !m_pause)
	{
		physx::PxU32 errorState = 0;
		m_apexScene->fetchResults(true, &errorState);

#if NX_SDK_VERSION_MAJOR == 2
		const NxDebugRenderable* debugRenderable = m_apexScene->getPhysXScene()->getDebugRenderable();
		if (debugRenderable && m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(*debugRenderable);
		}
#elif NX_SDK_VERSION_MAJOR == 3
		const physx::PxRenderBuffer& renderBuffer = m_apexScene->getPhysXScene()->getRenderBuffer();
		if (m_apexRenderDebug != NULL)
		{
			m_apexRenderDebug->addDebugRenderable(renderBuffer);
		}
#endif
	}

	// call the Open Automate handler...
	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::IDLE))
	{
		m_OaInterface->OAHandler();
	}

	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::EXIT))
	{
		close();
	}

	//do OA benchmarking stuff
	if (m_BenchmarkMode)
	{
		if (m_FrameNumber <= m_benchmarkNumFrames)
		{
			SampleApexApplication::reportOaResults();
			m_FrameNumber++;
		}
		else
		{
			if (m_CmdLineBenchmarkMode)
			{
				close();
			}
			else
			{
				if (m_OaInterface->getOaState() == OAState::BENCHMARKING)
				{
					//done with the benchmark
					oaValue v;
					v.Float = m_BenchmarkElapsedTime;
					m_OaInterface->setOaState(OAState::IDLE);
					oaAddResultValue("Total run time", OA_TYPE_FLOAT, &v);
					m_OaInterface->endBenchmark();
					// release the actors created by this benchmark in case OA runs another benchmark without exiting!
					for (PxU32 i = 0; i < m_actors.size(); i++)
					{
						m_actors[i]->release();
					}
					m_actors.clear();
				}
			}
		}
	}

	SampleApexApplication::onTickPostRender(dtime);
}

void SimpleTurbulenceApplication::collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
{
	using namespace SampleFramework;

	std::vector<const char*> inputDescriptions;
	collectInputDescriptions(inputDescriptions);

	//digital keyboard events
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_0,				WKEY_0,			XKEY_0,			PS3KEY_0,		AKEY_UNKNOWN,	OSXKEY_0,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_0);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_1,				WKEY_1,			XKEY_1,			PS3KEY_1,		ABUTTON_2,		OSXKEY_1,		PSP2KEY_UNKNOWN,	IBUTTON_2,		LINUXKEY_1);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_2,				WKEY_2,			XKEY_2,			PS3KEY_2,		ABUTTON_1,		OSXKEY_2,		PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_2);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_3,				WKEY_3,			XKEY_3,			PS3KEY_3,		ABUTTON_3,		OSXKEY_3,		PSP2KEY_UNKNOWN,	IBUTTON_3,		LINUXKEY_3);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_4,				WKEY_4,			XKEY_4,			PS3KEY_4,		ABUTTON_4,		OSXKEY_4,		PSP2KEY_UNKNOWN,	IBUTTON_4,		LINUXKEY_4);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_5,				WKEY_5,			XKEY_5,			PS3KEY_5,		ABUTTON_5,		OSXKEY_5,		PSP2KEY_UNKNOWN,	IBUTTON_5,		LINUXKEY_5);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_6,				WKEY_6,			XKEY_6,			PS3KEY_6,		ABUTTON_6,		OSXKEY_6,		PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_6);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_7,				WKEY_7,			XKEY_7,			PS3KEY_7,		ABUTTON_7,		OSXKEY_7,		PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_7);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_8,				WKEY_8,			XKEY_8,			PS3KEY_8,		ABUTTON_8,		OSXKEY_8,		PSP2KEY_UNKNOWN,	IBUTTON_8,		LINUXKEY_8);

	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_SIM_VEL,		WKEY_6,			XKEY_6,			PS3KEY_6,		ABUTTON_6,		OSXKEY_6,		PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_6);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_SIM_VEL,		WKEY_7,			XKEY_7,			PS3KEY_7,		ABUTTON_7,		OSXKEY_7,		PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_7);

	DIGITAL_INPUT_EVENT_DEF_2(CONTROL_FORWARD,		WKEY_I,			XKEY_I,			PS3KEY_I,		AKEY_UNKNOWN,	OSXKEY_I,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_I);
	DIGITAL_INPUT_EVENT_DEF_2(CONTROL_BACK,			WKEY_K,			XKEY_K,			PS3KEY_K,		AKEY_UNKNOWN,	OSXKEY_K,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_K);
	DIGITAL_INPUT_EVENT_DEF_2(CONTROL_RIGHT,		WKEY_L,			XKEY_L,			PS3KEY_L,		AKEY_UNKNOWN,	OSXKEY_L,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_L);
	DIGITAL_INPUT_EVENT_DEF_2(CONTROL_LEFT,			WKEY_J,			XKEY_J,			PS3KEY_J,		AKEY_UNKNOWN,	OSXKEY_J,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_J);
	DIGITAL_INPUT_EVENT_DEF_2(CONTROL_UP,			WKEY_U,			XKEY_U,			PS3KEY_U,		AKEY_UNKNOWN,	OSXKEY_U,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_U);
	DIGITAL_INPUT_EVENT_DEF_2(CONTROL_DOWN,			WKEY_Y,			XKEY_Y,			PS3KEY_Y,		AKEY_UNKNOWN,	OSXKEY_Y,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Y);

	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_SIM_LOD,		WKEY_SUBTRACT,	XKEY_SUBTRACT,	PS3KEY_SUBTRACT, AKEY_UNKNOWN,	OSXKEY_SUBTRACT, PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_SUBSTRACT);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_SIM_LOD,		WKEY_ADD,		XKEY_ADD,		PS3KEY_ADD,		AKEY_UNKNOWN,	OSXKEY_ADD,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_ADD);

	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_SIM_SCALE,	WKEY_DIVIDE,	XKEY_DIVIDE,	PS3KEY_DIVIDE,	AKEY_UNKNOWN,	OSXKEY_DIVIDE,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_DIVIDE);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_SIM_SCALE,	WKEY_MULTIPLY,	XKEY_MULTIPLY,	PS3KEY_MULTIPLY, AKEY_UNKNOWN,	OSXKEY_MULTIPLY, PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_MULTIPLY);

	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_TEMPERATURE,	WKEY_DIVIDE,	XKEY_DIVIDE,	PS3KEY_DIVIDE,	AKEY_UNKNOWN,	OSXKEY_DIVIDE,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_DIVIDE);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_TEMPERATURE,	WKEY_MULTIPLY,	XKEY_MULTIPLY,	PS3KEY_MULTIPLY, AKEY_UNKNOWN,	OSXKEY_MULTIPLY, PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_MULTIPLY);

	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_WIND_HEAT_COL,	WKEY_B,			XKEY_B,			PS3KEY_B,		AKEY_UNKNOWN,	OSXKEY_B,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_B);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_HEAT_VIZ,		WKEY_T,			XKEY_T,			PS3KEY_T,		AKEY_UNKNOWN,	OSXKEY_T,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_T);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_PARTICLES_VIZ,	WKEY_Q,			XKEY_Q,			PS3KEY_Q,		AKEY_UNKNOWN,	OSXKEY_Q,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Q);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_GRID_VIZ,		WKEY_X,			XKEY_X,			PS3KEY_X,		AKEY_UNKNOWN,	OSXKEY_X,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_X);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_PLANE_VIZ,		WKEY_Z,			XKEY_Z,			PS3KEY_Z,		AKEY_UNKNOWN,	OSXKEY_Z,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Z);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_LOD_VIZ,		WKEY_N,			XKEY_N,			PS3KEY_N,		AKEY_UNKNOWN,	OSXKEY_N,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_N);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_FIELD_VIZ,		WKEY_H,			XKEY_H,			PS3KEY_H,		AKEY_UNKNOWN,	OSXKEY_H,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_M);

	DIGITAL_INPUT_EVENT_DEF_2(RESET_SIM,			WKEY_R,			XKEY_R,			PS3KEY_R,		AKEY_UNKNOWN,	OSXKEY_R,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_R);

	SampleApexApplication::collectInputEvents(inputEvents);
}

void SimpleTurbulenceApplication::collectInputDescriptions(std::vector<const char*>& inputDescriptions)
{
	SampleApexApplication::collectInputDescriptions(inputDescriptions);
	inputDescriptions.insert(inputDescriptions.end(), SampleTurbulenceInputEventDescriptions, SampleTurbulenceInputEventDescriptions + PX_ARRAY_SIZE(SampleTurbulenceInputEventDescriptions));
}

void SimpleTurbulenceApplication::onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy)
{
	// if a benchmark is running ignore mouse events.
	if (m_BenchmarkMode)
	{
		return;
	}

	SampleApexApplication::onPointerInputEvent(ie, x, y, dx, dy);

	if (isInputEventActive(MODIFIER_SHIFT) && g_visualizePlane)
	{
		mPlaneOffset += (dx / 1000);
		NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_PLANE.offset",
		mPlaneOffset);
	}
	if(mPlaneOffset < -10 || mPlaneOffset > 10) mPlaneOffset = 0;

	if (isInputEventActive(MODIFIER_CONTROL) && g_visualizePlane)
	{
		PxReal alpha;
		alpha = atan(mPlaneY / mPlaneX);
		alpha += (dx / 1000);
		PxReal t = (mPlaneX * tan(alpha) - mPlaneY) / (1 + tan(alpha));
		mPlaneY += t;
		mPlaneX -= t;

		alpha = atan(mPlaneY / mPlaneZ);
		alpha += (dy / 1000);
		t = (mPlaneZ * tan(alpha) - mPlaneY) / (1 + tan(alpha));
		mPlaneY += t;
		mPlaneZ -= t;

		NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
			"TURBULENCE_FS_PLANE.normal",
			PxVec3(mPlaneX, mPlaneY, mPlaneZ));
	}

}

bool SimpleTurbulenceApplication::onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
{
	// if a benchmark is running ignore the key board.
	if (m_BenchmarkMode)
	{
		return true;
	}

	SampleApexApplication::onDigitalInputEvent(ie, val);

	updateInputsForMovement(ie, val);

	if (val)
	{
		switch (ie.m_Id)
		{
		case SCENE_SELECT:
			{
				physx::PxU32 sceneNumber = (mSceneNumber + 1) % (physx::PxU32)NumScenes;
				unloadCurrentAssetsAndActors();
				loadAssetsAndActors(static_cast<SceneSelection>(sceneNumber));
				initializeText();
			}
			break;
		case SCENE_0:
		case SCENE_1:
		case SCENE_2:
		case SCENE_3:
		case SCENE_4:
		case SCENE_5:
		case SCENE_6:
		case SCENE_7:
#if NX_SDK_VERSION_MAJOR == 2
		case SCENE_8:
		case SCENE_9:
#endif
			if (!isInputEventActive(MODIFIER_SHIFT))
			{
				physx::PxU32 index = (ie.m_Id - SCENE_0);
				physx::PxU32 scene = physx::PxMin(index, (physx::PxU32)NumScenes - 1);
				unloadCurrentAssetsAndActors();
				loadAssetsAndActors((SceneSelection)scene);
				initializeText();
			}
			break;
		case INCREASE_SIM_LOD:
			{
				if (mSceneNumber == TurbulenceTrailingSmokeScene)
				{
					m_simulationBudget += 1000;
					printf("resource budget: %f !\n", m_simulationBudget);
					m_apexScene->setLODResourceBudget(m_simulationBudget);
					updateScalableAndLodInfo();
					NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_LOD",
						m_simulationBudget);
				}
				else if (mSceneNumber == TurbulenceCollisionObjectScene || mSceneNumber == TurbulenceHeatScene)
				{
					m_collisionObjRadius += 0.1f;
					updateHeatSceneInfo();
				}
			}
			break;
		case DECREASE_SIM_LOD:
			{
				if (mSceneNumber == TurbulenceTrailingSmokeScene)
				{
					if (m_simulationBudget >= 1000)
					{
						m_simulationBudget -= 1000;
					}
					printf("resource budget: %f !\n", m_simulationBudget);
					m_apexScene->setLODResourceBudget(m_simulationBudget);
					updateScalableAndLodInfo();
					NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_LOD",
						m_simulationBudget);
				}
				else if (mSceneNumber == TurbulenceCollisionObjectScene || mSceneNumber == TurbulenceHeatScene)
				{
					m_collisionObjRadius -= 0.1f;
					if (m_collisionObjRadius < 0.1)
					{
						m_collisionObjRadius = 0.1f;
					}
					updateHeatSceneInfo();
				}
			}
		case INCREASE_TEMPERATURE:
			{		
				if (mSceneNumber == TurbulenceHeatScene)
				{
					if (isInputEventActive(MODIFIER_SHIFT))
					{
						mTemperature += 0.5f;
						if (mTemperature > 1000.0)
						{
							mTemperature = 500.0f;
						}
						updateHeatSceneInfo();
					}
				}
			}
			break;
		case DECREASE_TEMPERATURE:
			{
				if (mSceneNumber == TurbulenceHeatScene)
				{
					if (isInputEventActive(MODIFIER_SHIFT))
					{
						mTemperature -= 0.5f;
						if (mTemperature < 0.0)
						{
							mTemperature = 0.0f;
						}
						updateHeatSceneInfo();
					}
				}
			}
			break;
		case INCREASE_SIM_SCALE:// || (ie.m_Id == SCENE_8 && isKeyDown(MODIFIER_SHIFT)))
			{
				if (!isInputEventActive(MODIFIER_SHIFT))
				{
					for (physx::PxU32 i = 0; i < mNbEmitterModuleScalables; i++)
					{
						NxApexParameter& p = *mEmitterModuleScalables[i];
						physx::PxU32 newValue = physx::PxMin<physx::PxU32>(p.range.maximum, p.current + 1);
						mApexEmitterModule->setIntValue(i, newValue);
					}
					updateScalableAndLodInfo();
				}
			}
			break;
		case DECREASE_SIM_SCALE:
			{
				if (!isInputEventActive(MODIFIER_SHIFT))
				{
					for (physx::PxU32 i = 0; i < mNbEmitterModuleScalables; i++)
					{
						NxApexParameter& p = *mEmitterModuleScalables[i];
						physx::PxU32 newValue = physx::PxMax<physx::PxU32>(p.range.minimum, p.current - 1);
						mApexEmitterModule->setIntValue(i, newValue);
					}
					updateScalableAndLodInfo();
				}
			}
			break;
		case DECREASE_SIM_VEL:
			if (isInputEventActive(MODIFIER_SHIFT))
			{
				if (mSceneNumber == TurbulenceTrailingSmokeScene)
				{
					for (physx::PxU32 i = 0; i < mNbTurbulenceModuleScalables; i++)
					{
						NxApexParameter& p = *mTurbulenceModuleScalables[i];
						physx::PxU32 newValue = physx::PxMin<physx::PxU32>(p.range.maximum, p.current - 1);
						mTurbulenceFSModule->setIntValue(i, newValue);
					}
					updateScalableAndLodInfo();
				}
				else if (mSceneNumber == TurbulenceCollisionObjectScene)
				{
					mVelocityClamp -= 0.1f;
					if (mVelocityClamp < 0.0)
					{
						mVelocityClamp = 0.0f;
					}
					for (physx::PxU32 i = 0; i < mApexTurbulenceActors.size(); i++)
					{
						if (mApexTurbulenceActors[i])
						{
							mApexTurbulenceActors[i]->mActor->setAngularVelocityMultiplierAndClamp(1.0f, mVelocityClamp);
							mApexTurbulenceActors[i]->mActor->setLinearVelocityMultiplierAndClamp(1.0f, mVelocityClamp);
						}
					}
				}
			}
			break;
		case INCREASE_SIM_VEL:
			if (isInputEventActive(MODIFIER_SHIFT))
			{
				if (mSceneNumber == TurbulenceTrailingSmokeScene)
				{
					for (physx::PxU32 i = 0; i < mNbTurbulenceModuleScalables; i++)
					{
						NxApexParameter& p = *mTurbulenceModuleScalables[i];
						physx::PxU32 newValue = physx::PxMax<physx::PxU32>(p.range.minimum, p.current + 1);
						mTurbulenceFSModule->setIntValue(i, newValue);
					}
					updateScalableAndLodInfo();
				}
				else if (mSceneNumber == TurbulenceCollisionObjectScene)
				{
					mVelocityClamp += 0.1f;
					for (physx::PxU32 i = 0; i < mApexTurbulenceActors.size(); i++)
					{
						if (mApexTurbulenceActors[i])
						{
							mApexTurbulenceActors[i]->mActor->setAngularVelocityMultiplierAndClamp(1.0f, mVelocityClamp);
							mApexTurbulenceActors[i]->mActor->setLinearVelocityMultiplierAndClamp(1.0f, mVelocityClamp);
						}
					}
				}
			}
			break;
		case TOGGLE_WIND_HEAT_COL:
			{
				if (mSceneNumber == TurbulenceWindScene)
				{
					g_applyWind = !g_applyWind;
				}
				else if (mSceneNumber == TurbulenceCollisionObjectScene || mSceneNumber == TurbulenceHeatScene) 
				{
					++mRotationType;
					if(mRotationType == 4) mRotationType = 0;
				}
			}
			break;
		case TOGGLE_HEAT_VIZ:
			{
				g_visualizeHeat = !g_visualizeHeat;
				NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
					"VISUALIZE_TURBULENCE_FS_TEMPERATURE",
					g_visualizeHeat);
			}
			break;
		case RESET_SIM:
			{
				// TODO: SJB - make this a standard key
				m_pause = true;

				unloadCurrentAssetsAndActors();
				loadAssetsAndActors((SceneSelection)mSceneNumber);
				mRotationType = 0;
				m_pause = false;
			}
			break;
		case TOGGLE_PARTICLES_VIZ:
			{
				//toggle rendering particles
				mRenderParticles = !mRenderParticles;
			}
			break;
		case TOGGLE_PLANE_VIZ:
			{
				++mPlaneSwitch;
				if(mPlaneSwitch == 1)
				{
					g_visualizePlane = !g_visualizePlane;
					NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
						"VISUALIZE_TURBULENCE_FS_PLANE",
						g_visualizePlane);
					mPlaneX = normalA;
					mPlaneY = normalB;
					mPlaneZ = normalC;
					NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.normal",
						PxVec3(normalA, normalB, normalC));
					NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.offset",
						default_planeOffset);
				}
				else if(mPlaneSwitch == 2)
				{
					mPlaneX = 1.0f;
					mPlaneY = 0.0f;
					mPlaneZ = 0.0f;
					NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.normal",
						PxVec3(mPlaneX, mPlaneY, mPlaneZ));
					mPlaneOffset = 0.f;
					NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.offset",
						mPlaneOffset);
				}
				else if(mPlaneSwitch == 3)
				{
					mPlaneX = 0.0f;
					mPlaneY = 1.0f;
					mPlaneZ = 0.0f;
					NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.normal",
						PxVec3(mPlaneX, mPlaneY, mPlaneZ));
					mPlaneOffset = 0.f;
					NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.offset",
						mPlaneOffset);
				}
				else if(mPlaneSwitch == 4)
				{
					mPlaneX = 0.0f;
					mPlaneY = 0.0f;
					mPlaneZ = 1.0f;
					NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.normal",
						PxVec3(mPlaneX, mPlaneY, mPlaneZ));
					mPlaneOffset = 0.f;
					NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
						"TURBULENCE_FS_PLANE.offset",
						mPlaneOffset);
				}
				else if(mPlaneSwitch == 5)
				{
					mPlaneSwitch = 0;
					g_visualizePlane = !g_visualizePlane;
					NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
						"VISUALIZE_TURBULENCE_FS_PLANE",
						g_visualizePlane);
				}
			}
			break;
		case TOGGLE_GRID_VIZ:
			{
				g_visualizeGrid = !g_visualizeGrid;
				NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
					"VISUALIZE_TURBULENCE_FS_GRID",
					g_visualizeGrid);
			}
			break;
		case TOGGLE_LOD_VIZ:
			{
				g_visualizeLOD = !g_visualizeLOD;
				NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
					"VISUALIZE_TURBULENCE_FS_LOD",
					g_visualizeLOD);
			}
			break;
		case TOGGLE_FIELD_VIZ:
			{
				g_visualizeField = !g_visualizeField;
				NxParameterized::setParamBool(*mTurbulenceFSDebugRenderParams,
					"VISUALIZE_TURBULENCE_FS_FIELD",
					g_visualizeField);

			}
			break;
		default:
			break;
		};
	}
	return true;
}

void SimpleTurbulenceApplication::initializeApexCollisionNames()
{
#if NX_SDK_VERSION_MAJOR == 2
	// setup collision groups for wind
	m_resourceCallback->registerNxCollisionGroup("CollisionPrevailingAndGustsGroupName", 10);
	m_resourceCallback->registerNxCollisionGroup("CollisionDragAndFlutterGroupName", 10);
	m_resourceCallback->registerNxCollisionGroupsMask("gustRaycastMaskName", PX_MAX_U32);
#endif
}

void SimpleTurbulenceApplication::createCollisionObjectScene()
{
	//this scene demonstrates multiple collision objects interacting with the turbulence simulation
	//it contains:
	//one grid that remains static (with no collision object in it)
	//one emitter that remains static
	//the user can throw collision boxes at the scene and they interact with the turbulence simulation
	mApexCollisionShapeSpeed		= 3.5f * m_benchmarkNumActors;

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_STREAMLINES.grid",
		PxVec3(3, 3, 3));

	//create the turbulence objects
	mApexTurbulenceAsset = loadApexAsset("TurbulenceFSAsset", "collisionSceneTurbulenceFS");
	//create the emitter objects
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "collisionSceneEmitter" IOS_STRING)));

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();

	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, j, m_benchmarkNumActors, TurbulenceCollisionObjectScene));
			}
		}

		//turbulence
		if (mApexTurbulenceAsset)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				mApexTurbulenceActors.push_back(new SampleTurbulenceFSActor(*mApexTurbulenceAsset, *m_apexScene, j, m_benchmarkNumActors, TurbulenceCollisionObjectScene));
				NxTurbulenceFSActor* actor = mApexTurbulenceActors[j]->mActor;
				actor->setAngularVelocityMultiplierAndClamp(1.0f, 2.5);
				actor->setLinearVelocityMultiplierAndClamp(1.0f, 2.5);
			}
		}

		PxVec3	position(0.0, 0.0, 0.0);
	    PxVec3	velocity(mApexCollisionShapeSpeed, 0.0, 0.0);
	    PxF32	density = 100.0;

		PxVec3 verts[16];// = {PxVec3(2.0f, 2.0f, 2.0f), PxVec3(0.0f, 2.0f, 2.0f), PxVec3(2.0f, 0.0f, 2.0f), PxVec3(2.0f, 2.0f, 0.0f), PxVec3(0.0f, 2.0f, 0.0f), PxVec3(2.0f, 0.0f, 0.0f), PxVec3(0.0f, 0.0f, 2.0f), PxVec3(0.0f, 0.0f, 0.0f)};
		PxU32 nbVerts = 16;

		//fill Vertex Buffer
		for (PxU32 vi = 0; vi < 16; ++vi)
		{
			verts[vi].x = (vi % 2) ? ((vi & 0x1) ? +0.5f : -0.5f) : ((vi & 0x1) ? +1.0f : -1.0f);
			verts[vi].y = (vi % 3) ? ((vi & 0x2) ? +0.5f : -0.5f) : ((vi & 0x2) ? +1.0f : -1.0f);
			verts[vi].z = (vi & 0x4) ? +1.0f : -1.0f;
		}

		/*for (PxU32 vi = 9; vi < 16; ++vi)
		{
			verts[vi].x = (vi & 0x1) ? +2.0f : -2.0f;
			verts[vi].y = (vi & 0x2) ? +2.0f : -2.0f;
			verts[vi].z = (vi & 0x4) ? +2.0f : -2.0f;
		}*/

#if TEST_CONVEX
		//create the collision convex mesh and add it to the turbulence module
#if NX_SDK_VERSION_MAJOR == 2
		mApexMeshCollisionShape = new SampleConvexMeshActor(getRenderer(), 
														*m_simpleLitMaterial,
														*(m_apexScene->getPhysXScene()),
														verts,
														nbVerts,
														position,
														velocity,
														density,
														NULL,
														true);
#elif NX_SDK_VERSION_MAJOR == 3
		mApexMeshCollisionShape = new SampleConvexMeshActor(getRenderer(), 
														*m_simpleLitMaterial,
														*(m_apexScene->getPhysXScene()),
														verts,
														nbVerts,
														position,
														velocity,
														density,
														m_material,
														true);
#endif

		if (mApexMeshCollisionShape)
		{
			m_shape_actors.push_back(mApexMeshCollisionShape);
			collisionObjectAdded(*m_apexScene, *mApexMeshCollisionShape);
		}
#if NX_SDK_VERSION_MAJOR == 2
		NxActor* physxActor;
		physxActor = mApexMeshCollisionShape->getPhysXActor();
		physxActor->setAngularVelocity(NxVec3(mApexCollisionShapeSpeed, 0.0, 0.0));
#elif NX_SDK_VERSION_MAJOR == 3
		physx::PxRigidDynamic* physxActor;
		physxActor = static_cast<physx::PxRigidDynamic*>(mApexMeshCollisionShape->getPhysXActor());
		physxActor->setAngularVelocity(PxVec3(mApexCollisionShapeSpeed, 0.0, 0.0));
#endif
#else
		//create the collision sphere and add it to the turbulence module
#if NX_SDK_VERSION_MAJOR == 2
		mApexSphereCollisionShape = new SampleSphereActor(getRenderer(), 
														*m_simpleLitMaterial,
														*(m_apexScene->getPhysXScene()),
														position,
														velocity,
														PxVec3(1.0f, 1.0f, 1.0f),
														density,
														NULL,
														true);
#elif NX_SDK_VERSION_MAJOR == 3 
		mApexSphereCollisionShape = new SampleSphereActor(getRenderer(), 
														*m_simpleLitMaterial,
														*(m_apexScene->getPhysXScene()),
														position,
														velocity,
														PxVec3(1.0f, 1.0f, 1.0f),
														density,
														m_material,
														true);
#endif

		if (mApexSphereCollisionShape)
		{
			m_shape_actors.push_back(mApexSphereCollisionShape);
			collisionObjectAdded(*m_apexScene, *mApexSphereCollisionShape);
		}

#if NX_SDK_VERSION_MAJOR == 2
		NxActor* physxActor;
		physxActor = mApexSphereCollisionShape->getPhysXActor();
		physxActor->setAngularVelocity(NxVec3(mApexCollisionShapeSpeed, 0, 0));
#elif NX_SDK_VERSION_MAJOR == 3
		physx::PxRigidDynamic* physxActor;
		physxActor = static_cast<physx::PxRigidDynamic*>(mApexSphereCollisionShape->getPhysXActor());
		physxActor->setAngularVelocity(PxVec3(mApexCollisionShapeSpeed, 0, 0));
#endif

#endif
		addCollisionShape(HalfSpaceShapeType, PxVec3(0, -4, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
	}
}


void SimpleTurbulenceApplication::createJetFSPWScene()
{
	//this scene demonstrates a "prop wash effect"
	//it contains:
	//one grid that remains static (with no collision object in it)
	//one emitter that remains static (grid and emitter overlap, but emitter size is a lot larger than grid size)
	//one large collision plane (at the bottom of the grid)
	//one basicFS actor that can move based on key input.

	mApexCollisionShapeSpeed		= 4.5f * m_benchmarkNumActors;

	DebugRenderConfiguration config;
	config.flags.push_back(DebugRenderFlag("JetFS actor.", "BasicFS/VISUALIZE_JET_FS_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_STREAMLINES.grid",
		PxVec3(3, 3, 3));

	forceLoadAssets();

	//create the turbulence objects
	mApexTurbulenceAsset = loadApexAsset("TurbulenceFSAsset", "defaultTurbulenceFS");
	//create the emitter objects
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "PropWashEmitter" IOS_STRING)));
	//create the field sampler list
	mApexFieldSamplerAssetList.push_back(loadApexAsset("JetFSAsset", "propWashJetFS"));

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
	g_simpleMovableBody.reset();

	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, j, m_benchmarkNumActors, TurbulenceJetFSScene));
			}
		}

		//field samplers
		for (physx::PxU32 i = 0; i < mApexFieldSamplerAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0; j < 1; j++)
			{
				m_actors.push_back(new SampleJetFieldSamplerActor(*mApexFieldSamplerAssetList[i], *m_apexScene, j, m_benchmarkNumActors));
			}
		}

		//turbulence
		if (mApexTurbulenceAsset)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				mApexTurbulenceActors.push_back(new SampleTurbulenceFSActor(*mApexTurbulenceAsset, *m_apexScene, j, m_benchmarkNumActors));
			}
		}

		//create collision shapes and add them to the turbulence module
		mApexHalfSpaceCollisionShape = static_cast<SamplePlaneActor*>(addCollisionShape(HalfSpaceShapeType, PxVec3(0, -2.5, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0)));


		//set the parameters for the mesh we are going to be rendering in the place of the hovering object
		physx::PxVec3 extents(1.0, 1.0, 1.0);
		mRendererBoxShape				= new SampleRenderer::RendererBoxShape(*renderer, extents);
		mRenderMeshContext.mesh			= mRendererBoxShape->getMesh();
		mRenderMeshTransform			= physx::PxMat44::createIdentity();
		mRenderMeshContext.transform	= &mRenderMeshTransform;
		mVelocity						= Vector3D(mApexCollisionShapeSpeed, 0.f, 0.f);
	}
}


void SimpleTurbulenceApplication::createAttractorFSPWScene()
{
	//this scene demonstrates an attractor fs
	//it contains:
	//one grid that remains static (with no collision object in it)
	//one emitter that remains static (grid and emitter overlap, but emitter size is a lot larger than grid size)
	//one large collision plane (at the bottom of the grid)
	//one attractorFS actor that can move based on key input.

	mApexCollisionShapeSpeed		= 2.1f * m_benchmarkNumActors;

	DebugRenderConfiguration config;
	config.flags.push_back(DebugRenderFlag("AttractorFS actor.", "BasicFS/VISUALIZE_ATTRACTOR_FS_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_STREAMLINES.grid",
		PxVec3(3, 3, 3));

	forceLoadAssets();

	//create the turbulence objects
	mApexTurbulenceAsset = loadApexAsset("TurbulenceFSAsset", "attractorTurbulenceFS");
	//create the emitter objects
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "attractorSceneEmitter" IOS_STRING)));
	//create the field sampler list
	mApexFieldSamplerAssetList.push_back(loadApexAsset("AttractorFSAsset", "testAttractorFS"));

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
	g_simpleMovableBody.reset();

	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, j, m_benchmarkNumActors, TurbulenceAttractorFSScene));
			}
		}

		//field samplers
		for (physx::PxU32 i = 0; i < mApexFieldSamplerAssetList.size(); i++)
		{
				SampleAttractorFieldSamplerActor* actor = new SampleAttractorFieldSamplerActor(*mApexFieldSamplerAssetList[i], *m_apexScene, 0, 1);
				if(m_benchmarkNumActors != 1) actor->setConstFieldStrength(1.0f * m_benchmarkNumActors);
				if(m_benchmarkNumActors != 1) actor->setVariableFieldStrength(1.5f * m_benchmarkNumActors);
				m_actors.push_back(actor);
				//SampleAttractorFieldSamplerActor* actor = static_cast<SampleAttractorFieldSamplerActor*>(m_actors[i]);
		}

		//turbulence
		if (mApexTurbulenceAsset)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				mApexTurbulenceActors.push_back(new SampleTurbulenceFSActor(*mApexTurbulenceAsset, *m_apexScene, j, m_benchmarkNumActors, TurbulenceAttractorFSScene));

				mApexTurbulenceActors.back()->mActor->setFieldVelocityWeight(0.7f);
			}
		}

		//create collision shapes and add them to the turbulence module
		mApexHalfSpaceCollisionShape = static_cast<SamplePlaneActor*>(addCollisionShape(HalfSpaceShapeType, PxVec3(0, -2.5, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0)));

		//set the parameters for the mesh we are going to be rendering in the place of the hovering object
		//physx::PxVec3 extents(0.1, 0.1, 0.1);
		mRendererCapsuleShape			= new SampleRenderer::RendererCapsuleShape(*renderer, 0.f, 0.2f);
		mRenderMeshContext.mesh			= mRendererCapsuleShape->getMesh();
		mRenderMeshTransform			= physx::PxMat44::createIdentity();
		mRenderMeshContext.transform	= &mRenderMeshTransform;
		mVelocity						= Vector3D(mApexCollisionShapeSpeed, 0.f, 0.f);  
		g_simpleMovableBody.setPosition(Vector3D(actorPosition(0, m_benchmarkNumActors, TurbulenceAttractorFSScene, 'x'), 0.f, 0.f));
	}
}


void SimpleTurbulenceApplication::createHeatScene()
{
	//this scene demonstrates heat being applied to the base of a simulation grid, causing heated air to rise through the grid. It has the option of
	//turning on or off a collision sphere
	//it contains:
	//one grid that remains static (with no collision object in it)
	//one particle emitter that remains static
	//one heat source located at the bottom of the grid
	//optionally one collision sphere
	mApexCollisionShapeSpeed		= 3.5f * m_benchmarkNumActors;

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_STREAMLINES.grid",
		PxVec3(3, 3, 3));

	//create the turbulence objects
	mApexTurbulenceAsset = loadApexAsset("TurbulenceFSAsset", "heatTurbulenceFS");
	//create the emitter objects
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "heatSceneEmitter" IOS_STRING)));
	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();

	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, j, m_benchmarkNumActors, TurbulenceHeatScene));
			}
		}
		//turbulence
		if (mApexTurbulenceAsset)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				// create the Turbulance Field Sampler actor and push it on the vector.
				mApexTurbulenceActors.push_back(new SampleTurbulenceFSActor(*mApexTurbulenceAsset, *m_apexScene, j, m_benchmarkNumActors, TurbulenceHeatScene));
				//parameters for the turbulence actor that wont change during this scene
				mApexTurbulenceActors[j]->mActor->setAngularVelocityMultiplierAndClamp(1.0f, 2.5);
				mApexTurbulenceActors[j]->mActor->setLinearVelocityMultiplierAndClamp(1.0f, 2.5);
				mApexTurbulenceActors[j]->mActor->setUseHeat(true);
				//heat source
				mSphereHeatSource.push_back(mTurbulenceFSModule->createTurbulenceSphereHeatSource());
				mTurbulenceFSModule->addHeatSource(*m_apexScene, mSphereHeatSource[j]);
			}
		}

#if NX_SDK_VERSION_MAJOR == 2
		mApexSphereCollisionShape = new SampleSphereActor(getRenderer(), *m_simpleLitMaterial,
	                  *(m_apexScene->getPhysXScene()),
	                  PxVec3(0, 6, 0),
	                  PxVec3(mApexCollisionShapeSpeed, 0, 0),
	                  PxVec3(0.7f, 0.7f, 0.7f),
	                  100,
					  NULL,
	                  true);
#elif NX_SDK_VERSION_MAJOR == 3 
		mApexSphereCollisionShape = new SampleSphereActor(getRenderer(), *m_simpleLitMaterial,
	                  *(m_apexScene->getPhysXScene()),
	                  PxVec3(0, 6, 0),
	                  PxVec3(mApexCollisionShapeSpeed, 0, 0),
	                  PxVec3(0.7f, 0.7f, 0.7f),
	                  100,
	                  m_material,
	                  true);
#endif

		if (mApexSphereCollisionShape)
		{
			m_shape_actors.push_back(mApexSphereCollisionShape);
			collisionObjectAdded(*m_apexScene, *mApexSphereCollisionShape);
		}

#if NX_SDK_VERSION_MAJOR == 2
		NxActor* physxActor;
		physxActor = mApexSphereCollisionShape->getPhysXActor();
		physxActor->setAngularVelocity(NxVec3(mApexCollisionShapeSpeed, 0, 0));
#elif NX_SDK_VERSION_MAJOR == 3
		physx::PxRigidDynamic* physxActor;
		physxActor = static_cast<physx::PxRigidDynamic*>(mApexSphereCollisionShape->getPhysXActor());
		physxActor->setAngularVelocity(PxVec3(mApexCollisionShapeSpeed, 0, 0));
#endif

		addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
	}

	NxParameterized::setParamU32(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_TEMPERATURE_SPACING",
		2);
}


void SimpleTurbulenceApplication::createWindScene()
{
	//this scene demonstrates an external wind heat being applied to the simulation grid
	//it contains:
	//one grid that remains static, with a static rotated and translated collision implicit
	//one particle emitter that remains static

	g_applyWind = true;

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_STREAMLINES.grid",
		PxVec3(8, 8, 8));

	//create the turbulence objects
	mApexTurbulenceAsset = loadApexAsset("TurbulenceFSAsset", "defaultTurbulenceFS");
	//create the emitter objects
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "windSceneEmitter" IOS_STRING)));
	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();


	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, j, m_benchmarkNumActors));
			}
		}
		//turbulence
		if (mApexTurbulenceAsset)
		{
			for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
			{
				mApexTurbulenceActors.push_back(new SampleTurbulenceFSActor(*mApexTurbulenceAsset, *m_apexScene, j, m_benchmarkNumActors));
			}
		}

		for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
		{
			mApexBoxCollisionShape[j] = static_cast<SampleBoxActor*>(addCollisionShape(BoxShapeType, PxVec3(0, 0, 0), PxVec3(0, 0, 0), PxVec3(0.5, 1.5, 1.5)));
		}

		mApexHalfSpaceCollisionShape = static_cast<SamplePlaneActor*>(addCollisionShape(HalfSpaceShapeType, PxVec3(0, -4, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0)));
	}
}

void SimpleTurbulenceApplication::createWindFSScene()
{
#if NX_SDK_VERSION_MAJOR == 2
	//this scene demonstrates the effect of wind field samplers on basic ios

	forceLoadAssets();

	//initialize field boundary debug rendering parameters

	NxParameterized::Interface& dbgRenderParamsFieldBoundary = *m_apexScene->getModuleDebugRenderParams("FieldBoundary");
	PX_ASSERT(&dbgRenderParamsFieldBoundary);
	NxParameterized::setParamBool(dbgRenderParamsFieldBoundary, "VISUALIZE_FIELD_BOUNDARIES", true);

	//initialize wind debug rendering parameters
	NxParameterized::Interface& dbgRenderParamsWind = *m_apexScene->getModuleDebugRenderParams("Wind");
	PX_ASSERT(&dbgRenderParamsWind);
	NxParameterized::setParamBool(dbgRenderParamsWind, "VISUALIZE_FOR_WIND_FIELD_SAMPLER", 1);
	NxParameterized::setParamBool(dbgRenderParamsWind, "VISUALIZE_WIND_FORCE_FIELDS", 1);
	NxParameterized::setParamBool(dbgRenderParamsWind, "VISUALIZE_WIND_FORCES", 0);
	NxParameterized::setParamBool(dbgRenderParamsWind, "VISUALIZE_WIND_GRID", 0);
	NxParameterized::setParamBool(dbgRenderParamsWind, "VISUALIZE_WIND_BOX", 0);

	//initialize assets
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "WindFSEmitter")));
	mApexWindAsset = static_cast<NxWindAsset*>(loadApexAsset(NX_WIND_AUTHORING_TYPE_NAME, "WindFSWind"));

	mApexFieldBoundaryAssetList.push_back(loadApexAsset(NX_FIELD_BOUNDARY_AUTHORING_TYPE_NAME, "testFieldBoundary"));

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();

	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			for(physx::PxU32 j= 0; j < m_benchmarkNumActors; j++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, j));
			}
		}

		//position the emitter
		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
			{
				SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
				PX_ASSERT(actor);
				if (actor)
				{
					actor->setCurrentPosition(physx::PxVec3(actorPosition(i, m_benchmarkNumActors, TurbulenceWindFSScene, 'x'), 5, actorPosition(i, m_benchmarkNumActors, TurbulenceWindFSScene, 'z')));
				}
			}
		}

		//wind
		physx::PxMat44 boxWorldPose = physx::PxMat44::createIdentity();
		physx::PxMat33 windToWorld = physx::PxMat33::createIdentity();

		boxWorldPose.column3.z = 5.0f;
		physx::PxVec3 windScale(40.0f, 10.0f, 40.0f);

		for (physx::PxU32 i = 0; i < m_benchmarkNumActors; i++)
		{
			physx::PxVec3 location = PxVec3(0.0f, 0.0f, 0.0f);
			location.x = actorPosition(i, m_benchmarkNumActors, TurbulenceWindFSScene, 'x');
			boxWorldPose.setPosition(location);
			m_actors.push_back(new SampleWindActor(*mApexWindAsset, *m_apexScene, boxWorldPose, windToWorld, windScale));
		}

		for (physx::PxU32 i = 0; i < mApexFieldBoundaryAssetList.size(); i++)
		{
			m_actors.push_back(new SampleFieldBoundaryActor(*mApexFieldBoundaryAssetList[i], *m_apexScene, i));
		}

		//create ground plane
		addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
	}
#endif
}


void SimpleTurbulenceApplication::createExplosionFSScene()
{
#if NX_SDK_VERSION_MAJOR == 2
	//this scene demonstrates the effect of explosion field samplers on basic ios

	DebugRenderConfiguration config;

	config.flags.push_back(DebugRenderFlag("IOFX actor.", "Iofx/VISUALIZE_IOFX_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	config.flags.push_back(DebugRenderFlag("Emitter actor.", "Emitter/apexEmitterParameters.VISUALIZE_APEX_EMITTER_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	PxVec3 gravity(0.0f, -9.81f, 0.0f);
	m_apexScene->getPhysXScene()->setGravity(NxFromPxVec3Fast(gravity));

	forceLoadAssets();

	//initialize assets
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "ExplosionFSEmitter")));
	mApexExplosionAsset = static_cast<NxExplosionAsset*>(loadApexAsset(NX_EXPLOSION_AUTHORING_TYPE_NAME, "ExplosionFSExplosion"));

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();

	if (m_apexScene && renderer && assetManager)
	{
		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, i));
		}

		//position the emitter
		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
			{
				SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
				PX_ASSERT(actor);
				if (actor)
				{
					actor->setCurrentPosition(physx::PxVec3(0, 0, 0));
				}
			}
		}

		//create ground plane
		addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
	}
#endif
}




void SimpleTurbulenceApplication::createTrailingSmokeScene()
{
	DebugRenderConfiguration config;
	config.flags.push_back(DebugRenderFlag("JetFS actor.", "BasicFS/VISUALIZE_JET_FS_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	NxParameterized::setParamF32(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_LOD",
		m_simulationBudget);

	NxParameterized::setParamVec3(*mTurbulenceFSDebugRenderParams,
		"TURBULENCE_FS_STREAMLINES.grid",
		PxVec3(3, 3, 3));

	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "TrailingSmokeEmitter" IOS_STRING)));
	mApexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "spriteEmitter4" IOS_STRING)));

	NxRange<physx::PxF32> testasset = mApexEmitterAssetList[0]->getRateRange();

	forceLoadAssets();

	mApexTurbulenceFSAssetList.push_back(static_cast<NxTurbulenceFSAsset*>(loadApexAsset("TurbulenceFSAsset", "defaultTurbulenceFS")));
	mApexTurbulenceFSAssetList.push_back(static_cast<NxTurbulenceFSAsset*>(loadApexAsset("TurbulenceFSAsset", "trailingSmokeStaticTurbulenceFS")));

	mApexFieldSamplerAssetList.push_back(loadApexAsset("JetFSAsset", "staticJetFS"));
	mApexFieldSamplerAssetList.push_back(loadApexAsset("JetFSAsset", "testJetFS"));

	SampleRenderer::Renderer*            renderer     = getRenderer();
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
	initCar();

	if (m_apexScene && renderer && assetManager)
	{
		for (physx::PxU32 i = 0; i < mApexFieldSamplerAssetList.size(); i++)
		{
			m_actors.push_back(new SampleJetFieldSamplerActor(*mApexFieldSamplerAssetList[0], *m_apexScene, i));
		}

		//emitters
		for (physx::PxU32 i = 0; i < mApexEmitterAssetList.size(); i++)
		{
			m_actors.push_back(new SampleApexEmitterActor(*mApexEmitterAssetList[i], *m_apexScene, i, TurbulenceTrailingSmokeScene));
		}

		//turbulence
		for (physx::PxU32 i = 0; i < mApexTurbulenceFSAssetList.size(); i++)
		{
			mApexTurbulenceActors.push_back(new SampleTurbulenceFSActor(*mApexTurbulenceFSAssetList[i], *m_apexScene, i));
		}

		mApexSphereCollisionShape = static_cast<SampleSphereActor*>(addCollisionShape(SphereShapeType, PxVec3(0, 1, 0), PxVec3(0, 0, 0), PxVec3(0.5f, 0.5f, 0.5f)));
		mApexHalfSpaceCollisionShape = static_cast<SamplePlaneActor*>(addCollisionShape(HalfSpaceShapeType, PxVec3(5, -1.5, -5), PxVec3(0, 1, 0), PxVec3(0, 0, 0)));

		NxApexEmitterActor* apexActor;

#if NX_SDK_VERSION_MAJOR == 2
		NxActor* physxActor;
#elif NX_SDK_VERSION_MAJOR == 3
		physx::PxActor* physxActor;
#endif
		physxActor = mApexSphereCollisionShape->getPhysXActor();
		PxU32 emitter = 0;
		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
			{
				if(!emitter)
				{
					SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
					PX_ASSERT(actor);
					if (actor)
					{
						apexActor = static_cast<NxApexEmitterActor*>(actor->mActor);
						apexActor->setAttachActor(physxActor);
					}
				}
				emitter++;
			}
		}

		//create collision shapes and add them to the turbulence module
		physx::PxVec3 extents(0.25, 0.25, 0.75);
		mApexBoxCollisionShape[0] = static_cast<SampleBoxActor*>(addCollisionShape(BoxShapeType, PxVec3(0, 0, 0), PxVec3(0, 0, 0), extents));
		mApexHalfSpaceCollisionShape = static_cast<SamplePlaneActor*>(addCollisionShape(HalfSpaceShapeType, PxVec3(0, -1.5, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0)));
	}
}

//update scenes functions
void SimpleTurbulenceApplication::updateTurbulenceCollisionScene(float dtime)
{
#if TEST_CONVEX
#if NX_SDK_VERSION_MAJOR == 2
	NxVec3 pos;
	NxActor* physxActor;
	NxVec3 actorRotation;
	physxActor = mApexMeshCollisionShape->getPhysXActor();
	//physxActor->setAngularVelocity(NxVec3(mApexCollisionShapeSpeed, 0, 0));
	pos = physxActor->getGlobalPosition();
#elif NX_SDK_VERSION_MAJOR == 3
	PxTransform pos;
	physx::PxRigidDynamic* physxActor;
	PxVec3 actorRotation;
	physxActor = static_cast<physx::PxRigidDynamic*>(mApexMeshCollisionShape->getPhysXActor());
	pos = physxActor->getGlobalPose();
#endif
#else
#if NX_SDK_VERSION_MAJOR == 2
	NxVec3 pos;
	NxActor* physxActor;
	NxVec3 actorRotation;
	physxActor = mApexSphereCollisionShape->getPhysXActor();
	//physxActor->setAngularVelocity(NxVec3(mApexCollisionShapeSpeed, 0, 0));
	pos = physxActor->getGlobalPosition();
#elif NX_SDK_VERSION_MAJOR == 3
	PxTransform pos;
	physx::PxRigidDynamic* physxActor;
	PxVec3 actorRotation;
	physxActor = static_cast<physx::PxRigidDynamic*>(mApexSphereCollisionShape->getPhysXActor());
	pos = physxActor->getGlobalPose();
#endif
#endif

	switch (mRotationType)
	{
	case 0:
		{
			actorRotation.x = mApexCollisionShapeSpeed;
			actorRotation.y = 0;
			actorRotation.z = 0;
		}
		break;
	case 1:
		{
			actorRotation.x = 0;
			actorRotation.y = 0;
			actorRotation.z = mApexCollisionShapeSpeed;
		}
		break;
	case 2:
		{
			actorRotation.x = 0;
			actorRotation.y = mApexCollisionShapeSpeed;
			actorRotation.z = 0;
		}
		break;
	case 3:
		{
			actorRotation.x = 0;
			actorRotation.y = 0;
			actorRotation.z = 0;
		}
		break;
	}

	PxF32 delta = 3.9f * m_benchmarkNumActors;

#if NX_SDK_VERSION_MAJOR == 2
	if(pos.x > delta) 
	{
		physxActor->setLinearVelocity(NxVec3(-mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(-actorRotation);
	}
	if(pos.x < -delta)
	{
		physxActor->setLinearVelocity(NxVec3(mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(actorRotation);
	}
#elif NX_SDK_VERSION_MAJOR == 3
	if(pos.p.x > delta) 
	{
		physxActor->setLinearVelocity(PxVec3(-mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(-actorRotation);
	}
	if(pos.p.x < -delta)
	{
		physxActor->setLinearVelocity(PxVec3(mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(actorRotation);
	}
#endif


	for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
	{
		mApexTurbulenceActors[j]->mActor->setExternalVelocity(PxVec3(0, 2, 0));
	}

	updateCollisionObjectSceneInfo();
	updateCollisionShapes(dtime);
}


void SimpleTurbulenceApplication::updateTurbulenceJetFSPWScene(float dtime)
{
	const PxVec3 floorPos = PxVec3(0, -1, 0);
	const PxVec3 collisionPlanePos = floorPos - PxVec3(0, 0.5, 0);

	//advance the user controlled simpleMovableBody and update the grid to reflect these changes
	//we dont want the jet/basicFS force go beyond the collision plane pos
	float basicFSInnerRadius = 2.0f;
	g_simpleMovableBody.setCollisionPlane(Vector3D(0, 1, 0), Vector3D(collisionPlanePos.x, collisionPlanePos.y + basicFSInnerRadius, collisionPlanePos.z), true);
	if(!g_userLead) g_simpleMovableBody.setVelocity(mVelocity);
	updateSimpleMovableBodyInputs(g_simpleMovableBody);
	g_simpleMovableBody.update(dtime);
	Vector3D position = g_simpleMovableBody.GetPosition();

	PxF32 delta = 5.9f * m_benchmarkNumActors;

	if(position.X > delta) 
	{
		mVelocity = Vector3D(-mApexCollisionShapeSpeed, 0.f, 0.f);
	}
	if(position.X < -delta)
	{
		mVelocity = Vector3D(mApexCollisionShapeSpeed, 0.f, 0.f);
	}

	physx::PxU32 j = 0;
	//position the emitter:
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
		{
			SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
			PX_ASSERT(actor);
			if (actor)
			{
				actor->setCurrentPosition(PxVec3(actorPosition(j, m_benchmarkNumActors, TurbulenceJetFSScene, 'x'), -1, 0));
				++j;
			}
		}
	}

	//position the grid
	physx::PxU32 num = (physx::PxU32) mApexTurbulenceActors.size();
	for(physx::PxU32 i = 0; i < num; i++)
	{
		PxVec3 turbActorPosition = PxVec3(actorPosition(i, num, TurbulenceJetFSScene, 'x'), 2, 0);
		physx::PxMat44 turbPose;
		turbPose = turbPose.createIdentity();
		turbPose.setPosition(turbActorPosition);
		mApexTurbulenceActors[i]->mActor->setPose(turbPose);
	}

	//set the pose of the render mesh
	mRenderMeshTransform = physx::PxMat44::createIdentity();
	mRenderMeshTransform.setPosition(physx::PxVec3(position.X, position.Y, position.Z));
	mRenderMeshContext.transform = &mRenderMeshTransform;


#if USE_LEGACY_JETS
	//if we are using basicFS then disable the jet
	mApexTurbulenceActors[0]->mActor->setJetRadius(PxVec3(0, 0, 0), 0);
	mApexTurbulenceActors[0]->mActor->setJetRadius(PxVec3(0, 0, 0), 1);
	mApexTurbulenceActors[0]->mActor->setJetRadius(PxVec3(0, 0, 0), 2);
#endif


#if USE_LEGACY_JETS
	//if we are using basicFS then disable the jet
	mApexTurbulenceActors[0]->mActor->setJetRadius(PxVec3(0, 0, 0), 0);
	mApexTurbulenceActors[0]->mActor->setJetRadius(PxVec3(0, 0, 0), 1);
	mApexTurbulenceActors[0]->mActor->setJetRadius(PxVec3(0, 0, 0), 2);
#endif

	//position for jetFS
	j = 0;
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i]->getType() == SampleJetFieldSamplerActor::JetFieldSamplerActor)
		{
			SampleJetFieldSamplerActor* actor = static_cast<SampleJetFieldSamplerActor*>(m_actors[i]);
			PX_ASSERT(actor);
			if (actor)
			{
				actor->mActor->setCurrentPosition(physx::PxVec3(position.X, position.Y, position.Z));
			}
			++j;
		}
	}
}


void SimpleTurbulenceApplication::updateTurbulenceAttractorFSPWScene(float dtime)
{
	const PxVec3 floorPos = PxVec3(0, -1, 0);
	const PxVec3 collisionPlanePos = floorPos - PxVec3(0, 0.5, 0);

	float basicFSInnerRadius = 2.0f;
	g_simpleMovableBody.setCollisionPlane(Vector3D(0, 1, 0), Vector3D(collisionPlanePos.x, collisionPlanePos.y + basicFSInnerRadius, collisionPlanePos.z), true);
	if(!g_userLead) g_simpleMovableBody.setVelocity(mVelocity);
	updateSimpleMovableBodyInputs(g_simpleMovableBody);
	g_simpleMovableBody.update(dtime);

	Vector3D position = g_simpleMovableBody.GetPosition();

	PxF32 delta = 3.9f * m_benchmarkNumActors;

	if(position.X > delta) 
	{
		mVelocity = Vector3D(-mApexCollisionShapeSpeed, 0.f, 0.f);
	}
	if(position.X < -delta)
	{
		mVelocity = Vector3D(mApexCollisionShapeSpeed, 0.f, 0.f);
	}

	//position the emitter:
	physx::PxU32 j = 0;
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
		{
			SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
			PX_ASSERT(actor);
			if (actor)
			{
				actor->setCurrentPosition(PxVec3(actorPosition(j, m_benchmarkNumActors, TurbulenceAttractorFSScene, 'x'), -1, 0));
				++j;
			}
		}
	}

	//position the grid
	physx::PxU32 num = (physx::PxU32) mApexTurbulenceActors.size();
	for(physx::PxU32 i = 0; i < num; i++)
	{
		PxVec3 turbActorPosition = PxVec3(actorPosition(i, num, TurbulenceAttractorFSScene, 'x'), 2, 0);
		physx::PxMat44 turbPose;
		turbPose = turbPose.createIdentity();
		turbPose.setPosition(turbActorPosition);
		mApexTurbulenceActors[i]->mActor->setPose(turbPose);
		mApexTurbulenceActors[i]->mActor->setExternalVelocity(PxVec3(0, 2, 0));
	}

	//set the pose of the render mesh
	mRenderMeshTransform = physx::PxMat44::createIdentity();
	mRenderMeshTransform.setPosition(physx::PxVec3(position.X, position.Y, position.Z));
	mRenderMeshContext.transform  = &mRenderMeshTransform;

	//position for attractorFS
	j = 0;
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i]->getType() == SampleAttractorFieldSamplerActor::AttractorFieldSamplerActor)
		{
			SampleAttractorFieldSamplerActor* actor = static_cast<SampleAttractorFieldSamplerActor*>(m_actors[i]);
			PX_ASSERT(actor);
			if (actor)
			{
				actor->mActor->setCurrentPosition(physx::PxVec3(position.X, position.Y, position.Z));
			}
			++j;
		}
	}
}


void SimpleTurbulenceApplication::updateTurbulenceHeatScene(float dtime)
{
#if NX_SDK_VERSION_MAJOR == 2
	NxVec3 pos;
	NxActor* physxActor;
	NxVec3 actorRotation;
	physxActor = mApexSphereCollisionShape->getPhysXActor();
	pos = physxActor->getGlobalPosition();
#elif NX_SDK_VERSION_MAJOR == 3
	PxTransform pos;
	physx::PxRigidDynamic* physxActor;
	PxVec3 actorRotation;
	physxActor = static_cast<physx::PxRigidDynamic*>(mApexSphereCollisionShape->getPhysXActor());
	pos = physxActor->getGlobalPose();
#endif

	switch (mRotationType)
	{
	case 0:
		{
			actorRotation.x = mApexCollisionShapeSpeed;
			actorRotation.y = 0;
			actorRotation.z = 0;
		}
		break;
	case 1:
		{
			actorRotation.x = 0;
			actorRotation.y = 0;
			actorRotation.z = mApexCollisionShapeSpeed;
		}
		break;
	case 2:
		{
			actorRotation.x = 0;
			actorRotation.y = mApexCollisionShapeSpeed;
			actorRotation.z = 0;
		}
		break;
	case 3:
		{
			actorRotation.x = 0;
			actorRotation.y = 0;
			actorRotation.z = 0;
		}
		break;
	}

	PxF32 delta = 3.9f * m_benchmarkNumActors;

#if NX_SDK_VERSION_MAJOR == 2
	if(pos.x > delta) 
	{
		physxActor->setLinearVelocity(NxVec3(-mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(-actorRotation);
	}
	if(pos.x < -delta)
	{
		physxActor->setLinearVelocity(NxVec3(mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(actorRotation);
	}
#elif NX_SDK_VERSION_MAJOR == 3
	if(pos.p.x > delta) 
	{
		physxActor->setLinearVelocity(PxVec3(-mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(-actorRotation);
	}
	if(pos.p.x < -delta)
	{
		physxActor->setLinearVelocity(PxVec3(mApexCollisionShapeSpeed, 0, 0));
		physxActor->setAngularVelocity(actorRotation);
	}
#endif

	//position the grid
	for (physx::PxU32 i = 0; i < mApexTurbulenceActors.size(); i++)
	{
		physx::PxU32 num = (physx::PxU32) mApexTurbulenceActors.size();
		PxVec3 turbActorPosition = PxVec3(actorPosition(i, num, TurbulenceHeatScene, 'x'), 6.0f, 0);
		physx::PxMat44 turbPose;
		turbPose = turbPose.createIdentity();
		turbPose.setPosition(turbActorPosition);
		mApexTurbulenceActors[i]->mActor->setPose(turbPose);
		mApexTurbulenceActors[i]->mActor->setVelocity(physx::PxVec3(0));
	}

	//position the emitter:
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		physx::PxU32 num = (physx::PxU32)m_actors.size();
		if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
		{
			SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
			PX_ASSERT(actor);
			if (actor)
			{
				actor->setCurrentPosition(PxVec3(actorPosition(i, num, TurbulenceHeatScene, 'x'), 2, 0));
			}
		}
	}

	for(physx::PxU32 i = 0; i < mApexTurbulenceActors.size(); i++)
	{
		physx::PxU32 num = (physx::PxU32)mApexTurbulenceActors.size();
		mApexTurbulenceActors[i]->mActor->setHeatBasedParameters(0.1f, 0.0f, PxVec3(0, 1, 0));
		//update the position of the heat source
		physx::PxMat44 pose;
		pose = pose.createIdentity();
		pose.setPosition(PxVec3(actorPosition(i, num, TurbulenceHeatScene, 'x'), 2, 0));
		mSphereHeatSource[i]->getSphereShape()->setPose(pose);
		mSphereHeatSource[i]->getSphereShape()->setRadius(1.f);
		mSphereHeatSource[i]->setTemperature(mTemperature, 2.0f);

	}

	updateCollisionShapes(dtime);
	updateHeatSceneInfo();
}


void SimpleTurbulenceApplication::updateTurbulenceWindScene(float /*dtime*/)
{
	//position the grid
	physx::PxU32 num = (physx::PxU32) mApexTurbulenceActors.size();
	for(physx::PxU32 i = 0; i < num; i++)
	{
		PxVec3 turbActorPosition = PxVec3(actorPosition(i, num, TurbulenceWindScene, 'x'), 0, actorPosition(i, num, TurbulenceWindScene, 'z'));
		physx::PxMat44 turbPose;
		turbPose = turbPose.createIdentity();
		turbPose.setPosition(turbActorPosition);
		mApexTurbulenceActors[i]->mActor->setPose(turbPose);
		mApexTurbulenceActors[i]->mActor->setVelocity(physx::PxVec3(0));
	}

	//add wind:
	if (g_applyWind)
	{
		for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
		{
			mApexTurbulenceActors[j]->mActor->setExternalVelocity(PxVec3(1, 0, 0));
		}
	}
	else
	{
		for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
		{
			mApexTurbulenceActors[j]->mActor->setExternalVelocity(PxVec3(0, 0, 0));
		}
	}

	physx::PxU32 j = 0;
	//position the emitter:
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
		{
			SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
			PX_ASSERT(actor);
			if (actor)
			{
				actor->setCurrentPosition(PxVec3(actorPosition(j, m_benchmarkNumActors, TurbulenceWindScene, 'x') - 3, 0, actorPosition(j, m_benchmarkNumActors, TurbulenceWindScene, 'z')));
				++j;
			}
		}
	}

	PxMat44 collisionPose;
	for (physx::PxU32 j = 0; j < m_benchmarkNumActors; j++)
	{
		static const physx::PxReal sqrRootOfZeroPointFive = physx::PxSqrt(0.5f);
		collisionPose = PxMat44::PxMat44(PxVec4(sqrRootOfZeroPointFive, sqrRootOfZeroPointFive, 0, 0), PxVec4(-sqrRootOfZeroPointFive, sqrRootOfZeroPointFive, 0, 0), PxVec4(0, 0, 1, 0), PxVec4(actorPosition(j, m_benchmarkNumActors, TurbulenceWindScene, 'x') - 1.0f, 0, actorPosition(j, m_benchmarkNumActors, TurbulenceWindScene, 'z'), 1));
		mApexBoxCollisionShape[j]->setPose(physx::PxTransform(collisionPose));
	}
}


void SimpleTurbulenceApplication::updateTurbulenceWindFSScene(float /*dtime*/)
{

}


void SimpleTurbulenceApplication::updateTurbulenceExplosionFSScene(float /*dtime*/)
{

}


void SimpleTurbulenceApplication::updateTurbulenceTrailingSmokeScene(float dtime)
{
	if (mApexTurbulenceActors.size() > 0)
	{
		//advance the user controlled car and update the grid to reflect these changes
		advanceCar(dtime);

		//process car's inputs. Note that this particular car gives back 2D values, but the emitter and grid are set up to accept general 3D values
		Vector velocity = drivableCar->GetVelocity();
		Vector position = drivableCar->GetPosition();
		Vector cTangent = drivableCar->WorldToRelative(Vector(1, 0));
		PxVec3 carTangent = PxVec3(-cTangent.X, 0, cTangent.Y);
		carTangent.normalize();

		Vector axis1 = Vector(carTangent.x , carTangent.z);
		Vector axis2 = Vector(-carTangent.z, carTangent.x);

		//car example-------------------------------------------------------------------------------------------------

#if NX_SDK_VERSION_MAJOR == 2
		NxVec3 myVel(velocity.X, 0.0f, velocity.Y);
		NxActor* physxActor;
		physxActor = mApexSphereCollisionShape->getPhysXActor();
		physxActor->setLinearVelocity(myVel);
#elif NX_SDK_VERSION_MAJOR == 3
		PxVec3 myVel(velocity.X, 0.0f, velocity.Y);
		physx::PxRigidDynamic* physxActor;
		physxActor = static_cast<physx::PxRigidDynamic*>(mApexSphereCollisionShape->getPhysXActor());
		physxActor->setLinearVelocity(myVel);
#endif

		physx::PxMat44 turbPose;
		turbPose = physx::PxMat44(	PxVec4(axis1.X,		0.0,	axis1.Y,	0.0),
									PxVec4(0.0,			1.0,	0.0,		0.0),
									PxVec4(axis2.X,		0.0,	axis2.Y,	0.0),
									PxVec4(position.X,	0.0f,	position.Y, 1.0));   //static turb grid position relative to collision sphere
		mApexTurbulenceActors[0]->mActor->setPose(turbPose);	
		mApexTurbulenceActors[0]->mActor->setVelocity(PxVec3(velocity.X, 0.0f, velocity.Y));	

		PxVec3 staticTurbLoc = PxVec3(10, 0, -4.3);
		if (mApexTurbulenceActors.size() > 1)
		{

			turbPose.column3 = PxVec4(staticTurbLoc.x, staticTurbLoc.y, staticTurbLoc.z, 1);
			mApexTurbulenceActors[1]->mActor->setPose(turbPose);
		}


		//inputs for emitter
		float carCenterX = position.X;
		float carCenterY = 1.0f; 
		float carCenterZ = position.Y;

		int emitter = 0;
		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
			{
				SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
				PX_ASSERT(actor);
				if (actor)
				{
					if (emitter == 0) //moving object's emitter
					{
						physx::PxMat44 pose4x4 = turbPose;
						pose4x4.column3 = PxVec4(carCenterX, carCenterY, carCenterZ, 1);
						actor->setCurrentPose(pose4x4);
					}
					else
					{
						physx::PxMat44 pose4x4 = turbPose;
						pose4x4.column3 = PxVec4(staticTurbLoc.x, staticTurbLoc.y, staticTurbLoc.z, 1);
						//actor->setCurrentPose(pose4x4);
						actor->setCurrentPosition(physx::PxVec3(staticTurbLoc.x, 0.0f, staticTurbLoc.z));
					}
				}
				emitter++;
			}
		}

		//position for jetFS
		PxU32 jetfs = 0;
		for (size_t i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i]->getType() == SampleJetFieldSamplerActor::JetFieldSamplerActor)
			{
				if(jetfs) //moving object's emitter
				{
					SampleJetFieldSamplerActor* actor = static_cast<SampleJetFieldSamplerActor*>(m_actors[i]);
					PX_ASSERT(actor);
					if (actor)
					{
						physx::PxMat44 pose4x4 = turbPose;
						actor->mActor->setCurrentPose(pose4x4);
						if(g_userLead == false || g_throttle == 1) actor->mActor->setCurrentPosition(physx::PxVec3(carCenterX, carCenterY, carCenterZ));
						else actor->mActor->setCurrentPosition(physx::PxVec3(1000.0f, 1000.0f, 1000.0f));
					}
				}
				else
				{
					SampleJetFieldSamplerActor* actor = static_cast<SampleJetFieldSamplerActor*>(m_actors[i]);
					PX_ASSERT(actor);
					if (actor)
					{
						actor->mActor->setCurrentPosition(physx::PxVec3(staticTurbLoc.x, 0.0f, staticTurbLoc.z));
					}
				}
				jetfs++;
			}
		}
	}
}

unsigned int SimpleTurbulenceApplication::getNumScenes() const
{
	return NumScenes;
}

unsigned int SimpleTurbulenceApplication::getSelectedScene() const
{
	return mSceneNumber;
}

const char* SimpleTurbulenceApplication::getSceneName(unsigned int sceneNumber) const
{
	switch(sceneNumber)
	{
	case 0:
		return "NopScene";
	case 1:
		return "TurbulenceCollisionObjectScene";
	case 2:
		return "TurbulenceJetFSScene";
	case 3:
		return "TurbulenceAttractorFSScene";
	case 4:
		return "TurbulenceWindScene";
	case 5:
		return "TurbulenceHeatScene";
	case 6:
		return "TurbulenceTrailingSmokeScene";
	case 7:
		return "AssetPreviewScene";
#if NX_SDK_VERSION_MAJOR == 2
	case 8:
		return "TurbulenceWindFSScene";
	case 9:
		return "TurbulenceExplosionFSScene";	
#elif NX_SDK_VERSION_MAJOR == 3
	case 8:
	case 9:
#endif
	default:
		PX_ALWAYS_ASSERT();
		return NULL;
	}
}

void SimpleTurbulenceApplication::selectScene(unsigned int sceneNumber)
{
	unloadCurrentAssetsAndActors();
	loadAssetsAndActors((SceneSelection)sceneNumber);
	initializeText();
}
