// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#include <assert.h>

#include "ApexHelloWorldConfig.h"

#include "SampleApexApplication.h"
#include "NxParamUtils.h"

#include "NxUserRenderer.h"

// apex basic headers
#include "NxApexSDK.h"
#include "NxApexScene.h"
#include "NxModule.h"
#include "NxApexAsset.h"
#include "NxApexActor.h"

// apex legacy module headers
#include "NxModuleCommonLegacy.h"
#include "NxModuleFrameworkLegacy.h"

// apex clothing headers
#include "NxModuleClothing.h"
#include "NxModuleClothingLegacy.h"
#include "NxClothingAsset.h"
#include "NxClothingActor.h"

// apex destruction headers
#include "NxModuleDestructible.h"
#include "NxModuleDestructibleLegacy.h"
#include "NxDestructibleAsset.h"
#include "NxDestructibleActor.h"

#if APEX_USE_PARTICLES
// apex particle headers
#include "NxModuleEmitter.h"
#include "NxModuleIofx.h"
#if NX_SDK_VERSION_MAJOR == 2
#include "NxModuleFluidIos.h"
#elif NX_SDK_VERSION_MAJOR == 3
#include "NxModuleParticleIos.h"
#endif // NX_SDK_VERSION_MAJOR
#include "NxModuleEmitterLegacy.h"
#include "NxModuleIOFXLegacy.h"
#include "NxApexEmitterAsset.h"
#include "NxApexEmitterActor.h"
#include "NxIofxActor.h"
#include "NxApexRenderVolume.h"
#endif // APEX_USE_PARTICLES

#include "NxRenderMeshAsset.h"

#if NX_SDK_VERSION_MAJOR == 3
// physx headers
#include "PxShape.h"
#include "extensions/PxRigidBodyExt.h"
#endif // NX_SDK_VERSION_MAJOR == 3

namespace
{
	void verifyOk(bool b)
	{
		assert(b);
		(void)b;
	}
}; // namespace nameless

namespace ApexHelloWorldNamespace
{
/************************* Apex Singleton Resources *************************/
ApexModuleEntity::ApexModuleEntity(const char * moduleName_)
	:moduleName(moduleName_)
	,module(NULL)
{
	assert(NULL != moduleName);
}

ApexModuleEntity::~ApexModuleEntity()
{
	assert(NULL == module);
}

bool ApexModuleEntity::onCreateModule(physx::apex::NxApexSDK & sdk)
{
	bool valid = false;
	assert(NULL != &sdk);
	assert(NULL == module);
	if(NULL == module)
	{
		module = sdk.createModule(moduleName);
		assert(NULL != module);
		if(NULL != module)
		{
			NxParameterized::Interface * moduleParams = NULL;
			moduleParams = module->getDefaultModuleDesc();
			assert(NULL != moduleParams);
			if(NULL != moduleParams)
			{
				this->initializeModuleParams(*moduleParams);
				module->init(*moduleParams);
				valid = true;
			}
		}
	}
	return valid;
}

bool ApexModuleEntity::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	bool valid = false;
	assert(NULL != &sdk);
	assert(NULL != module);
	if(NULL != module)
	{
		sdk.releaseModule(module);
		module = NULL;
		valid = true;
	}
	return valid;
}

const physx::apex::NxModule * ApexModuleEntity::getModuleConst() const
{
	assert(NULL != module);
	return module;
}

physx::apex::NxModule * ApexModuleEntity::getModule()
{
	return const_cast<physx::apex::NxModule*>(ApexModuleEntity::getModuleConst());
}

void ApexModuleEntity::initializeModuleParams(NxParameterized::Interface &) const
{
}

ApexAssetEntity::ApexAssetEntity(const char * assetAuthoringName_)
	:assetAuthoringName(assetAuthoringName_)
{
	assert(NULL != assetAuthoringName);
}

ApexAssetEntity::~ApexAssetEntity()
{
	while(!assetContainer.empty())
	{
		assert(NULL == assetContainer.back());
		assetContainer.pop_back();
	}
}

bool ApexAssetEntity::onCreateAsset(SampleApexApplication & app, const char * assetFileName, unsigned int assetIndex)
{
	bool valid = false;
	assert(NULL != &app && NULL != assetFileName);
	assert(assetIndex == assetContainer.size());
	if(assetIndex == assetContainer.size())
	{
		physx::apex::NxApexAsset * candidateAsset = NULL;
		candidateAsset = app.loadApexAsset(assetAuthoringName, assetFileName);
		assert(NULL != candidateAsset);
		if(NULL != candidateAsset)
		{
			bool isExisting = false;
			for(std::vector<physx::apex::NxApexAsset*>::const_iterator kIter = assetContainer.begin(); kIter != assetContainer.end(); ++kIter)
			{
				assert(*kIter != candidateAsset);
				if(*kIter == candidateAsset)
				{
					isExisting = true;
					break;
				}
			}
			if(!isExisting)
			{
				assetContainer.push_back(candidateAsset);
				assert(assetContainer.size() == assetIndex + 1 ? NULL != assetContainer[assetIndex] : false);
				valid = true;
			}
		}
	}
	return valid;
}

bool ApexAssetEntity::onReleaseAsset(SampleApexApplication & app, unsigned int assetIndex)
{
	bool valid = false;
	assert(NULL != &app);
	assert(assetIndex < assetContainer.size() ? NULL != assetContainer[assetIndex] : false);
	if(assetIndex < assetContainer.size() ? NULL != assetContainer[assetIndex] : false)
	{
		app.releaseApexAsset(assetContainer[assetIndex]);
		assetContainer[assetIndex] = NULL;
		valid = true;
	}
	return valid;
}

const physx::apex::NxApexAsset * ApexAssetEntity::getAssetConst(unsigned int assetIndex) const
{
	assert(assetIndex < assetContainer.size() ? NULL != assetContainer[assetIndex] : false);
	return assetIndex < assetContainer.size() ? assetContainer[assetIndex] : NULL;
}

physx::apex::NxApexAsset * ApexAssetEntity::getAsset(unsigned int assetIndex)
{
	return const_cast<physx::apex::NxApexAsset*>(ApexAssetEntity::getAssetConst(assetIndex));
}

physx::apex::NxApexActor * ApexAssetEntity::instantiateActor(physx::apex::NxApexScene & scene, unsigned int actorId)
{
	assert(NULL != &scene);
	physx::apex::NxApexActor * actor = NULL;
	physx::apex::NxApexAsset * asset = NULL;
	asset = ApexAssetEntity::getAsset(this->lookUpAssetIndex(actorId));
	assert(NULL != asset);
	if(NULL != asset)
	{
		NxParameterized::Interface * actorParams = NULL;
		actorParams = asset->getDefaultActorDesc();
		assert(NULL != actorParams);
		if(NULL != actorParams)
		{
			this->initializeActorParams(*actorParams, actorId);
			if(asset->isValidForActorCreation(*actorParams, scene))
			{
				actor = asset->createApexActor(*actorParams, scene);
				assert(NULL != actor);
			}
		}
	}
	return actor;
}

void ApexAssetEntity::initializeActorParams(NxParameterized::Interface &, unsigned int) const
{
}

/************************* Apex Entity Managers *************************/
ApexClothingManager::ApexClothingManager()
	:ApexModuleEntity("Clothing")
	,ApexAssetEntity(NX_CLOTHING_AUTHORING_TYPE_NAME)
{
}

ApexClothingManager::~ApexClothingManager()
{
}

bool ApexClothingManager::onCreateModule(physx::apex::NxApexSDK & sdk)
{
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleClothing();
#endif // APEX_MODULES_STATIC_LINK
	return ApexModuleEntity::onCreateModule(sdk);
}

bool ApexClothingManager::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	return ApexModuleEntity::onReleaseModule(sdk);
}

bool ApexClothingManager::onCreateAssets(SampleApexApplication & app)
{
	return
		false == ApexAssetEntity::onCreateAsset(app, "CTDM/ctdm_trenchcoat_800", static_cast<unsigned int>(ApexClothingManager::AssetIdTrenchcoat)) ? false
		: true;
}

bool ApexClothingManager::onReleaseAssets(SampleApexApplication & app)
{
	return 
		false == ApexAssetEntity::onReleaseAsset(app, static_cast<unsigned int>(ApexClothingManager::AssetIdTrenchcoat)) ? false
		: true;
}

const physx::apex::NxModuleClothing * ApexClothingManager::getModuleConst() const
{
	return static_cast<const physx::apex::NxModuleClothing*>(ApexModuleEntity::getModuleConst());
}

physx::apex::NxModuleClothing * ApexClothingManager::getModule()
{
	return static_cast<physx::apex::NxModuleClothing*>(ApexModuleEntity::getModule());
}

const physx::apex::NxClothingAsset * ApexClothingManager::getAssetConst(ApexClothingManager::AssetId assetId) const
{
	return static_cast<const physx::apex::NxClothingAsset*>(ApexAssetEntity::getAssetConst(static_cast<unsigned int>(assetId)));
}

physx::apex::NxClothingAsset * ApexClothingManager::getAsset(ApexClothingManager::AssetId assetId)
{
	return static_cast<physx::apex::NxClothingAsset*>(ApexAssetEntity::getAsset(static_cast<unsigned int>(assetId)));
}

physx::apex::NxClothingActor * ApexClothingManager::instantiateActor(physx::apex::NxApexScene & scene, ApexClothingManager::ActorId actorId)
{
	return static_cast<physx::apex::NxClothingActor*>(ApexAssetEntity::instantiateActor(scene, static_cast<unsigned int>(actorId)));
}

void ApexClothingManager::initializeModuleParams(NxParameterized::Interface & moduleParams) const
{
	(void)moduleParams;
}

void ApexClothingManager::initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const
{
	assert(NULL != &actorParams);

	// write common actor parameters
	switch(static_cast<ApexClothingManager::ActorId>(actorId))
	{
	case ApexClothingManager::ActorIdTrenchcoat_0 :
		NxParameterized::setParamBool(actorParams, "useHardwareCloth", false);
		NxParameterized::setParamBool(actorParams, "flags.RecomputeNormals", false);
		NxParameterized::setParamBool(actorParams, "flags.CorrectSimulationNormals", true);
		NxParameterized::setParamBool(actorParams, "useInternalBoneOrder", true);
		NxParameterized::setParamU32(actorParams, "clothDescTemplate.groupsMask.bits0", 0);
		NxParameterized::setParamU32(actorParams, "clothDescTemplate.groupsMask.bits1", 1);
		NxParameterized::setParamU32(actorParams, "clothDescTemplate.groupsMask.bits2", 0);
		NxParameterized::setParamU32(actorParams, "clothDescTemplate.groupsMask.bits3", 1);
#if NX_SDK_VERSION_MAJOR == 2
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.groupsMask.bits0", 0);
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.groupsMask.bits1", 1);
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.groupsMask.bits2", 0);
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.groupsMask.bits3", 1);
#endif // NX_SDK_VERSION_MAJOR
		NxParameterized::setParamF32(actorParams, "lodWeights.maxDistance", 2000.0f);
		NxParameterized::setParamF32(actorParams, "lodWeights.distanceWeight", 1.0f);
		NxParameterized::setParamF32(actorParams, "lodWeights.bias", 0.0f);
		NxParameterized::setParamF32(actorParams, "lodWeights.benefitsBias", 0.0f);
		break;
	default :
		assert(!"invalid actorId!");
	}

	// write specific actor parameters
	switch(static_cast<ApexClothingManager::ActorId>(actorId))
	{
	case ApexClothingManager::ActorIdTrenchcoat_0 :
		{
			physx::PxMat44 trenchcoatPose = physx::PxMat44::createIdentity();
			trenchcoatPose(1, 1) =	0;
			trenchcoatPose(2, 2) =	0;
			trenchcoatPose(1, 2) =	1;
			trenchcoatPose(2, 1) = -1;
			float scale = 0.1f;
			trenchcoatPose *= scale;
			trenchcoatPose.setPosition(physx::PxVec3(-2.0f, 0.0f, 0.0f));
			NxParameterized::setParamMat44(actorParams, "globalPose", trenchcoatPose);
			NxParameterized::setParamF32(actorParams, "actorScale", scale);
		}
		break;
	default :
		assert(!"invalid actorId!");
	}
}

unsigned int ApexClothingManager::lookUpAssetIndex(unsigned int actorId) const
{
	unsigned int assetId = 0xFFFFFFFF;
	switch(static_cast<ApexClothingManager::ActorId>(actorId))
	{
	case ApexClothingManager::ActorIdTrenchcoat_0 :
		assetId = static_cast<unsigned int>(ApexClothingManager::AssetIdTrenchcoat);
		break;
	default :
		assert(!"invalid actorId!");
	}
	return assetId;
}

ApexDestructibleManager::ApexDestructibleManager()
	:ApexModuleEntity("Destructible")
	,ApexAssetEntity(NX_DESTRUCTIBLE_AUTHORING_TYPE_NAME)
{
}

ApexDestructibleManager::~ApexDestructibleManager()
{
}

bool ApexDestructibleManager::onCreateModule(physx::apex::NxApexSDK & sdk)
{
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleDestructible();
#endif // APEX_MODULES_STATIC_LINK
	return ApexModuleEntity::onCreateModule(sdk);
}

bool ApexDestructibleManager::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	return ApexModuleEntity::onReleaseModule(sdk);
}

bool ApexDestructibleManager::onCreateAssets(SampleApexApplication & app)
{
	return
		false == ApexAssetEntity::onCreateAsset(app, "Wall", static_cast<unsigned int>(ApexDestructibleManager::AssetIdWall)) ? false
		: true;
}

bool ApexDestructibleManager::onReleaseAssets(SampleApexApplication & app)
{
	return
		false == ApexAssetEntity::onReleaseAsset(app, static_cast<unsigned int>(ApexDestructibleManager::AssetIdWall)) ? false
		: true;
}

const physx::apex::NxModuleDestructible * ApexDestructibleManager::getModuleConst() const
{
	return static_cast<const physx::apex::NxModuleDestructible*>(ApexModuleEntity::getModuleConst());
}

physx::apex::NxModuleDestructible * ApexDestructibleManager::getModule()
{
	return static_cast<physx::apex::NxModuleDestructible*>(ApexModuleEntity::getModule());
}

const physx::apex::NxDestructibleAsset * ApexDestructibleManager::getAssetConst(ApexDestructibleManager::AssetId assetId) const
{
	return static_cast<const physx::apex::NxDestructibleAsset*>(ApexAssetEntity::getAssetConst(static_cast<unsigned int>(assetId)));
}

physx::apex::NxDestructibleAsset * ApexDestructibleManager::getAsset(ApexDestructibleManager::AssetId assetId)
{
	return static_cast<physx::apex::NxDestructibleAsset*>(ApexAssetEntity::getAsset(static_cast<unsigned int>(assetId)));
}

physx::apex::NxDestructibleActor * ApexDestructibleManager::instantiateActor(physx::apex::NxApexScene & scene, ApexDestructibleManager::ActorId actorId)
{
	return static_cast<physx::apex::NxDestructibleActor*>(ApexAssetEntity::instantiateActor(scene, static_cast<unsigned int>(actorId)));
}

void ApexDestructibleManager::initializeModuleParams(NxParameterized::Interface & moduleParams) const
{
	(void)moduleParams;
}

void ApexDestructibleManager::initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const
{
	assert(NULL != &actorParams);

	// write common actor parameters
	switch(static_cast<ApexDestructibleManager::ActorId>(actorId))
	{
	case ApexDestructibleManager::ActorIdWall_0 :
#if defined(PX_X360) || defined(PX_PS3)
		NxParameterized::setParamBool(actorParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", false);
#else
		NxParameterized::setParamBool(actorParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", true);
#endif // defined(PX_X360) || defined(PX_PS3)
		NxParameterized::setParamF32(actorParams, "destructibleParameters.forceToDamage", 0.1f);
		NxParameterized::setParamF32(actorParams, "destructibleParameters.damageThreshold", 10.0f);
		NxParameterized::setParamF32(actorParams, "destructibleParameters.damageCap", 10.0f);
		NxParameterized::setParamF32(actorParams, "destructibleParameters.damageToRadius", 0.0f);
		NxParameterized::setParamF32(actorParams, "destructibleParameters.fractureImpulseScale", 2.0f);
		NxParameterized::setParamBool(actorParams, "formExtendedStructures", true);
		{
			int depthParametersCount = 0;
			NxParameterized::getParamArraySize(actorParams, "depthParameters", depthParametersCount);
			NxParameterized::setParamI32(actorParams, "destructibleParameters.impactDamageDefaultDepth", depthParametersCount - 1);
			if(depthParametersCount > 0)
			{
				const unsigned int bufferCount = 128;
				for(physx::PxU32 index = 0; index < static_cast<unsigned int>(depthParametersCount); ++index)
				{
					char buffer[bufferCount] = {0};
					physx::string::sprintf_s(buffer, bufferCount, "depthParameters[%d].OVERRIDE_IMPACT_DAMAGE", index);
					NxParameterized::setParamBool(actorParams, buffer, false);
				}
			}
		}
#if NX_SDK_VERSION_MAJOR == 2
		NxParameterized::setParamBool(actorParams, "shapeDescTemplate.groupsMask.useGroupsMask", true);
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.groupsMask.bits0", 2);
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.groupsMask.bits2", ~0);
		NxParameterized::setParamU32(actorParams, "shapeDescTemplate.collisionGroup", 0);
		NxParameterized::setParamF32(actorParams, "actorDescTemplate.density", 1.0f);
#elif NX_SDK_VERSION_MAJOR == 3
		NxParameterized::setParamU32(actorParams, "p3ShapeDescTemplate.simulationFilterData.word0", 2);
		NxParameterized::setParamU32(actorParams, "p3ShapeDescTemplate.simulationFilterData.word2", ~0);
		NxParameterized::setParamF32(actorParams, "p3BodyDescTemplate.density", 1.0f);
#endif // NX_SDK_VERSION_MAJOR
		NxParameterized::setParamBool(actorParams, "dynamic", false);
		break;
	default :
		assert(!"invalid actorId!");
	}

	// write specific actor parameters
	switch(static_cast<ApexDestructibleManager::ActorId>(actorId))
	{
	case ApexDestructibleManager::ActorIdWall_0 :
		{
			physx::PxF32 scale = 0.5f;
			physx::PxMat44 wallPose = physx::PxMat44::createIdentity();
			wallPose(1, 1) =  0;
			wallPose(2, 2) =  0;
			wallPose(1, 2) =  1;
			wallPose(2, 1) = -1;
			const physx::apex::NxRenderMeshAsset * wallMesh = NULL;
			wallMesh = getAssetConst(ApexDestructibleManager::AssetIdWall)->getRenderMeshAsset();
			assert(NULL != wallMesh);
			if(NULL != wallMesh)
			{
				wallPose.setPosition(physx::PxVec3(0.0f, -wallMesh->getBounds().minimum.z * scale, 0.0f));
				NxParameterized::setParamMat44(actorParams, "globalPose", wallPose);
				NxParameterized::setParamVec3(actorParams, "scale", physx::PxVec3(scale));
			}

			// set up emitters in destructibles
#if NX_SDK_VERSION_MAJOR == 2
			NxParameterized::setParamString(actorParams, "crumbleEmitterName", "crumbleEmitter");
#elif NX_SDK_VERSION_MAJOR == 3
			NxParameterized::setParamString(actorParams, "crumbleEmitterName", "crumbleEmitterPhysX3");
#endif // NX_SDK_VERSION_MAJOR
		}
		break;
	default :
		assert(!"invalid actorId!");
	}
}

unsigned int ApexDestructibleManager::lookUpAssetIndex(unsigned int actorId) const
{
	unsigned int assetId = 0xFFFFFFFF;
	switch(static_cast<ApexDestructibleManager::ActorId>(actorId))
	{
	case ApexDestructibleManager::ActorIdWall_0 :
		assetId = static_cast<unsigned int>(ApexDestructibleManager::AssetIdWall);
		break;
	default :
		assert(!"invalid actorId!");
	}
	return assetId;
}

#if APEX_USE_PARTICLES
ApexEmitterManager::ApexEmitterManager()
	:ApexModuleEntity("Emitter")
	,ApexAssetEntity(NX_APEX_EMITTER_AUTHORING_TYPE_NAME)
{
}

ApexEmitterManager::~ApexEmitterManager()
{
}

bool ApexEmitterManager::onCreateModule(physx::apex::NxApexSDK & sdk)
{
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleEmitter();
#endif // APEX_MODULES_STATIC_LINK
	return ApexModuleEntity::onCreateModule(sdk);
}

bool ApexEmitterManager::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	return ApexModuleEntity::onReleaseModule(sdk);
}

bool ApexEmitterManager::onCreateAssets(SampleApexApplication & app)
{
#if NX_SDK_VERSION_MAJOR == 2
	const char * const spriteAssetName = "testSpriteEmitter4FluidIos";
#elif NX_SDK_VERSION_MAJOR == 3
	const char * const spriteAssetName = "testSpriteEmitter4ParticleFluidIos";
#endif //NX_SDK_VERSION_MAJOR
	return
		false == ApexAssetEntity::onCreateAsset(app, spriteAssetName, static_cast<unsigned int>(ApexEmitterManager::AssetIdSprite)) ? false
		: true;
}

bool ApexEmitterManager::onReleaseAssets(SampleApexApplication & app)
{
	return
		false == ApexAssetEntity::onReleaseAsset(app, static_cast<unsigned int>(ApexEmitterManager::AssetIdSprite)) ? false
		: true;
}

const physx::apex::NxModuleEmitter * ApexEmitterManager::getModuleConst() const
{
	return static_cast<const physx::apex::NxModuleEmitter*>(ApexModuleEntity::getModuleConst());
}

physx::apex::NxModuleEmitter * ApexEmitterManager::getModule()
{
	return static_cast<physx::apex::NxModuleEmitter*>(ApexModuleEntity::getModule());
}

const physx::apex::NxApexEmitterAsset * ApexEmitterManager::getAssetConst(ApexEmitterManager::AssetId assetId) const
{
	return static_cast<const physx::apex::NxApexEmitterAsset*>(ApexAssetEntity::getAssetConst(static_cast<unsigned int>(assetId)));
}

physx::apex::NxApexEmitterAsset * ApexEmitterManager::getAsset(ApexEmitterManager::AssetId assetId)
{
	return static_cast<physx::apex::NxApexEmitterAsset*>(ApexAssetEntity::getAsset(static_cast<unsigned int>(assetId)));
}

physx::apex::NxApexEmitterActor * ApexEmitterManager::instantiateActor(physx::apex::NxApexScene & scene, ApexEmitterManager::ActorId actorId)
{
	return static_cast<physx::apex::NxApexEmitterActor*>(ApexAssetEntity::instantiateActor(scene, static_cast<unsigned int>(actorId)));
}

void ApexEmitterManager::initializeModuleParams(NxParameterized::Interface & moduleParams) const
{
	(void)moduleParams;
}

void ApexEmitterManager::initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const
{
	assert(NULL != &actorParams);

	// write common actor parameters
	switch(static_cast<ApexEmitterManager::ActorId>(actorId))
	{
	case ApexEmitterManager::ActorIdSprite_0 :
		break;
	default :
		assert(!"invalid actorId!");
	}

	// write specific actor parameters
	switch(static_cast<ApexEmitterManager::ActorId>(actorId))
	{
	case ApexEmitterManager::ActorIdSprite_0 :
		{
			physx::PxMat44 spritePose = physx::PxMat44::createIdentity();
			spritePose.setPosition(physx::PxVec3(0.0f, 10.0f, 0.0f));
			NxParameterized::setParamMat34(actorParams, "initialPose", spritePose);
		}
		break;
	default :
		assert(!"invalid actorId!");
	}
}

unsigned int ApexEmitterManager::lookUpAssetIndex(unsigned int actorId) const
{
    unsigned int assetId = 0xFFFFFFFF;
	switch(static_cast<ApexEmitterManager::ActorId>(actorId))
	{
	case ApexEmitterManager::ActorIdSprite_0 :
		assetId = static_cast<unsigned int>(ApexEmitterManager::AssetIdSprite);
		break;
	default :
		assert(!"invalid actorId!");
	}
	return assetId;
}

ApexIofxManager::ApexIofxManager()
	:ApexModuleEntity("IOFX")
{
}

ApexIofxManager::~ApexIofxManager()
{
}

bool ApexIofxManager::onCreateModule(physx::apex::NxApexSDK & sdk)
{
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleIOFX();
#endif // APEX_MODULES_STATIC_LINK
	return ApexModuleEntity::onCreateModule(sdk);
}

bool ApexIofxManager::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	return ApexModuleEntity::onReleaseModule(sdk);
}

const physx::apex::NxModuleIofx * ApexIofxManager::getModuleConst() const
{
	return static_cast<const physx::apex::NxModuleIofx*>(ApexModuleEntity::getModuleConst());
}

physx::apex::NxModuleIofx * ApexIofxManager::getModule()
{
	return static_cast<physx::apex::NxModuleIofx*>(ApexModuleEntity::getModule());
}

physx::apex::NxApexRenderVolume * ApexIofxManager::instantiateRenderVolume(physx::apex::NxApexScene & scene, ApexIofxManager::RenderVolumeId renderVolumeId)
{
	assert(NULL != &scene);
	physx::apex::NxApexRenderVolume * renderVolume = NULL;
	physx::apex::NxModuleIofx * module = NULL;
	module = getModule();
	assert(NULL != module);
	if(NULL != module)
	{
		physx::PxBounds3 bounds = physx::PxBounds3(physx::PxVec3(0.0f), physx::PxVec3(0.0f));
		unsigned int priority = 0;
		bool allIOFX = true;
		switch(renderVolumeId)
		{
		case ApexIofxManager::RenderVolumeIdAxisAlignedBox_0 :
			bounds = physx::PxBounds3(physx::PxVec3(-15.0f), physx::PxVec3(15.0f));
			break;
		default :
			assert(!"invalid renderVolumeId!");
		}
		renderVolume = module->createRenderVolume(scene, bounds, priority, allIOFX);
		assert(NULL != renderVolume);
	}
	return renderVolume;
}

void ApexIofxManager::initializeModuleParams(NxParameterized::Interface & moduleParams) const
{
	(void)moduleParams;
}

#if NX_SDK_VERSION_MAJOR == 2
ApexIosManager::ApexIosManager()
	:ApexModuleEntity("NxFluidIOS")
{
}

ApexIosManager::~ApexIosManager()
{
}

bool ApexIosManager::onCreateModule(physx::apex::NxApexSDK & sdk)
{
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleNxFluidIOS();
#endif // APEX_MODULES_STATIC_LINK
	return ApexModuleEntity::onCreateModule(sdk);
}

bool ApexIosManager::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	return ApexModuleEntity::onReleaseModule(sdk);
}

const physx::apex::NxModuleFluidIos * ApexIosManager::getModuleConst() const
{
	return static_cast<const physx::apex::NxModuleFluidIos*>(ApexModuleEntity::getModuleConst());
}

physx::apex::NxModuleFluidIos * ApexIosManager::getModule()
{
	return static_cast<physx::apex::NxModuleFluidIos*>(ApexModuleEntity::getModule());
}

void ApexIosManager::initializeModuleParams(NxParameterized::Interface & moduleParams) const
{
	(void)moduleParams;
}

#elif NX_SDK_VERSION_MAJOR == 3
ApexIosManager::ApexIosManager()
	:ApexModuleEntity("ParticleIOS")
{
}

ApexIosManager::~ApexIosManager()
{
}

bool ApexIosManager::onCreateModule(physx::apex::NxApexSDK & sdk)
{
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleParticleIos();
#endif // APEX_MODULES_STATIC_LINK
	return ApexModuleEntity::onCreateModule(sdk);
}

bool ApexIosManager::onReleaseModule(physx::apex::NxApexSDK & sdk)
{
	return ApexModuleEntity::onReleaseModule(sdk);
}

const physx::apex::NxModuleParticleIos * ApexIosManager::getModuleConst() const
{
	return static_cast<const physx::apex::NxModuleParticleIos*>(ApexModuleEntity::getModuleConst());
}

physx::apex::NxModuleParticleIos * ApexIosManager::getModule()
{
	return static_cast<physx::apex::NxModuleParticleIos*>(ApexModuleEntity::getModule());
}

void ApexIosManager::initializeModuleParams(NxParameterized::Interface & moduleParams) const
{
	(void)moduleParams;
}
#endif // NX_SDK_VERSION_MAJOR
#endif // APEX_USE_PARTICLES

/************************* Apex Actor Wrappers *************************/
UserActorEntity::UserActorEntity()
{
}

UserActorEntity::~UserActorEntity()
{
}

void UserActorEntity::onTick(float)
{
}

void UserActorEntity::onRender(bool, physx::apex::NxUserRenderer &)
{
}

UserSpinningCoatActor::UserSpinningCoatActor()
	:UserActorEntity()
	,clothingActor(NULL)
	,currentLodIndex(-1)
	,increaseLod(false)
	,angleInitDone(false)
	,initialPose(physx::PxMat44::createIdentity())
	,spinningState(0)
	,previousAngle(0.0f)
	,angleStepSize(0.0f)
	,angleAdjustment(0.0f)
{
}

UserSpinningCoatActor::~UserSpinningCoatActor()
{
	assert(NULL == clothingActor);
}

UserActorEntity * UserSpinningCoatActor::instantiate(ApexClothingManager & clothingManager, physx::apex::NxApexScene & scene)
{
	assert(NULL != &clothingManager && NULL != &scene);
	UserSpinningCoatActor * userSpinningCoatActor = NULL;
	physx::apex::NxClothingActor * clothingActor = NULL;
	clothingActor = clothingManager.instantiateActor(scene, ApexClothingManager::ActorIdTrenchcoat_0);
	assert(NULL != clothingActor);
	if(NULL != clothingActor)
	{
		userSpinningCoatActor = ::new UserSpinningCoatActor();
		userSpinningCoatActor->clothingActor = clothingActor;
		clothingActor = NULL;
		if(NULL != userSpinningCoatActor ? NULL != userSpinningCoatActor->clothingActor : false)
		{
			userSpinningCoatActor->initialize(clothingManager);
		}
	}
	return (NULL != userSpinningCoatActor ? static_cast<UserActorEntity*>(userSpinningCoatActor) : NULL);
}

bool UserSpinningCoatActor::isType(UserActorEntity::Id id) const
{
	return (UserActorEntity::IdUserSpinningCoatActor == id);
}

void UserSpinningCoatActor::onDestroy()
{
	assert(NULL != clothingActor);
	clothingActor->release();
	clothingActor = NULL;
	::delete this;
}

void UserSpinningCoatActor::onTick(float dtime)
{
	UserActorEntity::onTick(dtime);

	clothingActor->lockRenderResources();
	clothingActor->updateRenderResources();
	clothingActor->unlockRenderResources();

	//build continuous rotation angle
	previousAngle += angleStepSize;
	if(!angleInitDone)
	{
		assert(0.0f != dtime);
		angleStepSize = dtime * 1.0f;
		angleAdjustment = angleStepSize * 0.5f;
		angleInitDone = true;
	}
	assert(spinningState >= -1 && spinningState <= 1);
	angleStepSize += (spinningState * angleAdjustment);
	const float boundaryValue = 0.035f;
	angleStepSize = (angleStepSize > boundaryValue) ? boundaryValue : (angleStepSize < -boundaryValue) ? -boundaryValue : angleStepSize;
	spinningState = 0;

	//build continuous rotation pose
	physx::PxMat44 rotationY = physx::PxMat44::createIdentity();
	rotationY(0, 0) =  physx::PxCos(previousAngle + angleStepSize);
	rotationY(2, 2) =  physx::PxCos(previousAngle + angleStepSize);
	rotationY(0, 2) =  physx::PxSin(previousAngle + angleStepSize);
	rotationY(2, 0) = -physx::PxSin(previousAngle + angleStepSize);
	physx::PxMat44 rotatingPose = physx::PxMat44::createIdentity();
	rotatingPose = rotationY * initialPose;

	clothingActor->updateState(rotatingPose, NULL, 0, 0, physx::apex::ClothingTeleportMode::Continuous);
}

void UserSpinningCoatActor::onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer)
{
	assert(NULL != &userRenderer);
	UserActorEntity::onRender(rewriteBuffers, userRenderer);

	clothingActor->dispatchRenderResources(userRenderer);
}

const physx::apex::NxClothingActor * UserSpinningCoatActor::getActorConst() const
{
	assert(NULL != clothingActor);
	return clothingActor;
}

physx::apex::NxClothingActor * UserSpinningCoatActor::getActor()
{
	return const_cast<physx::apex::NxClothingActor*>(getActorConst());
}

void UserSpinningCoatActor::changeRenderLod()
{
	if(lodValueContainer.size() > 1)
	{
		assert(currentLodIndex != -1);
		increaseLod = (0 == currentLodIndex) ? true : (lodValueContainer.size() - 1 == static_cast<unsigned int>(currentLodIndex)) ? false : increaseLod;
		increaseLod ? ++currentLodIndex : --currentLodIndex;
		clothingActor->setGraphicalLOD(lodValueContainer[currentLodIndex]);
	}
}

void UserSpinningCoatActor::changeSpinningVelocity(bool increase)
{
	spinningState = increase ? 1 : -1;
}

void UserSpinningCoatActor::initialize(const ApexClothingManager & clothingManager)
{
	(void)clothingManager;

	// retrieve initial pose
	NxParameterized::Interface * actorParams = NULL;
	actorParams = clothingActor->getActorDesc();
	assert(NULL != actorParams);
	verifyOk(NxParameterized::getParamMat44(*actorParams, "globalPose", initialPose));

	// retrieve LOD info
	const NxParameterized::Interface * assetParams = NULL;
	assetParams = clothingActor->getOwner()->getAssetNxParameterized();
	assert(NULL != assetParams);
	if(NULL != assetParams)
	{
		int graphicalLodsCount = -1;
		verifyOk(NxParameterized::getParamArraySize(*assetParams, "graphicalLods", graphicalLodsCount));
		if(graphicalLodsCount >= 0)
		{
			assert(graphicalLodsCount >= 0);
			assert(lodValueContainer.empty());
			const unsigned int bufferCount = 128;
			for(unsigned int index = 0; index < static_cast<unsigned int>(graphicalLodsCount); ++index)
			{
				char buffer[bufferCount] = {0};
				physx::string::sprintf_s(buffer, bufferCount, "graphicalLods[%d]", index);
				NxParameterized::Interface * graphicalLodParams = NULL;
				verifyOk(NxParameterized::getParamRef(*assetParams, buffer, graphicalLodParams));
				assert(NULL != graphicalLodParams);
				lodValueContainer.push_back(~0);
				verifyOk(NxParameterized::getParamU32(*graphicalLodParams, "lod", lodValueContainer.back()));
				assert(0 != ~(lodValueContainer.back()));
			}
			if(!lodValueContainer.empty())
			{
				clothingActor->setGraphicalLOD(lodValueContainer.back());
				currentLodIndex = static_cast<int>(lodValueContainer.size()) - 1;
			}
		}
	}
}

UserFracturableWallActor::UserFracturableWallActor()
	:UserActorEntity()
	,destructibleActor(NULL)
{
}

UserFracturableWallActor::~UserFracturableWallActor()
{
	assert(NULL == destructibleActor);
}

UserActorEntity * UserFracturableWallActor::instantiate(ApexDestructibleManager & destructibleManager, physx::apex::NxApexScene & scene)
{
	assert(NULL != &destructibleManager && NULL != &scene);
	UserFracturableWallActor * userFracturableWallActor = NULL;
	physx::apex::NxDestructibleActor * destructibleActor = NULL;
	destructibleActor = destructibleManager.instantiateActor(scene, ApexDestructibleManager::ActorIdWall_0);
	assert(NULL != destructibleActor);
	if(NULL != destructibleActor)
	{
		userFracturableWallActor = ::new UserFracturableWallActor();
		userFracturableWallActor->destructibleActor = destructibleActor;
		destructibleActor = NULL;
		if(NULL != userFracturableWallActor ? NULL != userFracturableWallActor->destructibleActor : false)
		{
			userFracturableWallActor->initialize(destructibleManager);
		}
	}
	return (NULL != userFracturableWallActor ? static_cast<UserActorEntity*>(userFracturableWallActor) : NULL);
}

bool UserFracturableWallActor::isType(UserActorEntity::Id id) const
{
	return (UserActorEntity::IdUserFracturableWallActor == id);
}

void UserFracturableWallActor::onDestroy()
{
	assert(NULL != destructibleActor);
	destructibleActor->release();
	destructibleActor = NULL;
	::delete this;
}

void UserFracturableWallActor::onTick(float dtime)
{
	UserActorEntity::onTick(dtime);
}

void UserFracturableWallActor::onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer)
{
	assert(NULL != &userRenderer);
	UserActorEntity::onRender(rewriteBuffers, userRenderer);

	destructibleActor->lockRenderResources();
	destructibleActor->updateRenderResources(rewriteBuffers);
	destructibleActor->dispatchRenderResources(userRenderer);
	destructibleActor->unlockRenderResources();
}

const physx::apex::NxDestructibleActor * UserFracturableWallActor::getActorConst() const
{
	assert(NULL != destructibleActor);
	return destructibleActor;
}

physx::apex::NxDestructibleActor * UserFracturableWallActor::getActor()
{
	return const_cast<physx::apex::NxDestructibleActor*>(getActorConst());
}

void UserFracturableWallActor::processRaycast(const physx::apex::NxModuleDestructible & module, physx::apex::NxApexScene & scene, const physx::PxVec3 & rayOrigin, const physx::PxVec3 & rayDirection)
{
	assert(NULL != &module && NULL != &scene && NULL != &rayOrigin && NULL != &rayDirection);

	// retrieve destructible hit info
	physx::apex::NxDestructibleActor* hitActor = NULL;
	physx::PxF32 hitTime = PX_MAX_F32;
	physx::PxVec3 hitNormal(0.0f);
	physx::PxI32 hitChunkIndex = physx::apex::NxModuleDestructibleConst::INVALID_CHUNK_INDEX;
	physx::PxF32 time = 0;
	physx::PxVec3 normal(0.0f);
#if NX_SDK_VERSION_MAJOR == 2
	NxVec3 nxRayOrigin(0.0f);
	NxVec3 nxRayDirection(0.0f);
	physx::NxFromPxVec3(nxRayOrigin, rayOrigin);
	physx::NxFromPxVec3(nxRayDirection, rayDirection);
	const NxRay worldRay(nxRayOrigin, nxRayDirection);
	const physx::PxI32 chunkIndex = destructibleActor->rayCast(time, normal, worldRay, physx::apex::NxDestructibleActorRaycastFlags::AllChunks);
#elif NX_SDK_VERSION_MAJOR == 3
	const physx::PxI32 chunkIndex = destructibleActor->rayCast(time, normal, rayOrigin, rayDirection, physx::apex::NxDestructibleActorRaycastFlags::AllChunks);
#endif // NX_SDK_VERSION_MAJOR
	if(chunkIndex != physx::apex::NxModuleDestructibleConst::INVALID_CHUNK_INDEX && time < hitTime)
	{
		hitActor = destructibleActor;
		hitTime = time;
		hitNormal = normal;
		hitChunkIndex = chunkIndex;
	}

	// also check PhysX scene
#if NX_SDK_VERSION_MAJOR == 2
	NxRaycastHit hit;
	NxShape* hitShape = NULL;
	hitShape = scene.getPhysXScene()->raycastClosestShape(worldRay, NX_DYNAMIC_SHAPES, hit);
	if(hitShape != NULL)
	{
		if(!module.owns(&hitShape->getActor()) && hit.distance < hitTime)
		{
			hitActor = NULL;
			hitShape->getActor().addForceAtPos(worldRay.dir * 30, hit.worldImpact, NX_IMPULSE, true);
		}
	}
#elif NX_SDK_VERSION_MAJOR == 3
	physx::PxRaycastHit	hit;
	bool isHit = scene.getPhysXScene()->raycastSingle(rayOrigin, rayDirection, PX_MAX_F32, physx::PxSceneQueryFlag::eIMPACT | physx::PxSceneQueryFlag::eNORMAL, hit, physx::PxSceneQueryFilterData(physx::PxSceneQueryFilterFlag::eDYNAMIC));
	if(isHit)
	{
		if(!module.owns(&hit.shape->getActor()) && hit.distance < hitTime)
		{
			hitActor = NULL;
			physx::PxRigidBodyExt::addForceAtPos(*hit.shape->getActor().isRigidBody(), rayDirection * 30, hit.impact, physx::PxForceMode::eIMPULSE, true);
		}
	}
#endif // NX_SDK_VERSION_MAJOR
	if(hitActor != NULL)
	{
		hitActor->applyDamage(10.0f, 10.0f, rayOrigin + (hitTime * rayDirection), rayDirection, hitChunkIndex);
	}
}

void UserFracturableWallActor::initialize(const ApexDestructibleManager & destructibleManager)
{
	(void)destructibleManager;
}

#if APEX_USE_PARTICLES
UserParticleSpawningActor::UserParticleSpawningActor()
	:UserActorEntity()
	,emitterActor(NULL)
	,currentCountLevel(0)
{
}

UserParticleSpawningActor::~UserParticleSpawningActor()
{
	assert(NULL == emitterActor);
}

UserActorEntity * UserParticleSpawningActor::instantiate(ApexEmitterManager & emitterManager, physx::apex::NxApexScene & scene)
{
	assert(NULL != &emitterManager && NULL != &scene);
	UserParticleSpawningActor * userParticleSpawningActor = NULL;
	physx::apex::NxApexEmitterActor * emitterActor = NULL;
	emitterActor = emitterManager.instantiateActor(scene, ApexEmitterManager::ActorIdSprite_0);
	assert(NULL != emitterActor);
	if(NULL != emitterActor)
	{
		userParticleSpawningActor = ::new UserParticleSpawningActor();
		userParticleSpawningActor->emitterActor = emitterActor;
		emitterActor = NULL;
		if(NULL != userParticleSpawningActor ? NULL != userParticleSpawningActor->emitterActor : false)
		{
			userParticleSpawningActor->initialize(emitterManager);
		}
	}
	return (NULL != userParticleSpawningActor ? static_cast<UserActorEntity*>(userParticleSpawningActor) : NULL);
}

bool UserParticleSpawningActor::isType(UserActorEntity::Id id) const
{
	return (UserActorEntity::IdUserParticleSpawningActor == id);
}

void UserParticleSpawningActor::onDestroy()
{
	assert(NULL != emitterActor);
	emitterActor->release();
	emitterActor = NULL;
	::delete this;
}

void UserParticleSpawningActor::onTick(float dtime)
{
	UserActorEntity::onTick(dtime);
}

void UserParticleSpawningActor::onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer)
{
	assert(NULL != &userRenderer);
	UserActorEntity::onRender(rewriteBuffers, userRenderer);
}

const physx::apex::NxApexEmitterActor * UserParticleSpawningActor::getActorConst() const
{
	assert(NULL != emitterActor);
	return emitterActor;
}

physx::apex::NxApexEmitterActor * UserParticleSpawningActor::getActor()
{
	return const_cast<physx::apex::NxApexEmitterActor*>(getActorConst());
}

void UserParticleSpawningActor::changeRenderCount(physx::apex::NxModuleEmitter & module)
{
	assert(NULL != &module);
	const unsigned int emitterModuleScalablesCount = module.getNbParameters();
	physx::NxApexParameter * const * emitterModuleScalablesContainer = NULL;
	emitterModuleScalablesContainer = module.getParameters();
	if(NULL != emitterModuleScalablesContainer && 0 != emitterModuleScalablesCount)
	{
		currentCountLevel = ++currentCountLevel >= 3 ? 0 : currentCountLevel;
		for(unsigned int index = 0; index < emitterModuleScalablesCount; ++index)
		{
			assert(NULL != emitterModuleScalablesContainer[index]);
			if(NULL != emitterModuleScalablesContainer[index])
			{
				const physx::PxU32 max = emitterModuleScalablesContainer[index]->range.maximum;
				const physx::PxU32 min = emitterModuleScalablesContainer[index]->range.minimum;
				const physx::PxU32 mid = static_cast<unsigned int>((max - min) / 2);
				module.setIntValue(index, (0 == currentCountLevel) ? min : (1 == currentCountLevel) ? mid : max);
			}
		}
	}
}

void UserParticleSpawningActor::initialize(const ApexEmitterManager & emitterManager)
{
	(void)emitterManager;
	emitterActor->startEmit(true);
	currentCountLevel = 0;
}

UserParticleEffectController::UserParticleEffectController()
	:UserActorEntity()
	,renderVolume(NULL)
{
}

UserParticleEffectController::~UserParticleEffectController()
{
	assert(NULL == renderVolume);
}

UserActorEntity * UserParticleEffectController::instantiate(ApexIofxManager & iofxManager, physx::apex::NxApexScene & scene)
{
	assert(NULL != &iofxManager && NULL != &scene);
	UserParticleEffectController * userParticleEffectController = NULL;
	physx::apex::NxApexRenderVolume * renderVolume = NULL;
	renderVolume = iofxManager.instantiateRenderVolume(scene, ApexIofxManager::RenderVolumeIdAxisAlignedBox_0);
	assert(NULL != renderVolume);
	if(NULL != renderVolume)
	{
		userParticleEffectController = ::new UserParticleEffectController();
		userParticleEffectController->renderVolume = renderVolume ;
		renderVolume  = NULL;
		if(NULL != userParticleEffectController ? NULL != userParticleEffectController->renderVolume : false)
		{
			userParticleEffectController->initialize(iofxManager);
		}
	}
	return (NULL != userParticleEffectController ? static_cast<UserActorEntity*>(userParticleEffectController) : NULL);
}

bool UserParticleEffectController::isType(UserActorEntity::Id id) const
{
	return (UserActorEntity::IdUserParticleEffectController == id);
}

void UserParticleEffectController::onDestroy()
{
	assert(NULL != renderVolume);
	renderVolume->release();
	renderVolume = NULL;
	::delete this;
}

void UserParticleEffectController::onTick(float dtime)
{
	UserActorEntity::onTick(dtime);
}

void UserParticleEffectController::onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer)
{
	assert(NULL != &userRenderer);
	UserActorEntity::onRender(rewriteBuffers, userRenderer);

	renderVolume->lockRenderResources();
	physx::PxU32 iofxActorCount = 0;
	physx::apex::NxIofxActor * const * iofxActorContainer = NULL;
	iofxActorContainer = renderVolume->getIofxActorList(iofxActorCount);
	if(NULL != iofxActorContainer && 0 != iofxActorCount)
	{
		for(unsigned int index = 0; index < iofxActorCount; ++index)
		{
			assert(NULL != iofxActorContainer[index]);
			if(NULL != iofxActorContainer[index])
			{
				iofxActorContainer[index]->lockRenderResources();
				if(!iofxActorContainer[index]->getBounds().isEmpty())
				{
					iofxActorContainer[index]->updateRenderResources(rewriteBuffers);
					iofxActorContainer[index]->dispatchRenderResources(userRenderer);
				}
				iofxActorContainer[index]->unlockRenderResources();
			}
		}
	}
	renderVolume->unlockRenderResources();
}

const physx::apex::NxApexRenderVolume * UserParticleEffectController::getRenderVolumeConst() const
{
	assert(NULL != renderVolume);
	return renderVolume;
}

physx::apex::NxApexRenderVolume * UserParticleEffectController::getRenderVolume()
{
	return const_cast<physx::apex::NxApexRenderVolume*>(getRenderVolumeConst());
}

void UserParticleEffectController::initialize(const ApexIofxManager & iofxManager)
{
	(void)iofxManager;
}
#endif // APEX_USE_PARTICLES
}; // namespace ApexHelloWorldNamespace
