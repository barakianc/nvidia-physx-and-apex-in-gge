// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Hello World
// Description: This sample program uses APEX clothing, destructible and particles.
// 1) Shows simple use of modules, assets and actors.
// 2) Shows simple use of LOD
// 3) Shows simple physics interaction
// 4) Shows timings and statistics
//
// ===============================================================================

#include "ApexHelloWorld.h"
#include "ApexHelloWorldConfig.h"

#include "SampleApexApplication.h"
#include "NxParamUtils.h"
#include "PsUtilities.h"


// apex basic headers
#include "NxApexSDK.h"
#include "NxApexScene.h"
#include "NxModule.h"
#include "NxApexAsset.h"
#include "NxApexActor.h"

// apex legacy module headers
#include "NxModuleCommonLegacy.h"
#include "NxModuleFrameworkLegacy.h"

// apex clothing headers
#include "NxModuleClothing.h"
#include "NxModuleClothingLegacy.h"
#include "NxClothingAsset.h"
#include "NxClothingActor.h"

// apex destruction headers
#include "NxModuleDestructible.h"
#include "NxModuleDestructibleLegacy.h"
#include "NxDestructibleAsset.h"
#include "NxDestructibleActor.h"

#if APEX_USE_PARTICLES
// apex particle headers
#include "NxModuleEmitter.h"
#include "NxModuleIofx.h"
#if NX_SDK_VERSION_MAJOR == 2
#include "NxModuleFluidIos.h"
#elif NX_SDK_VERSION_MAJOR == 3
#include "NxModuleParticleIos.h"
#endif // NX_SDK_VERSION_MAJOR
#include "NxModuleEmitterLegacy.h"
#include "NxModuleIOFXLegacy.h"
#include "NxApexEmitterAsset.h"
#include "NxApexEmitterActor.h"
#include "NxIofxActor.h"
#include "NxApexRenderVolume.h"
#endif // APEX_USE_PARTICLES

// sample renderer headers
#include "RendererDirectionalLightDesc.h"

// apex sample actor headers
#include "SampleActor.h"
#include "SampleBoxActor.h"

// apex sample input headers
#include "SamplePlatform.h"
#include "SampleUserInput.h"
#include "SampleUserInputIds.h"
#include "SampleInputEventIds.h"
#include "SampleInputDefines.h"

namespace
{
    template<typename T>
    void verifyOk(T * t)
    {
        PX_ASSERT(NULL != t);
        PX_FORCE_PARAMETER_REFERENCE(t);
    }

	void verifyOk(bool b)
	{
		PX_ASSERT(b);
        PX_FORCE_PARAMETER_REFERENCE(b);
	}

	enum ApexHelloWorldEventIds
	{
		APEX_HELLO_WORLD_FIRST = NUM_SAMPLE_BASE_INPUT_EVENT_IDS,

		SCENE_0 = APEX_HELLO_WORLD_FIRST,
		SCENE_1,
		SCENE_2,
#if APEX_USE_PARTICLES
		SCENE_3,
#endif // APEX_USE_PARTICLES
		INCREASE_SPINNING_VELOCITY,
		DECREASE_SPINNING_VELOCITY,
		RAYCAST_HIT,
		BOX_SHOT,
#if APEX_USE_PARTICLES
		CHANGE_PARTICLE_COUNT,
#endif // APEX_USE_PARTICLES

		NUM_APEX_HELLO_WORLD_INPUT_EVENT_IDS,
		NUM_EXCLUSIVE_HELLO_WORLD_INPUT_EVENT_IDS = NUM_APEX_HELLO_WORLD_INPUT_EVENT_IDS - APEX_HELLO_WORLD_FIRST
	};

	const char* const ApexHelloWorldEventDescriptions[] =
	{
		"switch to scene 0",
		"switch to scene 1",
		"switch to scene 2",
#if APEX_USE_PARTICLES
		"switch to scene 3",
#endif // APEX_USE_PARTICLES
		"increase the actor spinning velocity (scene 1)",
		"decrease the actor spinning velocity (scene 1)",
		"shoot a ray (scene 2)",
		"shoot a box (scene 2)",
#if APEX_USE_PARTICLES
		"change the particle count (scene 3)",
#endif // APEX_USE_PARTICLES
	};

	PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(ApexHelloWorldEventDescriptions) == NUM_EXCLUSIVE_HELLO_WORLD_INPUT_EVENT_IDS);
}; // namespace nameless

// Skeleton Application Class
ApexHelloWorld::ApexHelloWorld(const SampleFramework::SampleCommandLine& cmdline)
	:SampleApexApplication(cmdline)
#ifdef WITHOUT_DEBUG_VISUALIZE
	,useDebugRender(false)
#else
	,useDebugRender(true)
#endif // WITHOUT_DEBUG_VISUALIZE
	,currentSceneId(ApexHelloWorld::SceneIdCount)
{
	// No "Space to create box"
	m_allowCreateObject = false;
}

ApexHelloWorld::~ApexHelloWorld()
{
}

void ApexHelloWorld::onInit()
{
	SampleApexApplication::onInit();

	// set up application
	sampleInit("ApexHelloWorld", false, false);
	PX_ASSERT(NULL != m_apexSDK);
	PX_ASSERT(NULL != m_resourceCallback);
	PX_ASSERT(NULL != m_apexRenderer);
	PX_ASSERT(useDebugRender ? NULL != m_apexRenderDebug : true);

	// set up apex and physx scenes
	m_apexScene = createSampleScene(physx::PxVec3(0, -9.8f, 0));
	PX_ASSERT(NULL != m_apexScene);
	PX_ASSERT(NULL != m_apexScene ? NULL != m_apexScene->getPhysXScene() : false);

	// load apex resources. require a manual call to unload
	loadModules();
	loadAssets();

	// set up post-loadModules() settings
	setUpCollision();
	setUpLevelOfDetail();

	// load scene. require a manual call to unload
    loadStaticScene();
	loadScene(ApexHelloWorld::SceneIdClothing);
}

void ApexHelloWorld::onOpen()
{
	SampleApexApplication::onOpen();
	PX_ASSERT(NULL != m_renderer);
}

void ApexHelloWorld::onTickPreRender(float dtime)
{
	SampleApexApplication::onTickPreRender(dtime);

	if(!m_pause)
	{
		// dtime varies quite a bit, clothing likes fixed time-steps. dtime is also too large in the first frame
		dtime = 1.0f / 60.0f;

		// reset view and projection matrices for this tick
		setUpViewProjectionMatrices();

		// tick scene
		for(std::vector<SampleShapeActor*>::const_iterator kIter = m_shape_actors.begin(); kIter != m_shape_actors.end(); ++kIter)
		{
			PX_ASSERT(NULL != *kIter);
			(*kIter)->tick(dtime);
		}
		for(std::vector<UserActorEntity*>::const_iterator kIter = userActorContainer.begin(); kIter != userActorContainer.end(); ++kIter)
		{
			PX_ASSERT(NULL != *kIter);
			(*kIter)->onTick(dtime);
		}
		for(std::vector<SampleFramework::SampleActor*>::const_iterator kIter = m_actors.begin(); kIter != m_actors.end(); ++kIter)
		{
			PX_ASSERT(NULL != *kIter);
			(*kIter)->tick(dtime);
		}

        // simulate scene. must be matched with m_apexScene->fetchResults()
		m_apexScene->simulate(dtime);
	}
}

void ApexHelloWorld::onRender()
{
	SampleApexApplication::onRender();

    // pre-render call
	m_renderer->clearBuffers();

    // render lighting
	for(std::vector<SampleRenderer::RendererLight*>::const_iterator kIter = m_lights.begin(); kIter != m_lights.end(); ++kIter)
	{
		m_renderer->queueLightForRender(*(*kIter));
	}

	// render scene
    for (std::vector<SampleShapeActor*>::const_iterator kIter = m_shape_actors.begin(); kIter != m_shape_actors.end(); ++kIter)
	{
		PX_ASSERT(NULL != *kIter);
		(*kIter)->render();
	}
	for(std::vector<UserActorEntity*>::const_iterator kIter = userActorContainer.begin(); kIter != userActorContainer.end(); ++kIter)
	{
		PX_ASSERT(NULL != *kIter);
		(*kIter)->onRender(m_rewriteBuffers, *m_apexRenderer);
	}
	for (std::vector<SampleFramework::SampleActor*>::const_iterator kIter = m_actors.begin(); kIter != m_actors.end(); ++kIter)
	{
		PX_ASSERT(*kIter != NULL);
		(*kIter)->render(m_rewriteBuffers);
	}

    // render visualizations
	if(useDebugRender)
	{
		// render user debug-render
		m_apexRenderDebug->lockRenderResources();
		m_apexRenderDebug->updateRenderResources();
		m_apexRenderDebug->dispatchRenderResources(*m_apexRenderer);
		m_apexRenderDebug->unlockRenderResources();

		// render debug-render scene
		m_apexScene->lockRenderResources();
		m_apexScene->updateRenderResources();
		m_apexScene->dispatchRenderResources(*m_apexRenderer);
		m_apexScene->unlockRenderResources();
	}

    // post-render call
	m_renderer->render(getEyeTransform(), m_projection, NULL, false);

    // print scene info in the foreground
	const char * additionalSceneInfo = 
		ApexHelloWorld::SceneIdClothing		== currentSceneId ? "" :
		ApexHelloWorld::SceneIdDestructible == currentSceneId ? "" :
#if APEX_USE_PARTICLES
		ApexHelloWorld::SceneIdEmitter		== currentSceneId ? "" :
#endif // APEX_USE_PARTICLES
		"";
	printSceneInfo("", additionalSceneInfo);
}

void ApexHelloWorld::onTickPostRender(float dtime)
{
	// update scene. must be matched with m_apexScene->simulate()
	if(!m_pause)
	{
		physx::PxU32 errorState = 0;
		verifyOk(m_apexScene->fetchResults(true, &errorState));
        PX_ASSERT(0 == errorState);
	}

	// push physx debug-render visualization data
	if(useDebugRender)
	{
#if NX_SDK_VERSION_MAJOR == 2
		const NxDebugRenderable * debugRenderable = m_apexScene->getPhysXScene()->getDebugRenderable();
		if(NULL != debugRenderable && NULL != m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(*debugRenderable);
		}
#elif NX_SDK_VERSION_MAJOR == 3
		const physx::PxRenderBuffer & renderBuffer = m_apexScene->getPhysXScene()->getRenderBuffer();
		PX_ASSERT(NULL != &renderBuffer);
		if(NULL != m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(renderBuffer);
		}
#endif // NX_SDK_VERSION_MAJOR
	}

	SampleApexApplication::onTickPostRender(dtime);
}

void ApexHelloWorld::onShutdown()
{
	unloadScene();
    unloadStaticScene();
	unloadAssets();
	unloadModules();
	SampleApexApplication::onShutdown();
	PX_ASSERT(NULL == m_apexScene);
	PX_ASSERT(useDebugRender ? NULL == m_apexRenderDebug : true);
	PX_ASSERT(NULL == m_apexRenderer);
	PX_ASSERT(NULL == m_resourceCallback);
	PX_ASSERT(NULL == m_apexSDK);
}

void ApexHelloWorld::collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
{
	using namespace SampleFramework;

	std::vector<const char*> inputDescriptions;
	collectInputDescriptions(inputDescriptions);

	DIGITAL_INPUT_EVENT_DEF_2	(SCENE_0,						WKEY_0,						XKEY_0,			PS3KEY_0,			AKEY_UNKNOWN,	OSXKEY_0,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_0);
	DIGITAL_INPUT_EVENT_DEF_2	(SCENE_1,						WKEY_1,						XKEY_1,			PS3KEY_1,			ABUTTON_2,		OSXKEY_1,			PSP2KEY_UNKNOWN,	IBUTTON_2,		LINUXKEY_1);
	DIGITAL_INPUT_EVENT_DEF_2	(SCENE_2,						WKEY_2,						XKEY_2,			PS3KEY_2,			ABUTTON_1,		OSXKEY_2,			PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_2);
#if APEX_USE_PARTICLES
	DIGITAL_INPUT_EVENT_DEF_2	(SCENE_3,						WKEY_3,						XKEY_3,			PS3KEY_3,			ABUTTON_3,		OSXKEY_3,			PSP2KEY_UNKNOWN,	IBUTTON_3,		LINUXKEY_3);
#endif // APEX_USE_PARTICLES
	DIGITAL_INPUT_EVENT_DEF_2	(INCREASE_SPINNING_VELOCITY,	WKEY_J,						XKEY_J,			PS3KEY_J,			AKEY_UNKNOWN,	OSXKEY_J,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_J);
	DIGITAL_INPUT_EVENT_DEF_2	(DECREASE_SPINNING_VELOCITY,	WKEY_K,						XKEY_K,			PS3KEY_K,			AKEY_UNKNOWN,	OSXKEY_K,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_K);
	ANALOG_INPUT_EVENT_DEF_2	(RAYCAST_HIT,					MOUSE_BUTTON_RIGHT_DOWN,	XKEY_UNKNOWN,	PS3KEY_UNKNOWN,		AKEY_UNKNOWN,	MOUSE_MOVE,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	MOUSE_BUTTON_RIGHT_DOWN);
	DIGITAL_INPUT_EVENT_DEF_2	(BOX_SHOT,						WKEY_SPACE,					XKEY_SPACE,		PS3KEY_SPACE,		AKEY_UNKNOWN,	OSXKEY_SPACE,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_SPACE);	
#if APEX_USE_PARTICLES
	DIGITAL_INPUT_EVENT_DEF_2	(CHANGE_PARTICLE_COUNT,			WKEY_L,						XKEY_L,			PS3KEY_L,			AKEY_UNKNOWN,	OSXKEY_L,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_L);
#endif // APEX_USE_PARTICLES

	SampleApexApplication::collectInputEvents(inputEvents);
}

void ApexHelloWorld::collectInputDescriptions(std::vector<const char*>& inputDescriptions)
{
	SampleApexApplication::collectInputDescriptions(inputDescriptions);
	inputDescriptions.insert(inputDescriptions.end(), ApexHelloWorldEventDescriptions, ApexHelloWorldEventDescriptions + PX_ARRAY_SIZE(ApexHelloWorldEventDescriptions));
}

void ApexHelloWorld::onAnalogInputEvent(const SampleFramework::InputEvent& ie, float val)
{
	SampleApexApplication::onAnalogInputEvent(ie, val);
}

bool ApexHelloWorld::onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
{
    const bool validInput =
        m_BenchmarkMode ? false :
        !val            ? false :
        true;

	if(validInput)
	{
		switch(ie.m_Id)
		{
		case SCENE_0:
		case SCENE_1:
		case SCENE_2:
#if APEX_USE_PARTICLES
		case SCENE_3:
#endif // APEX_USE_PARTICLES
			{
				physx::PxU32 sceneId = ie.m_Id - SCENE_0;
                PX_ASSERT(sceneId < ApexHelloWorld::SceneIdCount);
				if(sceneId < ApexHelloWorld::SceneIdCount)
				{
					unloadScene();
					loadScene(static_cast<ApexHelloWorld::SceneId>(sceneId));
				}
			}
			break;
		case INCREASE_SPINNING_VELOCITY:
		case DECREASE_SPINNING_VELOCITY:
			{
				if(ApexHelloWorld::SceneIdClothing == currentSceneId)
				{
					// change spinning velocity
					for(std::vector<UserActorEntity*>::const_iterator kIter = userActorContainer.begin(); kIter != userActorContainer.end(); ++kIter)
					{
						PX_ASSERT(NULL != *kIter);
						if((*kIter)->isType(UserActorEntity::IdUserSpinningCoatActor))
						{
							(static_cast<UserSpinningCoatActor*>(*kIter))->changeSpinningVelocity(INCREASE_SPINNING_VELOCITY == ie.m_Id);
						}
					}
				}
			}
			break;
		case BOX_SHOT:
			{
				if(ApexHelloWorld::SceneIdDestructible == currentSceneId)
				{
                    // launch a box projectile
                    const SampleRenderer::RendererMesh * const boxmesh = NULL != m_unitBox ? m_unitBox->getMesh() : NULL;
                    const SampleRenderer::RendererMaterial * const material = NULL != m_simpleLitMaterial ? m_simpleLitMaterial->getMaterial() : NULL;
                    PX_ASSERT(NULL != boxmesh && NULL != material);
                    if(NULL != boxmesh && NULL != material)
                    {
                        physx::PxVec3 position = getEyeTransform().getPosition();
                        physx::PxVec3 velocity = getEyeTransform().column2.getXYZ() * -20.0f;
                        physx::PxVec3 extents(0.3f, 0.3f, 0.3f);
						
						// cap number of boxes in scene
						static physx::PxU32 boxShotCount = 0;
						const physx::PxU32 boxDisplayCap = 16;
						if(m_actors.empty())
						{
							boxShotCount = 0;
						}
						const physx::PxU32 boxPlacementIndex = boxShotCount++ % boxDisplayCap;						
						boxShotCount > boxDisplayCap ? m_actors[boxPlacementIndex]->release() : m_actors.push_back(NULL);
						PX_ASSERT(m_actors.size() <= boxDisplayCap && m_actors.size() > boxPlacementIndex);
						m_actors[boxPlacementIndex] = ::new SampleBoxActor(m_renderer, *m_simpleLitMaterial, *m_apexScene->getPhysXScene(), position, velocity, extents, 100, NULL, true);
					}
				}
			}
			break;
#if APEX_USE_PARTICLES
		case CHANGE_PARTICLE_COUNT:
			{
				if(ApexHelloWorld::SceneIdEmitter == currentSceneId)
				{
					// change particle count
					for(std::vector<UserActorEntity*>::const_iterator kIter = userActorContainer.begin(); kIter != userActorContainer.end(); ++kIter)
					{
						PX_ASSERT(NULL != *kIter);
						if((*kIter)->isType(UserActorEntity::IdUserParticleSpawningActor))
						{
							(static_cast<UserParticleSpawningActor*>(*kIter))->changeRenderCount(*apexEmitterManager.getModule());
						}
					}
				}
			}
			break;
#endif // APEX_USE_PARTICLES
		case NEXT_VISUALIZATION:
		case PREV_VISUALIZATION:
            // overwrite base class implementation, option to avoid using debug render
            verifyOk(SampleApexApplication::onDigitalInputEvent(ie, useDebugRender));
		default:
            verifyOk(SampleApexApplication::onDigitalInputEvent(ie, val));
		};
	}
	return true; // always return true, even if !validInput
}

void ApexHelloWorld::onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy)
{
    switch(ie.m_Id)
    {
    case MOUSE_SELECTION_BUTTON:
        {
            if(ApexHelloWorld::SceneIdDestructible == currentSceneId)
            {
                // build ray
                physx::PxU32 windowWidth  = 0;
                physx::PxU32 windowHeight = 0;
                m_renderer->getWindowSize(windowWidth, windowHeight);
                physx::PxVec3 nearPos = unproject(m_projection, physx::PxTransform(getViewTransform()), (x / static_cast<physx::PxF32>(windowWidth)) * 2 - 1, (y / static_cast<physx::PxF32>(windowHeight)) * 2 - 1);
                physx::PxVec3 eyePos = getEyeTransform().getPosition();
                physx::PxVec3 pickDir = (nearPos - eyePos).getNormalized();

                for(std::vector<UserActorEntity*>::const_iterator kIter = userActorContainer.begin(); kIter != userActorContainer.end(); ++kIter)
                {
                    PX_ASSERT(NULL != *kIter);
                    if((*kIter)->isType(UserActorEntity::IdUserFracturableWallActor))
                    {
                        (static_cast<UserFracturableWallActor*>(*kIter))->processRaycast(*apexDestructibleManager.getModule(), *m_apexScene, eyePos, pickDir);
                    }
                }
            }
        }
        break;
    default:
        SampleApexApplication::onPointerInputEvent(ie, x, y, dx, dy);
    }
}

unsigned int ApexHelloWorld::getNumScenes() const
{
    return static_cast<unsigned int>(ApexHelloWorld::SceneIdCount);
}

unsigned int ApexHelloWorld::getSelectedScene() const
{
    PX_ASSERT(currentSceneId < ApexHelloWorld::SceneIdCount);
	return static_cast<unsigned int>(currentSceneId);
}

const char * ApexHelloWorld::getSceneName(unsigned int sceneNumber) const
{
    switch(static_cast<ApexHelloWorld::SceneId>(sceneNumber))
	{
    case ApexHelloWorld::SceneIdCommandLine:
		return "CommandLine";
    case ApexHelloWorld::SceneIdClothing:
		return "Clothing";
    case ApexHelloWorld::SceneIdDestructible:
		return "Destructible";
#if APEX_USE_PARTICLES
    case ApexHelloWorld::SceneIdEmitter:
		return "Emitter";
#endif // APEX_USE_PARTICLES
	default:
        PX_ASSERT(!"invalid sceneNumber!");
        return NULL;
	}
}

void ApexHelloWorld::selectScene(unsigned int sceneNumber)
{
    PX_ASSERT(static_cast<ApexHelloWorld::SceneId>(sceneNumber) < ApexHelloWorld::SceneIdCount);
    if(static_cast<ApexHelloWorld::SceneId>(sceneNumber) < ApexHelloWorld::SceneIdCount)
    {
        unloadScene();
        loadScene(static_cast<ApexHelloWorld::SceneId>(sceneNumber));
    }
}

void ApexHelloWorld::loadModules()
{
	// create modules
	verifyOk(apexClothingManager.onCreateModule(*m_apexSDK));
	verifyOk(apexDestructibleManager.onCreateModule(*m_apexSDK));
#if APEX_USE_PARTICLES
	verifyOk(apexEmitterManager.onCreateModule(*m_apexSDK));
	verifyOk(apexIofxManager.onCreateModule(*m_apexSDK));
	verifyOk(apexIosManager.onCreateModule(*m_apexSDK));
#endif // APEX_USE_PARTICLES

	// create legacy modules
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleCommonLegacy();
	physx::apex::instantiateModuleFrameworkLegacy();
	physx::apex::instantiateModuleClothingLegacy();
	physx::apex::instantiateModuleDestructibleLegacy();
#if APEX_USE_PARTICLES
	physx::apex::instantiateModuleEmitterLegacy();
	physx::apex::instantiateModuleIOFXLegacy();
#endif // APEX_USE_PARTICLES
#endif // APEX_MODULES_STATIC_LINK
	verifyOk(loadModule("Common_Legacy"));
	verifyOk(loadModule("Framework_Legacy"));
	verifyOk(loadModule("Clothing_Legacy"));
	verifyOk(loadModule("Destructible_Legacy"));
#if APEX_USE_PARTICLES
	verifyOk(loadModule("Emitter_Legacy"));
	verifyOk(loadModule("IOFX_Legacy"));
#endif // APEX_USE_PARTICLES

    // set resource callback support
	m_resourceCallback->setApexSupport(*m_apexSDK);
}

void ApexHelloWorld::unloadModules()
{
	// release legacy modules
	while(!m_apexModuleList.empty())
	{
        PX_ASSERT(NULL != m_apexModuleList.back());
        m_apexModuleList.back()->release();
        m_apexModuleList.pop_back();
	}

	// release modules
#if APEX_USE_PARTICLES
	verifyOk(apexIosManager.onReleaseModule(*m_apexSDK));
	verifyOk(apexIofxManager.onReleaseModule(*m_apexSDK));
	verifyOk(apexEmitterManager.onReleaseModule(*m_apexSDK));
#endif // APEX_USE_PARTICLES
	verifyOk(apexDestructibleManager.onReleaseModule(*m_apexSDK));
	verifyOk(apexClothingManager.onReleaseModule(*m_apexSDK));
}

void ApexHelloWorld::loadAssets()
{
	// load search paths for assets
	std::string sampleResourceDir;
	sampleResourceDir = m_resourceDir + "/SimpleClothing/";
	m_resourceCallback->addResourceSearchPath(sampleResourceDir.c_str());
	SampleFramework::addSearchPath(sampleResourceDir.c_str());
	sampleResourceDir = m_resourceDir + "/SimpleDestruction/";
	m_resourceCallback->addResourceSearchPath(sampleResourceDir.c_str());
	SampleFramework::addSearchPath(sampleResourceDir.c_str());
#if APEX_USE_PARTICLES
	sampleResourceDir = m_resourceDir + "/SimpleParticlesTest/";
	m_resourceCallback->addResourceSearchPath(sampleResourceDir.c_str());
	SampleFramework::addSearchPath(sampleResourceDir.c_str());
#endif // APEX_USE_PARTICLES

	// create assets
	verifyOk(apexClothingManager.onCreateAssets(*this));
	verifyOk(apexDestructibleManager.onCreateAssets(*this));
#if APEX_USE_PARTICLES
	verifyOk(apexEmitterManager.onCreateAssets(*this));
#endif // APEX_USE_PARTICLES
}

void ApexHelloWorld::unloadAssets()
{
	// release assets
	PX_ASSERT(userActorContainer.empty());
#if APEX_USE_PARTICLES
	verifyOk(apexEmitterManager.onReleaseAssets(*this));
#endif // APEX_USE_PARTICLES
	verifyOk(apexDestructibleManager.onReleaseAssets(*this));
	verifyOk(apexClothingManager.onReleaseAssets(*this));
	
	// unload search paths for assets
	SampleFramework::clearSearchPaths();
	m_resourceCallback->clearResourceSearchPaths();
}

void ApexHelloWorld::loadActors()
{
	UserActorEntity * userActor = NULL;
	switch(currentSceneId)
	{
	case ApexHelloWorld::SceneIdCommandLine:
		break;
	case ApexHelloWorld::SceneIdClothing:
		{
			// cloth actor
			userActor = UserSpinningCoatActor::instantiate(apexClothingManager, *m_apexScene);
			PX_ASSERT(NULL != userActor);
			if(NULL != userActor)
			{
				userActorContainer.push_back(userActor);
				userActor = NULL;
			}
		}
		break;
	case ApexHelloWorld::SceneIdDestructible:
		{
			// wall actor
			userActor = UserFracturableWallActor::instantiate(apexDestructibleManager, *m_apexScene);
			PX_ASSERT(NULL != userActor);
			if(NULL != userActor)
			{
				userActorContainer.push_back(userActor);
				userActor = NULL;
			}

#if APEX_USE_PARTICLES
			// render volume encompassing wall actor's debris
			userActor = UserParticleEffectController::instantiate(apexIofxManager, *m_apexScene);
			PX_ASSERT(NULL != userActor);
			if(NULL != userActor)
			{
				userActorContainer.push_back(userActor);
				userActor = NULL;
			}
#endif // APEX_USE_PARTICLES
		}
		break;
#if APEX_USE_PARTICLES
	case ApexHelloWorld::SceneIdEmitter:
		{
			// emitter actor
			userActor = UserParticleSpawningActor::instantiate(apexEmitterManager, *m_apexScene);
			PX_ASSERT(NULL != userActor);
			if(NULL != userActor)
			{
				userActorContainer.push_back(userActor);
				userActor = NULL;
			}

			// render volume encompassing emitter actor
			userActor = UserParticleEffectController::instantiate(apexIofxManager, *m_apexScene);
			PX_ASSERT(NULL != userActor);
			if(NULL != userActor)
			{
				userActorContainer.push_back(userActor);
				userActor = NULL;
			}
		}
		break;
#endif // APEX_USE_PARTICLES
	default:
		PX_ASSERT(!"invalid sceneId!");
	}
}

void ApexHelloWorld::unloadActors()
{
	while(!m_actors.empty())
	{
        PX_ASSERT(ApexHelloWorld::SceneIdDestructible == currentSceneId);
        PX_ASSERT(NULL != m_actors.back());
		m_actors.back()->release();
		m_actors.pop_back();
	}
	while(!userActorContainer.empty())
	{
        PX_ASSERT(NULL != userActorContainer.back());
		userActorContainer.back()->onDestroy();
		userActorContainer.pop_back();
	}
}

void ApexHelloWorld::loadStaticScene()
{
    // load floor grid
	PX_ASSERT(m_shape_actors.empty());
    verifyOk(addCollisionShape(HalfSpaceShapeType, physx::PxVec3(0, 0, 0), physx::PxVec3(0, 1, 0), physx::PxVec3(0, 0, 0)));

    // load light
    SampleRenderer::RendererDirectionalLightDesc lightDesc;
    lightDesc.intensity = 1;
    lightDesc.color     = SampleRenderer::RendererColor(230, 230, 230, 255);
    lightDesc.direction = physx::PxVec3(-1, -2, -3).getNormalized();
    SampleRenderer::RendererLight * rendererLight = NULL;
    rendererLight = m_renderer->createLight(lightDesc);
    PX_ASSERT(NULL != rendererLight);
    if(NULL != rendererLight)
    {
		PX_ASSERT(m_lights.empty());
        m_lights.push_back(rendererLight);
        rendererLight = NULL;
    }
}

void ApexHelloWorld::unloadStaticScene()
{
    while(!m_lights.empty())
    {
        PX_ASSERT(NULL != m_lights.back());
        m_lights.back()->release();
        m_lights.pop_back();
    }
    while(!m_shape_actors.empty())
    {
        PX_ASSERT(NULL != m_shape_actors.back());
        m_shape_actors.back()->release();
        m_shape_actors.pop_back();
    }
}

void ApexHelloWorld::loadScene(ApexHelloWorld::SceneId sceneId)
{
	PX_ASSERT(ApexHelloWorld::SceneIdCount == currentSceneId && sceneId < ApexHelloWorld::SceneIdCount);
	currentSceneId = sceneId;
	loadActors();
	setUpDebugRenderDisplay();
}

void ApexHelloWorld::unloadScene()
{
	PX_ASSERT(ApexHelloWorld::SceneIdCount != currentSceneId);
	unloadActors();
    currentSceneId = ApexHelloWorld::SceneIdCount;
}

void ApexHelloWorld::setUpCollision()
{
	// set up collision scene
#if NX_SDK_VERSION_MAJOR == 2
	PX_ASSERT(NULL != m_apexScene);
	m_apexScene->getPhysXScene()->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_SWAP_AND);
	m_apexScene->getPhysXScene()->setFilterBool(true);
	NxGroupsMask zeroMask;
	zeroMask.bits0 = zeroMask.bits1 = zeroMask.bits2 = zeroMask.bits3 = 0;
	m_apexScene->getPhysXScene()->setFilterConstant0(zeroMask);
	m_apexScene->getPhysXScene()->setFilterConstant1(zeroMask);
	
	m_shapeGroupMask = MaskManager::getMask(MaskManager::GroundPlane);
#endif // NX_SDK_VERSION_MAJOR == 2

	// set up collision for destructible scene
	{
#if NX_SDK_VERSION_MAJOR == 2
		m_resourceCallback->registerNxCollisionGroup("PlaneAndDestructibleGroup", DummyManager::getMask());
		NxGroupsMask g = MaskManager::getMask(MaskManager::CrumbleEmitter);
		m_resourceCallback->registerNxGroupsMask128("PlaneAndDestructibleGroup", g);
#elif NX_SDK_VERSION_MAJOR == 3
		m_resourceCallback->registerSimulationFilterData("PlaneAndDestructibleGroup", MaskManager::getMask(MaskManager::CrumbleEmitter));
#endif // NX_SDK_VERSION_MAJOR
	}

	// set up collision for emitter scene
	{
#if NX_SDK_VERSION_MAJOR == 2
		m_resourceCallback->registerNxCollisionGroup("SPRITE_COL_GRP_MASK", DummyManager::getMask());
		NxGroupsMask g = MaskManager::getMask(MaskManager::SpriteEmitter);
		m_resourceCallback->registerNxGroupsMask128("SPRITE_COL_GRP_MASK", g);
#elif NX_SDK_VERSION_MAJOR == 3
		m_resourceCallback->registerSimulationFilterData("MESH_COL_GRP_MASK", MaskManager::getMask(MaskManager::SpriteEmitter));
#endif // NX_SDK_VERSION_MAJOR
	}
}

void ApexHelloWorld::setUpLevelOfDetail()
{
	setBudget(1000.0f);
	const float lodValue = 200.0f;

	apexClothingManager.getModule()->setLODBenefitValue(lodValue);
	apexClothingManager.getModule()->setLODEnabled(true);
	apexDestructibleManager.getModule()->setLODBenefitValue(lodValue);
	apexDestructibleManager.getModule()->setLODEnabled(true);
#if APEX_USE_PARTICLES
	apexEmitterManager.getModule()->setLODBenefitValue(lodValue);
	apexEmitterManager.getModule()->setLODEnabled(true);
	apexIofxManager.getModule()->setLODBenefitValue(lodValue);
	apexIofxManager.getModule()->setLODEnabled(true);
	apexIosManager.getModule()->setLODBenefitValue(lodValue);
	//apexIosManager.getModule()->setLODEnabled(true); // no suitable LOD in place yet. defaults to false
#endif // APEX_USE_PARTICLES
}

void ApexHelloWorld::setUpViewProjectionMatrices()
{
	// build projection matrix
	bool windowResized = false;
	const physx::PxF32 fieldOfView = 45.0f;
	const physx::PxF32 nearPlaneDistance = 1.0f;
	const physx::PxF32 farPlaneDistance = 10000.0f;
	static physx::PxU32 currentWindowWidth = 0;
	static physx::PxU32 currentWindowHeight = 0;
	physx::PxU32 nextWindowWidth = 0;
	physx::PxU32 nextWindowHeight = 0;
	m_renderer->getWindowSize(nextWindowWidth, nextWindowHeight);
	PX_ASSERT(nextWindowWidth > 0);
	PX_ASSERT(nextWindowHeight > 0);
	if(currentWindowWidth != nextWindowWidth || currentWindowHeight != nextWindowHeight)
	{
		windowResized = true;
		currentWindowWidth = nextWindowWidth;
		currentWindowHeight = nextWindowHeight;
		m_projection = SampleRenderer::RendererProjection(fieldOfView, currentWindowWidth / static_cast<physx::PxF32>(currentWindowHeight), nearPlaneDistance, farPlaneDistance);
	}
	physx::PxF32 projArray[16];
	m_projection.getColumnMajor44(projArray);

	// set view and projection matrices for user debug-render
	if(useDebugRender)
	{
		m_apexRenderDebug->setViewMatrix(getViewTransform().front());
		if(windowResized)
		{
			m_apexRenderDebug->setProjectionMatrix(projArray);
		}
	}

	// set view and projection matrices for debug-render scene
	physx::PxMat44 projMatrix;
	projMatrix.column0 = physx::PxVec4(projArray + 0);
	projMatrix.column1 = physx::PxVec4(projArray + 4);
	projMatrix.column2 = physx::PxVec4(projArray + 8);
	projMatrix.column3 = physx::PxVec4(projArray + 12);
	static const physx::PxU32 viewIDlookAtRightHand = m_apexScene->allocViewMatrix(physx::apex::ViewMatrixType::LOOK_AT_RH);
	static const physx::PxU32 projIDperspectiveCubicRightHand = m_apexScene->allocProjMatrix(physx::apex::ProjMatrixType::USER_CUSTOMIZED);
	m_apexScene->setViewMatrix(getViewTransform(), viewIDlookAtRightHand);
	if(windowResized)
	{
		m_apexScene->setProjMatrix(projMatrix, projIDperspectiveCubicRightHand);
		m_apexScene->setProjParams(nearPlaneDistance, farPlaneDistance, fieldOfView, currentWindowWidth, currentWindowHeight);
	}
	m_apexScene->setUseViewProjMatrix(viewIDlookAtRightHand, projIDperspectiveCubicRightHand);
}

void ApexHelloWorld::setUpDebugRenderDisplay()
{
	if(useDebugRender)
	{
		const physx::PxF32 floatTrue = 1.0f;
		const physx::PxF32 floatFalse = -1.0f;

		// toggle debug-render visualizations
		{
			applyApexDebugRenderFlag(m_apexScene, "VISUALIZATION_ENABLE", floatTrue);
			applyApexDebugRenderFlag(m_apexScene, "VISUALIZATION_SCALE", 1.0f);

			// enable specific module debug-render visualizations
			applyApexDebugRenderFlag(m_apexScene, "Clothing/VISUALIZE_CLOTHING_ACTOR", ApexHelloWorld::SceneIdClothing == currentSceneId ? floatTrue : floatFalse);
			applyApexDebugRenderFlag(m_apexScene, "Destructible/VISUALIZE_DESTRUCTIBLE_ACTOR", ApexHelloWorld::SceneIdDestructible == currentSceneId ? floatTrue : floatFalse);
#if APEX_USE_PARTICLES
			applyApexDebugRenderFlag(m_apexScene, "Emitter/apexEmitterParameters.VISUALIZE_APEX_EMITTER_ACTOR", ApexHelloWorld::SceneIdEmitter == currentSceneId ? floatTrue : floatFalse);
			applyApexDebugRenderFlag(m_apexScene, "Iofx/VISUALIZE_IOFX_ACTOR", ApexHelloWorld::SceneIdEmitter == currentSceneId ? floatTrue : floatFalse);
#endif // APEX_USE_PARTICLES
		}

		// set up debug-render scenes
		{
			m_debugRenderConfigs.clear();
			DebugRenderConfiguration config;

			// set up common physx debug-render scene
			PX_ASSERT(config.flags.empty());
#if NX_SDK_VERSION_MAJOR == 2
			config.flags.push_back(DebugRenderFlag("Physx - Collision Shapes", NX_VISUALIZE_COLLISION_SHAPES, floatTrue));
#elif NX_SDK_VERSION_MAJOR == 3
			config.flags.push_back(DebugRenderFlag("Physx - Collision Shapes", physx::PxVisualizationParameter::eCOLLISION_SHAPES, floatTrue));
#endif // NX_SDK_VERSION_MAJOR
			m_debugRenderConfigs.push_back(config);
			config.flags.clear();

			// set up common apex debug-render scene
			PX_ASSERT(config.flags.empty());
			config.flags.push_back(DebugRenderFlag("Apex - LOD Benefits", "VISUALIZE_LOD_BENEFITS", 1.0f));
			m_debugRenderConfigs.push_back(config);
			config.flags.clear();

			// set up specific module debug-render scene
			switch(currentSceneId)
			{
			case ApexHelloWorld::SceneIdCommandLine:
				break;
			case ApexHelloWorld::SceneIdClothing:
				{
					PX_ASSERT(config.flags.empty());
					config.flags.push_back(DebugRenderFlag("Apex - Clothing Physics Mesh (Wire)", "Clothing/VISUALIZE_CLOTHING_PHYSICS_MESH_WIRE", 1.0f));
					m_debugRenderConfigs.push_back(config);
					config.flags.clear();
				}
				break;
			case ApexHelloWorld::SceneIdDestructible:
				{
					PX_ASSERT(config.flags.empty());
					config.flags.push_back(DebugRenderFlag("Apex - Destructible Support", "Destructible/VISUALIZE_DESTRUCTIBLE_SUPPORT", 1.0f));
					m_debugRenderConfigs.push_back(config);
					config.flags.clear();
				}
				break;
#if APEX_USE_PARTICLES
			case ApexHelloWorld::SceneIdEmitter:
				{
					PX_ASSERT(config.flags.empty());
					config.flags.push_back(DebugRenderFlag("Apex - Emitter Actor Pose", "Emitter/apexEmitterParameters.VISUALIZE_APEX_EMITTER_ACTOR_POSE", floatTrue));
					config.flags.push_back(DebugRenderFlag("Apex - Emitter Name", "Emitter/apexEmitterParameters.VISUALIZE_APEX_EMITTER_ACTOR_NAME", floatTrue));
					m_debugRenderConfigs.push_back(config);
					config.flags.clear();

					PX_ASSERT(config.flags.empty());
					config.flags.push_back(DebugRenderFlag("Apex - IOFX Bounding Box", "Iofx/VISUALIZE_IOFX_BOUNDING_BOX", floatTrue));
					config.flags.push_back(DebugRenderFlag("Apex - IOFX Name", "Iofx/VISUALIZE_IOFX_ACTOR_NAME", floatTrue));
					m_debugRenderConfigs.push_back(config);
					config.flags.clear();
				}
				break;
#endif // APEX_USE_PARTICLES
			default:
				PX_ASSERT(!"invalid sceneId!");
			}
		}

		m_currentDebugRenderConfig = m_debugRenderConfigs.size();
		applyDebugRenderConfig(m_currentDebugRenderConfig, m_apexScene, m_sceneSize);
	}
}

#if NX_SDK_VERSION_MAJOR == 2
NxCollisionGroup ApexHelloWorld::CollisionGroupFiltering::System1::getMask(System1::Enum collisionObjectID)
{
	return static_cast<NxCollisionGroup>(collisionObjectID);
}

NxGroupsMask ApexHelloWorld::CollisionGroupFiltering::System2::getMask(System2::Enum collisionObjectID)
{
	PX_ASSERT(System2::Count < 32);
	NxGroupsMask g;
	g.bits0 = g.bits1 = g.bits3 = 0;
	g.bits2 = ~0;
	switch(collisionObjectID)
	{
	case GroundPlane:
		g.bits0 = (1 << static_cast<NxU32>(GroundPlane));
		break;
	case CrumbleEmitter:
		g.bits0 = (1 << static_cast<NxU32>(CrumbleEmitter));
		break;
	case SpriteEmitter:
		g.bits0 = (1 << static_cast<NxU32>(SpriteEmitter));
		break;
	default:
		PX_ASSERT(!"invalid collisionObjectID!");
	}
	return g;
}

#elif NX_SDK_VERSION_MAJOR == 3
physx::PxFilterData ApexHelloWorld::CollisionGroupFiltering::System2::getMask(System2::Enum collisionObjectID)
{
	PX_ASSERT(System2::Count < 32);
	physx::PxFilterData g;
	g.word0 = g.word1 = g.word3 = 0;
	g.word2 = ~0;
	switch(collisionObjectID)
	{
	case GroundPlane:
		g.word0 = (1 << static_cast<physx::PxU32>(GroundPlane));
		break;
	case CrumbleEmitter:
		g.word0 = (1 << static_cast<physx::PxU32>(CrumbleEmitter));
		break;
	case SpriteEmitter:
		g.word0 = (1 << static_cast<physx::PxU32>(SpriteEmitter));
		break;
	default:
		PX_ASSERT(!"invalid collisionObjectID!");
	}
	return g;
}
#endif //NX_SDK_VERSION_MAJOR
