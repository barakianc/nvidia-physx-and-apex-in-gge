// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.

#ifndef APEX_HELLO_WORLD_CONFIG_H
#define APEX_HELLO_WORLD_CONFIG_H

#include <vector>

#include "foundation/Px.h"
#include "foundation/PxMat44.h"
#include "NxPhysXSDKVersion.h"
#include "NxApex.h"

class SampleApexApplication;

namespace NxParameterized
{
	class Interface;
}; // namespace NxParameterized

namespace physx
{
namespace apex
{
	class NxUserRenderer;

	class NxApexSDK;
	class NxApexScene;
	class NxModule;
	class NxApexAsset;
	class NxApexActor;

    // apex clothing forward declarations
	class NxModuleClothing;
    class NxClothingAsset;
    class NxClothingActor;

    // apex destruction forward declarations
	class NxModuleDestructible;
    class NxDestructibleAsset;
    class NxDestructibleActor;

#if APEX_USE_PARTICLES
    // apex particle forward declarations
	class NxModuleEmitter;
	class NxModuleIofx;
#if NX_SDK_VERSION_MAJOR == 2
	class NxModuleFluidIos;
#elif NX_SDK_VERSION_MAJOR == 3
	class NxModuleParticleIos;
#endif // NX_SDK_VERSION_MAJOR
    class NxApexEmitterAsset;
    class NxApexEmitterActor;
    class NxApexRenderVolume;
#endif // APEX_USE_PARTICLES
}; // namespace apex
}; // namespace physx

#pragma warning(push)
#pragma warning(disable:4265) // class has virtual functions, but destructor is not virtual

#define DECLARE_COPY_ASSIGN(Class)	Class(const Class &);				\
									Class & operator= (const Class &);

namespace ApexHelloWorldNamespace
{
/************************* Apex Singleton Resources *************************/
class ApexModuleEntity
{
protected:
	explicit ApexModuleEntity(const char * moduleName);
	~ApexModuleEntity();

	bool										onCreateModule(physx::apex::NxApexSDK & sdk);
	bool										onReleaseModule(physx::apex::NxApexSDK & sdk);
	const physx::apex::NxModule *				getModuleConst() const;
	physx::apex::NxModule *						getModule();
private:
	ApexModuleEntity();
	DECLARE_COPY_ASSIGN(ApexModuleEntity)

	virtual void								initializeModuleParams(NxParameterized::Interface & moduleParams) const;

	const char * const							moduleName;
	physx::apex::NxModule *						module;
};

class ApexAssetEntity
{
protected:
	explicit ApexAssetEntity(const char * assetAuthoringName);
	~ApexAssetEntity();

	bool										onCreateAsset(SampleApexApplication & app, const char * assetFileName, unsigned int assetIndex);
	bool										onReleaseAsset(SampleApexApplication & app, unsigned int assetIndex);
	const physx::apex::NxApexAsset *			getAssetConst(unsigned int assetIndex) const;
	physx::apex::NxApexAsset *					getAsset(unsigned int assetIndex);
	physx::apex::NxApexActor *					instantiateActor(physx::apex::NxApexScene & scene, unsigned int actorId);
private:
	ApexAssetEntity();
	DECLARE_COPY_ASSIGN(ApexAssetEntity)

	virtual void								initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const;
	virtual unsigned int						lookUpAssetIndex(unsigned int actorId) const = 0;

	const char * const							assetAuthoringName;
	std::vector<physx::apex::NxApexAsset*>		assetContainer;
};

/************************* Apex Entity Managers *************************/
class ApexClothingManager : private ApexModuleEntity, private ApexAssetEntity
{
public:
	ApexClothingManager();
	~ApexClothingManager();
	enum AssetId
	{
		AssetIdTrenchcoat = 0,
	};
	enum ActorId
	{
		ActorIdTrenchcoat_0 = 0,
	};

	bool										onCreateModule(physx::apex::NxApexSDK & sdk);
	bool										onReleaseModule(physx::apex::NxApexSDK & sdk);
	bool										onCreateAssets(SampleApexApplication & app);
	bool										onReleaseAssets(SampleApexApplication & app);

	const physx::apex::NxModuleClothing *		getModuleConst() const;
	physx::apex::NxModuleClothing *				getModule();
	const physx::apex::NxClothingAsset *		getAssetConst(ApexClothingManager::AssetId assetId) const;
	physx::apex::NxClothingAsset *				getAsset(ApexClothingManager::AssetId assetId);

	physx::apex::NxClothingActor *				instantiateActor(physx::apex::NxApexScene & scene, ApexClothingManager::ActorId actorId);
private:
	DECLARE_COPY_ASSIGN(ApexClothingManager)

	/*virtual*/ void							initializeModuleParams(NxParameterized::Interface & moduleParams) const;
	/*virtual*/ void							initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const;
	/*virtual*/ unsigned int					lookUpAssetIndex(unsigned int actorId) const;
};

class ApexDestructibleManager : private ApexModuleEntity, private ApexAssetEntity
{
public:
	ApexDestructibleManager();
	~ApexDestructibleManager();
	enum AssetId
	{
		AssetIdWall = 0,
	};
	enum ActorId
	{
		ActorIdWall_0 = 0,
	};

	bool										onCreateModule(physx::apex::NxApexSDK & sdk);
	bool										onReleaseModule(physx::apex::NxApexSDK & sdk);
	bool										onCreateAssets(SampleApexApplication & app);
	bool										onReleaseAssets(SampleApexApplication & app);

	const physx::apex::NxModuleDestructible *	getModuleConst() const;
	physx::apex::NxModuleDestructible *			getModule();
	const physx::apex::NxDestructibleAsset *	getAssetConst(ApexDestructibleManager::AssetId assetId) const;
	physx::apex::NxDestructibleAsset *			getAsset(ApexDestructibleManager::AssetId assetId);

	physx::apex::NxDestructibleActor *			instantiateActor(physx::apex::NxApexScene & scene, ApexDestructibleManager::ActorId actorId);
private:
	DECLARE_COPY_ASSIGN(ApexDestructibleManager)
	
	/*virtual*/ void							initializeModuleParams(NxParameterized::Interface & moduleParams) const;
	/*virtual*/ void							initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const;
	/*virtual*/ unsigned int					lookUpAssetIndex(unsigned int actorId) const;
};

#if APEX_USE_PARTICLES
class ApexEmitterManager : private ApexModuleEntity, private ApexAssetEntity
{
public:
    ApexEmitterManager();
    ~ApexEmitterManager();
    enum AssetId
    {
        AssetIdSprite = 0,
    };
    enum ActorId
    {
        ActorIdSprite_0 = 0,
    };

    bool										onCreateModule(physx::apex::NxApexSDK & sdk);
    bool										onReleaseModule(physx::apex::NxApexSDK & sdk);
    bool										onCreateAssets(SampleApexApplication & app);
    bool										onReleaseAssets(SampleApexApplication & app);

    const physx::apex::NxModuleEmitter *		getModuleConst() const;
    physx::apex::NxModuleEmitter *				getModule();
    const physx::apex::NxApexEmitterAsset *		getAssetConst(ApexEmitterManager::AssetId assetId) const;
    physx::apex::NxApexEmitterAsset *			getAsset(ApexEmitterManager::AssetId assetId);

    physx::apex::NxApexEmitterActor *			instantiateActor(physx::apex::NxApexScene & scene, ApexEmitterManager::ActorId actorId);
private:
    DECLARE_COPY_ASSIGN(ApexEmitterManager)

    /*virtual*/ void							initializeModuleParams(NxParameterized::Interface & moduleParams) const;
    /*virtual*/ void							initializeActorParams(NxParameterized::Interface & actorParams, unsigned int actorId) const;
    /*virtual*/ unsigned int					lookUpAssetIndex(unsigned int actorId) const;
};

class ApexIofxManager : private ApexModuleEntity
{
public:
    ApexIofxManager();
    ~ApexIofxManager();
    enum RenderVolumeId
    {
        RenderVolumeIdAxisAlignedBox_0 = 0,
    };

    bool										onCreateModule(physx::apex::NxApexSDK & sdk);
    bool										onReleaseModule(physx::apex::NxApexSDK & sdk);

    const physx::apex::NxModuleIofx *			getModuleConst() const;
    physx::apex::NxModuleIofx *					getModule();

    physx::apex::NxApexRenderVolume *			instantiateRenderVolume(physx::apex::NxApexScene & scene, ApexIofxManager::RenderVolumeId renderVolumeId);
private:
    DECLARE_COPY_ASSIGN(ApexIofxManager)

    /*virtual*/ void							initializeModuleParams(NxParameterized::Interface & moduleParams) const;
};

#if NX_SDK_VERSION_MAJOR == 2
class ApexIosManager : private ApexModuleEntity
{
public:
    ApexIosManager();
    ~ApexIosManager();

    bool										onCreateModule(physx::apex::NxApexSDK & sdk);
    bool										onReleaseModule(physx::apex::NxApexSDK & sdk);

    const physx::apex::NxModuleFluidIos *		getModuleConst() const;
    physx::apex::NxModuleFluidIos *				getModule();
private:
    DECLARE_COPY_ASSIGN(ApexIosManager)

    /*virtual*/ void							initializeModuleParams(NxParameterized::Interface & moduleParams) const;
};

#elif NX_SDK_VERSION_MAJOR == 3
class ApexIosManager : private ApexModuleEntity
{
public:
    ApexIosManager();
    ~ApexIosManager();

    bool										onCreateModule(physx::apex::NxApexSDK & sdk);
    bool										onReleaseModule(physx::apex::NxApexSDK & sdk);

    const physx::apex::NxModuleParticleIos *	getModuleConst() const;
    physx::apex::NxModuleParticleIos *			getModule();
private:
    DECLARE_COPY_ASSIGN(ApexIosManager)

    /*virtual*/ void							initializeModuleParams(NxParameterized::Interface & moduleParams) const;
};
#endif // NX_SDK_VERSION_MAJOR
#endif // APEX_USE_PARTICLES

/************************* Apex Actor Wrappers *************************/
class UserActorEntity
{
public:
	enum Id
	{
		IdUserSpinningCoatActor = 0,
		IdUserFracturableWallActor,
		IdUserParticleSpawningActor,
		IdUserParticleEffectController,
	};

	virtual bool								isType(UserActorEntity::Id id) const = 0;
	virtual void								onDestroy() = 0;
	virtual void								onTick(float dtime);
	virtual void								onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer);
protected:
	UserActorEntity();
	~UserActorEntity();
private:
	DECLARE_COPY_ASSIGN(UserActorEntity);
};

class UserSpinningCoatActor : public UserActorEntity
{
public:
	static UserActorEntity *					instantiate(ApexClothingManager & clothingManager, physx::apex::NxApexScene & scene);

	/*virtual*/	bool							isType(UserActorEntity::Id id) const;
	/*virtual*/ void							onDestroy();
	/*virtual*/ void							onTick(float dtime);
	/*virtual*/ void							onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer);

	const physx::apex::NxClothingActor *		getActorConst() const;
	physx::apex::NxClothingActor *				getActor();
	void										changeRenderLod();
	void										changeSpinningVelocity(bool increase);
private:
	UserSpinningCoatActor(); 
	~UserSpinningCoatActor();
	DECLARE_COPY_ASSIGN(UserSpinningCoatActor)

	void										initialize(const ApexClothingManager & clothingManager);

	physx::apex::NxClothingActor *				clothingActor;
	std::vector<unsigned int>					lodValueContainer;
	int											currentLodIndex;
	bool										increaseLod;
	bool										angleInitDone;
	physx::PxMat44								initialPose;
	int											spinningState;
	float										previousAngle;
	float										angleStepSize;
	float										angleAdjustment;
};

class UserFracturableWallActor : public UserActorEntity
{
public:
	static UserActorEntity *					instantiate(ApexDestructibleManager & destructibleManager, physx::apex::NxApexScene & scene);

	/*virtual*/	bool							isType(UserActorEntity::Id id) const;
	/*virtual*/ void							onDestroy();
	/*virtual*/ void							onTick(float dtime);
	/*virtual*/ void							onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer);

	const physx::apex::NxDestructibleActor *	getActorConst() const;
	physx::apex::NxDestructibleActor *			getActor();
	void										processRaycast(const physx::apex::NxModuleDestructible & module, physx::apex::NxApexScene & scene, const physx::PxVec3 & rayOrigin, const physx::PxVec3 & rayDirection);
private:
	UserFracturableWallActor(); 
	~UserFracturableWallActor();
	DECLARE_COPY_ASSIGN(UserFracturableWallActor)

	void										initialize(const ApexDestructibleManager & destructibleManager);

	physx::apex::NxDestructibleActor *			destructibleActor;
};

#if APEX_USE_PARTICLES 
class UserParticleSpawningActor : public UserActorEntity
{
public:
	static UserActorEntity *					instantiate(ApexEmitterManager & emitterManager, physx::apex::NxApexScene & scene);

	/*virtual*/	bool							isType(UserActorEntity::Id id) const;
	/*virtual*/ void							onDestroy();
	/*virtual*/ void							onTick(float dtime);
	/*virtual*/ void							onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer);

	const physx::apex::NxApexEmitterActor *		getActorConst() const;
	physx::apex::NxApexEmitterActor *			getActor();
	void										changeRenderCount(physx::apex::NxModuleEmitter & module);
private:
	UserParticleSpawningActor(); 
	~UserParticleSpawningActor();
	DECLARE_COPY_ASSIGN(UserParticleSpawningActor)

	void										initialize(const ApexEmitterManager & emitterManager);

	physx::apex::NxApexEmitterActor *			emitterActor;
	unsigned int								currentCountLevel;
};

class UserParticleEffectController : public UserActorEntity
{
public:
	static UserActorEntity *					instantiate(ApexIofxManager & iofxManager, physx::apex::NxApexScene & scene);

	/*virtual*/	bool							isType(UserActorEntity::Id id) const;
	/*virtual*/ void							onDestroy();
	/*virtual*/ void							onTick(float dtime);
	/*virtual*/ void							onRender(bool rewriteBuffers, physx::apex::NxUserRenderer & userRenderer);

	const physx::apex::NxApexRenderVolume *		getRenderVolumeConst() const;
	physx::apex::NxApexRenderVolume *			getRenderVolume();
private:
	UserParticleEffectController(); 
	~UserParticleEffectController();
	DECLARE_COPY_ASSIGN(UserParticleEffectController)

	void										initialize(const ApexIofxManager & iofxManager);
	
	physx::apex::NxApexRenderVolume *			renderVolume;
};

#endif // APEX_USE_PARTICLES
}; // namespace ApexHelloWorldNamespace
#undef DECLARE_COPY_ASSIGN
#pragma warning(pop)
#endif // APEX_HELLO_WORLD_CONFIG_H
