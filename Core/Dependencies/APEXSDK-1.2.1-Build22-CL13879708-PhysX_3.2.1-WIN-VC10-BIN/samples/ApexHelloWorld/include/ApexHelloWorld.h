// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Hello World
// Description: This sample program uses APEX clothing, destructible and particles.
// 1) Shows simple use of modules, assets and actors
// 2) Shows simple use of LOD
// 3) Shows simple physics interaction 
// 4) Shows timings and statistics
// 5) Shows debug visualization
// ===============================================================================

#ifndef APEXHELLOWORLD_H
#define APEXHELLOWORLD_H

#include <vector>
#include <string>

#include "ApexHelloWorldConfig.h"

#include "SampleApexApplication.h"

class ApexHelloWorld : public SampleApexApplication
{
public:
	explicit ApexHelloWorld(const SampleFramework::SampleCommandLine & cmdline);
	~ApexHelloWorld();
private:
	ApexHelloWorld();
	ApexHelloWorld(const ApexHelloWorld &);
	ApexHelloWorld & operator= (const ApexHelloWorld &);

	typedef ApexHelloWorldNamespace::ApexClothingManager			ApexClothingManager;
	typedef ApexHelloWorldNamespace::ApexDestructibleManager		ApexDestructibleManager;
	typedef ApexHelloWorldNamespace::UserActorEntity				UserActorEntity;
	typedef ApexHelloWorldNamespace::UserSpinningCoatActor			UserSpinningCoatActor;
	typedef ApexHelloWorldNamespace::UserFracturableWallActor		UserFracturableWallActor;
#if APEX_USE_PARTICLES
	typedef ApexHelloWorldNamespace::ApexEmitterManager				ApexEmitterManager;
	typedef ApexHelloWorldNamespace::ApexIofxManager				ApexIofxManager;
	typedef ApexHelloWorldNamespace::ApexIosManager					ApexIosManager;
	typedef ApexHelloWorldNamespace::UserParticleSpawningActor		UserParticleSpawningActor;
	typedef ApexHelloWorldNamespace::UserParticleEffectController	UserParticleEffectController;
#endif // APEX_USE_PARTICLES

	enum SceneId
	{
		SceneIdCommandLine = 0,
		SceneIdClothing,
		SceneIdDestructible,
#if APEX_USE_PARTICLES
		SceneIdEmitter,
#endif // APEX_USE_PARTICLES
		SceneIdCount,
	};
private:
	/*virtual*/ void				onInit();
	/*virtual*/ void				onOpen();
	/*virtual*/ void				onTickPreRender(float dtime);
	/*virtual*/ void				onRender();
	/*virtual*/ void				onTickPostRender(float dtime);
	/*virtual*/ void				onShutdown();

	/*virtual*/ void				collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents);
	/*virtual*/ void				collectInputDescriptions(std::vector<const char*>& inputDescriptions);
	/*virtual*/ void				onAnalogInputEvent(const SampleFramework::InputEvent& ie, float val);
	/*virtual*/ bool				onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val);
	/*virtual*/ void				onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy);

    /*virtual*/ unsigned int        getNumScenes() const;
	/*virtual*/ unsigned int        getSelectedScene() const;
	/*virtual*/ const char *        getSceneName(unsigned int sceneNumber) const;
	/*virtual*/ void                selectScene(unsigned int sceneNumber);
private:
	void							loadModules();
	void							unloadModules();
	void							loadAssets();
	void							unloadAssets();
	void							loadActors();
	void							unloadActors();
    void                            loadStaticScene();
    void                            unloadStaticScene();
	void							loadScene(ApexHelloWorld::SceneId sceneId);
	void							unloadScene();

	void							setUpCollision();
	void							setUpLevelOfDetail();
	void							setUpViewProjectionMatrices();
	void							setUpDebugRenderDisplay();
private:
	const bool						useDebugRender;
	ApexHelloWorld::SceneId			currentSceneId;

	ApexClothingManager				apexClothingManager;
	ApexDestructibleManager			apexDestructibleManager;
#if APEX_USE_PARTICLES
	ApexEmitterManager				apexEmitterManager;
	ApexIofxManager					apexIofxManager;
	ApexIosManager					apexIosManager;
#endif // APEX_USE_PARTICLES
	std::vector<UserActorEntity*>	userActorContainer;
private:
#if NX_SDK_VERSION_MAJOR == 2
	class CollisionGroupFiltering
	{
	public:
		struct System1
		{
			enum Enum
			{
				NotInUse = 0
			};

			static NxCollisionGroup getMask(System1::Enum collisionObjectID = System1::NotInUse);
		};

		struct System2
		{
			enum Enum
			{
				GroundPlane = 0,
				CrumbleEmitter,
				SpriteEmitter,
				Count,
			};
			static NxGroupsMask getMask(System2::Enum collisionObjectID);
		};
	};
	typedef CollisionGroupFiltering::System1 DummyManager;
	typedef CollisionGroupFiltering::System2 MaskManager;
#elif NX_SDK_VERSION_MAJOR == 3
	class CollisionGroupFiltering
	{
	public:
		struct System2
		{
			enum Enum
			{
				GroundPlane = 0,
				CrumbleEmitter,
				SpriteEmitter,
				Count,
			};
			static physx::PxFilterData getMask(System2::Enum collisionObjectID);
		};
	};
	typedef CollisionGroupFiltering::System2 MaskManager;
#endif // NX_SDK_VERSION_MAJOR
};

#endif // APEXHELLOWORLD_H
