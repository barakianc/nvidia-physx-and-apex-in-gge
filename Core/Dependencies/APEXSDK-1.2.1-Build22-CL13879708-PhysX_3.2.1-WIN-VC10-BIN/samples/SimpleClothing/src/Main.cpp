// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Clothing Sample
// Description: This sample program shows how to create APEX clothing actors.
//
// ===============================================================================

#include <SimpleClothing.h>

#if defined(RENDERER_ANDROID)
#include <android/log.h>
#include "SamplePlatform.h"

struct android_app;
static android_app* gState;

const char* COMMANDLINE_FILE = "/sdcard/media/APEX/1.2/SimpleClothing/commandline.txt";

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "SampleClothing", __VA_ARGS__))
#endif

SampleClothingApplication* SampleClothingApplication::m_AppPtr = NULL;

bool SampleEntry(const SampleFramework::SampleCommandLine& cmdline)
{
	int width, height;	// display width and height
	//__asm int 3;		// break for OpenAutomate debugging
	bool ok = true;

	SampleClothingApplication app(cmdline);
	app.processEarlyCmdLineArgs();
	width = app.getDisplayWidth();
	height = app.getDisplayHeight();
#if defined(RENDERER_ANDROID)
	/* We need to register event handling callbacks and process events, while window is not yet shown. */
	if (!SampleFramework::SamplePlatform::platform()->preOpenWindow(gState))
	{
		return false;
	}

	while (!SampleFramework::SamplePlatform::platform()->isOpen())
	{
		SampleFramework::SamplePlatform::platform()->update();
	}
#endif
	app.open(width, height, "APEX Clothing Sample");
	while (app.isOpen())
	{
		app.update();
#if defined(RENDERER_ANDROID)
		if (!SampleFramework::SamplePlatform::platform()->isOpen())
		{
			break;
		}
#endif
		app.doInput();
	}
	app.close();
	return ok;
}

#if defined(RENDERER_WINDOWS)

int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE, LPSTR /*cmdLine*/, int show_command)
{
	SampleFramework::SampleCommandLine cl(GetCommandLineA());
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#elif defined(RENDERER_XBOX360)

int main()
{
	char* argv[32];
	int argc = 0;
	volatile LPSTR commandLineString;

	//__debugbreak(); debugging help when launching xbox app from winders

	commandLineString = GetCommandLine(); // xbox call to get command line arguments

	/* first pull out the application name */
	argv[argc] = strtok(commandLineString, " ");

	/* pull out the other args */
	while (argv[argc] != NULL)
	{
		argc++;
		argv[argc] = strtok(NULL, " ");
	}

	SampleFramework::SampleCommandLine cl((unsigned int)argc, argv);
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#elif defined(RENDERER_PS3)

int main(int argc, char** argv)
{
	SampleFramework::SampleCommandLine cl((unsigned int)argc, argv);
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#elif defined(RENDERER_ANDROID)

extern "C" void android_main(struct android_app* state)
{
	gState = state;
	const char* argv[] = { "dummy", 0 };
	SampleFramework::SampleCommandLine cl(1, argv, COMMANDLINE_FILE);
	SampleEntry(cl);
	exit(0);
	/* Will not return return code, because NDK's native_app_glue declares this function to return nothing. Too bad. */
}

#else

int main(int argc, const char* const* argv)
{
	SampleFramework::SampleCommandLine cl((unsigned int)argc, argv);
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#endif
