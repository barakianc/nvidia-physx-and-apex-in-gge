// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Clothing Sample
// Description: This sample program shows how to create APEX clothing actors.
//
// ===============================================================================

#include <SimpleClothing.h>

#include "SkeletalAnim.h"
#include "SampleMorphTargets.h"

const char* const SampleClothingInputEventDescriptions[] =
{
	"attach clothing",
	"detach clothing",

	"switch to scene 1",
	"switch to scene 2",
	"switch to scene 3",
	"switch to scene 4",
	"switch to scene 5",
	"switch to scene 6",
	"switch to scene 7",
	"switch to scene 8",
	"switch to scene 9",
	"switch to scene 10",

	"change the scene LOD",
	"use localspace simulation",
	"use gpu",
	"hide APEX render assets",
	"switch animation",

	"add an actor to the scene",
	"remove an added actor from the scene",
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleClothingInputEventDescriptions) == NUM_EXCLUSIVE_SAMPLE_CLOTHING_INPUT_EVENT_IDS);

///////////////////////////////////////////////////////////////////////////

struct SampleClothingCommandLineInputIds
{
	enum Enum
	{
		SCENE = SampleCommandLineInputIds::COUNT,
		ASYNC_RENDER,
		DISABLE_LOD,

		COUNT = DISABLE_LOD - SCENE + 1
	};
};

typedef SampleClothingCommandLineInputIds SCCLIDS;
const CommandLineInput SampleClothingCommandLineInputs[] =
{
	{ SCCLIDS::SCENE,           "scene",       "", "<scene_num> The index of the starting scene" },
	{ SCCLIDS::ASYNC_RENDER,    "asyncRender", "", "enable async rendering" },
	{ SCCLIDS::DISABLE_LOD,     "disableLod",  "", "disable scene LOD" }
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleClothingCommandLineInputs) == SCCLIDS::COUNT);

///////////////////////////////////////////////////////////////////////////

SampleClothingApplication::SampleClothingApplication(const SampleFramework::SampleCommandLine& cmdline)
	: SampleApexApplication(cmdline)
{
	m_displayResolution			= displayResType::res_1280x800;

	m_meshImporterEzm			= NULL;
	m_apexSceneRunning			= false;
	m_apexClothingModule		= NULL;
	m_separateActors			= 0;
	m_actorSpacing				= 0.0f;
	m_sceneNumber				= SceneNames::Male_Trenchcoat_Med;
	m_sceneFrames				= 0;
	m_animationNumber           = 1;
	m_sceneName					= NULL;
	m_showMeshes				= 0;

	m_AppPtr = this;

	attachedClothingId			= physx::PxU32(-1);
	attachedHitDistance			= FLT_MAX;
	attachedVertexId			= 0;

	m_animatedMesh				= NULL;
	m_apexClothingAssets.clear();

	m_benchmarkNumActors		= 3;

	m_useLocalSpace				= true;
#if NX_SDK_VERSION_MAJOR == 2
	m_clothSolverMode			= ClothSolverMode::v2;
#else
	m_clothSolverMode			= ClothSolverMode::v3;
#endif
	m_rewriteBuffers			= false;

	m_lodSetting				= 0;
	m_lodTimeout				= 0.0f;

	m_commandLineInput.registerCommands(&SampleClothingCommandLineInputs[0], SCCLIDS::COUNT);
	m_asynchronousRendering		= hasInputCommand(SCCLIDS::ASYNC_RENDER);
	m_disableLod				= hasInputCommand(SCCLIDS::DISABLE_LOD);

	// No "Space to create box"
	m_allowCreateObject = false;

	// set up debug rendering
	{
		DebugRenderConfiguration config;

		config.flags.push_back(DebugRenderFlag("Bounds", "Bounds", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("LOD Benefits", "VISUALIZE_LOD_BENEFITS", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Clothing Physics Mesh (Wire)", "Clothing/VISUALIZE_CLOTHING_PHYSICS_MESH_WIRE", 0.9f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Clothing Wind", "Clothing/VISUALIZE_CLOTHING_WIND", 0.1f));
		config.flags.push_back(DebugRenderFlag("Clothing Velocities", "Clothing/VISUALIZE_CLOTHING_VELOCITIES", 0.1f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Clothing Skinned Positions", "Clothing/VISUALIZE_CLOTHING_SKINNED_POSITIONS", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Render normals", "RenderNormals", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Render normals", "RenderNormals", 1.0f));
		config.flags.push_back(DebugRenderFlag("Render tangents", "RenderTangents", 1.0f));
		config.flags.push_back(DebugRenderFlag("Render bitangents", "RenderBitangents", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

#if NX_SDK_VERSION_MAJOR == 2
		config.flags.push_back(DebugRenderFlag("Collision Shapes", NX_VISUALIZE_COLLISION_SHAPES, 1.0f));
		config.flags.push_back(DebugRenderFlag("Cloth Collisions", NX_VISUALIZE_CLOTH_COLLISIONS));
#elif NX_SDK_VERSION_MAJOR == 3
		config.flags.push_back(DebugRenderFlag("Collision Shapes", physx::PxVisualizationParameter::eCOLLISION_SHAPES));
#endif
		// This flag will only be used by the 3.x solver, not the 284 solver
		config.flags.push_back(DebugRenderFlag("Wire Clothing Collision (3.x only)", "Clothing/VISUALIZE_CLOTHING_COLLISION_VOLUMES_WIRE"));

		config.flags.push_back(DebugRenderFlag("Backstop", "Clothing/VISUALIZE_CLOTHING_BACKSTOP", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Solid Clothing Collision", "Clothing/VISUALIZE_CLOTHING_COLLISION_VOLUMES", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Bone Frames", "Clothing/VISUALIZE_CLOTHING_BONE_FRAMES", 10.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Skeleton", "Clothing/VISUALIZE_CLOTHING_SKELETON", 10.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Local Space", "Clothing/VISUALIZE_CLOTHING_SHOW_LOCAL_SPACE", true));
		config.flags.push_back(DebugRenderFlag("Bone Frames", "Clothing/VISUALIZE_CLOTHING_BONE_FRAMES", 10.0f));
		config.flags.push_back(DebugRenderFlag("Skeleton", "Clothing/VISUALIZE_CLOTHING_SKELETON", 10.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Max Distance", "Clothing/VISUALIZE_CLOTHING_MAX_DISTANCE", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

#if NX_SDK_VERSION_MAJOR == 2
		config.flags.push_back(DebugRenderFlag("Hard Stretch Limit", NX_VISUALIZE_CLOTH_HARD_STRETCHING_LIMITATION, 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();
#endif

		config.flags.push_back(DebugRenderFlag("Stretching Phases", "Clothing/VISUALIZE_CLOTHING_STRETCHING_V_PHASES"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Stretching Horizontal Phases", "Clothing/VISUALIZE_CLOTHING_STRETCHING_H_PHASES"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("SemiImplicit Phases", "Clothing/VISUALIZE_CLOTHING_SEMIIMPLICIT_PHASES"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("GaussSeidel Phases", "Clothing/VISUALIZE_CLOTHING_GAUSSSEIDEL_PHASES"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("ZeroStretch Phases", "Clothing/VISUALIZE_CLOTHING_ZEROSTRETCH_PHASES"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Active Tethers", "Clothing/VISUALIZE_CLOTHING_TETHERS_ACTIVE"));
		addDebugRenderConfig(config);
		config.flags.clear();
	}

	for (physx::PxU32 i = 1; i < getNumScenes(); i++)
	{
		m_benchmarkNames.push_back(getSceneName(i));
	}
}

SampleClothingApplication::~SampleClothingApplication(void)
{
	PX_ASSERT(!m_apexSceneRunning);
	PX_ASSERT(!m_apexClothingModule);
	PX_ASSERT(m_sampleLights.empty());
	PX_ASSERT(m_actors.empty());
	PX_ASSERT(m_separateActors == 0);
	PX_ASSERT(!m_animatedMesh);
	PX_ASSERT(m_apexClothingAssets.empty());
}

PX_INLINE physx::NxClothingAsset* SampleClothingApplication::loadClothingAsset(const char* name)
{
	return (physx::NxClothingAsset*)loadApexAsset(NX_CLOTHING_AUTHORING_TYPE_NAME, name);
}

// Process the command line args that need to be processed before the window is open
void SampleClothingApplication::processEarlyCmdLineArgs(void)
{
	SampleApexApplication::initBenchmarks(&benchmarkStart);
	SampleApexApplication::processEarlyCmdLineArgs();

	int sceneNumber = m_sceneNumber;
	if (m_commandLineInput.inputCommandValue(SCCLIDS::SCENE, sceneNumber) &&
	        (sceneNumber > 0 && sceneNumber < SceneNames::NUM_SCENES))
	{
		m_sceneNumber = (SceneNames::Enum)sceneNumber;
	}
}

void SampleClothingApplication::setupRendererDescription(SampleRenderer::RendererDesc& renDesc)
{
	// Depth bias prevents z-fighting with cloth in multi-pass rendering
	SampleApexApplication::setupRendererDescription(renDesc);
	renDesc.multipassDepthBias = true;
}

void SampleClothingApplication::benchmarkStart(const char* benchmarkName)
{
	physx::PxU32 benchmarkNum;

	m_AppPtr->setBenchmarkEnable(true);
	if (m_AppPtr->m_OaInterface != NULL)
	{
		// get the benchmark parameters from openAutomate
		m_AppPtr->m_benchmarkNumFrames = m_AppPtr->m_OaInterface->getBenchmarkNumFrames();
		m_AppPtr->m_benchmarkNumActors = m_AppPtr->m_OaInterface->getBenchmarkNumActors();
		m_AppPtr->m_CmdLineBenchmarkMode = false;
	}
	else
	{
		m_AppPtr->m_CmdLineBenchmarkMode = true;
	}

	m_AppPtr->m_benchmarkNumActors = (m_AppPtr->m_benchmarkNumActors == 0) ? 1 : m_AppPtr->m_benchmarkNumActors;
	m_AppPtr->m_FrameNumber = 0;
	m_AppPtr->m_BenchMarkTimer.getElapsedSeconds();
	m_AppPtr->m_BenchmarkElapsedTime = 0.0;
	m_AppPtr->m_dataCollectionStarted = true;

	for (physx::PxU32 i = 0; m_AppPtr->m_benchmarkNames[i] != NULL; i++)
	{
		if (physx::string::stricmp(m_AppPtr->m_benchmarkNames[i], benchmarkName) == 0)
		{
			benchmarkNum = i + 1;
			m_AppPtr->loadAssetsAndActors(benchmarkNum);
			m_AppPtr->createCameraAndLights(false);
			break;
		}
	}
	oaStartBenchmark();
}

// called just AFTER the window opens.
void SampleClothingApplication::onInit(void)
{
	sampleInit("SimpleClothing", true, false);

#if NX_SDK_VERSION_MAJOR == 2
	NxSceneDesc sceneDesc;
	sceneDesc.internalThreadCount = 5;
	sceneDesc.threadMask = 0xfffffffe;
	sceneDesc.flags |= NX_SF_ENABLE_MULTITHREAD;
	// gravity will be set later on
	sceneDesc.maxTimestep = 1.0f / 60.0f;
	sceneDesc.maxIter = 1;

	m_apexScene = createSampleScene(sceneDesc);
	if (m_apexScene)
	{
		NxScene* scene = m_apexScene->getPhysXScene();

		// Set collision filter
		scene->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_SWAP_AND);
		scene->setFilterBool(true);

		NxGroupsMask zeroMask;
		zeroMask.bits0 = zeroMask.bits1 = zeroMask.bits2 = zeroMask.bits3 = 0;
		scene->setFilterConstant0(zeroMask);
		scene->setFilterConstant1(zeroMask);
	}
	else
	{
		return;
	}
#elif NX_SDK_VERSION_MAJOR == 3
	// gravity will be set later on
	m_apexScene = createSampleScene(PxVec3(0.0f, -9.81f, 0.0f));

	/// catch when cloth doesn't fit on the gpu
	m_errorCallback->addFilteredMessage("GPU cloth could not be added (too many particles to fit in GPU). Falling back to CPU implementation.", true, &m_cpuFallback);

	if (m_apexScene == NULL)
	{
		return;
	}
#endif

	// stop complaining about the cooked data versions and types
	m_errorCallback->addFilteredMessage("cooked data version", false);
	m_errorCallback->addFilteredMessage("cooked data type", false);

#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleClothing();
	physx::apex::instantiateModuleClothingLegacy();
	physx::apex::instantiateModuleCommonLegacy();
	physx::apex::instantiateModuleFrameworkLegacy();
#endif

	mimp::gMeshImport = getInterfaceMeshImport(MESHIMPORT_VERSION);
	if (mimp::gMeshImport)
	{
		m_meshImporterEzm = getInterfaceMeshImportEzm(MESHIMPORT_VERSION);
		if (m_meshImporterEzm)
		{
			mimp::gMeshImport->addImporter(m_meshImporterEzm);
		}
	}

	m_apexClothingModule = static_cast<physx::apex::NxModuleClothing*>(m_apexSDK->createModule("Clothing"));
	PX_ASSERT(m_apexClothingModule);
	if (m_apexClothingModule)
	{
		NxParameterized::Interface* params = m_apexClothingModule->getDefaultModuleDesc();
#ifdef PX_WINDOWS
		// Don't use compartments on consoles
		NxParameterized::setParamU32(*params, "maxNumCompartments", 3);
#endif
		NxParameterized::setParamBool(*params, "parallelizeFetchResults", true);

		// async fetchresults invalidates benchmark measurements.
		NxParameterized::setParamBool(*params, "asyncFetchResults", !m_BenchmarkMode);
		
		// disable async cooking on android 
#if defined(PX_ANDROID)
		NxParameterized::setParamBool(*params, "allowAsyncCooking", false);
#endif

		m_apexClothingModule->init(*params);

		m_apexClothingModule->setLODEnabled(!m_disableLod);

		// 15ms for a benefit of 1.0!
		m_apexClothingModule->setLODBenefitValue(15.0f);
		float cost = m_apexClothingModule->getLODUnitCost();
		m_apexClothingModule->setLODUnitCost(cost / 50.0f); // This just makes sure the LOD system doesn't kick in too early

		m_resourceCallback->setApexSupport(*m_apexSDK);
	}

	// Load older asset versions.
	loadModule("Common_Legacy");
	loadModule("Clothing_Legacy");
	loadModule("Framework_Legacy");

#if 0	// TODO: get these flags to createApexScene
	physx::apex::NxApexSceneDesc apexSceneDesc;
	apexSceneDesc.useDebugRenderable = true;
	apexSceneDesc.debugVisualizeRemotely = true;
#endif

#if NX_SDK_VERSION_MAJOR == 2
	if (m_apexScene)
	{
		NxScene* scene = m_apexScene->getPhysXScene();

		// Set collision filter
		scene->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_SWAP_AND);
		scene->setFilterBool(true);
		NxGroupsMask zeroMask;
		zeroMask.bits0 = zeroMask.bits1 = zeroMask.bits2 = zeroMask.bits3 = 0;
		scene->setFilterConstant0(zeroMask);
		scene->setFilterConstant1(zeroMask);
	}
#endif

	if (!m_apexScene)
	{
		return;
	}

	m_apexScene->allocViewMatrix(physx::apex::ViewMatrixType::LOOK_AT_RH);
	m_apexScene->allocProjMatrix(physx::apex::ProjMatrixType::USER_CUSTOMIZED);

	getRenderer()->setClearColor(SampleRenderer::RendererColor(20, 40, 80, 255));
	getRenderer()->setAmbientColor(SampleRenderer::RendererColor(100, 100, 100, 255));

	for (physx::PxU32 i = 0; i < m_benchmarkNames.size(); i++)
	{
		if (m_cmdline.hasSwitch(m_benchmarkNames[i]))
		{
			if (m_CmdLineBenchmarkMode == false)
			{
				// only 1 benchmark can be run from the command line.
				// OA mode can command sequential bench marks...
				benchmarkStart(m_benchmarkNames[i]);
				break;
			}
		}
	}

	if (!m_BenchmarkMode && m_OaInterface == NULL)
	{
		loadAssetsAndActors(m_sceneNumber);
		createCameraAndLights(true);
	}
}

unsigned int SampleClothingApplication::getNumScenes() const
{
	return SceneNames::NUM_SCENES;
}

unsigned int SampleClothingApplication::getSelectedScene() const
{
	return m_sceneNumber;
}

const char* SampleClothingApplication::getSceneName(physx::PxU32 sceneNumber) const
{
	switch (sceneNumber)
	{
	case SceneNames::Female_Cocktail:
		return "Female/Cocktail Dress 300";
	case SceneNames::Female_Dress:
		return "Female/Dress 600";
	case SceneNames::Female_SummerDress:
		return "Female/Summer Dress 2400";
	case SceneNames::Male_CapeLow:
		return "Male/Cape 100";
	case SceneNames::Male_CapeMed:
		return "Male/Cape 400";
	case SceneNames::Male_CapeHigh:
		return "Male/Cape 1400";
	case SceneNames::Male_Trenchcoat_Low:
		return "Male/Trenchcoat 200";
	case SceneNames::Male_Trenchcoat_Med:
		return "Male/Trenchcoat 800";
	case SceneNames::Male_Trenchcoat_High:
		return "Male/Trenchcoat 2000";
	case SceneNames::Male_Vacation:
		return "Male/Vacation Man";
	case SceneNames::TrenchCoatLow:
		return "TrenchCoatLow";
	case SceneNames::TrenchCoatMedium:
		return "TrenchCoatMedium";
	case SceneNames::TrenchCoatHigh:
		return "TrenchCoatHigh";
	case SceneNames::PantsShirtHigh:
		return "PantsShirtHigh";
	case SceneNames::FlagLow:
		return "FlagLow";
	case SceneNames::FlagMedium:
		return "FlagMedium";
	case SceneNames::FlagLoD:
		return "FlagLoD";
	default:
		PX_ALWAYS_ASSERT();
		return "sceneNumber out of bounds";
	}
}


void SampleClothingApplication::selectScene(unsigned int sceneNumber)
{
	releaseAssetsAndActors();
	releaseCameraAndLights();
	loadAssetsAndActors(sceneNumber);
	createCameraAndLights(!m_BenchmarkMode);
}


void SampleClothingApplication::loadAssetsAndActors(physx::PxU32 sceneNumber)
{
	PX_ASSERT(sceneNumber > 0 && sceneNumber < SceneNames::NUM_SCENES);
	if (sceneNumber < 1 || sceneNumber >= SceneNames::NUM_SCENES)
	{
		return;
	}

	m_sceneNumber = (SceneNames::Enum)sceneNumber;

	char fullpath[1024];
	physx::string::strcpy_s(fullpath, 1024, m_resourceDir.c_str());
	physx::string::strcat_s(fullpath, 1024, "/SimpleClothing/");

	SampleAnimatedMeshAsset::setPrefixPath(fullpath);

	m_rotateActors = false;
	m_randomAnimationMin = 0.4f;
	m_randomAnimationMax = 0.6f;
	m_actorSpacing = 5.0f;
	m_continuousAnimation = true;

	bool fixMaterialNames = false;

	m_windAdaption = 0.0f;
	m_windVelocity = physx::PxVec3(0.0f, 0.0f, 0.0f);

	m_startScale = 1.0f;
	m_endScale = 1.0f;

	m_sceneName = getSceneName(sceneNumber);

	m_sceneSize = 1.0f;

	if (m_apexSDK && m_apexClothingModule)
	{
		std::string animationFile;
		switch (sceneNumber)
		{
		case SceneNames::Female_Cocktail:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDF/ctdf_cocktail_300"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDF/ctdf_cocktail_300.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("CocktailDress");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			m_endScale = 0.5f;
			break;
		case SceneNames::Female_Dress:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDF/ctdf_dress_600"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDF/ctdf_dress_600.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("CocktailDress");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			m_endScale = 0.5f;
			break;
		case SceneNames::Female_SummerDress:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDF/ctdf_SummerDress_2400"));
				m_apexClothingAssets.push_back(loadClothingAsset("CTDF/ctdf_SummerDress_2400_SummerShirt"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDF/ctdf_SummerDress_2400.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Cloth");
				m_animatedMesh->hideByMaterialName("DressShirt");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			m_endScale = 0.5f;
			break;
		case SceneNames::Male_CapeLow:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_cape_100"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_cape_100.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Cape");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::Male_CapeMed:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_cape_400"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_cape_400.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Cape");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::Male_CapeHigh:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_cape_1400"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_cape_1400.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Cape");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::Male_Trenchcoat_Low:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_trenchcoat_200"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_trenchcoat_200.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Coat");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::Male_Trenchcoat_Med:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_trenchcoat_800"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_trenchcoat_800.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Coat");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::Male_Trenchcoat_High:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_trenchcoat_2000"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_trenchcoat_2000.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("Coat");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::Male_Vacation:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/VacationMan_VacationPants"));
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/VacationMan_VacationShirt"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/VacationMan.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("pants");
				m_animatedMesh->hideByMaterialName("shirt");
				fixMaterialNames = true;
			}
			m_sceneSize = 20.0f;
			m_rotateActors = true;
			m_actorSpacing = 5.0f;
			//m_endScale = 0.5f;
			break;
		case SceneNames::TrenchCoatLow:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_trenchcoat_low"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_trenchcoat_low.ezb";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("trenchcoat");
				fixMaterialNames = true;
			}
			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 1.2f;
			m_randomAnimationMax = 1.2f;
			m_actorSpacing = 2.0f;
			m_endScale = 0.5f;
			break;
		case SceneNames::TrenchCoatMedium:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_trenchcoat_med"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_trenchcoat_med.ezb";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("trenchcoat");
				fixMaterialNames = true;
			}
			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 1.3f;
			m_randomAnimationMax = 1.3f;
			m_actorSpacing = 2.0f;
			m_endScale = 0.5f;
			break;
		case SceneNames::TrenchCoatHigh:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_trenchcoat_high"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_trenchcoat_high.ezb";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("trenchcoat");
				fixMaterialNames = true;
			}
			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 1.4f;
			m_randomAnimationMax = 1.4f;
			m_actorSpacing = 2.0f;
			m_endScale = 0.5f;
			break;
		case SceneNames::PantsShirtHigh:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_pants_high"));
				m_apexClothingAssets.push_back(loadClothingAsset("CTDM/ctdm_shirt_high"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "CTDM/ctdm_pants_shirt_high.ezb";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("pants");
				m_animatedMesh->hideByMaterialName("shirt");
				fixMaterialNames = true;
			}

			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 1.2f;
			m_randomAnimationMax = 1.2f;
			m_actorSpacing = 2.0f;
			break;
		case SceneNames::FlagLow:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("flags/FlagForWind_Low_v2"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "flags/FlagForWind_Low_v2.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("materials/nvidia_flag.xml");
			}
			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 0.0f;
			m_randomAnimationMax = 0.0f;
			m_actorSpacing = 4.0f;
			m_windAdaption = 0.8f;
			m_windVelocity = physx::PxVec3(5000.0f, 0.0f, 5000.0f);
			m_endScale = 1.5f;
			break;
		case SceneNames::FlagMedium:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("flags/FlagForWind_Med_v2"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "flags/FlagForWind_Med_v2.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("materials/nvidia_flag.xml");
			}
			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 0.0f;
			m_randomAnimationMax = 0.0f;
			m_actorSpacing = 4.0f;
			m_windAdaption = 0.8f;
			m_windVelocity = physx::PxVec3(5000.0f, 0.0f, 5000.0f);
			m_endScale = 1.5f;
			break;
		case SceneNames::FlagLoD:
			if (m_apexClothingAssets.empty())
			{
				m_apexClothingAssets.push_back(loadClothingAsset("flags/Flag_lod_v4_Flag_High"));
			}
			if (m_animatedMesh == NULL)
			{
				animationFile = "flags/Flag_Lod_v4.ezm";
				m_animatedMesh = new SampleAnimatedMeshAsset(animationFile.c_str());
				m_animatedMesh->hideByMaterialName("materials/nvidia_flag.xml");
			}
			m_sceneSize = 38.0f;
			m_rotateActors = true;
			m_randomAnimationMin = 0.0f;
			m_randomAnimationMax = 0.0f;
			m_actorSpacing = 4.0f;
			m_windAdaption = 0.8f;
			m_windVelocity = physx::PxVec3(500.0f, 0.0f, 500.0f);
			m_endScale = 1.5f;
			break;
		}

		if (m_animatedMesh != NULL)
		{
			if (fixMaterialNames)
			{
				m_animatedMesh->loadMaterials(m_resourceCallback, m_renderResourceManager, "materials/clothing_", ".xml");
			}
			else
			{
				m_animatedMesh->loadMaterials(m_resourceCallback, m_renderResourceManager, NULL, NULL);
			}

			// Check if the animation was actually loaded
			if (NULL == m_animatedMesh->getSkeletalAnim())
			{
				std::string errString;
				errString = "Warning: Could not find animation file: " + animationFile;
#if defined(PX_WINDOWS)
				MessageBoxA(NULL, errString.c_str(), "Missing Animation", MB_OK);
#endif
				ERRORSTREAM_DEBUG_INFO(errString.c_str());
			}
			else
			{
				const char* ctdmAnims[] = 
				{
					"", // no animation
					"CTDM/ctdm_anim_Idle_Hero.ezm",
					"CTDM/ctdm_anim_ClimbLedgeCycle.ezm",
					"CTDM/ctdm_anim_Fig8Fly.ezm",
					"CTDM/ctdm_anim_RunJumpGap.ezm",
					"CTDM/ctdm_anim_vs_boxes.ezm",
					"CTDM/ctdm_anim_RunInPlace.ezm",
				};
				float ctdmAnimSpeeds[] = { 0.0f, 0.5f, 3.0f, 3.0f, 1.0f, 3.0f, 0.25f };

				const char* ctdfAnims[] = 
				{
					"", // no animation
					"CTDF/ctdf_anim_StairCycle.ezm",
					"CTDF/ctdf_anim_WalkInPlace.ezm",
				};
				float ctdfAnimSpeeds[] = { 0.0f, 3.0f, 0.25f };

				// Check if another animation has to be loaded
				if (m_animatedMesh->getSkeletalAnim()->getAnimations().empty())
				{
					switch (m_sceneNumber)
					{
					case SceneNames::Female_Cocktail:
					case SceneNames::Female_Dress:
					case SceneNames::Female_SummerDress:
						if (m_animationNumber > 2)
						{
							m_animationNumber = 0;
						}

						m_animatedMesh->loadAnimation(ctdfAnims[m_animationNumber]);
						m_randomAnimationMin = ctdfAnimSpeeds[m_animationNumber];
						m_randomAnimationMax = ctdfAnimSpeeds[m_animationNumber];
						break;
					case SceneNames::Male_CapeLow:
					case SceneNames::Male_CapeMed:
					case SceneNames::Male_CapeHigh:
					case SceneNames::Male_Trenchcoat_Low:
					case SceneNames::Male_Trenchcoat_Med:
					case SceneNames::Male_Trenchcoat_High:
					case SceneNames::Male_Vacation:
						if (m_animationNumber > 6)
						{
							m_animationNumber = 0;
						}

						m_animatedMesh->loadAnimation(ctdmAnims[m_animationNumber]);
						m_continuousAnimation = strstr(ctdmAnims[m_animationNumber], "Gap") == NULL;
						m_randomAnimationMin = ctdmAnimSpeeds[m_animationNumber];
						m_randomAnimationMax = ctdmAnimSpeeds[m_animationNumber];
						break;
					default:
						// no animation files for the other characters! :(
						break;
					}
				}
			}
		}
	}

	SampleAnimatedMeshAsset::setPrefixPath(NULL);

	loadActors();
}

void SampleClothingApplication::loadActors()
{
	m_cpuFallback = false;

	if (m_apexScene)
	{
#if NX_SDK_VERSION_MAJOR == 2
		NxScene* scene = m_apexScene->getPhysXScene();
		scene->setGravity(NxVec3(0.0f, -9.81f * m_sceneSize , 0.0f));

		// only add the first time
		if (m_actors.empty())
		{
			SampleBoxActor* boxActor = new SampleBoxActor(getRenderer(), *m_simpleLitMaterial, *scene, physx::PxVec3(0.0f, -1.0f, 0.0f), physx::PxVec3(0.0f, 0.0f, 0.0f), physx::PxVec3(100.0f * m_sceneSize, 1.0f, 100.0f * m_sceneSize), 0, NULL, false);
			m_actors.push_back(boxActor);
		}
#elif NX_SDK_VERSION_MAJOR == 3
		physx::PxScene* scene = m_apexScene->getPhysXScene();

		scene->setGravity(PxVec3(0.0f, -9.81f * m_sceneSize , 0.0f));

		// only add the first time
#if !defined(PX_ANDROID)
		if (m_actors.empty())
		{
			SampleBoxActor* boxActor = new SampleBoxActor(getRenderer(), *m_simpleLitMaterial, *scene, physx::PxVec3(0.0f, -1.0f, 0.0f), physx::PxVec3(0.0f, 0.0f, 0.0f), physx::PxVec3(100.0f * m_sceneSize, 1.0f, 100.0f * m_sceneSize), 0, NULL, false);
			m_actors.push_back(boxActor);
		}
#endif
#endif

		physx::PxI32 RowActorCount = 0;

		const physx::PxF32 dist = m_actorSpacing * m_sceneSize;
		physx::PxVec3 globalPosition(-dist, 0.0f, 0.0f);

		if (m_benchmarkNumActors > MAX_CLOTHING_ACTOR_COUNT)
		{
			ERRORSTREAM_DEBUG_WARNING("Capping clothing actor count to a maximum of %d.", MAX_CLOTHING_ACTOR_COUNT);
			m_benchmarkNumActors = MAX_CLOTHING_ACTOR_COUNT;
		}

#if defined(PX_PS3)
		bool useHardware = true; // use SPUs
#else
		bool useHardware = m_enableGpuPhysX && (m_cudaContext != NULL);
#endif

		physx::PxF32 scale = m_startScale;
		for (physx::PxU32 curActor = 0; curActor < m_benchmarkNumActors; curActor++)
		{
			globalPosition.x += dist;

			if (RowActorCount == 8)
			{
				RowActorCount = 0;
				globalPosition.x = 0.0f;
				globalPosition.z -= dist;
			}
			RowActorCount++;

			physx::PxMat44 globalPose = physx::PxMat44::createIdentity();
			globalPose.setPosition(globalPosition);

			if (m_rotateActors)
			{
				globalPose(1, 1) = 0;
				globalPose(2, 2) = 0;
				globalPose(1, 2) = 1;
				globalPose(2, 1) = -1;
			}

			globalPose.scale(physx::PxVec4(scale, scale, scale, 1.0f));
			scale += (m_endScale - m_startScale) / (m_benchmarkNumActors - 1);

			SampleAnimatedMeshActor* animatedMeshActor = NULL;
			if (m_animatedMesh != NULL)
			{
				animatedMeshActor = new SampleAnimatedMeshActor(globalPose, m_animatedMesh, m_renderResourceManager, m_apexRenderer, m_renderer);
				animatedMeshActor->setAnimationTimeScale(physx::rand(m_randomAnimationMin, m_randomAnimationMax));
				animatedMeshActor->setSecondsUntilRestart(0.0f);
				animatedMeshActor->setContinuousAnimation(m_continuousAnimation);
			}

			if (animatedMeshActor != NULL)
			{
				m_actors.push_back(animatedMeshActor);
			}

			for (physx::PxU32 curAsset = 0; curAsset < m_apexClothingAssets.size(); curAsset++)
			{
				const physx::PxVec3* morphDisp = NULL;
				physx::PxU32 numMorphDisp = 0;

				SampleClothingActor* clothingActor = NULL;
				if (animatedMeshActor != NULL)
				{
					clothingActor = new SampleClothingActor(*m_apexScene, *m_apexClothingAssets[curAsset], m_apexRenderer, animatedMeshActor, useHardware, m_useLocalSpace, morphDisp, numMorphDisp);
				}
				else
				{
					clothingActor = new SampleClothingActor(*m_apexScene, *m_apexClothingAssets[curAsset], m_apexRenderer, globalPose, useHardware, m_useLocalSpace);
				}

				m_clothSolverMode = clothingActor->getClothingActor()->getClothSolverMode();
				m_useLocalSpace = clothingActor->isLocalSpace();

				if (m_windAdaption > 0.0f)
				{
					clothingActor->setWind(m_windAdaption, m_windVelocity);
					clothingActor->setWindNoise(0.5f, 20.0f);
				}
				clothingActor->forcePhysicalLOD(getPhysicalLod());

				m_actors.push_back(clothingActor);
			}

			m_separateActors++;
		}
	}

	clearAverageStats();
}

void SampleClothingApplication::releaseActors()
{
	if (m_apexSceneRunning && m_apexScene != NULL)
	{
		physx::PxU32 errorState = 0;
		m_apexScene->fetchResults(true, &errorState);
	}

	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		m_actors[i]->release();
	}
	m_actors.clear();
}

void SampleClothingApplication::releaseAssetsAndActors()
{
	releaseActors();

	{
		// delete the memory
		std::vector<SampleFramework::SampleActor*> clearActors;
		m_actors.swap(clearActors);
	}
	m_separateActors = 0;

	if (m_animatedMesh != NULL)
	{
		Samples::TriangleMesh* tm = m_animatedMesh->getTriangleMesh();
		if (tm)
		{
			tm->clear(m_renderResourceManager, m_resourceCallback);
		}
		delete m_animatedMesh;
		m_animatedMesh = NULL;
	}

	for (physx::PxU32 i = 0; i < m_apexClothingAssets.size(); i++)
	{
		releaseApexAsset(m_apexClothingAssets[i]);
	}
	m_apexClothingAssets.clear();
}

void SampleClothingApplication::createCameraAndLights(bool withShadows)
{
	SampleRenderer::Renderer* renderer = getRenderer();

	// new camera perspective
	setEyeTransform(physx::PxVec3(0.0f, 5.0f, 14.0f) * m_sceneSize, physx::PxVec3(physx::degToRad(15.0f), physx::degToRad(15.0f), 0.0f));

	SampleLight* slight   = 0;

	if (withShadows)
	{
		SampleRenderer::RendererSpotLightDesc lightdesc;
		lightdesc.color     = SampleRenderer::RendererColor(255, 255, 255, 255);
		lightdesc.direction = physx::PxVec3(0, -1, -1);
		lightdesc.direction.normalize();
		lightdesc.innerRadius = 20 * m_sceneSize;
		lightdesc.outerRadius = 30 * m_sceneSize;
		lightdesc.innerCone   = 0.9f;
		lightdesc.outerCone   = 0.8f;
		lightdesc.intensity = 0.8f;

		lightdesc.position  = physx::PxVec3(2.5f, 10.0f, 10.0f);
		lightdesc.position *= m_sceneSize;
		slight = new SampleSpotLight(*renderer, lightdesc);
		if (slight->getLight())
		{
			m_sampleLights.push_back(slight);
		}
		else
		{
			delete slight;
		}

#if !defined(RENDERER_ANDROID)
		lightdesc.position  = physx::PxVec3(12.5f, 10.0f, 10.0f);
		lightdesc.position *= m_sceneSize;
		slight = new SampleSpotLight(*renderer, lightdesc);
		if (slight->getLight())
		{
			m_sampleLights.push_back(slight);
		}
		else
		{
			delete slight;
		}
#endif
	}
	else
	{
		SampleRenderer::RendererDirectionalLightDesc lightdesc;
		lightdesc.color     = SampleRenderer::RendererColor(255, 255, 255, 255);
		lightdesc.direction = physx::PxVec3(0, -1, -1);
		lightdesc.direction.normalize();
		lightdesc.intensity = 0.8f;

		slight = new SampleLight(*renderer, lightdesc);
		if (slight->getLight())
		{
			m_sampleLights.push_back(slight);
		}
		else
		{
			delete slight;
		}
	}
}

void SampleClothingApplication::releaseCameraAndLights()
{
	for (physx::PxU32 i = 0; i < m_sampleLights.size(); i++)
	{
		delete m_sampleLights[i];
	}
	m_sampleLights.clear();
	{
		// delete the memory
		std::vector<SampleLight*> clearLights;
		m_sampleLights.swap(clearLights);
	}

	m_sceneFrames = 0;
}

// called just BEFORE the window closes. return 'true' to confirm the window closure.
void SampleClothingApplication::onShutdown(void)
{
	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
	PX_ASSERT(assetManager);
	PX_FORCE_PARAMETER_REFERENCE(assetManager);

	releaseAssetsAndActors();

	releaseCameraAndLights();

	if (m_apexClothingModule)
	{
		m_apexSDK->releaseModule(m_apexClothingModule);
		m_apexClothingModule = 0;
	}

	SampleApexApplication::onShutdown();
}

void SampleClothingApplication::onTickPreRender(float dtime)
{
	// PH: Rendering needs to be done by here, buffers get updated!
	SampleRenderer::Renderer* renderer = getRenderer();

#if ENABLE_DEBUG_RENDERING
	if (m_apexRenderDebug && renderer && m_apexScene != NULL)
	{
		// set up the matrices for screen space text rendering
		physx::PxU32 windowWidth  = 0;
		physx::PxU32 windowHeight = 0;
		renderer->getWindowSize(windowWidth, windowHeight);

		m_projection = SampleRenderer::RendererProjection(45.0f, windowWidth / (float)windowHeight, 0.1f, 10000.0f);

		// The APEX scene needs the view and projection matrices for LOD, debug rendering, and other things
		updateApexSceneMatrices();

		if (m_apexScene->getPhysXScene())
		{
#if NX_SDK_VERSION_MAJOR == 2
			const NxDebugRenderable* debugRenderable = m_apexScene->getPhysXScene()->getDebugRenderable();
			if (debugRenderable)
			{
				m_apexRenderDebug->addDebugRenderable(*debugRenderable);
			}
#elif NX_SDK_VERSION_MAJOR == 3
			const physx::PxRenderBuffer& renderBuffer = m_apexScene->getPhysXScene()->getRenderBuffer();
			if (m_apexRenderDebug != NULL)
			{
				m_apexRenderDebug->addDebugRenderable(renderBuffer);
			}
#endif
		}

		m_apexRenderDebug->lockRenderResources();
		m_apexRenderDebug->updateRenderResources(0);
		m_apexRenderDebug->unlockRenderResources();
	}

	if (m_apexScene)
	{
		m_apexScene->lockRenderResources();
		m_apexScene->updateRenderResources();
		m_apexScene->unlockRenderResources();
	}
#endif

	// PH: For benchmark mode we should run at the fixed deltaT to get meaningful results
	if (m_BenchmarkMode)
	{
		dtime = 1.0f / 60.0f;
	}

	if (dtime > 1.0f / 30.0f)
	{
		dtime = 1.0f / 30.0f;
	}

	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		m_actors[i]->tick(m_pause ? 0.0f : dtime, m_rewriteBuffers);
	}

	if (m_apexScene && !m_pause)
	{
		m_apexScene->simulate(dtime);
		PX_ASSERT(!m_apexSceneRunning);
		m_apexSceneRunning = true;
	}

	if (!m_asynchronousRendering && m_apexScene && !m_pause)
	{
		physx::PxU32 errorState = 0;
		m_apexScene->fetchResults(true, &errorState);
		PX_ASSERT(m_apexSceneRunning);
		m_apexSceneRunning = false;
	}
	SampleApexApplication::onTickPreRender(dtime);
}

void SampleClothingApplication::renderScene(const physx::PxMat44& eyeTransform, const SampleRenderer::RendererProjection& proj, SampleRenderer::RendererTarget* target, bool depthOnly)
{
	SampleRenderer::Renderer* renderer = getRenderer();
	if (renderer)
	{
		if (!target)
		{
			for (physx::PxU32 i = 0; i < m_sampleLights.size(); i++)
			{
				SampleRenderer::RendererLight* light = m_sampleLights[i]->getLight();
				if (light)
				{
					renderer->queueLightForRender(*light);
				}
			}
		}

		// Queue up all actors
		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if ((m_showMeshes & 1) && m_actors[i]->getType() == SampleClothingActor::TYPE)
			{
				continue;
			}

			if ((m_showMeshes >= 2) && m_actors[i]->getType() == SampleAnimatedMeshActor::TYPE)
			{
				continue;
			}

			m_actors[i]->render();
		}

		// execute rendering
		renderer->render(eyeTransform, proj, target, depthOnly);
	}
}

// called when the window's contents needs to be redrawn.
void SampleClothingApplication::onRender(void)
{
	SampleRenderer::Renderer* renderer = getRenderer();
	if (renderer)
	{
		physx::PxU32 windowWidth  = 0;
		physx::PxU32 windowHeight = 0;
		renderer->getWindowSize(windowWidth, windowHeight);

		if (windowWidth > 0 && windowHeight > 0)
		{
			renderer->clearBuffers();

			m_projection = SampleRenderer::RendererProjection(45.0f, windowWidth / (float)windowHeight, 1.f, 10000.0f);

			for (physx::PxU32 i = 0; i < m_sampleLights.size(); i++)
			{
				SampleLight& light = *m_sampleLights[i];
				if (light.supportsShadows())
				{
					renderScene(light.getShadowTransform(), light.getShadowProjection(), light.getShadowTarget(), true);
				}
			}

#if ENABLE_DEBUG_RENDERING
			if (m_apexRenderDebug)
			{
				m_apexRenderDebug->dispatchRenderResources(*m_apexRenderer);
			}

			if (m_apexScene)
			{
				m_apexScene->dispatchRenderResources(*m_apexRenderer);
			}
#endif

			renderScene(getEyeTransform(), m_projection, NULL, false);

			{

#if defined(RENDERER_TABLET)
				/* draw screenquad & controls */
				SampleRenderer::ScreenQuad sq;
				sq.mLeftUpColor     = SampleRenderer::RendererColor(0x00, 0x00, 0x80);
				sq.mRightUpColor    = SampleRenderer::RendererColor(0x00, 0x00, 0x80);
				sq.mLeftDownColor   = SampleRenderer::RendererColor(0xff, 0xf0, 0xf0);
				sq.mRightDownColor  = SampleRenderer::RendererColor(0xff, 0xf0, 0xf0);
				renderer->drawScreenQuad(sq);
#endif

				std::string sceneInfo;
				bool addComma = false;
#ifdef PX_WINDOWS
				int gpuIndex = 0;
				const char* gpuMode[] = { "CPU", "GPU", "CPU Fallback" };
#if NX_SDK_VERSION_MAJOR == 2
				const bool embedded = m_clothSolverMode == ClothSolverMode::v3;
				if (embedded)
				{
					gpuIndex = (m_enableGpuPhysX && (m_cudaContext != NULL)) ? 1 : 0;
				}
				else if (m_physxSDK != NULL)
				{
					gpuIndex = (m_physxSDK->getHWVersion() == NX_HW_VERSION_ATHENA_1_0 && m_enableGpuPhysX) ? 1 : 0;
				}
#elif NX_SDK_VERSION_MAJOR == 3
				gpuIndex = (m_enableGpuPhysX && (m_cudaContext != NULL)) ? 1 : 0;
#endif
				if (gpuIndex == 1 && m_cpuFallback)
				{
					gpuIndex = 2;
				}

				sceneInfo.append(gpuMode[gpuIndex]);
				addComma = true;
#endif // PX_WINDOWS

#if NX_SDK_VERSION_MAJOR == 2
				const char* solverType[3] = { (m_clothSolverMode == ClothSolverMode::v2 ? "2.8.4 solver" : "3.x solver"), "force 2.8.4 solver", "force 3.x solver"};
				if (addComma)
				{
					sceneInfo.append(", ");
				}
				addComma = true;
				sceneInfo.append(solverType[0]);
#endif
				if (m_useLocalSpace)
				{
					if (addComma)
					{
						sceneInfo.append(", ");
					}
					addComma = true;
					sceneInfo.append("localspace");
				}

				std::string additionalInfo;
				if (m_lodTimeout > 0.0f)
				{
					m_lodTimeout -= 1.0f / 60.0f;
					char buf[32];
					physx::string::sprintf_s(buf, 32, "LOD set to %d", m_lodSetting);
					additionalInfo.append(buf);
				}
				printSceneInfo(sceneInfo.c_str(), additionalInfo.c_str());
			}

		}
	}
}

void SampleClothingApplication::onTickPostRender(float dtime)
{
	dumpApexStats(m_apexScene);

	if (m_asynchronousRendering && m_apexScene && !m_pause)
	{
		physx::PxU32 errorState = 0;
		m_apexScene->fetchResults(true, &errorState);
		PX_ASSERT(m_apexSceneRunning);
		m_apexSceneRunning = false;
	}

	// call the Open Automate handler...
	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::IDLE))
	{
		m_OaInterface->OAHandler();
	}

	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::EXIT))
	{
		close();
	}

	//if (in command line benchmarking) or (in OA benchmarking...)
	if (m_BenchmarkMode)
	{
		// are there still frames to run?
		if (m_FrameNumber < m_benchmarkNumFrames)
		{
			SampleApexApplication::reportOaResults();
			m_FrameNumber++;
		}
		else  // no more frames to run.  benchmark is done
		{
			if (m_OaInterface != NULL)
			{
				if (m_OaInterface->getOaState() == OAState::BENCHMARKING)
				{
					//done with the benchmark
					oaValue v;
					v.Float = m_BenchmarkElapsedTime;
					m_OaInterface->setOaState(OAState::IDLE);
					oaAddResultValue("Total run time", OA_TYPE_FLOAT, &v);
					m_OaInterface->endBenchmark();

					// release the actors created by this benchmark in case OA runs another benchmark without exiting!
					releaseAssetsAndActors();
					releaseCameraAndLights();
				}
			}
			else // close it down and exit the program
			{
				close();
			}
		}
	}

	SampleApexApplication::onTickPostRender(dtime);
}

void SampleClothingApplication::collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
{
	using namespace SampleFramework;

	std::vector<const char*> inputDescriptions;
	collectInputDescriptions(inputDescriptions);

	ANALOG_INPUT_EVENT_DEF_2(CLOTH_ATTACH, MOUSE_BUTTON_RIGHT_DOWN,	XKEY_UNKNOWN, PS3KEY_UNKNOWN, AKEY_UNKNOWN, MOUSE_MOVE,     PSP2KEY_UNKNOWN,    IKEY_UNKNOWN,   MOUSE_BUTTON_RIGHT_DOWN);
	ANALOG_INPUT_EVENT_DEF_2(CLOTH_DETACH, MOUSE_BUTTON_RIGHT_UP,	XKEY_UNKNOWN, PS3KEY_UNKNOWN, AKEY_UNKNOWN, MOUSE_MOVE,     PSP2KEY_UNKNOWN,    IKEY_UNKNOWN,   MOUSE_BUTTON_RIGHT_DOWN);

    DIGITAL_INPUT_EVENT_DEF_2(ACTOR_REMOVE,			WKEY_LEFT,	    XKEY_LEFT,	    PS3KEY_LEFT,    AKEY_N,	    OSXKEY_LEFT,    PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_LEFT);
	DIGITAL_INPUT_EVENT_DEF_2(ACTOR_ADD,			WKEY_RIGHT,		XKEY_RIGHT,		PS3KEY_RIGHT,	AKEY_M,	    OSXKEY_RIGHT,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_RIGHT);
	DIGITAL_INPUT_EVENT_DEF_2(LOD_CHANGE,			WKEY_L,			XKEY_L,			PS3KEY_L,		AKEY_L,	    OSXKEY_L,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_L);
	DIGITAL_INPUT_EVENT_DEF_2(USE_LOCALSPACE,		WKEY_K,			XKEY_K,			PS3KEY_K,		AKEY_K,	    OSXKEY_K,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_K);
#ifdef PX_WINDOWS
	//DIGITAL_INPUT_EVENT_DEF_2(USE_GPU			,	WKEY_G,			XKEY_G,			PS3KEY_G,		AKEY_UNKNOWN,	OSXKEY_G,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_G			);
#endif
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_1,				WKEY_1,			XKEY_1,			PS3KEY_1,		AKEY_1,		OSXKEY_1,		PSP2KEY_UNKNOWN,	IBUTTON_2,		LINUXKEY_1);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_2,				WKEY_2,			XKEY_2,			PS3KEY_2,		AKEY_2,		OSXKEY_2,		PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_2);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_3,				WKEY_3,			XKEY_3,			PS3KEY_3,		AKEY_3,		OSXKEY_3,		PSP2KEY_UNKNOWN,	IBUTTON_3,		LINUXKEY_3);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_4,				WKEY_4,			XKEY_4,			PS3KEY_4,		AKEY_4,		OSXKEY_4,		PSP2KEY_UNKNOWN,	IBUTTON_4,		LINUXKEY_4);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_5,				WKEY_5,			XKEY_5,			PS3KEY_5,		AKEY_5,		OSXKEY_5,		PSP2KEY_UNKNOWN,	IBUTTON_5,		LINUXKEY_5);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_6,				WKEY_6,			XKEY_6,			PS3KEY_6,		AKEY_6,		OSXKEY_6,		PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_6);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_7,				WKEY_7,			XKEY_7,			PS3KEY_7,		AKEY_7,		OSXKEY_7,		PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_7);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_8,				WKEY_8,			XKEY_8,			PS3KEY_8,		AKEY_8,		OSXKEY_8,		PSP2KEY_UNKNOWN,	IBUTTON_8,		LINUXKEY_8);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_9,				WKEY_9,			XKEY_9,			PS3KEY_9,		AKEY_9,		OSXKEY_9,		PSP2KEY_UNKNOWN,	IBUTTON_9,		LINUXKEY_9);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_0,				WKEY_0,			XKEY_0,			PS3KEY_0,		AKEY_0,		OSXKEY_0,		PSP2KEY_UNKNOWN,	IBUTTON_0,		LINUXKEY_0);
	DIGITAL_INPUT_EVENT_DEF_2(HIDE_GRAPHICS,		WKEY_G,			XKEY_G,			PS3KEY_G,		AKEY_G,		OSXKEY_G,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_G);
	DIGITAL_INPUT_EVENT_DEF_2(SWITCH_ANIMATION,		WKEY_J,			XKEY_J,			PS3KEY_J,		AKEY_J,		OSXKEY_J,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_J);

	SampleApexApplication::collectInputEvents(inputEvents);
}

void SampleClothingApplication::collectInputDescriptions(std::vector<const char*>& inputDescriptions)
{
	SampleApexApplication::collectInputDescriptions(inputDescriptions);
	inputDescriptions.insert(inputDescriptions.end(), SampleClothingInputEventDescriptions, SampleClothingInputEventDescriptions + PX_ARRAY_SIZE(SampleClothingInputEventDescriptions));
}

void SampleClothingApplication::onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy)
{
	// if a benchmark is running ignore mouse events.
	if (m_BenchmarkMode)
	{
		return;
	}

	SampleRenderer::Renderer* renderer = getRenderer();
	if (m_apexClothingModule && renderer)
	{
		if (CLOTH_ATTACH == ie.m_Id)
		{
			if (attachedClothingId != physx::PxU32(-1))
			{
				SampleClothingActor* clothingActor = static_cast<SampleClothingActor*>(m_actors[attachedClothingId]);
				clothingActor->getClothingActor()->freeVertex(attachedVertexId);
				attachedClothingId = physx::PxU32(-1);
			}

			physx::PxU32 width  = 0;
			physx::PxU32 height = 0;
			renderer->getWindowSize(width, height);

			const physx::PxMat44& eyeTransform = getEyeTransform();
			physx::PxTransform view(eyeTransform.inverseRT());
			physx::PxVec3 eyePos  = eyeTransform.getPosition();
			physx::PxVec3 nearPos = unproject(m_projection, view, (x / (physx::PxF32)width) * 2 - 1, (y / (physx::PxF32)height) * 2 - 1);
			physx::PxVec3 pickDir = nearPos - eyePos;

			if (pickDir.normalize() > 0)
			{
				attachedHitDistance = FLT_MAX;
				attachedVertexId = 0;
				PxVec3 bestHitPosition;
				for (size_t i = 0; i < m_actors.size(); i++)
				{
					if (m_actors[i]->getType() == SampleClothingActor::TYPE)
					{
						physx::PxF32 hitTime = 0.0f;
						physx::PxU32 vertexIndex = 0;
						physx::PxVec3 hitNormal;
						SampleClothingActor* clothingActor = static_cast<SampleClothingActor*>(m_actors[i]);
						if (clothingActor->getClothingActor()->rayCast(eyePos, pickDir, hitTime, hitNormal, vertexIndex))
						{
							if (hitTime < attachedHitDistance)
							{
								attachedHitDistance = hitTime;
								attachedClothingId = (physx::PxU32)i;
								attachedVertexId = vertexIndex;
								bestHitPosition = eyePos + pickDir * hitTime;
							}
						}
					}
				}

				if (attachedClothingId != physx::PxU32(-1))
				{
					assert(attachedClothingId < m_actors.size());
					assert(m_actors[attachedClothingId]->getType() == SampleClothingActor::TYPE);
					SampleClothingActor* clothingActor = static_cast<SampleClothingActor*>(m_actors[attachedClothingId]);
					clothingActor->getClothingActor()->attachVertexToGlobalPosition(attachedVertexId, bestHitPosition);

					// prevent raycast against rest of the scene
					return;
				}
			}
		}
		else if (CLOTH_DETACH == ie.m_Id)
		{
			if (attachedClothingId != physx::PxU32(-1))
			{
				SampleClothingActor* clothingActor = static_cast<SampleClothingActor*>(m_actors[attachedClothingId]);
				clothingActor->getClothingActor()->freeVertex(attachedVertexId);
				attachedClothingId = physx::PxU32(-1);
				attachedVertexId = 0;
			}
		}
		else if (CAMERA_MOUSE_LOOK == ie.m_Id)
		{
			if (attachedClothingId != physx::PxU32(-1) && renderer)
			{
				physx::PxU32 width  = 0;
				physx::PxU32 height = 0;
				renderer->getWindowSize(width, height);
				const physx::PxMat44& eyeTransform = getEyeTransform();
				physx::PxTransform view(eyeTransform.inverseRT());
				physx::PxVec3 eyePos  = eyeTransform.getPosition();
				physx::PxVec3 nearPos = unproject(m_projection, view, (x / (physx::PxF32)width) * 2 - 1, (y / (physx::PxF32)height) * 2 - 1);
				physx::PxVec3 pickDir = nearPos - eyePos;
				if (pickDir.normalize() > 0)
				{
					physx::PxVec3 newHitPosition = eyePos + pickDir * attachedHitDistance;

					SampleClothingActor* clothingActor = static_cast<SampleClothingActor*>(m_actors[attachedClothingId]);
					clothingActor->getClothingActor()->attachVertexToGlobalPosition(attachedVertexId, newHitPosition);
				}
			}
		}
	}

	SampleApexApplication::onPointerInputEvent(ie, x, y, dx, dy);
}

bool SampleClothingApplication::onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
{
	// if a benchmark is running ignore the key board.
	if (m_BenchmarkMode)
	{
		return true;
	}

	if (!getRenderer() || !m_apexScene)
	{
		return SampleApexApplication::onDigitalInputEvent(ie, val);
	}

	if (val)
	{
		switch (ie.m_Id)
		{
		case LOD_CHANGE:
		{
			int newLod = -1;
			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i]->getType() == SampleClothingActor::TYPE)
				{
					SampleClothingActor* clothingActor = static_cast<SampleClothingActor*>(m_actors[i]);
					newLod = clothingActor->changeRenderLOD();
				}
			}

			if (newLod >= 0)
			{
				m_lodSetting = newLod;
				m_lodTimeout = 2.0f;
			}
		}
		break;
		case SCENE_1:
		case SCENE_2:
		case SCENE_3:
		case SCENE_4:
		case SCENE_5:
		case SCENE_6:
		case SCENE_7:
		case SCENE_8:
		case SCENE_9:
		case SCENE_0:
		{
			physx::PxI32 sceneIndex = ie.m_Id - SCENE_1 + 1;
			if (sceneIndex > 0 && sceneIndex < (PxI32)getNumScenes())
			{
				releaseAssetsAndActors();
				releaseCameraAndLights();
				loadAssetsAndActors(sceneIndex);
				createCameraAndLights(!m_BenchmarkMode);
			}
		}
		break;
		case SCENE_SELECT:
		{
			physx::PxU32 sceneIndex = (m_sceneNumber + 1) % getNumScenes();
			releaseAssetsAndActors();
			releaseCameraAndLights();
			loadAssetsAndActors(sceneIndex);
			createCameraAndLights(!m_BenchmarkMode);
		}
		break;
		case ACTOR_ADD:
		case ACTOR_REMOVE:
		{
			releaseActors();

			m_benchmarkNumActors += ie.m_Id == ACTOR_ADD ? 1 : -1;

			if (m_benchmarkNumActors < 1)
			{
				m_benchmarkNumActors = MAX_CLOTHING_ACTOR_COUNT;
			}
			else if (m_benchmarkNumActors > MAX_CLOTHING_ACTOR_COUNT)
			{
				m_benchmarkNumActors = 1;
			}

			loadActors();
		}
		break;
		case HIDE_GRAPHICS:
			m_showMeshes = (m_showMeshes + 1) % 4;
			break;
		case SWITCH_ANIMATION:
			m_animationNumber++;
			releaseAssetsAndActors();
			releaseCameraAndLights();
			loadAssetsAndActors(m_sceneNumber);
			createCameraAndLights(!m_BenchmarkMode);
			break;
		case USE_LOCALSPACE:
			m_useLocalSpace = !m_useLocalSpace;

			for (unsigned int i = 0; i < m_actors.size(); ++i)
			{
				SampleFramework::SampleActor* sampleActor = m_actors[i];
				if (sampleActor->getType() == 9)
				{
					((SampleClothingActor*)sampleActor)->setLocalSpace(m_useLocalSpace);
				}
			}
			break;
		case USE_GPU:
			releaseActors();

			m_enableGpuPhysX = !m_enableGpuPhysX;

			loadActors();
			break;
		default:
			return SampleApexApplication::onDigitalInputEvent(ie, val);
		};
		return true;
	}
	else
	{
		return SampleApexApplication::onDigitalInputEvent(ie, val);
	}
}
