// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Clothing Sample
// Description: This sample program shows how to create APEX clothing actors.
//
// ===============================================================================

#include <SampleCommandLine.h>
#include <SampleApexApplication.h>
#include <SampleAssetManager.h>
#include <SampleMaterialAsset.h>
#include <SampleActor.h>
#include <SampleAnimatedMeshAsset.h>
#include <SampleAnimatedMeshActor.h>
#include <SampleClothingActor.h>
#include <SampleSpotLight.h>
#include <SamplePlatform.h>
#include <SampleUserInput.h>
#include <SampleUserInputIds.h>
#include <SampleInputEventIds.h>
#include <SampleInputDefines.h>

#include <Renderer.h>
#include <RendererBoxShape.h>
#include <RendererMeshContext.h>
#include <RendererMaterialDesc.h>
#include <RendererDirectionalLightDesc.h>
#include <RendererSpotLightDesc.h>
#include <RendererProjection.h>

#include "SampleBoxActor.h"
#include "UserAllocator.h"
#include "MeshImport.h"
#include "TriangleMesh.h"

#include <NxApex.h>
#include <NxModuleCommonLegacy.h>
#include <NxModuleFrameworkLegacy.h>
#include <NxModuleClothing.h>
#include <NxModuleClothingLegacy.h>
#include <NxClothingActor.h>
#include <NxClothingAsset.h>
#include <NxApexRenderDebug.h>
#include <NxParamUtils.h>

#include <SamplesVRDSettings.h>

#include "PsMathUtils.h"
#include "PsTime.h"
#include "PsUtilities.h"


#define ENABLE_DEBUG_RENDERING			1
#define SHOW_TIMING_STATS				1
#define MAX_CLOTHING_ACTOR_COUNT		32


enum SampleClothingInputEventIds
{
	SAMPLE_CLOTHING_FIRST = NUM_SAMPLE_BASE_INPUT_EVENT_IDS,

	CLOTH_ATTACH = SAMPLE_CLOTHING_FIRST,
	CLOTH_DETACH,

	SCENE_1,
	SCENE_2,
	SCENE_3,
	SCENE_4,
	SCENE_5,
	SCENE_6,
	SCENE_7,
	SCENE_8,
	SCENE_9,
	SCENE_0,

	LOD_CHANGE,
	USE_LOCALSPACE,
	USE_GPU,
	HIDE_GRAPHICS,
	SWITCH_ANIMATION,

	ACTOR_ADD,
	ACTOR_REMOVE,

	NUM_SAMPLE_CLOTHING_INPUT_EVENT_IDS,
	NUM_EXCLUSIVE_SAMPLE_CLOTHING_INPUT_EVENT_IDS = NUM_SAMPLE_CLOTHING_INPUT_EVENT_IDS - SAMPLE_CLOTHING_FIRST
};

using namespace physx;

class SampleClothingApplication : public SampleApexApplication
{
public:
	SampleClothingApplication(const SampleFramework::SampleCommandLine& cmdline);
	virtual ~SampleClothingApplication(void);
	PX_INLINE physx::NxClothingAsset* loadClothingAsset(const char* name);
	void processEarlyCmdLineArgs(void);
	void setupRendererDescription(SampleRenderer::RendererDesc& renDesc);

private:
	static SampleClothingApplication* m_AppPtr;

	static void benchmarkStart(const char* benchmarkName);
	virtual void onInit(void);
	virtual unsigned int getNumScenes() const;
	virtual unsigned int getSelectedScene() const;
	virtual const char* getSceneName(physx::PxU32 sceneNumber) const;
	virtual void selectScene(unsigned int sceneNumber);
	void loadAssetsAndActors(physx::PxU32 sceneNumber);
	void loadActors();
	void releaseActors();
	void releaseAssetsAndActors();
	void createCameraAndLights(bool withShadows);
	void releaseCameraAndLights();
	virtual void onShutdown(void);
	virtual void onTickPreRender(float dtime);
	void renderScene(const physx::PxMat44& eyeTransform, const SampleRenderer::RendererProjection& proj, SampleRenderer::RendererTarget* target, bool depthOnly);
	virtual void onRender(void);
	virtual void onTickPostRender(float dtime);
	virtual void collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents);
	virtual void collectInputDescriptions(std::vector<const char*>& inputDescriptions);

private:
	void onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy);
	virtual bool onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val);


private:
	mimp::MeshImporter*							m_meshImporterEzm;
	physx::apex::NxModuleClothing*				m_apexClothingModule;
	bool										m_apexSceneRunning;

	physx::PxU32								attachedClothingId;
	physx::PxF32								attachedHitDistance;
	physx::PxU32								attachedVertexId;

	SampleAnimatedMeshAsset*					m_animatedMesh;
	std::vector<physx::apex::NxClothingAsset*>	m_apexClothingAssets;
	physx::PxU32								m_separateActors;
	physx::PxF32								m_actorSpacing;
	physx::PxF32								m_startScale;
	physx::PxF32								m_endScale;
	physx::PxF32								m_randomAnimationMin;
	physx::PxF32								m_randomAnimationMax;
	physx::PxF32								m_windAdaption;
	physx::PxVec3								m_windVelocity;
	bool										m_rotateActors;
	bool										m_continuousAnimation;

	physx::PxU32								m_lodSetting;
	physx::PxF32								m_lodTimeout;

	struct SceneNames
	{
		enum Enum
		{
			Female_Cocktail = 1,
			Female_Dress,
			Female_SummerDress,
			Male_CapeLow,
			Male_CapeMed,
			Male_CapeHigh,
			Male_Trenchcoat_Low,
			Male_Trenchcoat_Med,
			Male_Trenchcoat_High,
			Male_Vacation,
			TrenchCoatLow,
			TrenchCoatMedium,
			TrenchCoatHigh,
			PantsShirtHigh,
			FlagLow,
			FlagMedium,
			FlagLoD,
			NUM_SCENES,
		};
	};

	SceneNames::Enum							m_sceneNumber;
	physx::PxU32								m_sceneFrames;
	physx::PxU32								m_animationNumber;
	const char*									m_sceneName;
	physx::PxU32								m_showMeshes;
	std::vector<SampleLight*>					m_sampleLights;

	bool										m_useLocalSpace;
	ClothSolverMode::Enum						m_clothSolverMode;
	bool										m_rewriteBuffers; // flag for rewriting static buffers on device reset
	bool										m_asynchronousRendering;
	bool										m_disableLod;
	bool										m_cpuFallback;
};

