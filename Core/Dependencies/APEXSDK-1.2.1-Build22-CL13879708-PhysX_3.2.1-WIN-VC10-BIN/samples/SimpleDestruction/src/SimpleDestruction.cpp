// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Destructible Sample
// Description: This sample program shows how to create APEX destructible actors.
//
// ===============================================================================

#include <algorithm>
#include <sstream>
#include <iomanip>

#include "SimpleDestruction.h"
#include "NxParamUtils.h"
#include "PsUtilities.h"
#include "SampleApexRenderer.h"
#include "SampleUserInput.h"
#include "SampleUserInputIds.h"
#include "SampleInputDefines.h"

#include "SamplePlatform.h"
#include "NxFromPx.h"

#include "NxModuleCommonLegacy.h"
#include "NxModuleFrameworkLegacy.h"
#include "NxDestructibleActor.h"
#include "NxDestructibleAsset.h"
#include "NxDestructiblePreview.h"
#include "NxModuleDestructible.h"
#include "NxModuleDestructibleLegacy.h"

#include "RendererDirectionalLightDesc.h"

#include "NxRenderMeshAsset.h"

#include "SampleDestructibleActor.h"
#include "SampleBoxActor.h"
#include "SampleInputEventIds.h"
#include "SimpleDestructionBoxActor.h"
#include "SimpleDestructionSync.h"
#include "SimpleDestructionReplay.h"

#if NX_SDK_VERSION_MAJOR == 2
#	include "Nx.h"
#	include "Nxp.h"
#	include "NxPhysics.h"
#	include "NxCooking.h"
#   include "NxTriangleMeshDesc.h"
#elif NX_SDK_VERSION_MAJOR == 3
#	include "cooking/PxCooking.h"
#	include "NxFromPx.h"
#	include "PxRigidStatic.h"
#	include "geometry/PxTriangleMesh.h"
#	include "cooking/PxTriangleMeshDesc.h"
#	include "geometry/PxTriangleMeshGeometry.h"
#	include "common/PxRenderBuffer.h"
#endif

#if APEX_USE_PARTICLES
#include "NxModuleFluidIos.h"
#include "NxModuleEmitter.h"
#include "NxModuleEmitterLegacy.h"
#include "NxModuleIofx.h"
#include "NxModuleIOFXLegacy.h"
#include "NxApexRenderVolume.h"
#if NX_SDK_VERSION_MAJOR==2
#	include "NxModuleFluidIos.h"
#elif NX_SDK_VERSION_MAJOR==3
#	include "NxModuleParticleIos.h"
#endif
#endif // APEX_USE_PARTICLES

#include "RendererTerrainShape.h"

///////////////////////////////////////////////////////////////////////////

struct SampleDestructionCommandLineInputIds
{
	enum Enum
	{
		YUP = SampleCommandLineInputIds::COUNT,
		USE_STATIC_MESH,
		DYNAMIC,
		TILE,
		LOAD_ASSET,
		CIRCLE,
		PILE,
		DEBUG_VIZ,

		SCENE,

		COWMOOMARK,
		WALMARK,
		HALLMARK,

#if USE_COMM_LAYER
		COMM,

		COUNT = COMM - YUP + 1,
#else
		COUNT = HALLMARK - YUP + 1,
#endif //USE_COMM_LAYER
		
	};
};

typedef SampleDestructionCommandLineInputIds SDCLIDS;
const CommandLineInput SampleDestructionCommandLineInputs[] =
{
	{ SDCLIDS::YUP,             "yup",                 "", "Assets loaded have y-up orientation (y/n)" },
	{ SDCLIDS::USE_STATIC_MESH, "usestaticmesh",       "", "Destructible will create a second (static mesh) to render static chunks (y/n).  Default = y." },
	{ SDCLIDS::DYNAMIC,         "dynamic",             "", "[0:1]: Set dynamic (vs static) mode for actors" },
	{
		SDCLIDS::TILE,          "tile",                "", "sets the x,y, and z (all integers) increments\n"
		"                                      over which assets loaded by loadasset are to be tiled.\n"
		"                                      For example tile 2,3,4 creates a grid of 2x3x4 actors\n"
		"                                      or 24 actors in all.\n"
	},
	{
		SDCLIDS::LOAD_ASSET,    "loadasset",           "", "<pdafile>: Create one actor at origin by default or at\n"
		"                                      the location specified by the tile command.\n"
		"                                      This argument must precede the benchmark name"
	},
	{ SDCLIDS::CIRCLE,          "circle",              "", "<pdafile>: Create a circle of actors" },
	{ SDCLIDS::PILE,            "pile",                "", "<pdafile>: Create a pile of actors" },
	{ SDCLIDS::DEBUG_VIZ,       "debugvisualization",  "", "display the debug visualization" },
	{ SDCLIDS::HALLMARK,        "hallMark",            "", "Run the Hall Benchmark. A hallway is created and each wall actor is destroyed." },
	{ SDCLIDS::COWMOOMARK,      "cowMooMark",          "", "Run the CowMooMark benchmark" },
	{ SDCLIDS::WALMARK,         "walMark",             "", "Run the WalMark benchmark" },
	
	{ SDCLIDS::SCENE,           "scene",               "", "<scene>: specifies the starting scene number" },
#if USE_COMM_LAYER
	{ SDCLIDS::COMM,            "asClient",            "", "Application is run as communications client" }
#endif //USE_COMM_LAYER
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleDestructionCommandLineInputs) == SDCLIDS::COUNT);

///////////////////////////////////////////////////////////////////////////

/*** Global variables ***/
const char* cowBmName  = "CowMooMark";
const char* wallBmName = "WalMark";
const char* hallBmName = "HallMark";
SampleDestructionApplication* gAppPtr = NULL;
typedef SampleDestructionApplication::DestructionSceneManager SceneManager;
typedef SampleDestructionApplication::DestructionSceneManager::DestructionSceneStateManager SceneState;
using SampleFramework::SampleUserInput;

const char* const SampleDestructionInputEventDescriptions[] =
{
	"shoot a ray",
	"shoot a box",
	"shoot a bunny",
	"shoot a cow",
	"shoot a sheet",
	"shoot a wall",

	"switch to scene 0",
	"switch to scene 1",
	"switch to scene 2",
	"switch to scene 3",
	"switch to scene 4",
	"switch to scene 5",
	"switch to scene 6",
	"switch to scene 7",
	"switch to scene 8",
	"switch to scene 9",

	"start/stop replay sequence",
	"change direction of replay sequence",
	"save the actor(s) state             (hold down (left ctrl) first)",
	"load the last saved actor(s) state  (hold down (left ctrl) first)",

	"toggle tessellation",
	"increase tessellation edge factor",
	"decrease tessellation edge factor",
	"increase tessellation displacement scale",
	"decrease tessellation displacement scale",
	"increase tessellation UV scale",
	"decrease tessellation UV scale",
	"increase tessellation LOD distance",
	"decrease tessellation LOD distance",

	"" /* "show actor add controls" */,
	"remove an added actor",
	"hide APEX render assets",

	"toggle hard sleeping",

	"trigger explosion"
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleDestructionInputEventDescriptions) == SampleDestructionInputEventIds::NUM_EXCLUSIVE_SAMPLE_DESTRUCTION_INPUT_EVENT_IDS);

/*** Local helper function ***/
void loadDestructibleActorBaseParams(NxParameterized::Interface* descParams, const physx::NxDestructibleAsset& asset)
{
	// Scene-specific parameters
	bool crumbleSmallestChunks = true;

	switch (gAppPtr->getSelectedScene())
	{
	case SampleDestructionApplication::DestructionSceneSelection::HallmarkConsole:
		crumbleSmallestChunks = false;
		break;
	case SampleDestructionApplication::DestructionSceneSelection::HallmarkPC:
		break;
	case SampleDestructionApplication::DestructionSceneSelection::WallConsole:
		crumbleSmallestChunks = false;
		break;
	case SampleDestructionApplication::DestructionSceneSelection::WallPC:
		break;
	case SampleDestructionApplication::DestructionSceneSelection::EasterIslandHeadsConsole:
		crumbleSmallestChunks = false;
		break;
	case SampleDestructionApplication::DestructionSceneSelection::EasterIslandHeadsPC:
		break;
	case SampleDestructionApplication::DestructionSceneSelection::EasterIslandHeadsVoronoi:
		crumbleSmallestChunks = false;
		break;
	case SampleDestructionApplication::DestructionSceneSelection::BlockTessellation:
		break;
	default:
		break;
	}

#if defined(PX_WINDOWS)
	NxParameterized::setParamBool(*descParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", crumbleSmallestChunks);
#else
	PX_UNUSED(crumbleSmallestChunks);
	NxParameterized::setParamBool(*descParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", false);
#endif

#if APEX_USE_PARTICLES
	if (gAppPtr->getPhysXIosModule() != NULL)
	{
#if NX_SDK_VERSION_MAJOR == 2
		NxParameterized::setParamString(*descParams, "crumbleEmitterName", "crumbleEmitter");
#elif NX_SDK_VERSION_MAJOR == 3
		NxParameterized::setParamString(*descParams, "crumbleEmitterName", "crumbleEmitterPhysX3");
#endif
	}
#endif // APEX_USE_PARTICLES

	NxParameterized::setParamF32(*descParams, "destructibleParameters.forceToDamage", 0.2f);
	NxParameterized::setParamF32(*descParams, "destructibleParameters.damageThreshold", 10.0f);
	NxParameterized::setParamF32(*descParams, "destructibleParameters.damageCap", 10.0f);
	NxParameterized::setParamF32(*descParams, "destructibleParameters.damageToRadius", 0.0f);
	NxParameterized::setParamF32(*descParams, "destructibleParameters.fractureImpulseScale", 2.0f);

	NxParameterized::setParamBool(*descParams, "formExtendedStructures", true);

	int dpCount = 0;
	NxParameterized::getParamArraySize(*descParams, "depthParameters", dpCount);
	const physx::PxI32 impactDamageDefaultDepth = asset.getDepthCount() > 2 ? (physx::PxI32)asset.getDepthCount() - 2 : (physx::PxI32)asset.getDepthCount() - 1;
	NxParameterized::setParamI32(*descParams, "destructibleParameters.impactDamageDefaultDepth", impactDamageDefaultDepth);
	for (int i = 0; i < dpCount; i++)
	{
		char tmpStr[128];
		physx::string::sprintf_s(tmpStr, 128, "depthParameters[%d].OVERRIDE_IMPACT_DAMAGE", i);
		NxParameterized::setParamBool(*descParams, tmpStr, false);
	}

#if NX_SDK_VERSION_MAJOR == 2
	NxParameterized::setParamBool(*descParams, "shapeDescTemplate.groupsMask.useGroupsMask", true);
	NxParameterized::setParamU32(*descParams, "shapeDescTemplate.groupsMask.bits0", 2);
	NxParameterized::setParamU32(*descParams, "shapeDescTemplate.groupsMask.bits2", ~0);
	NxParameterized::setParamU32(*descParams, "shapeDescTemplate.collisionGroup", 0);
	NxParameterized::setParamF32(*descParams, "actorDescTemplate.density", 1.0f);
#elif NX_SDK_VERSION_MAJOR == 3
	const physx::PxCookingParams& cookingParams = physx::NxGetApexSDK()->getCookingInterface()->getParams();
	NxParameterized::setParamU32(*descParams, "p3ShapeDescTemplate.simulationFilterData.word0", 2);
	NxParameterized::setParamU32(*descParams, "p3ShapeDescTemplate.simulationFilterData.word2", ~0);
	NxParameterized::setParamF32(*descParams, "p3BodyDescTemplate.density", 1.0f);
	NxParameterized::setParamF32(*descParams, "p3ShapeDescTemplate.restOffset", -cookingParams.skinWidth);
#endif
}

/*** FrameDistributionObject ***/
FrameDistributionObject::FrameDistributionObject(physx::PxU32 fireFrameNumber, physx::PxVec3 position)
	: m_fireFrameNumber(fireFrameNumber)
	, m_pos(position)
{
}

bool FrameDistributionObject::operator<(FrameDistributionObject rhs)
{
	return m_fireFrameNumber < rhs.m_fireFrameNumber;
}

bool FrameDistributionObject::reverseSortPredicate(const FrameDistributionObject& lhs, const FrameDistributionObject& rhs)
{
	return lhs.m_fireFrameNumber > rhs.m_fireFrameNumber;
}

/*** BoxBullet ***/
BoxBullet::BoxBullet(physx::PxU32   fireFrameNumber,
                     physx::PxVec3  pos,
                     physx::PxVec3  vel,
                     physx::PxVec3  extents,
                     physx::PxF32   density)
	: FrameDistributionObject(fireFrameNumber, pos)
	, m_Vel(vel)
	, m_Extents(extents)
	, m_Density(density)
{
}

/*** PointDamage ***/
PointDamage::PointDamage(physx::apex::NxDestructibleActor&  actor,
                         physx::PxU32			            frameNumber,
                         physx::PxF32                       damage,
                         physx::PxF32                       momentum,
                         const physx::PxVec3&               position,
                         const physx::PxVec3&               direction)
	: FrameDistributionObject(frameNumber, position)
	, m_actor(&actor)
	, m_damage(damage)
	, m_momentum(momentum)
	, m_direction(direction)
{}

void PointDamage::apply()
{
	if (m_actor)
	{
		m_actor->applyDamage(m_damage, m_momentum, m_pos, m_direction);
	}
}

/*** RadiusDamage ***/
RadiusDamage::RadiusDamage(physx::apex::NxDestructibleActor& actor,
                           physx::PxU32 frameNumber,
                           physx::PxF32 damage,
                           physx::PxF32 momentum,
                           const physx::PxVec3& position,
                           physx::PxF32 radius,
                           bool falloff)
	: FrameDistributionObject(frameNumber, position)
	, m_actor(&actor)
	, m_damage(damage)
	, m_momentum(momentum)
	, m_radius(radius)
	, m_falloff(falloff)
{}

void RadiusDamage::apply()
{
	if (m_actor)
	{
		m_actor->applyRadiusDamage(m_damage, m_momentum, m_pos, m_radius, m_falloff);
	}
}

/*** SampleDestructiblePreview ***/
SampleDestructiblePreview::SampleDestructiblePreview(SampleRenderer::Renderer& renderer, physx::apex::NxDestructibleAsset& asset, const physx::PxMat44& globalPose)
	: m_renderer(renderer)
	, m_asset(asset)
	, m_explodeSize(0.0f)
	, m_explodeDepth(0)
{
	NxParameterized::Interface* descParams = m_asset.getDefaultAssetPreviewDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	NxParameterized::setParamMat44(*descParams, "globalPose", globalPose);

	m_preview = (physx::NxDestructiblePreview*)asset.createApexAssetPreview(*descParams);
}

SampleDestructiblePreview::~SampleDestructiblePreview(void)
{
	if (m_preview)
	{
		m_preview->release();
	}
}

void SampleDestructiblePreview::render(bool /*rewriteBuffers*/)
{
	if (m_preview)
	{
		SampleApexRenderer apexRenderer;
		m_preview->lockRenderResources();
		m_preview->updateRenderResources();
		m_preview->dispatchRenderResources(apexRenderer);
		m_preview->unlockRenderResources();
	}
}

physx::apex::NxDestructiblePreview* SampleDestructiblePreview::getPreview()
{
	return m_preview;
}

physx::PxMat44 SampleDestructiblePreview::getPose()
{
	return m_preview->getPose();
}

void SampleDestructiblePreview::setPose(physx::PxMat44 pose)
{
	m_preview->setPose(pose);
	if (m_explodeSize)
	{
		m_preview->setExplodeView(m_explodeDepth, m_explodeSize);
	}
}

void SampleDestructiblePreview::changeExplodeSize(physx::PxF32 delta)
{
	m_explodeSize += delta;
	if (m_explodeSize < 0.0f)
	{
		m_explodeSize = 0.0f;
	}
	if (m_explodeSize > 8.0f)
	{
		m_explodeSize = 8.0f;
	}

	m_preview->setExplodeView(m_explodeDepth, m_explodeSize);
}

void SampleDestructiblePreview::cycleExplodeDepth()
{
	m_explodeDepth++;
	if (m_explodeDepth > 5)
	{
		m_explodeDepth = 0;
	}

	m_preview->setExplodeView(m_explodeDepth, m_explodeSize);
}

SampleDestructibleActor* SampleDestructiblePreview::instantiate(physx::apex::NxApexScene& scene, bool dynamic)
{
	NxParameterized::Interface* descParams = NULL;
	descParams = m_asset.getDefaultActorDesc();
	PX_ASSERT(descParams != NULL);
	loadDestructibleActorBaseParams(descParams, m_asset);
	NxParameterized::setParamMat44(*descParams, "globalPose", m_preview->getPose());
	NxParameterized::setParamBool(*descParams, "dynamic", dynamic);
	return new SampleDestructibleActor(m_renderer, scene, m_asset, descParams);
}

/*** SampleDestructionApplication ***/
SampleDestructionApplication::SampleDestructionApplication(const SampleFramework::SampleCommandLine& cmdline)
	: SampleApexApplication(cmdline)
#ifdef RENDERER_WINDOWS
	, m_kPlatformIsPC(true)
#else
	, m_kPlatformIsPC(false)
#endif //#ifdef RENDERER_WINDOWS
	, m_position(0.0f, 0.0f, 0.0f)
{
	m_FrameNumber				= 1;			// Frame number starts at 0.
	m_benchmarkNumActors		= 5;
#if APEX_USE_PARTICLES
	m_apexNxFluidIosModule		= 0;
	m_apexIofxModule			= 0;
	m_apexEmitterModule			= 0;
	m_renderVolume				= 0;
#endif // APEX_USE_PARTICLES
	m_apexDestructibleModule	= 0;
	m_terrainShape				= 0;
	m_groundMaterial			= NULL;
	m_terrainMesh				= NULL;
#if NX_SDK_VERSION_MAJOR == 2
	m_terrainActor				= NULL;
#endif
	m_simpleDestructiblePreview	= 0;
	m_perfScale					= 3;		// default to best scale
	m_dynamic					= false;	// default to static actors
	m_BoxBulletList.clear();
	m_boxBulletCounter			= 0;
	m_CowAsset					= NULL;
	for (physx::PxU32 i = 0; i < WallType::NUM_WALL_TYPES; ++i)
	{
		m_WallAsset[i] = NULL;
	}
	m_Yup						= true;
	m_RenderStaticChunkSeparately = true;
	m_hideGraphics				= false;
	m_hardSleeping				= false;
	m_explodeTimer				= 0;

	if (gAppPtr == NULL)
	{
		gAppPtr = this;
	}

	m_benchmarkNames.push_back(cowBmName);
	m_benchmarkNames.push_back(wallBmName);
	m_benchmarkNames.push_back(hallBmName);
	m_boxVelocityScale = 1.0f;
	m_useBunnyProjectile = false;
	m_velocity = physx::PxVec3(0.0f, 0.0f, 0.0f);

	// Debug rendering
	{
		DebugRenderConfiguration config;

		config.flags.push_back(DebugRenderFlag("LOD Benefits", "VISUALIZE_LOD_BENEFITS", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Destructible Support", "Destructible/VISUALIZE_DESTRUCTIBLE_SUPPORT"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Render normals", "RenderNormals", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Render normals", "RenderNormals", 1.0f));
		config.flags.push_back(DebugRenderFlag("Render tangents", "RenderTangents", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();


#if NX_SDK_VERSION_MAJOR == 2
		config.flags.push_back(DebugRenderFlag("Collision Shapes", NX_VISUALIZE_COLLISION_SHAPES, 1.0f));
#elif NX_SDK_VERSION_MAJOR == 3
		config.flags.push_back(DebugRenderFlag("Collision Shapes", physx::PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f));
#endif
		addDebugRenderConfig(config);
		config.flags.clear();

	}

	m_replay = new DestructibleReplay(*this);
	m_commandLineInput.registerCommands(&SampleDestructionCommandLineInputs[0], SDCLIDS::COUNT);
}

SampleDestructionApplication::~SampleDestructionApplication()
{
	PX_ASSERT(!m_lights.size());
	PX_ASSERT(!m_actors.size());
#if APEX_USE_PARTICLES
	PX_ASSERT(!m_apexNxFluidIosModule);
	PX_ASSERT(!m_apexIofxModule);
	PX_ASSERT(!m_renderVolume);
	PX_ASSERT(!m_apexEmitterModule);
#endif // APEX_USE_PARTICLES
	PX_ASSERT(!m_apexDestructibleModule);
	PX_ASSERT(!m_replay);
}

physx::apex::NxDestructibleAsset* SampleDestructionApplication::loadDestructibleAsset(const char* name)
{
	return (physx::apex::NxDestructibleAsset*)loadApexAsset(NX_DESTRUCTIBLE_AUTHORING_TYPE_NAME, name);
}

void SampleDestructionApplication::pushBackUniqueAsset(physx::apex::NxDestructibleAsset* candidateAsset)
{
	bool assetExist = false;
	for (std::vector<physx::apex::NxDestructibleAsset*>::const_iterator iter = m_destAssets.begin(); iter != m_destAssets.end(); ++iter)
	{
		if (*iter == candidateAsset)
		{
			assetExist = true;
			break;
		}
	}
	if (!assetExist)
	{
		m_destAssets.push_back(candidateAsset);
	}
}

void SampleDestructionApplication::loadAnAsset(SampleRenderer::Renderer* renderer, const char* assetName, const physx::PxVec3& scale)
{
	physx::apex::NxDestructibleAsset* asset;
	physx::PxMat44 globalPose;

	asset = loadDestructibleAsset(assetName);
	if (asset && asset->getRenderMeshAsset())
	{
		DestructionSceneManager::Instance().pushBackUniqueAsset(asset);
		globalPose = physx::PxMat44::createIdentity();
		globalPose.setPosition(m_position);
		if (!m_Yup)
		{
			globalPose.column3.y -= scale.z * asset->getRenderMeshAsset()->getBounds().minimum.z;
			globalPose.column1 = physx::PxVec4(0.0f, 0.0f, -1.0f, 0.0f);
			globalPose.column2 = physx::PxVec4(0.0f, 1.0f, 0.0f, 0.0f);
		}
		else
		{
			globalPose.column3.y -= scale.y * asset->getRenderMeshAsset()->getBounds().minimum.y;
		}
		globalPose.column3.y += OFFSET_HEIGHT;
		NxParameterized::Interface* descParams = NULL;
		descParams = asset->getDefaultActorDesc();
		PX_ASSERT(descParams != NULL);
		loadDestructibleActorBaseParams(descParams, *asset);

		NxParameterized::setParamBool(*descParams, "formExtendedStructures", true);
		NxParameterized::setParamMat44(*descParams, "globalPose", globalPose);
		NxParameterized::setParamVec3(*descParams, "scale", scale);
		NxParameterized::setParamBool(*descParams, "dynamic", m_dynamic);
		NxParameterized::setParamBool(*descParams, "renderStaticChunksSeparately", m_RenderStaticChunkSeparately);
		m_actors.push_back(new SampleDestructibleActor(*renderer, *m_apexScene, *asset, descParams, false, m_velocity));
	}

	forceLoadAssets();
}

void SampleDestructionApplication::loadAnAssetWithState(SampleRenderer::Renderer* renderer, const char* assetName, const char* actorStateName, const physx::PxVec3& scale)
{
	physx::apex::NxDestructibleAsset* asset = NULL;
	asset = loadDestructibleAsset(assetName);
	if (asset && asset->getRenderMeshAsset())
	{
		DestructionSceneManager::Instance().pushBackUniqueAsset(asset);

		NxParameterized::Interface* actorState = NULL;

		NxParameterizedSerializer deserializer(*m_apexSDK,
		                                       m_resourceCallback->findApexAsset(actorStateName),
		                                       extensionToType(actorStateName));
		deserializer >> actorState;
		if (NULL != actorState)
		{
			m_actors.push_back(new SampleDestructibleActor(*renderer, *m_apexScene, *asset, actorState, true));
		}
	}

	forceLoadAssets();
}


void SampleDestructionApplication::loadActorState(physx::apex::NxDestructibleActor* actor, const char* actorStateName)
{
	if (actor)
	{
		NxParameterized::Interface* actorState = NULL;
		NxParameterizedSerializer deserializer(*m_apexSDK,
		                                       m_resourceCallback->findApexAsset(actorStateName),
		                                       extensionToType(actorStateName));
		deserializer >> actorState;
		if (NULL != actorState)
		{
			actor->setNxParameterized(actorState);
			forceLoadAssets();
		}
	}
}

void SampleDestructionApplication::tileAnAsset(SampleRenderer::Renderer* renderer, const char* assetName, unsigned nx, unsigned ny, unsigned nz)
{
	if (nx* ny* nz == 0)
	{
		return;
	}

	physx::apex::NxDestructibleAsset* asset;
	physx::PxMat44 globalPose;

	asset = loadDestructibleAsset(assetName);
	if (asset && asset->getRenderMeshAsset())
	{
		const physx::PxVec3 scale(0.5f);
		DestructionSceneManager::Instance().pushBackUniqueAsset(asset);
		globalPose = physx::PxMat44::createIdentity();
		const physx::PxBounds3 bounds = asset->getRenderMeshAsset()->getBounds();
		physx::PxVec3 width = scale.multiply(bounds.maximum - bounds.minimum);
		if (!m_Yup)
		{
			m_position.y -= scale.z * bounds.minimum.z;
			globalPose.column1 = physx::PxVec4(0.0f, 0.0f, -1.0f, 0.0f);
			globalPose.column2 = physx::PxVec4(0.0f, 1.0f, 0.0f, 0.0f);
			physx::swap(width.y, width.z);
		}
		else
		{
			m_position.y -= scale.y * bounds.minimum.y;
		}
		m_position.y += OFFSET_HEIGHT;
		for (unsigned ix = 0; ix < nx; ++ix)
		{
			for (unsigned iy = 0; iy < ny; ++iy)
			{
				for (unsigned iz = 0; iz < nz; ++iz)
				{
					globalPose.setPosition(m_position + physx::PxVec3(((physx::PxF32)ix - 0.5f * (physx::PxF32)(nx - 1))*width.x, iy * width.y, iz * width.z));
					NxParameterized::Interface* descParams = NULL;
					descParams = asset->getDefaultActorDesc();
					PX_ASSERT(descParams != NULL);
					loadDestructibleActorBaseParams(descParams, *asset);
					NxParameterized::setParamMat44(*descParams, "globalPose", globalPose);
					NxParameterized::setParamVec3(*descParams, "scale", scale);
					NxParameterized::setParamBool(*descParams, "dynamic", m_dynamic);
					NxParameterized::setParamBool(*descParams, "renderStaticChunksSeparately", m_RenderStaticChunkSeparately);
					NxParameterized::setParamF32(*descParams, "sleepVelocityFrameDecayConstant", 100.0f);
					SampleDestructibleActor* newSampleDestructibleActor = new SampleDestructibleActor(*renderer, *m_apexScene, *asset, descParams, false);
					m_actors.push_back(newSampleDestructibleActor);
					DestructionSceneManager::Instance().mSceneDestructibleActorCount++;
				}
			}
		}
	}

	forceLoadAssets();
}

void SampleDestructionApplication::processEarlyCmdLineArgs(void)
{
	SampleApexApplication::initBenchmarks(&benchmarkStart);
	SampleApexApplication::processEarlyCmdLineArgs();

	typedef SampleDestructionCommandLineInputIds ID;
	for (ProcessedCommandLineInputs::const_iterator it = m_commandLineInput.inputCommandsBegin();
	        it != m_commandLineInput.inputCommandsEnd();
	        ++it)
	{
		const char* argPtr = it->value();
		switch (it->id())
		{
		case SDCLIDS::YUP:
			if (argPtr)
			{
				if (*argPtr == 'y' || *argPtr == 'Y')
				{
					m_Yup = true;
				}
				else if (*argPtr == 'n' || *argPtr == 'N')
				{
					m_Yup = false;
				}
			}
			break;
		case SDCLIDS::USE_STATIC_MESH:
			if (argPtr)
			{
				if (*argPtr == 'y' || *argPtr == 'Y')
				{
					m_RenderStaticChunkSeparately = true;
				}
				else if (*argPtr == 'n' || *argPtr == 'N')
				{
					m_RenderStaticChunkSeparately = false;
				}
			}
			break;
		case SDCLIDS::DYNAMIC:
			if (argPtr)
			{
				if (argPtr[0] == '1' || argPtr[0] == 't' || argPtr[0] == 'T')
				{
					m_dynamic = true;
				}
				else
				{
					m_dynamic = false;
				}
			}
			break;
#if USE_COMM_LAYER
		case SDCLIDS::COMM:
			if (argPtr)
			{
				if('0' == *argPtr)
					m_destructibleSyncManager.onCommandLineInit(CommTool::ConnectionType::CREATE_AS_SERVER);
				else if('1' == *argPtr)
					m_destructibleSyncManager.onCommandLineInit(CommTool::ConnectionType::CREATE_AS_CLIENT);
				else
					PX_ASSERT(!"bad command line argument for this set up!");
			}
			break;
#endif //USE_COMM_LAYER
			default:
			break;
		};
	}
}

void SampleDestructionApplication::onInit(void)
{
	SampleRenderer::Renderer* renderer = getRenderer();
	sampleInit("SimpleDestruction", false, false);    // no APEX CUDA
	m_apexScene = createSampleScene(physx::PxVec3(0, -9.8f, 0));
	if (!m_apexScene || !renderer)
	{
		return;
	}
#if APEX_MODULES_STATIC_LINK
	physx::apex::instantiateModuleDestructible();
	physx::apex::instantiateModuleDestructibleLegacy();
	physx::apex::instantiateModuleCommonLegacy();
	physx::apex::instantiateModuleFrameworkLegacy();
#if APEX_USE_PARTICLES
#	if NX_SDK_VERSION_MAJOR == 2
	physx::apex::instantiateModuleFluidIos();
#	elif NX_SDK_VERSION_MAJOR == 3
	physx::apex::instantiateModuleParticleIos();
#	endif
	physx::apex::instantiateModuleEmitter();
	physx::apex::instantiateModuleEmitterLegacy();
	physx::apex::instantiateModuleIofx();
	physx::apex::instantiateModuleIOFXLegacy();
#endif // APEX_USE_PARTICLES
#endif

#if APEX_USE_PARTICLES
#	if NX_SDK_VERSION_MAJOR == 2
	m_apexNxFluidIosModule = static_cast<physx::apex::NxModuleFluidIos*>(m_apexSDK->createModule("NxFluidIOS"));
#	elif NX_SDK_VERSION_MAJOR == 3
	m_apexNxFluidIosModule = static_cast<physx::apex::NxModuleParticleIos*>(m_apexSDK->createModule("ParticleIOS"));
#	endif // APEX_USE_PARTICLES

	if (m_apexNxFluidIosModule)
	{
		NxParameterized::Interface* params = m_apexNxFluidIosModule->getDefaultModuleDesc();
		m_apexNxFluidIosModule->init(*params);
	}

	m_apexIofxModule = static_cast<physx::apex::NxModuleIofx*>(m_apexSDK->createModule("IOFX"));
	PX_ASSERT(m_apexIofxModule);
	if (m_apexIofxModule)
	{
		NxParameterized::Interface* params = m_apexIofxModule->getDefaultModuleDesc();
		m_apexIofxModule->init(*params);
	}

	m_apexEmitterModule = static_cast<physx::apex::NxModuleEmitter*>(m_apexSDK->createModule("Emitter"));
	PX_ASSERT(m_apexEmitterModule);
	if (m_apexEmitterModule)
	{
		NxParameterized::Interface* params = m_apexEmitterModule->getDefaultModuleDesc();
		m_apexEmitterModule->init(*params);
	}
#endif

	m_apexDestructibleModule = static_cast<physx::apex::NxModuleDestructible*>(m_apexSDK->createModule("Destructible"));
	PX_ASSERT(m_apexDestructibleModule);
	if (m_apexDestructibleModule)
	{
		NxParameterized::Interface* params = m_apexDestructibleModule->getDefaultModuleDesc();
#if APEX_USE_GRB
		if (m_enableGpuPhysX)
		{
			NxParameterized::setParamI32(*params, "gpuRigidBodySettings.gpuDeviceOrdinal", -3);	// This value requests GRBs if a device is found, but fails silently
			NxParameterized::setParamF32(*params, "gpuRigidBodySettings.meshCellSize", 10.0f);
			
			// These can be altered if the GPU cannot allocate the default settings (64, 192)
			//NxParameterized::setParamU32(*params, "gpuRigidBodySettings.gpuMemSceneSize", 48);
			//NxParameterized::setParamU32(*params, "gpuRigidBodySettings.gpuMemTempDataSize", 124);
			
		}
#endif
		m_apexDestructibleModule->init(*params);
		gAppPtr->m_apexDestructibleModule->setLODEnabled(false);
	}

	loadModule("Common_Legacy");
	loadModule("Framework_Legacy");
	loadModule("Destructible_Legacy");

#if APEX_USE_PARTICLES
	loadModule("IOFX_Legacy");
	loadModule("Emitter_Legacy");

	if (m_apexScene && m_apexIofxModule)
	{
		physx::PxBounds3 b;
		b.setInfinite();
		m_renderVolume = m_apexIofxModule->createRenderVolume(*m_apexScene, b, 0, true);
	}
#endif // APEX_USE_PARTICLES

#if NX_SDK_VERSION_MAJOR == 2
	if (m_apexScene)
	{
		// Set collision filter
		NxScene* scene = m_apexScene->getPhysXScene();
		scene->setFilterOps(NX_FILTEROP_OR, NX_FILTEROP_OR, NX_FILTEROP_SWAP_AND);
		scene->setFilterBool(true);

		NxGroupsMask zeroMask;
		zeroMask.bits0 = zeroMask.bits1 = zeroMask.bits2 = zeroMask.bits3 = 0;
		scene->setFilterConstant0(zeroMask);
		scene->setFilterConstant1(zeroMask);

		m_shapeGroupMask.bits0 = 1;
		m_shapeGroupMask.bits2 = ~0;
		m_shapeGroupMask.bits1 = m_shapeGroupMask.bits3 = 0;
	}
#endif


#if CREATE_TRIMESH_TERRAIN
	createTrimeshTerrain();
	addCollisionShape(HalfSpaceShapeType, physx::PxVec3(0, -10.0f, 0), physx::PxVec3(0, 1, 0), physx::PxVec3(0, 0, 0));
#else
	// ground plane
	addCollisionShape(HalfSpaceShapeType, physx::PxVec3(0, 0, 0), physx::PxVec3(0, 1, 0), physx::PxVec3(0, 0, 0));
#endif

	SampleRenderer::RendererDirectionalLightDesc lightdesc;
	lightdesc.intensity = 1.0f;

	lightdesc.color     = SampleRenderer::RendererColor(200, 200, 200, 255);
	lightdesc.direction = physx::PxVec3(-0.707f, -0.707f, -0.707f);
	m_lights.push_back(renderer->createLight(lightdesc));

#if !defined(PX_ANDROID)
	lightdesc.color     = SampleRenderer::RendererColor(200, 200, 200, 255);
	lightdesc.direction = physx::PxVec3(0.707f, -0.707f,  0.707f);
	m_lights.push_back(renderer->createLight(lightdesc));
#endif

	if (m_apexScene && m_apexDestructibleModule)
	{
		m_resourceCallback->setApexSupport(*m_apexSDK);

#if NX_SDK_VERSION_MAJOR == 2
		// setup collision for the crumble particle IOS
		NxGroupsMask g;
		g.bits0 = 3;
		g.bits2 = ~0;
		g.bits1 = g.bits3 = 0;
		m_resourceCallback->registerNxGroupsMask128("PlaneAndDestructibleGroup", g);
		m_resourceCallback->registerNxCollisionGroup("PlaneAndDestructibleGroup", 0);
#elif NX_SDK_VERSION_MAJOR == 3
		physx::PxFilterData g;
		g.word0 = 3;
		g.word2 = ~0;
		g.word1 = g.word3 = 0;
		m_resourceCallback->registerSimulationFilterData("PlaneAndDestructibleGroup", g);
#endif

#if USE_COMM_LAYER
		//set up for sync, before actors are loaded
		if(CommTool::ConnectionType::COUNT != m_destructibleSyncManager.getConnectionType())
		{
			const bool connectionEstablished = m_destructibleSyncManager.onCreate();
			if(connectionEstablished)
			{
				PX_ASSERT(NULL != m_apexDestructibleModule);
				m_destructibleSyncManager.setUpModuleSyncParams(*m_apexDestructibleModule);
			}
		}
#endif //USE_COMM_LAYER
		//        static physx::PxU32 maxChunk[] = { 0, 50, 500, 4000 };
		//        static physx::PxU32 maxDepthOffset[] = { 0, 1, 1, 0 };
		//        static physx::PxF32 maxSeparation[] = { 0.0f, 0.2f, 0.5f, 1.0f };

		for (physx::PxU32 i = 0; i < m_benchmarkNames.size(); i++)
		{
			if (m_cmdline.hasSwitch(m_benchmarkNames[i]))
			{
				if (m_CmdLineBenchmarkMode == false)
				{
					// only 1 benchmark can be run from the command line.
					// OA mode can command sequential bench marks...
					benchmarkStart(m_benchmarkNames[i]);
				}
				break;
			}
		}

		physx::PxU32 sceneID = DestructionSceneSelection::CommandLineScene;
		if (!(hasInputCommand(SCLIDS::OPEN_AUTOMATE) ||
		      inputCommandValue(SDCLIDS::TILE)       ||
		      inputCommandValue(SDCLIDS::LOAD_ASSET) ||
		      inputCommandValue(SDCLIDS::PILE)       ||
		      inputCommandValue(SDCLIDS::CIRCLE)     ||
		      m_CmdLineBenchmarkMode))
		{
			sceneID = m_commandLineInput.inputCommandValueWithDefault(SDCLIDS::SCENE,
				m_kPlatformIsPC ? (physx::PxU32)DestructionSceneSelection::HallmarkPC :
			                      (physx::PxU32)DestructionSceneSelection::WallConsole);
		}
		DestructionSceneManager::Instance().onInit((DestructionSceneSelection::Enum)sceneID, this);

		physx::PxMat44 globalPose;
		physx::apex::NxDestructibleAsset* asset;
		int nx = 1;
		int ny = 1;
		int nz = 1;

		// parse the command line arguments
		for (ProcessedCommandLineInputs::const_iterator it  = m_commandLineInput.inputCommandsBegin();
		        it != m_commandLineInput.inputCommandsEnd();
		        ++it)
		{
			const char* argPtr = it->value();
			switch (it->id())
			{
			case SDCLIDS::TILE:
				if (argPtr)
				{
					sscanf(argPtr, "%d,%d,%d", &nx, &ny, &nz);
				}
				break;
			case SDCLIDS::LOAD_ASSET:
				if (argPtr)
				{
					DestructionSceneManager::Instance().setCustomSceneAssetName(argPtr);
					m_apexDestructibleModule->setMaxChunkDepthOffset(0);
					tileAnAsset(renderer, argPtr, nx, ny, nz);
				}
				break;
			case SDCLIDS::PILE:
				if (argPtr)
				{
					m_apexDestructibleModule->setMaxChunkDepthOffset(0);
					asset = loadDestructibleAsset(argPtr);
					if (asset)
					{
						DestructionSceneManager::Instance().pushBackUniqueAsset(asset);

						globalPose = physx::PxMat44::createIdentity();
#if 1 // Cow pile
						for (physx::PxF32 x = -4.5f; x < 0.5f; x += 5.0f)
							for (physx::PxF32 y = 1;     y < 30;   y += 5.0f)
								for (physx::PxF32 z = -2.5f; z < 5.0f; z += 2.5f)
#else // Bunny pile
						globalPose.M.rotX(physx::degToRad(-90.0f));
						for (physx::PxF32 x = 8.0f;  x < 11.0f; x += 5.0f)
							for (physx::PxF32 y = 5;     y < 40;    y += 6.0f)
								for (physx::PxF32 z = -2.5f; z < 5.0f;  z += 2.5f)
#endif
								{
									globalPose.setPosition(physx::PxVec3(x, y, z));
									NxParameterized::Interface* descParams = NULL;
									descParams = asset->getDefaultActorDesc();
									PX_ASSERT(descParams != NULL);
									loadDestructibleActorBaseParams(descParams, *asset);
									NxParameterized::setParamMat44(*descParams, "globalPose", globalPose);
									NxParameterized::setParamVec3(*descParams, "scale", physx::PxVec3(0.5f));
									NxParameterized::setParamBool(*descParams, "dynamic", m_dynamic);
									m_actors.push_back(new SampleDestructibleActor(*renderer, *m_apexScene, *asset, descParams));
									DestructionSceneManager::Instance().mSceneDestructibleActorCount++;
								}
					}
				}
				break;
			case SDCLIDS::CIRCLE:
				if (argPtr)
				{
					m_apexDestructibleModule->setMaxChunkDepthOffset(0);
					asset = loadDestructibleAsset(argPtr);
					if (asset)
					{
						DestructionSceneManager::Instance().pushBackUniqueAsset(asset);
						globalPose = physx::PxMat44::createIdentity();
						physx::PxF32 inc = 0.3f;
						physx::PxF32 rad = 15;
						for (physx::PxF32 a = 0; a < 2 * physx::PxPi - inc; a += inc)
						{
#if 0 // Rotate (for bunny)
							physx::PxMat44 temp;
							temp = physx::PxMat44::createIdentity();
							globalPose = physx::PxMat44::createIdentity();
							globalPose.M.rotX(physx::degToRad(-90.0f));
							temp.M.rotZ(-a);
							globalPose.multiply(globalPose, temp);
#endif
							globalPose.setPosition(physx::PxVec3(physx::PxCos(a)*rad, 1.5f, physx::PxSin(a)*rad));
							NxParameterized::Interface* descParams = NULL;
							descParams = asset->getDefaultActorDesc();
							PX_ASSERT(descParams != NULL);
							loadDestructibleActorBaseParams(descParams, *asset);
							NxParameterized::setParamMat44(*descParams, "globalPose", globalPose);
							NxParameterized::setParamVec3(*descParams, "scale", physx::PxVec3(0.5f));
							NxParameterized::setParamBool(*descParams, "dynamic", m_dynamic);
							m_actors.push_back(new SampleDestructibleActor(*renderer, *m_apexScene, *asset, descParams));
							DestructionSceneManager::Instance().mSceneDestructibleActorCount++;
						}
					}
				}
				break;
			case SDCLIDS::DEBUG_VIZ:
				nextDebugRenderConfig(1);
				break;
			default:
				break;
			};
		}
	}

	m_apexScene->allocViewMatrix(physx::ViewMatrixType::LOOK_AT_RH);
	m_apexScene->allocProjMatrix(physx::ProjMatrixType::USER_CUSTOMIZED);
}

void SampleDestructionApplication::onShutdown(void)
{
	for (physx::PxU32 i = 0; i < m_lights.size(); i++)
	{
		m_lights[i]->release();
	}
	m_lights.clear();
	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		m_actors[i]->release();
	}
	m_actors.clear();

	if (m_terrainShape)
	{
		delete m_terrainShape;
		m_terrainShape = 0;
	}
	if (m_CowAsset)
	{
		releaseApexAsset(m_CowAsset);
		m_CowAsset = 0;
	}
	for (physx::PxU32 i = 0; i < WallType::NUM_WALL_TYPES; ++i)
	{
		if (m_WallAsset[i])
		{
			m_WallAsset[i] = NULL;
		}
	}
	for (physx::PxU32 i = 0; i < m_destAssets.size(); i++)
	{
		releaseApexAsset(m_destAssets[i]);
	}
	m_destAssets.clear();

	if (m_apexDestructibleModule)
	{
		m_apexSDK->releaseModule(m_apexDestructibleModule);
		m_apexDestructibleModule = 0;
	}

#if APEX_USE_PARTICLES
	if (m_renderVolume)
	{
		m_apexIofxModule->releaseRenderVolume(*m_renderVolume);
		m_renderVolume = 0;
	}
	if (m_apexIofxModule)
	{
		m_apexSDK->releaseModule(m_apexIofxModule);
		m_apexIofxModule = 0;
	}
	if (m_apexNxFluidIosModule)
	{
		m_apexSDK->releaseModule(m_apexNxFluidIosModule);
		m_apexNxFluidIosModule = 0;
	}
	if (m_apexEmitterModule)
	{
		m_apexSDK->releaseModule(m_apexEmitterModule);
		m_apexEmitterModule = 0;
	}
#endif // APEX_USE_PARTICLES

	if (m_terrainMesh)
	{
#if NX_SDK_VERSION_MAJOR == 2
		if (m_terrainActor)
		{
			m_apexScene->getPhysXScene()->releaseActor(*m_terrainActor);
			m_terrainActor = NULL;
		}
		m_physxSDK->releaseTriangleMesh(*m_terrainMesh);
#elif NX_SDK_VERSION_MAJOR == 3
		m_terrainMesh->release();
#endif
		if (m_groundMaterial)
		{
			getAssetManager()->returnAsset(*m_groundMaterial);
			m_groundMaterial = NULL;
		}
		m_terrainMesh = 0;
		m_terrainContext.mesh = 0;
	}

	delete m_replay;
	m_replay = 0;

	DestructionSceneManager::Instance().onDestroy();

#if USE_COMM_LAYER
	if(CommTool::ConnectionType::COUNT != m_destructibleSyncManager.getConnectionType())
	{
		m_destructibleSyncManager.onDestroy();
	}
#endif //USE_COMM_LAYER

	SampleApexApplication::onShutdown();
}

void SampleDestructionApplication::onTickPreRender(float dtime)
{
	//If we are running in benchmark mode then we need to
	//ensure that the timestep is the same every frame.
	//This ensures that exactly the same set of substeps
	//is computed every time the benchmark is run.
	
	
	// if (m_BenchmarkMode)
	{
		dtime = 1.0f / 60.0f;
	}

	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i])
		{
			m_actors[i]->tick(dtime);

			if (m_actors[i]->getType() == SampleDestructibleActor::DestructibleActor)
			{
				physx::apex::NxDestructibleActor* dactor = ((SampleDestructibleActor*)m_actors[i])->getActor();
				if (m_hardSleeping != dactor->isHardSleepingEnabled())
				{
					if (m_hardSleeping)
					{
						dactor->enableHardSleeping();
					}
					else
					{
						dactor->disableHardSleeping(true);
					}
				}
			}
		}
	}

	if (m_explodeTimer)
	{
		if (m_explodeTimer == 1)
		{
			const physx::PxVec3 eyePosition = getEyeTransform().getPosition();
			const physx::PxF32 force = 1000.0f;
			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i])
				{
					if (m_actors[i]->getType() == SampleDestructibleActor::DestructibleActor)
					{
						physx::apex::NxDestructibleActor* dactor = ((SampleDestructibleActor*)m_actors[i])->getActor();
#if NX_SDK_VERSION_MAJOR == 2
						NxActor** buffer;
#else
						physx::PxRigidDynamic** buffer;
#endif
						physx::PxU32 bufferSize;
						const physx::PxU32 flags = physx::NxDestructiblePhysXActorQueryFlags::Dynamic;
						if (dactor->acquirePhysXActorBuffer(buffer, bufferSize, flags))
						{
							for (physx::PxU32 actorIndex = 0; actorIndex < bufferSize; ++actorIndex)
							{
#if NX_SDK_VERSION_MAJOR == 2
								NxVec3 dir = buffer[actorIndex]->getGlobalPosition() - NxVec3(&eyePosition.x);
								dir.normalize();
#else
								const physx::PxVec3 dir = (buffer[actorIndex]->getGlobalPose().p - eyePosition).getNormalized();
#endif
								buffer[actorIndex]->addForce(dir*force);
							}
							dactor->releasePhysXActorBuffer();
						}
					}
				}
			}
		}
		if (++m_explodeTimer == 60)
		{
			m_explodeTimer = 0;
		}
	}

	if (m_apexScene && !m_pause)
	{
		m_apexScene->simulate(dtime);
	}

#if USE_COMM_LAYER
	if(CommTool::ConnectionType::COUNT != m_destructibleSyncManager.getConnectionType())
	{
		m_destructibleSyncManager.onTick();
	}
#endif //USE_COMM_LAYER

	SampleApexApplication::onTickPreRender(dtime);
}

void SampleDestructionApplication::onRender(void)
{
	SampleRenderer::Renderer* renderer = getRenderer();
	if (renderer)
	{
		physx::PxU32 windowWidth  = 0;
		physx::PxU32 windowHeight = 0;
		renderer->getWindowSize(windowWidth, windowHeight);
		if (windowWidth > 0 && windowHeight > 0)
		{
			renderer->clearBuffers();

			m_projection = SampleRenderer::RendererProjection(45.0f, windowWidth / (float)windowHeight, 0.1f, 10000.0f);

			// The APEX scene needs the view and projection matrices for LOD, debug rendering, and other things
			updateApexSceneMatrices();

			if (m_terrainContext.isValid())
			{
				renderer->queueMeshForRender(m_terrainContext);
			}

			for (physx::PxU32 i = 0; i < m_lights.size(); i++)
			{
				renderer->queueLightForRender(*m_lights[i]);
			}

			if (!m_hideGraphics)
			{
				for (physx::PxU32 i = 0; i < m_actors.size(); i++)
				{
					if (m_actors[i])
					{
						if (m_replay->isPlaying() && !m_replay->validActorIndex(i))
						{
							continue;
						}

						m_actors[i]->render(m_rewriteBuffers);
					}
				}
			}

			if (m_apexScene)
			{
				SampleApexRenderer apexRenderer;
				m_apexScene->prepareRenderResourceContexts();
				m_apexScene->lockRenderResources();
				m_apexScene->updateRenderResources();
				m_apexScene->dispatchRenderResources(apexRenderer);
				m_apexScene->unlockRenderResources();
			}

#if APEX_USE_PARTICLES
			if (m_renderVolume && m_apexScene)
			{
				SampleApexRenderer apexRenderer;
				m_renderVolume->lockRenderResources();
				// m_renderVolume->getBounds() is safe while locked
				m_renderVolume->updateRenderResources(NULL);
				m_renderVolume->dispatchRenderResources(apexRenderer);
				m_renderVolume->unlockRenderResources();
			}
#endif // APEX_USE_PARTICLES

			if (m_apexRenderDebug)
			{
				SampleApexRenderer apexRenderer;
				m_apexRenderDebug->updateRenderResources(0);
				m_apexRenderDebug->dispatchRenderResources(apexRenderer);
			}

			for (physx::PxU32 i = 0; i < m_shape_actors.size(); i++)
			{
				if (m_shape_actors[i])
				{
					m_shape_actors[i]->render();
				}
			}

			renderer->render(getEyeTransform(), m_projection);

#if defined(RENDERER_TABLET)
			/* draw screenquad & controls */
			SampleRenderer::ScreenQuad sq;
			sq.mLeftUpColor     = SampleRenderer::RendererColor(0x00, 0x00, 0x80);
			sq.mRightUpColor    = SampleRenderer::RendererColor(0x00, 0x00, 0x80);
			sq.mLeftDownColor   = SampleRenderer::RendererColor(0xff, 0xf0, 0xf0);
			sq.mRightDownColor  = SampleRenderer::RendererColor(0xff, 0xf0, 0xf0);
			renderer->drawScreenQuad(sq);
#endif

			///////////////////////////////////////////////////////////////////////////

			std::string sceneInfo;
#if APEX_USE_GRB
			if (m_apexDestructibleModule && m_apexScene && m_apexDestructibleModule->isGrbSimulationEnabled(*m_apexScene))
			{
				sceneInfo.append("(GPU)");
			}
			else
			{
				sceneInfo.append("(CPU)");
			}
#endif
			std::string additionalInfo;
			m_replay->appendStatus(additionalInfo);

			printSceneInfo(sceneInfo.c_str(), additionalInfo.c_str());
		}
	}
}

void SampleDestructionApplication::onTickPostRender(float dtime)
{
	dumpApexStats(m_apexScene);
	if (m_apexScene && !m_pause)
	{
		physx::PxU32 errorState = 0;
		m_apexScene->fetchResults(true, &errorState);

#if NX_SDK_VERSION_MAJOR == 2
		const NxDebugRenderable* debugRenderable = m_apexScene->getPhysXScene()->getDebugRenderable();
		if (debugRenderable && m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(*debugRenderable);
		}
#elif NX_SDK_VERSION_MAJOR == 3
		const physx::PxRenderBuffer& renderBuffer = m_apexScene->getPhysXScene()->getRenderBuffer();
		if (m_apexRenderDebug != NULL)
		{
			m_apexRenderDebug->addDebugRenderable(renderBuffer);
		}
#endif
	}

	// call the Open Automate handler...
	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::IDLE))
	{
		m_OaInterface->OAHandler();
	}

	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::EXIT))
	{
		close();
	}

	//if in interactive hallmark scene (not benchmark)
	if (triggerDamageEnabled() && !m_BenchmarkMode)
	{
		triggerDamage();
	}

	//if (in command line benchmarking) or (in OA benchmarking...)
	if (m_BenchmarkMode)
	{
		// are there still frames to run and No errors have been reported...
		if ((m_FrameNumber <= m_benchmarkNumFrames) && (UserErrorCallback::instance()->getNumErrors() == 0))
		{
			SampleApexApplication::reportOaResults();

			triggerDamage();
		}
		else  // no more frames to run.  benchmark is done
		{
			if (m_CmdLineBenchmarkMode) // close it down and exit the program
			{
				close();
			}
			else
			{
				if (m_OaInterface->getOaState() == OAState::BENCHMARKING)
				{
					//done with the benchmark
					oaValue v;
					v.Float = m_BenchmarkElapsedTime;
					m_OaInterface->setOaState(OAState::IDLE);
					oaAddResultValue("Total run time", OA_TYPE_FLOAT, &v);
					SampleApexApplication::reportOaResultsValues();
					m_OaInterface->endBenchmark();
					// release the actors created by this benchmark in case OA runs another benchmark without exiting!
					for (physx::PxU32 i = 0; i < m_actors.size(); i++)
					{
						m_actors[i]->release();
					}
					m_actors.clear();
				}
			}
		}
	}

	if (m_replay)
		m_replay->tick();

	SampleApexApplication::onTickPostRender(dtime);
}

void SampleDestructionApplication::collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
{
	using namespace SampleFramework;

	typedef SampleDestructionInputEventIds IDS;

	std::vector<const char*> inputDescriptions;
	collectInputDescriptions(inputDescriptions);

	DIGITAL_INPUT_EVENT_DEF_2(IDS::BOX_SHOT,   SCAN_CODE_SPACE, XKEY_SPACE,	PS3KEY_SPACE,	AKEY_UNKNOWN,	OSXKEY_SPACE,	PSP2KEY_UNKNOWN, IKEY_UNKNOWN, 	LINUXKEY_SPACE);
	ANALOG_INPUT_EVENT_DEF_2(IDS::RAYCAST_HIT, MOUSE_BUTTON_RIGHT_DOWN, XKEY_UNKNOWN, PS3KEY_UNKNOWN, AFREETAP, MOUSE_MOVE, PSP2KEY_UNKNOWN, IKEY_UNKNOWN, MOUSE_BUTTON_RIGHT_DOWN);

	DIGITAL_INPUT_EVENT_DEF_2(IDS::BUNNY_SHOT,			WKEY_B,			XKEY_B,			PS3KEY_B,		AKEY_UNKNOWN,	OSXKEY_B,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_B);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::COW_SHOT,			WKEY_Z,			XKEY_Z,			PS3KEY_Z,		AKEY_UNKNOWN,	OSXKEY_Z,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Z);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SHEET_SHOT,			WKEY_N,			XKEY_N,			PS3KEY_N,		AKEY_UNKNOWN,	OSXKEY_N,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_N);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::WALL_SHOT,			WKEY_X,			XKEY_X,			PS3KEY_X,		AKEY_UNKNOWN,	OSXKEY_X,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_X);

	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_0,				WKEY_0,			XKEY_0,			PS3KEY_0,		AKEY_0,	OSXKEY_0,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_0);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_1,				WKEY_1,			XKEY_1,			PS3KEY_1,		AKEY_1,	OSXKEY_1,		PSP2KEY_UNKNOWN,	IBUTTON_2,		LINUXKEY_1);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_2,				WKEY_2,			XKEY_2,			PS3KEY_2,		AKEY_2,	OSXKEY_2,		PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_2);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_3,				WKEY_3,			XKEY_3,			PS3KEY_3,		AKEY_3,	OSXKEY_3,		PSP2KEY_UNKNOWN,	IBUTTON_3,		LINUXKEY_3);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_4,				WKEY_4,			XKEY_4,			PS3KEY_4,		AKEY_4,	OSXKEY_4,		PSP2KEY_UNKNOWN,	IBUTTON_4,		LINUXKEY_4);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_5,				WKEY_5,			XKEY_5,			PS3KEY_5,		AKEY_5,	OSXKEY_5,		PSP2KEY_UNKNOWN,	IBUTTON_5,		LINUXKEY_5);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_6,				WKEY_6,			XKEY_6,			PS3KEY_6,		AKEY_6,	OSXKEY_6,		PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_6);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_7,				WKEY_7,			XKEY_7,			PS3KEY_7,		AKEY_7,	OSXKEY_7,		PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_7);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_8,				WKEY_8,			XKEY_8,			PS3KEY_8,		AKEY_8,	OSXKEY_8,		PSP2KEY_UNKNOWN,	IBUTTON_8,		LINUXKEY_8);

	if (m_replay && m_replay->isEnabled())
	{
		DIGITAL_INPUT_EVENT_DEF_2(IDS::TOGGLE_REPLAY_SAVE,		WKEY_Q,	XKEY_Q,			PS3KEY_Q,		AKEY_UNKNOWN,	OSXKEY_Q,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Q);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::TOGGLE_REPLAY_DIRECTION,	WKEY_R,	XKEY_R,			PS3KEY_R,		AKEY_UNKNOWN,	OSXKEY_R,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_R);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::SAVE_ACTOR_STATE,		WKEY_X,	XKEY_X,			PS3KEY_X,		AKEY_UNKNOWN,	OSXKEY_X,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_X);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::LOAD_SAVED_ACTOR_STATE,	WKEY_Z,	XKEY_Z,			PS3KEY_Z,		AKEY_UNKNOWN,	OSXKEY_Z,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Z);
	}

	if (m_renderer && m_renderer->isTessellationSupported())
	{
		DIGITAL_INPUT_EVENT_DEF_2(IDS::SCENE_9,				WKEY_9,			XKEY_9,			PS3KEY_9,		AKEY_9,	OSXKEY_9,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_9);

		DIGITAL_INPUT_EVENT_DEF_2(IDS::TOGGLE_TESSELLATION,				WKEY_T,		XKEY_T,		PS3KEY_T,		AKEY_UNKNOWN,	OSXKEY_T,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_T);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::INCREASE_TESSELLATION_FACTOR,	WKEY_Y,		XKEY_Y,		PS3KEY_Y,		AKEY_UNKNOWN,	OSXKEY_Y,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_Y);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::DECREASE_TESSELLATION_FACTOR,	WKEY_U,		XKEY_U,		PS3KEY_U,		AKEY_UNKNOWN,	OSXKEY_U,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_U);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::INCREASE_TESSELLATION_SCALE,		WKEY_H,		XKEY_H,		PS3KEY_H,		AKEY_UNKNOWN,	OSXKEY_H,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_H);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::DECREASE_TESSELLATION_SCALE,		WKEY_J,		XKEY_J,		PS3KEY_J,		AKEY_UNKNOWN,	OSXKEY_J,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_J);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::INCREASE_TESSELLATION_UV_SCALE,	WKEY_COMMA,	XKEY_COMMA,	PS3KEY_COMMA,	AKEY_UNKNOWN,	OSXKEY_COMMA,	PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_COMMA);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::DECREASE_TESSELLATION_UV_SCALE,	WKEY_DIVIDE,XKEY_DIVIDE,PS3KEY_DIVIDE,	AKEY_UNKNOWN,	OSXKEY_DIVIDE,	PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_DIVIDE);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::INCREASE_TESSELLATION_LOD_DIST,	WKEY_K,		XKEY_K,		PS3KEY_K,		AKEY_UNKNOWN,	OSXKEY_K,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_K);
		DIGITAL_INPUT_EVENT_DEF_2(IDS::DECREASE_TESSELLATION_LOD_DIST,	WKEY_L,		XKEY_L,		PS3KEY_L,		AKEY_UNKNOWN,	OSXKEY_L,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_L);
		DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_WIREFRAME,						WKEY_I,		XKEY_I,		PS3KEY_I,		AKEY_UNKNOWN,	OSXKEY_I,		PSP2KEY_UNKNOWN, IKEY_UNKNOWN,	LINUXKEY_I);
	}

	DIGITAL_INPUT_EVENT_DEF_2(IDS::ACTOR_SERIALIZATION_MODIFIER,	WKEY_CONTROL,	XKEY_CONTROL,	PS3KEY_CONTROL,	 AKEY_UNKNOWN,	OSXKEY_CONTROL,	 PSP2KEY_UNKNOWN, IKEY_UNKNOWN, LINUXKEY_CONTROL);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::REMOVE_ACTOR,					WKEY_SUBTRACT,	XKEY_SUBTRACT,	PS3KEY_SUBTRACT, AKEY_UNKNOWN,	OSXKEY_SUBTRACT, PSP2KEY_UNKNOWN, IKEY_UNKNOWN, LINUXKEY_SUBSTRACT);
	DIGITAL_INPUT_EVENT_DEF_2(IDS::HIDE_GRAPHICS,			WKEY_G,					XKEY_G,			PS3KEY_G,		AKEY_G,			OSXKEY_G,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_G);

	DIGITAL_INPUT_EVENT_DEF_2(IDS::HARD_SLEEP_TOGGLE,			WKEY_S,					XKEY_H,			PS3KEY_H,		AKEY_H,			OSXKEY_H,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_H);

	DIGITAL_INPUT_EVENT_DEF_2(IDS::FORCE_FIELD,			WKEY_DECIMAL,		XKEY_UNKNOWN,		PS3KEY_UNKNOWN,		AKEY_UNKNOWN,			OSXKEY_UNKNOWN,			PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_UNKNOWN);

	TOUCH_INPUT_EVENT_DEF_2(IDS::BOX_SHOT, ABUTTON_2, IBUTTON_1);

	SampleApexApplication::collectInputEvents(inputEvents);
}

void SampleDestructionApplication::collectInputDescriptions(std::vector<const char*>& inputDescriptions)
{
	SampleApexApplication::collectInputDescriptions(inputDescriptions);
	inputDescriptions.insert(inputDescriptions.end(), SampleDestructionInputEventDescriptions, SampleDestructionInputEventDescriptions + PX_ARRAY_SIZE(SampleDestructionInputEventDescriptions));
}

void SampleDestructionApplication::onAnalogInputEvent(const SampleFramework::InputEvent& ie, float val)
{
	SampleApexApplication::onAnalogInputEvent(ie, val);
}

bool SampleDestructionApplication::onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
{
	// if a benchmark is running ignore mouse events.
	// if a benchmark is running ignore the key board.
	if (m_BenchmarkMode)
	{
		return true;
	}

	SampleRenderer::Renderer* renderer = getRenderer();
	if (!renderer || !m_apexScene)
	{
		return SampleApexApplication::onDigitalInputEvent(ie, val);
	}

	if (val)
	{
		static const physx::PxU32 tmpStrLength = 256;
		char tmpStr[tmpStrLength];
		
#define PRINT_POPUP(x, ...) \
		{ physx::string::sprintf_s(tmpStr, tmpStrLength, x, __VA_ARGS__); showHelpPopup(tmpStr); }

		typedef SampleDestructionInputEventIds IDS;

		SampleRenderer::Renderer::TessellationParams tParams = getRenderer()->getTessellationParams();
		switch (ie.m_Id)
		{
		case SCENE_SELECT:
		{
			DestructionSceneManager::Instance().onEventSwitchScene(
			    static_cast<DestructionSceneSelection::Enum>((DestructionSceneManager::Instance().getCurrentScene() + 1) % DestructionSceneSelection::SceneCount));
		}
		break;
		case IDS::BOX_SHOT:
		case CREATE_OBJECT:
		{
#if USE_COMM_LAYER
			const bool testManualChunkDeletion = false;
			if(testManualChunkDeletion)
			{
				m_destructibleSyncManager.onEvent(&m_destructibleSyncActorAlias);
			}
			else
#endif //USE_COMM_LAYER
			{
			SampleRenderer::RendererMesh* boxmesh = m_unitBox ? m_unitBox->getMesh() : 0;
			SampleRenderer::RendererMaterial* material = m_simpleLitMaterial ? m_simpleLitMaterial->getMaterial() : 0;
			if (boxmesh && material)
			{
				const physx::PxMat44& eyeT = getEyeTransform();
				const physx::PxVec3 pos    =  eyeT.getPosition();
				const physx::PxF32 density = 150;
				physx::PxVec3 vel          = -eyeT.column2.getXYZ() * 20.0f;
				vel *= m_boxVelocityScale;
				physx::PxVec3 extents(0.3f, 0.3f, 0.3f);
				if (m_useBunnyProjectile)
				{
					physx::PxVec3 saveVelocity = m_velocity;
					m_velocity = vel * 3;
					DestructionSceneManager::Instance().pushBackActor("Bunny", false, true, pos);
					m_velocity = saveVelocity;
				}
				else
				{
					m_actors.push_back(new SimpleDestructionBoxActor(renderer, *m_simpleLitMaterial,
					                   m_apexScene, m_apexDestructibleModule,
					                   pos, vel, extents, density,
#if NX_SDK_VERSION_MAJOR == 2
					                   NULL,
#elif  NX_SDK_VERSION_MAJOR == 3
					                   m_material,
#endif
					                   true));
				}
			}
			}
		}
		break;
		case IDS::SCENE_0:
		case IDS::SCENE_1:
		case IDS::SCENE_2:
		case IDS::SCENE_3:
		case IDS::SCENE_4:
		case IDS::SCENE_5:
		case IDS::SCENE_6:
		case IDS::SCENE_7:
		case IDS::SCENE_8:
		case IDS::SCENE_9:
		{
			physx::PxU32 sceneIndex = ie.m_Id - IDS::SCENE_0;
			if (sceneIndex < DestructionSceneSelection::SceneCount)
			{
				selectScene(sceneIndex);
			}
		}
		break;
		case IDS::BUNNY_SHOT:
		case IDS::COW_SHOT:
		case IDS::SHEET_SHOT:
		case IDS::WALL_SHOT:
		{
			if (!isInputEventActive(IDS::ACTOR_SERIALIZATION_MODIFIER) && !m_replay->isActive())
			{
				const physx::PxU32 modelIndex = ((ie.m_Id == CAMERA_MOVE_DOWN) ? IDS::COW_SHOT : ie.m_Id) - IDS::FIRST_MODEL_SHOT;
				if (modelIndex <= IDS::NUM_MODEL_SHOTS)
				{
					static const char* modelNames[IDS::NUM_MODEL_SHOTS]   = {"Bunny", "cow", "Sheet", "Wall"};
					static const bool  modelYUp[IDS::NUM_MODEL_SHOTS]     = {  false,  true,   false,  false};
					static const bool  modelDynamic[IDS::NUM_MODEL_SHOTS] = {   true,  true,   false,  false};

					const physx::PxVec3 placeAtPosition = m_apexScene->getEyePosition(0) + m_apexScene->getEyeDirection(0) * 5;
					const physx::PxVec3 saveVelocity = m_velocity;
					m_velocity = -getEyeTransform().column2.getXYZ() * 30.0f * m_boxVelocityScale;
					DestructionSceneManager::Instance().pushBackActor(modelNames[modelIndex], modelYUp[modelIndex], modelDynamic[modelIndex], placeAtPosition);
					m_velocity = saveVelocity;
				}
			}
		}
		break;
		case CAMERA_MOVE_DOWN:
			return SampleApexApplication::onDigitalInputEvent(ie, val);
		case IDS::TOGGLE_REPLAY_SAVE:
			if (m_replay)
				m_replay->toggleSave();
			break;
		case IDS::TOGGLE_REPLAY_DIRECTION:
			if (m_replay)
				m_replay->toggleReplayDirection();
			break;
		case IDS::SAVE_ACTOR_STATE:
			if (isInputEventActive(IDS::ACTOR_SERIALIZATION_MODIFIER) && m_replay)
				m_replay->saveSingle();
			break;
		case IDS::LOAD_SAVED_ACTOR_STATE:
			if (isInputEventActive(IDS::ACTOR_SERIALIZATION_MODIFIER) && m_replay)
				m_replay->loadSingle();
			break;
		case IDS::TOGGLE_TESSELLATION:
			getRenderer()->setEnableTessellation(!m_renderer->getEnableTessellation());
			PRINT_POPUP(" Tessellation %s", m_renderer->getEnableTessellation() ? "enabled" : "disabled");
			break;
		case IDS::INCREASE_TESSELLATION_FACTOR:
			tParams.tessFactor.x += 1.;
			tParams.tessFactor.y += 1.;
			PRINT_POPUP(" Tessellation factor set to: %.2f", tParams.tessFactor.x);
			break;
		case IDS::DECREASE_TESSELLATION_FACTOR:
			if (tParams.tessFactor.x > 2.)
			{
				tParams.tessFactor.x -= 1.;
			}
			if (tParams.tessFactor.y > 2.)
			{
				tParams.tessFactor.y -= 1.;
			}
			PRINT_POPUP(" Tessellation factor set to: %f", tParams.tessFactor.x);
			break;
		case IDS::INCREASE_TESSELLATION_SCALE:
			tParams.tessHeightScaleAndBias[0] += .1;
			PRINT_POPUP(" Tessellation scale set to: %f", tParams.tessHeightScaleAndBias[0]);
			break;
		case IDS::DECREASE_TESSELLATION_SCALE:
			tParams.tessHeightScaleAndBias[0] -= .1;
			PRINT_POPUP(" Tessellation scale set to: %f", tParams.tessHeightScaleAndBias[0]);
			break;
		case IDS::INCREASE_TESSELLATION_UV_SCALE:
			tParams.tessUVScale[0] += .05;
			tParams.tessUVScale[1] += .05;
			PRINT_POPUP(" Tessellation UV scale set to: %f", tParams.tessUVScale[0]);
			break;
		case IDS::DECREASE_TESSELLATION_UV_SCALE:
			tParams.tessUVScale[0] -= .05;
			tParams.tessUVScale[1] -= .05;
			PRINT_POPUP(" Tessellation UV scale set to: %f", tParams.tessUVScale[0]);
			break;
		case IDS::INCREASE_TESSELLATION_LOD_DIST:
			tParams.tessMinMaxDistance[0] *= .8;
			tParams.tessMinMaxDistance[1] *= .8;
			PRINT_POPUP(" Tessellation LOD min/max dist set to: (%f, %f)", tParams.tessMinMaxDistance[0], tParams.tessMinMaxDistance[1]);
			break;
		case IDS::DECREASE_TESSELLATION_LOD_DIST:
			tParams.tessMinMaxDistance[0] *= 1.25;
			tParams.tessMinMaxDistance[1] *= 1.25;
			PRINT_POPUP(" Tessellation LOD min/max dist set to: (%f, %f)", tParams.tessMinMaxDistance[0], tParams.tessMinMaxDistance[1]);
			break;
		case IDS::REMOVE_ACTOR:
		{
			if (!m_actors.empty())
			{
				// there are actor pointers left in some vectors that we must clean up
				if (m_actors.back()->getType() == SampleDestructibleActor::DestructibleActor)
				{
					physx::apex::NxDestructibleActor* dactor = ((SampleDestructibleActor*)m_actors.back())->getActor();
					m_pointDamageList.erase(std::remove_if(m_pointDamageList.begin(), m_pointDamageList.end(), DamagePredicate<PointDamage>(dactor)), m_pointDamageList.end());
					m_radiusDamageList.erase(std::remove_if(m_radiusDamageList.begin(), m_radiusDamageList.end(), DamagePredicate<RadiusDamage>(dactor)), m_radiusDamageList.end());
				}
				m_actors.back()->release();
				m_actors.pop_back();
			}
		}
		break;
		case IDS::HIDE_GRAPHICS:
		{
			m_hideGraphics = !m_hideGraphics;
		}
		break;
		case IDS::HARD_SLEEP_TOGGLE:
		{
			m_hardSleeping = !m_hardSleeping;
			PRINT_POPUP(" Hard sleeping %sabled", m_hardSleeping ? "en" : "dis");
		}
		break;
		case IDS::FORCE_FIELD:
		{
			if (!m_explodeTimer)
			{
				m_explodeTimer = 1;
			}
		}
		break;
		case TOGGLE_WIREFRAME:
			getRenderer()->toggleWireframe();
			break;
		default:
			return SampleApexApplication::onDigitalInputEvent(ie, val);
		};
		getRenderer()->setTessellationParams(tParams);
		return true;
	}
	else
	{
		return SampleApexApplication::onDigitalInputEvent(ie, val);
	}

#undef PRINT_POPUP

}

void SampleDestructionApplication::onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy)
{

	SampleApexApplication::onPointerInputEvent(ie, x, y, dx, dy);

	SampleRenderer::Renderer* renderer = getRenderer();
	if (m_apexScene && m_apexDestructibleModule && renderer)
	{
		if (ie.m_Id == SampleDestructionInputEventIds::RAYCAST_HIT)
		{
			physx::PxU32 width  = 0;
			physx::PxU32 height = 0;
			renderer->getWindowSize(width, height);
			physx::PxMat44 eyeTransform = getEyeTransform();
			physx::PxTransform view(eyeTransform.inverseRT());
			physx::PxVec3 eyePos  = eyeTransform.getPosition();
			physx::PxVec3 nearPos = unproject(m_projection, view, (x / (physx::PxF32)width) * 2 - 1, (y / (physx::PxF32)height) * 2 - 1);
			physx::PxVec3 pickDir = nearPos - eyePos;

#if NX_SDK_VERSION_MAJOR == 2
			if (pickDir.normalize() > 0)
			{
				NxVec3 neyePos, npickDir;
				physx::NxFromPxVec3(neyePos, eyePos);
				physx::NxFromPxVec3(npickDir, pickDir);
				NxRay worldRay(neyePos, npickDir);

				physx::apex::NxDestructibleActor* hitActor = NULL;
				physx::PxF32 hitTime = NX_MAX_F32;
				physx::PxVec3 hitNormal;
				physx::PxI32 hitChunkIndex = physx::apex::NxModuleDestructibleConst::INVALID_CHUNK_INDEX;
				for (size_t i = m_actors.size(); i--;)
				{
					if (m_actors[i]->getType() == SampleDestructibleActor::DestructibleActor)
					{
						physx::apex::NxDestructibleActor* dactor = ((SampleDestructibleActor*)m_actors[i])->getActor();
						physx::PxF32 time;
						physx::PxVec3 normal;
						const physx::PxI32 chunkIndex = dactor->rayCast(time, normal, worldRay, physx::apex::NxDestructibleActorRaycastFlags::AllChunks);
						if (chunkIndex != physx::apex::NxModuleDestructibleConst::INVALID_CHUNK_INDEX && time < hitTime)
						{
							hitTime = time;
							hitNormal = normal;
							hitActor = dactor;
							hitChunkIndex = chunkIndex;
						}
					}
				}
				// Also check PhysX scene
				NxRaycastHit hit;
				NxShape* hitShape = m_apexScene->getPhysXScene()->raycastClosestShape(worldRay, NX_DYNAMIC_SHAPES, hit);
				if (hitShape)
				{
					if (!m_apexDestructibleModule->owns(&hitShape->getActor()) && hit.distance < hitTime)
					{
						hitActor = NULL;
						hitShape->getActor().addForceAtPos(npickDir * 30, hit.worldImpact, NX_IMPULSE, true);
					}
				}
				if (hitActor != NULL)
				{
					hitActor->applyDamage(10, 10, eyePos + hitTime * pickDir, pickDir, hitChunkIndex);
				}
			}
#elif NX_SDK_VERSION_MAJOR == 3
			if (pickDir.normalize() > 0)
			{
				physx::NxDestructibleActor* hitActor = NULL;
				physx::PxF32 hitTime = PX_MAX_F32;
				physx::PxVec3 hitNormal;
				physx::PxI32 hitChunkIndex = physx::NxModuleDestructibleConst::INVALID_CHUNK_INDEX;
				for (size_t i = m_actors.size(); i--;)
				{
					if (m_actors[i]->getType() == SampleDestructibleActor::DestructibleActor)
					{
						physx::NxDestructibleActor* dactor = ((SampleDestructibleActor*)m_actors[i])->getActor();
						physx::PxF32 time;
						physx::PxVec3 normal;
						const physx::PxI32 chunkIndex = dactor->rayCast(time, normal, eyePos, pickDir, physx::NxDestructibleActorRaycastFlags::AllChunks);
						if (chunkIndex != physx::NxModuleDestructibleConst::INVALID_CHUNK_INDEX && time < hitTime)
						{
							hitTime = time;
							hitNormal = normal;
							hitActor = dactor;
							hitChunkIndex = chunkIndex;
						}
					}
				}
				// Also check PhysX scene
				physx::PxRaycastHit	hit;
				bool isHit = m_apexScene->getPhysXScene()->raycastSingle(eyePos, pickDir, PX_MAX_F32, physx::PxSceneQueryFlag::eIMPACT | physx::PxSceneQueryFlag::eNORMAL, hit, physx::PxSceneQueryFilterData(physx::PxSceneQueryFilterFlag::eDYNAMIC));
				if (isHit)
				{
					if (!m_apexDestructibleModule->owns(&hit.shape->getActor()) && hit.distance < hitTime)
					{
						hitActor = NULL;
						physx::PxRigidBodyExt::addForceAtPos(*hit.shape->getActor().isRigidBody(), pickDir * 30, hit.impact, physx::PxForceMode::eIMPULSE, true);
					}
				}
				if (hitActor != NULL)
				{
					hitActor->applyDamage(10, 10, eyePos + hitTime * pickDir, pickDir, hitChunkIndex);
				}
			}
#endif
		}
	}
}

unsigned int SampleDestructionApplication::getNumScenes() const
{
	return DestructionSceneSelection::SceneCount;
}

unsigned int SampleDestructionApplication::getSelectedScene() const
{
	return DestructionSceneManager::Instance().getCurrentScene();
}

const char* SampleDestructionApplication::getSceneName(unsigned int sceneNumber) const
{
	return DestructionSceneManager::Instance().getSceneName(sceneNumber);
}

void SampleDestructionApplication::selectScene(unsigned int sceneNumber)
{
	if (m_replay)
	{
		m_replay->reset();
	}
	DestructionSceneManager::Instance().onEventSwitchScene(static_cast<DestructionSceneSelection::Enum>(sceneNumber));
}

bool SampleDestructionApplication::triggerDamageEnabled() const
{
	return !m_pause &&
	       (NULL == m_replay || (!m_replay->isPlaying() && !m_replay->hasPlayed())) && 
	       (DestructionSceneManager::Instance().getCurrentScene() == DestructionSceneSelection::HallmarkConsole ||
	        DestructionSceneManager::Instance().getCurrentScene() == DestructionSceneSelection::HallmarkPC);
}

void SampleDestructionApplication::triggerDamage()
{
	m_FrameNumber++;

	// if there are more boxBullets to fire fire them at the appropriate time
	while (!m_BoxBulletList.empty())
	{
		BoxBullet bb = m_BoxBulletList.back();
		if (m_FrameNumber > bb.m_fireFrameNumber)
		{
			m_boxBulletCounter++;
			SampleRenderer::RendererMesh* boxmesh = m_unitBox ? m_unitBox->getMesh() : 0;
			SampleRenderer::RendererMaterial* material = m_simpleLitMaterial ? m_simpleLitMaterial->getMaterial() : 0;
			if (boxmesh && material)
			{
				SampleRenderer::Renderer* renderer = getRenderer();
				if (renderer && m_apexScene)
				{
					SampleFramework::SampleActor* actor = new SimpleDestructionBoxActor(
					    renderer,
					    *m_simpleLitMaterial,
					    m_apexScene,
					    m_apexDestructibleModule,
					    bb.m_pos,
					    bb.m_Vel,
					    bb.m_Extents,
					    bb.m_Density,
#if NX_SDK_VERSION_MAJOR == 2
					    NULL,
#elif NX_SDK_VERSION_MAJOR == 3
					    m_material,
#endif
					    true);

					m_actors.push_back(actor);
				}
			}
			m_BoxBulletList.pop_back();
		}
		else
		{
			break;
		}
	}
	while (!m_pointDamageList.empty())
	{
		PointDamage damage = m_pointDamageList.back();
		if (m_FrameNumber > damage.m_fireFrameNumber)
		{
			damage.apply();
			m_pointDamageList.pop_back();
		}
		else
		{
			break;
		}
	}
	while (!m_radiusDamageList.empty())
	{
		RadiusDamage damage = m_radiusDamageList.back();
		if (m_FrameNumber > damage.m_fireFrameNumber)
		{
			damage.apply();
			m_radiusDamageList.pop_back();
		}
		else
		{
			break;
		}
	}
}

void SampleDestructionApplication::createTrimeshTerrain()
{
#if NX_SDK_VERSION_MAJOR == 2
	if (m_terrainMesh != NULL)
	{
		return;
	}

	const physx::PxU32 gridSize = 20;
	const physx::PxF32 gridScale = 40.0f;
	const physx::PxF32 hillHeight = OFFSET_HEIGHT;
	const physx::PxF32 cliffCenterZ = 5.0f;
	const physx::PxF32 cliffWidthZ = 10.0f;
	const physx::PxF32 cliffOffsetZ = 1.0f;

	NxTriangleMeshDesc terrainDesc;
	terrainDesc.numVertices					= (gridSize + 1) * (gridSize + 1);
	terrainDesc.numTriangles				= 2 * gridSize * gridSize;
	physx::PxVec3* vertices = (physx::PxVec3*)m_apexSDK->getPhysXSDK()->getFoundationSDK().getAllocator().malloc(terrainDesc.numVertices * sizeof(physx::PxVec3));
	physx::PxVec3* normals = (physx::PxVec3*)m_apexSDK->getPhysXSDK()->getFoundationSDK().getAllocator().malloc(terrainDesc.numVertices * sizeof(physx::PxVec3));
	physx::PxU16* indices = (physx::PxU16*)m_apexSDK->getPhysXSDK()->getFoundationSDK().getAllocator().malloc(3 * terrainDesc.numTriangles * sizeof(physx::PxU16));
	terrainDesc.pointStrideBytes			= sizeof(physx::PxVec3);
	terrainDesc.triangleStrideBytes			= 3 * sizeof(physx::PxU16);
	terrainDesc.points						= vertices;
	terrainDesc.triangles					= indices;
	terrainDesc.flags						= NX_MF_16_BIT_INDICES;

	const physx::PxF32 corner = -(gridSize * gridScale) / 2;
	physx::PxVec3* vertex = vertices;
	physx::PxVec3* normal = normals;
	physx::PxU16* index = indices;
	for (physx::PxU16 iz = 0; iz <= gridSize; ++iz)
	{
		// vertex and normal
		const physx::PxF32 z = corner + iz * gridScale;
		physx::PxF32 y, dy;
		const physx::PxF32 zc = z + cliffCenterZ + 0.5f * cliffWidthZ;
		if (zc <= 0.0f)
		{
			y = 0.0f;
			dy = 0.0f;
		}
		else if (zc >= cliffWidthZ)
		{
			y = hillHeight;
			dy = 0.0f;
		}
		else
		{
			const physx::PxF32 theta = zc * physx::PxPi / cliffWidthZ;
			y = 0.5f * hillHeight * (1.0f - physx::PxCos(theta));
			dy = 0.5f * hillHeight * physx::PxSin(theta) * physx::PxPi / cliffWidthZ;
		}
		for (physx::PxU16 ix = 0; ix <= gridSize; ++ix)
		{
			const physx::PxF32 x = corner + ix * gridScale;
			*vertex++ = physx::PxVec3(x, y, -z + cliffOffsetZ);
			*normal = physx::PxVec3(0.0f, -dy, -1.0f);
			(normal++)->normalize();
			// triangle
			if (iz < gridSize && ix < gridSize)
			{
				*index++ = ix + (gridSize + 1) * iz;
				*index++ = ix + 1 + (gridSize + 1) * iz;
				*index++ = ix + (gridSize + 1) * (iz + 1);
				*index++ = ix + 1 + (gridSize + 1) * (iz + 1);
				*index++ = ix + (gridSize + 1) * (iz + 1);
				*index++ = ix + 1 + (gridSize + 1) * iz;
			}
		}
	}

	physx::PxFileBuf* writeStream = m_apexSDK->createMemoryWriteStream();
	writeStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxws(*writeStream);

	bool status = m_physxCooking->NxCookTriangleMesh(terrainDesc, nxws);
	if (!status)
	{
		printf("Unable to cook a triangle mesh.");
	}

	physx::PxU32 bufLen = 0;
	const void* buf = m_apexSDK->getMemoryWriteBuffer(*writeStream, bufLen);
	physx::PxFileBuf* readStream = m_apexSDK->createMemoryReadStream(buf, bufLen);
	readStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxrs(*readStream);
	m_terrainMesh = m_apexSDK->getPhysXSDK()->createTriangleMesh(nxrs);

	m_apexSDK->releaseMemoryWriteStream(*writeStream);
	m_apexSDK->releaseMemoryReadStream(*readStream);

	//
	// Please note about the created Triangle Mesh, user needs to release it when no one uses it to save memory. It can be detected
	// by API "meshData->getReferenceCount() == 0". And, the release API is "gPhysicsSDK->releaseTriangleMesh(*meshData);"
	//

	NxTriangleMeshShapeDesc terrainShapeDesc;
	terrainShapeDesc.meshData				= m_terrainMesh;
	terrainShapeDesc.shapeFlags				= NX_SF_VISUALIZATION;
	terrainShapeDesc.groupsMask.bits0 = 1;
	terrainShapeDesc.groupsMask.bits1 = 1;
	terrainShapeDesc.groupsMask.bits2 = 1;
	terrainShapeDesc.groupsMask.bits3 = 1;

	NxActorDesc actorDesc;
	actorDesc.shapes.pushBack(&terrainShapeDesc);
	m_terrainActor = m_apexScene->getPhysXScene()->createActor(actorDesc);

	//Build render model
	m_terrainShape = new SampleRenderer::RendererTerrainShape(*m_renderer,
	        vertices, terrainDesc.numVertices,
	        normals, terrainDesc.numVertices,
	        indices, terrainDesc.numTriangles,
	        0.1f);

	m_apexSDK->getPhysXSDK()->getFoundationSDK().getAllocator().free(indices);
	m_apexSDK->getPhysXSDK()->getFoundationSDK().getAllocator().free(normals);
	m_apexSDK->getPhysXSDK()->getFoundationSDK().getAllocator().free(vertices);

#elif NX_SDK_VERSION_MAJOR == 3
	if (m_terrainMesh != NULL)
	{
		return;
	}

	const physx::PxU32 gridSize = 20;
	const physx::PxF32 gridScale = 40.0f;
	const physx::PxF32 hillHeight = OFFSET_HEIGHT;
	const physx::PxF32 cliffCenterZ = 5.0f;
	const physx::PxF32 cliffWidthZ = 10.0f;
	const physx::PxF32 cliffOffsetZ = 1.0f;

	physx::PxTriangleMeshDesc terrainDesc;
	terrainDesc.points.count				= (gridSize + 1) * (gridSize + 1);
	terrainDesc.triangles.count				= 2 * gridSize * gridSize;

	std::vector<physx::PxVec3> vertices;
	std::vector<physx::PxVec3> normals;
	std::vector<physx::PxU16> indices;
	vertices.resize(terrainDesc.points.count);
	normals.resize(terrainDesc.points.count);
	indices.resize(3 * terrainDesc.triangles.count);

	terrainDesc.points.stride				= sizeof(physx::PxVec3);
	terrainDesc.triangles.stride			= 3 * sizeof(physx::PxU16);
	terrainDesc.points.data					= &(vertices[0]);
	terrainDesc.triangles.data				= &(indices[0]);
	terrainDesc.flags						= physx::PxMeshFlag::e16_BIT_INDICES;

	const physx::PxF32 corner = -(gridSize * gridScale) / 2;
	physx::PxVec3* vertex = &(vertices[0]);
	physx::PxVec3* normal = &(normals[0]);
	physx::PxU16* index = &(indices[0]);
	for (physx::PxU16 iz = 0; iz <= gridSize; ++iz)
	{
		// vertex and normal
		const physx::PxF32 z = corner + iz * gridScale;
		physx::PxF32 y, dy;
		const physx::PxF32 zc = z + cliffCenterZ + 0.5f * cliffWidthZ;
		if (zc <= 0.0f)
		{
			y = 0.0f;
			dy = 0.0f;
		}
		else if (zc >= cliffWidthZ)
		{
			y = hillHeight;
			dy = 0.0f;
		}
		else
		{
			const physx::PxF32 theta = zc * physx::PxPi / cliffWidthZ;
			y = 0.5f * hillHeight * (1.0f - physx::PxCos(theta));
			dy = 0.5f * hillHeight * physx::PxSin(theta) * physx::PxPi / cliffWidthZ;
		}
		for (physx::PxU16 ix = 0; ix <= gridSize; ++ix)
		{
			const physx::PxF32 x = corner + ix * gridScale;
			*vertex++ = physx::PxVec3(x, y, -z + cliffOffsetZ);
			*normal = physx::PxVec3(0.0f, -dy, 1.0f);
			(normal++)->normalize();
			// triangle
			if (iz < gridSize && ix < gridSize)
			{
				*index++ = ix + (gridSize + 1) * iz;
				*index++ = ix + 1 + (gridSize + 1) * iz;
				*index++ = ix + (gridSize + 1) * (iz + 1);
				*index++ = ix + 1 + (gridSize + 1) * (iz + 1);
				*index++ = ix + (gridSize + 1) * (iz + 1);
				*index++ = ix + 1 + (gridSize + 1) * iz;
			}
		}
	}

	physx::PxFileBuf* writeStream = m_apexSDK->createMemoryWriteStream();
	PX_ASSERT(writeStream);
	writeStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxws(*writeStream);

	bool status = m_physxCooking->cookTriangleMesh(terrainDesc, nxws);
	if (!status)
	{
		printf("Unable to cook a triangle mesh.");
	}

	physx::PxU32 bufLen = 0;
	const void* buf = m_apexSDK->getMemoryWriteBuffer(*writeStream, bufLen);
	physx::PxFileBuf* readStream = m_apexSDK->createMemoryReadStream(buf, bufLen);
	readStream->setEndianMode(physx::PxFileBuf::ENDIAN_NONE);
	physx::apex::NxFromPxStream nxrs(*readStream);
	m_terrainMesh = m_apexSDK->getPhysXSDK()->createTriangleMesh(nxrs);

	m_apexSDK->releaseMemoryWriteStream(*writeStream);
	m_apexSDK->releaseMemoryReadStream(*readStream);

	//
	// Please note about the created Triangle Mesh, user needs to release it when no one uses it to save memory. It can be detected
	// by API "meshData->getReferenceCount() == 0". And, the release API is "gPhysicsSDK->releaseTriangleMesh(*meshData);"
	//
	physx::PxRigidStatic* rigidStatic = m_apexSDK->getPhysXSDK()->createRigidStatic(physx::PxTransform(physx::PxVec3(0, 0, 0)));
	PX_ASSERT(rigidStatic && "creating trimesh terrain actor failed");

	physx::PxTriangleMeshGeometry geom(m_terrainMesh);
	physx::PxShape* shape = rigidStatic->createShape(geom, *m_material);
	shape->setSimulationFilterData(physx::PxFilterData(1, 1, 1, 0));	// word3 is reserved for contact report flags.
	PX_ASSERT(shape && "creating trimesh terrain shape failed");
	m_apexScene->getPhysXScene()->addActor(*rigidStatic);

	//Build render model
	m_terrainShape = new SampleRenderer::RendererTerrainShape(*m_renderer,
	        &(vertices[0]),
	        terrainDesc.points.count,
	        &(normals[0]),
	        terrainDesc.points.count,
	        &(indices[0]),
	        terrainDesc.triangles.count,
	        .1f);
#endif

	//Load an interesting material for the plane
	m_groundMaterial = static_cast<SampleFramework::SampleMaterialAsset*>(getAssetManager()->getAsset("ground.xml", SampleFramework::SampleAsset::ASSET_MATERIAL));
	if (m_groundMaterial)
	{
		m_terrainContext.material = m_groundMaterial->getMaterial();
		m_terrainContext.materialInstance = m_groundMaterial->getMaterialInstance();
	}
	else
	{
		m_terrainContext.material = m_simpleLitMaterial->getMaterial();
		m_terrainContext.materialInstance = m_simpleLitMaterial->getMaterialInstance();
	}

	//Create a render mesh context
	m_terrainContext.mesh     = m_terrainShape->getMesh();
	m_terrainContext.cullMode = SampleRenderer::RendererMeshContext::COUNTER_CLOCKWISE;
}

/*** SampleDestructionApplication::DestructionSceneManager ***/
SceneManager& SceneManager::Instance()
{
	static DestructionSceneManager manager;
	return manager;
}

void SceneManager::onInit(SampleDestructionApplication::DestructionSceneSelection::Enum startUpScene, SampleDestructionApplication* app)
{
	PX_ASSERT(startUpScene < DestructionSceneSelection::SceneCount);
	PX_ASSERT(app != NULL);
	PX_ASSERT(mDestructionApp == NULL);
	mCurrentScene = startUpScene >= DestructionSceneSelection::SceneCount ? DestructionSceneSelection::CommandLineScene : startUpScene;
	mDestructionApp = app;
	onCreateScene(startUpScene);
}

void SampleDestructionApplication::setupHallmark(physx::PxMat44& globalPose, physx::PxU32 numActors, WallType::Enum wallEnum)
{
	physx::PxF32		wallElevation;

	SampleRenderer::Renderer* myRenderer = gAppPtr->getRenderer();

	// set the max actors created per frame to amortize the cost
	//m_AppPtr->m_apexDestructibleModule->setMaxActorCreatesPerFrame(16);
	//m_AppPtr->m_apexDestructibleModule->setMaxChunkCount(1500);

	// load the wall asset and create some actors
	const physx::PxF32 wallScale = 1.0f;
	if (gAppPtr->m_WallAsset[wallEnum] == NULL)
	{
		if (wallEnum == WallType::PC)
		{
			gAppPtr->m_WallAsset[wallEnum] = gAppPtr->loadDestructibleAsset("Wall_PC");
		}
		else if (wallEnum == WallType::Console)
		{
			gAppPtr->m_WallAsset[wallEnum] = gAppPtr->loadDestructibleAsset("Wall_Console");
		}
	}
	if (gAppPtr->m_WallAsset[wallEnum])
	{
		//pushBackUniqueAsset code that will work whether we are called from interactive mode or benchmark mode
		{
			bool assetExist = false;
			for (std::vector<physx::apex::NxDestructibleAsset*>::const_iterator iter = gAppPtr->m_destAssets.begin(); iter != gAppPtr->m_destAssets.end(); ++iter)
			{
				if (*iter == gAppPtr->m_WallAsset[wallEnum])
				{
					assetExist = true;
					break;
				}
			}
			if (!assetExist)
			{
				gAppPtr->m_destAssets.push_back(gAppPtr->m_WallAsset[wallEnum]);
			}
		}

		const physx::apex::NxRenderMeshAsset* renderMeshAsset = gAppPtr->m_WallAsset[wallEnum]->getRenderMeshAsset();
		PX_ASSERT(renderMeshAsset);

		if (renderMeshAsset)
		{
			physx::PxBounds3 assetBounds = renderMeshAsset->getBounds();
			wallElevation = -assetBounds.minimum.z * wallScale;
			const physx::PxF32 wallWidth = (assetBounds.maximum.x - assetBounds.minimum.x) * wallScale;

			for (physx::PxU32 actorNumber = 0; actorNumber < numActors; actorNumber++)
			{
				physx::PxMat44 myPose;

				if (actorNumber % 2)	// left wall
				{
					myPose.column0 = physx::PxVec4(0.0f, 0.0f, -1.0f, 0.0f);
					myPose.column1 = physx::PxVec4(-1.0f, 0.0f, 0.0f, 0.0f);
					myPose.column2 = physx::PxVec4(0.0f, 1.0f, 0.0f, 0.0f);
					myPose.column3 = physx::PxVec4(-20.0f, wallElevation, actorNumber / 2 * -wallWidth, 1.0f);
				}
				else					// right wall
				{
					myPose.column0 = physx::PxVec4(0.0f, 0.0f, 1.0f, 0.0f);
					myPose.column1 = physx::PxVec4(1.0f, 0.0f, 0.0f, 0.0f);
					myPose.column2 = physx::PxVec4(0.0f, 1.0f, 0.0f, 0.0f);
					myPose.column3 = physx::PxVec4(20.0f, wallElevation, actorNumber / 2 * -wallWidth, 1.0f);
				}

				myPose = globalPose * myPose;

				NxParameterized::Interface* descParams = NULL;
				descParams = gAppPtr->m_WallAsset[wallEnum]->getDefaultActorDesc();
				PX_ASSERT(descParams != NULL);
				loadDestructibleActorBaseParams(descParams, *gAppPtr->m_WallAsset[wallEnum]);
				NxParameterized::setParamMat44( *descParams, "globalPose", myPose);
				NxParameterized::setParamVec3(  *descParams, "scale", physx::PxVec3(wallScale));
				NxParameterized::setParamF32(   *descParams, "destructibleParameters.forceToDamage", 1.0f);
				NxParameterized::setParamBool(  *descParams, "dynamic", false);
				NxParameterized::setParamString(*descParams, "crumbleEmitterName", NULL);
				NxParameterized::setParamBool(  *descParams, "destructibleParameters.flags.CRUMBLE_SMALLEST_CHUNKS", false);
				NxParameterized::setParamBool(  *descParams, "renderStaticChunksSeparately", gAppPtr->m_RenderStaticChunkSeparately);
				gAppPtr->m_actors.push_back(new SampleDestructibleActor(*myRenderer, *(gAppPtr->m_apexScene), *(gAppPtr->m_WallAsset[wallEnum]), descParams));
			}
		}


		gAppPtr->m_BoxBulletList.clear();

		for (physx::PxU32 actorNumber = 0; actorNumber < numActors; actorNumber++)
		{
			SampleDestructibleActor* myActor = (SampleDestructibleActor*)(gAppPtr->m_actors[actorNumber]);
			physx::PxF32 dmgSpacing = 6.67f;
			physx::PxF32 wallSize   = 20.0f;
			physx::PxF32 x          = myActor->getPosition().x;
			physx::PxF32 startZ     = myActor->getPosition().z + wallSize / 2.0f;
			physx::PxVec3 hitDir    = (actorNumber % 2) ? physx::PxVec3(-1.0f, 0, 0) : physx::PxVec3(1.0f, 0, 0);

			physx::PxU32 averageFramesBetweenBoxes = gAppPtr->m_benchmarkNumFrames / numActors;
			physx::PxU32 boxFrameNum = (numActors - actorNumber - 1) * averageFramesBetweenBoxes;

#define USE_APPLY_DAMAGE_IN_HALLMARK 1
#if USE_APPLY_DAMAGE_IN_HALLMARK
#define USE_APPLY_RADIUS_DAMAGE 1
#if USE_APPLY_RADIUS_DAMAGE

			physx::PxU32 hitCount = 0;
			physx::PxU32 maxHitCount = gAppPtr->m_benchmarkMaxHitCount;

            for (physx::PxF32 y = wallSize; y >= 0 && hitCount!=maxHitCount; y -= dmgSpacing)
            {
                for (physx::PxF32 z = startZ; z >= startZ - wallSize && hitCount!=maxHitCount; z -= dmgSpacing)
                {
					physx::PxF32 momentum = 0.1f;
					if (wallEnum == WallType::Console)
					{
						momentum = 50.0f;
					}

					gAppPtr->m_radiusDamageList.push_back(RadiusDamage(*(myActor->getActor()),
					                                      boxFrameNum,
					                                      10.0f,	// <-- damage must be at least "10"
					                                      momentum,// <-- momentum has no effect (.1 - 5)
					                                      physx::PxVec3(x, y, z),
					                                      10.0f,	// <-- radius
					                                      true));	// <-- falloff
					hitCount++;
				}
			}
#else
			//physx::PxF32 framesBnDamage = (wallSize/dmgSpacing) * ((startZ-wallSize)/dmgSpacing);

			for (physx::PxF32 y = wallSize; y >= 0; y -= dmgSpacing)
			{
				for (physx::PxF32 z = startZ; z >= startZ - wallSize; z -= dmgSpacing)
				{
					gAppPtr->m_pointDamageList.push_back(PointDamage(*(myActor->getActor()),
					                                     boxFrameNum,
					                                     10.0f,// <-- damage must be at least "10"
					                                     0.1f, // <-- momentum has no effect (.1 - 5)
					                                     physx::PxVec3(x, y, z),
					                                     hitDir));
				}
			}
#endif
#else
			// fire some box bullets at the wall(s)
			physx::PxMat44 boxPose = physx::PxMat44::createIdentity();

			boxPose.column3.x = 0.0f;

			for (physx::PxU32 i = 0; i < 3; i++)
			{
				physx::PxF32 tmpStartZ = startZ - wallSize;
				boxPose.column3.z = tmpStartZ;
				boxPose.column3.y = 4.0f * i;

				for (physx::PxU32 j = 0; j < 3; j++)
				{
					boxPose.column3.y += 2.0f;
					boxPose.column3.z = tmpStartZ + j * wallSize * 0.33f;
					physx::PxVec3 boxVel = hitDir.multiply(physx::PxVec3(100.0f, 0.0f, 0.0f));
					gAppPtr->m_BoxBulletList.push_back(BoxBullet(boxFrameNum, boxPose.getPosition(), boxVel));
				}
			}
#endif
		}
	}
	else
	{
		ERRORSTREAM_INVALID_OPERATION("Could not load the Wall asset!");
	}
}

void SampleDestructionApplication::benchmarkStart(const char* benchmarkName)
{
	// common setup for all benchmarks
	SampleRenderer::Renderer* myRenderer = gAppPtr->getRenderer();

	physx::PxMat44 globalPose;
	globalPose = physx::PxMat44::createIdentity();

	gAppPtr->m_FrameNumber   = 1;
	gAppPtr->setBenchmarkEnable(true);
	if (gAppPtr->m_OaInterface != NULL)
	{
		// get the benchmark parameters from openAutomate
		gAppPtr->m_benchmarkNumFrames       = gAppPtr->m_OaInterface->getBenchmarkNumFrames();
		gAppPtr->m_benchmarkNumActors       = gAppPtr->m_OaInterface->getBenchmarkNumActors();
		gAppPtr->m_simulateRigidBodiesOnGPU = gAppPtr->m_OaInterface->getGRBenabled();
		gAppPtr->m_CmdLineBenchmarkMode     = false;
	}
	else
	{
		gAppPtr->m_CmdLineBenchmarkMode = true;
	}

	physx::PxU32 numActors = gAppPtr->m_benchmarkNumActors;
	if (numActors < 1)
	{
		numActors = 1;
	}
	if (numActors > ActorConstants::MAX_ACTORS)
	{
		numActors = ActorConstants::MAX_ACTORS;
	}

	// reset camera position for each benchmark
	physx::PxMat44 eyeTransform = physx::PxMat44::createIdentity();

	if (physx::string::stricmp(cowBmName, benchmarkName) == 0)
	{
		// load the cow asset and create some actors
		const physx::PxF32 cowScale = 0.5f;

		if (gAppPtr->m_CowAsset == NULL)
		{
			gAppPtr->m_Yup      = true;
			gAppPtr->m_CowAsset = gAppPtr->loadDestructibleAsset("cow");
		}

		if (gAppPtr->m_CowAsset)
		{
			const physx::PxF32 rowSpacing      = -20.0f;
			const physx::PxF32 firstRowPos     = 0.0f;
			const physx::PxF32 cowSpacing      = 5.0f;
			const physx::PxU32 numCowsFirstRow = (numActors >= 9) ? 9 : (numActors - 1) % 9 + 1;
			const physx::PxF32 firstColPos     = -((numCowsFirstRow - 1.0f) / 2.0f) * cowSpacing;
			const physx::PxF32 firstBoxColPos  = 16.0f;

			physx::PxU32 firstBoxFrameNum;
			physx::PxU32 lastBoxFrameNum;
			physx::PxMat44 myPose       = globalPose;
			physx::PxMat44 myBoxPose    = myPose;
			physx::PxF32   cowElevation = 0.0f;

			// get the benchmark duration and numActors from openAutomate
			if (gAppPtr->m_OaInterface != NULL)
			{
				gAppPtr->m_benchmarkNumFrames = gAppPtr->m_OaInterface->getBenchmarkNumFrames();
			}
			if (gAppPtr->m_benchmarkNumActors > ActorConstants::MAX_ACTORS)
			{
				gAppPtr->m_benchmarkNumActors = ActorConstants::MAX_ACTORS;
			}
			if (gAppPtr->m_benchmarkNumActors < 1)
			{
				gAppPtr->m_benchmarkNumActors = 1;
			}

			firstBoxFrameNum = (physx::PxU32)(gAppPtr->m_benchmarkNumFrames * 0.2f);
			lastBoxFrameNum  = (physx::PxU32)(gAppPtr->m_benchmarkNumFrames * 0.8f);
			physx::PxMat44 newEyeTransform = gAppPtr->getEyeTransform();
			newEyeTransform.column3.x  = 0.0f;
			newEyeTransform.column3.y += 3.0f;
			newEyeTransform.column3.z  = 16.0f + ((physx::PxF32)numCowsFirstRow * 1.7f);
			gAppPtr->setEyeTransform(newEyeTransform);
			gAppPtr->m_BoxBulletList.clear();

			physx::PxU32 boxFrameNum = lastBoxFrameNum;
			physx::PxU32 averageFramesBetweenBoxes = (lastBoxFrameNum - firstBoxFrameNum) / ((numCowsFirstRow * 2) - 1);
			physx::PxU32 maxFramesBetweenBoxes     = (physx::PxU32)(averageFramesBetweenBoxes * 1.5f);
			physx::PxU32 minFramesBetweenBoxes     = (physx::PxU32)(averageFramesBetweenBoxes * 0.5f);
			myBoxPose.column3.y	= 2.5f;

			//adjust the elevation of the cow so it is not below ground!
			const physx::apex::NxRenderMeshAsset* renderMeshAsset = gAppPtr->m_CowAsset->getRenderMeshAsset();
			PX_ASSERT(renderMeshAsset);
			if (renderMeshAsset)
			{
				physx::PxBounds3 assetBounds = renderMeshAsset->getBounds();
				cowElevation = -assetBounds.minimum.y * cowScale + myPose.column3.y;
			}
			myPose.column3.y = cowElevation;

			physx::PxU32 numCowsInColumn[9];
			memset(numCowsInColumn, 0, sizeof(numCowsInColumn));
			for (physx::PxU32 i = 1; i <= numActors; i++)
			{
				physx::PxU32 columnNumber = (i - 1) % 9;
				myPose.column3.x = firstColPos + (columnNumber * cowSpacing);
				physx::PxU32 rowNumber = (i - 1) / 9;
				myPose.column3.z = firstRowPos + (rowNumber * rowSpacing);

				NxParameterized::Interface* descParams = NULL;
				descParams = gAppPtr->m_CowAsset->getDefaultActorDesc();
				PX_ASSERT(descParams != NULL);
				loadDestructibleActorBaseParams(descParams, *gAppPtr->m_CowAsset);
				NxParameterized::setParamMat44(*descParams, "globalPose", myPose);
				NxParameterized::setParamVec3( *descParams, "scale", physx::PxVec3(cowScale));
				NxParameterized::setParamF32(  *descParams, "destructibleParameters.forceToDamage", 1.0f);
				NxParameterized::setParamBool( *descParams, "dynamic", false);
				gAppPtr->m_actors.push_back(new SampleDestructibleActor(*myRenderer, *(gAppPtr->m_apexScene), *(gAppPtr->m_CowAsset), descParams));
				numCowsInColumn[columnNumber]++;
			}

			myBoxPose.column3.y = cowElevation;
			for (physx::PxU32 columnNumber = 0; columnNumber < numCowsFirstRow; columnNumber++)
			{
				myBoxPose.column3.x = firstColPos + (columnNumber * cowSpacing);

				for (physx::PxU32 rowNumber = 0; rowNumber < numCowsInColumn[columnNumber]; rowNumber++)
				{
					myBoxPose.column3.z = firstBoxColPos + (rowNumber * rowSpacing);
					gAppPtr->m_BoxBulletList.push_back(BoxBullet(boxFrameNum, myBoxPose.getPosition()));
				}
				boxFrameNum -= minFramesBetweenBoxes;
				for (physx::PxU32 rowNumber = 0; rowNumber < numCowsInColumn[columnNumber]; rowNumber++)
				{
					myBoxPose.column3.z = firstBoxColPos + (rowNumber * rowSpacing);
					gAppPtr->m_BoxBulletList.push_back(BoxBullet(boxFrameNum, myBoxPose.getPosition()));
				}
				boxFrameNum -= maxFramesBetweenBoxes;
			}
		}
		else
		{
			ERRORSTREAM_INVALID_OPERATION("Could not load cow.apb!");
		}
	}
	else if (physx::string::stricmp(wallBmName, benchmarkName) == 0)
	{
		physx::PxF32       wallElevation;
		const physx::PxF32 distanceFromWall          = 16.0f;
		physx::PxMat44     myPose                    = globalPose;
		physx::PxMat44     myBoxPose                 = myPose;
		const physx::PxU32 firstBoxFrameNum          = (physx::PxU32)(gAppPtr->m_benchmarkNumFrames * 0.2f);
		const physx::PxU32 lastBoxFrameNum           = (physx::PxU32)(gAppPtr->m_benchmarkNumFrames * 0.8f);
		const physx::PxU32 averageFramesBetweenBoxes = (lastBoxFrameNum - firstBoxFrameNum) / (16 * 2);
		const physx::PxF32 boxSpacing                = 6.0f;

		eyeTransform.column3.x  = 0.0f;
		eyeTransform.column3.y += 6.0f;
		eyeTransform.column3.z  = 30.0f;
		if (numActors > 1)
		{
			eyeTransform.column3.x = -25.0f;
			physx::PxF32 negativeThirtyDegrees = -30.0f * (physx::PxPi / 180.0f);
			eyeTransform.column0.x = physx::PxCos(negativeThirtyDegrees);
			eyeTransform.column0.z = -physx::PxSin(negativeThirtyDegrees);
			eyeTransform.column2.x = -physx::PxSin(negativeThirtyDegrees);
			eyeTransform.column2.z = physx::PxCos(negativeThirtyDegrees);
		}
		gAppPtr->setEyeTransform(eyeTransform);

		// load the wall asset and create some actors
		const physx::PxF32 wallScale = 1.0f;
		WallType::Enum wallEnum = WallType::PC;
		if (gAppPtr->m_WallAsset[wallEnum] == NULL)
		{
			gAppPtr->m_WallAsset[wallEnum] = gAppPtr->loadDestructibleAsset("Wall_PC");
		}
		if (gAppPtr->m_WallAsset[wallEnum])
		{
			gAppPtr->pushBackUniqueAsset(gAppPtr->m_WallAsset[wallEnum]);

			physx::PxMat44 myPose = globalPose;
			const physx::apex::NxRenderMeshAsset* renderMeshAsset = gAppPtr->m_WallAsset[wallEnum]->getRenderMeshAsset();
			PX_ASSERT(renderMeshAsset);
			if (renderMeshAsset)
			{
				physx::PxBounds3 assetBounds = renderMeshAsset->getBounds();
				wallElevation    = -assetBounds.minimum.z * wallScale + myPose.column3.z;
				myPose.column1   = physx::PxVec4(0.0f, 0.0f, -1.0f, 0.0f);
				myPose.column2   = physx::PxVec4(0.0f, 1.0f, 0.0f, 0.0f);
				myPose.column3.y = wallElevation;
			}
			physx::PxF32 const wallSeperation = 20;
			for (physx::PxU32 actorNumber = 0; actorNumber < numActors; actorNumber++)
			{
				NxParameterized::Interface* descParams = NULL;
				descParams = gAppPtr->m_WallAsset[wallEnum]->getDefaultActorDesc();
				PX_ASSERT(descParams != NULL);
				loadDestructibleActorBaseParams(descParams, *gAppPtr->m_WallAsset[wallEnum]);
				NxParameterized::setParamMat44(*descParams, "globalPose", myPose);
				NxParameterized::setParamVec3(*descParams, "scale", physx::PxVec3(wallScale));
				NxParameterized::setParamF32(*descParams, "destructibleParameters.forceToDamage", 1.0f);
				NxParameterized::setParamBool(*descParams, "dynamic", false);
				gAppPtr->m_actors.push_back(new SampleDestructibleActor(*myRenderer, *(gAppPtr->m_apexScene), *(gAppPtr->m_WallAsset[wallEnum]), descParams));
				myPose.column3.z -= wallSeperation;
			}

			// fire some box bullets at the wall(s)
			gAppPtr->m_BoxBulletList.clear();
			physx::PxU32 boxFrameNum = lastBoxFrameNum;
			myBoxPose.column3.y	= 4.0f;
			for (physx::PxU32 i = 0; i < 4; i++)
			{
				myBoxPose.column3.x = 10.0f;
				for (physx::PxU32 j = 0; j < 4; j++)
				{
					for (physx::PxU32 b = 0; b < 2; b++)
					{
						for (physx::PxU32 actorNumber = 0; actorNumber < numActors; actorNumber++)
						{
							myBoxPose.column3.z = distanceFromWall - (actorNumber * wallSeperation);
							gAppPtr->m_BoxBulletList.push_back(BoxBullet(boxFrameNum, myBoxPose.getPosition()));
						}
						boxFrameNum -= averageFramesBetweenBoxes;
					}
					myBoxPose.column3.x -= boxSpacing;
				}
				myBoxPose.column3.y += boxSpacing;
			}
		}
		else
		{
			ERRORSTREAM_INVALID_OPERATION("Could not load the Wall asset!");
		}
	}
	else if (physx::string::stricmp(hallBmName, benchmarkName) == 0)
	{
		physx::PxMat44		myPose = globalPose;

		if (gAppPtr->m_OaInterface != NULL)
		{
			gAppPtr->m_enableGpuPhysX = gAppPtr->m_OaInterface->getGRBenabled();
		}

#if APEX_USE_GRB
		gAppPtr->m_apexDestructibleModule->setGrbSimulationEnabled(*gAppPtr->m_apexScene, gAppPtr->m_enableGpuPhysX);
#endif

		gAppPtr->m_apexDestructibleModule->setLODBenefitValue(15.0f);
#if !defined(PX_WINDOWS)
		gAppPtr->m_apexDestructibleModule->setMaxChunkDepthOffset(1);
#endif

		gAppPtr->m_apexDestructibleModule->setLODEnabled(false);
		eyeTransform.column3.x = 0.0f;
		eyeTransform.column3.y = 6.0f;
		eyeTransform.column3.z = 38.0f;
		gAppPtr->setEyeTransform(eyeTransform);

		setupHallmark(globalPose, numActors, WallType::PC);
	}
	else  // Unknown Benchmark
	{
		ERRORSTREAM_INVALID_OPERATION("Unknown Benchmark");;
	}

	std::sort(gAppPtr->m_BoxBulletList.begin(), gAppPtr->m_BoxBulletList.end(), FrameDistributionObject::reverseSortPredicate);
	std::sort(gAppPtr->m_pointDamageList.begin(), gAppPtr->m_pointDamageList.end(), FrameDistributionObject::reverseSortPredicate);
	std::sort(gAppPtr->m_radiusDamageList.begin(), gAppPtr->m_radiusDamageList.end(), FrameDistributionObject::reverseSortPredicate);

	//common setup for all benchmarks
	gAppPtr->m_BenchMarkTimer.getElapsedSeconds();
	gAppPtr->m_BenchmarkElapsedTime    = 0.0;
	gAppPtr->m_boxBulletCounter        = 0;
	gAppPtr->m_dataCollectionStarted   = true;
	gAppPtr->m_dataCollectionStartTime = gAppPtr->m_BenchmarkElapsedTime;
	oaStartBenchmark();
}

bool SampleDestructionApplication::isGrbEnabled() const
{
#if APEX_USE_GRB
	// Reset if we're using GRB
	return m_apexDestructibleModule->isGrbSimulationEnabled(*m_apexScene);
#else
	return false;
#endif
}

void SceneManager::onDestroy()
{
	PX_ASSERT(mDestructionApp->m_actors.empty() && "actors allocated by manager to be released by app!");
	PX_ASSERT(mDestructionApp->m_destAssets.empty() && "assets allocated by manager to be released by app!");
	mCurrentScene = DestructionSceneSelection::SceneCount;
	mDestructionApp = NULL;
}

void SceneManager::onEventSwitchScene(const SampleDestructionApplication::DestructionSceneSelection::Enum newScene, bool loadActors)
{
	PX_ASSERT(newScene < DestructionSceneSelection::SceneCount);
	//if newScene == mCurrentScene, scene is reset
	onDestroyScene();
	mCurrentScene = newScene;
	onCreateScene(newScene, loadActors);
}

void SceneManager::pushBackUniqueAsset(physx::apex::NxDestructibleAsset* candidateAsset) const
{
	bool assetExist = false;
	for (std::vector<physx::apex::NxDestructibleAsset*>::const_iterator iter = mDestructionApp->m_destAssets.begin(); iter != mDestructionApp->m_destAssets.end(); ++iter)
	{
		if (*iter == candidateAsset)
		{
			assetExist = true;
			break;
		}
	}
	if (!assetExist)
	{
		mDestructionApp->m_destAssets.push_back(candidateAsset);
	}
}

void SceneManager::pushBackActor(const char* assetFilename, bool isYup, bool isDynamic, const physx::PxVec3& position, const physx::PxVec3& scale) const
{
	mDestructionApp->m_Yup      = isYup;
	mDestructionApp->m_dynamic  = isDynamic;
	mDestructionApp->m_position = position;
	mDestructionApp->loadAnAsset(mDestructionApp->getRenderer(), assetFilename, scale);
}

void SceneManager::pushBackActorWithState(const char* assetFilename, const char* actorStateFileName, bool isYup, bool isDynamic, const physx::PxVec3& position, const physx::PxVec3& scale) const
{
	mDestructionApp->m_Yup      = isYup;
	mDestructionApp->m_dynamic  = isDynamic;
	mDestructionApp->m_position = position;
	mDestructionApp->loadAnAssetWithState(mDestructionApp->getRenderer(), assetFilename, actorStateFileName, scale);
}

void SampleDestructionApplication::DestructionSceneManager::setCustomSceneAssetName(const char* name)
{
	mCustomSceneAssetName = name;
}

const char* SceneManager::getSceneName(int sceneNumber) const
{
	if (sceneNumber == -1)
	{
		sceneNumber = mCurrentScene;
	}

	switch (sceneNumber)
	{
	case DestructionSceneSelection::HallmarkConsole :
		return "Collapsing hallway - Console";
	case DestructionSceneSelection::HallmarkPC :
		return "Collapsing hallway - PC";
	case DestructionSceneSelection::WallConsole :
		return "Wall - Console";
	case DestructionSceneSelection::WallPC :
		return "Wall - PC";
	case DestructionSceneSelection::EasterIslandHeadsConsole :
		return "Easter Island Heads - Console";
	case DestructionSceneSelection::EasterIslandHeadsPC :
		return "Easter Island Heads - PC";
	case DestructionSceneSelection::EasterIslandHeadsVoronoi :
		return "Easter Island Heads - Voronoi";
	case DestructionSceneSelection::BlockTessellation :
		if (mDestructionApp->m_renderer->isTessellationSupported())
			return "Block - Tessellation";
	case DestructionSceneSelection::CommandLineScene :
#if USE_COMM_LAYER
		if (CommTool::ConnectionType::COUNT != mDestructionApp->m_destructibleSyncManager.getConnectionType() && (DestructionSceneSelection::CommandLineScene == mCurrentScene))
			return "Destructible Walls in sync - PC";
#endif //USE_COMM_LAYER
		return "Custom scene";
	default:
		PX_ASSERT(!"invalid scene!");
		return "Invalid scene";
	}
}


const char* SampleDestructionApplication::DestructionSceneManager::getSceneAssetName(DestructionSceneSelection::Enum e, const char* customSceneAssetName)
{
	switch (e)
	{
	case DestructionSceneSelection::CommandLineScene :
		return customSceneAssetName;
	case DestructionSceneSelection::HallmarkConsole :
		return "Wall_console";
	case DestructionSceneSelection::HallmarkPC:
		return "Wall_PC";
	case DestructionSceneSelection::WallConsole :
		return "testWall";
	case DestructionSceneSelection::WallPC :
		return "testWall2";
	case DestructionSceneSelection::EasterIslandHeadsConsole :
		return "Moai_Console";
	case DestructionSceneSelection::EasterIslandHeadsPC :
		return "testWall";
	case DestructionSceneSelection::EasterIslandHeadsVoronoi :
		return "Moai_Voronoi";
	case DestructionSceneSelection::BlockTessellation :
		return "Block_Tessellation";
	default:
		PX_ASSERT(0 && "Invalid scene type.");
		return NULL;
	}
}

const char* SampleDestructionApplication::DestructionSceneManager::getSceneAssetName() const
{
#if USE_COMM_LAYER
	if (DestructionSceneSelection::CommandLineScene == mCurrentScene && CommTool::ConnectionType::COUNT != mDestructionApp->m_destructibleSyncManager.getConnectionType())
		return getSceneAssetName(mCurrentScene, "Wall_PC");
#endif //USE_COMM_LAYER
	return getSceneAssetName(mCurrentScene, mCustomSceneAssetName);
}

physx::PxU32 SampleDestructionApplication::DestructionSceneManager::getSceneTargetActorCount() const
{
	switch (mCurrentScene)
	{
	case DestructionSceneSelection::HallmarkConsole :
	case DestructionSceneSelection::HallmarkPC:
	case DestructionSceneSelection::EasterIslandHeadsPC :
	case DestructionSceneSelection::EasterIslandHeadsVoronoi :
	case DestructionSceneSelection::BlockTessellation :
		return 3;
	case DestructionSceneSelection::CommandLineScene :
	case DestructionSceneSelection::WallConsole :
	case DestructionSceneSelection::WallPC :
	case DestructionSceneSelection::EasterIslandHeadsConsole :
	default:
		return 1;
	}
}

SampleDestructionApplication::DestructionSceneSelection::Enum SceneManager::getCurrentScene() const
{
	return mCurrentScene;
}

SceneManager::DestructionSceneManager()
	: mCurrentScene(DestructionSceneSelection::SceneCount)
	, mDestructionApp(NULL)
	, mCustomSceneAssetName(NULL)
	, mSceneDestructibleActorCount(0)
{
}

SceneManager::~DestructionSceneManager()
{
	PX_ASSERT(DestructionSceneSelection::SceneCount == mCurrentScene);
	PX_ASSERT(NULL == mDestructionApp);
}

void SceneManager::onCreateScene(const SampleDestructionApplication::DestructionSceneSelection::Enum e, bool loadActors)
{
	PX_ASSERT(mDestructionApp != NULL);
	static DestructionSceneStateManager stateManager(mDestructionApp);
	stateManager.tidySceneFeature();

	gAppPtr->setEyeTransform(physx::PxVec3(0.0f, 2.0f, 16.0f), physx::PxVec3(0.0f));

#if APEX_USE_GRB
	bool canUseGRBs = true;
	mSceneDestructibleActorCount = 0;

	switch (e)
	{
	case DestructionSceneSelection::CommandLineScene :
	case DestructionSceneSelection::HallmarkPC :
	case DestructionSceneSelection::WallPC :
	case DestructionSceneSelection::EasterIslandHeadsPC :
	case DestructionSceneSelection::EasterIslandHeadsVoronoi :
	case DestructionSceneSelection::BlockTessellation :
		canUseGRBs = true;
		break;
	case DestructionSceneSelection::HallmarkConsole :
	case DestructionSceneSelection::WallConsole :
	case DestructionSceneSelection::EasterIslandHeadsConsole :
		canUseGRBs = false;
		break;
	default:
		PX_ASSERT(!"invalid scene!");
	}

	mDestructionApp->m_apexDestructibleModule->setGrbSimulationEnabled(*mDestructionApp->m_apexScene, canUseGRBs && mDestructionApp->m_enableGpuPhysX);
#endif /* APEX_USE_GRB */

	if (loadActors)
	{
		switch (e)
		{
		case DestructionSceneSelection::HallmarkConsole :
			{
				physx::PxMat44 globalPose      = physx::PxMat44::createIdentity();
				globalPose.column3.z           = -30.0f;
				physx::PxU32 numActors         = 3;
				mDestructionApp->m_FrameNumber = 0;
				setupHallmark(globalPose, numActors, WallType::Console);
			}
			break;
		case DestructionSceneSelection::HallmarkPC :
			{
				physx::PxMat44 globalPose      = physx::PxMat44::createIdentity();
				globalPose.column3.z           = -30.0f;
				physx::PxU32 numActors         = 3;
				mDestructionApp->m_FrameNumber = 0;
				setupHallmark(globalPose, numActors, WallType::PC);
			}
			break;
		case DestructionSceneSelection::WallConsole :
			pushBackActor(getSceneAssetName(e), false, false, physx::PxVec3(0.0f), physx::PxVec3(0.05f));
			break;
		case DestructionSceneSelection::WallPC :
			pushBackActor(getSceneAssetName(e), false, false);
			break;
		case DestructionSceneSelection::EasterIslandHeadsConsole :
			for (physx::PxI32 i = 0; i < (physx::PxI32)getSceneTargetActorCount(); ++i)
			{
				pushBackActor(getSceneAssetName(e), false, false, physx::PxVec3(i * 7.0f, 0.0f, 0.0f), physx::PxVec3(0.06f));
			}
			break;
		case DestructionSceneSelection::EasterIslandHeadsPC :
			for (physx::PxI32 i = -1; i < -1 + (physx::PxI32)getSceneTargetActorCount(); ++i)
			{
				pushBackActor(getSceneAssetName(e), false, false, physx::PxVec3(i * 7.0f, 0.0f, 0.0f), physx::PxVec3(0.06f));
			}
			break;
		case DestructionSceneSelection::EasterIslandHeadsVoronoi :
			for (physx::PxI32 i = -1; i < -1 + (physx::PxI32)getSceneTargetActorCount(); ++i)
			{
				pushBackActor(getSceneAssetName(e), false, false, physx::PxVec3(i * 7.0f, 0.0f, 0.0f), physx::PxVec3(0.06f));
			}
			break;
		case DestructionSceneSelection::BlockTessellation :
			// If tessellation is not supported, skip to the command line scene
			if (mDestructionApp->m_renderer->isTessellationSupported())
			{
				std::string tessellationHelpPopup(mDestructionApp->inputInfoMsg(SampleDestructionInputEventIds::TOGGLE_TESSELLATION));
				tessellationHelpPopup.append("\n");
				tessellationHelpPopup.append(mDestructionApp->inputInfoMsg(TOGGLE_WIREFRAME));
				mDestructionApp->showHelpPopup(tessellationHelpPopup);
				for (physx::PxI32 i = -1; i < -1 + (physx::PxI32)getSceneTargetActorCount(); ++i)
				{
					pushBackActor(getSceneAssetName(e), false, true, physx::PxVec3(i * 7.0f, 0.0f, 0.0f));
				}
				break;
			}
		case DestructionSceneSelection::CommandLineScene :
#if USE_COMM_LAYER
			if (CommTool::ConnectionType::COUNT != mDestructionApp->m_destructibleSyncManager.getConnectionType() && (DestructionSceneSelection::CommandLineScene == mCurrentScene))
			{
				std::vector<physx::apex::NxDestructibleActor*> & container = mDestructionApp->m_destructibleSyncActorAlias;
				for (physx::PxI32 i = -1; i <= 1; ++i)
				{
					pushBackActor(getSceneAssetName(), false, false, physx::PxVec3(i * 11.0f, 0.0f, -7.0f));
					SampleDestructibleActor * currentSampleActor = static_cast<SampleDestructibleActor*>(mDestructionApp->m_actors.back());
					PX_ASSERT(NULL != currentSampleActor);
					physx::apex::NxDestructibleActor * currentApexActor = static_cast<physx::apex::NxDestructibleActor*>(currentSampleActor->getActor());
					PX_ASSERT(NULL != currentApexActor);
					container.push_back(currentApexActor);
				}
				PX_ASSERT(3 == mDestructionApp->m_destructibleSyncActorAlias.size());
				physx::PxU32 syncActorID = 0;
				mDestructionApp->m_destructibleSyncManager.setUpActorSyncParams(*(container[0]), ++syncActorID, SyncActorDetail::Low);
				mDestructionApp->m_destructibleSyncManager.setUpActorSyncParams(*(container[1]), ++syncActorID, SyncActorDetail::Mid);
				mDestructionApp->m_destructibleSyncManager.setUpActorSyncParams(*(container[2]), ++syncActorID, SyncActorDetail::High);
			}
			break;
#endif //USE_COMM_LAYER
			break;
		default:
			PX_ASSERT(!"invalid scene!");
		}
	}
	mSceneDestructibleActorCount = (physx::PxU32)mDestructionApp->m_actors.size();
}

void SceneManager::onDestroyScene() const
{
    PX_ASSERT(mDestructionApp != NULL);
#if USE_COMM_LAYER
	while(!mDestructionApp->m_destructibleSyncActorAlias.empty())
	{
		mDestructionApp->m_destructibleSyncActorAlias.pop_back();
	}
#endif //USE_COMM_LAYER
	while (!mDestructionApp->m_actors.empty())
	{
		mDestructionApp->m_actors.back()->release();
		mDestructionApp->m_actors.pop_back();
	}
	while (!mDestructionApp->m_radiusDamageList.empty())
	{
		mDestructionApp->m_radiusDamageList.pop_back();
	}
}

/*** SampleDestructionApplication::DestructionSceneManager::DestructionSceneStateManager ***/
SceneState::DestructionSceneStateManager(SampleDestructionApplication* a)
	: app(a)
	, checkFlag(0)
{
	PX_ASSERT(app != NULL);
#if NX_SDK_VERSION_MAJOR == 2
	app->m_apexScene->getPhysXScene()->getGravity(nxGravity);
#elif NX_SDK_VERSION_MAJOR == 3
	nxGravity	= app->m_apexScene->getPhysXScene()->getGravity();
#endif //NX_SDK_VERSION_MAJOR
	boxVelocityScale = app->m_boxVelocityScale;
	useBunnyProjectile = app->m_useBunnyProjectile;
}

SceneState::~DestructionSceneStateManager()
{
	app = NULL;
}

void SceneState::tidySceneFeature() const
{
	if (DestructionSceneFeature::FeatureGravityHigher & checkFlag)
	{
		app->m_apexScene->getPhysXScene()->setGravity(this->nxGravity);
	}
	if (DestructionSceneFeature::FeatureProjectileSpeedHigher & checkFlag)
	{
		app->m_boxVelocityScale = this->boxVelocityScale;
	}
	if (DestructionSceneFeature::FeatureBunnyProjectile & checkFlag)
	{
		app->m_useBunnyProjectile = this->useBunnyProjectile;
	}
	checkFlag = 0;
}

void SceneState::notifyFeatureChange(SceneManager::DestructionSceneFeature::Enum e) const
{
	checkFlag |= e;
}
