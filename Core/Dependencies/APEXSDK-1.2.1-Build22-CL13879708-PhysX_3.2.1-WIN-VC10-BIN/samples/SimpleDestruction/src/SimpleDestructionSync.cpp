// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================

#include "SimpleDestructionSync.h"

#if USE_COMM_LAYER

namespace
{
	physx::PxU32 elapsedTickCountSinceLastChunkTransformsSync = 0;
	void syncOperationOK(bool b)
	{
		PX_ASSERT(b);
		PX_FORCE_PARAMETER_REFERENCE(b);
	}
}; //namespace nameless

DestructibleSyncManager::DestructibleSyncManager()
	:connectionType(CommTool::ConnectionType::COUNT)
	,communicationInterface(NULL)
	,damageEventManager(NULL)
	,fractureEventManager(NULL)
	,chunkMotionManager(NULL)
	,hitChunkManager(NULL)
	,chunkSyncState(NULL)
	,chunkSyncStateCount(0)
{
}

DestructibleSyncManager::~DestructibleSyncManager()
{
	PX_ASSERT(NULL == chunkSyncState);
	PX_ASSERT(NULL == hitChunkManager);
	PX_ASSERT(NULL == chunkMotionManager);
	PX_ASSERT(NULL == fractureEventManager);
	PX_ASSERT(NULL == damageEventManager);
	PX_ASSERT(NULL == communicationInterface);
}

void DestructibleSyncManager::onCommandLineInit(CommTool::ConnectionType::Enum connectionType_)
{
	PX_ASSERT(CommTool::ConnectionType::COUNT == connectionType && CommTool::ConnectionType::COUNT != connectionType_);
	connectionType = connectionType_;
}

bool DestructibleSyncManager::onCreate()
{
	bool validOperation = false; 
	PX_ASSERT(NULL == communicationInterface && NULL == damageEventManager && NULL == fractureEventManager && NULL == chunkMotionManager && NULL == hitChunkManager);
	if (NULL == communicationInterface && NULL == damageEventManager && NULL == fractureEventManager && NULL == chunkMotionManager && NULL == hitChunkManager)
	{
		PX_ASSERT(CommTool::ConnectionType::CREATE_AS_SERVER == connectionType || CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType);
		if (CommTool::ConnectionType::CREATE_AS_SERVER == connectionType || CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType)
		{
			// set up connection properties
			const physx::PxU32 connectionCount = 4;
			CommTool::ConnectionProperty connectionProperties[connectionCount];
			for(physx::PxU32 index = 0; index < connectionCount; ++index)
			{
				connectionProperties[index].connectionType	= connectionType;
				connectionProperties[index].socketAddress	= "localhost";
				connectionProperties[index].portNumber		= 23 + index;
				connectionProperties[index].bufferSize		= 1024 * 1024 * ((2 == index) ? 16 : 1);
			}

			// initiate connection
			communicationInterface = CommTool::createCommInstance(connectionProperties, connectionCount);
			PX_ASSERT(NULL != communicationInterface);

			// set up sync variables
			if(NULL != communicationInterface)
			{
				// set up sync managers
				damageEventManager =	::new DamageEventManager	(connectionType, 0, communicationInterface);
				fractureEventManager =	::new FractureEventManager	(connectionType, 1, communicationInterface);
				chunkMotionManager =	::new ChunkMotionManager	(connectionType, 2, communicationInterface);
				hitChunkManager =		::new HitChunkManager		(connectionType, 3, communicationInterface);
				PX_ASSERT(NULL != damageEventManager && NULL != fractureEventManager && NULL != chunkMotionManager && NULL != hitChunkManager);

				// set up chunk sync properties
				chunkSyncStateCount = 2;
				chunkSyncState = new physx::apex::NxDestructibleChunkSyncState[chunkSyncStateCount];
				for(physx::PxU32 index = 0; index < chunkSyncStateCount; ++index)
				{
					chunkSyncState[index].disableTransformBuffering	= false;
					chunkSyncState[index].excludeSleepingChunks		= false;
					chunkSyncState[index].chunkTransformCopyDepth	= 0;
				}
				validOperation = true;
			}
			else
			{
				connectionType = CommTool::ConnectionType::COUNT;
			}
		}
	}
	return validOperation;
}

void DestructibleSyncManager::onDestroy()
{
	if(NULL != chunkSyncState)
	{
		::delete []chunkSyncState;
		chunkSyncState = NULL;
	}
	if(NULL != hitChunkManager)
	{
		::delete hitChunkManager;
		hitChunkManager = NULL;
	}
	if(NULL != chunkMotionManager)
	{
		::delete chunkMotionManager;
		chunkMotionManager = NULL;
	}
	if(NULL != fractureEventManager)
	{
		::delete fractureEventManager;
		fractureEventManager = NULL;
	}
	if(NULL != damageEventManager)
	{
		::delete damageEventManager;
		damageEventManager = NULL;
	}
	if(NULL != communicationInterface)
	{
		communicationInterface->release();
		communicationInterface = NULL;
	}
}

void DestructibleSyncManager::onTick()
{
	if(NULL != communicationInterface)
	{
		if(CommTool::ConnectionType::CREATE_AS_SERVER == getConnectionType())
		{
			// for chunk transforms
			getEditableChunkSyncState(0)->disableTransformBuffering = true;
			getEditableChunkSyncState(1)->disableTransformBuffering = true;
			const physx::PxU32 tickIntervalsForSyncingChunkTransforms = 5;
			if(++elapsedTickCountSinceLastChunkTransformsSync == tickIntervalsForSyncingChunkTransforms)
			{
				elapsedTickCountSinceLastChunkTransformsSync = 0;
				getEditableChunkSyncState(0)->disableTransformBuffering = false;
				getEditableChunkSyncState(1)->disableTransformBuffering = false;
			}
		}
		else if(CommTool::ConnectionType::CREATE_AS_CLIENT == getConnectionType())
		{
			// for hit chunks
			hitChunkManager->processListen();
		}
	}
}

void DestructibleSyncManager::onEvent(std::vector<physx::apex::NxDestructibleActor*> * destructibleSyncActorAlias)
{
	if(NULL != communicationInterface)
	{
		if(NULL != destructibleSyncActorAlias)
		{
			if(CommTool::ConnectionType::CREATE_AS_SERVER == getConnectionType())
			{
				hitChunkManager->processSend(destructibleSyncActorAlias);
			}
			else if(CommTool::ConnectionType::CREATE_AS_CLIENT == getConnectionType())
			{
				hitChunkManager->processReceived(destructibleSyncActorAlias);
			}
		}
	}
}

void DestructibleSyncManager::setUpModuleSyncParams(physx::apex::NxModuleDestructible & module) const
{
	PX_ASSERT(NULL != &module);
	syncOperationOK(module.setSyncParams(damageEventManager, fractureEventManager, chunkMotionManager));
}

void DestructibleSyncManager::setUpActorSyncParams(physx::apex::NxDestructibleActor & syncActor, physx::PxU32 syncActorID, SyncActorDetail::Enum syncActorDetail) const
{
	PX_ASSERT(NULL != &syncActor);
	PX_ASSERT((SyncActorDetail::None != syncActorDetail) ? (0 != syncActorID) : true);
	switch(syncActorDetail)
	{
	case SyncActorDetail::None :
		{
			const physx::PxU32 unregisterID = 0;
			syncActor.setSyncParams(unregisterID);
		}
		break;
	case SyncActorDetail::Low :
		{
			// set up actor sync state
			StateManager<physx::PxU32, physx::apex::NxDestructibleActorSyncFlags::Enum> syncActorState;
			if(CommTool::ConnectionType::CREATE_AS_SERVER == getConnectionType())
			{
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::CopyFractureEvents);
			}
			else if(CommTool::ConnectionType::CREATE_AS_CLIENT == getConnectionType())
			{
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::ReadFractureEvents);
			}
			else
			{
				PX_ASSERT(!"invalid connection type!");
			}

			// set up actor sync params
			syncOperationOK(syncActor.setSyncParams(syncActorID, syncActorState.get(), NULL));
		}
		break;
	case SyncActorDetail::Mid :
		{
			// set up actor sync state
			StateManager<physx::PxU32, physx::apex::NxDestructibleActorSyncFlags::Enum> syncActorState;
			if(CommTool::ConnectionType::CREATE_AS_SERVER == getConnectionType())
			{
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::CopyFractureEvents);
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::CopyChunkTransform);
			}
			else if(CommTool::ConnectionType::CREATE_AS_CLIENT == getConnectionType())
			{
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::ReadFractureEvents);
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::ReadChunkTransform);
			}
			else
			{
				PX_ASSERT(!"invalid connection type!");
			}

			// set up chunk sync state
			const physx::PxU32 chunkSyncStateIndex = 0;
			PX_ASSERT(chunkSyncStateIndex < chunkSyncStateCount);
			chunkSyncState[chunkSyncStateIndex].excludeSleepingChunks = true;
			chunkSyncState[chunkSyncStateIndex].chunkTransformCopyDepth = 3;

			// set up actor sync params
			syncOperationOK(syncActor.setSyncParams(syncActorID, syncActorState.get(), chunkSyncState + chunkSyncStateIndex));
		}
		break;
	case SyncActorDetail::High :
		{
			// set up actor sync state
			StateManager<physx::PxU32, physx::apex::NxDestructibleActorSyncFlags::Enum> syncActorState;
			if(CommTool::ConnectionType::CREATE_AS_SERVER == getConnectionType())
			{
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::CopyFractureEvents);
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::CopyChunkTransform);
			}
			else if(CommTool::ConnectionType::CREATE_AS_CLIENT == getConnectionType())
			{
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::ReadFractureEvents);
				syncActorState.set(physx::apex::NxDestructibleActorSyncFlags::ReadChunkTransform);
			}
			else
			{
				PX_ASSERT(!"invalid connection type!");
			}

			// set up chunk sync state
			const physx::PxU32 chunkSyncStateIndex = 1;
			PX_ASSERT(chunkSyncStateIndex < chunkSyncStateCount);
			chunkSyncState[chunkSyncStateIndex].excludeSleepingChunks = false;
			chunkSyncState[chunkSyncStateIndex].chunkTransformCopyDepth = 3;

			// set up actor sync params
			syncOperationOK(syncActor.setSyncParams(syncActorID, syncActorState.get(), chunkSyncState + chunkSyncStateIndex));
		}
		break;
	default :
		PX_ASSERT(!"invalid syncActorDetail!");
	}
}

CommTool::ConnectionType::Enum DestructibleSyncManager::getConnectionType() const
{
	return connectionType;
}

physx::apex::NxDestructibleChunkSyncState * DestructibleSyncManager::getEditableChunkSyncState(physx::PxU32 index)
{
	physx::apex::NxDestructibleChunkSyncState * subjectChunkSyncState = NULL;
	PX_ASSERT(NULL != chunkSyncState);
	PX_ASSERT(chunkSyncStateCount > index);
	if(chunkSyncStateCount > index)
	{
		subjectChunkSyncState = chunkSyncState + index;
	}
	return subjectChunkSyncState;
}

template<typename Header> DestructibleSyncCallbackManager<Header>::~DestructibleSyncCallbackManager()
{
	freeBuffer();
	PX_ASSERT(NULL == bufferStart);
}

template<typename Header> void DestructibleSyncCallbackManager<typename Header>::onWriteBegin(Header *& bufferStartRequired, physx::PxU32 bufferSizeRequired)
{
	bufferStartRequired = NULL;
	PX_ASSERT(0 != bufferSizeRequired);
	if(bufferSizeRequired > bufferCapacity)
	{
		freeBuffer();
		makeBuffer(bufferSizeRequired);
	}
	if(NULL != commInterface)
	{
		PX_ASSERT(CommTool::ConnectionType::CREATE_AS_SERVER == connectionType);
		if(CommTool::ConnectionType::CREATE_AS_SERVER == connectionType)
		{
			PX_ASSERT(NULL != bufferStart);
			bufferStartRequired = static_cast<Header*>(bufferStart);
			PX_ASSERT(0 == bufferSize);
			bufferSize = bufferSizeRequired;
		}
	}
}

template<typename Header> void DestructibleSyncCallbackManager<typename Header>::onWriteDone(physx::PxU32 headerCount)
{
	PX_ASSERT(headerCount > 0);
	PX_FORCE_PARAMETER_REFERENCE(headerCount);
	if(NULL != commInterface)
	{
		PX_ASSERT(CommTool::ConnectionType::CREATE_AS_SERVER == connectionType);
		if(CommTool::ConnectionType::CREATE_AS_SERVER == connectionType)
		{
			PX_ASSERT(NULL != bufferStart && 0 != bufferSize);
			syncOperationOK(commInterface->send(bufferStart, bufferSize, connectionIndex));
			bufferSize = 0;
		}
	}
}

template<typename Header> void DestructibleSyncCallbackManager<typename Header>::onReadBegin(Header *& bufferStartRequested, physx::PxU32 & bufferSizeRequested, bool & doPointerSwizzling)
{
	bufferStartRequested = NULL;
	bufferSizeRequested = 0;
	doPointerSwizzling = true;
	if(NULL != commInterface)
	{
		if(CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType)
		{
			PX_ASSERT(CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType);
			void * bufferStartReceived = NULL;
			physx::PxU32 bufferSizeReceived = 0;
			syncOperationOK(commInterface->receive(bufferStartReceived, bufferSizeReceived, connectionIndex));
			bufferStartRequested = static_cast<Header*>(bufferStartReceived);
			bufferSizeRequested = bufferSizeReceived;
		}
	}
}

template<typename Header> void DestructibleSyncCallbackManager<typename Header>::onReadDone(const char * debugMessage)
{
	PX_ASSERT(NULL == debugMessage);
	PX_FORCE_PARAMETER_REFERENCE(debugMessage);
	if(NULL != commInterface)
	{
		PX_ASSERT(CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType);
		if(CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType)
		{
		}
	}
}

template<typename Header> DestructibleSyncCallbackManager<typename Header>::DestructibleSyncCallbackManager(CommTool::ConnectionType::Enum connectionType_, physx::PxU32 connectionIndex_, CommTool::CommInterface * commInterface_)
	:connectionType(connectionType_)
	,connectionIndex(connectionIndex_)
	,commInterface(commInterface_)
	,bufferStart(NULL)
	,bufferSize(0)
	,bufferCapacity(1024 * 4)
{
	PX_ASSERT(CommTool::ConnectionType::COUNT != connectionType);
	PX_ASSERT(connectionIndex < 3);
	PX_ASSERT(NULL != commInterface);
	makeBuffer(bufferCapacity);
}

template<typename Header> void DestructibleSyncCallbackManager<typename Header>::makeBuffer(physx::PxU32 bufferSize_)
{
	PX_ASSERT(NULL == bufferStart);
	if(NULL == bufferStart)
	{
		bufferStart = ::malloc(bufferSize_);
		::memset(bufferStart, 0x00, bufferSize_);
		bufferCapacity = bufferSize_;
	}
}

template<typename Header> void DestructibleSyncCallbackManager<typename Header>::freeBuffer()
{
	PX_ASSERT(NULL != bufferStart);
	if(NULL != bufferStart)
	{
		::free(bufferStart);
		bufferStart = NULL;
		bufferCapacity = 0;
	}
}

template<typename Header> DamageEventCallbackManager<typename Header>::DamageEventCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface)
	:DestructibleSyncCallbackManager(connectionType, connectionIndex, commInterface)
{
}

template<typename Header> DamageEventCallbackManager<typename Header>::~DamageEventCallbackManager()
{
}

template<typename Header> void DamageEventCallbackManager<typename Header>::onWriteDone(physx::PxU32 headerCount)
{
	// user can customize data here, after write on local side
	
	DestructibleSyncCallbackManager<Header>::onWriteDone(headerCount);
}

template<typename Header> void DamageEventCallbackManager<typename Header>::onSwizzleDone(physx::PxU32 headerCount)
{
	PX_ASSERT(headerCount > 0);
	PX_FORCE_PARAMETER_REFERENCE(headerCount);

	// user can customize data here, before read on receiving side
}

template<typename Header> FractureEventCallbackManager<typename Header>::FractureEventCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface)
	:DestructibleSyncCallbackManager(connectionType, connectionIndex, commInterface)
{
}

template<typename Header> FractureEventCallbackManager<typename Header>::~FractureEventCallbackManager()
{
}

template<typename Header> void FractureEventCallbackManager<typename Header>::onWriteDone(physx::PxU32 headerCount)
{
	// user can customize data here, after write on local side
	
	DestructibleSyncCallbackManager<Header>::onWriteDone(headerCount);
}

template<typename Header> void FractureEventCallbackManager<typename Header>::onSwizzleDone(physx::PxU32 headerCount)
{
	PX_ASSERT(headerCount > 0);
	PX_FORCE_PARAMETER_REFERENCE(headerCount);

	// user can customize data here, before read on receiving side
}

template<typename Header> ChunkMotionCallbackManager<typename Header>::ChunkMotionCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface)
	:DestructibleSyncCallbackManager(connectionType, connectionIndex, commInterface)
{
}

template<typename Header> ChunkMotionCallbackManager<typename Header>::~ChunkMotionCallbackManager()
{
}

template<typename Header> void ChunkMotionCallbackManager<typename Header>::onWriteDone(physx::PxU32 headerCount)
{
	// user can customize data here, after write on local side
	
	DestructibleSyncCallbackManager<Header>::onWriteDone(headerCount);
}

template<typename Header> void ChunkMotionCallbackManager<typename Header>::onSwizzleDone(physx::PxU32 headerCount)
{
	PX_ASSERT(headerCount > 0);
	PX_FORCE_PARAMETER_REFERENCE(headerCount);

	// user can customize data here, before read on receiving side
}

HitChunkManager::HitChunkManager(CommTool::ConnectionType::Enum connectionType_, physx::PxU32 connectionIndex_, CommTool::CommInterface * commInterface_)
	:connectionType(connectionType_)
	,connectionIndex(connectionIndex_)
	,commInterface(commInterface_)
	,bufferStart(NULL)
	,bufferSize(0)
	,bufferCapacity(1024 * 4)
{
	PX_ASSERT(CommTool::ConnectionType::COUNT != connectionType);
	PX_ASSERT(3 == connectionIndex);
	PX_ASSERT(NULL != commInterface);
	bufferStart = static_cast<CustomHeader*>(::malloc(bufferCapacity));
	::memset(static_cast<void*>(bufferStart), 0x00, bufferCapacity);
}

HitChunkManager::~HitChunkManager()
{
	if(NULL != bufferStart)
	{
		::free(static_cast<void*>(bufferStart));
		bufferStart = NULL;
		bufferCapacity = 0;
	}
}

void HitChunkManager::processSend(const std::vector<physx::apex::NxDestructibleActor*> * destructibleSyncActorContainer)
{
	if(NULL != commInterface ? CommTool::ConnectionType::CREATE_AS_SERVER == connectionType : false)
	{
		if(NULL != destructibleSyncActorContainer)
		{
			CustomHeader customHeader;
			::memset(static_cast<void*>(&customHeader), 0x00, sizeof(customHeader));
			bufferSize = sizeof(CustomHeader);
			for(physx::PxU32 index = 0; index < destructibleSyncActorContainer->size(); ++index)
			{
				// write data
				const physx::apex::NxDestructibleHitChunk * hitChunkContainer = NULL;
				physx::PxU32 hitChunkCount = 0;
				physx::apex::NxDestructibleActor & currentActor = *((*destructibleSyncActorContainer)[index]);
				PX_ASSERT(NULL != &currentActor);
				syncOperationOK(currentActor.getHitChunkHistory(hitChunkContainer, hitChunkCount));
				PX_ASSERT(!((NULL != hitChunkContainer) ^ (0 != hitChunkCount)));
				const physx::PxU32 currentItemSize = sizeof(physx::apex::NxDestructibleHitChunk) * hitChunkCount;
				if(NULL != hitChunkContainer && 0 != hitChunkCount)
				{
					// make a new buffer if space is insufficient
					bufferSize += currentItemSize;
					if(bufferCapacity < bufferSize)
					{
						void * newBuffer = ::malloc(bufferSize);
						::memcpy(newBuffer, static_cast<void*>(bufferStart), bufferSize);
						::free(bufferStart);
						bufferStart = static_cast<CustomHeader*>(newBuffer);
						newBuffer = NULL;
						bufferCapacity = bufferSize;
					}

					// write data into buffer space
					PX_ASSERT(bufferCapacity >= bufferSize);
					if(bufferCapacity >= bufferSize)
					{
						if(bufferSize > sizeof(CustomHeader))
						{
#define STEP_BY(start, size) (static_cast<void*>((static_cast<char*>(static_cast<void*>(start))) + (size)))
							::memcpy(STEP_BY(bufferStart, (bufferSize - currentItemSize)), static_cast<const void*>(hitChunkContainer), currentItemSize);
#undef STEP_BY
						}
					}
				}

				// write header
				PX_ASSERT(bufferSize >= currentItemSize);
				PX_ASSERT(index < CustomHeader::ItemCount);
				if(index < CustomHeader::ItemCount)
				{
					customHeader.item[index].id		= index;
					customHeader.item[index].offset = bufferSize - currentItemSize;
					customHeader.item[index].size	= currentItemSize;
				}

				// flush actor's hitChunk history
				currentActor.setHitChunkTrackingParams(true, true);
			}

			// write header into buffer space
			if(bufferSize > sizeof(CustomHeader))
			{
				::memcpy(static_cast<void*>(bufferStart), static_cast<void*>(&customHeader), sizeof(CustomHeader));
			}
			else
			{
				bufferSize = 0;
			}
		}

		// send data
		if(0 != bufferSize)
		{
			PX_ASSERT(NULL != bufferStart);
			if(NULL != bufferStart)
			{
				syncOperationOK(commInterface->send(bufferStart, bufferSize, connectionIndex));
				bufferSize = 0;
			}
		}
	}
}

void HitChunkManager::processListen()
{
	if(NULL != commInterface ? CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType : false)
	{
		void * bufferStartReceived = NULL;
		physx::PxU32 bufferSizeReceived = 0;
		syncOperationOK(commInterface->receive(bufferStartReceived, bufferSizeReceived, connectionIndex));
		PX_ASSERT(!((NULL != bufferSizeReceived) ^ (0 != bufferSizeReceived)));
		if(NULL != bufferSizeReceived && 0 != bufferSizeReceived)
		{
			CustomHeader * customHeader = NULL;
			customHeader = static_cast<CustomHeader*>(bufferStartReceived);
			physx::PxU32 bufferSizeProcessed = sizeof(CustomHeader);

			// save data
			for(physx::PxU32 index = 0; index < CustomHeader::ItemCount; ++index)
			{
#define ASSERT_IF(condition) PX_ASSERT(condition); if(condition)
				ASSERT_IF(customHeader->item[index].id == index)
				{
					ASSERT_IF((customHeader->item[index].offset + customHeader->item[index].size) <= bufferSizeReceived)
					{
						ASSERT_IF(0 == (customHeader->item[index].size % sizeof(physx::apex::NxDestructibleHitChunk)))
						{
							physx::apex::NxDestructibleHitChunk * hitChunkContainer = NULL;
#define STEP_BY(start, size) (static_cast<void*>((static_cast<char*>(static_cast<void*>(start))) + (size)))
							hitChunkContainer = static_cast<physx::apex::NxDestructibleHitChunk*>(STEP_BY(bufferStartReceived, customHeader->item[index].offset));
#undef STEP_BY
							const physx::PxU32 hitChunkCount = customHeader->item[index].size / sizeof(physx::apex::NxDestructibleHitChunk);
							for(physx::PxU32 hitChunkIndex = 0; hitChunkIndex < hitChunkCount; ++hitChunkIndex)
							{
								clientHitChunkContainer.hitChunkContainers[index]->push_back(*(hitChunkContainer + hitChunkIndex));
								bufferSizeProcessed += sizeof(physx::apex::NxDestructibleHitChunk);
							}
						}
					}
				}
#undef ASSERT_IF
			}
			PX_ASSERT(bufferSizeReceived == bufferSizeProcessed);
		}
	}
}

void HitChunkManager::processReceived(const std::vector<physx::apex::NxDestructibleActor*> * destructibleSyncActorContainer)
{
	if(NULL != commInterface ? CommTool::ConnectionType::CREATE_AS_CLIENT == connectionType : false)
	{
		if(NULL != destructibleSyncActorContainer)
		{
			for(physx::PxU32 index = 0; index < destructibleSyncActorContainer->size(); ++index)
			{
				physx::apex::NxDestructibleActor & currentActor = *((*destructibleSyncActorContainer)[index]);
				PX_ASSERT(NULL != &currentActor);
				std::vector<physx::apex::NxDestructibleHitChunk> & currentHitChunkContainer = *(clientHitChunkContainer.hitChunkContainers[index]);
				PX_ASSERT(NULL != & currentHitChunkContainer);
				if(!currentHitChunkContainer.empty())
				{
					syncOperationOK(currentActor.forceChunkHits(&currentHitChunkContainer[0], static_cast<physx::PxU32>(currentHitChunkContainer.size()), true));
					currentHitChunkContainer.clear();
				}
			}
		}
	}
}

HitChunkManager::ClientHitChunkContainer::ClientHitChunkContainer()
{
	for(physx::PxU32 index = 0; index < CustomHeader::ItemCount; ++index)
	{
		hitChunkContainers[index] = NULL;
		hitChunkContainers[index] = ::new std::vector<physx::apex::NxDestructibleHitChunk>;
	}
}

HitChunkManager::ClientHitChunkContainer::~ClientHitChunkContainer()
{
	for(physx::PxU32 index = 0; index < CustomHeader::ItemCount; ++index)
	{
		hitChunkContainers[index]->clear();
		::delete hitChunkContainers[index];
		hitChunkContainers[index] = NULL;
	}
}

#endif //USE_COMM_LAYER
