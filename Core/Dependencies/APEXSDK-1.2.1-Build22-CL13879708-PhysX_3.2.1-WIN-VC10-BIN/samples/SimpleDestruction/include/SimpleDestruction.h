// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Destructible Sample
// Description: This sample program shows how to create APEX destructible actors.
//
// ===============================================================================

#ifndef SIMPLE_DESTRUCTION_H
#define SIMPLE_DESTRUCTION_H

#include "SampleApexApplication.h"
#include "SampleInputEventIds.h"

#include "NxModuleDestructible.h"
#include "NxDestructibleActor.h"

#include "SimpleDestructionSync.h"

#if defined(PX_ANDROID)
#define CREATE_TRIMESH_TERRAIN	0
#else
#define CREATE_TRIMESH_TERRAIN	1
#endif

#if CREATE_TRIMESH_TERRAIN
#define OFFSET_HEIGHT	(0.0f)
#else
#define OFFSET_HEIGHT	(0.0f)
#endif

// default command line:
// -yup n -usestaticmesh y -loadasset Wall

namespace physx
{
namespace apex
{
	class NxDestructibleActor;
	class NxDestructibleAsset;
	class NxModuleDestructible;
	class NxModuleIofx;
#if NX_SDK_VERSION_MAJOR == 2
	class NxModuleFluidIos;
#elif NX_SDK_VERSION_MAJOR == 3
	class NxModuleParticleIos;
#endif //NX_SDK_VERSION_MAJOR
	class NxModuleEmitter;
	class NxApexRenderVolume;
	class NxDestructiblePreview;
	class NxApexScene;
	class NxDestructibleActorJoint;
}; //namespace apex
}; //namespace physx

class SampleDestructibleActor;

class DestructibleReplay;

void loadDestructibleActorBaseParams(NxParameterized::Interface* descParams, const physx::NxDestructibleAsset& asset);

struct ActorConstants
{
	enum Enum
	{
		MAX_ACTORS = 81
	};
};

struct WallType
{
	enum Enum
	{
		PC = 0,
		Console = 1,
		Tessellation = 2,
		NUM_WALL_TYPES
	};
};

struct SampleDestructionInputEventIds
{
	enum Enum
	{
		SAMPLE_DESTRUCTION_FIRST = NUM_SAMPLE_BASE_INPUT_EVENT_IDS,

		RAYCAST_HIT = SAMPLE_DESTRUCTION_FIRST,

		BOX_SHOT,

		BUNNY_SHOT,
		COW_SHOT,
		SHEET_SHOT,
		WALL_SHOT,

		SCENE_0,
		SCENE_1,
		SCENE_2,
		SCENE_3,
		SCENE_4,
		SCENE_5,
		SCENE_6,
		SCENE_7,
		SCENE_8,
		SCENE_9,

		TOGGLE_REPLAY_SAVE,
		TOGGLE_REPLAY_DIRECTION,
		SAVE_ACTOR_STATE,
		LOAD_SAVED_ACTOR_STATE,

		TOGGLE_TESSELLATION,
		INCREASE_TESSELLATION_FACTOR,
		DECREASE_TESSELLATION_FACTOR,
		INCREASE_TESSELLATION_SCALE,
		DECREASE_TESSELLATION_SCALE,
		INCREASE_TESSELLATION_UV_SCALE,
		DECREASE_TESSELLATION_UV_SCALE,
		INCREASE_TESSELLATION_LOD_DIST,
		DECREASE_TESSELLATION_LOD_DIST,

		ACTOR_SERIALIZATION_MODIFIER,
		REMOVE_ACTOR,
		HIDE_GRAPHICS,

		HARD_SLEEP_TOGGLE,

		FORCE_FIELD,

		NUM_SAMPLE_DESTRUCTION_INPUT_EVENT_IDS,
		NUM_EXCLUSIVE_SAMPLE_DESTRUCTION_INPUT_EVENT_IDS = NUM_SAMPLE_DESTRUCTION_INPUT_EVENT_IDS - SAMPLE_DESTRUCTION_FIRST,

		FIRST_MODEL_SHOT = BUNNY_SHOT,
		LAST_MODEL_SHOT  = WALL_SHOT,
		NUM_MODEL_SHOTS  = LAST_MODEL_SHOT - FIRST_MODEL_SHOT + 1
	};
};

class FrameDistributionObject
{
public:
	FrameDistributionObject(physx::PxU32 fireFrameNumber, physx::PxVec3	position);

	physx::PxU32	m_fireFrameNumber;
	physx::PxVec3	m_pos;

	virtual bool operator<(FrameDistributionObject rhs);
	static bool reverseSortPredicate(const FrameDistributionObject& lhs, const FrameDistributionObject& rhs);
};

// container class for the stuff needed to make a box
// that will kill the cows with default values tailored for
// the benchmark demos.
class BoxBullet : public FrameDistributionObject
{
public:
	BoxBullet(  physx::PxU32    fireFrameNumber, 
		physx::PxVec3   pos = physx::PxVec3(0.0f, 2.5f, 16.0f),
		physx::PxVec3   vel = physx::PxVec3(0.0f, 0.0f, -50.0f),
		physx::PxVec3   extents = physx::PxVec3(0.3f, 0.3f, 0.3f),
		physx::PxF32    density = 150.0f);

	physx::PxVec3	m_Vel;		// the initial velocity
	physx::PxVec3	m_Extents;	// the size of the Box
	physx::PxF32    m_Density;	// the density of the box

protected:
	BoxBullet(void);
};

template <class T>
class DamagePredicate
{
public:
	DamagePredicate(physx::apex::NxDestructibleActor* actor)
		: m_actor(actor)
	{}

	bool operator()(const T& item) const
	{
		return item.m_actor == m_actor;
	}
	
	physx::apex::NxDestructibleActor*	m_actor;
};

class PointDamage : public FrameDistributionObject
{
public:
	//applyDamage(physx::PxF32 damage, physx::PxF32 momentum, const physx::PxVec3& position, const physx::PxVec3& direction, physx::PxI32 chunkIndex = NxModuleDestructibleConst::INVALID_CHUNK_INDEX);
	PointDamage(physx::apex::NxDestructibleActor&	actor,
	            physx::PxU32			            frameNumber,
	            physx::PxF32			            damage,
	            physx::PxF32			            momentum,
	            const physx::PxVec3&	            position,
	            const physx::PxVec3&	            direction);

	physx::apex::NxDestructibleActor*	m_actor;
	physx::PxF32			            m_damage;
	physx::PxF32			            m_momentum;
	physx::PxVec3			            m_direction;

	void apply();

protected:
	PointDamage(void);
};

class RadiusDamage : public FrameDistributionObject
{
public:
	//applyRadiusDamage(physx::PxF32 damage, physx::PxF32 momentum, const physx::PxVec3& position, physx::PxF32 radius, bool falloff);
	RadiusDamage(physx::apex::NxDestructibleActor&	actor,
	             physx::PxU32			            frameNumber,
	             physx::PxF32			            damage,
	             physx::PxF32			            momentum,
	             const physx::PxVec3&	            position,
	             physx::PxF32			            radius,
 	             bool					            falloff);

	physx::apex::NxDestructibleActor*	m_actor;
	physx::PxF32			            m_damage;
	physx::PxF32			            m_momentum;
	physx::PxF32			            m_radius;
	bool					            m_falloff;

	void apply();

protected:
	RadiusDamage(void);
};

class SampleDestructiblePreview : public SampleFramework::SampleActor
{
public:
	SampleDestructiblePreview(SampleRenderer::Renderer& renderer, physx::apex::NxDestructibleAsset& asset, const physx::PxMat44& globalPose);
	virtual ~SampleDestructiblePreview(void);

	virtual void render(bool /*rewriteBuffers*/);
	physx::apex::NxDestructiblePreview* getPreview();
	physx::PxMat44 getPose();
	void setPose(physx::PxMat44 pose);
	void changeExplodeSize(physx::PxF32 delta);
	void cycleExplodeDepth();
	SampleDestructibleActor* instantiate(physx::apex::NxApexScene& scene, bool dynamic);

protected:
	SampleRenderer::Renderer&           m_renderer;
	physx::apex::NxDestructibleAsset&   m_asset;
	physx::apex::NxDestructiblePreview* m_preview;
	physx::PxF32				        m_explodeSize;
	int							        m_explodeDepth;
};

class SampleDestructionApplication : public SampleApexApplication
{
public:
	SampleDestructionApplication(const SampleFramework::SampleCommandLine& cmdline);
	virtual ~SampleDestructionApplication(void);

	physx::apex::NxDestructibleAsset* loadDestructibleAsset(const char* name);
	void pushBackUniqueAsset(physx::apex::NxDestructibleAsset* candidateAsset);
	// Process the command line args that need to be processed before the window is open
	void processEarlyCmdLineArgs(void);
	virtual void loadAnAsset(SampleRenderer::Renderer* renderer, const char* assetPtr, const physx::PxVec3& scale=physx::PxVec3(0.5f));
	virtual void tileAnAsset(SampleRenderer::Renderer* renderer, const char* assetPtr, unsigned nx, unsigned ny, unsigned nz);
	void loadAnAssetWithState(SampleRenderer::Renderer* renderer, const char* assetPtr, const char* actorStatePtr, const physx::PxVec3& scale=physx::PxVec3(0.5f));
	void loadActorState(physx::apex::NxDestructibleActor* actor, const char* actorStatePtr);

	virtual unsigned int getNumScenes() const;
	virtual unsigned int getSelectedScene() const;
	virtual const char* getSceneName(unsigned int sceneNumber) const;

protected:
	virtual void selectScene(unsigned int sceneNumber);

	bool triggerDamageEnabled() const;	
	void triggerDamage();
	void createTrimeshTerrain();

	static void setupHallmark(physx::PxMat44& globalPose, physx::PxU32 numActors, WallType::Enum wallEnum);
	static void benchmarkStart(const char* benchmarkName);

	// called just AFTER the window opens.
	virtual void onInit(void);
	// called just BEFORE the window closes. return 'true' to confirm the window closure.
	virtual void onShutdown(void);
	virtual void onTickPreRender(float dtime);
	// called when the window's contents needs to be redrawn.
	virtual void onRender(void);
	virtual void onTickPostRender(float dtime);

	virtual void collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents);
	virtual void collectInputDescriptions(std::vector<const char*>& inputDescriptions);

	virtual void onAnalogInputEvent(const SampleFramework::InputEvent& , float val);
	virtual bool onDigitalInputEvent(const SampleFramework::InputEvent& , bool val);
	virtual void onPointerInputEvent(const SampleFramework::InputEvent& ie, physx::PxU32 x, physx::PxU32 y, physx::PxReal dx, physx::PxReal dy);

public:
#if APEX_USE_PARTICLES
	physx::apex::NxModule*	getPhysXIosModule()
	{
		return m_apexNxFluidIosModule;
	}
#endif // APEX_USE_PARTICLES

	bool isGrbEnabled() const;
	
	struct DestructionSceneSelection
	{
		enum Enum
		{
			CommandLineScene = 0
			, HallmarkConsole
			, HallmarkPC
			, WallConsole
			, WallPC
			, EasterIslandHeadsConsole
			, EasterIslandHeadsPC
			, EasterIslandHeadsVoronoi
			, BlockTessellation
			, SceneCount
		};
	};

	class DestructionSceneManager
	{
	public:
		static DestructionSceneManager& Instance();
		void onInit(DestructionSceneSelection::Enum startUpScene, SampleDestructionApplication* app);
		void onDestroy();
		void onEventSwitchScene(const DestructionSceneSelection::Enum newScene, bool loadActors = true);
		void pushBackUniqueAsset(physx::apex::NxDestructibleAsset* candidateAsset) const;
		void pushBackActor(const char* assetFilename, bool isYup, bool isDynamic, const physx::PxVec3& position = physx::PxVec3(0.0f), const physx::PxVec3& scale=physx::PxVec3(0.5f)) const;
		void pushBackActorWithState(const char* assetFilename, const char* actorStateFileName, bool isYup, bool isDynamic, const physx::PxVec3& position = physx::PxVec3(0.0f), const physx::PxVec3& scale=physx::PxVec3(0.5f)) const;
		void setCustomSceneAssetName(const char*);
		const char* getSceneName(int sceneNumber = -1) const;
		const char* getSceneAssetName() const;
		physx::PxU32 getSceneTargetActorCount() const;
		physx::PxU32 getSceneDestructibleActorCount() const { return mSceneDestructibleActorCount; }
		DestructionSceneSelection::Enum getCurrentScene() const;

		static const char* getSceneAssetName(DestructionSceneSelection::Enum, const char* customSceneAssetName = "");

		DestructionSceneManager();
		DestructionSceneManager(const DestructionSceneManager&);
		DestructionSceneManager& operator= (const DestructionSceneManager&);
		~DestructionSceneManager();

		struct DestructionSceneFeature
		{
			enum Enum
			{
				FeatureGravityHigher = 0x1
				, FeatureProjectileSpeedHigher = 0x2
				, FeatureBunnyProjectile = 0x4
			};
		};

		class DestructionSceneStateManager
		{
		public:
			DestructionSceneStateManager(SampleDestructionApplication* a);
			~DestructionSceneStateManager();

			void tidySceneFeature() const;
			void notifyFeatureChange(DestructionSceneFeature::Enum e) const;

		protected:
			DestructionSceneStateManager();
			DestructionSceneStateManager(const DestructionSceneStateManager&);
			DestructionSceneStateManager& operator= (const DestructionSceneStateManager&);
			SampleDestructionApplication* app;
			mutable physx::PxU32 checkFlag;
#if NX_SDK_VERSION_MAJOR == 2
			NxVec3 nxGravity;
#elif NX_SDK_VERSION_MAJOR == 3
			physx::PxVec3 nxGravity;
#endif //NX_SDK_VERSION_MAJOR
			physx::PxF32 boxVelocityScale;
			bool useBunnyProjectile;
		};

		void onCreateScene(const DestructionSceneSelection::Enum e, bool loadActors = true);
		void onDestroyScene() const;

		DestructionSceneSelection::Enum mCurrentScene;
		SampleDestructionApplication* mDestructionApp;
		const char* mCustomSceneAssetName;
		physx::PxU32 mSceneDestructibleActorCount;
	};

protected:

	const bool                                      m_kPlatformIsPC;
	// Open Automate Variables
	std::vector<BoxBullet>						    m_BoxBulletList;
	physx::PxU32								    m_boxBulletCounter;
	std::vector<PointDamage>					    m_pointDamageList;
	std::vector<RadiusDamage>					    m_radiusDamageList;
	physx::apex::NxDestructibleAsset*		        m_CowAsset;
	physx::apex::NxDestructibleAsset*			    m_WallAsset[WallType::NUM_WALL_TYPES];
	bool										    m_Yup;
	bool										    m_RenderStaticChunkSeparately;

	std::vector<physx::apex::NxDestructibleAsset*>  m_destAssets;

	physx::apex::NxModuleDestructible*				m_apexDestructibleModule;

#if APEX_USE_PARTICLES
	physx::apex::NxModuleIofx*						m_apexIofxModule;
	physx::apex::NxModule*							m_apexNxFluidIosModule;
	physx::apex::NxModuleEmitter*					m_apexEmitterModule;
	physx::apex::NxApexRenderVolume*				m_renderVolume;
#endif // APEX_USE_PARTICLES

#if NX_SDK_VERSION_MAJOR == 2
	NxActor*									    m_terrainActor;
	NxTriangleMesh*								    m_terrainMesh;
#elif NX_SDK_VERSION_MAJOR == 3
	physx::PxTriangleMesh*							m_terrainMesh;
#endif //NX_SDK_VERSION_MAJOR
	SampleRenderer::RendererShape*				    m_terrainShape;
	SampleRenderer::RendererMeshContext			    m_terrainContext;
	SampleFramework::SampleMaterialAsset*		    m_groundMaterial;

	SampleDestructiblePreview*					    m_simpleDestructiblePreview;

	int											    m_perfScale;	// APEX quality scale (for scalable parameters)
	bool										    m_dynamic;

	physx::PxVec3								    m_position;		// Position for next actor

	physx::PxF32                                    m_boxVelocityScale;
	bool                                            m_useBunnyProjectile;
	bool											m_hideGraphics;
	bool											m_hardSleeping;
	physx::PxU32									m_explodeTimer;
	physx::PxVec3                                   m_velocity;

	std::vector<physx::apex::NxDestructibleActorJoint*>     m_destructibleJointList;

	DestructibleReplay*								m_replay;

#if USE_COMM_LAYER
private:
	std::vector<physx::apex::NxDestructibleActor*>	m_destructibleSyncActorAlias;
	DestructibleSyncManager							m_destructibleSyncManager;
#endif //USE_COMM_LAYER

};
#endif //SIMPLE_DESTRUCTION_H
