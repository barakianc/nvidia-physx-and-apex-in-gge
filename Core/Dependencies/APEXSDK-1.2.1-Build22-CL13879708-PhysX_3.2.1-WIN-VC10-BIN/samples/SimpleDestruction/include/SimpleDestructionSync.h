// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================

#ifndef SIMPLE_DESTRUCTION_SYNC_H
#define SIMPLE_DESTRUCTION_SYNC_H

#include "CommunicationsTool.h"
#if defined COMM_TOOL_VALID
#define USE_COMM_LAYER 1
#else
#define USE_COMM_LAYER 0
#endif // COMM_TOOL_VALID

#if USE_COMM_LAYER
#include <vector>
#include "NxModuleDestructible.h"
#include "NxDestructibleActor.h"

#define ENFORCE(condition) extern char unusableName[(condition)?1:-1]
template<class State, typename Flag>
class StateManager
{
public:
	StateManager():s(0)			{ENFORCE(sizeof(State) == sizeof(*this));}
	~StateManager()				{}
	void	set(Flag f)			{s |= f;}
	void	unset(Flag f)		{s &= ~f;}
	bool	isSet(Flag f) const	{return (0 != (s & f));}
	State	get() const			{return s;}
private:
	State	s;
};
#undef ENFORCE

struct SyncActorDetail
{
	enum Enum
	{
		None = 0,
		Low,
		Mid,
		High,
	};
};

template<typename Header>	class DamageEventCallbackManager;
template<typename Header>	class FractureEventCallbackManager;
template<typename Header>	class ChunkMotionCallbackManager;
							class HitChunkManager;

class DestructibleSyncManager
{
public:
	typedef DamageEventCallbackManager		<physx::apex::NxApexDamageEventHeader>		DamageEventManager;
	typedef FractureEventCallbackManager	<physx::apex::NxApexFractureEventHeader>	FractureEventManager;
	typedef ChunkMotionCallbackManager		<physx::apex::NxApexChunkTransformHeader>	ChunkMotionManager;
	DestructibleSyncManager();
	~DestructibleSyncManager();
public:
	void													onCommandLineInit(CommTool::ConnectionType::Enum connectionType);
	bool													onCreate();
	void													onDestroy();
	void													onTick();
	void													onEvent(std::vector<physx::apex::NxDestructibleActor*> * destructibleSyncActorAlias);
	void													setUpModuleSyncParams(physx::apex::NxModuleDestructible & module) const;
	void													setUpActorSyncParams(physx::apex::NxDestructibleActor & syncActor, physx::PxU32 syncActorID, SyncActorDetail::Enum syncActorDetail) const;
	CommTool::ConnectionType::Enum							getConnectionType() const;
private:
	DestructibleSyncManager(const DestructibleSyncManager &);
	DestructibleSyncManager & operator = (const DestructibleSyncManager &);
	physx::apex::NxDestructibleChunkSyncState *				getEditableChunkSyncState(physx::PxU32 index);
	CommTool::ConnectionType::Enum							connectionType;
	CommTool::CommInterface *								communicationInterface;
	DamageEventManager *									damageEventManager;
	FractureEventManager *									fractureEventManager;
	ChunkMotionManager *									chunkMotionManager;
	HitChunkManager *										hitChunkManager;
	physx::apex::NxDestructibleChunkSyncState *				chunkSyncState;
	physx::PxU32											chunkSyncStateCount;
};

template <typename Header> class DestructibleSyncCallbackManager : public physx::apex::NxUserDestructibleSyncHandler<Header>
{
public:
	virtual ~DestructibleSyncCallbackManager();
	void									onWriteBegin(Header *& bufferStart, physx::PxU32 bufferSize);
	virtual void							onWriteDone(physx::PxU32 headerCount);
	void									onReadBegin(Header *& bufferStart, physx::PxU32 & bufferSize, bool & doPointerSwizzling);
	void									onReadDone(const char * debugMessage);
protected:
	DestructibleSyncCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface);
private:
	DestructibleSyncCallbackManager();
	DestructibleSyncCallbackManager(const DestructibleSyncCallbackManager &);
	DestructibleSyncCallbackManager & operator = (const DestructibleSyncCallbackManager &);
	void									makeBuffer(physx::PxU32 bufferSize);
	void									freeBuffer();
protected:
	const CommTool::ConnectionType::Enum	connectionType;
	const physx::PxU32						connectionIndex;
	CommTool::CommInterface * const			commInterface;
	void *									bufferStart;
	physx::PxU32							bufferSize;
	physx::PxU32							bufferCapacity;
};

template <typename Header> class DamageEventCallbackManager : public DestructibleSyncCallbackManager<typename Header>
{
public:
	DamageEventCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface);
	~DamageEventCallbackManager();
	void onWriteDone(physx::PxU32 headerCount);
	void onSwizzleDone(physx::PxU32 headerCount);
private:
	DamageEventCallbackManager();
};

template <typename Header> class FractureEventCallbackManager : public DestructibleSyncCallbackManager<typename Header>
{
public:
	FractureEventCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface);
	~FractureEventCallbackManager();
	void onWriteDone(physx::PxU32 headerCount);
	void onSwizzleDone(physx::PxU32 headerCount);
private:
	FractureEventCallbackManager();
};

template <typename Header> class ChunkMotionCallbackManager : public DestructibleSyncCallbackManager<typename Header>
{
public:
	ChunkMotionCallbackManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface);
	~ChunkMotionCallbackManager();
	void onWriteDone(physx::PxU32 headerCount);
	void onSwizzleDone(physx::PxU32 headerCount);
private:
	ChunkMotionCallbackManager();
};

struct CustomHeader
{
	enum {ItemCount = 3,};
	struct Item
	{
		Item():id(0),offset(0),size(0) {}
		~Item() {}
		physx::PxU32 id;
		physx::PxU32 offset;
		physx::PxU32 size;
	} item[ItemCount];
};

class HitChunkManager
{
public:
	HitChunkManager(CommTool::ConnectionType::Enum connectionType, physx::PxU32 connectionIndex, CommTool::CommInterface * commInterface);
	~HitChunkManager();
	void processSend(const std::vector<physx::apex::NxDestructibleActor*> * destructibleSyncActorContainer);
	void processListen();
	void processReceived(const std::vector<physx::apex::NxDestructibleActor*> * destructibleSyncActorContainer);
private:
	HitChunkManager();
	HitChunkManager(const HitChunkManager &);
	HitChunkManager & operator = (const HitChunkManager &);
	const CommTool::ConnectionType::Enum	connectionType;
	const physx::PxU32						connectionIndex;
	CommTool::CommInterface * const			commInterface;
	CustomHeader *							bufferStart;
	physx::PxU32							bufferSize;
	physx::PxU32							bufferCapacity;
private:
	struct ClientHitChunkContainer
	{
		ClientHitChunkContainer();
		~ClientHitChunkContainer();
		std::vector<physx::apex::NxDestructibleHitChunk> * hitChunkContainers[CustomHeader::ItemCount];
	}										clientHitChunkContainer;
};

#endif //USE_COMM_LAYER
#endif //SIMPLE_DESTRUCTION_SYNC_H