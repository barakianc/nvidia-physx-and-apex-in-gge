// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================

#ifndef SIMPLE_DESTRUCTION_REPLAY_H
#define SIMPLE_DESTRUCTION_REPLAY_H

#include <sstream>
#include <iomanip>

#include "SimpleDestruction.h"
#include "NxParameterized.h"
#include "UserErrorCallback.h"

// Determine serialization type according to the file's extension
PX_INLINE NxParameterized::Serializer::SerializeType extensionToType(const char* filename);

// Extract the file name from an absolute path
PX_INLINE std::string getFileName(const char* filePathPtr, bool bWithExtension);

// Create a name for serialization of actor state
PX_INLINE std::string createSavedActorName(const char* assetName,
										   physx::PxU32 actorParamType,
										   physx::PxU32 actorIndex,
										   physx::PxU32 actorSaveIndex,
										   const char* extension);

///////////////////////////////////////////////////////////////////////////

// Generic class for serializing to, and deserializing from, a particular file
class NxParameterizedSerializer
{
public:
	NxParameterizedSerializer(physx::apex::NxApexSDK& apexSDK, 
							  physx::PxFileBuf* fileBuffer, 
							  NxParameterized::Serializer::SerializeType serializerType)
		: mApexSDK(apexSDK),
		  mFileBuffer(fileBuffer),
		  mSerializerType(serializerType)
	{

	}

	bool serialize(const NxParameterized::Interface* objToSerialize)
	{
		PX_ASSERT(NULL != objToSerialize);
		PX_ASSERT(NULL != mFileBuffer);
		if (NULL == objToSerialize || NULL == mFileBuffer)
			return false;

		PX_ASSERT(mFileBuffer->isOpen());
		if (!mFileBuffer->isOpen())
			return false;

		NxParameterized::Serializer* ser = mApexSDK.createSerializer(mSerializerType);
		PX_ASSERT(NULL != ser);
		if (NULL == ser)
			return false;

		NxParameterized::Serializer::ErrorType serError = ser->serialize(*mFileBuffer, &objToSerialize, 1);
		ser->release();
		PX_ASSERT(serError == NxParameterized::Serializer::ERROR_NONE);
		return serError == NxParameterized::Serializer::ERROR_NONE;
	}

	bool deserialize(NxParameterized::Interface*& objToDeserialize)
	{
		PX_ASSERT(mFileBuffer);
		if (!mFileBuffer)
			return false;

		NxParameterized::SerializePlatform platform;
		NxParameterized::Serializer::SerializeType serType = mApexSDK.getSerializeType(*mFileBuffer);
		NxParameterized::Serializer::ErrorType serError    = mApexSDK.getSerializePlatform(*mFileBuffer, platform);
		PX_ASSERT(serError == NxParameterized::Serializer::ERROR_NONE);
		if (serError != NxParameterized::Serializer::ERROR_NONE)
			return false;

		NxParameterized::Serializer* ser = mApexSDK.createSerializer(mSerializerType);
		PX_ASSERT(NULL != ser);
		if (NULL == ser)
			return false;

		NxParameterized::SerializePlatform currentPlatform;
		mApexSDK.getCurrentPlatform(currentPlatform);

		NxParameterized::Serializer::DeserializedData data;
		if (NxParameterized::Serializer::NST_BINARY == serType)
		{
			const physx::PxU32 len = mFileBuffer->getFileLength();
			void* p = mApexSDK.getParameterizedTraits()->alloc(len);
			mFileBuffer->read(p, len);

			if (platform == currentPlatform)
			{
				serError = ser->deserializeInplace(p, len, data);
			}
			else
			{
				physx::PxFileBuf* memStream = mApexSDK.createMemoryReadStream(p, len);
				serError = ser->deserialize(*memStream, data);
				mApexSDK.releaseMemoryReadStream(*memStream);
				mApexSDK.getParameterizedTraits()->free(p);
			}
		}
		else
		{
			serError = ser->deserialize(*mFileBuffer, data);
		}

		ser->release();

		if (serError == NxParameterized::Serializer::ERROR_NONE && data.size() == 1)
		{
			objToDeserialize = data[0];
		}
		else
		{
			objToDeserialize = NULL;
		}

		PX_ASSERT(NULL != objToDeserialize);
		return NULL != objToDeserialize;
	}

	~NxParameterizedSerializer()
	{
		releaseFileBuffer();
	}

private:

	void releaseFileBuffer()
	{
		if (mFileBuffer != NULL)
		{
			mFileBuffer->release();
			mFileBuffer = NULL;
		}
	}

	physx::apex::NxApexSDK& mApexSDK;
	physx::PxFileBuf* mFileBuffer;
	NxParameterized::Serializer::SerializeType mSerializerType;
};

PX_INLINE NxParameterizedSerializer& operator<<(NxParameterizedSerializer& out, const NxParameterized::Interface* in)
{
	if (!out.serialize(in))
	{
		ERRORSTREAM_DEBUG_WARNING("Could not save actor from state.");
	}
	return out;
}

PX_INLINE NxParameterizedSerializer& operator>>(NxParameterizedSerializer& in, NxParameterized::Interface*& out)
{
	if (!in.deserialize(out))
	{
		ERRORSTREAM_DEBUG_WARNING("Could not load actor from state.");
	}
	return in;
}

///////////////////////////////////////////////////////////////////////////

// Handles destructible replay via actor serialization and deserialization
class DestructibleReplay
{
	typedef physx::apex::NxDestructibleParameterizedType ParameterizedTypes;
	typedef ParameterizedTypes::Enum ParameterizedType;

	typedef SampleDestructionApplication::DestructionSceneManager   SceneManager;
	typedef SampleDestructionApplication::DestructionSceneSelection SceneSelection;

public:
	DestructibleReplay(SampleDestructionApplication& app) :
		mApp(app),
		mActorSaveIndex(0),
		mActorLoadIndex(0),
		mMaxActorLoadIndex(0),
		mSceneActorCount(0),
		mbSaveActor(false),
		mbSaveActorOnce(false),
		mbLoadSavedActor(false),
		mbHasLoadedSavedActor(false),
		mbLoadActorsInReverse(false),
		mActorParameterizedType(physx::apex::NxDestructibleParameterizedType::State)
	{ }

	// Process the actor save/load requests
	void tick()
	{
		if (!isEnabled())
			return;

		if (isSaving())
		{
			for (physx::PxU32 i = 0; i < mSceneActorCount; ++i)
			{
				saveActor(parameterizedType(), i, mActorSaveIndex);
			}
			mActorSaveIndex++;
			mbSaveActorOnce = false;
		}
		else if (isPlaying())
		{
			setPaused(pauseOnPlay());
			mActorLoadIndex = physx::PxClamp(mActorLoadIndex, 0, mMaxActorLoadIndex - 1);
			if (sceneNeedsReset())
			{
				SceneManager::Instance().onEventSwitchScene(SceneManager::Instance().getCurrentScene(), false);
				SceneManager::Instance().mSceneDestructibleActorCount += mSceneActorCount;
			}
			for (physx::PxU32 i = 0; i < mSceneActorCount; ++i)
			{
				loadActor(parameterizedType(), i, mActorLoadIndex, loadSceneActorsFromAsset());
			}
			mActorLoadIndex = mActorLoadIndex + (mbLoadActorsInReverse ? -1 : 1);

			if (mActorLoadIndex < 0 || mActorLoadIndex >= (physx::PxI32)mMaxActorLoadIndex)
			{
				// Unpause the sim after we're done with the replay
				setPaused(false);
				mbLoadSavedActor = false;
			}
			mbHasLoadedSavedActor = true;
		}
	}

	bool isEnabled() const
	{
#if defined(PX_WINDOWS)
		return true;
#else
		return false;
#endif
	}
	bool isActive()  const
	{
		return isPlaying() || isSaving();
	}
	bool hasPlayed() const
	{
		return mbHasLoadedSavedActor;
	}
	bool isPlaying() const
	{
		return mbLoadSavedActor;
	}
	bool isSaving()  const
	{
		return mbSaveActor || mbSaveActorOnce;
	}
	void setPaused(bool pause)
	{
		mApp.setPaused(pause);
	}

	bool validActorIndex(physx::PxU32 actorIndex) const
	{
		return (actorIndex < mSceneActorCount &&
		        actorIndex < mApp.getActors().size() &&
		        mApp.getActors()[actorIndex]->getType() == SampleDestructibleActor::DestructibleActor);
	}

	void reset()
	{
		mbSaveActor           = false;
		mbSaveActorOnce       = false;
		mbLoadSavedActor      = false;
		mbHasLoadedSavedActor = false;
		mActorLoadIndex       = 0;
		mActorSaveIndex       = 0;
		mMaxActorLoadIndex    = 0;
		mSceneActorCount      = 0;
	}

	void toggleReplayDirection()
	{
		mbLoadActorsInReverse = !mbLoadActorsInReverse;
	}

	void toggleSave()
	{
		if (!isEnabled() || mbLoadSavedActor)
		{
			return;
		}

		mbSaveActor      = !mbSaveActor;
		mbSaveActorOnce  = false;
		mSceneActorCount = SceneManager::Instance().getSceneDestructibleActorCount();

		// Trigger actor replay when we're finished saving
		if (!mbSaveActor)
		{
			toggleLoad(mActorSaveIndex);
		}
	}
	void toggleLoad(physx::PxU32 maxActorLoadCount = 0)
	{
		if (!isEnabled() || mbSaveActor)
		{
			return;
		}

		mbLoadSavedActor = !mbLoadSavedActor;
		if (mbLoadSavedActor)
		{
			setPaused(pauseOnPlay());
			// Store the asset name for the custom scene
			SceneManager::Instance().setCustomSceneAssetName(SceneManager::Instance().getSceneAssetName());
		}
		mMaxActorLoadIndex = maxActorLoadCount;
		mActorLoadIndex    = mbLoadActorsInReverse ? mMaxActorLoadIndex - 1 : 0;
		mActorSaveIndex    = 0;
	}
	void saveSingle()
	{
		if (isEnabled() && !mbSaveActor && !mbLoadSavedActor)
		{
			mbSaveActorOnce  = true;
			mSceneActorCount = SceneManager::Instance().getSceneDestructibleActorCount();
		}
	}
	void loadSingle()
	{
		if (isEnabled() && !mbLoadSavedActor && !mbSaveActor && !mbSaveActorOnce && mActorSaveIndex > 0)
		{
			mActorLoadIndex       = mActorSaveIndex - 1;
			mMaxActorLoadIndex    = mActorSaveIndex;
			mbLoadActorsInReverse = false;
			mbLoadSavedActor      = true;
			// Store the asset name for the custom scene
			SceneManager::Instance().setCustomSceneAssetName(SceneManager::Instance().getSceneAssetName());
		}
	}

	void appendStatus(std::string& status) const
	{
		if (!isActive())
			return;

		const size_t numDotsToAppend = (size_t)((mbSaveActor ? mActorSaveIndex : mActorLoadIndex) / 8 % 4);
		status.append(mbSaveActor ? "Recording" : "Playing");
		status.append(mbLoadActorsInReverse && mbLoadSavedActor ? " (Reverse)" : "");
		status.append(mbLoadActorsInReverse ? 3 - numDotsToAppend : numDotsToAppend, '.');
	}

protected:

	DestructibleReplay();
	DestructibleReplay(const DestructibleReplay&);
	DestructibleReplay& operator=(DestructibleReplay&);

	void saveActor(ParameterizedType type = ParameterizedTypes::State,
	               physx::PxU32 actorIndex = 0, physx::PxU32 actorSaveIndex = 0)
	{
		if (!validActorIndex(actorIndex))
			return;

		const SampleDestructibleActor* dActor                 = static_cast<SampleDestructibleActor*>(mApp.getActors()[actorIndex]);
		const NxParameterized::Interface* dActorParameterized = dActor->getActor()->getNxParameterized(type);
		PX_ASSERT(dActorParameterized);
		if (!dActorParameterized)
			return;

		const std::string dActorFileName = createSavedActorName("Actor", type, actorIndex, actorSaveIndex, ".apb");
		NxParameterizedSerializer serializer(*physx::apex::NxGetApexSDK(),
			physx::apex::NxGetApexSDK()->createStream(dActorFileName.c_str(), physx::PxFileBuf::OPEN_WRITE_ONLY),
			extensionToType(dActorFileName.c_str()));
		serializer << dActorParameterized;
	}

	void loadActor(ParameterizedType type = ParameterizedTypes::State,
	               physx::PxU32 actorIndex = 0, physx::PxU32 actorLoadIndex = 0, bool loadFromAsset = false)
	{
		if (actorIndex >= mSceneActorCount)
			return;

		const std::string dActorFileName = createSavedActorName("Actor", type, actorIndex, actorLoadIndex, ".apb");
		if (loadFromAsset)
		{
			const char* dSceneName = SceneManager::Instance().getSceneAssetName();
			mApp.loadAnAssetWithState(mApp.getRenderer(), dSceneName, dActorFileName.c_str());
		}
		else
		{
			mApp.loadActorState(static_cast<SampleDestructibleActor*>(mApp.getActors()[actorIndex])->getActor(),
			                    dActorFileName.c_str());
		}
	}

	ParameterizedType parameterizedType() const
	{
		return mActorParameterizedType;
	}

	// Whether to reset the scene each frame when replaying the destructible sequence
	bool sceneNeedsReset() const
	{
		return mApp.isGrbEnabled() || sceneHasCompoundStructures();
	}

	// Whether the scene contains structures that have multiple destructible actors
	bool sceneHasCompoundStructures() const
	{
		const SceneSelection::Enum scene = SceneManager::Instance().getCurrentScene();
		return ((scene == SceneSelection::HallmarkConsole) ||
				(scene == SceneSelection::HallmarkPC)      ||
				(scene == SceneSelection::CommandLineScene && mSceneActorCount > 0 ));
	}

	bool loadSceneActorsFromAsset()
	{
		return sceneNeedsReset();
	}

	// Whether to suspend simulation when replaying the destructible sequence
	bool pauseOnPlay() const
	{
		// GRB requires a "flush" sim step, so don't pause when it is enabled
		return !mApp.isGrbEnabled();
	}

	SampleDestructionApplication& mApp;
	physx::PxU32                  mActorSaveIndex;
	physx::PxI32                  mActorLoadIndex;
	physx::PxI32                  mMaxActorLoadIndex;
	physx::PxU32                  mSceneActorCount;
	bool                          mbSaveActor;
	bool                          mbSaveActorOnce;
	bool                          mbLoadSavedActor;
	bool                          mbHasLoadedSavedActor;
	bool                          mbLoadActorsInReverse;
	ParameterizedType             mActorParameterizedType;
};


// Determine serialization type according to the file's extension
NxParameterized::Serializer::SerializeType extensionToType(const char* filename)
{
	const char* extension = SampleApexResourceCallback::getFileExtension(filename);

	NxParameterized::Serializer::SerializeType serType = NxParameterized::Serializer::NST_LAST;

	while (serType == NxParameterized::Serializer::NST_LAST && extension != NULL)
	{
		extension++; // move beyond the '.'
		if (physx::string::stricmp(extension, "apx") == 0)
		{
			serType = NxParameterized::Serializer::NST_XML;
		}
		else if (physx::string::stricmp(extension, "apb") == 0)
		{
			serType = NxParameterized::Serializer::NST_BINARY;
		}

		extension = strchr(extension, '.');
	}

	return serType;
}

// Extract the file name from an absolute path
std::string getFileName(const char* filePathPtr, bool bWithExtension)
{
	std::string filePath(filePathPtr);
	size_t p0 = filePath.find_last_of("/\\") + 1;
	if (bWithExtension)
	{
		return filePath.substr(p0);
	}
	else
	{
		return filePath.substr(p0, filePath.find_last_of(".") - p0);
	}
}

// Create a name for serialization of actor state
std::string createSavedActorName(const char* assetName,
								 physx::PxU32 actorParamType,
								 physx::PxU32 actorIndex,
								 physx::PxU32 actorSaveIndex,
								 const char* extension)
{
	std::stringstream dActorName;
	const char delim = '_';
	dActorName << getFileName(assetName, false);
	dActorName << actorParamType << delim;
	dActorName << actorIndex << delim;
	dActorName << std::setfill('0') << std::setw(4) << actorSaveIndex;
	dActorName << extension;
	return dActorName.str();
}

#endif // SIMPLE_DESTRUCTION_REPLAY_H