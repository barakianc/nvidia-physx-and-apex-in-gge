// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX ForceField Sample
// Description: This sample program shows how to create APEX forcefield actors.
//
// ===============================================================================

#include <SampleApplication.h>
#include <SampleApexApplication.h>
#include <SampleAssetManager.h>
#include <SampleMaterialAsset.h>
#include <SampleCommandLine.h>
#include <SamplePlatform.h>
#include <SampleUserInput.h>
#include <SampleUserInputIds.h>
#include <SampleInputEventIds.h>
#include <SampleInputDefines.h>

#include <Renderer.h>
#include <RendererBoxShape.h>
#include <RendererMeshContext.h>
#include <RendererMaterialDesc.h>
#include <RendererMaterialInstance.h>
#include <RendererDirectionalLightDesc.h>
#include <RendererProjection.h>

#include <SampleApexRenderer.h>
#include <SamplesVRDSettings.h>

#include <NxApex.h>
#include <NxApexSDK.h>

#include <NxModuleEmitter.h>
#include <NxModuleEmitterLegacy.h>
#include <NxModuleIofx.h>
#include <NxModuleIofxLegacy.h>
#include <NxModuleForceField.h>
#include <NxModuleFrameworkLegacy.h>
#include <NxModuleCommonLegacy.h>

#include <NxParamUtils.h>

#include <NxApexEmitterAsset.h>
#include <NxApexEmitterActor.h>
#include <NxApexEmitterPreview.h>
#include <NxEmitterLodParamDesc.h>
#include <NxIofxAsset.h>

#include <NxModuleFieldSampler.h>
#include <NxForceFieldAsset.h>
#include <NxForceFieldActor.h>

#include <NxModuleParticleIos.h>
#include <NxParticleIosAsset.h>

#include <PsString.h>
#include <PsShare.h>
#include <PsMathUtils.h>

#if NX_SDK_VERSION_MAJOR == 3
#include "extensions/PxExtensionsAPI.h"
#endif

enum SceneSelection
{
	CommandLineAsset = 0,	// not implemented
	ParticleIosScene = 1,
	PhysXObjectScene = 2,
	ExplosionsScene = 3,
	NumScenes,
	NopScene,
};


enum SampleForceFieldInputEventIds
{
	SAMPLE_FORCE_FIELD_FIRST = NUM_SAMPLE_BASE_INPUT_EVENT_IDS,

	SCENE_0 = SAMPLE_FORCE_FIELD_FIRST,
	SCENE_1,
	SCENE_2,
	SCENE_3,
	SCENE_4,
	SCENE_5,
	SCENE_6,
	SCENE_7,
	SCENE_8,
	SCENE_9,

	SCENE_TOGGLE,

	INCREASE_SIM_SCALE,
	DECREASE_SIM_SCALE,

	RESET_SIM,
	HIDE_GRAPHICS,

	NUM_SAMPLE_FORCE_FIELD_INPUT_EVENT_IDS,
	NUM_EXCLUSIVE_SAMPLE_FORCE_FIELD_INPUT_EVENT_IDS = NUM_SAMPLE_FORCE_FIELD_INPUT_EVENT_IDS - SAMPLE_FORCE_FIELD_FIRST,
};
const char* const SampleForceFieldInputEventDescriptions[] =
{
	"switch to scene 0",
	"switch to scene 1",
	"switch to scene 2",
	"switch to scene 3",
	"switch to scene 4",
	"switch to scene 5",
	"switch to scene 6",
	"switch to scene 7",
	"switch to scene 8",
	"switch to scene 9",

	"switch to another scene",

	"increase the simulation scale",
	"decrease the simulation scale",

	"reset the simulation",
	"show/hide particles",
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleForceFieldInputEventDescriptions) == NUM_EXCLUSIVE_SAMPLE_FORCE_FIELD_INPUT_EVENT_IDS);


#if defined(RENDERER_ANDROID)
#include "SamplePlatform.h"

struct android_app;
static android_app* gState;

const char* COMMANDLINE_FILE = "/sdcard/media/APEX/1.2/SimpleForceField/commandline.txt";
#endif

using namespace physx::apex;
using namespace physx;

#define GROUND_PLANE_COL_GROUP					18

class SampleApexEmitterActor : public SampleFramework::SampleActor
{
public:
	SampleApexEmitterActor(NxApexEmitterAsset& asset, NxApexScene& apexScene, physx::PxU32 i)
	{
		NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
		PX_ASSERT(descParams);
		if (!descParams)
		{
			return;
		}

		m_actor = static_cast<NxApexEmitterActor*>(asset.createApexActor(*descParams, apexScene));
		PX_ASSERT(m_actor);
		if (m_actor)
		{
			m_actor->setCurrentPosition(physx::PxVec3(i * 20.0f, 0.0f, 0.0f));
			m_actor->startEmit(true);
		}
	}

	void setCurrentPosition(const physx::PxVec3& pos)
	{
		m_actor->setCurrentPosition(pos);
	}

	void setCurrentPose(const physx::PxMat44& pose)
	{
		m_actor->setCurrentPose(pose);
	}

	virtual ~SampleApexEmitterActor(void)
	{
		if (m_actor)
		{
			m_actor->release();
		}
	}

	void updateEyePosition(physx::PxVec3& /*position*/)
	{
	}

	virtual void render(bool /*rewriteBuffers*/)
	{
	}

	virtual int getType()
	{
		return ApexEmitterActor;
	}

	enum { ApexEmitterActor = 5 };

	NxApexEmitterActor* m_actor;
};

class SampleForceFieldActor : public SampleFramework::SampleActor
{
public:
	SampleForceFieldActor(NxForceFieldAsset& asset, NxApexScene& apexScene, const physx::PxF32& scale, const physx::PxMat44 pos) :
		m_asset(asset)
	{
		m_actor = 0;
		NxParameterized::Interface* defaultParams = m_asset.getDefaultActorDesc();
		PX_ASSERT(defaultParams);
		NxParameterized::setParamF32(*defaultParams, "scale", scale);
		NxParameterized::setParamMat44(*defaultParams, "initialPose", pos);
		m_actor =  static_cast<NxForceFieldActor*>(m_asset.createApexActor(*defaultParams, apexScene));
		PX_ASSERT(m_actor);
	}

	virtual ~SampleForceFieldActor(void)
	{
		if (m_actor)
		{
			m_asset.releaseForceFieldActor(*m_actor);
		}
	}

	NxForceFieldActor* m_actor;
	NxForceFieldAsset& m_asset;
};

class SimpleForceFieldApplication : public SampleApexApplication
{
public:
	SimpleForceFieldApplication(const SampleFramework::SampleCommandLine& cmdline)
		: SampleApexApplication(cmdline)
	{
		m_simulationBudget				= 0xffffff00;
		m_renderParticles				= true;
		m_velocityClamp					= 2.5f;
		m_elapsedTime					= 0;
		m_nextExplosionTime				= 0;
		m_sceneNumber					= ParticleIosScene;

		m_apexParticleIosModule			= 0;
		m_apexEmitterModule				= 0;
		m_nbEmitterModuleScalables		= 0;
		m_emitterModuleScalables		= 0;
		m_apexIofxModule				= 0;
		m_apexFieldSamplerModule		= 0;

		m_apexIosAsset					= 0;
		m_apexIofxAsset					= 0;

		m_AppPtr = this;

		// set up debug rendering
		DebugRenderConfiguration config;

		config.flags.push_back(DebugRenderFlag("ForceField Shape", "ForceField/VISUALIZE_FORCEFIELD_ACTOR", 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Collision Shapes", physx::PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f));
		addDebugRenderConfig(config);
		config.flags.clear();

		m_commandLineInput.registerCommand(SampleCommandLineInputs[SCLIDS::NO_APEX_CUDA]);
		m_commandLineInput.registerCommand(SampleCommandLineInputs[SCLIDS::NO_INTEROP]);
	}

	void initializeText()
	{
		if (m_sceneNumber == ParticleIosScene)
		{
			strcpy(m_windowString[0], "");
			strcpy(m_windowString[1], "ParticleIOS with basic explosion force field");
			strcpy(m_windowString[2], "Use right mouse click to activate explosions");
			strcpy(m_windowString[3], "Use 'v' to turn on debug rendering");
			strcpy(m_windowString[4], "");
			strcpy(m_windowString[5], "");
		}
		else if (m_sceneNumber == PhysXObjectScene)
		{
			strcpy(m_windowString[0], "");
			strcpy(m_windowString[1], "PhysX 3 objects with basic explosion force field");
			strcpy(m_windowString[2], "Add boxes (space), capsules (ctrl-space) and spheres (shift-space) to scene");
			strcpy(m_windowString[3], "Use right mouse click to activate explosions");
			strcpy(m_windowString[4], "Use 'v' to turn on debug rendering");
			strcpy(m_windowString[5], "");
		}
		else if (m_sceneNumber == ExplosionsScene)
		{
			strcpy(m_windowString[0], "");
			strcpy(m_windowString[1], "ParticleIOS with three explosions");
			strcpy(m_windowString[2], "Left = Standard, Middle = Slow, Right = Noise");
			strcpy(m_windowString[3], "Use 'v' to turn on debug rendering");
			strcpy(m_windowString[4], "");
			strcpy(m_windowString[5], "");
		}
		else // NopScene
		{
			strcpy(m_windowString[0], "");
			strcpy(m_windowString[1], "");
			strcpy(m_windowString[2], "");
			strcpy(m_windowString[3], "");
			strcpy(m_windowString[4], "");
			strcpy(m_windowString[5], "");
		}
		
		if (m_cudaContext)
		{
			strcat(m_windowString[WINDOW_SCENE_NAME_INDEX], "CUDA");
			if (interopEnabled())
				strcat(m_windowString[WINDOW_SCENE_NAME_INDEX], " + Interop");
		}
		else
		{
			strcat(m_windowString[WINDOW_SCENE_NAME_INDEX], "CPU");
		}
	}

	// Process the command line args that need to be processed before the window is open
	void processEarlyCmdLineArgs(void)
	{
		// this logic isn't quite right but it broke the ability to run the sample with -ogl
		if (m_cmdline.getNumArgs() > 1 && !rendererOverride())
		{
			m_sceneNumber = NopScene;
		}

		SampleApexApplication::processEarlyCmdLineArgs();
	}

	// Determine world impact position to create explosion
#if NX_SDK_VERSION_MAJOR == 3
	virtual void onMouseClickShapeHit(const PxVec3& pickDir, PxRaycastHit& hit, PxShape* hitshape)
	{
		if (m_sceneNumber == ParticleIosScene)
		{
			physx::PxF32 scale = 1.0f;
			physx::PxMat44 pos = physx::PxMat44::createIdentity();
			pos.setPosition(hit.impact);
			for (physx::PxU32 i = 0; i < m_apexForceFieldAssetList.size(); i++)
			{
				m_actors.push_back(new SampleForceFieldActor(*m_apexForceFieldAssetList[i], *m_apexScene, scale, pos));
			}
		}
		else if (m_sceneNumber == PhysXObjectScene)
		{
			physx::PxF32 scale = 1.0f;
			physx::PxMat44 pos = physx::PxMat44::createIdentity();
			pos.setPosition(hit.impact);
			for (physx::PxU32 i = 0; i < m_apexForceFieldAssetList.size(); i++)
			{
				SampleForceFieldActor* actor = new SampleForceFieldActor(*m_apexForceFieldAssetList[i], *m_apexScene, scale, pos);
				actor->m_actor->setStrength(1500.0f);
				m_actors.push_back(actor);
			}
		}
	}
#endif

	virtual ~SimpleForceFieldApplication(void)
	{
		PX_ASSERT(!m_apexParticleIosModule);
		PX_ASSERT(!m_apexEmitterModule);
		PX_ASSERT(!m_apexIofxModule);
		PX_ASSERT(!m_apexFieldSamplerModule);
		PX_ASSERT(!m_lights.size());
		PX_ASSERT(!m_actors.size());
	}

private:
	static SimpleForceFieldApplication* m_AppPtr;

	void unloadCurrentAssetsAndActors()
	{
		for (physx::PxU32 i = 0; i < m_shape_actors.size(); i++)
		{
			if (m_shape_actors[i])
			{
				m_shape_actors[i]->release();
			}
		}
		m_shape_actors.clear();

		if (m_sceneNumber == ParticleIosScene || m_sceneNumber == PhysXObjectScene || m_sceneNumber == ExplosionsScene)
		{
			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i])
				{
					m_actors[i]->release();
				}
			}
			m_actors.clear();

			for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
			{
				releaseApexAsset(m_apexEmitterAssetList[i]);
			}
			m_apexEmitterAssetList.clear();

			for (physx::PxU32 i = 0; i < m_apexFieldSamplerAssetList.size(); i++)
			{
				releaseApexAsset(m_apexFieldSamplerAssetList[i]);
			}
			m_apexFieldSamplerAssetList.clear();

			for (physx::PxU32 i = 0; i < m_apexForceFieldAssetList.size(); i++)
			{
				releaseApexAsset(m_apexForceFieldAssetList[i]);
			}
			m_apexForceFieldAssetList.clear();

#if NX_SDK_VERSION_MAJOR == 3
			m_apexScene->getPhysXScene()->setGravity(PxVec3(0.0f));
#endif
		}
	}

	void loadAssetsAndActors(SceneSelection sceneNumber)
	{
		if (sceneNumber >= NumScenes)
		{
			return;
		}

		m_sceneNumber = sceneNumber;

		if (!m_apexScene || !m_apexSDK || !m_apexEmitterModule)
		{
			return;
		}

		if (m_sceneNumber == ParticleIosScene)
		{
			createParticleIosScene();
		}
		else if (m_sceneNumber == PhysXObjectScene)
		{
			createPhysXObjectScene();
		}
		else if (m_sceneNumber == ExplosionsScene)
		{
			createExplosionsScene();
		}
	}

	// called just AFTER the window opens.
	virtual void onInit(void)
	{
		sampleInit("SimpleForceField", 
			cudaSupported()    && !hasInputCommand(SCLIDS::NO_APEX_CUDA), 
			interopSupported() && !hasInputCommand(SCLIDS::NO_INTEROP));
		m_apexScene = createSampleScene(PxVec3(0, -9.8f, 0));
#if NX_SDK_VERSION_MAJOR == 3
		m_apexScene->getPhysXScene()->setGravity(PxVec3(0.0f));
#endif

#if APEX_MODULES_STATIC_LINK
		instantiateModuleParticleIos();
		instantiateModuleEmitter();
		instantiateModuleEmitterLegacy();
		instantiateModuleFieldSampler();
		instantiateModuleCommonLegacy();
		instantiateModuleFrameworkLegacy();
		instantiateModuleIofx();
		instantiateModuleIOFXLegacy();
# if NX_SDK_VERSION_MAJOR == 3
		instantiateModuleForceField();
#endif
#endif

		m_apexParticleIosModule = static_cast<NxModuleParticleIos*>(loadModule("ParticleIOS"));

		m_apexEmitterModule = static_cast<NxModuleEmitter*>(loadModule("Emitter"));
		if (m_apexEmitterModule)
		{
			m_nbEmitterModuleScalables = m_apexEmitterModule->getNbParameters();
			m_emitterModuleScalables = m_apexEmitterModule->getParameters();
			for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
			{
				NxApexParameter& p = *m_emitterModuleScalables[i];
				m_apexEmitterModule->setIntValue(i, p.range.maximum);
			}
		}

		m_apexIofxModule = static_cast<NxModuleIofx*>(loadModule("IOFX"));

		m_apexFieldSamplerModule = static_cast<NxModuleFieldSampler*>(loadModule("FieldSampler"));

		loadModule("ForceField");
		loadModule("Emitter_Legacy");
		loadModule("Common_Legacy");
		loadModule("Framework_Legacy");
		loadModule("IOFX_Legacy");

		if (!m_apexParticleIosModule && !m_apexIofxModule)
		{
			return;
		}


		if (interopSupported())
		{
			if (!m_cudaContext &&
				m_errorCallback &&
				!hasInputCommand(SCLIDS::NO_APEX_CUDA) &&
				!hasInputCommand(SCLIDS::NO_INTEROP))
			{
				char buf[256];
				sprintf(buf, "No GPU Dispatcher created, CUDA will not be used within APEX\n");
				m_errorCallback->reportError(physx::PxErrorCode::eDEBUG_INFO, buf, __FILE__, __LINE__);
			}
		}

		initializeText();

		m_resourceCallback->setApexSupport(*m_apexSDK);

		initializeApexCollisionNames();

		// if there are no command line arguments run the default scene(1)
//		physx::PxU32 argc = m_cmdline.getArgC();
//		const char* const* argv = m_cmdline.getArgV();
		int numArgs = m_cmdline.getNumArgs();
		if ((numArgs <= 1) || ((numArgs == 2) && rendererOverride()))
		{
			loadAssetsAndActors(m_sceneNumber);
		}

		if (m_apexIofxModule)
		{
			physx::PxBounds3 b;
			b.setInfinite();
			m_renderVolumeList.push_back(m_apexIofxModule->createRenderVolume(*m_apexScene, b, 0, true));
			//m_apexIofxModule->disableCudaModifiers();
		}

		mRenderMeshContext.material         = m_simpleLitMaterial->getMaterial();
		mRenderMeshContext.materialInstance = m_simpleLitMaterial->getMaterialInstance();

		// NOTE: Only one light here for now, the second light halved the framerate.
		SampleRenderer::RendererDirectionalLightDesc lightdesc;

		lightdesc.color     = SampleRenderer::RendererColor(100, 100, 50, 255);
		lightdesc.intensity = 1;
		lightdesc.direction = physx::PxVec3(0, -0.707f, -0.707f);
		m_lights.push_back(getRenderer()->createLight(lightdesc));

		m_apexScene->allocViewMatrix(ViewMatrixType::LOOK_AT_RH);
		m_apexScene->allocProjMatrix(ProjMatrixType::USER_CUSTOMIZED);
	}

	// called just BEFORE the window closes. return 'true' to confirm the window closure.
	virtual void onShutdown(void)
	{
		SampleFramework::SampleAssetManager* assetManager = getAssetManager();
		PX_ASSERT(assetManager);
		PX_FORCE_PARAMETER_REFERENCE(assetManager);

		for (physx::PxU32 i = 0; i < m_lights.size(); i++)
		{
			m_lights[i]->release();
		}
		m_lights.clear();

		for (physx::PxU32 i = 0; i < m_actors.size(); i++)
		{
			if (m_actors[i])
			{
				m_actors[i]->release();
			}
		}
		m_actors.clear();

		unloadCurrentAssetsAndActors();

		for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
		{
			releaseApexAsset(m_apexEmitterAssetList[i]);
		}
		m_apexEmitterAssetList.clear();

		for (physx::PxU32 i = 0; i < m_apexFieldSamplerAssetList.size(); i++)
		{
			releaseApexAsset(m_apexFieldSamplerAssetList[i]);
		}
		m_apexFieldSamplerAssetList.clear();

		for (physx::PxU32 i = 0; i < m_apexForceFieldAssetList.size(); i++)
		{
			releaseApexAsset(m_apexForceFieldAssetList[i]);
		}
		m_apexForceFieldAssetList.clear();

		if (m_apexEmitterModule)
		{
			m_apexSDK->releaseModule(m_apexEmitterModule);
			m_apexEmitterModule = 0;
		}

		if (m_renderVolumeList.size())
		{
			for (physx::PxU32 i = 0; i < m_renderVolumeList.size(); i++)
			{
				if (m_renderVolumeList[i])
				{
					m_apexIofxModule->releaseRenderVolume(*m_renderVolumeList[i]);
					m_renderVolumeList[i] = 0;
				}
			}
			m_renderVolumeList.clear();
		}

		if (m_apexIofxModule)
		{
			m_apexSDK->releaseModule(m_apexIofxModule);
			m_apexIofxModule = 0;
		}

		if (m_apexParticleIosModule)
		{
			m_apexSDK->releaseModule(m_apexParticleIosModule);
			m_apexParticleIosModule = 0;
		}

		if (m_apexFieldSamplerModule)
		{
			m_apexSDK->releaseModule(m_apexFieldSamplerModule);
			m_apexFieldSamplerModule = 0;
		}

		SampleApexApplication::onShutdown();
	}

	virtual void onTickPreRender(float dtime)
	{
		m_elapsedTime += dtime;

		if (m_apexScene && !m_pause)
		{
			if (m_sceneNumber == ParticleIosScene)
			{
				updateParticleIosScene(dtime);
			}
			else if (m_sceneNumber == PhysXObjectScene)
			{
				updatePhysXObjectScene(dtime);
			}
			else if(m_sceneNumber == ExplosionsScene)
			{
				updateExplosionsScene(dtime);
			}

			for (PxU32 i = 0; i < m_shape_actors.size(); i++)
			{
				m_shape_actors[i]->tick(dtime);
			}

			m_apexScene->simulate(dtime);
		}
		SampleApexApplication::onTickPreRender(dtime);
	}

	// called when the window's contents needs to be redrawn.
	virtual void onRender(void)
	{
		updateScalableAndLodInfo();

		SampleRenderer::Renderer* renderer = getRenderer();
		if (renderer)
		{
			physx::PxU32 windowWidth  = 0;
			physx::PxU32 windowHeight = 0;
			renderer->getWindowSize(windowWidth, windowHeight);
			if (windowWidth > 0 && windowHeight > 0)
			{
				renderer->clearBuffers();

				m_projection = SampleRenderer::RendererProjection(45.0f, windowWidth / (float)windowHeight, 0.1f, 10000.0f);

				// The APEX scene needs the view and projection matrices for LOD, IOFX modifiers, and other things
				updateApexSceneMatrices();

				for (physx::PxU32 i = 0; i < m_lights.size(); i++)
				{
					renderer->queueLightForRender(*m_lights[i]);
				}

				for (physx::PxU32 i = 0; i < m_actors.size(); i++)
				{
					if (m_actors[i])
					{
						m_actors[i]->render();
					}
				}

				for (PxU32 i = 0; i < m_shape_actors.size(); i++)
				{
					if (m_shape_actors[i])
					{
						m_shape_actors[i]->render();
					}
				}

				if (m_sceneNumber == ParticleIosScene)
				{
				}

				if (m_sceneNumber == PhysXObjectScene)
				{
				}

				if (m_sceneNumber == ExplosionsScene)
				{
					// display elapsed time
					static char time[128];
					physx::string::sprintf_s(time, sizeof(time), "%4.1f", m_elapsedTime);
					getRenderer()->print(720, 10, time, 0.6f, 6.0f, SampleRenderer::RendererColor(255, 255, 255), true);
				}

				if (m_apexScene)
				{
					SampleApexRenderer apexRenderer;

					if (m_apexRenderDebug)
					{
						m_apexRenderDebug->lockRenderResources();
						m_apexRenderDebug->updateRenderResources(0);
						m_apexRenderDebug->unlockRenderResources();
						m_apexRenderDebug->dispatchRenderResources(apexRenderer);
					}

					m_apexScene->lockRenderResources();
					m_apexScene->updateRenderResources();
					m_apexScene->dispatchRenderResources(apexRenderer);
					m_apexScene->unlockRenderResources();
				}

				if (m_apexIofxModule && m_apexScene && m_renderParticles)
				{
					m_apexScene->prepareRenderResourceContexts();

					SampleApexRenderer apexRenderer;
					NxApexRenderableIterator* iter = m_apexIofxModule->createRenderableIterator(*m_apexScene);
					for (NxApexRenderable* r = iter->getFirst(); r ; r = iter->getNext())
					{
						r->updateRenderResources(NULL);
						r->dispatchRenderResources(apexRenderer);
					}
					iter->release();
				}

				renderer->render(getEyeTransform(), m_projection);

				///////////////////////////////////////////////////////////////////////////

				std::string sceneInfo;
				for (int i = WINDOW_SCENE_INFO_INDEX; i < NUM_WINDOW_STRINGS; ++i)
				{
					if (!empty(m_windowString[i]))
					{
						sceneInfo.append(m_windowString[i]);
						sceneInfo.append("\n");
					}
				}
				printSceneInfo(m_windowString[WINDOW_SCENE_NAME_INDEX], sceneInfo.c_str());

				renderer->swapBuffers();
			}
		}
	}

	virtual void onTickPostRender(float dtime)
	{
		if (m_apexScene && !m_pause)
		{
			physx::PxU32 errorState = 0;
			m_apexScene->fetchResults(true, &errorState);
		}
		
#if NX_SDK_VERSION_MAJOR == 2
		const NxDebugRenderable* debugRenderable = m_apexScene->getPhysXScene()->getDebugRenderable();
		if (debugRenderable && m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(*debugRenderable);
		}
#elif NX_SDK_VERSION_MAJOR == 3
		const physx::PxRenderBuffer& renderBuffer = m_apexScene->getPhysXScene()->getRenderBuffer();
		if (m_apexRenderDebug != NULL)
		{
			m_apexRenderDebug->addDebugRenderable(renderBuffer);
		}
#endif // NX_SDK_VERSION_MAJOR

		SampleApexApplication::onTickPostRender(dtime);
	}


	virtual void collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
	{
		using namespace SampleFramework;

		std::vector<const char*> inputDescriptions;
		collectInputDescriptions(inputDescriptions);

		//digital keyboard events
		DIGITAL_INPUT_EVENT_DEF_2(SCENE_0,				WKEY_0,			XKEY_0,			PS3KEY_0,		AKEY_UNKNOWN,	OSXKEY_0,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_0);
		DIGITAL_INPUT_EVENT_DEF_2(SCENE_1,				WKEY_1,			XKEY_1,			PS3KEY_1,		ABUTTON_2,		OSXKEY_1,		PSP2KEY_UNKNOWN,	IBUTTON_2,		LINUXKEY_1);
		DIGITAL_INPUT_EVENT_DEF_2(SCENE_2,				WKEY_2,			XKEY_2,			PS3KEY_2,		ABUTTON_1,		OSXKEY_2,		PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_2);
		DIGITAL_INPUT_EVENT_DEF_2(SCENE_3,				WKEY_3,			XKEY_3,			PS3KEY_3,		ABUTTON_3,		OSXKEY_3,		PSP2KEY_UNKNOWN,	IBUTTON_3,		LINUXKEY_3);
		//DIGITAL_INPUT_EVENT_DEF_2(SCENE_4,				WKEY_4,			XKEY_4,			PS3KEY_4,		ABUTTON_4,		OSXKEY_4,		PSP2KEY_UNKNOWN,	IBUTTON_4,		LINUXKEY_4);
		//DIGITAL_INPUT_EVENT_DEF_2(SCENE_5,				WKEY_5,			XKEY_5,			PS3KEY_5,		ABUTTON_5,		OSXKEY_5,		PSP2KEY_UNKNOWN,	IBUTTON_5,		LINUXKEY_5);
		//DIGITAL_INPUT_EVENT_DEF_2(SCENE_6,				WKEY_6,			XKEY_6,			PS3KEY_6,		ABUTTON_6,		OSXKEY_6,		PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_6);
		//DIGITAL_INPUT_EVENT_DEF_2(SCENE_7,				WKEY_7,			XKEY_7,			PS3KEY_7,		ABUTTON_7,		OSXKEY_7,		PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_7);
		//DIGITAL_INPUT_EVENT_DEF_2(SCENE_8,				WKEY_8,			XKEY_8,			PS3KEY_7,		ABUTTON_8,		OSXKEY_8,		PSP2KEY_UNKNOWN,	IBUTTON_8,		LINUXKEY_8);
		//DIGITAL_INPUT_EVENT_DEF_2(SCENE_9,				WKEY_9,			XKEY_9,			PS3KEY_9,		ABUTTON_9,		OSXKEY_9,		PSP2KEY_UNKNOWN,	IBUTTON_9,		LINUXKEY_9);

		DIGITAL_INPUT_EVENT_DEF_2(SCENE_SELECT,			WKEY_Q,			XKEY_Q,			PS3KEY_Q,		AKEY_UNKNOWN,	OSXKEY_Q,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Q);

		DIGITAL_INPUT_EVENT_DEF_2(DECREASE_SIM_SCALE,	WKEY_DIVIDE,	XKEY_DIVIDE,	PS3KEY_DIVIDE,	AKEY_UNKNOWN,	OSXKEY_DIVIDE,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_DIVIDE);
		DIGITAL_INPUT_EVENT_DEF_2(INCREASE_SIM_SCALE,	WKEY_MULTIPLY,	XKEY_MULTIPLY,	PS3KEY_MULTIPLY, AKEY_UNKNOWN,	OSXKEY_MULTIPLY, PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_MULTIPLY);

		DIGITAL_INPUT_EVENT_DEF_2(RESET_SIM,			WKEY_R,			XKEY_R,			PS3KEY_R,		AKEY_UNKNOWN,	OSXKEY_R,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_R);
		DIGITAL_INPUT_EVENT_DEF_2(HIDE_GRAPHICS,		WKEY_G,			XKEY_G,			PS3KEY_G,		AKEY_UNKNOWN,	OSXKEY_G,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_G);

		SampleApexApplication::collectInputEvents(inputEvents);
	}

	virtual void collectInputDescriptions(std::vector<const char*>& inputDescriptions)
	{
		SampleApexApplication::collectInputDescriptions(inputDescriptions);
		inputDescriptions.insert(inputDescriptions.end(), SampleForceFieldInputEventDescriptions, SampleForceFieldInputEventDescriptions + PX_ARRAY_SIZE(SampleForceFieldInputEventDescriptions));
	}

protected:
	virtual unsigned int getNumScenes() const
	{
		return NumScenes;
	}

	virtual unsigned int getSelectedScene() const
	{
		return m_sceneNumber;
	}

	virtual const char* getSceneName(unsigned int sceneNumber) const
	{
		switch(sceneNumber)
		{
		case 0:
			return "CommandLine Asset (not implemented)";
		case 1:
			return "ParticleIosScene";
		case 2:
			return "PhysXObjectScene";
		case 3:
			return "ExplosionsScene";
		default:
			PX_ALWAYS_ASSERT();
			return "";
		}
	}

	virtual void selectScene(unsigned int sceneNumber)
	{
		unloadCurrentAssetsAndActors();
		loadAssetsAndActors((SceneSelection)sceneNumber);
		initializeText();
	}

private:

	virtual bool onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
	{
		if (!getRenderer() || !m_apexScene)
		{
			return SampleApexApplication::onDigitalInputEvent(ie, val);
		}

		if (val)
		{
			switch (ie.m_Id)
			{
			case SCENE_0:
			case SCENE_1:
			case SCENE_2:
			case SCENE_3:
			case SCENE_4:
			case SCENE_5:
			case SCENE_6:
			case SCENE_7:
			case SCENE_9:
			{
				physx::PxU32 index = (ie.m_Id - SCENE_0);
				physx::PxU32 scene = physx::PxMin(index, (physx::PxU32)NumScenes - 1);
				unloadCurrentAssetsAndActors();
				loadAssetsAndActors((SceneSelection)scene);
				initializeText();
			}
			break;
			case SCENE_SELECT:
			{
				unloadCurrentAssetsAndActors();
				loadAssetsAndActors((SceneSelection)((m_sceneNumber + 1) % NumScenes));
				initializeText();
			}
			break;
			case INCREASE_SIM_SCALE:
			{
				for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
				{
					NxApexParameter& p = *m_emitterModuleScalables[i];
					physx::PxU32 newValue = physx::PxMin<physx::PxU32>(p.range.maximum, p.current + 1);
					m_apexEmitterModule->setIntValue(i, newValue);
				}
				updateScalableAndLodInfo();
			}
			break;
			case DECREASE_SIM_SCALE:
			{
				for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
				{
					NxApexParameter& p = *m_emitterModuleScalables[i];
					physx::PxU32 newValue = physx::PxMax<physx::PxU32>(p.range.minimum, p.current - 1);
					m_apexEmitterModule->setIntValue(i, newValue);
				}
				updateScalableAndLodInfo();
			}
			case RESET_SIM:
			{
				// TODO: SJB - make this a standard key
				m_pause = true;

				unloadCurrentAssetsAndActors();
				loadAssetsAndActors((SceneSelection)m_sceneNumber);

				m_pause = false;
			}
			case HIDE_GRAPHICS:
				//toggle rendering particles
				m_renderParticles = !m_renderParticles;
				break;
			default:
				return SampleApexApplication::onDigitalInputEvent(ie, val);
			};
			return true;
		}
		else
		{
			return SampleApexApplication::onDigitalInputEvent(ie, val);
		}
	}

	void initializeApexCollisionNames()
	{
#if NX_SDK_VERSION_MAJOR == 3
		PxFilterData g;
		g.word1 = g.word2 = g.word3 = 0;
		g.word0 = 1;
		m_resourceCallback->registerSimulationFilterData("MESH_COL_GRP_MASK", g);
		g.word0 = 1;
		m_resourceCallback->registerSimulationFilterData("SPRITE_COL_GRP_MASK", g);
		g.word0 = 3;
		m_resourceCallback->registerSimulationFilterData("ALL_COL_GRP_MASK", g);

		g.word0 = 1;
		g.word2 = ~0;
		m_apexFieldSamplerModule->addPhysXMonitorFilterData(*m_apexScene, g);
#endif
	}

	void createParticleIosScene()
	{
		//this scene demonstrates the effect of forcefields on ParticleIos
		m_simulationBudget = 114000;
		m_apexScene->setLODResourceBudget(m_simulationBudget);

		PxVec3 gravity(0.0f, 0.0f, 0.0f);
#if NX_SDK_VERSION_MAJOR == 3
		m_apexScene->getPhysXScene()->setGravity(gravity);
#endif

		forceLoadAssets();

		//initialize assets
		m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "spriteEmitterParticleIos")));
		m_apexForceFieldAssetList.push_back(static_cast<NxForceFieldAsset*>(loadApexAsset(NX_FORCEFIELD_AUTHORING_TYPE_NAME, "basicExplosion")));

		SampleRenderer::Renderer*            renderer     = getRenderer();
		SampleFramework::SampleAssetManager* assetManager = getAssetManager();

		if (m_apexScene && renderer && assetManager)
		{
			//emitters
			for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[i], *m_apexScene, i));
			}

			//position the emitter
			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
				{
					SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
					PX_ASSERT(actor);
					if (actor)
					{
						actor->setCurrentPosition(physx::PxVec3(0, 1.5f, 0));
					}
				}
			}

			//create ground plane
			addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
		}
	}

	void createBoxStack(PxI32 size)
	{
		const float cubeSize = 0.3f;
		const float spacing = 0.0f;//-2.0f*gPhysicsSDK->getParameter(NX_SKIN_WIDTH);
		PxVec3 pos(0.0f, cubeSize, 0.0f);
		float offset = -size * (cubeSize * 2.0f + spacing) * 0.5f;
		while(size)
		{
			for(PxI32 i=0; i<size; i++)
			{
				pos.x = offset + (float)i * (cubeSize * 2.0f + spacing);
				addCollisionShape(BoxShapeType, pos, PxVec3(0), PxVec3(cubeSize));
			}

			offset += cubeSize;
			pos.y += (cubeSize * 2.0f + spacing);
			size--;
		}
	}

	void createBoxTower(PxU32 size, PxVec3 basePosition)
	{
		const float cubeSize = 0.3f;
		const float spacing = 0.00f;
		PxVec3 pos(0.0f, cubeSize, 0.0f);
		pos = pos + basePosition;
		while(size)
		{
			addCollisionShape(BoxShapeType, pos, PxVec3(0), PxVec3(cubeSize));
			pos.y += (cubeSize * 2.0f + spacing);
			size--;
		}
	}

	void createPhysXObjectScene()
	{
		//this scene demonstrates the effect of explosion field samplers on ParticleIos
		m_simulationBudget = 114000;
		m_apexScene->setLODResourceBudget(m_simulationBudget);

		PxVec3 gravity(0.0f, -9.81f, 0.0f);
#if NX_SDK_VERSION_MAJOR == 3
		m_apexScene->getPhysXScene()->setGravity(gravity);
#endif

		forceLoadAssets();

		//initialize assets
		m_apexForceFieldAssetList.push_back(static_cast<NxForceFieldAsset*>(loadApexAsset(NX_FORCEFIELD_AUTHORING_TYPE_NAME, "basicExplosion")));

		SampleRenderer::Renderer* renderer = getRenderer();
		SampleFramework::SampleAssetManager* assetManager = getAssetManager();

		if (m_apexScene && renderer && assetManager)
		{
			//create ground plane
			addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));

			for (PxF32 i=-4; i<=4; i+=8)
			{
				PxVec3 towerBase(-i,0,0);
				createBoxTower(4, towerBase + PxVec3(0,0,0));
				createBoxTower(4, towerBase + PxVec3(-1,0,-1));
				createBoxTower(4, towerBase + PxVec3(1,0,-1));
				createBoxTower(4, towerBase + PxVec3(1,0,1));
				createBoxTower(4, towerBase + PxVec3(-1,0,1));
			}

			createBoxStack(7);
		}
	}

	void createExplosionsScene()
	{
		//this scene demonstrates the effect of forcefields on ParticleIos
		m_simulationBudget = 114000;
		m_apexScene->setLODResourceBudget(m_simulationBudget);

		PxVec3 gravity(0.0f, 0.0f, 0.0f);
#if NX_SDK_VERSION_MAJOR == 3
		m_apexScene->getPhysXScene()->setGravity(gravity);
#endif

		forceLoadAssets();

		//initialize assets
		m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "spriteEmitterParticleIosLong")));
		m_apexForceFieldAssetList.push_back(static_cast<NxForceFieldAsset*>(loadApexAsset(NX_FORCEFIELD_AUTHORING_TYPE_NAME, "explosionStandard")));
		m_apexForceFieldAssetList.push_back(static_cast<NxForceFieldAsset*>(loadApexAsset(NX_FORCEFIELD_AUTHORING_TYPE_NAME, "explosionSlow")));
		m_apexForceFieldAssetList.push_back(static_cast<NxForceFieldAsset*>(loadApexAsset(NX_FORCEFIELD_AUTHORING_TYPE_NAME, "explosionNoise")));

		SampleRenderer::Renderer*            renderer     = getRenderer();
		SampleFramework::SampleAssetManager* assetManager = getAssetManager();

		if (m_apexScene && renderer && assetManager)
		{
			//emitters
			for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
			{
				m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[i], *m_apexScene, i));
			}

			//position the emitter
			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i]->getType() == SampleApexEmitterActor::ApexEmitterActor)
				{
					SampleApexEmitterActor* actor = static_cast<SampleApexEmitterActor*>(m_actors[i]);
					PX_ASSERT(actor);
					if (actor)
					{
						actor->setCurrentPosition(physx::PxVec3(0, 1.5f, 0));
					}
				}
			}

			//create ground plane
			addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
		}

		m_elapsedTime = 0;
		m_nextExplosionTime = 5;
	}

	void updateParticleIosScene(float /*dtime*/)
	{
	}

	void updatePhysXObjectScene(float /*dtime*/)
	{
	}

	void updateExplosionsScene(float /*dtime*/)
	{
		float interval = 10.0f;

		if (m_elapsedTime > m_nextExplosionTime)
		{
			//create explosion actors
			physx::PxF32 scale = 1.0f;
			physx::PxMat44 pos = physx::PxMat44::createIdentity();
			SampleForceFieldActor* actor = NULL;

			//left explosion (explosionStandard.apx)
			pos.setPosition(physx::PxVec3(-14.0f, 0.0f, 5.0f));
			actor = new SampleForceFieldActor(*m_apexForceFieldAssetList[0], *m_apexScene, scale, pos);
			m_actors.push_back(actor);

			//middle explosion (explosionSlow.apx)
			pos.setPosition(physx::PxVec3(0.0f, 0.0f, -5.0f));
			actor = new SampleForceFieldActor(*m_apexForceFieldAssetList[1], *m_apexScene, scale, pos);
			m_actors.push_back(actor);

			//right explosion (explosionNoise.apx)
			pos.setPosition(physx::PxVec3(14.0f, 0.0f, 5.0f));
			actor = new SampleForceFieldActor(*m_apexForceFieldAssetList[2], *m_apexScene, scale, pos);
			m_actors.push_back(actor);

			m_nextExplosionTime += interval;
		}
	}

private:

	enum {
		WINDOW_SCENE_NAME_INDEX = 0,
		WINDOW_SCENE_INFO_INDEX = 1,
		NUM_WINDOW_STRINGS      = 6,
		WINDOW_STRING_LENGTH    = 512
	};

	char							m_windowString[NUM_WINDOW_STRINGS][WINDOW_STRING_LENGTH];
	bool							m_renderParticles;
	SceneSelection					m_sceneNumber;
	float							m_capsuleHeight;
	float							m_velocityClamp;
	float							m_elapsedTime;
	float							m_nextExplosionTime;

	NxModuleParticleIos*			m_apexParticleIosModule;
	NxModuleEmitter*				m_apexEmitterModule;
	PxU32							m_nbEmitterModuleScalables;
	NxApexParameter** 				m_emitterModuleScalables;
	NxModuleIofx*					m_apexIofxModule;
	NxModuleFieldSampler*			m_apexFieldSamplerModule;

	std::vector<NxApexRenderVolume*> m_renderVolumeList;
	std::vector<NxApexEmitterAsset*> m_apexEmitterAssetList;
	std::vector<NxRenderMeshAsset*>	m_apexRenderMeshAssetList;
	std::vector<NxApexAsset*>		m_apexFieldSamplerAssetList;
	std::vector<NxForceFieldAsset*>	m_apexForceFieldAssetList;

	NxApexAsset*					m_apexIosAsset;
	NxApexAsset*					m_apexIofxAsset;

	SampleRenderer::RendererMeshContext		mRenderMeshContext;
};

SimpleForceFieldApplication* SimpleForceFieldApplication::m_AppPtr /* = NULL */;

bool SampleEntry(const SampleFramework::SampleCommandLine& cmdline)
{
	int width, height;	// display width and height
	bool ok = true;

	SimpleForceFieldApplication app(cmdline);
	app.processEarlyCmdLineArgs();
#if defined(RENDERER_ANDROID)
	/* We need to register event handling callbacks and process events, while window is not yet shown. */
	if (!SampleFramework::SamplePlatform::platform()->preOpenWindow(gState))
	{
		return false;
	}
	while (!SampleFramework::SamplePlatform::platform()->isOpen())
	{
		SampleFramework::SamplePlatform::platform()->update();
	}
#endif
	width = app.getDisplayWidth();
	height = app.getDisplayHeight();
	app.open(width, height, "APEX ForceField Test");
	while (app.isOpen())
	{
		app.update();
#if defined(RENDERER_ANDROID)
		if (!SampleFramework::SamplePlatform::platform()->isOpen())
		{
			break;
		}
#endif
		app.doInput();
	}
	app.close();
	return ok;
}

#if defined(RENDERER_WINDOWS)

int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE, LPSTR /*cmdLine*/, int show_command)
{
	SampleFramework::SampleCommandLine cl(GetCommandLineA());
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#elif defined(RENDERER_XBOX360)

int main(void)
{
	char* argv[32];
	int argc = 0;
	volatile LPSTR commandLineString;

	//__debugbreak(); debugging help when launching xbox app from winders

	commandLineString = GetCommandLine(); // xbox call to get command line arguments

	/* first pull out the application name */
	argv[argc] = strtok(commandLineString, " ");

	/* pull out the other args */
	while (argv[argc] != NULL)
	{
		argc++;
		argv[argc] = strtok(NULL, " ");
	}

	SampleFramework::SampleCommandLine cl((unsigned int)argc, argv);
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#elif defined(RENDERER_PS3)

int main(int argc, char** argv)
{
	SampleFramework::SampleCommandLine cl((unsigned int)argc, argv);
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#elif defined(RENDERER_ANDROID)

extern "C" void android_main(struct android_app* state)
{
	gState = state;
	const char* argv[] = { "dummy", 0 };
	SampleFramework::SampleCommandLine cl(1, argv, COMMANDLINE_FILE);
	SampleEntry(cl);
	exit(0);
	/* Will not return return code, because NDK's native_app_glue declares this function to return nothing. Too bad. */
}

#else

int main(int argc, const char* const* argv)
{
	SampleFramework::SampleCommandLine cl((unsigned int)argc, argv);
	bool ok = SampleEntry(cl);
	return ok ? 0 : 1;
}

#endif
