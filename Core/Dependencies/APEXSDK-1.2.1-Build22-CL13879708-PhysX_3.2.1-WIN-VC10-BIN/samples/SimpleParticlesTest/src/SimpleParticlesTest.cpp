// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Particles Sample
// Description: This sample program shows how to create APEX emitter actors.
//
// ===============================================================================

#include <SimpleParticlesTest.h>
#include <ApexParticleEditor.h>


const char* const SampleParticlesInputEventDescriptions[] =
{
	"move the actor backward",
	"move the actor forward",
	"move the actor left",
	"move the actor right",
	"move the actor up",
	"move the actor down",

	"switch to scene 0",
	"switch to scene 1",
	"switch to scene 2",
	"switch to scene 3",
	"switch to scene 4",
	"switch to scene 5",
	"switch to scene 6",
	"switch to scene 7",
	"switch to scene 8",
	"switch to scene 9",

	"switch to another scene",

	"reset the particle emitters",
	"add an actor to the scene",

	"increase the simulation scale",
	"decrease the simulation scale",

	"reset the simulation",

	"toggle the density visualization",
	"toggle object collsion groups (physx 2 only)",

	"toggle ground emitter attachment to character"
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleParticlesInputEventDescriptions) == NUM_EXCLUSIVE_SAMPLE_PARTICLES_INPUT_EVENT_IDS);

///////////////////////////////////////////////////////////////////////////

struct SampleParticlesCommandLineInputIds
{
	enum Enum
	{
		BASIC_IOS_MARK = SampleCommandLineInputIds::COUNT,
		NX_FLUID_IOS_MARK,
		IMPACT_EMITTER_MARK,
		GROUND_EMITTER_MARK,
		EXPLICIT_EMITTER_MARK,
		FIGURE_MARK,
		STAR_MARK,
		ASSET_PREVIEW_MARK,

		EDIT,

		FIRST = BASIC_IOS_MARK,
		LAST  = EDIT,
		COUNT = LAST - FIRST + 1,

		FIRST_BENCHMARK = BASIC_IOS_MARK,
		LAST_BENCHMARK  = ASSET_PREVIEW_MARK
	};
};

typedef SampleParticlesCommandLineInputIds SPCLIDS;
const CommandLineInput SampleParticlesCommandLineInputs[] =
{
	{ SPCLIDS::BASIC_IOS_MARK       , "basicIosMark",        "", "run the specified benchmark" },
	{ SPCLIDS::NX_FLUID_IOS_MARK    , "nxFluidIosMark",      "", "run the specified benchmark" },
	{ SPCLIDS::IMPACT_EMITTER_MARK  , "impactEmitterMark",   "", "run the specified benchmark" },
	{ SPCLIDS::GROUND_EMITTER_MARK  , "groundEmitterMark",   "", "run the specified benchmark" },
	{ SPCLIDS::EXPLICIT_EMITTER_MARK, "explicitEmitterMark", "", "run the specified benchmark" },
	{ SPCLIDS::FIGURE_MARK          , "figureMark",          "", "run the specified benchmark" },
	{ SPCLIDS::STAR_MARK            , "starMark",            "", "run the specified benchmark" },
	{ SPCLIDS::ASSET_PREVIEW_MARK   , "assetPreviewMark",    "", "run the specified benchmark" },
	{ SPCLIDS::EDIT                 , "edit",                "", "<asset_name> edits the specified asset" }
};
PX_COMPILE_TIME_ASSERT(PX_ARRAY_SIZE(SampleParticlesCommandLineInputs) == SPCLIDS::COUNT);

static const char* benchmarkName(unsigned id)
{
	if (id < SPCLIDS::FIRST_BENCHMARK || id > SPCLIDS::LAST_BENCHMARK)
	{
		return NULL;
	}
	return SampleParticlesCommandLineInputs[id - SPCLIDS::FIRST].mShortName;
}

//SampleApexEmitterActor

SampleApexEmitterActor::SampleApexEmitterActor(NxApexEmitterAsset& asset, NxApexScene& apexScene, size_t i)
{
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	m_actor = static_cast<NxApexEmitterActor*>(asset.createApexActor(*descParams, apexScene));
	PX_ASSERT(m_actor);
	if (m_actor)
	{
		m_actor->setCurrentPosition(physx::PxVec3(i * 20.0f - 10.0f, 10.0f, 0.0f));
		m_actor->startEmit(true);
	}
}

SampleApexEmitterActor::~SampleApexEmitterActor(void)
{
	if (m_actor)
	{
		m_actor->release();
	}
}

NxApexActor* SampleApexEmitterActor::getActor(void) const
{
	return m_actor;
}

int SampleApexEmitterActor::getType()
{
	return ApexEmitterActor;
}

void SampleApexEmitterActor::setActor(NxApexEmitterActor* act)
{
	m_actor = act;
}


//SampleImpactEmitterActor

SampleImpactEmitterActor::SampleImpactEmitterActor(NxImpactEmitterAsset& asset, NxApexScene& apexScene)
{

	NxParameterized::Interface* params = asset.getDefaultActorDesc();
	PX_ASSERT(params);

	m_actor = static_cast<NxImpactEmitterActor*>(asset.createApexActor(*params, apexScene));
	PX_ASSERT(m_actor);
}

SampleImpactEmitterActor::~SampleImpactEmitterActor()
{
	if (m_actor)
	{
		m_actor->release();
	}
}

void SampleImpactEmitterActor::registerImpact(const physx::PxVec3& hitPos, const physx::PxVec3& hitDir, const physx::PxVec3& surfNorm, physx::PxU32 surfType)
{
	m_actor->registerImpact(hitPos, hitDir, surfNorm, surfType);
}

//SampleGroundEmitterActor

SampleGroundEmitterActor::SampleGroundEmitterActor(NxGroundEmitterAsset& asset, NxApexScene& apexScene, const physx::PxVec3& position)
{
	NxParameterized::Interface* defaultParams = asset.getDefaultActorDesc();

	m_pose = physx::PxMat44::createIdentity();
	m_pose.setPosition(position);
	NxParameterized::setParamMat44(*defaultParams, "globalPose", m_pose);

	m_actor = static_cast<NxGroundEmitterActor*>(asset.createApexActor(*defaultParams, apexScene));
}


SampleGroundEmitterActor::~SampleGroundEmitterActor(void)
{
	if (m_actor)
	{
		m_actor->release();
		m_actor = 0;
	}
}

void SampleGroundEmitterActor::updatePosition(const physx::PxVec3& position)
{
	if (m_actor)
	{
		m_pose.setPosition(position);
		m_actor->setPose(m_pose);
	}
}



int SampleGroundEmitterActor::getType(void)
{
	return GroundEmitterActor;
}

//SampleFieldSamplerActor

SampleFieldSamplerActor::SampleFieldSamplerActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i)
{
	NxParameterized::Interface* descParams = asset.getDefaultActorDesc();
	PX_ASSERT(descParams);
	if (!descParams)
	{
		return;
	}

	NxParameterized::Handle handle(descParams);

	if (NxParameterized::ERROR_NONE == handle.getParameter("position"))
	{
		handle.setParamVec3(physx::PxVec3(i * 20.0f - 10.0f, 0.0f, 0.0f));
	}

	m_actor = asset.createApexActor(*descParams, apexScene);
	PX_ASSERT(m_actor);
}


SampleFieldSamplerActor::~SampleFieldSamplerActor(void)
{
	if (m_actor)
	{
		m_actor->release();
	}
}


void SampleFieldSamplerActor::updateEyePosition(physx::PxVec3& /*position*/)
{
}


void SampleFieldSamplerActor::render(bool /*rewriteBuffers*/)
{
}


int SampleFieldSamplerActor::getType()
{
	return FieldSamplerActor;
}

SimpleParticlesTestApplication::SimpleParticlesTestApplication(const SampleFramework::SampleCommandLine& cmdline)
: SampleApexApplication(cmdline)
{
	m_simulationBudget			= 4096;
	m_apexParticleIosModule		= 0;
	m_apexBasicIosModule		= 0;
	m_apexNxFluidIosModule		= 0;
	m_apexEmitterModule			= 0;
	m_nbEmitterModuleScalables	= 0;
	m_emitterModuleScalables	= 0;
	m_apexIofxModule			= 0;

	m_apexImpactEmitterAsset	= 0;
	m_apexGroundEmitterAsset	= 0;

	m_sceneNumber				= BasicIosScene;

	m_actorAddSwitch			= false;
	m_attachGroundEmitterToCamera = true;

#if NX_SDK_VERSION_MAJOR == 2
	m_shapeGroupMask.bits0 = 7;
#endif

	m_commandLineInput.registerCommands(&SampleParticlesCommandLineInputs[0], SPCLIDS::COUNT);
	m_commandLineInput.registerCommand(SampleCommandLineInputs[SCLIDS::NO_APEX_CUDA]);
	m_commandLineInput.registerCommand(SampleCommandLineInputs[SCLIDS::NO_INTEROP]);
	if(hasInputCommand(SPCLIDS::EDIT))
	{
		m_CommandLineAsset = inputCommandValue(SPCLIDS::EDIT); 
		m_sceneNumber = CommandLineAsset;
	}
	else m_CommandLineAsset = NULL;
	for (unsigned benchmarkID = SPCLIDS::FIRST_BENCHMARK; benchmarkID <= SPCLIDS::LAST_BENCHMARK; ++benchmarkID)
	{
		m_benchmarkNames.push_back(benchmarkName(benchmarkID));
	}

	m_impactActorIdx			= 0xffffffff;
	m_groundEmitterActorIdx		= 0xffffffff;

	m_AppPtr = this;

}


void SimpleParticlesTestApplication::initCommonDebugRenderConfigs()
{	
	DebugRenderConfiguration config;

	config.flags.push_back(DebugRenderFlag("IOFX actor.", "Iofx/VISUALIZE_IOFX_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

	config.flags.push_back(DebugRenderFlag("Emitter actor.", "Emitter/apexEmitterParameters.VISUALIZE_APEX_EMITTER_ACTOR"));
	addDebugRenderConfig(config);
	config.flags.clear();

#if NX_SDK_VERSION_MAJOR == 2
	config.flags.push_back(DebugRenderFlag("Collision Shapes", NX_VISUALIZE_COLLISION_SHAPES, 1.0f));
#elif NX_SDK_VERSION_MAJOR == 3
	config.flags.push_back(DebugRenderFlag("Collision Shapes", physx::PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f));
#endif
	addDebugRenderConfig(config);
	config.flags.clear();
}


void SimpleParticlesTestApplication::setSceneEyePos()
{
	PxMat44 eyeTransform = PxMat44::createIdentity(); //= m_AppPtr->getEyeTransform();
	PxF32 coeff;
	if(m_sceneNumber == NxFluidIosScene || m_sceneNumber == BasicIosScene || m_sceneNumber == GroundEmitterScene)
	{
		eyeTransform.column3.y = 7;
		coeff = 25;
	}
	else if(m_sceneNumber == ImpactEmitterScene)
	{
		eyeTransform.column3.y = 6;
		coeff = 21;
	}
	else if(m_sceneNumber == AssetPreviewScene)
	{
		eyeTransform.column3.y = 8;
		coeff = 31;
	}
	else if(m_sceneNumber == ExplicitEmitterScene)
	{
		eyeTransform.column3.x = -8;
		eyeTransform.column3.y = 11;
		coeff = 30;
	}
	else if(m_sceneNumber == FigureScene)
	{
		eyeTransform.column3.x = -6;
		eyeTransform.column3.y = 21;
		coeff = 45;
	}
	else if(m_sceneNumber == StarScene)
	{
		eyeTransform.column3.x = -7;
		eyeTransform.column3.y = 15;
		coeff = 39;
	}
	else coeff = 30.0f;
	eyeTransform.column3.z = 1.0f * coeff; 	// set the eye position based on the number of actors there will be
	m_AppPtr->setEyeTransform(eyeTransform);
}


void SimpleParticlesTestApplication::initializeText()
{
	strcpy(m_windowString[0], "");
	strcpy(m_windowString[1], "");
	strcpy(m_windowString[2], "Keys +- adjust LOD budget, */ adjust scalable parameters");

	strcpy(m_windowString[3], "Use space bar to create collision box (shift + space for spheres, ctrl + space for convexes, ctrl + shift + space for halfspaces)");
	strcpy(m_windowString[4], "");
#if NX_SDK_VERSION_MAJOR == 2
	strcpy(m_windowString[5], "Press \"Z\" to change collision groups.");
#elif NX_SDK_VERSION_MAJOR == 3
	strcpy(m_windowString[5], "");
#endif

	if (cudaEnabled())
	{
		strcat(m_windowString[0], "CUDA IOFX");
		if (interopEnabled())
			strcat(m_windowString[WINDOW_SCENE_NAME_INDEX], " + Interop");
	}
	else
	{
		strcat(m_windowString[0], "CPU IOFX");
	}

	updateCollisionObjectSceneInfo();
}

void SimpleParticlesTestApplication::updateCollisionObjectSceneInfo()
{
	char str[MAX_DEBUG_TEXT_STRING];
	sprintf(str, "Collision object radius (shift with +- to adjust): %0.1f", m_collisionObjRadius);
	strncpy(m_windowString[4], str, MAX_DEBUG_TEXT_STRING);
}

void SimpleParticlesTestApplication::setSceneString(const char* str)
{
	strncpy(m_windowString[1], str, MAX_DEBUG_TEXT_STRING);
}

void SimpleParticlesTestApplication::setScalableInfoString(const char* str)
{
	strncpy(m_windowString[2], str, MAX_DEBUG_TEXT_STRING);
}

void SimpleParticlesTestApplication::updateScalableAndLodInfo()
{
	if (m_apexEmitterModule)
	{
		char str[MAX_DEBUG_TEXT_STRING];
		sprintf(str, "LOD: %d, Scalables: ", (physx::PxI32)(m_simulationBudget));

		for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
		{
			char pStr[MAX_DEBUG_TEXT_STRING];
			NxApexParameter& p = *m_emitterModuleScalables[i];
			sprintf(pStr, "%s: %u (%u-%u) ", p.name, p.current, p.range.minimum, p.range.maximum);
			strcat(str, pStr);
		}
		setScalableInfoString(str);
	}
}

// Process the command line args that need to be processed before the window is open
void SimpleParticlesTestApplication::processEarlyCmdLineArgs(void)
{
	SampleApexApplication::initBenchmarks(&benchmarkStart);
	SampleApexApplication::processEarlyCmdLineArgs();
}

SimpleParticlesTestApplication::~SimpleParticlesTestApplication(void)
{
	PX_ASSERT(!m_apexParticleIosModule);
	PX_ASSERT(!m_apexBasicIosModule);
	PX_ASSERT(!m_apexNxFluidIosModule);
	PX_ASSERT(!m_apexEmitterModule);
	PX_ASSERT(!m_apexIofxModule);
	PX_ASSERT(m_renderVolumeList.size() == 0);
	PX_ASSERT(!m_lights.size());
	PX_ASSERT(!m_actors.size());
}

unsigned int SimpleParticlesTestApplication::getNumScenes() const
{
	return NumScenes;
}

unsigned int SimpleParticlesTestApplication::getSelectedScene() const 
{
	return m_sceneNumber;
}

const char* SimpleParticlesTestApplication::getSceneName(unsigned int sceneNumber) const 
{
	if (sceneNumber > 0 && sceneNumber < NumScenes)
	{
		return SampleParticlesCommandLineInputs[sceneNumber - 1].mShortName;
	}
	else if (sceneNumber == 0) return "Command line scene";

	return NULL;
}

void SimpleParticlesTestApplication::selectScene(unsigned int sceneNumber)
{
	releaseAssetsAndActors(m_sceneNumber);
	loadAssetsAndActors((SceneSelection)sceneNumber);
}

void SimpleParticlesTestApplication::benchmarkStart(const char* benchmarkName)
{
	PxU32 benchmarkNum;

	m_AppPtr->setBenchmarkEnable(true);
	if (m_AppPtr->m_OaInterface != NULL)
	{
		// get the benchmark parameters from openAutomate
		m_AppPtr->m_benchmarkNumFrames = m_AppPtr->m_OaInterface->getBenchmarkNumFrames();
		m_AppPtr->m_benchmarkNumActors = m_AppPtr->m_OaInterface->getBenchmarkNumActors();
		m_AppPtr->m_CmdLineBenchmarkMode = false;
	}
	else
	{
		m_AppPtr->m_CmdLineBenchmarkMode = true;
	}

	if (m_AppPtr->m_benchmarkNumFrames < 1000)
	{
		m_AppPtr->m_benchmarkNumFrames = 1000;	// run the benchmark for at least 1000 frames
	}
	m_AppPtr->m_FrameNumber = 0;
	m_AppPtr->m_BenchMarkTimer.getElapsedSeconds();
	m_AppPtr->m_BenchmarkElapsedTime = 0.0;
	m_AppPtr->m_dataCollectionStarted = true;
	m_AppPtr->releaseAssetsAndActors();
	for (physx::PxU32 i = 0; i < m_AppPtr->m_benchmarkNames.size(); i++)
	{
		if (physx::string::stricmp(m_AppPtr->m_benchmarkNames[i], benchmarkName) == 0)
		{
			benchmarkNum = i;
			m_AppPtr->loadAssetsAndActors((SceneSelection)(benchmarkNum + 1));
			break;
		}
	}
	oaStartBenchmark();
}

// called just AFTER the window opens.
void SimpleParticlesTestApplication::onInit(void)
{
	sampleInit("SimpleParticlesTest", 
		cudaSupported()    && !hasInputCommand(SCLIDS::NO_APEX_CUDA), 
		interopSupported() && !hasInputCommand(SCLIDS::NO_INTEROP),
		1024*1024*512  //gpuTotalMemLimit = 512 Mb
		);
	m_apexScene = createSampleScene(PxVec3(0, -9.8f, 0));

#if APEX_MODULES_STATIC_LINK
	instantiateModuleBasicIos();
	instantiateModuleBasicIOSLegacy();
	instantiateModuleEmitter();
	instantiateModuleEmitterLegacy();
	instantiateModuleIofx();
	instantiateModuleIOFXLegacy();
	instantiateModuleExampleFS();
	instantiateModuleFieldSampler();
	instantiateModuleCommonLegacy();
	instantiateModuleFrameworkLegacy();
#	if NX_SDK_VERSION_MAJOR == 2
	instantiateModuleExplosion();
	instantiateModuleFluidIos();
#	elif NX_SDK_VERSION_MAJOR == 3
	instantiateModuleParticleIos();
#	endif
#endif

	m_apexBasicIosModule = static_cast<NxModuleBasicIos*>(m_apexSDK->createModule("BasicIOS"));
	PX_ASSERT(m_apexBasicIosModule);
	if (m_apexBasicIosModule)
	{
		NxParameterized::Interface* params = m_apexBasicIosModule->getDefaultModuleDesc();
		m_apexBasicIosModule->init(*params);
		m_apexBasicIosModule->setLODBenefitValue(10000.0f);
	}

#if NX_SDK_VERSION_MAJOR == 2
	m_apexNxFluidIosModule = static_cast<NxModuleFluidIos*>(m_apexSDK->createModule("NxFluidIOS"));
	PX_ASSERT(m_apexNxFluidIosModule);
	if (m_apexNxFluidIosModule)
	{
		NxParameterized::Interface* params = m_apexNxFluidIosModule->getDefaultModuleDesc();
		m_apexNxFluidIosModule->init(*params);
	}
#elif NX_SDK_VERSION_MAJOR == 3
	m_apexParticleIosModule = static_cast<NxModuleParticleIos*>(m_apexSDK->createModule("ParticleIOS"));
	PX_ASSERT(m_apexParticleIosModule);
	if (m_apexParticleIosModule)
	{
		NxParameterized::Interface* params = m_apexParticleIosModule->getDefaultModuleDesc();
		m_apexParticleIosModule->init(*params);
	}
#endif

	m_apexEmitterModule = static_cast<NxModuleEmitter*>(m_apexSDK->createModule("Emitter"));
	PX_ASSERT(m_apexEmitterModule);
	if (m_apexEmitterModule)
	{
		NxParameterized::Interface* params = m_apexEmitterModule->getDefaultModuleDesc();
		m_apexEmitterModule->init(*params);
		m_nbEmitterModuleScalables = m_apexEmitterModule->getNbParameters();
		m_emitterModuleScalables = m_apexEmitterModule->getParameters();
		for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
		{
			NxApexParameter& p = *m_emitterModuleScalables[i];
			m_apexEmitterModule->setIntValue(i, p.range.maximum);
		}
		//updateScalableAndLodInfo();
	}

	m_apexIofxModule = static_cast<NxModuleIofx*>(m_apexSDK->createModule("IOFX"));
	PX_ASSERT(m_apexIofxModule);
	if (m_apexIofxModule)
	{
		NxParameterized::Interface* params = m_apexIofxModule->getDefaultModuleDesc();
		m_apexIofxModule->init(*params);
	}

#if NX_SDK_VERSION_MAJOR == 2
	loadModule("Explosion");
#endif

	loadModule("FieldSampler");
	loadModule("ExampleFS");

	loadModule("Common_Legacy");
	loadModule("Framework_Legacy");
	loadModule("BasicIOS_Legacy");
	loadModule("Emitter_Legacy");
	loadModule("IOFX_Legacy");

	if (!m_apexBasicIosModule && !m_apexIofxModule)
	{
		return;
	}

	if (interopSupported())
	{
		if (!m_cudaContext &&
			m_errorCallback &&
			!hasInputCommand(SCLIDS::NO_APEX_CUDA) &&
			!hasInputCommand(SCLIDS::NO_INTEROP))
		{
			char buf[256];
			sprintf(buf, "No GPU Dispatcher created, CUDA will not be used within APEX\n");
			m_errorCallback->reportError(physx::PxErrorCode::eDEBUG_INFO, buf, __FILE__, __LINE__);
		}
	}

	m_resourceCallback->setApexSupport(*m_apexSDK);

	initializeText();

#if NX_SDK_VERSION_MAJOR == 2
	NxGroupsMask g;
	g.bits1 = g.bits2 = g.bits3 = 0;
	g.bits0 = 1;
	m_resourceCallback->registerNxGroupsMask128("MESH_COL_GRP_MASK", g);
	g.bits0 = 1;
	m_resourceCallback->registerNxGroupsMask128("SPRITE_COL_GRP_MASK", g);
	g.bits0 = 3;
	m_resourceCallback->registerNxGroupsMask128("ALL_COL_GRP_MASK", g);
	m_resourceCallback->registerNxCollisionGroup("MESH_COL_GRP_MASK", 0);
	m_resourceCallback->registerNxCollisionGroup("SPRITE_COL_GRP_MASK", 1);

	m_physxSDK->setParameter(NX_VISUALIZATION_SCALE,        0);
#if ENABLE_SDK_VISUALIZATIONS
	m_physxSDK->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_POSITION,   1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_PACKETS,    1);
	m_physxSDK->setParameter(NX_VISUALIZE_FLUID_BOUNDS,     1);
	m_physxSDK->setParameter(NX_VISUALIZE_FORCE_FIELDS,     1);
#endif
#elif NX_SDK_VERSION_MAJOR == 3
	PxFilterData g;
	g.word1 = g.word2 = g.word3 = 0;
	g.word0 = 1;
	m_resourceCallback->registerSimulationFilterData("MESH_COL_GRP_MASK", g);
	g.word0 = 1;
	m_resourceCallback->registerSimulationFilterData("SPRITE_COL_GRP_MASK", g);
	g.word0 = 3;
	m_resourceCallback->registerSimulationFilterData("ALL_COL_GRP_MASK", g);

	m_apexScene->getPhysXScene()->setVisualizationParameter(PxVisualizationParameter::eSCALE, 0);
#if ENABLE_SDK_VISUALIZATIONS
	m_apexScene->getPhysXScene()->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES, 1);
#endif
#endif

	for (physx::PxU32 i = 0; i < m_benchmarkNames.size(); i++)
	{
		if (m_cmdline.hasSwitch(m_benchmarkNames[i]))
		{
			if (m_CmdLineBenchmarkMode == false)
			{
				// only 1 benchmark can be run from the command line.
				// OA mode can command sequential bench marks...
				benchmarkStart(m_benchmarkNames[i]);
			}
		}
	}
	if (!m_BenchmarkMode)
	{
		loadAssetsAndActors(m_sceneNumber);
	}

	// NOTE: Only one light here for now, the second light halved the framerate.
	SampleRenderer::RendererDirectionalLightDesc lightdesc;

	lightdesc.color     = SampleRenderer::RendererColor(100, 100, 50, 255);
	lightdesc.intensity = 1;
	lightdesc.direction = physx::PxVec3(0, -0.707f, -0.707f);
	m_lights.push_back(getRenderer()->createLight(lightdesc));

	const physx::PxReal boundsHalf = 1000.0f;
	physx::PxBounds3 b0 = physx::PxBounds3::boundsOfPoints(physx::PxVec3(0.0f, -boundsHalf, boundsHalf),
		physx::PxVec3(boundsHalf, boundsHalf, -boundsHalf));
	m_renderVolumeList.push_back(m_apexIofxModule->createRenderVolume(*m_apexScene, b0, 0, true));

	physx::PxBounds3 b1 = physx::PxBounds3::boundsOfPoints(physx::PxVec3(0.0f, -boundsHalf, boundsHalf),
		physx::PxVec3(-boundsHalf, boundsHalf, -boundsHalf));
	m_renderVolumeList.push_back(m_apexIofxModule->createRenderVolume(*m_apexScene, b1, 0, true));

	m_apexScene->allocViewMatrix(ViewMatrixType::LOOK_AT_RH);
	m_apexScene->allocProjMatrix(ProjMatrixType::USER_CUSTOMIZED);

	mEditIndex = 0xFFFFFFFF;
	mApexParticleEditor = createApexParticleEditor();
}

void SimpleParticlesTestApplication::loadAssetsAndActors(SceneSelection sceneNumber)
{
	if (sceneNumber >= NumScenes)
	{
		return;
	}

	m_sceneNumber = sceneNumber;
	setSceneEyePos();

	if (!m_apexScene || !m_apexSDK || !m_apexEmitterModule)
	{
		return;
	}

	if (sceneNumber != ImpactEmitterScene && sceneNumber != GroundEmitterScene && sceneNumber != AssetPreviewScene)
	{
		initCommonDebugRenderConfigs();
	}

	if (sceneNumber <= NxFluidIosScene)
	{
		const char* meshEmitterName = 0;
		const char* spriteEmitterName = 0;

		DebugRenderConfiguration config;
		config.flags.push_back(DebugRenderFlag("LOD benefits.", "VISUALIZE_LOD_BENEFITS"));
		addDebugRenderConfig(config);
		config.flags.clear();

		if (sceneNumber == NxFluidIosScene)
		{
#if NX_SDK_VERSION_MAJOR == 2
			meshEmitterName		= "testMeshEmitter4FluidIos";
			spriteEmitterName	= "testSpriteEmitter4FluidIos"; //sprites - green particles
			setSceneString("NxFluid IOS scene featuring a mesh and sprite emitter");
#elif NX_SDK_VERSION_MAJOR == 3
			meshEmitterName		= "testMeshEmitter4ParticleIos";
			spriteEmitterName	= "testMeshEmitter4ParticleFluidIos";
			setSceneString("Particle IOS with mesh and sprite emitters");
#endif
			// Make a ground plane
			addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
		}
		else if (sceneNumber == BasicIosScene)
		{
			meshEmitterName		= "testMeshEmitter4BasicIos";
			spriteEmitterName	= "testSpriteEmitter4BasicIos";
			setSceneString("Basic IOS with mesh and sprite emitters");

			// Make a ground plane
			addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
		}
		else
		{
			meshEmitterName		= NULL;
			spriteEmitterName	= m_CommandLineAsset;
			setSceneString("Command Line Scene");
		}

		//meshEmitterName = 0;
		if (meshEmitterName)
		{
			NxApexAsset* asset = loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, meshEmitterName);
			m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(asset));
		}

		//spriteEmitterName = 0;
		if (spriteEmitterName)
		{
			NxApexAsset* asset = loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, spriteEmitterName);
			m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(asset));
		}

		m_apexFieldSamplerAssetList.push_back(loadApexAsset("ExampleFSAsset", "testExampleFS"));

		// test forceload
		forceLoadAssets();

		for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
		{
			m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[i], *m_apexScene, i));
		}
		for (physx::PxU32 i = 0; i < m_apexFieldSamplerAssetList.size(); i++)
		{
			for (physx::PxU32 j = 0 ; j < 2 ; j++)
			{
				m_actors.push_back(new SampleFieldSamplerActor(*m_apexFieldSamplerAssetList[i], *m_apexScene, j));
			}
		}
	}
	else if (sceneNumber == ImpactEmitterScene)
	{
		DebugRenderConfiguration config;
		config.flags.push_back(DebugRenderFlag("IOFX actor.", "Iofx/VISUALIZE_IOFX_ACTOR"));
		addDebugRenderConfig(config);
		config.flags.clear();

#if NX_SDK_VERSION_MAJOR == 2
		NxApexAsset* asset = loadApexAsset(NX_IMPACT_EMITTER_AUTHORING_TYPE_NAME, "testImpactEmitter");
#elif NX_SDK_VERSION_MAJOR == 3
		NxApexAsset* asset = loadApexAsset(NX_IMPACT_EMITTER_AUTHORING_TYPE_NAME, "testImpactEmitterPhysX3");
#endif
		m_apexImpactEmitterAsset = static_cast<NxImpactEmitterAsset*>(asset);

		m_actors.push_back(new SampleImpactEmitterActor(*m_apexImpactEmitterAsset, *m_apexScene));
		m_impactActorIdx = m_actors.size() - 1;

		// Make a ground plane
		addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));
		setSceneString("Impact mesh emitter (right click to spawn)");	
	}
	else if (sceneNumber == GroundEmitterScene)
	{
		DebugRenderConfiguration config;

		config.flags.push_back(DebugRenderFlag("IOFX actor.", "Iofx/VISUALIZE_IOFX_ACTOR"));
		config.flags.push_back(DebugRenderFlag("Ground emitter actor.", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_ACTOR"));
		config.flags.push_back(DebugRenderFlag("Raycast", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_RAYCAST"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Ground emitter actor.", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_ACTOR"));
		config.flags.push_back(DebugRenderFlag("Grid", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_GRID"));
		config.flags.push_back(DebugRenderFlag("Raycast", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_RAYCAST"));
		addDebugRenderConfig(config);
		config.flags.clear();

		config.flags.push_back(DebugRenderFlag("Ground emitter actor.", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_ACTOR"));
		config.flags.push_back(DebugRenderFlag("Actor pose", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_ACTOR_POSE"));
		config.flags.push_back(DebugRenderFlag("Actor name", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_ACTOR_NAME"));
		config.flags.push_back(DebugRenderFlag("Raycast", "Emitter/groundEmitterParameters.VISUALIZE_GROUND_EMITTER_RAYCAST"));
		addDebugRenderConfig(config);
		config.flags.clear();
		
		// Make a ground plane
		addCollisionShape(HalfSpaceShapeType, PxVec3(0, 0, 0), PxVec3(0, 1, 0), PxVec3(0, 0, 0));

#if NX_SDK_VERSION_MAJOR == 2
		m_shape_actors.back()->getPhysXActor()->getShapes()[0]->setGroup(GROUND_PLANE_COL_GROUP);

		// Seed the ground plane's NxMaterialIndex in the NRP for the Ground Emitter
		m_resourceCallback->registerPhysicalMaterial("GroundPlaneMaterial", GROUND_PLANE_MATERIAL_INDEX);

		// Seed the ground emitter's raycast group mask name
		NxGroupsMask g;
		g.bits0 = 7;
		g.bits1 = g.bits2 = g.bits3 = 0;
		m_resourceCallback->registerNxGroupsMask128("GroundEmitterRaycastGroupBitMask", g);
		m_resourceCallback->registerNxCollisionGroupsMask("GroundEmitterRaycastGroupBitMask", (1 << GROUND_PLANE_COL_GROUP));
		NxApexAsset* asset = loadApexAsset(NX_GROUND_EMITTER_AUTHORING_TYPE_NAME, "testGroundEmitter");
#elif NX_SDK_VERSION_MAJOR == 3
		// Seed the ground plane's NxMaterialIndex in the NRP for the Ground Emitter
		m_resourceCallback->registerPhysicalMaterial("GroundPlaneMaterial", GROUND_PLANE_MATERIAL_INDEX);

		// Seed the ground emitter's raycast group mask name
		PxFilterData g;
		g.word0 = 7;
		g.word1 = g.word2 = g.word3 = 0;
		m_resourceCallback->registerSimulationFilterData("GroundEmitterRaycastGroupBitMask", g);
		NxApexAsset* asset = loadApexAsset(NX_GROUND_EMITTER_AUTHORING_TYPE_NAME, "testGroundEmitterPhysX3");
#endif

		m_apexGroundEmitterAsset = static_cast<NxGroundEmitterAsset*>(asset);

		m_actors.push_back(new SampleGroundEmitterActor(*m_apexGroundEmitterAsset, *m_apexScene, getEyeTransform().getPosition()));
		m_groundEmitterActorIdx = m_actors.size() - 1;

		setSceneString("Ground mesh emitter.  If you move around you'll see there are always particles on the ground.");
	}
	else if (sceneNumber == ExplicitEmitterScene)
	{
		// This scene is LOD-hungry
		if (m_simulationBudget < 15000)
		{
			setBudget(15000);
		}

		NxApexAsset* asset = loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "testExplicitEmitter");
		m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(asset));

		// test forceload
		forceLoadAssets();

		for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
		{
			m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[i], *m_apexScene, i));
		}

		setSceneString("Explicit mesh emitter");
	}
	else if (sceneNumber == FigureScene)
	{
		// This scene is LOD-hungry
		if (m_simulationBudget < 50000)
		{
			setBudget(50000);
		}

		NxApexAsset* asset = loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "testFigureEmitter");
		m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(asset));

		// test forceload
		forceLoadAssets();

		for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
		{
			m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[i], *m_apexScene, i));
		}

		setSceneString("Explicit emitter with complex shape");
	}
	else if (sceneNumber == StarScene)
	{
		// This scene is LOD-hungry
		if (m_simulationBudget < 50000)
		{
			setBudget(50000);
		}

		NxApexAsset* asset = loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "testStarEmitter");
		m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(asset));

		// test forceload
		forceLoadAssets();

		for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
		{
			m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[i], *m_apexScene, i));
		}

		setSceneString("Explicit emitter with shape intersection");
	}
	else if (sceneNumber == AssetPreviewScene)
	{
		DebugRenderConfiguration config;
		config.flags.push_back(DebugRenderFlag("IOFX actor.", "Iofx/VISUALIZE_IOFX_ACTOR"));
		addDebugRenderConfig(config);
		config.flags.clear();

		NxParameterized::Interface* p = NULL;
		NxApexAsset* asset = NULL;
		physx::PxMat44 pose = physx::PxMat44::createIdentity();

		// load an APEX ground emitter for preview
		{
#if NX_SDK_VERSION_MAJOR == 2
			// Seed the ground plane's NxMaterialIndex in the NRP for the Ground Emitter
			m_resourceCallback->registerPhysicalMaterial("GroundPlaneMaterial", GROUND_PLANE_MATERIAL_INDEX);

			// Seed the ground emitter's raycast group mask name
			NxGroupsMask g;
			g.bits0 = 7;
			g.bits1 = g.bits2 = g.bits3 = 0;
			m_resourceCallback->registerNxGroupsMask128("GroundEmitterRaycastGroupBitMask", g);
			m_resourceCallback->registerNxCollisionGroupsMask("GroundEmitterRaycastGroupBitMask", (1 << GROUND_PLANE_COL_GROUP));
#elif NX_SDK_VERSION_MAJOR == 3
			// Seed the ground plane's NxMaterialIndex in the NRP for the Ground Emitter
			m_resourceCallback->registerPhysicalMaterial("GroundPlaneMaterial", GROUND_PLANE_MATERIAL_INDEX);

			// Seed the ground emitter's raycast group mask name
			PxFilterData g;
			g.word0 = 7;
			g.word1 = g.word2 = g.word3 = 0;
			m_resourceCallback->registerSimulationFilterData("GroundEmitterRaycastGroupBitMask", g);
#endif
			asset = loadApexAsset(NX_GROUND_EMITTER_AUTHORING_TYPE_NAME, "testGroundEmitter");
			m_apexGroundEmitterAsset = static_cast<NxGroundEmitterAsset*>(asset);

			p = m_apexGroundEmitterAsset->getDefaultAssetPreviewDesc();
			PX_ASSERT(p);
			m_apexPreviewList.push_back(m_apexGroundEmitterAsset->createApexAssetPreview(*p));
		}

		// load an APEX shaped emitter for preview
		{
			asset = loadApexAsset(NX_APEX_EMITTER_AUTHORING_TYPE_NAME, "testMeshEmitter4FluidIos");
			m_apexEmitterAssetList.push_back(static_cast<NxApexEmitterAsset*>(asset));
			p = m_apexEmitterAssetList.back()->getDefaultAssetPreviewDesc();
			PX_ASSERT(p);

			pose.setPosition(physx::PxVec3(0.0f, 10.0f, 0.0f));
			NxParameterized::setParamMat44(*p, "pose", pose);

			m_apexPreviewList.push_back(m_apexEmitterAssetList.back()->createApexAssetPreview(*p));
		}

		setSceneString("Asset preview scene");
	}
	else
	{
		setSceneString("");
	}
}

void SimpleParticlesTestApplication::releaseAssetsAndActors(SceneSelection sceneNumber)
{
	clearDebugRenderConfig();
	m_actorAddSwitch = false;

	if (sceneNumber >= NumScenes)
	{
		return;
	}

	for (physx::PxU32 i = 0; i < m_actors.size(); i++)
	{
		if (m_actors[i])
		{
			m_actors[i]->release();
		}
	}
	m_actors.clear();

	// This allows us to change scenes while the edit window is still open
	if (mApexParticleEditor)
	{
		mApexParticleEditor->releaseEditApexActor();
		mEditIndex = 0xFFFFFFFF;
	}

	for (physx::PxU32 i = 0; i < m_apexFieldSamplerAssetList.size(); i++)
	{
		releaseApexAsset(m_apexFieldSamplerAssetList[i]);
	}
	m_apexFieldSamplerAssetList.clear();

	for (physx::PxU32 i = 0; i < m_apexPreviewList.size(); i++)
	{
		m_apexPreviewList[i]->release();
	}
	m_apexPreviewList.clear();

	for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
	{
		releaseApexAsset(m_apexEmitterAssetList[i]);
	}
	m_apexEmitterAssetList.clear();

	if (m_apexImpactEmitterAsset)
	{
		releaseApexAsset(m_apexImpactEmitterAsset);
		m_apexImpactEmitterAsset = 0;
		m_impactActorIdx = 0xffffffff;
	}
	if (m_apexGroundEmitterAsset)
	{
		releaseApexAsset(m_apexGroundEmitterAsset);
		m_apexGroundEmitterAsset = 0;
		m_groundEmitterActorIdx = 0xffffffff;
	}

	for (physx::PxU32 i = 0; i < m_shape_actors.size(); i++)
	{
		if (m_shape_actors[i])
		{
			m_shape_actors[i]->release();
		}
	}
	m_shape_actors.clear();
}

// called just BEFORE the window closes. return 'true' to confirm the window closure.
void SimpleParticlesTestApplication::onShutdown(void)
{
	if (mApexParticleEditor)
	{
		mApexParticleEditor->release();
		mApexParticleEditor = NULL;
	}

	SampleFramework::SampleAssetManager* assetManager = getAssetManager();
	PX_ASSERT(assetManager);
	PX_FORCE_PARAMETER_REFERENCE(assetManager);

	releaseAssetsAndActors();

	for (physx::PxU32 i = 0; i < m_lights.size(); i++)
	{
		m_lights[i]->release();
	}
	m_lights.clear();

	if (m_renderVolumeList.size())
	{
		for (physx::PxU32 i = 0; i < m_renderVolumeList.size(); i++)
		{
			if (m_renderVolumeList[i])
			{
				m_apexIofxModule->releaseRenderVolume(*m_renderVolumeList[i]);
				m_renderVolumeList[i] = 0;
			}
		}
		m_renderVolumeList.clear();
	}

	if (m_apexIofxModule)
	{
		m_apexSDK->releaseModule(m_apexIofxModule);
		m_apexIofxModule = 0;
	}

	if (m_apexBasicIosModule)
	{
		m_apexSDK->releaseModule(m_apexBasicIosModule);
		m_apexBasicIosModule = 0;
	}

	if (m_apexEmitterModule)
	{
		m_apexSDK->releaseModule(m_apexEmitterModule);
		m_apexEmitterModule = 0;
	}

	if (m_apexNxFluidIosModule)
	{
		m_apexSDK->releaseModule(m_apexNxFluidIosModule);
		m_apexNxFluidIosModule = 0;
	}

	if (m_apexParticleIosModule)
	{
		m_apexSDK->releaseModule(m_apexParticleIosModule);
		m_apexParticleIosModule = 0;
	}

	m_debugRenderConfigs.clear();
	SampleApexApplication::onShutdown();
}

void SimpleParticlesTestApplication::onTickPreRender(float dtime)
{
	//printApexStats( m_apexRenderDebug, m_apexScene, !m_pause, 0.15f, 0.06f, physx::PxVec3(-0.98f, 0.85f, 0.0f) );

	if (m_groundEmitterActorIdx != 0xffffffff &&
		m_actors[m_groundEmitterActorIdx] &&
		m_attachGroundEmitterToCamera)
	{
		SampleGroundEmitterActor* a = DYNAMIC_CAST(SampleGroundEmitterActor*)(m_actors[ m_groundEmitterActorIdx ]);
		a->updatePosition(getEyeTransform().getPosition());
	}


	updateCollisionShapes(dtime);
	for (PxU32 i = 0; i < m_shape_actors.size(); i++)
	{
		m_shape_actors[i]->tick(dtime);
	}

	if (m_apexScene && !m_pause)
	{
		m_apexScene->simulate(dtime);
	}

	SampleApexApplication::onTickPreRender(dtime);
}

// called when the window's contents needs to be redrawn.
void SimpleParticlesTestApplication::onRender(void)
{
	SampleRenderer::Renderer* renderer = getRenderer();
	if (renderer)
	{
		physx::PxU32 windowWidth  = 0;
		physx::PxU32 windowHeight = 0;
		renderer->getWindowSize(windowWidth, windowHeight);
		if (windowWidth > 0 && windowHeight > 0)
		{
			renderer->clearBuffers();

			m_projection = SampleRenderer::RendererProjection(45.0f, windowWidth / (float)windowHeight, 0.1f, 10000.0f);

			// The APEX scene needs the view and projection matrices for LOD, IOFX modifiers, and other things
			updateApexSceneMatrices();

			for (physx::PxU32 i = 0; i < m_lights.size(); i++)
			{
				renderer->queueLightForRender(*m_lights[i]);
			}

			for (physx::PxU32 i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i])
				{
					m_actors[i]->render();
				}
			}

			for (PxU32 i = 0; i < m_shape_actors.size(); i++)
			{
				if (m_shape_actors[i])
				{
					m_shape_actors[i]->render();
				}
			}

			for (physx::PxU32 i = 0; i < m_apexPreviewList.size(); i++)
			{
				if (m_apexPreviewList[i])
				{
					SampleApexRenderer apexRenderer;
					m_apexPreviewList[i]->lockRenderResources();
					m_apexPreviewList[i]->updateRenderResources();
					m_apexPreviewList[i]->dispatchRenderResources(apexRenderer);
					m_apexPreviewList[i]->unlockRenderResources();
				}
			}

			if (m_apexScene)
			{
				SampleApexRenderer apexRenderer;

				if (m_apexRenderDebug)
				{
					m_apexRenderDebug->lockRenderResources();
					m_apexRenderDebug->updateRenderResources(0);
					m_apexRenderDebug->unlockRenderResources();
					m_apexRenderDebug->dispatchRenderResources(apexRenderer);
				}

				m_apexScene->lockRenderResources();
				m_apexScene->updateRenderResources();
				m_apexScene->dispatchRenderResources(apexRenderer);
				m_apexScene->unlockRenderResources();
			}

			if (m_renderVolumeList.size() && m_apexScene)
			{
#if !defined(DOUBLE_BUFFER_INTEROP)
				m_apexScene->prepareRenderResourceContexts();
#endif
				SampleApexRenderer apexRenderer;
				for (physx::PxU32 i = 0; i < m_renderVolumeList.size(); i++)
				{
					physx::PxU32 numActors;
					physx::PxU32 drawnParticles = 0;

					m_renderVolumeList[i]->lockRenderResources();
					NxIofxActor* const* actors = m_renderVolumeList[i]->getIofxActorList(numActors);
					for (physx::PxU32 j = 0 ; j < numActors ; j++)
					{
						actors[j]->lockRenderResources();
						if (!actors[j]->getBounds().isEmpty())
						{
							actors[j]->updateRenderResources(m_rewriteBuffers);
							actors[j]->dispatchRenderResources(apexRenderer);
							drawnParticles += actors[j]->getObjectCount();
						}
						actors[j]->unlockRenderResources();
					}
					m_renderVolumeList[i]->unlockRenderResources();
				}
			}

			renderer->render(getEyeTransform(), m_projection);

			///////////////////////////////////////////////////////////////////////////

			std::string sceneInfo;
			for (int i = WINDOW_SCENE_INFO_INDEX; i < NUM_WINDOW_STRINGS; ++i)
			{
				if (!empty(m_windowString[i]))
				{
					sceneInfo.append(m_windowString[i]);
					sceneInfo.append("\n");
				}
			}
			printSceneInfo(m_windowString[WINDOW_SCENE_NAME_INDEX], sceneInfo.c_str());
		}
	}
}

void SimpleParticlesTestApplication::onTickPostRender(float dtime)
{
	if (m_apexScene && !m_pause)
	{
		physx::PxU32 errorState = 0;
		m_apexScene->fetchResults(true, &errorState);

		if (mApexParticleEditor && mEditIndex < m_actors.size())
		{
			bool exitApp = false;
			SampleApexEmitterActor* act = static_cast<SampleApexEmitterActor*>(m_actors[mEditIndex]);
			NxApexEmitterActor* old_actor = static_cast<NxApexEmitterActor*>(act->getActor());
			NxApexEmitterAsset* old_asset = static_cast<NxApexEmitterAsset*>(old_actor->getOwner());
			NxApexEmitterActor* actor = static_cast<NxApexEmitterActor*>(mApexParticleEditor->pump(exitApp));
			act->setActor(actor);
			NxApexEmitterAsset* new_asset = mApexParticleEditor->getApexEmitterAsset();
			for (physx::PxU32 i = 0; i < m_apexEmitterAssetList.size(); i++)
			{
				if (m_apexEmitterAssetList[i] == old_asset)
				{
					m_apexEmitterAssetList[i] = new_asset;
					break;
				}
			}

			if (exitApp)
			{
				mApexParticleEditor->releaseEditApexActor();
				mEditIndex = 0xFFFFFFFF;
//				RendererWindow::close();
			}
		}

#if defined(DOUBLE_BUFFER_INTEROP)
		m_apexScene->prepareRenderResourceContexts();
#endif

#if NX_SDK_VERSION_MAJOR == 2
		const NxDebugRenderable* debugRenderable = m_apexScene->getPhysXScene()->getDebugRenderable();
		if (debugRenderable && m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(*debugRenderable);
		}
		const NxDebugRenderable* apexDebugRenderable = m_apexScene->getDebugRenderable();
		if (apexDebugRenderable && m_apexRenderDebug)
		{
			m_apexRenderDebug->addDebugRenderable(*apexDebugRenderable);
		}
#elif NX_SDK_VERSION_MAJOR == 3
		const physx::PxRenderBuffer& renderBuffer = m_apexScene->getPhysXScene()->getRenderBuffer();
		if (m_apexRenderDebug != NULL)
		{
			m_apexRenderDebug->addDebugRenderable(renderBuffer);
		}
#endif
	}

	// call the Open Automate handler...
	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::IDLE))
	{
		m_OaInterface->OAHandler();
	}

	if ((m_OaInterface != NULL) && (m_OaInterface->getOaState() == OAState::EXIT))
	{
		close();
	}

	//if (in command line benchmarking) or (in OA benchmarking...)
	if (m_BenchmarkMode)
	{
		// if this is the ImpactEmitter benchmark create some impacts!
		if (m_sceneNumber == ImpactEmitterScene)
		{
			PxU32 framesBetweenImpacts = m_benchmarkNumFrames / (m_benchmarkNumActors + 1);
			// create impacts from the left to the right side of the window
			if ((m_FrameNumber != 0) && ((m_FrameNumber % framesBetweenImpacts) == 0))
			{
				const PxU32 actorSpacing = (730 - 160) / (m_benchmarkNumActors + 1);
				const PxU32 impactNum = m_FrameNumber / (framesBetweenImpacts + 1);
				const PxU32 impactX = 160 + (impactNum * actorSpacing);
				const PxU32 impactY = 230;
				SampleFramework::InputEvent ie(MOUSE_SELECTION_BUTTON, "MOUSE_SELECTION_BUTTON");
				onPointerInputEvent(ie, impactX, impactY, 0, 0);
			}
		}
		// are there still frames to run?
		if (m_FrameNumber <= m_benchmarkNumFrames)
		{
			SampleApexApplication::reportOaResults();
			m_FrameNumber++;
		}
		else  // no more frames to run.  benchmark is done
		{
			if (m_CmdLineBenchmarkMode) // close it down and exit the program
			{
				close();
			}
			else
			{
				if (m_OaInterface->getOaState() == OAState::BENCHMARKING)
				{
					//done with the benchmark
					oaValue v;
					v.Float = m_BenchmarkElapsedTime;
					m_OaInterface->setOaState(OAState::IDLE);
					oaAddResultValue("Total run time", OA_TYPE_FLOAT, &v);
					m_OaInterface->endBenchmark();
					// release the actors created by this benchmark in case OA runs another benchmark without exiting!
					m_groundEmitterActorIdx = 0xffffffff;
					releaseAssetsAndActors(m_sceneNumber);
				}
			}
		}
	}

	SampleApexApplication::onTickPostRender(dtime);
}

void SimpleParticlesTestApplication::collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents)
{
	using namespace SampleFramework;

	std::vector<const char*> inputDescriptions;
	collectInputDescriptions(inputDescriptions);

	DIGITAL_INPUT_EVENT_DEF_2(EDIT_PARTICLE_SYSTEM,	WKEY_Y,			XKEY_Y,			PS3KEY_Y,		AKEY_UNKNOWN,	OSXKEY_Y,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Y);
	DIGITAL_INPUT_EVENT_DEF_2(SAVE_PARTICLE_SYSTEM,	WKEY_X,			XKEY_X,			PS3KEY_X,		AKEY_UNKNOWN,	OSXKEY_X,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_X);

	DIGITAL_INPUT_EVENT_DEF_2(SCENE_SELECT,			WKEY_Q,			XKEY_Q,			PS3KEY_Q,		AKEY_UNKNOWN,	OSXKEY_Q,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Q);
	DIGITAL_INPUT_EVENT_DEF_2(ACTOR_ADD,			WKEY_U,			XKEY_U,			PS3KEY_U,		AKEY_UNKNOWN,	OSXKEY_U,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_U);
	DIGITAL_INPUT_EVENT_DEF_2(EMITTERS_RESET,		WKEY_I,			XKEY_I,			PS3KEY_I,		AKEY_UNKNOWN,	OSXKEY_I,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_I);
	DIGITAL_INPUT_EVENT_DEF_2(DECREASE_SIM_SCALE,	WKEY_DIVIDE,	XKEY_DIVIDE,	PS3KEY_DIVIDE,	AKEY_UNKNOWN,	OSXKEY_DIVIDE,	PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_DIVIDE);
	DIGITAL_INPUT_EVENT_DEF_2(INCREASE_SIM_SCALE,	WKEY_MULTIPLY,	XKEY_MULTIPLY,	PS3KEY_MULTIPLY, AKEY_UNKNOWN,	OSXKEY_MULTIPLY, PSP2KEY_UNKNOWN,	IKEY_UNKNOWN, 	LINUXKEY_MULTIPLY);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_DENSITY_VIZ,	WKEY_H,			XKEY_H,			PS3KEY_H,		AKEY_UNKNOWN,	OSXKEY_H,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_H);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_COLL_GROUPS,	WKEY_Z,			XKEY_Z,			PS3KEY_Z,		AKEY_UNKNOWN,	OSXKEY_Z,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_Z);
	DIGITAL_INPUT_EVENT_DEF_2(TOGGLE_EMTR_ATTACH,	WKEY_G,			XKEY_G,			PS3KEY_G,		AKEY_UNKNOWN,	OSXKEY_G,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_G);
	DIGITAL_INPUT_EVENT_DEF_2(RESET_SIM,			WKEY_R,			XKEY_R,			PS3KEY_R,		AKEY_UNKNOWN,	OSXKEY_R,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_R);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_0,				WKEY_0,			XKEY_0,			PS3KEY_0,		AKEY_UNKNOWN,	OSXKEY_0,		PSP2KEY_UNKNOWN,	IKEY_UNKNOWN,	LINUXKEY_0);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_1,				WKEY_1,			XKEY_1,			PS3KEY_1,		ABUTTON_2,		OSXKEY_1,		PSP2KEY_UNKNOWN,	IBUTTON_2,		LINUXKEY_1);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_2,				WKEY_2,			XKEY_2,			PS3KEY_2,		ABUTTON_1,		OSXKEY_2,		PSP2KEY_UNKNOWN,	IBUTTON_1,		LINUXKEY_2);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_3,				WKEY_3,			XKEY_3,			PS3KEY_3,		ABUTTON_3,		OSXKEY_3,		PSP2KEY_UNKNOWN,	IBUTTON_3,		LINUXKEY_3);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_4,				WKEY_4,			XKEY_4,			PS3KEY_4,		ABUTTON_4,		OSXKEY_4,		PSP2KEY_UNKNOWN,	IBUTTON_4,		LINUXKEY_4);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_5,				WKEY_5,			XKEY_5,			PS3KEY_5,		ABUTTON_5,		OSXKEY_5,		PSP2KEY_UNKNOWN,	IBUTTON_5,		LINUXKEY_5);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_6,				WKEY_6,			XKEY_6,			PS3KEY_6,		ABUTTON_6,		OSXKEY_6,		PSP2KEY_UNKNOWN,	IBUTTON_6,		LINUXKEY_6);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_7,				WKEY_7,			XKEY_7,			PS3KEY_7,		ABUTTON_7,		OSXKEY_7,		PSP2KEY_UNKNOWN,	IBUTTON_7,		LINUXKEY_7);
	DIGITAL_INPUT_EVENT_DEF_2(SCENE_8,				WKEY_8,			XKEY_8,			PS3KEY_7,		ABUTTON_8,		OSXKEY_8,		PSP2KEY_UNKNOWN,	IBUTTON_8,		LINUXKEY_8);
	//DIGITAL_INPUT_EVENT_DEF_2(SCENE_9,				WKEY_9,			XKEY_9,			PS3KEY_9,		ABUTTON_9,		OSXKEY_9,		PSP2KEY_UNKNOWN,	IBUTTON_9,		LINUXKEY_9);

	SampleApexApplication::collectInputEvents(inputEvents);
}

void SimpleParticlesTestApplication::collectInputDescriptions(std::vector<const char*>& inputDescriptions)
{
	SampleApexApplication::collectInputDescriptions(inputDescriptions);
	inputDescriptions.insert(inputDescriptions.end(), SampleParticlesInputEventDescriptions, SampleParticlesInputEventDescriptions + PX_ARRAY_SIZE(SampleParticlesInputEventDescriptions));
}

#if NX_SDK_VERSION_MAJOR == 2
void SimpleParticlesTestApplication::onMouseClickShapeHit(const PxVec3& pickDir, NxRaycastHit& hit, NxShape* /*hitshape*/)
{
	if (m_apexImpactEmitterAsset)
	{
		// this should be cached
		physx::PxU32 eventSetId = m_apexImpactEmitterAsset->querySetID("meshParticleEvent");
		if (m_impactActorIdx != 0xffffffff && m_actors[m_impactActorIdx])
		{
			SampleImpactEmitterActor* a = DYNAMIC_CAST(SampleImpactEmitterActor*)(m_actors[m_impactActorIdx]);
			a->m_actor->registerImpact(PXFROMNXVEC3(hit.worldImpact),
				pickDir,
				PXFROMNXVEC3(hit.worldNormal),
				eventSetId);
		}
	}
}
#elif NX_SDK_VERSION_MAJOR == 3
void SimpleParticlesTestApplication::onMouseClickShapeHit(const PxVec3& pickDir, PxRaycastHit& hit, PxShape* hitshape)
{
	if (m_apexImpactEmitterAsset)
	{
		// this should be cached
		physx::PxU32 eventSetId = m_apexImpactEmitterAsset->querySetID("meshParticleEvent");
		if (m_impactActorIdx != 0xffffffff && m_actors[m_impactActorIdx])
		{
			SampleImpactEmitterActor* a = DYNAMIC_CAST(SampleImpactEmitterActor*)(m_actors[m_impactActorIdx]);
			a->m_actor->registerImpact(hit.impact, pickDir, hit.normal, eventSetId);
		}
	}
}
#endif

bool SimpleParticlesTestApplication::onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val)
{
	// if a benchmark is running ignore the key board.
	if (m_BenchmarkMode)
	{
		return true;
	}

	if (!getRenderer() || !m_apexScene)
	{
		return SampleApexApplication::onDigitalInputEvent(ie, val);
	}

	if (val)
	{
		switch (ie.m_Id)
		{
		case SAVE_PARTICLE_SYSTEM:
			if ( mApexParticleEditor )
			{
				mApexParticleEditor->saveApexAssets();
			}
			break;
		case EDIT_PARTICLE_SYSTEM:
			if ( mApexParticleEditor)
			{
				physx::PxU32 count = (physx::PxU32)m_actors.size();
				for (physx::PxU32 i=0; i<count; i++)
				{
					physx::PxU32 c = i;
					if ( mEditIndex < count )
					{
						c = mEditIndex+1+i;
						if ( c >= count )
						{
							c-=count;
						}
					}
					if ( m_actors[c]->getType() == SampleApexEmitterActor::ApexEmitterActor)
					{
						SampleApexEmitterActor *a = static_cast< SampleApexEmitterActor *>(m_actors[c]);
						physx::apex::NxApexActor *apexActor = a->getActor();
						if (apexActor )
						{
							mEditIndex = c;
							mApexParticleEditor->editApexActor(apexActor,m_apexSDK, m_apexScene );
							break;
						}
					}
				}
			}
			break;
		case SCENE_0:
		case SCENE_1:
		case SCENE_2:
		case SCENE_3:
		case SCENE_4:
		case SCENE_5:
		case SCENE_6:
		case SCENE_7:
		case SCENE_8:
		case SCENE_9:
		case SCENE_SELECT:
			{
				if (isInputEventActive(MODIFIER_SHIFT) && ie.m_Id != SCENE_SELECT)
				{
					physx::PxU32 index = (ie.m_Id - SCENE_1);
					if (index < m_apexEmitterAssetList.size())
					{
						if (m_actors[index])
						{
							m_actors[index]->release();
							m_actors[index] = NULL;
						}
						else
						{
							m_actors[index] = new SampleApexEmitterActor(*m_apexEmitterAssetList[index], *m_apexScene, index);
						}
					}
				}
				else if (isInputEventActive(MODIFIER_CONTROL) && ie.m_Id != SCENE_SELECT)
				{
					physx::PxU32 index = (ie.m_Id - SCENE_0);
					NxParameterized::Interface* particleDebugRenderParams = m_apexScene->getModuleDebugRenderParams("FluidIos");
					PX_ASSERT(particleDebugRenderParams);

					NxParameterized::setParamU32(*particleDebugRenderParams, "GRAPH_ACTOR_INDEX", index);
				}
				else
				{
					SceneSelection scene;
					if (ie.m_Id == SCENE_SELECT)
					{
						scene = (SceneSelection)(m_sceneNumber + (SceneSelection)1);
						if (scene >= NumScenes)
						{
							scene = (SceneSelection)1;
						}
					}
					else
					{
						scene = (SceneSelection)(ie.m_Id - SCENE_0);
					}
					releaseAssetsAndActors(scene);
					loadAssetsAndActors(scene);
				}
			}
			break;
#if NX_SDK_VERSION_MAJOR == 2
		case TOGGLE_DENSITY_VIZ:
			{
				NxParameterized::Interface* particleDebugRenderParams = m_apexScene->getModuleDebugRenderParams("FluidIos");
				PX_ASSERT(particleDebugRenderParams);
				bool displayDensity;
				NxParameterized::getParamBool(*particleDebugRenderParams, "VISUALIZE_FLUID_DENSITY_HISTOGRAM", displayDensity);
				NxParameterized::setParamBool(*particleDebugRenderParams, "VISUALIZE_FLUID_DENSITY_HISTOGRAM", !displayDensity);
			}
			break;
#endif
		case EMITTERS_RESET:
			{
				physx::PxU32 c = 0;
				std::vector<SampleFramework::SampleActor*>::iterator it;

				it = m_actors.begin();
				do
				{
					if ((*it)->getType() == SampleApexEmitterActor::ApexEmitterActor)
					{
						(*it)->release();
						m_actors.erase(it);
						it = m_actors.begin();
						++c;
						continue;
					}
					it++;
				}
				while(it != m_actors.end());

				for (physx::PxU32 i = 0; i < c; i++)
				{
					m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[std::min(i, (physx::PxU32)1)], *m_apexScene, i));
				}
			}
			break;
		case ACTOR_ADD:
			{
				if(m_apexEmitterAssetList.size() != 0 && (m_sceneNumber == BasicIosScene || m_sceneNumber == NxFluidIosScene))
				{
					std::vector<SampleFramework::SampleActor*>::iterator it;
					if(m_actorAddSwitch)
					{
						it = m_actors.end();
						--it;
						(*it)->release();
						m_actors.erase(it);

						m_actorAddSwitch = false;
					}
					else
					{
						size_t i = 0; 
						for(it = m_actors.begin(); it != m_actors.end(); ++it)
						{
							if ((*it)->getType() == SampleApexEmitterActor::ApexEmitterActor) ++i;
						}

						if(i < 3)
						{
							size_t x = m_apexEmitterAssetList.size() - 1;
							m_actors.push_back(new SampleApexEmitterActor(*m_apexEmitterAssetList[x], *m_apexScene, i));
						}

						m_actorAddSwitch = true;
					}
				}
			}
			break;
		case INCREASE_SIM_SCALE:
			{
				for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
				{
					NxApexParameter& p = *m_emitterModuleScalables[i];
					physx::PxU32 newValue = physx::PxMin<physx::PxU32>(p.range.maximum, p.current + 1);
					m_apexEmitterModule->setIntValue(i, newValue);
				}
				updateScalableAndLodInfo();
			}
			break;
		case DECREASE_SIM_SCALE:
			{
				for (physx::PxU32 i = 0; i < m_nbEmitterModuleScalables; i++)
				{
					NxApexParameter& p = *m_emitterModuleScalables[i];
					physx::PxU32 newValue = physx::PxMax<physx::PxU32>(p.range.minimum, p.current - 1);
					m_apexEmitterModule->setIntValue(i, newValue);
				}
				updateScalableAndLodInfo();
			}
			break;
		case RESET_SIM:
			{
				m_pause = true;

				//unloadCurrentAssetsAndActors();
				releaseAssetsAndActors((SceneSelection)m_sceneNumber);
				loadAssetsAndActors((SceneSelection)m_sceneNumber);

				m_pause = false;
			}
			break;

#if NX_SDK_VERSION_MAJOR == 2
		case TOGGLE_COLL_GROUPS:
			{
				char str[MAX_DEBUG_TEXT_STRING];
				switch (++m_shapeGroupMask.bits0)
				{
				case 5:
					sprintf(str, "Col. groups state: collide with Mesh IOS particles only.");
					break;
				case 6:
					sprintf(str, "Col. groups state: collide with Sprite IOS particles only.");
					break;
				case 7:
					sprintf(str, "Col. groups state: collide with Mesh and Sprite IOS particles both.");
					break;
				case 8:
					m_shapeGroupMask.bits0 = 4;
					sprintf(str, "Col. groups state: don't collide with particles at all.");
					break;
				}
				strncpy(m_windowString[5], str, MAX_DEBUG_TEXT_STRING);
			}
			break;
#endif
		case TOGGLE_EMTR_ATTACH:
			{
				m_attachGroundEmitterToCamera = !m_attachGroundEmitterToCamera;
			}
			break;
		default:
			return SampleApexApplication::onDigitalInputEvent(ie, val);
		};
		return true;
	}
	else
	{
		return SampleApexApplication::onDigitalInputEvent(ie, val);
	}
}



