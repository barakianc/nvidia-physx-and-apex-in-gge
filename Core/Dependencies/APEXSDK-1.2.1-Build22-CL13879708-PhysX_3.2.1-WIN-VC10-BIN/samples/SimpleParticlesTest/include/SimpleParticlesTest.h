// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2012 NVIDIA Corporation. All rights reserved.
// ===============================================================================
//
//						   NVIDIA APEX SDK Sample Program
//
// Title: APEX Particles Sample
// Description: This sample program shows how to create APEX emitter actors.
//
// ===============================================================================

#ifndef SIMPLE_PARTICLES_H
#define SIMPLE_PARTICLES_H

#include <SampleApplication.h>
#include "SampleApexApplication.h"
#include <SampleAssetManager.h>
#include <SampleMaterialAsset.h>
#include <SampleCommandLine.h>
#include <SamplePlatform.h>
#include <SampleUserInput.h>
#include <SampleUserInputIds.h>
#include <SampleInputEventIds.h>
#include <SampleInputDefines.h>

#include <Renderer.h>

#include <RendererBoxShape.h>
#include <RendererMeshContext.h>
#include <RendererMaterialDesc.h>
#include <RendererMaterialInstance.h>
#include <RendererDirectionalLightDesc.h>
#include <RendererProjection.h>

#include <SampleApexRenderer.h>
#include <SamplesVRDSettings.h>

#include <UserErrorCallback.h>
#include <UserAllocator.h>

#include <NxApex.h>
#include <NxApexSDK.h>

#include <NxModuleEmitter.h>
#include <NxModuleEmitterLegacy.h>
#include <NxModuleIofx.h>
#include <NxModuleIofxLegacy.h>
#include <NxModuleExplosion.h>
#include <NxModuleExampleFS.h>
#include <NxModuleFieldSampler.h>
#include <NxModuleFrameworkLegacy.h>
#include <NxModuleCommonLegacy.h>

#include <NxParamUtils.h>
#include <NxApexEmitterAsset.h>
#include <NxApexEmitterActor.h>
#include <NxEmitterLodParamDesc.h>
#include <NxImpactEmitterAsset.h>
#include <NxImpactEmitterActor.h>
#include <NxGroundEmitterAsset.h>
#include <NxGroundEmitterActor.h>
#include <NxIofxAsset.h>
#include <NxIofxActor.h>
#include <NxApexRenderVolume.h>
#include <NxModifier.h>

#include <NxModuleBasicIos.h>
#include <NxModuleBasicIosLegacy.h>
#include <NxBasicIosAsset.h>

#if NX_SDK_VERSION_MAJOR == 2
#include <NxModuleFluidIos.h>
#include <NxFluidIosAsset.h>
#elif NX_SDK_VERSION_MAJOR == 3
#include <NxModuleParticleIos.h>
#include <NxParticleIosAsset.h>
#endif

#include <PsString.h>
#include <PsShare.h>
#include <PsUtilities.h>

class ApexParticleEditor;

using namespace physx::apex;
using namespace physx;

enum SceneSelection
{
	CommandLineAsset = 0,
	BasicIosScene = 1,
	NxFluidIosScene = 2,
	ImpactEmitterScene = 3,
	GroundEmitterScene = 4,
	ExplicitEmitterScene = 5,
	FigureScene = 6,
	StarScene = 7,
	AssetPreviewScene = 8,
	NumScenes,
};

///////////////////////////////////////////////////////////////////////////

#define ENABLE_SDK_VISUALIZATIONS			0

#define GROUND_PLANE_MATERIAL_INDEX			0
#define GROUND_PLANE_COL_GROUP				18


enum SampleParticlesInputEventIds
{
	SAMPLE_PARTICLES_FIRST = NUM_SAMPLE_BASE_INPUT_EVENT_IDS,

	CONTROL_BACK = SAMPLE_PARTICLES_FIRST,
	CONTROL_FORWARD,
	CONTROL_LEFT,
	CONTROL_RIGHT,
	CONTROL_UP,
	CONTROL_DOWN,

	SCENE_0,
	SCENE_1,
	SCENE_2,
	SCENE_3,
	SCENE_4,
	SCENE_5,
	SCENE_6,
	SCENE_7,
	SCENE_8,
	SCENE_9,

	SCENE_TOGGLE,

	EMITTERS_RESET,
	ACTOR_ADD,

	INCREASE_SIM_SCALE,
	DECREASE_SIM_SCALE,

	RESET_SIM,

	TOGGLE_DENSITY_VIZ,
	TOGGLE_COLL_GROUPS,

	TOGGLE_EMTR_ATTACH,

	NUM_SAMPLE_PARTICLES_INPUT_EVENT_IDS,
	NUM_EXCLUSIVE_SAMPLE_PARTICLES_INPUT_EVENT_IDS = NUM_SAMPLE_PARTICLES_INPUT_EVENT_IDS - SAMPLE_PARTICLES_FIRST
};


class SampleApexEmitterActor : public SampleFramework::SampleActor
{
public:
	SampleApexEmitterActor(NxApexEmitterAsset& asset, NxApexScene& apexScene, size_t i);
	virtual ~SampleApexEmitterActor(void);
	virtual NxApexActor* getActor(void) const;
	virtual int getType();
	enum { ApexEmitterActor = 5 };
	void setActor(NxApexEmitterActor* act);

	NxApexEmitterActor* m_actor;
};

class SampleImpactEmitterActor : public SampleFramework::SampleActor
{
public:
	SampleImpactEmitterActor(NxImpactEmitterAsset& asset, NxApexScene& apexScene);
	virtual ~SampleImpactEmitterActor();
	void registerImpact(const physx::PxVec3& hitPos, const physx::PxVec3& hitDir, const physx::PxVec3& surfNorm, physx::PxU32 surfType);

	NxImpactEmitterActor* m_actor;
};

class SampleGroundEmitterActor : public SampleFramework::SampleActor
{
public:
	SampleGroundEmitterActor(NxGroundEmitterAsset& asset, NxApexScene& apexScene, const physx::PxVec3& position);
	virtual ~SampleGroundEmitterActor(void);
	void updatePosition(const physx::PxVec3& position);
	virtual int getType();
	enum { GroundEmitterActor = 8 };

	NxGroundEmitterActor*	m_actor;
	physx::PxMat44			m_pose;
};

class SampleFieldSamplerActor : public SampleFramework::SampleActor
{
public:
	SampleFieldSamplerActor(NxApexAsset& asset, NxApexScene& apexScene, physx::PxU32 i);
	virtual ~SampleFieldSamplerActor(void);
	void updateEyePosition(physx::PxVec3& /*position*/);
	virtual void render(bool /*rewriteBuffers*/);
	virtual int getType();
	enum { FieldSamplerActor = 9 };

	NxApexActor* m_actor;
};


class SimpleParticlesTestApplication : public SampleApexApplication
{
public:
	SimpleParticlesTestApplication(const SampleFramework::SampleCommandLine& cmdline);
	virtual ~SimpleParticlesTestApplication(void);
	void initializeText();
	void updateCollisionObjectSceneInfo();
	void setSceneString(const char* str);
	void setScalableInfoString(const char* str);
	void updateScalableAndLodInfo();
	void processEarlyCmdLineArgs(void);

protected:
	virtual unsigned int getNumScenes() const;
	virtual unsigned int getSelectedScene() const;
	virtual const char* getSceneName(unsigned int sceneNumber) const;
	virtual void selectScene(unsigned int sceneNumber);

private:
	static void benchmarkStart(const char* benchmarkName);
	virtual void onInit(void);
	void loadAssetsAndActors(SceneSelection sceneNumber);
	void releaseAssetsAndActors(SceneSelection sceneNumber = BasicIosScene);
	virtual void onShutdown(void);
	virtual void onTickPreRender(float dtime);
	virtual void onRender(void);
	virtual void onTickPostRender(float /*dtime*/);
	virtual void collectInputEvents(std::vector<const SampleFramework::InputEvent*>& inputEvents);
	virtual void collectInputDescriptions(std::vector<const char*>& inputDescriptions);
	void initCommonDebugRenderConfigs();
	void setSceneEyePos();

#if NX_SDK_VERSION_MAJOR == 2
	virtual void onMouseClickShapeHit(const PxVec3& pickDir, NxRaycastHit& hit, NxShape* /*hitshape*/);
#elif NX_SDK_VERSION_MAJOR == 3
	virtual void onMouseClickShapeHit(const PxVec3& pickDir, PxRaycastHit& hit, PxShape* hitshape);
#endif

	virtual bool onDigitalInputEvent(const SampleFramework::InputEvent& ie, bool val);

	enum {
		WINDOW_SCENE_NAME_INDEX = 0,
		WINDOW_SCENE_INFO_INDEX = 1,
		NUM_WINDOW_STRINGS      = 6,
		WINDOW_STRING_LENGTH    = 256
	};

	static SimpleParticlesTestApplication* m_AppPtr;

	const char*							m_CommandLineAsset;
	char								m_windowString[NUM_WINDOW_STRINGS][WINDOW_STRING_LENGTH];

	NxModuleBasicIos*					m_apexBasicIosModule;
	NxModule*							m_apexNxFluidIosModule;
	NxModule*							m_apexParticleIosModule;

	NxModuleEmitter*					m_apexEmitterModule;
	physx::PxU32						m_nbEmitterModuleScalables;
	NxApexParameter** 					m_emitterModuleScalables;
	NxImpactEmitterAsset*				m_apexImpactEmitterAsset;
	NxGroundEmitterAsset*				m_apexGroundEmitterAsset;

	NxModuleIofx*						m_apexIofxModule;
	std::vector<NxApexAssetPreview*>	m_apexPreviewList;
	std::vector<NxRenderMeshAsset*>		m_apexRenderMeshAssetList;
	std::vector<NxApexRenderVolume*>	m_renderVolumeList;
	std::vector<NxApexEmitterAsset*>	m_apexEmitterAssetList;
	std::vector<NxApexAsset*>			m_apexFieldSamplerAssetList;

	SceneSelection						m_sceneNumber;
	size_t								m_impactActorIdx;
	size_t								m_groundEmitterActorIdx;

	bool								m_actorAddSwitch;
	bool								m_attachGroundEmitterToCamera;

	PxU32								mEditIndex;
	ApexParticleEditor*					mApexParticleEditor;
};

#endif //SIMPLE_PARTICLES_H