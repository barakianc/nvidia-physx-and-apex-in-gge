NVIDIA APEX 1.2.1 SDK distribution

To build the Visual Studio based APEX projects in this distribution, the 
PhysX SDK path in the nxpath.vsprops files must be modified.  The 
UpdatePhysXPaths.vbs script is provided for this purpose.  It will
modify the nxpath.vsprops files under the compiler folders.

The 3.x PhysX path should contain the folder location (absolute or relative)
that contains the "Include" directory.

The 2.8.4 PhysX path should contain the folder location (absolute or relative)
that contains the "SDKs" directory.

To build the makefile based APEX projects, the PhysX SDK path and other 
undefined paths at the top of the Makefile must be modified.