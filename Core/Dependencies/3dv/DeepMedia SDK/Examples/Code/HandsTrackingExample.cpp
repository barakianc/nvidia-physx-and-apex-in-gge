// HandsTracking.cpp : Defines the entry point for the console application.

#include "windows.h"
#include "TDVHandsTracking.h"
#include "TDVCameraInterface.h"
#include <stdio.h>
#include <conio.h>

unsigned char* g_pDepthData = NULL; // hold a pointer to the received frame
HANDLE g_frameReady = 0;			// for signalling a frame is ready to be processed


// video callback function
void videoCallBack(unsigned char * pDepthBuf, 
				   unsigned char * pRgbBuf, 
				   int bufIndex,
				   void* pObject)
{
	g_pDepthData = pDepthBuf;
	ReleaseMutex(g_frameReady);
}


int main(int argc, char* argv[])
{
	ITDVHandsTracking tracking;
	TDVCameraInterfaceBase* pCameraInterface = NULL;	

	char* pNextDepth = NULL;
	TDVHand firstHand;
	TDVHand secondHand;
	TDVHead head;

	int iWidth;
	int iHeight;
	int iRGBPixelSize;
	int iDPixelSize;

	// initialize the camera interface and callback functions
	pCameraInterface = new TDVCameraInterfaceBase();
	g_frameReady = CreateMutex(NULL, FALSE, NULL); 
	pCameraInterface->setVideoCallBack(videoCallBack);
	while (!pCameraInterface->isCommActive())
	{  
		printf("Comunication is not yet active...\n");
		Sleep(1000);
	}
	while (!pCameraInterface->isVideoActive())
	{
		printf("Video is not yet active...\n");
		Sleep(1000);
	}
	// get the frame size from the camera
	pCameraInterface->getVideoSize(iWidth, iHeight, iRGBPixelSize, iDPixelSize);
	tracking.Init(iWidth, iHeight, 4); // initialize the input frame size
	tracking.SetZSENSE();

	tracking.SetTrackingParameter(TDV_Sensitivity, 4);		  // medium temporal filtering
	tracking.SetTrackingParameter(TDV_TwoHandMode, true);	  // assuming two hands in scene
	tracking.SetTrackingParameter(TDV_CalibrationMode, 1);	  // Begin with calibration mode. 
	

	while(!kbhit())
	{
		if (WaitForSingleObject(g_frameReady, 1000) != WAIT_OBJECT_0)
			continue;
		pNextDepth = (char*)g_pDepthData;
		TDVCameraWindowParams params;
		params.m_nWindowDistance = pCameraInterface->getPrimaryDist();
		params.m_nWindowWidth = pCameraInterface->getPrimaryWidth();
		tracking.SetTrackingParameter(TDV_EnableHeadTracking,1.0f);
		tracking.SetWindowParams(params);
		tracking.ProcessFrame((unsigned char*)pNextDepth);
		if (SUCCEEDED(tracking.GetFirstHandGesture(firstHand)))
		{
			int fingerCount=0;
			printf("First hand position = (%d, %d) with ", (int)firstHand.handPos.x, (int)firstHand.handPos.y);
			for (int i=0; i<5; i++)
				if (firstHand.finger[i])
					fingerCount++;
			printf("%d fingers\n", fingerCount);				
		}
		if (SUCCEEDED(tracking.GetSecondHandGesture(secondHand)))
		{
			int fingerCount=0;
			printf("second hand position = (%d, %d) with ", (int)secondHand.handPos.x, (int)secondHand.handPos.y);
			for (int i=0; i<5; i++)
				if (secondHand.finger[i])
					fingerCount++;
			printf("%d fingers\n", fingerCount);
		}
		if (SUCCEEDED(tracking.GetHead(head)))
		{
			printf("Head position = (%d, %d)\n", (int)head.headPos.x, (int)head.headPos.y);

		}
	}
	delete pCameraInterface;
	CloseHandle(g_frameReady);
	return 0;
}
