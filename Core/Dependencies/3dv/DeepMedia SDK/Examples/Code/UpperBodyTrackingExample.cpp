// UpperBodyExample.cpp  : Defines the entry point for the console application.
#include "windows.h"
#include <iostream>
#include <conio.h>
#include "TDVCameraInterface.h"
#include "TDVUpperBodyTracking.h"

#pragma comment( lib, "ConnectDM_VC80.lib" )
#pragma comment( lib, "TDVUpperBodyTracking.lib" )

TDVCameraInterfaceBase* g_pCameraInterface = NULL;	
TDVUpperBodyTracking* g_ubTracking = NULL;

unsigned char* g_pDepthData = NULL; // Holds a pointer to the received depth frame
unsigned char* g_pPrimaryData = NULL; // Holds a pointer to the received IR (ungated) frame
HANDLE g_frameReady = 0;			// Used for signaling a frame is ready to be processed


// video callback function
void videoCallBack(unsigned char * pDepthBuf, 
				   unsigned char * pRgbBuf, 
				   int bufIndex,
				   void* pObject)
{
	g_pDepthData=pDepthBuf;//Get the depth data
	g_pCameraInterface->getVideoBuffer(BUFFER_TYPE_PRIMARY,g_pPrimaryData);//Get primary IR frame data
	SetEvent(g_frameReady);
}

// Command callback function
void cmdCallBack(int cmd, void* pObject)
{
	/*
	we only receive command if they value parameter is 
	different than currently set value

	only when the main DMachine application sends a message, we can 
	be sure the value was set in the camera. for example, when turning on the
	illumincation, only when CMD_ILLUMINCATION_ON is returned, the value
	was set in the camera.
	*/

	//Update 
	int windowWidth;
	int windowDist;
	switch (cmd)
	{
	case CMD_PRIMARY_DISTANCE:
		g_pCameraInterface->getCameraCommandVal(CMD_PRIMARY_DISTANCE, windowDist);
		g_ubTracking->SetParameter(TDV_UB_PARAM_CAM_WIN_DISTANCE_CM, windowDist);
		std::cout<<"Primary distance updated"<<std::endl;
		break;
	case CMD_PRIMARY_WIDTH:
		g_pCameraInterface->getCameraCommandVal(CMD_PRIMARY_WIDTH, windowWidth);
		g_ubTracking->SetParameter(TDV_UB_PARAM_CAM_WIN_WIDTH_CM, windowWidth);
		std::cout<<"Primary width updated"<<std::endl;
		break;
	}
}



int main(int argc, char* argv[])
{
	// Create the tracking object
	g_ubTracking= new TDVUpperBodyTracking();

	// Initialize the camera interface and callback functions
	g_pCameraInterface = new TDVCameraInterfaceBase();
	g_frameReady = CreateEvent(NULL,FALSE,FALSE,NULL); 
	g_pCameraInterface->setVideoCallBack(videoCallBack,NULL);
	g_pCameraInterface->setCommandCallBack(cmdCallBack, NULL);

	while (!g_pCameraInterface->isCommActive())
	{
		printf("Comunication is not yet active...\n");
		Sleep(1000);
	}
	while (!g_pCameraInterface->isVideoActive())
	{
		printf("Video is not yet active...\n");
		Sleep(1000);
	}
	printf("Video started...\n");

	// Get the frame size from the camera
	int width;
	int height;
	int rgbPixelSize;
	int irPixelSize;
	g_pCameraInterface->getVideoSize(width, height, rgbPixelSize, irPixelSize);
	g_pCameraInterface->setCameraCommand(CMD_USING_IR_INPUT,true);


	// Set some camera parameters
	g_pCameraInterface->setCameraCommand(CMD_CAMERA_RESOLUTION, RESOLUTION_160X120_60FPS);
	
	// Electric frequency: 1 = 60Hz; 0 = 50Hz
	g_pCameraInterface->setCameraCommand(CMD_ELEC_FREQUENCY, 1);
	
	
	g_pCameraInterface->setCameraCommand(CMD_PROCESSING_MODE, 1);
	g_pCameraInterface->setCameraCommand(CMD_RGB_PROCESSING, false);
	g_pCameraInterface->setCameraCommand(CMD_PRIMARY_BRIGHTNESS, 15);
	g_pCameraInterface->setCameraCommand(CMD_SECONDARY_BRIGHTNESS, 15);
	g_pCameraInterface->setCameraCommand(CMD_AUTO_BRIGHTNESS, 0);
	g_pCameraInterface->setCameraCommand(CMD_GAIN, 10);
	g_pCameraInterface->setCameraCommand(CMD_AUTO_DEPTH_POSITION, 1);
	g_pCameraInterface->setCameraCommand(CMD_CLEAN_VAL, 4);
	g_pCameraInterface->setCameraCommand(CMD_SMOOTH, 0);
	g_pCameraInterface->setCameraCommand(CMD_MEDIAN, 1);



	// The depth window parameters depends on the scenario.
	// You can change the values via the camera control while running this example
	// The following are just arbitrary values
	g_pCameraInterface->setCameraCommand(CMD_PRIMARY_DISTANCE, 90);
	g_pCameraInterface->setCameraCommand(CMD_PRIMARY_WIDTH, 180);




	// Initialize upper body tracking
	g_ubTracking->Initialize(width,height,rgbPixelSize);


	// Set the parameters
	int windowWidth;
	int windowDist;
	g_pCameraInterface->getCameraCommandVal(CMD_PRIMARY_WIDTH, windowWidth);
	g_pCameraInterface->getCameraCommandVal(CMD_PRIMARY_DISTANCE, windowDist);
	g_ubTracking->SetParameter(TDV_UB_PARAM_CAM_WIN_DISTANCE_CM, windowDist);
	g_ubTracking->SetParameter(TDV_UB_PARAM_CAM_WIN_WIDTH_CM, windowWidth);

	g_ubTracking->SetParameter(TDV_UB_PARAM_PERSON_HEIGHT_CM,double(175));//Set user height=175	
	g_ubTracking->SetParameter(TDV_UB_PARAM_CAM_ELEVATION_ANG,0);//Set camera elevation angle=0

	while(!_kbhit())
	{
		if (WaitForSingleObject(g_frameReady, 1000) != WAIT_OBJECT_0)
			continue;

		// Process the frame
		if (g_ubTracking->ProcessFrame(g_pDepthData,g_pPrimaryData) == TDV_OK)
		{

			// Get the results
			float x,y;

			//Head position
			if (g_ubTracking->GetDetectedFeature2D(TDV_UB_FEATURE_HEAD_POS,x,y) == TDV_OK)
				std::cout<<"Head found at (2D): ("<<int(x)<<","<<int(y)<<")"<<std::endl;

			//Left hand position
			if (g_ubTracking->GetDetectedFeature2D(TDV_UB_FEATURE_LEFT_HAND_POS,x,y) == TDV_OK)
				std::cout<<"Left Hand found at (2D): ("<<int(x)<<","<<int(y)<<")"<<std::endl;


			//Right hand position
			if (g_ubTracking->GetDetectedFeature2D(TDV_UB_FEATURE_RIGHT_HAND_POS,x,y) == TDV_OK)
				std::cout<<"Right Hand found at (2D): ("<<int(x)<<","<<int(y)<<")"<<std::endl;

			//Torso position
			if (g_ubTracking->GetDetectedFeature2D(TDV_UB_FEATURE_TORSO_POS,x,y) == TDV_OK)
				std::cout<<"Torso found at (2D): ("<<int(x)<<","<<int(y)<<")"<<std::endl;
		}
	}

	delete g_pCameraInterface;
	CloseHandle(g_frameReady);
	return 0;
}
