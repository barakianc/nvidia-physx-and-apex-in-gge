//****************************************************************************
//
//  Copyright (C) 1997-2005 3DV SYSTEMS LTD.
//  All rights reserved.
//
//  Module Name:	SDKTest.cpp
//  Project Name: 
//  Written By:		Gil Zigelman
//  Date:			21-09-2005
//  Description:	Header file for targa image saving.
//					contains a few definitions to allow calls to writeTargaFile
//					which saves a targa image file.
//  Updated By:		Gil Zigelman
//	Last Updated:	25-09-2006			
//****************************************************************************


#ifndef __TGA_WRITER_H___
#define __TGA_WRITER_H___

#include <stdio.h>


#ifndef TGA_RGB
struct TGAHeader
{
	unsigned char  IDLength;
	unsigned char  CoMapType;
	unsigned char  ImgType;
	unsigned short CoMapIndex;
	unsigned short CoMapLength;
	unsigned char  CoMapEntrySize;
	unsigned short XOrg;
	unsigned short YOrg;
	unsigned short Width;
	unsigned short Height;
	unsigned char  PixelSize;
	unsigned char  Flags; //AttBits, LRBit, OrgBit, Interleave - bit fields;
};

#define TGA_RGB 2 //Uncompressed truecolor
#define TGA_GRAY 3 //Uncompressed truecolor
#endif


// Shorthand for writing a word to a file
static void fwriteShort(unsigned short aWord, FILE *file)
{
#ifndef __MAC__
	 fwrite(&aWord, sizeof(aWord), 1, file);
#else
	 unsigned char * aByte = (unsigned char*)&aWord;
	 fwriteByte(*(aByte + 1), file);
	 fwriteByte(*aByte, file);
#endif
}

bool writeTargaFile(unsigned char* data, int width, 
					   int height, int pixelSize, 
					   const char *pFilename)
{
	FILE *pFile;

	pFile=fopen(pFilename,"wb");
	if (!pFile)
		return false;

	TGAHeader	Th;

	Th.IDLength=0;
	Th.CoMapType=0;
	Th.ImgType = (pixelSize == 1)?TGA_GRAY:TGA_RGB;
	Th.CoMapIndex=0;
	Th.CoMapLength=0;
	Th.CoMapEntrySize=0;
	Th.XOrg=0;
	Th.YOrg=(unsigned short)height;
	Th.Width=(unsigned short)width;
	Th.Height=(unsigned short)height;
	Th.PixelSize=pixelSize*8;
	Th.Flags=0x20;

	fwrite(&Th.IDLength,sizeof(Th.IDLength),1,pFile);
	fwrite(&Th.CoMapType,sizeof(Th.CoMapType),1,pFile);
	fwrite(&Th.ImgType,sizeof(Th.ImgType),1,pFile);
	fwriteShort(Th.CoMapIndex, pFile);
	fwriteShort(Th.CoMapLength, pFile);
	fwrite(&Th.CoMapEntrySize,sizeof(Th.CoMapEntrySize),1,pFile);
	fwriteShort(Th.XOrg, pFile);
	fwriteShort(Th.YOrg, pFile);
	fwriteShort(Th.Width, pFile);
	fwriteShort(Th.Height, pFile);
	fwrite(&Th.PixelSize,sizeof(Th.PixelSize),1,pFile);
	fwrite(&Th.Flags,sizeof(Th.Flags),1,pFile);

	for (int y = 0; y < height; ++y)
	{
		void* pLine = (void*)(data + y*width*pixelSize);
		fwrite((void*)pLine, pixelSize, width, pFile);
	}
	fclose(pFile);
	return true;
}


#endif //__TGA_WRITER_H___