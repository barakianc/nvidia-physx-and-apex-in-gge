// MaskExample.cpp : Defines the entry point for the console application.
#include <windows.h>
#include <conio.h>
#include "tgaWriter.h"
#include "TDVCameraInterface.h"
#include "TDVMask.h"

#pragma comment( lib, "ConnectDM_VC80.lib" )
#pragma comment( lib, "TDVMask.lib" )

unsigned char* g_pColorData = NULL; // hold a pointer to the received RGB frame
unsigned char* g_pPrimaryData = NULL; // hold a pointer to the received IR (ungated) frame
unsigned char* g_pSecondaryData = NULL; // hold a pointer to the received IR (gated) frame
HANDLE g_frameReady = 0;			// for signaling a frame is ready to be processed


// video callback function
void videoCallBack(unsigned char * pDepthBuf, 
				   unsigned char * pRgbBuf, 
				   int bufIndex,
				   void* pObject)
{

	TDVCameraInterfaceBase *pCameraInterface=(TDVCameraInterfaceBase*)pObject;
	pCameraInterface->getVideoBuffer(BUFFER_TYPE_RGB_FULL_RES,g_pColorData);
	pCameraInterface->getVideoBuffer(BUFFER_TYPE_PRIMARY,g_pPrimaryData);
	pCameraInterface->getVideoBuffer(BUFFER_TYPE_SECONDARY,g_pSecondaryData);
	SetEvent(g_frameReady);
}



int main(int argc, char* argv[])
{
	TDVMask bkgReplacement;
	TDVCameraInterfaceBase* pCameraInterface = NULL;	

	int irWidth;
	int irHeight;
	int irPixelSize;
	int rgbWidth;
	int rgbHeight;
	int rgbPixelSize;
	int outputWidth=320;
	int outputHeight=240;

	


	// initialize the camera interface and callback functions
	pCameraInterface = new TDVCameraInterfaceBase();
	g_frameReady = CreateEvent(NULL,FALSE,FALSE,NULL); 
	pCameraInterface->setVideoCallBack(videoCallBack,pCameraInterface);
	while (!pCameraInterface->isCommActive())
	{
		printf("Comunication is not yet active...\n");
		Sleep(1000);
	}
	while (!pCameraInterface->isVideoActive())
	{
		printf("Video is not yet active...\n");
		Sleep(1000);
	}
	printf("Video started...\n");
	// get the frame size from the camera
	pCameraInterface->getVideoSize(irWidth, irHeight, rgbPixelSize, irPixelSize);
	


	//Enable RGB processing
	pCameraInterface->setCameraCommand(CMD_RGB_PROCESSING,1);

	//Enable IR processing
	pCameraInterface->setCameraCommand(CMD_USING_IR_INPUT,true);
	
	//Enable full RGB resolution and get the video size
	pCameraInterface->setCameraCommand(CMD_RGB_FULL_RES,1);
	pCameraInterface->getRGBFullResSize(rgbWidth,rgbHeight,rgbPixelSize);

	//Wait for the changes to take place
	Sleep(1000);

	//Initialize the background replacement object
	if (irWidth==160)
		bkgReplacement.Init(TDV_Mask160X120,TDV_Mask640X480,TDV_Mask320X240,rgbPixelSize);
	else
		bkgReplacement.Init(TDV_Mask320X240,TDV_Mask640X480,TDV_Mask320X240,rgbPixelSize);

	//Set the parameters
	bkgReplacement.SetParameter(TDV_MaskThreshold,0);//Algorithm threshold=0
	bkgReplacement.SetParameter(TDV_MaskQuality,0);//0=high quality,1=medium,2=low
	bkgReplacement.SetParameter(TDV_MaskAlgorithm,0);//0=full algorithm,1=partial

	printf("Press any key...\n");
	while(!_kbhit())
	{
		unsigned char* pColorData = NULL;
		unsigned char* pPrimaryData = NULL;
		unsigned char* pSecondaryData = NULL;
		if (WaitForSingleObject(g_frameReady, 1000) != WAIT_OBJECT_0)
			continue;
		pColorData = g_pColorData;
		pPrimaryData = g_pPrimaryData;
		pSecondaryData = g_pSecondaryData;

		bkgReplacement.ProcessFrame(pPrimaryData,pSecondaryData,pColorData);

		unsigned char* pOutMask = NULL;
		unsigned char* pOutRGB = NULL;
		bkgReplacement.GetResult(&pOutMask,&pOutRGB);
		writeTargaFile(pOutMask,outputWidth,outputHeight,1,"mask_out.tga");
		writeTargaFile(pOutRGB,outputWidth,outputHeight,4,"rgb_out.tga");

	}

	delete pCameraInterface;
	CloseHandle(g_frameReady);
	return 0;
}
