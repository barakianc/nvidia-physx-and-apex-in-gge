//****************************************************************************
//
//  Copyright (C) 1997-2007 3DV SYSTEMS LTD.
//  All rights reserved.
//
//  Module Name:	TDVResult.h
//  Project Name:   General
//  Written By:		Sagi Katz
//  Date:			13-3-2008
//  Description:	External status codes header file
//  Updated By:		
//	Last Updated:	
//	Version:		1.00
//****************************************************************************
#pragma once

//Return codes
enum TDV_RESULT
{
	TDV_OK,
	TDV_NOT_CALIBRATED,
	TDV_NOT_INITIALIZED,
	TDV_NO_FRAME_SET,
	TDV_NO_RESULTS,
	TDV_INVALID_IMAGE,
	TDV_INVALID_POSE,
	TDV_INVALID_PARAMETER,
	TDV_INTERNAL_ERR,
	TDV_UNKNOWN
};