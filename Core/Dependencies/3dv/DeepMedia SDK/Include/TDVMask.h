//****************************************************************************
//
//  Copyright (C) 1997-2007 3DV SYSTEMS LTD.
//  All rights reserved.
//
//  Module Name:	TDVMask.h
//  Date:			19-06-07
//  Description:	Main header file for mask extraction SDK
//****************************************************************************
#pragma once
#include "TDVDeepMedia.h"

#ifdef MASK_EXPORTS
#define MASK_DLL __declspec(dllexport)
#else
#define MASK_DLL __declspec(dllimport)
#endif

//mask parameters
enum TDVMaskParameters
{
	TDV_MaskThreshold, //0-255
	TDV_MaskQuality,   //0,1,2 (0=HIGH,1=MED,2=LOW)
	TDV_MaskAlgorithm, //0=FULL,1=PARTIAL

	TDV_MASK_NUM_PARAMS  // N/A - this is just a counter for the number of parameters
};


//Possible mask resolutions
enum TDVMaskResolutions
{
	TDV_Mask104X72,
	TDV_Mask160X120,
	TDV_Mask208X148,
	TDV_Mask320X240,
	TDV_Mask640X480,

	TDV_MASK_NUM_RES // N/A - this is just a counter for the number of supported resolutions
};


class MASK_DLL TDVMask
{
public:


	TDVMask(void);
	~TDVMask(void);

	//Resolution relations must be:
	// irRes<=outputRes<=rgbRes
	HRESULT Init(TDVMaskResolutions irRes,
				 TDVMaskResolutions rgbRes,
				 TDVMaskResolutions outputRes,
				 int rgbPixelSize=4);


	HRESULT IsInitialized(void) const;

	HRESULT SetParameter(TDVMaskParameters paramID,float state);


	HRESULT ProcessFrame(const unsigned char* pPrimaryIR, 
						 const unsigned char* pSecondaryIR, 
						 const unsigned char* pRGB);


	void GetResult(unsigned char **pMask,unsigned char **pRGB);

private:
	bool m_bIsInitialized;
	TDVMaskResolutions m_irRes;
	TDVMaskResolutions m_rgbRes;
	TDVMaskResolutions m_outputRes;
	int m_rgbPixelSize;
	float m_params[TDV_MASK_NUM_PARAMS];

	int GetResWidth(TDVMaskResolutions resNum);
	int GetResHeight(TDVMaskResolutions resNum);

};
