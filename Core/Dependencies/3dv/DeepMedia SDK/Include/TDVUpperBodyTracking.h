//****************************************************************************
//
//  Copyright (C) 1997-2008 3DV SYSTEMS LTD.
//  All rights reserved.
//
//  Module Name:	TDVUpperBodyTracking.h 
//  Project Name: 
//  Written By:		Sagi Katz
//  Date:			26-6-2007
//  Description:	Upper body tracking and calibration header file
//  Updated By:		Sagi Katz
//	Last Updated:	1-04-2008
//	Version:		1.0.13
//****************************************************************************
#pragma once
#include "TDVResult.h"

#ifdef TDVUPPERBODYTRACKING_EXPORTS
#define TDVUPPERBODY_DLL __declspec(dllexport)
#else
#define TDVUPPERBODY_DLL __declspec(dllimport)
#endif

enum TDV_UB_FEATURE
{
	TDV_UB_FEATURE_TORSO_POS,// 2D or 3D feature.
	TDV_UB_FEATURE_TORSO_VELOCITY,// 2D or 3D feature.
	TDV_UB_FEATURE_TORSO_ANGS,// 3D feature. 
	TDV_UB_FEATURE_HEAD_POS,// 2D or 3D feature. 
	TDV_UB_FEATURE_HEAD_VELOCITY,// 2D or 3D feature. 
	TDV_UB_FEATURE_HEAD_ANGS,// 3D feature. 
	TDV_UB_FEATURE_LEFT_HAND_POS,// 2D or 3D feature. 
	TDV_UB_FEATURE_LEFT_HAND_VELOCITY,// 2D or 3D feature. 
	TDV_UB_FEATURE_RIGHT_HAND_POS,// 2D or 3D feature. 
	TDV_UB_FEATURE_RIGHT_HAND_VELOCITY,// 2D or 3D feature. 
	TDV_UB_FEATURE_LEFT_HAND_OPEN,//1D feature. Left hand open/closed
	TDV_UB_FEATURE_RIGHT_HAND_OPEN//1D feature. Right hand open/closed
};




//Parameters
enum TDV_UB_PARAM
{
	//Camera position parameters
	TDV_UB_PARAM_CAM_ELEVATION_ANG,//Camera elevation angle (degrees 0-90), default=0
								   //This parameter is calculated during the calibration, but can also be set 
								   //manually (overriding calibration)

	TDV_UB_PARAM_CAM_BASE_HEIGHT_CM,//Camera base height (distance from the floor) in cm, default=-1
									//This parameter is used for calibration, in case it is -1, then it is ignored and
									//the y value of the 3D coordinates are relative to the camera y. 

	//Internal camera parameters
	TDV_UB_PARAM_CAM_FOCAL_LEN_MM,//Focal length in millimeters, default=4.5
	TDV_UB_PARAM_CAM_PIXEL_WIDTH_MM,//Pixel width in millimeters, default=0.0112
	TDV_UB_PARAM_CAM_PIXEL_HEIGHT_MM,//Pixel height in millimeters, default=0.0112
	TDV_UB_PARAM_CAM_NUM_PIXEL_X,//Number of pixels of the imaging sensor in the x direction, default=320
	TDV_UB_PARAM_CAM_NUM_PIXEL_Y,//Number of pixels of the imaging sensor in the y direction, default=240
	TDV_UB_PARAM_CAM_FOCAL_CEN_X,//Image sensor focal point x position in pixels, default=160
	TDV_UB_PARAM_CAM_FOCAL_CEN_Y,//Image sensor focal point y position in pixels, default=120

	//depth camera parameters
	TDV_UB_PARAM_CAM_WIN_DISTANCE_CM,//Depth window distance to the camera in centimeters, default=100
	TDV_UB_PARAM_CAM_WIN_WIDTH_CM,//Depth window width to the camera in centimeters, default=100
	TDV_UB_PARAM_CAM_NUM_BITS_PER_PIXEL,//Number of bits per pixel of the depth image, default=8
	TDV_UB_PARAM_CAM_NUM_GRAY_LEVELS,//Number of gray levels of the depth image, default=256

	//Algorithm parameters
	TDV_UB_PARAM_PERSON_HEIGHT_CM,//Player height in centimeters, default=175
	TDV_UB_PARAM_PERSON_HEIGHT_TOLERANCE,//Tolerance to player size (0-1), default=0.8

	TDV_UB_PARAM_HAND_DETECTION_SENSETIVITY_CM,// Hand detection sensitivity in centimeters (this is the minimum distance between
												// the hand and its surroundings in Z axes allowed for detecting a hand)
												// Allowed values 10..30, default=15



	//Temporal filtering
	TDV_UB_PARAM_FEATURE_SMOOTHING, //Binary, 1- Enable result smoothing, 0- disable smoothing, default=1
	TDV_UB_PARAM_HEAD_2D_LOC_SMOOTHING,//Head 2D feature smoothing,  valid values (0-1), default=0.005
	TDV_UB_PARAM_HEAD_3D_LOC_SMOOTHING,//Head 3D feature smoothing,  valid values (0-1), default=0.005
	TDV_UB_PARAM_TORSO_2D_LOC_SMOOTHING,//Torso 2D feature smoothing, valid values (0-1), default=0.05
	TDV_UB_PARAM_TORSO_3D_LOC_SMOOTHING,//Torso 3D feature smoothing,  valid values (0-1), default=0.05
	TDV_UB_PARAM_LEFT_HAND_2D_LOC_SMOOTHING,//Left hand 2D feature smoothing, valid values (0-1),  default=0 (no smoothing)
	TDV_UB_PARAM_LEFT_HAND_3D_LOC_SMOOTHING,//Left hand 3D feature smoothing, valid values (0-1),  default=0.0005
	TDV_UB_PARAM_RIGHT_HAND_2D_LOC_SMOOTHING,//Right hand 2D feature smoothing, valid values (0-1),  default=0 (no smoothing)
	TDV_UB_PARAM_RIGHT_HAND_3D_LOC_SMOOTHING,//Right hand 3D feature smoothing, valid values (0-1),  default=0.0005
	TDV_UB_PARAM_LEAN_ANG_SMOOTHING,//Lean angle feature smoothing, valid values (0-1),  default=0.1
	TDV_UB_PARAM_HEAD_ANG_SMOOTHING,//Head angle feature smoothing, valid values (0-1),  default=0.1

	TDV_UB_PARAM_HAND_STATUS_STABILIZATION, //A value between 0 and 1, which indicate how stable the hand status is.
											//Larger value will cause the results to be more stable, but the lag
											// will be larger. default=0.05


	//Calibration parameters
	TDV_UB_PARAM_NO_CALIBRATION, //1- Override calibration requirements, 0 - Don't override calibration,default=1
									
	TDV_UB_PARAM_CALIB_STATIONARY_TORSO_Y_POS, //Torso y position when the person is standing still.
											  //This parameter is calculated during the calibration
	TDV_UB_PARAM_CALIB_STATIONARY_TORSO_Z_POS, //Torso z position when the person is standing still.
											  //This parameter is calculated during the calibration
	
	TDV_UB_PARAM_CALIB_CHANGE_THRESH,//Image change threshold for the calibration 0...1 where a higher value means that more changes are allowed.
									//default=0.5
	
	TDV_UB_PARAM_ENABLE_SIDE_HAND_DETECTION //1- enable, 0 - disable hand sideways detection. 
										    //In applications where sideways hand detection is not needed,
											//it can be disabled in order to get more stable results (and faster calculation).
											//Default=1

};


//Internal class;
class TDVSkeletonTracking;

class TDVUPPERBODY_DLL TDVUpperBodyTracking
{
public:
	TDVUpperBodyTracking(void);
	~TDVUpperBodyTracking(void);

	/****************************************************************
	Initialization/destruction functions
	*****************************************************************/

	//Initialize function must be called at least once before calling "ProcessFrame" function
	TDV_RESULT Initialize(int width,int height,int rgbPixelSize);

	void Destroy();

	/****************************************************************
	Parameter functions
	*****************************************************************/

	TDV_RESULT SetParameter(TDV_UB_PARAM param,double value);

	TDV_RESULT GetParameter(TDV_UB_PARAM param,double &value);

	TDV_RESULT GetParameterLimits(TDV_UB_PARAM param,double &min,double &max);


	/****************************************************************
	Calibration functions
	*****************************************************************/

	bool IsCalibrated();

	TDV_RESULT DoCalibration(const unsigned char* depthBuf,//Depth buffer
							 const unsigned char* irBuf_U,//Primary buffer
							 const unsigned char* irBuf_G=0,//Secondary buffer (not used)
							 const unsigned char* rgbBuf=0);//RGB buffer (not used)

	/****************************************************************
	Feature calculation/extraction functions
	*****************************************************************/

	TDV_RESULT ProcessFrame(const unsigned char* depthBuf,//Depth buffer
						    const unsigned char* irBuf_U, //Primary buffer
		                    const unsigned char* irBuf_G=0,//Secondary buffer (not used)
		                    const unsigned char* rgbBuf=0, //RGB buffer (not used)
							bool isPropDefined=false,         //Is a prop being held
							const unsigned char* propROI=0,//Binary mask of the prop
							bool isPropInLeftHand=true);	  //Is prop held in the left hand (false it is in the right hand)?

	// Get a detected feature value
	TDV_RESULT GetDetectedFeature1D(TDV_UB_FEATURE featId,float &x);

	// Get a detected feature in image coordinates
	TDV_RESULT GetDetectedFeature2D(TDV_UB_FEATURE featId,float &x,float &y);

	// Get a detected feature in world coordinates
	TDV_RESULT GetDetectedFeature3D(TDV_UB_FEATURE featId,float &x,float &y,float &z);

private:
	TDVSkeletonTracking* m_skeleton;

};
