//****************************************************************************
//
//  Copyright (C) 1997-2007 3DV SYSTEMS LTD.
//  All rights reserved.
//
//  Module Name:	DepthCameraExample.cpp
//  Date:			1-9-2007
//  Description:	implementation example demostrating the use of the CDepthCamera class
//  Updated By:		
//	Last Updated:			
//	Version:		1.00
//****************************************************************************

#include "DepthCamera.h"
#include <stdio.h>
#include <windows.h>
#include <Mmsystem.h>
#include <conio.h>
#include "..\code\tgaWriter.h"


void main()
{
	CDepthCamera depthCamera;
	if (!depthCamera.Initialize())
	{
		printf("Unable to initialize camera!... exiting...\n");
		return;
	}
	printf("Camera is connected!\n");

	// set the resolution - blocks until resolution change is complete
	depthCamera.ChangeResolution(RESOLUTION_320X240_30FPS);
	printf("Resolution set!\n");	

	// set default values for a selected depth range (distance=65 and width = 150, 
	// meaining a depth ranging from 65cm up to 215cm)
	depthCamera.SetDepthWindowPosition(65, 150);

	// initialize image buffers
	unsigned char * pDepth = NULL;
	unsigned char * pRGB = NULL;
	unsigned char * pRGBFullRes = NULL;
	unsigned char * pUngated = NULL;
	unsigned char * pGated = NULL;
	
	int iFramesToReceive = 100;
	int width, height, dSize, rgbSize, rgbFullResWidth, rgbFullResHeight;
	
	depthCamera.GetVideoSize(width, height, rgbFullResWidth, rgbFullResHeight, dSize, rgbSize);
	
	depthCamera.EnableDepthFilter(true); // turn filters on

	// lets capture a few images
	for (int i=0; i< iFramesToReceive; i++)
		depthCamera.GetNextFrame(pDepth, pRGB, pRGBFullRes, pUngated, pGated);

	// write the last video buffers that were received. We only receive buffers 
	// that were set to be transfered using EnableRGBFullResolution(...) or 
	// EnableIRVideoTransfer(...), otherwise their corresponding buffer pointers 
	// will be set to NULL.
	if (pDepth)
		writeTargaFile(pDepth, width, height, dSize, "depth.tga");
	if (pRGB)
		writeTargaFile(pRGB, width, height, rgbSize, "rgb.tga");
	if (pRGBFullRes)
		writeTargaFile(pRGBFullRes, rgbFullResWidth, rgbFullResHeight, rgbSize, "rgbFullRes.tga");

	printf("Press any key to exit...");
	getch(); // wait for key press to exit
}