//****************************************************************************
//
//  Copyright (C) 1997-2005 3DV SYSTEMS LTD.
//  All rights reserved.
//
//  Module Name:	SDKTest.cpp
//  Project Name: 
//  Written By:		Gil Zigelman
//  Date:			19-7-2005
//  Description:	Simple application demonstrating the use of the DMachine SDK.
//					The application displays a menu with a few numbered options. 
//					Each option selected performs the requested operationm
//  Updated By:		Gil Zigelman
//	Last Updated:	05-06-2007			
				
//****************************************************************************


 
#include <stdio.h>
#include <windows.h>
#include "TDVCameraInterface.h"
#include <conio.h>
#include "tgaWriter.h"

bool bCaptureImages = false;

// video callback function
void videoCallBack(unsigned char * pDepthBuf, unsigned char * pRgbBuf, 
				   int bufIndex, void* pObject)
{
	TDVCameraInterfaceBase *pCamera = (TDVCameraInterfaceBase *)pObject;
	if (!pCamera)
		return;

	unsigned char* pConfidenceMap = NULL;
	unsigned char* pUngated = NULL;
	unsigned char* pGated = NULL;
	unsigned char* pRGBFullRes	= NULL;
	unsigned char* pBackGround	= NULL;
	int iWidth=0, iHeight=0, iRgbSize=0, iDSize=0;
	int iRGBFullResWidth=0, iRGBFullResHeight=0, iRGBFullResSize=0;

	pCamera->getVideoSize(iWidth, iHeight, iRgbSize, iDSize);
	pCamera->getRGBFullResSize(iRGBFullResWidth, iRGBFullResHeight, iRGBFullResSize);

	if (iWidth > 0 && iHeight > 0)
	{
		//	printf("Received images width = %d    height = %d\n", iWidth, iHeight);
		// at this point, we can use iWidth, iHeight, iRgbSize, iDSize 
		// to build an image from the received buffers
		if (bCaptureImages)
		{
			bCaptureImages = false;
			writeTargaFile(pRgbBuf, iWidth, iHeight, iRgbSize, "rgb.tga");
			writeTargaFile(pDepthBuf, iWidth, iHeight, iDSize, "depth.tga");
			pCamera->getVideoBuffer(BUFFER_TYPE_UNGATED, pUngated);
			pCamera->getVideoBuffer(BUFFER_TYPE_GATED, pGated);
			pCamera->getVideoBuffer(BUFFER_TYPE_CONFIDENCE, pConfidenceMap);
			pCamera->getVideoBuffer(BUFFER_TYPE_RGB_FULL_RES, pRGBFullRes);
			pCamera->getVideoBuffer(BUFFER_TYPE_BACKGROUND, pBackGround);
			if (pBackGround)
				writeTargaFile(pBackGround,  iWidth, iHeight, iDSize, "background.tga");
			if (pConfidenceMap)
				writeTargaFile(pConfidenceMap,  iWidth, iHeight, iDSize, "confidence.tga");
			if (pUngated)
				writeTargaFile(pUngated, iWidth, iHeight, iDSize, "Ungated.tga");
			if (pGated)
				writeTargaFile(pGated, iWidth, iHeight, iDSize, "Gated.tga");
			if (pRGBFullRes)
				writeTargaFile(pRGBFullRes, iRGBFullResWidth, iRGBFullResHeight, iRGBFullResSize, "RGBFullRes.tga");
		}
	}
}

// Command callback function
void cmdCallBack(int cmd, void* pObject)
{
	/*
	we only receive command if they value parameter is 
	different than currently set value

	only when the main DMachine application sends a message, we can 
	be sure the value was set in the camera. for example, when turning on the
	illumincation, only when CMD_ILLUMINCATION_ON is returned, the value
	was set in the camera.
	*/

	TDVCameraInterfaceBase *pCamera = (TDVCameraInterfaceBase *)pObject;
	if (!pCamera)
		return;

	// at first run, or when the resolution has changed, we need to 
	// re-initialize the videos that we want to be processed
	if (cmd == CMD_CAMERA_RESOLUTION)
	{
		// for this sample we want to get the primary and secondary
		pCamera->setCameraCommand(CMD_USING_IR_INPUT, 1);

		// for this sample we also want to get the confidence map
		pCamera->setCameraCommand(CMD_USING_CONFIDENCE_MAP, 1);

		// for this sample we also want to capture full resolution RGB
		pCamera->setCameraCommand(CMD_RGB_FULL_RES, 1);
	};
}


//	Print an options menu
void PrintMenu()
{
	printf("Choose one of the following options:\n");
	printf("1. Toggle illumination on\\off\n");
	printf("2. Find object\n");
	printf("3. Toggle automatic brightness\n");
	printf("4. Move primary window 50cm forward\n");
	printf("5. Move primary window 50cm backward\n");
	printf("6. Save setting in index 5\n");
	printf("7. Switch resolution between 320x240 and 160x120\n");
	printf("8. Capture Images\n");
	printf("9. Exit...\n");
}

// wait for the user to select an option, and adjust the camera
// setting accordingly.
bool AnalyzeMenu(TDVCameraInterfaceBase* pCameraInterface)
{
	bool rc = false;
	int value=0, min=0, max = 0;
	unsigned char c = getch();
	int menuItem = c-'0';
	if (menuItem <1 || menuItem>9)
	{
		printf("invalid selection!\n");
		return rc;
	}
	switch (menuItem)
	{
	case 1:
		{
			pCameraInterface->getCameraCommandVal(CMD_ILLUMINATION_ON, value);
			if (value)
			{
				pCameraInterface->setCameraCommand(CMD_ILLUMINATION_ON, 0);
				printf("Illumination turned off\n");
			}
			else
			{
				pCameraInterface->setCameraCommand(CMD_ILLUMINATION_ON, 1);
				printf("Illumination turned on\n");
			}
		}
		break;
	case 2: 
		{
			pCameraInterface->getCameraCommandVal(CMD_ILLUMINATION_ON, value);
			if (value == 0)
			{
				printf("Illumination must be turned on before automatic find can be performed!\n");
			}
			else
			{
				printf("Starting to find object...\n");
				pCameraInterface->setCameraCommand(CMD_AUTO_FIND, 1);
				value = 0;
				while (value==0)
				{
					Sleep(500);
					pCameraInterface->getCameraCommandVal(CMD_AUTO_FIND_COMPLETED, value);
				}
				printf("Object Found!\n");
			}
		}
		break;
	case 3:
		{
			pCameraInterface->getCameraCommandVal(CMD_AUTO_BRIGHTNESS, value);
			if (value)
			{
				pCameraInterface->setCameraCommand(CMD_AUTO_BRIGHTNESS, 0);
				printf("Automatic Brightness turned off\n");
			}
			else
			{
				pCameraInterface->setCameraCommand(CMD_AUTO_BRIGHTNESS, 1);
				printf("Automatic Brightness turned on\n");
			}
		}
		break;
	case 4:
		{
			pCameraInterface->getCameraCommandVal(CMD_PRIMARY_DISTANCE, value);
			pCameraInterface->getCameraCommandLimits(CMD_PRIMARY_DISTANCE, min, max);
			value += 50;
			if (value > max)
				value = max;
			pCameraInterface->setCameraCommand(CMD_PRIMARY_DISTANCE, value);
			printf("Ungated distance set to %d\n", value);
		}
		break;
	case 5:
		{
			pCameraInterface->getCameraCommandVal(CMD_PRIMARY_DISTANCE, value);
			pCameraInterface->getCameraCommandLimits(CMD_PRIMARY_DISTANCE, min, max);
			int dist = pCameraInterface->getUngatedDist();
			value -= 50;
			if (value < min)
				value = min;
			pCameraInterface->setCameraCommand(CMD_PRIMARY_DISTANCE, value);
			printf("Ungated distance set to %d\n", value);
		}
		break;
	case 6:
		{
			pCameraInterface->setCameraCommand(CMD_SAVE_STATUS, 4);
			printf("Status 5 saved\n");
		}
		break;
	case 7:
		{
			pCameraInterface->getCameraCommandVal(CMD_CAMERA_RESOLUTION, value);
			if (value == RESOLUTION_160X120_60FPS)
				pCameraInterface->setCameraCommand(CMD_CAMERA_RESOLUTION, RESOLUTION_320X240_30FPS);
			else if (value == RESOLUTION_320X240_30FPS)
				pCameraInterface->setCameraCommand(CMD_CAMERA_RESOLUTION, RESOLUTION_160X120_60FPS);
			printf("Changing resolution changed to: %s...\n", (value==RESOLUTION_160X120_60FPS)?"320x240":"160x120");
		}
		break;	
	case 8: 
		bCaptureImages = true;
		break;
	case 9:
		rc = true;
		break;
	}
	return rc;
}


void main()
{
	// initialize the camera interface and callback functions
	TDVCameraInterfaceBase* pCameraInterface = new TDVCameraInterfaceBase();
	pCameraInterface->setCommandCallBack(cmdCallBack, pCameraInterface);
	pCameraInterface->setVideoCallBack(videoCallBack, pCameraInterface);

	
	while (!pCameraInterface->isCommActive())
	{
		printf("Comunication is not yet active...\n");
		Sleep(1000);
	}


	while (!pCameraInterface->isVideoActive())
	{
		printf("Video is not yet active...\n");
		Sleep(1000);
	}


	// we know now that the communication and video are active
	// lets display the menu options

	bool exitProgram = false;
	while (!exitProgram)
	{
		PrintMenu();
		exitProgram = AnalyzeMenu(pCameraInterface);
	}

	// disconnect the client from the DMachine application
	delete pCameraInterface;

}