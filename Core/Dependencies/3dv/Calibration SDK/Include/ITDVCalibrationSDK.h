#ifndef _ITDVCALIBRATIONSDK
#define _ITDVCALIBRATIONSDK

#ifdef ZCAM_CALIBRATION_EXPORTS
#define ZCAM_CALIBRATION_DLL __declspec(dllexport)
#else
#define ZCAM_CALIBRATION_DLL __declspec(dllimport)
#endif

#include <vector>
class TDVCalibrationProcess;
class TDVCalibrationSDK;
class TDVCameraInterfaceBase;





enum TDVCalibrationStages
{
	DEPTH_WINDOW_CALIBRATION,
	OBSTACLE_DETECTION,
	VIDEO_VERIFICATION,
	INVALID
};

enum TDVStageStatus
{
	COMPLETED,
	IN_PROCESS,
	IN_PROCESS_WAITING_FOR_USER,
	CALIBRATION_FAILED,
	CALIBRATION_FAILED_BACKGROUND_TOO_CLOSE,
	NOT_INITIALIZED,
	WAITING_FOR_RESOLUTION_320x240,
	WAITING_FOR_RESOLUTION_160x120
};

enum TDVTriggerStatus
{
	OFF,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	ON,
	REPEL = -10
};

enum TDVFrequency
{
	FREQ_50Hz = 50,
	FREQ_60HZ = 60
};

struct TDVHotSpot 
{
	int x,y,radius;
	bool bActive; //True if used by the current calibration stage.
};

struct TDVUserPosition 
{
	TDVUserPosition():x(0.f),y(0.f),z(0.f){};
	TDVUserPosition(float X,float Y, float Z):x(X),y(Y),z(Z){};
	float x,y,z;
};


struct TDVRect 
{
	TDVRect():x(0.f),y(0.f),width(0.f),height(0){};
	TDVRect(float X,float Y, float WIDTH,float HEIGHT):x(X),y(Y),width(WIDTH),height(HEIGHT){};
	float x,y;
	float width,height;
};


class ZCAM_CALIBRATION_DLL ITDVCalibrationSDK
{
public:
	ITDVCalibrationSDK(void);
	~ITDVCalibrationSDK(void);


	void					Initialize(TDVCameraInterfaceBase *pCamera,TDVFrequency freq);
	
	void					ProcessFrame(char* pDbuf,char* pUngatedbuf,char* pGatedbuf,char* pCbuf,const int& videowidth,const int& videoheight);
	//Set the current calibration routine.
	void					SetCurrentStage(TDVCalibrationStages StageID);

	//Returns the ID of the current calibration routine.
	TDVCalibrationStages	GetCurrentStage();
	
	//Returns the state of the trigger. The higher the state the higher the confidence of a trigger signal.
	int						GetTriggerState();

	//Forces the state of the trigger to specified value.
	//void					SetTriggerState(bool state);

	//Returns the area where the trigger sweep is expected.
	TDVRect					GetTriggerActiveArea();

	//The ID of the next calibration stage according to the default procedure flow.
	TDVCalibrationStages	GetSuggestedStage();

	//Returns the position and size of the area the Calibration SDK assumes the user's head to be 
	//positioned at if the returned objects "active" member variable is true.
	TDVHotSpot				GetHeadHotSpot();

	//Determines if they're disturbing obstacles around the user.
	bool					ExistObstacles();

	//Provides a map of the in the video disturbing objects.
	char*					GetDisturbingObjectsMap();

	//Provides a map of the light sources visible in the video. 
	char*					GetDisturbingIlluminationMap();

	//Returns the status of the current calibration procedure.
	TDVStageStatus			GetCurrentStatus();

	//Returns the status a specific calibration procedure.
	TDVStageStatus			GetStatus(TDVCalibrationStages stageID);

	//Checks if the current calibration procedure has been completed.
	inline bool				IsCurrentStageCompleted();

	//Returns the normalized position of the user. Center of frame in XY and center of the depth window is represented by (0,0,0);
	TDVUserPosition			GetUserPositionStatus();

	bool					UpdateExist();

	//If bUseGesture is true, trigger is released by user gesture, otherwise after "FramesToTrigger" number of frames have elapsed. 
	void					GestureTrigger(bool bUseGesture = true, int FramesToTrigger = 100);

	//Manual triggering - Sends a single trigger command.
	void					ManualTrigger();


private:
	void								Main();
	TDVCalibrationSDK					*m_pCalibrationSDK;
};

#endif _ITDVCALIBRATIONSDK
