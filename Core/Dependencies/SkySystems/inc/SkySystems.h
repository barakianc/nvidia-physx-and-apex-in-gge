// SkySystems.h

#pragma once

using namespace System;

#include "Ogre.h"
#include "OgreMaterial.h"
#include "OgreTexture.h"
#include <vector>

namespace SkySystems {

	/*!
	* \mainpage GGE ProceduralClouds Add-On
	* \version 1.0
	* \date 25 November 2007
	*
	* \section intro_sec Introduction
	*
	* This is the documentation for the Procedural Clouds Add-On for GGE. Click the links
	* above to navigate.
	*/

	/*!
	* \brief The procedural clouds encapsulation.
	*
	* This class provides the application with a realistic, dynamic, procedurally
	* generated cloud material. The application only needs to deal with 3 methods:
	* init, update, and getMaterialName.
	*
	* Usage is simple:
	*
	*	1). Allocate a ProceduralClouds object.
	*
	*	2). Call init() with the desired parameters.
	*
	*	3). Get the generated material with getMaterialName() and apply it to some object.
	*
	*	4). Call update() every frame.
	*
	* Parameters can be adjusted in real-time with the other methods, but this is
	* optional usage.
	*/
	class ProceduralClouds
	{
	public:
		ProceduralClouds();
		virtual ~ProceduralClouds() {}

		/*!
		* \brief Initialize all textures and materials. Create starting octaves.
		* \param updateInterval The cloud formation update interval (in seconds).
		* \param cloudCover 0 = no clouds, 1 = max clouds
		* \param cloudSharpness 0 = 'dull' edges, 1 = 'sharp' edges
		* \param scrollU Cloud scrolling in u-direction.
		* \param scrollV Cloud scrolling in v-direction.
		* \return True if initialization successful, false otherwise.
		*/
		bool init(float updateInterval, float cloudCover, float cloudSharpness, float scrollU, float scrollV);

		/*!
		* \brief Animate the material (cloud motion and formation).
		* \param timeSinceLastFrame ms
		*/
		void update(Ogre::Real timeSinceLastFrame);

		/*!
		* \brief Allows the application to get a handle to the cloud material.
		*
		* TODO: For future development, we will want to get rid of the .material file, and
		* just do everything in code. The material name should be generated in-code,
		* and verified to be unique with some unique ID.
		*
		* \return The name of the material created.
		*/
		const Ogre::String &getMaterialName();

		/*!
		* \brief Affects formation speed. # of seconds (> 0).
		*/
		void setCloudFormationUpdateInterval(float delta);
		float getCloudFormationUpdateInterval() { return m_updateInterval; }

		/*!
		* \brief Cloud scrolling speed in U direction. (> 0).
		*/
		void setCloudScrollU(float delta);
		float getCloudScrollU() { return m_scrollU; }

		/*!
		* \brief Cloud scrolling speed in V direction. (> 0).
		*/
		void setCloudScrollV(float delta);
		float getCloudScrollV() { return m_scrollV; }

		/*!
		* \brief Cloud cover. 0 (no clouds) to 1 (max clouds).
		*/
		void setCloudCover(float delta);
		float getCloudCover() { return m_cloudCover; }

		/*!
		* \brief Cloud 'sharpness'. 0 (fluffy) to 1 (hard edges).
		*/
		void setCloudSharpness(float delta);
		float getCloudSharpness() { return m_cloudSharpness; }

		/*!
		* \brief Allows us to cycle through some of the different cloud effects.
		*/
		void setCloudMode(int delta);
		int getCloudMode() { return m_cloudMode; }

	private:
		/*!
		* \brief Creates a manual texture of some width/height, and returns it.
		*/
		Ogre::TexturePtr createTexture(Ogre::String name, int w, int h);

		/*!
		* \brief Generate noise for the given texture, overwriting existing data.
		*
		* If prevTexture is supplied, save out texture to prevTexture before
		* generating new values.
		*/
		void generateNoise(Ogre::TexturePtr texture, Ogre::TexturePtr prevTexture);

		/*!
		* \brief Assign the sky some color.
		*
		* TODO: Sky gradients? Or do this entirely in shader?
		*/
		void generateSkyColor(Ogre::TexturePtr skyTexture);

		/*!
		* \brief LERPs between tex1 and tex2 by t. Write results to dest.
		*/
		void interpolateNoise(float t, Ogre::TexturePtr tex1, Ogre::TexturePtr tex2, Ogre::TexturePtr dest);

		Ogre::MaterialPtr m_cloudMat;

		// The textures.
		Ogre::TexturePtr m_skyColor;	//!< TODO: we could have a current/previous sky to do fading!?
		Ogre::TexturePtr m_tex_Octave1, m_tex_Octave1_cur, m_tex_Octave1_pre;
		Ogre::TexturePtr m_tex_Octave2, m_tex_Octave2_cur, m_tex_Octave2_pre;
		Ogre::TexturePtr m_tex_Octave3, m_tex_Octave3_cur, m_tex_Octave3_pre;
		Ogre::TexturePtr m_tex_Octave4, m_tex_Octave4_cur, m_tex_Octave4_pre;

		float m_updateInterval;		//!< Seconds between octave updates.
		float m_scrollU, m_scrollV;	//!< Scroll the cloud texture along u/v.
		float m_cloudCover;			//!< Cloud cover (density).
		float m_cloudSharpness;		//!< "Sharpness" of cloud outline.

		int m_cloudMode;			//!< Different cloud effects.
		const int NUM_MODES;		//!< Must match the # of cases in the shader.
	};


	//! Pure virtual class
	/*!
	All of the different objects are used to represent skies fall under the category of a SkyQuad. This interface class
	forces all skies to share a similar interface, and allows for a single list of mixed sky types.
	*/
	class SkyQuad
	{
	public:
		//! Pure virtual interface function needed to update the position of the SkyQuad.
		/*!
		To maintain the illusion of a distant sky, the SkyQuad must always remain at a fixed distance from the viewer.
		All SkyQuad types must implement this function to ensure the illusion is maintained.
		\param cam The Camera around which the SkyQuad will be centered.
		*/
		virtual void updatePosition(Ogre::Camera* cam) = 0;
		//! Retrieves the a pointer to the material that has been applied to the SkyQuad
		virtual void setMaterial(Ogre::MaterialPtr mat){material = mat;}
		//! Adds a rotation to the SkyQuad's current orientation
		/*! 
		This has not been tested and we are not sure if this is how it works with Ogre.
		*/
		virtual void addRotation(const Ogre::Quaternion& rot){orientation + rot;}
		//! Retrieves a pointer to the Ogre::SceneNode being used by the SkyQuad.
		virtual Ogre::SceneNode* getSceneNode(){return node;}

	protected:
		Ogre::Real distance;
		Ogre::Quaternion orientation;
		Ogre::MaterialPtr material;
		Ogre::SceneNode* node;
	};
	//! An Implementation of a SkyQuad.
	/*!
	The SkyDome is a set of 5 usually curved planes that give the illusion of a dome. The SkyDome is ideal for 
	representing large far reaching skies with uniform textures, unlike the SkyBox's warped and segmented textures.
	*/
	class SkyDome : public SkyQuad
	{
	public:
		/*!
		The Constructor for the SkyDome creates a new SkyDome mesh, applies it to a node, and registers the node 
		with the Ogre::SceneManager provided it.
		\param name The name of this particular SkyDome. The name needs to be unique to the scene.
		\param dist The distance the SkyDome is to be created away from the viewer. This distance will always remain constant
		\param orient The orientation of the SkyDome. This is most often set by the user to Ogre::Quaternion::IDENTITY
		\param matName The name of the Material to be applied to the SkyDome.
		\param smgr A pointer to the SceneManager this SkyDome is to be associated with.
		\param curve The curvature of the planes that make up the SkyDome. Larger values give the feeling of more closeness, while larger values give the feeling of "Big Sky"
		*/
		SkyDome(Ogre::String name, Ogre::Real dist, const Ogre::Quaternion& orient, Ogre::String matName, Ogre::SceneManager* smgr, Ogre::Real curve = 10); 
		//! Centers the dome around the camera, also puts the dome in the correct render queue.
		virtual void updatePosition(Ogre::Camera* cam);

	protected:
		Ogre::Entity* ent[5];
	};

	//! Deprecated.
	/* 
	There are currently some issues with the SkyBox class. Materials assigned to it are not applied properly. Due to lack
	of use, the problems with the SkyBox were never solved
	*/
	class SkyBox : public SkyQuad
	{
	public:
		SkyBox(Ogre::Real dist, const Ogre::Quaternion& orient, Ogre::MaterialPtr mat, Ogre::SceneManager* smgr);
		virtual void updatePosition(Ogre::Camera* cam);

	protected:
		Ogre::Entity* ent[6];
	};

	//! An implementation of a SkyPlane
	/*!
	SkyPlanes are ideal for having things far off in the distance that do not take up the full field of view, 
	such as the Sun or Moon, a flock of birds, or a far off mountain. SkyPlanes are also ideal for mostly 
	indoor scenes where some of the sky can be seen, but only a portion.
	*/
	class SkyPlane : public SkyQuad
	{
	public:
		/*!
		\param name The name of this particular SkyDome. The name needs to be unique to the scene.
		\param dist The distance the SkyDome is to be created away from the viewer. This distance will always remain constant
		\param normal The normal vector of the plane, this provides the orientation for the plane.
		\param bow The curvature of the plane.
		\param matName The name of the Material to be applied to the SkyDome.
		\param smgr A pointer to the SceneManager this SkyDome is to be associated with.
		\param scale The size of the plane. Because anywhere from a small SkyPlane for an airplane flying through the clouds, to an expanse of clouds in itself could be needed, scaling is important.
		*/
		SkyPlane(Ogre::String name, Ogre::Real dist, Ogre::Vector3 normal, Ogre::Real bow, Ogre::String matName, Ogre::SceneManager* smgr, Ogre::Real scale = 1);
		virtual void updatePosition(Ogre::Camera* cam);
	protected:
		Ogre::Entity* ent;
	};

	//! This management class helps to handle any number of SkyQuads of all types.
	class SkySystem
	{
	public:
		//! This Default constructor is for initialization purposes only
		/*!
		Because the class that can call SkySystems doesn't necessarily have a pointer to a SceneManager at startup
		this constructor allows for at least the creation of a SkySystem at this point, although unable to
		to do some of the management.
		*/
		SkySystem();

		//! The main constructor.
		/*! 
		Much of the management the SkySystem requires the SceneManager, so this is the main constructor.
		*/
		SkySystem(Ogre::SceneManager* smgr);

		//! Destructor clears out the memory that belongs to the SkySystem
		~SkySystem();

		//! Updates all of the SkyQuads
		/*!
		Uses the updatePosition function common to all SkyQuads to update regardless of type.
		\param cam The Camera needed to pass to the the updatePositions for SkyQuad centering
		\param evt The Event information from the RenderQueue provides the time step.
		*/
		void update(Ogre::Camera* cam, Ogre::FrameEvent evt);

		//! Adds a new SkyQuad to be handled.
		void addSky(SkyQuad* sky){mSkies.push_back(sky);}

		//! Linear search for matching pointer. O(n). Removes the Sky.
		void removeSky(SkyQuad* sky);

		//! Direct remove of indexed item O(1).
		void removeSky(unsigned int index);

		//! Turns on simulation of Time of Day.
		void setSimulateSky(bool simulate){if(mSceneManager != NULL) mSimulateSky = simulate;}

		//! Tells if Time of Day is being simulated.
		/*!
		\returns True if Time of Day is being simulated.
		*/
		bool getSimulateSky(){return mSimulateSky;}

		//! Sets the SceneManager pointer for SkySystem
		void setSceneManager(Ogre::SceneManager* smgr){mSceneManager = smgr;}

		//! Retrieves the a pointer to the SceneManager
		Ogre::SceneManager* getSceneManager(){return mSceneManager;}

		//! Sets the play speed of the Time of Day simulation. 
		/*!
		\param speed A multiplier to change the speed of simulation. By default the simulation runs at a multiplier of 1, or real time.
		*/
		void setPlaySpeed(Ogre::Real speed){mPlaySpeed = speed;}

		//! Adding a sun into the system is treated a special way because of the Time of Day.
		void addSunPlane(SkyPlane* sun, Ogre::Vector3 sunPos);

		//! Like addSunPlane, addMoonPlane is a special case because of the Time of Day, and is handled as such.
		void addMoonPlane(SkyPlane* moon, Ogre::Vector3 moonPos);

		//! Adds the special case of a Cloud Dome. 
		/*!
		\returns A convenient little pointer to the ProceduralClouds object, so we can easily adjust parameters.
		*/
		ProceduralClouds *addCloudDome(Ogre::String name, Ogre::Real distance, float updateInterval, float cloudCover, float cloudSharpness, float scrollU, float scrollV);

	private:
		void								simulateTimeOfDay(Ogre::FrameEvent evt);


		bool								mSimulateSky;
		bool								mHasSun;
		bool								mHasMoon;
		std::vector<SkyQuad* >				mSkies;
		Ogre::Light							mSun;
		Ogre::Light							mMoon;
		Ogre::Real							mTime;//between 0 and 24 
		Ogre::Vector3						mSunPosition;
		Ogre::Vector3						mMoonPosition;
		Ogre::Real							mSunDistance;
		Ogre::Real							mMoonDistance;
		Ogre::SceneManager*					mSceneManager;
		unsigned int						mSunPlaneIndex;
		unsigned int						mMoonPlaneIndex;
		Ogre::Light*						mSunLight;
		Ogre::Light*						mMoonLight;
		Ogre::Real							mPlaySpeed;
		ProceduralClouds					mProceduralClouds;
		bool								mProcClouds;

	};
}
