demoAudioVideoModule is similar to the DirectShow texture demo from the AudioVideoModule (AVM) minus FMOD sound track.  
There's no demo about about the Theora technology included with the installer (although necessary files are here).  
To access the full module and all 6 demos see gge\ogreaddons\AudioVideoModule or 
http://motorola.usc.edu/svn/gge/ogreaddons/AudioVideoModule/

Find the full AVM documentation at: 
http://motorola.usc.edu/svn/gge/ogreaddons/AudioVideoModule/docs/AudioVideoModuleDocumentation.pdf