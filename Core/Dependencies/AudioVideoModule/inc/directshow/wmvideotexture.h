#pragma once

#include "wmvideoplugin.h"
#include "OgreString.h"
#include "OgreExternalTextureSource.h"
#include "OgreFrameListener.h"

namespace Ogre
{

class _CWMVideoTexture;

class WMVIDEOPLUGIN_EXP CWMVideoTexture : public Ogre::ExternalTextureSource, public Ogre::FrameListener
{
public:
	CWMVideoTexture(void);
	virtual ~CWMVideoTexture(void);

	//Pure virtual functions that plugins must Override
	/** Call this function from manager to init system */
	virtual bool initialise();
	/** Shuts down PlugIn */
	virtual void shutDown();

	/** Creates a texture into an already defined material or one that is created new
	(it's up to plugin to use a material or create one)
	Before calling, ensure that needed params have been defined via the stringInterface
	or regular methods */
	virtual void createDefinedTexture(const Ogre::String& sMaterialName,
		const Ogre::String& groupName = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	/** What this destroys is dependent on the plugin... See specific plugin
	doc to know what is all destroyed (normally, plugins will destroy only
	what they created, or used directly - ie. just texture unit) */
	virtual void destroyAdvancedTexture(const Ogre::String& sTextureName,
		const Ogre::String& groupName = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	void start			(const Ogre::String& sMaterialName);
	void pause			(const Ogre::String& sMaterialName);
	void stop			(const Ogre::String& sMaterialName);
	void rewind			(const Ogre::String& sMaterialName);
	void restart		(const Ogre::String& sMaterialName);
	void setRate		(const Ogre::String& sMaterialName, double rate);
	void setVolume		(const Ogre::String& sMaterialName, int volume);
	void setBalance		(const Ogre::String& sMaterialName, int balance);
	void volumeDown		(const Ogre::String& sMaterialName);
	void volumeUp		(const Ogre::String& sMaterialName);

	//! Called from Ogre during frame event - update texture
	bool frameStarted(const Ogre::FrameEvent& evt);

	class CmdVideoWidth : public ParamCommand
    {
    public:
		Ogre::String doGet(const void* target) const;
        void doSet(void* target, const Ogre::String& val);
    };
	class CmdVideoHeight : public ParamCommand
    {
    public:
		Ogre::String doGet(const void* target) const;
        void doSet(void* target, const Ogre::String& val);
    };
private:
	static CmdVideoWidth  s_cmdVideoWidth;
	static CmdVideoHeight s_cmdVideoHeight;
	_CWMVideoTexture * m;
	long dVolume;
};

}