#pragma once

#include <streams.h>

struct __declspec(uuid("{DE2E3CF7-60B9-4735-A8E3-EEFD3F38F925}")) CLSID_TextureRenderer;

namespace Ogre
{

class CTextureRenderer : public CBaseVideoRenderer
{
public:
    CTextureRenderer(LPUNKNOWN pUnk,HRESULT *phr);
    ~CTextureRenderer();

public:
    HRESULT CheckMediaType(const CMediaType *pmt );     // Format acceptable?
    HRESULT SetMediaType  (const CMediaType *pmt );       // Video format notification
    HRESULT DoRenderSample(IMediaSample *pMediaSample); // New video sample
   
    LONG m_lVidWidth;   // Video width
    LONG m_lVidHeight;  // Video Height
    LONG m_lVidPitch;   // Video Pitch
	BYTE m_buffer[2048*2048*4];
};

}
