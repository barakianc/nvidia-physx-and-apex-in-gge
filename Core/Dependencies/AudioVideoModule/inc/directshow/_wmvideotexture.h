#pragma once

#include "OgreTexture.h"
#include "wmvideograph.h"
#include <map>

namespace Ogre
{

class CWMVideoTextureItem
{
public:
	CWMVideoGraph    graph;
	Ogre::TexturePtr pTexture;
	int width;
	int height;
	bool bLoop;
};

class _CWMVideoTexture
{
public:
	int width;
	int height;
	std::map<Ogre::String, CWMVideoTextureItem *> textures;
};

}