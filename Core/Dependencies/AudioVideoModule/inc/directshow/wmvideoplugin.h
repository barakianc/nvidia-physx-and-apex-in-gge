#pragma once

#if OGRE_PLATFORM == PLATFORM_WIN32 
#   ifdef WMVIDEO_PLUGIN_EXPORTS 
#       define WMVIDEOPLUGIN_EXP __declspec(dllexport) 
#   else 
#       define WMVIDEOPLUGIN_EXP __declspec(dllimport) 
#   endif 
#else 
#   define WMVIDEOPLUGIN_EXP 
#endif 
