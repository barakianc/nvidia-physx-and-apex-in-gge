#pragma once

#include "OgreString.h"
#include <dshow.h>
#include <qedit.h>
#include "texturerenderer.h"

namespace Ogre
{

class CTextureRenderer;

class CWMVideoGraph
{
public:
	CWMVideoGraph(void);
	virtual ~CWMVideoGraph(void);

	void Initialise(const char * pszUri);
	void Start     ();
	void Pause     ();
	void Stop      ();
	void Rewind    ();
	void Restart    ();
	void SetRate (double rate);
	void SetVolume (int volume);
	void SetBalance (int balance);
	BOOL IsRunning ();
	void Shutdown  ();

	void GetFrame  (BYTE * pBuffer, UINT unLength);
private:
	//void Start     ();
private:
	Ogre::String            m_strUri;
	CComPtr<IGraphBuilder>  m_pGraph;
	CComPtr<IMediaControl>  m_pControl;
	CComPtr<IMediaPosition>  m_pPosition;
	//CComPtr<IVideoWindow>   m_pVideoWindow;
	CComPtr<ISampleGrabber> m_pGrabber;
	CTextureRenderer m_grabber;
	CComPtr<IBaseFilter>   m_pGrabberFilter;
	//HWND                    m_hWnd;
	BOOL                    m_bIsRunning;
	int						dVolume;
};

}