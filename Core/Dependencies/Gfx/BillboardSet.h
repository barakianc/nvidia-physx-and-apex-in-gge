#pragma once

#include "Ogre.h"
//#include "OgreBillboardSet.h"

namespace GamePipe
{
	class BillboardSet : public Ogre::BillboardSet
	{
	public:
							BillboardSet( const Ogre::String &name );
		virtual				~BillboardSet(void);

		//-- inherited from MovabledObject
		virtual void		_updateRenderQueue(Ogre::RenderQueue* queue);

		virtual void		setPoolSize(size_t size);


		void				beginBillboards(size_t numBillboards = 0);

		void				injectBillboard(const Ogre::Billboard& bb);

        void				endBillboards(void);

	protected:
		
		void				genVertices(const Ogre::Vector3* const offsets, 
										const Ogre::Billboard &billboard);
	
	private:
        
		void				_createBuffers(void);
        void				_destroyBuffers(void);

        bool				mBuffersCreated;

		size_t				mPoolSize;

	};
}