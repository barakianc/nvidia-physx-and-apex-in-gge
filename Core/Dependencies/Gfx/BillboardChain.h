#pragma once
#include "Ogre.h"
//#include "OgreBillboardChain.h"

namespace GamePipe
{
	class BillboardChain : public Ogre::BillboardChain
	{
	public:
						BillboardChain( const Ogre::String &name );
						~BillboardChain(void);

	protected:

		virtual void	setupVertexDeclaration(void);
			
		virtual void	updateVertexBuffer(Ogre::Camera* cam);

		void			ComputeTangentSpace(	const Element &current,
												const Element &next,
												const Ogre::Vector3 &currentPerpendicular,
												const Ogre::Vector3 &nextPerpendicular,
												Ogre::Vector3 &tangent,
												Ogre::Vector3 &binormal,
												Ogre::Vector3 &normal );
	};

}
