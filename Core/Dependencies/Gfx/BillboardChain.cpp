#include "StdAfx.h"
#include "BillboardChain.h"

GamePipe::BillboardChain::BillboardChain( const Ogre::String &name )
:	Ogre::BillboardChain( name )
{
}

GamePipe::BillboardChain::~BillboardChain(void)
{
}

void GamePipe::BillboardChain::setupVertexDeclaration(void)
{
	if (mVertexDeclDirty)
	{
		Ogre::VertexDeclaration* decl = mVertexData->vertexDeclaration;
		decl->removeAllElements();

		size_t offset = 0;
		// Add a description for the buffer of the positions of the vertices
		decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
		offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
		//--- additional stuff begin ---------------------------------------
		decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_TANGENT);
		offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
		decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_BINORMAL);
		offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
		decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
		offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
		//--- additional stuff end -----------------------------------------

		if (mUseVertexColour)
		{
			decl->addElement(0, offset, Ogre::VET_COLOUR, Ogre::VES_DIFFUSE);
			offset += Ogre::VertexElement::getTypeSize(Ogre::VET_COLOUR);
		}

		if (mUseTexCoords)
		{
			decl->addElement(0, offset, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES);
			offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT2);
		}

		if (!mUseTexCoords && !mUseVertexColour)
		{
			Ogre::LogManager::getSingleton().logMessage(
				"Error - BillboardChain '" + mName + "' is using neither "
				"texture coordinates or vertex colours; it will not be "
				"visible on some rendering APIs so you should change this "
				"so you use one or the other.");
		}
		mVertexDeclDirty = false;
	}
}
		
void GamePipe::BillboardChain::updateVertexBuffer(Ogre::Camera* cam)
{
	setupBuffers();
	Ogre::HardwareVertexBufferSharedPtr pBuffer =
		mVertexData->vertexBufferBinding->getBuffer(0);
	void* pBufferStart = pBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);

	const Ogre::Vector3& camPos = cam->getDerivedPosition();
	Ogre::Vector3 eyePos = mParentNode->_getDerivedOrientation().Inverse() *
		(camPos - mParentNode->_getDerivedPosition()) / mParentNode->_getDerivedScale();

	Ogre::Vector3 chainTangent;
	for (ChainSegmentList::iterator segi = mChainSegmentList.begin();
		segi != mChainSegmentList.end(); ++segi)
	{
		ChainSegment& seg = *segi;

		// Skip 0 or 1 element segment counts
		if (seg.head != SEGMENT_EMPTY && seg.head != seg.tail)
		{
			size_t laste = seg.head;
			for (size_t e = seg.head; ; ++e) // until break
			{
				// Wrap forwards
				if (e == mMaxElementsPerChain)
					e = 0;

				Element& elem = mChainElementList[e + seg.start];
				assert (((e + seg.start) * 2) < 65536 && "Too many elements!");
				Ogre::uint16 baseIdx = static_cast<Ogre::uint16>((e + seg.start) * 2);

				// Determine base pointer to vertex #1
				void* pBase = static_cast<void*>(
					static_cast<char*>(pBufferStart) +
						pBuffer->getVertexSize() * baseIdx);

				// Get index of next item
				size_t nexte = e + 1;
				if (nexte == mMaxElementsPerChain)
					nexte = 0;

				if (e == seg.head)
				{
					// No laste, use next item
					chainTangent = mChainElementList[nexte + seg.start].position - elem.position;
				}
				else if (e == seg.tail)
				{
					// No nexte, use only last item
					chainTangent = elem.position - mChainElementList[laste + seg.start].position;
				}
				else
				{
					// A mid position, use tangent across both prev and next
					chainTangent = mChainElementList[nexte + seg.start].position - mChainElementList[laste + seg.start].position;
				}

				Ogre::Vector3 vP1ToEye = eyePos - elem.position;
				Ogre::Vector3 vPerpendicular = chainTangent.crossProduct(vP1ToEye);
				vPerpendicular.normalise();
				vPerpendicular *= Ogre::Real(elem.width * 0.5f);

				//--- added stuff begin -------------------------------------
				Ogre::Vector3 tangent;
				Ogre::Vector3 binormal;
				Ogre::Vector3 normal;
				
				{
					size_t nextnext = nexte + 1;
					if ( nextnext == mMaxElementsPerChain )
						nextnext = 0;

					Ogre::Vector3 nextTangent;

					if ( nexte == seg.head )
					{
						// No laste, use next item
						nextTangent = mChainElementList[nextnext + seg.start].position - mChainElementList[nexte + seg.start].position;
					}
					else if ( nexte == seg.tail )
					{
						// No nexte, use only last item
						nextTangent = mChainElementList[nexte + seg.start].position - mChainElementList[e + seg.start].position;
					}
					else
					{
						// A mid position, use tangent across both prev and next
						nextTangent = mChainElementList[nextnext + seg.start].position - mChainElementList[e + seg.start].position;
					}

					Ogre::Vector3 vP2ToEye = eyePos - mChainElementList[nexte + seg.start].position;
					Ogre::Vector3 vNextPerpendicular = nextTangent.crossProduct(vP2ToEye);
					vNextPerpendicular.normalise();
					vNextPerpendicular *= Ogre::Real(mChainElementList[nexte + seg.start].width * 0.5f);

					Element *pCurrent = &elem;
					Element *pNext = &mChainElementList[nexte + seg.start];

					if ( nexte < e )
					{
						pCurrent = &mChainElementList[nexte + seg.start];
						pNext = &elem;
					}

					ComputeTangentSpace(	*pCurrent,
											*pNext,
											vPerpendicular,
											vNextPerpendicular,
											tangent,
											binormal,
											normal );
				}
				//--- added stuff end ---------------------------------------

				Ogre::Vector3 pos0 = elem.position - vPerpendicular;
				Ogre::Vector3 pos1 = elem.position + vPerpendicular;

				float* pFloat = static_cast<float*>(pBase);
				// pos1
				*pFloat++ = pos0.x;
				*pFloat++ = pos0.y;
				*pFloat++ = pos0.z;

				//--- additional tangent space begin ------------------------
				*pFloat++ = tangent.x;
				*pFloat++ = tangent.y;
				*pFloat++ = tangent.z;
				*pFloat++ = binormal.x;
				*pFloat++ = binormal.y;
				*pFloat++ = binormal.z;
				*pFloat++ = normal.x;
				*pFloat++ = normal.y;
				*pFloat++ = normal.z;
				//--- additional tangent space end --------------------------

				pBase = static_cast<void*>(pFloat);

				
				if (mUseVertexColour)
				{
					Ogre::RGBA* pCol = static_cast<Ogre::RGBA*>(pBase);
					Ogre::Root::getSingleton().convertColourValue(elem.colour, pCol);
					pCol++;
					pBase = static_cast<void*>(pCol);
				}

				if (mUseTexCoords)
				{
					pFloat = static_cast<float*>(pBase);
					if (mTexCoordDir == TCD_U)
					{
						*pFloat++ = elem.texCoord;
						*pFloat++ = mOtherTexCoordRange[0];
					}
					else
					{
						*pFloat++ = mOtherTexCoordRange[0];
						*pFloat++ = elem.texCoord;
					}
					pBase = static_cast<void*>(pFloat);
				}

				// pos2
				pFloat = static_cast<float*>(pBase);
				*pFloat++ = pos1.x;
				*pFloat++ = pos1.y;
				*pFloat++ = pos1.z;

				//--- additional tangent space begin ------------------------
				*pFloat++ = tangent.x;
				*pFloat++ = tangent.y;
				*pFloat++ = tangent.z;
				*pFloat++ = binormal.x;
				*pFloat++ = binormal.y;
				*pFloat++ = binormal.z;
				*pFloat++ = normal.x;
				*pFloat++ = normal.y;
				*pFloat++ = normal.z;
				//--- additional tangent space end --------------------------

				pBase = static_cast<void*>(pFloat);

				
				if (mUseVertexColour)
				{
					Ogre::RGBA* pCol = static_cast<Ogre::RGBA*>(pBase);
					Ogre::Root::getSingleton().convertColourValue(elem.colour, pCol);
					pCol++;
					pBase = static_cast<void*>(pCol);
				}

				if (mUseTexCoords)
				{
					pFloat = static_cast<float*>(pBase);
					if (mTexCoordDir == TCD_U)
					{
						*pFloat++ = elem.texCoord;
						*pFloat++ = mOtherTexCoordRange[1];
					}
					else
					{
						*pFloat++ = mOtherTexCoordRange[1];
						*pFloat++ = elem.texCoord;
					}
					pBase = static_cast<void*>(pFloat);
				}

				if (e == seg.tail)
					break; // last one

				laste = e;

			} // element
		} // segment valid?

	} // each segment

	pBuffer->unlock();
}

void GamePipe::BillboardChain::ComputeTangentSpace
(
	const Element &current,
	const Element &next,
	const Ogre::Vector3 &currentPerpendicular,
	const Ogre::Vector3 &nextPerpendicular,
	Ogre::Vector3 &tangent,
	Ogre::Vector3 &binormal,
	Ogre::Vector3 &normal
)
{
	Ogre::Vector3 pos0 = current.position - currentPerpendicular;
	Ogre::Vector3 pos1 = current.position + currentPerpendicular;
	Ogre::Vector3 pos2 = next.position - nextPerpendicular;
	Ogre::Vector3 pos3 = next.position + nextPerpendicular;

	float uv1[2];
	float uv2[2];

	if (mTexCoordDir == TCD_U)
	{
		uv1[0] = current.texCoord;
		uv1[1] = mOtherTexCoordRange[0];
		uv2[0] = next.texCoord;
		uv2[1] = mOtherTexCoordRange[1];
	}
	else
	{
		uv1[0] = mOtherTexCoordRange[0];
		uv1[1] = current.texCoord;
		uv2[0] = mOtherTexCoordRange[1];
		uv2[1] = next.texCoord;
	}

	normal = Ogre::Math::calculateBasicFaceNormal( pos0, pos1, pos2 );

	tangent = Ogre::Math::calculateTangentSpaceVector(	pos0, pos1, pos2,
														uv1[0], uv1[1], 
														uv1[0], uv2[1],
														uv2[0], uv1[1] );

	binormal = normal.crossProduct( tangent );
	binormal.normalise();
}