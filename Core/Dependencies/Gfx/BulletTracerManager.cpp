#include "StdAfx.h"
#include "BulletTracerManager.h"
#include "Engine.h"

using namespace std;

GamePipe::BulletTracerManager::BulletTracerManager(void)
:	m_BillboardChain( "BulletTracerManager" ),
	m_nBulletTracerCount( 0 )
{
	m_BillboardChain.setNumberOfChains( MAX_BULLET_TRACER );
	m_BillboardChain.setMaxChainElements(2);

	for (int i=MAX_BULLET_TRACER;i--;)
	{
		m_BulletTracerBuffer[i].m_nChainIndex = i;

		m_FreeList.push_back( &m_BulletTracerBuffer[i] );
	}
}

GamePipe::BulletTracerManager::~BulletTracerManager(void)
{
	//no need to delete anything because everything is locally allocated
}

void GamePipe::BulletTracerManager::AttachToNode( Ogre::SceneNode *pNode )
{
	//m_BillboardChain.detatchFromParent();

	pNode->attachObject( &m_BillboardChain );
}

void GamePipe::BulletTracerManager::SetMaterial( const Ogre::String &materialName )
{
	m_BillboardChain.setMaterialName( materialName );
}

void GamePipe::BulletTracerManager::Update()
{
	BulletTracerList::iterator i = m_ActiveList.begin();
	BulletTracerList::iterator end = m_ActiveList.end();

	while ( i != end )
	{
		BulletTracer *pBulletTracer = *i++;

		if ( pBulletTracer->Update() )
		{
			//this one is done, recycle it
			m_BillboardChain.removeChainElement( pBulletTracer->m_nChainIndex );
			m_BillboardChain.removeChainElement( pBulletTracer->m_nChainIndex );
						
			m_ActiveList.remove( pBulletTracer );
			m_FreeList.push_back( pBulletTracer );
			m_nBulletTracerCount--;
		}
		else
		{
			m_BillboardChain.updateChainElement(	pBulletTracer->m_nChainIndex,
													0,
													pBulletTracer->GetHead() );
													
			m_BillboardChain.updateChainElement(	pBulletTracer->m_nChainIndex,
													1,
													pBulletTracer->GetTail() );
		}
	}
}

bool GamePipe::BulletTracerManager::CreateBulletTracer
(
	const Ogre::Vector3 &begin,
	const Ogre::Vector3 &end,
	float fWidth,
	float fBulletSpeed,
	float fFadeTime
)
{
	if ( m_nBulletTracerCount >= MAX_BULLET_TRACER ) return false;

	BulletTracer *pNewTracer = m_FreeList.back();

	pNewTracer->m_vBegin = begin;
	pNewTracer->m_vPos = begin;
	pNewTracer->m_vEnd = end;
	pNewTracer->m_fWidth = fWidth;
	pNewTracer->m_fSpeed = fBulletSpeed;
	pNewTracer->m_fHeadFadeTime = fFadeTime;
	pNewTracer->m_fTailFadeTime = fFadeTime;
	pNewTracer->m_fFadeDuration = fFadeTime;

	m_BillboardChain.addChainElement(	pNewTracer->m_nChainIndex,
										pNewTracer->GetHead() );
	m_BillboardChain.addChainElement(	pNewTracer->m_nChainIndex,
										pNewTracer->GetTail() );

	m_ActiveList.push_back( pNewTracer );
	m_FreeList.pop_back();

	m_nBulletTracerCount++;

	return true;
}

bool GamePipe::BulletTracerManager::BulletTracer::Update()
{
	float fDt = EnginePtr->GetDeltaTime();

	Ogre::Vector3 vDir = m_vEnd - m_vBegin;
	vDir.normalise();

	m_vPos += vDir * m_fSpeed * fDt;

	m_fTailFadeTime = max( m_fTailFadeTime - fDt, 0.0f );

	if ( vDir.dotProduct(m_vPos - m_vEnd) >= 0.0f )
	{
		m_vPos = m_vEnd;

		m_fHeadFadeTime -= fDt;

		return m_fHeadFadeTime <= 0.0f;
	}

	return false;
}

Ogre::BillboardChain::Element 
GamePipe::BulletTracerManager::BulletTracer::GetHead()
{
	BillboardChain::Element head;
	
	head.position = m_vPos;
	head.width = m_fWidth;
	head.texCoord = 0.0f;

	float alpha = m_fHeadFadeTime / m_fFadeDuration;
	
	head.colour = Ogre::ColourValue(alpha,alpha,alpha,alpha);

	return head;
}

Ogre::BillboardChain::Element
GamePipe::BulletTracerManager::BulletTracer::GetTail()
{
	Ogre::Vector3 vTotalDisp = m_vEnd - m_vBegin;
	Ogre::Vector3 vDisp = m_vPos - m_vBegin;

	BillboardChain::Element tail;
	tail.position = m_vBegin;
	tail.width = .5f * m_fWidth;
	tail.texCoord = vDisp.length() / vTotalDisp.length();

	float alpha = m_fTailFadeTime / m_fFadeDuration;
	
	tail.colour = Ogre::ColourValue(alpha,alpha,alpha,alpha);

	return tail;
}
