#include "StdAfx.h"
#include "RenderTargetTexture.h"
//#include "Ogre.h"


GamePipe::RenderTargetTexture					GamePipe::RenderTargetTexture::ms_Singleton;
GamePipe::RenderTargetTexture::CmdTextureName	GamePipe::RenderTargetTexture::ms_CmdTextureName;

GamePipe::RenderTargetTexture & GamePipe::RenderTargetTexture::Get()
{
	return ms_Singleton;
}

bool GamePipe::RenderTargetTexture::initialise()
{
	if ( createParamDictionary( mDictionaryName ) )
	{
        Ogre::ParamDictionary* dict = getParamDictionary();
		
		dict->addParameter(	Ogre::ParameterDef("texture_name", 
			 "leave this empty to use first added render target"
			 , Ogre::PT_STRING),
			&ms_CmdTextureName );
	}

	return true;
}
void GamePipe::RenderTargetTexture::shutDown()
{
}

void GamePipe::RenderTargetTexture::createDefinedTexture
(
	const Ogre::String& sMaterialName,
	const Ogre::String& groupName
)
{
	Ogre::MaterialManager &materialMan = Ogre::MaterialManager::getSingleton();

	Ogre::MaterialPtr materialPtr = materialMan.getByName(sMaterialName);
	Ogre::Technique * pTechnique = materialPtr->getTechnique(mTechniqueLevel);
	Ogre::Pass * pPass = pTechnique->getPass(mPassLevel);
	Ogre::TextureUnitState * pTextureUnit = pPass->getTextureUnitState(mStateLevel);

	if ( m_TextureName.empty() )
	{
		m_TextureName = m_FirstTextureName;
	}

	pTextureUnit->setTextureName( m_TextureName );

	MaterialClient client;
	client.material = materialPtr;
	client.techniqueLevel = mTechniqueLevel;
	client.passLevel = mPassLevel;
	client.stateLevel = mStateLevel;
	
	m_ClientList.push_back(client);

	m_TextureName.clear();
}
		
void GamePipe::RenderTargetTexture::destroyAdvancedTexture
(
	const Ogre::String& sTextureName,
	const Ogre::String& groupName
)
{

}

void GamePipe::RenderTargetTexture::AddRenderTarget( Ogre::Camera *pCamera, const Ogre::String &textureName )
{
	RenderTargetMap::iterator i = m_RenderTargetMap.find(textureName);

	Ogre::RenderTexture *pRenderTexture;

	if ( i == m_RenderTargetMap.end() )
	{
		Ogre::RenderWindow * pRenderWindow = EnginePtr->GetRenderWindow();

		Ogre::TextureManager &texMan = Ogre::TextureManager::getSingleton();

		Ogre::TexturePtr pTexture = texMan.createManual(textureName,
														m_GroupName,
														Ogre::TEX_TYPE_2D,
														pRenderWindow->getWidth(),
														pRenderWindow->getHeight(),
														pRenderWindow->getColourDepth(),
														0,
														Ogre::PF_R8G8B8A8,
														Ogre::TU_RENDERTARGET );

		pTexture->createInternalResources();

		Ogre::HardwarePixelBufferSharedPtr buffer = pTexture->getBuffer();

		pRenderTexture = buffer->getRenderTarget();

		if ( m_RenderTargetMap.empty() )
		{
			m_FirstTextureName = textureName;
		}

		m_RenderTargetMap[textureName] = pRenderTexture;
	}
	else
	{
		pRenderTexture = i->second;
	}

	pRenderTexture->removeAllViewports();

	Ogre::Viewport *pViewport = pRenderTexture->addViewport( pCamera );
	pViewport->setOverlaysEnabled(false);
	
	pRenderTexture->setAutoUpdated(false);

	//-- register our external texture source plugin
	static bool s_bDoOnce = true;
	if ( s_bDoOnce )
	{
		Ogre::ExternalTextureSourceManager &externalTexSrcMan = 
			Ogre::ExternalTextureSourceManager::getSingleton();

		externalTexSrcMan.setExternalTextureSource(m_PluginType, this);

		s_bDoOnce = false;
	}
}

void GamePipe::RenderTargetTexture::ClearRenderTarget()
{
	Ogre::TextureManager &texMan = Ogre::TextureManager::getSingleton();

	RenderTargetMap::iterator i = m_RenderTargetMap.begin();
	RenderTargetMap::iterator end = m_RenderTargetMap.end();

	while ( i != end )
	{
		texMan.remove( i->first );
		++i;
	}

	m_RenderTargetMap.clear();
}

void GamePipe::RenderTargetTexture::PreRender()
{
	int clientListSize = m_ClientList.size();

	std::vector<bool> colourWriteValues( clientListSize );
	std::vector<bool> depthWriteValues( clientListSize );
	std::vector<bool> depthCheckValues( clientListSize );

	for (int i=clientListSize;i--;)
	{
		MaterialClient &client = m_ClientList[i];

		Ogre::Technique * pTechnique = client.material->getTechnique(client.techniqueLevel);
		Ogre::Pass * pPass = pTechnique->getPass(client.passLevel);
		
		colourWriteValues[i] = pPass->getColourWriteEnabled();
		depthWriteValues[i] = pPass->getDepthWriteEnabled();
		depthCheckValues[i] = pPass->getDepthCheckEnabled();

		pPass->setColourWriteEnabled( false );
		pPass->setDepthWriteEnabled( false );
		pPass->setDepthCheckEnabled( true );
	}

	//-------------------------------------------------------------------
	RenderTargetMap::iterator i = m_RenderTargetMap.begin();
	RenderTargetMap::iterator end = m_RenderTargetMap.end();

	while ( i != end )
	{
		i->second->update();
		++i;
	}
	//-------------------------------------------------------------------
	
	for (int i=clientListSize;i--;)
	{
		MaterialClient &client = m_ClientList[i];

		Ogre::Technique * pTechnique = client.material->getTechnique(client.techniqueLevel);
		Ogre::Pass * pPass = pTechnique->getPass(client.passLevel);
		
		pPass->setColourWriteEnabled( colourWriteValues[i] );
		pPass->setDepthWriteEnabled( depthWriteValues[i] );
		pPass->setDepthCheckEnabled( depthCheckValues[i] );
	}
}

GamePipe::RenderTargetTexture::RenderTargetTexture(void)
{
	mPluginName = "RenderTargetTexture";
	mDictionaryName = "render_target";

	m_GroupName = "GamePipeGroup";
	m_PluginType = "gamepipe_rendertarget";
}

GamePipe::RenderTargetTexture::~RenderTargetTexture(void)
{

}

Ogre::String GamePipe::RenderTargetTexture::CmdTextureName::doGet(const void* target) const
{
	const RenderTargetTexture *pRT = static_cast<const RenderTargetTexture*>(target);

	return pRT->m_TextureName;
}

void GamePipe::RenderTargetTexture::CmdTextureName::doSet(void* target, const Ogre::String& val)
{
	RenderTargetTexture *pRT = static_cast<RenderTargetTexture*>(target);

	pRT->m_TextureName = val;
}