#pragma once
#include "Engine.h"
//#include "Ogre.h"
#include "OgreExternalTextureSourceManager.h"
/*

#include "OgreTexture.h"
#include "OgreMaterial.h"
*/
namespace GamePipe
{

	class RenderTargetTexture : public Ogre::ExternalTextureSource
	{
	public:
		static RenderTargetTexture	&	Get();
		
		//-- ExternalTextureSource functions -------------------
		virtual bool	initialise();
		virtual void	shutDown();

		virtual void	createDefinedTexture(	const Ogre::String& sMaterialName,
												const Ogre::String& groupName = 
												Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
		
		virtual void	destroyAdvancedTexture(	const Ogre::String& sTextureName,
												const Ogre::String& groupName = 
												Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );

		//------------------------------------------------------
		void			AddRenderTarget( Ogre::Camera *pCamera, const Ogre::String &textureName );
		
		void			ClearRenderTarget();

		void			PreRender();

	protected:

		struct MaterialClient
		{
			Ogre::MaterialPtr	material;
			int					techniqueLevel;
			int					passLevel;	
			int					stateLevel;
		};

		typedef std::map< Ogre::String, Ogre::RenderTexture* > RenderTargetMap;
		typedef std::vector< MaterialClient > ClientList;

		//------------------------------------------------------
		Ogre::String	m_GroupName;
		Ogre::String	m_PluginType;
		Ogre::String	m_TextureName;
		Ogre::String	m_FirstTextureName;

		RenderTargetMap	m_RenderTargetMap;

		// we keep track of each material who uses this external texture
		ClientList		m_ClientList;

	private:
		class CmdTextureName : public Ogre::ParamCommand
        {
        public:
			Ogre::String doGet(const void* target) const;
			void doSet(void* target, const Ogre::String& val);
        };

		//-----------------------------------------------------------
		static RenderTargetTexture	ms_Singleton;
		static CmdTextureName		ms_CmdTextureName;

							RenderTargetTexture(void);
		virtual				~RenderTargetTexture(void);
	};
}
