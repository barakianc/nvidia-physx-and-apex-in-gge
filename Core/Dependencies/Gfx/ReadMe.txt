//--------------------------------------------------------------------------------------------------------------------
// Various Material Usage
//--------------------------------------------------------------------------------------------------------------------

Material_BulletTracer

- This material uses shader that requires tangent space information.

- Create an object of type GamePipe::BillboardChain and setMaterial to "Material_BulletTracer."

- If you want to apply this material on an ogre mesh, the ogre mesh must be exported with tangent space information.

- For best result, entity that uses Material_BulletTracer should be rendered last (after alpha blended objects).

- Shader Constants under BulletTracer_VS_CG:

-- worldViewProjMat <= modelview projection matrix of the scene, do not change.

-- normalMat <= inverse transpose of the modelview matrix, do not change.

- Shader Constants under BulletTracer_PS_CG:

-- eta <= ratio of refractive index, .7504 is the ratio between water and air.

-- thickness <= the thickness of the bullet tracer, increasing this value increases uv offset caused by refraction.

-- verticalexp <= this controls the blending between the top and bottom edge of the tracer with the scene.

-- falloffexp <= this controls the blending between the head and tail of the tracer.

Material_HeatShimmer

- This material uses shader that requires tangent space information.

- Create an object of type GamePipe::BillboardSet and setMaterial to "Material_HeatShimmer"

- If you want to apply this material on an ogre mesh, the ogre mesh must be exported with tangent space information.

- For best result, entity that uses Material_HeatShimmer should be rendered last (after alpha blended objects).

- Shader Constants under HeatShimmer_VS_CG:

-- worldViewProjMat <= modelview projection matrix of the scene, do not change.

-- time <= variable for temporal effects, do not change.

-- scroll1~3 <= float2 that controls the uv scrolling for the shimmer effect.

-- scale1~3 <= float2 that controls the uv scrolling scale for the shimmer effect.

- Shader Constants under HeatShimmer_PS_CG:

-- distortion1~3 <= float2 that controls the distortion amount corresponding to the uv scrolling in the vertex shader.

-- scale <= float2 that controls the blending between the effect and the scene in the x and y direction.

Material_ProceduralFire

- Simply call setMaterial on an entity of your choice.

- Shader Constants under ProceduralFire_VS_CG:

-- worldViewProjMat <= modelview projection matrix of the scene, do not change.

-- time <= variable for temporal effects, do not change.

-- scroll1~3 <= float2 that controls the uv scrolling for the fire effect.

-- scale1~3 <= float2 that controls the uv scrolling scale for the fire effect.

- Shader Constants under ProceduralFire_PS_CG:

-- distortion1~3 <= float2 that controls the distortion amount corresponding to the uv scrolling in the vertex shader.

-- scalebias <= float2 that controls the scale (x) and bias (y) of the height based blending factor.

-- smokecolor <= color of the smoke.

Material_ProceduralSun

- Simply call setMaterial on an entity of your choice.

- Shader Constants under ProceduralSun_PS_CG:

-- time <= variable for temporal effects, do not change.

-- distortscale <= float that controls the distortion toward the edge of the sun.

Compositor_RadialBlur

- Simply enable/disable through compositor manager.

- Shader Constants under RadialBlur_PS_CG:

-- time <= variable for temporal effects, do not change.

-- center <= float2 that indicates the center of the radial blur effect.

//--------------------------------------------------------------------------------------------------------------------
// RenderTargetTexture Usage
//--------------------------------------------------------------------------------------------------------------------

- RenderTargetTexture allows you to use the screenbuffer as an external texture in ogre material script.

- In you texture_unit section of your material script, use texture_source gamepipe_rendertarget to indicate the use of screenbuffer as external texture.

- You may use "texture_name" within the texture_source block to indicate the name of the render texture (render target) to use.

- Not specifying "texture_name" will default to the first render target being added via RenderTargetTexture::AddRenderTarget.

- Inside your code, you must call GamePipe::RenderTargetTexture::Get().AddRenderTarget before your material loads.

- In your draw function, you must call GamePipe::RenderTargetTexture::Get().PreRender();

//--------------------------------------------------------------------------------------------------------------------
// BulletTracerManager Usage
//--------------------------------------------------------------------------------------------------------------------

- BulletTracerManager allows you to add bullet tracer effects in the world with just one function call.

- Inside your init function, you must call BulletTracerManager::AttachToNode to add bullet tracers into the scene graph.

- Unless you have a different material in mind, call BulletTracerManager::SetMaterial( "Material_BulletTracer" );

- In your update function, you must call m_BulletTracerManager.Update();

- Call BulletTracerManager::CreateBulletTracer anytime to create a bullet tracer!

//---------------------------------------------------------------------------------------------------------------------
// GamePipe::BillboardSet and GamePipe::BillboardChain Usage
//---------------------------------------------------------------------------------------------------------------------

- GamePipe::BillboardSet and GamePipe::BillboardChain works exactly the same as Ogre::BillboardSet and Ogre::BillboardChain, the only difference is 
  GamePipe::BillboardSet and GamePipe::BillboardChain provides shaders with tangent space information.

- Instantiate GamePipe::BillboardSet or GamePipe::BillboardChain using the new operator.

- Add them to the scene graph by calling a scene node's attachObject function.

- Access Normal, Tangent and Binormal in your shader and it will not be garbage.