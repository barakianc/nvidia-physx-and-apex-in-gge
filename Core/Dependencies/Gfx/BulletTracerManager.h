#pragma once

#include "BillboardChain.h"
#include <list>

namespace GamePipe
{
	class BulletTracerManager
	{
	public:
		// Do you really need more than this many bullets visible?
		const static int	MAX_BULLET_TRACER = 128;

						BulletTracerManager(void);
						~BulletTracerManager(void);

		void			AttachToNode( Ogre::SceneNode *pNode );

		void			SetMaterial( const Ogre::String &materialName );	

		void			Update();

		bool			CreateBulletTracer(	const Ogre::Vector3 &begin,
											const Ogre::Vector3 &end,
											float fWidth,
											float fBulletSpeed,
											float fFadeTime );

	private:

		GamePipe::BillboardChain	m_BillboardChain;

		size_t						m_nBulletTracerCount;

		struct BulletTracer
		{
			bool					Update();

			BillboardChain::Element	GetHead();
			BillboardChain::Element	GetTail();

			Ogre::Vector3			m_vBegin, m_vEnd;

			Ogre::Vector3			m_vPos;	
			
			float					m_fWidth;

			float					m_fSpeed;
			
			float					m_fHeadFadeTime;
			float					m_fTailFadeTime;

			float					m_fFadeDuration;

			size_t					m_nChainIndex;

		}							m_BulletTracerBuffer[MAX_BULLET_TRACER];

		typedef std::list<BulletTracer*>	BulletTracerList;

		BulletTracerList			m_FreeList;	//allocate from freelist
		BulletTracerList			m_ActiveList; //update the activelist
	};

}