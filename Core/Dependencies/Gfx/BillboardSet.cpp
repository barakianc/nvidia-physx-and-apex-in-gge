#include "StdAfx.h"
#include "BillboardSet.h"

/*
#include "OgreBillboard.h"
#include "OgreLogManager.h"
#include "OgreHardwareBufferManager.h"
#include "OgreRoot.h"
*/
GamePipe::BillboardSet::BillboardSet(const Ogre::String &name)
:	Ogre::BillboardSet(name),
	mBuffersCreated( false ),
	mPoolSize( 20 )
{
	setPoolSize( mPoolSize );
}

GamePipe::BillboardSet::~BillboardSet(void)
{
	_destroyBuffers();
}

void GamePipe::BillboardSet::_updateRenderQueue(Ogre::RenderQueue* queue)
{
	// GamePipe Billboards won't support external data
	//if (!mExternalData)
    {
		if (mSortingEnabled)
        {
            _sortBillboards(mCurrentCamera);
        }

        beginBillboards(mActiveBillboards.size());
        ActiveBillboardList::iterator it;
        for(it = mActiveBillboards.begin();
            it != mActiveBillboards.end();
            ++it )
        {
            injectBillboard(*(*it));
        }
        endBillboards();
    }

    //only set the render queue group if it has been explicitly set.
    if( mRenderQueueIDSet )
    {
		queue->addRenderable(this, mRenderQueueID);
    } else {
		queue->addRenderable(this);
    }
}

void GamePipe::BillboardSet::setPoolSize(size_t size)
{
	// If we're driving this from our own data, allocate billboards
    //if (!mExternalData)
    {
        // Never shrink below size()
        size_t currSize = mBillboardPool.size();
        if (currSize >= size)
            return;

        this->increasePool(size);

        for( size_t i = currSize; i < size; ++i )
        {
            // Add new items to the queue
            mFreeBillboards.push_back( mBillboardPool[i] );
        }
    }

    mPoolSize = size;
}

void GamePipe::BillboardSet::beginBillboards(size_t numBillboards)
{
	/* Generate the vertices for all the billboards relative to the camera
       Also take the opportunity to update the vertex colours
       May as well do it here to save on loops elsewhere
    */

    /* NOTE: most engines generate world coordinates for the billboards
       directly, taking the world axes of the camera as offsets to the
       center points. I take a different approach, reverse-transforming
       the camera world axes into local billboard space.
       Why?
       Well, it's actually more efficient this way, because I only have to
       reverse-transform using the billboardset world matrix (inverse)
       once, from then on it's simple additions (assuming identically
       sized billboards). If I transformed every billboard center by it's
       world transform, that's a matrix multiplication per billboard
       instead.
       I leave the final transform to the render pipeline since that can
       use hardware TnL if it is available.
    */

    // create vertex and index buffers if they haven't already been
    if(!mBuffersCreated)
        _createBuffers();

	// Only calculate vertex offets et al if we're not point rendering
	if (!mPointRendering)
	{
		// Get offsets for origin type
		getParametricOffsets(mLeftOff, mRightOff, mTopOff, mBottomOff);

		// Generate axes etc up-front if not oriented per-billboard
		if (mBillboardType != Ogre::BBT_ORIENTED_SELF &&
			mBillboardType != Ogre::BBT_PERPENDICULAR_SELF && 
			!(mAccurateFacing && mBillboardType != Ogre::BBT_PERPENDICULAR_COMMON))
		{
			genBillboardAxes(&mCamX, &mCamY);

			/* If all billboards are the same size we can precalculate the
			   offsets and just use '+' instead of '*' for each billboard,
			   and it should be faster.
			*/
			genVertOffsets(mLeftOff, mRightOff, mTopOff, mBottomOff,
				mDefaultWidth, mDefaultHeight, mCamX, mCamY, mVOffset);

		}
	}

    // Init num visible
    mNumVisibleBillboards = 0;

    // Lock the buffer
	if (numBillboards) // optimal lock
	{
		// clamp to max
		//numBillboards = std::min(mPoolSize, numBillboards);
		numBillboards = std::min(getPoolSize(), numBillboards);

		size_t billboardSize;
		if (mPointRendering)
		{
			// just one vertex per billboard (this also excludes texcoords)
			billboardSize = mMainBuf->getVertexSize();
		}
		else
		{
			// 4 corners
			billboardSize = mMainBuf->getVertexSize() * 4;
		}
		assert (numBillboards * billboardSize <= mMainBuf->getSizeInBytes());

		mLockPtr = static_cast<float*>(
			mMainBuf->lock(0, numBillboards * billboardSize, 
			Ogre::HardwareBuffer::HBL_DISCARD) );
	}
	else // lock the entire thing
		mLockPtr = static_cast<float*>(
			mMainBuf->lock(Ogre::HardwareBuffer::HBL_DISCARD) );
}

void GamePipe::BillboardSet::injectBillboard(const Ogre::Billboard &bb)
{
	// Don't accept injections beyond pool size
	//if (mNumVisibleBillboards == mPoolSize) return;
	if (mNumVisibleBillboards == getPoolSize()) return;

	// Skip if not visible (NB always true if not bounds checking individual billboards)
    if (!billboardVisible(mCurrentCamera, bb)) return;

    if (!mPointRendering &&
		(mBillboardType == Ogre::BBT_ORIENTED_SELF ||
        mBillboardType == Ogre::BBT_PERPENDICULAR_SELF ||
        (mAccurateFacing && mBillboardType != Ogre::BBT_PERPENDICULAR_COMMON)))
    {
        // Have to generate axes & offsets per billboard
        genBillboardAxes(&mCamX, &mCamY, &bb);
    }

	// If they're all the same size or we're point rendering
    if( mAllDefaultSize || mPointRendering)
    {
        /* No per-billboard checking, just blast through.
        Saves us an if clause every billboard which may
        make a difference.
        */

        if (!mPointRendering &&
			(mBillboardType == Ogre::BBT_ORIENTED_SELF ||
       		mBillboardType == Ogre::BBT_PERPENDICULAR_SELF ||
       		(mAccurateFacing && mBillboardType != Ogre::BBT_PERPENDICULAR_COMMON)))
        {
            genVertOffsets(mLeftOff, mRightOff, mTopOff, mBottomOff,
                mDefaultWidth, mDefaultHeight, mCamX, mCamY, mVOffset);
        }
        genVertices(mVOffset, bb);
    }
    else // not all default size and not point rendering
    {
		Ogre::Vector3 vOwnOffset[4];
        // If it has own dimensions, or self-oriented, gen offsets
        if (mBillboardType == Ogre::BBT_ORIENTED_SELF ||
            mBillboardType == Ogre::BBT_PERPENDICULAR_SELF ||
            bb.hasOwnDimensions() ||
            (mAccurateFacing && mBillboardType != Ogre::BBT_PERPENDICULAR_COMMON))
        {
            // Generate using own dimensions
            genVertOffsets(mLeftOff, mRightOff, mTopOff, mBottomOff,
                bb.getOwnWidth(), bb.getOwnHeight(), mCamX, mCamY, vOwnOffset);
            // Create vertex data
            genVertices(vOwnOffset, bb);
        }
        else // Use default dimension, already computed before the loop, for faster creation
        {
            genVertices(mVOffset, bb);
        }
    }
    // Increment visibles
    mNumVisibleBillboards++;
}

void GamePipe::BillboardSet::endBillboards(void)
{
	mMainBuf->unlock();
}

void GamePipe::BillboardSet::genVertices
(
	const Ogre::Vector3* const offsets, 
	const Ogre::Billboard &bb
)
{
	// Colors
	Ogre::RGBA colour;
    Ogre::Root::getSingleton().convertColourValue(bb.mColour, &colour);
	Ogre::RGBA* pCol;

    // Texcoords
    assert( bb.isUseTexcoordRect() || bb.getTexcoordIndex() < mTextureCoords.size() );
    const Ogre::FloatRect & r =
        bb.isUseTexcoordRect() ?
		bb.getTexcoordRect() : mTextureCoords[bb.getTexcoordIndex()];

	// Tangent Space
	Ogre::Vector3 normal;
	Ogre::Vector3 tangent;
	Ogre::Vector3 binormal;

	if (mPointRendering)
	{
		// Single vertex per billboard, ignore offsets
		// position
        *mLockPtr++ = bb.mPosition.x;
        *mLockPtr++ = bb.mPosition.y;
        *mLockPtr++ = bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
		pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
		// Update lock pointer
		mLockPtr = static_cast<float*>(static_cast<void*>(pCol));

		//I am crashing here because I don't know what a point billboard's
		//tangent space should be, if you want to use it, figure it out.
		//don't just simply comment out this crash.
		*(int*)NULL = NULL;

		// Normal
		*mLockPtr++ = 0.0f;;
        *mLockPtr++ = 0.0f;
        *mLockPtr++ = 0.0f;
		// Tangent
		*mLockPtr++ = 0.0f;
        *mLockPtr++ = 0.0f;
        *mLockPtr++ = 0.0f;
		// Binormal
		*mLockPtr++ = 0.0f;
        *mLockPtr++ = 0.0f;
        *mLockPtr++ = 0.0f;
		
        // No texture coords in point rendering
	}
	else if (mAllDefaultRotation || bb.mRotation == Ogre::Radian(0))
    {
		normal = Ogre::Math::calculateBasicFaceNormal(
						offsets[1],
						offsets[0],
						offsets[2] );

		tangent = Ogre::Math::calculateTangentSpaceVector(
						offsets[1],
						offsets[0],
						offsets[2],
						r.right, r.top, r.left, r.top, r.left, r.bottom );
		
		binormal = normal.crossProduct( tangent );
		binormal.normalise();

        // Left-top
        // Positions
        *mLockPtr++ = offsets[0].x + bb.mPosition.x;
        *mLockPtr++ = offsets[0].y + bb.mPosition.y;
        *mLockPtr++ = offsets[0].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.left;
        *mLockPtr++ = r.top;

        // Right-top
        // Positions
        *mLockPtr++ = offsets[1].x + bb.mPosition.x;
        *mLockPtr++ = offsets[1].y + bb.mPosition.y;
        *mLockPtr++ = offsets[1].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.right;
        *mLockPtr++ = r.top;

        // Left-bottom
        // Positions
        *mLockPtr++ = offsets[2].x + bb.mPosition.x;
        *mLockPtr++ = offsets[2].y + bb.mPosition.y;
        *mLockPtr++ = offsets[2].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.left;
        *mLockPtr++ = r.bottom;

        // Right-bottom
        // Positions
        *mLockPtr++ = offsets[3].x + bb.mPosition.x;
        *mLockPtr++ = offsets[3].y + bb.mPosition.y;
        *mLockPtr++ = offsets[3].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.right;
        *mLockPtr++ = r.bottom;
    }
    else if (mRotationType == Ogre::BBR_VERTEX)
    {
        // TODO: Cache axis when billboard type is BBT_POINT or BBT_PERPENDICULAR_COMMON
        Ogre::Vector3 axis = (offsets[3] - offsets[0]).crossProduct(offsets[2] - offsets[1]).normalisedCopy();

        Ogre::Quaternion rotation(bb.mRotation, axis);
        Ogre::Vector3 pt;

		// implement tangent space calculation here, then you may remove this crash
		*(int*)NULL = NULL;

        // Left-top
        // Positions
        pt = rotation * offsets[0];
        *mLockPtr++ = pt.x + bb.mPosition.x;
        *mLockPtr++ = pt.y + bb.mPosition.y;
        *mLockPtr++ = pt.z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.left;
        *mLockPtr++ = r.top;

        // Right-top
        // Positions
        pt = rotation * offsets[1];
        *mLockPtr++ = pt.x + bb.mPosition.x;
        *mLockPtr++ = pt.y + bb.mPosition.y;
        *mLockPtr++ = pt.z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.right;
        *mLockPtr++ = r.top;

        // Left-bottom
        // Positions
        pt = rotation * offsets[2];
        *mLockPtr++ = pt.x + bb.mPosition.x;
        *mLockPtr++ = pt.y + bb.mPosition.y;
        *mLockPtr++ = pt.z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.left;
        *mLockPtr++ = r.bottom;

        // Right-bottom
        // Positions
        pt = rotation * offsets[3];
        *mLockPtr++ = pt.x + bb.mPosition.x;
        *mLockPtr++ = pt.y + bb.mPosition.y;
        *mLockPtr++ = pt.z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = r.right;
        *mLockPtr++ = r.bottom;
    }
    else
    {
        const Ogre::Real      cos_rot  ( Ogre::Math::Cos(bb.mRotation)   );
        const Ogre::Real      sin_rot  ( Ogre::Math::Sin(bb.mRotation)   );

        float width = (r.right-r.left)/2;
        float height = (r.bottom-r.top)/2;
        float mid_u = r.left+width;
        float mid_v = r.top+height;

        float cos_rot_w = cos_rot * width;
        float cos_rot_h = cos_rot * height;
        float sin_rot_w = sin_rot * width;
        float sin_rot_h = sin_rot * height;

		// implement tangent space calculation here, then you may remove this crash
		*(int*)NULL = NULL;

        // Left-top
        // Positions
        *mLockPtr++ = offsets[0].x + bb.mPosition.x;
        *mLockPtr++ = offsets[0].y + bb.mPosition.y;
        *mLockPtr++ = offsets[0].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = mid_u - cos_rot_w + sin_rot_h;
        *mLockPtr++ = mid_v - sin_rot_w - cos_rot_h;

        // Right-top
        // Positions
        *mLockPtr++ = offsets[1].x + bb.mPosition.x;
        *mLockPtr++ = offsets[1].y + bb.mPosition.y;
        *mLockPtr++ = offsets[1].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = mid_u + cos_rot_w + sin_rot_h;
        *mLockPtr++ = mid_v + sin_rot_w - cos_rot_h;

        // Left-bottom
        // Positions
        *mLockPtr++ = offsets[2].x + bb.mPosition.x;
        *mLockPtr++ = offsets[2].y + bb.mPosition.y;
        *mLockPtr++ = offsets[2].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = mid_u - cos_rot_w - sin_rot_h;
        *mLockPtr++ = mid_v - sin_rot_w + cos_rot_h;

        // Right-bottom
        // Positions
        *mLockPtr++ = offsets[3].x + bb.mPosition.x;
        *mLockPtr++ = offsets[3].y + bb.mPosition.y;
        *mLockPtr++ = offsets[3].z + bb.mPosition.z;
        // Colour
        // Convert float* to RGBA*
        pCol = static_cast<Ogre::RGBA*>(static_cast<void*>(mLockPtr));
        *pCol++ = colour;
        // Update lock pointer
        mLockPtr = static_cast<float*>(static_cast<void*>(pCol));
		// Tangent
		*mLockPtr++ = tangent.x;
        *mLockPtr++ = tangent.y;
        *mLockPtr++ = tangent.z;
		// Binormal
		*mLockPtr++ = binormal.x;
        *mLockPtr++ = binormal.y;
        *mLockPtr++ = binormal.z;
		// Normal
		*mLockPtr++ = normal.x;
        *mLockPtr++ = normal.y;
        *mLockPtr++ = normal.z;
        // Texture coords
        *mLockPtr++ = mid_u + cos_rot_w - sin_rot_h;
        *mLockPtr++ = mid_v + sin_rot_w + cos_rot_h;
    }
}

void GamePipe::BillboardSet::_createBuffers(void)
{
	/* Allocate / reallocate vertex data
       Note that we allocate enough space for ALL the billboards in the pool, but only issue
       rendering operations for the sections relating to the active billboards
    */

    /* Alloc positions   ( 1 or 4 verts per billboard, 3 components )
             colours     ( 1 x RGBA per vertex )
             indices     ( 6 per billboard ( 2 tris ) if not point rendering )
             tex. coords ( 2D coords, 1 or 4 per billboard )
    */

	// Warn if user requested an invalid setup
	// Do it here so it only appears once
	if (mPointRendering && mBillboardType != Ogre::BBT_POINT)
	{

		Ogre::LogManager::getSingleton().logMessage("Warning: BillboardSet " +
			mName + " has point rendering enabled but is using a type "
			"other than BBT_POINT, this may not give you the results you "
			"expect.");
	}

	mVertexData = new Ogre::VertexData();
	if (mPointRendering)
		mVertexData->vertexCount = mPoolSize;
	else
		mVertexData->vertexCount = mPoolSize * 4;

    mVertexData->vertexStart = 0;

    // Vertex declaration
	Ogre::VertexDeclaration* decl = mVertexData->vertexDeclaration;
	Ogre::VertexBufferBinding* binding = mVertexData->vertexBufferBinding;

    size_t offset = 0;
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
    offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    decl->addElement(0, offset, Ogre::VET_COLOUR, Ogre::VES_DIFFUSE);
    offset += Ogre::VertexElement::getTypeSize(Ogre::VET_COLOUR);

	decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_TANGENT);
	offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
	decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_BINORMAL);
	offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
	decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
	offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);

    // Texture coords irrelevant when enabled point rendering (generated
    // in point sprite mode, and unused in standard point mode)
    if (!mPointRendering)
    {
        decl->addElement(0, offset, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES, 0);
    }

    mMainBuf =
        Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
            decl->getVertexSize(0),
            mVertexData->vertexCount,
            Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    // bind position and diffuses
    binding->setBinding(0, mMainBuf);

	if (!mPointRendering)
	{
		mIndexData  = new Ogre::IndexData();
		mIndexData->indexStart = 0;
		mIndexData->indexCount = mPoolSize * 6;

		mIndexData->indexBuffer = Ogre::HardwareBufferManager::getSingleton().
			createIndexBuffer(Ogre::HardwareIndexBuffer::IT_16BIT,
				mIndexData->indexCount,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);

		/* Create indexes (will be the same every frame)
		   Using indexes because it means 1/3 less vertex transforms (4 instead of 6)

		   Billboard layout relative to camera:

			0-----1
			|    /|
			|  /  |
			|/    |
			2-----3
		*/

		Ogre::ushort* pIdx = static_cast<Ogre::ushort*>(
			mIndexData->indexBuffer->lock(0,
			  mIndexData->indexBuffer->getSizeInBytes(),
			  Ogre::HardwareBuffer::HBL_DISCARD) );

		for(
			size_t idx, idxOff, bboard = 0;
			bboard < mPoolSize;
			++bboard )
		{
			// Do indexes
			idx    = bboard * 6;
			idxOff = bboard * 4;

			pIdx[idx] = static_cast<unsigned short>(idxOff); // + 0;, for clarity
			pIdx[idx+1] = static_cast<unsigned short>(idxOff + 2);
			pIdx[idx+2] = static_cast<unsigned short>(idxOff + 1);
			pIdx[idx+3] = static_cast<unsigned short>(idxOff + 1);
			pIdx[idx+4] = static_cast<unsigned short>(idxOff + 2);
			pIdx[idx+5] = static_cast<unsigned short>(idxOff + 3);

		}

		mIndexData->indexBuffer->unlock();
	}
    mBuffersCreated = true;
}

void GamePipe::BillboardSet::_destroyBuffers(void)
{
	if (mVertexData)
    {
		delete mVertexData;
        mVertexData = 0;
    }
    if (mIndexData)
    {
        delete mIndexData;
        mIndexData = 0;
    }

    mMainBuf.setNull();

	mBuffersCreated = false;
}