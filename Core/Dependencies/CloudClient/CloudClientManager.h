#include "GameScreen.h"
#include "Engine.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include "FullyConnectedMesh2.h"
#include "TwoWayAuthentication.h"
#include "CloudClient.h"
#include "DynDNS.h"
#include "RakPeerInterface.h"
#include "RakSleep.h"
#include "ConnectionGraph2.h"
#include <list>
#include <stdio.h>

#define CLOUD_CLIENT_PRIMARY_KEY "CC_Sample_PK"

namespace GamePipe
{
	class CloudClientManager
	{
		public:

			// ---- RAKPEER -----
			RakNet::RakPeerInterface *rakPeer;

			// ---- Cloud Client ----
			RakNet::CloudClient cloudClient;
			
			// Used to determine the host of the server fully connected mesh, 
			//as well as to connect servers automatically
			RakNet::FullyConnectedMesh2 fullyConnectedMesh2;
			
			// Used for servers to verify each other - otherwise any system could pose as a server
			// Could also be used to verify and restrict clients if paired with the MessageFilter plugin
			RakNet::TwoWayAuthentication twoWayAuthentication;
			
			// Used to update DNS
			RakNet::DynDNS dynDNS;
			
			// Used to tell servers about each other
			RakNet::ConnectionGraph2 connectionGraph2;
			
			RakNet::Packet *packet;
			RakNet::Packet *initPacket;
			
			//RakNet::CloudServerHelperFilter sampleFilter; // Keeps clients from updating stuff to the server they are not supposed to

			//For GGE
			GameObject* gameObject;
			std::string clientMsg;

			const char *serverAddress;
			RakNet::SystemAddress serverAddressObject;
			unsigned short serverPort;
			RakNet::BitStream bs;
			int flag; 

			int CreateCloudClient();
			int CloudUpdateClient();
			int ReconnectCloudClient();

			CloudClientManager();
			~CloudClientManager();
			void UploadInstanceToCloud(RakNet::CloudClient *cloudClient, RakNet::RakNetGUID serverGuid);
			void GetClientSubscription(RakNet::CloudClient *cloudClient, RakNet::RakNetGUID serverGuid);
			void GetServers(RakNet::CloudClient *cloudClient, RakNet::RakNetGUID serverGuid);
			void DestroyCloud();	


	};

}