#include "StdAfx.h"

//Raknet
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakNetObjects.h"
#include "Kbhit.h"
#include "Bitstream.h"

//Cloud
#include "CloudClientManager.h"
#include "CloudClientGlobal.h"
#include "CloudMessageIdentifiers.h"


namespace GamePipe
{
	char DEFAULT_SERVER_ADDRESS [512];
	const unsigned short DEFAULT_SERVER_PORT = 60000;

	CloudClientManager::CloudClientManager()
	{
	}

	CloudClientManager::~CloudClientManager()
	{
	}

	void CloudClientManager::UploadInstanceToCloud(RakNet::CloudClient *cloudClient, RakNet::RakNetGUID serverGuid)
	{
		RakNet::CloudKey cloudKey(CLOUD_CLIENT_PRIMARY_KEY,0);
		RakNet::BitStream bs;
		
		bs.Write("Hello World. This is Ajinkya"); // This could be anything such as player list, game name, etc.

		cloudClient->Post(&cloudKey, bs.GetData(), bs.GetNumberOfBytesUsed(), serverGuid);
	}

	void CloudClientManager::GetClientSubscription(RakNet::CloudClient *cloudClient, RakNet::RakNetGUID serverGuid)
	{
		RakNet::CloudQuery cloudQuery;
		cloudQuery.keys.Push(RakNet::CloudKey(CLOUD_CLIENT_PRIMARY_KEY,0),_FILE_AND_LINE_);
		cloudQuery.subscribeToResults=true; // Causes ID_CLOUD_SUBSCRIPTION_NOTIFICATION
		cloudClient->Get(&cloudQuery, serverGuid);
	}

	void CloudClientManager::GetServers(RakNet::CloudClient *cloudClient, RakNet::RakNetGUID serverGuid)
	{
		RakNet::CloudQuery cloudQuery;
		cloudQuery.keys.Push(RakNet::CloudKey("CloudConnCount",0),_FILE_AND_LINE_); // CloudConnCount is defined at the top of CloudServerHelper.cpp
		cloudQuery.subscribeToResults=false;
		cloudClient->Get(&cloudQuery, serverGuid);
	}

	/***************************************************************************/
	// This function establishes a connection with the cloud and then the client
	// get access to all the game objects
	/***************************************************************************/
	int CloudClientManager::CreateCloudClient()
	{

		//serverAddress = "test.dnsalias.net";
		serverPort = DEFAULT_SERVER_PORT;
		rakPeer = RakNet::RakPeerInterface::GetInstance();
		RakNet::SocketDescriptor sd;
		sd.socketFamily=AF_INET; // Only IPV4 supports broadcast on 255.255.255.255

		RakNet::StartupResult sr = rakPeer->Startup(1,&sd,1);	// Change this if you want
		rakPeer->SetMaximumIncomingConnections(0);				// Change this if you want
	
		if (sr!=RakNet::RAKNET_STARTED)
		{
			return 1;
		}

		rakPeer->AttachPlugin(&cloudClient);
				
		printf("\nMy GUID is %s\n", rakPeer->GetMyGUID().ToString());		

		RakNet::ConnectionAttemptResult car = rakPeer->Connect(serverAddress, serverPort, 0, 0);
		if (car==RakNet::CANNOT_RESOLVE_DOMAIN_NAME)
		{

			clientMsg = "\n\nCANNOT RESOLVE DOMAIN NAME..";
			clientMsg += "\n";
			UtilitiesPtr->GetDebugText("Status")->SetText(clientMsg);		

			return 1;
		}

		return 0;
	}


	/***************************************************************************/
	// If the connection fails the client will try to reconnect using this 
	//function. The ReconnectCloudClient is called CloudUpdateClient continuously
	//in the update loop.
	/***************************************************************************/
	int CloudClientManager::ReconnectCloudClient()
	{

		serverPort = DEFAULT_SERVER_PORT;
		rakPeer = RakNet::RakPeerInterface::GetInstance();

		static const unsigned short clientLocalPort=0;
		RakNet::SocketDescriptor sd(clientLocalPort,0);			// Change this if you want
		RakNet::StartupResult sr = rakPeer->Startup(1,&sd,1);	// Change this if you want
		rakPeer->SetMaximumIncomingConnections(0);				// Change this if you want

		if (sr!=RakNet::RAKNET_STARTED)
		{
			return 1;
		}

		rakPeer->AttachPlugin(&cloudClient);

		RakNet::ConnectionAttemptResult car = rakPeer->Connect(serverAddress, serverPort, 0, 0);
		if (car==RakNet::CANNOT_RESOLVE_DOMAIN_NAME)
		{
			return 1;
		}

		return 0;
	}


	/***************************************************************************/
	// The update loop continuously switches between different Message Ids as 
	// the packets flow in the communication. During the initial setup the client
	// will check for ID_CONNECTION_REQUEST_ACCEPTED. Then it will receive packets
	// and all the objects sent by server at ID_GGE_GAME_OBJECT and will store 
	// them in objectData. Next for continuous update it will check at the 
	// ID_GGE_GAME_OBJECT_UPDATE to see if there are any changes in the object 
	// attributes. 
	/***************************************************************************/
	int CloudClientManager::CloudUpdateClient()
	{

		for (packet = rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet = rakPeer->Receive())
		{
			switch (packet->data[0])
			{
				case ID_CONNECTION_ATTEMPT_FAILED:
					printf("ID_CONNECTION_ATTEMPT_FAILED\n");
				
					clientMsg = "\n\nCONNECTION FAILED.. RECONNECTING....";
					clientMsg += "\n";
					UtilitiesPtr->GetDebugText("Status")->SetText(clientMsg);		

					ReconnectCloudClient();
				break;

				case ID_NO_FREE_INCOMING_CONNECTIONS:
					printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				break;

				case ID_CONNECTION_REQUEST_ACCEPTED:
					clientMsg = "\n\nCONNECTED TO CLOUD. SERVER ";
					clientMsg += serverAddress;
					clientMsg += "\n";
					UtilitiesPtr->GetDebugText("Status")->SetText(clientMsg);		

					serverAddressObject = packet->systemAddress;				
				break;

				case ID_NEW_INCOMING_CONNECTION:
					printf("ID_NEW_INCOMING_CONNECTION from %s\n", packet->systemAddress.ToString());
				break;
	
				case ID_DISCONNECTION_NOTIFICATION:
					printf("ID_DISCONNECTION_NOTIFICATION\n");
				break;

				case ID_CONNECTION_LOST:
					printf("ID_CONNECTION_LOST\n");
				break;

				case ID_ADVERTISE_SYSTEM:
					// The first conditional is needed because ID_ADVERTISE_SYSTEM may be from 
					//a system we are connected to, but replying on a different address.
					// The second conditional is because AdvertiseSystem also sends to the loopback
					if (rakPeer->GetSystemAddressFromGuid(packet->guid)==RakNet::UNASSIGNED_SYSTEM_ADDRESS && rakPeer->GetMyGUID()!=packet->guid)
					{
						printf("Connecting to %s\n", packet->systemAddress.ToString(true));
						rakPeer->Connect(packet->systemAddress.ToString(false), packet->systemAddress.GetPort(),0,0);
					}
				break;
			
				case ID_SND_RECEIPT_LOSS:
			
				case ID_SND_RECEIPT_ACKED:
				break;

				case ID_GGE_GAME_OBJECT:

					flag =0;
					if(packet != NULL)
					{
						serverAddressObject = packet->systemAddress;
						RakNet::BitStream dataStream(packet->data, packet->length, false);					
						string rs;
						dataStream.IgnoreBytes(sizeof(RakNet::MessageID)); // ignores the first msg data in packet

						//ObjectData tempData;
						ObjectData *tempData = new ObjectData;
					
						dataStream.Read(tempData->posX);
						dataStream.Read(tempData->posY);
						dataStream.Read(tempData->posZ);

						dataStream.Read(tempData->yaw);
						dataStream.Read(tempData->pitch);
						dataStream.Read(tempData->roll);
					
						dataStream.Read(tempData->name);
						dataStream.Read(tempData->meshName);
					
						dataStream.Read(tempData->objectType);
					
						dataStream.Read(tempData->isAlive);
						dataStream.Read(tempData->isCreated);
						dataStream.Read(tempData->isUpdated);
				
						objectDataList.push_back(*tempData);
					}
				break;

				case ID_GGE_GAME_OBJECT_UPDATE:
		
					if(packet != NULL)
					{
						RakNet::BitStream dataStream(packet->data, packet->length, false);					
						string rs;
						dataStream.IgnoreBytes(sizeof(RakNet::MessageID)); // ignores the first msg data in packet

						ObjectData *tempData = new ObjectData;
					
						dataStream.Read(tempData->posX);
						dataStream.Read(tempData->posY);
						dataStream.Read(tempData->posZ);

						/*	
						dataStream.Read(tempData->yaw);
						dataStream.Read(tempData->pitch);
						dataStream.Read(tempData->roll);
						*/

						dataStream.Read(tempData->name);

						/*					
						dataStream.Read(tempData->meshName);
						dataStream.Read(tempData->objectType);					
						dataStream.Read(tempData->isAlive);
						dataStream.Read(tempData->isCreated);
						*/
					
						dataStream.Read(tempData->isUpdated);
	
						std::list<ObjectData>::iterator it;
						if(objectDataList.size() > 0)
						{
							for(it = objectDataList.begin(); it != objectDataList.end(); it++)
							{
								if(it->name == tempData->name)
								{
									it->posX = tempData->posX;
									it->posY = tempData->posY;
									it->posZ = tempData->posZ;
									it->setIsUpdated(tempData->isUpdated);
									break;
								}
							}
						}
					}		
				break;
			}
			return 1;
		}
		RakSleep(30);
		return 1;
	}

}