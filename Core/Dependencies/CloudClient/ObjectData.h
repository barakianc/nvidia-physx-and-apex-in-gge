#include<string>
#include "GameObject.h"
using namespace std;

#ifndef OBJECT_DATA_H
#define OBJECT_DATA_H

struct ObjectData{

	float posX, posY, posZ;		// X, Y, and Z position of the object
	float yaw, pitch, roll;		// X, Y, and Z orientation of the object
	string name, meshName;		// Name of the object as well as the mesh name
	string havokName;			// Name of the .hkx file associated with the mesh
	GameObjectType objectType;	// The type of the object, see the GameObject.h for more details
	bool isCreated;				// bool value that tells the game whether the object has been created or not
	bool isAlive;				// bool value that tells the game whether the object is alive or not
	bool isUpdated;				// bool value that tells the game whether the object has been updated

	void setIsCreated(bool b)
	{
		isCreated = b;
	}
	void setIsAlive(bool b)
	{
		isAlive = b;
	}
	void setIsUpdated(bool b)
	{
		isUpdated = b;
	}
};

#endif