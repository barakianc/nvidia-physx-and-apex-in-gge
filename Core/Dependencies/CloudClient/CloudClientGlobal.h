#ifndef CLOUD_CLIENT_GLOBAL_H
#define CLOUD_CLIENT_GLOBAL_H

#include <list>
#include "GameObject.h"
#include "ObjectData.h"
#include "RakNetTypes.h"

extern std::list<ObjectData> objectDataList;	// A list that stores all of our ObjectData structs
extern int isConnected;

#endif