//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by dshowplayer.rc
//
#define ID_IMAGE_PLAY                   0
#define ID_IMAGE_STOP                   1
#define IDC_MYICON                      2
#define ID_IMAGE_PAUSE                  2
#define ID_IMAGE_MUTE_OFF               3
#define ID_IMAGE_MUTE_ON                4
#define IDD_DSHOWPLAYER_DIALOG          102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_DSHOWPLAYER                 107
#define IDI_SMALL                       108
#define IDC_DSHOWPLAYER                 109
#define IDR_MAINFRAME                   128
#define IDB_TOOLBAR_IMAGES_NORMAL       129
#define IDB_TOOLBAR_IMAGES_DISABLED     131
#define IDB_SLIDER_THUMB                132
#define IDB_SLIDER_VOLUME               133
#define IDC_REBAR_CONTROL               400
#define IDC_BUTTON_PLAY                 401
#define IDC_BUTTON_STOP                 402
#define IDC_TOOLBAR                     403
#define IDC_BUTTON_PAUSE                404
#define IDC_SEEKBAR                     405
#define IDC_BUTTON_MUTE                 406
#define IDC_VOLUME                      407
#define ID_FILE_OPENFILE                32771
#define ID_FILE_TRANSCODEFILE           32772
#define ID_TRANSCODEFILE_MP4H           32773
#define ID_TRANSCODEFILE_MP4            32774
#define ID_TRANSCODEFILE_MP4_H264_MP3   32775
#define ID_TRANSCODEFILE_MP4_H264_AAC   32776
#define ID_CONFIGURE_ENCODER            32778
#define ID_HELP_ABOUT                   32779
#define ID_TRANSCODEFILE_MPEG           32780
#define ID_TRANSCODEFILE_MPEG32781      32781
#define ID_TRANSCODEFILE_MPEG_TS_MPEG2_MP3 32782
#define ID_TRANSCODEFILE_MPEG32783      32783
#define ID_TRANSCODEFILE_MPEG_TS_H264_AAC 32784
#define ID_CONFIGURE_RENDER             32785
#define ID_FILE_PLAYINS3D               32786
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
