/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#ifndef __MFX_MFT_MP2VD_H__
#define __MFX_MFT_MP2VD_H__

#include "..\..\..\sample_common\include\current_date.h"

#define MFX_MF_PLUGIN_NAME "Intel\xae Media SDK MPEG2 Decoder MFT"
#define MFX_MF_DLL_NAME    "mfx_mft_mp2vd.dll"

#endif // #ifndef __MFX_MFT_MP2VD_H__
