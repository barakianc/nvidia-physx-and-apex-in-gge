/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mfx_mft_mp2vd.cpp

Purpose: contains registration data for MPEG2 decoder MFT.

*********************************************************************************/

#define DBG_FILE_INIT
//#define DBG_FILE_NAME "C:\\mfx_mft_mp2vd.log"

#include "mf_utils.h"
#include "mfx_mft_mp2vd.h"

/*------------------------------------------------------------------------------*/

mfxStatus FillMpeg2DecoderParams(mfxVideoParam* pVideoParams)
{
    CHECK_POINTER(pVideoParams, MFX_ERR_NULL_PTR);

    pVideoParams->mfx.NumThread = (mfxU16)mf_get_cpu_num();
    pVideoParams->mfx.CodecId   = MFX_CODEC_MPEG2;
    pVideoParams->mfx.ExtendedPicStruct = 1;
    return MFX_ERR_NONE;
}

/*------------------------------------------------------------------------------*/

static const GUID_info g_InputTypes[] =
{
    { MFMediaType_Video, MEDIASUBTYPE_MPEG2_VIDEO },
    { MFMediaType_Video, MEDIASUBTYPE_MPEG1Packet },
    { MFMediaType_Video, MEDIASUBTYPE_MPEG1Payload },
    { MFMediaType_Video, MEDIASUBTYPE_MPEG2_MFX },
    { MFMediaType_Video, MEDIASUBTYPE_MPEG1_MFX }
};

static ClassRegData g_RegData =
{
    (GUID*)&CLSID_MF_MPEG2DecFilter,
    TEXT(MFX_MF_PLUGIN_NAME),
    REG_AS_VIDEO_DECODER,
    CreateVDecPlugin,
    FillMpeg2DecoderParams,
    NULL,
    (GUID_info*)g_InputTypes, ARRAY_SIZE(g_InputTypes),
    (GUID_info*)g_UncompressedVideoTypes, ARRAY_SIZE(g_UncompressedVideoTypes),
    g_DecoderRegFlags
};

/*------------------------------------------------------------------------------*/

BOOL APIENTRY DllMain(HANDLE hModule,
                      DWORD  ul_reason_for_call,
                      LPVOID /*lpReserved*/)
{
    BOOL ret = false;
    HRESULT hr = S_OK;

    if (DLL_PROCESS_ATTACH == ul_reason_for_call)
    {
    }
    if (!ret) ret = myDllMain(hModule, ul_reason_for_call, &g_RegData, 1);
    if (DLL_PROCESS_DETACH == ul_reason_for_call)
    {
        SAFE_RELEASE(g_RegData.pAttributes);
    }
    return ret;
}
