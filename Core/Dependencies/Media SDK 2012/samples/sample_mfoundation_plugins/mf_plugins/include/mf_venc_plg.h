/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_venc_plg.h

Purpose: define common code for MSDK based encoder MFTs.

Defined Types:
  * MFEncVideoFilter - [class] - MFT for MSDK encoders
  * MFEncState - [enum] - MFT states enum

Defined Macroses:
  * MF_BITSTREAMS_NUM - initial number of ouput bitstreams (samples)

*********************************************************************************/

#ifndef __MF_VENC_PLG_H__
#define __MF_VENC_PLG_H__

#include "mf_utils.h"
#include "mf_dxva_support.h"
#include "mf_plugin.h"
#include "mf_yuv_ibuf.h"
#include "mf_venc_obuf.h"

#include "vpp_ex.h"

/*------------------------------------------------------------------------------*/

#define REG_ENC_ASYNC       "MfxMftEnc_async"
#define REG_ENC_MSDK        "MfxMftEnc_MSDK"
#define REG_ENC_MEMORY      "MfxMftEnc_Memory"
#define REG_ENC_VPPIN_FILE  "MfxMftEnc_vppin_fname"
#define REG_ENC_VPPOUT_FILE "MfxMftEnc_vppout_fname"
#define REG_ENC_OUT_FILE    "MfxMftEnc_out_fname"

/*------------------------------------------------------------------------------*/

// initial number of output bitstreams (samples)
#define MF_BITSTREAMS_NUM 1
// number of buffered output events to start receiving input data again
#define MF_OUTPUT_EVENTS_COUNT_LOW_LIMIT 5
// maximum number of buffered output events (bitstreams)
#define MF_OUTPUT_EVENTS_COUNT_HIGH_LIMIT 15
// number of additional input surfaces:
// = 1 - just to always have free surface
#define MF_ADDITIONAL_IN_SURF_NUM 1
// number of additional output surfaces
// = 1 - just to always have free surface
#define MF_ADDITIONAL_OUT_SURF_NUM 1
// multiplier for the minimum number of surfaces
#define MF_ADDITIONAL_SURF_MULTIPLIER 2

/*------------------------------------------------------------------------------*/

enum MFEncState
{
    stHeaderNotSet, stHeaderSetAwaiting, stHeaderSampleNotSent, stReady
};

/*------------------------------------------------------------------------------*/

class MFPluginVEnc : public MFPlugin,
                     public MFEncoderParams,
                     public MFDeviceDXVA
{
public:
    static HRESULT CreateInstance(REFIID iid,
                                  void **ppMFT,
                                  ClassRegData *pRegData);

    // IUnknown methods
    virtual STDMETHODIMP_(ULONG) AddRef(void);
    virtual STDMETHODIMP_(ULONG) Release(void);
    virtual STDMETHODIMP QueryInterface(REFIID iid, void** ppv);

    // IMFTransform methods
    virtual STDMETHODIMP GetInputAvailableType  (DWORD dwInputStreamID,
                                                 DWORD dwTypeIndex,
                                                 IMFMediaType** ppType);

    virtual STDMETHODIMP GetOutputAvailableType (DWORD dwOutputStreamID,
                                                 DWORD dwTypeIndex,
                                                 IMFMediaType** ppType);

    virtual STDMETHODIMP SetInputType           (DWORD dwInputStreamID,
                                                 IMFMediaType* pType,
                                                 DWORD dwFlags);

    virtual STDMETHODIMP SetOutputType          (DWORD dwOutputStreamID,
                                                 IMFMediaType* pType,
                                                 DWORD dwFlags);

    virtual STDMETHODIMP ProcessMessage         (MFT_MESSAGE_TYPE eMessage,
                                                 ULONG_PTR ulParam);

    virtual STDMETHODIMP ProcessInput           (DWORD dwInputStreamID,
                                                 IMFSample *pSample,
                                                 DWORD dwFlags);

    virtual STDMETHODIMP ProcessOutput          (DWORD dwFlags,
                                                 DWORD cOutputBufferCount,
                                                 MFT_OUTPUT_DATA_BUFFER* pOutputSamples,
                                                 DWORD* pdwStatus);

    // IMFDeviceDXVA methods
    virtual mfxStatus InitPlg(IMfxVideoSession* pSession,
                              mfxVideoParam*    pDecVideoParams,
                              mfxU32*           pDecSurfacesNum);

    // MFConfigureMfxCodec methods
    virtual LPWSTR GetCodecName(void){ return m_Reg.pPluginName; }

protected: // functions
    // Constructor is protected. Client should use static CreateInstance method.
    MFPluginVEnc(HRESULT &hr, ClassRegData *pRegistrationData);

    // Destructor is protected. The object deletes itself when the reference count is zero.
    virtual ~MFPluginVEnc(void);

    // Errors handling
    virtual void SetPlgError(HRESULT hr, mfxStatus sts = MFX_ERR_NONE);
    void HandlePlgError(MyAutoLock& lock, bool bCalledFromAsyncThread = false);
    bool ReturnPlgError(void);

    // Initialization functions
    mfxStatus  InitCodec(MyAutoLock& lock, 
                         mfxU16  uiMemPattern = 0,
                         mfxU32* pSurfacesNum = NULL);
    void       CloseCodec(MyAutoLock& lock);
    mfxStatus  ResetCodec(MyAutoLock& lock);
    mfxStatus  InitFRA(void);
    mfxStatus  InitInSRF(mfxU32* pSurfacesNum);
    mfxStatus  InitVPP(mfxU32* pSurfacesNum);
    mfxStatus  InitENC(mfxU32* pSurfacesNum);
    mfxStatus  QueryENC(mfxVideoParam* pParam, mfxVideoParam* pCorrectedParams);
    // Work functions
    HRESULT    ProcessFrame(mfxStatus& sts);
    HRESULT    ProcessFrameEnc(mfxStatus& sts);
    HRESULT    ProcessFrameVpp(mfxStatus& sts);
    HRESULT    TryReinit(MyAutoLock& lock, mfxStatus& sts);
    // Help functions
    bool       HandleDevBusy(mfxStatus& sts);
    HRESULT    GetHeader(mfxBitstream* pBst);
    MFYuvInSurface* SetFreeSurface(MFYuvInSurface* pSurfaces,
                                   mfxU32 nSurfacesNum);
    mfxStatus  SetFreeBitstream(void);
    void       CheckBitstreamsNumLimit(void);
    bool       CheckHwSupport(void);
    HRESULT    CheckInputMediaType(IMFMediaType* pType);
    mfxStatus  CheckMfxParams(void);

    // Async thread functionality
    virtual HRESULT AsyncThreadFunc(void);

protected: // variables
    // Reference count, critical section
    long                   m_nRefCount;

    // Plug-in information
    const GUID_info*       m_pInputInfo;
    const GUID_info*       m_pOutputInfo;

    // MSDK components variables
    mfxIMPL                m_MSDK_impl;
    IMfxVideoSession*      m_pMfxVideoSession;
    IMfxVideoSession*      m_pInternalMfxVideoSession;
    MFXVideoENCODE*        m_pmfxENC;
    MFXVideoVPPEx*         m_pmfxVPP;

    // Work variables
    mfxVideoParam          m_VPPInitParams;
    mfxVideoParam          m_VPPInitParams_original;
    mfxInfoMFX             m_ENCInitInfo_original;

    mfxF64                 m_Framerate;
    mfxU32                 m_uiHeaderSize;
    mfxU8*                 m_pHeader;
    MFSamplesPool*         m_pFreeSamplesPool;
    mfxU32                 m_uiHasOutputEventExists;

    mfxFrameAllocResponse  m_VppAllocResponse;      // vpp alloc response
    mfxFrameAllocResponse  m_EncAllocResponse;      // enc alloc response

    MFEncState             m_State;
    mfxStatus              m_LastSts;
    mfxStatus              m_SyncOpSts;

    // VPP in surfaces
    mfxU16                 m_uiInSurfacesMemType;
    mfxU32                 m_nInSurfacesNum;
    MFYuvInSurface*        m_pInSurfaces;
    MFYuvInSurface*        m_pInSurface;             // pointer to current surface
    // VPP out surfaces
    mfxU16                 m_uiOutSurfacesMemType;
    mfxU32                 m_nOutSurfacesNum;
    MFYuvInSurface*        m_pOutSurfaces;
    MFYuvInSurface*        m_pOutSurface;            // pointer to current surface
    // encoded bitstreams
    mfxU32                 m_nOutBitstreamsNum;
    MFEncOutData*          m_pOutBitstreams;
    MFEncOutData*          m_pOutBitstream;
    MFEncOutData*          m_pDispBitstream;

    mfxSyncPoint*          m_pSyncPoint;             // VPP sync point (is not used by default)

    bool                   m_bInitialized;
    bool                   m_bDirectConnectionMFX;
    bool                   m_bConnectedWithVpp;
    bool                   m_bEndOfInput;
    bool                   m_bNeedInSurface;
    bool                   m_bNeedOutSurface;
    bool                   m_bBitstreamsLimit;
    bool                   m_bStartDrain;
    bool                   m_bStartDrainEnc;
    bool                   m_bSendDrainComplete;
    bool                   m_bReinit;
    bool                   m_bSetDiscontinuityAttribute;

    IMFSample*             m_pFakeSample;

    FILE*                  m_dbg_vppin;
    FILE*                  m_dbg_vppout;
    FILE*                  m_dbg_encout;

    // Debug statistics variables
    int                    m_iNumberInputSamplesBeforeVPP;
    int                    m_iNumberInputSamples;
    int                    m_iNumberOutputSamples;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFPluginVEnc(const MFPluginVEnc&);
    MFPluginVEnc& operator=(const MFPluginVEnc&);
};

#endif // #ifndef __MF_VENC_PLG_H__
