/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_venc_plg.cpp

Purpose: define common code for MSDK based encoder MFTs.

*********************************************************************************/

#include "mf_venc_plg.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Encoder"

//#define DISABLE_VPP_ON_CONNECTION_STAGE
#define OMIT_VPP_SYNC_POINT

/*------------------------------------------------------------------------------*/

HRESULT CreateVEncPlugin(REFIID iid,
                         void** ppMFT,
                         ClassRegData* pRegistrationData)
{
    return MFPluginVEnc::CreateInstance(iid, ppMFT, pRegistrationData);
}

/*------------------------------------------------------------------------------*/

bool UseVPP(mfxInfoVPP* pVppInfo)
{
    bool bUseVPP = false;

    if (pVppInfo)
    {
        if (!bUseVPP && (pVppInfo->In.Width != pVppInfo->Out.Width)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.Height != pVppInfo->Out.Height)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.CropX != pVppInfo->Out.CropX)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.CropY != pVppInfo->Out.CropY)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.CropW != pVppInfo->Out.CropW)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.CropH != pVppInfo->Out.CropH)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.AspectRatioH != pVppInfo->Out.AspectRatioH)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.AspectRatioW != pVppInfo->Out.AspectRatioW)) bUseVPP = true;
        if (!bUseVPP && !mf_are_framerates_equal(pVppInfo->In.FrameRateExtN,
                                                 pVppInfo->In.FrameRateExtD,
                                                 pVppInfo->Out.FrameRateExtN,
                                                 pVppInfo->Out.FrameRateExtD))
        {
            bUseVPP = true;
        }
        if (!bUseVPP && (pVppInfo->In.PicStruct != pVppInfo->Out.PicStruct)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.FourCC != pVppInfo->Out.FourCC)) bUseVPP = true;
        if (!bUseVPP && (pVppInfo->In.ChromaFormat != pVppInfo->Out.ChromaFormat)) bUseVPP = true;
    }
    return bUseVPP;
}

/*------------------------------------------------------------------------------*/

HRESULT FindNalUnit(mfxU32 length, mfxU8* data, mfxU32* start, mfxU32* end)
{
    DBG_TRACE("+");
    mfxU32 i = 0;

    CHECK_POINTER(data,  E_POINTER);
    CHECK_POINTER(start, E_POINTER);
    CHECK_POINTER(end,   E_POINTER);

    *start = 0; *end = 0;
    for (i = 2; i < length; ++i)
    {
        if ((0x00 == data[i-2]) && (0x00 == data[i-1]) && (0x01 == data[i]))
        {
            *start = i-2;
            *end   = ((i+1) < length)? i+1: i;
            break;
        }
    }
    for (i = (*end) + 1; i < length; ++i)
    {
        if (((i+2) < length) &&
            (0x00 == data[i]) && (0x00 == data[i+1]) && (0x01 == data[i+2]))
        {
            *end = i-1;
            break;
        }
        else
            ++(*end);
    }
#if USE_DBG == DBG_YES
    fprintf(g_dbg_file, "%S: %s: nalu_len = %d, nalu = '", DBG_MODULE_NAME, __FUNCTION__, (*end)-(*start)+1);
    for (i = *start; i < (*end)+1; ++i)
    {
        fprintf(g_dbg_file, "0x%02x, ", data[i]);
    }
    fprintf(g_dbg_file, "'\n");
#endif
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/
// MFPluginVEnc class

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME m_Reg.pPluginName

/*------------------------------------------------------------------------------*/

MFPluginVEnc::MFPluginVEnc(HRESULT &hr,
                           ClassRegData *pRegistrationData) :
    MFPlugin(hr, pRegistrationData),
    m_nRefCount(0),
    m_MSDK_impl(MF_MFX_IMPL),
    m_pMfxVideoSession(NULL),
    m_pInternalMfxVideoSession(NULL),
    m_pmfxENC(NULL),
    m_pmfxVPP(NULL),
    m_Framerate(0),
    m_uiHeaderSize(0),
    m_pHeader(NULL),
    m_pInputInfo(NULL),
    m_pOutputInfo(NULL),
    m_iNumberInputSamplesBeforeVPP(0),
    m_iNumberInputSamples(0),
    m_iNumberOutputSamples(0),
    m_State(stHeaderNotSet),
    m_LastSts(MFX_ERR_NONE),
    m_SyncOpSts(MFX_ERR_NONE),
    m_bInitialized(false),
    m_bDirectConnectionMFX(false),
    m_bConnectedWithVpp(false),
    m_bEndOfInput(false),
    m_bNeedInSurface(false),
    m_bNeedOutSurface(false),
    m_bBitstreamsLimit(false),
    m_bStartDrain(false),
    m_bStartDrainEnc(false),
    m_bSendDrainComplete(false),
    m_bReinit(false),
    m_bSetDiscontinuityAttribute(false),
    m_pFakeSample(NULL),
    m_uiInSurfacesMemType(MFX_IOPATTERN_IN_SYSTEM_MEMORY), // VPP in surfaces
    m_nInSurfacesNum(MF_ADDITIONAL_IN_SURF_NUM),
    m_pInSurfaces(NULL),
    m_pInSurface(NULL),
    m_uiOutSurfacesMemType(MFX_IOPATTERN_OUT_VIDEO_MEMORY), // VPP out surfaces
    m_nOutSurfacesNum(MF_ADDITIONAL_OUT_SURF_NUM),
    m_pOutSurfaces(NULL),
    m_pOutSurface(NULL),
    m_nOutBitstreamsNum(0), // Encoded bitstreams
    m_pOutBitstreams(NULL),
    m_pOutBitstream(NULL),
    m_pDispBitstream(NULL),
    m_pSyncPoint(NULL),
    m_uiHasOutputEventExists(0),
    m_pFreeSamplesPool(NULL),
    m_dbg_vppin(NULL),
    m_dbg_vppout(NULL),
    m_dbg_encout(NULL)
{
    memset(&m_VPPInitParams, 0, sizeof(mfxVideoParam));

    if (FAILED(hr)) return;
    SAFE_NEW(m_pWorkTicker, MFTicker(&m_ticks_WorkTime));
    SAFE_NEW(m_pCpuUsager, MFCpuUsager(&m_TimeTotal, &m_TimeKernel, &m_TimeUser, &m_CpuUsage));

    DBG_TRACE("+");
    if (!m_Reg.pFillParams) hr = E_POINTER;
    if (SUCCEEDED(hr))
    { // setting default encoder parameters
        mfxStatus sts = MFX_ERR_NONE;
        mfxVideoParam tmp_params;

        sts = m_Reg.pFillParams(&m_MfxParamsVideo);

        memset(&m_VPPInitParams_original, 0, sizeof(mfxVideoParam));
        memset(&m_ENCInitInfo_original, 0, sizeof(mfxInfoMFX));
        memset((void*)&m_VppAllocResponse, 0, sizeof(mfxFrameAllocResponse));
        memset((void*)&m_EncAllocResponse, 0, sizeof(mfxFrameAllocResponse));

        tmp_params.NumExtParam = sizeof(g_pVppExtBuf) / sizeof(g_pVppExtBuf[0]);
        tmp_params.ExtParam    = (mfxExtBuffer**)g_pVppExtBuf;
        sts = mf_copy_extparam(&tmp_params, &m_VPPInitParams);

        if (MFX_ERR_NONE != sts) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    if (SUCCEEDED(hr))
    {
        hr = MFCreateMfxVideoSession(&m_pMfxVideoSession);
    }
    if (SUCCEEDED(hr))
    { // internal media session initialization
        mfxStatus sts = MFX_ERR_NONE;

        // adjustment of MSDK implementation
        if (dbgHwMsdk == m_dbg_MSDK) m_MSDK_impl = MFX_IMPL_HARDWARE;
        else if (dbgSwMsdk == m_dbg_MSDK) m_MSDK_impl = MFX_IMPL_SOFTWARE;

        sts = m_pMfxVideoSession->Init(m_MSDK_impl, &g_MfxVersion);

        if (MFX_ERR_NONE == sts)
        {
            sts = m_pMfxVideoSession->QueryIMPL(&m_MSDK_impl);
            m_MfxCodecInfo.m_MSDKImpl = m_MSDK_impl;
        }
        if (MFX_ERR_NONE == sts)
        {
            MFXVideoSession* session = (MFXVideoSession*)m_pMfxVideoSession;

            SAFE_NEW(m_pmfxENC, MFXVideoENCODE(*session));
            if (!m_pmfxENC) sts = MFX_ERR_MEMORY_ALLOC;
        }
        if (MFX_ERR_NONE != sts) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    { // initializing of the DXVA support
        hr = DXVASupportInit();
    }
    if (SUCCEEDED(hr))
    {
        SAFE_NEW(m_pSyncPoint, mfxSyncPoint);
        if (!m_pSyncPoint) hr = E_OUTOFMEMORY;
    }
    GetPerformance();
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFPluginVEnc::~MFPluginVEnc(void)
{
    DBG_TRACE("+");
    MyAutoLock lock(m_CritSec);

    DBG_TRACE_1("m_iNumberInputSamplesBeforeVPP = %d", m_iNumberInputSamplesBeforeVPP);
    DBG_TRACE_1("m_iNumberInputSamples          = %d", m_iNumberInputSamples);
    DBG_TRACE_1("m_iNumberOutputSamples         = %d", m_iNumberOutputSamples);

    lock.Unlock();
    AsyncThreadWait();
    lock.Lock();

    SAFE_DELETE(m_pWorkTicker);
    SAFE_DELETE(m_pCpuUsager);
    UpdateTimes();
    GetPerformance();
    // print work info
    PrintInfo();
    // destructing objects
    SAFE_RELEASE(m_pInputType);
    SAFE_RELEASE(m_pOutputType);
    // closing codec(s)
    CloseCodec(lock);
    SAFE_DELETE(m_pmfxVPP);
    SAFE_DELETE(m_pmfxENC);
    SAFE_DELETE(m_pSyncPoint);
    // internal session deinitialization
    SAFE_RELEASE(m_pMfxVideoSession);
    SAFE_RELEASE(m_pInternalMfxVideoSession);
    // deinitalization of the DXVA support
    DXVASupportClose();

    mf_free_extparam(&m_VPPInitParams);
    if (m_Reg.pFreeParams) m_Reg.pFreeParams(&m_MfxParamsVideo);

    if (m_dbg_vppin)  fclose(m_dbg_vppin);
    if (m_dbg_vppout) fclose(m_dbg_vppout);
    if (m_dbg_encout) fclose(m_dbg_encout);
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// IUnknown methods

ULONG MFPluginVEnc::AddRef(void)
{
    DBG_TRACE_1("RefCount = %d", m_nRefCount+1);
    return InterlockedIncrement(&m_nRefCount);
}

ULONG MFPluginVEnc::Release(void)
{
    DBG_TRACE("+");
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    DBG_TRACE_1("RefCount = %d: -", uCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT MFPluginVEnc::QueryInterface(REFIID iid, void** ppv)
{
    DBG_TRACE("+");
    bool bAggregation = false;

    if (!ppv) return E_POINTER;
    if ((iid == IID_IUnknown) || (iid == IID_IMFTransform))
    {
        DBG_TRACE("IUnknown or IMFTransform");
        *ppv = static_cast<IMFTransform*>(this);
    }
    else if (iid == IID_IMFMediaEventGenerator)
    {
        DBG_TRACE("IMFMediaEventGenerator");
        *ppv = static_cast<IMFMediaEventGenerator*>(this);
    }
    else if (iid == IID_IMFShutdown)
    {
        DBG_TRACE("IMFShutdown");
        *ppv = static_cast<IMFShutdown*>(this);
    }
    else if (iid == IID_IConfigureMfxEncoder)
    {
        DBG_TRACE("IConfigureMfxEncoder");
        *ppv = static_cast<IConfigureMfxCodec*>(this);
    }
    else if (iid == IID_ICodecAPI)
    {
        DBG_TRACE("ICodecAPI");
        *ppv = static_cast<ICodecAPI*>(this);
    }
    else if (iid == IID_IMFDeviceDXVA)
    {
        DBG_TRACE("IMFDeviceDXVA");
        *ppv = static_cast<IMFDeviceDXVA*>(this);
    }
    else if ((iid == IID_IPropertyStore) && m_pPropertyStore)
    {
        DBG_TRACE("IPropertyStore");
        m_pPropertyStore->AddRef();
        *ppv = m_pPropertyStore;
        bAggregation = true;
    }
    else
    {
        DBG_TRACE_1("not found: %08x: -", iid.Data1);
        return E_NOINTERFACE;
    }
    if (!bAggregation) AddRef();

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/
// IMFTransform methods

HRESULT MFPluginVEnc::GetInputAvailableType(DWORD dwInputStreamID,
                                            DWORD dwTypeIndex,
                                            IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE_1("dwTypeIndex = %d: +", dwTypeIndex);
    HRESULT       hr    = S_OK;
    IMFMediaType* pType = NULL;

    GetPerformance();
    hr = MFPlugin::GetInputAvailableType(dwInputStreamID, dwTypeIndex, &pType);
    if (SUCCEEDED(hr))
    {
        if (m_pInputType)
        { // if input type was set we may offer only this type
            hr = m_pInputType->CopyAllItems(pType);
        }
        else
        {
            // setting hint for acccepted type
            if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_MAJOR_TYPE, m_Reg.pInputTypes[dwTypeIndex].major_type);
            if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_SUBTYPE,    m_Reg.pInputTypes[dwTypeIndex].subtype);
            // if output type was set we may be more precise about acceptable types
#ifdef DISABLE_VPP_ON_CONNECTION_STAGE
            if (m_pOutputType)
            {
                UINT32 par1 = 0, par2 = 0;

                // Currently MSFT prohibits in some ways VPP behavior from encoder.
                // Potentially changeable parameters (VPP enabling):
                // - interlace mode (MF_MT_INTERLACE_MODE)
                // - frame rate (MF_MT_FRAME_RATE)
                // - width, height, aspect ratio (MF_MT_FRAME_SIZE, MF_MT_PIXEL_ASPECT_RATIO)
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeSize(m_pOutputType, MF_MT_FRAME_SIZE, &par1, &par2)))
                    hr = MFSetAttributeSize(pType, MF_MT_FRAME_SIZE, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeRatio(m_pOutputType, MF_MT_PIXEL_ASPECT_RATIO, &par1, &par2)))
                    hr = MFSetAttributeRatio(pType, MF_MT_PIXEL_ASPECT_RATIO, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(m_pOutputType->GetUINT32(MF_MT_INTERLACE_MODE, &par1)))
                    hr = pType->SetUINT32(MF_MT_INTERLACE_MODE, par1);
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeRatio(m_pOutputType, MF_MT_FRAME_RATE, &par1, &par2)))
                    hr = MFSetAttributeRatio(pType, MF_MT_FRAME_RATE, par1, par2);
            }
#endif
        }
    }
    if (SUCCEEDED(hr))
    {
        pType->AddRef();
        *ppType = pType;
    }
    SAFE_RELEASE(pType);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::GetOutputAvailableType(DWORD dwOutputStreamID,
                                             DWORD dwTypeIndex,
                                             IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT       hr    = S_OK;
    IMFMediaType* pType = NULL;

    GetPerformance();
    hr = MFPlugin::GetOutputAvailableType(dwOutputStreamID, dwTypeIndex, &pType);
    if (SUCCEEDED(hr))
    {
        if (m_pOutputType)
        { // if output type was set we may offer only this type
            hr = m_pOutputType->CopyAllItems(pType);
        }
        else
        {
            // setting hint for acccepted type
            if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_MAJOR_TYPE, m_Reg.pOutputTypes[dwTypeIndex].major_type);
            if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_SUBTYPE,    m_Reg.pOutputTypes[dwTypeIndex].subtype);
#ifdef DISABLE_VPP_ON_CONNECTION_STAGE
            // if input type was set we may be more precise about acceptable types
            if (m_pInputType)
            {
                UINT32 par1 = 0, par2 = 0;

                // Currently MSFT prohibits in some ways VPP behavior from encoder.
                // Potentially changeable parameters (VPP enabling):
                // - interlace mode (MF_MT_INTERLACE_MODE)
                // - frame rate (MF_MT_FRAME_RATE)
                // - width, height, aspect ratio (MF_MT_FRAME_SIZE, MF_MT_PIXEL_ASPECT_RATIO)
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeSize(m_pInputType, MF_MT_FRAME_SIZE, &par1, &par2)))
                    hr = MFSetAttributeSize(pType, MF_MT_FRAME_SIZE, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeRatio(m_pInputType, MF_MT_PIXEL_ASPECT_RATIO, &par1, &par2)))
                    hr = MFSetAttributeRatio(pType, MF_MT_PIXEL_ASPECT_RATIO, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(m_pInputType->GetUINT32(MF_MT_INTERLACE_MODE, &par1)))
                    hr = pType->SetUINT32(MF_MT_INTERLACE_MODE, par1);
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeRatio(m_pInputType, MF_MT_FRAME_RATE, &par1, &par2)))
                    hr = MFSetAttributeRatio(pType, MF_MT_FRAME_RATE, par1, par2);
            }
#endif
        }
    }
    if (SUCCEEDED(hr))
    {
        pType->AddRef();
        *ppType = pType;
    }
    SAFE_RELEASE(pType);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::CheckInputMediaType(IMFMediaType* pType)
{
    HRESULT hr = S_OK;

#ifdef MF_ENABLE_PICTURE_COMPLEXITY_LIMITS
    GUID dec_subtype = GUID_NULL;

    if (SUCCEEDED(pType->GetGUID(MF_MT_DEC_SUBTYPE, &dec_subtype)))
    {
        // some code to restrict usage of the encoder plug-in depending on
        // type of input stream
    }
#else
    pType;
#endif
    return hr;
}

/*------------------------------------------------------------------------------*/

// Checks if data provided in pType could be decoded
bool MFPluginVEnc::CheckHwSupport(void)
{
    bool ret_val = MFPlugin::CheckHwSupport();
    mfxStatus sts = MFX_ERR_NONE;
    mfxVideoParam inVideoParams, outVideoParams;

    if (ret_val && m_pInputType && m_pOutputType)
    { // checking VPP
        memcpy(&inVideoParams, &m_VPPInitParams, sizeof(mfxVideoParam));
        memset(&outVideoParams, 0, sizeof(mfxVideoParam));
        sts = mf_copy_extparam(&inVideoParams, &outVideoParams);
        if (MFX_ERR_NONE != sts) ret_val = false;
        else
        {
            // set any color format to make suitable values for Query, suppose it is NV12;
            // actual color format is obtained on output type setting
            if (!(inVideoParams.vpp.In.FourCC) || !(inVideoParams.vpp.In.ChromaFormat))
            {
                inVideoParams.vpp.In.FourCC        = MAKEFOURCC('N','V','1','2');
                inVideoParams.vpp.In.ChromaFormat  = MFX_CHROMAFORMAT_YUV420;
            }
            if (!(inVideoParams.vpp.Out.FourCC) || !(inVideoParams.vpp.Out.ChromaFormat))
            {
                inVideoParams.vpp.Out.FourCC       = MAKEFOURCC('N','V','1','2');
                inVideoParams.vpp.Out.ChromaFormat = MFX_CHROMAFORMAT_YUV420;
            }
            inVideoParams.IOPattern = MFX_IOPATTERN_OUT_VIDEO_MEMORY;

            if (UseVPP(&(inVideoParams.vpp)))
            {
                if (!m_pmfxVPP)
                {
                    MFXVideoSession* session = (MFXVideoSession*)m_pMfxVideoSession;

                    SAFE_NEW(m_pmfxVPP, MFXVideoVPPEx(*session));
                    if (!m_pmfxVPP) ret_val = false;
                }
                if (ret_val)
                {
                    sts = m_pmfxVPP->Query(&inVideoParams, &outVideoParams);
                    if ((MFX_ERR_NONE != sts) && (m_dbg_no_SW_fallback || (MFX_WRN_PARTIAL_ACCELERATION != sts)))
                        ret_val = false;
                }
            }
        }
        sts = mf_free_extparam(&outVideoParams);
        if (ret_val && (MFX_ERR_NONE != sts)) ret_val = false;
    }
    if (ret_val && m_pOutputType)
    { // checking Encoder
        memcpy(&inVideoParams, &m_MfxParamsVideo, sizeof(mfxVideoParam));
        memset(&outVideoParams, 0, sizeof(mfxVideoParam));
        if (!(inVideoParams.mfx.FrameInfo.FourCC) || !(inVideoParams.mfx.FrameInfo.ChromaFormat))
        {
            inVideoParams.mfx.FrameInfo.FourCC       = MAKEFOURCC('N','V','1','2');
            inVideoParams.mfx.FrameInfo.ChromaFormat = MFX_CHROMAFORMAT_YUV420;
        }
        if (MFX_ERR_NONE == sts)
        {
            sts = QueryENC(&inVideoParams, &outVideoParams);
            if ((MFX_ERR_NONE != sts) && (m_dbg_no_SW_fallback || (MFX_WRN_PARTIAL_ACCELERATION != sts)))
                ret_val = false;
        }
    }
    return ret_val;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::SetInputType(DWORD         dwInputStreamID,
                                   IMFMediaType* pType,
                                   DWORD         dwFlags)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE_1("pType = %p: +", pType);
    HRESULT hr  = S_OK;

    GetPerformance();
    hr = MFPlugin::SetInputType(dwInputStreamID, pType, dwFlags);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);
    // currently we do not support dynamic change of input type
    CHECK_EXPRESSION(!m_bInitialized, MF_E_TRANSFORM_CANNOT_CHANGE_MEDIATYPE_WHILE_PROCESSING);

    if (!pType)
    { // resetting media type
        ReleaseMediaType(m_pInputType);
    }
    CHECK_POINTER(pType, S_OK);
    // Validate the type: general checking, input type checking
    if (SUCCEEDED(hr)) hr = CheckMediaType(pType, m_Reg.pInputTypes, m_Reg.cInputTypes, &m_pInputInfo);
#ifdef DISABLE_VPP_ON_CONNECTION_STAGE
    if (SUCCEEDED(hr)) hr = (IsVppNeeded(pType, m_pOutputType))? E_FAIL: S_OK;
#endif
    if (0 == (dwFlags & MFT_SET_TYPE_TEST_ONLY))
    {
        if (SUCCEEDED(hr))
        {
            // set major type; check major type is the same as was on input
            if (GUID_NULL == m_MajorType)
                m_MajorType = m_pInputInfo->major_type;
            else if (m_pInputInfo->major_type != m_MajorType)
                hr = MF_E_INVALIDTYPE;
        }
        if (SUCCEEDED(hr))
        {
            IUnknown *pUnk = static_cast<IMFTransform*>(this);

            // Really set the type, unless the caller was just testing.
            ReleaseMediaType(m_pInputType);
            pType->AddRef();
            m_pInputType = pType;

            hr = m_pInputType->SetUnknown(MF_MT_D3D_DEVICE, pUnk);
        }
        if (SUCCEEDED(hr)) hr = mf_mftype2mfx_frame_info(m_pInputType, &(m_VPPInitParams_original.vpp.In));
        if (SUCCEEDED(hr))
        {
            memcpy(&(m_VPPInitParams.vpp.In), &(m_VPPInitParams_original.vpp.In), sizeof(mfxFrameInfo));
            m_Framerate = mf_get_framerate(m_VPPInitParams.vpp.In.FrameRateExtN, m_VPPInitParams.vpp.In.FrameRateExtD);

            hr = mf_align_geometry(&(m_VPPInitParams.vpp.In));

            if (m_pOutputType)
            {
                // setting cropping parameters (letter & pillar boxing)
                mf_set_cropping(&(m_VPPInitParams.vpp));
                // re-synchronizing VPP and ENC parameters
                memcpy(&(m_MfxParamsVideo.mfx.FrameInfo), &(m_VPPInitParams.vpp.Out), sizeof(mfxFrameInfo));
                memcpy(&(m_ENCInitInfo_original), &(m_MfxParamsVideo.mfx), sizeof(mfxInfoMFX));
            }
        }
        if (SUCCEEDED(hr) && !CheckHwSupport()) hr = E_FAIL;
        if (FAILED(hr))
        {
            ReleaseMediaType(m_pInputType);
        }
    }
    DBG_TRACE_1("m_pInputType = %p, m_pInputTypeCandidate = %p", m_pInputType);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::SetOutputType(DWORD         dwOutputStreamID,
                                    IMFMediaType* pType,
                                    DWORD         dwFlags)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE_1("pType = %p: +", pType);
    HRESULT hr = S_OK;

    GetPerformance();
    hr = MFPlugin::SetOutputType(dwOutputStreamID, pType, dwFlags);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);

    if (!pType)
    { // resetting media type
        SAFE_RELEASE(m_pOutputType);
    }
    CHECK_POINTER(pType, S_OK);
    // Validate the type: general checking, output type checking
    if (SUCCEEDED(hr)) hr = CheckMediaType(pType, m_Reg.pOutputTypes, m_Reg.cOutputTypes, &m_pOutputInfo);
#ifdef DISABLE_VPP_ON_CONNECTION_STAGE
    if (SUCCEEDED(hr)) hr = (IsVppNeeded(pType, m_pOutputType))? E_FAIL: S_OK;
#endif
    if (0 == (dwFlags & MFT_SET_TYPE_TEST_ONLY))
    {
        if (SUCCEEDED(hr))
        {
            // set major type; check major type is the same as was on input
            if (GUID_NULL == m_MajorType)
                m_MajorType = m_pOutputInfo->major_type;
            else if (m_pOutputInfo->major_type != m_MajorType)
                hr = MF_E_INVALIDTYPE;
        }
        if (stHeaderSetAwaiting == m_State)
        {
            DBG_TRACE("Second set of type (header)");
            if (SUCCEEDED(hr) && (m_pOutputType != pType)) hr = m_pOutputType->CopyAllItems(pType);
            if (SUCCEEDED(hr)) hr = pType->SetBlob(MF_MT_MPEG_SEQUENCE_HEADER, m_pHeader, m_uiHeaderSize);
            if (SUCCEEDED(hr))
            {
                SAFE_RELEASE(m_pOutputType);
                pType->AddRef();
                m_pOutputType = pType;
            }
            m_State = stHeaderSampleNotSent;

            HRESULT hr_sts = SendOutput();
            if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
        }
        else
        {
            DBG_TRACE("First set of type");
            if (SUCCEEDED(hr))
            {
                SAFE_RELEASE(m_pOutputType);
                pType->AddRef();
                m_pOutputType = pType;
            }
            if (SUCCEEDED(hr)) hr = mf_mftype2mfx_info(m_pOutputType, &(m_MfxParamsVideo.mfx));
            if (SUCCEEDED(hr))
            {
                memcpy(&(m_VPPInitParams_original.vpp.Out),&(m_MfxParamsVideo.mfx.FrameInfo),sizeof(mfxFrameInfo));
                hr = mf_align_geometry(&(m_MfxParamsVideo.mfx.FrameInfo));
                // synchronizing VPP and ENC parameters
                memcpy(&(m_VPPInitParams.vpp.Out), &(m_MfxParamsVideo.mfx.FrameInfo), sizeof(mfxFrameInfo));

                if (m_pInputType)
                {
                    // setting cropping parameters (letter & pillar boxing)
                    mf_set_cropping(&(m_VPPInitParams.vpp));
                    // re-synchronizing VPP and ENC parameters
                    memcpy(&(m_MfxParamsVideo.mfx.FrameInfo), &(m_VPPInitParams.vpp.Out), sizeof(mfxFrameInfo));
                    memcpy(&(m_ENCInitInfo_original), &(m_MfxParamsVideo.mfx), sizeof(mfxInfoMFX));
                }
            }
            if (SUCCEEDED(hr) && !CheckHwSupport()) hr = E_FAIL;
            if (FAILED(hr))
            {
                SAFE_RELEASE(m_pOutputType);
            }
        }
    }
    DBG_TRACE_1("m_pOutputType = %p", m_pOutputType);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::ProcessMessage(MFT_MESSAGE_TYPE eMessage,
                                     ULONG_PTR        ulParam)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    GetPerformance();
    hr = MFPlugin::ProcessMessage(eMessage, ulParam);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);
    switch (eMessage)
    {
    case MFT_MESSAGE_COMMAND_FLUSH:
        DBG_TRACE("MFT_MESSAGE_COMMAND_FLUSH");
        if (MFX_ERR_NONE != ResetCodec(lock)) hr = hr;//E_FAIL;
        break;

    case MFT_MESSAGE_COMMAND_DRAIN:
        DBG_TRACE("MFT_MESSAGE_COMMAND_DRAIN");
        if (m_pmfxENC)
        { // processing eos only if we were initialized
            m_bEndOfInput = true;
            m_bSendDrainComplete = true;
            // we can't accept any input if draining has begun
            m_bDoNotRequestInput = true; // prevent calling RequestInput
            m_NeedInputEventInfo.m_requested = 0; // prevent processing of sent RequestInput
            if (!m_bReinit) AsyncThreadPush();
        }
        break;

    case MFT_MESSAGE_COMMAND_MARKER:
        DBG_TRACE("MFT_MESSAGE_COMMAND_MARKER");
        // we always produce as much data as possible
        if (m_pmfxENC) hr = SendMarker(ulParam);
        break;

    case MFT_MESSAGE_NOTIFY_BEGIN_STREAMING:
        DBG_TRACE("MFT_MESSAGE_NOTIFY_BEGIN_STREAMING");
        m_bStreamingStarted = true;
        break;

    case MFT_MESSAGE_NOTIFY_END_STREAMING:
        DBG_TRACE("MFT_MESSAGE_NOTIFY_END_STREAMING");
        m_bStreamingStarted = false;
        break;

    case MFT_MESSAGE_NOTIFY_START_OF_STREAM:
        DBG_TRACE("MFT_MESSAGE_NOTIFY_START_OF_STREAM");
        if (!m_pAsyncThread)
        {
            SAFE_NEW(m_pAsyncThread, MyThread(hr, thAsyncThreadFunc, this));
            if (SUCCEEDED(hr) && !m_pAsyncThread) hr = E_FAIL;
        }
        else if (MFX_ERR_NONE != ResetCodec(lock)) hr = E_FAIL;
        m_bStreamingStarted  = true;
        m_bDoNotRequestInput = false;
        if (SUCCEEDED(hr)) hr = RequestInput();
        break;

    case MFT_MESSAGE_NOTIFY_END_OF_STREAM:
        DBG_TRACE("MFT_MESSAGE_NOTIFY_END_OF_STREAM");
        // stream ends, we must not accept input data
        m_bDoNotRequestInput = true; // prevent calling RequestInput
        m_NeedInputEventInfo.m_requested = 0; // prevent processing of sent RequestInput
        break;

    case MFT_MESSAGE_SET_D3D_MANAGER:
        DBG_TRACE("MFT_MESSAGE_SET_D3D_MANAGER");
        if (ulParam) hr = E_NOTIMPL; // this return status is an exception (see MS docs)
        break;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFPluginVEnc::QueryENC(mfxVideoParam* pParams,
                                 mfxVideoParam* pCorrectedParams)
{
    mfxStatus sts = MFX_ERR_NONE;

    if (MFX_ERR_NONE == sts)
    {
        bool bLevelChanged = false;

        pCorrectedParams->mfx.CodecId = pParams->mfx.CodecId;
        if ((MFX_CODEC_AVC == pParams->mfx.CodecId) &&
            (MFX_PROFILE_UNKNOWN != pParams->mfx.CodecProfile) &&
            (MFX_LEVEL_UNKNOWN == pParams->mfx.CodecLevel))
        { // setting mimimal level to enable it's correction
            pParams->mfx.CodecLevel = MFX_LEVEL_AVC_1;
            bLevelChanged = true;
        }
        if ((MFX_WRN_INCOMPATIBLE_VIDEO_PARAM == (sts = m_pmfxENC->Query(pParams, pCorrectedParams))) &&
            bLevelChanged)
        {
            pParams->mfx.CodecLevel = pCorrectedParams->mfx.CodecLevel;
            sts = m_pmfxENC->Query(pParams, pCorrectedParams);
        }
    }
    return sts;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFPluginVEnc::CheckMfxParams(void)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    mfxVideoParam corrected_params;

    memset(&corrected_params, 0, sizeof(mfxVideoParam));
    if (MFX_ERR_NONE == sts)
    {
        sts = QueryENC(&m_MfxParamsVideo, &corrected_params);
        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts)) sts = MFX_ERR_NONE;
    }
    if (MFX_ERR_NONE == sts)
    {
        DBG_TRACE_2("EP: requested: profile = %d, level = %d", m_MfxParamsVideo.mfx.CodecProfile, m_MfxParamsVideo.mfx.CodecLevel);
        DBG_TRACE_2("EP: obtained:  profile = %d, level = %d", corrected_params.mfx.CodecProfile, corrected_params.mfx.CodecLevel);
        if (MFX_PROFILE_UNKNOWN != m_MfxParamsVideo.mfx.CodecProfile)
        { // check profile only if it was requested
            if (corrected_params.mfx.CodecProfile != m_MfxParamsVideo.mfx.CodecProfile)
            {
                DBG_TRACE("EP: status: invalid profile obtained");
                sts = MFX_ERR_INVALID_VIDEO_PARAM;
            }
        }
        else m_MfxParamsVideo.mfx.CodecProfile = corrected_params.mfx.CodecProfile;
        if (MFX_LEVEL_UNKNOWN != m_MfxParamsVideo.mfx.CodecLevel)
        { // check level only if it was requested
            if (corrected_params.mfx.CodecLevel != m_MfxParamsVideo.mfx.CodecLevel)
            {
                DBG_TRACE("EP: status: invalid level obtained");
                sts = MFX_ERR_INVALID_VIDEO_PARAM;
            }
        }
        else m_MfxParamsVideo.mfx.CodecLevel = corrected_params.mfx.CodecLevel;
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize Frame Allocator

mfxStatus MFPluginVEnc::InitFRA(void)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;

    if (m_pDeviceManager)
    {
        if (IS_MEM_VIDEO(m_uiInSurfacesMemType) ||
            IS_MEM_VIDEO(m_uiOutSurfacesMemType))
        {
            DBG_TRACE("Creating frame allocator");
            if (!m_pFrameAllocator)
            {
                if (MFX_ERR_NONE == sts)
                {
                    sts = m_pMfxVideoSession->SetHandle(MFX_HANDLE_D3D9_DEVICE_MANAGER, m_pDeviceManager);
                }
                if (MFX_ERR_NONE == sts)
                {
                    SAFE_NEW(m_pFrameAllocator, MFFrameAllocator);
                    if (!m_pFrameAllocator) sts = MFX_ERR_MEMORY_ALLOC;
                    else m_pFrameAllocator->AddRef();
                }
                if (MFX_ERR_NONE == sts)
                {
                    D3DAllocatorParams AllocParams;

                    m_pDeviceManager->AddRef();
                    AllocParams.pManager = m_pDeviceManager;
                    sts = m_pFrameAllocator->Init(&AllocParams);
                    SAFE_RELEASE(AllocParams.pManager);
                }
                if (MFX_ERR_NONE == sts) sts = m_pMfxVideoSession->SetFrameAllocator(m_pFrameAllocator);
                DBG_TRACE_1("EP: m_pFrameAllocator = %p", m_pFrameAllocator);
            }
        }
        else
        {
            DBG_TRACE_1("EP: m_pFrameAllocator = %p (SW fallback)", m_pFrameAllocator);
            DXVASupportClose();
        }
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize input surfaces

mfxStatus MFPluginVEnc::InitInSRF(mfxU32* pSurfacesNum)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    mfxU32 i = 0;

    if (m_pmfxVPP)
    {
        if (MFX_ERR_NONE == sts) sts = InitFRA();
        if ((MFX_ERR_NONE == sts) &&
            m_pFrameAllocator && IS_MEM_VIDEO(m_uiInSurfacesMemType))
        {
            mfxFrameAllocRequest request;

            memset((void*)&request, 0, sizeof(mfxFrameAllocRequest));
            memset((void*)&m_VppAllocResponse, 0, sizeof(mfxFrameAllocResponse));
            memcpy(&(request.Info),&(m_VPPInitParams.vpp.In),sizeof(mfxFrameInfo));
            request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET |
                           MFX_MEMTYPE_FROM_DECODE | MFX_MEMTYPE_FROM_VPPIN;
            request.NumFrameMin = request.NumFrameSuggested = (mfxU16)m_nInSurfacesNum;

            sts = m_pFrameAllocator->Alloc(m_pFrameAllocator->pthis, &request, &m_VppAllocResponse);
            if (MFX_ERR_NONE == sts)
            {
                if (m_nInSurfacesNum > m_VppAllocResponse.NumFrameActual) sts = MFX_ERR_MEMORY_ALLOC;
                else
                {
                    m_nInSurfacesNum = m_VppAllocResponse.NumFrameActual;
                    if (pSurfacesNum) *pSurfacesNum = m_nInSurfacesNum;
                }
            }
        }
        if (MFX_ERR_NONE == sts)
        {
            SAFE_NEW_ARRAY(m_pInSurfaces, MFYuvInSurface, m_nInSurfacesNum);
            if (!m_pInSurfaces) sts = MFX_ERR_MEMORY_ALLOC;
            else
            {
                for (i = 0; (MFX_ERR_NONE == sts) && (i < m_nInSurfacesNum); ++i)
                {
                    m_pInSurfaces[i].SetFile(m_dbg_vppin);
                    sts = m_pInSurfaces[i].Init(&(m_VPPInitParams.vpp.In),
                                                (m_bDirectConnectionMFX)? NULL: &(m_VPPInitParams_original.vpp.In),
                                                NULL, false);
                }
            }
            if (MFX_ERR_NONE == sts) m_pInSurface = &(m_pInSurfaces[0]);
        }
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize VPP (if needed)

mfxStatus MFPluginVEnc::InitVPP(mfxU32* pSurfacesNum)
{
    DBG_TRACE_1("pSurfacesNum = %d: +", (pSurfacesNum)? *pSurfacesNum: 0);
    mfxStatus sts = MFX_ERR_NONE;
    bool bUseVpp = UseVPP(&(m_VPPInitParams.vpp));

    // VPP creation (if needed)
    if (MFX_ERR_NONE == sts)
    {
        m_MfxCodecInfo.m_bVppUsed = bUseVpp;
        if (bUseVpp && !m_pmfxVPP)
        {
            MFXVideoSession* session = (MFXVideoSession*)m_pMfxVideoSession;

            SAFE_NEW(m_pmfxVPP, MFXVideoVPPEx(*session));
            if (!m_pmfxVPP) sts = MFX_ERR_MEMORY_ALLOC;
        }
        if (!bUseVpp && m_pmfxVPP)
        {
            SAFE_DELETE(m_pmfxVPP);
        }
    }
    DBG_TRACE_1("EP: m_pmfxVPP = %p (use vpp)", m_pmfxVPP);
    if (m_pmfxVPP)
    {
        // VPP surfaces allocation
        if (MFX_ERR_NONE == sts)
        {
            mfxFrameAllocRequest VPPRequest[2]; // [0] - input, [1] - output

            memcpy(&(m_VPPInitParams.vpp.Out),&(m_MfxParamsVideo.mfx.FrameInfo),sizeof(mfxFrameInfo));
            memset((void*)&VPPRequest, 0, 2*sizeof(mfxFrameAllocRequest));

            sts = m_pmfxVPP->QueryIOSurf(&m_VPPInitParams, VPPRequest);
            if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts))
            {
                sts = MFX_ERR_NONE;
                if ((dbgHwMemory != m_dbg_Memory) && IS_MEM_VIDEO(m_uiOutSurfacesMemType))
                {
                    DBG_TRACE("Switching to System Memory (VPP out = ENC in)");
                    m_uiOutSurfacesMemType    = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;
                    m_VPPInitParams.IOPattern = m_uiInSurfacesMemType | m_uiOutSurfacesMemType;

                    sts = m_pmfxVPP->QueryIOSurf(&m_VPPInitParams, VPPRequest);
                    if (MFX_WRN_PARTIAL_ACCELERATION == sts) sts = MFX_ERR_NONE;
                }
            }
            if (MFX_ERR_NONE == sts)
            {
                mfxU32 in_frames_num  = MAX((VPPRequest[0]).NumFrameSuggested, MAX((VPPRequest[0]).NumFrameMin, 1));
                mfxU32 out_frames_num = MAX((VPPRequest[1]).NumFrameSuggested, MAX((VPPRequest[1]).NumFrameMin, 1));

                m_nInSurfacesNum  += MF_ADDITIONAL_SURF_MULTIPLIER * in_frames_num;
                m_nOutSurfacesNum += MF_ADDITIONAL_SURF_MULTIPLIER * out_frames_num;

                if (pSurfacesNum)
                {
                    m_nInSurfacesNum += (*pSurfacesNum) - 1;
                    *pSurfacesNum = m_nInSurfacesNum;
                }
            }
        }
        if (MFX_ERR_NONE == sts) sts = InitInSRF(pSurfacesNum);
        // VPP initialization
        if (MFX_ERR_NONE == sts)
        {
            sts = m_pmfxVPP->Init(&m_VPPInitParams);
            if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts)) sts = MFX_ERR_NONE;
        }
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/

void MFPluginVEnc::CheckBitstreamsNumLimit(void)
{
    if (m_uiHasOutputEventExists > MF_OUTPUT_EVENTS_COUNT_HIGH_LIMIT)
    {
        m_bBitstreamsLimit = true;
    }
    else if (m_uiHasOutputEventExists > MF_OUTPUT_EVENTS_COUNT_LOW_LIMIT)
    {
        if (!m_bBitstreamsLimit) m_bBitstreamsLimit = false;
    }
    else m_bBitstreamsLimit = false;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFPluginVEnc::SetFreeBitstream(void)
{
    mfxStatus sts = MFX_ERR_NONE;
    mfxU32 i = 0;
    MFEncBitstream** pMFBitstream = NULL;

    if (!m_pOutBitstreams)
    {
        m_pOutBitstreams = (MFEncOutData*)calloc(MF_BITSTREAMS_NUM, sizeof(MFEncOutData));
        if (!m_pOutBitstreams) sts = MFX_ERR_MEMORY_ALLOC;
        else
        {
            m_nOutBitstreamsNum = MF_BITSTREAMS_NUM;
            for (i = 0; (MFX_ERR_NONE == sts) && (i < m_nOutBitstreamsNum); ++i)
            {
                m_pOutBitstreams[i].bFreeData = true;

                pMFBitstream = &(m_pOutBitstreams[i].pMFBitstream);
                SAFE_NEW(*pMFBitstream, MFEncBitstream);
                if (!(*pMFBitstream)) sts = MFX_ERR_MEMORY_ALLOC;
                if (MFX_ERR_NONE == sts) sts = (*pMFBitstream)->Init(&m_MfxParamsVideo, m_pFreeSamplesPool);
                if (MFX_ERR_NONE == sts) sts = (*pMFBitstream)->Alloc();
                if (MFX_ERR_NONE == sts) (*pMFBitstream)->SetFile(m_dbg_encout);
                if (MFX_ERR_NONE == sts)
                {
                    SAFE_NEW(m_pOutBitstreams[i].pSyncPoint, mfxSyncPoint);
                    if (!m_pOutBitstreams[i].pSyncPoint) sts = MFX_ERR_MEMORY_ALLOC;
                }
            }
            if (MFX_ERR_NONE == sts)
            {
                m_pOutBitstream  = m_pOutBitstreams;
                m_pDispBitstream = m_pOutBitstreams;
            }
        }
    }
    else if (!(m_pOutBitstream->bFreeData))
    {
        ++m_pOutBitstream;
        if ((mfxU32)(m_pOutBitstream - m_pOutBitstreams) >= m_nOutBitstreamsNum) m_pOutBitstream = m_pOutBitstreams;
        if (!(m_pOutBitstream->bFreeData)) // i. e. m_pOutBitstream == m_pDispBitstream
        { // increasing number of output bitstreams
            MFEncOutData *pOutBitstreams = NULL;
            mfxU32 disp_index = (mfxU32)(m_pDispBitstream - m_pOutBitstreams);

            pOutBitstreams = (MFEncOutData*)realloc(m_pOutBitstreams, (m_nOutBitstreamsNum+1)*sizeof(MFEncOutData));
            if (!pOutBitstreams) sts = MFX_ERR_MEMORY_ALLOC;
            else
            {
                m_pOutBitstreams = pOutBitstreams;
                ++m_nOutBitstreamsNum;
                m_pOutBitstream  = &(m_pOutBitstreams[disp_index]);
                m_pDispBitstream = &(m_pOutBitstreams[disp_index+1]);

                // shifting tail of the array
                memmove(m_pDispBitstream, m_pOutBitstream, (m_nOutBitstreamsNum - disp_index - 1)*sizeof(MFEncOutData));

                // initializing new bitstream instance
                memset(m_pOutBitstream, 0, sizeof(MFEncOutData));
                m_pOutBitstream->bFreeData = true;
                pMFBitstream = &(m_pOutBitstream->pMFBitstream);

                SAFE_NEW(*pMFBitstream, MFEncBitstream);
                if (!(*pMFBitstream)) sts = MFX_ERR_MEMORY_ALLOC;
                if (MFX_ERR_NONE == sts) sts = (*pMFBitstream)->Init(&m_MfxParamsVideo, m_pFreeSamplesPool);
                if (MFX_ERR_NONE == sts) sts = (*pMFBitstream)->Alloc();
                if (MFX_ERR_NONE == sts) (*pMFBitstream)->SetFile(m_dbg_encout);
                if (MFX_ERR_NONE == sts)
                {
                    SAFE_NEW(m_pOutBitstream->pSyncPoint, mfxSyncPoint);
                    if (!m_pOutBitstream->pSyncPoint) sts = MFX_ERR_MEMORY_ALLOC;
                }
            }
        }
    }
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize Encoder

mfxStatus MFPluginVEnc::InitENC(mfxU32* pSurfacesNum)
{
    DBG_TRACE_1("pSurfacesNum = %d: +", (pSurfacesNum)? *pSurfacesNum: 0);
    mfxStatus sts = MFX_ERR_NONE;
    mfxU32 i = 0;

    // Encoder creation
    // encoder surfaces allocation
    if (MFX_ERR_NONE == sts)
    {
        mfxFrameAllocRequest EncoderRequest;

        memset((void*)&EncoderRequest, 0, sizeof(mfxFrameAllocRequest));
        if (!m_pmfxVPP) m_uiOutSurfacesMemType = (mfxU16)MEM_IN_TO_OUT(m_uiInSurfacesMemType);
        m_MfxParamsVideo.IOPattern = (mfxU16)MEM_OUT_TO_IN(m_uiOutSurfacesMemType);

        sts = m_pmfxENC->QueryIOSurf(&m_MfxParamsVideo, &EncoderRequest);
        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts)) sts = MFX_ERR_NONE;
        if (MFX_ERR_NONE == sts)
        {
            m_nOutSurfacesNum += MAX(EncoderRequest.NumFrameSuggested, MAX(EncoderRequest.NumFrameMin, 1));
            if (m_pmfxVPP)
            {
                m_nOutSurfacesNum -= 1;
            }
            else if (pSurfacesNum)
            {
                m_nOutSurfacesNum += (*pSurfacesNum) - 1;
                *pSurfacesNum = m_nOutSurfacesNum;
            }
        }
    }
    if (MFX_ERR_NONE == sts) sts = InitFRA();
    if ((MFX_ERR_NONE == sts) && m_pFrameAllocator && IS_MEM_VIDEO(m_uiOutSurfacesMemType))
    {
        mfxFrameAllocRequest request;

        memset((void*)&request, 0, sizeof(mfxFrameAllocRequest));
        memcpy(&(request.Info),&(m_MfxParamsVideo.mfx.FrameInfo),sizeof(mfxFrameInfo));
        if (m_pmfxVPP)
        {
            request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET |
                           MFX_MEMTYPE_FROM_VPPOUT | MFX_MEMTYPE_FROM_ENCODE;
        }
        else
        {
            if (m_bConnectedWithVpp)
            {
                request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET |
                               MFX_MEMTYPE_FROM_VPPOUT | MFX_MEMTYPE_FROM_ENCODE;
            }
            else
            {
                request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET |
                               MFX_MEMTYPE_FROM_DECODE | MFX_MEMTYPE_FROM_ENCODE;
            }
        }
        request.NumFrameMin = request.NumFrameSuggested = (mfxU16)m_nOutSurfacesNum;
        memset(&m_EncAllocResponse, 0, sizeof(mfxFrameAllocResponse));

        sts = m_pFrameAllocator->Alloc(m_pFrameAllocator->pthis, &request, &m_EncAllocResponse);
        if (MFX_ERR_NONE == sts)
        {
            if (m_nOutSurfacesNum > m_EncAllocResponse.NumFrameActual) sts = MFX_ERR_MEMORY_ALLOC;
            else
            {
                m_nOutSurfacesNum = m_EncAllocResponse.NumFrameActual;
                if (!m_pmfxVPP && pSurfacesNum) *pSurfacesNum = m_nOutSurfacesNum;
            }
        }
    }
    if (MFX_ERR_NONE == sts)
    {
        SAFE_NEW_ARRAY(m_pOutSurfaces, MFYuvInSurface, m_nOutSurfacesNum);
        if (!m_pOutSurfaces) sts = MFX_ERR_MEMORY_ALLOC;
        else
        {
            for (i = 0; (MFX_ERR_NONE == sts) && (i < m_nOutSurfacesNum); ++i)
            {
                m_pOutSurfaces[i].SetFile((m_pmfxVPP)? m_dbg_vppout: m_dbg_vppin);
                sts = m_pOutSurfaces[i].Init(&(m_MfxParamsVideo.mfx.FrameInfo),
                                             (m_pmfxVPP)? NULL: ((m_bDirectConnectionMFX)? NULL: &(m_VPPInitParams_original.vpp.In)),
                                             (m_pmfxVPP && m_pFrameAllocator && m_EncAllocResponse.mids)? m_EncAllocResponse.mids[i]: NULL,
                                             (m_pmfxVPP && (!m_pFrameAllocator || !m_EncAllocResponse.mids)));
            }
        }
        if (MFX_ERR_NONE == sts) m_pOutSurface = &(m_pOutSurfaces[0]);
    }
    // Encoder initialization
    if (MFX_ERR_NONE == sts) sts = CheckMfxParams();
    if (MFX_ERR_NONE == sts)
    {
        sts = m_pmfxENC->Init(&m_MfxParamsVideo);
        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts) ||
            (MFX_WRN_INCOMPATIBLE_VIDEO_PARAM == sts)) sts = MFX_ERR_NONE;
        // currently, we need to send SPS/PPS data only for H.264 encoder
        if (MFX_CODEC_AVC != m_MfxParamsVideo.mfx.CodecId) m_State = stReady;
    }
    if (MFX_ERR_NONE == sts) sts = m_pmfxENC->GetVideoParam(&m_MfxParamsVideo);
    // Free samples pool allocation
    if (MFX_ERR_NONE == sts)
    {
        SAFE_RELEASE(m_pFreeSamplesPool);
        if (!m_pFreeSamplesPool)
        {
            SAFE_NEW(m_pFreeSamplesPool, MFSamplesPool);
        }
        if (!m_pFreeSamplesPool) sts = MFX_ERR_MEMORY_ALLOC;
        else
        {
            m_pFreeSamplesPool->AddRef();
            if (FAILED(m_pFreeSamplesPool->Init(MF_BITSTREAMS_NUM))) sts = MFX_ERR_UNKNOWN;
        }
    }
    if (MFX_ERR_NONE == sts) sts = SetFreeBitstream();
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize codec

mfxStatus MFPluginVEnc::InitCodec(MyAutoLock& lock,
                                  mfxU16  uiMemPattern,
                                  mfxU32* pSurfacesNum)
{
    DBG_TRACE_2("uiMemPattern = %d, pSurfacesNum = %d: +",
        uiMemPattern, (pSurfacesNum)? *pSurfacesNum: 0);
    mfxStatus sts = MFX_ERR_NONE;

    // Delitioning of unneded properties:
    // MF_MT_D3D_DEVICE - it contains pointer to enc plug-in, need to delete
    // it or enc destructor will not be invoked
    m_pInputType->DeleteItem(MF_MT_D3D_DEVICE);
    // parameters correction
    if (MFX_ERR_NONE == sts)
    {
        // adjustment of out memory type
        if (dbgSwMemory == m_dbg_Memory) m_uiOutSurfacesMemType = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;

        if (pSurfacesNum)
        {
            m_uiInSurfacesMemType = (mfxU16)MEM_OUT_TO_IN(uiMemPattern);
            if (IS_MEM_VIDEO(m_uiInSurfacesMemType))
                m_bShareAllocator = true;
        }
        if ((dbgHwMemory != m_dbg_Memory) && (MFX_IMPL_SOFTWARE == m_MSDK_impl))
        {
            if (!m_bShareAllocator)
            {
                DXVASupportClose();
                m_uiInSurfacesMemType  = MFX_IOPATTERN_IN_SYSTEM_MEMORY;
            }
            m_uiOutSurfacesMemType = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;
        }
        m_VPPInitParams.IOPattern  = m_uiInSurfacesMemType | m_uiOutSurfacesMemType;
        DBG_TRACE_1("EP: m_uiInSurfacesMemType  = %d", m_uiInSurfacesMemType);
        DBG_TRACE_1("EP: m_uiOutSurfacesMemType = %d", m_uiOutSurfacesMemType);
    }
    // Setting some encoder & VPP parameters
    if (MFX_ERR_NONE == sts)
    {
        mfxU32 colorFormat  = mf_get_color_format (m_pInputInfo->subtype.Data1);
        mfxU16 chromaFormat = mf_get_chroma_format(m_pInputInfo->subtype.Data1);

        if (colorFormat)
        {
            m_MfxParamsVideo.mfx.FrameInfo.FourCC = colorFormat;
            m_VPPInitParams.vpp.In.FourCC         = colorFormat;
            m_VPPInitParams.vpp.Out.FourCC        = colorFormat;

            m_MfxParamsVideo.mfx.FrameInfo.ChromaFormat = chromaFormat;
            m_VPPInitParams.vpp.In.ChromaFormat         = chromaFormat;
            m_VPPInitParams.vpp.Out.ChromaFormat        = chromaFormat;
        }
        else sts = MFX_ERR_UNSUPPORTED;
    }
    if (MFX_ERR_NONE == sts) sts = InitVPP(pSurfacesNum);
    if (MFX_ERR_NONE == sts) sts = InitENC(pSurfacesNum);
    m_MfxCodecInfo.m_InitStatus = sts;
    if (MFX_ERR_NONE != sts) CloseCodec(lock);
    else
    {
        m_bInitialized = true;
        m_MfxCodecInfo.m_uiInFramesType     = m_uiInSurfacesMemType;
        m_MfxCodecInfo.m_uiVppOutFramesType = m_uiOutSurfacesMemType;
        DBG_TRACE_1("EP: obtained: m_MSDK_impl = %d", m_MSDK_impl);
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/

void MFPluginVEnc::CloseCodec(MyAutoLock& /*lock*/)
{
    DBG_TRACE("+");
    mfxU32 i = 0;

    if (m_pmfxVPP) m_pmfxVPP->Close();
    if (m_pmfxENC) m_pmfxENC->Close();
    SAFE_RELEASE(m_pFreeSamplesPool);
    SAFE_DELETE_ARRAY(m_pInSurfaces);
    SAFE_DELETE_ARRAY(m_pOutSurfaces);
    if (m_pOutBitstreams)
    {
        for (i = 0; i < m_nOutBitstreamsNum; ++i)
        {
            SAFE_DELETE(m_pOutBitstreams[i].pMFBitstream);
            SAFE_DELETE(m_pOutBitstreams[i].pSyncPoint);
        }
    }
    SAFE_FREE(m_pOutBitstreams);
    SAFE_FREE(m_pHeader);
    if (m_pFrameAllocator)
    {
        if (m_pmfxVPP && IS_MEM_VIDEO(m_uiInSurfacesMemType))
            m_pFrameAllocator->Free(m_pFrameAllocator->pthis, &m_VppAllocResponse);
        if (IS_MEM_VIDEO(m_uiOutSurfacesMemType))
            m_pFrameAllocator->Free(m_pFrameAllocator->pthis, &m_EncAllocResponse);
        memset((void*)&m_VppAllocResponse, 0, sizeof(mfxFrameAllocResponse));
        memset((void*)&m_EncAllocResponse, 0, sizeof(mfxFrameAllocResponse));
    }
    if (!m_bReinit)
    {
        SAFE_RELEASE(m_pFrameAllocator);
        SAFE_RELEASE(m_pFakeSample);
    }
    // return parameters to initial state
    {
        if (m_Reg.pFreeParams) m_Reg.pFreeParams(&m_MfxParamsVideo);
        memset(&m_MfxParamsVideo, 0, sizeof(mfxVideoParam));
        m_Reg.pFillParams(&m_MfxParamsVideo);
        memcpy(&(m_MfxParamsVideo.mfx), &m_ENCInitInfo_original, sizeof(mfxInfoMFX));
    }
    m_State                = (m_bReinit)? stReady: stHeaderNotSet; // no need to send header again on reinit
    m_uiInSurfacesMemType  = MFX_IOPATTERN_IN_SYSTEM_MEMORY;
    m_uiOutSurfacesMemType = MFX_IOPATTERN_OUT_VIDEO_MEMORY;
    m_nInSurfacesNum       = MF_ADDITIONAL_IN_SURF_NUM;
    m_nOutSurfacesNum      = MF_ADDITIONAL_OUT_SURF_NUM;
    m_nOutBitstreamsNum    = 0;
    m_pInSurface           = NULL;
    m_pOutSurface          = NULL;
    m_pOutBitstream        = NULL;
    m_pDispBitstream       = NULL;
    m_uiHeaderSize         = 0;
    m_iNumberInputSamples  = 0;

    m_bInitialized = false;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFPluginVEnc::ResetCodec(MyAutoLock& /*lock*/)
{
    mfxStatus sts = MFX_ERR_NONE, res = MFX_ERR_NONE;
    mfxU32 i = 0;
    MFEncOutData* pDispBitstream = NULL;

    m_pAsyncThreadSemaphore->Reset();
    m_pAsyncThreadEvent->Reset();
    while (m_uiHasOutputEventExists && (m_pDispBitstream != m_pOutBitstream))
    {
        --m_uiHasOutputEventExists;
        // dropping encoded frames
        if (!(m_pDispBitstream->bSyncPointUsed))
        {
            m_pMfxVideoSession->SyncOperation(*(m_pDispBitstream->pSyncPoint),
                                              INFINITE);
        }
        m_pDispBitstream->bFreeData      = true;
        m_pDispBitstream->bSyncPointUsed = false;
        m_pDispBitstream->iSyncPointSts  = MFX_ERR_NONE;
        // moving to next undisplayed surface
        pDispBitstream = m_pDispBitstream;
        ++pDispBitstream;
        if ((mfxU32)(pDispBitstream-m_pOutBitstreams) >= m_nOutBitstreamsNum)
            pDispBitstream = m_pOutBitstreams;
        m_pDispBitstream = pDispBitstream;
    }
    if (m_pmfxVPP)
    { // resetting encoder
        res = m_pmfxVPP->Reset(&m_VPPInitParams);
        if (MFX_ERR_NONE == sts) sts = res;
    }
    if (m_pmfxENC)
    { // resetting encoder
        res = m_pmfxENC->Reset(&m_MfxParamsVideo);

        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == res) ||
            (MFX_WRN_INCOMPATIBLE_VIDEO_PARAM == res))
        {
            res = MFX_ERR_NONE;
        }

        if (MFX_ERR_NONE == sts) sts = res;
    }
    if (m_pInSurfaces)
    { // releasing resources
        for (i = 0; i < m_nInSurfacesNum; ++i)
        {
            if (FAILED(m_pInSurfaces[i].Release()) && (MFX_ERR_NONE == sts)) sts = MFX_ERR_UNKNOWN;
        }
    }
    if (m_pOutSurfaces)
    { // releasing resources
        for (i = 0; i < m_nOutSurfacesNum; ++i)
        {
            if (FAILED(m_pOutSurfaces[i].Release()) && (MFX_ERR_NONE == sts)) sts = MFX_ERR_UNKNOWN;
        }
    }
    m_uiHeaderSize = 0;
    SAFE_FREE(m_pHeader);

    m_State = stReady;

    m_bEndOfInput      = false;
    m_bStartDrain      = false;
    m_bStartDrainEnc   = false;
    m_bNeedInSurface   = false;
    m_bNeedOutSurface  = false;
    m_bBitstreamsLimit = false;

    if (!m_bStreamingStarted)
    {
        /* We should zero event counts only if we are resetting before
         * processing new stream. We should not zero them on errors during
         * current processing - this may cause hangs, because ProcessOutput
         * may be called out of order.
         */
        m_NeedInputEventInfo.m_requested = 0;
        m_HasOutputEventInfo.m_requested = 0;
        m_NeedInputEventInfo.m_sent = 0;
        m_HasOutputEventInfo.m_sent = 0;
    }
    m_uiHasOutputEventExists = 0;
    m_iNumberInputSamples = 0;

    m_LastSts   = MFX_ERR_NONE;
    m_SyncOpSts = MFX_ERR_NONE;
    return sts;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::TryReinit(MyAutoLock& lock, mfxStatus& sts)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    sts = MFX_ERR_NONE;
    CloseCodec(lock);
    m_bReinit = false;
    m_bSetDiscontinuityAttribute = true;
    SAFE_RELEASE(m_pFakeSample);
    /* Releasing here fake sample we will invoke SampleAppeared function
     * in upstream vpp (or decoder) plug-in and it will continue processing.
     */
    if (!m_bDoNotRequestInput && !m_NeedInputEventInfo.m_requested) hr = RequestInput();
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFPluginVEnc::InitPlg(IMfxVideoSession* pSession,
                                mfxVideoParam*    pVideoParams,
                                mfxU32*           pSurfacesNum)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;

    if (FAILED(m_hrError)) sts = MFX_ERR_UNKNOWN;
    if ((MFX_ERR_NONE == sts) && !pSession) sts = MFX_ERR_NULL_PTR;
    if ((MFX_ERR_NONE == sts) && !pVideoParams) sts = MFX_ERR_NULL_PTR;
    if ((MFX_ERR_NONE == sts) && !pSurfacesNum) sts = MFX_ERR_NULL_PTR;
    if (MFX_ERR_NONE == sts)
    {
        m_bDirectConnectionMFX = true;
        m_bConnectedWithVpp = (pVideoParams->IOPattern & (MFX_IOPATTERN_IN_VIDEO_MEMORY | MFX_IOPATTERN_IN_SYSTEM_MEMORY)) &&
                              (pVideoParams->IOPattern & (MFX_IOPATTERN_OUT_VIDEO_MEMORY | MFX_IOPATTERN_OUT_SYSTEM_MEMORY));
        if (!m_bConnectedWithVpp && !(IS_MEM_OUT(pVideoParams->IOPattern)))
        {
            sts = MFX_ERR_UNSUPPORTED;
        }
    }
    if (MFX_ERR_NONE == sts)
    { // correcting of parameters obtained on plug-ins connection
        if (m_bConnectedWithVpp)
        {
            if ((m_VPPInitParams.vpp.In.Width != pVideoParams->vpp.Out.Width) ||
                (m_VPPInitParams.vpp.In.Height != pVideoParams->vpp.Out.Height))
            {
                sts = MFX_ERR_UNKNOWN;
            }
            /* We should not copy vpp info here because it may contain letter boxing settings which
             * will be interpreted by encoder as crop area, instead encoder should process the whole
             * frame with letter boxing:
             *   memcpy(&(m_VPPInitParams.vpp.In), &(pVideoParams->vpp.Out), sizeof(mfxFrameInfo));
             */
        }
        else
        {
            memcpy(&(m_VPPInitParams.vpp.In), &(pVideoParams->mfx.FrameInfo), sizeof(mfxFrameInfo));
        }
    }
    if ((MFX_ERR_NONE == sts) && pSession)
    { 
        MFXVideoVPPEx*  vpp = NULL;
        MFXVideoENCODE* enc = NULL;
        MFXVideoSession* session = (MFXVideoSession*)pSession;

        if (m_pmfxVPP)
        { // vpp may be not needed
            SAFE_NEW(vpp, MFXVideoVPPEx(*session));
        }
        SAFE_NEW(enc, MFXVideoENCODE(*session));
        if ((m_pmfxVPP && !vpp) || !enc)
        {
            SAFE_DELETE(vpp);
            SAFE_DELETE(enc);
            sts = MFX_ERR_MEMORY_ALLOC;
        }
        if (MFX_ERR_NONE == sts)
        {
            if (m_pmfxVPP)
            {
                m_pmfxVPP->Close();
                SAFE_DELETE(m_pmfxVPP);
            }
            if (m_pmfxENC)
            {
                m_pmfxENC->Close();
                SAFE_DELETE(m_pmfxENC);
            }
            if (!m_pInternalMfxVideoSession)
            {
                m_pMfxVideoSession->AddRef();
                m_pInternalMfxVideoSession = m_pMfxVideoSession;
            }
            SAFE_RELEASE(m_pMfxVideoSession);

            pSession->AddRef();
            m_pMfxVideoSession = pSession;
            m_pmfxVPP = vpp;
            m_pmfxENC = enc;
        }
    }
    if (MFX_ERR_NONE == sts)
    {
        // changing original geometry for direct connection of decoder & encoder
        memcpy(&(m_VPPInitParams_original.vpp.In), &(m_VPPInitParams.vpp.In), sizeof(mfxFrameInfo));
        // setting cropping parameters (letter & pillar boxing)
        mf_set_cropping(&(m_VPPInitParams.vpp)); // resynchronisation with enc parameters is not needed
    }
    // encoder initialization
    if (MFX_ERR_NONE == sts)
    {
        mfxU16 mem_pattern = pVideoParams->IOPattern & (MFX_IOPATTERN_OUT_VIDEO_MEMORY | MFX_IOPATTERN_OUT_SYSTEM_MEMORY);
        sts = InitCodec(lock, mem_pattern, pSurfacesNum);
    }
    /* NOTE:
     *  If plug-in error will not be set here, then crash can occur on
     * ProcessFrame call from async thread.
     */
    if ((MFX_ERR_NONE != sts) && SUCCEEDED(m_hrError))
    {
        // setting plug-in error
        SetPlgError(E_FAIL, sts);
        // handling error
        HandlePlgError(lock);
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}


/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::GetHeader(mfxBitstream* pBst)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxU8* data = NULL;
    mfxU32 len = 0, start = 0, end = 0, mask = 0;
    mfxU32 uiOldHeaderSize = 0;
    bool bSPSFound = false, bPPSFound = false;

    CHECK_POINTER(pBst && (pBst->Data), E_POINTER);

    data = &(pBst->Data[pBst->DataOffset]);
    len = pBst->DataLength;
    do
    {
        hr = FindNalUnit(len, data, &start, &end);
        if (SUCCEEDED(hr) && (end-start+1 > 4))
        {
            mask = 0x1f & data[start+3];
            DBG_TRACE_1("mask = %02x", mask);
            if ((0x07 == mask) || (0x08 == mask))
            {
                DBG_TRACE("SPS or PPS found");
                if (0x07 == mask) bSPSFound = true;
                if (0x08 == mask) bPPSFound = true;
                uiOldHeaderSize = m_uiHeaderSize;
                m_uiHeaderSize += end-start+1;

                if (!m_pHeader) m_pHeader = (mfxU8*)malloc(m_uiHeaderSize);
                else m_pHeader = (mfxU8*)realloc(m_pHeader, m_uiHeaderSize);

                if (m_pHeader) memcpy(&(m_pHeader[uiOldHeaderSize]), &(data[start]), end-start+1);
                else hr = E_FAIL;
                if (bSPSFound && bPPSFound) break;
            }
            else if ((0x01 == mask) || (0x05 == mask)) break;
            data = &(data[end+1]);
            len -= end;
        }
    } while (SUCCEEDED(hr) && len);
#if USE_DBG == DBG_YES
    mfxU32 i = 0;
    fprintf(g_dbg_file, "%S: %s: m_uiHeaderSize = %d, m_pHeader = '", DBG_MODULE_NAME, __FUNCTION__, m_uiHeaderSize);
    for (i = 0; i < m_uiHeaderSize; ++i)
    {
        fprintf(g_dbg_file, "0x%02x, ", m_pHeader[i]);
    }
    fprintf(g_dbg_file, "'\n");
#endif
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

inline MFYuvInSurface* MFPluginVEnc::SetFreeSurface(MFYuvInSurface* pSurfaces,
                                                    mfxU32 nSurfacesNum)
{
    DBG_TRACE("+");
    MFYuvInSurface* pSurface = NULL;
    HRESULT hr = S_OK;
    mfxU32 i = 0;
    bool bFound = false;

    for (i = 0; i < nSurfacesNum; ++i)
    {
        hr = pSurfaces[i].Release();
        // if release failed then surface could not be unlocked yet,
        // that's not an error
        if (SUCCEEDED(hr) && !bFound)
        {
            bFound = true;
            pSurface = &(pSurfaces[i]);
        }
    }
    DBG_TRACE_1("pSurface = %p: -", pSurface);
    return pSurface;
}

/*------------------------------------------------------------------------------*/

void MFPluginVEnc::SetPlgError(HRESULT hr, mfxStatus sts)
{
    MFPlugin::SetPlgError(hr, sts);
    if (FAILED(hr))
    { // dumping error information
        if (SUCCEEDED(m_MfxCodecInfo.m_hrErrorStatus))
        {
            m_MfxCodecInfo.m_hrErrorStatus = hr;
        }
        if (MFX_ERR_NONE == m_MfxCodecInfo.m_ErrorStatus)
        {
            m_MfxCodecInfo.m_ErrorStatus = sts;
        }
    }
    return;
}

/*------------------------------------------------------------------------------*/

void MFPluginVEnc::HandlePlgError(MyAutoLock& lock, bool bCalledFromAsyncThread)
{
    if (FAILED(m_hrError))
    {
        if (m_bErrorHandlingFinished)
        {
            if (m_bEndOfInput && m_bSendDrainComplete)
            {
                m_bEndOfInput = false;
                m_bSendDrainComplete = false;
                DrainComplete();
            }
        }
        else if (!m_bErrorHandlingStarted)
        {
            m_bErrorHandlingStarted = true;

            // notifying upstream plug-in about error occurred only if no output was generated
            if (m_pInputType && m_dbg_return_errors)
            {
                m_pInputType->SetUINT32(MF_MT_DOWNSTREAM_ERROR, m_hrError);
            }
            // notifying async thread
            if (m_pAsyncThread && !bCalledFromAsyncThread)
            {
                m_pAsyncThreadEvent->Signal();
                m_pAsyncThreadSemaphore->Post();
                // awaiting while error will be detected by async thread
                lock.Unlock();
                m_pErrorFoundEvent->Wait();
                m_pErrorFoundEvent->Reset();
                lock.Lock();
            }
            // resetting
            ResetCodec(lock); // status is not needed here

            if (m_bSendDrainComplete)
            {
                m_bSendDrainComplete = false;
                DrainComplete();
            }
            else if (m_bReinit)
            {
                mfxStatus sts = MFX_ERR_NONE;

                TryReinit(lock, sts);
            }
            else
            { // forcing calling of ProcessInput on errors during processing
                if (!m_bDoNotRequestInput && !m_NeedInputEventInfo.m_requested) RequestInput();
            }

            // sending event that error was handled
            if (m_pAsyncThread && !bCalledFromAsyncThread)
            {
                m_pErrorHandledEvent->Signal();
            }
            m_bErrorHandlingFinished = true;
        }
    }
}

/*------------------------------------------------------------------------------*/

bool MFPluginVEnc::ReturnPlgError(void)
{
    if (m_bDirectConnectionMFX) return false;
    return (m_dbg_return_errors)? true: false;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::AsyncThreadFunc(void)
{
    MyAutoLock lock(m_CritSec);
    mfxStatus sts = MFX_ERR_NONE;
    mfxSyncPoint* pSyncPoint = NULL;

    while (1)
    {
        HRESULT hr = S_OK;

        // avaiting for the signal that next surface is ready
        lock.Unlock();
        {
            m_pAsyncThreadSemaphore->Wait();
        }
        lock.Lock();
        if (m_bAsyncThreadStop) break;
        if (FAILED(m_hrError) && m_bErrorHandlingStarted && !m_bErrorHandlingFinished)
        {
            // sending event that error was detected and thread was stopped
            m_pErrorFoundEvent->Signal();
            // awaiting while error will be handled
            lock.Unlock();
            m_pErrorHandledEvent->Wait();
            m_pErrorHandledEvent->Reset();
            lock.Lock();
            continue;
        }
        if (SUCCEEDED(m_hrError) && m_bEndOfInput && !m_uiHasOutputEventExists)
        {
            if (m_bInitialized)
            {
                m_bStartDrain = true;
                if (!m_pmfxVPP) m_bStartDrainEnc = true;
                hr = ProcessFrame(sts);
            }
            else hr = MF_E_TRANSFORM_NEED_MORE_INPUT;

            if (MF_E_TRANSFORM_NEED_MORE_INPUT == hr)
            {
                hr = S_OK;

                m_bEndOfInput = false;
                m_bStartDrain = false;
                m_bStartDrainEnc = false;

                if (m_bReinit) hr = TryReinit(lock, sts);

                if (SUCCEEDED(hr))
                {
                    if (m_bSendDrainComplete)
                    {
                        m_bSendDrainComplete = false;
                        DrainComplete();
                    }
                    continue;
                }
            }
            else if (SUCCEEDED(hr))
            {
                AsyncThreadPush();
                continue;
            }
        }
        if (SUCCEEDED(hr) && SUCCEEDED(m_hrError))
        {
            // awaiting for the async operation completion if needed
            if (m_pDispBitstream->bSyncPointUsed) sts = m_pDispBitstream->iSyncPointSts;
            else
            {
                m_pDispBitstream->bSyncPointUsed = true;
                pSyncPoint = m_pDispBitstream->pSyncPoint;
                lock.Unlock();
                {
                    m_SyncOpSts = sts = m_pMfxVideoSession->SyncOperation(*pSyncPoint, INFINITE);
                    // sending event that some resources may be free
                    m_pDevBusyEvent->Signal();
                }
                lock.Lock();
                m_pDispBitstream->iSyncPointSts = sts;
            }
            // sending request for ProcessOutput call
            if (MFX_ERR_NONE == sts) hr = SendOutput();
            else hr = E_FAIL;
        }
        if (SUCCEEDED(hr) && SUCCEEDED(m_hrError))
        {
            // awaiting for ProcessOuput completion
            lock.Unlock();
            {
                m_pAsyncThreadEvent->Wait();
                m_pAsyncThreadEvent->Reset();
            }
            lock.Lock();
        }
        // handling errors
        if (FAILED(hr) || FAILED(m_hrError))
        { // only here we will call HandlePlgError on errors - we need to send DrainComplete event
            SetPlgError(hr, sts);
            HandlePlgError(lock, true);
        }
    };
    return S_OK;
}

/*------------------------------------------------------------------------------*/

// Function returns 'true' if SyncOperation was called and 'false' overwise
bool MFPluginVEnc::HandleDevBusy(mfxStatus& sts)
{
    bool ret_val = false;
    MFEncOutData* pOutBitstream = m_pDispBitstream;

    sts = MFX_ERR_NONE;
    do
    {
        if (!(pOutBitstream->bSyncPointUsed) && !(pOutBitstream->bFreeData))
        {
            pOutBitstream->bSyncPointUsed = true;
            sts = pOutBitstream->iSyncPointSts  = m_pMfxVideoSession->SyncOperation(*(pOutBitstream->pSyncPoint), INFINITE);
            ret_val = true;
            break;
        }
        ++pOutBitstream;
        if ((mfxU32)(pOutBitstream-m_pOutBitstreams) >= m_nOutBitstreamsNum) pOutBitstream = m_pOutBitstreams;
    } while(pOutBitstream != m_pDispBitstream);
    return ret_val;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::ProcessFrameEnc(mfxStatus& sts)
{
    HRESULT hr = S_OK;
    mfxFrameSurface1* pOutFrameSurface = NULL;

    sts = MFX_ERR_NONE;
    pOutFrameSurface = (m_bStartDrainEnc)? NULL : m_pOutSurface->GetSurface();

    if (SUCCEEDED(hr))
    {
        mfxBitstream* pBitstream = m_pOutBitstream->pMFBitstream->GetMfxBitstream();
        mfxSyncPoint* pSyncPoint = m_pOutBitstream->pSyncPoint;

        if (m_pOutSurface && m_pmfxVPP) m_pOutSurface->DropDataToFile();
        if (pOutFrameSurface) pOutFrameSurface->Data.FrameOrder = m_iNumberInputSamples;

        do
        {
            sts = m_pmfxENC->EncodeFrameAsync(NULL, pOutFrameSurface, pBitstream, pSyncPoint);

            if ((MFX_WRN_DEVICE_BUSY == sts) && (MFX_ERR_NONE == m_SyncOpSts))
            {
                mfxStatus res = MFX_ERR_NONE;

                if (!HandleDevBusy(res))
                {
                    m_pDevBusyEvent->TimedWait(MF_DEV_BUSY_SLEEP_TIME);
                    m_pDevBusyEvent->Reset();
                }
                if (MFX_ERR_NONE != res) sts = res;
            }
        } while ((MFX_WRN_DEVICE_BUSY == sts) && (MFX_ERR_NONE == m_SyncOpSts));
        // valid cases are:
        // MFX_ERR_NONE - data processed, output will be generated
        // MFX_ERR_MORE_DATA - data buffered, output will not be generated
        // MFX_WRN_INCOMPATIBLE_VIDEO_PARAM - frame info is not synchronized with initialization one

        if (MFX_WRN_INCOMPATIBLE_VIDEO_PARAM == sts) sts = MFX_ERR_NONE; // ignoring this warning

        if ((MFX_ERR_MORE_DATA == sts) && m_bStartDrainEnc) hr = MF_E_TRANSFORM_NEED_MORE_INPUT;
        else if ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_DATA == sts))
        {
            bool bOutExists = (MFX_ERR_NONE == sts);

            ++m_iNumberInputSamples;
            if (bOutExists)
            {
                m_pOutBitstream->bFreeData = false;
                m_pOutBitstream->pMFBitstream->IsGapBitstream(m_bSetDiscontinuityAttribute);
                if (m_bSetDiscontinuityAttribute) m_bSetDiscontinuityAttribute = false;
                sts = SetFreeBitstream();
                {
                    // sending output in sync or async way
                    ++m_uiHasOutputEventExists;
                    AsyncThreadPush();
                }
                if (SUCCEEDED(hr) && (MFX_ERR_NONE != sts)) hr = E_FAIL;
            }
            if (!m_pmfxVPP || m_bStartDrainEnc)
            {
                // trying to set free surface
                m_pOutSurface = SetFreeSurface(m_pOutSurfaces, m_nOutSurfacesNum);
                if (!m_pOutSurface) m_bNeedOutSurface = true;
            }
        }
        else hr = E_FAIL;
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::ProcessFrameVpp(mfxStatus& sts)
{
    HRESULT hr = S_OK;
    mfxFrameSurface1* pInFrameSurface  = NULL;
    mfxFrameSurface1* pOutFrameSurface = NULL;

    sts = MFX_ERR_MORE_SURFACE;
    pInFrameSurface  = (m_bStartDrain)? NULL : m_pInSurface->GetSurface();

    while (SUCCEEDED(hr) && (MFX_ERR_MORE_SURFACE == sts) && !m_bNeedOutSurface)
    {
        m_pOutSurface->PseudoLoad();
        pOutFrameSurface = m_pOutSurface->GetSurface();
        do
        {
            m_LastSts = sts = m_pmfxVPP->RunFrameVPPAsync(pInFrameSurface,
                                                          pOutFrameSurface,
                                                          NULL,
                                                          m_pSyncPoint);
            if ((MFX_WRN_DEVICE_BUSY == sts) && (MFX_ERR_NONE == m_SyncOpSts))
            {
                mfxStatus res = MFX_ERR_NONE;

                if (!HandleDevBusy(res))
                {
                    m_pDevBusyEvent->TimedWait(MF_DEV_BUSY_SLEEP_TIME);
                    m_pDevBusyEvent->Reset();
                }
                if (MFX_ERR_NONE != res) sts = res;
            }
        } while ((MFX_WRN_DEVICE_BUSY == sts) && (MFX_ERR_NONE == m_SyncOpSts));
        // valid cases for the status are:
        // MFX_WRN_DEVICE_BUSY - no buffers in HW device, need to wait; treating as MFX_ERR_MORE_DATA
        // MFX_ERR_NONE - data processed, output will be generated
        // MFX_ERR_MORE_DATA - data buffered, output will not be generated

        if ((MFX_ERR_MORE_DATA == sts) && m_bStartDrain)
        {
            m_bStartDrainEnc = true;
            hr = ProcessFrameEnc(sts);
            break;
        }
        else if ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts))
        {
            HRESULT hr_sts = S_OK;
            bool bOutExists = ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_SURFACE == sts));

            if (bOutExists)
            {
#ifdef OMIT_VPP_SYNC_POINT
                ++(m_MfxCodecInfo.m_nVppOutFrames);
#else
                sts = m_pMfxSession->SyncOperation(*m_pSyncPoint, INFINITE);
                if (MFX_ERR_NONE == sts) ++(m_MfxCodecInfo.m_nVppOutFrames);
                else hr_sts = E_FAIL;
#endif
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                if (SUCCEEDED(hr))
                {
                    mfxStatus res = MFX_ERR_NONE;

                    hr_sts = ProcessFrameEnc(res);
                    if (FAILED(hr_sts)) sts = res;
                }
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
            }
            if (pOutFrameSurface->Data.Locked || bOutExists)
            {
                // setting free out surface
                m_pOutSurface = SetFreeSurface(m_pOutSurfaces, m_nOutSurfacesNum);
                if (m_bStartDrain)
                {
                    if (SUCCEEDED(hr) && !m_pOutSurface) hr = E_FAIL;
                }
                else
                { // will wait for free surface
                    if (!m_pOutSurface) m_bNeedOutSurface = true;
                }
            }
        }
        else hr = E_FAIL;
        // no iterations on EOS
        if (m_bEndOfInput) break;
    }
    if (!m_bStartDrain)
    {
        if (MFX_ERR_MORE_SURFACE == m_LastSts) m_bNeedInSurface = true;
        else
        {
            // trying to set free input surface
            m_pInSurface = SetFreeSurface(m_pInSurfaces, m_nInSurfacesNum);
            if (SUCCEEDED(hr) && !m_pInSurface) m_bNeedInSurface = true;
        }
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::ProcessFrame(mfxStatus& sts)
{
    HRESULT hr = S_OK;

    if (m_bInitialized)
    {
        if (m_pmfxVPP && !m_bStartDrainEnc)
        {
            hr = ProcessFrameVpp(sts);
        }
        else
        {
            hr = ProcessFrameEnc(sts);
        }
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::ProcessInput(DWORD      dwInputStreamID,
                                   IMFSample* pSample,
                                   DWORD      dwFlags)
{
    MFTicker ticker(&m_ticks_ProcessInput);
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;

    GetPerformance();
    hr = MFPlugin::ProcessInput(dwInputStreamID, pSample, dwFlags);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);

    SET_HR_ERROR(0 == dwFlags, hr, E_INVALIDARG);
    SET_HR_ERROR(!m_bEndOfInput, hr, E_FAIL);
    SET_HR_ERROR(stHeaderSetAwaiting != m_State, hr, MF_E_TRANSFORM_TYPE_NOT_SET);

    if (SUCCEEDED(hr)) hr = m_hrError;
    // Encoder initialization
    if (SUCCEEDED(hr) && !m_bInitialized)
    {
        sts = InitCodec(lock);
        if (MFX_ERR_NONE != sts) hr = E_FAIL;
    }
    if (SUCCEEDED(hr) && !m_pmfxVPP) m_pInSurface = m_pOutSurface;
    if (SUCCEEDED(hr)) hr = m_pInSurface->Load(pSample, m_bDirectConnectionMFX);
    if (SUCCEEDED(hr) && !m_pInSurface->IsFakeSrf()) ++(m_MfxCodecInfo.m_nInFrames);
    if (SUCCEEDED(hr))
    {
        if (m_pInSurface->IsFakeSrf())
        {
            // saving this sample
            pSample->AddRef();
            m_pFakeSample = pSample;

            // setting flags to process this situation
            m_bReinit     = true;
            m_bEndOfInput = true; // we need to obtain buffered frames
            // pushing async thread (starting call EncodeFrameAsync with NULL bitstream)
            AsyncThreadPush();
        }
        else hr = ProcessFrame(sts);
    }
    if (SUCCEEDED(hr) && !m_bReinit)
    {
        CheckBitstreamsNumLimit();
        // requesting input while free surfaces (in & out) are available
        if (!m_bDoNotRequestInput && !m_NeedInputEventInfo.m_requested &&
            !m_bNeedInSurface && !m_bNeedOutSurface && !m_bBitstreamsLimit)
        {
            hr = RequestInput();
        }
    }
    if (FAILED(hr) && SUCCEEDED(m_hrError))
    {
        // setting plug-in error
        SetPlgError(hr, sts);
        // handling error
        HandlePlgError(lock);
    }
    // errors will be handled either in this function or in downstream plug-in
    if (!ReturnPlgError()) hr = S_OK;
    DBG_TRACE_1("m_iNumberInputSamplesBeforeVPP = %d", m_iNumberInputSamplesBeforeVPP);
    DBG_TRACE_1("m_iNumberInputSamples          = %d", m_iNumberInputSamples);
    DBG_TRACE_2("sts = %d, hr = %x: -", sts, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVEnc::ProcessOutput(DWORD  dwFlags,
                                    DWORD  cOutputBufferCount,
                                    MFT_OUTPUT_DATA_BUFFER* pOutputSamples,
                                    DWORD* pdwStatus)
{
    MFTicker ticker(&m_ticks_ProcessOutput);
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;
    bool bDisplayFrame = false;

    GetPerformance();
    hr = MFPlugin::ProcessOutput(dwFlags, cOutputBufferCount, pOutputSamples, pdwStatus);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);

    SET_HR_ERROR(0 == dwFlags, hr, E_INVALIDARG);
    SET_HR_ERROR(stHeaderSetAwaiting != m_State, hr, MF_E_TRANSFORM_TYPE_NOT_SET);
    SET_HR_ERROR(m_pDispBitstream && ((m_pDispBitstream != m_pOutBitstream) || m_bEndOfInput),
                 hr, E_FAIL);

    if (SUCCEEDED(hr)) hr = m_hrError;
    if (stHeaderNotSet == m_State)
    {
        DBG_TRACE("Starting header set process");
        if (SUCCEEDED(hr)) hr = GetHeader(m_pDispBitstream->pMFBitstream->GetMfxBitstream());
        if (SUCCEEDED(hr))
        {
            pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_FORMAT_CHANGE;
            *pdwStatus = 0;
            hr = MF_E_TRANSFORM_STREAM_CHANGE;
            m_State = stHeaderSetAwaiting;
        }
    }
    else
    {
        DBG_TRACE("Normal encoding");
        if (m_uiHasOutputEventExists) --m_uiHasOutputEventExists;
        m_State = stReady;
        if (SUCCEEDED(hr) && (m_pDispBitstream != m_pOutBitstream)) bDisplayFrame = true;
        if (SUCCEEDED(hr) && bDisplayFrame) hr = m_pDispBitstream->pMFBitstream->Sync();
        // Set status flags
        if (SUCCEEDED(hr))
        {
            pOutputSamples[0].pSample = m_pDispBitstream->pMFBitstream->GetSample();
            if (pOutputSamples[0].pSample)
            {
                REFERENCE_TIME duration = SEC2REF_TIME((m_Framerate)? 1.0/m_Framerate: 0.04);
                pOutputSamples[0].pSample->SetSampleDuration(duration);
            }
            if (m_HasOutputEventInfo.m_requested)
                pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_INCOMPLETE;
            *pdwStatus = 0;

            ++(m_MfxCodecInfo.m_nOutFrames);
        }
        if (SUCCEEDED(hr) && bDisplayFrame) ++m_iNumberOutputSamples;
        if (bDisplayFrame)
        {
            MFEncOutData* pDispBitstream = NULL;

            // releasing bitstream sample and alocating new one
            m_pDispBitstream->pMFBitstream->Release();
            m_pDispBitstream->bSyncPointUsed = false;
            m_pDispBitstream->bFreeData = true;
            // moving to next undisplayed surface
            pDispBitstream = m_pDispBitstream;
            ++pDispBitstream;
            if ((mfxU32)(pDispBitstream - m_pOutBitstreams) >= m_nOutBitstreamsNum)
                pDispBitstream = m_pOutBitstreams;
            m_pDispBitstream = pDispBitstream;

            if (m_bBitstreamsLimit) CheckBitstreamsNumLimit();
            // trying to release resources
            if (m_pInSurfaces)  SetFreeSurface(m_pInSurfaces, m_nInSurfacesNum);
            if (m_pOutSurfaces) SetFreeSurface(m_pOutSurfaces, m_nOutSurfacesNum);

            m_pAsyncThreadEvent->Signal();
        }
        if (m_bNeedOutSurface)
        { // processing remained output and setting free out surface
            HRESULT hr_sts = S_OK;
            // setting free out surface
            m_pOutSurface = SetFreeSurface(m_pOutSurfaces, m_nOutSurfacesNum);
            if (m_pOutSurface)
            {
                m_bNeedOutSurface = false;
                if (MFX_ERR_MORE_SURFACE == m_LastSts)
                {
                    hr_sts = ProcessFrame(sts);
                    if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                }
            }
        }
        if (m_bNeedInSurface && (MFX_ERR_MORE_SURFACE != m_LastSts))
        { // setting free in surface
            m_pInSurface = SetFreeSurface(m_pInSurfaces, m_nInSurfacesNum);
            if (m_pInSurface) m_bNeedInSurface = false;
        }
        if (!m_bBitstreamsLimit && !m_NeedInputEventInfo.m_requested && !m_bEndOfInput && !m_bDoNotRequestInput)
        { // trying to request more input only if there are no pending requests
            if (!m_bNeedInSurface && !m_bNeedOutSurface)
            { // we have free input & output surfaces
                RequestInput();
            }
            else if (SUCCEEDED(hr) && (m_pDispBitstream == m_pOutBitstream))
            { // we have: 1) no free surfaces, 2) no buffered output
                hr = E_FAIL;
            }
        }
    }
    if (MF_E_TRANSFORM_STREAM_CHANGE != hr)
    {
        if (pOutputSamples && (FAILED(hr) || !(pOutputSamples[0].pSample) && !(pOutputSamples[0].dwStatus)))
        { // we should set MFT_OUTPUT_DATA_BUFFER_NO_SAMPLE status if we do not generate output
            SAFE_RELEASE(pOutputSamples[0].pSample);
            pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_NO_SAMPLE;
        }
        if (FAILED(hr) && SUCCEEDED(m_hrError))
        {
            // setting plug-in error
            SetPlgError(hr, sts);
            // handling error
            HandlePlgError(lock);
        }
        // errors will be handled either in ProcessInput or in upstream plug-in
        hr = S_OK;
    }
    DBG_TRACE_1("m_iNumberOutputSamples = %d", m_iNumberOutputSamples);
    DBG_TRACE_2("sts = %d, hr = %x: -", sts, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/
// Global functions

#undef  PTR_THIS // no 'this' pointer in global functions
#define PTR_THIS NULL

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME pRegData->pPluginName

// CreateInstance
// Static method to create an instance of the source.
//
// iid:         IID of the requested interface on the source.
// ppSource:    Receives a ref-counted pointer to the source.
HRESULT MFPluginVEnc::CreateInstance(REFIID iid,
                                     void** ppMFT,
                                     ClassRegData* pRegData)

{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    MFPluginVEnc *pMFT = NULL;
    mf_tick tick = mf_get_tick();

    CHECK_POINTER(ppMFT, E_POINTER);
#if 0 // checking of supported platform
    if (!CheckPlatform())
    {
        *ppMFT = NULL;
        return E_FAIL;
    }
#endif

    SAFE_NEW(pMFT, MFPluginVEnc(hr, pRegData));
    CHECK_POINTER(pMFT, E_OUTOFMEMORY);

    pMFT->SetStartTick(tick);
    if (SUCCEEDED(hr))
    {
        hr = pMFT->QueryInterface(iid, ppMFT);
    }
    if (FAILED(hr))
    {
        SAFE_DELETE(pMFT);
    }
    DBG_TRACE_1("hr = %d: -", hr);
    return hr;
}
