/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_plugin.cpp

Purpose: define base class for Intel MSDK based MediaFoundation MFTs.

*********************************************************************************/

#include "mf_plugin.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME m_Reg.pPluginName

MFPlugin::MFPlugin(HRESULT &hr, ClassRegData *pRegData):
    m_bUnlocked(false),
    m_bIsShutdown(false),
    m_bStreamingStarted(false),
    m_bDoNotRequestInput(false),
    m_pPropertyStore(NULL),
    m_pAttributes(NULL),
    m_pEventQueue(NULL),
    m_hrError(S_OK),
    m_uiErrorResetCount(0),
    m_bErrorHandlingStarted(false),
    m_bErrorHandlingFinished(false),
    m_pInputType(NULL),
    m_pOutputType(NULL),
    m_MajorType(GUID_NULL),
    m_pAsyncThread(NULL),
    m_pAsyncThreadSemaphore(NULL),
    m_bAsyncThreadStop(false),
    m_pAsyncThreadEvent(NULL),
    m_pDevBusyEvent(NULL),
    m_pErrorFoundEvent(NULL),
    m_pErrorHandledEvent(NULL),
    m_dbg_MSDK(dbgDefMsdk),
    m_dbg_Memory(dbgDefMemory),
    m_dbg_return_errors(false),
#ifdef MF_NO_SW_FALLBACK
    m_dbg_no_SW_fallback(true)
#else
    m_dbg_no_SW_fallback(false)
#endif
{
    hr = E_FAIL;
    memset(&m_NeedInputEventInfo, 0, sizeof(MFEventsInfo));
    memset(&m_HasOutputEventInfo, 0, sizeof(MFEventsInfo));
    memset(&m_Reg, 0, sizeof(ClassRegData));
    if (pRegData && pRegData->pPluginName) m_Reg = *pRegData;
    else return;
    DBG_TRACE("+");

    hr = MFCreateEventQueue(&m_pEventQueue);
    if (FAILED(hr)) return;

    hr = PSCreateMemoryPropertyStore(IID_PPV_ARGS(&m_pPropertyStore));
    if (SUCCEEDED(hr))
    {
        PROPVARIANT var;

        // preparing Property Store which states that MFT will handle out sample attributes
        memset(&var, 0, sizeof(PROPVARIANT));
        var.vt      = VT_BOOL;
        var.boolVal = VARIANT_TRUE;
        hr = m_pPropertyStore->SetValue(MFPKEY_EXATTRIBUTE_SUPPORTED, var);
    }
    if (FAILED(hr)) return;

    SAFE_NEW(m_pAsyncThreadSemaphore, MySemaphore(hr));
    if (!m_pAsyncThreadSemaphore) hr = E_OUTOFMEMORY;
    if (FAILED(hr)) return;

    SAFE_NEW(m_pAsyncThreadEvent, MyEvent(hr));
    if (!m_pAsyncThreadEvent) hr = E_OUTOFMEMORY;
    if (FAILED(hr)) return;

    SAFE_NEW(m_pErrorFoundEvent, MyEvent(hr));
    if (!m_pErrorFoundEvent) hr = E_OUTOFMEMORY;
    if (FAILED(hr)) return;
    
    SAFE_NEW(m_pErrorHandledEvent, MyEvent(hr));
    if (!m_pErrorHandledEvent) hr = E_OUTOFMEMORY;
    if (FAILED(hr)) return;

    SAFE_NEW(m_pDevBusyEvent, MyEvent(hr));
    if (!m_pDevBusyEvent) hr = E_OUTOFMEMORY;
    if (FAILED(hr)) return;

/*    SAFE_NEW(m_pAsyncThread, MyThread(hr, thAsyncThreadFunc, this));
    if (!m_pAsyncThread) hr = E_OUTOFMEMORY;
    if (FAILED(hr)) return;*/

    // Creating attributs store initially for 3 items:
    // * MF_TRANSFORM_ASYNC
    // * MFT_SUPPORT_DYNAMIC_FORMAT_CHANGE
    // * MF_TRANSFORM_ASYNC_UNLOCK
    hr = MFCreateAttributes(&m_pAttributes, 3);
    if (FAILED(hr)) return;

    {
        DWORD align = 1;
        if ((S_OK == mf_get_reg_dword(_T(REG_PARAMS_PATH),
                                      _T(REG_ALIGN_DEC), &align)) && align)
        {
            m_gMemAlignDec = (mfxU16)align;
        }
        if ((S_OK == mf_get_reg_dword(_T(REG_PARAMS_PATH),
                                      _T(REG_ALIGN_VPP), &align)) && align)
        {
            m_gMemAlignVpp = (mfxU16)align;
        }
        if ((S_OK == mf_get_reg_dword(_T(REG_PARAMS_PATH),
                                      _T(REG_ALIGN_ENC), &align)) && align)
        {
            m_gMemAlignEnc = (mfxU16)align;
        }
        DBG_TRACE_1("m_gMemAlignDec = %d", m_gMemAlignDec);
        DBG_TRACE_1("m_gMemAlignVpp = %d", m_gMemAlignVpp);
        DBG_TRACE_1("m_gMemAlignEnc = %d", m_gMemAlignEnc);
    }

    DllAddRef();

    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFPlugin::~MFPlugin(void)
{
    DBG_TRACE("+");

    SAFE_DELETE(m_pAsyncThread);
    SAFE_DELETE(m_pAsyncThreadSemaphore);
    SAFE_DELETE(m_pAsyncThreadEvent);
    SAFE_DELETE(m_pDevBusyEvent);
    SAFE_DELETE(m_pErrorFoundEvent);
    SAFE_DELETE(m_pErrorHandledEvent);

    SAFE_RELEASE(m_pPropertyStore);
    SAFE_RELEASE(m_pAttributes);
    SAFE_RELEASE(m_pEventQueue);
    SAFE_RELEASE(m_pInputType);
    SAFE_RELEASE(m_pOutputType);

    DllRelease();

    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

void MFPlugin::SetPlgError(HRESULT hr, mfxStatus /*sts*/)
{
    if (SUCCEEDED(m_hrError)) m_hrError = hr;
}

/*------------------------------------------------------------------------------*/

void MFPlugin::ResetPlgError(void)
{
    ++m_uiErrorResetCount;
    m_hrError = S_OK;
}

/*------------------------------------------------------------------------------*/

inline bool MFPlugin::IsMftUnlocked(void)
{
    DBG_TRACE("+");
    if (!m_bUnlocked)
    { // checking unlock status if MFT is still locked
        UINT32 unlocked = false;
        unlocked = MFGetAttributeUINT32(m_pAttributes,
                                        MF_TRANSFORM_ASYNC_UNLOCK, FALSE);
        m_bUnlocked = (unlocked)? TRUE: FALSE;
    }
    DBG_TRACE_1("m_bUnlocked = %d: -", m_bUnlocked);
    return m_bUnlocked;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::CheckMediaType(IMFMediaType*     pType,
                                 const GUID_info*  pArray,
                                 DWORD             arraySize,
                                 const GUID_info** pEntry)
{
    HRESULT hr = S_OK;
    GUID major_type = GUID_NULL;
    GUID subtype = GUID_NULL;
    DWORD i;

    // get major type
    hr = pType->GetGUID(MF_MT_MAJOR_TYPE, &major_type);
    // get subtype
    if (SUCCEEDED(hr)) hr = pType->GetGUID(MF_MT_SUBTYPE, &subtype);
    if (SUCCEEDED(hr))
    {
        // check major type and subtype to be in our list of accepted types
        hr = MF_E_INVALIDTYPE;
        for (i = 0; i < arraySize; ++i)
        {
            if ((pArray[i].major_type == major_type) && (pArray[i].subtype == subtype))
            {
                hr = S_OK;
                if (pEntry) *pEntry = &pArray[i];
                break;
            }
        }
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

void MFPlugin::ReleaseMediaType(IMFMediaType*& pType)
{
    if (pType)
    {
        HRESULT hr = pType->DeleteItem(MF_MT_D3D_DEVICE);
        hr = hr;
    }
    SAFE_RELEASE(pType);
}

/*------------------------------------------------------------------------------*/

bool MFPlugin::IsVppNeeded(IMFMediaType* pType1, IMFMediaType* pType2)
{
    UINT32 par1 = 0, par2 = 0;
    UINT32 cpar1 = 0, cpar2 = 0;

    CHECK_POINTER(pType1, false);
    CHECK_POINTER(pType2, false);
    CHECK_EXPRESSION(pType1 != pType2, false);

    if (SUCCEEDED(MFGetAttributeSize(pType2, MF_MT_FRAME_SIZE, &cpar1, &cpar2)) &&
        SUCCEEDED(MFGetAttributeSize(pType1, MF_MT_FRAME_SIZE, &par1, &par2)))
    {
        CHECK_EXPRESSION((par1 == cpar1) && (par2 == cpar2), true);
    }
    if (SUCCEEDED(MFGetAttributeRatio(pType2, MF_MT_PIXEL_ASPECT_RATIO, &cpar1, &cpar2)) &&
        SUCCEEDED(MFGetAttributeRatio(pType1, MF_MT_PIXEL_ASPECT_RATIO, &par1, &par2)))
    {
        CHECK_EXPRESSION((par1 == cpar1) && (par2 == cpar2), true);
    }
    if (SUCCEEDED(MFGetAttributeRatio(pType2, MF_MT_FRAME_RATE, &cpar1, &cpar2)) &&
        SUCCEEDED(MFGetAttributeRatio(pType1, MF_MT_FRAME_RATE, &par1, &par2)))
    {
        CHECK_EXPRESSION((par1 == cpar1) && (par2 == cpar2), true);
    }
    if (SUCCEEDED(pType2->GetUINT32(MF_MT_INTERLACE_MODE, &cpar1)) &&
        SUCCEEDED(pType1->GetUINT32(MF_MT_INTERLACE_MODE, &par1)))
    {
        CHECK_EXPRESSION((par1 == cpar1), true);
    }
    return false;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::RequestInput(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    IMFMediaEvent* pEvent = NULL;

    hr = MFCreateMediaEvent(METransformNeedInput, GUID_NULL,
                            S_OK, NULL, &pEvent);
    if (SUCCEEDED(hr)) hr = pEvent->SetUINT32(MF_EVENT_MFT_INPUT_STREAM_ID, 0);
    if (SUCCEEDED(hr)) hr = m_pEventQueue->QueueEvent(pEvent);
    if (SUCCEEDED(hr)) ++(m_NeedInputEventInfo.m_requested);
    SAFE_RELEASE(pEvent);
    DBG_TRACE_1("m_NeedInputEventInfo.m_requested = %d", m_NeedInputEventInfo.m_requested);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::SendOutput(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    IMFMediaEvent* pEvent = NULL;

    hr = MFCreateMediaEvent(METransformHaveOutput, GUID_NULL,
                            S_OK, NULL, &pEvent);
    if (SUCCEEDED(hr)) hr = m_pEventQueue->QueueEvent(pEvent);
    if (SUCCEEDED(hr)) ++(m_HasOutputEventInfo.m_requested);
    SAFE_RELEASE(pEvent);
    DBG_TRACE_1("m_HasOutputEventInfo.m_requested = %d", m_HasOutputEventInfo.m_requested);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::DrainComplete(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    IMFMediaEvent* pEvent = NULL;

    hr = MFCreateMediaEvent(METransformDrainComplete, GUID_NULL,
                            S_OK, NULL, &pEvent);
    if (SUCCEEDED(hr)) hr = pEvent->SetUINT32(MF_EVENT_MFT_INPUT_STREAM_ID, 0);
    if (SUCCEEDED(hr)) hr = m_pEventQueue->QueueEvent(pEvent);
    SAFE_RELEASE(pEvent);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::SendMarker(UINT64 ulParam)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    IMFMediaEvent* pEvent = NULL;

    hr = MFCreateMediaEvent(METransformMarker, GUID_NULL,
                            S_OK, NULL, &pEvent);
    if (SUCCEEDED(hr)) hr = pEvent->SetUINT64(MF_EVENT_MFT_CONTEXT, ulParam);
    if (SUCCEEDED(hr)) hr = m_pEventQueue->QueueEvent(pEvent);
    SAFE_RELEASE(pEvent);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

unsigned int MY_THREAD_CALLCONVENTION thAsyncThreadFunc(void* arg)
{
    MFPlugin* plg = (MFPlugin*)arg;

    if (plg && SUCCEEDED(plg->AsyncThreadFunc())) return 0;
    return 1;
}

/*------------------------------------------------------------------------------*/

void MFPlugin::AsyncThreadPush(void)
{
    m_pAsyncThreadSemaphore->Post();
}

/*------------------------------------------------------------------------------*/

void MFPlugin::AsyncThreadWait(void)
{
    if (m_pAsyncThread && m_pAsyncThreadSemaphore)
    {
        m_bAsyncThreadStop = true;
        m_pAsyncThreadSemaphore->Post();
        m_pAsyncThreadEvent->Signal();
        m_pAsyncThread->Wait();
        m_bAsyncThreadStop = false;
    }
}

/*------------------------------------------------------------------------------*/
// IMFTransform

HRESULT MFPlugin::GetAttributes(IMFAttributes** pAttributes)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    // checking errors
    CHECK_POINTER(pAttributes, E_POINTER);
    // setting attributes required for ASync plug-ins
    if (SUCCEEDED(hr)) hr = m_pAttributes->SetUINT32(MF_TRANSFORM_ASYNC, TRUE);
    if (SUCCEEDED(hr))
        hr = m_pAttributes->SetUINT32(MFT_SUPPORT_DYNAMIC_FORMAT_CHANGE, TRUE);

    if (SUCCEEDED(hr))
    {
        m_pAttributes->AddRef();
        *pAttributes = m_pAttributes;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetStreamLimits(DWORD *pdwInputMinimum,
                                  DWORD *pdwInputMaximum,
                                  DWORD *pdwOutputMinimum,
                                  DWORD *pdwOutputMaximum)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    // Fixed stream limits.
    if (pdwInputMinimum) *pdwInputMinimum = 1;
    if (pdwInputMaximum) *pdwInputMaximum = 1;
    if (pdwOutputMinimum) *pdwOutputMinimum = 1;
    if (pdwOutputMaximum) *pdwOutputMaximum = 1;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetStreamCount(DWORD *pcInputStreams,
                                 DWORD *pcOutputStreams)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    //no locking check is required in this function - exception pointed in MSDN

    // Fixed stream count.
    if (pcInputStreams) *pcInputStreams = 1;
    if (pcOutputStreams) *pcOutputStreams = 1;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetStreamIDs(DWORD  /*dwInputIDArraySize*/,
                               DWORD* /*pdwInputIDs*/,
                               DWORD  /*dwOutputIDArraySize*/,
                               DWORD* /*pdwOutputIDs*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    // Do not need to implement, because MFT has a fixed number of
    // streams and the stream IDs match the stream indexes.
    DBG_TRACE("E_NOTIMPL: -");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetInputStreamInfo(DWORD dwInputStreamID,
                                     MFT_INPUT_STREAM_INFO* pStreamInfo)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");

    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(0 == dwInputStreamID, MF_E_INVALIDSTREAMNUMBER);

    memset(pStreamInfo, 0, sizeof(MFT_INPUT_STREAM_INFO));

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetOutputStreamInfo(DWORD dwOutputStreamID,
                                      MFT_OUTPUT_STREAM_INFO* pStreamInfo)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");

    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(0 == dwOutputStreamID, MF_E_INVALIDSTREAMNUMBER);

    memset(pStreamInfo, 0, sizeof(MFT_OUTPUT_STREAM_INFO));
    pStreamInfo->dwFlags = MFT_OUTPUT_STREAM_PROVIDES_SAMPLES;

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetInputStreamAttributes(DWORD           /*dwInputStreamID*/,
                                           IMFAttributes** ppAttributes)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);

    // Fixed stream count = 1
    hr = GetAttributes(ppAttributes);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetOutputStreamAttributes(DWORD           /*dwOutputStreamID*/,
                                            IMFAttributes** ppAttributes)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);

    // Fixed stream count = 1
    hr = GetAttributes(ppAttributes);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::DeleteInputStream(DWORD /*dwStreamID*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    DBG_TRACE("E_NOTIMPL: -");
    return E_NOTIMPL; // Fixed streams.
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::AddInputStreams(DWORD  /*cStreams*/,
                                  DWORD* /*adwStreamIDs*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    DBG_TRACE("E_NOTIMPL: -");
    return E_NOTIMPL; // Fixed streams.
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetInputAvailableType(DWORD dwInputStreamID,
                                        DWORD dwTypeIndex, // 0-based
                                        IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(0 == dwInputStreamID, MF_E_INVALIDSTREAMNUMBER);
    CHECK_EXPRESSION(dwTypeIndex < m_Reg.cInputTypes, MF_E_NO_MORE_TYPES);
    CHECK_POINTER(ppType, E_POINTER);

    hr = MFCreateMediaType(ppType);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetOutputAvailableType(DWORD dwOutputStreamID,
                                         DWORD dwTypeIndex, // 0-based
                                         IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(0 == dwOutputStreamID, MF_E_INVALIDSTREAMNUMBER);
    CHECK_EXPRESSION(dwTypeIndex < m_Reg.cOutputTypes, MF_E_NO_MORE_TYPES);
    CHECK_POINTER(ppType, E_POINTER);

    hr = MFCreateMediaType(ppType);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::SetInputType(DWORD         dwInputStreamID,
                               IMFMediaType* /*pType*/,
                               DWORD         /*dwFlags*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(0 == dwInputStreamID, MF_E_INVALIDSTREAMNUMBER);
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::SetOutputType(DWORD         dwOutputStreamID,
                                IMFMediaType* /*pType*/,
                                DWORD         /*dwFlags*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    if ((REG_AS_VIDEO_ENCODER != m_Reg.iPluginCategory) &&
        (REG_AS_AUDIO_ENCODER != m_Reg.iPluginCategory))
    {
        CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    }
    CHECK_EXPRESSION(0 == dwOutputStreamID, MF_E_INVALIDSTREAMNUMBER);
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetInputCurrentType(DWORD dwInputStreamID,
                                      IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(dwInputStreamID == 0, MF_E_INVALIDSTREAMNUMBER);
    CHECK_POINTER(ppType, E_POINTER);
    CHECK_POINTER(m_pInputType, MF_E_TRANSFORM_TYPE_NOT_SET);

    m_pInputType->AddRef();
    *ppType = m_pInputType;

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetOutputCurrentType(DWORD dwOutputStreamID,
                                       IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    if ((REG_AS_VIDEO_ENCODER != m_Reg.iPluginCategory) &&
        (REG_AS_AUDIO_ENCODER != m_Reg.iPluginCategory))
    {
        CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    }
    CHECK_EXPRESSION(dwOutputStreamID == 0, MF_E_INVALIDSTREAMNUMBER);
    CHECK_POINTER(ppType, E_POINTER);
    CHECK_POINTER(m_pOutputType, MF_E_TRANSFORM_TYPE_NOT_SET);

    m_pOutputType->AddRef();
    *ppType = m_pOutputType;

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetInputStatus(DWORD  dwInputStreamID,
                                 DWORD* pdwFlags)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_EXPRESSION(0 == dwInputStreamID, MF_E_INVALIDSTREAMNUMBER);
    CHECK_POINTER(pdwFlags, E_POINTER);

    *pdwFlags = MFT_INPUT_STATUS_ACCEPT_DATA;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetOutputStatus(DWORD *pdwFlags)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    CHECK_POINTER(pdwFlags, E_POINTER);
    *pdwFlags = MFT_OUTPUT_STATUS_SAMPLE_READY;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::SetOutputBounds(LONGLONG /*hnsLowerBound*/,
                                  LONGLONG /*hnsUpperBound*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    DBG_TRACE("E_NOTIMPL: -");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::ProcessEvent(DWORD /*dwInputStreamID*/,
                               IMFMediaEvent* /*pEvent*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    DBG_TRACE("E_NOTIMPL: -");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::ProcessMessage(MFT_MESSAGE_TYPE eMessage,
                                 ULONG_PTR        ulParam)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);
    switch (eMessage)
    {
    case MFT_MESSAGE_COMMAND_DRAIN:
        DBG_TRACE("MFT_MESSAGE_COMMAND_DRAIN");
        CHECK_EXPRESSION(0 == (DWORD)ulParam, MF_E_INVALIDSTREAMNUMBER);
        break;
    case MFT_MESSAGE_NOTIFY_END_OF_STREAM:
        DBG_TRACE("MFT_MESSAGE_NOTIFY_END_OF_STREAM");
        CHECK_EXPRESSION(0 == (DWORD)ulParam, MF_E_INVALIDSTREAMNUMBER);
        break;
    };
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::ProcessInput(DWORD      dwInputStreamID,
                               IMFSample* pSample,
                               DWORD      /*dwFlags*/)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr_sts = S_OK;

    // Checking request acceptance (MFT locking)
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);

    { // Checking request acceptance (event counts)
        // We can not accept samples which do not match pending events
        CHECK_EXPRESSION(m_NeedInputEventInfo.m_sent, MF_E_NOTACCEPTING);

        if (m_NeedInputEventInfo.m_sent) --(m_NeedInputEventInfo.m_sent);
        if (m_NeedInputEventInfo.m_requested) --(m_NeedInputEventInfo.m_requested);
    }

    // Checking errors
    if (FAILED(m_hrError)) hr_sts = RequestInput();

    // Client must set input and output types.
    SET_HR_ERROR(m_pInputType, hr_sts, MF_E_TRANSFORM_TYPE_NOT_SET);
    SET_HR_ERROR(m_pOutputType, hr_sts, MF_E_TRANSFORM_TYPE_NOT_SET);
    // Only 1 stream
    SET_HR_ERROR((0 == dwInputStreamID), hr_sts, MF_E_INVALIDSTREAMNUMBER);
    // Input sample should exist
    SET_HR_ERROR(pSample, hr_sts, E_POINTER);

    SetPlgError(hr_sts);

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::ProcessOutput(DWORD  /*dwFlags*/,
                                DWORD  cOutputBufferCount,
                                MFT_OUTPUT_DATA_BUFFER* pOutputSamples,
                                DWORD* pdwStatus)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr_sts = S_OK;

    // Checking request acceptance (MFT locking)
    CHECK_EXPRESSION(IsMftUnlocked(), MF_E_TRANSFORM_ASYNC_LOCKED);

    { // Checking request acceptance (event counts)
        if (!m_HasOutputEventInfo.m_sent)
        {
            if (pOutputSamples && (cOutputBufferCount > 0))
            {
                pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_NO_SAMPLE;
            }
            return MF_E_UNEXPECTED;
        }
        // We can not accept requests for samples which do not match pending events
        if (m_HasOutputEventInfo.m_sent) --(m_HasOutputEventInfo.m_sent);
        if (m_HasOutputEventInfo.m_requested) --(m_HasOutputEventInfo.m_requested);

    }

    // Checking errors
    // Client must set input and output types.
    SET_HR_ERROR(m_pInputType, hr_sts, MF_E_TRANSFORM_TYPE_NOT_SET);
    SET_HR_ERROR(m_pOutputType, hr_sts, MF_E_TRANSFORM_TYPE_NOT_SET);
    // Must be exactly one output buffer.
    SET_HR_ERROR(1 == cOutputBufferCount, hr_sts, E_INVALIDARG);
    SET_HR_ERROR(pOutputSamples, hr_sts, E_POINTER);
     // MFT provides samples
    SET_HR_ERROR(!(pOutputSamples[0].pSample), hr_sts, E_FAIL);
    SET_HR_ERROR(pdwStatus, hr_sts, E_POINTER);

    SetPlgError(hr_sts);

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/
// IMFMediaEventGenerator

HRESULT MFPlugin::IncrementEventsCount(IMFMediaEvent* pEvent)
{
    HRESULT        hr = S_OK;
    MediaEventType met;

    hr = pEvent->GetType(&met);

    if (SUCCEEDED(hr))
    {
        if (METransformNeedInput == met)
        {
            if (m_NeedInputEventInfo.m_requested) ++(m_NeedInputEventInfo.m_sent);
        }
        else if (METransformHaveOutput == met)
        {
            if (m_HasOutputEventInfo.m_requested) ++(m_HasOutputEventInfo.m_sent);
        }
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::BeginGetEvent(IMFAsyncCallback* pCallback,
                                IUnknown* pUnkState)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(!m_bIsShutdown, MF_E_SHUTDOWN);
    hr = m_pEventQueue->BeginGetEvent(pCallback, pUnkState);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::EndGetEvent(IMFAsyncResult* pResult,
                              IMFMediaEvent** ppEvent)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(!m_bIsShutdown, MF_E_SHUTDOWN);

    hr = m_pEventQueue->EndGetEvent(pResult, ppEvent);
    if (SUCCEEDED(hr)) hr = IncrementEventsCount(*ppEvent);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetEvent(DWORD dwFlags,
                           IMFMediaEvent** ppEvent)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(!m_bIsShutdown, MF_E_SHUTDOWN);

    hr = m_pEventQueue->GetEvent(dwFlags, ppEvent);
    if (SUCCEEDED(hr)) hr = IncrementEventsCount(*ppEvent);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::QueueEvent(MediaEventType met,
                             REFGUID guidExtendedType,
                             HRESULT hrStatus,
                             const PROPVARIANT* pvValue)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(!m_bIsShutdown, MF_E_SHUTDOWN);
    hr = m_pEventQueue->QueueEventParamVar(met, guidExtendedType,
                                           hrStatus, pvValue);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/
// IMFShutdown

HRESULT MFPlugin::Shutdown(void)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    if (!m_bIsShutdown)
    {
        hr = m_pEventQueue->Shutdown();
        m_bIsShutdown = true;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPlugin::GetShutdownStatus(MFSHUTDOWN_STATUS* pStatus)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_POINTER(pStatus, E_POINTER);
    if (m_bIsShutdown) *pStatus = MFSHUTDOWN_COMPLETED;
    else hr = MF_E_INVALIDREQUEST; // shutdown should be called firstly
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}
