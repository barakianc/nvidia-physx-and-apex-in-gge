/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_vdec_plg.cpp

Purpose: define common code for MSDK based decoder MFTs.

*********************************************************************************/

#include "mf_vdec_plg.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Decoder"

#define OMIT_DEC_ASYNC_SYNC_POINT

/*------------------------------------------------------------------------------*/

HRESULT CreateVDecPlugin(REFIID iid,
                         void** ppMFT,
                         ClassRegData* pRegistrationData)
{
    return MFPluginVDec::CreateInstance(iid, ppMFT, pRegistrationData);
}

/*------------------------------------------------------------------------------*/
// MFPluginVDec class

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME m_Reg.pPluginName

MFPluginVDec::MFPluginVDec(HRESULT &hr,
                           ClassRegData *pRegistrationData) :
    MFPlugin(hr, pRegistrationData),
    m_nRefCount(0),
    m_MSDK_impl(MF_MFX_IMPL),
    m_pMfxVideoSession(NULL),
    m_pmfxDEC(NULL),
    m_pInputInfo(NULL),
    m_pOutputInfo(NULL),
    m_pOutputTypeCandidate(NULL),
    m_iNumberLockedSurfaces(0),
    m_iNumberInputSurfaces(0),
    m_State(stDecoderNotCreated),
    m_bEndOfInput(false),
    m_bSendDrainComplete(false),
    m_bNeedWorkSurface(false),
    m_bStartDrain(false),
    m_bReinit(false),
    m_bReinitStarted(false),
    m_bSendFakeSrf(false),
    m_bSetDiscontinuityAttribute(false),
    m_uiSurfacesNum(MF_RENDERING_SURF_NUM),
    m_pWorkSurfaces(NULL),
    m_pWorkSurface(NULL),
    m_pOutSurfaces(NULL),
    m_pOutSurface(NULL),
    m_pDispSurface(NULL),
    m_pMFBitstream(NULL),
    m_pReinitMFBitstream(NULL),
    m_pBitstream(NULL),
    m_pFreeSamplesPool(NULL),
    m_pDeviceDXVA(NULL),
    m_pHWDevice(NULL),
    m_pD3DFrameAllocator(NULL),
    m_bChangeOutputType(false),
    m_bOutputTypeChangeRequired(false),
    m_uiHasOutputEventExists(0),
    m_pPostponedInput(NULL),
    m_LastPts(0),
    m_LastFramerate(0.0),
    m_SyncOpSts(MFX_ERR_NONE),
    m_dbg_decin(NULL),
    m_dbg_decin_fc(NULL),
    m_dbg_decout(NULL),
    m_bNotifyStartOfStream(false)
{
    m_D3DAllocatorParams.pManager = NULL;

    if (FAILED(hr)) return;
    SAFE_NEW(m_pWorkTicker, MFTicker(&m_ticks_WorkTime));
    SAFE_NEW(m_pCpuUsager, MFCpuUsager(&m_TimeTotal, &m_TimeKernel, &m_TimeUser, &m_CpuUsage));

    DBG_TRACE("+");
    if (!m_Reg.pFillParams) hr = E_POINTER;
    if (SUCCEEDED(hr))
    {
        // NOTE: decoders (curently VC1) may pretend to support extended
        // number of output types in registration; the following code
        // corrects this for the running plug-ins
        m_Reg.cOutputTypes = ARRAY_SIZE(g_UncompressedVideoTypes);
        m_Reg.pOutputTypes = (GUID_info*)g_UncompressedVideoTypes;
    }
    if (SUCCEEDED(hr))
    { // setting default decoder parameters
        mfxStatus sts = MFX_ERR_NONE;

        sts = m_Reg.pFillParams(&m_MfxParamsVideo);

        memcpy((void*)&m_VideoParams_input, &m_MfxParamsVideo, sizeof(mfxVideoParam));
        memset((void*)&m_FrameInfo_original, 0, sizeof(mfxFrameInfo));
        memset((void*)&m_DecAllocResponse, 0, sizeof(mfxFrameAllocResponse));

        if (MFX_ERR_NONE != sts) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        SAFE_NEW(m_pMFBitstream, MFDecBitstream(hr));
        if (m_pMFBitstream) m_pMFBitstream->SetFiles(m_dbg_decin, m_dbg_decin_fc);
        else hr = E_OUTOFMEMORY;
    }
    if (SUCCEEDED(hr))
    {
        hr = MFCreateMfxVideoSession(&m_pMfxVideoSession);
    }
    if (SUCCEEDED(hr))
    { // prepare MediaSDK
        mfxStatus sts = MFX_ERR_NONE;

        // Adjustment of MSDK implementation
        if (dbgHwMsdk == m_dbg_MSDK) m_MSDK_impl = MFX_IMPL_HARDWARE;
        else if (dbgSwMsdk == m_dbg_MSDK) m_MSDK_impl = MFX_IMPL_SOFTWARE;

        DBG_TRACE_1("m_MSDK_impl = %d: requested", m_MSDK_impl);
        sts = m_pMfxVideoSession->Init(m_MSDK_impl, &g_MfxVersion);

        if (MFX_ERR_NONE == sts)
        {
            sts = m_pMfxVideoSession->QueryIMPL(&m_MSDK_impl);
            m_MfxCodecInfo.m_MSDKImpl = m_MSDK_impl;
        }
        // Decoder creation
        if (MFX_ERR_NONE == sts)
        {
            MFXVideoSession* session = (MFXVideoSession*)m_pMfxVideoSession;

            SAFE_NEW(m_pmfxDEC, MFXVideoDECODE(*session));
            if (!m_pmfxDEC) sts = MFX_ERR_MEMORY_ALLOC;
        }
        if (MFX_ERR_NONE == sts) m_State = stHeaderNotDecoded;
        else hr = E_FAIL;
        DBG_TRACE_1("m_MSDK_impl = %d: obtained", m_MSDK_impl);
    }
    GetPerformance();
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFPluginVDec::~MFPluginVDec(void)
{
    DBG_TRACE("+");
    MyAutoLock lock(m_CritSec);

    DBG_TRACE_1("m_MfxCodecInfo.m_nInFrames = %d", m_MfxCodecInfo.m_nInFrames);
    DBG_TRACE_1("m_MfxCodecInfo.m_nOutFrames = %d", m_MfxCodecInfo.m_nOutFrames);
    DBG_TRACE_1("m_uiSurfacesNum         = %d", m_uiSurfacesNum);
    DBG_TRACE_1("m_iNumberLockedSurfaces = %d", m_iNumberLockedSurfaces);
    DBG_TRACE_1("m_iNumberInputSurfaces  = %d", m_iNumberInputSurfaces);
    DBG_TRACE_1("m_iNumberOutputSurfaces = %d", m_iNumberOutputSurfaces);

    lock.Unlock();
    AsyncThreadWait();
    lock.Lock();

    SAFE_DELETE(m_pWorkTicker);
    SAFE_DELETE(m_pCpuUsager);
    UpdateTimes();
    GetPerformance();
    // print work info
    PrintInfo();
    // destructing objects
    SAFE_RELEASE(m_pAttributes);
    SAFE_RELEASE(m_pEventQueue);
    SAFE_RELEASE(m_pInputType);
    // Delitioning of unneded properties:
    // MF_MT_D3D_DEVICE - it contains pointer to enc plug-in, need to delete
    // it or enc destructor will not be invoked
    ReleaseMediaType(m_pOutputType);
    ReleaseMediaType(m_pOutputTypeCandidate);
    // closing codec
    CloseCodec(lock);
    SAFE_DELETE(m_pmfxDEC);
    SAFE_DELETE(m_pMFBitstream);
    SAFE_DELETE(m_pReinitMFBitstream);
    // session deinitialization
    SAFE_RELEASE(m_pMfxVideoSession);

    if (m_Reg.pFreeParams) m_Reg.pFreeParams(&m_MfxParamsVideo);

    if (m_dbg_decin) fclose(m_dbg_decin);
    if (m_dbg_decin_fc) fclose(m_dbg_decin_fc);
    if (m_dbg_decout) fclose(m_dbg_decout);
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// IUnknown methods

ULONG MFPluginVDec::AddRef(void)
{
    DBG_TRACE_1("RefCount = %d", m_nRefCount+1);
    return InterlockedIncrement(&m_nRefCount);
}

ULONG MFPluginVDec::Release(void)
{
    DBG_TRACE("+");
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    DBG_TRACE_1("RefCount = %d: -", uCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT MFPluginVDec::QueryInterface(REFIID iid, void** ppv)
{
    DBG_TRACE("+");
    bool bAggregation = false;

    if (!ppv) return E_POINTER;
    if ((iid == IID_IUnknown) || (iid == IID_IMFTransform))
    {
        DBG_TRACE("IUnknown or IMFTransform");
        *ppv = static_cast<IMFTransform*>(this);
    }
    else if (iid == IID_IMFMediaEventGenerator)
    {
        DBG_TRACE("IMFMediaEventGenerator");
        *ppv = static_cast<IMFMediaEventGenerator*>(this);
    }
    else if (iid == IID_IMFShutdown)
    {
        DBG_TRACE("IMFShutdown");
        *ppv = static_cast<IMFShutdown*>(this);
    }
    else if (iid == IID_IConfigureMfxDecoder)
    {
        DBG_TRACE("IConfigureMfxDecoder");
        *ppv = static_cast<IConfigureMfxCodec*>(this);
    }
    else if ((iid == IID_IPropertyStore) && m_pPropertyStore)
    {
        DBG_TRACE("IPropertyStore");
        m_pPropertyStore->AddRef();
        *ppv = m_pPropertyStore;
        bAggregation = true;
    }
    else
    {
        DBG_TRACE_1("not found: %08x: -", iid.Data1);
        return E_NOINTERFACE;
    }
    if (!bAggregation) AddRef();

    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::GetAttributes(IMFAttributes** pAttributes)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = MFPlugin::GetAttributes(pAttributes);

    GetPerformance();
    // setting D3D aware attribute to notify renderer that plug-in supports HWA
    if ((dbgSwMsdk != m_dbg_MSDK) || (dbgHwMsdk == m_dbg_Memory))
    {
        if (SUCCEEDED(hr)) hr = (*pAttributes)->SetUINT32(MF_SA_D3D_AWARE, TRUE);
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::GetInputAvailableType(DWORD dwInputStreamID,
                                            DWORD dwTypeIndex, // 0-based
                                            IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT       hr    = S_OK;
    IMFMediaType* pType = NULL;

    GetPerformance();
    hr = MFPlugin::GetInputAvailableType(dwInputStreamID, dwTypeIndex, &pType);
    if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_MAJOR_TYPE, m_Reg.pInputTypes[dwTypeIndex].major_type);
    if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_SUBTYPE, m_Reg.pInputTypes[dwTypeIndex].subtype);
    if (SUCCEEDED(hr))
    {
        pType->AddRef();
        *ppType = pType;
    }
    SAFE_RELEASE(pType);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::GetOutputAvailableType(DWORD dwOutputStreamID,
                                             DWORD dwTypeIndex, // 0-based
                                             IMFMediaType** ppType)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT       hr    = S_OK;
    IMFMediaType* pType = NULL;

    GetPerformance();
    hr = MFPlugin::GetOutputAvailableType(dwOutputStreamID, dwTypeIndex, &pType);
    if (SUCCEEDED(hr))
    {
        if (m_bChangeOutputType && m_pOutputType)
        {
            if (!m_pOutputTypeCandidate)
            { // creating candidate type
                UINT32 w = 0, h = 0;
                MFVideoArea area;
                UINT32 arw = 0, arh = 0;

                hr = MFCreateMediaType(&m_pOutputTypeCandidate);
                if (SUCCEEDED(hr))
                {
                    w = m_MfxParamsVideo.mfx.FrameInfo.Width;
                    h = m_MfxParamsVideo.mfx.FrameInfo.Height;
                    arw = m_MfxParamsVideo.mfx.FrameInfo.AspectRatioW;
                    arh = m_MfxParamsVideo.mfx.FrameInfo.AspectRatioH;
                    memset(&area, 0, sizeof(MFVideoArea));
                    area.OffsetX.value = m_MfxParamsVideo.mfx.FrameInfo.CropX;
                    area.OffsetY.value = m_MfxParamsVideo.mfx.FrameInfo.CropY;
                    area.Area.cx = m_MfxParamsVideo.mfx.FrameInfo.CropW;
                    area.Area.cy = m_MfxParamsVideo.mfx.FrameInfo.CropH;
                    DBG_TRACE_1("CropX = %d", m_MfxParamsVideo.mfx.FrameInfo.CropX);
                    DBG_TRACE_1("CropY = %d", m_MfxParamsVideo.mfx.FrameInfo.CropY);
                    DBG_TRACE_1("CropW = %d", m_MfxParamsVideo.mfx.FrameInfo.CropW);
                    DBG_TRACE_1("CropH = %d", m_MfxParamsVideo.mfx.FrameInfo.CropH);
                    DBG_TRACE_1("PicStruct = %d", m_MfxParamsVideo.mfx.FrameInfo.PicStruct);
                }
                if (SUCCEEDED(hr)) hr = m_pOutputType->CopyAllItems(m_pOutputTypeCandidate);
                // correcting some parameters
                if (SUCCEEDED(hr)) hr = MFSetAttributeSize(m_pOutputTypeCandidate, MF_MT_FRAME_SIZE, w, h);
                if (SUCCEEDED(hr)) hr = MFSetAttributeRatio(m_pOutputTypeCandidate, MF_MT_PIXEL_ASPECT_RATIO, arw, arh);
                if (SUCCEEDED(hr)) hr = m_pOutputTypeCandidate->SetUINT32(MF_MT_INTERLACE_MODE, mf_mfx2ms_imode(m_MfxParamsVideo.mfx.FrameInfo.PicStruct));
                if (SUCCEEDED(hr)) hr = m_pOutputTypeCandidate->SetBlob(MF_MT_MINIMUM_DISPLAY_APERTURE, (UINT8*)&area, sizeof(MFVideoArea));
                if (SUCCEEDED(hr)) hr = m_pOutputTypeCandidate->SetBlob(MF_MT_GEOMETRIC_APERTURE, (UINT8*)&area, sizeof(MFVideoArea));
            }
            if (SUCCEEDED(hr))
            {
                m_pOutputTypeCandidate->AddRef();
                pType = m_pOutputTypeCandidate;
            }
        }
        else if (m_pOutputType)
        { // if output type was set we may offer only this type
            // should not occur
            hr = m_pOutputType->CopyAllItems(pType);
        }
        else
        {
            if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_MAJOR_TYPE, m_Reg.pOutputTypes[dwTypeIndex].major_type);
            if (SUCCEEDED(hr)) hr = pType->SetGUID(MF_MT_SUBTYPE,    m_Reg.pOutputTypes[dwTypeIndex].subtype);
            // if next attribute will not be set topology will insert MS color converter between decoder and EVR,
            // this may lead to unworking HW decoding
            if (SUCCEEDED(hr)) hr = pType->SetUINT32(MF_MT_ALL_SAMPLES_INDEPENDENT, TRUE);
            // if input type is not set we can only produce the hint to the caller about output types which MFT generates
            // otherwise we can be more precise and specify parameters of accepted output type
            if (m_pInputType)
            {
                GUID subtype = GUID_NULL;
                UINT32 par1 = 0, par2 = 0;

                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeSize(m_pInputType, MF_MT_FRAME_SIZE, &par1, &par2)))
                    hr = MFSetAttributeSize(pType, MF_MT_FRAME_SIZE, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeRatio(m_pInputType, MF_MT_PIXEL_ASPECT_RATIO, &par1, &par2)))
                    hr = MFSetAttributeRatio(pType, MF_MT_PIXEL_ASPECT_RATIO, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(m_pInputType->GetUINT32(MF_MT_INTERLACE_MODE, &par1)))
                    hr = pType->SetUINT32(MF_MT_INTERLACE_MODE, par1);
                if (SUCCEEDED(hr) && SUCCEEDED(MFGetAttributeRatio(m_pInputType, MF_MT_FRAME_RATE, &par1, &par2)))
                    hr = MFSetAttributeRatio(pType, MF_MT_FRAME_RATE, par1, par2);
                if (SUCCEEDED(hr) && SUCCEEDED(m_pInputType->GetGUID(MF_MT_SUBTYPE, &subtype)))
                    hr = pType->SetGUID(MF_MT_DEC_SUBTYPE, subtype);
            }
        }
    }
    if (SUCCEEDED(hr))
    {
        pType->AddRef();
        *ppType = pType;
    }
    SAFE_RELEASE(pType);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::CheckInputMediaType(IMFMediaType* pType)
{
    HRESULT hr = S_OK;

#ifdef MF_ENABLE_PICTURE_COMPLEXITY_LIMITS
    GUID subtype = GUID_NULL;

    // checking for supported resolution on connection stage only
    if (!m_bStreamingStarted && SUCCEEDED(pType->GetGUID(MF_MT_SUBTYPE, &subtype)))
    {
        // some code to restrict usage of the decoder plug-in depending on
        // type of input stream
    }
#else
    pType;
#endif
    return hr;
}

/*------------------------------------------------------------------------------*/

// Checks if data provided in pType could be decoded
bool MFPluginVDec::CheckHwSupport(void)
{
    bool ret_val = MFPlugin::CheckHwSupport();

    if (ret_val)
    {
        mfxStatus     sts = MFX_ERR_NONE;
        mfxVideoParam mfxVideoParamTemp;
        mfxFrameInfo* pmfxFrameInfo = &mfxVideoParamTemp.mfx.FrameInfo;

        memcpy(&mfxVideoParamTemp, &m_VideoParams_input, sizeof(mfxVideoParam));

        if (!(pmfxFrameInfo->FourCC) || !(pmfxFrameInfo->ChromaFormat))
        {
            // set any color format to make suitable values for Query, suppose it is NV12;
            // actual color format is obtained on output type setting
            pmfxFrameInfo->FourCC       = MAKEFOURCC('N','V','1','2');
            pmfxFrameInfo->ChromaFormat = MFX_CHROMAFORMAT_YUV420;
        }
        // temporal alignments: width to 16 and height to 32 to make suitable values for Query
        pmfxFrameInfo->Width        = (pmfxFrameInfo->CropW + 15) &~ 15;
        pmfxFrameInfo->Height       = (pmfxFrameInfo->CropH + 31) &~ 31;

        if (MFX_ERR_NONE == sts) sts = m_pmfxDEC->Query(&mfxVideoParamTemp, &mfxVideoParamTemp);
        if ((MFX_ERR_NONE != sts) && (m_dbg_no_SW_fallback || (MFX_WRN_PARTIAL_ACCELERATION != sts)))
            ret_val = false;
    }
    return ret_val;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::SetInputType(DWORD         dwInputStreamID,
                                   IMFMediaType* pType,
                                   DWORD         dwFlags)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE_1("pType = %p: +", pType);
    HRESULT hr = S_OK;
    bool bReinitNeeded = false;

    GetPerformance();
    hr = MFPlugin::SetInputType(dwInputStreamID, pType, dwFlags);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);

    if (!pType)
    { // resetting media type
        SAFE_RELEASE(m_pInputType);
    }
    CHECK_POINTER(pType, S_OK);
    // Validate the type.
    if (SUCCEEDED(hr)) hr = CheckMediaType(pType, m_Reg.pInputTypes, m_Reg.cInputTypes, &m_pInputInfo);
    if (SUCCEEDED(hr)) hr = CheckInputMediaType(pType);
    if (0 == (dwFlags & MFT_SET_TYPE_TEST_ONLY))
    {
        if (SUCCEEDED(hr))
        {
            // set major type; check major type is the same as was on output
            if (GUID_NULL == m_MajorType)
                m_MajorType = m_pInputInfo->major_type;
            else if (m_pInputInfo->major_type != m_MajorType)
                hr = MF_E_INVALIDTYPE;
        }
        if (SUCCEEDED(hr))
        {
            SAFE_RELEASE(m_pInputType);
            pType->AddRef();
            m_pInputType = pType;

            bReinitNeeded = (stReady == m_State) && IsVppNeeded(pType, m_pOutputType);
        }
        if (SUCCEEDED(hr)) hr = mf_mftype2mfx_frame_info(m_pInputType, &(m_VideoParams_input.mfx.FrameInfo));
        if (SUCCEEDED(hr) && bReinitNeeded)
        {
            SAFE_DELETE(m_pReinitMFBitstream);

            SAFE_NEW(m_pReinitMFBitstream, MFDecBitstream(hr));
            if (m_pReinitMFBitstream) m_pReinitMFBitstream->SetFiles(m_dbg_decin, m_dbg_decin_fc);
            else hr = E_OUTOFMEMORY;
        }
        if (SUCCEEDED(hr))
        {
            HRESULT hr_sts = S_OK;
            mfxU32  size = 0;
            MFDecBitstream* pMFBitstream = (bReinitNeeded)? m_pReinitMFBitstream: m_pMFBitstream;

            if ((WMMEDIASUBTYPE_WVC1 == m_pInputInfo->subtype) ||
                (WMMEDIASUBTYPE_WMV3 == m_pInputInfo->subtype))
            {
                hr_sts = m_pInputType->GetBlobSize(MF_MT_USER_DATA, &size);
                DBG_TRACE_2("m_pInputType->GetBlobSize(MF_MT_USER_DATA): size = %d, hr_sts = %x", size, hr_sts);
                if (SUCCEEDED(hr_sts) && size)
                {
                    mfxU8 *buf = (mfxU8*)malloc(size), *bdata = buf;
                    if (buf)
                    {
                        hr = m_pInputType->GetBlob(MF_MT_USER_DATA, bdata, size, &size);
                        if (SUCCEEDED(hr))
                        {
                            if (WMMEDIASUBTYPE_WVC1 == m_pInputInfo->subtype)
                            {
                                DBG_TRACE("WMMEDIASUBTYPE_WVC1");
                                m_VideoParams_input.mfx.CodecProfile = MFX_PROFILE_VC1_ADVANCED;
                                ++bdata; // skipping 1-st byte
                            }
                            else
                            {
                                DBG_TRACE("WMMEDIASUBTYPE_WMV3");
                                m_VideoParams_input.mfx.CodecProfile = MFX_PROFILE_VC1_SIMPLE; // or main, that's the same for FC initialization
                            }
                            hr = pMFBitstream->InitFC(&m_VideoParams_input, bdata, size);
                        }
                        SAFE_FREE(buf);
                    }
                    else hr = E_OUTOFMEMORY;
                }
            }
            if (MEDIASUBTYPE_H264 == m_pInputInfo->subtype)
            {
                // TODO: this is not currently used. Written for future purposes.
                hr_sts = m_pInputType->GetBlobSize(MF_MT_MPEG4_SAMPLE_DESCRIPTION, &size);
                DBG_TRACE_2("m_pInputType->GetBlobSize(MF_MT_MPEG4_SAMPLE_DESCRIPTION): size = %d, hr_sts = %x", size, hr_sts);
                if (SUCCEEDED(hr_sts) && size)
                {
                    mfxU8 *buf = (mfxU8*)malloc(size), *bdata = buf;
                    if (buf)
                    {
                        hr = m_pInputType->GetBlob(MF_MT_MPEG4_SAMPLE_DESCRIPTION, bdata, size, &size);
                        if (SUCCEEDED(hr))
                        {
                            DBG_TRACE("MEDIASUBTYPE_H264");
#if 0
                            hr = pMFBitstream->InitFC(&m_VideoParams_input, bdata, size);
#endif
                        }
                        SAFE_FREE(buf);
                    }
                    else hr = E_OUTOFMEMORY;
                }
            }
        }
        /* We will not check whether HW supports DEC operations in the following cases:
         *  - if "resolution change" occurs (we can encounter error and better to found it on
         * actual reinitialization; otherwise we can hang)
         */
        if (SUCCEEDED(hr) && (stReady != m_State) && !CheckHwSupport()) hr = E_FAIL;
        if (FAILED(hr))
        {
            SAFE_RELEASE(m_pInputType);
        }
        else if (bReinitNeeded)
        {
            if (SUCCEEDED(m_hrError))
            {
                // that's "resolution change"; need to reinit decoder
                m_bReinit     = true;
                m_bEndOfInput = true; // we need to obtain buffered frames
                m_bOutputTypeChangeRequired = true; // forcing change of output type
                if (m_pDeviceDXVA) m_bSendFakeSrf = true; // we need to send fake frame
                else m_bSendFakeSrf = false;
                // pushing async thread (starting call DecFrameAsync with NULL bitstream)
                AsyncThreadPush();
            }
            else if (m_bErrorHandlingFinished && !m_pDeviceDXVA)
            { // TODO: spread this opportunity on case when decoder works with INTC encoder and vpp
                // trying to recover from error if error handling is already finished
                HRESULT hr_sts = S_OK;
                mfxStatus sts = MFX_ERR_NONE;

                ResetPlgError();
                SAFE_RELEASE(m_pPostponedInput);
                m_bReinit = true;
                hr_sts = TryReinit(lock, sts, false);
                if (FAILED(hr_sts)) SetPlgError(hr_sts, sts);
                if (SUCCEEDED(m_hrError))
                {
                    /* If no errors occured in TryReinit, then we successfully
                     * recovered from previous plug-in error and need to reset
                     * error handling flags
                     */
                    m_bErrorHandlingStarted  = false;
                    m_bErrorHandlingFinished = false;
                }
            }
        }
        else
        {
            // that's plug-ins connnection stage
            memcpy(&m_MfxParamsVideo, &m_VideoParams_input, sizeof(mfxVideoParam));
        }
    }
    DBG_TRACE_2("m_pInputType = %p, hr = %x: -", m_pInputType, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::SetOutputType(DWORD         dwOutputStreamID,
                                    IMFMediaType* pType,
                                    DWORD         dwFlags)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE_1("pType = %p: +", pType);
    HRESULT     hr = S_OK;

    GetPerformance();
    hr = MFPlugin::SetOutputType(dwOutputStreamID, pType, dwFlags);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);
    // input type should be set previously
//    CHECK_POINTER(m_pInputType, MF_E_TRANSFORM_TYPE_NOT_SET);

    if (!pType)
    { // resetting media type
        ReleaseMediaType(m_pOutputType);
        ReleaseMediaType(m_pOutputTypeCandidate);
    }
    CHECK_POINTER(pType, S_OK);
    // Validate the type.
    if (SUCCEEDED(hr))
    {
        hr = CheckMediaType(pType, m_Reg.pOutputTypes, m_Reg.cOutputTypes, &m_pOutputInfo);

        if (SUCCEEDED(hr))
        {
            if (m_bChangeOutputType)
            {
                hr = (IsVppNeeded(pType, m_pOutputTypeCandidate))? E_FAIL: S_OK;
                if (FAILED(hr)) hr = S_OK; // will try "Pretend" algo
                else
                { // switching off pretending
                    memcpy(&m_FrameInfo_original, &(m_MfxParamsVideo.mfx.FrameInfo), sizeof(mfxFrameInfo));
                }
            }
            else
            {
                if (m_pOutputType)
                    hr = (IsVppNeeded(pType, m_pOutputType))? E_FAIL: S_OK;
                else
                    hr = (IsVppNeeded(pType, m_pInputType))? E_FAIL: S_OK;
            }
        }
    }
    if (0 == (dwFlags & MFT_SET_TYPE_TEST_ONLY))
    {
        if (SUCCEEDED(hr))
        {
            // set major type; check major type is the same as was on input
            if (GUID_NULL == m_MajorType)
                m_MajorType = m_pOutputInfo->major_type;
            else if (m_pOutputInfo->major_type != m_MajorType)
                hr = MF_E_INVALIDTYPE;
        }
        if (SUCCEEDED(hr))
        {
            // Really set the type, unless the caller was just testing.
            ReleaseMediaType(m_pOutputType);

            pType->AddRef();
            m_pOutputType = pType;
        }
        if (SUCCEEDED(hr) && m_bChangeOutputType)
        {
            DBG_TRACE("Finishing change of output type");
            m_bChangeOutputType = false;
            ReleaseMediaType(m_pOutputTypeCandidate);
            hr = SendOutput();
        }
    }
    DBG_TRACE_2("m_pOutputType = %p, hr = %x: -", m_pOutputType, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::ProcessMessage(MFT_MESSAGE_TYPE eMessage,
                                     ULONG_PTR        ulParam)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    GetPerformance();
    hr = MFPlugin::ProcessMessage(eMessage, ulParam);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);
    switch (eMessage)
    {
    case MFT_MESSAGE_COMMAND_FLUSH:
        DBG_TRACE("MFT_MESSAGE_COMMAND_FLUSH");
        if (MFX_ERR_NONE != ResetCodec(lock)) hr = hr;//E_FAIL;
        break;

    case MFT_MESSAGE_COMMAND_DRAIN:
        DBG_TRACE("MFT_MESSAGE_COMMAND_DRAIN");
        if (m_pmfxDEC)
        { // processing eos only if we were initialized
            m_bEndOfInput = true;
            m_bSendDrainComplete = true;
            // we can't accept any input if draining has begun
            m_bDoNotRequestInput = true; // prevent calling RequestInput
            m_NeedInputEventInfo.m_requested = 0; // prevent processing of sent RequestInput
            // pushing async thread (starting call DecFrameAsync with NULL bitstream)
            if (!m_bReinit) AsyncThreadPush();
        }
        break;

    case MFT_MESSAGE_COMMAND_MARKER:
        DBG_TRACE("MFT_MESSAGE_COMMAND_MARKER");
        // we always produce as much data as possible
        if (m_pmfxDEC) hr = SendMarker(ulParam);
        break;

    case MFT_MESSAGE_NOTIFY_BEGIN_STREAMING:
        m_bStreamingStarted = true;
        DBG_TRACE("MFT_MESSAGE_NOTIFY_BEGIN_STREAMING");
        break;

    case MFT_MESSAGE_NOTIFY_END_STREAMING:
        m_bStreamingStarted = false;
        DBG_TRACE("MFT_MESSAGE_NOTIFY_END_STREAMING");
        break;

    case MFT_MESSAGE_NOTIFY_START_OF_STREAM:
        if (!m_bNotifyStartOfStream)
        {
            // perform this only on first consistent MFT_MESSAGE_NOTIFY_START_OF_STREAM message
            //and skip all next MFT_MESSAGE_NOTIFY_START_OF_STREAM messages if no MFT_MESSAGE_NOTIFY_END_OF_STREAM was called

            if (!m_pAsyncThread)
            {
                SAFE_NEW(m_pAsyncThread, MyThread(hr, thAsyncThreadFunc, this));
                if (SUCCEEDED(hr) && !m_pAsyncThread) hr = E_FAIL;
            }
            else if (MFX_ERR_NONE != ResetCodec(lock)) hr = E_FAIL;
            m_bStreamingStarted  = true;
            m_bDoNotRequestInput = false;
            m_bNotifyStartOfStream = true;
            if (SUCCEEDED(hr)) hr = RequestInput();
            DBG_TRACE("MFT_MESSAGE_NOTIFY_START_OF_STREAM");
        }
        break;

    case MFT_MESSAGE_NOTIFY_END_OF_STREAM:
        // stream ends, we must not accept input data
        m_bDoNotRequestInput = true; // prevent calling RequestInput
        m_NeedInputEventInfo.m_requested = 0; // prevent processing of sent RequestInput

        m_bNotifyStartOfStream = false;

        DBG_TRACE("MFT_MESSAGE_NOTIFY_END_OF_STREAM");
        break;

    case MFT_MESSAGE_SET_D3D_MANAGER:
        DBG_TRACE("MFT_MESSAGE_SET_D3D_MANAGER");
        if (ulParam)
        {
            DBG_TRACE("Trying HW mode...");
            if (!m_pHWDevice)
            {
                m_pHWDevice = (IUnknown*)ulParam;
                m_pHWDevice->AddRef();

                hr = m_pHWDevice->QueryInterface(IID_IDirect3DDeviceManager9,
                                                 (void**)&m_D3DAllocatorParams.pManager);
                DBG_TRACE_1("m_D3DAllocatorParams.pManager = %p",
                    m_D3DAllocatorParams.pManager);
            }
            else
            {
                DBG_TRACE("Somewthing is going wrong - message will be ignored");
                hr = E_NOTIMPL;
            }
        }
        else
        {
            DBG_TRACE("Fallback to the SW mode, releasing HW interfaces...");
            SAFE_RELEASE(m_D3DAllocatorParams.pManager);
            SAFE_RELEASE(m_pHWDevice);
        }
        break;
    default:
        DBG_TRACE_1("Unknown message: %x", eMessage);
        break;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/
// Initialize Frame Allocator (if needed)

mfxStatus MFPluginVDec::InitFRA(void)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;

    if (!m_pDeviceDXVA && m_pHWDevice)
    { // decoder plug-in creates allocator
        if (!m_pD3DFrameAllocator)
        {
            if (MFX_ERR_NONE == sts)
            {
                sts = m_pMfxVideoSession->SetHandle(MFX_HANDLE_D3D9_DEVICE_MANAGER,
                                                    m_D3DAllocatorParams.pManager);
            }
            if (MFX_ERR_NONE == sts)
            {
                SAFE_NEW(m_pD3DFrameAllocator, MFFrameAllocator);
                if (!m_pD3DFrameAllocator) sts = MFX_ERR_MEMORY_ALLOC;
                else m_pD3DFrameAllocator->AddRef();
            }
            if (MFX_ERR_NONE == sts)
            {
                sts = m_pD3DFrameAllocator->Init(&m_D3DAllocatorParams);
            }
            if (MFX_ERR_NONE == sts) sts = m_pMfxVideoSession->SetFrameAllocator(m_pD3DFrameAllocator);
        }
        if (MFX_ERR_NONE == sts)
        {
            mfxFrameAllocRequest request;

            memset((void*)&request, 0, sizeof(mfxFrameAllocRequest));
            memset((void*)&m_DecAllocResponse, 0, sizeof(mfxFrameAllocResponse));
            memcpy(&(request.Info),&(m_MfxParamsVideo.mfx.FrameInfo),sizeof(mfxFrameInfo));
            request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET | MFX_MEMTYPE_FROM_DECODE;
            request.NumFrameMin = request.NumFrameSuggested = (mfxU16)m_uiSurfacesNum;
            sts = m_pD3DFrameAllocator->Alloc(m_pD3DFrameAllocator->pthis, &request, &m_DecAllocResponse);
        }
        if ((MFX_ERR_NONE == sts) && (m_uiSurfacesNum != m_DecAllocResponse.NumFrameActual))
        {
            sts = MFX_ERR_MEMORY_ALLOC;
        }
    }
    else if (m_pDeviceDXVA && m_pD3DFrameAllocator)
    { // downstream plug-in creates allocator
        if (MFX_ERR_NONE == sts)
        {
            mfxFrameAllocRequest request;

            memset((void*)&request, 0, sizeof(mfxFrameAllocRequest));
            memset((void*)&m_DecAllocResponse, 0, sizeof(mfxFrameAllocResponse));
            memcpy(&(request.Info),&(m_MfxParamsVideo.mfx.FrameInfo),sizeof(mfxFrameInfo));
            request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET | MFX_MEMTYPE_FROM_DECODE;
            request.NumFrameMin = request.NumFrameSuggested = (mfxU16)m_uiSurfacesNum;
            sts = m_pD3DFrameAllocator->Alloc(m_pD3DFrameAllocator->pthis, &request, &m_DecAllocResponse);
        }
        if ((MFX_ERR_NONE == sts) && (m_uiSurfacesNum != m_DecAllocResponse.NumFrameActual))
        {
            sts = MFX_ERR_MEMORY_ALLOC;
        }
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize Surfaces

mfxStatus MFPluginVDec::InitSRF(MyAutoLock& lock)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    mfxU32 i = 0;

    if (MFX_ERR_NONE == sts)
    {
        mfxFrameAllocRequest DecoderRequest;

        memset((void*)&DecoderRequest, 0, sizeof(mfxFrameAllocRequest));
        sts = m_pmfxDEC->QueryIOSurf(&m_MfxParamsVideo, &DecoderRequest);
        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts))
        {
            sts = MFX_ERR_NONE;
            if ((dbgHwMemory != m_dbg_Memory) && m_pDeviceDXVA)
            {
                // switching to SW mode
                DBG_TRACE("Switching to System Memory");
                m_MfxParamsVideo.IOPattern = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;

                sts = m_pmfxDEC->QueryIOSurf(&m_MfxParamsVideo, &DecoderRequest);
                if (MFX_WRN_PARTIAL_ACCELERATION == sts) sts = MFX_ERR_NONE;
            }
        }
        if (MFX_ERR_NONE == sts)
        {
            m_uiSurfacesNum += MAX(DecoderRequest.NumFrameSuggested,
                                   MAX(DecoderRequest.NumFrameMin, 1));
        }
    }
    if (m_pDeviceDXVA)
    {
        // if our decoder and encoder are directly connected, we will not change
        // output type
        m_bChangeOutputType = false;
        if (MFX_ERR_NONE == sts)
        {
            sts = m_pDeviceDXVA->InitPlg(m_pMfxVideoSession,
                                         &m_MfxParamsVideo,
                                         &m_uiSurfacesNum);
        }
        if ((MFX_ERR_NONE == sts) && (MFX_IOPATTERN_OUT_VIDEO_MEMORY == m_MfxParamsVideo.IOPattern))
        {
            // requesting additional interfaces if we are going to work on HW memory
            if (!m_pHWDevice)
            {
                m_pD3DFrameAllocator = m_pDeviceDXVA->GetFrameAllocator();
                m_D3DAllocatorParams.pManager = m_pDeviceDXVA->GetDeviceManager();

                if (m_D3DAllocatorParams.pManager)
                {
                    (m_D3DAllocatorParams.pManager)->AddRef();
                    m_pHWDevice = m_D3DAllocatorParams.pManager;
                }
                else sts = MFX_ERR_NULL_PTR;
                DBG_TRACE_1("m_pD3DFrameAllocator = %p", m_pD3DFrameAllocator);
                DBG_TRACE_1("m_D3DAllocatorParams.pManager = %p", m_D3DAllocatorParams.pManager);
            }
        }
    }
    // Free samples pool allocation (required for HW mode, nice to have for SW)
    if (MFX_ERR_NONE == sts)
    {
        if (m_pFreeSamplesPool)
        {
            lock.Unlock();
            m_pFreeSamplesPool->RemoveCallback();
            lock.Lock();
            SAFE_RELEASE(m_pFreeSamplesPool);
        }
        SAFE_NEW(m_pFreeSamplesPool, MFSamplesPool);
        if (!m_pFreeSamplesPool) sts = MFX_ERR_MEMORY_ALLOC;
        else
        {
            m_pFreeSamplesPool->AddRef();
            if (FAILED(m_pFreeSamplesPool->Init(m_uiSurfacesNum, this))) sts = MFX_ERR_UNKNOWN;
        }
    }
    if (MFX_ERR_NONE == sts) sts = InitFRA(); // initialize FRA if needed
    if (MFX_ERR_NONE == sts)
    {
        m_pWorkSurfaces = (MFYuvOutSurface**)calloc(m_uiSurfacesNum, sizeof(MFYuvOutSurface*));
        m_pOutSurfaces  = (MFYuvOutData*)calloc(m_uiSurfacesNum, sizeof(MFYuvOutData));
        if (!m_pWorkSurfaces || !m_pOutSurfaces) sts = MFX_ERR_MEMORY_ALLOC;
        else
        {
            for (i = 0; (MFX_ERR_NONE == sts) && (i < m_uiSurfacesNum); ++i)
            {
                SAFE_NEW(m_pWorkSurfaces[i], MFYuvOutSurface);
                if (!m_pWorkSurfaces[i]) sts = MFX_ERR_MEMORY_ALLOC;
                if (MFX_ERR_NONE == sts) m_pWorkSurfaces[i]->SetFile(m_dbg_decout);
                if (MFX_ERR_NONE == sts) sts = m_pWorkSurfaces[i]->Init(&(m_MfxParamsVideo.mfx.FrameInfo), m_pFreeSamplesPool);
                if (MFX_ERR_NONE == sts) sts = m_pWorkSurfaces[i]->Alloc((m_pD3DFrameAllocator)? m_DecAllocResponse.mids[i]: NULL);
                if (MFX_ERR_NONE == sts)
                {
                    SAFE_NEW(m_pOutSurfaces[i].pSyncPoint, mfxSyncPoint);
                    if (!m_pOutSurfaces[i].pSyncPoint) sts = MFX_ERR_MEMORY_ALLOC;
                }
            }
        }
    }
    if (MFX_ERR_NONE == sts)
    {
        m_pWorkSurface = m_pWorkSurfaces[0];
        m_pOutSurface  = m_pOutSurfaces;
        m_pDispSurface = m_pOutSurfaces;
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/
// Initialize codec

mfxStatus MFPluginVDec::InitCodec(MyAutoLock& lock)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;

    if ((MFX_ERR_NONE == sts) && (stHeaderNotDecoded == m_State))
    {
        mfxVideoParam VideoParams;

        // saving parameters
        memcpy(&VideoParams, &m_MfxParamsVideo, sizeof(mfxVideoParam));
        // decoding header
        sts = m_pmfxDEC->DecodeHeader(m_pBitstream, &m_MfxParamsVideo);
        // valid cases are:
        // MFX_ERR_NONE - header decoded, initialization can be performed
        // MFX_ERR_MORE_DATA - header not decoded, need provide more data
        if (MFX_ERR_MORE_DATA != sts) m_State = stReady;
        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts)) sts = MFX_ERR_NONE;
        if (MFX_ERR_NONE == sts)
        {
            // returning some parameter values
            m_MfxParamsVideo.mfx.NumThread = VideoParams.mfx.NumThread;
            if (!m_MfxParamsVideo.mfx.FrameInfo.FrameRateExtN || !m_MfxParamsVideo.mfx.FrameInfo.FrameRateExtD)
            {
                m_MfxParamsVideo.mfx.FrameInfo.FrameRateExtN = VideoParams.mfx.FrameInfo.FrameRateExtN;
                m_MfxParamsVideo.mfx.FrameInfo.FrameRateExtD = VideoParams.mfx.FrameInfo.FrameRateExtD;
            }
            if (!m_MfxParamsVideo.mfx.FrameInfo.AspectRatioW || !m_MfxParamsVideo.mfx.FrameInfo.AspectRatioH)
            {
                m_MfxParamsVideo.mfx.FrameInfo.AspectRatioW = VideoParams.mfx.FrameInfo.AspectRatioW;
                m_MfxParamsVideo.mfx.FrameInfo.AspectRatioH = VideoParams.mfx.FrameInfo.AspectRatioH;
            }
            // catching changes in output media type
            if (m_bOutputTypeChangeRequired ||
                (VideoParams.mfx.FrameInfo.Width != m_MfxParamsVideo.mfx.FrameInfo.Width) ||
                (VideoParams.mfx.FrameInfo.Height != m_MfxParamsVideo.mfx.FrameInfo.Height) ||
                (VideoParams.mfx.FrameInfo.AspectRatioW != m_MfxParamsVideo.mfx.FrameInfo.AspectRatioW) ||
                (VideoParams.mfx.FrameInfo.AspectRatioH != m_MfxParamsVideo.mfx.FrameInfo.AspectRatioH) ||
                (VideoParams.mfx.FrameInfo.CropX != m_MfxParamsVideo.mfx.FrameInfo.CropX) ||
                (VideoParams.mfx.FrameInfo.CropY != m_MfxParamsVideo.mfx.FrameInfo.CropY) ||
                (VideoParams.mfx.FrameInfo.CropW != m_MfxParamsVideo.mfx.FrameInfo.CropW) ||
                (VideoParams.mfx.FrameInfo.CropH != m_MfxParamsVideo.mfx.FrameInfo.CropH) ||
                (VideoParams.mfx.FrameInfo.PicStruct != m_MfxParamsVideo.mfx.FrameInfo.PicStruct))
            {
                DBG_TRACE("Output Type is needed to be changed");
                m_bOutputTypeChangeRequired = false;
                m_bChangeOutputType = true;
            }
            if (m_bChangeOutputType)
            {
                memcpy(&m_FrameInfo_original, &(VideoParams.mfx.FrameInfo), sizeof(mfxFrameInfo));
            }
            else
            {
                memcpy(&m_FrameInfo_original, &(m_MfxParamsVideo.mfx.FrameInfo), sizeof(mfxFrameInfo));
            }
        }
        else
        {
            // copying parameters back on MFX_ERR_MORE_DATA and errors
            memcpy(&m_MfxParamsVideo, &VideoParams, sizeof(mfxVideoParam));
        }
    }
    // parameters correction
    if (MFX_ERR_NONE == sts)
    {
        if (!m_pDeviceDXVA)
        {
            m_pOutputType->GetUnknown(MF_MT_D3D_DEVICE, IID_IMFDeviceDXVA, (void**)&m_pDeviceDXVA);
        }
        // by default setting preffered memory type
        m_MfxParamsVideo.IOPattern = (mfxU16)((m_pHWDevice || m_pDeviceDXVA)?
                                        MFX_IOPATTERN_OUT_VIDEO_MEMORY:
                                        MFX_IOPATTERN_OUT_SYSTEM_MEMORY);
        // now corrrecting memory type
        if (dbgHwMemory != m_dbg_Memory)
        {
            if (m_pDeviceDXVA && (MFX_IMPL_SOFTWARE == m_MSDK_impl))
            { // forcing SW memory (playback case is excluded, if m_pHWDevice was set)
                m_MfxParamsVideo.IOPattern = (mfxU16)MFX_IOPATTERN_OUT_SYSTEM_MEMORY;
            }
        }
        else
        { // we were forced to use HW memory, checking if this is possible
            if (!m_pHWDevice && !m_pDeviceDXVA) sts = MFX_ERR_ABORTED;
        }
    }
    // setting some parameters from output type
    if (MFX_ERR_NONE == sts)
    {
        mfxU32 colorFormat  = mf_get_color_format (m_pOutputInfo->subtype.Data1);
        mfxU16 chromaFormat = mf_get_chroma_format(m_pOutputInfo->subtype.Data1);

        if (colorFormat)
        {
            m_MfxParamsVideo.mfx.FrameInfo.FourCC       = colorFormat;
            m_MfxParamsVideo.mfx.FrameInfo.ChromaFormat = chromaFormat;
        }
        else sts = MFX_ERR_UNSUPPORTED;
    }
    if (MFX_ERR_NONE == sts) sts = InitSRF(lock); // init surfaces
    // Decoder initialization
    if (MFX_ERR_NONE == sts)
    {
        sts = m_pmfxDEC->Init(&m_MfxParamsVideo);
        if (!m_dbg_no_SW_fallback && (MFX_WRN_PARTIAL_ACCELERATION == sts)) sts = MFX_ERR_NONE;
    }
    if (MFX_ERR_NONE == sts)
    {
        sts = m_pmfxDEC->GetVideoParam(&m_MfxParamsVideo);
        m_MfxCodecInfo.m_uiOutFramesType = m_MfxParamsVideo.IOPattern;
    }
    if (MFX_ERR_MORE_DATA != sts)
    {
        m_MfxCodecInfo.m_InitStatus = sts;
        if (MFX_ERR_NONE != sts) CloseCodec(lock);
        else
        {
            DBG_TRACE_1("DP: obtained: m_MSDK_impl = %d", m_MSDK_impl);
            DBG_TRACE_1("DP: obtained: m_pHWDevice = %p (HW mem)", m_pHWDevice);
        }
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/

void MFPluginVDec::CloseCodec(MyAutoLock& lock)
{
    DBG_TRACE("+");
    mfxU32 i = 0;

    if (m_pFreeSamplesPool)
    {
        lock.Unlock();
        m_pFreeSamplesPool->RemoveCallback();
        lock.Lock();
        SAFE_RELEASE(m_pFreeSamplesPool);
    }
    if (m_pmfxDEC) m_pmfxDEC->Close();
    if (m_pWorkSurfaces)
    {
        for (i = 0; i < m_uiSurfacesNum; ++i)
        {
            if (m_pWorkSurfaces) SAFE_DELETE(m_pWorkSurfaces[i]);
            if (m_pOutSurfaces)  SAFE_DELETE(m_pOutSurfaces[i].pSyncPoint);
        }
    }
    SAFE_FREE(m_pWorkSurfaces);
    SAFE_FREE(m_pOutSurfaces);
    if (m_pD3DFrameAllocator)
    { // free memory if it was allocated inside decoder
        m_pD3DFrameAllocator->Free(m_pD3DFrameAllocator->pthis, &m_DecAllocResponse);
        memset((void*)&m_DecAllocResponse, 0, sizeof(mfxFrameAllocResponse));
    }
    if (!m_bReinit)
    {
        SAFE_RELEASE(m_D3DAllocatorParams.pManager);
        SAFE_RELEASE(m_pD3DFrameAllocator);
        SAFE_RELEASE(m_pHWDevice);
        if (m_pDeviceDXVA) m_pDeviceDXVA->ReleaseFrameAllocator();
        SAFE_RELEASE(m_pDeviceDXVA);

        if (m_pMFBitstream) m_pMFBitstream->Reset();
        SAFE_RELEASE(m_pPostponedInput);
    }
    // return parameters to initial state
    m_State         = stHeaderNotDecoded;
    m_uiSurfacesNum = MF_RENDERING_SURF_NUM;
    m_pWorkSurface  = NULL;
    m_pOutSurface   = NULL;
    m_pDispSurface  = NULL;
    m_iNumberLockedSurfaces = 0;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFPluginVDec::ResetCodec(MyAutoLock& /*lock*/)
{
    mfxStatus sts = MFX_ERR_NONE, res = MFX_ERR_NONE;
    mfxU32 i = 0;
    MFYuvOutData* pDispSurface = NULL;

    m_pAsyncThreadSemaphore->Reset();
    m_pAsyncThreadEvent->Reset();
    while (m_uiHasOutputEventExists && (m_pDispSurface != m_pOutSurface))
    {
        --m_uiHasOutputEventExists;
        // dropping decoded frames
        if (!(m_pDispSurface->bSyncPointUsed))
        {
            m_pMfxVideoSession->SyncOperation(*(m_pDispSurface->pSyncPoint),
                                              INFINITE);
        }
        // trying to free this surface
        if (m_pDispSurface->pMFSurface) // should not be NULL, checking just in case
        {
            m_pDispSurface->pMFSurface->IsDisplayed(true);
            m_pDispSurface->pMFSurface->Release();
        }
        m_pDispSurface->pSurface   = NULL;
        m_pDispSurface->pMFSurface = NULL;
        m_pDispSurface->bSyncPointUsed = false;
        m_pDispSurface->iSyncPointSts  = MFX_ERR_NONE;
        // moving to next undisplayed surface
        pDispSurface = m_pDispSurface;
        ++pDispSurface;
        if ((mfxU32)(pDispSurface-m_pOutSurfaces) >= m_uiSurfacesNum)
            pDispSurface = m_pOutSurfaces;
        m_pDispSurface = pDispSurface;
    }
    if (m_pmfxDEC)
    { // resetting encoder
        sts = m_pmfxDEC->Reset(&m_MfxParamsVideo);
        if (MFX_ERR_NONE == sts) sts = res;
    }
    if (m_pMFBitstream)
    {
        if (FAILED(m_pMFBitstream->Reset()) && (MFX_ERR_NONE == sts)) sts = MFX_ERR_UNKNOWN;
    }
    if (m_pWorkSurfaces)
    { // releasing resources
        for (i = 0; i < m_uiSurfacesNum; ++i)
        {
            if (FAILED((m_pWorkSurfaces[i])->Release()) && (MFX_ERR_NONE == sts)) sts = MFX_ERR_UNKNOWN;
        }
    }
    m_bEndOfInput      = false;
    m_bStartDrain      = false;
    m_bNeedWorkSurface = false;
    m_bOutputTypeChangeRequired = false;

    if (!m_bStreamingStarted)
    {
        /* We should zero event counts only if we are resetting before
         * processing new stream. We should not zero them on errors during
         * current processing - this may cause hangs, because ProcessOutput
         * may be called out of order.
         */
        m_NeedInputEventInfo.m_requested = 0;
        m_HasOutputEventInfo.m_requested = 0;
        m_NeedInputEventInfo.m_sent = 0;
        m_HasOutputEventInfo.m_sent = 0;
    }
    m_uiHasOutputEventExists = 0;
    m_iNumberLockedSurfaces  = 0;

    m_SyncOpSts = MFX_ERR_NONE;
    return sts;
}

/*------------------------------------------------------------------------------*/

/* TryReset function notes:
 *  1. This function reinits decoder plug-in only if downstream plug-in is
 * 3-d party one.
 *  2. If dowsntream plug-in is INTC then decoder will be reinited thru
 * IMFResetCallback interface.
 *  3. Reset handled by TryReinit function can't start if not all frames arrived
 * from downstream plug-in.
 */
HRESULT MFPluginVDec::TryReinit(MyAutoLock& lock, mfxStatus& sts, bool bReady)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    sts = MFX_ERR_NONE;
    if (SUCCEEDED(m_hrError))
    {
        if (!m_pDeviceDXVA || m_pDeviceDXVA && bReady)
        { // if we are connected with 3-d party plug-in we can reinitialize here
            HRESULT hr_sts = S_OK;

            m_bReinitStarted = true;

            m_LastFramerate = GetCurrentFramerate();
            if (m_LastFramerate) m_LastPts += (mfxU64)((mfxF64)MF_TIME_STAMP_FREQUENCY/m_LastFramerate);

            CloseCodec(lock);
            m_bReinit = false;
            m_bSetDiscontinuityAttribute = true;

            if (m_pReinitMFBitstream) // "resolution change" from SetInputType
            {
                // loading new frame info arrived from SetInputType
                memcpy(&m_MfxParamsVideo, &m_VideoParams_input, sizeof(mfxVideoParam));

                // deleting old bitstream
                m_pMFBitstream->Reset();
                SAFE_DELETE(m_pMFBitstream);
                // setting new bitstream
                m_pMFBitstream = m_pReinitMFBitstream;
                m_pReinitMFBitstream = NULL;

                if (m_pPostponedInput)
                {
                    hr = DecodeSample(lock, m_pPostponedInput, sts);
                    SAFE_RELEASE(m_pPostponedInput);
                }
                else if (!m_bDoNotRequestInput && !m_NeedInputEventInfo.m_requested) hr = RequestInput();
            }
            else
            {
                m_pBitstream = m_pMFBitstream->GetInternalMfxBitstream();
                if (SUCCEEDED(hr) && m_pBitstream && m_bSetDiscontinuityAttribute)
                {
                    // setting first time stamp after reinitilization if there is no time stamp on bitstream
                    if (MF_TIME_STAMP_INVALID == m_pBitstream->TimeStamp)
                    {
                        m_pBitstream->TimeStamp = m_LastPts;
                    }
                }
                if (SUCCEEDED(hr))
                {
                    sts = InitCodec(lock);
                    if ((MFX_ERR_NONE != sts) && (MFX_ERR_MORE_DATA != sts)) hr = E_FAIL;
                }
                if (SUCCEEDED(hr)) hr_sts = DecodeFrame(lock, sts);
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                hr_sts = m_pMFBitstream->Sync();
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                m_pBitstream = NULL;
                if (SUCCEEDED(hr) && !m_bReinit && !m_bDoNotRequestInput)
                {
                    // requesting input while free work surfaces are available
                    if (!m_NeedInputEventInfo.m_requested && !m_bNeedWorkSurface)
                    {
                        HRESULT hr_sts = S_OK;

                        hr_sts = RequestInput();
                        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                    }
                }
            }
            m_bReinitStarted = false;
        }
    }
    else
    {
        m_bReinitStarted = true;
        CloseCodec(lock);
        m_bReinit = false;
        SAFE_RELEASE(m_pPostponedInput);
        if (!m_bDoNotRequestInput && !m_NeedInputEventInfo.m_requested) hr = RequestInput();
        m_bReinitStarted = false;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

mfxF64 MFPluginVDec::GetCurrentFramerate(void)
{
    mfxF64 framerate = 0;
    mfxVideoParam params;

    memset(&params, 0, sizeof(mfxVideoParam));
    if (!m_pmfxDEC || (MFX_ERR_NONE != m_pmfxDEC->GetVideoParam(&params)))
    {
        memcpy(&params, &m_MfxParamsVideo, sizeof(mfxVideoParam));
    }
    if (!params.mfx.FrameInfo.FrameRateExtN ||
        !params.mfx.FrameInfo.FrameRateExtD)
    {
        params.mfx.FrameInfo.FrameRateExtN = MF_DEFAULT_FRAMERATE_NOM;
        params.mfx.FrameInfo.FrameRateExtD = MF_DEFAULT_FRAMERATE_DEN;
    }
    framerate = mf_get_framerate(params.mfx.FrameInfo.FrameRateExtN,
                                 params.mfx.FrameInfo.FrameRateExtD);
    return framerate;
}

/*------------------------------------------------------------------------------*/

inline HRESULT MFPluginVDec::SetFreeWorkSurface(MyAutoLock& /*lock*/)
{
    DBG_TRACE("+");
    HRESULT hr = E_FAIL, hr_sts = S_OK;
    mfxU32 i = 0, j = 0;
    bool bFound = false, bWait = false;

    do
    {
        for (i = 0; i < m_uiSurfacesNum; ++i)
        {
            hr_sts = m_pWorkSurfaces[i]->Release();
            // if release failed then surface could not be unlocked yet,
            // that's not an error
            if (SUCCEEDED(hr_sts) && !bFound)
            {
                hr = S_OK;
                bFound = true;
                m_pWorkSurface = m_pWorkSurfaces[i];
            }
        }
#if 0 // will not wait
#if 0
        // wait for free surface in HW case only
        bWait = (m_pHWDevice && !bFound);
#else
        // wait for free surface in SW and HW case
        bWait = !bFound;
#endif
        if (bWait) Sleep(MF_DEV_BUSY_SLEEP_TIME);
#endif
        ++j;
    } while (bWait && (j < MF_FREE_SURF_WAIT_NUM));

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

inline HRESULT MFPluginVDec::SetFreeOutSurface(MyAutoLock& /*lock*/)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    MFYuvOutData* pOutSurface = m_pOutSurface;

    ++pOutSurface;
    if ((mfxU32)(pOutSurface-m_pOutSurfaces) >= m_uiSurfacesNum)
        pOutSurface = m_pOutSurfaces;
    // setting free out surface using atomic operation
    m_pOutSurface = pOutSurface;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

/* SampleAppeared function notes:
 *  1. Whether arrived surface can be locked?
 *    - on normal processing with CIP downstrea plug-in arrived sample will be
 * associated with the surface already unlocked by MediaSDK.
 *    - if error occured in CIP downstream plug-in then surface can be still
 * locked (by current plug-in).
 *    - on processing with 3-d party downstream plug-in surface can also be
 * still locked (by current plug-in), but this should be rare case.
 *  2. If call of th function is fake, it is needed to be careful with used
 * objects: they can be not initialized yet.
 */

void MFPluginVDec::SampleAppeared(bool bFakeSample, bool bReinitSample)
{
    MyAutoLock lock(m_CritSec);
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;

    // errors checking
    if (m_bReinit && m_bReinitStarted) return;
    if (bFakeSample)
    { // additional checks on fake call
        if (stReady != m_State) return;
    }
    if (bReinitSample || m_bReinit && !m_bEndOfInput)
    {
        hr = TryReinit(lock, sts, bReinitSample);
    }
    else if (SUCCEEDED(m_hrError) && m_bNeedWorkSurface)
    {

        // setting free work surface
        if (SUCCEEDED(SetFreeWorkSurface(lock)) && SUCCEEDED(m_hrError))
        {
            m_bNeedWorkSurface = false;

            if (!m_bStartDrain)
            {
                HRESULT hr_sts = S_OK;

                if (NULL != (m_pBitstream = m_pMFBitstream->GetInternalMfxBitstream()))
                { // pushing buffered data to the decoder
                    hr_sts = DecodeFrame(lock, sts);
                    // in DecodeFrame m_hrError could be set and handled - checking errors
                    if (SUCCEEDED(m_hrError) && SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                    hr_sts = m_pMFBitstream->Sync();
                    if (SUCCEEDED(m_hrError) && SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                    m_pBitstream = NULL;
                }
                if (SUCCEEDED(m_hrError) && !m_bReinit && !m_bDoNotRequestInput)
                {
                    if (!m_NeedInputEventInfo.m_requested && !m_bNeedWorkSurface)
                    {
                        RequestInput();
                    }
                }
            }
            else if (!bFakeSample) AsyncThreadPush(); // async thread already started, status is unneeded
        }
    }
    if (FAILED(hr) && SUCCEEDED(m_hrError))
    {
        // setting plug-in error
        SetPlgError(hr, sts);
        // handling error
        HandlePlgError(lock, bFakeSample);
    }
}

/*------------------------------------------------------------------------------*/

void MFPluginVDec::SetPlgError(HRESULT hr, mfxStatus sts)
{
    MFPlugin::SetPlgError(hr, sts);
    if (FAILED(hr))
    { // dumping error information
        if (SUCCEEDED(m_MfxCodecInfo.m_hrErrorStatus))
        {
            m_MfxCodecInfo.m_hrErrorStatus = hr;
        }
        if (MFX_ERR_NONE == m_MfxCodecInfo.m_ErrorStatus)
        {
            m_MfxCodecInfo.m_ErrorStatus = sts;
        }        
    }
    return;
}

/*------------------------------------------------------------------------------*/

void MFPluginVDec::ResetPlgError(void)
{
    MFPlugin::ResetPlgError();
    m_MfxCodecInfo.m_uiErrorResetCount = m_uiErrorResetCount;
}

/*------------------------------------------------------------------------------*/

void MFPluginVDec::HandlePlgError(MyAutoLock& lock, bool bCalledFromAsyncThread)
{
    if (FAILED(m_hrError))
    {
        if (m_bErrorHandlingFinished)
        {
            if (m_bEndOfInput && m_bSendDrainComplete)
            {
                m_bEndOfInput = false;
                m_bSendDrainComplete = false;
                DrainComplete();
            }
        }
        else if (!m_bErrorHandlingStarted)
        {
            m_bErrorHandlingStarted = true;

            // notifying upstream plug-in about error occurred only if no output was generated
            if (m_pInputType && m_dbg_return_errors)
            {
                m_pInputType->SetUINT32(MF_MT_DOWNSTREAM_ERROR, m_hrError);
            }
            // notifying async thread
            if (m_pAsyncThread && !bCalledFromAsyncThread)
            {
                m_pAsyncThreadEvent->Signal();
                m_pAsyncThreadSemaphore->Post();
                // awaiting while error will be detected by async thread
                lock.Unlock();
                m_pErrorFoundEvent->Wait();
                m_pErrorFoundEvent->Reset();
                lock.Lock();
            }
            // resetting
            ResetCodec(lock); // status is not needed here

            if (m_bSendDrainComplete)
            {
                m_bSendDrainComplete = false;
                DrainComplete();
            }
            else if (m_bReinit)
            {
                if (!m_bReinitStarted)
                {
                    mfxStatus sts = MFX_ERR_NONE;

                    TryReinit(lock, sts, false);
                }
            }
            else
            { // forcing calling of ProcessInput on errors during processing
                if (!m_bDoNotRequestInput && !m_NeedInputEventInfo.m_requested) RequestInput();
            }

            // sending event that error was handled
            if (m_pAsyncThread && !bCalledFromAsyncThread)
            {
                m_pErrorHandledEvent->Signal();
            }
            m_bErrorHandlingFinished = true;
        }
    }
}

/*------------------------------------------------------------------------------*/

bool MFPluginVDec::ReturnPlgError(void)
{
    return (m_dbg_return_errors)? true: false;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::AsyncThreadFunc(void)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    mfxSyncPoint* pSyncPoint = NULL;

    while (1)
    {
        HRESULT hr = S_OK;

        // avaiting for the signal that next surface is ready
        lock.Unlock();
        {
            DBG_TRACE("Wait: +");
            while (WAIT_TIMEOUT == m_pAsyncThreadSemaphore->TimedWait(MF_DEC_FAKE_SAMPLE_APPEARED_SLEEP_TIME))
            {
                SampleAppeared(true, false);
            }
            DBG_TRACE("Wait: -");
        }
        lock.Lock();
        if (m_bAsyncThreadStop) break;
        if (FAILED(m_hrError) && m_bErrorHandlingStarted && !m_bErrorHandlingFinished)
        {
            // sending event that error was detected and thread was stopped
            m_pErrorFoundEvent->Signal();
            // awaiting while error will be handled
            lock.Unlock();
            m_pErrorHandledEvent->Wait();
            m_pErrorHandledEvent->Reset();
            lock.Lock();
            continue;
        }
        if (SUCCEEDED(m_hrError) && m_bEndOfInput && !m_uiHasOutputEventExists)
        {
            if (stHeaderNotDecoded != m_State)
            {
                m_bStartDrain = true;
                if (m_bNeedWorkSurface)
                {
                    lock.Unlock();
                    SampleAppeared(true, false);
                    lock.Lock();
                }
                // getting remained data in the buffer
                m_pBitstream = (!m_bReinit && m_pMFBitstream)? m_pMFBitstream->GetInternalMfxBitstream(): NULL;
                // decoding data or getting remained frames from the decoder
                hr = DecodeFrame(lock, sts);
                if (m_pBitstream)
                {
                    if (MF_E_TRANSFORM_NEED_MORE_INPUT == hr) m_pMFBitstream->SetNoDataStatus(true);
                    // we are not interested in the status from Sync here
                    m_pMFBitstream->Sync();
                    m_pBitstream = NULL;
                    // getting remained frames from the decoder
                    if (MF_E_TRANSFORM_NEED_MORE_INPUT == hr) hr = DecodeFrame(lock, sts);
                }
            }
            else hr = MF_E_TRANSFORM_NEED_MORE_INPUT;

            if (MF_E_TRANSFORM_NEED_MORE_INPUT == hr)
            {
                hr = S_OK;

                m_bEndOfInput = false;
                m_bStartDrain = false;

                if (m_pMFBitstream) m_pMFBitstream->SetNoDataStatus(false);
                if (m_bReinit && !m_bReinitStarted) hr = TryReinit(lock, sts, false);

                if (SUCCEEDED(hr))
                {
                    if (m_bSendDrainComplete)
                    {
                        m_bSendDrainComplete = false;
                        DrainComplete();
                    }
                    continue;
                }
            }
            else if (SUCCEEDED(hr))
            {
                if (!m_bNeedWorkSurface) AsyncThreadPush();
                continue;
            }
        }
        if (SUCCEEDED(hr) && SUCCEEDED(m_hrError))
        {
            // awaiting for the async operation completion if needed
            if (m_pDispSurface->bSyncPointUsed) sts = m_pDispSurface->iSyncPointSts;
            else
            {
#ifdef OMIT_DEC_ASYNC_SYNC_POINT
                if (!m_pDeviceDXVA)
#else
                if (1)
#endif
                {
                    m_pDispSurface->bSyncPointUsed = true;
                    pSyncPoint = m_pDispSurface->pSyncPoint;
                    lock.Unlock();
                    {
                        m_SyncOpSts = sts = m_pMfxVideoSession->SyncOperation(*pSyncPoint, INFINITE);
                        // sending event that some resources may be free
                        m_pDevBusyEvent->Signal();
                    }
                    lock.Lock();
                }
                else sts = MFX_ERR_NONE;
                m_pDispSurface->iSyncPointSts = sts;
            }
            // sending request for ProcessOutput call
            if (MFX_ERR_NONE == sts) hr = SendOutput();
            else hr = E_FAIL;
        }
        if (SUCCEEDED(hr) && SUCCEEDED(m_hrError))
        {
            // awaiting for ProcessOuput completion
            lock.Unlock();
            {
                DBG_TRACE("Waiting for ProcessOutput: +");
                m_pAsyncThreadEvent->Wait();
                m_pAsyncThreadEvent->Reset();
                DBG_TRACE("Waiting for ProcessOutput: -");
            }
            lock.Lock();
        }
        // handling errors
        if (FAILED(hr) || FAILED(m_hrError))
        { // only here we will call HandlePlgError on errors - we need to send DrainComplete event
            SetPlgError(hr, sts);
            HandlePlgError(lock, true);
        }
    };
    DBG_TRACE("S_OK");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

// Function returns 'true' if SyncOperation was called and 'false' overwise
bool MFPluginVDec::HandleDevBusy(mfxStatus& sts)
{
    bool ret_val = false;
    MFYuvOutData* pOutSurface = m_pDispSurface;

    sts = MFX_ERR_NONE;
    do
    {
        if (!(pOutSurface->bSyncPointUsed) && pOutSurface->pMFSurface)
        {
            pOutSurface->bSyncPointUsed = true;
            sts = pOutSurface->iSyncPointSts  = m_pMfxVideoSession->SyncOperation(*(pOutSurface->pSyncPoint), INFINITE);
            ret_val = true;
            break;
        }
        ++pOutSurface;
        if ((mfxU32)(pOutSurface-m_pOutSurfaces) >= m_uiSurfacesNum) pOutSurface = m_pOutSurfaces;
    } while(pOutSurface != m_pDispSurface);
    return ret_val;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::DecodeFrame(MyAutoLock& lock, mfxStatus& sts)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxU32 i = 0;
    bool bSendFakeSrf = false;

    sts = MFX_ERR_NONE;
    while (SUCCEEDED(hr) && (stReady == m_State) && !m_bNeedWorkSurface &&
           (!m_pBitstream || m_pBitstream->DataLength))
    {
        mfxFrameSurface1* pWorkSurface = m_pWorkSurface->GetSurface(); // can not be NULL

        if (pWorkSurface) // should alwais be valid pointer
        {
            pWorkSurface->Data.FrameOrder = m_iNumberLockedSurfaces;
            // Decoding bitstream. If function called on EOS then drainnig begins and we must
            // wait while decoder can accept input data
            do
            {
                sts = m_pmfxDEC->DecodeFrameAsync(m_pBitstream, pWorkSurface,
                                                  &(m_pOutSurface->pSurface),
                                                  m_pOutSurface->pSyncPoint);
                if ((MFX_WRN_DEVICE_BUSY == sts) && (MFX_ERR_NONE == m_SyncOpSts))
                {
                    mfxStatus res = MFX_ERR_NONE;

                    if (!HandleDevBusy(res))
                    {
                        m_pDevBusyEvent->TimedWait(MF_DEV_BUSY_SLEEP_TIME);
                        m_pDevBusyEvent->Reset();
                    }
                    if (MFX_ERR_NONE != res) sts = res;
                }
            } while ((MFX_WRN_DEVICE_BUSY == sts) && (MFX_ERR_NONE == m_SyncOpSts));
        }
        else sts = MFX_ERR_NULL_PTR;
        // valid cases for the status are:
        // MFX_ERR_NONE - data processed, output will be generated
        // MFX_ERR_MORE_DATA - data buffered, output will not be generated
        // MFX_WRN_VIDEO_PARAM_CHANGED - some params changed, but decoding can be continued
        // MFX_ERR_INCOMPATIBLE_VIDEO_PARAM - some params changed, decoding should be reinitialized

        // Status correction
        if (MFX_WRN_VIDEO_PARAM_CHANGED == sts) sts = MFX_ERR_MORE_SURFACE;
        else if ((MFX_ERR_MORE_DATA == sts) && m_bStartDrain && m_bSendFakeSrf)
        {
            sts = MFX_ERR_NONE;
            m_pOutSurface->pSurface = pWorkSurface;
            m_pOutSurface->iSyncPointSts  = MFX_ERR_NONE;
            m_pOutSurface->bSyncPointUsed = true;

            // we will send fake surface only once on each reinit
            bSendFakeSrf   = true;
            m_bSendFakeSrf = false;
        }
        // Status processing
        if ((MFX_ERR_MORE_DATA == sts) && m_bStartDrain && !bSendFakeSrf)
        {
            hr = MF_E_TRANSFORM_NEED_MORE_INPUT;
        }
        else if ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts))
        {
            HRESULT hr_sts = S_OK;
            bool bOutExists = (NULL != m_pOutSurface->pSurface);

            if ((MFX_ERR_NONE == sts) && bOutExists)
            {
                // searching for MFSurface
                for (i = 0; i < m_uiSurfacesNum; ++i)
                {
                    if (m_pOutSurface->pSurface == m_pWorkSurfaces[i]->GetSurface())
                    {
                        if (!bSendFakeSrf) ++m_iNumberInputSurfaces;
                        m_pOutSurface->pMFSurface = m_pWorkSurfaces[i];
                        // locking surface for future displaying
                        m_pOutSurface->pMFSurface->IsDisplayed(false);
                        m_pOutSurface->pMFSurface->IsFakeSrf(bSendFakeSrf);
                        m_pOutSurface->pMFSurface->IsGapSrf(m_bSetDiscontinuityAttribute);
                        if (m_bSetDiscontinuityAttribute) m_bSetDiscontinuityAttribute = false;
                        // saving time stamp for future use
                        m_LastPts = m_pOutSurface->pSurface->Data.TimeStamp;
                        // sending output
                        ++m_uiHasOutputEventExists;
                        AsyncThreadPush();
                        break;
                    }
                }
                // normally, the following will not occur
                if (SUCCEEDED(hr_sts) && (i >= m_uiSurfacesNum)) hr_sts = E_FAIL;
                // checking errors
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
                // setting free out surface
                hr_sts = SetFreeOutSurface(lock);
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
            }
            if (pWorkSurface->Data.Locked || bOutExists)
            {
                if (pWorkSurface->Data.Locked) ++m_iNumberLockedSurfaces;
                // setting free work surface
                if (FAILED(SetFreeWorkSurface(lock)))
                { // will wait for free surface
                    sts = MFX_ERR_MORE_DATA;
                    m_bNeedWorkSurface = true;
                }
                DBG_TRACE_1("m_bNeedWorkSurface = %d", m_bNeedWorkSurface);
            }
            if (MFX_ERR_MORE_DATA == sts) break;
        }
        else if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == sts)
        {
            // setting flags to process this situation
            m_bReinit      = true;
            m_bEndOfInput  = true; // we need to obtain buffered frames
            if (m_pDeviceDXVA) m_bSendFakeSrf = true; // we need to send fake frame
            else m_bSendFakeSrf = false;
            // pushing async thread (starting call DecFrameAsync with NULL bitstream)
            AsyncThreadPush();
            break;
        }
        else hr = E_FAIL;
        // no iterations on EOS
        if (m_bEndOfInput) break;
    }
    DBG_TRACE_2("sts = %d, hr = %x: -", sts, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::DecodeSample(MyAutoLock& lock, IMFSample* pSample, mfxStatus& sts)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    sts = MFX_ERR_NONE;
    if (m_bReinit)
    {
        /* Reinitalization detected in SetInputType function. We can't process
         * input sample right now - buffering it for future processing.
         */
        SAFE_RELEASE(m_pPostponedInput); // should be NULL here, releasing just in case
        pSample->AddRef();
        m_pPostponedInput = pSample;
    }
    else
    {
        if (SUCCEEDED(hr)) hr = m_pMFBitstream->Load(pSample);

        while (SUCCEEDED(hr) && (NULL != (m_pBitstream = m_pMFBitstream->GetMfxBitstream())))
        {
            HRESULT hr_sts = S_OK;

            if (SUCCEEDED(hr) && m_bSetDiscontinuityAttribute)
            {
                // setting first time stamp after reinitilization if there is no time stamp on bitstream
                if (MF_TIME_STAMP_INVALID == m_pBitstream->TimeStamp)
                {
                    m_pBitstream->TimeStamp = m_LastPts;
                }
            }
            // Decoder initialization
            if (SUCCEEDED(hr) && (stReady != m_State))
            {
                sts = InitCodec(lock);
                if ((MFX_ERR_NONE != sts) && (MFX_ERR_MORE_DATA != sts)) hr = E_FAIL;
            }
            // Input data processing
            if (SUCCEEDED(hr)) hr = DecodeFrame(lock, sts);
            // Synchronizing input buffer
            hr_sts = m_pMFBitstream->Sync();
            if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;

            if (m_bReinit) break;
        }
        m_pMFBitstream->Unload();
        if (SUCCEEDED(hr) && !m_bReinit && !m_bDoNotRequestInput)
        {
            // requesting input while free work surfaces are available
            if (!m_NeedInputEventInfo.m_requested && !m_bNeedWorkSurface)
            {
                HRESULT hr_sts = S_OK;

                hr_sts = RequestInput();
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
            }
        }
    }
    DBG_TRACE_2("sts = %d, hr = %x: -", sts, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::ProcessInput(DWORD      dwInputStreamID,
                                   IMFSample* pSample,
                                   DWORD      dwFlags)
{
    MFTicker ticker(&m_ticks_ProcessInput);
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;

    GetPerformance();
    hr = MFPlugin::ProcessInput(dwInputStreamID, pSample, dwFlags);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);

    SET_HR_ERROR(0 == dwFlags, hr, E_INVALIDARG);
    SET_HR_ERROR(!m_bEndOfInput || m_bReinit, hr, E_FAIL);
    if (SUCCEEDED(hr) && m_pOutputType)
    {
        UINT32 hr_sts = S_OK;

        if (SUCCEEDED(m_pOutputType->GetUINT32(MF_MT_DOWNSTREAM_ERROR, &hr_sts)))
        {
            hr = hr_sts;
        }
    }

    if (SUCCEEDED(hr)) hr = m_hrError;
    if (SUCCEEDED(hr)) ++(m_MfxCodecInfo.m_nInFrames);
    if (SUCCEEDED(hr)) hr = DecodeSample(lock, pSample, sts);
    if (FAILED(hr) && SUCCEEDED(m_hrError))
    {
        // setting plug-in error
        SetPlgError(hr, sts);
        // handling error
        HandlePlgError(lock);
    }
    if (!ReturnPlgError()) hr = S_OK;
    DBG_TRACE_1("m_MfxCodecInfo.m_nInFrames = %d", m_MfxCodecInfo.m_nInFrames);
    DBG_TRACE_1("m_iNumberLockedSurfaces = %d", m_iNumberLockedSurfaces);
    DBG_TRACE_1("m_iNumberInputSurfaces  = %d", m_iNumberInputSurfaces);
    DBG_TRACE_2("sts = %d, hr = %x: -", sts, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFPluginVDec::ProcessOutput(DWORD  dwFlags,
                                    DWORD  cOutputBufferCount,
                                    MFT_OUTPUT_DATA_BUFFER* pOutputSamples,
                                    DWORD* pdwStatus)
{
    MFTicker ticker(&m_ticks_ProcessOutput);
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;
    bool bDisplayFrame = false;

    GetPerformance();
    hr = MFPlugin::ProcessOutput(dwFlags, cOutputBufferCount, pOutputSamples, pdwStatus);
    CHECK_EXPRESSION(SUCCEEDED(hr), hr);

    SET_HR_ERROR(SUCCEEDED(hr), hr, E_FAIL);
    SET_HR_ERROR(0 == dwFlags, hr, E_INVALIDARG);
    SET_HR_ERROR(m_pmfxDEC, hr, E_FAIL);
    SET_HR_ERROR(m_pDispSurface && ((m_pDispSurface != m_pOutSurface) || m_bNeedWorkSurface || m_bEndOfInput),
                 hr, E_FAIL);

    if (SUCCEEDED(hr)) hr = m_hrError;
    if (m_bChangeOutputType)
    {
        DBG_TRACE("Starting change of output type");
        if (SUCCEEDED(hr))
        {
            pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_FORMAT_CHANGE;
            *pdwStatus = 0;
            hr = MF_E_TRANSFORM_STREAM_CHANGE;
        }
    }
    else
    {
        DBG_TRACE("Normal decoding");
        if (m_uiHasOutputEventExists) --m_uiHasOutputEventExists;
        if (SUCCEEDED(hr) && ((m_pDispSurface != m_pOutSurface) || m_bNeedWorkSurface)) bDisplayFrame = true;
        if (SUCCEEDED(hr) && bDisplayFrame)
        {
            hr = m_pDispSurface->pMFSurface->Sync();
            if (!m_pDeviceDXVA &&
                ((m_MfxParamsVideo.mfx.FrameInfo.Width != m_FrameInfo_original.Width) ||
                 (m_MfxParamsVideo.mfx.FrameInfo.Height != m_FrameInfo_original.Height)))
            {
                HRESULT hr_sts = S_OK;
                hr_sts = m_pDispSurface->pMFSurface->Pretend(m_FrameInfo_original.Width,
                                                             m_FrameInfo_original.Height);
                if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
            }
        }
        // Set status flags
        if (SUCCEEDED(hr))
        {
            pOutputSamples[0].pSample  = (bDisplayFrame)? m_pDispSurface->pMFSurface->GetSample(): NULL;
            if (m_HasOutputEventInfo.m_requested)
            {
                pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_INCOMPLETE;
            }
            *pdwStatus = 0;

            if (pOutputSamples[0].pSample)
            {
                UINT32 bFakeSrf = FALSE;

                if (FAILED(pOutputSamples[0].pSample->GetUINT32(MF_MT_FAKE_SRF, &bFakeSrf)) || !bFakeSrf)
                {
                    ++(m_MfxCodecInfo.m_nOutFrames);
                }
            }
        }
        if (bDisplayFrame)
        {
            MFYuvOutData* pDispSurface = NULL;

            // trying to free this surface
            m_pDispSurface->pMFSurface->IsDisplayed(true);
            m_pDispSurface->pMFSurface->Release();
            m_pDispSurface->pSurface   = NULL;
            m_pDispSurface->pMFSurface = NULL;
            m_pDispSurface->bSyncPointUsed = false;
            // moving to next undisplayed surface
            pDispSurface = m_pDispSurface;
            ++pDispSurface;
            if ((mfxU32)(pDispSurface-m_pOutSurfaces) >= m_uiSurfacesNum)
                pDispSurface = m_pOutSurfaces;
            m_pDispSurface = pDispSurface;

            m_pAsyncThreadEvent->Signal();
        }
    }
    if (MF_E_TRANSFORM_STREAM_CHANGE != hr)
    {
        if (pOutputSamples && (FAILED(hr) || !(pOutputSamples[0].pSample) && !(pOutputSamples[0].dwStatus)))
        { // we should set MFT_OUTPUT_DATA_BUFFER_NO_SAMPLE status if we do not generate output
            SAFE_RELEASE(pOutputSamples[0].pSample);
            pOutputSamples[0].dwStatus = MFT_OUTPUT_DATA_BUFFER_NO_SAMPLE;
        }
        if (FAILED(hr))
        {
            if (SUCCEEDED(m_hrError))
            {
                // setting plug-in error
                SetPlgError(hr, sts);
                // handling error
                HandlePlgError(lock);
            }
        }
        // errors will be handled either in ProcessInput
        hr = S_OK;
    }
    DBG_TRACE_1("m_MfxCodecInfo.m_nOutFrames = %d", m_MfxCodecInfo.m_nOutFrames);
    DBG_TRACE_1("m_iNumberOutputSurfaces = %d", m_iNumberOutputSurfaces);
    DBG_TRACE_2("sts = %d, hr = %x: -", sts, hr);
    return hr;
}

/*------------------------------------------------------------------------------*/
// Global functions

#undef  PTR_THIS // no 'this' pointer in global functions
#define PTR_THIS NULL

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME pRegData->pPluginName

// CreateInstance
// Static method to create an instance of the source.
//
// iid:         IID of the requested interface on the source.
// ppSource:    Receives a ref-counted pointer to the source.
HRESULT MFPluginVDec::CreateInstance(REFIID iid,
                                     void** ppMFT,
                                     ClassRegData* pRegData)

{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    MFPluginVDec *pMFT = NULL;
    mf_tick tick = mf_get_tick();

    CHECK_POINTER(ppMFT, E_POINTER);
#if 0 // checking of supported platform
    if (!CheckPlatform())
    {
        *ppMFT = NULL;
        return E_FAIL;
    }
#endif
    SAFE_NEW(pMFT, MFPluginVDec(hr, pRegData));
    CHECK_POINTER(pMFT, E_OUTOFMEMORY);

    pMFT->SetStartTick(tick);
    if (SUCCEEDED(hr))
    {
        hr = pMFT->QueryInterface(iid, ppMFT);
    }
    if (FAILED(hr))
    {
        SAFE_DELETE(pMFT);
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}
