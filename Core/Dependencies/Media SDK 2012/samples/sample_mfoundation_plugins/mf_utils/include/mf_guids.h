/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_guids.h

Purpose: define common code for MSDK based MF plug-ins.

Defined Classes, Structures & Enumerations:
  * GUID_info - stores types of MFT (major & sub types)
  * ClassRegData - stores registration data of MF plug-ins

Defined Types:
  * CreateInstanceFn - defines type of plug-ins CreateInstance functions
  * FillParamsFn - defines type of parameters filling functions
  * ClassRegDataFn - defines type of helper function

Defined Macroses:
  * CHARS_IN_GUID - number of chars in GUID
  * SAFE_NEW - calls new for specified class, catches exceptions
  * SAFE_NEW_ARRAY - calls new for specified array, catches exceptions
  * SAFE_DELETE - deletes class object and frees pointer
  * SAFE_DELETE_ARRAY - deletes array object and frees pointer
  * SAFE_RELEASE - releases com object and frees pointer
  * SAFE_FREE - deletes variable and frees pointer
  * myfopen - redirection on fsopen
  * mywfopen - redirection on _wfsopen

Defined Global Variables:
  * g_UncompressedVideoTypes - list of uncompressed video types used in plug-ins
  * g_DecoderRegFlags - decoder MFTs registry flags
  * g_EncoderRegFlags - encoder MFTs registry flags
  # GUIDS
  * CLSID_MF_MPEG2EncFilter
  * CLSID_MF_H264EncFilter
  * CLSID_MF_MPEG2DecFilter
  * CLSID_MF_H264DecFilter
  * CLSID_MF_VC1DecFilter
  * MFVideoFormat_NV12_MFX
  * MEDIASUBTYPE_MPEG1_MFX
  * MEDIASUBTYPE_MPEG2_MFX
  * MEDIASUBTYPE_H264_MFX
  * MEDIASUBTYPE_VC1_MFX
  * MEDIASUBTYPE_VC1P_MFX

Defined Global Functions:
  * myDllMain
  * AMovieSetupRegisterServer
  * AMovieSetupUnregisterServer
  * CreateVDecPlugin - creates video decoder plug-in instance
  * CreateVEncPlugin - creates video encoder plug-in instance

*********************************************************************************/

#ifndef __MF_GUIDS_H__
#define __MF_GUIDS_H__

// switching off Microsoft warnings
#pragma warning(disable: 4201) // nameless structs/unions
#pragma warning(disable: 4995) // 'name' was declared deprecated
#include <windows.h>
#include <tchar.h>
#include <assert.h>
#include <strsafe.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <share.h>
#include <list>
#include <objbase.h>
#include <uuids.h>
#include <initguid.h>
#include <atlbase.h>
#include <mfapi.h>
#include <mfidl.h>
#include <mferror.h>
#include <mftransform.h>
#include <dmoreg.h>      // DirectX SDK registration
#include <wmpservices.h>
#include <wmsdkidl.h>
#include <wmcodecdsp.h>
#include <evr.h>
#include <d3d9.h>
#include <dxva2api.h>
#include <KS.h>
#include <Codecapi.h>
#include <Psapi.h>
#pragma warning(default: 4995) // 'name' was declared deprecated
#pragma warning(default: 4201) // nameless structs/unions

/*------------------------------------------------------------------------------*/

#include "mfxdefs.h"
#include "mfxstructures.h"
#include "mfxvideo.h"
#include "mfxvideo++.h"

/*------------------------------------------------------------------------------*/

#define CHARS_IN_GUID  39

#ifndef SAFE_NEW
#define SAFE_NEW(P, C){ try { (P) = new C; } catch(...) { (P) = NULL; } }
#endif

#ifndef SAFE_NEW_ARRAY
#define SAFE_NEW_ARRAY(P, C, N){ try { (P) = new C[N]; } catch(...) { (P) = NULL; } }
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(P) {if (P) {delete (P); (P) = NULL;}}
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(P) {if (P) {delete[] (P); (P) = NULL;}}
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(P) if (P) { (P)->Release(); (P) = NULL; }
#endif

#ifndef SAFE_FREE
#define SAFE_FREE(P) if (P) { free(P); (P) = NULL; }
#endif

#define SAFE_MFT_FREE(P) if (P) { CoTaskMemFree(P); (P) = NULL; }

#define myfopen(FNAME, FMODE) _fsopen(FNAME, FMODE, _SH_DENYNO)
#define mywfopen(FNAME, FMODE) _wfsopen(FNAME, FMODE, _SH_DENYNO)

/*------------------------------------------------------------------------------*/

// Structure to store input types of MFT
typedef struct {
    GUID major_type; // major type (VIDEO, AUDIO, etc)
    GUID subtype;    // subtype
} GUID_info;

struct ClassRegData;

// Function pointer for creating COM objects. (Used by the class factory.)
typedef HRESULT (*CreateInstanceFn)(REFIID iid,
                                    void **ppObject,
                                    ClassRegData *pRegistrationData);

// Function pointer to work with codec params
typedef mfxStatus (*CodecParamsFn)(mfxVideoParam* pVideoParams);

// Function pointer to some function
typedef HRESULT (*ClassRegDataFn)(void);

// Class factory data.
// Defines a look-up table of class IDs and corresponding creation functions.
struct ClassRegData
{
    GUID*             guidClassID;          // Class ID of plug-in
    LPWSTR            pPluginName;          // friendly name
    UINT32            iPluginCategory;      // category
    CreateInstanceFn  pCreationFunction;    // create function
    CodecParamsFn     pFillParams;          // function to fill codec params
    CodecParamsFn     pFreeParams;          // function to free codec params
    GUID_info*        pInputTypes;          // array of input types
    DWORD             cInputTypes;          // size of array of input types
    GUID_info*        pOutputTypes;         // array of output types
    DWORD             cOutputTypes;         // size of array of output types
    UINT32            iFlags;               // registry flags (for MFTs only)
    IMFAttributes*    pAttributes;          // MFT attributes
    LPWSTR*           pFileExtensions;      // for ByteStreamHandler only
    UINT32            cFileExtensions;      // for ByteStreamHandler only
    ClassRegDataFn    pDllRegisterFn;       // plug-in specific register function
    ClassRegDataFn    pDllUnregisterFn;     // plug-in specific unregister function
};

// MF plug-in types
enum
{
    REG_UNKNOWN                 = 0x000,
    REG_AS_BYTE_STREAM_HANDLER  = 0x001,
    REG_AS_AUDIO_DECODER        = 0x002,
    REG_AS_VIDEO_DECODER        = 0x004,
    REG_AS_AUDIO_ENCODER        = 0x008,
    REG_AS_VIDEO_ENCODER        = 0x010,
    REG_AS_AUDIO_EFFECT         = 0x020,
    REG_AS_VIDEO_EFFECT         = 0x040,
    REG_AS_VIDEO_PROCESSOR      = 0x080,
    REG_AS_SINK                 = 0x100,
    REG_AS_WMP_PLUGIN           = 0x200
};

BOOL myDllMain(HANDLE hModule,
               DWORD  ul_reason_for_call,
               ClassRegData *pClassRegData,
               UINT32 numberClassRegData);

STDAPI AMovieSetupRegisterServer(CLSID   clsServer,
                                 LPCWSTR szDescription,
                                 LPCWSTR szFileName,
                                 LPCWSTR szThreadingModel = L"Both",
                                 LPCWSTR szServerType = L"InprocServer32");

STDAPI AMovieSetupUnregisterServer(CLSID clsServer);

/*------------------------------------------------------------------------------*/
// Plug-ins GUIDs

// {07F19984-4FC7-45ba-9AD0-418449E81283}
DEFINE_GUID(CLSID_MF_MPEG2EncFilter,
0x07f19984, 0x4fc7, 0x45ba, 0x9a, 0xd0, 0x41, 0x84, 0x49, 0xe8, 0x12, 0x83);

// {08B2F572-51BF-4e93-8B15-33864546DC9A}
DEFINE_GUID(CLSID_MF_H264EncFilter,
0x08b2f572, 0x51bf, 0x4e93, 0x8b, 0x15, 0x33, 0x86, 0x45, 0x46, 0xdc, 0x9a);

// {0CF65E82-CCFF-4e02-99EB-A579B667820D}
DEFINE_GUID(CLSID_MF_MPEG2DecFilter, 
0x0cf65e82, 0xccff, 0x4e02, 0x99, 0xeb, 0xa5, 0x79, 0xb6, 0x67, 0x82, 0xd);

// {0855C9AC-BC6F-4371-8954-671CCD4EC16F}
DEFINE_GUID(CLSID_MF_H264DecFilter, 
0x0855c9ac, 0xbc6f, 0x4371, 0x89, 0x54, 0x67, 0x1c, 0xcd, 0x4e, 0xc1, 0x6f);

// {04C76F2E-128C-4f2b-8270-F9C0B6DFAD31}
DEFINE_GUID(CLSID_MF_VC1DecFilter, 
0x04c76f2e, 0x128c, 0x4f2b, 0x82, 0x70, 0xf9, 0xc0, 0xb6, 0xdf, 0xad, 0x31);

// {0489CFBB-3D2D-4f90-BBC2-350F9DDECF77}
DEFINE_GUID(CLSID_MF_VppFilter, 
0x0489cfbb, 0x3d2d, 0x4f90, 0xbb, 0xc2, 0x35, 0x0f, 0x9d, 0xde, 0xcf, 0x77);

/*--------------------------------------------------------------------*/
// Additional Color Format GUIDs

// {CB08E88B-3961-42ae-BA67-FF47CCC13EED}
DEFINE_GUID(MFVideoFormat_NV12_MFX,
MAKEFOURCC('N','V','1','2'), 0x3961, 0x42ae, 0xba, 0x67, 0xff, 0x47, 0xcc, 0xc1, 0x3e, 0xed);

/*--------------------------------------------------------------------*/
// Additional Media Types GUIDs

DEFINE_GUID(MEDIASUBTYPE_MPEG1_MFX,
MAKEFOURCC('M','P','G','1'), 0x0000, 0x0010, 0x90, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);

DEFINE_GUID(MEDIASUBTYPE_MPEG2_MFX,
MAKEFOURCC('M','P','G','2'), 0x0000, 0x0010, 0x90, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);

DEFINE_GUID(MEDIASUBTYPE_H264_MFX,
MAKEFOURCC('H','2','6','4'), 0x0000, 0x0010, 0x90, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);

DEFINE_GUID(MEDIASUBTYPE_VC1_MFX, // pure VC1
0x59e58851, 0x02b2, 0x431c, 0xa3, 0x7f, 0xe5, 0x5d, 0x98, 0xeb, 0x09, 0x72);

DEFINE_GUID(MEDIASUBTYPE_VC1P_MFX,
0xe77f04f1, 0xd2cf, 0x42d3, 0xbd, 0x8f, 0xe7, 0xfe, 0x63, 0x19, 0xc2, 0x6a);

/*--------------------------------------------------------------------*/
// HW support GUIDs

// {26F6BB9A-EAA2-45f8-86D2-69DCD984B9B1}
DEFINE_GUID(IID_IMFDeviceDXVA, 
0x26f6bb9a, 0xeaa2, 0x45f8, 0x86, 0xd2, 0x69, 0xdc, 0xd9, 0x84, 0xb9, 0xb1);

// {85E4DCCF-F1FE-4117-854D-7CDA2ACC2C77}
// Media type attribute containing IUnknown pointer to IMFDeviceDXVA
DEFINE_GUID(MF_MT_D3D_DEVICE,
0x85e4dccf, 0xf1fe, 0x4117, 0x85, 0x4d, 0x7c, 0xda, 0x2a, 0xcc, 0x2c, 0x77);

// {80860F81-298D-4828-BB7A-323558ECB6AF}
// Media type attribute containing decoder's subtype
DEFINE_GUID(MF_MT_DEC_SUBTYPE,
0x80860f81, 0x298d, 0x4828, 0xbb, 0x7a, 0x32, 0x35, 0x58, 0xec, 0xb6, 0xaf);

// {4ACC65CB-BE19-4cd6-80BD-B28F8E112054}
// Media type attribute containing error occurred in dowsntream plug-in
DEFINE_GUID(MF_MT_DOWNSTREAM_ERROR, 
0x4acc65cb, 0xbe19, 0x4cd6, 0x80, 0xbd, 0xb2, 0x8f, 0x8e, 0x11, 0x20, 0x54);

// {7E151065-C321-4e28-A6A5-BA4C84DAA7B9}
DEFINE_GUID(MF_MT_MFX_FRAME_SRF, 
0x7e151065, 0xc321, 0x4e28, 0xa6, 0xa5, 0xba, 0x4c, 0x84, 0xda, 0xa7, 0xb9);

// {FC8875C8-8B57-479e-B89E-D4D10E174645}
DEFINE_GUID(MF_MT_FAKE_SRF, 
0xfc8875c8, 0x8b57, 0x479e, 0xb8, 0x9e, 0xd4, 0xd1, 0xe, 0x17, 0x46, 0x45);

/*--------------------------------------------------------------------*/
// Other GUIDs

// {31670B7E-6A65-4a0f-BC78-62B0AE86DDC3}
DEFINE_GUID(IID_IMfxFrameSurface, 
0x31670b7e, 0x6a65, 0x4a0f, 0xbc, 0x78, 0x62, 0xb0, 0xae, 0x86, 0xdd, 0xc3);

// {7F48BBA1-8680-4af9-83BD-DE8FBBD3B32D}
DEFINE_GUID(IID_MFVideoBuffer, 
0x7f48bba1, 0x8680, 0x4af9, 0x83, 0xbd, 0xde, 0x8f, 0xbb, 0xd3, 0xb3, 0x2d);

/*--------------------------------------------------------------------*/
// Global variables

static const GUID_info g_UncompressedVideoTypes[] =
{
    { MFMediaType_Video, MFVideoFormat_NV12_MFX },
    { MFMediaType_Video, MFVideoFormat_NV12 },
    //{ MFMediaType_Video, MFVideoFormat_YV12 },
};

static const UINT32 g_DecoderRegFlags = MFT_ENUM_FLAG_ASYNCMFT;
static const UINT32 g_EncoderRegFlags = MFT_ENUM_FLAG_ASYNCMFT;
static const UINT32 g_VppRegFlags     = MFT_ENUM_FLAG_ASYNCMFT;

extern HINSTANCE g_hInst;

/*--------------------------------------------------------------------*/

static const mfxU32 g_tabDoNotUseVppAlg[] =
{
    MFX_EXTBUFF_VPP_DENOISE,
    MFX_EXTBUFF_VPP_SCENE_ANALYSIS,
    MFX_EXTBUFF_VPP_PROCAMP,
    MFX_EXTBUFF_VPP_DETAIL
};

static const mfxExtVPPDoNotUse g_extVppDoNotUse =
{
    {
        MFX_EXTBUFF_VPP_DONOTUSE, // BufferId
        sizeof(mfxExtVPPDoNotUse) // BufferSz
    }, // Header
    sizeof(g_tabDoNotUseVppAlg)/sizeof(mfxU32), // NumAlg
    (mfxU32*)&g_tabDoNotUseVppAlg //AlgList
};

static const mfxExtBuffer* g_pVppExtBuf[] =
{
    (mfxExtBuffer*)&g_extVppDoNotUse
};

/*------------------------------------------------------------------------------*/

extern mfxVersion g_MfxVersion;

/*------------------------------------------------------------------------------*/

extern void DllAddRef(void);
extern void DllRelease(void);

/*--------------------------------------------------------------------*/
// Create functions

extern HRESULT CreateVEncPlugin(REFIID iid, void **ppObject, ClassRegData *pRegistrationData);
extern HRESULT CreateVDecPlugin(REFIID iid, void **ppObject, ClassRegData *pRegistrationData);
extern HRESULT CreateVppPlugin (REFIID iid, void **ppObject, ClassRegData *pRegistrationData);

#endif // #ifndef __MF_GUIDS_H__
