/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_utils.h

Purpose: define utilities code for plug-ins support.

Defined Classes, Structures & Enumerations:
  * MyCritSec - wraps a critical section
  * MyAutoLock - provides automatic lock/unlock of a critical section
  * MyEvent - provides events suport
  * MySemaphore - provides semaphores support
  * MyThread - provides threads support
  * MFSamplesPool - provides pool for free samples
  * MFTicker - provides functions execition time measurement

Defined Types:
  * mf_tick - timer tick type
  * my_thread_callback - threads callback function

Defined Macroses:
  # Behavior
  * MF_MFX_IMPL - defines MSFK library to use (SW, HW, AUTO)
  * MF_ENABLE_PICTURE_COMPLEXITY_LIMITS - enables limits on supported features
  * MF_NO_SW_FALLBACK - disables SW fallback for HW MSDK library
  * MF_DEFAULT_FRAMERATE_NOM - default frame rate nomenator
  * MF_DEFAULT_FRAMERATE_DEN - default frame rate denominator
  # Debugging
  * DBG_NO/DBG_YES - debug switchers
  * USE_DBG - defines whether print debug trace
  * DBG_STDOUT - defines whether to print debug trace to stdout
  * DBG_FILE_NAME - defines debug trace file name
  * DBG_TRACE - prints string to debug file
  * DBG_TRACE_1 - prints string with 1 parameter to debug file
  * DBG_TRACE_2 - prints string with 2 parameters to debug file
  # MSDK switchers
  * MF_FORCE_SW_MSDK - force SW MSDK usage
  * MF_FORCE_HW_MSDK - force HW MSDK usage
  * MF_FORCE_SW_MEMORY - force SW memory usage (not always possible)
  * MF_FORCE_HW_MEMORY - force HW memory usage
  # Other defines
  * MAX - calculates max value
  * MF_TIME_STAMP_FREQUENCY - MSDK time stamp frequency
  * MF_TIME_STAMP_INVALID - sets invalid time stamp
  * REF2MFX_TIME - converts reference to MSDK time
  * MFX2REF_TIME - converts MSDK to reference time
  * SEC2REF_TIME - converts seconds to reference time
  * REF2SEC_TIME - converts reference time to seconds
  * CHECK_POINTER - checks pointer validity
  * CHECK_EXPRESSION - checks expression
  * CHECK_RESULT - checks result
  * ARRAY_SIZE - calculates array size
  * GET_TIME - calculates time from ticks and frequency
  * IS_MEM_IN - checks whether MSDK memory is "IN"
  * IS_MEM_SYSTEM - checks whether MSDK memory is "SYSTEM"
  * IS_MEM_VIDEO - checks whether MSDK memory is "HW VIDEO"
  * MEM_IN_TO_OUT - converts MSDK in memory to out
  * MEM_OUT_TO_IN - converts MSDK out memory to in
  * MEM_ALIGN - aligns memory to specified amount of bytes

Defined Global Variables:
  * m_gFrequency - CPU frequency
  * g_dbg_file - debug variable: file to store debug trace

Defined Global Functions:
  * mf_get_tick - gets timer tick
  * mf_get_frequency - gets CPU frequency
  * mf_get_cpu_num - gets number of CPUs
  * mf_get_color_format - gets color format code from FOURCC
  * mf_get_chroma_format - gets chroma format code from FOURCC
  * mf_ms2mfx_imode - converts MS imode to MSDK one
  * mf_mfx2ms_imode - converts MSDK imode to MS one
  * mf_mftype2mfx_frame_info - converts IMFMediaType to mfxFrameInfo
  * mf_mftype2mfx_info - converts IMFMediaType to mfxInfoMFX
  * mf_align_geometry - aligns geometry with according to MFX requirements
  * mf_set_cropping - sets cropping parameters for vpp
  * mf_dump_YUV_from_NV12_data - debug function: dumps YUV data to file
  * mf_create_reg_key - creates key in the registry
  * mf_delete_reg_key - deletes key from the registry
  * mf_delete_reg_value - deletes value from the registry
  * mf_create_reg_string - sets string to registry
  * mf_get_reg_string - gets string from registry
  * mf_get_reg_dword - gets DWORD form registry

*********************************************************************************/

#ifndef __MF_UTILS_H__
#define __MF_UTILS_H__

#include "mfxdefs.h"
#include "mfxstructures.h"

#include "mf_guids.h"

/*------------------------------------------------------------------------------*/

#define MF_MFX_IMPL MFX_IMPL_AUTO
//#define MF_ENABLE_PICTURE_COMPLEXITY_LIMITS
// Define switches off SW fallback in HW plug-ins
//#define MF_NO_SW_FALBACK

/*------------------------------------------------------------------------------*/

// default framerate (if was not set by other MF component in Media Type)
#define MF_DEFAULT_FRAMERATE_NOM 25
#define MF_DEFAULT_FRAMERATE_DEN 1

/*------------------------------------------------------------------------------*/

#define DBG_NO  0
#define DBG_YES 1

#define USE_DBG DBG_NO
#define DBG_STDOUT DBG_NO

#if USE_DBG == DBG_YES
    #ifdef DBG_FILE_INIT
        #if DBG_STDOUT == DBG_YES
            FILE* g_dbg_file = stdout;
        #else
            #ifndef DBG_FILE_NAME
                #define DBG_FILE_NAME "C:\\mf_plugins.log"
            #endif
            FILE* g_dbg_file = myfopen(DBG_FILE_NAME, "a");
        #endif
    #else
        extern FILE* g_dbg_file;
    #endif
#endif

/*------------------------------------------------------------------------------*/

#ifndef DBG_MODULE_NAME
#define DBG_MODULE_NAME L"unknown"
#endif

#ifndef DBG_TRACE
#if USE_DBG == DBG_YES
    #define DBG_TRACE(MSG) \
    { \
        if (g_dbg_file) \
        { \
            fprintf(g_dbg_file, "%S: %s: %s\n", DBG_MODULE_NAME, __FUNCTION__,  MSG); \
            fflush(g_dbg_file); \
        } \
    }
#else
    #define DBG_TRACE(MSG)
#endif
#endif // #ifndef DBG_TRACE

#ifndef DBG_TRACE_1
#if USE_DBG == DBG_YES
    #define DBG_TRACE_1(MSG, P1) \
    { \
        if (g_dbg_file) \
        { \
            fprintf(g_dbg_file, "%S: %s: " MSG "\n", DBG_MODULE_NAME, __FUNCTION__, (P1)); \
            fflush(g_dbg_file); \
        } \
    }
#else
    #define DBG_TRACE_1(MSG, P1)
#endif
#endif // #ifndef DBG_TRACE_2

#ifndef DBG_TRACE_2
#if USE_DBG == DBG_YES
    #define DBG_TRACE_2(MSG, P1, P2) \
    { \
        if (g_dbg_file) \
        { \
            fprintf(g_dbg_file, "%S: %s: " MSG "\n", DBG_MODULE_NAME, __FUNCTION__, (P1), (P2)); \
            fflush(g_dbg_file); \
        } \
    }
#else
    #define DBG_TRACE_2(MSG, P1, P2)
#endif
#endif // #ifndef DBG_TRACE_2

/*------------------------------------------------------------------------------*/

#ifndef MAX
#define MAX(A, B) (((A) > (B)) ? (A) : (B))
#endif

#ifndef MIN
#define MIN(A, B) (((A) < (B)) ? (A) : (B))
#endif

#define MF_TIME_STAMP_FREQUENCY 90000
#define MF_CPU_TIME_FREQUENCY   10000000
#define MF_TIME_STAMP_INVALID   ((mfxU64)-1.0)

#ifndef REF2MFX_TIME
#define REF2MFX_TIME(REF_TIME) (-1e7 == (REF_TIME))? MF_TIME_STAMP_INVALID: (mfxU64)(((REF_TIME) / 1e7) * MF_TIME_STAMP_FREQUENCY)
#endif

#ifndef MFX2REF_TIME
#define MFX2REF_TIME(MFX_TIME) (REFERENCE_TIME)(((mfxF64)(MFX_TIME) / (mfxF64)MF_TIME_STAMP_FREQUENCY) * 1e7)
#endif

#ifndef SEC2REF_TIME
#define SEC2REF_TIME(SEC_TIME) (REFERENCE_TIME)((SEC_TIME)*1e7)
#endif

#ifndef REF2SEC_TIME
#define REF2SEC_TIME(REF_TIME) ((mfxF64)(REF_TIME)/(mfxF64)1e7)
#endif

#ifndef CHECK_POINTER
#define CHECK_POINTER(P, ERR) {if (!(P)) {DBG_TRACE("CHECK_POINTER: (" #P ") = nil: -"); return ERR;}}
#endif

#ifndef CHECK_EXPRESSION
#define CHECK_EXPRESSION(E, ERR) {if (!(E)) {DBG_TRACE("CHECK_EXPRESSION: (" #E ") != true : -"); return ERR;}}
#endif

#ifndef CHECK_RESULT
#define CHECK_RESULT(P, X, ERR) {if ((X) != (P)) {DBG_TRACE("CHECK_RESULT: (" #P ") = (" #X ") : -"); return ERR;}}
#endif

#ifndef SET_HR_ERROR
#define SET_HR_ERROR(E, STS, ERR) {if (SUCCEEDED(STS) && !(E)) {DBG_TRACE("SET_ERROR: (" #E ") != true : -"); (STS) = (ERR);}}
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(P) (sizeof(P)/sizeof(P[0]))
#endif

#ifndef GET_TIME
#define GET_TIME(T,S,F) ((mfxF64)(mfxI64)((T)-(S))/(mfxF64)(mfxI64)(F))
#endif

/*------------------------------------------------------------------------------*/

#ifndef GET_CPU_TIME
#define GET_CPU_TIME(T,S,F) GET_TIME(T,S,F)
#endif

/*------------------------------------------------------------------------------*/

#define IS_MEM_IN(M) ((MFX_IOPATTERN_IN_VIDEO_MEMORY == (M)) || (MFX_IOPATTERN_IN_SYSTEM_MEMORY == (M)))
#define IS_MEM_OUT(M) ((MFX_IOPATTERN_OUT_VIDEO_MEMORY == (M)) || (MFX_IOPATTERN_OUT_SYSTEM_MEMORY == (M)))
#define IS_MEM_SYSTEM(M) ((MFX_IOPATTERN_IN_SYSTEM_MEMORY == (M)) || (MFX_IOPATTERN_OUT_SYSTEM_MEMORY == (M)))
#define IS_MEM_VIDEO(M)  ((MFX_IOPATTERN_IN_VIDEO_MEMORY == (M)) || (MFX_IOPATTERN_OUT_VIDEO_MEMORY == (M)))
#define MEM_IN_TO_OUT(M) \
    ((IS_MEM_IN(M))? \
        ((MFX_IOPATTERN_IN_VIDEO_MEMORY == (M))? \
            MFX_IOPATTERN_OUT_VIDEO_MEMORY: \
            MFX_IOPATTERN_OUT_SYSTEM_MEMORY) \
        : \
        (M))
#define MEM_OUT_TO_IN(M) \
    ((IS_MEM_OUT(M))? \
        ((MFX_IOPATTERN_OUT_VIDEO_MEMORY == (M))? \
            MFX_IOPATTERN_IN_VIDEO_MEMORY: \
            MFX_IOPATTERN_IN_SYSTEM_MEMORY) \
        : \
        (M))

#define MEM_ALIGN(X, N) (0 == ((N) & ((N)-1)))? (((X)+(N)-1) & (~((N)-1))): (X)

/*------------------------------------------------------------------------------*/

typedef LONGLONG mf_tick;
typedef mf_tick mf_cpu_tick;

extern mf_tick m_gFrequency;

extern mf_tick mf_get_tick(void);
extern mf_tick mf_get_frequency(void);
extern mfxU32  mf_get_cpu_num(void);
extern size_t  mf_get_mem_usage(void);
extern void    mf_get_cpu_usage(mf_cpu_tick* pTimeKernel, mf_cpu_tick* pTimeUser);

extern mfxU32 mf_get_color_format (DWORD fourcc);
extern mfxU16 mf_get_chroma_format(DWORD fourcc);

extern mfxF64 mf_get_framerate(mfxU32 fr_n, mfxU32 fr_d);
extern bool mf_are_framerates_equal(mfxU32 fr1_n, mfxU32 fr1_d,
                                    mfxU32 fr2_n, mfxU32 fr2_d);

extern IDirect3DSurface9* mf_get_d3d_srf_from_mid(mfxMemId mid);

extern mfxU16 mf_ms2mfx_imode(UINT32 imode);
extern UINT32 mf_mfx2ms_imode(mfxU16 imode);

extern HRESULT mf_mftype2mfx_frame_info(IMFMediaType* pType, mfxFrameInfo* pMfxInfo);
extern HRESULT mf_mftype2mfx_info(IMFMediaType* pType, mfxInfoMFX* pMfxInfo);
extern HRESULT mf_align_geometry(mfxFrameInfo* pMfxInfo);

extern void mf_set_cropping(mfxInfoVPP* pVppInfo);

extern mfxStatus mf_copy_extbuf(mfxExtBuffer* pIn, mfxExtBuffer*& pOut);
extern mfxStatus mf_free_extbuf(mfxExtBuffer*& pBuf);

extern mfxStatus mf_copy_extparam(mfxVideoParam* pIn, mfxVideoParam* pOut);
extern mfxStatus mf_free_extparam(mfxVideoParam* pParam);

extern void mf_dump_YUV_from_NV12_data(FILE* f, mfxU8* buf, mfxFrameInfo* info, mfxU32 p);

extern HRESULT mf_create_reg_key(const TCHAR *pPath, const TCHAR* pKey);
extern HRESULT mf_delete_reg_key(const TCHAR *pPath, const TCHAR* pKey);
extern HRESULT mf_delete_reg_value(const TCHAR *pPath, const TCHAR* pName);
extern HRESULT mf_create_reg_string(const TCHAR *pPath, const TCHAR* pName,
                                    TCHAR* pValue);
extern HRESULT mf_get_reg_string(const TCHAR *pPath, const TCHAR* pName,
                                 TCHAR* pValue, UINT32 cValueMaxSize);
extern HRESULT mf_get_reg_dword(const TCHAR *pPath, const TCHAR* pName,
                                DWORD* pValue);

extern HRESULT mf_test_createdevice();

/*------------------------------------------------------------------------------*/

class MyCritSec
{
public:
    MyCritSec(void);
    ~MyCritSec(void);

    void Lock(void);
    void Unlock(void);
    int  Try(void);
private:
    CRITICAL_SECTION m_CritSec;
};

/*------------------------------------------------------------------------------*/

class MyAutoLock
{
public:
    MyAutoLock(MyCritSec& crit_sec);
    ~MyAutoLock(void);

    void Lock(void);
    void Unlock(void);
private:
    MyCritSec* m_pCritSec;
    bool       m_bLocked;
    // avoiding possible problems by defining operator= and copy constructor
    MyAutoLock(const MyAutoLock&);
    MyAutoLock& operator=(const MyAutoLock&);
};


/*------------------------------------------------------------------------------*/

class MyNamedMutex
{
public:
    MyNamedMutex(HRESULT &hr, TCHAR* name);
    ~MyNamedMutex(void);

    void Lock(void);
    void Unlock(void);
private:
    HANDLE m_mt_handle;
    // avoiding possible problems by defining operator= and copy constructor
    MyNamedMutex(const MyNamedMutex&);
    MyNamedMutex& operator=(const MyNamedMutex&);
};

/*------------------------------------------------------------------------------*/

class MyEvent
{
public:
    MyEvent(HRESULT &hr);
    ~MyEvent(void);

    void Signal(void);
    void Reset(void);
    void Wait(void);

    DWORD TimedWait(mfxU32 msec);

private:
    void* m_ev_handle;
    // avoiding possible problems by defining operator= and copy constructor
    MyEvent(const MyEvent&);
    MyEvent& operator=(const MyEvent&);
};

/*------------------------------------------------------------------------------*/

class MySemaphore
{
public:
    MySemaphore(HRESULT &hr, LONG count = 0);
    ~MySemaphore(void);

    void Post(void);
    void Wait(void);
    void Reset(void);

    DWORD TimedWait(mfxU32 msec);

private:
    void* m_sm_handle;
    // avoiding possible problems by defining operator= and copy constructor
    MySemaphore(const MySemaphore&);
    MySemaphore& operator=(const MySemaphore&);
};

/*------------------------------------------------------------------------------*/

#define MY_THREAD_CALLCONVENTION __stdcall
typedef unsigned int (MY_THREAD_CALLCONVENTION * my_thread_callback)(void*);

class MyThread
{
public:
    MyThread(HRESULT &hr, my_thread_callback func, void* arg);
    ~MyThread(void);

    void Wait(void);
private:
    void* m_th_handle;
    // avoiding possible problems by defining operator= and copy constructor
    MyThread(const MyThread&);
    MyThread& operator=(const MyThread&);
};

/*------------------------------------------------------------------------------*/

class IMFSamplesPoolCallback
{
public:
    virtual void SampleAppeared(bool bFakeSample, bool bReinitSample) = 0;
};

/*------------------------------------------------------------------------------*/

class MFSamplesPool: public IMFAsyncCallback
{
public:
    MFSamplesPool(void);
    virtual ~MFSamplesPool(void);

    // IUnknown methods
    STDMETHODIMP_(ULONG) AddRef(void);
    STDMETHODIMP_(ULONG) Release(void);
    STDMETHODIMP QueryInterface(REFIID iid, void** ppv);

    // IMFAsyncCallback methods
    STDMETHODIMP GetParameters(DWORD *pdwFlags, DWORD *pdwQueue);
    STDMETHODIMP Invoke(IMFAsyncResult *pAsyncResult);

    // MFSamplesPool methods
    HRESULT Init(mfxU32 uiSamplesSize, IMFSamplesPoolCallback* pCallback = NULL);
    void    Close(void);
    // NOTE: do not call RemoveCallback inside plug-in critical section if
    // Invoke can be called during SetCallback execution
    void RemoveCallback(void);

    HRESULT    AddSample(IMFSample* pSample);
    IMFSample* GetSample(void);

protected:
    long        m_nRefCount;     // reference count
    MyCritSec   m_CritSec;
    MyCritSec   m_CallbackCritSec;

    bool        m_bInitialized;  // flag to indicate initialization
    mfxU32      m_uiSamplesNum;  // number of free samples
    mfxU32      m_uiSamplesSize; // size of samples array
    IMFSample** m_pSamples;      // list of free samples

    // optional callback interface
    IMFSamplesPoolCallback* m_pCallback;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFSamplesPool(const MFSamplesPool&);
    MFSamplesPool& operator=(const MFSamplesPool&);
};

/*------------------------------------------------------------------------------*/

class MFTicker
{
public:
    MFTicker(mf_tick* pTime)
    {
        m_StartTick = mf_get_tick();
        if (pTime) m_pTime = pTime;
        else m_pTime = NULL;
    }
    ~MFTicker(void)
    {
        if (m_pTime) *m_pTime += mf_get_tick() - m_StartTick;
    }
protected:
    mf_tick  m_StartTick;
    mf_tick* m_pTime;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFTicker(const MFTicker&);
    MFTicker& operator=(const MFTicker&);
};

/*------------------------------------------------------------------------------*/

class MFCpuUsager
{
public:
    MFCpuUsager(mf_tick* pTotalTime, mf_cpu_tick* pKernelTime, mf_cpu_tick* pUserTime,
                mfxF64* pCpuUsage = NULL);
    ~MFCpuUsager(void);

protected:
    mf_tick      m_StartTick;
    mf_cpu_tick  m_StartKernelTime;
    mf_cpu_tick  m_StartUserTime;

    mfxF64*      m_pCpuUsage;
    mf_tick*     m_pTotalTime;
    mf_cpu_tick* m_pKernelTime;
    mf_cpu_tick* m_pUserTime;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFCpuUsager(const MFCpuUsager&);
    MFCpuUsager& operator=(const MFCpuUsager&);
};

#endif // #ifndef __MF_UTILS_H__
