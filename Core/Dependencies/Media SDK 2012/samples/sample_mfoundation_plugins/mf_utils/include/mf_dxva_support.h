/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#ifndef __MF_DXVA_SUPPORT_H__
#define __MF_DXVA_SUPPORT_H__

#include "mf_utils.h"

#include "d3d_allocator.h"

/*------------------------------------------------------------------------------*/

class MFFrameAllocator: public D3DFrameAllocator, public IUnknown
{
public:
    MFFrameAllocator(void);
    // IUnknown methods
    virtual STDMETHODIMP_(ULONG) AddRef (void);
    virtual STDMETHODIMP_(ULONG) Release(void);
    virtual STDMETHODIMP QueryInterface(REFIID iid, void** ppv);
protected:
    long m_nRefCount; // reference count
};

/*------------------------------------------------------------------------------*/

class IMfxVideoSession : public MFXVideoSession, public IUnknown
{
public:
    // IMfxVideoSession methods
    IMfxVideoSession(void);
    virtual ~IMfxVideoSession(void);

    // IUnknown methods
    virtual STDMETHODIMP_(ULONG) AddRef (void);
    virtual STDMETHODIMP_(ULONG) Release(void);
    virtual STDMETHODIMP QueryInterface(REFIID iid, void** ppv);
protected:
    long m_nRefCount; // reference count
};

/*------------------------------------------------------------------------------*/
// Interface (for encoder plug-in)
//
// Notes:
//  GetDeviceManager - returns d3d device manager allocated by plug-in
//
//  InitPlg - inits plug-in (encoder or vpp); MFX_WRN_PARTIAL_ACCELERATION status
//              means that plug-in prefers SW frames on the input
//    - pDecVideoParams [in]     - mfx decoder parameters
//    - pDecSurfacesNum [in,out] - number of surfaces required by decoder;
//                               encoder (vpp) may change (increase) this number
//
//  GetFrameAllocator - returns frames allocator created by plug-in

class IMFDeviceDXVA: public IUnknown
{
public:
    // IMFDeviceDXVA methods
    virtual mfxStatus InitPlg(IMfxVideoSession* pSession,
                              mfxVideoParam*    pVideoParams,
                              mfxU32*           pSurfacesNum) = 0;

    virtual IDirect3DDeviceManager9* GetDeviceManager (void) = 0;
    virtual MFFrameAllocator*        GetFrameAllocator(void) = 0;
    virtual void                     ReleaseFrameAllocator(void) = 0;
};

/*------------------------------------------------------------------------------*/

class MFDeviceDXVA: public IMFDeviceDXVA
{
public:
    MFDeviceDXVA(void);
    virtual ~MFDeviceDXVA(void);

    // MFDeviceDXVA methods
    HRESULT DXVASupportInit (void);
    void    DXVASupportClose(void);
    HRESULT DXVASupportInitWrapper (IMFDeviceDXVA* pDeviceDXVA);
    void    DXVASupportCloseWrapper(void);

    // IMFDeviceDXVA methods
    virtual IDirect3DDeviceManager9* GetDeviceManager(void);
    virtual MFFrameAllocator*        GetFrameAllocator(void);
    virtual void                     ReleaseFrameAllocator(void);

protected:
    bool                     m_bInitialized;
    IDirect3D9*              m_pD3D;
    IDirect3DDevice9*        m_pDevice;
    IDirect3DDeviceManager9* m_pDeviceManager;
    D3DPRESENT_PARAMETERS    m_PresentParams;
    // Objects which encoder-vpp may share
    bool                     m_bShareAllocator;
    MFFrameAllocator*        m_pFrameAllocator;
    // External DXVA Device interface
    IMFDeviceDXVA*           m_pDeviceDXVA;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFDeviceDXVA(const MFDeviceDXVA&);
    MFDeviceDXVA& operator=(const MFDeviceDXVA&);
};

/*------------------------------------------------------------------------------*/

HRESULT MFCreateMfxVideoSession(IMfxVideoSession** ppSession);

#endif // #ifndef __MF_DXVA_SUPPORT_H__
