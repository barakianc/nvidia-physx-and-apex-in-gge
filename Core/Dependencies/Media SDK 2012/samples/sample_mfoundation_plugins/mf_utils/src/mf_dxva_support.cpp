/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "mf_dxva_support.h"

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Plug-in"

/*--------------------------------------------------------------------*/

HRESULT MFCreateMfxVideoSession(IMfxVideoSession** ppSession)
{
    DBG_TRACE("+");
    IMfxVideoSession* pSession = NULL;

    CHECK_POINTER(ppSession, E_POINTER);

    SAFE_NEW(pSession, IMfxVideoSession);
    CHECK_EXPRESSION(pSession, E_OUTOFMEMORY);
    pSession->AddRef();
    *ppSession = pSession;
    DBG_TRACE("-");
    return S_OK;
}

/*--------------------------------------------------------------------*/
// MFFrameAllocator class

MFFrameAllocator::MFFrameAllocator(void):
    m_nRefCount(0)
{
    DBG_TRACE("~");
}

/*--------------------------------------------------------------------*/
// IUnknown methods

ULONG MFFrameAllocator::AddRef(void)
{
    DBG_TRACE_1("RefCount = %d", m_nRefCount+1);
    return InterlockedIncrement(&m_nRefCount);
}

ULONG MFFrameAllocator::Release(void)
{
    DBG_TRACE("+");
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    DBG_TRACE_1("RefCount = %d: -", uCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT MFFrameAllocator::QueryInterface(REFIID iid, void** ppv)
{
    DBG_TRACE("+");
    if (!ppv) return E_POINTER;
    if (iid == IID_IUnknown)
    {
        DBG_TRACE("IUnknown");
        *ppv = this;
    }
    else if (iid == IID_IDirect3DDeviceManager9)
    {
        DBG_TRACE("IDirect3DDeviceManager9");
        *ppv = m_manager;
    }
    else if (iid == IID_IDirectXVideoDecoderService)
    {
        DBG_TRACE("IDirectXVideoDecoderService");
        *ppv = m_decoderService;
    }
    else
    {
        DBG_TRACE_1("not found: %08x: -", iid.Data1);
        return E_NOINTERFACE;
    }
    AddRef();
    DBG_TRACE("S_OK");
    return S_OK;
}

/*--------------------------------------------------------------------*/
// IMfxVideoSession class

IMfxVideoSession::IMfxVideoSession(void) :
    m_nRefCount(0)
{
    DBG_TRACE("+");
    DllAddRef();
}

IMfxVideoSession::~IMfxVideoSession(void)
{
    DBG_TRACE("+");
    Close();
    DllRelease();
}

/*--------------------------------------------------------------------*/
// IUnknown methods


ULONG IMfxVideoSession::AddRef(void)
{
    return InterlockedIncrement(&m_nRefCount);
}

ULONG IMfxVideoSession::Release(void)
{
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT IMfxVideoSession::QueryInterface(REFIID iid, void** ppv)
{
    if (!ppv) return E_POINTER;
    if (iid == IID_IUnknown)
    {
        *ppv = this;
    }
    else
    {
        return E_NOINTERFACE;
    }
    AddRef();
    return S_OK;
}

/*--------------------------------------------------------------------*/
// MFDeviceDXVA class

MFDeviceDXVA::MFDeviceDXVA(void) :
    m_bInitialized(false),
    m_pD3D(NULL),
    m_pDevice(NULL),
    m_pDeviceManager(NULL),
    m_pFrameAllocator(NULL),
    m_bShareAllocator(false),
    m_pDeviceDXVA(NULL)
{
    DBG_TRACE("+");
    memset(&m_PresentParams, 0, sizeof(D3DPRESENT_PARAMETERS));
    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

MFDeviceDXVA::~MFDeviceDXVA(void)
{
    DBG_TRACE("+");
    DXVASupportClose();
    DXVASupportCloseWrapper();
    DllRelease();
    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

HRESULT MFDeviceDXVA::DXVASupportInit(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    UINT reset_token = 0;

    CHECK_POINTER(!m_pDeviceDXVA, E_FAIL);
    CHECK_EXPRESSION(!m_bInitialized, E_FAIL);

    if (SUCCEEDED(hr))
    {
        m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
        if (NULL == m_pD3D) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        POINT point = {0, 0};
        HWND hWindow = WindowFromPoint(point);

        // TODO: check this
        m_PresentParams.BackBufferFormat           = D3DFMT_X8R8G8B8;
        m_PresentParams.BackBufferCount            = 1;
        m_PresentParams.SwapEffect                 = D3DSWAPEFFECT_DISCARD;
        m_PresentParams.hDeviceWindow              = hWindow;
        m_PresentParams.Windowed                   = true;
        m_PresentParams.Flags                      = D3DPRESENTFLAG_VIDEO | D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
        m_PresentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
        m_PresentParams.PresentationInterval       = D3DPRESENT_INTERVAL_ONE;

        hr = m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWindow,
                                  D3DCREATE_SOFTWARE_VERTEXPROCESSING |
                                  D3DCREATE_FPU_PRESERVE | 
                                  D3DCREATE_MULTITHREADED,
                                  &m_PresentParams, &m_pDevice);
    }
    if (SUCCEEDED(hr)) hr = DXVA2CreateDirect3DDeviceManager9(&reset_token, &m_pDeviceManager);
    if (SUCCEEDED(hr)) hr = m_pDeviceManager->ResetDevice(m_pDevice, reset_token);
    if (SUCCEEDED(hr)) m_bInitialized = true;
    else DXVASupportClose();
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*--------------------------------------------------------------------*/

void MFDeviceDXVA::DXVASupportClose(void)
{
    DBG_TRACE("+");
    SAFE_RELEASE(m_pFrameAllocator);
    SAFE_RELEASE(m_pDeviceManager);
    SAFE_RELEASE(m_pDevice);
    SAFE_RELEASE(m_pD3D);
    memset(&m_PresentParams, 0, sizeof(D3DPRESENT_PARAMETERS));

    m_bInitialized = false;

    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

HRESULT MFDeviceDXVA::DXVASupportInitWrapper(IMFDeviceDXVA* pDeviceDXVA)
{
    CHECK_POINTER(pDeviceDXVA, E_POINTER);

    // deinitializing built-in DXVA device
    DXVASupportClose();

    SAFE_RELEASE(m_pDeviceDXVA);
    pDeviceDXVA->AddRef();
    m_pDeviceDXVA = pDeviceDXVA;

    m_pDeviceManager  = m_pDeviceDXVA->GetDeviceManager();
    m_pFrameAllocator = m_pDeviceDXVA->GetFrameAllocator();

    CHECK_POINTER(m_pDeviceManager, E_POINTER);
    CHECK_POINTER(m_pFrameAllocator, E_POINTER);

    return S_OK;
}

/*--------------------------------------------------------------------*/

void MFDeviceDXVA::DXVASupportCloseWrapper(void)
{
    SAFE_RELEASE(m_pDeviceManager);
    SAFE_RELEASE(m_pFrameAllocator);
    if (m_pDeviceDXVA) m_pDeviceDXVA->ReleaseFrameAllocator();
    SAFE_RELEASE(m_pDeviceDXVA);
}

/*--------------------------------------------------------------------*/

IDirect3DDeviceManager9* MFDeviceDXVA::GetDeviceManager(void)
{
    DBG_TRACE("+");
    IDirect3DDeviceManager9* pDeviceManager = NULL;

    if (m_pDeviceManager)
    {
        m_pDeviceManager->AddRef();
        pDeviceManager = m_pDeviceManager;
    }
    DBG_TRACE_1("m_pDeviceManager = %p: -", m_pDeviceManager);
    return m_pDeviceManager;
}

/*--------------------------------------------------------------------*/

MFFrameAllocator* MFDeviceDXVA::GetFrameAllocator(void)
{
    DBG_TRACE("+");
    MFFrameAllocator* pFrameAllocator = NULL;

    if (m_pFrameAllocator && m_bShareAllocator)
    {
        m_pFrameAllocator->AddRef();
        pFrameAllocator = m_pFrameAllocator;
        DllAddRef();
    }
    DBG_TRACE_1("m_pFrameAllocator = %p: -", m_pFrameAllocator);
    return m_pFrameAllocator;
}

/*--------------------------------------------------------------------*/

void MFDeviceDXVA::ReleaseFrameAllocator(void)
{
    DllRelease();
}
