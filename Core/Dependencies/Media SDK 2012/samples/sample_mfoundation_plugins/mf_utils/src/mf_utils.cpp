/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "mf_utils.h"

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Plug-in"

/*------------------------------------------------------------------------------*/

mf_tick m_gFrequency = mf_get_frequency();

/*------------------------------------------------------------------------------*/

MyCritSec::MyCritSec(void)
{
    InitializeCriticalSection(&m_CritSec);
}

MyCritSec::~MyCritSec(void)
{
    DeleteCriticalSection(&m_CritSec);
}

void MyCritSec::Lock(void)
{
    EnterCriticalSection(&m_CritSec);
}

void MyCritSec::Unlock(void)
{
    LeaveCriticalSection(&m_CritSec);
}

int MyCritSec::Try(void)
{
    return TryEnterCriticalSection(&m_CritSec);
}

/*------------------------------------------------------------------------------*/

MyAutoLock::MyAutoLock(MyCritSec& crit_sec)
{
    m_pCritSec = &crit_sec;
    m_bLocked  = false;
    Lock();
}

MyAutoLock::~MyAutoLock(void)
{
    Unlock();
}

void MyAutoLock::Lock(void)
{
    if (!m_bLocked)
    {
        if (!m_pCritSec->Try())
        {
            m_pCritSec->Lock();
        }
        m_bLocked = true;
    }
}

void MyAutoLock::Unlock(void)
{
    if (m_bLocked)
    {
        m_pCritSec->Unlock();
        m_bLocked = false;
    }
}

/*------------------------------------------------------------------------------*/

MyNamedMutex::MyNamedMutex(HRESULT &hr, TCHAR* name):
    m_mt_handle(NULL)
{
    hr = E_FAIL;
    m_mt_handle = CreateMutex(NULL, FALSE, name);
    if (m_mt_handle) hr = S_OK;
}

MyNamedMutex::~MyNamedMutex(void)
{
    if (m_mt_handle) CloseHandle(m_mt_handle);
}

void MyNamedMutex::Lock(void)
{
    if (m_mt_handle) WaitForSingleObject(m_mt_handle, INFINITE);
}

void MyNamedMutex::Unlock(void)
{
    if (m_mt_handle) ReleaseMutex(m_mt_handle);
}

/*------------------------------------------------------------------------------*/

MyEvent::MyEvent(HRESULT &hr):
    m_ev_handle(NULL)
{
    hr = E_FAIL;
    m_ev_handle = CreateEvent(NULL, 0, 0, NULL);
    if (m_ev_handle) hr = S_OK;
}

MyEvent::~MyEvent(void)
{
    if (m_ev_handle) CloseHandle(m_ev_handle);
}

void MyEvent::Signal(void)
{
    if (m_ev_handle) SetEvent(m_ev_handle);
}

void MyEvent::Reset(void)
{
    if (m_ev_handle) ResetEvent(m_ev_handle);
}

void MyEvent::Wait(void)
{
    if (m_ev_handle) WaitForSingleObject(m_ev_handle, INFINITE);
}

DWORD MyEvent::TimedWait(mfxU32 msec)
{
    DWORD res = ERROR_SUCCESS;

    if (m_ev_handle) res = WaitForSingleObject(m_ev_handle, msec);
    return res;
}

/*------------------------------------------------------------------------------*/

MySemaphore::MySemaphore(HRESULT &hr, LONG count):
    m_sm_handle(NULL)
{
    hr = E_FAIL;
    m_sm_handle = CreateSemaphore(NULL, count, LONG_MAX, 0);
    if (m_sm_handle) hr = S_OK;
}

MySemaphore::~MySemaphore(void)
{
    if (m_sm_handle) CloseHandle(m_sm_handle);
}

void MySemaphore::Post(void)
{
    if (m_sm_handle) ReleaseSemaphore(m_sm_handle, 1, NULL);
}

void MySemaphore::Wait(void)
{
    if (m_sm_handle) WaitForSingleObject(m_sm_handle, INFINITE);
}

DWORD MySemaphore::TimedWait(mfxU32 msec)
{
    DWORD res = ERROR_SUCCESS;

    if (m_sm_handle) res = WaitForSingleObject(m_sm_handle, msec);
    return res;
}

void MySemaphore::Reset(void)
{
    if (m_sm_handle)
    {
        while (WAIT_TIMEOUT != WaitForSingleObject(m_sm_handle, 0)) ;
    }
}

/*------------------------------------------------------------------------------*/

MyThread::MyThread(HRESULT &hr, my_thread_callback func, void* arg):
    m_th_handle(NULL)
{
    hr = E_FAIL;
    m_th_handle = (void*)_beginthreadex(NULL, 0, func, arg, 0, NULL);
    if (m_th_handle) hr = S_OK;
}

MyThread::~MyThread(void)
{
    if (m_th_handle) CloseHandle(m_th_handle);
}

void MyThread::Wait(void)
{
    if (m_th_handle) WaitForSingleObject(m_th_handle, INFINITE);
}

/*------------------------------------------------------------------------------*/

IDirect3DSurface9* mf_get_d3d_srf_from_mid(mfxMemId mid)
{
    IDirect3DSurface9* pSurface = (IDirect3DSurface9*)mid;
    if (pSurface) pSurface->AddRef();
    return pSurface;
}

/*------------------------------------------------------------------------------*/
// CSamplesPool methods

MFSamplesPool::MFSamplesPool(void):
    m_nRefCount(0),
    m_bInitialized(false),
    m_uiSamplesNum(0),
    m_uiSamplesSize(0),
    m_pSamples(NULL),
    m_pCallback(NULL)
{
    DBG_TRACE("+");
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFSamplesPool::~MFSamplesPool(void)
{
    DBG_TRACE("+");
    Close();
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// IUnknown methods

ULONG MFSamplesPool::AddRef(void)
{
    DBG_TRACE_1("RefCount = %d", m_nRefCount+1);
    return InterlockedIncrement(&m_nRefCount);
}

ULONG MFSamplesPool::Release(void)
{
    DBG_TRACE("+");
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    DBG_TRACE_1("RefCount = %d: -", uCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT MFSamplesPool::QueryInterface(REFIID iid, void** ppv)
{
    DBG_TRACE("+");
    if (!ppv) return E_POINTER;
    if ((iid == IID_IUnknown) || (iid == IID_IMFAsyncCallback))
    {
        *ppv = static_cast<IMFAsyncCallback*>(this);
    }
    else
    {
        return E_NOINTERFACE;
    }
    AddRef();
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFSamplesPool::Init(mfxU32 uiSamplesSize,
                            IMFSamplesPoolCallback* pCallback)
{
    DBG_TRACE("+");
    CHECK_EXPRESSION(!m_bInitialized, E_FAIL);
    CHECK_EXPRESSION(uiSamplesSize, E_FAIL);

    m_pSamples = (IMFSample**)calloc(uiSamplesSize,sizeof(IMFSample*));
    CHECK_POINTER(m_pSamples, E_OUTOFMEMORY);

    m_uiSamplesSize = uiSamplesSize;
    m_pCallback = pCallback;

    m_bInitialized = true;

    DBG_TRACE_2("m_uiSamplesNum = %d, m_uiSamplesSize = %d", m_uiSamplesNum, m_uiSamplesSize);
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

void MFSamplesPool::Close(void)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    mfxU32 i = 0;

    if (!m_bInitialized) return;
    for (i = 0; i < m_uiSamplesNum; ++i)
    {
        SAFE_RELEASE(m_pSamples[i]);
    }
    m_uiSamplesNum = 0;
    SAFE_FREE(m_pSamples);
    m_uiSamplesSize = 0;
    m_pCallback = NULL;
    m_bInitialized = false;
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

void MFSamplesPool::RemoveCallback(void)
{
    MyAutoLock callback_lock(m_CallbackCritSec);

    m_pCallback = NULL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFSamplesPool::AddSample(IMFSample* pSample)
{
    DBG_TRACE_1("pSample = %p: +", pSample);
    HRESULT hr = S_OK;
    IMFTrackedSample* pTSample = NULL;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    CHECK_POINTER(pSample, E_POINTER);

    hr = pSample->QueryInterface(IID_IMFTrackedSample, (void**)&pTSample);
    if (SUCCEEDED(hr)) hr = pTSample->SetAllocator(this, NULL);
    SAFE_RELEASE(pTSample);
    DBG_TRACE_2("m_uiSamplesNum = %d, m_uiSamplesSize = %d", m_uiSamplesNum, m_uiSamplesSize);
    DBG_TRACE("-");
    return hr;
}

/*------------------------------------------------------------------------------*/

IMFSample* MFSamplesPool::GetSample(void)
{
    MyAutoLock lock(m_CritSec);
    DBG_TRACE("+");
    IMFSample* pSample = NULL;

    CHECK_EXPRESSION(m_bInitialized, NULL);
    CHECK_EXPRESSION(m_uiSamplesNum, NULL);
    --m_uiSamplesNum;
    pSample = m_pSamples[m_uiSamplesNum];
    m_pSamples[m_uiSamplesNum] = NULL;
    DBG_TRACE_2("m_uiSamplesNum = %d, m_uiSamplesSize = %d", m_uiSamplesNum, m_uiSamplesSize);
    DBG_TRACE_1("pSample = %p: -", pSample);
    return pSample;
}

/*------------------------------------------------------------------------------*/

HRESULT MFSamplesPool::GetParameters(DWORD* /*pdwFlags*/, DWORD* /*pdwQueue*/)
{
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFSamplesPool::Invoke(IMFAsyncResult *pAsyncResult)
{
    MyAutoLock lock(m_CritSec);
    MyAutoLock callback_lock(m_CallbackCritSec);
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    IMFSample*  pSample  = NULL;
    IMFSample** pSamples = NULL;
    IUnknown*   pUnk = NULL;
    IMFSamplesPoolCallback* pCallback = m_pCallback;
    bool bReinitSample = false;

    CHECK_EXPRESSION(m_bInitialized, S_OK);
    CHECK_POINTER(pAsyncResult, E_POINTER);

    hr = pAsyncResult->GetObject(&pUnk);
    if (SUCCEEDED(hr)) hr = pUnk->QueryInterface(IID_IMFSample, (void**)&pSample);
    if (SUCCEEDED(hr) && pSample)
    {
        UINT32 bFakeSample = FALSE;

        if (FAILED(pSample->GetUINT32(MF_MT_FAKE_SRF, &bFakeSample))) bFakeSample = FALSE;
        bReinitSample = (TRUE == bFakeSample)? true: false;

        if (m_uiSamplesSize == m_uiSamplesNum)
        {
            pSamples = (IMFSample**)realloc(m_pSamples, (m_uiSamplesSize+1)*sizeof(IMFSample*));
            if (!pSamples) hr = E_OUTOFMEMORY;
            else
            {
                m_pSamples = pSamples;
                m_pSamples[m_uiSamplesSize] = NULL;
                ++m_uiSamplesSize;
            }
        }
        if (SUCCEEDED(hr))
        {
            pSample->AddRef();
            m_pSamples[m_uiSamplesNum] = pSample;
            ++m_uiSamplesNum;
        }
        DBG_TRACE_2("m_uiSamplesNum = %d, m_uiSamplesSize = %d", m_uiSamplesNum, m_uiSamplesSize);
    }
    SAFE_RELEASE(pUnk);
    SAFE_RELEASE(pSample);

    lock.Unlock();
    if (SUCCEEDED(hr) && pCallback) pCallback->SampleAppeared(false, bReinitSample);

    DBG_TRACE("-");
    return hr;
}

/*------------------------------------------------------------------------------*/
// MFCpuUsager

MFCpuUsager::MFCpuUsager(mf_tick*     pTotalTime,
                         mf_cpu_tick* pKernelTime,
                         mf_cpu_tick* pUserTime,
                         mfxF64*      pCpuUsage)
{
    m_StartTick = mf_get_tick();
    mf_get_cpu_usage(&m_StartKernelTime, &m_StartUserTime);

    if (pCpuUsage) m_pCpuUsage = pCpuUsage;
    else m_pCpuUsage = NULL;
    if (pTotalTime) m_pTotalTime = pTotalTime;
    else m_pTotalTime = NULL;
    if (pKernelTime) m_pKernelTime = pKernelTime;
    else m_pKernelTime = NULL;
    if (pUserTime) m_pUserTime = pUserTime;
    else m_pUserTime = NULL;
}

/*------------------------------------------------------------------------------*/

MFCpuUsager::~MFCpuUsager(void)
{
    mf_tick total_time;
    mf_cpu_tick kernel_time_cur, user_time_cur;
    mf_cpu_tick kernel_time, user_time;

    total_time = mf_get_tick() - m_StartTick;
    mf_get_cpu_usage(&kernel_time_cur, &user_time_cur);

    kernel_time = kernel_time_cur - m_StartKernelTime;
    user_time   = user_time_cur - m_StartUserTime;
    if (m_pTotalTime)  *m_pTotalTime  += total_time;
    if (m_pKernelTime) *m_pKernelTime += kernel_time;
    if (m_pUserTime)   *m_pUserTime   += user_time;
    // Cpu usage is calculated for the current period
    if (m_pCpuUsage) *m_pCpuUsage = 100.0*GET_CPU_TIME(kernel_time+user_time,0,MF_CPU_TIME_FREQUENCY) /
                                          (mf_get_cpu_num()*GET_TIME(total_time,0,m_gFrequency));
}

/*------------------------------------------------------------------------------*/

mfxF64 mf_get_framerate(mfxU32 fr_n, mfxU32 fr_d)
{
    if (fr_n && fr_d) return (mfxF64)fr_n / (mfxF64)fr_d;
    return 0.0;
}

/*------------------------------------------------------------------------------*/

void FramerateNorm(mfxU32& fr_n, mfxU32& fr_d)
{
    if (!fr_n || !fr_d) return;

    mfxF64 f_fr = (mfxF64)fr_n / (mfxF64)fr_d;
    mfxU32 i_fr = (mfxU32)(f_fr + .5);

    if (fabs(i_fr - f_fr) < 0.0001)
    {
        fr_n = i_fr;
        fr_d = 1;
        return;
    }

    i_fr = (mfxU32)(f_fr * 1.001 + .5);
    if (fabs(i_fr * 1000 - f_fr * 1001) < 10)\
    {
        fr_n = i_fr * 1000;
        fr_d = 1001;
        return;
    }

    fr_n = (mfxU32)(f_fr * 10000 + .5);
    fr_d = 10000;
    return;
}

/*------------------------------------------------------------------------------*/

bool mf_are_framerates_equal(mfxU32 fr1_n, mfxU32 fr1_d,
                             mfxU32 fr2_n, mfxU32 fr2_d)
{
    FramerateNorm(fr1_n, fr1_d);
    FramerateNorm(fr2_n, fr2_d);
    return (fr1_n == fr2_n) && (fr1_d == fr2_d);
}

/*------------------------------------------------------------------------------*/

static const struct
{
    mfxU32 color_format;
    GUID   guid;
    mfxU32 fourcc;
    mfxU16 chroma_format;
}
color_formats[] =
{
    { MFX_FOURCC_NV12, MEDIASUBTYPE_NV12, MAKEFOURCC('N','V','1','2'), MFX_CHROMAFORMAT_YUV420 },
    { MFX_FOURCC_YV12, MEDIASUBTYPE_YV12, MAKEFOURCC('Y','V','1','2'), MFX_CHROMAFORMAT_YUV420 },
    { MFX_FOURCC_YUY2, MEDIASUBTYPE_YUY2, MAKEFOURCC('Y','U','Y','2'), MFX_CHROMAFORMAT_YUV422 },
};

mfxU32 mf_get_color_format(DWORD fourcc)
{
    int i;
    for (i = 0; i < ARRAY_SIZE(color_formats); i++)
    {
        if (color_formats[i].fourcc == fourcc) return color_formats[i].color_format;
    }
    return 0;
}

mfxU16 mf_get_chroma_format(DWORD fourcc)
{
    int i;
    for (i = 0; i < ARRAY_SIZE(color_formats); i++)
    {
        if (color_formats[i].fourcc == fourcc) return color_formats[i].chroma_format;
    }
    return 0;
}

/*------------------------------------------------------------------------------*/

mfxU16 mf_ms2mfx_imode(UINT32 imode)
{
    mfxU16 mfx_imode = MFX_PICSTRUCT_UNKNOWN;
    switch(imode)
    {
    case MFVideoInterlace_Progressive:
    case MFVideoInterlace_FieldSingleUpper: // treating as progressive
    case MFVideoInterlace_FieldSingleLower: // treating as progressive
        mfx_imode = MFX_PICSTRUCT_PROGRESSIVE;
        break;
    case MFVideoInterlace_FieldInterleavedUpperFirst:
        mfx_imode = MFX_PICSTRUCT_FIELD_TFF;
        break;
    case MFVideoInterlace_FieldInterleavedLowerFirst:
        mfx_imode = MFX_PICSTRUCT_FIELD_BFF;
        break;
    case MFVideoInterlace_MixedInterlaceOrProgressive:
        mfx_imode = MFX_PICSTRUCT_UNKNOWN;
        break;
    }
    return mfx_imode;
}

/*------------------------------------------------------------------------------*/

UINT32 mf_mfx2ms_imode(mfxU16 imode)
{
    mfxU16 ms_imode = MFVideoInterlace_Unknown;
    switch(imode)
    {
    case MFX_PICSTRUCT_PROGRESSIVE:
        ms_imode = MFVideoInterlace_Progressive;
        break;
    case MFX_PICSTRUCT_FIELD_TFF:
        ms_imode = MFVideoInterlace_FieldInterleavedUpperFirst;
        break;
    case MFX_PICSTRUCT_FIELD_BFF:
        ms_imode = MFVideoInterlace_FieldInterleavedLowerFirst;
        break;
    case MFX_PICSTRUCT_UNKNOWN:
        ms_imode = MFVideoInterlace_MixedInterlaceOrProgressive;
        break;
    }
    return ms_imode;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_mftype2mfx_frame_info(IMFMediaType* pType, mfxFrameInfo* pMfxInfo)
{
    HRESULT hr = S_OK;
    UINT32 width = 0, height = 0;
    UINT32 framerate_nom = MF_DEFAULT_FRAMERATE_NOM;
    UINT32 framerate_den = MF_DEFAULT_FRAMERATE_DEN;
    UINT32 imode = 0;
    UINT32 aratio_nom = 0, aratio_den = 0;
    MFVideoArea area;
    GUID subtype = GUID_NULL;

    // checking errors
    CHECK_POINTER(pType, E_POINTER);
    CHECK_POINTER(pMfxInfo, E_POINTER);

    memset(&area, 0, sizeof(MFVideoArea));

    if (SUCCEEDED(hr) && SUCCEEDED(pType->GetGUID(MF_MT_SUBTYPE, &subtype)))
    {
        pMfxInfo->FourCC       = mf_get_color_format (subtype.Data1);
        pMfxInfo->ChromaFormat = mf_get_chroma_format(subtype.Data1);
    }
    if (SUCCEEDED(hr) && SUCCEEDED(pType->GetUINT32(MF_MT_INTERLACE_MODE, &imode)))
    { // this parameter may absent on the given type
        pMfxInfo->PicStruct = mf_ms2mfx_imode(imode);
    }
    if (SUCCEEDED(hr) && SUCCEEDED(hr = MFGetAttributeSize(pType, MF_MT_FRAME_SIZE, &width, &height)))
    { // this parameter should present on the given type
        pMfxInfo->Width  = (mfxU16)width;
        pMfxInfo->Height = (mfxU16)height;
        pMfxInfo->CropX  = 0;
        pMfxInfo->CropY  = 0;
        pMfxInfo->CropW  = (mfxU16)width;
        pMfxInfo->CropH  = (mfxU16)height;
    }
    if (SUCCEEDED(hr) && SUCCEEDED(pType->GetBlob(MF_MT_MINIMUM_DISPLAY_APERTURE, (UINT8*)&area, sizeof(MFVideoArea), NULL)))
    { // this parameter may absent on the given type
        pMfxInfo->CropX = (mfxU16)area.OffsetX.value;
        pMfxInfo->CropY = (mfxU16)area.OffsetY.value;
        pMfxInfo->CropW = (mfxU16)area.Area.cx;
        pMfxInfo->CropH = (mfxU16)area.Area.cy;
    }
    if (SUCCEEDED(hr))
    {
        if (FAILED(MFGetAttributeRatio(pType, MF_MT_FRAME_RATE, &framerate_nom, &framerate_den)) ||
            !framerate_nom || !framerate_den)
        { // if parameter is absent it will be corrected
            framerate_nom = MF_DEFAULT_FRAMERATE_NOM;
            framerate_den = MF_DEFAULT_FRAMERATE_DEN;
            // correcting this parameter on the given type
            hr = MFSetAttributeRatio(pType, MF_MT_FRAME_RATE, framerate_nom, framerate_den);
        }
        if (SUCCEEDED(hr))
        {
            pMfxInfo->FrameRateExtN = framerate_nom;
            pMfxInfo->FrameRateExtD = framerate_den;
        }
    }
    if (SUCCEEDED(hr) &&
        SUCCEEDED(MFGetAttributeRatio(pType, MF_MT_PIXEL_ASPECT_RATIO, &aratio_nom, &aratio_den)) &&
        aratio_nom && aratio_den)
    { // this parameter may absent on the given type
        pMfxInfo->AspectRatioW = (mfxU16)aratio_nom;
        pMfxInfo->AspectRatioH = (mfxU16)aratio_den;
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_mftype2mfx_info(IMFMediaType* pType, mfxInfoMFX* pMfxInfo)
{
    HRESULT hr = S_OK;
    UINT32 bitrate = 0;

    // checking errors
    CHECK_POINTER(pType, E_POINTER);
    CHECK_POINTER(pMfxInfo, E_POINTER);

    hr = mf_mftype2mfx_frame_info(pType, &(pMfxInfo->FrameInfo));
    if (SUCCEEDED(hr) && SUCCEEDED(pType->GetUINT32(MF_MT_AVG_BITRATE, &bitrate)) && bitrate)
    {
        //max possible value is max(mfxU16),
        //need to use max(mfxU16) for all bitrates more then max(mfxU16)
        pMfxInfo->TargetKbps = (mfxU16) MIN(_UI16_MAX, ceil((mfxF32)bitrate/1000));
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_align_geometry(mfxFrameInfo* pMfxInfo)
{
    mfxU16 width = 0, height = 0;

    CHECK_POINTER(pMfxInfo, E_POINTER);

    width  = pMfxInfo->Width;
    height = pMfxInfo->Height;
    // making MFX geometry corrections
    pMfxInfo->Width  = (mfxU16)(((width+15)>>4)<<4);
    pMfxInfo->Height = (MFX_PICSTRUCT_PROGRESSIVE == pMfxInfo->PicStruct)?
                           (mfxU16)(((height + 15)>>4)<<4) :
                           (mfxU16)(((height + 31)>>5)<<5);

    return S_OK;
}

/*------------------------------------------------------------------------------*/

void mf_set_cropping(mfxInfoVPP* pVppInfo)
{
    if (!pVppInfo) return;

    if (pVppInfo->In.AspectRatioW &&
        pVppInfo->In.AspectRatioH &&
        pVppInfo->In.CropW &&
        pVppInfo->In.CropH &&
        pVppInfo->Out.AspectRatioW &&
        pVppInfo->Out.AspectRatioH &&
        pVppInfo->Out.CropW &&
        pVppInfo->Out.CropH)
    {
        mfxF64 dFrameAR         = ((mfxF64)pVppInfo->In.AspectRatioW * pVppInfo->In.CropW) /
                                   (mfxF64)pVppInfo->In.AspectRatioH /
                                   (mfxF64)pVppInfo->In.CropH;

        mfxF64 dPixelAR         = pVppInfo->Out.AspectRatioW / (mfxF64)pVppInfo->Out.AspectRatioH;

        mfxU16 dProportionalH   = (mfxU16)(pVppInfo->Out.CropW * dPixelAR / dFrameAR + 1) & -2; //round to closest odd (values are always positive)

        if (dProportionalH < pVppInfo->Out.CropH)
        {
            pVppInfo->Out.CropY = (mfxU16)((pVppInfo->Out.CropH - dProportionalH) / 2. + 1) & -2;
            pVppInfo->Out.CropH = pVppInfo->Out.CropH - 2 * pVppInfo->Out.CropY;
        } 
        else if (dProportionalH > pVppInfo->Out.CropH)
        {
            mfxU16 dProportionalW = (mfxU16)(pVppInfo->Out.CropH * dFrameAR / dPixelAR + 1) & -2;

            pVppInfo->Out.CropX = (mfxU16)((pVppInfo->Out.CropW - dProportionalW) / 2 + 1) & -2;
            pVppInfo->Out.CropW = pVppInfo->Out.CropW - 2 * pVppInfo->Out.CropX;
        }
    }
}

/*------------------------------------------------------------------------------*/

mfxStatus mf_copy_extbuf(mfxExtBuffer* pIn, mfxExtBuffer*& pOut)
{
    mfxStatus sts = MFX_ERR_NONE;

    CHECK_POINTER(pIn, MFX_ERR_NULL_PTR);
    if (MFX_EXTBUFF_VPP_DONOTUSE == pIn->BufferId)
    {
        mfxExtVPPDoNotUse* pInVppExtBuf = (mfxExtVPPDoNotUse*)pIn;
        mfxExtVPPDoNotUse* pVppExtBuf = (mfxExtVPPDoNotUse*)calloc(1, sizeof(mfxExtVPPDoNotUse));
        mfxU32 num_alg = pInVppExtBuf->NumAlg;

        if (pVppExtBuf)
        {
            pVppExtBuf->Header.BufferId = MFX_EXTBUFF_VPP_DONOTUSE;
            pVppExtBuf->Header.BufferSz = sizeof(mfxExtVPPDoNotUse);
            pVppExtBuf->NumAlg  = num_alg;
            pVppExtBuf->AlgList = (mfxU32*)malloc(num_alg * sizeof(mfxU32));
            if (pVppExtBuf->AlgList)
            {
                memcpy(pVppExtBuf->AlgList, pInVppExtBuf->AlgList, num_alg * sizeof(mfxU32));
            }
            else sts = MFX_ERR_MEMORY_ALLOC;
        }
        else sts = MFX_ERR_MEMORY_ALLOC;
        if (MFX_ERR_NONE == sts) pOut = (mfxExtBuffer*)pVppExtBuf;
    }
    else if (sizeof(mfxExtBuffer) == pIn->BufferSz)
    {
        pOut = (mfxExtBuffer*)calloc(1, sizeof(mfxExtBuffer));
        if (pOut)
        {
            memcpy(pOut, pIn, sizeof(mfxExtBuffer));
        }
        else sts = MFX_ERR_MEMORY_ALLOC;
    }
    else sts = MFX_ERR_UNSUPPORTED;
    return sts;
}

/*------------------------------------------------------------------------------*/

mfxStatus mf_free_extbuf(mfxExtBuffer*& pBuf)
{
    mfxStatus sts = MFX_ERR_NONE;

    CHECK_POINTER(pBuf, MFX_ERR_NONE);
    if (MFX_EXTBUFF_VPP_DONOTUSE == pBuf->BufferId)
    {
        mfxExtVPPDoNotUse* pVppBuf = (mfxExtVPPDoNotUse*)pBuf;

        SAFE_FREE(pVppBuf->AlgList);
        SAFE_FREE(pBuf);
    }
    else if (sizeof(mfxExtBuffer) == pBuf->BufferSz)
    {
        SAFE_FREE(pBuf);
    }
    else sts = MFX_ERR_UNSUPPORTED;
    return sts;
}

/*------------------------------------------------------------------------------*/

mfxStatus mf_copy_extparam(mfxVideoParam* pIn, mfxVideoParam* pOut)
{
    mfxStatus sts = MFX_ERR_NONE;
    mfxU32 i = 0;

    CHECK_POINTER(pIn,  MFX_ERR_NONE);
    CHECK_POINTER(pOut, MFX_ERR_NONE);
    if (pIn->NumExtParam)
    {
        pOut->NumExtParam = pIn->NumExtParam;
        pOut->ExtParam = (mfxExtBuffer**)calloc(pIn->NumExtParam, sizeof(mfxExtBuffer*));
        if (pOut->ExtParam)
        {
            for (i = 0; i < pIn->NumExtParam; ++i)
            {
                sts = mf_copy_extbuf(pIn->ExtParam[i], pOut->ExtParam[i]);
                if (MFX_ERR_NONE != sts) break;
            }
        }
        else sts = MFX_ERR_MEMORY_ALLOC;
    }
    return sts;
}

/*------------------------------------------------------------------------------*/

mfxStatus mf_free_extparam(mfxVideoParam* pParam)
{
    mfxStatus sts = MFX_ERR_NONE, res = MFX_ERR_NONE;
    mfxU32 i = 0;

    CHECK_POINTER(pParam, MFX_ERR_NONE);
    if (pParam->NumExtParam)
    {
        for (i = 0; i < pParam->NumExtParam; ++i)
        {
            res = mf_free_extbuf(pParam->ExtParam[i]);
            if ((MFX_ERR_NONE == sts) && (MFX_ERR_NONE != res)) sts = res;
        }
        SAFE_FREE(pParam->ExtParam);
        pParam->NumExtParam = 0;
    }
    return sts;
}

/*------------------------------------------------------------------------------*/

void mf_dump_YUV_from_NV12_data(FILE* f,
                                mfxU8* buf,
                                mfxFrameInfo* info,
                                mfxU32 p)
{
    DBG_TRACE("+");
    mfxU32 i = 0, j = 0;
    mfxU32 w = 0, h = 0, cx = 0, cy = 0, cw = 0, ch = 0;

    if (f && buf && info)
    {
        w = info->Width;
        h = info->Height;
        cx = info->CropX;
        cy = info->CropY;
        cw = info->CropW;
        ch = info->CropH;
        // dumping Y
        for (i = 0; i < ch; ++i)
        {
            fwrite(buf + cx + (cy + i)*p, 1, cw, f);
            fflush(f);
        }
        buf += p*h + cx + cy*p/2;
        // dumping U
        for (i = 0; i < ch/2; ++i)
        for (j = 0; j < cw/2; ++j)
        {
            fwrite(buf + p*i + 2*j, 1, 1, f);
        }
        fflush(f);
        buf += 1;
        // dumping V
        for (i = 0; i < ch/2; ++i)
        for (j = 0; j < cw/2; ++j)
        {
            fwrite(buf + p*i + 2*j, 1, 1, f);
        }
        fflush(f);
    }
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

mf_tick mf_get_frequency(void)
{
    LARGE_INTEGER t;

    QueryPerformanceFrequency(&t);
    return t.QuadPart;
}

/*------------------------------------------------------------------------------*/

mf_tick mf_get_tick(void)
{
    LARGE_INTEGER t;

    QueryPerformanceCounter(&t);
    return t.QuadPart;
}

/*------------------------------------------------------------------------------*/

mfxU32 mf_get_cpu_num(void)
{
    SYSTEM_INFO s;

    memset(&s, 0, sizeof(SYSTEM_INFO));
    GetSystemInfo(&s);
    DBG_TRACE_1("s.dwNumberOfProcessors = %d", s.dwNumberOfProcessors);
    return s.dwNumberOfProcessors;
}

/*------------------------------------------------------------------------------*/

size_t mf_get_mem_usage(void)
{
    PROCESS_MEMORY_COUNTERS m;

    memset(&m, 0, sizeof(PROCESS_MEMORY_COUNTERS));
    GetProcessMemoryInfo(GetCurrentProcess(), &m, sizeof(PROCESS_MEMORY_COUNTERS));
    return m.WorkingSetSize;
}

/*------------------------------------------------------------------------------*/

void mf_get_cpu_usage(mf_cpu_tick* pTimeKernel, mf_cpu_tick* pTimeUser)
{
    HANDLE process = GetCurrentProcess();
    FILETIME ftime_start, ftime_exit, ftime_kernel, ftime_user;
    mf_cpu_tick time_kernel, time_user;

    if (GetProcessTimes(process, &ftime_start, &ftime_exit, &ftime_kernel, &ftime_user))
    {
        time_kernel = (mf_cpu_tick)((((ULONGLONG) ftime_kernel.dwHighDateTime) << 32) + ftime_kernel.dwLowDateTime);
        time_user   = (mf_cpu_tick)((((ULONGLONG) ftime_user.dwHighDateTime) << 32) + ftime_user.dwLowDateTime);

        if (pTimeKernel) *pTimeKernel = time_kernel;
        if (pTimeUser)   *pTimeUser   = time_user;
    }
    CloseHandle(process);
    return;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_create_reg_key(const TCHAR *pPath, const TCHAR* pKey)
{
    HRESULT hr = S_OK;
    HKEY key = NULL, subkey = NULL;
    LSTATUS res = ERROR_SUCCESS;

    if (SUCCEEDED(hr))
    {
        res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pPath, 0, KEY_CREATE_SUB_KEY, &key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        LSTATUS res = RegCreateKeyEx(key, pKey,      // [in] parent key, name of subkey
            0, NULL,                                 // [in] reserved, class string (can be NULL)
            REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, // [in] options, access rights
            NULL,                                    // [in] security attributes
            &subkey,                                 // [out] handled of opened/created key
            NULL);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
        else RegCloseKey(subkey);
        RegCloseKey(key);
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_delete_reg_key(const TCHAR *pPath, const TCHAR* pKey)
{
    HRESULT hr = S_OK;
    HKEY key = NULL;
    LSTATUS res = ERROR_SUCCESS;

    if (SUCCEEDED(hr))
    {
        res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pPath, 0, KEY_READ, &key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        LSTATUS res = RegDeleteKey(key, pKey);
        if (ERROR_FILE_NOT_FOUND == res) hr = ERROR_FILE_NOT_FOUND;
        else if (ERROR_SUCCESS != res) hr = E_FAIL;
        RegCloseKey(key);
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_delete_reg_value(const TCHAR *pPath, const TCHAR* pName)
{
    HRESULT hr = S_OK;
    HKEY key = NULL;
    LSTATUS res = ERROR_SUCCESS;

    if (SUCCEEDED(hr))
    {
        res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pPath, 0, KEY_SET_VALUE, &key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        res = RegDeleteValue(key, pName);
        RegCloseKey(key);
        if (ERROR_FILE_NOT_FOUND == res) hr = ERROR_FILE_NOT_FOUND;
        else if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_create_reg_string(const TCHAR *pPath, const TCHAR* pName, TCHAR* pValue)
{
    HRESULT hr = S_OK;
    HKEY key = NULL;
    LSTATUS res = ERROR_SUCCESS;

    if (SUCCEEDED(hr))
    {
        res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pPath, 0, KEY_SET_VALUE, &key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        res = RegSetValueEx(key, pName, 0, REG_SZ, (BYTE*)pValue, (DWORD)(_tcslen(pValue)+1)*sizeof(TCHAR));
        RegCloseKey(key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_get_reg_string(const TCHAR *pPath, const TCHAR* pName,
                          TCHAR* pValue, UINT32 cValueMaxSize)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    HKEY key = NULL;
    LSTATUS res = ERROR_SUCCESS;
    DWORD size = cValueMaxSize*sizeof(TCHAR), type = 0;

    if (SUCCEEDED(hr))
    {
        res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pPath, 0, KEY_QUERY_VALUE, &key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        res = RegQueryValueEx(key, pName, NULL, &type, (LPBYTE)pValue, &size);
        RegCloseKey(key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr) && (REG_SZ != type)) hr = E_FAIL;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_get_reg_dword(const TCHAR *pPath, const TCHAR* pName, DWORD* pValue)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    HKEY key = NULL;
    LSTATUS res = ERROR_SUCCESS;
    DWORD size = sizeof(DWORD), type = 0;

    if (SUCCEEDED(hr))
    {
        res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pPath, 0, KEY_QUERY_VALUE, &key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr))
    {
        res = RegQueryValueEx(key, pName, NULL, &type, (LPBYTE)pValue, &size);
        RegCloseKey(key);
        if (ERROR_SUCCESS != res) hr = E_FAIL;
    }
    if (SUCCEEDED(hr) && (REG_DWORD != type)) hr = E_FAIL;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT mf_test_createdevice(void)
{
    HRESULT hr = E_FAIL;
    IDirect3D9* pD3D = Direct3DCreate9(D3D_SDK_VERSION);
    
    if (pD3D)
    {
        D3DPRESENT_PARAMETERS params;
        IDirect3DDevice9* pDevice = NULL;
        POINT point = {0, 0};
        HWND  hWindow = 0;
        
        hWindow = WindowFromPoint(point);

        memset(&params, 0, sizeof(D3DPRESENT_PARAMETERS));

        params.BackBufferFormat           = D3DFMT_X8R8G8B8;
        params.BackBufferCount            = 1;
        params.SwapEffect                 = D3DSWAPEFFECT_DISCARD;
        params.hDeviceWindow              = hWindow;
        params.Windowed                   = true;
        params.Flags                      = D3DPRESENTFLAG_VIDEO |
                                            D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
        params.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
        params.PresentationInterval       = D3DPRESENT_INTERVAL_ONE;

        hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT,
                                D3DDEVTYPE_HAL,
                                hWindow,
                                D3DCREATE_SOFTWARE_VERTEXPROCESSING |
                                D3DCREATE_FPU_PRESERVE |
                                D3DCREATE_MULTITHREADED,
                                &params,
                                &pDevice);

        SAFE_RELEASE(pDevice);
        SAFE_RELEASE(pD3D);
    }

    return hr;
};
