/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "mf_utils.h"
#include "mf_codec_params.h"

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Plug-in"

/*------------------------------------------------------------------------------*/

typedef struct _ValueItem
{
    mfxU32 ms_value;
    mfxU32 mfx_value;
} ValueItem;

const ValueItem ProfilesTbl[] =
{
    { eAVEncMPVProfile_unknown,     MFX_PROFILE_UNKNOWN },
    { eAVEncH264VProfile_unknown,   MFX_PROFILE_UNKNOWN },
    // MP profiles
    { eAVEncMPVProfile_Simple,      MFX_PROFILE_MPEG2_SIMPLE },
    { eAVEncMPVProfile_Main,        MFX_PROFILE_MPEG2_MAIN },
    { eAVEncMPVProfile_High,        MFX_PROFILE_MPEG2_HIGH },
    { eAVEncMPVProfile_422,         MFX_PROFILE_UNKNOWN },
    // H.264 profiles
    { eAVEncH264VProfile_Simple,    MFX_PROFILE_AVC_BASELINE }, // currently simple=base in MS
    { eAVEncH264VProfile_Base,      MFX_PROFILE_AVC_BASELINE},
    { eAVEncH264VProfile_Main,      MFX_PROFILE_AVC_MAIN},
    { eAVEncH264VProfile_High,      MFX_PROFILE_AVC_HIGH},
    { eAVEncH264VProfile_422,       MFX_PROFILE_UNKNOWN },
    { eAVEncH264VProfile_High10,    MFX_PROFILE_UNKNOWN },
    { eAVEncH264VProfile_444,       MFX_PROFILE_UNKNOWN },
    { eAVEncH264VProfile_Extended,  MFX_PROFILE_UNKNOWN }
};

const ValueItem LevelsTbl[] =
{
    { 0,                        MFX_LEVEL_UNKNOWN },
    // MP levels
    { eAVEncMPVLevel_Low,       MFX_LEVEL_MPEG2_LOW },
    { eAVEncMPVLevel_Main,      MFX_LEVEL_MPEG2_MAIN },
    { eAVEncMPVLevel_High1440,  MFX_LEVEL_MPEG2_HIGH1440 },
    { eAVEncMPVLevel_High,      MFX_LEVEL_MPEG2_HIGH },
    // H.264 levels
    { eAVEncH264VLevel1,        MFX_LEVEL_AVC_1 },
    { eAVEncH264VLevel1_b,      MFX_LEVEL_AVC_1b },
    { eAVEncH264VLevel1_1,      MFX_LEVEL_AVC_11 },
    { eAVEncH264VLevel1_2,      MFX_LEVEL_AVC_12 },
    { eAVEncH264VLevel1_3,      MFX_LEVEL_AVC_13 },
    { eAVEncH264VLevel2,        MFX_LEVEL_AVC_2 },
    { eAVEncH264VLevel2_1,      MFX_LEVEL_AVC_21 },
    { eAVEncH264VLevel2_2,      MFX_LEVEL_AVC_22 },
    { eAVEncH264VLevel3,        MFX_LEVEL_AVC_3 },
    { eAVEncH264VLevel3_1,      MFX_LEVEL_AVC_31 },
    { eAVEncH264VLevel3_2,      MFX_LEVEL_AVC_32 },
    { eAVEncH264VLevel4,        MFX_LEVEL_AVC_4 },
    { eAVEncH264VLevel4_1,      MFX_LEVEL_AVC_41 },
    { eAVEncH264VLevel4_2,      MFX_LEVEL_AVC_42 },
    { eAVEncH264VLevel5,        MFX_LEVEL_AVC_5 },
    { eAVEncH264VLevel5_1,      MFX_LEVEL_AVC_51 }
};

/*------------------------------------------------------------------------------*/

static const mfxU32 Ms2MfxValue(ValueItem *table, mfxU32 table_size,
                                mfxU32 ms_value)
{
    mfxU32 i = 0;
    for (i = 0; i < table_size; ++i)
    {
        if (table[i].ms_value == ms_value) return table[i].mfx_value;
    }
    return 0;
}

/*------------------------------------------------------------------------------*/

static const mfxU32 Mfx2MsValue(ValueItem *table, mfxU32 table_size,
                                mfxU32 mfx_value)
{
    mfxU32 i = 0;
    for (i = 0; i < table_size; ++i)
    {
        if (table[i].mfx_value == mfx_value) return table[i].mfx_value;
    }
    return 0;
}

/*------------------------------------------------------------------------------*/

#define MFX_2_MS_VALUE(table, mfx_value) \
    Mfx2MsValue((ValueItem*)(table), sizeof(table)/sizeof(ValueItem), mfx_value)

#define MS_2_MFX_VALUE(table, ms_value) \
    Ms2MfxValue((ValueItem*)(table), sizeof(table)/sizeof(ValueItem), ms_value)

/*------------------------------------------------------------------------------*/
// MFConfigureMfxCodec class

MFConfigureMfxCodec::MFConfigureMfxCodec(void):
    m_bVpp(false),
    m_pWorkTicker(NULL),
    m_ticks_LiveTimeStartTick(0),
    m_ticks_WorkTime(0),
    m_ticks_ProcessInput(0),
    m_ticks_ProcessOutput(0),
    m_AvgMemUsage(0),
    m_MaxMemUsage(0),
    m_MemCount(0),
    m_pCpuUsager(NULL),
    m_CpuUsage(0),
    m_TimeTotal(0),
    m_TimeKernel(0),
    m_TimeUser(0),
    m_pInfoFile(NULL)
{
    DBG_TRACE("+");
    TCHAR file_name[MAX_PATH] = {0};

    memset(&m_MfxParamsVideo, 0, sizeof(mfxVideoParam));
    memset(&m_MfxCodecInfo, 0, sizeof(mfxCodecInfo));

    if (S_OK == mf_get_reg_string(_T(REG_PARAMS_PATH),
                                  _T(REG_INFO_FILE), file_name, MAX_PATH))
    {
        m_pInfoFile = mywfopen(file_name, L"a");
    }
    DBG_TRACE_2("file_name = %S, m_pInfoFile = %p: -", file_name, m_pInfoFile);
}

/*------------------------------------------------------------------------------*/

MFConfigureMfxCodec::~MFConfigureMfxCodec(void)
{
    DBG_TRACE("+");
    if (m_pInfoFile) fclose(m_pInfoFile);
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

HRESULT MFConfigureMfxCodec::GetParams(mfxVideoParam *params)
{
    DBG_TRACE("+");
    CHECK_POINTER(params, E_POINTER);
    memset(params, 0, sizeof(mfxVideoParam));
    if (m_bVpp)
        memcpy((void*)&(params->vpp), (void*)&(m_MfxParamsVideo.vpp), sizeof(mfxInfoVPP));
    else
        memcpy((void*)&(params->mfx), (void*)&(m_MfxParamsVideo.mfx), sizeof(mfxInfoMFX));
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFConfigureMfxCodec::GetInfo(mfxCodecInfo *info)
{
    DBG_TRACE("+");
    CHECK_POINTER(info, E_POINTER);
    memcpy((void*)info, (void*)&m_MfxCodecInfo, sizeof(mfxCodecInfo));
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

void MFConfigureMfxCodec::UpdateTimes(void)
{
    DBG_TRACE("+");

    m_MfxCodecInfo.m_LiveTime = GET_TIME(mf_get_tick(), m_ticks_LiveTimeStartTick, m_gFrequency);
    m_MfxCodecInfo.m_WorkTime = GET_TIME(m_ticks_WorkTime, 0, m_gFrequency);
    m_MfxCodecInfo.m_ProcessInputTime  = GET_TIME(m_ticks_ProcessInput, 0, m_gFrequency);
    m_MfxCodecInfo.m_ProcessOutputTime = GET_TIME(m_ticks_ProcessOutput, 0, m_gFrequency);

    DBG_TRACE_1("m_MfxCodecInfo.m_LiveTime = %f", m_MfxCodecInfo.m_LiveTime);
    DBG_TRACE_1("m_MfxCodecInfo.m_ProcessInputTime = %f",  m_MfxCodecInfo.m_ProcessInputTime);
    DBG_TRACE_1("m_MfxCodecInfo.m_ProcessOutputTime = %f: -", m_MfxCodecInfo.m_ProcessOutputTime);
}

/*------------------------------------------------------------------------------*/

void MFConfigureMfxCodec::GetPerformance(void)
{
    size_t mem_usage = mf_get_mem_usage();

    if (mem_usage > m_MaxMemUsage) m_MaxMemUsage = mem_usage;
    m_AvgMemUsage = (mfxF64)m_AvgMemUsage + (mfxF64)(mem_usage - m_AvgMemUsage)/(mfxF64)(m_MemCount+1);
    ++m_MemCount;
}

/*------------------------------------------------------------------------------*/

#define PRINT_PARAM(NAME) \
{ \
    _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: %s.%s = %d\n"), \
        plugin, id, (m_bVpp)? _T("vpp"): _T("mfx"), _T(#NAME), params->NAME); \
    fflush(m_pInfoFile); \
}

/*------------------------------------------------------------------------------*/

void MFConfigureMfxCodec::PrintInfo(void)
{
    DBG_TRACE("+");

    if (m_pInfoFile)
    {
        void* id = GetID();
        LPWSTR plugin = GetCodecName();

        if (!plugin) plugin = _T("Unknown plug-in");

        _ftprintf(m_pInfoFile, _T("\nplugin = %s: id = %p: PLUG-IN INFO\n"),
            plugin, id); fflush(m_pInfoFile);

        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_MSDKImpl = %d\n"),
            plugin, id, m_MfxCodecInfo.m_MSDKImpl); fflush(m_pInfoFile);
        // statistics: times, mem and cpu usages
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_LiveTime = %f\n"),
            plugin, id, m_MfxCodecInfo.m_LiveTime); fflush(m_pInfoFile);
//        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_WorkTime = %f\n"),
//            plugin, id, m_MfxCodecInfo.m_WorkTime); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_ProcessInputTime = %f\n"),
            plugin, id, m_MfxCodecInfo.m_ProcessInputTime); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_ProcessOutputTime = %f\n"),
            plugin, id, m_MfxCodecInfo.m_ProcessOutputTime); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_AvgMemUsage = %d (%f mb)\n"),
            plugin, id, (mfxU32)m_AvgMemUsage, m_AvgMemUsage/(1024.0*1024.0)); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_MaxMemUsage = %d (%f mb)\n"),
            plugin, id, m_MaxMemUsage, m_MaxMemUsage/(1024.0*1024.0)); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_CpuUsage = %f\n"),
            plugin, id, m_CpuUsage); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_TimeTotal = %f\n"),
            plugin, id, GET_TIME(m_TimeTotal, 0, m_gFrequency)); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_TimeKernel = %f\n"),
            plugin, id, GET_CPU_TIME(m_TimeKernel, 0, MF_CPU_TIME_FREQUENCY)); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_TimeUser = %f\n"),
            plugin, id, GET_CPU_TIME(m_TimeUser, 0, MF_CPU_TIME_FREQUENCY)); fflush(m_pInfoFile);
        // work statistics
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_bVppUsed = %d\n"),
            plugin, id, m_MfxCodecInfo.m_bVppUsed); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_nInFrames = %d\n"),
            plugin, id, m_MfxCodecInfo.m_nInFrames); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_uiInFramesType = %d\n"),
            plugin, id, m_MfxCodecInfo.m_uiInFramesType); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_nVppOutFrames = %d\n"),
            plugin, id, m_MfxCodecInfo.m_nVppOutFrames); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_uiVppOutFramesType = %d\n"),
            plugin, id, m_MfxCodecInfo.m_uiVppOutFramesType); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_nOutFrames = %d\n"),
            plugin, id, m_MfxCodecInfo.m_nOutFrames); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_uiOutFramesType = %d\n"),
            plugin, id, m_MfxCodecInfo.m_uiOutFramesType); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_InitStatus = %d\n"),
            plugin, id, m_MfxCodecInfo.m_InitStatus); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_ErrorStatus = %d\n"),
            plugin, id, m_MfxCodecInfo.m_ErrorStatus); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_hrErrorStatus = %x\n"),
            plugin, id, m_MfxCodecInfo.m_hrErrorStatus); fflush(m_pInfoFile);
        _ftprintf(m_pInfoFile, _T("plugin = %s: id = %p: m_uiErrorResetCount = %d\n"),
            plugin, id, m_MfxCodecInfo.m_uiErrorResetCount); fflush(m_pInfoFile);

        if (!m_bVpp)
        { // decoders and encoders common info
            mfxInfoMFX *params = &(m_MfxParamsVideo.mfx);

            _ftprintf(m_pInfoFile, _T("\nplugin = %s: id = %p: MFX COMMON (DECODER/ENCODER) INFO\n"),
                plugin, id); fflush(m_pInfoFile);

            PRINT_PARAM(FrameInfo.FourCC);
            PRINT_PARAM(FrameInfo.Width);
            PRINT_PARAM(FrameInfo.Height);
            PRINT_PARAM(FrameInfo.CropX);
            PRINT_PARAM(FrameInfo.CropY);
            PRINT_PARAM(FrameInfo.CropW);
            PRINT_PARAM(FrameInfo.CropH);
            PRINT_PARAM(FrameInfo.FrameRateExtN);
            PRINT_PARAM(FrameInfo.FrameRateExtD);
            PRINT_PARAM(FrameInfo.AspectRatioW);    
            PRINT_PARAM(FrameInfo.AspectRatioH);
            PRINT_PARAM(FrameInfo.PicStruct);
            PRINT_PARAM(FrameInfo.ChromaFormat);
            PRINT_PARAM(CodecId);
            PRINT_PARAM(CodecProfile);
            PRINT_PARAM(CodecLevel);
            PRINT_PARAM(NumThread);
        }
    }
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// MFEncoderParams class

MFEncoderParams::MFEncoderParams(void)
{
}

/*------------------------------------------------------------------------------*/
// ICodecAPI methods

HRESULT MFEncoderParams::IsSupported(const GUID *Api)
{
    DBG_TRACE("+");
    CHECK_POINTER(Api, E_POINTER);
    if ((CODECAPI_AVEncMPVProfile == *Api) ||
        (CODECAPI_AVEncMPVLevel == *Api))
    {
        DBG_TRACE("S_OK: -");
        return S_OK;
    }
    DBG_TRACE("E_NOTIMPL: -");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::IsModifiable(const GUID* /*Api*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::GetParameterRange(const GUID* /*Api*/,
                                           VARIANT* /*ValueMin*/,
                                           VARIANT* /*ValueMax*/,
                                           VARIANT* /*SteppingDelta*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::GetParameterValues(const GUID* /*Api*/,
                                            VARIANT** /*Values*/,
                                            ULONG* /*ValuesCount*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::GetDefaultValue(const GUID* /*Api*/, VARIANT* /*Value*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::GetValue(const GUID* Api, VARIANT* Value)
{
    DBG_TRACE("+");
    HRESULT hr = E_FAIL;

    CHECK_POINTER(Api, E_POINTER);
    CHECK_POINTER(Value, E_POINTER);
    if (CODECAPI_AVEncMPVProfile == *Api)
    {
        mfxU32 ms_profile = 0, mfx_profile = 0;

        mfx_profile = (mfxU32)m_MfxParamsVideo.mfx.CodecProfile;
        ms_profile  = MFX_2_MS_VALUE(ProfilesTbl, mfx_profile);
        if ((eAVEncMPVProfile_unknown != ms_profile) ||
            (eAVEncH264VProfile_unknown != ms_profile))
        {
            Value->vt   = VT_UI4;
            Value->lVal = ms_profile;
            hr = S_OK;
        }
        DBG_TRACE_2("CODECAPI_AVEncMPVProfile: ms_profile = %d, mfx_profile = %d",
            ms_profile, mfx_profile);
    }
    else if (CODECAPI_AVEncMPVLevel == *Api)
    {
        mfxU32 ms_level = 0, mfx_level = 0;

        mfx_level = (mfxU32)m_MfxParamsVideo.mfx.CodecLevel;
        ms_level  = MFX_2_MS_VALUE(LevelsTbl, mfx_level);
        if (ms_level)
        {
            Value->vt   = VT_UI4;
            Value->lVal = ms_level;
            hr = S_OK;
        }
        DBG_TRACE_2("CODECAPI_AVEncMPVLevel: ms_level = %d, mfx_level = %d",
            ms_level, mfx_level);
    }
    else
    {
        DBG_TRACE_1("Api->Data1 = %08x", Api->Data1);
    }
    DBG_TRACE_1("hr = %d: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::SetValue(const GUID *Api, VARIANT *Value)
{
    DBG_TRACE("+");
    HRESULT hr = ERROR_NOT_SUPPORTED;

    CHECK_POINTER(Api, E_POINTER);
    CHECK_POINTER(Value, E_POINTER);
    if (CODECAPI_AVEncMPVProfile == *Api)
    {
        mfxU32 ms_profile = 0, mfx_profile = 0;
        if (VT_UI4 == Value->vt)
        {
            ms_profile  = Value->lVal;
            mfx_profile = MS_2_MFX_VALUE(ProfilesTbl, ms_profile);
            if (MFX_PROFILE_UNKNOWN != mfx_profile)
            {
                m_MfxParamsVideo.mfx.CodecProfile = (mfxU16)mfx_profile;
                hr = S_OK;
            }
        }
        DBG_TRACE_2("CODECAPI_AVEncMPVProfile: ms_profile = %d, mfx_profile = %d",
            ms_profile, mfx_profile);
    }
    else if (CODECAPI_AVEncMPVLevel == *Api)
    {
        mfxU32 ms_level = 0, mfx_level = 0;
        if (VT_UI4 == Value->vt)
        {
            ms_level  = Value->lVal;
            mfx_level = MS_2_MFX_VALUE(LevelsTbl, ms_level);
            if (MFX_PROFILE_UNKNOWN != mfx_level)
            {
                m_MfxParamsVideo.mfx.CodecLevel = (mfxU16)mfx_level;
                hr = S_OK;
            }
        }
        DBG_TRACE_2("CODECAPI_AVEncMPVLevel: ms_level = %d, mfx_level = %d",
            ms_level, mfx_level);
    }
    else
    {
        DBG_TRACE_1("Api->Data1 = %08x", Api->Data1);
    }
    DBG_TRACE_1("hr = %d: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::RegisterForEvent(const GUID* /*Api*/, LONG_PTR /*userData*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::UnregisterForEvent(const GUID* /*Api*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::SetAllDefaults(void)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::SetValueWithNotify(const GUID* /*Api*/,
                                            VARIANT* /*Value*/,
                                            GUID** /*ChangedParam*/,
                                            ULONG* /*ChangedParamCount*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::SetAllDefaultsWithNotify(GUID** /*ChangedParam*/,
                                                  ULONG* /*ChangedParamCount*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::GetAllSettings(IStream* /*pStream*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::SetAllSettings(IStream* /*pStream*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncoderParams::SetAllSettingsWithNotify(IStream* /*pStream*/,
                                                  GUID** /*ChangedParam*/,
                                                  ULONG* /*ChangedParamCount*/)
{
    DBG_TRACE("");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/
// IConfigureMfxCodec methods

#define SET_PARAM_MFX(NAME) \
    if (pattern->mfx.NAME) info_mfx->NAME = params->mfx.NAME;

HRESULT MFEncoderParams::SetParams(mfxVideoParam *pattern, mfxVideoParam *params)
{
    DBG_TRACE("+");
    CHECK_POINTER(params, E_POINTER);
    CHECK_POINTER(pattern, E_POINTER);

    mfxInfoMFX *info_mfx = &(m_MfxParamsVideo.mfx);
    SET_PARAM_MFX(FrameInfo.FourCC);
    SET_PARAM_MFX(FrameInfo.Width);
    SET_PARAM_MFX(FrameInfo.Height);
    SET_PARAM_MFX(FrameInfo.CropX);
    SET_PARAM_MFX(FrameInfo.CropY);
    SET_PARAM_MFX(FrameInfo.CropW);
    SET_PARAM_MFX(FrameInfo.CropH);
    SET_PARAM_MFX(FrameInfo.FrameRateExtN);
    SET_PARAM_MFX(FrameInfo.FrameRateExtD);
    SET_PARAM_MFX(FrameInfo.AspectRatioW);    
    SET_PARAM_MFX(FrameInfo.AspectRatioH);
    SET_PARAM_MFX(FrameInfo.PicStruct);
    SET_PARAM_MFX(FrameInfo.ChromaFormat);
    SET_PARAM_MFX(CodecId);
    SET_PARAM_MFX(CodecProfile);
    SET_PARAM_MFX(CodecLevel);
    SET_PARAM_MFX(NumThread);
    SET_PARAM_MFX(TargetUsage);
    SET_PARAM_MFX(GopPicSize);
    SET_PARAM_MFX(GopRefDist);
    SET_PARAM_MFX(GopOptFlag);
    SET_PARAM_MFX(IdrInterval);
    SET_PARAM_MFX(RateControlMethod);
    SET_PARAM_MFX(InitialDelayInKB);
    SET_PARAM_MFX(BufferSizeInKB);
    SET_PARAM_MFX(TargetKbps);
    SET_PARAM_MFX(MaxKbps);
    SET_PARAM_MFX(NumSlice);
    SET_PARAM_MFX(NumRefFrame);
    SET_PARAM_MFX(EncodedOrder);
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

void MFEncoderParams::PrintInfo(void)
{
    DBG_TRACE("+");
    MFConfigureMfxCodec::PrintInfo();
    if (m_pInfoFile)
    {
        void* id = GetID();
        LPWSTR plugin = GetCodecName();
        mfxInfoMFX *params = &(m_MfxParamsVideo.mfx);

        if (!plugin) plugin = _T("Unknown encoder plug-in");

        _ftprintf(m_pInfoFile, _T("\nplugin = %s: id = %p: MFX ENCODER INFO\n"),
            plugin, id); fflush(m_pInfoFile);
        PRINT_PARAM(TargetUsage);
        PRINT_PARAM(GopPicSize);
        PRINT_PARAM(GopRefDist);
        PRINT_PARAM(GopOptFlag);
        PRINT_PARAM(IdrInterval);
        PRINT_PARAM(RateControlMethod);
        PRINT_PARAM(InitialDelayInKB);
        PRINT_PARAM(BufferSizeInKB);
        PRINT_PARAM(TargetKbps);
        PRINT_PARAM(MaxKbps);
        PRINT_PARAM(NumSlice);
        PRINT_PARAM(NumRefFrame);
        PRINT_PARAM(EncodedOrder);
        _ftprintf(m_pInfoFile, _T("\n"), plugin, id); fflush(m_pInfoFile);
    }
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// MFDecoderParams class

MFDecoderParams::MFDecoderParams(void)
{
}

/*------------------------------------------------------------------------------*/
// IConfigureCodec methods

HRESULT MFDecoderParams::SetParams(mfxVideoParam *pattern, mfxVideoParam *params)
{
    DBG_TRACE("+");
    CHECK_POINTER(params, E_POINTER);
    CHECK_POINTER(pattern, E_POINTER);

    mfxInfoMFX *info_mfx = &(m_MfxParamsVideo.mfx);
    SET_PARAM_MFX(FrameInfo.FourCC);
    SET_PARAM_MFX(FrameInfo.Width);
    SET_PARAM_MFX(FrameInfo.Height);
    SET_PARAM_MFX(FrameInfo.CropX);
    SET_PARAM_MFX(FrameInfo.CropY);
    SET_PARAM_MFX(FrameInfo.CropW);
    SET_PARAM_MFX(FrameInfo.CropH);
    SET_PARAM_MFX(FrameInfo.FrameRateExtN);
    SET_PARAM_MFX(FrameInfo.FrameRateExtD);
    SET_PARAM_MFX(FrameInfo.AspectRatioW);    
    SET_PARAM_MFX(FrameInfo.AspectRatioH);
    SET_PARAM_MFX(FrameInfo.PicStruct);
    SET_PARAM_MFX(FrameInfo.ChromaFormat);
    SET_PARAM_MFX(CodecId);
    SET_PARAM_MFX(CodecProfile);
    SET_PARAM_MFX(CodecLevel);
    SET_PARAM_MFX(NumThread);
    SET_PARAM_MFX(DecodedOrder);
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

void MFDecoderParams::PrintInfo(void)
{
    DBG_TRACE("+");
    MFConfigureMfxCodec::PrintInfo();
    if (m_pInfoFile)
    {
        void* id = GetID();
        LPWSTR plugin = GetCodecName();
        mfxInfoMFX *params = &(m_MfxParamsVideo.mfx);

        if (!plugin) plugin = _T("Unknown decoder plug-in");

        _ftprintf(m_pInfoFile, _T("\nplugin = %s: id = %p: MFX DECODER INFO\n"),
            plugin, id); fflush(m_pInfoFile);
        PRINT_PARAM(DecodedOrder);
        _ftprintf(m_pInfoFile, _T("\n"), plugin, id); fflush(m_pInfoFile);
    }
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// MFVppParams class

MFVppParams::MFVppParams(void)
{
    m_bVpp = true;
}

/*------------------------------------------------------------------------------*/
// IConfigureCodec methods

#define SET_PARAM_VPP(NAME) \
    if (pattern->vpp.NAME) info_vpp->NAME = params->vpp.NAME;

HRESULT MFVppParams::SetParams(mfxVideoParam *pattern, mfxVideoParam *params)
{
    DBG_TRACE("+");
    CHECK_POINTER(params, E_POINTER);
    CHECK_POINTER(pattern, E_POINTER);

    mfxInfoVPP *info_vpp = &(m_MfxParamsVideo.vpp);
    // setting input parameters
    SET_PARAM_VPP(In.FourCC);
    SET_PARAM_VPP(In.Width);
    SET_PARAM_VPP(In.Height);
    SET_PARAM_VPP(In.CropX);
    SET_PARAM_VPP(In.CropY);
    SET_PARAM_VPP(In.CropW);
    SET_PARAM_VPP(In.CropH);
    SET_PARAM_VPP(In.FrameRateExtN);
    SET_PARAM_VPP(In.FrameRateExtD);
    SET_PARAM_VPP(In.AspectRatioW);    
    SET_PARAM_VPP(In.AspectRatioH);
    SET_PARAM_VPP(In.PicStruct);
    SET_PARAM_VPP(In.ChromaFormat);
    // setting output parameters
    SET_PARAM_VPP(Out.FourCC);
    SET_PARAM_VPP(Out.Width);
    SET_PARAM_VPP(Out.Height);
    SET_PARAM_VPP(Out.CropX);
    SET_PARAM_VPP(Out.CropY);
    SET_PARAM_VPP(Out.CropW);
    SET_PARAM_VPP(Out.CropH);
    SET_PARAM_VPP(Out.FrameRateExtN);
    SET_PARAM_VPP(Out.FrameRateExtD);
    SET_PARAM_VPP(Out.AspectRatioW);    
    SET_PARAM_VPP(Out.AspectRatioH);
    SET_PARAM_VPP(Out.PicStruct);
    SET_PARAM_VPP(Out.ChromaFormat);
    DBG_TRACE("-");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

void MFVppParams::PrintInfo(void)
{
    DBG_TRACE("+");
    MFConfigureMfxCodec::PrintInfo();
    if (m_pInfoFile)
    {
        void* id = GetID();
        LPWSTR plugin = GetCodecName();
        mfxInfoVPP *params = &(m_MfxParamsVideo.vpp);

        if (!plugin) plugin = _T("Unknown VPP plug-in");

        _ftprintf(m_pInfoFile, _T("\nplugin = %s: id = %p: MFX VPP INFO\n"),
            plugin, id); fflush(m_pInfoFile);
        // setting input parameters
        PRINT_PARAM(In.FourCC);
        PRINT_PARAM(In.Width);
        PRINT_PARAM(In.Height);
        PRINT_PARAM(In.CropX);
        PRINT_PARAM(In.CropY);
        PRINT_PARAM(In.CropW);
        PRINT_PARAM(In.CropH);
        PRINT_PARAM(In.FrameRateExtN);
        PRINT_PARAM(In.FrameRateExtD);
        PRINT_PARAM(In.AspectRatioW);    
        PRINT_PARAM(In.AspectRatioH);
        PRINT_PARAM(In.PicStruct);
        PRINT_PARAM(In.ChromaFormat);
        // setting output parameters
        PRINT_PARAM(Out.FourCC);
        PRINT_PARAM(Out.Width);
        PRINT_PARAM(Out.Height);
        PRINT_PARAM(Out.CropX);
        PRINT_PARAM(Out.CropY);
        PRINT_PARAM(Out.CropW);
        PRINT_PARAM(Out.CropH);
        PRINT_PARAM(Out.FrameRateExtN);
        PRINT_PARAM(Out.FrameRateExtD);
        PRINT_PARAM(Out.AspectRatioW);    
        PRINT_PARAM(Out.AspectRatioH);
        PRINT_PARAM(Out.PicStruct);
        PRINT_PARAM(Out.ChromaFormat);
        _ftprintf(m_pInfoFile, _T("\n"), plugin, id); fflush(m_pInfoFile);
    }
    DBG_TRACE("-");
}
