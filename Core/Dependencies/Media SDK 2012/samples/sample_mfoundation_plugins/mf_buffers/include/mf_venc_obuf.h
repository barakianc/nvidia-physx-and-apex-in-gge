/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_venc_obuf.h

Purpose: define code for support of output buffering in Intel MediaFoundation
encoder plug-ins.

Defined Classes, Structures & Enumerations:
  * MFEncBitstream - enables synchronization between IMFSample and mfxBitstream

*********************************************************************************/

#ifndef __MF_VENC_OBUF_H__
#define __MF_VENC_OBUF_H__

#include "mf_utils.h"

/*------------------------------------------------------------------------------*/

class MFEncBitstream
{
public:
    MFEncBitstream (void);
    ~MFEncBitstream(void);

    mfxStatus Init   (mfxVideoParam* pParams,
                      MFSamplesPool* pSamplesPool);
    void      Close  (void);
    mfxStatus Alloc  (void);
    HRESULT   Sync   (void);
    HRESULT   Release(void);

    mfxBitstream* GetMfxBitstream(void){ return &m_Bst; }
    void IsGapBitstream(bool bGap){ m_bGap = bGap; }

    IMFSample* GetSample(void)
    { if (m_pSample) m_pSample->AddRef(); return m_pSample; }

    void SetFile(FILE* file) { m_dbg_file = file; }

protected:
    bool            m_bInitialized; // flag to indicate initialization
    bool            m_bLocked;      // flag to indicate lock of MF buffer
    bool            m_bGap;         // flag to indicate discontinuity frame
    mfxU32          m_BufSize;
    IMFSample*      m_pSample;
    IMFMediaBuffer* m_pMediaBuffer;
    mfxBitstream    m_Bst;
    MFSamplesPool*  m_pSamplesPool;
    FILE*           m_dbg_file;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFEncBitstream(const MFEncBitstream&);
    MFEncBitstream& operator=(const MFEncBitstream&);
};

/*------------------------------------------------------------------------------*/

struct MFEncOutData
{
    bool             bFreeData;
    mfxSyncPoint*    pSyncPoint;
    bool             bSyncPointUsed;
    mfxStatus        iSyncPointSts;
    MFEncBitstream*  pMFBitstream;
};

#endif // #ifndef __MF_VENC_OBUF_H__
