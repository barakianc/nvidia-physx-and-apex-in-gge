/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_yuv_ibuf.h

Purpose: define code for support of input buffering in Intel MediaFoundation
encoder and vpp plug-ins.

Defined Classes, Structures & Enumerations:
  * MFYuvInSurface - enables synchronization between mfxFrameSurface1 and IMFSample

*********************************************************************************/

#ifndef __MF_YUV_IBUF_H__
#define __MF_YUV_IBUF_H__

#include "mf_utils.h"
#include "mf_vbuf.h"

/*------------------------------------------------------------------------------*/

class MFYuvInSurface
{
public:
    MFYuvInSurface (void);
    ~MFYuvInSurface(void);

    // pInfo - MediaSDK initialization parameters
    // pInInfo - parameters of actual input frames
    mfxStatus Init   (mfxFrameInfo* pInfo, mfxFrameInfo* pInInfo,
                      mfxMemId memid, bool bAlloc);
    void      Close  (void);
    HRESULT   Load   (IMFSample* pSample, bool bIsIntetnalSurface);
    HRESULT   Release(void);
    HRESULT   PseudoLoad(void);

    mfxFrameSurface1* GetSurface  (void){ return (m_pComMfxSurface)? m_pComMfxSurface->GetMfxFrameSurface(): &m_mfxSurface; }
    bool IsFakeSrf(void) { return m_bFake; }
    void SetFile(FILE* file) { m_dbg_file = file; }
    void DropDataToFile(void);

protected: // functions
    HRESULT   LoadSW(void);

protected: // variables
    IMFSample*        m_pSample;
    IMFMediaBuffer*   m_pMediaBuffer;
    IMF2DBuffer*      m_p2DBuffer;
    IMfxFrameSurface* m_pComMfxSurface;
    mfxFrameSurface1  m_mfxSurface;
    mfxFrameSurface1  m_mfxInSurface;
    bool              m_bAllocated;   // flag to indicate allocation inside this class
    bool              m_bLocked;      // flag to indicate existence of data
    bool              m_bReleased;    // flag to skip release if it was done
    bool              m_bFake;        // flag to indicate fake frame
    bool              m_bInitialized; // flag to indicate initialization
    mfxU8*            m_pVideo16;
    mfxU16            m_nPitch;
    FILE*             m_dbg_file;

private:
    // avoiding possible problems by defining operator= and copy constructor
    MFYuvInSurface(const MFYuvInSurface&);
    MFYuvInSurface& operator=(const MFYuvInSurface&);
};

/*------------------------------------------------------------------------------*/

#endif // #ifndef __MF_YUV_IBUF_H__
