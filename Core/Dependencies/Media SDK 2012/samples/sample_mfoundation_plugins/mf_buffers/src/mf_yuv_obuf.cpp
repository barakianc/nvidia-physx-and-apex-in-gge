/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_yuv_obuf.cpp

Purpose: define code for support of output buffering in Intel MediaFoundation
decoder and vpp plug-ins.

*********************************************************************************/

#include "mf_yuv_obuf.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Plug-in"

/*------------------------------------------------------------------------------*/
// MFSurface class

MFYuvOutSurface::MFYuvOutSurface(void):
    m_State(stSurfaceFree),
    m_pSample(NULL),
    m_p2DBuffer(NULL),
    m_pComMfxSurface(NULL),
    m_bLocked(false),
    m_bInitialized(false),
    m_bDoNotAlloc(false),
    m_bFake(false),
    m_bGap(false),
    m_pSamplesPool(NULL),
    m_nPitch(0),
    m_dbg_file(NULL)
{
    DBG_TRACE("+");
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFYuvOutSurface::~MFYuvOutSurface(void)
{
    DBG_TRACE("+");
    if (m_bLocked && m_p2DBuffer)
    {
        m_p2DBuffer->Unlock2D();
        m_bLocked = false;
    }
    SAFE_RELEASE(m_pComMfxSurface);
    SAFE_RELEASE(m_p2DBuffer);
    SAFE_RELEASE(m_pSample);
    SAFE_RELEASE(m_pSamplesPool);
    Close();
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

mfxStatus MFYuvOutSurface::Init(mfxFrameInfo*  pInfo,
                                MFSamplesPool* pSamplesPool)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;

    CHECK_EXPRESSION(!m_bInitialized, MFX_ERR_ABORTED);
    CHECK_POINTER(pInfo, MFX_ERR_NULL_PTR);
    CHECK_POINTER(pSamplesPool, MFX_ERR_NULL_PTR);
    CHECK_EXPRESSION((MFX_FOURCC_NV12 == pInfo->FourCC), MFX_ERR_ABORTED);
    CHECK_EXPRESSION((MFX_CHROMAFORMAT_YUV420 == pInfo->ChromaFormat), MFX_ERR_ABORTED);

    if (!m_pComMfxSurface &&
        FAILED(MFCreateMfxFrameSurface(&m_pComMfxSurface)))
    {
        sts = MFX_ERR_MEMORY_ALLOC;
    }
    if (MFX_ERR_NONE == sts)
    {
        mfxFrameSurface1* srf = m_pComMfxSurface->GetMfxFrameSurface();

        memcpy(&(srf->Info), pInfo, sizeof(mfxFrameInfo));
        memset(&(srf->Data),0,sizeof(mfxFrameData));

        m_nPitch = (mfxU16)(MEM_ALIGN(pInfo->Width, m_gMemAlignDec));
        DBG_TRACE_2("nWidth = %d, nPitch = %d", pInfo->Width, m_nPitch);

        pSamplesPool->AddRef();
        m_pSamplesPool = pSamplesPool;

        m_bInitialized = true;
    }
    DBG_TRACE("-");
    return sts;
}

/*------------------------------------------------------------------------------*/

HRESULT MFYuvOutSurface::Pretend(mfxU16 w, mfxU16 h)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    if (m_p2DBuffer)
    {
        MFVideoBuffer *pVideoBuffer = NULL;

        hr = m_p2DBuffer->QueryInterface(IID_MFVideoBuffer, (void**)&pVideoBuffer);
        if (SUCCEEDED(hr) && pVideoBuffer)
        {
            bool flag = (w && h)? true: false;
            pVideoBuffer->PretendOn(flag);
            if (flag) hr = pVideoBuffer->Pretend(w, h);
        }
        SAFE_RELEASE(pVideoBuffer);
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFYuvOutSurface::AllocHW(IDirect3DSurface9* pSurface)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    HRESULT hr = S_OK;
    mfxFrameSurface1* srf = NULL;

    CHECK_POINTER(pSurface, MFX_ERR_NULL_PTR);
    if (!m_bDoNotAlloc)
    {
        DBG_TRACE("HW Sample created");
        if (SUCCEEDED(hr) && !m_pComMfxSurface) hr = E_FAIL;
        if (SUCCEEDED(hr)) hr = MFCreateVideoSampleFromSurface(pSurface, &m_pSample);
        if (SUCCEEDED(hr)) hr = m_pSamplesPool->AddSample(m_pSample);
        if (SUCCEEDED(hr)) hr = m_pSample->SetUnknown(MF_MT_MFX_FRAME_SRF, m_pComMfxSurface);
        if (SUCCEEDED(hr))
        {
            srf = m_pComMfxSurface->GetMfxFrameSurface();
            srf->Data.MemId = pSurface;

            m_bDoNotAlloc = true;
            m_State = stSurfaceReady;
        }
        else
        {
            SAFE_RELEASE(m_pSample);
        }
    }
    else hr = E_FAIL;
    if (FAILED(hr)) sts = MFX_ERR_UNKNOWN;
    DBG_TRACE("-");
    return sts;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFYuvOutSurface::AllocSW(void)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    HRESULT hr = S_OK;
    mfxU8*  pData = NULL;
    LONG    uiPitch = 0;
    mfxU16  nWidth = 0, nHeight = 0;
    IMFMediaBuffer* pMediaBuffer = NULL;
    mfxFrameSurface1* srf = NULL;

    // trying to obtain surface from pool
    if (!m_bDoNotAlloc)
    {
        DBG_TRACE("SW Sample created");
        if (SUCCEEDED(hr) && !m_pComMfxSurface) hr = E_FAIL;
        // trying to obtain pointer to mfx surface
        if (SUCCEEDED(hr))
        {
            srf = m_pComMfxSurface->GetMfxFrameSurface();
            nWidth  = srf->Info.Width;
            nHeight = srf->Info.Height;
        }
        if (SUCCEEDED(hr)) hr = MFCreateVideoSampleFromSurface(NULL, &m_pSample);
        if (SUCCEEDED(hr)) hr = MFCreateVideoBuffer(nWidth, nHeight, m_nPitch, MFX_FOURCC_NV12, &pMediaBuffer);
        if (SUCCEEDED(hr)) hr = m_pSample->AddBuffer(pMediaBuffer);
        if (SUCCEEDED(hr)) hr = m_pSamplesPool->AddSample(m_pSample);
        if (SUCCEEDED(hr)) hr = m_pSample->SetUnknown(MF_MT_MFX_FRAME_SRF, m_pComMfxSurface);
        if (SUCCEEDED(hr)) hr = pMediaBuffer->QueryInterface(IID_IMF2DBuffer, (void**)&m_p2DBuffer);
        SAFE_RELEASE(pMediaBuffer);
        // "Pretend" is workaround for MSFT components: removing any pretending for our code
        if (SUCCEEDED(hr)) hr = Pretend(0, 0);
        if (SUCCEEDED(hr)) hr = m_p2DBuffer->Lock2D((BYTE**)&pData, &uiPitch);

        m_bLocked = SUCCEEDED(hr);

        CHECK_EXPRESSION(SUCCEEDED(hr), MFX_ERR_UNKNOWN);
        CHECK_EXPRESSION(uiPitch == m_nPitch, MFX_ERR_UNKNOWN); // should not occur

        switch(srf->Info.FourCC)
        {
        case MFX_FOURCC_NV12:
            {
                srf->Data.Y  = pData;
                srf->Data.UV = pData + m_nPitch * nHeight;
            }
        }
        srf->Data.Pitch = m_nPitch;
        if (SUCCEEDED(hr))
        {
            m_bDoNotAlloc = true;
            m_State = stSurfaceReady;
        }
        else
        {
            SAFE_RELEASE(m_pSample);
        }
    }
    else hr = E_FAIL;
    if (FAILED(hr)) sts = MFX_ERR_UNKNOWN;
    DBG_TRACE("-");
    return sts;
}

/*------------------------------------------------------------------------------*/

mfxStatus MFYuvOutSurface::Alloc(mfxMemId memid)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(m_bInitialized, MFX_ERR_NOT_INITIALIZED);
    m_pSample = m_pSamplesPool->GetSample();
    if (m_pSample)
    {
        DBG_TRACE("Sample taken from pool");
        if (SUCCEEDED(hr)) hr = m_pSamplesPool->AddSample(m_pSample);
        if (SUCCEEDED(hr)) hr = m_pSample->GetUnknown(MF_MT_MFX_FRAME_SRF,
                                                      IID_IMfxFrameSurface,
                                                      (void**)&m_pComMfxSurface);
        if (SUCCEEDED(hr))
        {
            mfxFrameSurface1* srf = m_pComMfxSurface->GetMfxFrameSurface();
            if (srf->Data.Locked)
            {
                m_State = stSurfaceLocked;
                sts = MFX_ERR_LOCK_MEMORY;
            }
            else m_State = stSurfaceReady;
        }
        else
        {
            SAFE_RELEASE(m_pSample);
            SAFE_RELEASE(m_pComMfxSurface);
            sts = MFX_ERR_UNKNOWN;
        }
    }
    else
    {
        IDirect3DSurface9* pSurface = mf_get_d3d_srf_from_mid(memid);

        if (pSurface) sts = AllocHW(pSurface);
        else sts = AllocSW();

        SAFE_RELEASE(pSurface);
    }
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/

void MFYuvOutSurface::Close(void)
{
    DBG_TRACE("+");
    m_bInitialized = false;
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

HRESULT MFYuvOutSurface::Sync(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxFrameSurface1* srf = NULL;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    CHECK_POINTER(m_pComMfxSurface, E_POINTER); // should not occur

    srf = m_pComMfxSurface->GetMfxFrameSurface();
    if (!m_bFake)
    {
        if (!(srf->Data.MemId))
        {
            // SW only part
            switch(srf->Info.FourCC)
            {
            case MFX_FOURCC_NV12:
                if (m_dbg_file)
                {
                    mf_dump_YUV_from_NV12_data(m_dbg_file, srf->Data.Y,
                                               &(srf->Info), m_nPitch);
                }
                break;
            default:
                // will not occurr
                break;
            }
            // here sample should be already created and buffer locked
            if (m_bLocked)
            {
                m_p2DBuffer->Unlock2D();
                m_bLocked = false;
            }
        }
        else
#if USE_DBG != DBG_YES
            if (m_dbg_file)
#endif
        {
            HRESULT hr_sts = S_OK;
            IDirect3DSurface9* pSurface = mf_get_d3d_srf_from_mid(srf->Data.MemId);
            D3DSURFACE_DESC d3d_desc;
            D3DLOCKED_RECT  d3d_rect = {NULL,0};

            memset(&d3d_desc, 0, sizeof(D3DSURFACE_DESC));
            if (!pSurface) hr_sts = MFX_ERR_NULL_PTR;
            if (SUCCEEDED(hr_sts)) hr_sts = pSurface->GetDesc(&d3d_desc);
            if (SUCCEEDED(hr_sts)) hr_sts = pSurface->LockRect(&d3d_rect, NULL, D3DLOCK_READONLY);
            DBG_TRACE_2("HW surface: Format         = %x (dec = %d)", d3d_desc.Format, d3d_desc.Format);
            DBG_TRACE_2("HW surface: Format_NV12    = %x (dec = %d)", MAKEFOURCC('N', 'V', '1', '2'), MAKEFOURCC('N', 'V', '1', '2'));
            DBG_TRACE_2("HW surface: Width x Height = %d x %d", d3d_desc.Width, d3d_desc.Height);
            DBG_TRACE_1("HW surface: Pitch          = %d", d3d_rect.Pitch);
            if (m_dbg_file && SUCCEEDED(hr_sts) && (MAKEFOURCC('N', 'V', '1', '2') == d3d_desc.Format))
            {
                mfxFrameInfo info;

                memset(&info, 0, sizeof(mfxFrameInfo));
                info.Width  = (mfxU16)d3d_desc.Width;
                info.Height = (mfxU16)d3d_desc.Height;
                info.CropW  = (mfxU16)d3d_desc.Width;
                info.CropH  = (mfxU16)d3d_desc.Height;
                mf_dump_YUV_from_NV12_data(m_dbg_file, (mfxU8*)d3d_rect.pBits, &info, d3d_rect.Pitch);
            }
            if (SUCCEEDED(hr_sts)) hr_sts = pSurface->UnlockRect();
            DBG_TRACE_1("hr_sts = %x (HW surface dumping)", hr_sts);
            SAFE_RELEASE(pSurface);
        }
    }
    if (SUCCEEDED(hr) && m_pSample)
    {
        HRESULT hr_sts = S_OK;
        mfxF64 framerate = (mfxF64)srf->Info.FrameRateExtN/(mfxF64)srf->Info.FrameRateExtD;

        mfxU16 imode = srf->Info.PicStruct;
        UINT32 bInterlaced = (((MFX_PICSTRUCT_FIELD_TFF & imode) ||
                               (MFX_PICSTRUCT_FIELD_BFF & imode)) && 
                               !(MFX_PICSTRUCT_PROGRESSIVE & imode))? TRUE: FALSE;
        UINT32 bBFF = (MFX_PICSTRUCT_FIELD_BFF & imode)? TRUE: FALSE;
        UINT32 bRepeated = (MFX_PICSTRUCT_FIELD_REPEATED & imode)? TRUE: FALSE;

        DBG_TRACE_1("bInterlaced = %d", bInterlaced);
        DBG_TRACE_1("bBFF = %d", bBFF);
        DBG_TRACE_1("bRepeated = %d", bRepeated);

        // we will always set these attributes, overwise MF core may set them and canflict may occur
        hr_sts = m_pSample->SetUINT32(MFSampleExtension_Interlaced, bInterlaced);
        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
        hr_sts = m_pSample->SetUINT32(MFSampleExtension_BottomFieldFirst, bBFF);
        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
        hr_sts = m_pSample->SetUINT32(MFSampleExtension_RepeatFirstField, bRepeated);
        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;

        if (MF_TIME_STAMP_INVALID != srf->Data.TimeStamp)
        {
            hr_sts = m_pSample->SetSampleTime(MFX2REF_TIME(srf->Data.TimeStamp));
            if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
            DBG_TRACE_1("SampleTime = %f", REF2SEC_TIME(MFX2REF_TIME(m_mfxSurface.Data.TimeStamp)));
        }
        else
        {
            hr_sts = m_pSample->SetSampleTime(SEC2REF_TIME(0));
            if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
            DBG_TRACE_1("SampleTime = %f (invalid time stamp)", 0);
        }
        hr_sts = m_pSample->SetSampleDuration(SEC2REF_TIME(1/framerate));
        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;

        hr_sts = m_pSample->SetUINT32(MF_MT_FAKE_SRF, (m_bFake)? TRUE: FALSE);
        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;

        hr_sts = m_pSample->SetUINT32(MFSampleExtension_Discontinuity, (m_bGap)? TRUE: FALSE);
        if (SUCCEEDED(hr) && FAILED(hr_sts)) hr = hr_sts;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/
// Release of objects
// Surface should be unlocked by decoder or vpp

HRESULT MFYuvOutSurface::Release(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    switch (m_State)
    {
    case stSurfaceFree:
        {
            // allocating new surface
            sts = Alloc();
            if (MFX_ERR_NONE != sts) hr = E_FAIL;
        }
        break;
    case stSurfaceLocked:
        {
            mfxFrameSurface1* srf = m_pComMfxSurface->GetMfxFrameSurface();
            if (srf->Data.Locked) hr = E_FAIL;
            else m_State = stSurfaceReady;
        }
        break;
    case stSurfaceReady:
        {
            mfxFrameSurface1* srf = m_pComMfxSurface->GetMfxFrameSurface();
            if (srf->Data.Locked)
            {
                m_State = stSurfaceLocked;
                hr = E_FAIL;
            }
        }
        break;
    case stSurfaceNotDisplayed:
        {
            hr = E_FAIL;
        }
        break;
    case stSurfaceDisplayed:
        {
            if (m_bLocked && m_p2DBuffer)
            {
                m_p2DBuffer->Unlock2D();
                m_bLocked = false;
            }
            SAFE_RELEASE(m_pComMfxSurface);
            SAFE_RELEASE(m_p2DBuffer);
            SAFE_RELEASE(m_pSample);
            m_State = stSurfaceFree;

            // allocating new surface
            sts = Alloc();
            if (MFX_ERR_NONE != sts) hr = E_FAIL;
        }
        break;
    };
    DBG_TRACE_2("m_bWasDisplayed = %d, m_bDisplayed = %d", m_bWasDisplayed, m_bDisplayed);
    DBG_TRACE_2("m_mfxSurface.Data.Locked = %d, hr = %x: -", m_mfxSurface.Data.Locked, hr);
    return hr;
}
