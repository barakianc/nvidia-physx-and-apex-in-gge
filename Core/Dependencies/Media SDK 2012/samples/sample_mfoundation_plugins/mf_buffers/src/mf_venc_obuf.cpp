/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_yuv_ibuf.cpp

Purpose: define code for support of input buffering in Intel MediaFoundation
encoder plug-ins.

*********************************************************************************/

#include "mf_venc_obuf.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Encoder"

/*------------------------------------------------------------------------------*/

MFEncBitstream::MFEncBitstream(void):
    m_bInitialized(false),
    m_bLocked(false),
    m_bGap(false),
    m_BufSize(0),
    m_pSample(NULL),
    m_pMediaBuffer(NULL),
    m_pSamplesPool(NULL),
    m_dbg_file(NULL)
{
    DBG_TRACE("+");
    memset(&m_Bst, 0, sizeof(mfxBitstream));
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFEncBitstream::~MFEncBitstream(void)
{
    DBG_TRACE("+");
    if (m_bLocked && m_pMediaBuffer)
    {
        m_pMediaBuffer->Unlock();
        m_bLocked = false;
    }
    SAFE_RELEASE(m_pMediaBuffer);
    SAFE_RELEASE(m_pSample);
    SAFE_RELEASE(m_pSamplesPool);
    Close();
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

mfxStatus MFEncBitstream::Init(mfxVideoParam* pParams,
                               MFSamplesPool* pSamplesPool)
{
    DBG_TRACE("+");
    CHECK_EXPRESSION(!m_bInitialized, MFX_ERR_ABORTED);
    CHECK_POINTER(pParams, MFX_ERR_NULL_PTR);
    CHECK_POINTER(pSamplesPool, MFX_ERR_NULL_PTR);

    if (pParams->mfx.BufferSizeInKB)
        m_BufSize  = 1000*pParams->mfx.BufferSizeInKB;
    else
        m_BufSize  = 2 * pParams->mfx.FrameInfo.Height *
                         pParams->mfx.FrameInfo.Width;
    m_Bst.TimeStamp  = MF_TIME_STAMP_INVALID;

    pSamplesPool->AddRef();
    m_pSamplesPool = pSamplesPool;

    m_bInitialized = true;
    DBG_TRACE("-");
    return MFX_ERR_NONE;
}

/*------------------------------------------------------------------------------*/

void MFEncBitstream::Close(void)
{
    DBG_TRACE("+");
    m_bInitialized = false;
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

mfxStatus MFEncBitstream::Alloc(void)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(m_bInitialized, MFX_ERR_NOT_INITIALIZED);
    m_pSample = m_pSamplesPool->GetSample();
    if (m_pSample)
    {
        DBG_TRACE("Sample taken from pool");
        if (SUCCEEDED(hr)) hr = m_pSample->GetBufferByIndex(0, &m_pMediaBuffer);
        if (SUCCEEDED(hr)) hr = m_pSamplesPool->AddSample(m_pSample);
        else
        {
            SAFE_RELEASE(m_pMediaBuffer);
            SAFE_RELEASE(m_pSample);
        }
    }
    else
    {
        DBG_TRACE("Sample created");
        if (SUCCEEDED(hr)) hr = MFCreateMemoryBuffer(m_BufSize, &m_pMediaBuffer);
        if (SUCCEEDED(hr)) hr = MFCreateVideoSampleFromSurface(NULL, &m_pSample);
        if (SUCCEEDED(hr)) hr = m_pSample->AddBuffer(m_pMediaBuffer);
        if (SUCCEEDED(hr)) hr = m_pSamplesPool->AddSample(m_pSample);
        else
        {
            SAFE_RELEASE(m_pMediaBuffer);
            SAFE_RELEASE(m_pSample);
        }
    }
    if (SUCCEEDED(hr)) hr = m_pMediaBuffer->Lock((BYTE**)&(m_Bst.Data),
                                                 (DWORD*)&(m_Bst.MaxLength),
                                                 NULL);
    m_bLocked = SUCCEEDED(hr);
    CHECK_EXPRESSION(SUCCEEDED(hr), MFX_ERR_UNKNOWN);
    DBG_TRACE_1("sts = %d: -", sts);
    return sts;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncBitstream::Sync(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    CHECK_POINTER(m_pSample, E_POINTER);
    CHECK_POINTER(m_pMediaBuffer, E_POINTER);
    if (SUCCEEDED(hr))
    {
        if (m_dbg_file)
        {
            fwrite(&(m_Bst.Data[m_Bst.DataOffset]), 1, m_Bst.DataLength, m_dbg_file);
            fflush(m_dbg_file);
        }
        m_pMediaBuffer->SetCurrentLength(m_Bst.DataLength);
        if (MF_TIME_STAMP_INVALID != m_Bst.TimeStamp)
        {
            m_pSample->SetSampleTime(MFX2REF_TIME(m_Bst.TimeStamp));
            DBG_TRACE_1("frame time = %f", (mfxF64)REF2SEC_TIME(MFX2REF_TIME(m_Bst.TimeStamp)));
        }
        if (MFX_FRAMETYPE_I & m_Bst.FrameType)
        {
            DBG_TRACE("I frame");
            m_pSample->SetUINT32(MFSampleExtension_CleanPoint, TRUE);
        }
        else
        {
            DBG_TRACE("not I frame");
            m_pSample->SetUINT32(MFSampleExtension_CleanPoint, FALSE);
        }
        m_pSample->SetUINT32(MFSampleExtension_Discontinuity, (m_bGap)? TRUE: FALSE);
    }
    DBG_TRACE("-");
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFEncBitstream::Release(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    memset(&m_Bst, 0, sizeof(mfxBitstream));
    m_Bst.TimeStamp = MF_TIME_STAMP_INVALID;
    if (m_bLocked && m_pMediaBuffer)
    {
        m_pMediaBuffer->Unlock();
        m_bLocked = false;
    }
    SAFE_RELEASE(m_pMediaBuffer);
    SAFE_RELEASE(m_pSample);
    // allocating new buffer
    sts = Alloc();
    if (MFX_ERR_NONE != sts) hr = E_FAIL;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}
