/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_yuv_ibuf.cpp

Purpose: define code for support of input buffering in Intel MediaFoundation
encoder and vpp plug-ins.

*********************************************************************************/

#include "mf_yuv_ibuf.h"
#include "mf_vbuf.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Plug-in"

/*------------------------------------------------------------------------------*/

MFYuvInSurface::MFYuvInSurface(void):
    m_pSample(NULL),
    m_pMediaBuffer(NULL),
    m_p2DBuffer(NULL),
    m_pComMfxSurface(NULL),
    m_bAllocated(false),
    m_bLocked(false),
    m_bReleased(true),
    m_bFake(false),
    m_bInitialized(false),
    m_pVideo16(NULL),
    m_nPitch(0),
    m_dbg_file(NULL)
{
    DBG_TRACE("+");
    memset((void*)&m_mfxSurface, 0, sizeof(mfxFrameSurface1));
    memset((void*)&m_mfxInSurface, 0, sizeof(mfxFrameSurface1));
    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

MFYuvInSurface::~MFYuvInSurface(void)
{
    DBG_TRACE("+");
    if (m_bLocked && m_pMediaBuffer)
    {
        m_pMediaBuffer->Unlock();
        m_bLocked = false;
    }
    if (m_bLocked && m_p2DBuffer)
    {
        m_p2DBuffer->Unlock2D();
        m_bLocked = false;
    }
    SAFE_RELEASE(m_pComMfxSurface);
    SAFE_RELEASE(m_pMediaBuffer);
    SAFE_RELEASE(m_p2DBuffer);
    SAFE_RELEASE(m_pSample);
    SAFE_FREE(m_pVideo16);
    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

// Notes:
//  - pInfo - on surface with such info MediaSDK can work
//  - pInInfo - this info of surface which actually arrived
//  - pInfo may differ from pInInfo only in width and height fields,
// cropping parameters are equal; besides width and height in pInInfo
// always le than in pInfo
mfxStatus MFYuvInSurface::Init(mfxFrameInfo* pInfo,
                               mfxFrameInfo* pInInfo,
                               mfxMemId memid,
                               bool bAlloc)
{
    DBG_TRACE("+");
    mfxStatus sts = MFX_ERR_NONE;

    CHECK_EXPRESSION(!m_bInitialized, MFX_ERR_ABORTED);
    CHECK_POINTER(pInfo, MFX_ERR_NULL_PTR);
    if (memid) bAlloc = false; // no allocations in HW mode

    memcpy(&(m_mfxSurface.Info), pInfo, sizeof(mfxFrameInfo));
    if (pInInfo) memcpy(&(m_mfxInSurface.Info), pInInfo, sizeof(mfxFrameInfo));
    else memcpy(&(m_mfxInSurface.Info), pInfo, sizeof(mfxFrameInfo));

    CHECK_EXPRESSION((MFX_FOURCC_NV12 == m_mfxSurface.Info.FourCC || 
                      MFX_FOURCC_YUY2 == m_mfxSurface.Info.FourCC), MFX_ERR_ABORTED);
    CHECK_EXPRESSION((MFX_CHROMAFORMAT_YUV420 == m_mfxSurface.Info.ChromaFormat ||
                      MFX_CHROMAFORMAT_YUV422 == m_mfxSurface.Info.ChromaFormat), MFX_ERR_ABORTED);

    memset(&(m_mfxSurface.Data),0,sizeof(mfxFrameData));

    if (memid)
    {
        m_bAllocated = true;
        m_mfxSurface.Data.MemId = memid;
    }
    else
    {
        mfxU16 align = m_gMemAlignDec; // default: request for [vpp/enc in] srf from our decoder

        if (bAlloc) align = m_gMemAlignEnc; // request for [vpp out] = [enc in] srf
        else if (pInInfo) align = m_gMemAlignVpp; // request for [vpp/enc in] srf from not our decoder
        m_nPitch = (mfxU16)(MEM_ALIGN(m_mfxSurface.Info.Width, align));

        if (bAlloc || pInInfo &&
                      (m_mfxInSurface.Info.Width < m_mfxSurface.Info.Width) ||
                      (m_mfxInSurface.Info.Height < m_mfxSurface.Info.Height))
        {
            switch(m_mfxSurface.Info.FourCC)
            {
                case MFX_FOURCC_NV12:
                    m_pVideo16 = (mfxU8*)calloc(3*m_nPitch*m_mfxSurface.Info.Height/2,sizeof(mfxU8));
                    break;
                case MFX_FOURCC_YUY2:
                    m_pVideo16 = (mfxU8*)calloc(2*m_nPitch*m_mfxSurface.Info.Height,sizeof(mfxU8));
                    break;
            };
            
            CHECK_POINTER(m_pVideo16, MFX_ERR_MEMORY_ALLOC);
        }
        if (bAlloc)
        {
            m_bAllocated = true;

            switch(m_mfxSurface.Info.FourCC)
            {
                case MFX_FOURCC_NV12:
                    m_mfxSurface.Data.Y     = m_pVideo16;
                    m_mfxSurface.Data.UV    = m_pVideo16 + m_nPitch * m_mfxSurface.Info.Height;
                    m_mfxSurface.Data.Pitch = m_nPitch;
                    break;
                case MFX_FOURCC_YUY2:
                    m_mfxSurface.Data.Y     = m_pVideo16;                    
                    m_mfxSurface.Data.U     = m_pVideo16 + 1;
                    m_mfxSurface.Data.V     = m_pVideo16 + 3;
                    m_mfxSurface.Data.Pitch = 2 * m_nPitch;
                    break;
            };
        }
    }
    CHECK_EXPRESSION(MFX_ERR_NONE == sts, sts);
    m_bInitialized = true;
    DBG_TRACE("-");
    return MFX_ERR_NONE;
}

/*--------------------------------------------------------------------*/

void MFYuvInSurface::Close(void)
{
    DBG_TRACE("+");
    m_bInitialized = false;
    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

void MFYuvInSurface::DropDataToFile(void)
{
    DBG_TRACE("+");
    if (m_dbg_file)
    {
        IDirect3DSurface9* pSurface = mf_get_d3d_srf_from_mid(m_mfxSurface.Data.MemId);

        if (pSurface)
        {
            HRESULT hr_sts = S_OK;
            D3DSURFACE_DESC d3d_desc;
            D3DLOCKED_RECT  d3d_rect = {NULL,0};

            memset(&d3d_desc, 0, sizeof(D3DSURFACE_DESC));
            if (SUCCEEDED(hr_sts)) hr_sts = pSurface->GetDesc(&d3d_desc);
            if (SUCCEEDED(hr_sts)) hr_sts = pSurface->LockRect(&d3d_rect, NULL, D3DLOCK_READONLY);
            DBG_TRACE_2("HW surface: Format         = %x (dec = %d)", d3d_desc.Format, d3d_desc.Format);
            DBG_TRACE_2("HW surface: Format_NV12    = %x (dec = %d)", MAKEFOURCC('N', 'V', '1', '2'), MAKEFOURCC('N', 'V', '1', '2'));
            DBG_TRACE_2("HW surface: Width x Height = %d x %d", d3d_desc.Width, d3d_desc.Height);
            DBG_TRACE_1("HW surface: Pitch          = %d", d3d_rect.Pitch);
            if (SUCCEEDED(hr_sts) && (MAKEFOURCC('N', 'V', '1', '2') == d3d_desc.Format))
            {
                mfxFrameInfo info;

                memset(&info, 0, sizeof(mfxFrameInfo));
                info.Width  = (mfxU16)d3d_desc.Width;
                info.Height = (mfxU16)d3d_desc.Height;
                info.CropW  = (mfxU16)d3d_desc.Width;
                info.CropH  = (mfxU16)d3d_desc.Height;
                mf_dump_YUV_from_NV12_data(m_dbg_file, (mfxU8*)d3d_rect.pBits, &info, d3d_rect.Pitch);
            }
            if (SUCCEEDED(hr_sts)) hr_sts = pSurface->UnlockRect();
            DBG_TRACE_1("hr_sts = %x (HW surface dumping)", hr_sts);
        }
        else
        {
            mf_dump_YUV_from_NV12_data(m_dbg_file, m_mfxSurface.Data.Y, &(m_mfxInSurface.Info), m_nPitch);
            fflush(m_dbg_file);
        }
        SAFE_RELEASE(pSurface);
    }
    DBG_TRACE("-");
}

/*--------------------------------------------------------------------*/

HRESULT MFYuvInSurface::LoadSW(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxU8*  pData = NULL;
    mfxU32  uiDataLen = 0, uiDataSize = 0;
    LONG    uiPitch = 0;
    mfxU16  nWidth = 0, nHeight = 0;
    mfxU16  nOWidth = 0, nOHeight = 0; // O = Original
    mfxU16  nCropX = 0, nCropY  = 0;
    mfxU16  nCropW = 0, nCropH  = 0;

    if (SUCCEEDED(hr))
    {
        m_pMediaBuffer->QueryInterface(IID_IMF2DBuffer, (void**)&m_p2DBuffer);
        if (m_p2DBuffer)
        {
            hr = m_p2DBuffer->Lock2D((BYTE**)&pData, &uiPitch);
        }
        else
        {
            hr = m_pMediaBuffer->Lock((BYTE**)&pData, (DWORD*)&uiDataLen, (DWORD*)&uiDataSize);
            uiPitch = m_mfxInSurface.Info.Width;
        }
    }
    m_bLocked = SUCCEEDED(hr);
    if (SUCCEEDED(hr) && (uiPitch != m_nPitch))
    {
        // should not occur in case of our decoder-(vpp)-encoder
        if (!m_pVideo16)
        {
            m_pVideo16 = (mfxU8*)calloc(3*m_nPitch*m_mfxSurface.Info.Height/2,sizeof(mfxU8));
            CHECK_POINTER(m_pVideo16, MFX_ERR_MEMORY_ALLOC);
        }
    }
    if (SUCCEEDED(hr))
    {
        nWidth   = m_mfxSurface.Info.Width;
        nHeight  = m_mfxSurface.Info.Height;
        nOWidth  = m_mfxInSurface.Info.Width;
        nOHeight = m_mfxInSurface.Info.Height;
        nCropX   = m_mfxSurface.Info.CropX;
        nCropY   = m_mfxSurface.Info.CropY;
        nCropW   = m_mfxSurface.Info.CropW;
        nCropH   = m_mfxSurface.Info.CropH;

        switch(m_mfxSurface.Info.FourCC)
        {
        case MFX_FOURCC_NV12:
            {
                if (!m_pVideo16)
                {
                    m_mfxSurface.Data.Y  = pData;
                    m_mfxSurface.Data.UV = pData + m_nPitch * nHeight;
                }
                else
                {
                    mfxU16 i = 0;

                    m_mfxSurface.Data.Y  = m_pVideo16;
                    m_mfxSurface.Data.UV = m_pVideo16 + m_nPitch * nHeight;
                    m_mfxInSurface.Data.Y  = pData;
                    m_mfxInSurface.Data.UV = pData + uiPitch * nOHeight;
                    for (i = 0; i < nCropH/2; ++i)
                    {
                        // copying Y
                        memcpy(m_mfxSurface.Data.Y + nCropX + (nCropY + i)*m_nPitch,
                               m_mfxInSurface.Data.Y + nCropX + (nCropY + i)*uiPitch,
                               nCropW);
                        // copying UV
                        memcpy(m_mfxSurface.Data.UV + nCropX + (nCropY/2 + i)*m_nPitch,
                               m_mfxInSurface.Data.UV + nCropX + (nCropY/2 + i)*uiPitch,
                               nCropW);
                    }
                    for (i = nCropH/2; i < nCropH; ++i)
                    {
                        // copying Y
                        memcpy(m_mfxSurface.Data.Y + nCropX + (nCropY + i)*m_nPitch,
                               m_mfxInSurface.Data.Y + nCropX + (nCropY + i)*uiPitch,
                               nCropW);
                    }
                }

                //Set Pitch
                m_mfxSurface.Data.Pitch = m_nPitch; //factor = 1 for NV12 and YV12
                break;
            }
        case MFX_FOURCC_YUY2:
            {
                if (!m_pVideo16)
                {
                    m_mfxSurface.Data.Y = pData;
                    m_mfxSurface.Data.U = pData + 1;
                    m_mfxSurface.Data.V = pData + 3;
                }
                else
                {
                    m_mfxSurface.Data.Y = m_pVideo16;
                    m_mfxSurface.Data.U = m_pVideo16 + 1;
                    m_mfxSurface.Data.V = m_pVideo16 + 3;

                    m_mfxInSurface.Data.Y = pData;
                    m_mfxInSurface.Data.U = pData + 1;
                    m_mfxInSurface.Data.V = pData + 3;

                    for (mfxU16 i = 0; i < nCropH; ++i)
                    {
                        memcpy(m_mfxSurface.Data.Y + nCropX + (nCropY + i) * 2 * m_nPitch,
                               m_mfxInSurface.Data.Y + nCropX + (nCropY + i) * 2 * uiPitch,
                               2 * nCropW);
                    }
                }

                //Set Pitch
                m_mfxSurface.Data.Pitch = 2 * m_nPitch; //factor = 2 for YUY2

                break;
            }
        default:
            {
                // will not occurr
                break;
            }
        }        
        DropDataToFile();
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*--------------------------------------------------------------------*/

HRESULT MFYuvInSurface::PseudoLoad(void)
{
    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    CHECK_EXPRESSION(!m_mfxSurface.Data.Locked, E_FAIL);

    m_bReleased = false;
    return S_OK;
}

/*--------------------------------------------------------------------*/

HRESULT MFYuvInSurface::Load(IMFSample* pSample, bool bIsIntetnalSurface)
{
    DBG_TRACE("+");
    HRESULT  hr = S_OK;
    DWORD    cBuffers = 0;
    LONGLONG hnsTime = -1;
    UINT32 bSingleField = false; // (!) should be false
    UINT32 bInterlaced = false, bBFF = false, bRepeated = false;
    mfxFrameSurface1* srf = &m_mfxSurface;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    CHECK_EXPRESSION(!m_bAllocated, E_FAIL); // we do not support loading under alloc mode
    CHECK_EXPRESSION(!m_bLocked, E_FAIL);
    CHECK_EXPRESSION(!m_pComMfxSurface, E_FAIL);
    CHECK_POINTER(pSample, E_POINTER);

    pSample->AddRef();
    m_pSample = pSample;
    m_bReleased = false;

    if (SUCCEEDED(hr)) pSample->GetUINT32(MFSampleExtension_SingleField, &bSingleField);
    CHECK_EXPRESSION(!bSingleField, E_FAIL); // single fields are not supported
    if (SUCCEEDED(hr)) hr = pSample->GetBufferCount(&cBuffers);
    CHECK_EXPRESSION(1 == cBuffers, E_FAIL);

    if (SUCCEEDED(hr) && bIsIntetnalSurface)
    {
        HRESULT hr_sts = S_OK;
        UINT32 bFake = false;

        hr_sts = m_pSample->GetUnknown(MF_MT_MFX_FRAME_SRF,
                                       IID_IMfxFrameSurface,
                                       (void**)&m_pComMfxSurface);

        if (SUCCEEDED(m_pSample->GetUINT32(MF_MT_FAKE_SRF, &bFake)))
        {
            m_bFake = (TRUE == bFake)? true: false;
        }
        else m_bFake = false;
    }
    if (m_pComMfxSurface)
    { // loading shared surface
        srf = m_pComMfxSurface->GetMfxFrameSurface();
    }
    else
    { // loading data from sample
        if (SUCCEEDED(hr) && srf->Data.Locked) hr = E_FAIL;
        if (SUCCEEDED(hr)) hr = pSample->GetBufferByIndex(0, &m_pMediaBuffer);
        if (SUCCEEDED(hr)) hr = LoadSW();
        // loading time
        if (SUCCEEDED(pSample->GetSampleTime(&hnsTime)))
        {
            srf->Data.TimeStamp = REF2MFX_TIME(hnsTime);
        }
        else
        {
            srf->Data.TimeStamp = MF_TIME_STAMP_INVALID;
        }
        pSample->GetUINT32(MFSampleExtension_Interlaced, &bInterlaced);
        DBG_TRACE_1("bInterlaced = %d", bInterlaced);
        if (bInterlaced)
        { // interlacing
            pSample->GetUINT32(MFSampleExtension_BottomFieldFirst, &bBFF);
            pSample->GetUINT32(MFSampleExtension_RepeatFirstField, &bRepeated);

            DBG_TRACE_1("bBFF = %d", bBFF);
            DBG_TRACE_1("bRepeated = %d", bRepeated);

            srf->Info.PicStruct = (bBFF)? (mfxU16)MFX_PICSTRUCT_FIELD_BFF:
                                          (mfxU16)MFX_PICSTRUCT_FIELD_TFF;
            if (bRepeated) srf->Info.PicStruct |= MFX_PICSTRUCT_FIELD_REPEATED;
        }
        else
        {
            srf->Info.PicStruct = MFX_PICSTRUCT_PROGRESSIVE;
        }
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*--------------------------------------------------------------------*/
// Release of objects
// Surface should be unlocked by encoder or vpp

HRESULT MFYuvInSurface::Release(void)
{
    DBG_TRACE("+");
    mfxFrameSurface1* srf = (m_pComMfxSurface)? m_pComMfxSurface->GetMfxFrameSurface(): &m_mfxSurface;

    CHECK_EXPRESSION(m_bInitialized, E_FAIL);
    CHECK_EXPRESSION(!m_bReleased, S_OK);
    CHECK_EXPRESSION(!srf->Data.Locked, E_FAIL); // this check can (and maybe should - for better performance) be deleted
    if (!m_bAllocated)
    {
        if (m_bLocked && m_pMediaBuffer)
        {
            m_pMediaBuffer->Unlock();
            m_bLocked = false;
        }
        if (m_bLocked && m_p2DBuffer)
        {
            m_p2DBuffer->Unlock2D();
            m_bLocked = false;
        }
        SAFE_RELEASE(m_pComMfxSurface);
        SAFE_RELEASE(m_pMediaBuffer);
        SAFE_RELEASE(m_p2DBuffer);
        SAFE_RELEASE(m_pSample);
    }
    m_bReleased = true;
    DBG_TRACE("-");
    return S_OK;
}
