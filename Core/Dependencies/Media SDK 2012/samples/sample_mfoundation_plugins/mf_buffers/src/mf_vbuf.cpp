/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_vbuf.cpp

Purpose: define MF Video Buffer class.

Notes:
  - "Pretend" feature is workaround to avoid input type changing on MSFT encoder.

*********************************************************************************/

#include "mf_vbuf.h"

/*------------------------------------------------------------------------------*/

mfxU16 m_gMemAlignDec = 128; // [dec/vpp out]: SYS mem align value
mfxU16 m_gMemAlignVpp = 1;   // [enc/vpp in]:  SYS mem align value
mfxU16 m_gMemAlignEnc = 1;   // [enc in = vpp out]: SYS mem align value

/*------------------------------------------------------------------------------*/

HRESULT MFCreateVideoBuffer(mfxU16 w, mfxU16 h, mfxU16 p, mfxU32 fourcc,
                            IMFMediaBuffer** ppBuffer)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    MFVideoBuffer* pBuf = NULL;

    CHECK_POINTER(ppBuffer, E_POINTER);

    SAFE_NEW(pBuf, MFVideoBuffer);
    CHECK_EXPRESSION(pBuf, E_OUTOFMEMORY);
    hr = pBuf->Alloc(w, h, p, fourcc);
    if (SUCCEEDED(hr)) hr = pBuf->QueryInterface(IID_IMFMediaBuffer, (void**)ppBuffer);
    if (FAILED(hr))
    {
        SAFE_DELETE(pBuf);
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFCreateMfxFrameSurface(IMfxFrameSurface** ppSurface)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    MFMfxFrameSurface* pSrf = NULL;

    CHECK_POINTER(ppSurface, E_POINTER);

    SAFE_NEW(pSrf, MFMfxFrameSurface);
    CHECK_EXPRESSION(pSrf, E_OUTOFMEMORY);
    if (SUCCEEDED(hr)) hr = pSrf->QueryInterface(IID_IMfxFrameSurface, (void**)ppSurface);
    if (FAILED(hr))
    {
        SAFE_DELETE(pSrf);
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/
//MFMfxFrameSurface

MFMfxFrameSurface::MFMfxFrameSurface(void):
    m_nRefCount(0)
{
    memset(&m_mfxSurface, 0, sizeof(mfxFrameSurface1));
    DllAddRef();
}

/*------------------------------------------------------------------------------*/

MFMfxFrameSurface::~MFMfxFrameSurface(void)
{
    DllRelease();
}

/*------------------------------------------------------------------------------*/
// IUnknown methods

ULONG MFMfxFrameSurface::AddRef(void)
{
    return InterlockedIncrement(&m_nRefCount);
}

ULONG MFMfxFrameSurface::Release(void)
{
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT MFMfxFrameSurface::QueryInterface(REFIID iid, void** ppv)
{
    if (!ppv) return E_POINTER;
    if ((iid == IID_IUnknown) || (iid == IID_IMfxFrameSurface))
    {
        *ppv = static_cast<IMfxFrameSurface*>(this);
    }
    else
    {
        return E_NOINTERFACE;
    }
    AddRef();
    return S_OK;
}

/*------------------------------------------------------------------------------*/

mfxFrameSurface1* MFMfxFrameSurface::GetMfxFrameSurface(void)
{
    return &m_mfxSurface;
}

/*------------------------------------------------------------------------------*/
// MFVideoBuffer

MFVideoBuffer::MFVideoBuffer(void):
    m_nRefCount(0),
    m_pData(NULL),
    m_pCData(NULL),
    m_nWidth(0),
    m_nHeight(0),
    m_nPitch(0),
    m_FourCC(0),
    m_nLength(0),
    m_nCLength(0),
    m_bPretend(false),
    m_nPretendWidth(0),
    m_nPretendHeight(0)
{
    DBG_TRACE("~");
}

/*------------------------------------------------------------------------------*/

MFVideoBuffer::~MFVideoBuffer(void)
{
    DBG_TRACE("+");
    Free();
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

HRESULT MFVideoBuffer::Alloc(mfxU16 w, mfxU16 h, mfxU16 p, mfxU32 fourcc)
{
    DBG_TRACE("+");
    mfxU32 length = 3*p*h/2;
    CHECK_EXPRESSION(w && h && p && (w <= p), E_FAIL);
    CHECK_EXPRESSION((MFX_FOURCC_NV12 == fourcc), E_FAIL);
    CHECK_EXPRESSION(!m_pData, E_FAIL);

    if (!m_pData) m_pData = (mfxU8*)malloc(length);
    CHECK_EXPRESSION(m_pData, E_OUTOFMEMORY);
    m_FourCC   = fourcc;
    m_nWidth   = w;
    m_nHeight  = h;
    m_nPitch   = p;
    m_nLength  = length;
    m_nCLength = 3*w*h/2;
    m_nPretendWidth  = w;
    m_nPretendHeight = h;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFVideoBuffer::Pretend(mfxU16 w, mfxU16 h)
{
    DBG_TRACE("+");
    CHECK_EXPRESSION(h && w, E_FAIL);

    m_nPretendWidth  = w;
    m_nPretendHeight = h;
    m_nCLength       = 3*w*h/2;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

void MFVideoBuffer::Free(void)
{
    DBG_TRACE("+");
    SAFE_FREE(m_pCData);
    SAFE_FREE(m_pData);
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/
// IUnknown methods

ULONG MFVideoBuffer::AddRef(void)
{
    DBG_TRACE_1("RefCount = %d", m_nRefCount+1);
    return InterlockedIncrement(&m_nRefCount);
}

ULONG MFVideoBuffer::Release(void)
{
    DBG_TRACE("+");
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    DBG_TRACE_1("RefCount = %d: -", uCount);
    if (uCount == 0) delete this;
    // For thread safety, return a temporary variable.
    return uCount;
}

HRESULT MFVideoBuffer::QueryInterface(REFIID iid, void** ppv)
{
    DBG_TRACE("+");
    if (!ppv) return E_POINTER;
    if ((iid == IID_IUnknown) || (iid == IID_IMFMediaBuffer))
    {
        DBG_TRACE("IUnknown or IMFMediaBuffer");
        *ppv = static_cast<IMFMediaBuffer*>(this);
    }
    else if (iid == IID_IMF2DBuffer)
    {
        DBG_TRACE("IMF2DBuffer");
        *ppv = static_cast<IMF2DBuffer*>(this);
    }
    else if (iid == IID_MFVideoBuffer)
    {
        DBG_TRACE("MFVideoBuffer");
        *ppv = static_cast<MFVideoBuffer*>(this);
    }
    else
    {
        DBG_TRACE_1("not found: %08x: -", iid.Data1);
        return E_NOINTERFACE;
    }
    AddRef();
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

// IMFMediaBuffer methods
STDMETHODIMP MFVideoBuffer::Lock(BYTE** ppbBuffer,
                                 DWORD* pcbMaxLength,
                                 DWORD* pcbCurrentLength)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(ppbBuffer, E_POINTER);
//    CHECK_POINTER(pcbMaxLength, E_POINTER); // can be NULL by MSFT spec
//    CHECK_POINTER(pcbCurrentLength, E_POINTER); // can be NULL by MSFT spec

    if ((m_nPitch > m_nWidth) || m_bPretend)
    {
        mfxU32 h = (m_bPretend)? m_nPretendHeight: m_nHeight;
        mfxU32 w = (m_bPretend)? m_nPretendWidth: m_nWidth;
        mfxU32 length = 3*w*h/2;

        if (!m_pCData || (length != m_nCLength))
        {
            SAFE_FREE(m_pCData);
            m_pCData = (mfxU8*)calloc(length, sizeof(mfxU8));
        }
        if (!m_pCData) hr = E_OUTOFMEMORY;
        else
        {
            mfxU32 i = 0;
            mfxU8 *buf_src = NULL, *buf_dst = NULL;
            mfxU32 copy_h = (m_nHeight < m_nPretendHeight)? m_nHeight: m_nPretendHeight;
            mfxU32 copy_w = (m_nWidth < m_nPretendWidth)? m_nWidth: m_nPretendWidth;

            m_nCLength = length;
            for (i = 0; i < copy_h; ++i)
            {
                memcpy(m_pCData + i*w, m_pData + i*m_nPitch, copy_w);
            }
            buf_src = m_pData + m_nPitch*m_nHeight;
            buf_dst = m_pCData + w*h;
            for (i = 0; i < (mfxU32)copy_h/2; ++i)
            {
                memcpy(buf_dst + i*w, buf_src + i*m_nPitch, copy_w);
            }
            *ppbBuffer = m_pCData;
        }
    }
    else *ppbBuffer = m_pData;
    if (pcbMaxLength) *pcbMaxLength = m_nCLength;
    if (pcbCurrentLength) *pcbCurrentLength = m_nCLength;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::Unlock(void)
{
    DBG_TRACE("~");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::GetCurrentLength(DWORD* pcbCurrentLength)
{
    DBG_TRACE("+");
    mfxU32 w = (m_bPretend)? m_nPretendWidth: m_nWidth;
    mfxU32 h = (m_bPretend)? m_nPretendHeight: m_nHeight;
        
    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(pcbCurrentLength, E_POINTER);
    *pcbCurrentLength = 3*w*h/2;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::SetCurrentLength(DWORD /*cbCurrentLength*/)
{
    DBG_TRACE("~");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::GetMaxLength(DWORD* pcbMaxLength)
{
    DBG_TRACE("+");
    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(pcbMaxLength, E_POINTER);
    *pcbMaxLength = m_nCLength;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

// IMF2DBuffer methods
STDMETHODIMP MFVideoBuffer::Lock2D(BYTE **pbScanline0,
                                   LONG *plPitch)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(pbScanline0, E_POINTER);
    CHECK_POINTER(plPitch, E_POINTER);
    
    if (!m_bPretend)
    {
        *pbScanline0 = m_pData;
        *plPitch = m_nPitch;
    }
    else
    {
        DWORD max_len = 0, cur_len = 0;

        hr = Lock(pbScanline0, &max_len, &cur_len);
        *plPitch = m_nPretendWidth;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::Unlock2D(void)
{
    DBG_TRACE("~");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::GetScanline0AndPitch(BYTE **pbScanline0,
                                                 LONG *plPitch)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(pbScanline0, E_POINTER);
    CHECK_POINTER(plPitch, E_POINTER);
    
    if (!m_bPretend)
    {
        *pbScanline0 = m_pData;
        *plPitch = m_nPitch;
    }
    else
    {
        DWORD max_len = 0, cur_len = 0;

        hr = Lock(pbScanline0, &max_len, &cur_len);
        *plPitch = m_nPretendWidth;
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::IsContiguousFormat(BOOL *pfIsContiguous)
{
    DBG_TRACE("+");
    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(pfIsContiguous, E_POINTER);
    if ((m_nPitch > m_nWidth) || m_bPretend) *pfIsContiguous = FALSE;
    else *pfIsContiguous = TRUE;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::GetContiguousLength(DWORD *pcbLength)
{
    DBG_TRACE("+");
    mfxU32 w = (m_bPretend)? m_nPretendWidth: m_nWidth;
    mfxU32 h = (m_bPretend)? m_nPretendHeight: m_nHeight;

    CHECK_EXPRESSION(m_pData, E_FAIL);
    CHECK_POINTER(pcbLength, E_POINTER);
    *pcbLength = 3*w*h/2;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::ContiguousCopyTo(BYTE* /*pbDestBuffer*/,
                                             DWORD /*cbDestBuffer*/)
{
    DBG_TRACE("~");
    return E_NOTIMPL;
}

/*------------------------------------------------------------------------------*/

STDMETHODIMP MFVideoBuffer::ContiguousCopyFrom(const BYTE* /*pbSrcBuffer*/,
                                               DWORD /*cbSrcBuffer*/)
{
    DBG_TRACE("~");
    return E_NOTIMPL;
}
