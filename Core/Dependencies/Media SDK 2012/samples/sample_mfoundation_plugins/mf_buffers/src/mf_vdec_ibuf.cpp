/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mf_vdec_ibuf.cpp

Purpose: define code for support of input buffering in Intel MediaFoundation
decoder plug-ins.

*********************************************************************************/

#include "mf_vdec_ibuf.h"

/*------------------------------------------------------------------------------*/

#undef  DBG_MODULE_NAME
#define DBG_MODULE_NAME L"Unknown Decoder"

/*------------------------------------------------------------------------------*/

MFDecBitstream::MFDecBitstream(HRESULT &hr):
    m_pBst(NULL),
    m_pBstBuf(NULL),
    m_pBstIn(NULL),
    m_bNeedFC(false),
    m_bNoData(false),
    m_pSample(NULL),
    m_pMediaBuffer(NULL),
    m_nBuffersCount(0),
    m_nBufferIndex(0),
    m_SampleTime(MF_TIME_STAMP_INVALID),
    m_nBstBufReallocs(0),
    m_nBstBufCopyBytes(0),
    m_dbg_file(NULL),
    m_dbg_file_fc(NULL)
{
    DBG_TRACE("+");
    hr = E_FAIL;
    m_pBstBuf = (mfxBitstream*)calloc(1, sizeof(mfxBitstream));
    if (!m_pBstBuf) return;
    m_pBstIn = (mfxBitstream*)calloc(1, sizeof(mfxBitstream));
    if (!m_pBstIn) return;
    memset(&m_VideoParams, 0, sizeof(mfxVideoParam));
    hr = S_OK;
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

MFDecBitstream::~MFDecBitstream(void)
{
    DBG_TRACE("+");
    if (m_pBstBuf)
    {
        DBG_TRACE_1("m_pBstBuf->MaxLength = %d", m_pBstBuf->MaxLength);
        DBG_TRACE_1("m_nBstBufReallocs = %d", m_nBstBufReallocs);
        DBG_TRACE_1("m_nBstBufCopyBytes = %d", m_nBstBufCopyBytes);

        SAFE_FREE(m_pBstBuf->Data);
        SAFE_FREE(m_pBstBuf);
    }
    SAFE_FREE(m_pBstIn);
    DBG_TRACE("-");
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::Reset(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_POINTER(m_pBstBuf, E_POINTER);
    CHECK_POINTER(m_pBstIn, E_POINTER);

    m_pBst = NULL;
    memset(m_pBstIn, 0, sizeof(mfxBitstream));
    m_pBstBuf->DataOffset = 0;
    m_pBstBuf->DataLength = 0;
    m_pBstBuf->TimeStamp  = 0;
    // the following code is just in case (it should occur)
    if (m_pMediaBuffer) m_pMediaBuffer->Unlock();
    SAFE_RELEASE(m_pMediaBuffer);
    m_nBuffersCount = 0;
    m_nBufferIndex  = 0;
    m_SampleTime    = MF_TIME_STAMP_INVALID;
    SAFE_RELEASE(m_pSample);

    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

static inline mfxU32 BstGetValue32(mfxU8* pBuf)
{
    if (!pBuf) return 0;
    return ((*pBuf) << 24) | (*(pBuf + 1) << 16) | (*(pBuf + 2) << 8) | (*(pBuf + 3));
}

/*------------------------------------------------------------------------------*/

static inline void BstSetValue32(mfxU32 nValue, mfxU8* pBuf)
{
    if (pBuf)
    {
        mfxU32 i = 0;
        for (i = 0; i < 4; ++i)
        {
            *pBuf = (mfxU8)(nValue >> (8 * i));
            ++pBuf;
        }
    }
    return;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::BstBufRealloc(mfxU32 add_size)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxU32  needed_MaxLength = 0;

    CHECK_POINTER(m_pBstBuf, E_POINTER);
    if (add_size)
    {
        needed_MaxLength = m_pBstBuf->DataLength + add_size;
        if (m_pBstBuf->MaxLength < needed_MaxLength)
        { // increasing buffer capacity if needed
            // statistics (for successfull operation)
            m_nBstBufCopyBytes += m_pBstBuf->MaxLength;
            ++m_nBstBufReallocs;

            m_pBstBuf->Data = (mfxU8*)realloc(m_pBstBuf->Data, needed_MaxLength);
            m_pBstBuf->MaxLength = needed_MaxLength;
        }
        if (!(m_pBstBuf->Data))
        {
            m_pBstBuf->MaxLength = 0;
            hr = E_OUTOFMEMORY;
        }
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::BstBufMalloc(mfxU32 new_size)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    mfxU32  needed_MaxLength = 0;

    CHECK_POINTER(m_pBstBuf, E_POINTER);
    if (new_size)
    {
        needed_MaxLength = new_size;
        if (m_pBstBuf->MaxLength < needed_MaxLength)
        { // increasing buffer capacity if needed
            SAFE_FREE(m_pBstBuf->Data);
            m_pBstBuf->Data = (mfxU8*)malloc(needed_MaxLength);
            m_pBstBuf->MaxLength = needed_MaxLength;
            ++m_nBstBufReallocs;
        }
        if (!(m_pBstBuf->Data))
        {
            m_pBstBuf->MaxLength = 0;
            hr = E_OUTOFMEMORY;
        }
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

// NeedHeader: returns buffer size needed for the bitstream header
mfxU32 MFDecBitstream::BstBufNeedHdr(mfxU8* data, mfxU32 size)
{
    DBG_TRACE("+");

    CHECK_EXPRESSION(m_bNeedFC, 0);
    CHECK_EXPRESSION(data && size, 0);
    if (MFX_CODEC_VC1 == m_VideoParams.mfx.CodecId)
    {
        if ((MFX_PROFILE_VC1_SIMPLE == m_VideoParams.mfx.CodecProfile) ||
            (MFX_PROFILE_VC1_MAIN   == m_VideoParams.mfx.CodecProfile))
        {
            size = 8; // header will be: "<4 bytes: frame_size> 0x00000000"
        }
        else if (MFX_PROFILE_VC1_ADVANCED == m_VideoParams.mfx.CodecProfile)
        {
            CHECK_EXPRESSION(data && (size >= 4), 0);
            switch (BstGetValue32(data))
            {
            case 0x010A: case 0x010C: case 0x010D: case 0x010E: case 0x010F:
            case 0x011B: case 0x011C: case 0x011D: case 0x011E: case 0x011F:
                size = 0;
                break;
            default:
                size = 4; // header will be 0x0D010000
            }
        }
    }
    DBG_TRACE_1("size = %d: -", size);
    return size;
}

/*------------------------------------------------------------------------------*/

// AppendHeader: appends header to the buffered bistream
HRESULT MFDecBitstream::BstBufAppendHdr(mfxU8* /*data*/, mfxU32 /*size*/)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    CHECK_POINTER(m_pBstBuf, E_POINTER);
    if (MFX_CODEC_VC1 == m_VideoParams.mfx.CodecId)
    {
        if ((MFX_PROFILE_VC1_SIMPLE == m_VideoParams.mfx.CodecProfile) ||
            (MFX_PROFILE_VC1_MAIN   == m_VideoParams.mfx.CodecProfile))
        {
            hr = BstBufRealloc(8);
            if (SUCCEEDED(hr))
            {
                DWORD total_size = 0;
                mfxU8* buf = m_pBstBuf->Data + m_pBstBuf->DataOffset + m_pBstBuf->DataLength;

                if (m_pSample) m_pSample->GetTotalLength(&total_size);

                BstSetValue32(total_size, buf);
                BstSetValue32(0x00000000, buf + 4);
                m_pBstBuf->DataLength += 8;
                m_nBstBufCopyBytes += 8;
                if (m_dbg_file_fc)
                {
                    fwrite(buf, 1, 8, m_dbg_file_fc); fflush(m_dbg_file_fc);
                }
            }
        }
        else if (MFX_PROFILE_VC1_ADVANCED == m_VideoParams.mfx.CodecProfile)
        {
            hr = BstBufRealloc(4);
            if (SUCCEEDED(hr))
            {
                mfxU8* buf = m_pBstBuf->Data + m_pBstBuf->DataOffset + m_pBstBuf->DataLength;

                BstSetValue32(0x0D010000, buf);
                m_pBstBuf->DataLength += 4;
                m_nBstBufCopyBytes += 4;
                if (m_dbg_file_fc)
                {
                    fwrite(buf, 1, 4, m_dbg_file_fc); fflush(m_dbg_file_fc);
                }
            }
        }
    }
    else hr = E_FAIL;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::InitFC(mfxVideoParam* pVideoParams,
                               mfxU8* data, mfxU32 size)
{
    DBG_TRACE_1("size = %d: +", size);
    HRESULT hr = S_OK;

    CHECK_POINTER(pVideoParams, MFX_ERR_NULL_PTR);
    CHECK_POINTER(m_pBstBuf, E_POINTER);
    if ((MFX_CODEC_VC1 == pVideoParams->mfx.CodecId) &&
        (MFX_PROFILE_UNKNOWN != pVideoParams->mfx.CodecProfile))
    {
        m_bNeedFC = true;
        memcpy(&m_VideoParams, pVideoParams, sizeof(mfxVideoParam));
        if ((MFX_PROFILE_VC1_SIMPLE == m_VideoParams.mfx.CodecProfile) ||
            (MFX_PROFILE_VC1_MAIN   == m_VideoParams.mfx.CodecProfile))
        {
            hr = BstBufRealloc(8 + size + 12);
            if (SUCCEEDED(hr))
            {
                mfxU8* buf = m_pBstBuf->Data + m_pBstBuf->DataOffset + m_pBstBuf->DataLength;

                // set start code
                BstSetValue32(0xC5000000, buf);
                // set size of sequence header
                BstSetValue32(size,  buf + 4);
                // copy frame data
                memcpy(buf + 8, data, size);
                // set sizes to the end of data
                BstSetValue32(m_VideoParams.mfx.FrameInfo.CropH, buf + 8 + size);
                BstSetValue32(m_VideoParams.mfx.FrameInfo.CropW,  buf + 8 + size + 4);
                // set 0 to the last 4 bytes
                BstSetValue32(0, buf + 8 + size + 8);
                m_pBstBuf->DataLength += 8 + size + 12;
                m_nBstBufCopyBytes += 8 + size + 12;
                if (m_dbg_file_fc)
                {
                    fwrite(buf, 1, 8 + size + 12, m_dbg_file_fc);
                    fflush(m_dbg_file_fc);
                }
            }
        }
        else if (MFX_PROFILE_VC1_ADVANCED == m_VideoParams.mfx.CodecProfile)
        {
            hr = BstBufRealloc(size);
            if (SUCCEEDED(hr))
            {
                mfxU8* buf = m_pBstBuf->Data + m_pBstBuf->DataOffset + m_pBstBuf->DataLength;

                memcpy(buf, data, size);
                m_pBstBuf->DataLength += size;
                m_nBstBufCopyBytes += size;
                if (m_dbg_file_fc)
                {
                    fwrite(buf, 1, size, m_dbg_file_fc); fflush(m_dbg_file_fc);
                }
            }
        }
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::LoadNextBuffer(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    bool   bLocked = false, bUnlock = false;
    mfxU8* pData = NULL;
    mfxU32 uiDataLen = 0, uiDataSize = 0, hsize = 0;

    if (m_pSample && (m_nBufferIndex < m_nBuffersCount))
    {
        hr = m_pSample->GetBufferByIndex(m_nBufferIndex, &m_pMediaBuffer);
        if (SUCCEEDED(hr)) hr = m_pMediaBuffer->Lock((BYTE**)&pData, (DWORD*)&uiDataLen, (DWORD*)&uiDataSize);
        bLocked = SUCCEEDED(hr);
        if (SUCCEEDED(hr))
        {
            if (m_dbg_file)
            {
                fwrite(pData, 1, uiDataSize, m_dbg_file); fflush(m_dbg_file);
            }

            if (0 == m_nBufferIndex) hsize = BstBufNeedHdr(pData, uiDataSize);
            else hsize = 0;
            if (hsize || (m_pBstBuf->DataLength))
            {
                hr = BstBufRealloc(hsize + uiDataSize);
                if (hsize) hr = BstBufAppendHdr(pData, uiDataSize);
                if (SUCCEEDED(hr))
                {
                    mfxU8* buf = m_pBstBuf->Data + m_pBstBuf->DataOffset + m_pBstBuf->DataLength;

                    if (m_dbg_file_fc)
                    {
                        fwrite(pData, 1, uiDataSize, m_dbg_file_fc);
                        fflush(m_dbg_file_fc);
                    }
                    memcpy(buf, pData, uiDataSize);
                    m_pBstBuf->DataLength += uiDataSize;
                    m_nBstBufCopyBytes += uiDataSize;
                }
            }
            else
            {
                if (m_dbg_file_fc)
                {
                    fwrite(pData, 1, uiDataSize, m_dbg_file_fc);
                    fflush(m_dbg_file_fc);
                }
            }
            if (m_pBstBuf->DataLength) bUnlock = true;
            else bUnlock = false;
        }
        if (bUnlock || FAILED(hr))
        {
            if (bLocked) m_pMediaBuffer->Unlock();
            SAFE_RELEASE(m_pMediaBuffer);
        }
        ++m_nBufferIndex;
    }
    else hr = E_FAIL;
    if (SUCCEEDED(hr))
    {
        if (m_pBstBuf->DataLength) m_pBst = m_pBstBuf;
        else
        {
            m_pBstIn->DataLength = uiDataSize;
            m_pBstIn->MaxLength  = uiDataLen;
            m_pBstIn->Data       = pData;
            m_pBst = m_pBstIn;
        }
        m_pBst->TimeStamp = m_SampleTime;
    }
    else m_pBst = NULL;
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

mfxBitstream* MFDecBitstream::GetInternalMfxBitstream(void)
{
    DBG_TRACE("+");

    if (!m_bNoData && m_pBstBuf->DataLength) m_pBst = m_pBstBuf;
    DBG_TRACE_1("m_pBst = %p: -", m_pBst);
    return m_pBst;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::Load(IMFSample* pSample)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;
    REFERENCE_TIME time = 0;

    CHECK_POINTER(pSample, E_POINTER);
    CHECK_POINTER(m_pBstBuf, E_POINTER);
    CHECK_POINTER(m_pBstIn, E_POINTER);

    SAFE_RELEASE(m_pSample);
    pSample->AddRef();
    m_pSample = pSample;

    hr = m_pSample->GetBufferCount(&m_nBuffersCount);

    if (SUCCEEDED(m_pSample->GetSampleTime(&time)))
        m_SampleTime = REF2MFX_TIME(time);
    else
        m_SampleTime = MF_TIME_STAMP_INVALID;

    if (SUCCEEDED(hr)) hr = LoadNextBuffer();
    DBG_TRACE_1("SampleTime = %f", REF2SEC_TIME(MFX2REF_TIME(m_SampleTime)));
    DBG_TRACE_1("m_nBuffersCount = %d", m_nBuffersCount);
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::Unload(void)
{
    DBG_TRACE("+");
    SAFE_RELEASE(m_pSample);
    m_nBuffersCount = 0;
    m_nBufferIndex  = 0;
    m_SampleTime = MF_TIME_STAMP_INVALID;
    DBG_TRACE("S_OK: -");
    return S_OK;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::BstBufSync(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    if (SUCCEEDED(hr) && m_pBst)
    {
        if (m_pBst == m_pBstBuf)
        {
            if (m_pBstBuf->DataLength && m_pBstBuf->DataOffset)
            { // shifting data to the beginning of the buffer
                memmove(m_pBstBuf->Data, m_pBstBuf->Data + m_pBstBuf->DataOffset,
                    m_pBstBuf->DataLength);
                m_nBstBufCopyBytes += m_pBstBuf->DataLength;
            }
            m_pBstBuf->DataOffset = 0;
        }
        if ((m_pBst == m_pBstIn) && m_pBstIn->DataLength)
        { // copying data from m_pBstOut to m_pBstBuf
            // Note: here m_pBstBuf is empty
            hr = BstBufMalloc(m_pBstIn->DataLength);
            if (SUCCEEDED(hr))
            {
                memcpy(m_pBstBuf->Data, m_pBstIn->Data + m_pBstIn->DataOffset, m_pBstIn->DataLength);
                m_pBstBuf->DataOffset = 0;
                m_pBstBuf->DataLength = m_pBstIn->DataLength;
                m_pBstBuf->TimeStamp  = m_pBstIn->TimeStamp;
                m_nBstBufCopyBytes += m_pBstIn->DataLength;
            }
        }
    }
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}

/*------------------------------------------------------------------------------*/

HRESULT MFDecBitstream::Sync(void)
{
    DBG_TRACE("+");
    HRESULT hr = S_OK;

    hr = BstBufSync();
    m_pBst = NULL;
    if (m_pBstIn) memset(m_pBstIn, 0, sizeof(mfxBitstream));
    if (m_pMediaBuffer) m_pMediaBuffer->Unlock();
    SAFE_RELEASE(m_pMediaBuffer);

    // NOTE: LoadNextBuffer may fail if there is no more data in sample - that's
    // not an error
    if (SUCCEEDED(hr)) /*hr =*/ LoadNextBuffer();
    DBG_TRACE_1("hr = %x: -", hr);
    return hr;
}
