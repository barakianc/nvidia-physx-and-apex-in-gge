/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mfx_mft_vc1vd.cpp

Purpose: contains registration data for VC1 decoder MFT.

*********************************************************************************/

#define DBG_FILE_INIT
//#define DBG_FILE_NAME "C:\\mfx_mft_vc1vd.log"

#include "mf_utils.h"
#include "mfx_mft_vc1vd.h"

mfxStatus FillVC1DecoderParams(mfxVideoParam* pVideoParams)
{
    CHECK_POINTER(pVideoParams, MFX_ERR_NULL_PTR);

    pVideoParams->mfx.NumThread = (mfxU16)mf_get_cpu_num();
    pVideoParams->mfx.CodecId   = MFX_CODEC_VC1;
    pVideoParams->mfx.ExtendedPicStruct = 1;
    return MFX_ERR_NONE;
}

/*------------------------------------------------------------------------------*/

static const GUID_info g_InputTypes[] =
{
    { MFMediaType_Video, MEDIASUBTYPE_VC1_MFX },
    { MFMediaType_Video, MEDIASUBTYPE_VC1P_MFX },
    { MFMediaType_Video, WMMEDIASUBTYPE_WVC1 },
    { MFMediaType_Video, WMMEDIASUBTYPE_WMV3 }
};

static const GUID_info g_OutputTypes[] =
{
    { MFMediaType_Video, MFVideoFormat_NV12_MFX },
    { MFMediaType_Video, MFVideoFormat_NV12 },
    { MFMediaType_Video, MFVideoFormat_IYUV }
};

static ClassRegData g_RegData =
{
    (GUID*)&CLSID_MF_VC1DecFilter,
    TEXT(MFX_MF_PLUGIN_NAME),
    REG_AS_VIDEO_DECODER,
    CreateVDecPlugin,
    FillVC1DecoderParams,
    NULL,
    (GUID_info*)g_InputTypes, ARRAY_SIZE(g_InputTypes),
    (GUID_info*)g_OutputTypes, ARRAY_SIZE(g_OutputTypes),
    g_DecoderRegFlags
};

/*------------------------------------------------------------------------------*/

BOOL APIENTRY DllMain(HANDLE hModule,
                      DWORD  ul_reason_for_call,
                      LPVOID /*lpReserved*/)
{
    BOOL ret = false;
    HRESULT hr = S_OK;

    if (DLL_PROCESS_ATTACH == ul_reason_for_call)
    {
    }
    if (!ret) ret = myDllMain(hModule, ul_reason_for_call, &g_RegData, 1);
    if (DLL_PROCESS_DETACH == ul_reason_for_call)
    {
        SAFE_RELEASE(g_RegData.pAttributes);
    }
    return ret;
}
