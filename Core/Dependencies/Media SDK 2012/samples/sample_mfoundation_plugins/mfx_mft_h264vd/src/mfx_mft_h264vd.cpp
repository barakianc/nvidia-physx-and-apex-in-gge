/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mfx_mft_h264vd.cpp

Purpose: contains registration data for H.264 decoder MFT.

*********************************************************************************/

#define DBG_FILE_INIT
//#define DBG_FILE_NAME "C:\\mfx_mft_h264vd.log"

#include "mf_utils.h"
#include "mfx_mft_h264vd.h"

/*------------------------------------------------------------------------------*/

mfxStatus FillH264DecoderParams(mfxVideoParam* pVideoParams)
{
    CHECK_POINTER(pVideoParams, MFX_ERR_NULL_PTR);

    pVideoParams->mfx.NumThread = (mfxU16)mf_get_cpu_num();
    pVideoParams->mfx.CodecId   = MFX_CODEC_AVC;
    pVideoParams->mfx.ExtendedPicStruct = 1;
    return MFX_ERR_NONE;
}

/*------------------------------------------------------------------------------*/

static const GUID_info g_InputTypes[] =
{
    { MFMediaType_Video, MEDIASUBTYPE_H264 },
    { MFMediaType_Video, MEDIASUBTYPE_H264_MFX },
    { MFMediaType_Video, MEDIASUBTYPE_AVC1 }
};

static ClassRegData g_RegData =
{
    (GUID*)&CLSID_MF_H264DecFilter,
    TEXT(MFX_MF_PLUGIN_NAME),
    REG_AS_VIDEO_DECODER,
    CreateVDecPlugin,
    FillH264DecoderParams,
    NULL,
    (GUID_info*)g_InputTypes, ARRAY_SIZE(g_InputTypes),
    (GUID_info*)g_UncompressedVideoTypes, ARRAY_SIZE(g_UncompressedVideoTypes),
    g_DecoderRegFlags
};

/*------------------------------------------------------------------------------*/

BOOL APIENTRY DllMain(HANDLE hModule,
                      DWORD  ul_reason_for_call,
                      LPVOID /*lpReserved*/)
{
    BOOL ret = false;

    if (DLL_PROCESS_ATTACH == ul_reason_for_call)
    {
    }
    if (!ret) ret = myDllMain(hModule, ul_reason_for_call, &g_RegData, 1);
    if (DLL_PROCESS_DETACH == ul_reason_for_call)
    {
        SAFE_RELEASE(g_RegData.pAttributes);
    }
    return ret;
}
