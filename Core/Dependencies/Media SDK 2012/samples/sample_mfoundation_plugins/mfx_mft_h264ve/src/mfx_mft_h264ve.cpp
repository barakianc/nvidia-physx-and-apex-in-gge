/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************

File: mfx_mft_h264ve.cpp

Purpose: contains registration data for H.264 encoder MFT.

*********************************************************************************/

#define DBG_FILE_INIT
//#define DBG_FILE_NAME "C:\\mfx_mft_h264ve.log"

#include "mf_utils.h"
#include "mfx_mft_h264ve.h"

/*------------------------------------------------------------------------------*/

mfxStatus FillH264EncoderParams(mfxVideoParam* pVideoParams)
{
    CHECK_POINTER(pVideoParams, MFX_ERR_NULL_PTR);

    // Setting mimimum number of parameters:
    //  - NumThread: 1
    //  - CodecId: AVC: which encoder to use
    //  - TargetUsage: best speed: to mimimize number of used features
    //  - RateControlMethod: constant bitrate
    //  - PicStruct: progressive
    //  - TargetKbps: 2222: some
    //  - GopRefDist: 1: to exclude B-frames which can be unsupported on some devices
    //  - GopPicSize: 15: some
    //  - NumSlice: 1
    pVideoParams->mfx.NumThread           = (mfxU16)mf_get_cpu_num();
    pVideoParams->mfx.CodecId             = MFX_CODEC_AVC;
    pVideoParams->mfx.TargetUsage         = MFX_TARGETUSAGE_BEST_SPEED;
    pVideoParams->mfx.FrameInfo.PicStruct = MFX_PICSTRUCT_PROGRESSIVE;
    pVideoParams->mfx.TargetKbps          = 2222;
    pVideoParams->mfx.RateControlMethod   = MFX_RATECONTROL_CBR;
    pVideoParams->mfx.GopRefDist          = 1;
    pVideoParams->mfx.GopPicSize          = 15;
    pVideoParams->mfx.NumSlice            = 0;
    return MFX_ERR_NONE;
}

/*------------------------------------------------------------------------------*/

static const GUID_info g_OutputTypes[] =
{
    { MFMediaType_Video, MEDIASUBTYPE_H264 }
};

static ClassRegData g_RegData =
{
    (GUID*)&CLSID_MF_H264EncFilter,
    TEXT(MFX_MF_PLUGIN_NAME),
    REG_AS_VIDEO_ENCODER,
    CreateVEncPlugin,
    FillH264EncoderParams,
    NULL,
    (GUID_info*)g_UncompressedVideoTypes, ARRAY_SIZE(g_UncompressedVideoTypes),
    (GUID_info*)g_OutputTypes, ARRAY_SIZE(g_OutputTypes),
    g_EncoderRegFlags
};

/*------------------------------------------------------------------------------*/

BOOL APIENTRY DllMain(HANDLE hModule,
                      DWORD  ul_reason_for_call,
                      LPVOID /*lpReserved*/)
{
    BOOL ret = false;

    if (DLL_PROCESS_ATTACH == ul_reason_for_call)
    {
    }
    if (!ret) ret = myDllMain(hModule, ul_reason_for_call, &g_RegData, 1);
    if (DLL_PROCESS_DETACH == ul_reason_for_call)
    {
        SAFE_RELEASE(g_RegData.pAttributes);
    }
    return ret;
}
