/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2008-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#ifndef __MFX_MFT_H264VE_H__
#define __MFX_MFT_H264VE_H__

#include "..\..\..\sample_common\include\current_date.h"

#define MFX_MF_PLUGIN_NAME "Intel\xae Media SDK H.264 Encoder MFT"
#define MFX_MF_DLL_NAME    "mfx_mft_h264ve.dll"

#endif // #ifndef __MFX_MFT_H264VE_H__
