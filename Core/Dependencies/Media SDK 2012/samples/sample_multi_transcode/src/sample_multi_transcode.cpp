//* ////////////////////////////////////////////////////////////////////////////// */
//*
//
//              INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license  agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in  accordance  with the terms of that agreement.
//        Copyright (c) 2010 - 2011 Intel Corporation. All Rights Reserved.
//
//
//*/

#include <windows.h>
#include "sample_multi_transcode.h"

#include "d3d_allocator.h"

using namespace std;
using namespace TranscodingSample;

Launcher::Launcher():m_StartTime(0),
                     m_bUseD3D(false)
{
    m_MinRequiredAPIVersion.Major = 1;
    m_MinRequiredAPIVersion.Minor = 0;
} // Launcher::Launcher()

Launcher::~Launcher()
{
    Close();
} // Launcher::~Launcher()

mfxStatus Launcher::Init(int argc, TCHAR *argv[])
{
    mfxStatus sts;
    mfxU32 i = 0;
    SafetySurfaceBuffer* pBuffer = NULL;
    mfxU32 BufCounter = 0;

    HANDLE hdl = NULL;

    sInputParams    InputParams;

    //parent transcode pipeline
    CTranscodingPipeline *pParentPipeline = NULL;
    // source transcode pipeline use instead parent in heterogeneous pipeline
    CTranscodingPipeline *pSinkPipeline = NULL;

    // parse input par file
    sts = m_parser.ParseCmdLine(argc, argv);
    MSDK_CHECK_PARSE_RESULT(sts, MFX_ERR_NONE, sts); 

    // get parameters for each session from parser
    while(m_parser.GetNextSessionParams(InputParams))
    {
        m_InputParamsArray.push_back(InputParams);
        InputParams.Reset();
    }

    // check correctness of input parameters
    sts = VerifyCrossSessionsOptions();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts); 

    if (m_bUseD3D)
    {
        m_pAllocParam.reset(new D3DAllocatorParams);   
        sts = m_d3dKeeper.CreateD3DEnv(m_pAllocParam.get());
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        hdl = (HANDLE)m_d3dKeeper.GetDeviceManager();
    }
    else
    {
        m_pAllocParam.reset(new mfxAllocatorParams);
    }

    // each pair of source and sink has own safety buffer
    sts = CreateSafetyBuffers();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // create sessions, allocators 
    for (i = 0; i < m_InputParamsArray.size(); i++)
    {
        GeneralAllocator* pAllocator = new GeneralAllocator;
        sts = pAllocator->Init(m_pAllocParam.get());
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts); 
        m_pAllocArray.push_back(pAllocator);

        std::auto_ptr<ThreadTranscodeContext> pThreadPipeline(new ThreadTranscodeContext);
        // extend BS processing init
        m_pExtBSProcArray.push_back(new FileBitstreamProcessor);
        pThreadPipeline->pPipeline.reset(new CTranscodingPipeline);        
     
        pThreadPipeline->pBSProcessor = m_pExtBSProcArray.back();
        if (Sink == m_InputParamsArray[i].eMode)
        {
            pBuffer = m_pBufferArray[m_pBufferArray.size()-1];
            pSinkPipeline = pThreadPipeline->pPipeline.get();
            sts = m_pExtBSProcArray.back()->Init(m_InputParamsArray[i].strSrcFile, NULL);
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts); 
        }
        else if (Source == m_InputParamsArray[i].eMode)
        {
            pBuffer = m_pBufferArray[BufCounter];
            BufCounter++;
            sts = m_pExtBSProcArray.back()->Init(NULL, m_InputParamsArray[i].strDstFile);
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        }
        else 
        {
            sts = m_pExtBSProcArray.back()->Init(m_InputParamsArray[i].strSrcFile, m_InputParamsArray[i].strDstFile);
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
            pBuffer = NULL;
        }

        // if session has VPP plus ENCODE only (-i::source option)
        // use decode source session as input
        sts = MFX_ERR_MORE_DATA;
        mfxStatus bs_sts;
        if (Source == m_InputParamsArray[i].eMode)
        {

            while(MFX_ERR_MORE_DATA == sts)
            {
                sts = pThreadPipeline->pPipeline->Init(&m_InputParamsArray[i], 
                                                       m_pAllocArray[i], 
                                                       hdl, 
                                                       pSinkPipeline,
                                                       pBuffer,
                                                       m_pExtBSProcArray.back(),
                                                       m_MinRequiredAPIVersion);
                if (MFX_ERR_NONE <= sts)
                    break;
                else if (MFX_ERR_MORE_DATA != sts) 
                    return sts; // error
                bs_sts = m_pExtBSProcArray.back()->PrepareBitstream();
                MSDK_CHECK_RESULT(bs_sts, MFX_ERR_NONE, bs_sts);

            }
        }
        else
        {
            while(MFX_ERR_MORE_DATA == sts)
            {             
                sts =  pThreadPipeline->pPipeline->Init(&m_InputParamsArray[i], 
                                                        m_pAllocArray[i], 
                                                        hdl, 
                                                        pParentPipeline,
                                                        pBuffer,
                                                        m_pExtBSProcArray.back(),
                                                        m_MinRequiredAPIVersion);
                if (MFX_ERR_NONE <= sts)
                    break;
                else if (MFX_ERR_MORE_DATA != sts) 
                    return sts; // error
                bs_sts = m_pExtBSProcArray.back()->PrepareBitstream();
                MSDK_CHECK_RESULT(bs_sts, MFX_ERR_NONE, bs_sts);

            }
        }

        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);         

        if (!pParentPipeline && m_InputParamsArray[i].bIsJoin)
            pParentPipeline = pThreadPipeline->pPipeline.get();

        // set the session's start status (like it is waiting)
        pThreadPipeline->startStatus = MFX_WRN_DEVICE_BUSY;
        // set other session's parameters
        pThreadPipeline->implType = m_InputParamsArray[i].libType;
        m_pSessionArray.push_back(pThreadPipeline.release());

        mfxVersion ver = {0};
        sts = m_pSessionArray[i]->pPipeline->QueryMFXVersion(&ver);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

        PrintInfo(i, &m_InputParamsArray[i], &ver);            
    }

    for (i = 0; i < m_InputParamsArray.size(); i++)
    {
        sts = m_pSessionArray[i]->pPipeline->CompleteInit();
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

        if (m_pSessionArray[i]->pPipeline->GetJoiningFlag())
            _tprintf(_T("Session %d was joined with other sessions\n"), i);
        else
            _tprintf(_T("Session %d was NOT joined with other sessions\n"), i);
    }

    _tprintf(_T("\n"));

    return sts;

} // mfxStatus Launcher::Init()

void Launcher::Run()
{
    mfxU32 totalSessions;

    _tprintf(_T("Transcoding started\n"));

    // mark start time
    m_StartTime = GetTick();

    // get parallel sessions parameters
    totalSessions = (mfxU32) m_pSessionArray.size();

        mfxU32 i;

        for (i = 0; i < totalSessions; i++)
        {
            HANDLE hdl = (HANDLE)_beginthreadex(0,
                                                0,
                                                ThranscodeRoutine,
                                                (void *)m_pSessionArray[i],
                                                0,
                                                0);
            m_HDLArray.push_back(hdl);
        }   
    
    for (i = 0; i < m_pSessionArray.size(); i++)
    {        
        WaitForSingleObject(m_HDLArray[i], INFINITE);        
    }

    _tprintf(_T("\nTranscoding finished\n"));

} // mfxStatus Launcher::Init()

mfxStatus Launcher::ProcessResult()
{
    FILE* pPerfFile = m_parser.GetPerformanceFile();
    _tprintf(_T("\nCommon transcoding time is  %.2f sec \n"), GetTime(m_StartTime));    

    m_parser.PrintParFileName();

    if (pPerfFile)
    {
        fprintf(pPerfFile, "Common transcoding time is  %.2f sec \n", GetTime(m_StartTime));
    }

    // get result
    bool SuccessTranscode = true;
    mfxU32 i;
    for (i = 0; i < m_pSessionArray.size(); i++)
    {
        mfxStatus sts = m_pSessionArray[i]->transcodingSts;
        if (MFX_ERR_NONE != sts)
        {
            SuccessTranscode = false;
            _tprintf(_T("MFX session %d transcoding FAILED:\nProcessing time: %.2f sec \nNumber of processed frames: %d\n"), 
                i, 
                m_pSessionArray[i]->working_time, 
                m_pSessionArray[i]->numTransFrames);
            if (pPerfFile)
            {
                fprintf(pPerfFile, "MFX session %d transcoding FAILED:\nProcessing time: %.2f sec \nNumber of processed frames: %d\n", 
                    i, 
                    m_pSessionArray[i]->working_time, 
                    m_pSessionArray[i]->numTransFrames);
            }
            
        }
        else
        {
            _tprintf(_T("MFX session %d transcoding PASSED:\nProcessing time: %.2f sec \nNumber of processed frames: %d\n"), 
                i, 
                m_pSessionArray[i]->working_time, 
                m_pSessionArray[i]->numTransFrames);
            if (pPerfFile)
            {
                fprintf(pPerfFile, "MFX session %d transcoding PASSED:\nProcessing time: %.2f sec \nNumber of processed frames: %d\n", 
                    i, 
                    m_pSessionArray[i]->working_time, 
                    m_pSessionArray[i]->numTransFrames);
            }
        }

        if (pPerfFile)
        {
            if (Native == m_InputParamsArray[i].eMode || Sink == m_InputParamsArray[i].eMode)
            {
                TCHAR *pSrcFile = m_InputParamsArray[i].strSrcFile;
                CHAR SrcFileASCII[1024] = {0};
                CHAR *pSrcASCII = SrcFileASCII;
                while (*pSrcFile)
                    *pSrcASCII++ = (CHAR)*pSrcFile++;
                fprintf(pPerfFile, "Input stream: %s\n", SrcFileASCII);
            }
            else
                fprintf(pPerfFile, "Input stream: from parent session\n");
            fprintf(pPerfFile,"\n");
        }


    }

    if (SuccessTranscode)
    {
        _tprintf(_T("\nThe test PASSED\n"));
        if (pPerfFile)
        {
            fprintf(pPerfFile, "\nThe test PASSED\n");
        }
        return MFX_ERR_NONE;
    }
    else
    {
        _tprintf(_T("\nThe test FAILED\n"));
        if (pPerfFile)
        {
            fprintf(pPerfFile, "\nThe test FAILED\n");
        }
        return MFX_ERR_UNKNOWN;
    }
} // mfxStatus Launcher::ProcessResult()

// to compare mfxVersions
bool operator <(const mfxVersion &left, const mfxVersion &right)
{
    return (left.Major < right.Major) && (left.Minor < right.Minor);
}

mfxStatus Launcher::DetermineMinRequiredAPIVersion(sInputParams *pParams)
{
    MSDK_CHECK_POINTER(pParams, MFX_ERR_NULL_PTR);    
    
    mfxVersion version;
    version.Major = 1;
    version.Minor = 0;
    if (pParams->bIsJoin || pParams->nRotationAngle)
    {
        version.Minor = 1;
    }

    if (pParams->bIsMVC || pParams->DecodeId == MFX_CODEC_JPEG)
    {
        version.Minor = 3;
    }

    // min required version is max version of all transcoding sessions
    if (m_MinRequiredAPIVersion < version)
        m_MinRequiredAPIVersion = version;

    return MFX_ERR_NONE;
}

mfxStatus Launcher::VerifyCrossSessionsOptions()
{
    bool IsSinkPresence = false;
    bool IsSourcePresence = false;
    bool IsHeterSessionJoin = false;
    bool IsFirstInTopology = true;

    for (mfxU32 i = 0; i < m_InputParamsArray.size(); i++)
    {
        if (Source == m_InputParamsArray[i].eMode)
        {
            // topology definition
            if (!IsSinkPresence)
            {
                PrintHelp(NULL, _T("Error in par file. Decode source session must be declared BEFORE encode sinks \n"));
                return MFX_ERR_UNSUPPORTED;
            }
            IsSourcePresence = true;

            if (IsFirstInTopology)
            {
                if (m_InputParamsArray[i].bIsJoin)
                    IsHeterSessionJoin = true;
                else
                    IsHeterSessionJoin = false;
            }
            else
            {
                if (m_InputParamsArray[i].bIsJoin && !IsHeterSessionJoin)
                {
                    PrintHelp(NULL, _T("Error in par file. All heterogeneous sessions must be joined \n"));
                    return MFX_ERR_UNSUPPORTED;
                }
                if (!m_InputParamsArray[i].bIsJoin && IsHeterSessionJoin)
                {
                    PrintHelp(NULL, _T("Error in par file. All heterogeneous sessions must be NOT joined \n"));
                    return MFX_ERR_UNSUPPORTED;
                }
            }
            
            if (IsFirstInTopology)
                IsFirstInTopology = false;

        }
        else if (Sink == m_InputParamsArray[i].eMode)
        {
            if (IsSinkPresence)
            {
                PrintHelp(NULL, _T("Error in par file. Only one source can be used"));
                return MFX_ERR_UNSUPPORTED;
            }
            IsSinkPresence = true;
            
            if (IsFirstInTopology)
            {
                if (m_InputParamsArray[i].bIsJoin)
                    IsHeterSessionJoin = true;
                else
                    IsHeterSessionJoin = false;
            }
            else
            {
                if (m_InputParamsArray[i].bIsJoin && !IsHeterSessionJoin)
                {
                    PrintHelp(NULL, _T("Error in par file. All heterogeneous sessions must be joined \n"));
                    return MFX_ERR_UNSUPPORTED;
                }
                if (!m_InputParamsArray[i].bIsJoin && IsHeterSessionJoin)
                {
                    PrintHelp(NULL, _T("Error in par file. All heterogeneous sessions must be NOT joined \n"));
                    return MFX_ERR_UNSUPPORTED;
                }
            }

            if (IsFirstInTopology)
                IsFirstInTopology = false;
        }
        if (MFX_IMPL_SOFTWARE != m_InputParamsArray[i].libType)
            m_bUseD3D = true;
        
        mfxStatus sts = DetermineMinRequiredAPIVersion(&m_InputParamsArray[i]);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    }

    if (IsSinkPresence && !IsSourcePresence)
    {
        PrintHelp(NULL, _T("Error: Sink should be defined"));
        return MFX_ERR_UNSUPPORTED;
    }
    return MFX_ERR_NONE;

} // mfxStatus Launcher::VerifyCrossSessionsOptions()

mfxStatus Launcher::CreateSafetyBuffers()
{
    SafetySurfaceBuffer* pBuffer     = NULL;
    SafetySurfaceBuffer* pPrevBuffer = NULL;
    
    for (mfxU32 i = 0; i < m_InputParamsArray.size(); i++)
    {
        if (Source == m_InputParamsArray[i].eMode)
        {
            pBuffer = new SafetySurfaceBuffer(pPrevBuffer);
            pPrevBuffer = pBuffer;
            m_pBufferArray.push_back(pBuffer);
        }
    }
    return MFX_ERR_NONE;

} // mfxStatus Launcher::CreateSafetyBuffers

void Launcher::Close()
{
    while(m_pSessionArray.size())
    {
        delete m_pSessionArray[m_pSessionArray.size()-1];
        m_pSessionArray[m_pSessionArray.size() - 1] = NULL;
        delete m_pAllocArray[m_pSessionArray.size()-1];
        m_pAllocArray[m_pSessionArray.size() - 1] = NULL;
        m_pAllocArray.pop_back();
        m_pSessionArray.pop_back();
    }
    while(m_pBufferArray.size())
    {
        delete m_pBufferArray[m_pBufferArray.size()-1];
        m_pBufferArray[m_pBufferArray.size() - 1] = NULL;
        m_pBufferArray.pop_back();
    }

    while(m_pExtBSProcArray.size())
    {
        delete m_pExtBSProcArray[m_pExtBSProcArray.size() - 1];
        m_pExtBSProcArray[m_pExtBSProcArray.size() - 1] = NULL;
        m_pExtBSProcArray.pop_back();
    }
} // void Launcher::Close()

int _tmain(int argc, TCHAR *argv[])
{
    mfxStatus sts;
    Launcher transcode;
    sts = transcode.Init(argc, argv);
    MSDK_CHECK_PARSE_RESULT(sts, MFX_ERR_NONE, 1);
    
    transcode.Run();

    sts = transcode.ProcessResult();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);

    return 0;
}

