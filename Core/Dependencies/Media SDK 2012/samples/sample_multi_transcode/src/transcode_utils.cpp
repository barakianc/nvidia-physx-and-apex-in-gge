//* ////////////////////////////////////////////////////////////////////////////// */
//*
//
//              INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license  agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in  accordance  with the terms of that agreement.
//        Copyright (c) 2010 - 2011 Intel Corporation. All Rights Reserved.
//
//
//*/



#include <windows.h>
#include <psapi.h>


#include "sysmem_allocator.h"
#include "transcode_utils.h"

#include <d3d9.h>
#include "d3d_allocator.h"


using namespace TranscodingSample;

// parsing defines
#define IS_SEPARATOR(ch)  ((ch) <= ' ' || (ch) == '=')
#define VAL_CHECK(val, argIdx, argName) {\
    if (val) \
    {\
        PrintHelp(NULL, _T("Input argument number %d \"%s\" require more parameters"), argIdx, argName); \
        return MFX_ERR_UNSUPPORTED;\
    }}

mfxU64 TranscodingSample::GetTick()
{
    LARGE_INTEGER tick;
    QueryPerformanceCounter(&tick);
    return tick.QuadPart;
}

mfxF64 TranscodingSample::GetTime(mfxU64 start)
{
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);

    LARGE_INTEGER tick;
    QueryPerformanceCounter(&tick);
    return (mfxF64)((tick.QuadPart - start)/(mfxF64)freq.QuadPart);
}
void TranscodingSample::PrintHelp(TCHAR *strAppName, TCHAR *strErrorMessage, ...)
{
    _tprintf(_T("Intel(R) Media SDK Multi Transcoding Sample Version %s\n\n"), MSDK_SAMPLE_VERSION);

    if (strErrorMessage)
    {
        va_list args;
        _tprintf(_T("ERROR: "));
        va_start(args, strErrorMessage);
        _vtprintf(strErrorMessage, args);
        va_end(args);
        _tprintf(_T("\n\n"));
    }
    
    _tprintf(_T("Command line parameters\n"));
    
    if (strAppName)
        _tprintf(_T("Usage: %s -par ParFile [-p PerfStatFile] \n"), strAppName);
    _tprintf(_T("\
    Information on ParFile format can be found in readme-multi-transcode.rtf\n\
\n\
    If the only transcoding session is needed the following usage is acceptable\n"));
    if (strAppName)
        _tprintf(_T("Usage: %s -i::h264|mpeg2|vc1|mvc|jpeg InputCodedBSFile -o::h264|mpeg2|mvc OutputEncodedFile -w width -h height [options]\n"), strAppName);
    _tprintf(_T("\
Where [options] can be: \n\
    [-f frameRate] - video frame rate (frames per second)\n\
    [-b bitRate] - encoded bit rate (Kbits per second)\n\
    [-u speed|quality|balanced] - target usage\n\
    [-l numSlices] - number of slices for encoder; default value 0 \n\
    [-w] - destination picture width, invokes VPP resizing\n\
    [-h] - destination picture height, invokes VPP resizing\n\
    [-async] - depth of asynchronous pipeline. default value 1\n\
    [-sw|-hw] - SDK implementation to use: \n\t-hw - platform-specific on default display adapter, \n\t-sw - software (default). \n\
    [-join] - use session joining. If several sessions are running, you can join them. By default the sessions are not joining\n\
    [-priority] - use priority for join sessions. 0 - Low, 1 - Normal, 2 - High. Normal by default\n\
    [-n ] - Number of frames to transcode \n\
    [-angle 180] - enables 180 degrees picture rotation user module before encoding\n\
    [-opencl] - uses implementation of rotation plugin (enabled with -angle option) through Intel(R) OpenCL\n\
    [-deinterlace] - forces VPP to deinterlace input stream\n\
\n\
    Example: -i::mpeg2 in.mpeg2 -o::h264 out.h264\n\
    Example: -i::mvc in.mvc -o::mvc out.mvc -w 320 -h 240\n"));
}

void TranscodingSample::PrintInfo(mfxU32 session_number, sInputParams* pParams, mfxVersion *pVer)
{
    TCHAR buf[2048];
    MSDK_CHECK_POINTER_NO_RET(pVer);    
    
    if ((MFX_IMPL_AUTO <= pParams->libType) && (MFX_IMPL_HARDWARE4 >= pParams->libType))
    {
        _tprintf(_T("MFX %s Session %d API ver %d.%d parameters: \n"),
            (MFX_IMPL_SOFTWARE == pParams->libType)? _T("SOFTWARE") : _T("HARDWARE"),
                 session_number,
                 pVer->Major,
                 pVer->Minor);
    }

    if (0 == pParams->DecodeId)
        _tprintf(_T("Input  video: From parent session\n"));
    else
        _tprintf(_T("Input  video: %s\n"), CodecIdToStr(pParams->DecodeId));

    // means that source is parent session
    if (0 == pParams->EncodeId)
        _tprintf(_T("Output video: To child session\n"));
    else
        _tprintf(_T("Output video: %s\n"), CodecIdToStr(pParams->EncodeId));

    if (PrintDllInfo(buf, ARRAYSIZE(buf), pParams))
        _tprintf(_T("MFX dll: %s\n"),buf);

    _tprintf(_T("\n"));   
}

bool TranscodingSample::PrintDllInfo(TCHAR* buf, mfxU32 buf_size, sInputParams* pParams)
{
    HANDLE   hCurrent = GetCurrentProcess();
    HMODULE *pModules;
    DWORD    cbNeeded;
    int      nModules;
    if (NULL == EnumProcessModules(hCurrent, NULL, 0, &cbNeeded))
        return false;

    nModules = cbNeeded / sizeof(HMODULE);

    pModules = new HMODULE[nModules];
    if (NULL == pModules)
    {
        return false;
    }
    if (NULL == EnumProcessModules(hCurrent, pModules, cbNeeded, &cbNeeded))
    {
        delete []pModules;
        return false;
    }

    for (int i = 0; i < nModules; i++)
    {
        GetModuleFileName(pModules[i], buf, buf_size);
        if (_tcsstr(buf, _T("libmfxhw")) && (MFX_IMPL_SOFTWARE != pParams->libType))
        {
            delete []pModules;
            return true;
        }
        else if (_tcsstr(buf, _T("libmfxsw")) && (MFX_IMPL_SOFTWARE == pParams->libType))
        {
            delete []pModules;
            return true;
        }

    }
    delete []pModules;
    return false;
}

D3DKeeper::D3DKeeper():m_pd3dDeviceManager(NULL),
                       m_pd3dDevice(NULL),
                       m_pd3d(NULL)

{
}
mfxStatus D3DKeeper::CreateDeviceManager()
{    
    m_pd3d = Direct3DCreate9(D3D_SDK_VERSION);
    if (!m_pd3d)
        return MFX_ERR_NULL_PTR;

    POINT point = {0, 0};
    HWND window = WindowFromPoint(point);

    D3DPRESENT_PARAMETERS d3dParams;
    memset(&d3dParams, 0, sizeof(d3dParams));
    d3dParams.Windowed = TRUE;
    d3dParams.hDeviceWindow = window;
    d3dParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
    d3dParams.Flags = D3DPRESENTFLAG_VIDEO;
    d3dParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
    d3dParams.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
    d3dParams.BackBufferCount = 1;
    d3dParams.BackBufferFormat = D3DFMT_X8R8G8B8;
    d3dParams.BackBufferWidth = 0;
    d3dParams.BackBufferHeight = 0;
    
    HRESULT hr = m_pd3d->CreateDevice(
        D3DADAPTER_DEFAULT,
        D3DDEVTYPE_HAL,
        window,
        D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
        &d3dParams,
        &m_pd3dDevice);
    if (FAILED(hr))
        return MFX_ERR_NULL_PTR;

    UINT resetToken = 0;    
    hr = DXVA2CreateDirect3DDeviceManager9(&resetToken, &m_pd3dDeviceManager);
    if (FAILED(hr))
        return MFX_ERR_NULL_PTR;

    hr = m_pd3dDeviceManager->ResetDevice(m_pd3dDevice, resetToken);
    if (FAILED(hr))
        return MFX_ERR_UNDEFINED_BEHAVIOR;
    
    m_resetToken = resetToken;

    return MFX_ERR_NONE;
}

mfxStatus D3DKeeper::CreateD3DEnv(mfxAllocatorParams *pd3dAllocParams)
{
    mfxStatus sts;
    D3DAllocatorParams *pD3Dparams = dynamic_cast<D3DAllocatorParams*>(pd3dAllocParams);
    if (!pD3Dparams)
        return MFX_ERR_UNSUPPORTED;
    sts = CreateDeviceManager();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // provide device manager to MediaSDK
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    pD3Dparams->pManager = m_pd3dDeviceManager;
    return MFX_ERR_NONE;
}
D3DKeeper::~D3DKeeper()
{
   MSDK_SAFE_RELEASE(m_pd3dDevice);
   MSDK_SAFE_RELEASE(m_pd3dDeviceManager);
   MSDK_SAFE_RELEASE(m_pd3d);  
}

CmdProcessor::CmdProcessor()
{
    m_SessionParamId = 0;
    m_SessionArray.clear();
    m_PerfFILE = NULL;
    m_parName = NULL;

} //CmdProcessor::CmdProcessor()

CmdProcessor::~CmdProcessor()
{
    m_SessionArray.clear();
    if (m_PerfFILE)
        fclose(m_PerfFILE);

} //CmdProcessor::~CmdProcessor()

void CmdProcessor::PrintParFileName()
{
    if (m_parName && m_PerfFILE)
    {
        CHAR SrcFileASCII[1024] = {0};
        CHAR *pSrcASCII = SrcFileASCII;
        while (*m_parName) 
            *pSrcASCII++ = (CHAR)*m_parName++;
        fprintf(m_PerfFILE, "Input par file: %s\n\n", SrcFileASCII);
    }
}

mfxStatus CmdProcessor::ParseCmdLine(int argc, TCHAR *argv[])
{
    FILE *parFile = NULL;
    mfxStatus sts = MFX_ERR_UNSUPPORTED;
      
    if (1 == argc)
    {
       PrintHelp(argv[0], NULL);
       return MFX_ERR_UNSUPPORTED;
    }

    // parse command line
    for (mfxU8 i = 1; i < argc; i++)
    {
        if (0 == _tcscmp(argv[i], _T("-par")))
        {
            if (parFile)
            {
                PrintHelp(argv[0], _T("Only one par file is supported"));
                return MFX_ERR_UNSUPPORTED;
            }
            i++;
            m_parName = argv[i];
        }
        else if (0 == _tcscmp(argv[i], _T("-?")) )
        {
            PrintHelp(argv[0], NULL);
            return MFX_ERR_UNSUPPORTED;
        }
        else if (0 == _tcscmp(argv[i], _T("-p")))
        {
            if (m_PerfFILE)
            {
                PrintHelp(argv[0], _T("Only one performance file is supported"));
                return MFX_ERR_UNSUPPORTED;
            }
            i++;
            _tfopen_s(&m_PerfFILE, argv[i],_T("w"));
            if (NULL == m_PerfFILE)
            {
                PrintHelp(argv[0], _T("performance file \"%s\" not found"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }
        }
    }

    if (NULL != m_parName)
    {
        _tfopen_s(&parFile, m_parName, _T("r"));
        if (NULL == parFile)
        {
            PrintHelp(argv[0], _T("ParFile \"%s\" not found"), m_parName);
            return MFX_ERR_UNSUPPORTED;
        }
    }
    _tprintf(_T("Intel(R) Media SDK Multi Transcoding Sample Version %s\n\n"), MSDK_SAMPLE_VERSION);
    _tprintf(_T("Par file is: %s\n\n"), m_parName);
    sts = _ParseParamsForOneSession(argc-1, argv+1);
    if (MFX_ERR_NONE != sts)
    {
        if (NULL != parFile)
        {
            fclose(parFile);
            parFile = NULL;
        }
        return sts;
    }
    
    if (NULL != m_parName)
    {
        sts = ParseParFile(parFile);
        if (MFX_ERR_NONE != sts)
        {
            PrintHelp(argv[0], _T("Command line is invalid"));
            if (NULL != parFile)
            {
                fclose(parFile);
                parFile = NULL;
            }
            return sts;
        }

        fclose(parFile);
    }

    return sts;

} //mfxStatus CmdProcessor::ParseCmdLine(int argc, TCHAR *argv[])

mfxStatus CmdProcessor::ParseParFile(FILE *parFile)
{
    mfxStatus sts = MFX_ERR_UNSUPPORTED;
    if (!parFile)
        return MFX_ERR_UNSUPPORTED;

    mfxU32 currPos = 0;
    mfxU32 lineIndex = 0;

    // calculate file size
    fseek(parFile, 0, SEEK_END);
    mfxU32 fileSize = ftell(parFile) + 1;
    fseek(parFile, 0, SEEK_SET);

    // allocate buffer for parsing
    s_ptr<TCHAR, false> parBuf;
    parBuf.reset(new TCHAR[fileSize]);
    TCHAR *pCur;

    while(currPos < fileSize)
    {
        pCur = _fgetts(parBuf.get(), fileSize, parFile);
        if (!pCur)
            return MFX_ERR_NONE;
        while(pCur[currPos] != '\n' && pCur[currPos] != 0)
        {
            currPos++;
            if  (pCur + currPos >= parBuf.get() + fileSize)
                return sts;
        }
        // zero string
        if (!currPos)
            continue;

        sts = ParseParamsForOneSession(pCur, currPos);
        if (MFX_ERR_NONE != sts)
            PrintHelp(NULL, _T("Error in par file parameters at line %d"), lineIndex);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        currPos = 0;
        lineIndex++;
    }
    
    return MFX_ERR_NONE;

} //mfxStatus CmdProcessor::ParseParFile(FILE *parFile)

mfxStatus CmdProcessor::ParseParamsForOneSession(TCHAR *pLine, mfxU32 length)
{
    mfxU32 i;
    const mfxU8 maxArgNum = 255;
    TCHAR *argv[maxArgNum+1];
    mfxU32 argc = 0;
    s_ptr<TCHAR, false> pMemLine;

    pMemLine.reset(new TCHAR[length+2]);

    TCHAR *pTempLine = pMemLine.get();
    pTempLine[0] = ' ';
    pTempLine++;

    memcpy(pTempLine, pLine, length*sizeof(TCHAR));

    // parse into command streams
    for (i = 0; i < length ; i++)
    {
        // check if separator
        if (IS_SEPARATOR(pTempLine[-1]) && !IS_SEPARATOR(pTempLine[0]))
        {       
            argv[argc++] = pTempLine;            
            if (argc > maxArgNum)
            {
                PrintHelp(NULL, _T("Too many parameters (reached maximum of %d)"), maxArgNum);
                return MFX_ERR_UNSUPPORTED;
            }
        }
        if (pTempLine[0] == ' ')
        {
            pTempLine[0] = 0;
        }
        pTempLine++;
    }   

    // EOL for last parameter
    pTempLine[0] = 0;

    return _ParseParamsForOneSession(argc, argv);
}

mfxStatus CmdProcessor::_ParseParamsForOneSession(mfxU32 argc, TCHAR *argv[])
{
    mfxStatus sts;
    mfxU32 i;
    mfxU32 skipped = 0;
    TranscodingSample::sInputParams InputParams;   
    TCHAR endC;

    for (i = 0; i < argc; i++)
    {
        // process multi-character options
        if (0 == _tcscmp(argv[i], _T("-i::mpeg2")))
        {
            // only encode supports
            if (InputParams.eMode == Source)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strSrcFile, argv[i]);      
            InputParams.DecodeId = MFX_CODEC_MPEG2;
        }
        else if (0 == _tcscmp(argv[i], _T("-i::h264")))
        {
            // only encode supports
            if (InputParams.eMode == Source)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strSrcFile, argv[i]);
            InputParams.DecodeId = MFX_CODEC_AVC;
        }
        else if (0 == _tcscmp(argv[i], _T("-i::vc1")))
        {
            // only encode supports
            if (InputParams.eMode == Source)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strSrcFile, argv[i]);
            InputParams.DecodeId = MFX_CODEC_VC1;
        }
        else if (0 == _tcscmp(argv[i], _T("-i::mvc")))
        {
            // only encode supports
            if (InputParams.eMode == Source)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strSrcFile, argv[i]);
            InputParams.DecodeId = MFX_CODEC_AVC;
            InputParams.bIsMVC = true;
        }
        else if (0 == _tcscmp(argv[i], _T("-i::jpeg")))
        {
            // only encode supports
            if (InputParams.eMode == Source)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strSrcFile, argv[i]);
            InputParams.DecodeId = MFX_CODEC_JPEG;            
        }
        else if (0 == _tcscmp(argv[i], _T("-o::mpeg2")))
        {
            // only decode supports
            if (InputParams.eMode == Sink)
                return MFX_ERR_UNSUPPORTED;

            // In case of MVC only MVC-MVC transcoding is supported
            if (InputParams.bIsMVC)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strDstFile, argv[i]);
            InputParams.EncodeId = MFX_CODEC_MPEG2;
        }
        else if (0 == _tcscmp(argv[i], _T("-o::h264")))
        {
            // only decode supports
            if (InputParams.eMode == Sink)
                return MFX_ERR_UNSUPPORTED;

            // In case of MVC only MVC-MVC transcoding is supported
            if (InputParams.bIsMVC)
                return MFX_ERR_UNSUPPORTED;

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strDstFile, argv[i]);
            InputParams.EncodeId = MFX_CODEC_AVC;
        }
        else if (0 == _tcscmp(argv[i], _T("-o::mvc")))
        {
            // only decode supports
            if (InputParams.eMode == Sink)
                return MFX_ERR_UNSUPPORTED;            

            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            _tcscpy_s(InputParams.strDstFile, argv[i]);
            InputParams.EncodeId = MFX_CODEC_AVC;
            InputParams.bIsMVC = true;
        }
        else if (0 == _tcscmp(argv[i], _T("-sw")))
        {
            InputParams.libType = MFX_IMPL_SOFTWARE;
        }
        else if (0 == _tcscmp(argv[i], _T("-hw")))
        {
            InputParams.libType = MFX_IMPL_HARDWARE;
        }        
        else if (0 == _tcscmp(argv[i], _T("-perf_opt")))
        {
            InputParams.bIsPerf = true;
        }
        else if(0 == _tcscmp(argv[i], _T("-f")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%lf%c"), &InputParams.dFrameRate, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("frameRate \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }          
        }
        else if(0 == _tcscmp(argv[i], _T("-b")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%d%c"), &InputParams.nBitRate, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("bitRate \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }
        }
        else if(0 == _tcscmp(argv[i], _T("-u")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.nTargetUsage, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T(" \"%s\" target usage is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }  
        }
        else if (0 == _tcscmp(argv[i], _T("-w")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.nDstWidth, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("width \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }  
        }
        else if (0 == _tcscmp(argv[i], _T("-h")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.nDstHeight, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("height \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }  
        }        
        else if (0 == _tcscmp(argv[i], _T("-l")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.nSlices, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("numSlices \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }  
        }
        else if (0 == _tcscmp(argv[i], _T("-async")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.nAsyncDepth, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("async \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }  
        }
        else if (0 == _tcscmp(argv[i], _T("-join")))
        {
           InputParams.bIsJoin = true;
        }
        else if (0 == _tcscmp(argv[i], _T("-priority")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.priority, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("priority \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }
        }
        else if (0 == _tcscmp(argv[i], _T("-i::source")))
        {
            if (InputParams.eMode != Native)
                return MFX_ERR_UNSUPPORTED;


            InputParams.eMode = Source;
        } 
        else if (0 == _tcscmp(argv[i], _T("-o::sink")))
        {
             if (InputParams.eMode != Native)
                return MFX_ERR_UNSUPPORTED;


            InputParams.eMode = Sink;

        }        
        else if (0 == _tcscmp(argv[i], _T("-n")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%d%c"), &InputParams.MaxFrameNumber, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("-n \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }
        }
        else if (0 == _tcscmp(argv[i], _T("-angle")))
        {
            VAL_CHECK(i+1 == argc, i, argv[i]);
            i++;
            if (1 != _stscanf_s(argv[i], _T("%hd%c"), &InputParams.nRotationAngle, &endC, sizeof (endC)))
            {
                PrintHelp(NULL, _T("-angle \"%s\" is invalid"), argv[i]);
                return MFX_ERR_UNSUPPORTED;
            }
            _tcscpy_s(InputParams.strPluginDLLPath, _T("sample_rotate_plugin.dll")); 
        }
        else if (0 == _tcscmp(argv[i], _T("-opencl")))
        {            
            _tcscpy_s(InputParams.strPluginDLLPath, _T("sample_plugin_opencl.dll"));             
        }
        // output PicStruct
        else if (0 == _tcscmp(argv[i], _T("-deinterlace")))
        {
            InputParams.bEnableDeinterlacing = true;
        }
        // skip already parsed global command line options
        else if (0 == _tcscmp(argv[i], _T("-par")) || 
            0 == _tcscmp(argv[i], _T("-p")))
        {
            skipped += 2;
            i++;
        }
        else if (0 == _tcscmp(argv[i], _T("-?")))
        {
            skipped++;
        }
        else
        {
            PrintHelp(NULL, _T("Invalid input argument number %d \"%s\""), i, argv[i]);
            return MFX_ERR_UNSUPPORTED;
        }
    }
    
    if (skipped < argc)
    {
        sts = VerifyAndCorrectInputParams(InputParams);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        m_SessionArray.push_back(InputParams);
    }

    return MFX_ERR_NONE;

} //mfxStatus CmdProcessor::ParseParamsForOneSession(TCHAR *pLine, mfxU32 length)

mfxStatus CmdProcessor::VerifyAndCorrectInputParams(TranscodingSample::sInputParams &InputParams)
{
    if (0 == _tcslen(InputParams.strSrcFile) && (InputParams.eMode == Sink || InputParams.eMode == Native))
    {
        PrintHelp(NULL, _T("Source file name not found"));
        return MFX_ERR_UNSUPPORTED;
    };

    if (0 == _tcslen(InputParams.strDstFile) && (InputParams.eMode == Source || InputParams.eMode == Native))
    {
        PrintHelp(NULL, _T("Destination file name not found"));
        return MFX_ERR_UNSUPPORTED;
    };

    if (MFX_CODEC_MPEG2 != InputParams.EncodeId && MFX_CODEC_AVC != InputParams.EncodeId && InputParams.eMode != Sink)
    {
        PrintHelp(NULL, _T("Unknown encoder\n"));
        return MFX_ERR_UNSUPPORTED;
    }

   if (MFX_CODEC_MPEG2 != InputParams.DecodeId &&
       MFX_CODEC_AVC != InputParams.DecodeId && 
       MFX_CODEC_VC1 != InputParams.DecodeId && 
       MFX_CODEC_JPEG != InputParams.DecodeId &&
       InputParams.eMode != Source)
    {
        PrintHelp(NULL, _T("Unknown decoder\n"));
        return MFX_ERR_UNSUPPORTED;
    }

    // set default values for optional parameters that were not set or were set incorrectly   
    if (MFX_TARGETUSAGE_BEST_QUALITY != InputParams.nTargetUsage && MFX_TARGETUSAGE_BEST_SPEED != InputParams.nTargetUsage)
    {
        InputParams.nTargetUsage = MFX_TARGETUSAGE_BALANCED;
    }
    
    if (InputParams.dFrameRate <= 0)
    {
        InputParams.dFrameRate = 30;
    }
    return MFX_ERR_NONE;

} //mfxStatus CmdProcessor::VerifyAndCorrectInputParams(TranscodingSample::sInputParams &InputParams)

bool  CmdProcessor::GetNextSessionParams(TranscodingSample::sInputParams &InputParams)
{
    if (!m_SessionArray.size())
        return false;
    if (m_SessionParamId == m_SessionArray.size())
    {
        return false;
    }
    InputParams = m_SessionArray[m_SessionParamId];
    m_SessionParamId++;
    return true;

} //bool  CmdProcessor::GetNextSessionParams(TranscodingSample::sInputParams &InputParams)

// Wrapper on standard allocator for concurrent allocation of 
// D3D and system surfaces
GeneralAllocator::GeneralAllocator() 
{
    m_D3DAllocator.reset(new D3DFrameAllocator);
    m_SYSAllocator.reset(new SysMemFrameAllocator);

};
GeneralAllocator::~GeneralAllocator() 
{
};
mfxStatus GeneralAllocator::Init(mfxAllocatorParams *pParams)
{
    mfxStatus sts = m_D3DAllocator.get()->Init(pParams);
    MSDK_CHECK_RESULT(MFX_ERR_NONE, sts, sts);
    
    sts = m_SYSAllocator.get()->Init(0);
    MSDK_CHECK_RESULT(MFX_ERR_NONE, sts, sts);

    return sts;
}
mfxStatus GeneralAllocator::Close()
{
    mfxStatus sts = m_D3DAllocator.get()->Close();
    MSDK_CHECK_RESULT(MFX_ERR_NONE, sts, sts);

    sts = m_SYSAllocator.get()->Close();
    MSDK_CHECK_RESULT(MFX_ERR_NONE, sts, sts);

   return sts;
}

mfxStatus GeneralAllocator::LockFrame(mfxMemId mid, mfxFrameData *ptr)
{
    return isD3DMid(mid)?m_D3DAllocator.get()->Lock(m_D3DAllocator.get(), mid, ptr):
                         m_SYSAllocator.get()->Lock(m_SYSAllocator.get(),mid, ptr);
}
mfxStatus GeneralAllocator::UnlockFrame(mfxMemId mid, mfxFrameData *ptr)
{
    return isD3DMid(mid)?m_D3DAllocator.get()->Unlock(m_D3DAllocator.get(), mid, ptr):
                         m_SYSAllocator.get()->Unlock(m_SYSAllocator.get(),mid, ptr);
}

mfxStatus GeneralAllocator::GetFrameHDL(mfxMemId mid, mfxHDL *handle)
{
    return isD3DMid(mid)?m_D3DAllocator.get()->GetHDL(m_D3DAllocator.get(), mid, handle):
                         m_SYSAllocator.get()->GetHDL(m_SYSAllocator.get(), mid, handle);

}

mfxStatus GeneralAllocator::ReleaseResponse(mfxFrameAllocResponse *response)
{
    // try to ReleaseResponse via D3D allocator
    return isD3DMid(response->mids[0])?m_D3DAllocator.get()->Free(m_D3DAllocator.get(),response):
                                       m_SYSAllocator.get()->Free(m_SYSAllocator.get(), response);
}
mfxStatus GeneralAllocator::AllocImpl(mfxFrameAllocRequest *request, mfxFrameAllocResponse *response)
{
    mfxStatus sts;
    if (request->Type & MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET || request->Type & MFX_MEMTYPE_VIDEO_MEMORY_PROCESSOR_TARGET)
    {
        sts = m_D3DAllocator.get()->Alloc(m_D3DAllocator.get(), request, response);
        StoreFrameMids(true, response);
    }
    else
    {
        sts = m_SYSAllocator.get()->Alloc(m_SYSAllocator.get(), request, response);
        StoreFrameMids(false, response);
    }
    return sts;
}
void    GeneralAllocator::StoreFrameMids(bool isD3DFrames, mfxFrameAllocResponse *response)
{
    for (mfxU32 i = 0; i < response->NumFrameActual; i++)
        m_Mids.insert(std::pair<mfxHDL, bool>(response->mids[i], isD3DFrames));
}
bool GeneralAllocator::isD3DMid(mfxHDL mid)
{
    std::map<mfxHDL, bool>::iterator it;
    it = m_Mids.find(mid);
    return it->second;
} // GeneralAllocator::isD3DMid(mfxHDL mid)
