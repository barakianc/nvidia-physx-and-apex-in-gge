//* ////////////////////////////////////////////////////////////////////////////// */
//*
//
//              INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license  agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in  accordance  with the terms of that agreement.
//        Copyright (c) 2011 Intel Corporation. All Rights Reserved.
//
//
//*/

#ifndef __TRANSCODE_UTILS_H__
#define __TRANSCODE_UTILS_H__


#include <process.h>

#pragma warning(disable : 4201)
#include <d3d9.h>
#include <dxva2api.h>

#include <vector>
#include <map>
#include "pipeline_transcode.h"

class D3DFrameAllocator;
class SysMemFrameAllocator;
struct D3DAllocatorParams;

#pragma warning(disable: 4127) // constant expression


namespace TranscodingSample
{

    struct sInputParams;

    mfxU64 GetTick();
    mfxF64 GetTime(mfxU64 start);

    void PrintHelp(TCHAR *strAppName, TCHAR *strErrorMessage, ...);
    void PrintInfo(mfxU32 session_number, sInputParams* pParams, mfxVersion *pVer);

    bool PrintDllInfo(TCHAR *buf, mfxU32 buf_size, sInputParams* pParams);


    template <class T, bool isSingle>
    class s_ptr
    {
    public:
        s_ptr():m_ptr(0)
        {
        };
        ~s_ptr()
        {
            reset(0);
        }
        T* get()
        {
            return m_ptr;
        }
        T* pop()
        {
            T* ptr = m_ptr;
            m_ptr = 0;
            return ptr;
        }
        void reset(T* ptr) 
        {
            if (m_ptr)
            {
                if (isSingle)
                    delete m_ptr;
                else
                    delete[] m_ptr;
            }
            m_ptr = ptr;
        }
    protected:
        T* m_ptr;
    };

    class D3DKeeper
    {
    public:

        D3DKeeper();
        virtual ~D3DKeeper();
        mfxStatus CreateD3DEnv(mfxAllocatorParams *pd3dAllocParams);
        IDirect3DDeviceManager9* GetDeviceManager() {return m_pd3dDeviceManager;}

    protected:
        mfxStatus CreateDeviceManager();
        IDirect3D9* m_pd3d;
        IDirect3DDeviceManager9* m_pd3dDeviceManager; 
        IDirect3DDevice9*        m_pd3dDevice;
        UINT m_resetToken;
    private:
        DISALLOW_COPY_AND_ASSIGN(D3DKeeper);
    };

    class CmdProcessor
    {
    public:
        CmdProcessor();
        virtual ~CmdProcessor();
        mfxStatus ParseCmdLine(int argc, TCHAR *argv[]);
        bool GetNextSessionParams(TranscodingSample::sInputParams &InputParams);
        FILE*     GetPerformanceFile() {return m_PerfFILE;};
        void      PrintParFileName();
    protected:
        mfxStatus ParseParFile(FILE* file);
        mfxStatus ParseParamsForOneSession(TCHAR *pLine, mfxU32 length);
        mfxStatus _ParseParamsForOneSession(mfxU32 argc, TCHAR *argv[]);
        mfxStatus VerifyAndCorrectInputParams(TranscodingSample::sInputParams &InputParams);
        mfxU32                                       m_SessionParamId;
        std::vector<TranscodingSample::sInputParams> m_SessionArray;
        FILE                                         *m_PerfFILE;
        TCHAR                                        *m_parName;
    private:
        DISALLOW_COPY_AND_ASSIGN(CmdProcessor);

    };


    // Wrapper on standard allocator for concurrent allocation of 
    // D3D and system surfaces
    class GeneralAllocator : public BaseFrameAllocator
    {
    public:
        GeneralAllocator();
        virtual ~GeneralAllocator();    

        virtual mfxStatus Init(mfxAllocatorParams *pParams);
        virtual mfxStatus Close();

    protected:
        virtual mfxStatus LockFrame(mfxMemId mid, mfxFrameData *ptr);
        virtual mfxStatus UnlockFrame(mfxMemId mid, mfxFrameData *ptr);
        virtual mfxStatus GetFrameHDL(mfxMemId mid, mfxHDL *handle);       

        virtual mfxStatus ReleaseResponse(mfxFrameAllocResponse *response);
        virtual mfxStatus AllocImpl(mfxFrameAllocRequest *request, mfxFrameAllocResponse *response);

        void    StoreFrameMids(bool isD3DFrames, mfxFrameAllocResponse *response);
        bool    isD3DMid(mfxHDL mid);

        std::map<mfxHDL, bool>              m_Mids;
        std::auto_ptr<D3DFrameAllocator>    m_D3DAllocator;
        std::auto_ptr<SysMemFrameAllocator> m_SYSAllocator;
    private:
       DISALLOW_COPY_AND_ASSIGN(GeneralAllocator);

    };

}
#endif
