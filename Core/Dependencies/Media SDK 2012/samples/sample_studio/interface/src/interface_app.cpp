/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "stdafx.h"
#include "interface_app.h"
#include "interface_dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CInterfaceApp construction
CInterfaceApp::CInterfaceApp()
{
}

// The one and only CInterfaceApp object
CInterfaceApp theApp;

// CInterfaceApp initialization
BOOL CInterfaceApp::InitInstance()
{
    // InitCommonControlsEx() is required on Windows XP if an application
    // manifest specifies use of ComCtl32.dll version 6 or later to enable
    // visual styles.  Otherwise, any window creation will fail.
    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof(InitCtrls);
    // Set this to include all the common control classes you want to use
    // in your application.
    InitCtrls.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&InitCtrls);

    CWinApp::InitInstance();

    AfxEnableControlContainer();

    // Check for Flash ActiveX controll installed
    {
        LONG lRet;
        HKEY hKey;

        lRet = RegOpenKeyEx(HKEY_CLASSES_ROOT, _T("CLSID\\{D27CDB6E-AE6D-11cf-96B8-444553540000}"), 0, KEY_READ , &hKey);

        if (lRet != ERROR_SUCCESS)
        {
            MessageBox(NULL, _T("Please install Flash Player ActiveX control"), _T("Unable to find required components"), MB_OK);
            return FALSE;
        }

        lRet = RegCloseKey(hKey);
    }

    CInterfaceDlg       l_Dialog;
    STARTUPINFO         l_StartupInfo = {0,};
    PROCESS_INFORMATION l_ProcessInfo = {0,};

    l_StartupInfo.cb            = sizeof(l_StartupInfo);
    l_StartupInfo.dwFlags       = STARTF_USESHOWWINDOW;
    l_StartupInfo.wShowWindow   = SW_HIDE;

#ifdef WOW64
    PVOID   oV  = NULL;

    if (!Wow64DisableWow64FsRedirection(&oV))
    {
        MessageBox(0, L"Wow64DisableWow64FsRedirection", L"Error", MB_OK);
        return FALSE;
    }
#endif
    if (!CreateProcess(_T("sample_studio_builder.exe"), NULL, NULL, NULL, TRUE, 0, NULL, NULL, &l_StartupInfo, &l_ProcessInfo))
    {
        MessageBox(0, _T("CreateProcess"), _T("Error"), MB_OK);
        return FALSE;
    }

    l_Dialog.m_BuilderThread = l_ProcessInfo.dwThreadId;

#ifdef WOW64
    Wow64RevertWow64FsRedirection(oV);
#endif

    l_Dialog.DoModal();

    // Since the dialog has been closed, return FALSE so that we exit the
    //  application, rather than start the application's message pump.
    return FALSE;
}
