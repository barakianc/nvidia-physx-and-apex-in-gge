/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "stdafx.h"
#include "interface_app.h"
#include "interface_dlg.h"
#include "message.h"

CInterfaceDlg::CInterfaceDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CInterfaceDlg::IDD, pParent)
{
    m_BuilderWindow         = NULL;
    m_BuilderThread         = 0;
    m_hIcon                 = AfxGetApp()->LoadIcon(IDR_MainFrame);
    m_Render_Window_X       = 0;
    m_Render_Window_Y       = 0;
    m_Render_Window_Width   = 0;
    m_Render_Window_Height  = 0;

    memset(&m_TranscodeProfiles, 0, sizeof(m_TranscodeProfiles));

    m_TranscodeProfileCurrent = -1;
}

CInterfaceDlg::~CInterfaceDlg()
{
    if (m_BuilderThread)
        ::PostThreadMessage(m_BuilderThread, WM_CloseBuilder, 0, 0);
}

void CInterfaceDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_ShockwaveFlash, m_Shockwave);
}

BEGIN_MESSAGE_MAP(CInterfaceDlg, CDialog)
    ON_COMMAND(1, OnCommand) // 1 - OnOK
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_WM_DROPFILES()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_COPYDATA()
    ON_MESSAGE(WM_InitWindowHandle, OnInitWindowHandle)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

// CInterfaceDlg message handlers

void CInterfaceDlg::OnCommand()
{
    return;
}

BOOL CInterfaceDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);         // Set big icon
    SetIcon(m_hIcon, FALSE);        // Set small icon

/*
    // Set WS_EX_LAYERED on this window
    SetWindowLong(m_hWnd, GWL_EXSTYLE,
            GetWindowLong(m_hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
    // Make this window 70% alpha
    SetLayeredWindowAttributes(0, (255 * 95) / 100, LWA_ALPHA);
*/

    FILE*   l_File;
    errno_t l_Error;
    char    l_String[1024] = {0,};
    int     l_CurrentProfile = -1;
    char*   l_Name;
    char*   l_Value;
    char*   l_TokenNext;
    size_t  l_Converted;

    PluginParameter*    l_InfoEncoderAudio = NULL;
    PluginParameter*    l_InfoEncoderVideo = NULL;

    l_Error = fopen_s(&l_File, "sample_studio_profiles.txt", "rb");

    if (l_Error)
        return FALSE;

    while(!feof(l_File))
    {
        fgets(l_String, sizeof(l_String) - 1, l_File);

        if (l_String[0] == '[')
        {
            l_Name = strtok_s(l_String, "[]", &l_TokenNext);

            if (l_Name)
            {
                if (++l_CurrentProfile >= sizeof(m_TranscodeProfiles) / sizeof(m_TranscodeProfiles[0]))
                    break;

                memset(&m_TranscodeProfiles[l_CurrentProfile], 0, sizeof(TranscodeProfile));

                l_InfoEncoderAudio = &m_TranscodeProfiles[l_CurrentProfile].m_InfoEncoderAudio;
                l_InfoEncoderVideo = &m_TranscodeProfiles[l_CurrentProfile].m_InfoEncoderVideo;

                l_Converted  = sizeof(m_TranscodeProfiles[l_CurrentProfile].m_Name);

#ifdef  _UNICODE
                l_Converted /= sizeof(m_TranscodeProfiles[l_CurrentProfile].m_Name[0]);
                l_Converted -= 1;

                l_Error = mbstowcs_s(&l_Converted, m_TranscodeProfiles[l_CurrentProfile].m_Name, l_Name, l_Converted);
#else
                l_Error = strcpy_s(m_TranscodeProfiles[l_CurrentProfile].m_Name, l_Converted, l_Name);
#endif
                l_InfoEncoderAudio->m_Type                  = ParameterType_Audio;
                l_InfoEncoderAudio->m_SubType               = ParameterSubType_AudioAAC;
                l_InfoEncoderAudio->m_Audio_BlockAlignment  = 1;
                l_InfoEncoderAudio->m_Audio_Compressed      = 1;

                l_InfoEncoderVideo->m_Type                  = ParameterType_Video;
                l_InfoEncoderVideo->m_SubType               = ParameterSubType_VideoH264;
                l_InfoEncoderVideo->m_Video_InterlaceMode   = 7;
            }
        }
        else
        {
            if (!l_InfoEncoderAudio || !l_InfoEncoderVideo)
                continue;

            l_Name  = strtok_s(l_String, " =\t", &l_TokenNext);
            l_Value = strtok_s(NULL,     " =\t", &l_TokenNext);

            if (!l_Name || !l_Value)
                continue;

            if (!strcmp(l_Name, "audio_bit_sample"))    l_InfoEncoderAudio->m_Audio_BitsPerSample        = atoi(l_Value);
            if (!strcmp(l_Name, "audio_bit_rate"))      l_InfoEncoderAudio->m_Audio_BytesPerSecond       = atoi(l_Value) / 8;
            if (!strcmp(l_Name, "audio_channels"))      l_InfoEncoderAudio->m_Audio_Channels             = atoi(l_Value);
            if (!strcmp(l_Name, "audio_sample_rate"))   l_InfoEncoderAudio->m_Audio_SampleRate           = atoi(l_Value);
            
            if (!strcmp(l_Name, "video_bit_rate"))      l_InfoEncoderVideo->m_Video_BitRate              = atoi(l_Value);
            if (!strcmp(l_Name, "video_width"))         l_InfoEncoderVideo->m_Video_FrameWidth           = atoi(l_Value);
            if (!strcmp(l_Name, "video_height"))        l_InfoEncoderVideo->m_Video_FrameHeight          = atoi(l_Value);
            if (!strcmp(l_Name, "video_framerate_num")) l_InfoEncoderVideo->m_Video_FrameRateNumerator   = atoi(l_Value);
            if (!strcmp(l_Name, "video_framerate_den")) l_InfoEncoderVideo->m_Video_FrameRateDenominator = atoi(l_Value);
            if (!strcmp(l_Name, "video_aspect_x"))      l_InfoEncoderVideo->m_Video_PixelAspectRatioX    = atoi(l_Value);
            if (!strcmp(l_Name, "video_aspect_y"))      l_InfoEncoderVideo->m_Video_PixelAspectRatioY    = atoi(l_Value);
        }
    }

    fclose(l_File);

    return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CInterfaceDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CInterfaceDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void CInterfaceDlg::OnDropFiles(HDROP a_DropInfo)
{
    CString         l_FileName;
    PluginParameter l_PluginParameter;
    COPYDATASTRUCT  l_CopyData;

    if (NULL != a_DropInfo)
    {
        UINT nFiles = DragQueryFile(a_DropInfo, (UINT)-1, NULL, 0);

        for(UINT nNames = 0; nNames < nFiles; nNames++)
        {
            DragQueryFile(a_DropInfo, nNames, (LPTSTR)l_FileName.GetBuffer(_MAX_PATH + 1), _MAX_PATH);

            if (_tcscmp(l_FileName, _T("")))
            {
                l_CopyData.dwData = MessageType_StreamInput;
                l_CopyData.cbData = sizeof(PluginParameter);
                l_CopyData.lpData = &l_PluginParameter;

                l_PluginParameter.m_Type = ParameterType_Stream;
                _tcscpy_s(l_PluginParameter.m_Stream_Name, l_FileName.GetBuffer());

                ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }
    }

    return;
}

void CInterfaceDlg::OnSize(UINT nType, int cx, int cy)
{
    CWnd*   l_Flash;
    TCHAR   l_Size  [256] = {0,};
    TCHAR   l_Width [32]  = {0,};
    TCHAR   l_Height[32]  = {0,};

    l_Flash = GetDlgItem(IDC_ShockwaveFlash);

    if (l_Flash)
    {
        l_Flash->SetWindowPos(NULL, 0, 0, cx, cy, SWP_NOZORDER | SWP_NOMOVE);

        _itot_s(cx, l_Width, 10);
        _itot_s(cy, l_Height, 10);
        _tcscat_s(l_Size, l_Width);
        _tcscat_s(l_Size, _T(" "));
        _tcscat_s(l_Size, l_Height);

        Call_DialogToFlash(_T("Resize"), l_Size);
    }
}

BOOL CInterfaceDlg::OnEraseBkgnd(CDC* pDC)
{
    return 0;
}

LRESULT CInterfaceDlg::OnInitWindowHandle(WPARAM a_ParamW, LPARAM a_ParamL)
{
    OSVERSIONINFO   l_VersionInformation;
    BOOL            l_Ret;
    BOOL            l_AllowMF = 0;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;
    PluginParameter l_PluginParameter;

    m_BuilderWindow = (HWND)a_ParamL;

    // Windows 7 detection
    if (m_BuilderWindow)
    {
        l_VersionInformation.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

        l_Ret = GetVersionEx(&l_VersionInformation);

        if (l_Ret)
        if (l_VersionInformation.dwPlatformId   == VER_PLATFORM_WIN32_NT)
        if (l_VersionInformation.dwMajorVersion == 6)
        if (l_VersionInformation.dwMinorVersion == 1)
            l_AllowMF = 1;

        if (!l_AllowMF)
        {
            l_CopyData.dwData = MessageType_Command;
            l_CopyData.cbData = sizeof(l_BuilderCommand);
            l_CopyData.lpData = &l_BuilderCommand;

            _tcscpy_s(l_BuilderCommand.m_Command, _T("Use_DS"));
            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            Call_DialogToFlash(_T("EnableControl"), _T("id_Button_MF=false"));
        }

        // Init Video render window
        {
            l_PluginParameter.m_Type                    = ParameterType_RenderVideo;
            l_PluginParameter.m_Render_Window_X         = m_Render_Window_X;
            l_PluginParameter.m_Render_Window_Y         = m_Render_Window_Y;
            l_PluginParameter.m_Render_Window_Width     = m_Render_Window_Width;
            l_PluginParameter.m_Render_Window_Height    = m_Render_Window_Height;
            l_PluginParameter.m_Render_Window_Parent    = m_hWnd;

            l_CopyData.dwData = MessageType_InfoWindow;
            l_CopyData.cbData = sizeof(PluginParameter);
            l_CopyData.lpData = &l_PluginParameter;

            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    return 0;
}

BOOL CInterfaceDlg::OnCopyData(CWnd* a_Wnd, COPYDATASTRUCT* a_CopyDataStruct)
{
    PluginParameter*    l_PluginParameter;
    BuilderCommand*     l_BuilderCommand;

    if (!a_CopyDataStruct)
        return FALSE;

    l_PluginParameter   = (PluginParameter*)(a_CopyDataStruct->lpData);
    l_BuilderCommand    = (BuilderCommand*) (a_CopyDataStruct->lpData);

    if (a_CopyDataStruct->dwData == MessageType_Command)
    {
        if (!_tcscmp(l_BuilderCommand->m_Command, _T("DisplayStatus")))
            Call_DialogToFlash(_T("SetStatus"), l_BuilderCommand->m_Info);

        if (!_tcscmp(l_BuilderCommand->m_Command, _T("EnableControl")))
            Call_DialogToFlash(_T("EnableControl"), l_BuilderCommand->m_Info);

        if (!_tcscmp(l_BuilderCommand->m_Command, _T("DisplayDecodeParameters")))
            DisplayDecodeParameters();

        if (!_tcscmp(l_BuilderCommand->m_Command, _T("DisplayEncodeParameters")))
            Apply_TranscodeProfile();
    }

    if (a_CopyDataStruct->dwData == MessageType_InfoDecoder)
    {
        if (l_PluginParameter->m_Type == ParameterType_Audio)
            memcpy(&m_InfoDecoderAudio, l_PluginParameter, sizeof(PluginParameter));

        if (l_PluginParameter->m_Type == ParameterType_Video)
            memcpy(&m_InfoDecoderVideo, l_PluginParameter, sizeof(PluginParameter));
    }

    if (a_CopyDataStruct->dwData == MessageType_InfoEncoder)
    {
        if (l_PluginParameter->m_Type == ParameterType_Audio)
            memcpy(&m_InfoEncoderAudio, l_PluginParameter, sizeof(PluginParameter));

        if (l_PluginParameter->m_Type == ParameterType_Video)
            memcpy(&m_InfoEncoderVideo, l_PluginParameter, sizeof(PluginParameter));

        if (l_PluginParameter->m_Type == ParameterType_Stream)
            memcpy(&m_InfoMuxer, l_PluginParameter, sizeof(PluginParameter));
    }

    return TRUE;
}

BEGIN_EVENTSINK_MAP(CInterfaceDlg, CDialog)
    ON_EVENT(CInterfaceDlg, IDC_ShockwaveFlash, 197, CInterfaceDlg::Call_FlashToDialog, VTS_BSTR)
END_EVENTSINK_MAP()

int GetXMLdata(const TCHAR* a_XML, const TCHAR* a_Key, const TCHAR** a_Data)
{
    int             l_Index;
    TCHAR           l_KeyOpen [256] = {0, };
    TCHAR           l_KeyClose[256] = {0, };
    const TCHAR*    l_DataOpen;
    const TCHAR*    l_DataClose;

    if (a_Data == NULL)
        return 0;

    if (a_XML == NULL)
    {
        *a_Data = NULL;
        return 0;
    }

    _tcscat_s(l_KeyOpen,  _T("<"));  _tcscat_s(l_KeyOpen,  a_Key); _tcscat_s(l_KeyOpen,  _T(">"));
    _tcscat_s(l_KeyClose, _T("</")); _tcscat_s(l_KeyClose, a_Key); _tcscat_s(l_KeyClose, _T(">"));

    *a_Data = _tcsstr(a_XML, l_KeyOpen);

    if (*a_Data == NULL)
        return 0;

    *a_Data += _tcsclen(l_KeyOpen);

    for(l_Index = 1, l_DataOpen = *a_Data; l_Index > 0; )
    {
        l_DataClose = _tcsstr(l_DataOpen, l_KeyClose);
        l_DataOpen  = _tcsstr(l_DataOpen, l_KeyOpen);

        if (l_DataClose == NULL)
            return 0;

        if (l_DataOpen == NULL || l_DataClose < l_DataOpen)
            l_Index --;
        else
            l_Index ++;

        l_DataOpen = l_DataClose;
    }

    return int(l_DataClose - *a_Data);
}

int IsCommand(const TCHAR* a_Command, int a_Lenght, const TCHAR* a_Key)
{
    if (_tcsclen(a_Key) != a_Lenght)
        return 0;

    if (_tcsnccmp(a_Command, a_Key, a_Lenght))
        return 0;

    return 1;
}

void CInterfaceDlg::Call_FlashToDialog(LPCTSTR a_Request)
{
    int             l_Lenght;
    int             l_LenghtCommand;
    int             l_Index;
    const TCHAR*    l_Agruments = NULL;
    const TCHAR*    l_Command   = NULL;
    const TCHAR*    l_Parameter = NULL;
    const TCHAR*    l_ControlId = NULL;
    TCHAR           l_Value[32 * 1024] = {0,};
    PluginParameter l_PluginParameter;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_Lenght        = GetXMLdata(a_Request,   _T("arguments"), &l_Agruments);
    l_LenghtCommand = GetXMLdata(l_Agruments, _T("string"),    &l_Command);

    if (l_Command == NULL)
        return;

    if (IsCommand(l_Command, l_LenghtCommand, _T("VideoRender")))
    {
        l_Lenght = GetXMLdata(l_Command,   _T("number"), &l_Parameter);
        if (l_Parameter) m_Render_Window_X       = _wtol(l_Parameter);

        l_Lenght = GetXMLdata(l_Parameter, _T("number"), &l_Parameter);
        if (l_Parameter) m_Render_Window_Y       = _wtol(l_Parameter);

        l_Lenght = GetXMLdata(l_Parameter, _T("number"), &l_Parameter);
        if (l_Parameter) m_Render_Window_Width   = _wtol(l_Parameter);

        l_Lenght = GetXMLdata(l_Parameter, _T("number"), &l_Parameter);
        if (l_Parameter) m_Render_Window_Height  = _wtol(l_Parameter);

        if (m_BuilderWindow)
        {
            l_PluginParameter.m_Type                    = ParameterType_RenderVideo;
            l_PluginParameter.m_Render_Window_X         = m_Render_Window_X;
            l_PluginParameter.m_Render_Window_Y         = m_Render_Window_Y;
            l_PluginParameter.m_Render_Window_Width     = m_Render_Window_Width;
            l_PluginParameter.m_Render_Window_Height    = m_Render_Window_Height;
            l_PluginParameter.m_Render_Window_Parent    = m_hWnd;

            l_CopyData.dwData = MessageType_InfoWindow;
            l_CopyData.cbData = sizeof(PluginParameter);
            l_CopyData.lpData = &l_PluginParameter;

            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    if (IsCommand(l_Command, l_LenghtCommand, _T("InitComplete")))
    {
        ::PostThreadMessage(m_BuilderThread, WM_InitWindowHandle, 0, (LPARAM) m_hWnd);

        Call_DialogToFlash(_T("EnableControl"), _T("id_Button_Play=false"));
        Call_DialogToFlash(_T("EnableControl"), _T("id_Button_Pause=false"));
        Call_DialogToFlash(_T("EnableControl"), _T("id_Button_Stop=false"));
      //Call_DialogToFlash(_T("EnableControl"), _T("id_Button_Transcode=false"));
        Call_DialogToFlash(_T("EnableControl"), _T("id_Button_TranscodeRun=false"));

        for (l_Index = 0; l_Index < sizeof(m_TranscodeProfiles) / sizeof(m_TranscodeProfiles[0]); l_Index++)
        {
            if (_tcslen(m_TranscodeProfiles[l_Index].m_Name) == 0)
                break;

            Call_DialogToFlash(_T("AddProfile"), m_TranscodeProfiles[l_Index].m_Name);

            if (l_Index == 0)
            {
                m_TranscodeProfileCurrent = 0;
                Apply_TranscodeProfile();
            }
        }
    }

    if (IsCommand(l_Command, l_LenghtCommand, _T("SetProfile")))
    {
        l_Lenght = GetXMLdata(l_Command, _T("string"), &l_Parameter);

        if (l_Lenght)
        for (l_Index = 0; l_Index < sizeof(m_TranscodeProfiles) / sizeof(m_TranscodeProfiles[0]); l_Index++)
        {
            if (l_Lenght == _tcslen(m_TranscodeProfiles[l_Index].m_Name))
            if (!_tcsnccmp(l_Parameter, m_TranscodeProfiles[l_Index].m_Name, l_Lenght))
            {
                m_TranscodeProfileCurrent = l_Index;
                Apply_TranscodeProfile();
            }
        }
    }

    if (IsCommand(l_Command, l_LenghtCommand, _T("SetEncoderParameter")))
    {
        l_Lenght = GetXMLdata(l_Command, _T("string"), &l_Parameter);

        if (l_Lenght)
        {
            l_ControlId = _T("id_Muxer_StreamName");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                _tcsncpy_s(m_InfoMuxer.m_Stream_Name, l_Parameter + _tcslen(l_ControlId) + 1, l_Lenght - _tcslen(l_ControlId) - 1);

            l_ControlId = _T("id_Audio_BitRate");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderAudio.m_Audio_BytesPerSecond = _wtol(l_Parameter + _tcslen(l_ControlId) + 1) / 8;

            l_ControlId = _T("id_Audio_SampleRate");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderAudio.m_Audio_SampleRate = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Audio_Channels");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderAudio.m_Audio_Channels = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Audio_BitsPerSample");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderAudio.m_Audio_BitsPerSample = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_BitRate");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_BitRate = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_FrameRateNum");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_FrameRateNumerator = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_FrameRateDen");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_FrameRateDenominator = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_FrameWidth");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_FrameWidth = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_FrameHeight");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_FrameHeight = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_AspectRatioX");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_PixelAspectRatioX = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_ControlId = _T("id_Video_AspectRatioY");

            if (!_tcsnccmp(l_Parameter, l_ControlId, _tcslen(l_ControlId)))
                m_InfoEncoderVideo.m_Video_PixelAspectRatioY = _wtol(l_Parameter + _tcslen(l_ControlId) + 1);

            l_CopyData.dwData = MessageType_InfoEncoder;
            l_CopyData.cbData = sizeof(PluginParameter);

            l_CopyData.lpData = &m_InfoEncoderAudio;
            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            l_CopyData.lpData = &m_InfoEncoderVideo;
            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            l_CopyData.lpData = &m_InfoMuxer;
            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_Open")))
    {
        CFileDialog l_Dialog(TRUE);
        CString     l_FileName;

        const int l_BuffSize = MAX_PATH + 1;

        l_Dialog.GetOFN().lpstrFile = l_FileName.GetBuffer(l_BuffSize);
        l_Dialog.DoModal();

        if (_tcscmp(l_FileName, _T("")))
        {
            l_CopyData.dwData = MessageType_StreamInput;
            l_CopyData.cbData = sizeof(PluginParameter);
            l_CopyData.lpData = &l_PluginParameter;

            l_PluginParameter.m_Type = ParameterType_Stream;
            _tcscpy_s(l_PluginParameter.m_Stream_Name, l_FileName.GetBuffer());

            ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }

        l_FileName.ReleaseBuffer();
    }

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_Play")))
        _tcscpy_s(l_BuilderCommand.m_Command, _T("Play"));

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_Pause")))
        _tcscpy_s(l_BuilderCommand.m_Command, _T("Pause"));

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_Stop")))
        _tcscpy_s(l_BuilderCommand.m_Command, _T("Stop"));

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_TranscodeRun")))
        _tcscpy_s(l_BuilderCommand.m_Command, _T("TranscodeRun"));

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_MF")))
        _tcscpy_s(l_BuilderCommand.m_Command, _T("Use_MF"));

    if (IsCommand(l_Command, l_LenghtCommand, _T("id_Button_DS")))
        _tcscpy_s(l_BuilderCommand.m_Command, _T("Use_DS"));

    if (_tcscmp(l_BuilderCommand.m_Command, _T("")))
        ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    return;
}

void CInterfaceDlg::Apply_TranscodeProfile()
{
    COPYDATASTRUCT      l_CopyData;
    PluginParameter*    l_InfoEncoderAudio = NULL;
    PluginParameter*    l_InfoEncoderVideo = NULL;

    if (m_TranscodeProfileCurrent < 0)
        return;

    l_InfoEncoderAudio = &m_TranscodeProfiles[m_TranscodeProfileCurrent].m_InfoEncoderAudio;
    l_InfoEncoderVideo = &m_TranscodeProfiles[m_TranscodeProfileCurrent].m_InfoEncoderVideo;

    if (!l_InfoEncoderAudio || !l_InfoEncoderVideo)
        return;

    m_InfoEncoderAudio.m_Type       = l_InfoEncoderAudio->m_Type;
    m_InfoEncoderAudio.m_SubType    = l_InfoEncoderAudio->m_SubType;

    m_InfoEncoderVideo.m_Type       = l_InfoEncoderVideo->m_Type;
    m_InfoEncoderVideo.m_SubType    = l_InfoEncoderVideo->m_SubType;

    if (l_InfoEncoderAudio->m_Audio_BitsPerSample)          m_InfoEncoderAudio.m_Audio_BitsPerSample        = l_InfoEncoderAudio->m_Audio_BitsPerSample;
    if (l_InfoEncoderAudio->m_Audio_BlockAlignment)         m_InfoEncoderAudio.m_Audio_BlockAlignment       = l_InfoEncoderAudio->m_Audio_BlockAlignment;
    if (l_InfoEncoderAudio->m_Audio_BytesPerSecond)         m_InfoEncoderAudio.m_Audio_BytesPerSecond       = l_InfoEncoderAudio->m_Audio_BytesPerSecond;
    if (l_InfoEncoderAudio->m_Audio_Channels)               m_InfoEncoderAudio.m_Audio_Channels             = l_InfoEncoderAudio->m_Audio_Channels;
    if (l_InfoEncoderAudio->m_Audio_Compressed)             m_InfoEncoderAudio.m_Audio_Compressed           = l_InfoEncoderAudio->m_Audio_Compressed;
    if (l_InfoEncoderAudio->m_Audio_SampleRate)             m_InfoEncoderAudio.m_Audio_SampleRate           = l_InfoEncoderAudio->m_Audio_SampleRate;

    if (l_InfoEncoderVideo->m_Video_BitRate)                m_InfoEncoderVideo.m_Video_BitRate              = l_InfoEncoderVideo->m_Video_BitRate;
    if (l_InfoEncoderVideo->m_Video_FrameWidth)             m_InfoEncoderVideo.m_Video_FrameWidth           = l_InfoEncoderVideo->m_Video_FrameWidth;
    if (l_InfoEncoderVideo->m_Video_FrameHeight)            m_InfoEncoderVideo.m_Video_FrameHeight          = l_InfoEncoderVideo->m_Video_FrameHeight;
    if (l_InfoEncoderVideo->m_Video_FrameRateNumerator)     m_InfoEncoderVideo.m_Video_FrameRateNumerator   = l_InfoEncoderVideo->m_Video_FrameRateNumerator;
    if (l_InfoEncoderVideo->m_Video_FrameRateDenominator)   m_InfoEncoderVideo.m_Video_FrameRateDenominator = l_InfoEncoderVideo->m_Video_FrameRateDenominator;
    if (l_InfoEncoderVideo->m_Video_InterlaceMode)          m_InfoEncoderVideo.m_Video_InterlaceMode        = l_InfoEncoderVideo->m_Video_InterlaceMode;
    if (l_InfoEncoderVideo->m_Video_PixelAspectRatioX)      m_InfoEncoderVideo.m_Video_PixelAspectRatioX    = l_InfoEncoderVideo->m_Video_PixelAspectRatioX;
    if (l_InfoEncoderVideo->m_Video_PixelAspectRatioY)      m_InfoEncoderVideo.m_Video_PixelAspectRatioY    = l_InfoEncoderVideo->m_Video_PixelAspectRatioY;

    DisplayEncodeParameters();

    l_CopyData.dwData = MessageType_InfoEncoder;
    l_CopyData.cbData = sizeof(PluginParameter);

    l_CopyData.lpData = &m_InfoEncoderAudio;
    ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    l_CopyData.lpData = &m_InfoEncoderVideo;
    ::SendMessage(m_BuilderWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
}

void CInterfaceDlg::DisplayDecodeParameters()
{
    TCHAR           l_Info[32*1024] = {0,};
    TCHAR           l_Value[32]  = {0,};

    if (m_InfoDecoderAudio.m_SubType != ParameterSubType_None)
    {
        _tcscat_s(l_Info, _T("<Folder name='Audio info'>"));

        // format
        {
            if (m_InfoDecoderAudio.m_SubType == ParameterSubType_AudioPCM)
                _tcscat_s(l_Info, _T("  <Item name='format' value='PCM'/>"));

            if (m_InfoDecoderAudio.m_SubType == ParameterSubType_AudioAAC)
                _tcscat_s(l_Info, _T("  <Item name='format' value='AAC'/>"));

            if (m_InfoDecoderAudio.m_SubType == ParameterSubType_AudioAC3)
                _tcscat_s(l_Info, _T("  <Item name='format' value='AC3'/>"));

            if (m_InfoDecoderAudio.m_SubType == ParameterSubType_AudioMP3)
                _tcscat_s(l_Info, _T("  <Item name='format' value='MP3'/>"));

            if (m_InfoDecoderAudio.m_SubType == ParameterSubType_AudioWMA)
                _tcscat_s(l_Info, _T("  <Item name='format' value='WMA'/>"));
        }

        // channels
        if (m_InfoDecoderAudio.m_Audio_Channels)
        {
            _tcscat_s(l_Info, _T("  <Item name='channels' value='"));
            _itot_s(m_InfoDecoderAudio.m_Audio_Channels, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        // bits per sample
        if (m_InfoDecoderAudio.m_Audio_BitsPerSample)
        {
            _tcscat_s(l_Info, _T("  <Item name='bits per sample' value='"));
            _itot_s(m_InfoDecoderAudio.m_Audio_BitsPerSample, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        // sample rate
        if (m_InfoDecoderAudio.m_Audio_SampleRate)
        {
            _tcscat_s(l_Info, _T("  <Item name='sample rate' value='"));
            _itot_s(m_InfoDecoderAudio.m_Audio_SampleRate, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        _tcscat_s(l_Info, _T("</Folder>"));
    }

    if (m_InfoDecoderVideo.m_SubType != ParameterSubType_None)
    {
        _tcscat_s(l_Info, _T("<Folder name='Video info'>"));

        // format
        {
            if (m_InfoDecoderVideo.m_SubType == ParameterSubType_VideoMPEG2)
                _tcscat_s(l_Info, _T("  <Item name='format' value='MPEG2'/>"));

            if (m_InfoDecoderVideo.m_SubType == ParameterSubType_VideoH264)
                _tcscat_s(l_Info, _T("  <Item name='format' value='H.264'/>"));

            if (m_InfoDecoderVideo.m_SubType == ParameterSubType_VideoVC1)
                _tcscat_s(l_Info, _T("  <Item name='format' value='VC1'/>"));
        }

        // bit rate
        if (m_InfoDecoderVideo.m_Video_BitRate)
        {
            _tcscat_s(l_Info, _T("  <Item name='bit rate' value='"));
            _itot_s(m_InfoDecoderVideo.m_Video_BitRate, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        // width
        if (m_InfoDecoderVideo.m_Video_FrameWidth)
        {
            _tcscat_s(l_Info, _T("  <Item name='width' value='"));
            _itot_s(m_InfoDecoderVideo.m_Video_FrameWidth, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        // height
        if (m_InfoDecoderVideo.m_Video_FrameHeight)
        {
            _tcscat_s(l_Info, _T("  <Item name='height' value='"));
            _itot_s(m_InfoDecoderVideo.m_Video_FrameHeight, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        // frame rate
        if (m_InfoDecoderVideo.m_Video_FrameRateNumerator)
        {
            float l_Numerator   = float(m_InfoDecoderVideo.m_Video_FrameRateNumerator);
            float l_Denominator = float(m_InfoDecoderVideo.m_Video_FrameRateDenominator);
            float l_FrameRate   = 0;

            if (m_InfoDecoderVideo.m_Video_FrameRateDenominator > 0)
                l_FrameRate = l_Numerator / l_Denominator;

            _tcscat_s(l_Info, _T("  <Item name='frame rate' value='"));
            _itot_s(int(l_FrameRate), l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("."));
            _itot_s(int((l_FrameRate - int(l_FrameRate)) * 100), l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        // pixel aspect ratio
        if (m_InfoDecoderVideo.m_Video_PixelAspectRatioX)
        {
            _tcscat_s(l_Info, _T("  <Item name='pixel aspect ratio' value='"));
            _itot_s(m_InfoDecoderVideo.m_Video_PixelAspectRatioX, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T(":"));
            _itot_s(m_InfoDecoderVideo.m_Video_PixelAspectRatioY, l_Value, 10);
            _tcscat_s(l_Info, l_Value);
            _tcscat_s(l_Info, _T("'/>"));
        }

        _tcscat_s(l_Info, _T("</Folder>"));
    }

    Call_DialogToFlash(_T("DisplayDecodeParameters"), l_Info);
}

void CInterfaceDlg::DisplayEncodeParameters()
{
    TCHAR           l_Info[32*1024] = {0,};
    TCHAR           l_Value[32]     = {0,};

    _tcscat_s(l_Info, _T("<Folder name='Audio encoder'>"));

    // SubType
    {
        if (m_InfoEncoderAudio.m_SubType == ParameterSubType_AudioAAC)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Microsoft AAC Encoder' enabled='false'/>"));

        if (m_InfoEncoderAudio.m_SubType == ParameterSubType_AudioAC3)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Microsoft AC3 Encoder' enabled='false'/>"));

        if (m_InfoEncoderAudio.m_SubType == ParameterSubType_AudioMP3)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Microsoft MP3 Encoder' enabled='false'/>"));

        if (m_InfoEncoderAudio.m_SubType == ParameterSubType_AudioWMA)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Microsoft WMA Encoder' enabled='false'/>"));
    }

    // BitRate
    {
        _tcscat_s(l_Info, _T("  <Item name='BitRate' value='"));
        _itot_s(m_InfoEncoderAudio.m_Audio_BytesPerSecond * 8, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // SampleRate
    {
        _tcscat_s(l_Info, _T("  <Item name='SampleRate' value='"));
        _itot_s(m_InfoEncoderAudio.m_Audio_SampleRate, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // Channels
    {
        _tcscat_s(l_Info, _T("  <Item name='Channels' value='"));
        _itot_s(m_InfoEncoderAudio.m_Audio_Channels, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // BitsPerSample
    {
        _tcscat_s(l_Info, _T("  <Item name='BitsPerSample' value='"));
        _itot_s(m_InfoEncoderAudio.m_Audio_BitsPerSample, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    _tcscat_s(l_Info, _T("</Folder>"));

    _tcscat_s(l_Info, _T("<Folder name='Video encoder'>"));

    // SubType
    {
        if (m_InfoEncoderVideo.m_SubType == ParameterSubType_VideoMPEG2)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Microsoft MPEG2 Encoder' enabled='false'/>"));

        if (m_InfoEncoderVideo.m_SubType == ParameterSubType_VideoH264)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Intel H.264 Encoder' enabled='false'/>"));

        if (m_InfoEncoderVideo.m_SubType == ParameterSubType_VideoVC1)
            _tcscat_s(l_Info, _T("  <Item name='Codec' value='Microsoft VC1 Encoder' enabled='false'/>"));
    }

    // BitRate
    {
        _tcscat_s(l_Info, _T("  <Item name='BitRate' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_BitRate, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // FrameRateNumerator
    {
        _tcscat_s(l_Info, _T("  <Item name='FrameRateNumerator' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_FrameRateNumerator, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // FrameRateDenominator
    {
        _tcscat_s(l_Info, _T("  <Item name='FrameRateDenominator' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_FrameRateDenominator, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    //FrameWidth
    {
        _tcscat_s(l_Info, _T("  <Item name='FrameWidth' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_FrameWidth, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // FrameHeight
    {
        _tcscat_s(l_Info, _T("  <Item name='FrameHeight' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_FrameHeight, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // PixelAspectRatioX
    {
        _tcscat_s(l_Info, _T("  <Item name='PixelAspectRatioX' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_PixelAspectRatioX, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    // PixelAspectRatioY
    {
        _tcscat_s(l_Info, _T("  <Item name='PixelAspectRatioY' value='"));
        _itot_s(m_InfoEncoderVideo.m_Video_PixelAspectRatioY, l_Value, 10);
        _tcscat_s(l_Info, l_Value);
        _tcscat_s(l_Info, _T("' enabled='true'/>"));
    }

    _tcscat_s(l_Info, _T("</Folder>"));

    _tcscat_s(l_Info, _T("<Folder name='Muxer'>"));

    // StreamName
    {
        _tcscat_s(l_Info, _T("  <Item name='StreamName' value='"));
        _tcscat_s(l_Info, m_InfoMuxer.m_Stream_Name);
        _tcscat_s(l_Info, _T("'/>"));
    }

    _tcscat_s(l_Info, _T("</Folder>"));

    Call_DialogToFlash(_T("DisplayEncodeParameters"), l_Info);
}

void CInterfaceDlg::Call_DialogToFlash(TCHAR* a_Function, TCHAR* a_Argument)
{
    CString l_Result;
    TCHAR   l_Request[32*1024] = {0,};

    _tcscat_s(l_Request, _T("<invoke name='"));
    _tcscat_s(l_Request, a_Function);
    _tcscat_s(l_Request, _T("' returntype='xml'>"));

    if (a_Argument != NULL)
    {
        _tcscat_s(l_Request, _T("<arguments><string>"));
        _tcscat_s(l_Request, a_Argument);
        _tcscat_s(l_Request, _T("</string></arguments>"));
    }

    _tcscat_s(l_Request, _T("</invoke>"));

    m_Shockwave.InvokeHelper(0xC6, DISPATCH_METHOD, VT_BSTR, (void*)&l_Result, (BYTE*)VTS_BSTR, l_Request);

    return;
}
