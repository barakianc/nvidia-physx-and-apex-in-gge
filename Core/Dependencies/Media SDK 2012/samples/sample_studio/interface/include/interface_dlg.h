/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once
#include "shockwave_flash.h"
#include "pipeline.h"

struct TranscodeProfile
{
    TCHAR           m_Name[256];
    PluginParameter m_InfoEncoderAudio;
    PluginParameter m_InfoEncoderVideo;
};

// CInterfaceDlg dialog
class CInterfaceDlg : public CDialog
{
    HWND    m_BuilderWindow;

// Construction
public:
     CInterfaceDlg(CWnd* pParent = NULL);    // standard constructor
    ~CInterfaceDlg();

    DWORD   m_BuilderThread;

// Dialog Data
    enum { IDD = IDD_Dialog };

    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support


// Implementation
protected:
    HICON           m_hIcon;

    // Generated message map functions
    virtual BOOL    OnInitDialog();
    afx_msg void    OnCommand();
    afx_msg void    OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void    OnDropFiles(HDROP a_DropInfo);
    afx_msg void    OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL    OnEraseBkgnd(CDC* pDC);
    afx_msg BOOL    OnCopyData(CWnd* a_Wnd, COPYDATASTRUCT* a_CopyDataStruct);
    afx_msg LRESULT OnInitWindowHandle(WPARAM, LPARAM);
    DECLARE_MESSAGE_MAP()

    HRESULT    CreateRenderWindow();

protected:
    void    Apply_TranscodeProfile();

    void    DisplayDecodeParameters();
    void    DisplayEncodeParameters();

    DECLARE_EVENTSINK_MAP()
    void Call_FlashToDialog(LPCTSTR request);
    void Call_DialogToFlash(TCHAR* a_Function, TCHAR* a_Argument = NULL);
    CShockwaveFlash m_Shockwave;

    unsigned int    m_Render_Window_X;
    unsigned int    m_Render_Window_Y;
    unsigned int    m_Render_Window_Width;
    unsigned int    m_Render_Window_Height;

    PluginParameter m_InfoDecoderAudio;
    PluginParameter m_InfoDecoderVideo;

    PluginParameter m_InfoEncoderAudio;
    PluginParameter m_InfoEncoderVideo;
    PluginParameter m_InfoMuxer;

    TranscodeProfile    m_TranscodeProfiles[32];
    int                 m_TranscodeProfileCurrent;
};
