/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#ifndef __AFXWIN_H__
    #error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"   // main symbols


// CInterfaceApp:
// See Interface.cpp for the implementation of this class
//

class CInterfaceApp : public CWinApp
{
public:
    CInterfaceApp();

// Overrides
    public:
    virtual BOOL InitInstance();

// Implementation
};

extern CInterfaceApp theApp;