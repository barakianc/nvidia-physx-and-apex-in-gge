/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include <tchar.h>

#include "builder.h"
#include "message.h"

#include "ds_pipeline.h"
#include "mf_pipeline.h"

Builder g_Builder;

void UpdateAvailablePlugins(HWND hWindow)
{
    HRESULT       hr = S_OK;
    UINT32        nCount = 0;
    IMFActivate** ppActivate = NULL;

    UINT32        nStrLength = 0;
    TCHAR*        pString = NULL;
    BOOL          bPluginFound = FALSE;

    OSVERSIONINFO infoOSVersion;
    BOOL          bVersion = FALSE;
    BOOL          bWin7 = TRUE;

    infoOSVersion.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);    
    bVersion = GetVersionEx(&infoOSVersion);

    if (bVersion
        && (VER_PLATFORM_WIN32_NT == infoOSVersion.dwPlatformId)
        && (6 == infoOSVersion.dwMajorVersion)
        && (1 == infoOSVersion.dwMinorVersion))
    {
        hr = MFTEnumEx(MFT_CATEGORY_VIDEO_DECODER,
            MFT_ENUM_FLAG_ALL,
            NULL,
            NULL,
            &ppActivate,
            &nCount);

        for (size_t i = 0; i < nCount; i++)
        {
            hr = ppActivate[i]->GetStringLength(MFT_FRIENDLY_NAME_Attribute, &nStrLength);

            if (SUCCEEDED(hr))
            {
                pString = new TCHAR[nStrLength + 1];
                if (pString == NULL)
                {
                    hr = E_OUTOFMEMORY;
                }
            }
            if (SUCCEEDED(hr))
            {
                hr = ppActivate[i]->GetString(MFT_FRIENDLY_NAME_Attribute, pString, nStrLength + 1, &nStrLength);
            }

            if (SUCCEEDED(hr) && pString)
            {
                if (_tcsstr(pString, L"Intel"))
                {
                    bPluginFound = TRUE;    
                }
            }

            if (pString)
            {
                delete [] pString;
                pString = NULL;
            }

            ppActivate[i]->Release();

            if (bPluginFound)
            {
                break;
            }        
        }

        CoTaskMemFree(ppActivate);
    }
    else
    {
        bWin7 = false;
    }

    if ((SUCCEEDED(hr) && FALSE == bPluginFound) || !bWin7)
    {
        COPYDATASTRUCT  l_CopyData;
        BuilderCommand  l_BuilderCommand;

        l_CopyData.dwData = MessageType_Command;
        l_CopyData.cbData = sizeof(l_BuilderCommand);
        l_CopyData.lpData = &l_BuilderCommand;

        _tcscpy_s(l_BuilderCommand.m_Command, _T("Use_DS"));
        ::SendMessage(hWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));
        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_MF=false"));
        ::SendMessage(g_Builder.m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
    }

    return;
};

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
    MSG l_Message = {0,};

    if (nCmdShow)
        return 0;

    for(;;PeekMessage(&l_Message, NULL, 0, 0, PM_REMOVE))
    {
        if (l_Message.message == WM_InitWindowHandle)
        {
            WNDCLASS    l_WndClass = {0,};
            HINSTANCE   l_Instance = (HINSTANCE) GetModuleHandle(NULL);

            l_WndClass.lpfnWndProc      = WindowProc;
            l_WndClass.hInstance        = l_Instance;
            l_WndClass.lpszClassName    = _T("Builder");

            RegisterClass(&l_WndClass);

            HWND    l_Window = CreateWindow(_T("Builder"), _T(""), 0, 0, 0, 0, 0, 0, 0, 0, 0);

            g_Builder.m_InterfaceWindow = (HWND)(l_Message.lParam);

            SendMessage(g_Builder.m_InterfaceWindow, WM_InitWindowHandle, 0, (LPARAM)l_Window);

            UpdateAvailablePlugins(l_Window);
        }

        if (l_Message.message == WM_CloseBuilder)
        {
            g_Builder.Close();
            break;
        }

        g_Builder.UpdateStatus();

        TranslateMessage(&l_Message);
        DispatchMessage(&l_Message);

        Sleep(10);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND a_Window, UINT a_Message, WPARAM a_ParamW, LPARAM a_ParamL)
{
    COPYDATASTRUCT*     l_CopyData = (COPYDATASTRUCT*) a_ParamL;
    PluginParameter*    l_PluginParameter;
    BuilderCommand*     l_BuilderCommand;

    if (a_Message == WM_COPYDATA)
    {
        if (!l_CopyData)
            return 0;

        l_PluginParameter   = (PluginParameter*)(l_CopyData->lpData);
        l_BuilderCommand    = (BuilderCommand*) (l_CopyData->lpData);

        if (l_CopyData->dwData == MessageType_StreamInput)
            g_Builder.OpenStream(l_PluginParameter->m_Stream_Name);

        if (l_CopyData->dwData == MessageType_Command)
        {
            if (!_tcscmp(l_BuilderCommand->m_Command, _T("Play")))
                g_Builder.Play();

            if (!_tcscmp(l_BuilderCommand->m_Command, _T("Pause")))
                g_Builder.Pause();

            if (!_tcscmp(l_BuilderCommand->m_Command, _T("Stop")))
                g_Builder.Stop();

            if (!_tcscmp(l_BuilderCommand->m_Command, _T("TranscodeRun")))
                g_Builder.Transcode();

            if (!_tcscmp(l_BuilderCommand->m_Command, _T("Use_MF")))
                g_Builder.m_MediaEngine = 1;

            if (!_tcscmp(l_BuilderCommand->m_Command, _T("Use_DS")))
                g_Builder.m_MediaEngine = 0;
        }

        if (l_CopyData->dwData == MessageType_InfoWindow)
        {
            g_Builder.m_Window_X      = l_PluginParameter->m_Render_Window_X;
            g_Builder.m_Window_Y      = l_PluginParameter->m_Render_Window_Y;
            g_Builder.m_Window_Width  = l_PluginParameter->m_Render_Window_Width;
            g_Builder.m_Window_Height = l_PluginParameter->m_Render_Window_Height;
            g_Builder.m_Window_Parent = l_PluginParameter->m_Render_Window_Parent;

            if (g_Builder.m_RenderVideo)
                g_Builder.m_RenderVideo->SetPluginParameter(l_PluginParameter);
        }

        if (l_CopyData->dwData == MessageType_InfoEncoder)
        {
            if (l_PluginParameter->m_Type == ParameterType_Audio)
                memcpy(&g_Builder.m_InfoEncoderAudio, l_PluginParameter, l_CopyData->cbData);

            if (l_PluginParameter->m_Type == ParameterType_Video)
                memcpy(&g_Builder.m_InfoEncoderVideo, l_PluginParameter, l_CopyData->cbData);

            if (l_PluginParameter->m_Type == ParameterType_Stream)
                memcpy(&g_Builder.m_InfoMuxer, l_PluginParameter, l_CopyData->cbData);
        }
    }

    return DefWindowProc(a_Window, a_Message, a_ParamW, a_ParamL);
}

Builder::Builder()
{
    m_InterfaceWindow   = NULL;
    m_MediaEngine       = 1;
    m_Status            = Empty;

    m_Pipeline          = NULL;
    m_Source            = NULL;
    m_Splitter          = NULL;
    m_DecoderVideo      = NULL;
    m_EncoderVideo      = NULL;
    m_DecoderAudio      = NULL;
    m_EncoderAudio      = NULL;
    m_RenderVideo       = NULL;
    m_RenderAudio       = NULL;
    m_WriterMP4         = NULL;
    m_WriterFile        = NULL;

    m_Window_X          = 0;
    m_Window_Y          = 0;
    m_Window_Width      = 0;
    m_Window_Height     = 0;
    m_Window_Parent     = NULL;

    // Setup default audio encoder profile
    {
        m_InfoEncoderAudio.m_Type                       = ParameterType_Audio;
        m_InfoEncoderAudio.m_SubType                    = ParameterSubType_AudioAAC;
        m_InfoEncoderAudio.m_Audio_SampleRate           = 44100;
        m_InfoEncoderAudio.m_Audio_Channels             = 2;
        m_InfoEncoderAudio.m_Audio_BitsPerSample        = 16;
        m_InfoEncoderAudio.m_Audio_BlockAlignment       = 1;
        m_InfoEncoderAudio.m_Audio_BytesPerSecond       = 16000;
        m_InfoEncoderAudio.m_Audio_Compressed           = 1;
    }

    // Setup default video encoder profile
    {
        m_InfoEncoderVideo.m_Type                       = ParameterType_Video;
        m_InfoEncoderVideo.m_SubType                    = ParameterSubType_VideoH264;
        m_InfoEncoderVideo.m_Video_BitRate              = 528560;
        m_InfoEncoderVideo.m_Video_FrameRateNumerator   = 30000;
        m_InfoEncoderVideo.m_Video_FrameRateDenominator = 1001;
        m_InfoEncoderVideo.m_Video_FrameWidth           = 720;
        m_InfoEncoderVideo.m_Video_FrameHeight          = 480;
        m_InfoEncoderVideo.m_Video_InterlaceMode        = 7;
        m_InfoEncoderVideo.m_Video_PixelAspectRatioX    = 1;
        m_InfoEncoderVideo.m_Video_PixelAspectRatioY    = 1;
    }

    // Setup default output file name
    {
        m_InfoMuxer.m_Type      = ParameterType_Stream;
        m_InfoMuxer.m_SubType   = ParameterSubType_StreamMP4;
        _tcscpy_s(m_InfoMuxer.m_Stream_Name, _T("transcoded.mp4"));
    }

    m_VideoDisplayAspectRatio = 0;
}

Builder::~Builder()
{
    Close();
}

HRESULT Builder::OpenStream(TCHAR* a_FileName)
{
    HRESULT         l_Result = S_OK;
    int             l_Index;
    PluginParameter l_ParameterStream;
    PluginParameter l_ParameterDecoder;
    DS_Pipeline*    l_PipelineDS    = NULL;
    MF_Pipeline*    l_PipelineMF    = NULL;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_ParameterStream.m_Type = ParameterType_Stream;
    _tcscpy_s(l_ParameterStream.m_Stream_Name, a_FileName);

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayStatus"));
    _tcscpy_s(l_BuilderCommand.m_Info,    _T("Error: status is unknown"));

    Close();

    // Pipeline initialization
    if (SUCCEEDED(l_Result))
    {
        if (m_MediaEngine == 0)
            m_Pipeline = new DS_Pipeline();

        if (m_MediaEngine == 1)
            m_Pipeline = new MF_Pipeline();

        if (m_Pipeline == NULL)
            l_Result = E_FAIL;

        if (SUCCEEDED(l_Result))
            l_Result = m_Pipeline->Init();

        l_PipelineDS = dynamic_cast<DS_Pipeline*> (m_Pipeline);
        l_PipelineMF = dynamic_cast<MF_Pipeline*> (m_Pipeline);

        if ( NULL == l_PipelineDS && NULL == l_PipelineMF )
            l_Result = E_FAIL;

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Pipeline"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Source creation
    if (SUCCEEDED(l_Result))
    {
        m_Source = m_Pipeline->AddPlugin(Source_File);

        if (m_Source == NULL)
            l_Result = E_FAIL;

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Source Reader"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Source & demuxer initialization
    if (SUCCEEDED(l_Result))
    {
        l_Result = m_Source->SetPluginParameter(&l_ParameterStream);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unsupported media container"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    if (SUCCEEDED(l_Result))
    {
        if (l_PipelineMF)
            m_Splitter = m_Source;

        if (l_PipelineDS)
        {
            l_Result = E_FAIL;

            // Search for applicable splitter
            for (l_Index = 0; FAILED(l_Result); l_Index++)
            {
                if (m_Splitter)
                {
                    m_Pipeline->DelPlugin(m_Splitter);
                    m_Splitter = NULL;
                }

                if (l_Index == 0)
                    m_Splitter = m_Pipeline->AddPlugin(Splitter_MPEG2);

                if (l_Index == 1)
                    m_Splitter = m_Pipeline->AddPlugin(Splitter_MP4);

                if (m_Splitter == NULL)
                    break;

                l_Result = m_Pipeline->Connect(m_Source, m_Splitter);
            }

            if (FAILED(l_Result))
            {
                _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Splitter"));
                SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }
    }

    // Decoders initialization
    if (SUCCEEDED(l_Result))
    {
        l_ParameterDecoder.m_Type = ParameterType_Audio;
        l_Result = m_Splitter->GetPluginParameter(&l_ParameterDecoder);

        if (SUCCEEDED(l_Result))
        {
            if (l_ParameterDecoder.m_SubType == ParameterSubType_AudioPCM)
                m_DecoderAudio  = m_Splitter;

            if (l_ParameterDecoder.m_SubType == ParameterSubType_AudioAAC)
                m_DecoderAudio  = m_Pipeline->AddPlugin(DecoderAudio_AAC);

            if (l_ParameterDecoder.m_SubType == ParameterSubType_AudioAC3)
                m_DecoderAudio  = m_Pipeline->AddPlugin(DecoderAudio_AC3);

            if (l_ParameterDecoder.m_SubType == ParameterSubType_AudioMP3)
                m_DecoderAudio  = m_Pipeline->AddPlugin(DecoderAudio_MP3);

            if (l_ParameterDecoder.m_SubType == ParameterSubType_AudioWMA)
                m_DecoderAudio  = m_Pipeline->AddPlugin(DecoderAudio_WMA);

            if (m_DecoderAudio == NULL)
                l_Result = E_FAIL;

            if (FAILED(l_Result))
            {
                _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Audio decoder"));
                SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }

        l_ParameterDecoder.m_Type = ParameterType_Video;
        l_Result = m_Splitter->GetPluginParameter(&l_ParameterDecoder);

        if (SUCCEEDED(l_Result))
        {
            if (l_ParameterDecoder.m_SubType == ParameterSubType_VideoMPEG2)
                m_DecoderVideo  = m_Pipeline->AddPlugin(DecoderVideo_MPEG2);

            if (l_ParameterDecoder.m_SubType == ParameterSubType_VideoH264)
                m_DecoderVideo  = m_Pipeline->AddPlugin(DecoderVideo_H264);

            if (l_ParameterDecoder.m_SubType == ParameterSubType_VideoVC1)
                m_DecoderVideo  = m_Pipeline->AddPlugin(DecoderVideo_VC1);

            if (m_DecoderVideo == NULL)
                l_Result = E_FAIL;

            if (FAILED(l_Result))
            {
                _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Video decoder"));
                SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }

        if (m_DecoderAudio || m_DecoderVideo)
            l_Result = S_OK;
        else
        {
            l_Result = E_FAIL;
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to use any decoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Connect audio decoder
    if (SUCCEEDED(l_Result))
    {
        if (m_DecoderAudio != NULL && m_DecoderAudio != m_Splitter)
            l_Result = m_Pipeline->Connect(m_Splitter, m_DecoderAudio);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Demuxer to Audio decoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Connect video decoder
    if (SUCCEEDED(l_Result))
    {
        if (m_DecoderVideo != NULL)
            l_Result = m_Pipeline->Connect(m_Splitter, m_DecoderVideo);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Demuxer to Video decoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    if (SUCCEEDED(l_Result))
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: stream is opened"));
    else
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to open a stream"));

    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    if (SUCCEEDED(l_Result))
    {
        _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Play=true"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_TranscodeRun=true"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        //_tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Transcode=true"));
        //SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        if (m_MediaEngine == 0)
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_MF=false"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }

        if (m_MediaEngine == 1)
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_DS=false"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }
    else
        Close();

    // Init Audio Encoder profile
    if (SUCCEEDED(l_Result))
    {
        l_ParameterDecoder.m_Type = ParameterType_Audio;
        l_Result = m_Splitter->GetPluginParameter(&l_ParameterDecoder);

        if (SUCCEEDED(l_Result))
        {
            m_InfoEncoderAudio.m_Audio_SampleRate           = 44100;    //l_ParameterDecoder.m_Audio_SampleRate;
            m_InfoEncoderAudio.m_Audio_Channels             = l_ParameterDecoder.m_Audio_Channels;
            m_InfoEncoderAudio.m_Audio_BitsPerSample        = 16;       //l_ParameterDecoder.m_Audio_BitsPerSample;
            m_InfoEncoderAudio.m_Audio_BlockAlignment       = 1;        //l_ParameterDecoder.m_Audio_BlockAlignment;
            m_InfoEncoderAudio.m_Audio_BytesPerSecond       = 16000;    //l_ParameterDecoder.m_Audio_BytesPerSecond;
            m_InfoEncoderAudio.m_Audio_Compressed           = 1;
        }

        l_Result = S_OK;
    }

    // Init Video Encoder profile
    if (SUCCEEDED(l_Result))
    {
        l_ParameterDecoder.m_Type = ParameterType_Video;
        l_Result = m_Splitter->GetPluginParameter(&l_ParameterDecoder);

        if (SUCCEEDED(l_Result))
        {
            m_InfoEncoderVideo.m_Video_BitRate              = 200 * 1024;
            m_InfoEncoderVideo.m_Video_FrameRateNumerator   = l_ParameterDecoder.m_Video_FrameRateNumerator;
            m_InfoEncoderVideo.m_Video_FrameRateDenominator = l_ParameterDecoder.m_Video_FrameRateDenominator;
            m_InfoEncoderVideo.m_Video_FrameWidth           = l_ParameterDecoder.m_Video_FrameWidth;
            m_InfoEncoderVideo.m_Video_FrameHeight          = l_ParameterDecoder.m_Video_FrameHeight;
            m_InfoEncoderVideo.m_Video_InterlaceMode        = l_ParameterDecoder.m_Video_InterlaceMode;
            m_InfoEncoderVideo.m_Video_PixelAspectRatioX    = l_ParameterDecoder.m_Video_PixelAspectRatioX;
            m_InfoEncoderVideo.m_Video_PixelAspectRatioY    = l_ParameterDecoder.m_Video_PixelAspectRatioY;

            if (m_InfoEncoderVideo.m_Video_FrameHeight)
                m_VideoDisplayAspectRatio =
                    (double) m_InfoEncoderVideo.m_Video_FrameWidth /
                    (double) m_InfoEncoderVideo.m_Video_FrameHeight;
            else
                m_VideoDisplayAspectRatio = 0;

            if (m_InfoEncoderVideo.m_Video_FrameWidth >  352)
                m_InfoEncoderVideo.m_Video_BitRate          =   900 * 1024;

            if (m_InfoEncoderVideo.m_Video_FrameWidth >  640)
                m_InfoEncoderVideo.m_Video_BitRate          =  6000 * 1024;

            if (m_InfoEncoderVideo.m_Video_FrameWidth > 1280)
                m_InfoEncoderVideo.m_Video_BitRate          =  9000 * 1024;

            if (m_InfoEncoderVideo.m_Video_FrameWidth > 1920)
                m_InfoEncoderVideo.m_Video_BitRate          = 11500 * 1024;
        }

        l_Result = S_OK;
    }

    // Display decode parameters
    if (SUCCEEDED(l_Result))
    {
        l_CopyData.dwData = MessageType_InfoDecoder;
        l_CopyData.cbData = sizeof(PluginParameter);
        l_CopyData.lpData = &l_ParameterDecoder;

        l_ParameterDecoder.m_Type    = ParameterType_Audio;
        l_ParameterDecoder.m_SubType = ParameterSubType_None;
        l_Result = m_Splitter->GetPluginParameter(&l_ParameterDecoder);

        if (SUCCEEDED(l_Result))
            ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_ParameterDecoder.m_Type    = ParameterType_Video;
        l_ParameterDecoder.m_SubType = ParameterSubType_None;
        l_Result = m_Splitter->GetPluginParameter(&l_ParameterDecoder);

        if (SUCCEEDED(l_Result))
            ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_CopyData.dwData = MessageType_Command;
        l_CopyData.cbData = sizeof(l_BuilderCommand);
        l_CopyData.lpData = &l_BuilderCommand;

        _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayDecodeParameters"));
        ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_Result = S_OK;
    }

    // Display encode parameters
    if (SUCCEEDED(l_Result))
    {
        l_CopyData.dwData = MessageType_InfoEncoder;
        l_CopyData.cbData = sizeof(PluginParameter);

        l_CopyData.lpData = &m_InfoEncoderAudio;
        ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_CopyData.lpData = &m_InfoEncoderVideo;
        ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_CopyData.lpData = &m_InfoMuxer;
        ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_CopyData.dwData = MessageType_Command;
        l_CopyData.cbData = sizeof(l_BuilderCommand);
        l_CopyData.lpData = &l_BuilderCommand;

        _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayEncodeParameters"));
        ::SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        l_Result = S_OK;
    }

    if (SUCCEEDED(l_Result))
        m_Status = ReadyToDecode;

    return l_Result;
}

HRESULT Builder::Play()
{
    HRESULT         l_Result = S_OK;
    PluginParameter l_ParameterRender;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayStatus"));
    _tcscpy_s(l_BuilderCommand.m_Info,    _T("Error: status is unknown"));

    if (m_Status == RunningPlayback)
    {
        l_Result = m_Pipeline->Play();

        if (SUCCEEDED(l_Result))
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: playback is started"));
        else
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to start playback"));

        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        if (SUCCEEDED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));

            _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Play=false"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Pause=true"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Stop=true"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }

        return l_Result;
    }

    if (m_Status != ReadyToDecode)
        l_Result = E_FAIL;

    // Audio render initialization
    if (SUCCEEDED(l_Result))
    {
        if (m_DecoderAudio != NULL)
        {
            m_RenderAudio   = m_Pipeline->AddPlugin(RenderAudio);

            if (m_RenderAudio == NULL)
                l_Result = E_FAIL;
        }

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Audio render"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Video render initialization
    if (SUCCEEDED(l_Result))
    {
        if (m_DecoderVideo != NULL)
        {
            m_RenderVideo   = m_Pipeline->AddPlugin(RenderVideoEnhanced);

            if (m_RenderVideo == NULL)
                l_Result = E_FAIL;
        }

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Video render"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Connect Audio decoder to render
    if (SUCCEEDED(l_Result))
    if (m_RenderAudio != NULL)
    {
        l_Result = m_Pipeline->Connect(m_DecoderAudio, m_RenderAudio);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Audio decoder to render"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Connect Video decoder to render
    if (SUCCEEDED(l_Result))
    if (m_RenderVideo != NULL)
    {
        l_Result = m_Pipeline->Connect(m_DecoderVideo, m_RenderVideo);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Video decoder to render"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Setup Video Render output window
    if (SUCCEEDED(l_Result))
    {
        if (m_RenderVideo != NULL)
        {
            l_ParameterRender.m_Type                    = ParameterType_RenderVideo;
            l_ParameterRender.m_Render_Window_X         = m_Window_X;
            l_ParameterRender.m_Render_Window_Y         = m_Window_Y;
            l_ParameterRender.m_Render_Window_Width     = m_Window_Width;
            l_ParameterRender.m_Render_Window_Height    = m_Window_Height;
            l_ParameterRender.m_Render_Window_Parent    = m_Window_Parent;

            l_Result = m_RenderVideo->SetPluginParameter(&l_ParameterRender);

            if (FAILED(l_Result))
            {
                _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to set Video render output window"));
                SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }
    }

    if (SUCCEEDED(l_Result))
        l_Result = m_Pipeline->Play();

    if (SUCCEEDED(l_Result))
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: playback is started"));
    else
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to start playback"));

    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    if (SUCCEEDED(l_Result))
    {
        _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Play=false"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Pause=true"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Stop=true"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_TranscodeRun=false"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
    }
    else
        Close();

    if (SUCCEEDED(l_Result))
        m_Status = RunningPlayback;

    return l_Result;
}

HRESULT Builder::Pause()
{
    HRESULT         l_Result = E_FAIL;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayStatus"));
    _tcscpy_s(l_BuilderCommand.m_Info,    _T("Error: status is unknown"));

    if (m_Pipeline)
        l_Result = m_Pipeline->Pause();

    if (SUCCEEDED(l_Result))
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: playback is paused"));
    else
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to pause playback"));

    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    if (SUCCEEDED(l_Result))
    {
        _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Play=true"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Pause=false"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
    }

    return l_Result;
}

HRESULT Builder::Stop()
{
    HRESULT         l_Result = E_FAIL;
    PluginParameter l_ParameterStream;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayStatus"));
    _tcscpy_s(l_BuilderCommand.m_Info,    _T("Error: status is unknown"));

    if (m_Pipeline)
        l_Result = m_Pipeline->Stop();

    if (SUCCEEDED(l_Result))
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: playback is stopped"));
    else
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to stop playback"));

    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    if (SUCCEEDED(l_Result))
    {
        l_ParameterStream.m_Type = ParameterType_Stream;

        if (SUCCEEDED(l_Result))
            l_Result = m_Source->GetPluginParameter(&l_ParameterStream);

        if (SUCCEEDED(l_Result))
            l_Result = OpenStream(l_ParameterStream.m_Stream_Name);
    }

    return l_Result;
}

HRESULT Builder::Transcode()
{
    HRESULT         l_Result = S_OK;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;
    DS_Pipeline*    l_PipelineDS    = dynamic_cast<DS_Pipeline*> (m_Pipeline);
    MF_Pipeline*    l_PipelineMF    = dynamic_cast<MF_Pipeline*> (m_Pipeline);

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayStatus"));
    _tcscpy_s(l_BuilderCommand.m_Info,    _T("Error: status is unknown"));

    if (!m_Pipeline)
        l_Result = E_FAIL;

    if (m_Status != ReadyToDecode)
        l_Result = E_FAIL;

    if (FAILED(l_Result))
    {
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to start transcoding"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
    }

    // Audio encoder initialization
    if (SUCCEEDED(l_Result))
    if (m_DecoderAudio != NULL)
    {
        if (m_InfoEncoderAudio.m_SubType == ParameterSubType_AudioAAC)
            m_EncoderAudio  = m_Pipeline->AddPlugin(EncoderAudio_AAC);

        if (m_InfoEncoderAudio.m_SubType == ParameterSubType_AudioAC3)
            m_EncoderAudio  = m_Pipeline->AddPlugin(EncoderAudio_AC3);

        if (m_EncoderAudio == NULL)
            l_Result = E_FAIL;

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Audio encoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Video encoder initialization
    if (SUCCEEDED(l_Result))
    if (m_DecoderVideo != NULL)
    {
        if (m_InfoEncoderVideo.m_SubType == ParameterSubType_VideoMPEG2)
            m_EncoderVideo  = m_Pipeline->AddPlugin(EncoderVideo_MPEG2);

        if (m_InfoEncoderVideo.m_SubType == ParameterSubType_VideoH264)
            m_EncoderVideo  = m_Pipeline->AddPlugin(EncoderVideo_H264);

        if (m_InfoEncoderVideo.m_SubType == ParameterSubType_VideoVC1)
            m_EncoderVideo  = m_Pipeline->AddPlugin(EncoderVideo_VC1);

        if (m_EncoderVideo == NULL)
            l_Result = E_FAIL;

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create Video encoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Apply Audio encoder profile
    if (SUCCEEDED(l_Result))
    if (m_EncoderAudio != NULL)
    {
        l_Result = m_EncoderAudio->SetPluginParameter(&m_InfoEncoderAudio);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to apply Audio encoder settings"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Apply Video encoder profile
    if (SUCCEEDED(l_Result))
    if (m_EncoderVideo != NULL)
    {
        l_Result = m_EncoderVideo->SetPluginParameter(&m_InfoEncoderVideo);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to apply Video encoder settings"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Muxer initialization
    if (SUCCEEDED(l_Result))
    {
        m_WriterMP4 = m_Pipeline->AddPlugin(Writer_MP4);

        if (m_WriterMP4 == NULL)
            l_Result = E_FAIL;

        if (l_PipelineDS)
        {
            m_WriterFile = m_Pipeline->AddPlugin(Writer_File);

            if (m_WriterFile == NULL)
                l_Result = E_FAIL;
        }

        if (l_PipelineMF)
            m_WriterFile = m_WriterMP4;

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to create MP4 muxer"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Setup output file name
    if (SUCCEEDED(l_Result))
    {
        l_Result = m_WriterFile->SetPluginParameter(&m_InfoMuxer);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to set output file name"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    // Connect Audio encoder plugins
    if (SUCCEEDED(l_Result))
    if (m_EncoderAudio != NULL)
    {
        l_Result = m_Pipeline->Connect(m_DecoderAudio, m_EncoderAudio);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Audio decoder to encoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }

        if (SUCCEEDED(l_Result))
        {
            l_Result = m_Pipeline->Connect(m_EncoderAudio, m_WriterMP4);

            if (FAILED(l_Result))
            {
                _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Audio encoder to muxer"));
                SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }
    }

    // Connect Video encoder plugins
    if (SUCCEEDED(l_Result))
    if (m_EncoderVideo != NULL)
    {
        l_Result = m_Pipeline->Connect(m_DecoderVideo, m_EncoderVideo);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Video decoder to encoder"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }

        if (SUCCEEDED(l_Result))
        {
            l_Result = m_Pipeline->Connect(m_EncoderVideo, m_WriterMP4);

            if (FAILED(l_Result))
            {
                _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Video encoder to muxer"));
                SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
            }
        }
    }

    // Connect Muxer to File Writer
    if (SUCCEEDED(l_Result))
    if (l_PipelineDS)
    {
        l_Result = m_Pipeline->Connect(m_WriterMP4, m_WriterFile);

        if (FAILED(l_Result))
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to connect Muxer to File Writer"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
        }
    }

    if (SUCCEEDED(l_Result))
        l_Result = m_Pipeline->Play();

    if (SUCCEEDED(l_Result))
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: transcoding is started"));
    else
        _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: unable to start transcoding"));

    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    if (SUCCEEDED(l_Result))
    {
        m_Status = RunningTranscode;

        _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Play=false"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Stop=true"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

        _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_TranscodeRun=false"));
        SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);
    }
    else
        Stop();

    return l_Result;
}

HRESULT Builder::Close()
{
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("EnableControl"));

    _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Play=false"));
    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Pause=false"));
    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    _tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Stop=false"));
    SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    //_tcscpy_s(l_BuilderCommand.m_Info, _T("id_Button_Transcode=false"));
    //SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

    if (m_Pipeline != NULL)
        delete m_Pipeline;

    m_Pipeline      = NULL;
    m_Source        = NULL;
    m_Splitter      = NULL;
    m_DecoderVideo  = NULL;
    m_EncoderVideo  = NULL;
    m_DecoderAudio  = NULL;
    m_EncoderAudio  = NULL;
    m_RenderVideo   = NULL;
    m_RenderAudio   = NULL;
    m_WriterMP4     = NULL;
    m_WriterFile    = NULL;

    m_Status = Empty;

    return S_OK;
}

void Builder::UpdateStatus()
{
    DWORD           l_Result = WAIT_ABANDONED;
    COPYDATASTRUCT  l_CopyData;
    BuilderCommand  l_BuilderCommand;

    l_CopyData.dwData = MessageType_Command;
    l_CopyData.cbData = sizeof(l_BuilderCommand);
    l_CopyData.lpData = &l_BuilderCommand;

    _tcscpy_s(l_BuilderCommand.m_Command, _T("DisplayStatus"));

    if (m_Pipeline == NULL)
        return;

    if (m_Status == RunningPlayback || m_Status == RunningTranscode)
    {
        l_Result = m_Pipeline->Wait(0);

        if (l_Result == WAIT_OBJECT_0)
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Success: end of stream"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            Stop();
        }

        if (l_Result == -1)
        {
            _tcscpy_s(l_BuilderCommand.m_Info, _T("Error: playback is interrupted"));
            SendMessage(m_InterfaceWindow, WM_COPYDATA, (WPARAM)0, (LPARAM)&l_CopyData);

            Stop();
        }
    }
}
