/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#include "pipeline.h"

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&);               \
    void operator=(const TypeName&)   
#endif

LRESULT CALLBACK WindowProc(HWND a_Window, UINT a_Message, WPARAM a_ParamW, LPARAM a_ParamL);

enum BuilderStatus
{
    Empty,
    ReadyToDecode,
    RunningPlayback,
    RunningTranscode
};

class Builder
{
public:
     Builder();
    ~Builder();

public:
    HRESULT         OpenStream(TCHAR* a_FileName);
    HRESULT         Play();
    HRESULT         Pause();
    HRESULT         Stop();
    HRESULT         Transcode();
    HRESULT         Close();
    void            UpdateStatus();

    HWND            m_InterfaceWindow;
    int             m_MediaEngine;      // 0 - Direct Show, 1 - Media Foundation (default and enabled only on Windows 7)
    BuilderStatus   m_Status;
    int             m_Window_X;
    int             m_Window_Y;
    int             m_Window_Width;
    int             m_Window_Height;
    HWND            m_Window_Parent;

    PluginParameter m_InfoEncoderAudio;
    PluginParameter m_InfoEncoderVideo;
    PluginParameter m_InfoMuxer;

    BasePipeline*   m_Pipeline;

public:
    BasePlugin*     m_Source;
    BasePlugin*     m_Splitter;
    BasePlugin*     m_DecoderVideo;
    BasePlugin*     m_EncoderVideo;
    BasePlugin*     m_DecoderAudio;
    BasePlugin*     m_EncoderAudio;
    BasePlugin*     m_RenderVideo;
    BasePlugin*     m_RenderAudio;
    BasePlugin*     m_WriterMP4;
    BasePlugin*     m_WriterFile;

private:
    double          m_VideoDisplayAspectRatio;
    DISALLOW_COPY_AND_ASSIGN(Builder);
};
