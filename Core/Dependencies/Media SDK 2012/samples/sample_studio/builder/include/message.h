/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#define WM_InitWindowHandle WM_USER + 1
#define WM_CloseBuilder     WM_USER + 2

enum BuiderMessageType
{
    MessageType_Command,
    MessageType_StreamInput,
    MessageType_StreamOutput,
    MessageType_InfoWindow,
    MessageType_InfoDecoder,
    MessageType_InfoEncoder
};

class BuilderCommand
{
public:
    TCHAR   m_Command[32];
    TCHAR   m_Info[256];

    BuilderCommand() { memset(this, 0, sizeof(this)); }
};