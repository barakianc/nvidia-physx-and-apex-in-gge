/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#include "pipeline.h"
#include "ds_guids.h"

#include <dshow.h>
#include <evr.h>
#include <dvdmedia.h>

class DS_Plugin : public BasePlugin
{
    HWND        m_Window;
    HRESULT     SetVideoRenderWindow(PluginParameter* a_Parameter);

public:

    PluginType      m_Type;
    IBaseFilter*    m_Filter;

public:

     DS_Plugin(PluginType a_Type);
     virtual ~DS_Plugin();

    HRESULT     GetInterface(GUID a_GUID, void** a_Interface);

    HRESULT     SetInputStream(PluginParameter* a_Parameter);
    HRESULT     SetOutputStream(PluginParameter* a_Parameter);

    HRESULT     SetPluginParameter(PluginParameter* a_Parameter, int a_Index = 0);
};

class DS_Pipeline : public BasePipeline
{
public:

    IGraphBuilder*      m_Graph;
    IMediaControl*      m_MediaControl;
    IMediaSeeking*      m_MediaSeeking;
    IFileSourceFilter*  m_Source;
    IMediaEvent*        m_MediaEvent;

public:

    Array               m_Plugins;

public:

     DS_Pipeline();
    ~DS_Pipeline();

    HRESULT     Init();
    HRESULT     Close();

    BasePlugin* AddPlugin(PluginName a_PluginName);
    BasePlugin* AddPlugin(PluginType a_PluginType, GUID a_GUID = GUID_NULL);
    HRESULT     DelPlugin(BasePlugin* a_Plugin);

    HRESULT     Connect   (BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst);
    HRESULT     Disconnect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst);

    HRESULT     Play();
    HRESULT     Pause();
    HRESULT     Stop();

    HRESULT     Seek(double a_AbsolutePosition, double a_Rate);
    HRESULT     Move(double a_RelativePosition, double a_Rate);
    HRESULT     SetRate(double a_Rate);

    HRESULT     GetDuration(double* a_Duration);
    HRESULT     GetPosition(double* a_Position);

    DWORD       Wait(DWORD a_Milliseconds);

    virtual HRESULT     GetInterface(GUID a_GUID, void** a_Interface);

private:

    HRESULT     Connect_Pin(IPin* a_Pin, DS_Plugin* a_PluginDst);
};
