/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#include "pipeline.h"
#include "mf_guids.h"

#include <mfapi.h>
#include <mfidl.h>
#include <evr.h>

class MF_Plugin : public BasePlugin, public IMFAsyncCallback
{
    ULONG                       m_RefCount;
    IMFMediaSession*            m_Session;
    IMFTopology*                m_Topology;

    HRESULT     SetVideoRenderWindow(PluginParameter* a_Parameter);

public:

    PluginType                  m_Type;
    Array                       m_Nodes;
    IMFSourceResolver*          m_SourceResolver;
    IMFMediaSource*             m_MediaSource;
    IMFByteStream*              m_ByteStream;
    IMFMediaSink*               m_MediaSink;
    IMFMediaType*               m_MediaType;
    IMFTransform*               m_Transform;
    IMFFinalizableMediaSink*    m_FinalizableMediaSink;
    HWND                        m_Window;
    HANDLE                      m_EventFinalized;
    Array                       m_PluginsToConnect;

public:

     MF_Plugin(PluginType a_Type);
     virtual ~MF_Plugin();

    IMFTopologyNode*    GetNode(int a_Index);
    HRESULT             AddNode(IMFTopologyNode* a_Node);
    HRESULT             GetInterface(GUID a_GUID, void** a_Interface);

    HRESULT SetInputStream(PluginParameter* a_Parameter);
    HRESULT SetOutputStream(PluginParameter* a_Parameter);

    HRESULT SetPluginParameter(PluginParameter* a_Parameter, int a_Index = 0);

    void    SetTopology(IMFTopology* a_Topology, IMFMediaSession* a_Session) { m_Topology = a_Topology; m_Session = a_Session; }

    // IUnknown
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID a_IID, void** a_Interface);
    ULONG   STDMETHODCALLTYPE AddRef(void);
    ULONG   STDMETHODCALLTYPE Release(void);

    // IMFAsyncCallback
    HRESULT STDMETHODCALLTYPE GetParameters(DWORD* a_Flags, DWORD* a_Queue);
    HRESULT STDMETHODCALLTYPE Invoke(IMFAsyncResult* a_Result);

public:
    HRESULT ConvertMediaTypeToPluginParameter(IMFMediaType* a_MediaType, PluginParameter* a_Parameter);
    HRESULT ConvertPluginParameterToMediaType(PluginParameter* a_Parameter, IMFMediaType** a_MediaType);
    HRESULT ConnectFinalize();
    HRESULT ConnectFinalize_RenderVideo();
    HRESULT ConnectFinalize_Muxer();
};

class MF_Pipeline : public BasePipeline, public IMFAsyncCallback
{
public:

    ULONG                       m_RefCount;

    IMFMediaSession*            m_Session;
    IMFTopology*                m_Topology;
    IMFVideoDisplayControl*     m_VideoDisplay;
    IMFRateSupport*             m_RateSupport;
    IMFRateControl*             m_RateControl;
    IMFPresentationClock*       m_PresentationClock;
    UINT64                      m_Duration;

    HANDLE                      m_EventSessionStarted;
    HANDLE                      m_EventSessionStopped;
    HANDLE                      m_EventSessionClosed;
    HANDLE                      m_EventTopologyReady;
    HANDLE                      m_EventSessionTopologySet;
    HANDLE                      m_EventError;

    Array                       m_Plugins;

public:

     MF_Pipeline();
    ~MF_Pipeline();

    HRESULT     Init();
    HRESULT     Close();

    BasePlugin* AddPlugin(PluginName a_PluginName);
    BasePlugin* AddPlugin(PluginType a_PluginType, GUID a_GUID = GUID_NULL);
    HRESULT     DelPlugin(BasePlugin* a_Plugin);

    HRESULT     Connect   (BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst);
    HRESULT     Disconnect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst);

    HRESULT     Resolve();
    HRESULT     ResolveTranscodeTopology(MF_Plugin* a_TranscodeProfile);

    HRESULT     Play();
    HRESULT     Pause();
    HRESULT     Stop();

    HRESULT     Seek    (double a_AbsolutePosition, double a_Rate);
    HRESULT     Move    (double a_RelativePosition, double a_Rate);
    HRESULT     SetRate (double a_Rate);

    HRESULT     GetDuration(double* a_Duration);
    HRESULT     GetPosition(double* a_Position);

    DWORD       Wait(DWORD a_Milliseconds);

    virtual HRESULT     GetInterface(GUID a_GUID, void** a_Interface);

    // IUnknown
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID a_IID, void** a_Interface);
    ULONG   STDMETHODCALLTYPE AddRef(void);
    ULONG   STDMETHODCALLTYPE Release(void);

    // IMFAsyncCallback
    HRESULT STDMETHODCALLTYPE GetParameters(DWORD* a_Flags, DWORD* a_Queue);
    HRESULT STDMETHODCALLTYPE Invoke(IMFAsyncResult* a_Result);

private:
    MF_Plugin*  AddPlugin_Simple(PluginType a_PluginType);
    MF_Plugin*  AddPlugin_Transform(GUID a_GUID);
    MF_Plugin*  AddPlugin_RenderAudio();
    MF_Plugin*  AddPlugin_FileWriter();

    HRESULT     Connect_Delayed(MF_Plugin* a_PluginSrc, MF_Plugin* a_PluginDst);

    HRESULT     PluginArray_Del(MF_Plugin* a_Plugin);
    HRESULT     PluginArray_DelAll();
};
