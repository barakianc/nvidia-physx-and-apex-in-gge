/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#include "utils.h"

#include <windows.h>
#include <cguid.h>

class BasePlugin
{
public:
    ~BasePlugin();

    Array   m_Parameters;

    virtual HRESULT GetInterface(GUID a_GUID, void** a_Interface) = 0;

    virtual HRESULT GetPluginParameter(PluginParameter* a_Parameter, int a_Index = 0);
    virtual HRESULT SetPluginParameter(PluginParameter* a_Parameter, int a_Index = 0);
};

class BasePipeline
{
public:
    virtual ~BasePipeline() {}

    virtual HRESULT Init()  = 0;
    virtual HRESULT Close() = 0;

    virtual BasePlugin* AddPlugin(PluginName a_PluginName) = 0;
    virtual BasePlugin* AddPlugin(PluginType a_PluginType, GUID a_GUID = GUID_NULL) = 0;
    virtual HRESULT     DelPlugin(BasePlugin* a_Plugin) = 0;

    virtual HRESULT     Connect   (BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst) = 0;
    virtual HRESULT     Disconnect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst) = 0;

    virtual HRESULT     Play()  = 0;
    virtual HRESULT     Pause() = 0;
    virtual HRESULT     Stop()  = 0;

    virtual HRESULT     Seek(double a_AbsolutePosition, double a_Rate) = 0;
    virtual HRESULT     Move(double a_RelativePosition, double a_Rate) = 0;
    virtual HRESULT     SetRate(double a_Rate) = 0;

    virtual HRESULT     GetDuration(double* a_Duration) = 0;
    virtual HRESULT     GetPosition(double* a_Position) = 0;

    virtual DWORD       Wait(DWORD a_Milliseconds) = 0;

    virtual HRESULT     GetInterface(GUID a_GUID, void** a_Interface) = 0;
};
