/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#pragma once

#include <windows.h>

#ifndef MSDK_SAFE_RELEASE
#define MSDK_SAFE_RELEASE(x) { if (x) { x->Release(); x = NULL; } }
#endif

#ifndef MSDK_SAFE_DELETE
#define MSDK_SAFE_DELETE(x) { delete x; x = NULL; }
#endif

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&);               \
    void operator=(const TypeName&)   
#endif

enum PluginName
{
    Source_File,
    Source_Url,

    Splitter_MP4,
    Splitter_MPEG2,

    DecoderVideo_MPEG2, EncoderVideo_MPEG2,
    DecoderVideo_H264,  EncoderVideo_H264,
    DecoderVideo_VC1,   EncoderVideo_VC1,

    DecoderAudio_AAC,   EncoderAudio_AAC,
    DecoderAudio_AC3,   EncoderAudio_AC3,
    DecoderAudio_MP3,   EncoderAudio_MP3,
    DecoderAudio_WMA,   EncoderAudio_WMA,

    RenderVideo,
    RenderVideoDefault,
    RenderVideoEnhanced,
    RenderAudio,

    Writer_MP4,
    Writer_File
};

enum PluginType
{
    PluginType_Source,
    PluginType_Transform,
    PluginType_RenderAudio,
    PluginType_RenderVideo,
    PluginType_FileWriter,
    PluginType_Muxer,
    PluginType_TranscodeProfile
};

enum PluginParameter_Type
{
    ParameterType_None = 0,
    ParameterType_Stream,
    ParameterType_Video,
    ParameterType_Audio,
    ParameterType_RenderVideo
};

enum PluginParameter_SubType
{
    ParameterSubType_None = 0,
    ParameterSubType_StreamMPEG2,
    ParameterSubType_StreamMP4,
    ParameterSubType_VideoMPEG2,
    ParameterSubType_VideoH264,
    ParameterSubType_VideoVC1,
    ParameterSubType_AudioPCM,
    ParameterSubType_AudioAAC,
    ParameterSubType_AudioAC3,
    ParameterSubType_AudioMP3,
    ParameterSubType_AudioWMA,
    ParameterSubType_Transcode,
};

class PluginParameter
{
public:
    PluginParameter_Type    m_Type;
    PluginParameter_SubType m_SubType;

    TCHAR           m_Stream_Name[1024];

    unsigned int    m_Audio_SampleRate;
    unsigned int    m_Audio_Channels;
    unsigned int    m_Audio_BlockAlignment;
    unsigned int    m_Audio_BytesPerSecond;
    unsigned int    m_Audio_BitsPerSample;
    unsigned int    m_Audio_Compressed;

    unsigned int    m_Video_BitRate;
    unsigned int    m_Video_FrameRateNumerator;
    unsigned int    m_Video_FrameRateDenominator;
    unsigned int    m_Video_FrameWidth;
    unsigned int    m_Video_FrameHeight;
    unsigned int    m_Video_InterlaceMode;
    unsigned int    m_Video_PixelAspectRatioX;
    unsigned int    m_Video_PixelAspectRatioY;

    unsigned int    m_Render_Window_X;
    unsigned int    m_Render_Window_Y;
    unsigned int    m_Render_Window_Width;
    unsigned int    m_Render_Window_Height;
    HWND            m_Render_Window_Parent;

    PluginParameter() { memset(this, 0, sizeof(PluginParameter)); }
};

class Array
{
    void**  m_Array;
    int     m_Count;
    int     m_Allocated;

public:
     Array();
    ~Array();

    HRESULT Add(void* a_Item);
    HRESULT Del(int   a_Index);
    void*   Get(int   a_Index);
private:
    DISALLOW_COPY_AND_ASSIGN(Array);
};