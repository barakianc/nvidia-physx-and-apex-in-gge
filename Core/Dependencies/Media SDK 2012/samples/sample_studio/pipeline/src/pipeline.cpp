/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "pipeline.h"

BasePlugin::~BasePlugin()
{
    PluginParameter*    l_Parameter;

    do
    {
        l_Parameter = (PluginParameter*) m_Parameters.Get(0);

        delete l_Parameter;

        m_Parameters.Del(0);
    }
    while (l_Parameter != NULL);
}

HRESULT BasePlugin::GetPluginParameter(PluginParameter* a_Parameter, int a_Index)
{
    PluginParameter*    l_Parameter;
    int                 l_Index;

    for (l_Index = 0; ; l_Index++)
    {
        l_Parameter = (PluginParameter*) m_Parameters.Get(l_Index);

        if (l_Parameter == NULL)
            return E_FAIL;

        if (l_Parameter->m_Type != a_Parameter->m_Type)
            continue;

        if (a_Index == 0)
            break;

        a_Index --;
    }

    *a_Parameter = *l_Parameter;

    return S_OK;
}

HRESULT BasePlugin::SetPluginParameter(PluginParameter* a_Parameter, int a_Index)
{
    PluginParameter*    l_Parameter;
    int                 l_Index;

    for (l_Index = 0; ; l_Index++)
    {
        l_Parameter = (PluginParameter*) m_Parameters.Get(l_Index);

        if (l_Parameter == NULL)
        {
            l_Parameter = new PluginParameter;

            if (l_Parameter == NULL)
                return E_FAIL;

            m_Parameters.Add(l_Parameter);
            break;
        }

        if (l_Parameter->m_Type != a_Parameter->m_Type)
            continue;

        if (a_Index == 0)
            break;

        a_Index --;
    }

    *l_Parameter = *a_Parameter;

    return S_OK;
}
