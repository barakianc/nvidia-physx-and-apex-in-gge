/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include <tchar.h>
#include <atlbase.h>

#include "ds_pipeline.h"
#include "ds_guids.h"

DS_Plugin::DS_Plugin(PluginType a_Type)
{
    m_Type      = a_Type;
    m_Filter    = NULL;
    m_Window    = NULL;
}

DS_Plugin::~DS_Plugin()
{
    if (m_Window)
        DestroyWindow(m_Window);

    MSDK_SAFE_RELEASE(m_Filter);
}

HRESULT DS_Plugin::GetInterface(GUID a_GUID, void** a_Interface)
{
    HRESULT l_Result = E_NOINTERFACE;

    if (m_Filter != NULL)
        l_Result = m_Filter->QueryInterface(a_GUID, a_Interface);

    return l_Result;
}

HRESULT DS_Plugin::SetInputStream(PluginParameter* a_Parameter)
{
    HRESULT             l_Result = E_FAIL;
    IFileSourceFilter*  l_Source = NULL;

    if (m_Filter != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = m_Filter->QueryInterface(IID_IFileSourceFilter, (void**)&l_Source);

    if (SUCCEEDED(l_Result))
        l_Result = l_Source->Load((LPCOLESTR) a_Parameter->m_Stream_Name, NULL);

    MSDK_SAFE_RELEASE(l_Source);

    return l_Result;
}

HRESULT DS_Plugin::SetOutputStream(PluginParameter* a_Parameter)
{
    HRESULT             l_Result = E_FAIL;
    IFileSinkFilter*    l_Sink   = NULL;

    if (m_Filter != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = m_Filter->QueryInterface(IID_IFileSinkFilter, (void**)&l_Sink);

    if (SUCCEEDED(l_Result))
        l_Result = l_Sink->SetFileName((LPCOLESTR) a_Parameter->m_Stream_Name, NULL);

    MSDK_SAFE_RELEASE(l_Sink);

    return l_Result;
}

HRESULT DS_Plugin::SetVideoRenderWindow(PluginParameter* a_Parameter)
{
    HRESULT                 l_Result;
    IVideoWindow*           l_VideoWindow           = NULL;
    IMFGetService*          l_GetService            = NULL;
    IMFVideoDisplayControl* l_VideoDisplayControl   = NULL;
    RECT                    l_Rect                  = {0,};
    WNDCLASS                l_WndClass              = {0,};

    if (m_Type != PluginType_RenderVideo || a_Parameter->m_Type != ParameterType_RenderVideo)
        return E_FAIL;

    if (m_Window == NULL)
    {
        l_WndClass.lpfnWndProc      = ::DefWindowProc;
        l_WndClass.lpszClassName    = _T("VideoStudioWindow");
        
        RegisterClass(&l_WndClass);

        m_Window = CreateWindowEx(
            0,
            _T("VideoStudioWindow"),                // name of window class
            _T("Video Studio"),                     // title-bar string
            a_Parameter->m_Render_Window_Parent ? WS_CHILD : WS_OVERLAPPEDWINDOW,
            a_Parameter->m_Render_Window_X,         // default horizontal position
            a_Parameter->m_Render_Window_Y,         // default vertical position
            a_Parameter->m_Render_Window_Width,     // default width
            a_Parameter->m_Render_Window_Height,    // default height
            a_Parameter->m_Render_Window_Parent,    // parent window
            (HMENU) NULL,                           // use class menu
            0,
            0);

        if (m_Window)
            ShowWindow(m_Window, SW_SHOW);
    }

    if (m_Window == NULL)
        return E_FAIL;

    l_Rect.right    = a_Parameter->m_Render_Window_Width;
    l_Rect.bottom   = a_Parameter->m_Render_Window_Height;

    SetWindowPos(m_Window, 0, 0, 0, l_Rect.right, l_Rect.bottom, SWP_NOMOVE | SWP_NOZORDER);

    // Check for Video Renderer
    l_Result = m_Filter->QueryInterface(IID_IVideoWindow, (void**)&l_VideoWindow);

    if (SUCCEEDED(l_Result))
    {
        l_Result = l_VideoWindow->put_Owner((OAHWND)(m_Window));
        l_Result = l_VideoWindow->put_WindowStyle(WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);

        if (SUCCEEDED(l_Result))
            l_Result = l_VideoWindow->SetWindowPosition(0, 0, l_Rect.right, l_Rect.bottom);

        MSDK_SAFE_RELEASE(l_VideoWindow);

        return l_Result;
    }

    // Check for Enhanced Video Renderer
    l_Result = m_Filter->QueryInterface(__uuidof(IMFGetService), (void**)&l_GetService);

    if (SUCCEEDED(l_Result))
    {
        l_Result = l_GetService->GetService(MR_VIDEO_RENDER_SERVICE, __uuidof(IMFVideoDisplayControl), (void**)&l_VideoDisplayControl);

        if (SUCCEEDED(l_Result))
            l_Result = l_VideoDisplayControl->SetVideoWindow(m_Window);

        if (SUCCEEDED(l_Result))
            l_Result = l_VideoDisplayControl->SetVideoPosition(NULL, &l_Rect);

        MSDK_SAFE_RELEASE(l_GetService);
        MSDK_SAFE_RELEASE(l_VideoDisplayControl);

        return l_Result;
    }

    return l_Result;
}

HRESULT DS_Plugin::SetPluginParameter(PluginParameter* a_Parameter, int a_Index)
{
    HRESULT l_Result = E_FAIL;

    if (a_Parameter->m_Type == ParameterType_Stream)
    {
        if (m_Type == PluginType_Source)
            l_Result = SetInputStream(a_Parameter);

        if (m_Type == PluginType_FileWriter || m_Type == PluginType_Muxer)
            l_Result = SetOutputStream(a_Parameter);
    }

    if (a_Parameter->m_Type == ParameterType_RenderVideo)
    {
        if (m_Type == PluginType_RenderVideo)
            SetVideoRenderWindow(a_Parameter);
    }

    if (a_Parameter->m_Type == ParameterType_Video)
    {        
        if (m_Type == PluginType_Transform)
        {
            CComPtr<IConfigureVideoEncoder> pConfigureVideoEncoder = NULL;            
            l_Result = GetInterface(IID_IConfigureVideoEncoder, reinterpret_cast<void **>(&pConfigureVideoEncoder));
            if (SUCCEEDED(l_Result))
            {
                IConfigureVideoEncoder::Params params;
                pConfigureVideoEncoder->GetParams(&params);
                params.rc_control.bitrate = a_Parameter->m_Video_BitRate / 1000;
                params.frame_control.width = a_Parameter->m_Video_FrameWidth;
                params.frame_control.height = a_Parameter->m_Video_FrameHeight;
                pConfigureVideoEncoder->SetParams(&params);
            }
        }
    }

    l_Result = BasePlugin::SetPluginParameter(a_Parameter, a_Index);

    return l_Result;
}

DS_Pipeline::DS_Pipeline()
{
    m_Graph         = NULL;
    m_MediaControl  = NULL;
    m_MediaSeeking  = NULL;
    m_MediaEvent    = NULL;

    CoInitialize(NULL);
}

DS_Pipeline::~DS_Pipeline()
{
    Close();

    CoUninitialize();
}

HRESULT DS_Pipeline::Init()
{
    HRESULT l_Result = S_OK;

    Close();

    if (SUCCEEDED(l_Result))
        l_Result = CoCreateInstance(
            CLSID_FilterGraph, 
            NULL, 
            CLSCTX_INPROC_SERVER,
            IID_IGraphBuilder,
            (void**)&m_Graph);

    if (SUCCEEDED(l_Result))
        l_Result = m_Graph->QueryInterface(IID_IMediaControl, (void**)&m_MediaControl);

    if (SUCCEEDED(l_Result))
        l_Result = m_Graph->QueryInterface(IID_IMediaSeeking, (void**)&m_MediaSeeking);

    if (SUCCEEDED(l_Result))
        l_Result = m_Graph->QueryInterface(IID_IMediaEventEx, (void**)&m_MediaEvent);

    if (FAILED(l_Result))
        Close();

    return l_Result;
}

HRESULT DS_Pipeline::Close()
{
    DS_Plugin*  l_Plugin;

    if (m_MediaControl)
        m_MediaControl->Stop();

    for (;;)
    {
        l_Plugin = (DS_Plugin*) m_Plugins.Get(0);

        if (l_Plugin == NULL)
            break;

        delete l_Plugin;

        m_Plugins.Del(0);
    }

    MSDK_SAFE_RELEASE(m_MediaEvent);
    MSDK_SAFE_RELEASE(m_MediaSeeking);
    MSDK_SAFE_RELEASE(m_MediaControl);
    MSDK_SAFE_RELEASE(m_Graph);

    return S_OK;
}

BasePlugin* DS_Pipeline::AddPlugin(PluginName a_PluginName)
{
    BasePlugin*  l_Plugin = NULL;

    switch (a_PluginName)
    {
        case Source_File:           l_Plugin = AddPlugin(PluginType_Source,      CLSID_AsyncReader);            break;
        case Source_Url:            l_Plugin = AddPlugin(PluginType_Source,      CLSID_SourceUrl);              break;

        case Splitter_MPEG2:        l_Plugin = AddPlugin(PluginType_Transform,   CLSID_MPEG2SplitterFilter);    break;
        case Splitter_MP4:          l_Plugin = AddPlugin(PluginType_Transform,   CLSID_MP4SplitterFilter);      break;

        case DecoderVideo_MPEG2:    l_Plugin = AddPlugin(PluginType_Transform,   CLSID_MPEG2DecFilter);         break;
        case DecoderVideo_H264:     l_Plugin = AddPlugin(PluginType_Transform,   CLSID_H264DecFilter);          break;
        case DecoderVideo_VC1:      l_Plugin = AddPlugin(PluginType_Transform,   CLSID_VC1DecFilter);           break;

        case EncoderVideo_MPEG2:    l_Plugin = AddPlugin(PluginType_Transform,   CLSID_MPEG2EncFilter);         break;
        case EncoderVideo_H264:     l_Plugin = AddPlugin(PluginType_Transform,   CLSID_H264EncFilter);          break;

        case DecoderAudio_AAC:      l_Plugin = AddPlugin(PluginType_Transform,   CLSID_AACDecFilter);           break;
        case DecoderAudio_AC3:      l_Plugin = AddPlugin(PluginType_Transform,   CLSID_AC3DecFilter);           break;
        case DecoderAudio_MP3:      l_Plugin = AddPlugin(PluginType_Transform,   CLSID_MP3DecFilter);           break;

        case EncoderAudio_AAC:      l_Plugin = AddPlugin(PluginType_Transform,   CLSID_AACEncFilter);           break;
        case EncoderAudio_MP3:      l_Plugin = AddPlugin(PluginType_Transform,   CLSID_MP3EncFilter);           break;

        case RenderAudio:           l_Plugin = AddPlugin(PluginType_RenderAudio, CLSID_AudioRender);            break;
        case RenderVideo:           l_Plugin = AddPlugin(PluginType_RenderVideo, CLSID_VideoRenderer);          break;
        case RenderVideoDefault:    l_Plugin = AddPlugin(PluginType_RenderVideo, CLSID_VideoRendererDefault);   break;
        case RenderVideoEnhanced:   l_Plugin = AddPlugin(PluginType_RenderVideo, CLSID_EnhancedVideoRenderer);  break;

        case Writer_MP4:            l_Plugin = AddPlugin(PluginType_Muxer,       CLSID_MP4MuxerFilter);         break;
        case Writer_File:           l_Plugin = AddPlugin(PluginType_FileWriter,  CLSID_FileWriter);             break;
    }

    return l_Plugin;
}

BasePlugin* DS_Pipeline::AddPlugin(PluginType a_PluginType, GUID a_GUID)
{
    HRESULT     l_Result = E_FAIL;

    DS_Plugin*  l_Plugin = new DS_Plugin(a_PluginType);

    if (l_Plugin != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = CoCreateInstance(
            a_GUID,
            NULL,
            CLSCTX_INPROC_SERVER,
            IID_IBaseFilter,
            (void**)&l_Plugin->m_Filter);


    if (SUCCEEDED(l_Result))
       l_Result = m_Graph->AddFilter(l_Plugin->m_Filter, L"");

    if (FAILED(l_Result))
    {
        delete l_Plugin;
        l_Plugin = NULL;
    }

    if (l_Plugin != NULL)
        m_Plugins.Add(l_Plugin);

    return l_Plugin;
}

HRESULT DS_Pipeline::DelPlugin(BasePlugin* a_Plugin)
{
    int         l_Index;
    DS_Plugin*  l_Plugin;
    DS_Plugin*  l_PluginTarget = dynamic_cast<DS_Plugin*> (a_Plugin);

    for (l_Index = 0; ; l_Index++)
    {
        l_Plugin = (DS_Plugin*) m_Plugins.Get(l_Index);

        if (l_Plugin == NULL)
            break;

        if (l_Plugin == l_PluginTarget)
        {
            m_Graph->RemoveFilter(l_Plugin->m_Filter);

            delete l_Plugin;
            m_Plugins.Del(l_Index);

            return S_OK;
        }
    }

    return E_FAIL;
}

HRESULT DS_Pipeline::Connect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst)
{
    HRESULT         l_Result    = S_OK;
    HRESULT         l_Connected = E_FAIL;
    IEnumPins*      l_EnumPins  = NULL;
    IPin*           l_Pin       = NULL;
    PIN_DIRECTION   l_Direction;

    DS_Plugin*  l_PluginSrc = dynamic_cast<DS_Plugin*> (a_PluginSrc);
    DS_Plugin*  l_PluginDst = dynamic_cast<DS_Plugin*> (a_PluginDst);

    if (l_PluginSrc == NULL || l_PluginDst == NULL)
        return E_FAIL;

    if (l_PluginSrc->m_Filter == NULL)
        return E_FAIL;

    if (SUCCEEDED(l_Result))
        l_Result = l_PluginSrc->m_Filter->EnumPins(&l_EnumPins);

    if (SUCCEEDED(l_Result))
    {
        while (FAILED(l_Connected) && S_OK == l_EnumPins->Next(1, &l_Pin, NULL))
        {
            l_Result = l_Pin->QueryDirection(&l_Direction);

            if (SUCCEEDED(l_Result) && l_Direction == PINDIR_OUTPUT)
                l_Connected = Connect_Pin(l_Pin, l_PluginDst);

            MSDK_SAFE_RELEASE(l_Pin);
        }

        MSDK_SAFE_RELEASE(l_EnumPins);
    }

    return l_Connected;
}

HRESULT DS_Pipeline::Connect_Pin(IPin* a_Pin, DS_Plugin* a_PluginDst)
{
    HRESULT             l_Result;
    HRESULT             l_Connected     = E_FAIL;
    IEnumPins*          l_EnumPins      = NULL;
    IPin*               l_Pin           = NULL;
    PIN_DIRECTION       l_Direction;
    PluginParameter     l_PluginParameter;
    IEnumMediaTypes*    l_EnumMediaTypes;
    AM_MEDIA_TYPE*      l_MediaType;
    WAVEFORMATEX*       l_InfoAudio;
    VIDEOINFOHEADER2*   l_InfoVideo;
    int                 l_CountAudio    = 0;
    int                 l_CountVideo    = 0;

    if (a_PluginDst->m_Filter == NULL)
        return E_FAIL;

    l_Result = a_PluginDst->m_Filter->EnumPins(&l_EnumPins);

    if (SUCCEEDED(l_Result))
    {
        while (FAILED(l_Connected) && S_OK == l_EnumPins->Next(1, &l_Pin, NULL))
        {
            l_Result = l_Pin->QueryDirection(&l_Direction);

            if (SUCCEEDED(l_Result) && l_Direction == PINDIR_INPUT)
                l_Connected = m_Graph->ConnectDirect(a_Pin, l_Pin, NULL);

            MSDK_SAFE_RELEASE(l_Pin);
        }

        MSDK_SAFE_RELEASE(l_EnumPins);
    }

    l_Result = l_Connected;

    if (SUCCEEDED(l_Result))
        l_Result = a_PluginDst->m_Filter->EnumPins(&l_EnumPins);

    if (SUCCEEDED(l_Result))
    {
        while (S_OK == l_EnumPins->Next(1, &l_Pin, NULL))
        {
            l_Result = l_Pin->QueryDirection(&l_Direction);

            if (SUCCEEDED(l_Result) && l_Direction == PINDIR_OUTPUT)
            {
                l_Result = l_Pin->EnumMediaTypes(&l_EnumMediaTypes);

                if (SUCCEEDED(l_Result))
                {
                    for (l_MediaType; l_EnumMediaTypes->Next(1, &l_MediaType, 0) == S_OK; CoTaskMemFree(l_MediaType))
                    {
                        if (l_MediaType->majortype == MEDIATYPE_Audio) 
                        {
                            l_PluginParameter.m_Type    = ParameterType_Audio;
                            l_PluginParameter.m_SubType = ParameterSubType_None;

                            if (l_MediaType->subtype == MEDIASUBTYPE_AAC)
                                l_PluginParameter.m_SubType = ParameterSubType_AudioAAC;

                            if (l_MediaType->subtype == MEDIASUBTYPE_DOLBY_AC3_SPDIF)
                                l_PluginParameter.m_SubType = ParameterSubType_AudioAC3;

                            if (l_MediaType->subtype == MEDIASUBTYPE_DOLBY_AC3)
                                l_PluginParameter.m_SubType = ParameterSubType_AudioAC3;

                            if (l_MediaType->subtype == MEDIASUBTYPE_MPEG1Payload)
                                l_PluginParameter.m_SubType = ParameterSubType_AudioMP3;

                            if (l_MediaType->formattype == FORMAT_WaveFormatEx)
                            {
                                l_InfoAudio = (WAVEFORMATEX*) l_MediaType->pbFormat;

                                l_PluginParameter.m_Audio_SampleRate        = l_InfoAudio->nSamplesPerSec;
                                l_PluginParameter.m_Audio_Channels          = l_InfoAudio->nChannels;
                                l_PluginParameter.m_Audio_BlockAlignment    = l_InfoAudio->nBlockAlign;
                                l_PluginParameter.m_Audio_BytesPerSecond    = l_InfoAudio->nAvgBytesPerSec;
                                l_PluginParameter.m_Audio_BitsPerSample     = l_InfoAudio->wBitsPerSample;
                                l_PluginParameter.m_Audio_Compressed        = TRUE;
                            }

                            l_Result = a_PluginDst->SetPluginParameter(&l_PluginParameter, l_CountAudio);

                            if (SUCCEEDED(l_Result))
                                l_CountAudio ++;
                        }

                        if (l_MediaType->majortype == MEDIATYPE_Video)
                        {
                            l_PluginParameter.m_Type    = ParameterType_Video;
                            l_PluginParameter.m_SubType = ParameterSubType_None;

                            if (l_MediaType->subtype == MEDIASUBTYPE_MPEG2_VIDEO)
                                l_PluginParameter.m_SubType = ParameterSubType_VideoMPEG2;

                            if (l_MediaType->subtype == MEDIASUBTYPE_H264)
                                l_PluginParameter.m_SubType = ParameterSubType_VideoH264;

                            if (l_MediaType->formattype == FORMAT_VideoInfo2)
                            {
                                l_InfoVideo = (VIDEOINFOHEADER2*) l_MediaType->pbFormat;

                                l_PluginParameter.m_Video_BitRate               = l_InfoVideo->dwBitRate;
                                l_PluginParameter.m_Video_FrameWidth            = l_InfoVideo->bmiHeader.biWidth;
                                l_PluginParameter.m_Video_FrameHeight           = l_InfoVideo->bmiHeader.biHeight;
                                l_PluginParameter.m_Video_InterlaceMode         = l_InfoVideo->dwInterlaceFlags;
                                l_PluginParameter.m_Video_PixelAspectRatioX     = l_InfoVideo->dwPictAspectRatioX;
                                l_PluginParameter.m_Video_PixelAspectRatioY     = l_InfoVideo->dwPictAspectRatioY;

                                switch(l_InfoVideo->AvgTimePerFrame)
                                {
                                    case 166833: // 59.94 (NTSC)
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 60000;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = 1001;
                                        break;

                                    case 333667: // 29.97 (NTSC)
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 30000;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = 1001;
                                        break;

                                    case 417188: // 23.97 (NTSC)
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 24000;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = 1001;
                                        break;

                                    case 200000: // 50.00 (PAL)
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 50;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = 1;
                                        break;

                                    case 400000: // 25.00 (PAL)
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 25;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = 1;
                                        break;

                                    case 416667: // 24.00 (Film)
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 24;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = 1;
                                        break;

                                    default:
                                        l_PluginParameter.m_Video_FrameRateNumerator    = 10000;
                                        l_PluginParameter.m_Video_FrameRateDenominator  = (unsigned int) (l_InfoVideo->AvgTimePerFrame / 1000);
                                        break;
                                }
                            }

                            l_Result = a_PluginDst->SetPluginParameter(&l_PluginParameter, l_CountAudio);

                            if (SUCCEEDED(l_Result))
                                l_CountVideo ++;
                        }
                    }

                    l_EnumMediaTypes->Release();
                }
            }

            MSDK_SAFE_RELEASE(l_Pin);
        }

        MSDK_SAFE_RELEASE(l_EnumPins);
    }

    return l_Result;
}

HRESULT DS_Pipeline::Disconnect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst)
{
    return E_NOTIMPL;
}

HRESULT DS_Pipeline::Play()
{
    HRESULT l_Result = E_FAIL;

    if (m_MediaControl)
        l_Result = m_MediaControl->Run();

    return l_Result;
}

HRESULT DS_Pipeline::Pause()
{
    HRESULT l_Result = E_FAIL;

    if (m_MediaControl)
        l_Result = m_MediaControl->Pause();

    return l_Result;
}


HRESULT DS_Pipeline::Stop()
{
    HRESULT l_Result = E_FAIL;

    if (m_MediaControl)
        l_Result = m_MediaControl->Stop();

    return l_Result;
}

HRESULT DS_Pipeline::Seek(double a_AbsolutePosition, double a_Rate)
{
    HRESULT     l_Result   = E_FAIL;
    LONGLONG    l_Position = (LONGLONG)(a_AbsolutePosition * 1e7);

    if (m_MediaSeeking)
        l_Result = m_MediaSeeking->SetTimeFormat(&TIME_FORMAT_MEDIA_TIME);

    if (SUCCEEDED(l_Result))
        l_Result = m_MediaSeeking->SetPositions(&l_Position, AM_SEEKING_AbsolutePositioning, NULL, AM_SEEKING_NoPositioning);

    return l_Result;
}

HRESULT DS_Pipeline::Move(double a_RelativePosition, double a_Rate)
{
    HRESULT     l_Result   = E_FAIL;
    LONGLONG    l_Position = (LONGLONG)(a_RelativePosition * 1e7);

    if (m_MediaSeeking)
        l_Result = m_MediaSeeking->SetTimeFormat(&TIME_FORMAT_MEDIA_TIME);

    if (SUCCEEDED(l_Result))
        l_Result = m_MediaSeeking->SetPositions(&l_Position, AM_SEEKING_RelativePositioning, NULL, AM_SEEKING_NoPositioning);

    return l_Result;
}

HRESULT DS_Pipeline::SetRate(double a_Rate)
{
    return E_NOTIMPL;
}

HRESULT DS_Pipeline::GetDuration(double* a_Duration)
{
    return E_NOTIMPL;
}

HRESULT DS_Pipeline::GetPosition(double* a_Position)
{
    return E_NOTIMPL;
}

DWORD DS_Pipeline::Wait(DWORD a_Milliseconds)
{
    HRESULT l_Result;
    long    l_Code = 0;

    if (m_MediaEvent == NULL)
        return WAIT_ABANDONED;

    if (a_Milliseconds < 0)
        a_Milliseconds = INFINITE;

    l_Result = m_MediaEvent->WaitForCompletion(a_Milliseconds, &l_Code);

    if (l_Result == E_ABORT)
        return WAIT_TIMEOUT;

    if (l_Result == VFW_E_WRONG_STATE)
        return WAIT_ABANDONED;

    return WAIT_OBJECT_0;
}

HRESULT DS_Pipeline::GetInterface(GUID a_GUID, void** a_Interface)
{
    return E_NOINTERFACE;
}
