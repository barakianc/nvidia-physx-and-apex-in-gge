/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "pipeline.h"

Array::Array()
{
    m_Array     = NULL;
    m_Count     = 0;
    m_Allocated = 0;
}

Array::~Array()
{
    free(m_Array);
    
    m_Array = NULL;
}

HRESULT Array::Add(void* a_Item)
{
    if (m_Allocated == m_Count)
    {
        size_t l_OldSize = m_Allocated * sizeof (void*);
        m_Allocated += 10;
        size_t l_NewSize = m_Allocated * sizeof (void*);
        
        void** l_NewPtr = (void**)malloc(l_NewSize);
        if (NULL != l_NewPtr)
        {
            memset(l_NewPtr, 0, l_NewSize);
            memcpy(l_NewPtr, m_Array, l_OldSize);
        }
        memset(m_Array, 0, l_OldSize);
        free(m_Array);
        m_Array = l_NewPtr;
    }

    if (m_Array == NULL)
    {
        m_Allocated = 0;
        m_Count     = 0;

        return E_FAIL;
    }

    m_Array[m_Count++] = a_Item;

    return S_OK;
}

HRESULT Array::Del(int a_Index)
{
    if (a_Index < m_Count && a_Index >= 0)
    {
        m_Array[a_Index] = m_Array[--m_Count];

        return S_OK;
    }

    return E_FAIL;
}

void* Array::Get(int a_Index)
{
    if (m_Count > a_Index && a_Index >= 0)
        return m_Array[a_Index];

    return NULL;
}
