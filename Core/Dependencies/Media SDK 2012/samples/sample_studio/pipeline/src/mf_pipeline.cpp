/********************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2009-2011 Intel Corporation. All Rights Reserved.

*********************************************************************************/

#include "mf_pipeline.h"
#include "mf_guids.h"
#include "uuids.h"
#include "wmcodecdsp.h"
#include "wmcontainer.h"

#include <tchar.h>

MF_Plugin::MF_Plugin(PluginType a_Type)
{
    m_RefCount              = 0;
    m_Type                  = a_Type;

    m_SourceResolver        = NULL;
    m_MediaSource           = NULL;
    m_ByteStream            = NULL;
    m_MediaSink             = NULL;
    m_MediaType             = NULL;
    m_Transform             = NULL;
    m_FinalizableMediaSink  = NULL;
    m_Window                = NULL;
    m_EventFinalized        = CreateEvent(NULL, FALSE, TRUE,  NULL);
}

MF_Plugin::~MF_Plugin()
{
    HRESULT             l_Result;
    int                 l_Index;
    IMFTopologyNode*    l_Node;

    for (l_Index = 0; ; l_Index++)
    {
        l_Node = (IMFTopologyNode*) m_Nodes.Get(l_Index);

        if (l_Node == NULL)
            break;

        MSDK_SAFE_RELEASE(l_Node);
    }

    if (m_MediaSource != NULL)
        m_MediaSource->Shutdown();

    if (m_MediaSink != NULL)
    {
        IMFFinalizableMediaSink*    l_FinalizableMediaSink = NULL;

        l_Result = m_MediaSink->QueryInterface(IID_IMFFinalizableMediaSink, (void**)&l_FinalizableMediaSink);

        if (SUCCEEDED(l_Result))
        {
            ResetEvent(m_EventFinalized);
            l_Result = l_FinalizableMediaSink->BeginFinalize(this, NULL);
        }

        if (SUCCEEDED(l_Result))
            WaitForSingleObject(m_EventFinalized, 10 * 1000);

        MSDK_SAFE_RELEASE(l_FinalizableMediaSink);
    }

    CloseHandle(m_EventFinalized);

    if (m_Window)
        DestroyWindow(m_Window);

    if (m_MediaSink != NULL)
        m_MediaSink->Shutdown();

    if (m_ByteStream != NULL)
        m_ByteStream->Close();

    MSDK_SAFE_RELEASE(m_Transform);
    MSDK_SAFE_RELEASE(m_MediaType);
    MSDK_SAFE_RELEASE(m_MediaSink);
    MSDK_SAFE_RELEASE(m_ByteStream);
    MSDK_SAFE_RELEASE(m_MediaSource);
    MSDK_SAFE_RELEASE(m_SourceResolver);
}

IMFTopologyNode* MF_Plugin::GetNode(int a_Index)
{
    return (IMFTopologyNode*) m_Nodes.Get(a_Index);
}

HRESULT MF_Plugin::AddNode(IMFTopologyNode* a_Node)
{
    return m_Nodes.Add(a_Node);
}

HRESULT MF_Plugin::GetInterface(GUID a_GUID, void** a_Interface)
{
    a_GUID;         // to avoid C4100 warning
    a_Interface;    // to avoid C4100 warning

    if (m_Transform)
        return m_Transform->QueryInterface(a_GUID, a_Interface);

    return E_NOINTERFACE;
}

HRESULT MF_Plugin::SetVideoRenderWindow(PluginParameter* a_Parameter)
{
    HRESULT                 l_Result    = S_OK;
    WNDCLASS                l_WndClass  = {0,};
    RECT                    l_Rect      = {0,};
    IMFVideoDisplayControl* l_VideoDisplayControl = NULL;

    if (m_Type != PluginType_RenderVideo || a_Parameter == NULL || a_Parameter->m_Type != ParameterType_RenderVideo)
        return E_FAIL;

    if (m_Window == NULL)
    {
        l_WndClass.lpfnWndProc      = ::DefWindowProc;
        l_WndClass.lpszClassName    = _T("VideoStudioWindow");
        
        RegisterClass(&l_WndClass);

        m_Window = CreateWindowEx(
            0,
            _T("VideoStudioWindow"),                // name of window class
            _T("Video Studio"),                     // title-bar string
            a_Parameter->m_Render_Window_Parent ? WS_CHILD : WS_OVERLAPPEDWINDOW,
            a_Parameter->m_Render_Window_X,         // default horizontal position
            a_Parameter->m_Render_Window_Y,         // default vertical position
            a_Parameter->m_Render_Window_Width,     // default width
            a_Parameter->m_Render_Window_Height,    // default height
            a_Parameter->m_Render_Window_Parent,    // parent window
            (HMENU) NULL,                           // use class menu
            0,
            0);

        if (m_Window)
            ShowWindow(m_Window, SW_SHOW);
    }

    if (m_Window == NULL)
        return E_FAIL;

    l_Rect.right    = a_Parameter->m_Render_Window_Width;
    l_Rect.bottom   = a_Parameter->m_Render_Window_Height;

    SetWindowPos(m_Window, 0, 0, 0, l_Rect.right, l_Rect.bottom, SWP_NOMOVE | SWP_NOZORDER);

    MFGetService(m_Session, MR_VIDEO_RENDER_SERVICE, __uuidof(IMFVideoDisplayControl), (void**)&l_VideoDisplayControl);

    if (l_VideoDisplayControl)
    {
        l_VideoDisplayControl->SetVideoPosition(NULL, &l_Rect);
        l_VideoDisplayControl->Release();
    }

    return l_Result;
}

HRESULT MF_Plugin::SetPluginParameter(PluginParameter* a_Parameter, int a_Index)
{
    HRESULT         l_Result = E_FAIL;
    IMFMediaType*   l_MediaType = NULL;

    if (a_Parameter->m_Type == ParameterType_Stream)
    {
        if (m_Type == PluginType_Source)
            l_Result = SetInputStream(a_Parameter);

        if (m_Type == PluginType_FileWriter || m_Type == PluginType_Muxer)
            l_Result = SetOutputStream(a_Parameter);

        if (m_Type == PluginType_TranscodeProfile)
            l_Result = S_OK;
    }

    if (a_Parameter->m_Type == ParameterType_Video ||
        a_Parameter->m_Type == ParameterType_Audio)
    {
        if (m_Type == PluginType_Transform && m_Transform != NULL)
        {
            l_Result = ConvertPluginParameterToMediaType(a_Parameter, &l_MediaType);

            if (SUCCEEDED(l_Result))
                l_Result = m_Transform->SetOutputType(0, l_MediaType, 0);

            MSDK_SAFE_RELEASE(l_MediaType);
        }

        if (m_Type == PluginType_Source && m_MediaSource != NULL)
            l_Result = S_OK;

        if (m_Type == PluginType_TranscodeProfile)
            l_Result = S_OK;
    }

    if (a_Parameter->m_Type == ParameterType_RenderVideo)
    {
        if (m_Type == PluginType_RenderVideo)
            l_Result = SetVideoRenderWindow(a_Parameter);
    }

    if (SUCCEEDED(l_Result))
        l_Result = BasePlugin::SetPluginParameter(a_Parameter, a_Index);

    return l_Result;
}

HRESULT MF_Plugin::QueryInterface(REFIID a_IID, void** a_Interface)
{
    a_IID;          // to avoid C4100 warning
    a_Interface;    // to avoid C4100 warning

    return E_NOINTERFACE;
}

ULONG MF_Plugin::AddRef(void)
{
    m_RefCount ++;

    return m_RefCount;
}

ULONG MF_Plugin::Release(void)
{
    m_RefCount --;

    return m_RefCount;
}

HRESULT MF_Plugin::GetParameters(DWORD* a_Flags, DWORD* a_Queue)
{
    *a_Flags = 0;
    *a_Queue = MFASYNC_CALLBACK_QUEUE_STANDARD;

    return S_OK;
}

HRESULT MF_Plugin::Invoke(IMFAsyncResult* a_Result)
{
    HRESULT l_Result = S_OK;

    if (m_FinalizableMediaSink != NULL)
    {
        l_Result = m_FinalizableMediaSink->EndFinalize(a_Result);

        m_FinalizableMediaSink->Release();

        SetEvent(m_EventFinalized);
    }

    return l_Result;
}

HRESULT MF_Plugin::ConvertMediaTypeToPluginParameter(IMFMediaType* a_MediaType, PluginParameter* a_Parameter)
{
    HRESULT l_Result = E_FAIL;
    GUID    l_MajorType;
    GUID    l_SubType;

    if (a_MediaType != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = a_MediaType->GetMajorType(&l_MajorType);

    if (FAILED(l_Result))
        return l_Result;

    if (l_MajorType == MFMediaType_Audio)
    {
        a_Parameter->m_Type = ParameterType_Audio;

        l_Result = a_MediaType->GetGUID(MF_MT_SUBTYPE, &l_SubType);

        if (l_SubType == MFAudioFormat_PCM )            a_Parameter->m_SubType = ParameterSubType_AudioPCM;
        if (l_SubType == MFAudioFormat_AAC )            a_Parameter->m_SubType = ParameterSubType_AudioAAC;
        if (l_SubType == MFAudioFormat_Dolby_AC3_SPDIF) a_Parameter->m_SubType = ParameterSubType_AudioAC3;
        if (l_SubType == MFAudioFormat_MPEG)            a_Parameter->m_SubType = ParameterSubType_AudioMP3;
        if (l_SubType == MFAudioFormat_MP3 )            a_Parameter->m_SubType = ParameterSubType_AudioMP3;
        if (l_SubType == MFAudioFormat_WMAudioV8 )      a_Parameter->m_SubType = ParameterSubType_AudioWMA;
        if (l_SubType == MFAudioFormat_WMAudioV9 )      a_Parameter->m_SubType = ParameterSubType_AudioWMA;

        l_Result = a_MediaType->GetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND,   &a_Parameter->m_Audio_SampleRate);
        l_Result = a_MediaType->GetUINT32(MF_MT_AUDIO_NUM_CHANNELS,         &a_Parameter->m_Audio_Channels);
        l_Result = a_MediaType->GetUINT32(MF_MT_AUDIO_BLOCK_ALIGNMENT,      &a_Parameter->m_Audio_BlockAlignment);
        l_Result = a_MediaType->GetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, &a_Parameter->m_Audio_BytesPerSecond);
        l_Result = a_MediaType->GetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE,      &a_Parameter->m_Audio_BitsPerSample);
        l_Result = a_MediaType->GetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE,      &a_Parameter->m_Audio_BitsPerSample);
        l_Result = a_MediaType->GetUINT32(MF_MT_COMPRESSED,                 &a_Parameter->m_Audio_Compressed);

        return S_OK;
    }

    if (l_MajorType == MFMediaType_Video)
    {
        a_Parameter->m_Type = ParameterType_Video;

        l_Result = a_MediaType->GetGUID(MF_MT_SUBTYPE, &l_SubType);

        if (l_SubType == MFVideoFormat_H264)    a_Parameter->m_SubType = ParameterSubType_VideoH264;
        if (l_SubType == MFVideoFormat_MPEG2)   a_Parameter->m_SubType = ParameterSubType_VideoMPEG2;
        if (l_SubType == MFVideoFormat_WMV1)    a_Parameter->m_SubType = ParameterSubType_VideoVC1;
        if (l_SubType == MFVideoFormat_WMV2)    a_Parameter->m_SubType = ParameterSubType_VideoVC1;
        if (l_SubType == MFVideoFormat_WMV3)    a_Parameter->m_SubType = ParameterSubType_VideoVC1;
        if (l_SubType == MFVideoFormat_WVC1)    a_Parameter->m_SubType = ParameterSubType_VideoVC1;

        l_Result = MFGetAttributeRatio(
            a_MediaType, MF_MT_FRAME_RATE, 
            &a_Parameter->m_Video_FrameRateNumerator,
            &a_Parameter->m_Video_FrameRateDenominator);

        l_Result = MFGetAttributeSize(
            a_MediaType, MF_MT_FRAME_SIZE,
            &a_Parameter->m_Video_FrameWidth,
            &a_Parameter->m_Video_FrameHeight);

        l_Result = MFGetAttributeRatio(
            a_MediaType, MF_MT_PIXEL_ASPECT_RATIO, 
            &a_Parameter->m_Video_PixelAspectRatioX,
            &a_Parameter->m_Video_PixelAspectRatioY);

        l_Result = a_MediaType->GetUINT32(MF_MT_INTERLACE_MODE, &a_Parameter->m_Video_InterlaceMode);

        return S_OK;
    }

    return E_FAIL;
}

HRESULT MF_Plugin::ConvertPluginParameterToMediaType(PluginParameter* a_Parameter, IMFMediaType** a_MediaType)
{
    HRESULT         l_Result;
    IMFMediaType*   l_MediaType = NULL;

    if (a_Parameter->m_Type == ParameterType_Audio)
    {
        l_Result = MFCreateMediaType(&l_MediaType);

        if (SUCCEEDED(l_Result))
        {
            l_Result = l_MediaType->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio);
            
            if (a_Parameter->m_SubType == ParameterSubType_AudioPCM)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM);

            if (a_Parameter->m_SubType == ParameterSubType_AudioAAC)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_AAC);

            if (a_Parameter->m_SubType == ParameterSubType_AudioAC3)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_Dolby_AC3_SPDIF);

            if (a_Parameter->m_SubType == ParameterSubType_AudioMP3)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_MP3);

            l_Result = l_MediaType->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND,   a_Parameter->m_Audio_SampleRate);
            l_Result = l_MediaType->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS,         a_Parameter->m_Audio_Channels);
            l_Result = l_MediaType->SetUINT32(MF_MT_AUDIO_BLOCK_ALIGNMENT,      a_Parameter->m_Audio_BlockAlignment);
            l_Result = l_MediaType->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, a_Parameter->m_Audio_BytesPerSecond);
            l_Result = l_MediaType->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE,      a_Parameter->m_Audio_BitsPerSample);
            l_Result = l_MediaType->SetUINT32(MF_MT_COMPRESSED,                 a_Parameter->m_Audio_Compressed);

            l_Result = l_MediaType->SetUINT32(MF_MT_AUDIO_PREFER_WAVEFORMATEX,  1);
            l_Result = l_MediaType->SetUINT32(MF_MT_FIXED_SIZE_SAMPLES,         0);

            *a_MediaType = l_MediaType;

            return S_OK;
        }
    }

    if (a_Parameter->m_Type == ParameterType_Video)
    {
        l_Result = MFCreateMediaType(&l_MediaType);

        if (SUCCEEDED(l_Result))
        {
            l_Result = l_MediaType->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);

            if (a_Parameter->m_SubType == ParameterSubType_VideoMPEG2)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_MPEG2);

            if (a_Parameter->m_SubType == ParameterSubType_VideoH264)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_H264);

            if (a_Parameter->m_SubType == ParameterSubType_VideoVC1)
                l_Result = l_MediaType->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_WVC1);

            l_Result = MFSetAttributeRatio(l_MediaType, MF_MT_FRAME_RATE,
                a_Parameter->m_Video_FrameRateNumerator,
                a_Parameter->m_Video_FrameRateDenominator);

            l_Result = MFSetAttributeSize(l_MediaType, MF_MT_FRAME_SIZE,
                a_Parameter->m_Video_FrameWidth,
                a_Parameter->m_Video_FrameHeight);

            l_Result = l_MediaType->SetUINT32(MF_MT_AVG_BITRATE, a_Parameter->m_Video_BitRate);
            l_Result = l_MediaType->SetUINT32(MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive);
            l_Result = l_MediaType->SetUINT32(MF_MT_ALL_SAMPLES_INDEPENDENT, TRUE);

            l_Result = MFSetAttributeRatio(l_MediaType, MF_MT_PIXEL_ASPECT_RATIO,
                a_Parameter->m_Video_PixelAspectRatioX,
                a_Parameter->m_Video_PixelAspectRatioY);

            *a_MediaType = l_MediaType;

            return S_OK;
        }
    }

    return E_FAIL;
}

MF_Pipeline::MF_Pipeline()
{
    m_RefCount                  = 0;
    m_Session                   = NULL;
    m_Topology                  = NULL;
    m_VideoDisplay              = NULL;
    m_RateSupport               = NULL;
    m_RateControl               = NULL;
    m_PresentationClock         = NULL;
    m_Duration                  = 0;
    m_EventSessionStarted       = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_EventSessionStopped       = CreateEvent(NULL, TRUE, TRUE,  NULL);
    m_EventSessionClosed        = CreateEvent(NULL, TRUE, TRUE,  NULL);
    m_EventTopologyReady        = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_EventSessionTopologySet   = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_EventError                = CreateEvent(NULL, TRUE, FALSE, NULL);

    MFStartup(MF_VERSION);
}

MF_Pipeline::~MF_Pipeline()
{
    Close();

    CloseHandle(m_EventSessionStarted);
    CloseHandle(m_EventSessionStopped);
    CloseHandle(m_EventSessionClosed);
    CloseHandle(m_EventTopologyReady);
    CloseHandle(m_EventSessionTopologySet);
    CloseHandle(m_EventError);

    MFShutdown();
}

HRESULT MF_Pipeline::Init()
{
    HRESULT l_Result = S_OK;

    Close();

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateMediaSession(NULL, &m_Session);

    if (SUCCEEDED(l_Result))
    {
        ResetEvent(m_EventSessionClosed);
        m_Session->BeginGetEvent(this, NULL);
    }

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateTopology(&m_Topology);

    if (FAILED(l_Result))
        Close();

    return l_Result;
}

HRESULT MF_Pipeline::Close()
{
    HRESULT     l_Result = S_OK;
    MF_Plugin*  l_Plugin;

    if (m_Session != NULL)
    {
        l_Result = m_Session->Close();

        if (SUCCEEDED(l_Result))
            WaitForSingleObject(m_EventSessionClosed, 1000);
    }

    if (m_Session != NULL)
        m_Session->Shutdown();

    for (;;)
    {
        l_Plugin = (MF_Plugin*) m_Plugins.Get(0);

        if (l_Plugin == NULL)
            break;

        delete l_Plugin;

        m_Plugins.Del(0);
    }

    MSDK_SAFE_RELEASE(m_PresentationClock);
    MSDK_SAFE_RELEASE(m_RateControl);
    MSDK_SAFE_RELEASE(m_RateSupport);
    MSDK_SAFE_RELEASE(m_VideoDisplay);
    MSDK_SAFE_RELEASE(m_Topology);
    MSDK_SAFE_RELEASE(m_Session);

    return S_OK;
}

BasePlugin* MF_Pipeline::AddPlugin(PluginName a_PluginName)
{
    BasePlugin*             l_Plugin = NULL;

    switch (a_PluginName)
    {
        case Source_File:           l_Plugin = AddPlugin(PluginType_Source);  break;
        case Source_Url:            l_Plugin = AddPlugin(PluginType_Source);  break;

        case DecoderVideo_MPEG2:    l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Decoder_MPEG2); break;
        case DecoderVideo_H264:     l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Decoder_H264);  break;
        case DecoderVideo_VC1:      l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Decoder_VC1);   break;

        case EncoderVideo_MPEG2:    l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Encoder_MPEG2); break;
        case EncoderVideo_H264:     l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Encoder_H264);  break;

        case DecoderAudio_AAC:      l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Decoder_AAC);   break;
        case DecoderAudio_MP3:      l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Decoder_MP3);   break;
      //case DecoderAudio_WMA:      l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Decoder_WMA);   break;
        
        case EncoderAudio_AAC:      l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Encoder_AAC);   break;
      //case EncoderAudio_WMA:      l_Plugin = AddPlugin(PluginType_Transform, CLSID_MF_Encoder_WMA);   break;

        case RenderAudio:           l_Plugin = AddPlugin(PluginType_RenderAudio);   break;
        case RenderVideo:           l_Plugin = AddPlugin(PluginType_RenderVideo);   break;
        case RenderVideoDefault:    l_Plugin = AddPlugin(PluginType_RenderVideo);   break;
        case RenderVideoEnhanced:   l_Plugin = AddPlugin(PluginType_RenderVideo);   break;
        
        case Writer_MP4:            l_Plugin = AddPlugin(PluginType_Muxer);         break;
    }

    return l_Plugin;
}

BasePlugin* MF_Pipeline::AddPlugin(PluginType a_PluginType, GUID a_GUID)
{
    MF_Plugin*  l_Plugin = NULL;

    if (a_PluginType == PluginType_Source       ||
        a_PluginType == PluginType_RenderVideo  ||
        a_PluginType == PluginType_Muxer        ||
        a_PluginType == PluginType_TranscodeProfile)
        l_Plugin = AddPlugin_Simple(a_PluginType);

    if (a_PluginType == PluginType_Transform)
        l_Plugin = AddPlugin_Transform(a_GUID);

    if (a_PluginType == PluginType_RenderAudio)
        l_Plugin = AddPlugin_RenderAudio();

    if (a_PluginType == PluginType_FileWriter)
        l_Plugin = AddPlugin_FileWriter();

    if (l_Plugin != NULL)
        m_Plugins.Add(l_Plugin);

    return (BasePlugin*) l_Plugin;
}

HRESULT MF_Pipeline::DelPlugin(BasePlugin* a_Plugin)
{
    int         l_Index;
    MF_Plugin*  l_Plugin;
    MF_Plugin*  l_PluginTarget = dynamic_cast<MF_Plugin*> (a_Plugin);

    for (l_Index = 0; ; l_Index++)
    {
        l_Plugin = (MF_Plugin*) m_Plugins.Get(l_Index);

        if (l_Plugin == NULL)
            break;

        if (l_Plugin == l_PluginTarget)
        {
            delete l_Plugin;
            m_Plugins.Del(l_Index);

            return S_OK;
        }
    }

    return E_FAIL;
}

HRESULT MF_Pipeline::Connect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst)
{
    HRESULT             l_Result = E_FAIL;

    int                 l_IndexSrc;
    int                 l_IndexDst;

    MF_Plugin*          l_PluginSrc = dynamic_cast<MF_Plugin*> (a_PluginSrc);
    MF_Plugin*          l_PluginDst = dynamic_cast<MF_Plugin*> (a_PluginDst);

    IMFTopologyNode*    l_NodeSrc;
    IMFTopologyNode*    l_NodeDst;
    IMFTopologyNode*    l_NodeOut = NULL;

    GUID                l_MajorTypeSrc;
    GUID                l_MajorTypeDst;

    if (l_PluginSrc == NULL || l_PluginDst == NULL)
        return E_FAIL;

    if (l_PluginDst->m_Type == PluginType_Muxer ||
        l_PluginDst->m_Type == PluginType_RenderVideo)
        return Connect_Delayed(l_PluginSrc, l_PluginDst);

    for(l_IndexSrc = 0; FAILED(l_Result); l_IndexSrc++)
    {
        l_NodeSrc = l_PluginSrc->GetNode(l_IndexSrc);

        if (l_NodeSrc == NULL)
            break;

        for(l_IndexDst = 0; FAILED(l_Result); l_IndexDst++)
        {
            l_NodeDst = l_PluginDst->GetNode(l_IndexDst);

            if (l_NodeDst == NULL)
                break;

            l_MajorTypeSrc = GUID_NULL;
            l_MajorTypeDst = GUID_NULL;

            l_NodeSrc->GetGUID(CLSID_MF_NodeMajorType, &l_MajorTypeSrc);
            l_NodeDst->GetGUID(CLSID_MF_NodeMajorType, &l_MajorTypeDst);

            if (l_MajorTypeSrc == l_MajorTypeDst || l_MajorTypeDst == MFMediaType_Binary)
            {
                l_Result = l_NodeSrc->GetOutput(0, &l_NodeOut, 0);

                if (l_NodeOut)
                {
                    MSDK_SAFE_RELEASE(l_NodeOut);
                    l_Result = E_FAIL;
                }
                else
                    l_Result = l_NodeSrc->ConnectOutput(0, l_NodeDst, 0);
            }
        }
    }

    return l_Result;
}

HRESULT MF_Pipeline::Disconnect(BasePlugin* a_PluginSrc, BasePlugin* a_PluginDst)
{
    a_PluginSrc;        // to avoid C4100 warning
    a_PluginDst;        // to avoid C4100 warning

    return E_NOTIMPL;
}

HRESULT MF_Pipeline::Resolve()
{
    HRESULT     l_Result;
    DWORD       l_Ret;
    MF_Plugin*  l_Plugin;
    int         l_Index;

    l_Ret = WaitForSingleObject(m_EventTopologyReady, 0);

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return E_FAIL;

    if (l_Ret == WAIT_OBJECT_0)
        return S_OK;

    for (l_Index = 0; ; l_Index++)
    {
        l_Plugin = (MF_Plugin*) m_Plugins.Get(l_Index);

        if (l_Plugin == NULL)
            break;

        if (l_Plugin->m_Type == PluginType_TranscodeProfile)
            return ResolveTranscodeTopology(l_Plugin);

        l_Plugin->ConnectFinalize();
    }

    l_Ret    = WAIT_FAILED;
    l_Result = m_Session->SetTopology(0, m_Topology);

    if (SUCCEEDED(l_Result))
        l_Ret = WaitForSingleObject(m_EventTopologyReady, 10 * 1000);

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return E_FAIL;

    if (l_Ret == WAIT_OBJECT_0)
        return S_OK;

    return E_FAIL;
}

HRESULT MF_Pipeline::ResolveTranscodeTopology(MF_Plugin* a_TranscodeProfile)
{
    HRESULT     l_Result = S_OK;
    DWORD       l_Ret;
    MF_Plugin*  l_Plugin;
    int         l_Index;

    PluginParameter         l_ParameterAudio;
    PluginParameter         l_ParameterVideo;
    PluginParameter         l_ParameterStream;

    IMFTranscodeProfile*    l_TranscodeProfile    = NULL;
    IMFAttributes*          l_AttributesContainer = NULL;
    IMFAttributes*          l_AttributesAudio     = NULL;
    IMFAttributes*          l_AttributesVideo     = NULL;

    IMFMediaSource*         l_MediaSource         = NULL;

    l_Ret = WaitForSingleObject(m_EventTopologyReady, 0);

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return E_FAIL;

    if (l_Ret == WAIT_OBJECT_0)
        return S_OK;

    l_ParameterAudio .m_Type   = ParameterType_Audio;
    l_ParameterVideo .m_Type   = ParameterType_Video;
    l_ParameterStream.m_Type   = ParameterType_Stream;

    if (SUCCEEDED(l_Result))
        l_Result = a_TranscodeProfile->GetPluginParameter(&l_ParameterAudio);

    if (SUCCEEDED(l_Result))
        l_Result = a_TranscodeProfile->GetPluginParameter(&l_ParameterVideo);

    if (SUCCEEDED(l_Result))
        l_Result = a_TranscodeProfile->GetPluginParameter(&l_ParameterStream);

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateAttributes(&l_AttributesContainer, 1);

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateAttributes(&l_AttributesAudio, 0);

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateAttributes(&l_AttributesVideo, 0);

    if (SUCCEEDED(l_Result))
        l_Result = l_AttributesContainer->SetGUID(
            MF_TRANSCODE_CONTAINERTYPE,
            MFTranscodeContainerType_MPEG4);

    if (SUCCEEDED(l_Result))
        l_Result = l_AttributesContainer->SetUINT32(
            MF_TRANSCODE_TOPOLOGYMODE,
            1);

    if (SUCCEEDED(l_Result))
    {
        l_Result = l_AttributesAudio->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio);

        if (l_ParameterAudio.m_SubType == ParameterSubType_AudioAAC)
            l_Result = l_AttributesAudio->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_AAC);

        if (l_ParameterAudio.m_SubType == ParameterSubType_AudioAC3)
            l_Result = l_AttributesAudio->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_Dolby_AC3_SPDIF);

        if (l_ParameterAudio.m_SubType == ParameterSubType_AudioMP3)
            l_Result = l_AttributesAudio->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_MP3);

        l_Result = l_AttributesAudio->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND,   l_ParameterAudio.m_Audio_SampleRate);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS,         l_ParameterAudio.m_Audio_Channels);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_AUDIO_BLOCK_ALIGNMENT,      l_ParameterAudio.m_Audio_BlockAlignment);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, l_ParameterAudio.m_Audio_BytesPerSecond);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE,      l_ParameterAudio.m_Audio_BitsPerSample);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_COMPRESSED,                 l_ParameterAudio.m_Audio_Compressed);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_AUDIO_PREFER_WAVEFORMATEX,  1);
        l_Result = l_AttributesAudio->SetUINT32(MF_MT_FIXED_SIZE_SAMPLES,         0);
    }

    if (SUCCEEDED(l_Result))
    {
        l_Result = l_AttributesVideo->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);

        if (l_ParameterVideo.m_SubType == ParameterSubType_VideoMPEG2)
            l_Result = l_AttributesVideo->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_MPEG2);

        if (l_ParameterVideo.m_SubType == ParameterSubType_VideoH264)
            l_Result = l_AttributesVideo->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_H264);

        if (l_ParameterVideo.m_SubType == ParameterSubType_VideoVC1)
            l_Result = l_AttributesVideo->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_WVC1);

        l_Result = MFSetAttributeRatio(l_AttributesVideo, MF_MT_FRAME_RATE,
            l_ParameterVideo.m_Video_FrameRateNumerator,
            l_ParameterVideo.m_Video_FrameRateDenominator);
        
        l_Result = MFSetAttributeSize(l_AttributesVideo, MF_MT_FRAME_SIZE,
            l_ParameterVideo.m_Video_FrameWidth,
            l_ParameterVideo.m_Video_FrameHeight);
        
        l_Result = l_AttributesVideo->SetUINT32(MF_MT_AVG_BITRATE, l_ParameterVideo.m_Video_BitRate);
        l_Result = l_AttributesVideo->SetUINT32(MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive);
        l_Result = l_AttributesVideo->SetUINT32(MF_MT_ALL_SAMPLES_INDEPENDENT, TRUE);
        
        l_Result = MFSetAttributeRatio(l_AttributesVideo, MF_MT_PIXEL_ASPECT_RATIO,
            l_ParameterVideo.m_Video_PixelAspectRatioX,
            l_ParameterVideo.m_Video_PixelAspectRatioY);
    }

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateTranscodeProfile(&l_TranscodeProfile);

    if (SUCCEEDED(l_Result))
        l_Result = l_TranscodeProfile->SetContainerAttributes(l_AttributesContainer);

    if (SUCCEEDED(l_Result))
        l_Result = l_TranscodeProfile->SetAudioAttributes(l_AttributesAudio);

    if (SUCCEEDED(l_Result))
        l_Result = l_TranscodeProfile->SetVideoAttributes(l_AttributesVideo);

    MSDK_SAFE_RELEASE(l_AttributesContainer);
    MSDK_SAFE_RELEASE(l_AttributesAudio);
    MSDK_SAFE_RELEASE(l_AttributesVideo);

    if (SUCCEEDED(l_Result))
    {
        for (l_Index = 0; ; l_Index++)
        {
            l_Plugin = (MF_Plugin*) m_Plugins.Get(l_Index);

            if (l_Plugin == NULL)
                break;

            if (l_Plugin->m_Type == PluginType_Source)
            {
                l_MediaSource = l_Plugin->m_MediaSource;
                break;
            }
        }
    }

    if (l_MediaSource)
    {
        MSDK_SAFE_RELEASE(m_Topology);
        
        l_Result = MFCreateTranscodeTopology(
            l_MediaSource,
            l_ParameterStream.m_Stream_Name,
            l_TranscodeProfile,
            &m_Topology);
    }

    MSDK_SAFE_RELEASE(l_TranscodeProfile);

    if (SUCCEEDED(l_Result))
        l_Result = m_Session->SetTopology(0, m_Topology);

    l_Ret = WAIT_FAILED;

    if (SUCCEEDED(l_Result))
        l_Ret = WaitForSingleObject(m_EventTopologyReady, 10 * 1000);

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return E_FAIL;

    if (l_Ret == WAIT_OBJECT_0)
        return S_OK;

    return E_FAIL;
}

HRESULT MF_Pipeline::Play()
{
    HRESULT     l_Result;
    DWORD       l_Ret;
    PROPVARIANT l_PropVariant;

    l_Result = Resolve();

    if (SUCCEEDED(l_Result))
    {
        PropVariantInit(&l_PropVariant);

        ResetEvent(m_EventSessionStarted);
        ResetEvent(m_EventSessionStopped);

        l_Result = m_Session->Start(&GUID_NULL, &l_PropVariant);
    }

    l_Ret = WAIT_FAILED;

    if (SUCCEEDED(l_Result))
        l_Ret = WaitForSingleObject(m_EventSessionStarted, 10 * 1000);

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return E_FAIL;

    if (l_Ret == WAIT_OBJECT_0)
        return S_OK;

    return E_FAIL;
}

HRESULT MF_Pipeline::Pause()
{
    HRESULT     l_Result = S_OK;

    if (m_Session)
        l_Result = m_Session->Pause();

    return l_Result;
}

HRESULT MF_Pipeline::Stop()
{
    HRESULT     l_Result = S_OK;

    if (m_Session)
        l_Result = m_Session->Stop();

    return l_Result;
}

HRESULT MF_Pipeline::Seek(double a_AbsolutePosition, double a_Rate)
{
    HRESULT     l_Result;
    PROPVARIANT l_PropVariant;

    a_Rate;     // to avoid C4100 warning

    PropVariantInit(&l_PropVariant);

    if (a_AbsolutePosition != 0)
    {
        l_PropVariant.vt = VT_I8;
        l_PropVariant.hVal.QuadPart = (LONGLONG)(a_AbsolutePosition * 1e7);
    }

    l_Result = m_Session->Start(&GUID_NULL, &l_PropVariant);

    return l_Result;
}

HRESULT MF_Pipeline::Move(double a_RelativePosition, double a_Rate)
{
    HRESULT     l_Result;
    PROPVARIANT l_PropVariant;

    a_Rate;     // to avoid C4100 warning

    PropVariantInit(&l_PropVariant);

    if (a_RelativePosition != 0)
    {
        l_PropVariant.vt = VT_I8;
        l_PropVariant.hVal.QuadPart = (LONGLONG)(a_RelativePosition * 1e7);
    }

    l_Result = m_Session->Start(&GUID_NULL, &l_PropVariant);

    return l_Result;
}

HRESULT MF_Pipeline::SetRate(double a_Rate)
{
    if (m_RateControl == NULL)
        return E_FAIL;

    return m_RateControl->SetRate(false, (float)a_Rate);
}

HRESULT MF_Pipeline::GetDuration(double* a_Duration)
{
    if (a_Duration == NULL)
        return E_POINTER;

    *a_Duration = (double)m_Duration;

    return S_OK;
}

HRESULT MF_Pipeline::GetPosition(double* a_Position)
{
    DWORD   l_Result = S_OK;
    
    LONGLONG    l_Position = 0;

    if (a_Position == NULL)
        return E_POINTER;

    *a_Position = 0;
    
    if (m_PresentationClock)
    {
        l_Result = m_PresentationClock->GetTime(&l_Position);
        *a_Position = l_Position * 1e-7;
    }

    return S_OK;
}

DWORD MF_Pipeline::Wait(DWORD a_Milliseconds)
{
    DWORD   l_Result;

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return (DWORD) -1;

    l_Result = WaitForSingleObject(m_EventSessionStopped, a_Milliseconds);

    if (WaitForSingleObject(m_EventError, 0) == WAIT_OBJECT_0)
        return (DWORD) -1;

    return l_Result;
}

HRESULT MF_Pipeline::GetInterface(GUID a_GUID, void** a_Interface)
{
    a_GUID;         // to avoid C4100 warning
    a_Interface;    // to avoid C4100 warning

    return E_NOINTERFACE;
}

HRESULT MF_Pipeline::QueryInterface(REFIID a_IID, void** a_Interface)
{
    a_IID;          // to avoid C4100 warning
    a_Interface;    // to avoid C4100 warning

    return E_NOINTERFACE;
}

ULONG MF_Pipeline::AddRef(void)
{
    m_RefCount ++;

    return m_RefCount;
}

ULONG MF_Pipeline::Release(void)
{
    m_RefCount --;

    return m_RefCount;
}

HRESULT MF_Pipeline::GetParameters(DWORD* a_Flags, DWORD* a_Queue)
{
    *a_Flags = 0;
    *a_Queue = MFASYNC_CALLBACK_QUEUE_STANDARD;

    return S_OK;
}

HRESULT MF_Pipeline::Invoke(IMFAsyncResult* a_Result)
{
    HRESULT l_Result = S_OK;

    IMFMediaEvent*  l_Event         = NULL;
    MediaEventType  l_EventType     = MEUnknown;
    HRESULT         l_EventStatus   = S_OK;
    MF_TOPOSTATUS   l_TopoStatus    = MF_TOPOSTATUS_INVALID;
    IMFClock*       l_Clock         = NULL;

    l_Result = m_Session->EndGetEvent(a_Result, &l_Event);

    if (SUCCEEDED(l_Result))
        l_Result = l_Event->GetType(&l_EventType);

    if (SUCCEEDED(l_Result))
        l_Result = l_Event->GetStatus(&l_EventStatus);

    if (SUCCEEDED(l_Result))
    {
        if (FAILED(l_EventStatus))
        {
            SetEvent(m_EventError);

            SetEvent(m_EventSessionStarted);
            SetEvent(m_EventSessionStopped);
            SetEvent(m_EventTopologyReady);
        }

        if (SUCCEEDED(l_EventStatus))
        {
            switch (l_EventType)
            {
                case MESessionTopologyStatus:
                    
                    l_Result = l_Event->GetUINT32(MF_EVENT_TOPOLOGY_STATUS, (UINT32*)&l_TopoStatus);
                    
                    if (SUCCEEDED(l_Result))
                    {
                        switch (l_TopoStatus)
                        {
                            case MF_TOPOSTATUS_READY:
                                MFGetService(
                                    m_Session,
                                    MR_VIDEO_RENDER_SERVICE,
                                    __uuidof(IMFVideoDisplayControl),
                                    (void**)&m_VideoDisplay);

                                MFGetService(
                                    m_Session,
                                    MF_RATE_CONTROL_SERVICE,
                                    __uuidof(IMFRateSupport),
                                    (void**)&m_RateSupport);

                                MFGetService(
                                    m_Session,
                                    MF_RATE_CONTROL_SERVICE,
                                    __uuidof(IMFRateControl),
                                    (void**)&m_RateControl);

                                l_Result = m_Session->GetClock(&l_Clock);

                                if (SUCCEEDED(l_Result))
                                {
                                    l_Clock->QueryInterface(
                                        __uuidof(IMFPresentationClock),
                                        (void**)&m_PresentationClock);
                                }

                                MSDK_SAFE_RELEASE(l_Clock);

                                SetEvent(m_EventTopologyReady);

                                break;

                            case MF_TOPOSTATUS_STARTED_SOURCE:
                                break;

                            case MF_TOPOSTATUS_SINK_SWITCHED:
                                break;

                            case MF_TOPOSTATUS_ENDED:
                                break;
                        }
                    }

                    break;

                case MESessionTopologySet:
                    SetEvent(m_EventSessionTopologySet);
                    l_Result = S_OK;
                    break;

                case MESessionStarted:
                    SetEvent(m_EventSessionStarted);
                    l_Result = S_OK;
                    break;

                case MESessionPaused:
                    l_Result = S_OK;
                    break;

                case MESessionClosed:
                    SetEvent(m_EventSessionClosed);
                    l_Result = S_OK;
                    break;

                case MESessionStopped:
                    SetEvent(m_EventSessionStopped);
                    l_Result = S_OK;
                    break;

                case MEEndOfPresentation:
                    l_Result = m_Session->Stop();
                    break;
            }
        }
    }

    if (l_EventType != MESessionClosed)
        l_Result = m_Session->BeginGetEvent(this, NULL);

    MSDK_SAFE_RELEASE(l_Event);

    return l_Result;
}

HRESULT MF_Plugin::SetInputStream(PluginParameter* a_Parameter)
{
    HRESULT                     l_Result;
    MF_OBJECT_TYPE              l_ObjectType                = MF_OBJECT_INVALID;
    IUnknown*                   l_SourceUnknown             = NULL;
    IMFPresentationDescriptor*  l_PresentationDescriptor    = NULL;
    IMFStreamDescriptor*        l_StreamDescriptor          = NULL;
    IMFTopologyNode*            l_Node                      = NULL;
    DWORD                       l_DescriptorCount           = 0;
    DWORD                       l_MediaTypeCount;
    DWORD                       l_MediaTypeIndex;
    DWORD                       l_Index;
    DWORD                       l_Source;
    BOOL                        l_Selected;
    IMFMediaTypeHandler*        l_MediaTypeHandler          = NULL;
    IMFMediaType*               l_MediaType                 = NULL;
    PluginParameter             l_PluginParameter;
    GUID                        l_MajorType                 = GUID_NULL;
    GUID                        l_SubType                   = GUID_NULL;
    int                         l_CountAudio                = 0;
    int                         l_CountVideo                = 0;

    for (l_Source = 0, l_Result = E_FAIL; l_Source < 2 && FAILED(l_Result); l_Source++)
    {
        MSDK_SAFE_RELEASE(m_MediaSource);
        MSDK_SAFE_RELEASE(m_SourceResolver);
        MSDK_SAFE_RELEASE(l_PresentationDescriptor);

        if (l_Source == 0) // Try native Media Foundation SourceResolver (supports MP4 and ASF container)
            l_Result = MFCreateSourceResolver(&m_SourceResolver);

        if (l_Source == 1) // Try MF wrapper for DirectShow SourceResolver (supports mpeg2 container)
            l_Result = CoCreateInstance(CLSID_MF_DShowSourceResolver, NULL, CLSCTX_INPROC_SERVER,
                                        IID_IMFSourceResolver, (void**)&m_SourceResolver);

        if (SUCCEEDED(l_Result))
            l_Result = m_SourceResolver->CreateObjectFromURL(
                (LPCWSTR) a_Parameter->m_Stream_Name,           // URL of the source.
                MF_RESOLUTION_MEDIASOURCE,  // Create a source object.
                NULL,                       // Optional property store.
                &l_ObjectType,              // Receives the object type. 
                &l_SourceUnknown            // Receives a pointer to the source.
                );

        if (SUCCEEDED(l_Result))
            l_Result = l_SourceUnknown->QueryInterface(IID_IMFMediaSource, (void**)&m_MediaSource);

        if (SUCCEEDED(l_Result))
            l_Result = m_MediaSource->CreatePresentationDescriptor(&l_PresentationDescriptor);

        if (SUCCEEDED(l_Result))
            l_Result = l_PresentationDescriptor->GetStreamDescriptorCount(&l_DescriptorCount);

        if (SUCCEEDED(l_Result))
        for (l_Index = 0; l_Index < l_DescriptorCount; l_Index++)
        {
            l_Result = l_PresentationDescriptor->GetStreamDescriptorByIndex(l_Index, &l_Selected, &l_StreamDescriptor);

            if (SUCCEEDED(l_Result))
                l_Result = l_StreamDescriptor->GetMediaTypeHandler(&l_MediaTypeHandler);

            MSDK_SAFE_RELEASE(l_StreamDescriptor);

            if (SUCCEEDED(l_Result))
            {
                l_Result = l_MediaTypeHandler->GetMediaTypeCount(&l_MediaTypeCount);

                if (SUCCEEDED(l_Result))
                for(l_MediaTypeIndex = 0; l_MediaTypeIndex < l_MediaTypeCount; l_MediaTypeIndex++)
                {
                    l_Result = l_MediaTypeHandler->GetMediaTypeByIndex(l_MediaTypeIndex, &l_MediaType);

                    if (SUCCEEDED(l_Result))
                        l_Result = l_MediaType->GetGUID(MF_MT_SUBTYPE, &l_SubType);

                    if (SUCCEEDED(l_Result))
                    if (l_Source == 0)
                    {
                        UINT32      l_Size32;
                        UINT64      l_Size64;
                        IUnknown*   l_OpenFile;

                        // Check for MP4 container
                        l_Result = l_MediaType->GetBlobSize(MF_MT_MPEG4_SAMPLE_DESCRIPTION, &l_Size32);

                        // Check for ASF container
                        if (FAILED(l_Result))
                            l_Result = l_PresentationDescriptor->GetUINT64(MF_PD_ASF_DATA_LENGTH, &l_Size64);

                        // Check for internal splitter (do not Release() it)
                        if (FAILED(l_Result))
                            l_Result = l_SourceUnknown->QueryInterface(CLSID_MF_OpenFile, (void**)&l_OpenFile);

                        // there is no MPEG1 decoder by default
                        if (l_SubType == MEDIASUBTYPE_MPEG1Payload)
                            l_Result = E_FAIL;
                    }

                    if (SUCCEEDED(l_Result))
                    if (l_Source == 1)
                    {
                        if (l_SubType == MFAudioFormat_PCM  ||
                            l_SubType == MFVideoFormat_H264 ||
                            l_SubType == MFVideoFormat_WMV1 ||
                            l_SubType == MFVideoFormat_WMV2 ||
                            l_SubType == MFVideoFormat_WMV3 ||
                            l_SubType == MFVideoFormat_WVC1 ||
                            l_SubType == MFVideoFormat_MPEG2 && a_Parameter->m_SubType != ParameterSubType_Transcode)
                        {
                            l_Result = l_MediaTypeHandler->SetCurrentMediaType(l_MediaType);
                            break;
                        }
                    }

                    MSDK_SAFE_RELEASE(l_MediaType);
                }

                MSDK_SAFE_RELEASE(l_MediaTypeHandler);
            }
        }
    }

    if (SUCCEEDED(l_Result))
    for (l_Index = 0; l_Index < l_DescriptorCount; l_Index++)
    {
        l_Node = NULL;

        MSDK_SAFE_RELEASE(l_MediaType);
        MSDK_SAFE_RELEASE(l_MediaTypeHandler);
        MSDK_SAFE_RELEASE(l_StreamDescriptor);

        l_Result = l_PresentationDescriptor->GetStreamDescriptorByIndex(l_Index, &l_Selected, &l_StreamDescriptor);

        if (SUCCEEDED(l_Result))
            l_Result = l_StreamDescriptor->GetMediaTypeHandler(&l_MediaTypeHandler);

        if (SUCCEEDED(l_Result))
            l_Result = l_MediaTypeHandler->GetCurrentMediaType(&l_MediaType);

        if (SUCCEEDED(l_Result))
            l_Result = l_MediaType->GetMajorType(&l_MajorType);

        if (SUCCEEDED(l_Result))
        {
            if (l_MajorType == MFMediaType_Audio && l_CountAudio)
                continue;

            if (l_MajorType == MFMediaType_Video && l_CountVideo)
                continue;
        }

        if (SUCCEEDED(l_Result))
            l_Result = ConvertMediaTypeToPluginParameter(l_MediaType, &l_PluginParameter);

        if (SUCCEEDED(l_Result))
        {
            if (l_PluginParameter.m_Type == ParameterType_Audio)
            {
                l_Result = SetPluginParameter(&l_PluginParameter, l_CountAudio);

                if (SUCCEEDED(l_Result))
                    l_CountAudio ++;
            }

            if (l_PluginParameter.m_Type == ParameterType_Video)
            {
                l_Result = SetPluginParameter(&l_PluginParameter, l_CountVideo);

                if (SUCCEEDED(l_Result))
                    l_CountVideo ++;
            }
        }

        if (SUCCEEDED(l_Result))
            l_Result = MFCreateTopologyNode(MF_TOPOLOGY_SOURCESTREAM_NODE, &l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetUnknown(MF_TOPONODE_SOURCE, m_MediaSource);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetUnknown(MF_TOPONODE_PRESENTATION_DESCRIPTOR, l_PresentationDescriptor);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetUnknown(MF_TOPONODE_STREAM_DESCRIPTOR, l_StreamDescriptor);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetGUID(CLSID_MF_NodeMajorType, l_MajorType);

        if (SUCCEEDED(l_Result))
            l_Result = m_Topology->AddNode(l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = AddNode(l_Node);
    }

    MSDK_SAFE_RELEASE(l_MediaType);
    MSDK_SAFE_RELEASE(l_MediaTypeHandler);
    MSDK_SAFE_RELEASE(l_StreamDescriptor);
    MSDK_SAFE_RELEASE(l_PresentationDescriptor);
    MSDK_SAFE_RELEASE(l_SourceUnknown);

    if (FAILED(l_Result))
    {
        MSDK_SAFE_RELEASE(m_MediaSource);
        MSDK_SAFE_RELEASE(m_SourceResolver);
    }

    return l_Result;
}

MF_Plugin* MF_Pipeline::AddPlugin_Simple(PluginType a_PluginType)
{
    HRESULT l_Result = E_FAIL;

    MF_Plugin*  l_Plugin = new MF_Plugin(a_PluginType);

    if (l_Plugin != NULL)
    {
        l_Plugin->SetTopology(m_Topology, m_Session);
        l_Result = S_OK;
    }

    if (FAILED(l_Result))
    {
        MSDK_SAFE_DELETE(l_Plugin);
    }

    return l_Plugin;
}

MF_Plugin* MF_Pipeline::AddPlugin_Transform(GUID a_GUID)
{
    HRESULT             l_Result    = E_FAIL;
    IMFTopologyNode*    l_Node      = NULL;
    IMFMediaType*       l_MediaType = NULL;
    GUID                l_MajorType;

    MF_Plugin*  l_Plugin = new MF_Plugin(PluginType_Transform);

    if (l_Plugin != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = CoCreateInstance(a_GUID, NULL, CLSCTX_INPROC_SERVER,
                                    IID_IMFTransform, (void**)&l_Plugin->m_Transform);

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateTopologyNode(MF_TOPOLOGY_TRANSFORM_NODE, &l_Node);

    if (SUCCEEDED(l_Result))
        l_Result = l_Node->SetObject(l_Plugin->m_Transform);

    if (SUCCEEDED(l_Result))
        l_Result = m_Topology->AddNode(l_Node);

    if (SUCCEEDED(l_Result))
        l_Result = l_Plugin->AddNode(l_Node);

    if (SUCCEEDED(l_Result))
    {
        IMFAttributes* l_Attributes = NULL;

        l_Result = l_Result = l_Plugin->m_Transform->GetAttributes(&l_Attributes);

        if (SUCCEEDED(l_Result))
        {
            l_Result = l_Attributes->SetUINT32(MF_TRANSFORM_ASYNC_UNLOCK, TRUE);
            l_Attributes->Release();
        }

        l_Result = l_Plugin->m_Transform->GetInputAvailableType(0, 0, &l_MediaType);

        if (FAILED(l_Result))
            l_Result = l_Plugin->m_Transform->GetOutputAvailableType(0, 0, &l_MediaType);

        if (SUCCEEDED(l_Result))
            l_Result = l_MediaType->GetMajorType(&l_MajorType);
    }
  
    if (SUCCEEDED(l_Result))
        l_Result = l_Node->SetGUID(CLSID_MF_NodeMajorType, l_MajorType);

    if (FAILED(l_Result))
    {
        MSDK_SAFE_DELETE(l_Plugin);
    }

    return l_Plugin;
}

MF_Plugin* MF_Pipeline::AddPlugin_RenderAudio()
{
    HRESULT             l_Result    = E_FAIL;
    IMFTopologyNode*    l_Node      = NULL;
    IMFActivate*        l_Activate  = NULL;

    MF_Plugin*  l_Plugin = new MF_Plugin(PluginType_RenderAudio);

    if (l_Plugin != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateTopologyNode(MF_TOPOLOGY_OUTPUT_NODE, &l_Node);

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateAudioRendererActivate(&l_Activate);

    if (SUCCEEDED(l_Result))
        l_Result = l_Node->SetObject(l_Activate);

    if (SUCCEEDED(l_Result))
        l_Result = l_Node->SetGUID(CLSID_MF_NodeMajorType, MFMediaType_Audio);

    if (SUCCEEDED(l_Result))
        l_Result = m_Topology->AddNode(l_Node);

    if (SUCCEEDED(l_Result))
        l_Result = l_Plugin->AddNode(l_Node);

    if (FAILED(l_Result))
    {
        MSDK_SAFE_DELETE(l_Plugin);
    }

    MSDK_SAFE_RELEASE(l_Activate);

    return l_Plugin;
}

MF_Plugin* MF_Pipeline::AddPlugin_FileWriter()
{
    HRESULT                 l_Result            = E_FAIL;
    DWORD                   l_StreamSinkCount   = 0;
    IMFStreamSink*          l_StreamSink;
    DWORD                   l_Index;
    IMFTopologyNode*        l_Node;

    MF_Plugin*  l_Plugin = new MF_Plugin(PluginType_FileWriter);

    if (l_Plugin != NULL)
        l_Result = S_OK;

    if (SUCCEEDED(l_Result))
        l_Result = CoCreateInstance(CLSID_MF_FileWriter, NULL, CLSCTX_INPROC_SERVER,
                                    IID_IMFMediaSink, (void**)&l_Plugin->m_MediaSink);

    if (SUCCEEDED(l_Result))
        l_Result = l_Plugin->m_MediaSink->GetStreamSinkCount(&l_StreamSinkCount);

    if (SUCCEEDED(l_Result))
    for (l_Index = 0; l_Index < l_StreamSinkCount; l_Index++)
    {
        l_Result = l_Plugin->m_MediaSink->GetStreamSinkByIndex(l_Index, &l_StreamSink);

        l_Node = NULL;

        if (SUCCEEDED(l_Result))
            l_Result = MFCreateTopologyNode(MF_TOPOLOGY_OUTPUT_NODE, &l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetObject(l_StreamSink);

        if (SUCCEEDED(l_Result))
            l_Result = m_Topology->AddNode(l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = l_Plugin->AddNode(l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetGUID(CLSID_MF_NodeMajorType, MFMediaType_Binary);

        MSDK_SAFE_RELEASE(l_StreamSink);
    }

    if (FAILED(l_Result))
    {
        MSDK_SAFE_DELETE(l_Plugin);
    }

    return l_Plugin;
}

HRESULT MF_Plugin::SetOutputStream(PluginParameter* a_Parameter)
{
    HRESULT l_Result;

    l_Result = MFCreateFile(
        MF_ACCESSMODE_READWRITE,
        MF_OPENMODE_DELETE_IF_EXIST,
        MF_FILEFLAGS_NONE,
        (LPCWSTR) a_Parameter->m_Stream_Name,
        &m_ByteStream);

    return l_Result;
}

HRESULT MF_Pipeline::Connect_Delayed(MF_Plugin* a_PluginSrc, MF_Plugin* a_PluginDst)
{
    return a_PluginDst->m_PluginsToConnect.Add(a_PluginSrc);
}

HRESULT MF_Plugin::ConnectFinalize()
{
    if (m_Type == PluginType_RenderVideo)
        return ConnectFinalize_RenderVideo();

    if (m_Type == PluginType_Muxer)
        return ConnectFinalize_Muxer();

    return S_OK;
}

HRESULT MF_Plugin::ConnectFinalize_RenderVideo()
{
    HRESULT             l_Result        = S_OK;
    IMFTopologyNode*    l_Node          = NULL;
    IMFActivate*        l_Activate      = NULL;
    MF_Plugin*          l_PluginSrc     = NULL;
    DWORD               l_Index;
    IMFTopologyNode*    l_NodeSrc;
    GUID                l_MajorTypeSrc;
    PluginParameter     l_Parameter;

    if (m_Type != PluginType_RenderVideo)
        return E_FAIL;

    if (m_Window == NULL)
    {
        l_Parameter.m_Type = ParameterType_RenderVideo;

        l_Parameter.m_Render_Window_Width   = 720;
        l_Parameter.m_Render_Window_Height  = 480;

        SetVideoRenderWindow(&l_Parameter);
    }

    if (m_Window == NULL)
        return E_FAIL;

    if (SUCCEEDED(l_Result))
        ShowWindow(m_Window, SW_SHOW);

    if (SUCCEEDED(l_Result))
        l_Result = MFCreateTopologyNode(MF_TOPOLOGY_OUTPUT_NODE, &l_Node);
    
    if (SUCCEEDED(l_Result))
        l_Result = MFCreateVideoRendererActivate(m_Window, &l_Activate);

    if (SUCCEEDED(l_Result))
        l_Result = l_Node->SetObject(l_Activate);

    if (SUCCEEDED(l_Result))
        l_Result = l_Node->SetGUID(CLSID_MF_NodeMajorType, MFMediaType_Video);

    if (SUCCEEDED(l_Result))
        l_Result = m_Topology->AddNode(l_Node);

    if (SUCCEEDED(l_Result))
        l_Result = AddNode(l_Node);

    MSDK_SAFE_RELEASE(l_Activate);

    if (SUCCEEDED(l_Result))
        l_PluginSrc = (MF_Plugin*) m_PluginsToConnect.Get(0);

    if (l_PluginSrc != NULL)
    for(l_Index = 0, l_Result = E_FAIL; FAILED(l_Result); l_Index++)
    {
        l_NodeSrc = l_PluginSrc->GetNode(l_Index);

        if (l_NodeSrc == NULL)
            break;

        l_MajorTypeSrc = GUID_NULL;

        l_NodeSrc->GetGUID(CLSID_MF_NodeMajorType, &l_MajorTypeSrc);

        if (l_MajorTypeSrc == MFMediaType_Video)
            l_Result = l_NodeSrc->ConnectOutput(0, l_Node, 0);
    }

    return l_Result;
}

HRESULT MF_Plugin::ConnectFinalize_Muxer()
{
    HRESULT                 l_Result;
    MF_Plugin*              l_PluginSrc;
    IMFTopologyNode*        l_Node;
    IMFStreamSink*          l_StreamSink;
    PluginParameter         l_PluginParameter;
    IMFMediaTypeHandler*    l_MediaTypeHandler  = NULL;
    IMFMediaType*           l_MediaType         = NULL;
    IMFMediaType*           l_MediaTypeAudio    = NULL;
    IMFMediaType*           l_MediaTypeVideo    = NULL;
    IMFTopologyNode*        l_NodeAudio         = NULL;
    IMFTopologyNode*        l_NodeVideo         = NULL;
    GUID                    l_MajorType;
    DWORD                   l_StreamSinkCount   = 0;
    DWORD                   l_Index;

    if (m_Type != PluginType_Muxer)
        return E_FAIL;

    if (m_PluginsToConnect.Get(0) == NULL)
        return S_OK;

    if (m_MediaSink != NULL)
        return E_FAIL;

    for (l_Index = 0; ; l_Index++)
    {
        l_MediaType = NULL;
        l_PluginSrc = (MF_Plugin*) m_PluginsToConnect.Get(l_Index);

        if (l_PluginSrc == NULL)
            break;

        if (l_MediaType == NULL && l_MediaTypeAudio == NULL)
        {
            l_PluginParameter.m_Type = ParameterType_Audio;

            l_Result = l_PluginSrc->GetPluginParameter(&l_PluginParameter);

            if (SUCCEEDED(l_Result))
                l_Result = l_PluginSrc->ConvertPluginParameterToMediaType(&l_PluginParameter, &l_MediaType);

            if (SUCCEEDED(l_Result))
            {
                l_MediaTypeAudio = l_MediaType;
                l_NodeAudio      = l_PluginSrc->GetNode(0);
            }
        }

        if (l_MediaType == NULL && l_MediaTypeVideo == NULL)
        {
            l_PluginParameter.m_Type = ParameterType_Video;

            l_Result = l_PluginSrc->GetPluginParameter(&l_PluginParameter);

            if (SUCCEEDED(l_Result))
                l_Result = l_PluginSrc->ConvertPluginParameterToMediaType(&l_PluginParameter, &l_MediaType);

            if (SUCCEEDED(l_Result))
            {
                l_MediaTypeVideo = l_MediaType;
                l_NodeVideo      = l_PluginSrc->GetNode(0);
            }
        }

        if (l_MediaType == NULL && l_PluginSrc->m_Transform != NULL)
        {
            l_Result = l_PluginSrc->m_Transform->GetOutputAvailableType(0, 0, &l_MediaType);

            if (SUCCEEDED(l_Result))
                l_Result = l_MediaType->GetMajorType(&l_MajorType);

            if (SUCCEEDED(l_Result))
            {
                if (l_MediaTypeAudio == NULL && l_MajorType == MFMediaType_Audio)
                {
                    l_MediaTypeAudio = l_MediaType;
                    l_NodeAudio      = l_PluginSrc->GetNode(0);
                }

                if (l_MediaTypeVideo == NULL && l_MajorType == MFMediaType_Video)
                {
                    l_MediaTypeVideo = l_MediaType;
                    l_NodeVideo      = l_PluginSrc->GetNode(0);
                }
            }
        }
    }

    l_Result = MFCreateMPEG4MediaSink(
        m_ByteStream,
        l_MediaTypeVideo,
        l_MediaTypeAudio,
        &m_MediaSink);

    if (SUCCEEDED(l_Result))
        l_Result = m_MediaSink->GetStreamSinkCount(&l_StreamSinkCount);

    if (SUCCEEDED(l_Result))
    for (l_Index = 0; l_Index < l_StreamSinkCount; l_Index++)
    {
        l_Result = m_MediaSink->GetStreamSinkByIndex(l_Index, &l_StreamSink);

        l_Node = NULL;

        if (SUCCEEDED(l_Result))
            l_Result = MFCreateTopologyNode(MF_TOPOLOGY_OUTPUT_NODE, &l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = l_Node->SetObject(l_StreamSink);

        if (SUCCEEDED(l_Result))
            l_Result = l_StreamSink->GetMediaTypeHandler(&l_MediaTypeHandler);
    
        if (SUCCEEDED(l_Result))
            l_Result = l_MediaTypeHandler->GetMajorType(&l_MajorType);

        if (SUCCEEDED(l_Result))
            l_Result = m_Topology->AddNode(l_Node);

        if (SUCCEEDED(l_Result))
            l_Result = AddNode(l_Node);

        if (SUCCEEDED(l_Result))
        {
            if (l_MajorType == MFMediaType_Audio && l_NodeAudio != NULL)
                l_Result = l_NodeAudio->ConnectOutput(0, l_Node, 0);

            if (l_MajorType == MFMediaType_Video && l_NodeVideo != NULL)
                l_Result = l_NodeVideo->ConnectOutput(0, l_Node, 0);
        }

        MSDK_SAFE_RELEASE(l_MediaTypeHandler);
        MSDK_SAFE_RELEASE(l_StreamSink);
    }
    
    MSDK_SAFE_RELEASE(l_MediaTypeAudio);
    MSDK_SAFE_RELEASE(l_MediaTypeVideo);

    return l_Result;
}
