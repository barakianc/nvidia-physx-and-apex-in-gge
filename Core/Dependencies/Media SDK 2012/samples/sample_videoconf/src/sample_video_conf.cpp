/* ****************************************************************************** *\

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2011 Intel Corporation. All Rights Reserved.

\* ****************************************************************************** */

#include "video_conf_pipeline.h"
#include "actions.h"
#include "action_processor.h"
#include "brc.h"
#include <sample_defs.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>


void PrintHelp( const std::basic_string<TCHAR> & strAppName
              , const std::basic_string<TCHAR> & strErrorMessage = _T(""))
{
    if (!strErrorMessage.empty())
    {
        _tprintf(_T("Error: %s\n\n"), strErrorMessage.c_str());
    }    

    _tprintf(_T("Usage: %s [Options] -i InputYUVFile -o OutputEncodedFile -w width -h height\n"), strAppName.c_str());
    _tprintf(_T("Options: \n"));
    _tprintf(_T("   [-i1, -i2 ... frame_num fileName] - after reaching frame_num encoding starts from second input file, encoder will be reset with new corresponding resolution\n"));
    _tprintf(_T("   [-w1, -w2 ... width] - width to be used for encoding n-th input file\n"));
    _tprintf(_T("   [-h1, -h2 ... height] - height to be used for encoding n-th file\n"));
    _tprintf(_T("   [-f, -f1, -f2 ... frameRate] - video frame rate (frames per second) for n-th file\n"));
    _tprintf(_T("   [-b bitRate] - encoded bit rate (Kbits per second)\n"));
    _tprintf(_T("   [-hw] - use platform specific SDK implementation, if not specified software implementation is used\n"));
    _tprintf(_T("   [-par parameters_file] - parameters file may contain same options as a command line\n"));
    _tprintf(_T("   [-bs frame_num value] - prior encoding of 'frame_num' scales current bitrate by 'value'\n"));
    _tprintf(_T("   [-bf frame_num broken_frame_num] - prior encoding of 'frame_num' frame, encoder received information that already encoded frame with  number 'broken_frame_num' failed to be decoded\n"));
    _tprintf(_T("   [-gkf frame_num] - generates key frame at position 'frame_num'\n"));
    _tprintf(_T("   [-ltr frame_num] - 'frame_num' wil be converted into longterm\n"));
    _tprintf(_T("   [-ts num_layers] - will create up to 4 temporal layers with scale_base=2 -> 1,2,4,8\n"));
    _tprintf(_T("   [-brc] - enables external bitrate control based on per-frame QP\n"));
    _tprintf(_T("   [-l0 frame_num L0_len ] - specifies number of reference frames in L0 array for encoding 'frame_num'\n"));
    _tprintf(_T("   [-rpmrs] - enables reference picure marking repetion SEI feature\n"));
    _tprintf(_T("   [-latency] - enables latency calculation and reporting\n"));
    _tprintf(_T("\n"));
    _tprintf(_T("Example: %s -i InputYUVFile -o OutputEncodedFile -w width -h height -bs 10 0.5\n"), strAppName.c_str());
    _tprintf(_T("\n"));
}

//convert from string to specific type, like double, int, etc
template <class T> 
void lexical_cast(const std::basic_string<TCHAR>& from, T &value)
{
    std::basic_stringstream<TCHAR> sstream; 

    sstream.str(from); 
    if ((sstream >> value).fail() || !sstream.eof())
    {
        TCHAR msg[1024];
#ifdef UNICODE
        _stprintf_s(msg, _T("Cannot cast %s to %S\n"), from.c_str(), typeid(value).name());
#else
        _stprintf_s(msg, _T("Cannot cast %s to %s\n"), from.c_str(), typeid(value).name());
#endif
        throw std::basic_string<TCHAR>(msg);
    }
}

int get_index(const std::basic_string<TCHAR>& from, int prefix_len )
{
    int idx = 0;
    if (from.length() != (size_t)prefix_len)
    {
        try
        {
            lexical_cast(from.substr(prefix_len), idx);
        }
        catch (...)
        {
            idx = -1;
        }
    }
    return idx;
}

#define CHECK_OPTION_ARGS(n)\
if (i + n == nArgNum)\
{\
    throw std::basic_string<TCHAR>(_T("Invalid syntax for option:")) + strInput[i];\
}

void ParseParFile(const TCHAR* file_path, VideoConfParams& pParams);
void CheckInitParams(VideoConfParams& pParams);
void ParseInputString(TCHAR** strInput, int nArgNum, VideoConfParams& params)
{
    if (0 == nArgNum)
    {
        throw std::basic_string<TCHAR>(_T(""));
    }

    // parse command line parameters
    for (mfxU8 i = 0; i < nArgNum; i++)
    {
        int idx  = get_index(strInput[i], 2);
        if (0 == _tcsicmp(strInput[i], _T("-hw")))
        {
            params.bUseHWLib = true;
        }
        else if (0 == _tcsnicmp(strInput[i], _T("-w"), 2) && -1 != idx)
        {
            CHECK_OPTION_ARGS(1);
            lexical_cast(strInput[++i], params.sources[idx].nWidth);
        }
        else if (0 == _tcsnicmp(strInput[i], _T("-h"), 2) && -1 != idx)
        {
            CHECK_OPTION_ARGS(1);
            lexical_cast(strInput[++i], params.sources[idx].nHeight);
        }
        else if (0 == _tcsnicmp(strInput[i], _T("-f"), 2) && -1 != idx)
        {
            CHECK_OPTION_ARGS(1);
            lexical_cast(strInput[++i], params.sources[idx].dFrameRate);
        }
        else if (0 == _tcsicmp(strInput[i], _T("-b")))
        {
            CHECK_OPTION_ARGS(1);
            lexical_cast(strInput[++i], params.nTargetKbps);
        }
        else if (0 == _tcsnicmp(strInput[i], _T("-i"), 2) && -1 != idx)
        {        
            CHECK_OPTION_ARGS(1);
            //we expect frame number attached with -i1, -i2
            if (idx != 0)
            {
                CHECK_OPTION_ARGS(2);
                int nFrame = 0;
                lexical_cast(strInput[++i], nFrame);
                params.pActionProc->RegisterAction(nFrame, new SetSourceAction(idx));
            }
            params.sources[idx].srcFile = strInput[++i];
        }
        else if (0 == _tcsicmp(strInput[i], _T("-o")))
        {
            CHECK_OPTION_ARGS(1);
            params.dstFile = strInput[++i];
        }
        else if (0 == _tcsicmp(strInput[i], _T("-par")))
        {
            CHECK_OPTION_ARGS(1);
            ParseParFile(strInput[++i], params);
        }
        else if (0 == _tcsicmp(strInput[i], _T("-bs" )))
        {
            CHECK_OPTION_ARGS(2);

            double dBitrateScale;
            int nFrameOrder;
            lexical_cast(strInput[++i], nFrameOrder);
            lexical_cast(strInput[++i], dBitrateScale);

            params.pActionProc->RegisterAction(nFrameOrder, new ChangeBitrateAction(dBitrateScale));
        }
        else if (0 == _tcsicmp(strInput[i], _T("-l0")))
        {
            CHECK_OPTION_ARGS(2);

            mfxU16 nLen;
            int nFrameOrder;
            lexical_cast(strInput[++i], nFrameOrder);
            lexical_cast(strInput[++i], nLen);

            params.pActionProc->RegisterAction(nFrameOrder, new SetL0SizeAction(nLen));
        }
        else if (0 == _tcsicmp(strInput[i], _T("-brc")))
        {
            params.pBrc.reset(new SampleBRC());
        }
        else if (0 == _tcsicmp(strInput[i], _T("-rpmrs")))
        {
            params.bRPMRS = true;
        }
        else if (0 == _tcsicmp(strInput[i], _T("-bf")))
        {
            CHECK_OPTION_ARGS(2);

            int nFrameOrder;
            int nFrameBroken;
            lexical_cast(strInput[++i], nFrameOrder);
            lexical_cast(strInput[++i], nFrameBroken);
            if (nFrameBroken >= nFrameOrder)
            {
                std::basic_stringstream<TCHAR> stream;
                stream<<_T("in parsing -bf option. Broken frameorder(current=")<<nFrameBroken<<_T(") ");;
                stream<<_T("should be less than reported frameorder(current=")<<nFrameOrder<<_T(")");

                throw stream.str();
            }

            //to instruct encoder to DONOT predict from specific frame it is necessary to add this frame to rejected ref list            
            //putting frame into rejected list does make this frame permanently rejected

            //if feedback is delayed frames predicted from broken can be already encoded, need to also remove all of them from references
            for (int j = nFrameBroken; j < nFrameOrder; j++)
            {
                params.pActionProc->RegisterAction(nFrameOrder, new PutFrameIntoRefListAction(REFLIST_REJECTED, j, false));
            }
        }
        else if (0 == _tcsicmp(strInput[i], _T("-gkf")))
        {
            CHECK_OPTION_ARGS(1);

            int nFrameOrder;
            lexical_cast(strInput[++i], nFrameOrder);

            params.pActionProc->RegisterAction(nFrameOrder, new KeyFrameInsertAction());
        }
        else if (0 == _tcsicmp(strInput[i], _T("-ltr")))
        {
            CHECK_OPTION_ARGS(1);

            mfxU32 nLTFrameOrder;
            lexical_cast(strInput[++i], nLTFrameOrder);

            static int j = 0;
            //firstly frame should be added to long term list once
            //NOTE: to remove this longterm, you need to put it once into rejected reflist
            params.pActionProc->RegisterAction(nLTFrameOrder, new PutFrameIntoRefListAction(REFLIST_LONGTERM, nLTFrameOrder, false));

            //secondary to say MediaSDK that prediction from this longterm is necessary you need to put frameorder into preferred reflist
            //preferred list is stateless and should be created for every frame, that is why we creating permanent action untill next IDR
            params.pActionProc->RegisterAction(nLTFrameOrder, new PutFrameIntoRefListAction(REFLIST_PREFERRED, nLTFrameOrder, true));
        } else if ( 0== _tcsicmp(strInput[i], _T("-ts")))
        {
            CHECK_OPTION_ARGS(1);
            params.nTemporalScalabilityBase = 2;
            lexical_cast(strInput[++i], params.nTemporalScalabilityLayers);
            if (params.nTemporalScalabilityLayers > 4)
            {
                throw std::basic_string<TCHAR>(_T("in -ts option maximum layers value is 4"));
            }
        } else if (0 == _tcsicmp(strInput[i], _T("-latency")))
        {
            params.bCalcLAtency = true;
        }
            
        else
        {
            throw std::basic_string<TCHAR>(_T("Unknown option : ")) + strInput[i];
        }
    }
}

void CheckInitParams(VideoConfParams& params)
{
    std::map<int, VideoConfParams::SourceInfo> :: iterator i;
    std::basic_stringstream<TCHAR> error;
    if (params.sources.empty())
    {
        throw std::basic_string<TCHAR>(_T("Source file name not set"));
    }

    for (i = params.sources.begin(); i != params.sources.end(); i++)
    {
        VideoConfParams::SourceInfo &info = i->second;
        // check if all mandatory parameters were set
        if (info.srcFile.empty())
        {
            if (i->first != 0)
            {
                error<< i->first << _T(" ");
            }
            error<< _T("source file name not set");
            
            throw error.str();
        }

        if (params.dstFile.empty())
        {
            throw std::basic_string<TCHAR>(_T("Destination file name not set"));
        }

        if (0 == info.nWidth)
        {
            if (i->first != 0)
            {
                error<< i->first << _T(" ");
            }
            error<< _T("width must be specidied");
            
            if (i->first != 0)
            {
                error<< _T("(-w") << i->first << _T(")");
            }
            else
            {
                error<< _T("(-w)");
            }

            throw error.str();
        }
        
        if (0 == info.nHeight)
        {
            if (i->first!= 0)
            {
                error<< i->first << _T(" ");
            }
            error<< _T("height must be specidied");

            if (i->first != 0)
            {
                error<< _T("(-h") << i->first << _T(")");
            }
            else
            {
                error<< _T("(-h)");
            }

            throw error.str();
        }

        if (info.dFrameRate <= 0)
        {
            info.dFrameRate = 30;
        }    
    }
    // calculate default bitrate based on the resolution (a parameter for encoder, so Dst resolution is used)
    if (params.nTargetKbps == 0)
    {        
        params.nTargetKbps = CalculateDefaultBitrate(MFX_CODEC_AVC, 0, params.sources[0].nWidth, params.sources[0].nHeight, params.sources[0].dFrameRate);         
    }    
}

void ParseParFile(const TCHAR* file_path, VideoConfParams& pParams)
{
    //reading whole file into string stream
    std::basic_fstream<TCHAR> file_stream(file_path, std::ios_base::in );
    if (file_stream.fail())
        throw std::basic_string<TCHAR>(_T("Failed to open par file : ")) + file_path;

    std::basic_stringstream<TCHAR> str_stream;
    file_stream >> str_stream.rdbuf();

    std::basic_string<TCHAR> str = str_stream.str();

    //removing comments
    for (size_t offset = 0; offset != std::basic_string<TCHAR>::npos;)
    {
        offset = str.find(_T("#"), offset);
        if (offset != std::basic_string<TCHAR>::npos)
        {
            //removing whole line
            size_t offset2 = str.find(_T("\n"), offset);
            size_t count = offset2 == std::basic_string<TCHAR>::npos ? offset2 : offset2 - offset;
            str.erase(offset, count);
            offset++;
        }
    }
    //removing line endings
    std::replace(str.begin(), str.end(), _T('\n'), _T(' '));
    
    //put this 1 line file into stream;
    str_stream.str(str);

    //split it into words by spaces, and forming TCHAR** array
    std::list<std::basic_string<TCHAR> > command_line_strings;
    std::vector<TCHAR*> command_line_args;

    for (;!str_stream.eof();)
    {
        std::basic_string<TCHAR> current_arg;
        if ((str_stream >> current_arg).fail())
            break;
        command_line_strings.push_back(current_arg);
        command_line_args.push_back(&command_line_strings.back().at(0));
    }
    
    if (!command_line_args.empty())
    {
        ParseInputString((TCHAR**)&command_line_args.front(), (int)command_line_args.size(), pParams);
    }
}

int _tmain(int argc, TCHAR *argv[])
{  
    VideoConfParams init_params;   // input parameters from command line
    std::auto_ptr<IPipeline>  pPipeline;
    mfxStatus sts = MFX_ERR_NONE; // return value check
    std::basic_string<TCHAR> appName (*argv);

    try
    {
        _tprintf(_T("Intel(R) Media SDK Video Conference Sample Version %s\n\n"), MSDK_SAMPLE_VERSION);

        init_params.pActionProc.reset(new ActionProcessor);

        ParseInputString(++argv, --argc, init_params);
        CheckInitParams(init_params);

        pPipeline.reset(new VideoConfPipeline()); 

        sts = pPipeline->Init(&init_params);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);   

        pPipeline->PrintInfo();

        _tprintf(_T("Processing started\n"));

        sts = pPipeline->Run();

        if (MFX_ERR_DEVICE_LOST == sts || MFX_ERR_DEVICE_FAILED == sts)
        {            
            _tprintf(_T("\nERROR: Hardware device was lost or returned an unexpected error\n"));
        }

        _tprintf(_T("\rProcessing finished\n"));   

    }
    catch (const std::basic_string<TCHAR> & message)
    {
        //get the application name from path
        size_t file_name_start_pos = appName.find_last_of((_T("/\\")));
        if (std::basic_string<TCHAR>::npos != file_name_start_pos)
        {
            appName.erase(0, file_name_start_pos + 1);
        }
        PrintHelp(appName, message);
    }
    catch (std::exception &ex)
    {
#ifdef UNICODE
        wprintf(L"\nstd::exception caught: %S\n", ex.what());    
#else
        printf("\nstd::exception caught: %s\n", ex.what());    
#endif
    }
    catch (...)
    {
        _tprintf(_T("\nUnknown exception caught\n"));    
    }

    return 0;
}