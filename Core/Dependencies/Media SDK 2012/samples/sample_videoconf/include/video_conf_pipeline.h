/* ****************************************************************************** *\

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2011 Intel Corporation. All Rights Reserved.

\* ****************************************************************************** */

#pragma once

#define D3D_SURFACES_SUPPORT

#ifdef D3D_SURFACES_SUPPORT
#pragma warning(disable : 4201)
#include <d3d9.h>
#include <dxva2api.h>
#endif

#include <tchar.h>
#include <string>
#include <memory>
#include <map>

#include "mfxvideo++.h"
#include "sample_utils.h"
#include "base_allocator.h"
#include "interface.h"


class VideoConfParams 
    : public IInitParams
{
public:
    VideoConfParams ()
        : bRPMRS()
        , bUseHWLib()
        , nTemporalScalabilityBase()
        , nTemporalScalabilityLayers()
        , nTargetKbps()
        , bCalcLAtency()
    {
    }
public:
    struct SourceInfo
    {
        std::basic_string<TCHAR> srcFile;
        mfxU16 nWidth;      // picture width
        mfxU16 nHeight;     // picture height
        mfxF64 dFrameRate;  // framerate 
        SourceInfo()
            : nWidth()
            , nHeight()
            , dFrameRate()
        {
        }
    };

    mfxU16 nTargetKbps;//target bitrate

    bool bRPMRS; //Reference Picture Marking Repetition SEI
    bool bUseHWLib; // whether to use HW mfx library
    bool bCalcLAtency;// optional printing of latency information per frame

    std::map<int, SourceInfo> sources;
    std::basic_string<TCHAR> dstFile;

    mfxU16 nTemporalScalabilityBase;
    mfxU16 nTemporalScalabilityLayers;
    
    //dynamic encoder control actions processor
    //prior encoding of particular frame several actions could be applied as a simulation of generic video conferencing infrastructure events
    std::auto_ptr <IActionProcessor> pActionProc;

    //external brc
    std::auto_ptr<IBRC> pBrc;
};

class VideoConfPipeline
    : public IPipeline
{
public:
    VideoConfPipeline();
    virtual ~VideoConfPipeline();
    
    //////////////////////////////////////////////////////////////////////////
    //IPipeline
    virtual mfxStatus   Run();
    virtual mfxStatus   Init(IInitParams *);
    virtual mfxStatus   Close();  
    virtual mfxStatus   InsertKeyFrame();
    virtual mfxStatus   ScaleBitrate(double dScaleBy); 
    virtual mfxStatus   AddFrameToRefList(RefListType refList, mfxU32 nFrameOrder);
    virtual mfxStatus   SetNumL0Active(mfxU16 nActive); 
    virtual mfxStatus   ResetPipeline(int nSourceIdx); 
    virtual void        PrintInfo();

protected:

    mfxStatus CreateAllocator();
    mfxStatus AllocFrames();
    mfxStatus GetFreeSurface(mfxFrameSurface1*& pSurfacesPool);
    mfxStatus CreateDeviceManager();
    //special parameters setup to configure MediaSDK Encoder for producing low latency bitstreams
    mfxStatus InitMfxEncParamsLowLatency();
    //temporal scalability initialisation
    mfxStatus InitTemporalScalability();
    //mostly separated to call allign functions
    void InitFrameInfo(mfxU16 nWidth, mfxU16 nHeight);
    //initiliaze either Mediasdk's BRC or own
    void InitBrc();
    void AttachAvcRefListCtrl();
    mfxStatus InitMFXComponents();
    mfxStatus EncodeFrame(mfxFrameSurface1 *pSurface);
    
    
    void DeleteFrames();
    void DeleteDeviceManager();
    void DeleteAllocator();

    VideoConfParams m_initParams;
    CSmplBitstreamWriter m_FileWriter;
    CSmplYUVReader m_FileReader;
    
    //encode query and initialization params
    std::auto_ptr<MFXVideoENCODE> m_encoder;
    MFXVideoSession m_mfxSession;
    mfxVideoParam m_mfxEncParams; 
    MFXFrameAllocator* m_pMFXAllocator; 
    mfxAllocatorParams* m_pmfxAllocatorParams;


#ifdef D3D_SURFACES_SUPPORT
    IDirect3D9*              m_pd3d;
    IDirect3DDeviceManager9* m_pd3dDeviceManager; 
    IDirect3DDevice9* m_pd3dDevice;
    UINT m_resetToken;
#endif

    
    mfxFrameAllocResponse m_EncResponse;  // memory allocation response for encoder  
    std::vector <mfxFrameSurface1> m_EncSurfaces;

    mfxBitstream m_encodedBs;
    std::vector <mfxU8> m_bsData; // data for encoded bitstream

    //
    mfxU32 m_nFramesProcessed;

    //ctrl always passed to encode frame async , however it is not NULL only for specific scenario frames
    mfxExtAVCRefListCtrl m_avcRefList;
    std::vector<mfxExtBuffer*> m_extBuffers;
    mfxEncodeCtrl m_ctrl, *m_pCtrl;

    struct RefListElement{
        mfxU32      FrameOrder;
        mfxU16      PicStruct;
        mfxU16      ViewId;
        mfxU32      reserved[2];
    } ;

    struct RefListExt
    {
        std::vector<RefListElement> elements;
        mfxU32 nSize; //currently valid items number
        std::basic_string<TCHAR> name;
        RefListExt()
            : nSize()
        {
        }
    };

    std::vector<RefListExt> m_ReferenceLists; // number of frames currently in specific reference list
    bool m_bAvcRefListAttached ;
    
    //extcoding options for instructing encoder to specify maxdecodebuffering=1 
    mfxExtCodingOption m_extCO;
    //
    mfxExtAvcTemporalLayers m_temporalScale;
};