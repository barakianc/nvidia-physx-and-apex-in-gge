//* ////////////////////////////////////////////////////////////////////////////// */
//*
//
//              INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license  agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in  accordance  with the terms of that agreement.
//        Copyright (c) 2005-2011 Intel Corporation. All Rights Reserved.
//
//
//*/
   
#ifndef __PIPELINE_DECODE_H__
#define __PIPELINE_DECODE_H__

#define D3D_SURFACES_SUPPORT

#ifdef D3D_SURFACES_SUPPORT
#pragma warning(disable : 4201)
#include <d3d9.h>
#include <dxva2api.h>
#include "decode_render.h"
#endif

#include "sample_defs.h"
#include "sample_utils.h"
#include "base_allocator.h"

#include "mfxmvc.h"
#include "mfxjpeg.h"
#include "mfxvideo.h"
#include "mfxvideo++.h"

struct sInputParams
{
    mfxU32 videoType;
    bool   bOutput; // if renderer is enabled, possibly no need in output file
    bool   bd3dAlloc; // true - frames in video memory (d3d surfaces),  false - in system memory
    bool   bUseHWLib; // true if application wants to use HW mfx library
    bool   bIsMVC; // true if Multi-View-Codec is in use
    bool   bRendering; // true if d3d rendering is in use
    bool   bCalLatency; // latency calculation for synhronous mode
    mfxU32 nWallCell;
    mfxU32 nWallW;//number of windows located in each row
    mfxU32 nWallH;//number of windows located in each column
    mfxU32 nWallMonitor;//monitor id, 0,1,.. etc
    mfxU32 nWallFPS;//rendering limited by certain fps
    bool   bNoTitle;//whether to show title for each window with fps value
    mfxU32 numViews; // number of views for Multi-View-Codec

    TCHAR  strSrcFile[MSDK_MAX_FILENAME_LEN];
    TCHAR  strDstFile[MSDK_MAX_FILENAME_LEN];
    sInputParams()
    {
        MSDK_ZERO_MEMORY(*this);
    }
};

class CDecodingPipeline
{
public:

    CDecodingPipeline();
    virtual ~CDecodingPipeline();

    virtual mfxStatus Init(sInputParams *pParams);
    virtual mfxStatus RunDecoding();
    virtual void Close();
    virtual mfxStatus ResetDecoder(sInputParams *pParams);
    virtual mfxStatus ResetDevice();

    void SetMultiView();
    void SetExtBuffersFlag()       { m_bIsExtBuffers = true; }
    void SetRenderingFlag()        { m_bIsRender = true; }
    void SetOutputfileFlag(bool b) { m_bOutput = b; }
    virtual void PrintInfo();

protected:
    CSmplYUVWriter          m_FileWriter;
    std::auto_ptr<CSmplBitstreamReader>  m_FileReader;
    mfxU32                  m_nFrameIndex; // index of processed frame
    mfxBitstream            m_mfxBS; // contains encoded data

    MFXVideoSession     m_mfxSession;
    MFXVideoDECODE*     m_pmfxDEC;
    mfxVideoParam       m_mfxVideoParams; 

    mfxExtBuffer** m_ppExtBuffers;
    mfxU16 m_nNumExtBuffers;
    
    MFXFrameAllocator*      m_pMFXAllocator; 
    mfxAllocatorParams*     m_pmfxAllocatorParams;
    bool                    m_bd3dAlloc; // use d3d surfaces
    bool                    m_bExternalAlloc; // use memory allocator as external for Media SDK
    mfxFrameSurface1*       m_pmfxSurfaces; // frames array
    mfxFrameAllocResponse   m_mfxResponse;  // memory allocation response for decoder  

    bool                    m_bIsMVC; // enables MVC mode (need to support several files as an output)
    bool                    m_bIsExtBuffers; // indicates if external buffers were allocated
    bool                    m_bIsRender; // enables rendering mode
    bool                    m_bOutput; // enables/disables output file
    bool                    m_bIsVideoWall;//indicates special mode: decoding will be done in a loop
    bool                    m_bIsCompleteFrame;
    
    IDirect3D9*              m_pd3d;    
    CHWDevice               *m_hwdev;
    IGFXS3DControl          *m_pS3DControl;
    UINT                     m_resetToken;
    
    IDirect3DSurface9*       m_pRenderSurface;
    CDecodeD3DRender         m_d3dRender; 

    virtual mfxStatus InitMfxParams(sInputParams *pParams);

    virtual mfxStatus CreateExtMVCBuffers();
    virtual void DeleteExtBuffers();

    virtual mfxStatus AllocateExtMVCBuffers();
    virtual void    DeallocateExtMVCBuffers();

    virtual void AttachExtParam();

    virtual mfxStatus CreateAllocator();
    virtual mfxStatus CreateHWDevice(); 
    virtual mfxStatus AllocFrames();
    virtual void DeleteFrames();         
    virtual void DeleteAllocator();       
};

#endif // __PIPELINE_DECODE_H__ 