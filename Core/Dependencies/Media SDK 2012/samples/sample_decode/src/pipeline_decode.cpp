//
//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 2005-2011 Intel Corporation. All Rights Reserved.
//

#include <tchar.h>
#include <windows.h>

#include "pipeline_decode.h"
#include "sysmem_allocator.h"
#include "d3d_allocator.h"

#pragma warning(disable : 4100)

mfxStatus CDecodingPipeline::InitMfxParams(sInputParams *pParams)
{
    MSDK_CHECK_POINTER(m_pmfxDEC, MFX_ERR_NULL_PTR);
    mfxStatus sts = MFX_ERR_NONE;
    mfxU32 &numViews = pParams->numViews;

    // try to find a sequence header in the stream
    // if header is not found this function exits with error (e.g. if device was lost and there's no header in the remaining stream)
    for(;;)
    {
        // trying to find PicStruct information in AVI headers
        if ( m_mfxVideoParams.mfx.CodecId == MFX_CODEC_JPEG )
            MJPEG_AVI_ParsePicStruct(&m_mfxBS);

        // parse bit stream and fill mfx params
        sts = m_pmfxDEC->DecodeHeader(&m_mfxBS, &m_mfxVideoParams);

        if (MFX_ERR_MORE_DATA == sts)
        {
            if (m_mfxBS.MaxLength == m_mfxBS.DataLength)
            {
                sts = ExtendMfxBitstream(&m_mfxBS, m_mfxBS.MaxLength * 2); 
                MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
            }
            // read a portion of data             
            sts = m_FileReader->ReadNextFrame(&m_mfxBS);
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

            continue;
        }
        else
        {
            // Enter MVC mode
            if (m_bIsMVC)
            {
                // Check for attached external parameters - if we have them already,
                // we don't need to attach them again
                if (NULL != m_mfxVideoParams.ExtParam)
                    break;

                // Create, attach and allocate external parameters for MVC decoder
                sts = CreateExtMVCBuffers();
                MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

                AttachExtParam();
                sts = m_pmfxDEC->DecodeHeader(&m_mfxBS, &m_mfxVideoParams);

                if (MFX_ERR_NOT_ENOUGH_BUFFER == sts)
                {
                    sts = AllocateExtMVCBuffers();
                    SetExtBuffersFlag();

                    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
                    MSDK_CHECK_POINTER(m_mfxVideoParams.ExtParam, MFX_ERR_MEMORY_ALLOC);
                    continue;
                }
            }

            // if input is interlaced JPEG stream
            if ( m_mfxBS.PicStruct == MFX_PICSTRUCT_FIELD_TFF || m_mfxBS.PicStruct == MFX_PICSTRUCT_FIELD_BFF)
            {
                m_mfxVideoParams.mfx.FrameInfo.CropH *= 2;
                m_mfxVideoParams.mfx.FrameInfo.Height = MSDK_ALIGN16(m_mfxVideoParams.mfx.FrameInfo.CropH);
                m_mfxVideoParams.mfx.FrameInfo.PicStruct = m_mfxBS.PicStruct;
            }

            break;
        }
    }

    // check DecodeHeader status
    MSDK_IGNORE_MFX_STS(sts, MFX_WRN_PARTIAL_ACCELERATION);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // If MVC mode we need to detect number of views in stream
    if (m_bIsMVC)
    {
        mfxExtMVCSeqDesc* pSequenceBuffer;
        pSequenceBuffer = (mfxExtMVCSeqDesc*) m_mfxVideoParams.ExtParam[0];

        mfxU32 i = 0;
        numViews = 0;
        for (i = 0; i < pSequenceBuffer->NumView; ++i)
        {
            /* Some MVC streams can contain different information about
               number of views and view IDs, e.x. numVews = 2 
               and ViewId[0, 1] = 0, 2 instead of ViewId[0, 1] = 0, 1.
               numViews should be equal (max(ViewId[i]) + 1) 
               to prevent crashes during output files writing */
            if (pSequenceBuffer->View[i].ViewId >= numViews)
                numViews = pSequenceBuffer->View[i].ViewId + 1;
        }
    }
    else
    {
        numViews = 1;
    }
    
    // specify memory type 
    m_mfxVideoParams.IOPattern = (mfxU16)(m_bd3dAlloc ? MFX_IOPATTERN_OUT_VIDEO_MEMORY : MFX_IOPATTERN_OUT_SYSTEM_MEMORY);

    //reduce memory usage by allocation less surfaces
    if(m_bIsVideoWall || pParams->bCalLatency)
        m_mfxVideoParams.AsyncDepth = 1;

    return MFX_ERR_NONE;
}

mfxStatus CDecodingPipeline::CreateHWDevice()
{  
    mfxStatus sts = MFX_ERR_NONE;

    HWND window = NULL;
    
    if (!m_bIsRender)
    {
        POINT point = {0, 0};
        window = WindowFromPoint(point);
    }
    else
        window = m_d3dRender.GetWindowHandle();

    m_hwdev = new CD3D9Device();
    if (NULL == m_hwdev)
        return MFX_ERR_MEMORY_ALLOC;
    if (m_bIsRender && m_bIsMVC)
    {
        sts = m_hwdev->SetHandle((mfxHandleType)MFX_HANDLE_GFXS3DCONTROL, m_pS3DControl);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    }
    sts = m_hwdev->Init(
        window, 
        m_bIsMVC ? 2 : 1);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    
    if (m_bIsRender)
        m_d3dRender.SetHWDevice(m_hwdev);    

    return MFX_ERR_NONE;
}

mfxStatus CDecodingPipeline::ResetDevice()
{
    return m_hwdev->Reset();
}

mfxStatus CDecodingPipeline::AllocFrames()
{
    MSDK_CHECK_POINTER(m_pmfxDEC, MFX_ERR_NULL_PTR);

    mfxStatus sts = MFX_ERR_NONE;

    mfxFrameAllocRequest Request;

    mfxU16 nSurfNum = 0; // number of surfaces for decoder

    MSDK_ZERO_MEMORY(Request);

    sts = m_pmfxDEC->Query(&m_mfxVideoParams, &m_mfxVideoParams);
    MSDK_IGNORE_MFX_STS(sts, MFX_WRN_INCOMPATIBLE_VIDEO_PARAM); 
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    // calculate number of surfaces required for decoder
    sts = m_pmfxDEC->QueryIOSurf(&m_mfxVideoParams, &Request);
    MSDK_IGNORE_MFX_STS(sts, MFX_WRN_PARTIAL_ACCELERATION);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    nSurfNum = MSDK_MAX(Request.NumFrameSuggested, 1);

    // prepare allocation request
    Request.NumFrameMin = nSurfNum;
    Request.NumFrameSuggested = nSurfNum;
    memcpy(&(Request.Info), &(m_mfxVideoParams.mfx.FrameInfo), sizeof(mfxFrameInfo));
    Request.Type = MFX_MEMTYPE_EXTERNAL_FRAME | MFX_MEMTYPE_FROM_DECODE; 

    // add info about memory type to request 
    Request.Type |= m_bd3dAlloc ? MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET : MFX_MEMTYPE_SYSTEM_MEMORY; 
    // alloc frames for decoder
    sts = m_pMFXAllocator->Alloc(m_pMFXAllocator->pthis, &Request, &m_mfxResponse);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // prepare mfxFrameSurface1 array for decoder
    nSurfNum = m_mfxResponse.NumFrameActual;
    m_pmfxSurfaces = new mfxFrameSurface1 [nSurfNum];
    MSDK_CHECK_POINTER(m_pmfxSurfaces, MFX_ERR_MEMORY_ALLOC);       

    for (int i = 0; i < nSurfNum; i++)
    {       
        memset(&(m_pmfxSurfaces[i]), 0, sizeof(mfxFrameSurface1));
        memcpy(&(m_pmfxSurfaces[i].Info), &(m_mfxVideoParams.mfx.FrameInfo), sizeof(mfxFrameInfo));

        if (m_bExternalAlloc)
        {
            m_pmfxSurfaces[i].Data.MemId = m_mfxResponse.mids[i];    
        }
        else
        {
            sts = m_pMFXAllocator->Lock(m_pMFXAllocator->pthis, m_mfxResponse.mids[i], &(m_pmfxSurfaces[i].Data));
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        }
    }  

    return MFX_ERR_NONE;
}

mfxStatus CDecodingPipeline::CreateAllocator()
{   
    mfxStatus sts = MFX_ERR_NONE;
 
    if (m_bd3dAlloc)
    {    
        sts = CreateHWDevice();
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

        // provide device manager to MediaSDK
        IDirect3DDeviceManager9 *pd3dDeviceManager = NULL;
        sts = m_hwdev->GetHandle(MFX_HANDLE_D3D9_DEVICE_MANAGER, (mfxHDL*)&pd3dDeviceManager);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        sts = m_mfxSession.SetHandle(MFX_HANDLE_D3D9_DEVICE_MANAGER, pd3dDeviceManager);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
        

        // crate D3D allocator
        m_pMFXAllocator = new D3DFrameAllocator;
        MSDK_CHECK_POINTER(m_pMFXAllocator, MFX_ERR_MEMORY_ALLOC);
        D3DAllocatorParams *pd3dAllocParams = new D3DAllocatorParams;        
        MSDK_CHECK_POINTER(pd3dAllocParams, MFX_ERR_MEMORY_ALLOC);
         
        pd3dAllocParams->pManager = pd3dDeviceManager;
        m_pmfxAllocatorParams = pd3dAllocParams;

        /* In case of video memory we must provide MediaSDK with external allocator 
        thus we demonstrate "external allocator" usage model.
        Call SetAllocator to pass allocator to mediasdk */
        sts = m_mfxSession.SetFrameAllocator(m_pMFXAllocator);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

        m_bExternalAlloc = true;  
    } 
    else
    {        
        // create system memory allocator       
        m_pMFXAllocator = new SysMemFrameAllocator;         
        MSDK_CHECK_POINTER(m_pMFXAllocator, MFX_ERR_MEMORY_ALLOC);

        /* In case of system memory we demonstrate "no external allocator" usage model.  
        We don't call SetAllocator, MediaSDK uses internal allocator. 
        We use system memory allocator simply as a memory manager for application*/           
    }   

    // initialize memory allocator
    sts = m_pMFXAllocator->Init(m_pmfxAllocatorParams);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    
    return MFX_ERR_NONE;
}

void CDecodingPipeline::DeleteFrames()
{
    // delete surfaces array
    MSDK_SAFE_DELETE_ARRAY(m_pmfxSurfaces);    

    // delete frames
    if (m_pMFXAllocator)
    {        
        m_pMFXAllocator->Free(m_pMFXAllocator->pthis, &m_mfxResponse);
    }

    return;
}

void CDecodingPipeline::DeleteAllocator()
{    
    // delete allocator
    MSDK_SAFE_DELETE(m_pMFXAllocator);   
    MSDK_SAFE_DELETE(m_pmfxAllocatorParams);
     
    MSDK_SAFE_DELETE(m_hwdev);
}

CDecodingPipeline::CDecodingPipeline()
{
    m_nFrameIndex = 0;
    m_pmfxDEC = NULL;
    m_pMFXAllocator = NULL;
    m_pmfxAllocatorParams = NULL;
    m_bd3dAlloc = false;
    m_bExternalAlloc = false;
    m_pmfxSurfaces = NULL; 
    m_bIsMVC = false;
    m_bIsExtBuffers = false;
    m_bIsRender = false;
    m_bOutput = true;
    m_bIsVideoWall = false;
    m_ppExtBuffers = NULL;
    m_bIsCompleteFrame = false;
    m_pS3DControl = NULL;

    m_hwdev = NULL;

    MSDK_ZERO_MEMORY(m_mfxVideoParams);
    MSDK_ZERO_MEMORY(m_mfxResponse);
    MSDK_ZERO_MEMORY(m_mfxBS);
}

CDecodingPipeline::~CDecodingPipeline()
{
    Close();
}

void CDecodingPipeline::SetMultiView()
{
    m_FileWriter.SetMultiView();
    m_bIsMVC = true;
}

bool operator < (const IGFX_DISPLAY_MODE &l, const IGFX_DISPLAY_MODE& r)
{
    if (r.ulResWidth >= 0xFFFF || r.ulResHeight >= 0xFFFF || r.ulRefreshRate >= 0xFFFF)
        return false;

         if (l.ulResWidth < r.ulResWidth) return true;
    else if (l.ulResHeight < r.ulResHeight) return true;
    else if (l.ulRefreshRate < r.ulRefreshRate) return true;    
        
    return false;
}
mfxStatus CDecodingPipeline::Init(sInputParams *pParams)
{
    MSDK_CHECK_POINTER(pParams, MFX_ERR_NULL_PTR);

    mfxStatus sts = MFX_ERR_NONE;
    HRESULT hr = S_OK;

    // prepare input stream file reader
    // creating reader that support completeframe mode
    if (pParams->bCalLatency)
    {
        switch (pParams->videoType)
        {
        case MFX_CODEC_AVC:
            m_FileReader.reset(new CH264FrameReader());
            m_bIsCompleteFrame = true;
            break;
        case MFX_CODEC_JPEG:
            m_FileReader.reset(new CJPEGFrameReader());
            m_bIsCompleteFrame = true;
            break;
        default:
            return MFX_ERR_UNSUPPORTED; // latency mode is supported only for H.264 and JPEG codecs
        }
        
    }
    else
    {
        m_FileReader.reset(new CSmplBitstreamReader());
    }

    sts = m_FileReader->Init(pParams->strSrcFile);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
  
    // init session
    mfxIMPL impl = pParams->bUseHWLib ? MFX_IMPL_HARDWARE : MFX_IMPL_SOFTWARE;
    
    // API version
    mfxVersion ver10 = {0, 1};
    mfxVersion ver13 = {3, 1};    
    mfxVersion version;
    version = (m_bIsMVC || pParams->bCalLatency || pParams->videoType == MFX_CODEC_JPEG) ? ver13 : ver10; 

    sts = m_mfxSession.Init(impl, &version);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // create decoder
    m_pmfxDEC = new MFXVideoDECODE(m_mfxSession);
    MSDK_CHECK_POINTER(m_pmfxDEC, MFX_ERR_MEMORY_ALLOC);    

    // set video type in parameters
    m_mfxVideoParams.mfx.CodecId = pParams->videoType; 
    // set memory type
    m_bd3dAlloc = pParams->bd3dAlloc;

    // Initialize rendering window
    if (pParams->bRendering)
    {
        if (m_bIsMVC)
        {
            m_pS3DControl = CreateIGFXS3DControl();
            MSDK_CHECK_POINTER(m_pS3DControl, MFX_ERR_DEVICE_FAILED);

            // check if s3d supported and get a list of supported display modes
            IGFX_S3DCAPS caps;
            MSDK_ZERO_MEMORY(caps);
            hr = m_pS3DControl->GetS3DCaps(&caps);
            if (FAILED(hr) || 0 >= caps.ulNumEntries)
            {
                MSDK_SAFE_DELETE(m_pS3DControl);
                return MFX_ERR_DEVICE_FAILED;
            } 
            
            // switch to 3D mode
            ULONG max = 0;
            MSDK_CHECK_POINTER(caps.S3DSupportedModes, MFX_ERR_NOT_INITIALIZED);
            for (ULONG i = 0; i < caps.ulNumEntries; i++)
            {
                if (caps.S3DSupportedModes[max] < caps.S3DSupportedModes[i]) 
                    max = i;
            }
            
            if (0 == pParams->nWallCell)
            {
                hr = m_pS3DControl->SwitchTo3D(&caps.S3DSupportedModes[max]);
                if (FAILED(hr))
                {
                    MSDK_SAFE_DELETE(m_pS3DControl);
                    return MFX_ERR_DEVICE_FAILED;
                }
            }
        }
        sWindowParams windowParams;

        windowParams.lpWindowName = pParams->bNoTitle ? NULL : _T("sample_decode");
        windowParams.nx           = pParams->nWallW;
        windowParams.ny           = pParams->nWallH;
        windowParams.nWidth       = CW_USEDEFAULT;
        windowParams.nHeight      = CW_USEDEFAULT;        
        windowParams.ncell        = pParams->nWallCell;
        windowParams.nAdapter     = pParams->nWallMonitor;
        windowParams.nMaxFPS      = pParams->nWallFPS;

        windowParams.lpClassName  = _T("Render Window Class");
        windowParams.dwStyle      = WS_OVERLAPPEDWINDOW;
        windowParams.hWndParent   = NULL;
        windowParams.hMenu        = NULL;
        windowParams.hInstance    = GetModuleHandle(NULL);
        windowParams.lpParam      = NULL;
        windowParams.bFullScreen  = FALSE;

        m_d3dRender.Init(windowParams);
        
        SetRenderingFlag();
        //setting videowall flag
        m_bIsVideoWall = 0 != windowParams.nx;
    }

    // prepare bit stream
    sts = InitMfxBitstream(&m_mfxBS, 1024 * 1024);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);    

    sts = InitMfxParams(pParams);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    SetOutputfileFlag(pParams->bOutput);
    if (m_bOutput)
    {
        // prepare YUV file writer
        sts = m_FileWriter.Init(pParams->strDstFile, pParams->numViews);
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    }
  
    // init allocator 
    sts = CreateAllocator();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // if allocator is provided to MediaSDK as external, frames must be allocated prior to decoder initialization
    sts = AllocFrames();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // init decoder
    sts = m_pmfxDEC->Init(&m_mfxVideoParams);
    MSDK_IGNORE_MFX_STS(sts, MFX_WRN_PARTIAL_ACCELERATION);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    return MFX_ERR_NONE;
}

void CDecodingPipeline::AttachExtParam()
{
    m_mfxVideoParams.ExtParam = m_ppExtBuffers;     
    m_mfxVideoParams.NumExtParam = m_nNumExtBuffers; 
}

mfxStatus CDecodingPipeline::CreateExtMVCBuffers()
{
    std::auto_ptr<mfxExtMVCSeqDesc> pExtMVCSeqDesc (new mfxExtMVCSeqDesc()); 
    MSDK_CHECK_POINTER(pExtMVCSeqDesc.get(), MFX_ERR_MEMORY_ALLOC);
    pExtMVCSeqDesc->Header.BufferId = MFX_EXTBUFF_MVC_SEQ_DESC;
    pExtMVCSeqDesc->Header.BufferSz = sizeof(mfxExtMVCSeqDesc);

    m_ppExtBuffers = new mfxExtBuffer* [1]; 
    MSDK_CHECK_POINTER(m_ppExtBuffers, MFX_ERR_MEMORY_ALLOC);
    
    m_ppExtBuffers[0] = (mfxExtBuffer*) pExtMVCSeqDesc.release();
    m_nNumExtBuffers = 1;

    return MFX_ERR_NONE;
}

void CDecodingPipeline::DeleteExtBuffers()
{
    if (m_ppExtBuffers != NULL)
    {
        mfxU32 i;
        for (i = 0; i < m_nNumExtBuffers; ++i)
            if (m_ppExtBuffers[i] != NULL)
                MSDK_SAFE_DELETE(m_ppExtBuffers[i]);
    }

    MSDK_SAFE_DELETE_ARRAY(m_ppExtBuffers);
}

mfxStatus CDecodingPipeline::AllocateExtMVCBuffers()
{
    mfxU32 i;

    mfxExtMVCSeqDesc* pExtMVCBuffer = (mfxExtMVCSeqDesc*) m_mfxVideoParams.ExtParam[0];
    MSDK_CHECK_POINTER(pExtMVCBuffer, MFX_ERR_MEMORY_ALLOC);

    pExtMVCBuffer->View = new mfxMVCViewDependency[pExtMVCBuffer->NumView];
    MSDK_CHECK_POINTER(pExtMVCBuffer->View, MFX_ERR_MEMORY_ALLOC);
    for (i = 0; i < pExtMVCBuffer->NumView; ++i)
    {
        MSDK_ZERO_MEMORY(pExtMVCBuffer->View[i]);
    }
    pExtMVCBuffer->NumViewAlloc = pExtMVCBuffer->NumView;

    pExtMVCBuffer->ViewId = new mfxU16[pExtMVCBuffer->NumViewId];
    MSDK_CHECK_POINTER(pExtMVCBuffer->ViewId, MFX_ERR_MEMORY_ALLOC);
    for (i = 0; i < pExtMVCBuffer->NumViewId; ++i)
    {
        MSDK_ZERO_MEMORY(pExtMVCBuffer->ViewId[i]);
    }
    pExtMVCBuffer->NumViewIdAlloc = pExtMVCBuffer->NumViewId;

    pExtMVCBuffer->OP = new mfxMVCOperationPoint[pExtMVCBuffer->NumOP];
    MSDK_CHECK_POINTER(pExtMVCBuffer->OP, MFX_ERR_MEMORY_ALLOC);
    for (i = 0; i < pExtMVCBuffer->NumOP; ++i)
    {
        MSDK_ZERO_MEMORY(pExtMVCBuffer->OP[i]);
    }
    pExtMVCBuffer->NumOPAlloc = pExtMVCBuffer->NumOP;

    return MFX_ERR_NONE;
}

void CDecodingPipeline::DeallocateExtMVCBuffers()
{
    mfxExtMVCSeqDesc* pExtMVCBuffer = (mfxExtMVCSeqDesc*) m_mfxVideoParams.ExtParam[0];
    if (pExtMVCBuffer != NULL)
    {
        MSDK_SAFE_DELETE_ARRAY(pExtMVCBuffer->View);
        MSDK_SAFE_DELETE_ARRAY(pExtMVCBuffer->ViewId);
        MSDK_SAFE_DELETE_ARRAY(pExtMVCBuffer->OP);
    }

    MSDK_SAFE_DELETE(m_mfxVideoParams.ExtParam[0]);

    m_bIsExtBuffers = false;
}

void CDecodingPipeline::Close()
{
    if (NULL != m_pS3DControl)
    {
        m_pS3DControl->SwitchTo2D(NULL);
        MSDK_SAFE_DELETE(m_pS3DControl);
    }
    WipeMfxBitstream(&m_mfxBS);
    MSDK_SAFE_DELETE(m_pmfxDEC);   

    DeleteFrames();
    
    // allocator if used as external for MediaSDK must be deleted after decoder
    DeleteAllocator();

    if (m_bIsExtBuffers)
    {
        DeallocateExtMVCBuffers();
        DeleteExtBuffers();
    }

    if (m_pRenderSurface != NULL)
        m_pRenderSurface = NULL;

    m_mfxSession.Close();
    m_FileWriter.Close();
    if (m_FileReader.get())
        m_FileReader->Close();

    return;
}

mfxStatus CDecodingPipeline::ResetDecoder(sInputParams *pParams)
{
    mfxStatus sts = MFX_ERR_NONE;    

    // close decoder
    sts = m_pmfxDEC->Close();
    MSDK_IGNORE_MFX_STS(sts, MFX_ERR_NOT_INITIALIZED);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // free allocated frames
    DeleteFrames();
    
    // initialize parameters with values from parsed header 
    sts = InitMfxParams(pParams);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // allocate frames prior to decoder initialization (if allocator used as external)
    sts = AllocFrames();
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // init decoder
    sts = m_pmfxDEC->Init(&m_mfxVideoParams);
    MSDK_IGNORE_MFX_STS(sts, MFX_WRN_PARTIAL_ACCELERATION);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    return MFX_ERR_NONE;
}

mfxStatus CDecodingPipeline::RunDecoding()
{   
    mfxSyncPoint        syncp;
    mfxFrameSurface1    *pmfxOutSurface = NULL;
    mfxStatus           sts = MFX_ERR_NONE;
    mfxU16              nIndex = 0; // index of free surface   
    CTimeInterval<>     decodeTimer(m_bIsCompleteFrame);


    while (MFX_ERR_NONE <= sts || MFX_ERR_MORE_DATA == sts || MFX_ERR_MORE_SURFACE == sts)          
    {
        if (MFX_WRN_DEVICE_BUSY == sts)
        {
            Sleep(1); // just wait and then repeat the same call to DecodeFrameAsync
            if (m_bIsCompleteFrame)
            {
                //in low latency mode device busy lead to increasing of latency, printing is optional since 15 ms s more than threshold
                _tprintf(_T("Warning : latency increased due to MFX_WRN_DEVICE_BUSY\n"));
            }
        }
        else if (MFX_ERR_MORE_DATA == sts || m_bIsCompleteFrame )
        {
            sts = m_FileReader->ReadNextFrame(&m_mfxBS); // read more data to input bit stream

            if (m_bIsCompleteFrame)
            {
                m_mfxBS.TimeStamp = (mfxU64)(decodeTimer.Commit() * 90000);
            }

            //videowall mode: decoding in a loop
            if (MFX_ERR_MORE_DATA == sts && m_bIsVideoWall)
            {
                m_FileReader->Reset();
                sts = MFX_ERR_NONE;
                continue;
            }
            else
            {
                MSDK_BREAK_ON_ERROR(sts);            
            }
        }
        if (MFX_ERR_MORE_SURFACE == sts || MFX_ERR_NONE == sts)
        {
            nIndex = GetFreeSurfaceIndex(m_pmfxSurfaces, m_mfxResponse.NumFrameActual); // find new working surface 
            if (MSDK_INVALID_SURF_IDX == nIndex)
            {
                return MFX_ERR_MEMORY_ALLOC;            
            }
        }
        
        sts = m_pmfxDEC->DecodeFrameAsync(&m_mfxBS, &(m_pmfxSurfaces[nIndex]), &pmfxOutSurface, &syncp);

        // ignore warnings if output is available, 
        // if no output and no action required just repeat the same call
        if (MFX_ERR_NONE < sts && syncp) 
        {
            sts = MFX_ERR_NONE;
        }

        if (MFX_ERR_NONE == sts)
        {
            sts = m_mfxSession.SyncOperation(syncp, MSDK_DEC_WAIT_INTERVAL);             
        }          
        
        if (MFX_ERR_NONE == sts)
        {                
            if (m_bExternalAlloc) 
            {

                bool bRenderStatus = m_bIsRender;

                if (m_bOutput)
                {
                    sts = m_pMFXAllocator->Lock(m_pMFXAllocator->pthis, pmfxOutSurface->Data.MemId, &(pmfxOutSurface->Data));
                    MSDK_BREAK_ON_ERROR(sts);

                    decodeTimer.Commit();

                    sts = m_FileWriter.WriteNextFrame(pmfxOutSurface);
                    MSDK_BREAK_ON_ERROR(sts);

                    sts = m_pMFXAllocator->Unlock(m_pMFXAllocator->pthis, pmfxOutSurface->Data.MemId, &(pmfxOutSurface->Data));
                    MSDK_BREAK_ON_ERROR(sts);
                }

                if (bRenderStatus)
                {
                    sts = m_d3dRender.RenderFrame(pmfxOutSurface, m_pMFXAllocator);
                    if (sts == MFX_ERR_NULL_PTR)
                        sts = MFX_ERR_NONE;
                    MSDK_BREAK_ON_ERROR(sts);
                }

            }
            else 
            {
                decodeTimer.Commit();

                sts = m_FileWriter.WriteNextFrame(pmfxOutSurface);
                MSDK_BREAK_ON_ERROR(sts);
            }            

            if (!m_bIsVideoWall)
            {
                if (m_bIsCompleteFrame)
                {
                    double dStartTime = (mfxF64)pmfxOutSurface->Data.TimeStamp / (mfxF64)90000;
                    _tprintf(_T("Frame %4d, type=?, latency=%5.2lf ms\n"), ++m_nFrameIndex, (decodeTimer.Last() - dStartTime)  * 1000);
                }
                else 
                {
                    //decoding progress
                    _tcprintf(_T("Frame number: %d\r"), ++m_nFrameIndex);
                }
            }
        }

    } //while processing    

    //save the main loop exit status (required for the case of ERR_INCOMPATIBLE_PARAMS)
    mfxStatus mainloop_sts = sts; 

    // means that file has ended, need to go to buffering loop
    MSDK_IGNORE_MFX_STS(sts, MFX_ERR_MORE_DATA);
    // incompatible video parameters detected, 
    //need to go to the buffering loop prior to reset procedure 
    MSDK_IGNORE_MFX_STS(sts, MFX_ERR_INCOMPATIBLE_VIDEO_PARAM); 
    // exit in case of other errors
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);          
      
    // loop to retrieve the buffered decoded frames
    while (MFX_ERR_NONE <= sts || MFX_ERR_MORE_SURFACE == sts)        
    {        
        if (MFX_WRN_DEVICE_BUSY == sts)
        {
            Sleep(5);
        }

        mfxU16 nIndex = GetFreeSurfaceIndex(m_pmfxSurfaces, m_mfxResponse.NumFrameActual);

        if (MSDK_INVALID_SURF_IDX == nIndex)
        {
            return MFX_ERR_MEMORY_ALLOC;            
        }

        sts = m_pmfxDEC->DecodeFrameAsync(NULL, &(m_pmfxSurfaces[nIndex]), &pmfxOutSurface, &syncp);

        // ignore warnings if output is available, 
        // if no output and no action required just repeat the same call        
        if (MFX_ERR_NONE < sts && syncp) 
        {
            sts = MFX_ERR_NONE;
        }

        if (MFX_ERR_NONE == sts)
        {
            sts = m_mfxSession.SyncOperation(syncp, MSDK_DEC_WAIT_INTERVAL);
        }

        if (MFX_ERR_NONE == sts)
        {
            if (m_bExternalAlloc) 
            {

                bool bRenderStatus = m_bIsRender;

                if (m_bOutput)
                {
                    sts = m_pMFXAllocator->Lock(m_pMFXAllocator->pthis, pmfxOutSurface->Data.MemId, &(pmfxOutSurface->Data));
                    MSDK_BREAK_ON_ERROR(sts);

                    sts = m_FileWriter.WriteNextFrame(pmfxOutSurface);
                    MSDK_BREAK_ON_ERROR(sts);

                    sts = m_pMFXAllocator->Unlock(m_pMFXAllocator->pthis, pmfxOutSurface->Data.MemId, &(pmfxOutSurface->Data));
                    MSDK_BREAK_ON_ERROR(sts);
                }

                if (bRenderStatus)
                {
                    sts = m_d3dRender.RenderFrame(pmfxOutSurface, m_pMFXAllocator);
                    if (sts == MFX_ERR_NULL_PTR)
                        sts = MFX_ERR_NONE;
                    MSDK_BREAK_ON_ERROR(sts);
                }

            }
            else 
            {
                sts = m_FileWriter.WriteNextFrame(pmfxOutSurface);
                MSDK_BREAK_ON_ERROR(sts);
            }            

            //decoding progress
            _tcprintf(_T("Frame number: %d\r"), ++m_nFrameIndex);          
        }
    } 

    // MFX_ERR_MORE_DATA is the correct status to exit buffering loop with
    MSDK_IGNORE_MFX_STS(sts, MFX_ERR_MORE_DATA);
    // exit in case of other errors
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    // if we exited main decoding loop with ERR_INCOMPATIBLE_PARAM we need to send this status to caller
    if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == mainloop_sts) 
    {
        sts = mainloop_sts; 
    }

    return sts; // ERR_NONE or ERR_INCOMPATIBLE_VIDEO_PARAM
}

void CDecodingPipeline::PrintInfo()
{       
    _tprintf(_T("Intel(R) Media SDK Decoding Sample Version %s\n\n"), MSDK_SAMPLE_VERSION);
    _tprintf(_T("\nInput video\t%s\n"), CodecIdToStr(m_mfxVideoParams.mfx.CodecId));
    _tprintf(_T("Output format\t%s\n"), _T("YUV420"));

    mfxFrameInfo Info = m_mfxVideoParams.mfx.FrameInfo;
    _tprintf(_T("Resolution\t%dx%d\n"), Info.Width, Info.Height);
    _tprintf(_T("Crop X,Y,W,H\t%d,%d,%d,%d\n"), Info.CropX, Info.CropY, Info.CropW, Info.CropH);

    mfxF64 dFrameRate = CalculateFrameRate(Info.FrameRateExtN, Info.FrameRateExtD);
    _tprintf(_T("Frame rate\t%.2f\n"), dFrameRate);

    TCHAR* sMemType = m_bd3dAlloc ? _T("d3d") : _T("system");
    _tprintf(_T("Memory type\t\t%s\n"), sMemType);

    mfxIMPL impl;
    m_mfxSession.QueryIMPL(&impl);

    TCHAR* sImpl = (MFX_IMPL_HARDWARE == impl) ? _T("hw") : _T("sw");
    _tprintf(_T("MediaSDK impl\t\t%s\n"), sImpl);

    mfxVersion ver;
    m_mfxSession.QueryVersion(&ver);
    _tprintf(_T("MediaSDK version\t%d.%d\n"), ver.Major, ver.Minor);

    _tprintf(_T("\n"));

    return;
}