//
//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright(c) 2005-2011 Intel Corporation. All Rights Reserved.
//
#include "StdAfx.h"
#include <tchar.h>
#include "pipeline_decode.h"
#include <sstream>

void PrintHelp(const TCHAR *strAppName, const TCHAR *strErrorMessage)
{
    _tprintf(_T("Intel(R) Media SDK Decoding Sample Version %s\n\n"), MSDK_SAMPLE_VERSION);

    if (strErrorMessage)
    {
        _tprintf(_T("Error: %s\n"), strErrorMessage);
    } 
    
    _tprintf(_T("Usage: %s mpeg2|h264|vc1|mvc|jpeg -i InputBitstream -o OutputYUVFile\n"), strAppName);
    _tprintf(_T("Options: \n"));
    _tprintf(_T("   [-hw]               - use platform specific SDK implementation, if not specified software implementation is used\n"));
#ifdef D3D_SURFACES_SUPPORT    
    _tprintf(_T("   [-d3d]              - work with d3d surfaces\n"));
    _tprintf(_T("   [-r]                - render decoded data in a separate window \n"));
    _tprintf(_T("   [-wall w h n m f t] - same as -r, and positioned rendering window in a particular cell on specific monitor \n"));
    _tprintf(_T("       w               - number of columns of video windows on selected monitor\n"));
    _tprintf(_T("       h               - number of rows of video windows on selected monitor\n"));
    _tprintf(_T("       n(0,.,w*h-1)    - order of video window in table that will be rendered\n"));
    _tprintf(_T("       m(0,1..)        - monitor id \n"));
    _tprintf(_T("       f               - rendering framerate\n"));
    _tprintf(_T("       t(0/1)          - enable/disable window's title\n"));
#endif
    _tprintf(_T("   [-latency]          - configures decoder for low latency mode and calculates latency during decoding (supported only for H.264 and JPEG codec)\n"));
    _tprintf(_T("\nFeatures: \n"));
    _tprintf(_T("   Press 1 to toggle fullscreen rendering on/off\n"));
    _tprintf(_T("\n"));
}

#define GET_OPTION_POINTER(PTR)        \
{                                      \
    if (2 == _tcslen(strInput[i]))     \
    {                                  \
        i++;                           \
        if (strInput[i][0] == _T('-')) \
        {                              \
            i = i - 1;                 \
        }                              \
        else                           \
        {                              \
            PTR = strInput[i];         \
        }                              \
    }                                  \
    else                               \
    {                                  \
        PTR = strInput[i] + 2;         \
    }                                  \
}

mfxStatus ParseInputString(TCHAR* strInput[], mfxU8 nArgNum, sInputParams* pParams)
{   
    TCHAR* strArgument = _T("");

    if (1 == nArgNum)
    {
        PrintHelp(strInput[0], NULL);
        return MFX_ERR_UNSUPPORTED;
    }

    MSDK_CHECK_POINTER(pParams, MFX_ERR_NULL_PTR);
    

    for (mfxU8 i = 1; i < nArgNum; i++)
    {
        if (_T('-') != strInput[i][0])
        {
            if (0 == _tcscmp(strInput[i], _T("mpeg2")))
            {
                pParams->videoType = MFX_CODEC_MPEG2;
            }
            else if (0 == _tcscmp(strInput[i], _T("h264")))
            {
                pParams->videoType = MFX_CODEC_AVC;
            }
            else if (0 == _tcscmp(strInput[i], _T("vc1")))
            {
                pParams->videoType = MFX_CODEC_VC1;
            }
            else if (0 == _tcscmp(strInput[i], _T("mvc")))
            {
                pParams->videoType = MFX_CODEC_AVC;
                pParams->bIsMVC = true;
            }
            else if (0 == _tcscmp(strInput[i], _T("jpeg")))
            {
                pParams->videoType = MFX_CODEC_JPEG;
            }
            else
            {
                PrintHelp(strInput[0], _T("Unknown codec"));
                return MFX_ERR_UNSUPPORTED;
            }
            continue;
        }

        // multi-character options
        if (0 == _tcscmp(strInput[i], _T("-hw")))
        {
            pParams->bUseHWLib = true;
        }
#ifdef D3D_SURFACES_SUPPORT
        else if (0 == _tcscmp(strInput[i], _T("-d3d")))
        {
            pParams->bd3dAlloc = true;
        }
        else if (0 == _tcscmp(strInput[i], _T("-r")))
        {
            pParams->bd3dAlloc  = true;
            pParams->bRendering = true;
        }
        else if (0 == _tcscmp(strInput[i], _T("-wall")))
        {
            if(i + 6 >= nArgNum)
            {
                PrintHelp(strInput[0], _T("Not enough parameters for -wall key"));
                return MFX_ERR_UNSUPPORTED;
            }
            pParams->bd3dAlloc  = true;
            pParams->bRendering = true;

            _stscanf_s(strInput[++i], _T("%d"), &pParams->nWallW);
            _stscanf_s(strInput[++i], _T("%d"), &pParams->nWallH);
            _stscanf_s(strInput[++i], _T("%d"), &pParams->nWallCell);
            _stscanf_s(strInput[++i], _T("%d"), &pParams->nWallMonitor);
            _stscanf_s(strInput[++i], _T("%d"), &pParams->nWallFPS);
            int nTitle;
            _stscanf_s(strInput[++i], _T("%d"), &nTitle);

            pParams->bNoTitle = 0 == nTitle;
        }
#endif    
        else if (0 == _tcscmp(strInput[i], _T("-latency")))
        {            
            switch (pParams->videoType)
            {
                case MFX_CODEC_AVC:
                case MFX_CODEC_JPEG:
                {
                    pParams->bCalLatency = true;
                    if (!pParams->bIsMVC)               
                        break;
                }
                default:
                {
                     PrintHelp(strInput[0], _T("-latency mode is suppoted only for H.264 and JPEG codecs"));
                     return MFX_ERR_UNSUPPORTED;
                }
            }
        }
        else // 1-character options
        {
            switch (strInput[i][1])
            {
                case _T('i'):
                {
                    GET_OPTION_POINTER(strArgument);
                    _tcscpy_s(pParams->strSrcFile, strArgument);
                    break;
                }
                case _T('o'):
                {
                    GET_OPTION_POINTER(strArgument);
                    pParams->bOutput = true;
                    _tcscpy_s(pParams->strDstFile, strArgument);
                    break;            
                }
                case _T('?'):
                {
                    PrintHelp(strInput[0], NULL);
                    return MFX_ERR_UNSUPPORTED;
                }
                default:
                {
                    std::basic_stringstream<TCHAR> stream;
                    stream<<_T("Unknown option: ")<<strInput[i];
                    PrintHelp(strInput[0], stream.str().c_str());
                    return MFX_ERR_UNSUPPORTED;
                }
            }
        }
    }

    if (0 == _tcslen(pParams->strSrcFile))
    {
        PrintHelp(strInput[0], _T("Source file name not found"));
        return MFX_ERR_UNSUPPORTED;
    }

    if (0 == _tcslen(pParams->strDstFile))
    {
        if (!pParams->bRendering)
        {
            PrintHelp(strInput[0], _T("Destination file name not found"));
            return MFX_ERR_UNSUPPORTED;
        }

        pParams->bOutput = false;
    }

    if (MFX_CODEC_MPEG2 != pParams->videoType && 
        MFX_CODEC_AVC   != pParams->videoType && 
        MFX_CODEC_VC1   != pParams->videoType &&
        MFX_CODEC_JPEG  != pParams->videoType)
    {
        PrintHelp(strInput[0], _T("Unknown codec"));
        return MFX_ERR_UNSUPPORTED;
    }

    return MFX_ERR_NONE;
}

int _tmain(int argc, TCHAR *argv[])
{
    sInputParams        Params;   // input parameters from command line
    CDecodingPipeline   Pipeline; // pipeline for decoding, includes input file reader, decoder and output file writer

    mfxStatus sts = MFX_ERR_NONE; // return value check

    sts = ParseInputString(argv, (mfxU8)argc, &Params);
    MSDK_CHECK_PARSE_RESULT(sts, MFX_ERR_NONE, 1);

    if (Params.bIsMVC)
        Pipeline.SetMultiView();

    sts = Pipeline.Init(&Params);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);   

    // print stream info 
    Pipeline.PrintInfo(); 

    _tprintf(_T("Decoding started\n"));

    for (;;)
    {
        sts = Pipeline.RunDecoding();

        if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == sts || MFX_ERR_DEVICE_LOST == sts || MFX_ERR_DEVICE_FAILED == sts)
        {
            if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == sts)
            {
                _tprintf(_T("\nERROR: Incompatible video parameters detected. Recovering...\n"));
            }
            else
            {
                _tprintf(_T("\nERROR: Hardware device was lost or returned unexpected error. Recovering...\n"));
                sts = Pipeline.ResetDevice();
                MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);
            }           

            sts = Pipeline.ResetDecoder(&Params);
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);            
            continue;
        }        
        else
        {
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);
            break;
        }
    }
    
    _tprintf(_T("\nDecoding finished\n"));

    Pipeline.Close();

    return 0;
}
