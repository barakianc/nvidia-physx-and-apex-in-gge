//* ////////////////////////////////////////////////////////////////////////////// */
//*
//
//              INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license  agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in  accordance  with the terms of that agreement.
//        Copyright (c) 2011 Intel Corporation. All Rights Reserved.
//
//
//*/

#ifndef __PIPELINE_USER_H__
#define __PIPELINE_USER_H__

#include "pipeline_encode.h"
#include "mfx_plugin_base.h"
#include "rotate_plugin_api.h"

/* This class implements the following pipeline: user plugin (frame rotation) -> mfxENCODE */
class CUserPipeline : public CEncodingPipeline
{
public:

    CUserPipeline();
    virtual ~CUserPipeline();

    virtual mfxStatus Init(sInputParams *pParams);
    virtual mfxStatus Run();
    virtual void Close();
    virtual mfxStatus ResetMFXComponents(sInputParams* pParams);    
    virtual void PrintInfo();

protected:        
    HMODULE                 m_PluginModule;
    MFXPluginBase*              m_pusrPlugin;     
    mfxFrameSurface1*       m_pPluginSurfaces; // frames array for rotate input 
    mfxFrameAllocResponse   m_PluginResponse;  // memory allocation response for rotate plugin   
            
    mfxVideoParam           m_pluginVideoParams;
    RotateParam             m_RotateParams;

    virtual mfxStatus InitRotateParam(sInputParams *pParams);
    virtual mfxStatus AllocFrames();
    virtual void DeleteFrames();    
};


#endif // __PIPELINE_USER_H__ 