//
//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 2005-2011 Intel Corporation. All Rights Reserved.
//
#include "StdAfx.h"
#include <tchar.h>
#include "pipeline_encode.h"
#include "pipeline_user.h"

void PrintHelp(TCHAR *strAppName, TCHAR *strErrorMessage)
{
    _tprintf(_T("Intel(R) Media SDK Encoding Sample Version %s\n\n"), MSDK_SAMPLE_VERSION);
    
    if (strErrorMessage)
    {
        _tprintf(_T("Error: %s\n\n"), strErrorMessage);
    }    
    
    _tprintf(_T("Usage: %s h264|mpeg2|mvc [Options] -i InputYUVFile -o OutputEncodedFile -w width -h height\n"), strAppName);    
    _tprintf(_T("Options: \n"));
    _tprintf(_T("   [-nv12] - input is in NV12 color format, if not specified YUV420 is expected\n"));  
    _tprintf(_T("   [-tff|bff] - input stream is interlaced, top|bottom fielf first, if not specified progressive is expected\n"));      
    _tprintf(_T("   [-f frameRate] - video frame rate (frames per second)\n"));
    _tprintf(_T("   [-b bitRate] - encoded bit rate (Kbits per second)\n"));
    _tprintf(_T("   [-u speed|quality|balanced] - target usage\n"));    
    _tprintf(_T("   [-dstw width] - destination picture width, invokes VPP resizing\n")); 
    _tprintf(_T("   [-dsth height] - destination picture height, invokes VPP resizing\n")); 
    _tprintf(_T("   [-hw] - use platform specific SDK implementation, if not specified software implementation is used\n"));
#ifdef D3D_SURFACES_SUPPORT
    _tprintf(_T("   [-d3d] - work with d3d surfaces\n"));
#endif
    // user module options
    _tprintf(_T("User module options: \n"));
    _tprintf(_T("   [-angle 180] - enables 180 degrees picture rotation before encoding, CPU implementation by default.\n"));
    _tprintf(_T("                  Options -tff|bff, -dstw, -dsth, -d3d and MVC output are not supported together with this one\n"));
    _tprintf(_T("   [-opencl] - invokes 180 degrees rotation implementation through Intel(R) OpenCL. This option can be used alone or with -angle 180\n"));
    _tprintf(_T("               Options -tff|bff, -dstw, -dsth, -d3d and MVC output are not supported together with this one\n\n"));
    _tprintf(_T("Example: %s h264|mpeg2 -i InputYUVFile -o OutputEncodedFile -w width -h height -angle 180 -opencl \n"), strAppName);
    _tprintf(_T("Example for MVC: %s mvc -i InputYUVFile_1 -i InputYUVFile_2 -o OutputEncodedFile -w width -h height \n"), strAppName);

    _tprintf(_T("\n"));
}

#define GET_OPTION_POINTER(PTR)         \
{                                       \
    if (2 == _tcslen(strInput[i]))      \
    {                                   \
        i++;                            \
        if (strInput[i][0] == _T('-'))  \
        {                               \
            i = i - 1;                  \
        }                               \
        else                            \
        {                               \
            PTR = strInput[i];          \
        }                               \
    }                                   \
    else                                \
    {                                   \
        PTR = strInput[i] + 2;          \
    }                                   \
}

mfxStatus ParseInputString(TCHAR* strInput[], mfxU8 nArgNum, sInputParams* pParams)
{
    TCHAR* strArgument = _T("");
    TCHAR* stopCharacter;

    if (1 == nArgNum)
    {
        PrintHelp(strInput[0], NULL);
        return MFX_ERR_UNSUPPORTED;
    }

    MSDK_CHECK_POINTER(pParams, MFX_ERR_NULL_PTR);
    MSDK_ZERO_MEMORY(*pParams);
    _tcscpy_s(pParams->strPluginDLLPath, _T("sample_rotate_plugin.dll"));

    // parse command line parameters
    for (mfxU8 i = 1; i < nArgNum; i++)
    {
        MSDK_CHECK_POINTER(strInput[i], MFX_ERR_NULL_PTR);

        if (_T('-') != strInput[i][0])
        {
            if (0 == _tcscmp(strInput[i], _T("h264")))
            {
                pParams->CodecId = MFX_CODEC_AVC;
            }
            else if (0 == _tcscmp(strInput[i], _T("mpeg2")))
            {
                pParams->CodecId = MFX_CODEC_MPEG2;
            }
            else if (0 == _tcscmp(strInput[i], _T("mvc")))
            {
                pParams->CodecId = MFX_CODEC_AVC;
                pParams->bIsMVC = true;
            }
            else
            {
                PrintHelp(strInput[0], _T("Unknown codec"));
                return MFX_ERR_UNSUPPORTED;
            }
            continue;
        }

        // process multi-character options
        if (0 == _tcscmp(strInput[i], _T("-dstw")))
        {
            i++;
            pParams->nDstWidth = (mfxU16)_tcstol(strInput[i], &stopCharacter, 10);
        }
        else if (0 == _tcscmp(strInput[i], _T("-dsth")))
        {
            i++;
            pParams->nDstHeight = (mfxU16)_tcstol(strInput[i], &stopCharacter, 10);
        }
        else if (0 == _tcscmp(strInput[i], _T("-hw")))
        {
            pParams->bUseHWLib = true;
        }
        else if (0 == _tcscmp(strInput[i], _T("-nv12")))
        {
            pParams->ColorFormat = MFX_FOURCC_NV12;
        }
        else if (0 == _tcscmp(strInput[i], _T("-tff")))
        {
            pParams->nPicStruct = MFX_PICSTRUCT_FIELD_TFF;
        }
        else if (0 == _tcscmp(strInput[i], _T("-bff")))
        {
            pParams->nPicStruct = MFX_PICSTRUCT_FIELD_BFF;
        }
        else if (0 == _tcscmp(strInput[i], _T("-angle")))
        {
            i++;           
            pParams->nRotationAngle = (mfxU8)_tcstol(strInput[i], &stopCharacter, 10);
        }
        else if (0 == _tcscmp(strInput[i], _T("-opencl")))
        {            
            _tcscpy_s(pParams->strPluginDLLPath, _T("sample_plugin_opencl.dll")); 
            pParams->nRotationAngle = 180;
        }
#ifdef D3D_SURFACES_SUPPORT
        else if (0 == _tcscmp(strInput[i], _T("-d3d")))
        {
            pParams->bd3dAlloc = true;
        }
#endif
        else // 1-character options
        {
            switch (strInput[i][1])
            {
            case _T('u'):
                GET_OPTION_POINTER(strArgument);
                pParams->nTargetUsage = StrToTargetUsage(strArgument);
                break;
            case _T('w'):
                GET_OPTION_POINTER(strArgument);
                pParams->nWidth = (mfxU16)_tcstol(strArgument, &stopCharacter, 10);
                break;
            case _T('h'):
                GET_OPTION_POINTER(strArgument);
                pParams->nHeight = (mfxU16)_tcstol(strArgument, &stopCharacter, 10);
                break;
            case _T('f'):
                GET_OPTION_POINTER(strArgument);
                pParams->dFrameRate = (mfxF64)_tcstod(strArgument, &stopCharacter);
                break;
            case _T('b'):
                GET_OPTION_POINTER(strArgument);
                pParams->nBitRate = (mfxU16)_tcstol(strArgument, &stopCharacter, 10);
                break;            
            case _T('i'):
                GET_OPTION_POINTER(strArgument);
                _tcscpy_s(pParams->strSrcFile, strArgument);
                if (pParams->bIsMVC)
                {
                    pParams->srcFileBuff.push_back(strArgument);
                }
                break;
            case _T('o'):
                GET_OPTION_POINTER(strArgument);
                _tcscpy_s(pParams->strDstFile, strArgument);
                break;
            case _T('?'):
                PrintHelp(strInput[0], NULL);
                return MFX_ERR_UNSUPPORTED;
            default:
                PrintHelp(strInput[0], _T("Unknown options"));
            }
        }
    }

    // check if all mandatory parameters were set
    if (0 == _tcslen(pParams->strSrcFile))
    {
        PrintHelp(strInput[0], _T("Source file name not found"));
        return MFX_ERR_UNSUPPORTED;
    };

    if (0 == _tcslen(pParams->strDstFile))
    {
        PrintHelp(strInput[0], _T("Destination file name not found"));
        return MFX_ERR_UNSUPPORTED;
    };

    if (0 == pParams->nWidth || 0 == pParams->nHeight)
    {
        PrintHelp(strInput[0], _T("-w, -h must be specified"));
        return MFX_ERR_UNSUPPORTED;
    }

    if (MFX_CODEC_MPEG2 != pParams->CodecId && MFX_CODEC_AVC != pParams->CodecId)
    {
        PrintHelp(strInput[0], _T("Unknown codec"));
        return MFX_ERR_UNSUPPORTED;
    }

    // set default values for optional parameters that were not set or were set incorrectly
    mfxU32 nviews = (mfxU32)pParams->srcFileBuff.size();
    if ((nviews <= 1) || (nviews > 2))
    {
        if (!pParams->bIsMVC)
        {
            pParams->numViews = 1;
        }
        else
        {
            PrintHelp(strInput[0], _T("Only 2 views are supported right now in this sample."));
            return MFX_ERR_UNSUPPORTED;
        }
    }
    else
    {
        pParams->numViews = nviews;
    }
    
    if (MFX_TARGETUSAGE_BEST_QUALITY != pParams->nTargetUsage && MFX_TARGETUSAGE_BEST_SPEED != pParams->nTargetUsage)
    {
        pParams->nTargetUsage = MFX_TARGETUSAGE_BALANCED;
    }
    
    if (pParams->dFrameRate <= 0)
    {
        pParams->dFrameRate = 30;
    }    

    // if no destination picture width or height wasn't specified set it to the source picture size
    if (pParams->nDstWidth == 0)
    {
        pParams->nDstWidth = pParams->nWidth;
    }

    if (pParams->nDstHeight == 0)
    {
        pParams->nDstHeight = pParams->nHeight;
    }

    // calculate default bitrate based on the resolution (a parameter for encoder, so Dst resolution is used)
    if (pParams->nBitRate == 0)
    {        
        pParams->nBitRate = CalculateDefaultBitrate(pParams->CodecId, pParams->nTargetUsage, pParams->nDstWidth,
            pParams->nDstHeight, pParams->dFrameRate);         
    }    

    // if nv12 option wasn't specified we expect input YUV file in YUV420 color format
    if (!pParams->ColorFormat)
    {
        pParams->ColorFormat = MFX_FOURCC_YV12;
    }

    if (!pParams->nPicStruct)
    {
        pParams->nPicStruct = MFX_PICSTRUCT_PROGRESSIVE;
    }

    if (pParams->nRotationAngle != 0 && pParams->nRotationAngle != 180)
    {
        PrintHelp(strInput[0], _T("Angles other than 180 degrees are not supported."));
        return MFX_ERR_UNSUPPORTED; // other than 180 are not supported 
    }  

    // not all options are supported if rotate plugin is enabled
    if (pParams->nRotationAngle == 180 && (
        MFX_PICSTRUCT_PROGRESSIVE != pParams->nPicStruct ||
        pParams->nDstWidth != pParams->nWidth ||
        pParams->nDstHeight != pParams->nHeight ||
        pParams->bd3dAlloc ||
        pParams->bIsMVC)) 
    {        
        PrintHelp(strInput[0], _T("Some of the command line options are not supported with rotation plugin."));
        return MFX_ERR_UNSUPPORTED;
    }

    return MFX_ERR_NONE;
}

int _tmain(int argc, TCHAR *argv[])
{  
    sInputParams Params;   // input parameters from command line
    std::auto_ptr<CEncodingPipeline>  pPipeline;

    mfxStatus sts = MFX_ERR_NONE; // return value check

    sts = ParseInputString(argv, (mfxU8)argc, &Params);
    MSDK_CHECK_PARSE_RESULT(sts, MFX_ERR_NONE, 1);

    pPipeline.reset((Params.nRotationAngle) ? new CUserPipeline : new CEncodingPipeline); 
    MSDK_CHECK_POINTER(pPipeline.get(), MFX_ERR_MEMORY_ALLOC);

    if (Params.bIsMVC)
    {
        pPipeline->SetMultiView();
        pPipeline->SetNumView(Params.numViews);
    }

    sts = pPipeline->Init(&Params);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);   

    pPipeline->PrintInfo();

    _tprintf(_T("Processing started\n"));

    for (;;)
    {
        sts = pPipeline->Run();

        if (MFX_ERR_DEVICE_LOST == sts || MFX_ERR_DEVICE_FAILED == sts)
        {            
            _tprintf(_T("\nERROR: Hardware device was lost or returned an unexpected error. Recovering...\n"));
            sts = pPipeline->ResetDevice();
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);         

            sts = pPipeline->ResetMFXComponents(&Params);
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);
            continue;
        }        
        else
        {            
            MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, 1);
            break;
        }
    }    

    pPipeline->Close();  
    _tprintf(_T("\nProcessing finished\n"));    
    
    return 0;
}