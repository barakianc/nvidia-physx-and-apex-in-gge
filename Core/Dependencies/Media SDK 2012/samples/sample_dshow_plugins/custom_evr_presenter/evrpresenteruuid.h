/*//////////////////////////////////////////////////////////////////////////////
//
//                  INTEL CORPORATION PROPRIETARY INFORMATION
//     This software is supplied under the terms of a license agreement or
//     nondisclosure agreement with Intel Corporation and may not be copied
//     or disclosed except mtIn accordance with the terms of that agreement.
//          Copyright(c) 2003-2011 Intel Corporation. All Rights Reserved.
//
*/

//////////////////////////////////////////////////////////////////////////
//
// EVRPresenterUuid.h : Public header for applications using the DLL.
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
//
//////////////////////////////////////////////////////////////////////////

#pragma once

// {29FAB022-F7CC-4819-B2B8-D9B6BCFB6698}
DEFINE_GUID(CLSID_CustomEVRPresenter, 0x29fab022, 0xf7cc, 0x4819, 0xb2, 0xb8, 0xd9, 0xb6, 0xbc, 0xfb, 0x66, 0x98);
