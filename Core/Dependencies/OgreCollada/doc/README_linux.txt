OgreCollada Plugin
******************

NOTES:
 * Building with SCons (on LINUX) only works for FCollada, OgreCollada and the ColladaViewer.
 
TODO: 
 * Add support for building with SCons under Windows.
 * Add support for building the modified Max and Maya exporters.
 * Add 
 
Prerequisites for building under Linux:
***************************************
 * wxGTK - This implies that the wx-config and gtk-config utilities be present on your system.
 * Ogre - The Ogre engines needs to be installed (www.ogre3d.org)
 
Compiling with SCons:
*********************
Compile the package by typing 'scons' in the project root, i.e., where the SConstruct file resides.

This will build the (dynamic) FCollada library in bin/libFCollada.a and the
(dynamic) OgreCollada plugin as bin/libPlugin_OgreCollada.so.

Compile targets:
----------------
FCollada library: 'fcollada'
OgreCollada Plugin: 'ogrecollada'
ColladaViewer: 'colladaviewer'

To compile a specific compile target simply type:
scons [target]

for example,
scons ogrecollada

Clean a build:
-------------------
To clean a build, type:
scons -c

or

scons [target] -c

NOTE: If no compile target is specified, then *all* targets is assumed to be
included in the command.

Contact:
********
The SCons scripts for this package is maintained by Van Aarde Krynauw (nanocell@gmail.com).

This file was last modified: Jan, 24th 2007.
