/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

#if defined(_CLR)

#include <vcclr.h>

namespace OgreCollada
{
	namespace Managed
	{
		OgreCollada::OgreCollada()
		{
			m_pImpExp = CreateImpExp();
//			m_pListener = new Listener(this);
			//m_pImpExp->setResourceNoticationListener(m_pListener);
		}

		OgreCollada::~OgreCollada()
		{
			DestroyImpExp(m_pImpExp);
//			delete m_pListener;
//			m_pListener = 0;
			m_pImpExp = 0;
		}

		OgreCollada::!OgreCollada()
		{
		}

		// import a COLLADA file (all elements are imported)
		bool OgreCollada::Import(System::String ^ daeFileName)
		{
			if (m_pImpExp)
			{
				pin_ptr<const wchar_t> wch = PtrToStringChars( daeFileName );
				int len = (( daeFileName->Length+1) * 2);
				char* target = new char[ len ];
				wcstombs( target, wch, len );

				Ogre::Root* pRoot = Ogre::Root::getSingletonPtr();
				pin_ptr<Ogre::SceneManager> pSM = (Ogre::SceneManager*)SceneManager;
				m_pImpExp->setSceneManager(pSM);
				m_pImpExp->setRoot(pRoot);
				bool rtn = m_pImpExp->importCollada(target);
				delete [] target;
				return rtn;
			}
			
			return false;
		}

		// import a COLLADA file (only elements specified by the bitmask in flags, are imported)
		bool OgreCollada::Import(System::String ^ daeFileName, ImportFlags flags)
		{
			return true;
		}

		// export a COLLADA file using the name of the current COLLADA file (all elements exported)
		bool OgreCollada::Export()
		{
			return true;
		}

		// export a COLLADA file using the provided filename of the current COLLADA file (all elements exported)
		bool OgreCollada::Export(System::String ^ daeFileName)
		{
			return true;
		}

		// export a COLLADA file using the provided filename of the current COLLADA file (only elements specified by the bitmask in flags are exported)
		bool OgreCollada::Export(System::String ^ daeFileName, ImportFlags flags)
		{
			return true;
		}
	}
}

#endif // _CLR