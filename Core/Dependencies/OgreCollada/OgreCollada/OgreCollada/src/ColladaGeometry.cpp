/*
Copyright - Gamepipe Labs, USC
Author - Vairavan Laxman, Gamepipe Labs, USC
Professor - Prof. Jose Villeta
Date: 10-26-2010
Changelog:
-  Modified to perform Z-buffering and Z-order along with the Painter's 
algorithm to increase stability and to enable loading more complex 
ploygon meshes

-  Loss of polygon error has been solved.

-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

#include "FCDocument/FCDGeometryPolygons.h"
#include "FCDocument/FCDGeometryPolygonsInput.h"
#include "FCDocument/FCDGeometrySource.h"
#include "FCDocument/FCDAsset.h"
#include "FCDocument/FCDGeometryPolygonsTools.h"

#include "OgreDefaultHardwareBufferManager.h"
#include <list>

#include "ColladaBindingManager.h"

// helper lookup tables
//

// vertex data is split up into 4 different buffers 
static OgreCollada::UInt32 lutVertElemToBufferIndex[] = 
{
	-1,	// unused
	0,	// VES_POSITION
	2,	// VES_BLEND_WEIGHTS
	2,	// VES_BLEND_INDICES
	0,	// VES_NORMAL
	0,	// VES_DIFFUSE
	0,	// VES_SPECULAR
	0,	// VES_TEXTURE_COORDINATES
	3,	// VES_BINORMAL
	3,	// VES_TANGENT
};

namespace OgreCollada
{
	// the material-instance vertex bind list is used to remap vertex element sources
	void setupVertexDeclarations(Ogre::VertexDeclaration* pDecl, FCDGeometryPolygonsInput* pInput, size_t* elemSizes, BindingManager* pBindMgr)
	{
		UInt32 set = (UInt32)(pInput->GetSet() > 0 ? pInput->GetSet() : 0);
		
		switch (pInput->GetSemantic())
		{
		// Laxman - Modified for proper loading
		case FUDaeGeometryInput::TEXCOORD:
			{
				// determine how many components are in this texture coord
				Ogre::VertexElementType typ = Ogre::VET_FLOAT2;
				if (pInput->GetSource()->GetStride() == 1)
				{
					typ = Ogre::VET_FLOAT1;
				}
				if (pInput->GetSource()->GetStride() == 3)
				{
					typ = Ogre::VET_FLOAT3;
				}

				const Ogre::VertexElement& elem = 
					pDecl->addElement(
					lutVertElemToBufferIndex[Ogre::VES_TEXTURE_COORDINATES], 
					elemSizes[lutVertElemToBufferIndex[Ogre::VES_TEXTURE_COORDINATES]], 
					typ, 
					Ogre::VES_TEXTURE_COORDINATES, 
					set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_TEXTURE_COORDINATES]] += elem.getSize();
			}
			break;
		// Laxman - Newly added for stablization
		case FUDaeGeometryInput::ZBUFFER_ORDERING:
			{
				const Ogre::VertexElement& elem = 
					pDecl->addElement(
					lutVertElemToBufferIndex[Ogre::VES_TEXTURE_COORDINATES], 
					elemSizes[lutVertElemToBufferIndex[Ogre::VES_TEXTURE_COORDINATES]], 
					Ogre::VET_FLOAT3, 
					Ogre::VES_POSITION, 
					set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_TEXTURE_COORDINATES]] += elem.getSize();
			}
			break;

		case FUDaeGeometryInput::COLOR:
			{
				const Ogre::VertexElement& elem = 
					pDecl->addElement(
					lutVertElemToBufferIndex[Ogre::VES_DIFFUSE], 
					elemSizes[lutVertElemToBufferIndex[Ogre::VES_DIFFUSE]], 
					Ogre::VET_COLOUR_ARGB, // default to D3D vertex color format for now; TODO: fix this with a config option
					Ogre::VES_DIFFUSE, 
					set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_DIFFUSE]] += elem.getSize();
			}
			break;

		case FUDaeGeometryInput::POSITION:
			{
				const Ogre::VertexElement& elem = 
					pDecl->addElement(
						lutVertElemToBufferIndex[Ogre::VES_POSITION], 
						elemSizes[lutVertElemToBufferIndex[Ogre::VES_POSITION]], 
						Ogre::VET_FLOAT3, 
						Ogre::VES_POSITION, 
						set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_POSITION]] += elem.getSize();
			}
			break;

		case FUDaeGeometryInput::TEXBINORMAL:
			{
				const Ogre::VertexElement& elem = 
					pDecl->addElement(
					lutVertElemToBufferIndex[Ogre::VES_BINORMAL], 
					elemSizes[lutVertElemToBufferIndex[Ogre::VES_BINORMAL]], 
					Ogre::VET_FLOAT3, 
					Ogre::VES_BINORMAL, 
					set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_BINORMAL]] += elem.getSize();
			}
			break;

		case FUDaeGeometryInput::NORMAL:
			{
				const Ogre::VertexElement& elem = 
					pDecl->addElement(
						lutVertElemToBufferIndex[Ogre::VES_NORMAL], 
						elemSizes[lutVertElemToBufferIndex[Ogre::VES_NORMAL]], 
						Ogre::VET_FLOAT3, 
						Ogre::VES_NORMAL, 
						set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_NORMAL]] += elem.getSize();
			}
			break;

		case FUDaeGeometryInput::TEXTANGENT:
			{
				const Ogre::VertexElement& elem = 
					pDecl->addElement(
						lutVertElemToBufferIndex[Ogre::VES_TANGENT], 
						elemSizes[lutVertElemToBufferIndex[Ogre::VES_TANGENT]], 
						Ogre::VET_FLOAT3, 
						Ogre::VES_TANGENT, 
						set);

				elemSizes[lutVertElemToBufferIndex[Ogre::VES_TANGENT]] += elem.getSize();
			}
			break;
		}
	}

	typedef std::map<UInt32, UInt32> UniqueIndexMap;

	inline UInt32 getUniqueIndex(uint32* pIndices, size_t stride, UniqueIndexMap& indexMap, UInt32 nextIndex, bool& bIsNew)
	{
		// make a hash of the indices passed into us, so that we can use it in the lookup map
		UInt32 hash = 0;
		for (size_t i=0; i<stride; ++i)
		{
			hash = hash * 131 + pIndices[i];
		}

		UniqueIndexMap::iterator it = indexMap.find(hash);
		if (it != indexMap.end())
		{
			// the particular combination of indices used here already exists -- pass back the 
			// existing index
			bIsNew = false;
			return (UInt32)it->second;
		}

		// we need to add a new index
		indexMap[hash] = nextIndex;
		bIsNew = true;
		return nextIndex;
	}

	// Laxman - Modified from here on
	void fillVertexBuffers(Ogre::SubMesh* pSubMesh, FCDGeometryPolygons* pTris, Ogre::AxisAlignedBox& bbox, Ogre::Real& fBRadius)
	{
		Ogre::VertexData* pVertexData = pSubMesh->vertexData;
		Ogre::IndexData* pIndexData = pSubMesh->indexData;
		Ogre::VertexDeclaration* pDecl = pVertexData->vertexDeclaration;

		// Information for calculating bounds
		Ogre::Vector3 min, max, pos;
        Ogre::Real maxSquaredRadius = -1;
        bool first = true;
		bool bIsNew = false;
		float* pFloat = 0;
		UInt32 currentIndex = -1;
		size_t indexCount = 0;
		size_t inputCount = pTris->GetInputCount();
		UniqueIndexMap indexMap;
		
//		Ogre::VertexDeclaration* zBufferRendererHelper;
		Ogre::Vector3 swapVector;
		Ogre::Vector3 tempVectorToMoveVertices;
		bool firstVertexSet;
		float valueOfM;
		Ogre::Vector3 finalVertex;
		Ogre::Vector3 pointVertex;
		Ogre::Vector3* finalEdges = new Ogre::Vector3();
		Ogre::Real edgesCounter;
		int pointsCounter;
		

		// lock all of the buffers for writing the vertex data -- note that by this time any gaps will have been closed
		UInt8** pBufs = new UInt8*[pDecl->getMaxSource()+1];
		for (UInt16 i=0; i<pDecl->getMaxSource()+1; ++i)
		{
			Ogre::HardwareVertexBufferSharedPtr pBuf = pVertexData->vertexBufferBinding->getBuffer(i);
			pBufs[i] = static_cast<UInt8*>(pBuf->lock(Ogre::HardwareBuffer::HBL_NORMAL));
		}

		// lock the index buffer too
		UInt8* pIdxBuf = static_cast<UInt8*>(pIndexData->indexBuffer->lock(Ogre::HardwareBuffer::HBL_NORMAL));

		// Each input owns its own stream of indices from the <p> element within <triangles>.
		// This means that each input will have indexCount indices into its source data. What this means
		// for us is that in order to figure out if each is a unique vertex or not, we have to pick one
		// index from each stream and assemble the uint32 buffer that we pass to getUniqueVertex.
		indexCount = pTris->GetFaceCount() * 3;
		uint32* currentElement = new uint32[inputCount];

		// now we can start through the indices, checking each element's worth for uniqueness,
		// and filling in buffer data as we go
		for (size_t i=0; i<indexCount; ++i)
		{
			// zero out the element indices
			memset(currentElement, 0, inputCount * sizeof(uint32));

			for (size_t r=0; r<inputCount; ++r)
			{
				FCDGeometryPolygonsInput* pInput = pTris->GetInput(r);
				if (!pInput->OwnsIndices())
					continue;

				currentElement[r] = pInput->GetIndices()[i];
			}

			UInt32 idx = getUniqueIndex(currentElement, inputCount, indexMap, currentIndex+1, bIsNew);
			
			// we can use the index returned and the state of bIsNew to determine whether we need to write
			// new data to the vertex buffers; the index buffer will always have a new index inserted for
			// each element we examine, but vertex data is added only if bIsNew is true.
			if (pIndexData->indexBuffer->getType() == Ogre::HardwareIndexBuffer::IT_16BIT)
			{
				*((UInt16*)(pIdxBuf + (pIndexData->indexBuffer->getIndexSize() * pIndexData->indexCount))) = idx;
			}
			else
			{
				*((UInt32*)(pIdxBuf + (pIndexData->indexBuffer->getIndexSize() * pIndexData->indexCount))) = idx;
			}

			pIndexData->indexCount++;

			// if this is an existing index, we can short-circuit the rest of this 
			if (!bIsNew)
			{
				continue;
			}

			currentIndex = idx;
			bIsNew = false;

			// increment the vertex count in each vertex buffer used
			pVertexData->vertexCount++;

			// at this point we need to cycle through the inputs, grabbing data from the COLLADA sources, indexed by the
			// current values pointed to by pIndices. First we need to grab the up-axis -- Max and Blender export Z-up, where
			// in Ogre Y is "up" by convention; Z-up verts need rotated around the X axis
			const FMVector3& upAxis = pTris->GetDocument()->GetAsset()->GetUpAxis();
			Ogre::Matrix4 rotMat = Ogre::Matrix4::IDENTITY;

			if (upAxis == FMVector3::ZAxis)
			{
				// set up rotation matrix properly
				rotMat[1][1] = 0.f;
				rotMat[1][2] = 1.0f;
				rotMat[2][2] = 0.f;
				rotMat[2][1] = -1.f;
			}

			for (size_t inp=0; inp<inputCount; ++inp)
			{
				FCDGeometryPolygonsInput* pInput = pTris->GetInput(inp);
				FCDGeometrySource* pSource = pInput->GetSource();
				UInt32 set = (UInt32)(pInput->GetSet() > 0 ? pInput->GetSet() : 0);

				switch(pInput->GetSemantic())
				{
				case FUDaeGeometryInput::POSITION:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_POSITION, set);
						UInt16 src = pElem->getSource();
						UInt8* pBuf = pBufs[src];
						pElem->baseVertexPointerToElement(pBuf + currentIndex * pDecl->getVertexSize(src), &pFloat);

						// values are the higher-level constructs (vec3, vec4, etc); data is the raw data (each element in the original 
						// float array) -- easier to use the higher-level construct
						const float* pVal = pSource->GetValue(pInput->GetIndices()[i]);

						pos.x = *pVal++;
						pos.y = *pVal++;
						pos.z = *pVal++;
						//pos = rotMat * pos;
						*pFloat++ = pos.x;
						*pFloat++ = pos.y;
						*pFloat++ = pos.z;

						// push out the opposite corners of this submesh's bounding box
						if (first)
						{
							min = max = pos;
							maxSquaredRadius = pos.squaredLength();
							first = false;
						}
						else
						{
							min.makeFloor(pos);
							max.makeCeil(pos);
							maxSquaredRadius = std::max(pos.squaredLength(), maxSquaredRadius);
						}
					}
					break;

				case FUDaeGeometryInput::NORMAL:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_NORMAL, set);
						UInt16 src = pElem->getSource();
						UInt8* pBuf = pBufs[src];
						pElem->baseVertexPointerToElement(pBuf + currentIndex * pDecl->getVertexSize(src), &pFloat);

						// values are the higher-level constructs (vec3, vec4, etc); data is the raw data (each element in the original 
						// float array) -- easier to use the higher-level construct
						const float* pVal = pSource->GetValue(pInput->GetIndices()[i]);

						Ogre::Vector3 vec;
						vec.x = *pVal++;
						vec.y = *pVal++;
						vec.z = *pVal++;
						//vec = rotMat * vec;
						*pFloat++ = vec.x;
						*pFloat++ = vec.y;
						*pFloat++ = vec.z;
					}
					break;
				
				case FUDaeGeometryInput::ZBUFFER_ORDERING:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_POSITION, set);
						UInt16 srcElement = pElem->getSource();
						UInt8* pBuffer = pBufs[srcElement];
						pElem->baseVertexPointerToElement(pBuffer + currentIndex * pDecl->getVertexSize(srcElement), &pFloat);

						const float* pValue = pSource->GetValue(pInput->GetIndices()[i]);

						pos.x = *pValue++;
						pos.y = *pValue++;
						pos.z = *pValue++;

						*pFloat++ = pos.x;
						*pFloat++ = pos.y;
						*pFloat++ = pos.z;

						if (!first)
						{
							min.makeFloor(pos);
							max.makeCeil(pos);
							maxSquaredRadius = std::max(pos.squaredLength(), maxSquaredRadius);
							firstVertexSet = true;
						}
						else
						{
							min = max = pos;
							maxSquaredRadius = pos.squaredLength();
							first = false;
						}
						
						if(firstVertexSet)
						{
							swapVector.x = pos.x;
							swapVector.y = pos.y;
							swapVector.z = pos.z;
							firstVertexSet = false;
						}
						else
						{
							tempVectorToMoveVertices.x = pos.x;
							tempVectorToMoveVertices.y = pos.y;
							tempVectorToMoveVertices.z = pos.z;
							firstVertexSet = true;
						}

						// Find the value of m
						valueOfM = (tempVectorToMoveVertices.x - swapVector.x) / (tempVectorToMoveVertices.y - swapVector.y);

						// Since we have the value of m, we can now determine any 
						// any value of y
						finalVertex.x = valueOfM * (finalVertex.y - swapVector.y) + swapVector.x;

						// Now we can solve this equation for every integer y value between swapVector.y
						// and tempVectorToMoveVertices.y, but this is extremely slow.  Have to do some
						// performance improvement on this.
						// Since we need only the value of x in each scanline, we can simply the equation
						// by doing the following:
						for(pointsCounter=0; pointsCounter<tempVectorToMoveVertices.y-swapVector.y; pointsCounter++)
						{
							pointVertex.x = valueOfM * ((swapVector.y + pointsCounter) - swapVector.y) + swapVector.x;
						}
						
						// Since the difference between the x values of consecutive y values is m.
						// So since we have a recursive definition of x, i.e. since the current x value is based
						// on the previous x value, we do the following:
						finalVertex.x = pointVertex.x + valueOfM;

						// Convert everything to our 16bit integer
						//finalVertex.x = finalVertex.x << 16;
							
						// Sorted edges
						for(edgesCounter = swapVector.y; edgesCounter<tempVectorToMoveVertices.y; edgesCounter+=1.0)
						{
							//finalEdges[edgesCounter] = finalVertex.x >> 16;
							int index=(int)edgesCounter;
							finalEdges[index] = finalVertex.x;
							finalVertex.x = finalVertex.x + valueOfM;
						}

					}
					break;

				case FUDaeGeometryInput::TEXBINORMAL:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_BINORMAL, set);
						UInt16 src = pElem->getSource();
						UInt8* pBuf = pBufs[src];
						pElem->baseVertexPointerToElement(pBuf + currentIndex * pDecl->getVertexSize(src), &pFloat);

						// values are the higher-level constructs (vec3, vec4, etc); data is the raw data (each element in the original 
						// float array) -- easier to use the higher-level construct
						const float* pVal = pSource->GetValue(pInput->GetIndices()[i]);

						Ogre::Vector3 vec;
						vec.x = *pVal++;
						vec.y = *pVal++;
						vec.z = *pVal++;
						//vec = rotMat * vec;
						*pFloat++ = vec.x;
						*pFloat++ = vec.y;
						*pFloat++ = vec.z;
					}
					break;

				case FUDaeGeometryInput::TEXTANGENT:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_TANGENT, set);
						UInt16 src = pElem->getSource();
						UInt8* pBuf = pBufs[src];
						pElem->baseVertexPointerToElement(pBuf + currentIndex * pDecl->getVertexSize(src), &pFloat);

						// values are the higher-level constructs (vec3, vec4, etc); data is the raw data (each element in the original 
						// float array) -- easier to use the higher-level construct
						const float* pVal = pSource->GetValue(pInput->GetIndices()[i]);

						Ogre::Vector3 vec;
						vec.x = *pVal++;
						vec.y = *pVal++;
						vec.z = *pVal++;
						//vec = rotMat * vec;
						*pFloat++ = vec.x;
						*pFloat++ = vec.y;
						*pFloat++ = vec.z;
					}
					break;

				case FUDaeGeometryInput::TEXCOORD:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_TEXTURE_COORDINATES, set);
						UInt16 src = pElem->getSource();
						UInt8* pBuf = pBufs[src];
						pElem->baseVertexPointerToElement(pBuf + currentIndex * pDecl->getVertexSize(src), &pFloat);

						// values are the higher-level constructs (vec3, vec4, etc); data is the raw data (each element in the original 
						// float array) -- easier to use the higher-level construct
						const float* pVal = pSource->GetValue(pInput->GetIndices()[i]);

						for (size_t j=0; j<pSource->GetStride(); ++j)
						{
							// flip Y texcoord because DCC tools like to put the origin in the LL corner of the sampler
							*pFloat++ = (j==1? 1.f-*pVal++ : *pVal++);
						}
					}
					break;

				case FUDaeGeometryInput::COLOR:
					{
						const Ogre::VertexElement* pElem = pDecl->findElementBySemantic(Ogre::VES_DIFFUSE, set);

						Ogre::ARGB* pCol;
						UInt16 src = pElem->getSource();
						UInt8* pBuf = pBufs[src];
						pElem->baseVertexPointerToElement(pBuf + currentIndex * pDecl->getVertexSize(src), &pCol);

						// values are the higher-level constructs (vec3, vec4, etc); data is the raw data (each element in the original 
						// float array) -- easier to use the higher-level construct
						const float* pVal = pSource->GetValue(pInput->GetIndices()[i]);

						// TODO: add support for GL vertex color format here -- this is the D3D packed format

						// if one component is less than 0, then they all are -- this means the color doesn't actually
						// exist, and we should substitute white for it
						if (*pVal < 0.f)
						{
							*pCol = 0xFFFFFFFF;
						}
						else
						{
							*pCol = 
								((UInt8)(*(pVal+3)*255.f)	<< 24)	// A
							  | ((UInt8)(*(pVal)*255.f)		<< 16)	// R
							  | ((UInt8)(*(pVal+1)*255.f)	<< 8 )	// G
							  | ((UInt8)(*(pVal+2)*255.f)	     );	// B
						}
					}
					break;
				}
			}
		}

		// copy the vertex and index data to a new properly-sized buffer
		for (UInt16 i=0; i<pDecl->getMaxSource()+1; ++i)
		{
			// make a new proper write-only buffer from the properties of the ones we filled
			Ogre::HardwareVertexBufferSharedPtr pOldVB = pVertexData->vertexBufferBinding->getBuffer(i);
			Ogre::HardwareVertexBufferSharedPtr pNewVB = Ogre::HardwareBufferManager::getSingletonPtr()->createVertexBuffer(
				pOldVB->getVertexSize(), pVertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
			
			pOldVB->unlock();
			pNewVB->copyData(*pOldVB, 0, 0, pOldVB->getVertexSize() * pVertexData->vertexCount);

			// swap the buffers; should cause the old one to drop
			pVertexData->vertexBufferBinding->setBinding(i, pNewVB);
		}

		// do the same thing for the index buffer
		pIndexData->indexBuffer->unlock();
		Ogre::HardwareIndexBufferSharedPtr pNewIB = Ogre::HardwareBufferManager::getSingletonPtr()->createIndexBuffer(
			pIndexData->indexBuffer->getType(), pIndexData->indexCount, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
		pNewIB->copyData(*pIndexData->indexBuffer, 0, 0, pIndexData->indexBuffer->getIndexSize() * pIndexData->indexCount);

		// swap index buffers
		pIndexData->indexBuffer.setNull();
		pIndexData->indexBuffer = pNewIB;

		delete [] pBufs;
		delete [] currentElement;

		bbox.setMinimum(min);
		bbox.setMaximum(max);
		fBRadius = maxSquaredRadius;
	}

	void fillSubmesh(
		Ogre::HardwareBufferManager* pHWBMgr, 
		Ogre::SubMesh* pSubMesh, 
		FCDGeometryPolygons* pTris, 
		FCDMaterialInstance* pMaterialInst, 
		Ogre::AxisAlignedBox& bbox, 
		Ogre::Real& fBRadius,
		BindingManager* pBindMgr)
	{

		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, "OgreCollada : Filling new Submesh");
		FUDaeGeometryInput::SemanticList inputOrder;
		inputOrder.push_back(FUDaeGeometryInput::VERTEX);
		inputOrder.push_back(FUDaeGeometryInput::POSITION);
		inputOrder.push_back(FUDaeGeometryInput::NORMAL);
		inputOrder.push_back(FUDaeGeometryInput::COLOR);
		inputOrder.push_back(FUDaeGeometryInput::TEXCOORD);
		inputOrder.push_back(FUDaeGeometryInput::TEXBINORMAL);
		inputOrder.push_back(FUDaeGeometryInput::TEXTANGENT);
		inputOrder.push_back(FUDaeGeometryInput::UV);

		Ogre::Material* pMtl = 0;
		
		if (pMaterialInst)
			pMtl = static_cast<Ogre::Material*>(pMaterialInst->GetMaterial()->GetUserHandle());
		
		if (!pMtl)
		{
			// should never ever get here, but just in case...
			//pSubMesh->setMaterialName(CONVERT_STRING(pMaterialInst->GetSemantic().c_str()));
			pSubMesh->setMaterialName("BaseWhite");
			Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, "OgreCollada : No material found, selecting default");
		}
		else
		{
			// this is the preferred method since a material may have been cloned and a unique
			// name generated
			pSubMesh->setMaterialName(pMtl->getName());
			std::stringstream str;
			str<<"OgreCollada : Material found, instantiating "<<pMtl->getName().c_str();
			Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, str.str());
		}

		// dissect the vertex declaration for this submesh
		pSubMesh->vertexData = new Ogre::VertexData();
		pSubMesh->indexData = new Ogre::IndexData();
		pSubMesh->useSharedVertices = false;
		Ogre::VertexDeclaration* pDecl = pSubMesh->vertexData->vertexDeclaration;

		size_t elemSizes[4] = {0,0,0,0};

		std::stringstream str;
		str<<"OgreCollada : Processing "<<inputOrder.size()<<" inputs";
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, str.str());

		// do inputs in an order that makes fixed-function happy
		for (size_t inpSem = 0; inpSem < inputOrder.size(); ++inpSem)
		{
			FCDGeometryPolygonsInputList inputs;
			pTris->FindInputs(inputOrder[inpSem], inputs);
			for (size_t inp=0; inp<inputs.size(); ++inp)
			{
				setupVertexDeclarations(pDecl, inputs[inp], elemSizes, pBindMgr);
			}
		}

		// now make the vertex buffers. FCollada is kind enough to provide a nice util function
		// to make the unique vertices/indices we need to fill the various buffers
		// NOTE this does not work quite as expected -- it has a serious problem on large data sets (seems
		//     to work fine for small data sets, such as a single model, however). Instead of this, we 
		//     will have to make unique vertex/index data when we fill in the buffers. Since this will
		//     wreak havoc on any blend data (for animation) we still need to maintain an index
		//     mapping for when we hook up blend weights and bone assignments later on.
		//
		//FCDGeometryIndexTranslationMap map;
		//FCDGeometryPolygonsTools::GenerateUniqueIndices(pTris->GetParent(), pTris, &map);

		// we really have no clue how large a buffer we'll need in any of these cases, other than it won't 
		// be larger than the number of triangles times three (worst case, every single vertex in every triangle
		// is unique). We can adjust the sizes down later. Knowing we will, we will let the HWB mgr know about it.
		size_t maxVCount = pTris->GetFaceCount() * 3;

		for (int i=0; i<4; ++i)
		{
			if (elemSizes[i])
			{
				Ogre::HardwareVertexBufferSharedPtr pBuffer = 
					pHWBMgr->createVertexBuffer(elemSizes[i], maxVCount, Ogre::HardwareBuffer::HBU_DYNAMIC);
				pSubMesh->vertexData->vertexBufferBinding->setBinding(i, pBuffer);

			}
		}

		// create index buffer for the submesh -- likewise for the vertex data above, we don't know really
		// how many indices we will end up with, so make a buffer large enough now and we can resize down later
		size_t iCount = maxVCount;
		Ogre::IndexData* pIndexData = pSubMesh->indexData;
		pIndexData->indexBuffer = pHWBMgr->createIndexBuffer(
			iCount > 0x0000FFFF ? Ogre::HardwareIndexBuffer::IT_32BIT : Ogre::HardwareIndexBuffer::IT_16BIT,
			iCount, 
			Ogre::HardwareBuffer::HBU_DYNAMIC
		);

		// in case there were gaps due to unused buffers in our static setup of 4 buffers...
		pSubMesh->vertexData->closeGapsInBindings();

		// fill the new buffers with actual data from the FCDGeometryPolygons...
		fillVertexBuffers(pSubMesh, pTris, bbox, fBRadius);

		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, "OgreCollada : Done filling submesh");
	}

	Ogre::Mesh* ImpExpImpl::createMesh(FCDGeometryInstance* pGeomInst)
	{
		FCDGeometry* pGeo = static_cast<FCDGeometry*>(pGeomInst->GetEntity());
		if (pGeo->GetUserHandle())
			return static_cast<Ogre::Mesh*>(pGeo->GetUserHandle());

//		Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Creating mesh");
		Ogre::MeshPtr pRtn = m_pRoot->getMeshManager()->create(
			CONVERT_STRING(pGeo->GetDaeId().c_str()), 
			m_resGroupName, true);
		printf("									Create Ogre Mesh Pointer(User Handle).\n");

		pGeo->SetUserHandle(pRtn.getPointer());
		printf("									Set up the user handle.\n");

		// pick a hardware buffer manager to use based on what render system is in place; if none exists yet, use
		// the default HW buffer mgr
//		Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Setting hardware buffer manager");
		Ogre::HardwareBufferManager* pHWBMgr = Ogre::HardwareBufferManager::getSingletonPtr();
		if (!pHWBMgr)
		{
			pHWBMgr = new Ogre::DefaultHardwareBufferManager();
		}
		printf("									Get the hardware buffer manager.\n");

		// extract submeshes
		FCDGeometryMesh* pMesh = pGeo->GetMesh();
		// TODO: note, we have BOX_NULL in later Ogre versions
		Ogre::AxisAlignedBox bbox = Ogre::AxisAlignedBox();
		bbox.setNull();
		Ogre::Real fBRadius = 0.f;
		printf("									Get the mesh and process all polygons and materials, count %d.\n", pMesh->GetPolygonsCount());
		for (size_t i=0; i < pMesh->GetPolygonsCount(); ++i)
		{
			printf("										Polygon %d:\n", i);
			if (!pMesh->GetPolygons(i)->IsTriangles())
				continue;
//			Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Processing material instance");
			FCDMaterialInstance* pMaterialInst = pGeomInst->GetMaterialInstance(i);
			if (pMaterialInst)
			{
				// create (if needed) an instance of the material for this submesh				
				printf("											Process Material for this item: %s\n", pMaterialInst->GetName().c_str());
				processMaterialInstance(pMaterialInst);
				
			}
			
			// skip submeshes with no vertices or faces
			printf("											Judge whether I should skip submeshes.\n");
			if (!pMesh->GetPolygons(i)->GetFaceCount() || 
					!pMesh->GetPolygons(i)->GetFaceVertexCount())
			{
				printf("											Skip submeshes with no vertices or faces:\n");
				continue;
			}

			Ogre::SubMesh* pSubMesh = pRtn->createSubMesh();
			fillSubmesh(pHWBMgr, pSubMesh, pMesh->GetPolygons(i), pMaterialInst, bbox, fBRadius, m_pBindingManager);
			printf("											fillSubmeshes.\n");

			// attach this SubMesh to the FCDGeometryPolygons that created it,
			// so that we can get to it again later when we are dealing with possible
			// animation/skinning data
			pMesh->GetPolygons(i)->SetUserHandle(pSubMesh);
			printf("											Set up the submesh as user handle.\n");

			// Set bounds
			const Ogre::AxisAlignedBox& currBox = pRtn->getBounds();
			Ogre::Real currRadius = pRtn->getBoundingSphereRadius();
			if (currBox.isNull())
			{
				pRtn->_setBounds(bbox, false);
				pRtn->_setBoundingSphereRadius(Ogre::Math::Sqrt(fBRadius));
			}
			else
			{
				bbox.merge(currBox);
				pRtn->_setBounds(bbox, false);
				pRtn->_setBoundingSphereRadius(std::max(Ogre::Math::Sqrt(fBRadius), currRadius));
			}
			printf("											Set bounds.\n");
		}

		return pRtn.getPointer();
	}

	bool ImpExpImpl::processGeometryInstance(FCDGeometryInstance* pGeomInst)
	{
//		Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Processing Geometry Instance... ");
		FCDGeometry* pGeo = static_cast<FCDGeometry*>(pGeomInst->GetEntity());
		printf("								Geometry Instance name: %s, url %s.\n", pGeo->GetName().c_str(), pGeo->GetDaeId().c_str());

		// we only deal with triangulated mesh (non-parametric) surfaces
		if (!pGeo->IsMesh())
		{
			// TODO: add warning about non-mesh object during import - DONE
//			Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Non-mesh Object");
			printf("								It is not mesh, exit.\n");
			return false;
		}

		// taken from FCollada code, and altered a bit to logical-or
		bool isTriangles = false;
		printf("								Judge whether they are all triangles.\n");
		for (size_t i = 0; i < pGeo->GetMesh()->GetPolygonsCount(); ++i)
		{
//			Ogre::LogManager::getSingletonPtr()->logMessage("OgreCollada : Polygon edges : " + std::string(ltoa(pGeo->GetMesh()->GetPolygons(i)->TestPolyType(),new char[], 10)) );
			isTriangles |= pGeo->GetMesh()->GetPolygons(i)->IsTriangles();
		}

		// early-out without creating mesh for objects with no triangle-list submeshes
		if (!isTriangles)
		{
			// TODO: add warning about non-triangulated mesh object during import - DONE
//			Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Non-triangulated mesh");
			printf("								Some polygons are not triangles, just quit it.\n");
			return false;
		}
		// import the mesh data into an Ogre Mesh
		printf("								Function create the mesh.\n");
		Ogre::Mesh* pMesh = createMesh(pGeomInst);
//		Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL,"OgreCollada : Imported mesh data into Ogre...");

		std::stringstream str;
		str<<"OgreCollada : Polygon Faces Count = "<<pGeo->GetMesh()->GetFaceCount()<<" ... done... ";

//		Ogre::LogManager::getSingleton().logMessage(Ogre::LML_NORMAL, str.str());

		if (m_pCallback && pMesh)
		{
			printf("								Callback function resourceCreated.\n");
			m_pCallback->resourceCreated(pMesh, pGeomInst->GetEntity());
		}
		
		return true;
	}
}