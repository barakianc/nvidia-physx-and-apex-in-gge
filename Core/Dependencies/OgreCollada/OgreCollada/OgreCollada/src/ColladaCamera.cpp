/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

namespace OgreCollada
{
	Ogre::Camera* ImpExpImpl::processCameraInstance(FCDEntityInstance* pCameraInstance)
	{
		FCDCamera* pCamera = static_cast<FCDCamera*>(pCameraInstance->GetEntity());
		Ogre::Camera* pCam = m_pSceneMgr->createCamera(CONVERT_STRING(pCamera->GetName().c_str()));

		pCam->setAspectRatio(pCamera->GetAspectRatio());
		pCam->setNearClipDistance(pCamera->GetNearZ());
		pCam->setFarClipDistance(pCamera->GetFarZ());
		pCam->setFOVy(Ogre::Radian(Ogre::Degree(pCamera->GetFovY())));

		FCDSceneNode* pNode = pCamera->GetTargetNode();

		if (pNode)
		{
		}

		if (m_pCallback)
			m_pCallback->cameraCreated(pCam, pCamera);

		return pCam;
	}
}
