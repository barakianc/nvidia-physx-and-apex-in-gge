/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

namespace OgreCollada
{
	Ogre::Light* ImpExpImpl::processLightInstance(FCDEntityInstance* pLightInstance)
	{
		FCDLight* pL = static_cast<FCDLight*>(pLightInstance->GetEntity());
		FMVector3& lightColor = pL->GetColor();

		Ogre::Light* pLight = m_pSceneMgr->createLight(CONVERT_STRING(pL->GetName().c_str()));
		pLight->setDiffuseColour(Ogre::ColourValue(lightColor.x, lightColor.y, lightColor.z));

		switch(pL->GetLightType())
		{
		case FCDLight::POINT:
			pLight->setType(Ogre::Light::LT_POINT);
			pLight->setAttenuation(
				1000.f,		// TODO: figure out how to get a real value for this from the data
				pL->GetConstantAttenuationFactor(),
				pL->GetLinearAttenuationFactor() /  pL->GetIntensity(),
				pL->GetQuadraticAttenuationFactor() /  pL->GetIntensity()
				);
			break;

		case FCDLight::SPOT:
			pLight->setType(Ogre::Light::LT_SPOTLIGHT);
			pLight->setSpotlightFalloff(pL->GetFallOffExponent());
			pLight->setSpotlightInnerAngle(Ogre::Degree(pL->GetFallOffAngle()));
			pLight->setSpotlightOuterAngle(Ogre::Degree(pL->GetOuterAngle()));
			pLight->setAttenuation(
				1000.f,		// TODO: figure out how to get a real value for this from the data
				pL->GetConstantAttenuationFactor(),
				pL->GetLinearAttenuationFactor() /  pL->GetIntensity(),
				pL->GetQuadraticAttenuationFactor() /  pL->GetIntensity()
				);
			break;

		case FCDLight::DIRECTIONAL:
			pLight->setType(Ogre::Light::LT_DIRECTIONAL);
			break;
		}

		if (m_pCallback)
			m_pCallback->lightCreated(pLight, pL);

		return pLight;
	}
}
