/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

// We do not need to check for uniqueness of textures -- COLLADA will have sorted 
// that out for us already.
// Since we are creating the texture outside of the normal resource manager path,
// we need to make and instantiate a manual loader for each texture we encounter.
// We can track the loader by its associated texture name, so that when Ogre comes
// calling for a texture to be loaded, we invoke the loader for that texture.


namespace OgreCollada
{
	// extreme fallback texture -- 2x2 hot pink
	static UInt8 s_RGB[] = {128, 0, 255, 128, 0, 255, 128, 0, 255, 128, 0, 255};
	
	class TextureLoader : public Ogre::ManualResourceLoader
	{
	public:
		TextureLoader(const FCDImage* pImage, IResourceNotification* pCallback = 0)
		{
			// extract the pertinent information from the FCDImage
			m_fileName = CONVERT_STRING(pImage->GetFilename().c_str());
			m_pCallback = pCallback;
		}

		// here's where the real magic happens -- when Ogre calls for a texture, we need
		// to get it from disk and fill in the Ogre::Texture resource with it.
		void loadResource(Ogre::Resource* pResource)
		{
			Ogre::Texture* pTex = static_cast<Ogre::Texture*>(pResource);
			Ogre::Image img;

			// extract extension from filename
			size_t pos = m_fileName.find_last_of('.');
			Ogre::String ext = m_fileName.substr(pos+1);

			std::ifstream i;
			i.open(m_fileName.c_str(), std::ios::binary);
			Ogre::DataStreamPtr strm(new Ogre::FileStreamDataStream(&i, false));

			if (!strm->size() || strm->size() == 0xffffffff)
			{
				// call out for the default texture instead
				if (m_pCallback)
				{
					m_pCallback->getDefaultTexture(img);
					assert(img.getWidth() != 0 && img.getHeight() != 0);
					//assert(img.getFormat() != Ogre::PF_UNKNOWN); // GGJ - until Ogre applies my patch to init Image::mFormat...
				}

				// last resort...
				//if (img.getFormat() == Ogre::PF_UNKNOWN) 
				if (img.getWidth() == 0 || img.getHeight() == 0)
				{
					// fall back to our very simple and very hardcoded hot-pink version
					Ogre::DataStreamPtr altStrm(new Ogre::MemoryDataStream(s_RGB, sizeof(s_RGB)));
					img.loadRawData(altStrm, 2, 2, Ogre::PF_R8G8B8);
				}
			}
			else
			{
				// TODO: allow for alternate resource group names
				img.load(strm, ext);
				i.close();
			}

			Ogre::ConstImagePtrList images;
			images.push_back(&img);
			pTex->_loadImages(images);
		}

	private:
		Ogre::String m_fileName;
		IResourceNotification* m_pCallback;
	};

	template <class _V>
	class TextureLoaderList : public std::list<_V>
	{
	public:
		TextureLoaderList() {}
		~TextureLoaderList()
		{
			for (typename std::list<_V>::iterator it = this->begin(); it != this->end(); ++it)
			{
				delete *it;
			}
		}
	};

	// we have this for automatic cleanup of the manual loader pointers on app exit
	static TextureLoaderList<TextureLoader*> loaderList;

	void ImpExpImpl::addTexture(FCDImage* pImage)
	{
		Ogre::TextureManager* pTexMgr = m_pRoot->getTextureManager();
		TextureLoader* pLoader = new TextureLoader(pImage, m_pCallback);
		loaderList.push_back(pLoader);

		Ogre::TexturePtr pTex = pTexMgr->create(
			CONVERT_STRING(pImage->GetDaeId().c_str()), 
			m_resGroupName,
			true,
			pLoader
		);

		pImage->SetUserHandle(pTex.getPointer());

		if (m_pCallback)
			m_pCallback->resourceCreated(pTex.getPointer(), pImage);
	}
}
