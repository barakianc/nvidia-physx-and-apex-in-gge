/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "FCDocument/FCDAnimationClip.h"
#include "FCDocument/FCDAnimation.h"
#include "FCDocument/FCDAnimationCurve.h"
#include "FCDocument/FCDAnimated.h"
#include "FCDocument/FCDAsset.h"
#include "FCDocument/FCDAnimationChannel.h"
#include "FCDocument/FCDSceneNodeTools.h"

using namespace OgreCollada;

typedef struct AnimClipData
{
	std::string		name;
	float			start;
	float			end;

	bool operator==(AnimClipData& other)
	{
		//return (other.name == name) && (other.start == start) && (other.end == end); 
		return (other.name == name); 
	}

	bool operator!=(AnimClipData& other)
	{
		return (other.name != name); 
	}

	bool operator<(AnimClipData& other)
	{
		return (other.name < name); 
	}

} AnimClipData;

void findAnimationClipNames(std::list<AnimClipData>& animNames, std::list<FCDAnimated*> animateds)
{
	animNames.clear();

	for (std::list<FCDAnimated*>::iterator it = animateds.begin(); it != animateds.end(); ++it)
	{
		FCDAnimated* pAnimated = *it;

		FCDAnimationCurveListList& curves = pAnimated->GetCurves();
		for (FCDAnimationCurveListList::iterator curve = curves.begin(); 
			curve != curves.end(); ++curve)
		{
			for (FCDAnimationCurveTrackList::iterator track = curve->begin();
				track != curve->end(); ++track)
			{
				FCDAnimationCurve* pCurve = *track;
				size_t clips = pCurve->GetClipCount();
				for (size_t i=0; i<clips; ++i)
				{
					FCDAnimationClip* pClip = pCurve->GetClip(i);

					AnimClipData data;
					data.name = pClip->GetName().c_str();
					data.start = pClip->GetStart();
					data.end = pClip->GetEnd();
					animNames.push_back(data);
				}

				// break out because all components of this curve are in the same major value
				//break;
			}
		}
	}

	animNames.sort();
	animNames.unique();
}

void ImpExpImpl::addAnimationTrack(
	Ogre::Animation*	__restrict	pAnim, 
	float							startTime, 
	Ogre::Node*			__restrict	pNode, 
	FCDSceneNode*		__restrict	pFCNode, 
	bool							bRelative
)
{
	// let FCollada figure out how to assemble all of the node's transform curves into 
	// 4x4 matrices
	FCDSceneNodeTools::ClearSampledAnimation();
	FCDSceneNodeTools::GenerateSampledAnimation(pFCNode);
	const FloatList& keyTimes = FCDSceneNodeTools::GetSampledAnimationKeys();
	const FMMatrix44List& keyTransforms = FCDSceneNodeTools::GetSampledAnimationMatrices();

	if (!keyTimes.size())
		return;

	// if there are keys, make a track for this node in the animation
	Ogre::NodeAnimationTrack* pTrack = pAnim->createNodeTrack(pAnim->getNumNodeTracks(), pNode);

	// extract the input node's transform bits into a 4x4 transform
	FMMatrix44 baseXform;
	bool bHaveBaseXform = false;

	// for each keytime, 
	size_t keyCount = keyTimes.size();
	float animLength = pAnim->getLength();

	for (size_t i=0; i<keyCount; ++i)
	{
		float time = keyTimes[i];

		// ignore keys/values outside the time bounds for this anim
		if (time < startTime || time > (startTime + animLength))
			continue;

		const FMMatrix44& xform = keyTransforms[i];

		if (!bHaveBaseXform)
		{
			// mark this as the first animation in the track
			baseXform = xform;
		}

		const FMQuaternion q(FMQuaternion::MatrixRotationQuaternion(xform));
		const FMVector3 p(xform.GetTranslation());

		// create a node transform keyframe for this time, with this xform
		Ogre::TransformKeyFrame* pKF = pTrack->createNodeKeyFrame(time - startTime);

		if (!bRelative)
		{
			pKF->setRotation(Ogre::Quaternion(q.w, q.x, q.y, q.z));
			pKF->setTranslate(Ogre::Vector3(p.x, p.y, p.z));
		}
		else
		{
			//pKF->setRotation(mat.extractQuaternion().Inverse() * baseXform.extractQuaternion());
			//pKF->setTranslate(mat.getTrans() - baseXform.getTrans());
		}

		FMVector3 scale, r, t;
		float inv;
		xform.Decompose(scale, r, t, inv);

		pKF->setScale(Ogre::Vector3(scale.x, scale.y, scale.z));
		bHaveBaseXform = true;
	}
}

void findAnimateds(std::list<FCDAnimated*>& animateds, FCDSceneNode* pFCNode)
{
	animateds.clear();
	size_t xformCount = pFCNode->GetTransformCount();
	for (size_t i=0; i<xformCount; ++i)
	{
		FCDTransform* pXform = pFCNode->GetTransform(i);
		FCDAnimated* pAnimated = pXform->GetAnimated();

		if (pAnimated)
			animateds.push_back(pAnimated);
	}
}

// "animateds" can be more than one "track" (i.e. control scale and rotation, or
// just X and Y of position, and so on). Each of these sets of tracks control the 
// transform for one bone or node, so they need to be coalesced into a usable Ogre
// Matrix4 for creation of NodeTransform animation tracks.

// The bone passed in can be used to look up the skeleton containing the bone; animations
// need to be created on that skeleton. The animation will be named after the clip name.
void ImpExpImpl::createSkeletonAnimationTracks(Ogre::Bone *pBone, FCDSceneNode* pFCNode)
{
	// gather up all of the animated for this FCD scene node
	std::list<FCDAnimated*> animateds;
	findAnimateds(animateds, pFCNode);

	// find clip names that reference the animateds; these names will be used to look
	// up animations in the skeleton
	std::list<AnimClipData> animClips;
	findAnimationClipNames(animClips, animateds);

	// if no clips were found, exit -- skeletal animation is required to have named clips
	if (!animClips.size())
		return;

	Ogre::Skeleton* pSkeleton = 0;
	std::map<Ogre::Bone*, Ogre::Skeleton*>::iterator it = m_boneToSkeletonLUT.find(pBone);
	if (it == m_boneToSkeletonLUT.end())
		return;
	else
		pSkeleton = it->second;

	for (std::list<AnimClipData>::iterator it = animClips.begin(); 
		it != animClips.end(); ++it)
	{
		Ogre::Animation* pAnim = 0;
		try
		{
			pAnim = pSkeleton->getAnimation(it->name);
		}
		catch(Ogre::ItemIdentityException e)
		{
			pAnim = pSkeleton->createAnimation(it->name, it->end - it->start);
		}

		addAnimationTrack(pAnim, it->start, pBone, pFCNode, true);
	}
}

// normal scene node animation tracks should be collected under the clip controlling
// the animation; all animation instances under the clip will fall under a single 
// Ogre Animation, named after the clip. Animations that do not have a referencing clip 
// will create their own Animation, with just the single track defined by the contents 
// of the animateds parameter, and named after the node being controlled. 
void ImpExpImpl::createNodeAnimationTracks(Ogre::Node *pNode, FCDSceneNode* pFCNode)
{
	// gather up all of the animated for this FCD scene node
	std::list<FCDAnimated*> animateds;
	findAnimateds(animateds, pFCNode);

	// find clip names that reference the animateds; these names will be used to look
	// up animations in the skeleton
	std::list<AnimClipData> animClips;
	findAnimationClipNames(animClips, animateds);

	// if no named clips were found, use the name of the node as the anim name
	std::string animName(pNode->getName());

	if (!animClips.size())
	{
		// TODO: figure out a good way to get the span of time for the contained keys
		//std::cout << "[ColladaAnimation::createNodeAnimationTracks] animClips.size() == 0" << std::endl;

		// treat as one animation, full length of its contained keys
		//Ogre::Animation* pAnim = 0;
		//try
		//{
		//	pAnim = m_pSceneMgr->getAnimation(animName);
		//}
		//catch(Ogre::ItemIdentityException e)
		//{
		//	pAnim = m_pSceneMgr->createAnimation(animName, it->end - it->start);
		//}

		//addAnimationTrack(pAnim, it->start, pNode, pFCNode);
	}
	else
	{
		for (std::list<AnimClipData>::iterator it = animClips.begin(); 
			it != animClips.end(); ++it)
		{
			Ogre::Animation* pAnim = 0;
			try
			{
				pAnim = m_pSceneMgr->getAnimation(it->name);
			}
			catch(Ogre::ItemIdentityException e)
			{
				pAnim = m_pSceneMgr->createAnimation(it->name, it->end - it->start);
			}

			addAnimationTrack(pAnim, it->start, pNode, pFCNode, false);
		}
	}
}

void ImpExpImpl::createAnimationTracks(Ogre::Node* pNode, FCDSceneNode* pFCNode)
{
	Ogre::Bone* pBone = dynamic_cast<Ogre::Bone*>(pNode);
	if (pBone)
	{
		createSkeletonAnimationTracks(pBone, pFCNode);
	}
	else
	{
		createNodeAnimationTracks(pNode, pFCNode);
	}
}
