/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/


#include "stdafx.h"

// we need to examine each effect for GPU program definitions, and create Ogre GPU programs
// from them when we encounter those. These programs will be used later when Ogre Materials
// are assembled from the COLLADA material library. Everything that is not pertinent to GPU
// programs and their parameters/inputs, will be ignored here. The other effect properties 
// will be examined when the Ogre Material is created.

#include "FCDocument/FCDEffectProfileFX.h"
#include "FCDocument/FCDEffectCode.h"
#include "FCDocument/FCDEffectTechnique.h"
#include "FCDocument/FCDEffectParameter.h"
#include "FCDocument/FCDEffectParameterSampler.h"
#include "FCDocument/FCDEffectParameterSurface.h"
#include "FCDocument/FCDEffectPass.h"
#include "FCDocument/FCDEffectPassShader.h"
#include "FCDocument/FCDEffectTools.h"

namespace OgreCollada
{
	size_t getAutoParamExtra(const std::string& ann)
	{
		size_t rtn = 0;
		size_t pos = ann.find_last_of('/');

		if (pos != std::string::npos)
		{
			rtn = Ogre::StringConverter::parseUnsignedInt(ann.substr(pos+1));
		}

		return rtn;
	}

	size_t getLightNum(const std::string& ann)
	{
		size_t rtn = 0;
		size_t pos = ann.find_last_of('_');

		if (pos != std::string::npos)
		{
			rtn = Ogre::StringConverter::parseUnsignedInt(ann.substr(pos+1));
		}
 
		return rtn;
	}

  void ImpExpImpl::bindParameters(FCDEffectPassShader* pProgram, Ogre::Pass* pPass, Ogre::GpuProgramParametersSharedPtr pCgParams)
	{ 
		Ogre::LogManager::getSingletonPtr()->logMessage("OgreCollada : Binding Parameters, auto constant count" + std::string(ltoa((int)pCgParams->getAutoConstantCount(),new char[],10)));
		// we want to override any effect definitions with the defs from the material that instanced us
		size_t bindingCount = pProgram->GetBindingCount();
		for (size_t binding=0; binding<bindingCount; ++binding)
		{
			FCDEffectPassBind* pBind = pProgram->GetBinding(binding);
			FCDEffectParameter* pGenParam = FCDEffectTools::FindEffectParameterByReference(pProgram->GetParent()->GetParent()->GetParent(), pBind->reference->c_str());
			FCDEffectParameter* pParam = FCDEffectTools::FindEffectParameterByReference(m_pCurrentMtlInst, pBind->reference->c_str());

			//-- FCDEffectPassShader includes all bindings, we need to ensure if the binding actually exists for the shader
		//TODO : Ensure the included files are actually included! Otherwise module will crash! - DONE
			if(!pCgParams->hasNamedParameters())
			{
				Ogre::LogManager::getSingletonPtr()->logMessage("OgreCollada : Binding parameters failed... check if shader header files exist in media path");
				return;
			}
			Ogre::GpuConstantDefinitionIterator it = pCgParams->getConstantDefinitionIterator(); 
			while((pParam->GetType()!=FCDEffectParameter::SAMPLER)&&(it.hasMoreElements() && strcmp(pBind->symbol->c_str(),it.peekNextKey().c_str())!= 0)) it.moveNext();
			if(!it.hasMoreElements()) continue;
			//-- End check

			if (!pParam || !pGenParam)
				continue;

			if (pParam->GetType() == FCDEffectParameter::SAMPLER)
			{
				FCDEffectParameterSampler* pSampler = static_cast<FCDEffectParameterSampler*>(pParam);
				FCDEffectParameterSurface* pSurface = pSampler->GetSurface();
				StringList& names = pSurface->GetNames();

				size_t imgCount = pSurface->GetImageCount();
				for (size_t i=0; i<imgCount; ++i)
				{
					FCDImage* pImage = pSurface->GetImage(i);
					if (!pImage->GetUserHandle())
					{
						addTexture(pImage);
					}
				}

				// create a texture unit state in this pass
				Ogre::TextureUnitState* pTUS = pPass->createTextureUnitState(pBind->symbol->c_str());
				pTUS->setName(pBind->symbol->c_str());

				switch(pSampler->GetSamplerType())
				{
				case FCDEffectParameterSampler::SAMPLER1D:
					pTUS->setTextureName(names[0].c_str(), Ogre::TEX_TYPE_1D);
					break;
				case FCDEffectParameterSampler::SAMPLER2D:
					pTUS->setTextureName(names[0].c_str());
					break;
				case FCDEffectParameterSampler::SAMPLER3D:
					pTUS->setTextureName(names[0].c_str(), Ogre::TEX_TYPE_3D);
					break;
				case FCDEffectParameterSampler::SAMPLERCUBE:
					{
						Ogre::String pNames[6];
						for (size_t i=0; i<names.size(); ++i)
						{
							pNames[i] = names[i].c_str();
						}

						pTUS->setCubicTextureName(pNames);
					}
					break;
				}
				continue;
			}

      std::string semantic = pGenParam->GetSemantic();
      std::string paramName(pBind->symbol->c_str());
      if(semantic == "WorldInverseTranspose")
      {
        std::string val("inverse_transpose_world_matrix");
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val.substr(0, val.find_last_of('/')))->acType,
          getAutoParamExtra(val)
        );
      }
      else if(semantic == "WorldViewProjection")
      {
        std::string val("worldviewproj_matrix");
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val.substr(0, val.find_last_of('/')))->acType,
          getAutoParamExtra(val)
        );
      }
      else if(semantic == "World")
      {
        std::string val("world_matrix");
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val.substr(0, val.find_last_of('/')))->acType,
          getAutoParamExtra(val)
        );
      }
      else if(semantic == "ViewInverse")
      {
        std::string val("inverse_view_matrix");
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val.substr(0, val.find_last_of('/')))->acType,
          getAutoParamExtra(val)
        );
      } 
      else if(semantic == "AMBIENT")
      {
        std::string val("ambient_light_colour");
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val.substr(0, val.find_last_of('/')))->acType,
          getAutoParamExtra(val)
        );
      } 
      else if( "LampPos" == paramName.substr( 0, paramName.find_last_of('_') ) )
      {
        std::string val("light_position");
        size_t lightNum = getLightNum(paramName);
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val)->acType,
          lightNum
        );
      } 
      else if( "LampSpec" == paramName.substr( 0, paramName.find_last_of('_') ) )
      {
        std::string val("light_specular_colour");
        size_t lightNum = getLightNum(paramName);
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val)->acType,
          lightNum
        );
      } 
      else if( "LampDiff" == paramName.substr( 0, paramName.find_last_of('_') ) )
      {
        std::string val("light_diffuse_colour");
        size_t lightNum = getLightNum(paramName);
        pCgParams->setNamedAutoConstant(
          paramName,
          pCgParams->getAutoConstantDefinition(val)->acType,
          lightNum
        );
      } 
      else {}

			size_t annotations = pGenParam->GetAnnotationCount();
			for (size_t i=0; i<annotations; ++i)
			{
				FCDEffectParameterAnnotation* pAnn = pGenParam->GetAnnotation(i);
				std::string ann(pAnn->name->c_str());

				if (ann == "param_named_auto")
				{
					std::string val(pAnn->value->c_str());
					std::string paramName(pBind->symbol->c_str());
					pCgParams->setNamedAutoConstant(
						paramName,
						pCgParams->getAutoConstantDefinition(val.substr(0, val.find_last_of('/')))->acType,
						getAutoParamExtra(val)
					);

					break;
				}

				FCDEffectParameter::Type typ = pGenParam->GetType();
				if (ann == "param_named")
				{
					std::string bindingName(pBind->symbol->c_str());

					switch(typ)
					{
					case FCDEffectParameter::FLOAT:
						{
							FCDEffectParameterFloat* pFloat = static_cast<FCDEffectParameterFloat*>(pParam);
							pCgParams->setNamedConstant(bindingName, pFloat->GetValue());
						}
						break;
					case FCDEffectParameter::INTEGER:
						{
							FCDEffectParameterInt* pInt = static_cast<FCDEffectParameterInt*>(pParam);
							pCgParams->setNamedConstant(bindingName, (int) (pInt->GetValue()));
						}
						break;
					case FCDEffectParameter::VECTOR:
						{
							FCDEffectParameterVector* pVec = static_cast<FCDEffectParameterVector*>(pParam);
							pCgParams->setNamedConstant(bindingName, 
								Ogre::Vector4(
									pVec->GetValue()->x,
									pVec->GetValue()->y,
									pVec->GetValue()->z,
									pVec->GetValue()->w
							));
						}
						break;
					case FCDEffectParameter::FLOAT3:
						{
							FCDEffectParameterFloat3* pVec = static_cast<FCDEffectParameterFloat3*>(pParam);
							pCgParams->setNamedConstant(bindingName, 
								Ogre::Vector3(
									pVec->GetValue()->x,
									pVec->GetValue()->y,
									pVec->GetValue()->z
							));
						}
						break;
					//case FCDEffectParameter::FLOAT2:
					//	pCgParams->setNamedConstant(pParam->GetReference().c_str(), Ogre::StringConverter::parseVector2(pAnn->value.c_str()));
					//	break;
					case FCDEffectParameter::MATRIX:
						{
							FCDEffectParameterMatrix* pMat = static_cast<FCDEffectParameterMatrix*>(pParam);
							FMMatrix44& mat = pMat->GetValue();
							pCgParams->setNamedConstant(bindingName,
								Ogre::Matrix4(
									mat[0][0], mat[0][1], mat[0][2], mat[0][3],
									mat[1][0], mat[1][1], mat[1][2], mat[1][3],
									mat[2][0], mat[2][1], mat[2][2], mat[2][3],
									mat[3][0], mat[3][1], mat[3][2], mat[3][3]
								));
						}
						break;
					}

					break;
				}
			}
		}
	}

	void ImpExpImpl::addEffect(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile, const std::string& preferredTechnique)
	{
		printf("																			Input: Ogre::Material: %s, FCDEffectProfile Parametor Count: %d, preferredTechnique: %s.\n", pMtl.getPointer()->getName().c_str(), pProfile->GetEffectParameterCount(), preferredTechnique.c_str());
		/*	Output input parameters: Begin*/
		int profileParameterCount = (int)pProfile->GetEffectParameterCount();

		printf("																			Output Parameter Information:\n");
		for(int i=0;i<profileParameterCount;i++)
		{
			FCDEffectParameter *fEffPa=pProfile->GetEffectParameter(i);
			if(!fEffPa)
			{
				continue;
			}

			//Output type
			FCDEffectParameter::Type paType = fEffPa->GetType();
			FMVector3 v3;
			FMVector4 v4;
			FMMatrix44 v44;
			FCDEffectParameterFloat3 *paFloat3;
			FCDEffectParameterMatrix *paMatrix;
			FCDEffectParameterVector *paVector;

			switch(paType)
			{
			case FCDEffectParameter::BOOLEAN:
				break;
			case FCDEffectParameter::FLOAT:				
				break;
			case FCDEffectParameter::FLOAT2:				
				break;
			case FCDEffectParameter::FLOAT3:
				paFloat3=(FCDEffectParameterFloat3*)fEffPa;
				v3=paFloat3->GetValue();
				printf("																				Type Float3, Value: %f, %f, %f.\n", v3[0], v3[1], v3[2]);
				break;
			case FCDEffectParameter::INTEGER:				
				break;
			case FCDEffectParameter::MATRIX:
				paMatrix=(FCDEffectParameterMatrix*)fEffPa;
				v44=paMatrix->GetValue();
				printf("																				Type Matrix, Value %f, %f, %f, %f,      %f, %f, %f, %f,      %f, %f, %f, %f,      %f, %f, %f, %f.\n", v44[0][0], v44[0][1], v44[0][2], v44[0][3], v44[1][0], v44[1][1], v44[1][2], v44[1][3], v44[2][0], v44[2][1], v44[2][2], v44[2][3], v44[3][0], v44[3][1], v44[3][2], v44[3][3]);
				break;
			case FCDEffectParameter::SAMPLER:				
				break;
			case FCDEffectParameter::STRING:				
				break;
			case FCDEffectParameter::SURFACE:				
				break;

			case FCDEffectParameter::VECTOR:
				paVector = (FCDEffectParameterVector*)fEffPa;
				v4=paVector->GetValue();
				printf("																				Type Vector, Value: %f, %f, %f, %f,\n", v4[0], v4[1], v4[2], v4[3]);
				break;
			}
		}
		/* Output input parameters: End */

		FCDEffectProfileFX* pFX = static_cast<FCDEffectProfileFX*>(pProfile);
		size_t techCount = pFX->GetTechniqueCount();
		Ogre::HighLevelGpuProgramManager* pMgr = Ogre::HighLevelGpuProgramManager::getSingletonPtr();

		/* Output input parameters: Begin */
		printf("																			Create High Level Gpu Programmer Manager Class.\n");
		printf("																			Iterate Technique: count: %d.\n", techCount);
		/* Output input parameters: End */
		
		for (size_t techIdx=0; techIdx<techCount; ++techIdx)
		{
			FCDEffectTechnique* pTech = pFX->GetTechnique(techIdx);
			std::string techName(pTech->GetName().c_str());

			/* Output input parameters: Begin */
			printf("																				Technique Name: %s.\n", pTech->GetName().c_str());
			/* Output input parameters: End */

			// if this is not the "preferred" technique as dictated by the COLLADA doc, skip it
			if (techName != preferredTechnique)
			{
				printf("																				Technique name is not equal to preferred Technique, give up the remaining operations.\n");
				continue;
			}

			size_t passCount = pTech->GetPassCount();

			// create matching Ogre technique
			Ogre::Technique* pOgreTech = pMtl->createTechnique();
			pOgreTech->setName(pTech->GetName().c_str());
			printf("																				Create the OgreTechnique. Ogre::Technique->pOgreTech\n");
			printf("																				Iterate the passes of this technique: count: %d\n", passCount);

			for (size_t passIdx=0; passIdx<passCount; ++passIdx)
			{
				FCDEffectPass* pPass = pTech->GetPass(passIdx);

				/* Output input parameters: Begin */
				printf("																					Pass Name: %s\n", pPass->GetPassName().c_str());
				/* Output input parameters: End */			

				// create an Ogre pass for each pass in this technique
				Ogre::Pass* pOgrePass = pOgreTech->createPass();
				pOgrePass->setName(pPass->GetPassName().c_str());
				pOgrePass->setLightingEnabled(true);
				printf("																					Create OgrePass: pOgrePass. Name: %s\n", pOgrePass->getName().c_str());

				Ogre::HighLevelGpuProgram* pVertex = 0;
				Ogre::HighLevelGpuProgram* pFragment = 0;
				Ogre::NameValuePairList vParams, fParams;

				// vertex program
				FCDEffectPassShader* pVP = pPass->GetVertexShader();
				printf("																					Get FCDEffectPassShader From FCDEffectPass.\n");

				
				if (pVP)
				{
					/* Output input parameters: Begin */
					printf("																					Vertex Shader Name: %s\n", pVP->GetName().c_str());
					printf("																					Vertex Shader Bindings Count is: %d\n", pVP->GetBindingCount());
					printf("																					Vertex Shader Compiling Target is %s\n", pVP->GetCompilerTarget().c_str());
					/* Output input parameters: End */

					// assemble a syntax code string
					vParams["language"] = "cg";
//					vParams["profiles"] = "vp40";//pVP->GetCompilerTarget().c_str();
					vParams["profiles"] = pVP->GetCompilerTarget().c_str();
					vParams["entry_point"] = pVP->GetName().c_str();

					/* Output input parameters: Begin */
					printf("																					vParams['profiles']: %s\n",vParams["profiles"].c_str());
					printf("																					vParams['entry_point']: %s\n", vParams["entry_point"].c_str());

					/* Output input parameters: End */

					// set include directories
					std::string path(pProfile->GetDocument()->GetFileUrl().c_str());
					std::string path_arguments = std::string("-I ") + path.substr(0, path.find_last_of("\\/"));
					printf("																					path is: %s\n", path.c_str());
					printf("																					path.find_last_of(\\/): %d\n", path.find_last_of("\\/"));
					vParams["compile_arguments"] = path_arguments;
					printf("																					vParams['compile_arguments']: %s\n", vParams["compile_arguments"].c_str());

					pVertex = (Ogre::HighLevelGpuProgram*)pVP->GetUserHandle();

					if (!pVertex)
					{
						// need to make a unique name for this program
						std::stringstream vpName;
						vpName << pProfile->GetParent()->GetDaeId().c_str();
						vpName << "_";
						vpName << pTech->GetName().c_str();
						vpName << "_";
						vpName << pPass->GetPassName().c_str();
						vpName << "_VERTEX";

						/* Output input parameters: Begin */
						printf("																					Compiling %s.\n" , vpName.str().c_str());
						/* Output input parameters: End */

						pVertex = static_cast<Ogre::HighLevelGpuProgram*>(
							pMgr->create(
								vpName.str(), 
								m_resGroupName, 
								false, 
								0, 
								&vParams
							).getPointer()
						);
						printf("																					Created %s\n", vpName.str().c_str());

						if (pVertex)
						{
							// extract program source code
							std::stringstream ss;
							FCDEffectCode* pCode = pVP->GetCode();
							printf("																					Get FCDEffectCode ->pCode.\n");
							if (pCode->GetType() == FCDEffectCode::INCLUDE)
							{
								// extract included file into code
								printf("																					FCDEffectCode is include type, the url is %s.\n", pCode->GetFilename().c_str());
								std::ifstream i;
								i.open(pCode->GetFilename().c_str());
								if (!i.is_open())
								{
									printf("																					The code file is failed to open.\n");
									continue;
								}

								ss << i.rdbuf();
								i.close();
							}
							else
								ss << pCode->GetCode().c_str();

							/* Output input parameters: Begin */
//							printf("																					Vertex Source Code:\n\n%s\n\n",ss.str().c_str());
							/* Output input parameters: End */

							pVertex->setSource(ss.str());
							printf("																					Set up the source code at Ogre::HighLevelGpuProgram.\n");
							

							if (pVertex->isSupported())
							{
								pVP->SetUserHandle(pVertex);

								// TODO: figure out a way to make FCollada give us the legal shader annotations

								// Laxman - Commented
//								pVertex->setSurfaceAndPassLightStates(true);
								pVertex->setType(Ogre::GPT_VERTEX_PROGRAM);
								pOgrePass->setVertexProgram(pVertex->getName());

								printf("																					the Vertex HighLevelGpuProgram is supported. Set up the user handle at pVP\n");

								// bind vertex program parameters
								bindParameters(pVP, pOgrePass, pOgrePass->getVertexProgramParameters());
								printf("																					bind Parameters to Ogre::Pass.\n");
							}
							else
							{
								printf("																					the Vertex HighLevelGpuProgram is not supported.\n");
								pVertex = 0;
							}
						}
					}
				}

				// fragment program
				FCDEffectPassShader* pFP = pPass->GetFragmentShader();

				if (pFP)
				{
					fParams["language"] = "cg";
//					fParams["profiles"] = "fp40";//pFP->GetCompilerTarget().c_str();
					fParams["profiles"] = pFP->GetCompilerTarget().c_str();
					fParams["entry_point"] = pFP->GetName().c_str();

					// set include directories
					std::string path(pProfile->GetDocument()->GetFileUrl().c_str());
					std::string path_arguments = std::string("-I ") + path.substr(0, path.find_last_of("\\/"));
					fParams["compile_arguments"] = path_arguments;

					/* Output input parameters: Begin */
					printf("																					fParams['profiles']: %s\n", fParams["profiles"].c_str());
					printf("																					fParams['entry_point']: %s\n", fParams["entry_point"].c_str());
					printf("																					fParams['compile_arguments']: %s\n", fParams["compile_arguments"].c_str());
					/* Output input parameters: End */

					pFragment = (Ogre::HighLevelGpuProgram*)pFP->GetUserHandle();

					if (!pFragment)
					{
						// need to make a unique name for this program
						std::stringstream fpName;
						fpName << pProfile->GetParent()->GetDaeId().c_str();
						fpName << "_";
						fpName << pTech->GetName().c_str();
						fpName << "_";
						fpName << pPass->GetPassName().c_str();
						fpName << "_FRAGMENT";

						/* Output input parameters: Begin */
						printf("																					OgreCollada: Compiling %s\n", fpName.str().c_str());
						/* Output input parameters: End */

						pFragment = static_cast<Ogre::HighLevelGpuProgram*>(
							pMgr->create(
								fpName.str(), 
								m_resGroupName,
								false,
								0,
							&fParams).getPointer()
						);
						printf("																					Created the fragment HighLevelGpuProgram.\n");
						printf("																					Extract the FCDEffectCode.\n");
						if (pFragment)
						{
							// extract program source code							
							std::stringstream ss;
							FCDEffectCode* pCode = pFP->GetCode();
							if (pCode->GetType() == FCDEffectCode::INCLUDE)
							{
								// extract included file into code
								printf("																					The FCDEffectCode Type is INCLUDE.\n");
								printf("																					Open the file: %s\n", pCode->GetFilename().c_str());
								std::ifstream i;
								i.open(pCode->GetFilename().c_str());
								if (!i.is_open())
								{
									printf("																					The file could not open.\n");
									continue;
								}

								ss << i.rdbuf();
								i.close();
							}
							else
								ss << pCode->GetCode().c_str();

							pFragment->setSource(ss.str().c_str());


							/* Output input parameters: Begin */
							printf("																					Fragment Source Code: \n\n%s\n\n", ss.str().c_str());
							/* Output input parameters: End */

							if (pFragment->isSupported())
							{
								printf("																					the fragment HighLevelGPUProgram is supported.\n");
								pFP->SetUserHandle(pFragment);

								// TODO: figure out a way to make FCollada give us the legal shader annotations
								
								// Laxman -- Commented
//								pFragment->setSurfaceAndPassLightStates(true);
								pFragment->setType(Ogre::GPT_FRAGMENT_PROGRAM);
								pOgrePass->setFragmentProgram(pFragment->getName());

								// bind fragment program parameters
								bindParameters(pFP, pOgrePass, pOgrePass->getFragmentProgramParameters());
								printf("																					Set up the user handle and bind parameters.\n");
							}
							else
							{
								printf("																					the fragment HighLevelGPUProgram is not supported.\n");
								pFragment = 0;
							}
						}
					}
				}

				// if we could not make any programs for this pass, eliminate the pass
				if (!pVertex && !pFragment)
				{
					printf("																					There is no vertex and fragment HighLevelGPUProgram, remove the ogre pass.\n");
					pOgreTech->removePass(pOgrePass->getIndex());
				}
			}

			// if this turned into an empty technique, discard it
			printf("																				The number of pass for this tech is(Ogre): %d\n", pOgreTech->getNumPasses());
			if (!pOgreTech->getNumPasses())
			{
				printf("																				remove all passes.\n");
				for (unsigned int i=0; i<pMtl->getNumTechniques(); ++i)
				{
					if (pOgreTech == pMtl->getTechnique(i))
					{
						pMtl->removeTechnique(i);
						break;
					}
				}
			}
		}
	}
}
