/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "FCDocument/FCDEffectTools.h"
#include "FCDocument/FCDEffectProfile.h"
#include "FCDocument/FCDEffectStandard.h"
#include "FCDocument/FCDEffectTechnique.h"
#include "FCDocument/FCDTexture.h"
#include "FCDocument/FCDEffectPass.h"
#include "FCDocument/FCDEffectParameter.h"
#include "FCDocument/FCDEffectParameterSampler.h"
#include "FCDocument/FCDEffectParameterSurface.h"

#include "FMath/FMVector4.h"
#include "FMath/FMVector3.h"
#include "FMath/FMVector2.h"

// Ogre Materials and COLLADA Materials are a bit different from one another. The primary difference
// is that COLLADA treats "effects" as templates, whose tweakable parameters are filled in by a
// COLLADA Material, which creates an instance of an effect template. Ogre, on the other hand, treats
// Material as a monolithic object -- there can only be one of a Material, and beyond the atomic items that 
// are referenced within that Material (textures, GPU programs), the Material itself is not instanceable.
//
// So, for OgreCollada, that leaves us with a bit of a problem. The way we will solve it, is to create
// a new Ogre Material for every COLLADA material in the material library. Each of these is an instance
// of a COLLADA effect, and since each COLLADA material has its own name, we can create unique Ogre
// Materials from each COLLADA material. The effect template instances will go to create the actual 
// Ogre Material, apart from the aforementioned textures and GPU programs, which are resources managed
// independent resource managers. Ogre named Textures and GPU programs will have been created already
// by passes over the image and effect libraries.
//
// Note that we do not need to check for material uniqueness here -- COLLADA will have sorted that out already

#include "ColladaBindingManager.h"

namespace OgreCollada
{
	void ImpExpImpl::addTextureStage(Ogre::Pass* pPass, FCDTexture* pTex)
	{
		FCDEffectParameterInt* pSet = pTex->GetSet();
		std::string texSemantic(pSet->GetSemantic().c_str());
		int set = pSet->GetValue();

		// check to see if the bindings override this texcoord setting
//		const FCDMaterialInstanceBindVertexInput* pVertexBinding = 
//			m_pBindingManager->getVertexBinding(FUDaeGeometryInput::FromString(pSet->GetSemantic()));
		const FCDMaterialInstanceBindVertexInput* pVertexBinding = 
			m_pBindingManager->getVertexBinding(texSemantic);

		if (pVertexBinding && pVertexBinding->inputSemantic == FUDaeGeometryInput::TEXCOORD)
		{
			set = pVertexBinding->inputSet;
		}

		FCDEffectParameterSampler* pSampler = pTex->GetSampler();
		if (!pSampler)
			return;

		FCDEffectParameterSurface* pSurface = pSampler->GetSurface();
		if (!pSurface)
			return;

		// check for material override first
		FCDEffectParameter* pSurfParam = FCDEffectTools::FindEffectParameterByReference(m_pCurrentMtlInst, pSurface->GetReference());

		FCDImage* pImage = 0;
		if (pSurfParam)
		{
			pImage = static_cast<FCDEffectParameterSurface*>(pSurfParam)->GetImage();
		}
		else
		{
			pImage = pSurface->GetImage();
		}

		if (pImage)
		{
			if (!pImage->GetUserHandle())
			{
				// create this texture first
				addTexture(pImage);
			}

			Ogre::TextureUnitState* pTUS = pPass->createTextureUnitState();
			pTUS->setTextureName(CONVERT_STRING(pImage->GetDaeId().c_str()));
			pTUS->setTextureCoordSet(set);

			// TODO: figure out how to get texture/coord effects here

			// TODO: figure out how to get blend ops instead
			FCDExtra* pExtra = pTex->GetExtra();
			pTUS->setColourOperation(Ogre::LBO_REPLACE);
		}
	}

	void ImpExpImpl::createBasicShading(Ogre::Pass *pPass, FCDEffectStandard *pEffect)
	{
		FCDEffectParameterColor4* pColor = 0;
		FMVector4 color;

		size_t texCount = pEffect->GetTextureCount(FUDaeTextureChannel::EMISSION);
		if (texCount)
		{
			for (size_t i=0; i<texCount; ++i)
			{
				FCDTexture* pTex = pEffect->GetTexture(FUDaeTextureChannel::EMISSION, i);
				addTextureStage(pPass, pTex);
			}
		}
		else
		{
			pColor = pEffect->GetEmissionColorParam();
			color = pEffect->GetEmissionColor();
			if (pColor)
			{
				color = pColor->GetValue();
			}
			pPass->setSelfIllumination(color.x, color.y, color.z);
		}

		// ggj hacky hack for scene transparency -- Maya exports things that should
		// be scene-blend-alpha as a second "TRANSPARENT" texture channel; just set
		// the material pass to be "scene_blend alpha_blend" instead
		texCount = pEffect->GetTextureCount(FUDaeTextureChannel::TRANSPARENT);
		if (texCount)
		{
			pPass->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
			pPass->setDepthWriteEnabled(false);
			pPass->setDepthCheckEnabled(false);
		}


		texCount = pEffect->GetTextureCount(FUDaeTextureChannel::AMBIENT);
		if (texCount)
		{
			for (size_t i=0; i<texCount; ++i)
			{
				FCDTexture* pTex = pEffect->GetTexture(FUDaeTextureChannel::AMBIENT, i);
				addTextureStage(pPass, pTex);
			}
		}
		else
		{
			pColor = pEffect->GetAmbientColorParam();
			color = pEffect->GetAmbientColor();
			if (pColor)
			{
				color = pColor->GetValue();
			}
			pPass->setAmbient(color.x, color.y, color.z);
		}


		texCount = pEffect->GetTextureCount(FUDaeTextureChannel::DIFFUSE);
		if (texCount)
		{
			for (size_t i=0; i<texCount; ++i)
			{
				FCDTexture* pTex = pEffect->GetTexture(FUDaeTextureChannel::DIFFUSE, i);
				addTextureStage(pPass, pTex);
			}
		}
		else
		{
			pColor = pEffect->GetDiffuseColorParam();
			color = pEffect->GetDiffuseColor();
			if (pColor)
			{
				color = pColor->GetValue();
			}
			pPass->setDiffuse(color.x, color.y, color.z, color.w);
		}

		texCount = pEffect->GetTextureCount(FUDaeTextureChannel::SPECULAR);
		if (texCount)
		{
			for (size_t i=0; i<texCount; ++i)
			{
				FCDTexture* pTex = pEffect->GetTexture(FUDaeTextureChannel::SPECULAR, i);
				addTextureStage(pPass, pTex);
			}
		}
		else
		{
			pColor = pEffect->GetSpecularColorParam();
			color = pEffect->GetSpecularColor();
			if (pColor)
			{
				color = pColor->GetValue();
			}
			pPass->setSpecular(color.x, color.y, color.z, color.w);

			FCDEffectParameterFloat* pFloat = pEffect->GetSpecularFactorParam();
			float specFactor = pEffect->GetSpecularFactor();
			if (pFloat)
			{
				specFactor = pFloat->GetValue();
			}
			pPass->setShininess(specFactor);
		}
	}

	// all COMMON techniques are single-pass affairs
	void ImpExpImpl::createConstantTechnique(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile)
	{
		FCDEffectStandard* pEffect = (FCDEffectStandard*)pProfile;

		Ogre::Technique* pTech = pMtl->createTechnique();
		Ogre::Pass* pPass = pTech->createPass();
		pPass->setShadingMode(Ogre::SO_GOURAUD);

		createBasicShading(pPass, pEffect);
	}

	void ImpExpImpl::createBlinnTechnique(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile)
	{
		FCDEffectStandard* pEffect = (FCDEffectStandard*)pProfile;

		Ogre::Technique* pTech = pMtl->createTechnique();
		Ogre::Pass* pPass = pTech->createPass();
		pPass->setShadingMode(Ogre::SO_PHONG);

		createBasicShading(pPass, pEffect);
	}

	void ImpExpImpl::createLambertTechnique(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile)
	{
		FCDEffectStandard* pEffect = (FCDEffectStandard*)pProfile;

		Ogre::Technique* pTech = pMtl->createTechnique();
		Ogre::Pass* pPass = pTech->createPass();
		pPass->setShadingMode(Ogre::SO_GOURAUD);

		createBasicShading(pPass, pEffect);
	}

	void ImpExpImpl::createPhongTechnique(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile)
	{
		FCDEffectStandard* pEffect = (FCDEffectStandard*)pProfile;

		Ogre::Technique* pTech = pMtl->createTechnique();
		Ogre::Pass* pPass = pTech->createPass();
		pPass->setShadingMode(Ogre::SO_PHONG);

		createBasicShading(pPass, pEffect);
	}

	void ImpExpImpl::createCgTechnique(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile, const std::string& preferredTechnique)
	{
//		Ogre::LogManager::getSingletonPtr()->logMessage(std::string("OgreCollada : Material Information : ") + std::string(pMtl.getPointer()->getName()) + std::string(" ") + std::string(preferredTechnique));
		printf("																		Material Information: %s\n", pMtl.getPointer()->getName().c_str());
		printf("																		Call Function addEffect(Ogre::MaterialPtr, FCDEffectProfile, string preferredTechnique);\n");
		addEffect(pMtl, pProfile, preferredTechnique);
	}

	void ImpExpImpl::createTechnique(Ogre::MaterialPtr pMtl, FCDEffectProfile* pProfile, const std::string& preferredTechnique)
	{
		// fixed-function techniques
		printf("																	Retrieve the FCDEffectProfile Type, based on the type, to call the appropriate function.\n");
		if (pProfile->GetType() == FUDaeProfileType::COMMON)
		{
			FCDEffectStandard* pEffect = (FCDEffectStandard*)pProfile;

			switch(pEffect->GetLightingType())
			{
			case FCDEffectStandard::CONSTANT:
				createConstantTechnique(pMtl, pProfile);
				break;
			case FCDEffectStandard::BLINN:
				createBlinnTechnique(pMtl, pProfile);
				break;
			case FCDEffectStandard::LAMBERT:
				createLambertTechnique(pMtl, pProfile);
				break;
			case FCDEffectStandard::PHONG:
				createPhongTechnique(pMtl, pProfile);
				break;
			}
		}

		// nVidia Cg-language programmable techniques
		if (pProfile->GetType() == FUDaeProfileType::CG)
		{
			printf("																	Call the createCgTechnique.  PreferredTechnique: %s\n", preferredTechnique.c_str());
			createCgTechnique(pMtl, pProfile, preferredTechnique);
		}
	}

	void ImpExpImpl::createMaterial(Ogre::MaterialPtr pMtl, FCDMaterial* pMaterial)
	{
		// COLLADA materials are just instanced effects with different params
		FCDEffect* pEffect = pMaterial->GetEffect();

		if (!pEffect)
			return;

		// given that Ogre uses a preferential technique selection system based on order of 
		// appearance within a material, we would like the fixed-function techniques to come
		// last. Therefore, we need to be able to extract the profiles from an effect in such a way
		// that we process the fixed-function techniques last -- a nice list should suffice.
		std::list<FCDEffectProfile*> profileList;
		FCDEffectProfile* pCommonProfile = 0;

		// effects can have a single profile_COMMON (fixed-function) and zero or more
		// profile_CG (programmable) or other alternate profiles
		size_t profileCount = pEffect->GetProfileCount();
		printf("														Retrieve the FCDEffect.\n");
		printf("														Iterate each profile. Count:%d\n", profileCount);
		for (size_t i=0; i<profileCount; ++i)
		{
			FCDEffectProfile* pProfile = pEffect->GetProfile(i);
			FUDaeProfileType::Type profileType = pProfile->GetType();

			if (profileType == FUDaeProfileType::COMMON)
			{
				printf("															Assign this profile to the common Profile, Name %s.\n", pProfile->GetDaeId().c_str());
				pCommonProfile = pProfile;
			}
			else
			{
				printf("															Push this profile into the non-common profile list. Name: %s.\n", pProfile->GetDaeId().c_str());
				profileList.push_back(pProfile);
			}
		}

		// go through the profiles, making Ogre Material Techniques of each
		printf("															Iterate each non-common FCDEffectProfile.\n");
		for (std::list<FCDEffectProfile*>::iterator it = profileList.begin(); it != profileList.end(); ++it)
		{
			std::string preferredTechnique;
			std::string targetPlatform("PC-OGL"); // FX Composer-specific for now
			std::string targetD3DPlatform("PC-DX9"); // FX Composer-specific for now
			FCDMaterialTechniqueHintList& hints = pMaterial->GetTechniqueHints();

			printf("																Retrieve technique hints list and iterate.\n");

			for (FCDMaterialTechniqueHintList::iterator hint = hints.begin();
				hint != hints.end(); ++hint)
			{
				printf("																	Hint: Platform: %s, Technique: %s.\n", hint->platform.c_str(), hint->technique.c_str());
				if (targetPlatform == hint->platform.c_str() ||
					targetD3DPlatform == hint->platform.c_str())
				{					
					preferredTechnique = hint->technique.c_str();
					printf("																		Assign it to the preferredTechnique: %s.\n", preferredTechnique.c_str());
					break;
				}
			}
	
			printf("																Call createTechnique Function for non-common profile.\n");
			createTechnique(pMtl, *it, preferredTechnique);
		}

		// and then finally the fixed-function guy -- note, might not exist if the artist did not make
		// a fixed-function fallback technique
		if (pCommonProfile)
		{
			printf("																Call createTechnqiue Function for common profile.\n");
			createTechnique(pMtl, pCommonProfile, "");
		}
	}

	Ogre::Material* ImpExpImpl::addMaterial(FCDMaterialInstance* pMaterialInst)
	{
		FCDMaterial* pMaterial = pMaterialInst->GetMaterial();
		//FCDMaterial* pMaterial = pMaterialInst->FlattenMaterial();
		printf("													Retrieve FCDMaterial.\n");

		// TODO: hmm, singletons...
		Ogre::MaterialManager* pMtlMgr = Ogre::MaterialManager::getSingletonPtr();

		// TODO: support alternate resource group names here
		Ogre::MaterialPtr pMtl = pMtlMgr->create(
			CONVERT_STRING(pMaterial->GetDaeId().c_str()), 
			m_resGroupName);
		printf("													Create Material Pointer based on the daeId of material :%s.\n", pMaterial->GetDaeId().c_str());

		// Ogre puts in a single default technique and pass with every material -- no additional charge...
		pMtl->removeAllTechniques();
		printf("													Remove Material Pointer's all techniques.\n");
		// ... except that we'd rather just make them all up ourselves...

		printf("													Call Function createMaterial()\n");
		createMaterial(pMtl, pMaterial);
		Ogre::LogManager::getSingleton().logMessage(pMtl->getUnsupportedTechniquesExplanation());
		printf("													Return the material Pointer.\n");
		return pMtl.getPointer();
	}

	void ImpExpImpl::processMaterialInstance(FCDMaterialInstance* pMaterialInst)
	{
		// convenience member variable for processing deeper in the material tree
		printf("											Function processMaterialInstance(), name: %s.\n", pMaterialInst->GetMaterial()->GetName().c_str());
		m_pCurrentMtlInst = pMaterialInst;

		Ogre::Material* pMtl = static_cast<Ogre::Material*>(pMaterialInst->GetMaterial()->GetUserHandle());

		printf("												Call addbindings().\n");
		m_pBindingManager->clear();
		m_pBindingManager->addBindings(pMaterialInst);
		//m_pBindingManager->addBindings(pMaterialInst->GetVertexInputBindings());
		//m_pBindingManager->addBindings(pMaterialInst->GetBindings());

		// we have two cases where we have to make a new material -- either pMtl will be null, or there are bindings
		// on this material instance. If there are bindings and we have an existing material, we clone the existing
		// one and set its values to the bindings; otherwise, we can just use the existing material. No matter what, 
		// the user handle on this instance will be set to the resulting material.
		printf("												Judge whether we should create the material instance.\n");
		if (!pMtl)
		{
//			Ogre::LogManager::getSingletonPtr()->logMessage("OgreCollada : Material not found, creating... " + std::string(pMaterialInst->GetName()));
			printf("												Call addMaterial(): Convert material info from collada format into ogre format.\n");
			pMtl = addMaterial(pMaterialInst);
			pMaterialInst->GetMaterial()->SetUserHandle(pMtl);

			if (m_pCallback && pMtl)
			{
				m_pCallback->resourceCreated(pMtl, pMaterialInst->GetMaterial());
			}
		}
		else
		{
			pMtl = (pMtl->clone(pMtl->getName() + Ogre::StringConverter::toString(m_uniqueCounter++, 6))).getPointer();
		}
	}
}
