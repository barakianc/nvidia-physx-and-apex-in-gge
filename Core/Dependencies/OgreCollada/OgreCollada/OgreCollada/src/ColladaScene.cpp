/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

#include "FMath/FMVector2.h"
#include "FMath/FMVector3.h"
#include "FMath/FMVector4.h"
#include "FCDocument/FCDAsset.h"

#include "ColladaBindingManager.h"

namespace OgreCollada
{

	Ogre::Entity* createEntity(Ogre::SceneManager* pSm, FCDControllerInstance* pInst)
	{
		FCDController* pController = static_cast<FCDController*>(pInst->GetEntity());
		const char_t* pEntName = 0;

		FCDSkinController* pSkin = pController->GetSkinController();
		if (pSkin)
		{
			pEntName = CONVERT_STRING(pSkin->GetTarget()->GetDaeId().c_str());
		}

		FCDMorphController* pMorph = pController->GetMorphController();
//		if (pMorph)
//		{
//			pEntName = CONVERT_STRING(pMorph->GetTarget()->GetDaeId().c_str());
//		}

		if (!pEntName)
			return 0;

		Ogre::Entity* pEnt = 0;
		try
		{
			 pEnt = pSm->createEntity(pEntName, pEntName);
		}
		catch(...)
		{
			// just return
			return pEnt;
		}

		return pEnt;
	}


	Ogre::Entity* createEntity(Ogre::SceneManager* pSm, const FCDGeometryInstance* pInst)
	{
		const char_t* pEntName = CONVERT_STRING(pInst->GetEntity()->GetDaeId().c_str());

		Ogre::Entity* pEnt = 0;
		try
		{
			 pEnt = pSm->createEntity(pEntName, pEntName);
		}
		catch(...)
		{
		}
		return pEnt;
	}

	void ImpExpImpl::setNodeTransform(Ogre::Node* pOgreNode, FMMatrix44& mat)
	{

		FMVector3 scale, r, t;
		float inv;
		mat.Decompose(scale, r, t, inv);

		FMQuaternion quat = FMQuaternion::MatrixRotationQuaternion(mat);
		FMVector3 pos(mat.GetTranslation());

		pOgreNode->setOrientation(quat.w, quat.x, quat.y, quat.z);
		pOgreNode->setPosition(pos.x, pos.y, pos.z);
		pOgreNode->setScale(scale.x, scale.y, scale.z);
	}

	void ImpExpImpl::setNodeTransform(Ogre::Node* pOgreNode, FCDSceneNode* pNode)
	{
		// there is only a single matrix transform allowed in the transform block; if this is a 
		// matrix transform, perform that and leave...
		if (pNode->GetTransformCount() == 1 && pNode->GetTransform(0)->GetType() == FCDTransform::MATRIX)
		{
			FCDTMatrix* pMat = static_cast<FCDTMatrix*>(pNode->GetTransform(0));
			FMMatrix44& mat = pMat->GetTransform();
			setNodeTransform(pOgreNode, mat);
			return;
		}

		// ... otherwise, construct an aggregate transform from the pieces
		FMVector3 trans = FMVector3::Origin;
		FMVector3 scale = FMVector3::One;
		FMQuaternion rot = FMQuaternion::Identity;

		for (size_t t=0; t<pNode->GetTransformCount(); ++t)
		{
			FCDTransform* pXF = pNode->GetTransform(t);

			switch(pXF->GetType())
			{
			case FCDTransform::TRANSLATION:
				{
					FCDTTranslation* pTrans = static_cast<FCDTTranslation*>(pXF);
					trans += pTrans->GetTranslation();
				}
				break;

			case FCDTransform::ROTATION:
				{
					FCDTRotation* pRot = static_cast<FCDTRotation*>(pXF);
					rot = pRot->GetOrientation() * rot;
				}
				break;

			case FCDTransform::SCALE:
				{
					FCDTScale* pScale = static_cast<FCDTScale*>(pXF);
					FMVector3& rScale = pScale->GetScale();
					scale.x *= rScale.x;
					scale.y *= rScale.y;
					scale.z *= rScale.z;
				}
				break;

			case FCDTransform::LOOKAT:
				// TODO: lookAt not supported for nodes -- yet
				break;

			case FCDTransform::MATRIX:
				break;

			case FCDTransform::SKEW:
				// not supported
				break;
			}
		}
		rot.NormalizeIt();
		FMMatrix44 mat = rot.ToMatrix();
		FMMatrix44 scaleMat = FMMatrix44::ScaleMatrix(scale);
		mat = scaleMat * mat;
		mat.SetTranslation(trans);
		setNodeTransform(pOgreNode, mat);
	}

	void ImpExpImpl::processVisualScene(FCDSceneNode* pScene)
	{
		printf("		Function processVisualScene for %s. \n", pScene->GetName().c_str());
		// do all object types next
		size_t childCount = pScene->GetChildrenCount(); 

		// one pass at top level for any skeleton definitions -- we need
		// these done before we do controller nodes so that skeletons can
		// be hooked up properly to their submeshes
		printf("			Create the Skeleton for all possible children nodes. count: %d\n", childCount);
		for (size_t child=0; child<childCount; ++child)
		{
			FCDSceneNode* pNode = pScene->GetChild(child);

			// TODO: IsJoint is deprecated, use GetJointFlag() instead
			if (pNode->IsJoint())
			{
				// drill down into children to create a skeleton from this root bone
				m_skeletons[std::string(pNode->GetName().c_str())] = createSkeleton(pNode);
				printf("				Create the skeleton for the child: %s\n", pNode->GetName().c_str());
				// skip the rest of the processing in this block; any time we process the 
				// tree beginning with a joint, it's a skeleton definition so there is
				// nothing else to do for that the rest of this method.
				continue;
			}
		}
		
		printf("			Process all instances for all children. Children Count: %d.\n", childCount);
		for (size_t child=0; child<childCount; ++child)
		{
			FCDSceneNode* pNode = pScene->GetChild(child);
			printf("				Process Child : %s\n", pNode->GetName().c_str());

			// TODO: IsJoint is deprecated, use GetJointFlag() instead
			if (pNode->IsJoint())
				continue; // skip skeleton nodes

			size_t instanceCount = pNode->GetInstanceCount();
			printf("					The child has %d instances.\n", instanceCount);

			for (size_t instance=0; instance<instanceCount; ++instance)
			{
				Ogre::MovableObject* pMovable = 0;
				FCDEntityInstance* pInst = pNode->GetInstance(instance);
				printf("						Process Instance %s\n", pInst->GetName().c_str());

				switch (pInst->GetType())
				{
				case FCDEntityInstance::CONTROLLER:
					{
						// we don't attach these to nodes because even though they create meshes,
						// they are controlled by bones, not scene nodes, so they do not get attached to 
						// a scene node (all other types of objects created do get attached to a node). The
						// fact that pMovable will be 0 when we hit that check later will flag that no node
						// needs to be created.

						// create the meshes and all their dependents for this instance
						printf("							The instance is controller.\n");
						processControllerInstance(static_cast<FCDControllerInstance*>(pInst));

						// make an Ogre Entity for the resultant mesh
						Ogre::Entity* pEnt = createEntity(m_pSceneMgr, static_cast<FCDControllerInstance*>(pInst));

						if (pEnt && m_pCallback)
							m_pCallback->entityCreated(pEnt, pNode);
					}
					break;

				case FCDEntityInstance::GEOMETRY:
					{
						// create the meshes and all their dependents for this instance
						printf("							The instance is geometry.\n");
						printf("							Function processGeometryInstance.\n");
						bool ret = processGeometryInstance(static_cast<FCDGeometryInstance*>(pInst));

						// make an Ogre Entity for the mesh
						if ( ret ) 
						{
							printf("							Call Function createEntity().\n");
							Ogre::Entity* pEnt = createEntity(m_pSceneMgr, static_cast<FCDGeometryInstance*>(pInst));

							if (pEnt && m_pCallback)
							{
								printf("							Call Callback function entityCreated().\n");
								m_pCallback->entityCreated(pEnt, pNode);
							}

							pMovable = pEnt;
						}
					}
					break;


				case FCDEntityInstance::SIMPLE:
					printf("							The instance is simple.\n");
					switch(pInst->GetEntityType())
					{
					case FCDEntity::LIGHT:
						{
							FCDLight* pLight = static_cast<FCDLight*>(pInst->GetEntity());
							if (pLight->GetLightType() != FCDLight::AMBIENT)
							{
								pMovable = processLightInstance(pInst);
							}
							else
							{
								FMVector3& lightColor = pLight->GetColor();
								m_pSceneMgr->setAmbientLight(Ogre::ColourValue(lightColor.x, lightColor.y, lightColor.z));
							}
						}
						break;

					case FCDEntity::CAMERA:
						pMovable = processCameraInstance(pInst);
						break;
					}
					break;
				}

				if (pMovable)
				{
					printf("						Got one movable Instance: %s \n", pMovable->getName().c_str());
					Ogre::SceneNode* pParent = static_cast<Ogre::SceneNode*>(pScene->GetUserHandle());

					if(m_pSceneMgr->hasSceneNode(this->m_uniqueDaeName))
						pParent = m_pSceneMgr->getSceneNode(this->m_uniqueDaeName);
					else
						pParent = m_pSceneMgr->getRootSceneNode()->createChildSceneNode(this->m_uniqueDaeName);

					Ogre::SceneNode* pChild;
					if (pParent)
					{
						pChild = pParent->createChildSceneNode((std::string(pNode->GetDaeId().c_str()) + std::string(ltoa(this->m_uniqueCounter,new char[],10))));
					}
					else
					{
						pChild = m_pSceneMgr->getRootSceneNode()->createChildSceneNode((std::string(pNode->GetDaeId().c_str()) + std::string(ltoa(this->m_uniqueCounter,new char[],10))));
					}

					this->m_uniqueCounter++;

					pChild->attachObject(pMovable);
					pNode->SetUserHandle(pChild);
					setNodeTransform(pChild, pNode);
					createAnimationTracks(pChild, pNode);

					// notify of node created
					if (m_pCallback)
						m_pCallback->sceneNodeCreated(pChild, pNode);
				}
			}

			// recurse down this branch
			processVisualScene(pNode);
		}
	}
}
