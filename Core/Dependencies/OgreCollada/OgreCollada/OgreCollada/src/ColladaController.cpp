/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/


#include "stdafx.h"

#include "FCDocument/FCDGeometryPolygons.h"
#include "FCDocument/FCDGeometrySource.h"
#include "FCDocument/FCDAsset.h"

#include "OgreDefaultHardwareBufferManager.h"

#include "ColladaBindingManager.h"

// we need to examine each effect for GPU program definitions, and create Ogre GPU programs
// from them when we encounter those. These programs will be used later when Ogre Materials
// are assembled from the COLLADA material library. Everything that is not pertinent to GPU
// programs and their parameters/inputs, will be ignored here. The other effect properties 
// will be examined when the Ogre Material is created.

namespace OgreCollada
{
	extern void fillSubmesh(Ogre::HardwareBufferManager* pHWBMgr, Ogre::SubMesh* pSubMesh, FCDGeometryPolygons* pTris, FCDMaterialInstance* pMaterialInst, Ogre::AxisAlignedBox& bbox, Ogre::Real& fBRadius, BindingManager* pBindMgr);

	void ImpExpImpl::skinSubmesh(Ogre::SubMesh* pSubMesh, FCDSkinController* pSkinController)
	{
		if (!pSkinController || !pSubMesh)
			return;

		std::vector<unsigned short> jointToIndexMap;
		size_t jointCount = pSkinController->GetJointCount();
		jointToIndexMap.resize(jointCount);

		// map local (to COLLADA element) joint indices to bones' unique indices
		for (size_t j=0; j<jointCount; ++j)
		{
			jointToIndexMap[j] = 0xFFFF; // flag as invalid

			FCDSkinControllerJoint* pJoint = pSkinController->GetJoint(j);
			Ogre::Bone* pBone = m_nameToBoneLUT[pJoint->GetId().c_str()];

			if (pBone)
			{
				jointToIndexMap[j] = pBone->getHandle();

				Ogre::Skeleton* pSkeleton = m_boneToSkeletonLUT[pBone];

				if (pSkeleton)
				{
					pSubMesh->parent->setSkeletonName(pSkeleton->getName());
				}

				//const FMMatrix44& mat = pJoint->GetBindPoseInverse();
				//setNodeTransform(pBone, mat);
			}
		}

		// now we can use that map to assign out the vertex weights (since those will
		// reference the local joint indices, but Ogre SubMesh needs the Bone handle
		// that corresponds to that index)
		size_t vertWeightCount = pSkinController->GetInfluenceCount();
		for (size_t i=0; i<vertWeightCount; ++i)
		{
			FCDSkinControllerVertex* pInf = pSkinController->GetVertexInfluence(i);

			size_t pairCount = pInf->GetPairCount();
			for (size_t j=0; j<pairCount; ++j)
			{
				Ogre::VertexBoneAssignment vba;
				FCDJointWeightPair* pPair = pInf->GetPair(j);

				vba.boneIndex = jointToIndexMap[pPair->jointIndex];
				vba.vertexIndex = (unsigned int)i;
				vba.weight = pPair->weight;

				pSubMesh->addBoneAssignment(vba);
			}
		}
	}

	Ogre::Mesh* ImpExpImpl::createMesh(FCDControllerInstance* pControllerInst)
	{
		FCDController* pController = static_cast<FCDController*>(pControllerInst->GetEntity());
		FCDSkinController* pSkin = pController->GetSkinController();
		FCDMorphController* pMorph = pController->GetMorphController();

		FCDGeometry* pGeo = 0;
		if (pSkin)
		{
			pGeo = static_cast<FCDGeometry*>(pSkin->GetTarget());
		}

		if (!pGeo)
			return 0;

		Ogre::MeshPtr pRtn = m_pRoot->getMeshManager()->create(
			CONVERT_STRING(pGeo->GetDaeId().c_str()), 
			m_resGroupName, true);

		pGeo->SetUserHandle(pRtn.getPointer());

		// pick a hardware buffer manager to use based on what render system is in place; if none exists yet, use
		// the default HW buffer mgr
		Ogre::HardwareBufferManager* pHWBMgr = Ogre::HardwareBufferManager::getSingletonPtr();
		if (!pHWBMgr)
		{
			pHWBMgr = new Ogre::DefaultHardwareBufferManager();
		}

		// extract submeshes
		FCDGeometryMesh* pMesh = pGeo->GetMesh();
		// TODO: note, we have BOX_NULL in later Ogre versions
		Ogre::AxisAlignedBox bbox = Ogre::AxisAlignedBox();
		bbox.setNull();
		Ogre::Real fBRadius = 0.f;
		for (size_t i=0; i < pMesh->GetPolygonsCount(); ++i)
		{
			FCDMaterialInstance* pMaterialInst = 0;
			if (pControllerInst->GetMaterialInstanceCount())
			{
				pMaterialInst = pControllerInst->GetMaterialInstance(i);
				if (pMaterialInst)
				{
					// create (if needed) an instance of the material for this submesh
					processMaterialInstance(pMaterialInst);
				}
			}

			// skip submeshes with no vertices or faces
			if (!pMesh->GetPolygons(i)->GetFaceCount() || 
					!pMesh->GetPolygons(i)->GetFaceVertexCount())
				continue;

			Ogre::SubMesh* pSubMesh = pRtn->createSubMesh();
			fillSubmesh(pHWBMgr, pSubMesh, pMesh->GetPolygons(i), pMaterialInst, bbox, fBRadius, m_pBindingManager);

			// assign submesh bone weights based on skinning data
			skinSubmesh(pSubMesh, pSkin);

			// attach this SubMesh to the FCDGeometryPolygons that created it,
			// so that we can get to it again later when we are dealing with possible
			// animation/skinning data
			pMesh->GetPolygons(i)->SetUserHandle(pSubMesh);

			// Set bounds
			const Ogre::AxisAlignedBox& currBox = pRtn->getBounds();
			Ogre::Real currRadius = pRtn->getBoundingSphereRadius();
			if (currBox.isNull())
			{
				pRtn->_setBounds(bbox, false);
				pRtn->_setBoundingSphereRadius(Ogre::Math::Sqrt(fBRadius));
			}
			else
			{
				bbox.merge(currBox);
				pRtn->_setBounds(bbox, false);
				pRtn->_setBoundingSphereRadius(std::max(Ogre::Math::Sqrt(fBRadius), currRadius));
			}
		}

		return pRtn.getPointer();
	}

	void ImpExpImpl::createSkinnedMesh(FCDControllerInstance* pControllerInst)
	{
		// by the time this method is called, all skeletons will have been constructed
		// and their bones assigned unique IDs. We can now use that information to 
		// associate skinning influences to submeshes
		Ogre::Mesh* pMesh = createMesh(pControllerInst);
	}

	void ImpExpImpl::processControllerInstance(FCDControllerInstance* pControllerInst)
	{
		createSkinnedMesh(pControllerInst);
	}
}
