/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "ColladaBindingManager.h"

namespace OgreCollada
{
	// my favorite simple one-way hash...
	static UInt32 hashString(const char_t* pString)
	{
		if (pString == 0)
			return 0;

		unsigned int rtn = 0;
		while (*pString)
		{
			rtn = rtn * 131 + (UInt32)*pString++;
		}

		return rtn;
	}

	ImpExp* CreateImpExp(Ogre::Root* pRoot, Ogre::SceneManager* pSceneMgr)
	{
		return new ImpExpImpl(pRoot, pSceneMgr);
	}

	void DestroyImpExp(ImpExp* pImpExp)
	{
		delete pImpExp;
	}

	void ImpExpImpl::createScene()
	{
		if (!m_pDoc)
			return;

		// do visual scenes
		printf("Function createScene().\n");
		
		FCDVisualSceneNodeLibrary* pSceneLib = m_pDoc->GetVisualSceneLibrary();
		printf("	Got the visual scene node library, count: %d.\n", pSceneLib->GetEntityCount());

		for (size_t i=0; i<pSceneLib->GetEntityCount(); ++i)
		{
			FCDSceneNode* pNode = static_cast<FCDSceneNode*>(pSceneLib->GetEntity(i));
			std::string name(pNode->GetName().c_str());
			processVisualScene(pNode);
		}
	}

	ImpExpImpl::ImpExpImpl(Ogre::Root* pRoot, Ogre::SceneManager* pSm)
		: m_bGenerateTangents(true),
		m_bBuildEdgeLists(false),
		m_bOptimizeSkeleton(true),
		m_bOptimizeAnimations(true),
		m_fOptimizationFactor(0.0f),
		m_pCallback(0),
		m_pRoot(pRoot),
		m_pSceneMgr(pSm),
		m_pDoc(0),
		m_uniqueCounter(1),
		m_nextBoneIndex(0)
	{
		FCollada::Initialize();
		m_pBindingManager = new BindingManager();
		m_resGroupName = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
	}

	ImpExpImpl::~ImpExpImpl()
	{
		delete m_pBindingManager;

		if (m_pDoc)
			m_pDoc->Release();

		FCollada::Release();
	}

	/// Laxman 
	// Do the same as the ImportCollada function, but over here, just pass a reference to the Scenenode that
	// is being created to the calling function so that the parent function can do any processing with the
	// node that was created
	bool ImpExpImpl::importColladaGetSceneNode(Ogre::SceneNode **node, const char_t* pDaeName, ImportFlags flags)
	{
		bool importStatus = importCollada(pDaeName, flags);

		if(importStatus)
		{
			// Just passing back the pointer for the Scenenode that was created during the import
			*node = m_pSceneMgr->getSceneNode(this->m_uniqueDaeName);
		}
		else
		{
			// If the import fails, return a null and then move out
			*node = NULL;
		}

		return importStatus;
	}

	bool ImpExpImpl::importCollada(const char_t* pDaeName, ImportFlags flags)
	{
		// cannot load over an existing doc
		if (m_pDoc)
			return false;

		if (!pDaeName)
			return false;
		
		m_pDoc = FCollada::NewTopDocument();

		/* Output input parameters: Begin */
		printf("\n\n\nCollada Cgfx Loading Log:\n\n");
		printf("FCDcoument created successfully!\n");
		/* Output input parameters: End */

		if (!FCollada::LoadDocumentFromFile(m_pDoc, pDaeName))
			return false;
		/* Output input parameters: Begin */
		printf("\n");
		/* Output input parameters: End */

		// adjust for up-axis throughout the document
		FCDocumentTools::StandardizeUpAxisAndLength(m_pDoc, FMVector3::YAxis);

		//this->m_uniqueDaeName = new std::string(this->m_pDoc->GetFileUrl().length()+1,'x');

		// Create Unique Dae Name
		this->m_uniqueDaeName = std::string(this->m_pDoc->GetFileUrl().c_str());
		
		// Laxman - Modified for custom name generation
		// After getting the file name, now just remove all the unwanted path data and retain
		// only the filename without the extension
		if(this->m_uniqueDaeName.find_last_of('\\') < this->m_uniqueDaeName.size())
		{
			// Mark the starting position
			size_t posStart = this->m_uniqueDaeName.find_last_of('\\');

			// Mart the ending position
			size_t posStart2 = this->m_uniqueDaeName.find_last_of("/");

			// Validate the starting and the ending position for the string
			if((posStart2>=0)&&(posStart2<this->m_uniqueDaeName.length()))
			{
				if((posStart>=0)&&(posStart<this->m_uniqueDaeName.length()))
				{
					if((posStart2>posStart)) 
					{
						posStart = posStart2;
					}
				}
				else
				{
					posStart = posStart2;
				}
			}

			// Mark the position for the extension to be removed
			size_t mLength = this->m_uniqueDaeName.find(".dae") - posStart - 1;

			// Form the name for the SceneNode that will be created out here.
			// Earlier it was using timestamp, but now the code has been changed to use
			// a counter number so that we can have a control over the calling function
			if(mLength > 0) 
			{
				this->m_uniqueDaeName = std::string(this->m_uniqueDaeName.substr(posStart+1,mLength)) 
					+ std::string("_") 
					+ std::string(ltoa(this->m_uniqueCounter, new char[], 10)); 
			}	
			
				// Increment the counter for the number of times files are imported
				this->m_uniqueCounter++;
		}
		
		// populate the scene
		createScene();

		// create animation states for any node animations in the scene
		Ogre::SceneManager::AnimationIterator anims = m_pSceneMgr->getAnimationIterator();
		while (anims.hasMoreElements())
		{
			Ogre::Animation* pAnim = anims.getNext();
			m_pSceneMgr->createAnimationState(pAnim->getName());
		}

		// Walk the physics scene(s) in the document; this mainly just calls
		// out to a callback handler with either mesh or phsyics primitive shapes,
		// and whatever physical properties are defined for the physics objects
		// (since Ogre does not handle physics, but we want to let the user get
		// at the physics data in the doc).
		createPhysics();

		return true;
	}

	void ImpExpImpl::setResourceGroupName(const std::string& name)
	{
		m_resGroupName = name;
	}

	bool ImpExpImpl::exportCollada(const char_t* pDaeName)
	{
		FCollada::SaveDocument(m_pDoc, pDaeName);
		return true;
	}

	void ImpExpImpl::setRoot(Ogre::Root* pRoot)
	{
		m_pRoot = pRoot;
	}

	void ImpExpImpl::setSceneManager(Ogre::SceneManager* pSceneMgr)
	{
		m_pSceneMgr = pSceneMgr;
	}

	void ImpExpImpl::setResourceNoticationListener(IResourceNotification* pCallback)
	{
		m_pCallback = pCallback;
	}

	void ImpExpImpl::setCustomProfileName(const std::string& name)
	{
	}

	const char_t* convertString(const wchar_t* pUnicode)
	{
		static char_t convBuf[1024];
		wcstombs(convBuf, pUnicode, 1024);
		return convBuf;
	}

	const wchar_t* convertString(const char_t* pAnsi)
	{
		static wchar_t convBuf[1024];
		mbstowcs(convBuf, pAnsi, 1024);
		return convBuf;
	}
}