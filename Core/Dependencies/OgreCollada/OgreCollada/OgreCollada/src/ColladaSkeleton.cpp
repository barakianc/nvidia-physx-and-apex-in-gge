/*
-----------------------------------------------------------------------------
This source file is part of NESE
    (Networked Extensible Simulation Environment)

For the latest info, see http://www.clashofsteel.net/

Copyright (c) The Clash Of Steel Team
Also see acknowledgements in Readme.txt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------------------------------------------------------------------------
*/

#include "stdafx.h"

#include "FMath/FMVector2.h"
#include "FMath/FMVector3.h"
#include "FMath/FMVector4.h"
#include "FCDocument/FCDAsset.h"
#include "FCDocument/FCDAnimated.h"
#include "FCDocument/FCDAnimationCurve.h"
#include "FCDocument/FCDAnimationChannel.h"

namespace OgreCollada
{
	void makeTransform(const Ogre::Vector3& pos, const Ogre::Quaternion& rot, const Ogre::Vector3& scale, Ogre::Matrix4& mat)
	{
		Ogre::Matrix3 matRot;
		rot.ToRotationMatrix(matRot);
		mat = Ogre::Matrix4(matRot);
		mat.setTrans(pos);
		mat[0][0] = scale.x;
		mat[1][1] = scale.y;
		mat[2][2] = scale.z;
	}

	void ImpExpImpl::getNodeTransform(Ogre::Node* pNode, Ogre::Matrix4& rMat)
	{
		makeTransform(pNode->getPosition(), pNode->getOrientation(), pNode->getScale(), rMat);
	}

	void ImpExpImpl::setNodeTransform(Ogre::Node* pNode, Ogre::Matrix4& rMat)
	{
		pNode->setPosition(rMat.getTrans());
		pNode->setOrientation(rMat.extractQuaternion());
		pNode->setScale(rMat[0][0], rMat[1][1], rMat[2][2]);
	}

	void ImpExpImpl::populateSkeleton(Ogre::SkeletonPtr pSkeleton, Ogre::Bone* pParentBone, FCDSceneNode* pFCBone)
	{
		if (!pFCBone)
			return;

		Ogre::String boneName(pFCBone->GetName().c_str());
		Ogre::Bone* pChild = pSkeleton->createBone(boneName, m_nextBoneIndex++);
		pFCBone->SetUserHandle(pChild);
		pParentBone->addChild(pChild);

		// set initial state
		setNodeTransform(pChild, FMMatrix44::Identity);
		setNodeTransform(pChild, pFCBone);

		// add bone to bone-skeleton LUT
		m_boneToSkeletonLUT[pChild] = pSkeleton.getPointer();

		// add bone to bone-name-to bone LUT
		m_nameToBoneLUT[pFCBone->GetSubId().c_str()] = pChild;

		// sort out the animations on this bone for this skeleton
		createAnimationTracks(pChild, pFCBone);

		size_t childCount = pFCBone->GetChildrenCount();
		for (size_t i=0; i<childCount; ++i)
		{
			// TODO: IsJoint is deprecated, use GetJointFlag() instead
			if (pFCBone->GetChild(i)->IsJoint())
				populateSkeleton(pSkeleton, pChild, pFCBone->GetChild(i));
		}
	}

	//! Create an Ogre Skeleton from this root COLLADA joint
	Ogre::Skeleton* ImpExpImpl::createSkeleton(FCDSceneNode* pRootNode)
	{
		Ogre::SkeletonManager* pMgr = Ogre::SkeletonManager::getSingletonPtr();
		Ogre::SkeletonPtr pSkeleton = pMgr->create(Ogre::String(pRootNode->GetName().c_str()), m_resGroupName, true);

		m_nextBoneIndex = 0;
		Ogre::Bone* pRootBone = pSkeleton->createBone(
			Ogre::String(pRootNode->GetName().c_str()), m_nextBoneIndex++);

		pRootNode->SetUserHandle(pRootBone);

		// set initial state
		setNodeTransform(pRootBone, FMMatrix44::Identity);
		setNodeTransform(pRootBone, pRootNode);

		// add bone to bone-skeleton LUT
		m_boneToSkeletonLUT[pRootBone] = pSkeleton.getPointer();

		// add bone to name-to-bone LUT
		m_nameToBoneLUT[pRootNode->GetSubId().c_str()] = pRootBone;

		// sort out the animations on this bone for this skeleton
		createAnimationTracks(pRootBone, pRootNode);

		for (size_t i=0; i<pRootNode->GetChildrenCount(); ++i)
		{
			// TODO: IsJoint is deprecated, use GetJointFlag() instead
			if (pRootNode->GetChild(i)->IsJoint())
				populateSkeleton(pSkeleton, pRootBone, pRootNode->GetChild(i));
		}
		
		return pSkeleton.getPointer();
	}
}