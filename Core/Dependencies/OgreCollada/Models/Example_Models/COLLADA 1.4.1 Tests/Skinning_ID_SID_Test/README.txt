This folder contains a test package for SID and ID use in skinning.  It is used to verify that an importer is searching the proper namespace for joint names during skinning.

For additional information post messages on www.collada.org or mail collada@collada.org

These models are Copyright 2006 Sony Computer Entertainment Inc. and are distributed under the terms of the SCEA Shared Source License, available at http://research.scea.com/scea_shared_source_license.html

-----Running The Test-----

Attempt to load each of the 8 .dae files into your program one at a time, clearing the scene after each one.  

Tests 1, 2 should load without errors.  Tests 3, 4, 5 and 6 should fail to load, generate an error or render as a straight cylinder. Test 7 should produce a cylinder that bends to the right.  Test 8 should produce a cylinder that bends to the left.

None of the tests should cause a program to crash.  
If you get this result you have passed the test.

-----Explanation of Results-----

skintest_1_ID.dae 

The model should load and skin properly when you manipulate a joint.  This model uses an <IDREF_array> full of IDs to identify the joints to the skin controller.  If this fails to load it means you are not searching ID space when a <joint> <input> has semantic=JOINT" that points at an <IDREF_array>.

skintest_2_SID.dae

The model should load and skin properly when you manipulate a joint.  This model uses an <Name_array> full of SIDs to identify the joints to the skin controller.  If this fails to load it means you are not searching SID space when a <joint> <input> has semantic=JOINT" that points at an <Name_array>.

skintest_3_ID_FAIL.dae
THIS MODEL SHOULD FAIL TO LOAD.  If it loads successfully it indicates you are searching ID space for the values found in the <Name_array>.  This is INCORRECT, <Name_array> items are SIDs and should NOT match IDs.

skintest_4_SID_FAIL.dae
THIS MODEL SHOULD FAIL TO LOAD.  If it loads successfully it indicates you are searching SID space for the values found in the <IDREF_array>.  This is INCORRECT, <IDREF_array> items are IDs and should NOT match SIDs.

skintest_5_Name_FAIL.dae
THIS MODEL SHOULD FAIL TO LOAD.  If it loads successfully it indicates you are searching the values of name= parameters for the values found in a <Name_array>.  This is INCORRECT, <Name_array> items are SIDs and should NOT match name= parameters.

skintest_6_Name_FAIL.dae
THIS MODEL SHOULD FAIL TO LOAD.  If it loads successfully it indicates you are searching the values of name= parameters for the values found in a <IDREF_array>.  This is INCORRECT, <IDREF_array> items are IDs and should NOT match name= parameters.

skintest_7_SID_first.dae
The model contains two hierarchies, one that bends to the right and is identified by SIDs and a second that bends to the left and is identified by IDs.  The same strings are used for the IDs and SIDs.  This model uses a <Name_array> and should produce a skinned cylinder that bends to the right.  If you get a cylinder that bends to the left it indicates you are searching IDs when you should be searching for SIDs.  If the document fails to load it indicates your implementation has some problem with IDs and SIDs having the same name.  These should be in seperate namespaces so there should be no name collisions.

skintest_8_ID_first.dae
The model contains two hierarchies, one that bends to the right and is identified by SIDs and a second that bends to the left and is identified by IDs.  The same strings are used for the IDs and SIDs.  This model uses a <IDREF_array> and should produce a skinned cylinder that bends to the left.  If you get a cylinder that bends to the right it indicates you are searching SIDs when you should be searching for IDs.  If the document fails to load it indicates your implementation has some problem with IDs and SIDs having the same name.  These should be in seperate namespaces so there should be no name collisions.

-----ERROR REPORTING-----

If your program reports errors, for test 3 it should report something similar to the following.
In <controller> <skin> SID joint1 not found under ID joint1
In <controller> <skin> SID joint2 not found under ID joint1 
In <controller> <skin> SID joint3 not found under ID joint1 
In <controller> <skin> SID joint4 not found under ID joint1 
In <controller> <skin> SID joint5 not found under ID joint1 

For test 4 it should report something similar to the following
In <controller> <skin> ID sidjoint1 not found 
In <controller> <skin> ID sidjoint2 not found 
In <controller> <skin> ID sidjoint3 not found  
In <controller> <skin> ID sidjoint4 not found  
In <controller> <skin> ID sidjoint5 not found

For test 5 it should report something similer to the following
In <controller> <skin> SID name_joint1 not found under ID joint1
In <controller> <skin> SID name_joint2 not found under ID joint1 
In <controller> <skin> SID name_joint3 not found under ID joint1 
In <controller> <skin> SID name_joint4 not found under ID joint1 
In <controller> <skin> SID name_joint5 not found under ID joint1 

For test 6 it should report something similar to the following
In <controller> <skin> ID name_joint1 not found 
In <controller> <skin> ID name_joint2 not found 
In <controller> <skin> ID name_joint3 not found  
In <controller> <skin> ID name_joint4 not found  
In <controller> <skin> ID name_joint5 not found

