This tests for correct interpretation of the <transparency> tag when THERE IS NO <transparent> TAG PRESENT.
See page 8 of the COLLADA 1.4.1 readme for a full explanation of this.

This model contains a cube, cone and sphere against a yellow background plane.

The cube has transparency = 0.0 which is fully transparent, the cone has a value of 0.5 which should be 50% transparent and the sphere a value of 1.0 which should be fully opaque.  

Sorry there is no sample image in this package because currently NO renderer I have is importing this correctly.  Currently everyone seems to render everything in this sample as fully opaque.

For additional information post messages on www.collada.org or mail collada@collada.org

These models are Copyright 2006 Sony Computer Entertainment Inc. and are distributed under the terms of the SCEA Shared Source License, available at http://research.scea.com/scea_shared_source_license.html