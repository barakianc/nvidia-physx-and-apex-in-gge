This tests for correct interpretation of translate, rotate and scale tags in a hierarchy of nodes.

This model contains four sets of node hierarchies. Each hierarchy contains 6 nodes with an instance of a disk in each one.  There is also a directional light and a camera.

Viewed from left to right (through camera1) each node of the red hierarchy contains the same rotate, translate and scale producing a group of disks that bends to the left and gets gradually smaller.  The green hierarchy contains just a translate in each node.  The blue one a translate and scale and the yellow one a translate and rotate.

See the sample image for what this should look like.

For additional information post messages on www.collada.org or mail collada@collada.org

These models are Copyright 2006 Sony Computer Entertainment Inc. and are distributed under the terms of the SCEA Shared Source License, available at http://research.scea.com/scea_shared_source_license.html