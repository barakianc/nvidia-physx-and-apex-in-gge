This model is a "Moon Buggy" with an astronaut standing beside it.  The astronaut is skinned and animated and should wave at you if you play the animation.  

The model has been stripped of all <extra> tags and should be COLLADA 1.4.1 compliant.

The model contains diffuse textures and lightmaps (actually ambient occlusion maps) in the ambient channel.  The actual COLLADA lights are very dim.  If your software doesn't handle the lightmaps correctly, the model may appear black.

The model should display correctly in Maya and Feeling Viewer, XSI and MAX have problems with it that are being investigated.  Other software has not been tested yet, please report any problems you encounter directly to greg_corson@playstation.sony.com

For additional information post messages on www.collada.org or mail collada@collada.org

This model is Copyright 2006 Sony Computer Entertainment Inc. and are distributed under the terms of the SCEA Shared Source License, available at http://research.scea.com/scea_shared_source_license.html

The model was originaly created by Feeling Software www.feelingsoftware.com

--- KNOWN PROBLEMS ---

This model will not load into XSI 5.11 due to bugs in their importer that can't handle 1.4.1 texture linking.

Loading this model into 3ds Max 8 causes it to crash.

