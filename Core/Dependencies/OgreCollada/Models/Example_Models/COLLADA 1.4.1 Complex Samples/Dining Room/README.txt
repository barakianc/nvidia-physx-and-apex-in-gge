This model is a simple dining room with a table and chairs.  

The model has been stripped of all <extra> tags and should be COLLADA 1.4.1 compliant.

The model contains diffuse textures and lightmaps (actually ambient occlusion maps) in the ambient channel.  The actual COLLADA lights are very dim.  If your software doesn't handle the lightmaps correctly, the model may appear black.  In Maya, to see the effect in the modeling window you need to set "high quality rendering" and "use all lights".

The model should display correctly in Maya and Feeling Viewer, XSI and MAX have problems with it that are being investigated.  Other software has not been tested yet, please report any problems you encounter directly to greg_corson@playstation.sony.com

For additional information post messages on www.collada.org or mail collada@collada.org

This model is Copyright 2006 Sony Computer Entertainment Inc. and are distributed under the terms of the SCEA Shared Source License, available at http://research.scea.com/scea_shared_source_license.html

The model was originaly created by Feeling Software www.feelingsoftware.com

--- KNOWN PROBLEMS ---

This model will not load into XSI 5.11 due to bugs in their importer that can't handle 1.4.1 texture linking.

The model loads into Max but I can't get the lightmaps to render.

