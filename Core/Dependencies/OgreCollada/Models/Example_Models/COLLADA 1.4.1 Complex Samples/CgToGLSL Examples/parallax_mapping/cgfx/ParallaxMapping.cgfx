/************************************************************************/
/* Testing CGFX File for Normal Mapping                                 */
/************************************************************************/

//Rewritten for worldspace test



sampler2D ColorMap = sampler_state {
	MinFilter = LinearMipMapLinear;
	MagFilter = Linear;
	WrapS = WRAP;
	WrapT = WRAP;
};

sampler2D SpecularMap = sampler_state {
	MinFilter = LinearMipMapLinear;
	MagFilter = Linear;
	WrapS = WRAP;
	WrapT = WRAP;
};


//NormalMap w. Height in AlphaChannel

sampler2D ParallaxMap = sampler_state {
	MinFilter = LinearMipMapLinear;
	MagFilter = Linear;
	WrapS = Repeat;
	WrapT = Repeat;
};

//Tweakables

uniform float SpecExpon <
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 100.0;
	float UIStep = 0.5;
> = 30.0f;

uniform float depth
<
	string UIName = "Depth Factor";
	string UIWidget = "slider";
	float UIMin = 0.0f;
	float UIStep = 0.05f;
	float UIMax = 4.0f;	
> = 0.1;

uniform float LightIntensity <
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 1.0f;

uniform float4 LightColor = { 1.0f, 1.0f, 1.0f, 1.0f };
uniform float4 AmbientColor = { .2f, .2f, .2f, 1.0f };
uniform float4 SpecularColor = { 1.0f, 1.0f, 1.0f, 1.0f };

//Input - Output - Structs
struct appdataBump {
    float3 Position	: POSITION;
    float4 UV		: TEXCOORD0;
    float4 Normal	: NORMAL;
	float3 tangent : TEXCOORD6;
	float3 bitangent : TEXCOORD7;
};

struct vertexOutputBump {
    float4 HPosition	: POSITION;
    float4 TexCoord	: TEXCOORD0;
	float4 EyeNormal : TEXCOORD1;
    float4 EyeTangent	: TEXCOORD2;
    float4 EyeBitangent	: TEXCOORD3;
	float4 PosEyespace : TEXCOORD4;
	float4 LightVec : TEXCOORD5;
};

//DEBUG (as we cant dynamically calc. viewspace light-position
//static const float4 LightPos = { 0.0f, 0.0f, 0.0f, 1.0f };

//float4x4 mvpMatrx: WORLDVIEWPROJECTION;
//float4x4 mvMatrx : WORLDVIEW;

float4x4 mvpMatrx: WorldViewProjection;
float4x4 mvMatrx : WorldView;

//Testiong for viewmatrix
float4x4 viewMatrix : VIEW;

uniform float3 lightpos : LIGHTPOS1;

vertexOutputBump objectToEye_VP(appdataBump IN) {
    
	vertexOutputBump OUT;

	float3x3 mvRotOnly = float3x3(mvMatrx);

	OUT.EyeNormal.xyz = mul(mvRotOnly, IN.Normal.xyz);
	OUT.EyeTangent.xyz = mul(mvRotOnly, IN.tangent);
	OUT.EyeBitangent.xyz = mul(mvRotOnly, IN.bitangent);

    float4 posObjectSpace = float4(IN.Position.xyz,1.0);
   	float3 posEyeSpace = mul(mvMatrx, posObjectSpace).xyz;
    OUT.PosEyespace.xyz = posEyeSpace;
    OUT.TexCoord = IN.UV;
    OUT.HPosition = mul(mvpMatrx, posObjectSpace);

	//TESTING VIEWMATRIX

//transform into viewspace
	float4 viewLightPos = mul(viewMatrix, float4(lightpos, 1.0f));
	OUT.LightVec.xyz = (viewLightPos- posEyeSpace);

    return OUT;
}

float4 parallaxMapping_FP(vertexOutputBump IN) : COLOR {



	// view and light directions
	float3 v = normalize(IN.PosEyespace.xyz);
	float3 l = normalize(IN.LightVec.xyz);

	float2 uv = IN.TexCoord.xy;

	float4 color=tex2D(ColorMap,uv);
	//specular map
	float specularity = tex2D(SpecularMap, uv).x;

	// parallax code
	float3x3 tbn = float3x3(normalize(IN.EyeTangent.xyz),normalize(IN.EyeBitangent.xyz),normalize(IN.EyeNormal.xyz) );
	float height = tex2D(ParallaxMap,uv).w * 0.06 - 0.03;
	height = height * depth;

	uv += height * mul(tbn,v);
	float4 normal=tex2D(ParallaxMap,uv);
	normal.xyz-=0.5;
	
	normal.xyz=normalize(normal.x*normalize(IN.EyeTangent.xyz)-normal.y*normalize(IN.EyeBitangent.xyz)+normal.z*normalize(IN.EyeNormal.xyz) );
	//uv.y = 1 - uv.y;




	float3 Hn = normalize(-v + l);
	float ldn = dot(l,normal);
	ldn = (max(0.0,ldn)) * LightIntensity;
	float hdn = pow(max(0,dot(Hn,normal)),SpecExpon)*specularity;
	
	float4 diffContrib = (ldn*LightColor);
	float4 specContrib = ((ldn * hdn)*LightColor);


	float4 result = { .0f, .0f, .0f, 1.0f };

	result.xyz = color.xyz*(diffContrib.xyz + AmbientColor + specContrib*SpecularColor.xyz).xyz;
	//result.xyz = normal;
	return result;
}

technique ParallaxMapping {
	pass {
		DepthFunc = LEqual;
		DepthTestEnable = true;
		VertexProgram = compile arbvp1 objectToEye_VP();
		FragmentProgram = compile arbfp1 parallaxMapping_FP();
	}
}