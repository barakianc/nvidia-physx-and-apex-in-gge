/*
CgToGLSL-Log:

Cg Preprocessing:
	Replaced the Matrix mvpMatrx with gl_ModelViewProjectionMatrixTranspose having the semantic WORLDVIEWPROJECTION using the transposed version. .
	The original declaration of mvpMatrx was removed.

	Replaced the Matrix mvMatrx with gl_ModelViewMatrixTranspose having the semantic WORLDVIEW using the transposed version. .
	The original declaration of mvMatrx was removed.


GLSL PostProcessing: 
	Restored uniform pend_s2_ColorMap with ColorMap 3 times.
	Restored uniform pend_s2_SpecularMap with SpecularMap 3 times.
	Restored uniform pend_s2_ParallaxMap with ParallaxMap 4 times.
	Restored uniform pend_s2_SpecExpon with SpecExpon 3 times.
	Restored uniform pend_s2_depth with depth 3 times.
	Restored uniform pend_s2_LightIntensity with LightIntensity 3 times.
	Restored uniform pend_s2_LightColor with LightColor 4 times.
	Restored uniform pend_s2_AmbientColor with AmbientColor 3 times.
	Restored uniform pend_s2_SpecularColor with SpecularColor 3 times.	
Found a symbol viewMatrix in CGC-Header that was not used in src, skipped restoration.
	Didn't find any declaration of the translation of uniform vec4 viewMatrix
	Restored the matrix declaration of float4x4 viewMatrix.
	Replaced all "__" with "_".

Cg Transposing Matrices: 
	Transposed the matrix viewMatrix with simple Cg-transpose.

*/
// transl output by Cg compiler
// cgc version 1.5.0014, build date Sep 18 2006 20:41:01
// command line args: -q -profile glslf -entry parallaxMapping_FP
//vendor NVIDIA Corporation
//version 1.5.0.14
//profile glslf
//program parallaxMapping_FP
//semantic ColorMap
//semantic SpecularMap
//semantic ParallaxMap
//semantic SpecExpon
//semantic depth
//semantic LightIntensity
//semantic LightColor
//semantic AmbientColor
//semantic SpecularColor
//semantic viewMatrix : VIEW
//semantic lightpos : LIGHTPOS1
//var sampler2D ColorMap :  : ColorMap : -1 : 1
//var sampler2D SpecularMap :  : SpecularMap : -1 : 1
//var sampler2D ParallaxMap :  : ParallaxMap : -1 : 1
//var float SpecExpon :  : SpecExpon : -1 : 1
//var float depth :  : depth : -1 : 1
//var float LightIntensity :  : LightIntensity : -1 : 1
//var float4 LightColor :  : LightColor : -1 : 1
//var float4 AmbientColor :  : AmbientColor : -1 : 1
//var float4 SpecularColor :  : SpecularColor : -1 : 1
//var float4x4 viewMatrix : VIEW : , 4 : -1 : 1
//var float3 lightpos : LIGHTPOS1 :  : -1 : 0
//var float4 IN.HPosition : $vin.POSITION :  : 0 : 0
//var float4 IN.TexCoord : $vin.TEXCOORD0 : TEXCOORD0 : 0 : 1
//var float4 IN.EyeNormal : $vin.TEXCOORD1 : TEXCOORD1 : 0 : 1
//var float4 IN.EyeTangent : $vin.TEXCOORD2 : TEXCOORD2 : 0 : 1
//var float4 IN.EyeBitangent : $vin.TEXCOORD3 : TEXCOORD3 : 0 : 1
//var float4 IN.PosEyespace : $vin.TEXCOORD4 : TEXCOORD4 : 0 : 1
//var float4 IN.LightVec : $vin.TEXCOORD5 : TEXCOORD5 : 0 : 1
//var float4 parallaxMapping_FP : $vout.COLOR : COLOR : -1 : 1
//default SpecExpon = 30
//default depth = 0.1
//default LightIntensity = 1
//default LightColor = 1 1 1 1
//default AmbientColor = 0.2 0.2 0.2 1
//default SpecularColor = 1 1 1 1

struct appdataBump {
    vec4 pend_UV;
    vec4 pend_Normal;
    vec3 pend_tangent;
    vec3 pend_bitangent;
};

struct vertexOutputBump {
    vec4 pend_TexCoord;
    vec4 pend_EyeNormal;
    vec4 pend_EyeTangent;
    vec4 pend_EyeBitangent;
    vec4 pend_PosEyespace;
    vec4 pend_LightVec;
};

vec3 cash0_pend_s3_temp0009;
vec3 cash0_pend_s3_temp0011;
vec3 cash0_pend_s3_temp0017;
vec3 cash0_pend_s3_temp0019;
vec3 cash0_pend_s3_temp0021;
vec3 dash1_pend_s3_r_0026;
vec3 cash0_pend_s3_temp0029;
vec3 cash0_pend_s3_temp0031;
vec3 cash0_pend_s3_temp0033;
vec3 cash0_pend_s3_temp0035;
vec3 dash1_pend_s3_v_0036;
vec3 cash0_pend_s3_temp0037;
vec3 dash1_pend_s3_v_0038;
float dash1_pend_s3_b_0046;
float dash1_pend_s3_x_0048;
float dash1_pend_s3_x_0052;
float dash1_pend_s3_x_0056;
float dash1_pend_s3_x_0060;
float dash1_pend_s3_x_0064;
float dash1_pend_s3_x_0068;
float dash1_pend_s3_x_0078;
float dash1_pend_s3_x_0082;
float dash1_pend_s3_x_0086;
float dash1_pend_s3_x_0090;
float dash1_pend_s3_x_0094;
uniform sampler2D ColorMap;
uniform sampler2D SpecularMap;
uniform sampler2D ParallaxMap;
uniform float SpecExpon;
uniform float depth;
uniform float LightIntensity;
uniform vec4 LightColor;
uniform vec4 AmbientColor;
uniform vec4 SpecularColor;

 // main procedure, the original name was parallaxMapping_FP
void main()
{

    vec2 pend_s4_uv;
    vec4 pend_s4_color;
    float pend_s4_specularity;
    float pend_s4_height;
    vec4 pend_s4_normal;
    float pend_s4_ldn;
    float pend_s4_hdn;
    vec4 pend_s4_diffContrib;
    vec4 pend_s4_specContrib;
    vec4 pend_s4_result;

    dash1_pend_s3_x_0052 = dot(gl_TexCoord[4].xyz, gl_TexCoord[4].xyz);
    cash0_pend_s3_temp0009 = inversesqrt(dash1_pend_s3_x_0052)*gl_TexCoord[4].xyz;
    dash1_pend_s3_x_0056 = dot(gl_TexCoord[5].xyz, gl_TexCoord[5].xyz);
    cash0_pend_s3_temp0011 = inversesqrt(dash1_pend_s3_x_0056)*gl_TexCoord[5].xyz;
    pend_s4_color = texture2D(ColorMap, gl_TexCoord[0].xy);
    pend_s4_specularity = texture2D(SpecularMap, gl_TexCoord[0].xy).x;
    dash1_pend_s3_x_0060 = dot(gl_TexCoord[2].xyz, gl_TexCoord[2].xyz);
    cash0_pend_s3_temp0017 = inversesqrt(dash1_pend_s3_x_0060)*gl_TexCoord[2].xyz;
    dash1_pend_s3_x_0064 = dot(gl_TexCoord[3].xyz, gl_TexCoord[3].xyz);
    cash0_pend_s3_temp0019 = inversesqrt(dash1_pend_s3_x_0064)*gl_TexCoord[3].xyz;
    dash1_pend_s3_x_0068 = dot(gl_TexCoord[1].xyz, gl_TexCoord[1].xyz);
    cash0_pend_s3_temp0021 = inversesqrt(dash1_pend_s3_x_0068)*gl_TexCoord[1].xyz;
    pend_s4_height = texture2D(ParallaxMap, gl_TexCoord[0].xy).w*0.06 - 0.03;
    pend_s4_height = pend_s4_height*depth;
    dash1_pend_s3_r_0026.x = dot(cash0_pend_s3_temp0017, cash0_pend_s3_temp0009);
    dash1_pend_s3_r_0026.y = dot(cash0_pend_s3_temp0019, cash0_pend_s3_temp0009);
    dash1_pend_s3_r_0026.z = dot(cash0_pend_s3_temp0021, cash0_pend_s3_temp0009);
    pend_s4_uv = gl_TexCoord[0].xy + (pend_s4_height*dash1_pend_s3_r_0026).xy;
    pend_s4_normal = texture2D(ParallaxMap, pend_s4_uv);
    pend_s4_normal.xyz = pend_s4_normal.xyz - 0.5;
    dash1_pend_s3_x_0078 = dot(gl_TexCoord[2].xyz, gl_TexCoord[2].xyz);
    cash0_pend_s3_temp0029 = inversesqrt(dash1_pend_s3_x_0078)*gl_TexCoord[2].xyz;
    dash1_pend_s3_x_0082 = dot(gl_TexCoord[3].xyz, gl_TexCoord[3].xyz);
    cash0_pend_s3_temp0031 = inversesqrt(dash1_pend_s3_x_0082)*gl_TexCoord[3].xyz;
    dash1_pend_s3_x_0086 = dot(gl_TexCoord[1].xyz, gl_TexCoord[1].xyz);
    cash0_pend_s3_temp0033 = inversesqrt(dash1_pend_s3_x_0086)*gl_TexCoord[1].xyz;
    dash1_pend_s3_v_0036 = pend_s4_normal.x*cash0_pend_s3_temp0029 - pend_s4_normal.y*cash0_pend_s3_temp0031 + pend_s4_normal.z*cash0_pend_s3_temp0033;
    dash1_pend_s3_x_0090 = dot(dash1_pend_s3_v_0036, dash1_pend_s3_v_0036);
    cash0_pend_s3_temp0035 = inversesqrt(dash1_pend_s3_x_0090)*dash1_pend_s3_v_0036;
    dash1_pend_s3_v_0038 = -cash0_pend_s3_temp0009 + cash0_pend_s3_temp0011;
    dash1_pend_s3_x_0094 = dot(dash1_pend_s3_v_0038, dash1_pend_s3_v_0038);
    cash0_pend_s3_temp0037 = inversesqrt(dash1_pend_s3_x_0094)*dash1_pend_s3_v_0038;
    pend_s4_ldn = dot(cash0_pend_s3_temp0011, cash0_pend_s3_temp0035);
    pend_s4_ldn = max(0.0, pend_s4_ldn)*LightIntensity;
    dash1_pend_s3_b_0046 = dot(cash0_pend_s3_temp0037, cash0_pend_s3_temp0035);
    dash1_pend_s3_x_0048 = max(0.0, dash1_pend_s3_b_0046);
    pend_s4_hdn = pow(dash1_pend_s3_x_0048, SpecExpon)*pend_s4_specularity;
    pend_s4_diffContrib = pend_s4_ldn*LightColor;
    pend_s4_specContrib = pend_s4_ldn*pend_s4_hdn*LightColor;
    pend_s4_result = vec4( 0.0, 0.0, 0.0, 1.0);
    pend_s4_result.xyz = pend_s4_color.xyz*(pend_s4_diffContrib.xyz + AmbientColor.xyz + pend_s4_specContrib.xyz*SpecularColor.xyz).xyz;
    gl_FragColor = pend_s4_result;
    return;
} // main end
