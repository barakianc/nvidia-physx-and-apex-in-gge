Simple Phong (separate Cg) Example *
************************************

This COLLADAFX material uses a very simple phong perpixel shader with
one direction light source. It was mainly used to test conversion of 
separated Cg-Shaders instead of the CGFX files like in the other
examples. 

dae/scene_maya.dae:
	Exported Maya-Scene from COLLADAMAYA 3.01 [1] containing COLLADAFX profile_CG

dae/scene_glsl.dae:
	Converted scene_maya.dae containing a translation in profile_GLSL
	Use the experimental dae-Reader for OpenSceneGraph [2, 3]
	Note this files uses embedded GLSL code, not external references like most
	of the other examples.

cg/: 
	Contains vertex and fragment shader files in CG

References:

	[1] http://www.feelingsoftware.com
	[2] http://sourceforge.net/projects/collada-cg2glsl
	[3] http://www.openscenegraph.net