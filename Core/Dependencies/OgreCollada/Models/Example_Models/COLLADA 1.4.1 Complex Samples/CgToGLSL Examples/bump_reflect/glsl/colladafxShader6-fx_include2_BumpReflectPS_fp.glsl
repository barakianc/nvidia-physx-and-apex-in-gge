/*
CgToGLSL-Log:

Cg Preprocessing:
	Replaced the Matrix WVPXf with gl_ModelViewProjectionMatrixTranspose having the semantic WORLDVIEWPROJECTION using the transposed version. .
	The original declaration of WVPXf was removed.


GLSL PostProcessing: 	
Found a symbol MVXf in CGC-Header that was not used in src, skipped restoration.
	Didn't find any declaration of the translation of uniform vec4 MVXf
	Restored the matrix declaration of float4x4 MVXf.	
Found a symbol ViewITXf in CGC-Header that was not used in src, skipped restoration.
	Didn't find any declaration of the translation of uniform vec4 ViewITXf
	Restored the matrix declaration of float4x4 ViewITXf.
	Restored uniform pend_s2_BumpHeight with BumpHeight 3 times.
	Restored uniform pend_s2_NormalSampler with NormalSampler 3 times.
	Restored uniform pend_s2_EnvSampler with EnvSampler 3 times.
	Replaced all "__" with "_".

Cg Transposing Matrices: 
	Transposed the matrix MVXf with simple Cg-transpose.
	Transposed the matrix ViewITXf with simple Cg-transpose.

*/
// transl output by Cg compiler
// cgc version 1.5.0014, build date Sep 18 2006 20:41:01
// command line args: -q -profile glslf -entry BumpReflectPS
//vendor NVIDIA Corporation
//version 1.5.0.14
//profile glslf
//program BumpReflectPS
//semantic Category
//semantic keywords
//semantic description
//semantic MVXf : WORLD
//semantic ViewITXf : VIEWINVERSETRANSPOSE
//semantic BumpHeight
//semantic NormalSampler
//semantic EnvSampler
//var string Category :  :  : -1 : 0
//var string keywords :  :  : -1 : 0
//var string description :  :  : -1 : 0
//var float4x4 MVXf : WORLD : , 4 : -1 : 1
//var float4x4 ViewITXf : VIEWINVERSETRANSPOSE : , 4 : -1 : 1
//var float BumpHeight :  : BumpHeight : -1 : 1
//var sampler2D NormalSampler :  : NormalSampler : -1 : 1
//var samplerCUBE EnvSampler :  : EnvSampler : -1 : 1
//var float4 IN.Position : $vin.POSITION :  : 0 : 0
//var float2 IN.TexCoord : $vin.TEXCOORD0 : TEXCOORD0 : 0 : 1
//var float4 IN.EyeNormal : $vin.TEXCOORD1 : TEXCOORD1 : 0 : 1
//var float4 IN.EyeTangent : $vin.TEXCOORD2 : TEXCOORD2 : 0 : 1
//var float4 IN.EyeBitangent : $vin.TEXCOORD3 : TEXCOORD3 : 0 : 1
//var float4 IN.PosEyespace : $vin.TEXCOORD5 : TEXCOORD5 : 0 : 1
//var float4 IN.CamPos : $vin.TEXCOORD6 : TEXCOORD6 : 0 : 1
//var float4 BumpReflectPS : $vout.COLOR : COLOR : -1 : 1
//default Category = "Effects/Cg/Bump"
//default keywords = "bumpmap,texture"
//default description = "Bumpy-shiny"
//default BumpHeight = 0.5

struct a2v {
    vec2 pend_TexCoord;
    vec3 pend_Tangent;
    vec3 pend_Binormal;
    vec3 pend_Normal;
};

struct v2f {
    vec2 pend_TexCoord;
    vec4 pend_EyeNormal;
    vec4 pend_EyeTangent;
    vec4 pend_EyeBitangent;
    vec4 pend_PosEyespace;
    vec4 pend_CamPos;
};

vec4 pend_s3_ret_0;
vec4 cash0_pend_s3_temp0008;
vec4 cash0_pend_s3_temp0010;
vec4 cash0_pend_s3_temp0012;
vec3 cash0_pend_s3_temp0014;
vec3 dash1_pend_s3_v_0015;
vec3 cash0_pend_s3_temp0016;
float dash1_pend_s3_x_0023;
float dash1_pend_s3_x_0027;
float dash1_pend_s3_x_0031;
float dash1_pend_s3_x_0035;
uniform float BumpHeight;
uniform sampler2D NormalSampler;
uniform samplerCube EnvSampler;

 // main procedure, the original name was BumpReflectPS
void main()
{

    vec3 pend_s4_bumpNormal;
    vec3 pend_s4_bumps;
    vec3 pend_s4_viewPosToEye;

    pend_s4_bumpNormal = texture2D(NormalSampler, gl_TexCoord[0].xy).xyz;
    pend_s4_bumps = BumpHeight*(pend_s4_bumpNormal.xyz - vec3( 0.5, 0.5, 0.5));
    dash1_pend_s3_x_0023 = dot(gl_TexCoord[1], gl_TexCoord[1]);
    cash0_pend_s3_temp0008 = inversesqrt(dash1_pend_s3_x_0023)*gl_TexCoord[1];
    dash1_pend_s3_x_0027 = dot(gl_TexCoord[2], gl_TexCoord[2]);
    cash0_pend_s3_temp0010 = inversesqrt(dash1_pend_s3_x_0027)*gl_TexCoord[2];
    dash1_pend_s3_x_0031 = dot(gl_TexCoord[3], gl_TexCoord[3]);
    cash0_pend_s3_temp0012 = inversesqrt(dash1_pend_s3_x_0031)*gl_TexCoord[3];
    dash1_pend_s3_v_0015 = pend_s4_bumps.x*cash0_pend_s3_temp0010.xyz - pend_s4_bumps.y*cash0_pend_s3_temp0012.xyz + pend_s4_bumps.z*cash0_pend_s3_temp0008.xyz;
    dash1_pend_s3_x_0035 = dot(dash1_pend_s3_v_0015, dash1_pend_s3_v_0015);
    cash0_pend_s3_temp0014 = inversesqrt(dash1_pend_s3_x_0035)*dash1_pend_s3_v_0015;
    pend_s4_viewPosToEye = gl_TexCoord[5].xyz - gl_TexCoord[6].xyz;
    cash0_pend_s3_temp0016 = pend_s4_viewPosToEye - 2.0*cash0_pend_s3_temp0014*dot(cash0_pend_s3_temp0014, pend_s4_viewPosToEye);
    pend_s3_ret_0 = textureCube(EnvSampler, cash0_pend_s3_temp0016);
    gl_FragColor = pend_s3_ret_0;
    return;
} // main end
