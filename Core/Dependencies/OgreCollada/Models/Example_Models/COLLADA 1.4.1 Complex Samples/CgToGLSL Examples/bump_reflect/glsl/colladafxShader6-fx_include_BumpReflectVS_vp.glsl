/*
CgToGLSL-Log:

Cg Preprocessing:
	Replaced the Matrix WVPXf with gl_ModelViewProjectionMatrixTranspose having the semantic WORLDVIEWPROJECTION using the transposed version. .
	The original declaration of WVPXf was removed.


GLSL PostProcessing: 
	Restored uniform pend_s2_MVXf with MVXf 41 times.
	Restored the matrix declaration of float4x4 MVXf.
	Restored uniform pend_s2_ViewITXf with ViewITXf 5 times.
	Restored the matrix declaration of float4x4 ViewITXf.
	Replaced all "__" with "_".

Cg Transposing Matrices: 
	Transposed the matrix MVXf with simple Cg-transpose.
	Transposed the matrix ViewITXf with simple Cg-transpose.

*/
// transl output by Cg compiler
// cgc version 1.5.0014, build date Sep 18 2006 20:41:01
// command line args: -q -profile glslv -entry BumpReflectVS
//vendor NVIDIA Corporation
//version 1.5.0.14
//profile glslv
//program BumpReflectVS
//semantic gl_ModelViewProjectionMatrixTranspose : STATE.MATRIX.MVP
//semantic Category
//semantic keywords
//semantic description
//semantic MVXf : WORLD
//semantic ViewITXf : VIEWINVERSETRANSPOSE
//semantic BumpHeight
//semantic NormalSampler
//semantic EnvSampler
//var float4x4 gl_ModelViewProjectionMatrixTranspose : STATE.MATRIX.MVP : gl_ModelViewProjectionMatrixTranspose[0], 4 : -1 : 1
//var string Category :  :  : -1 : 0
//var string keywords :  :  : -1 : 0
//var string description :  :  : -1 : 0
//var float4x4 MVXf : WORLD : MVXf[0], 4 : -1 : 1
//var float4x4 ViewITXf : VIEWINVERSETRANSPOSE : ViewITXf[0], 4 : -1 : 1
//var float BumpHeight :  :  : -1 : 0
//var sampler2D NormalSampler :  :  : -1 : 0
//var samplerCUBE EnvSampler :  :  : -1 : 0
//var float4 IN.Position : $vin.POSITION : POSITION : 0 : 1
//var float2 IN.TexCoord : $vin.TEXCOORD0 : TEXCOORD0 : 0 : 1
//var float3 IN.Tangent : $vin.TEXCOORD6 : TEXCOORD6 : 0 : 1
//var float3 IN.Binormal : $vin.TEXCOORD7 : TEXCOORD7 : 0 : 1
//var float3 IN.Normal : $vin.NORMAL : NORMAL : 0 : 1
//var float4 BumpReflectVS.Position : $vout.POSITION : POSITION : -1 : 1
//var float2 BumpReflectVS.TexCoord : $vout.TEXCOORD0 : TEXCOORD0 : -1 : 1
//var float4 BumpReflectVS.EyeNormal : $vout.TEXCOORD1 : TEXCOORD1 : -1 : 1
//var float4 BumpReflectVS.EyeTangent : $vout.TEXCOORD2 : TEXCOORD2 : -1 : 1
//var float4 BumpReflectVS.EyeBitangent : $vout.TEXCOORD3 : TEXCOORD3 : -1 : 1
//var float4 BumpReflectVS.PosEyespace : $vout.TEXCOORD5 : TEXCOORD5 : -1 : 1
//var float4 BumpReflectVS.CamPos : $vout.TEXCOORD6 : TEXCOORD6 : -1 : 1
//default Category = "Effects/Cg/Bump"
//default keywords = "bumpmap,texture"
//default description = "Bumpy-shiny"
//default BumpHeight = 0.5

struct a2v {
    vec4 pend_Position;
    vec2 pend_TexCoord;
    vec3 pend_Tangent;
    vec3 pend_Binormal;
    vec3 pend_Normal;
};

struct v2f {
    vec4 pend_Position;
    vec2 pend_TexCoord;
    vec4 pend_EyeNormal;
    vec4 pend_EyeTangent;
    vec4 pend_EyeBitangent;
    vec4 pend_PosEyespace;
    vec4 pend_CamPos;
};

vec4 dash1_pend_s3_r_0009;
vec4 dash1_pend_s3_r_0011;
vec3 dash1_pend_s3_r_0013;
vec3 dash1_pend_s3_r_0015;
vec3 dash1_pend_s3_r_0017;
vec4 at0_pend_s3_TMP52;
vec4 at0_pend_s3_TMP53;
vec4 at0_pend_s3_TMP54;
vec3 at0_pend_s3_TMP55;
vec3 at0_pend_s3_TMP56;
vec3 at0_pend_s3_TMP57;
vec3 at0_pend_s3_TMP58;
vec3 at0_pend_s3_TMP59;
vec3 at0_pend_s3_TMP60;
vec3 at0_pend_s3_TMP61;
vec3 at0_pend_s3_TMP62;
vec3 at0_pend_s3_TMP63;
vec3 at0_pend_s3_TMP64;
uniform mat4 MVXf;
uniform mat4 ViewITXf;

 // main procedure, the original name was BumpReflectVS
void main()
{

    v2f pend_s4_OUT;

    dash1_pend_s3_r_0009.x = dot(gl_ModelViewProjectionMatrixTranspose[0], gl_Vertex);
    dash1_pend_s3_r_0009.y = dot(gl_ModelViewProjectionMatrixTranspose[1], gl_Vertex);
    dash1_pend_s3_r_0009.z = dot(gl_ModelViewProjectionMatrixTranspose[2], gl_Vertex);
    dash1_pend_s3_r_0009.w = dot(gl_ModelViewProjectionMatrixTranspose[3], gl_Vertex);
    at0_pend_s3_TMP52.x = MVXf[0].x;
    at0_pend_s3_TMP52.y = MVXf[1].x;
    at0_pend_s3_TMP52.z = MVXf[2].x;
    at0_pend_s3_TMP52.w = MVXf[3].x;
    dash1_pend_s3_r_0011.x = dot(at0_pend_s3_TMP52, gl_Vertex);
    at0_pend_s3_TMP53.x = MVXf[0].y;
    at0_pend_s3_TMP53.y = MVXf[1].y;
    at0_pend_s3_TMP53.z = MVXf[2].y;
    at0_pend_s3_TMP53.w = MVXf[3].y;
    dash1_pend_s3_r_0011.y = dot(at0_pend_s3_TMP53, gl_Vertex);
    at0_pend_s3_TMP54.x = MVXf[0].z;
    at0_pend_s3_TMP54.y = MVXf[1].z;
    at0_pend_s3_TMP54.z = MVXf[2].z;
    at0_pend_s3_TMP54.w = MVXf[3].z;
    dash1_pend_s3_r_0011.z = dot(at0_pend_s3_TMP54, gl_Vertex);
    pend_s4_OUT.pend_PosEyespace.xyz = dash1_pend_s3_r_0011.xyz;
    at0_pend_s3_TMP55.x = MVXf[0].x;
    at0_pend_s3_TMP55.y = MVXf[1].x;
    at0_pend_s3_TMP55.z = MVXf[2].x;
    dash1_pend_s3_r_0013.x = dot(at0_pend_s3_TMP55, gl_Normal.xyz);
    at0_pend_s3_TMP56.x = MVXf[0].y;
    at0_pend_s3_TMP56.y = MVXf[1].y;
    at0_pend_s3_TMP56.z = MVXf[2].y;
    dash1_pend_s3_r_0013.y = dot(at0_pend_s3_TMP56, gl_Normal.xyz);
    at0_pend_s3_TMP57.x = MVXf[0].z;
    at0_pend_s3_TMP57.y = MVXf[1].z;
    at0_pend_s3_TMP57.z = MVXf[2].z;
    dash1_pend_s3_r_0013.z = dot(at0_pend_s3_TMP57, gl_Normal.xyz);
    pend_s4_OUT.pend_EyeNormal.xyz = dash1_pend_s3_r_0013;
    at0_pend_s3_TMP58.x = MVXf[0].x;
    at0_pend_s3_TMP58.y = MVXf[1].x;
    at0_pend_s3_TMP58.z = MVXf[2].x;
    dash1_pend_s3_r_0015.x = dot(at0_pend_s3_TMP58, gl_MultiTexCoord6.xyz);
    at0_pend_s3_TMP59.x = MVXf[0].y;
    at0_pend_s3_TMP59.y = MVXf[1].y;
    at0_pend_s3_TMP59.z = MVXf[2].y;
    dash1_pend_s3_r_0015.y = dot(at0_pend_s3_TMP59, gl_MultiTexCoord6.xyz);
    at0_pend_s3_TMP60.x = MVXf[0].z;
    at0_pend_s3_TMP60.y = MVXf[1].z;
    at0_pend_s3_TMP60.z = MVXf[2].z;
    dash1_pend_s3_r_0015.z = dot(at0_pend_s3_TMP60, gl_MultiTexCoord6.xyz);
    pend_s4_OUT.pend_EyeTangent.xyz = dash1_pend_s3_r_0015;
    at0_pend_s3_TMP61.x = MVXf[0].x;
    at0_pend_s3_TMP61.y = MVXf[1].x;
    at0_pend_s3_TMP61.z = MVXf[2].x;
    dash1_pend_s3_r_0017.x = dot(at0_pend_s3_TMP61, gl_MultiTexCoord7.xyz);
    at0_pend_s3_TMP62.x = MVXf[0].y;
    at0_pend_s3_TMP62.y = MVXf[1].y;
    at0_pend_s3_TMP62.z = MVXf[2].y;
    dash1_pend_s3_r_0017.y = dot(at0_pend_s3_TMP62, gl_MultiTexCoord7.xyz);
    at0_pend_s3_TMP63.x = MVXf[0].z;
    at0_pend_s3_TMP63.y = MVXf[1].z;
    at0_pend_s3_TMP63.z = MVXf[2].z;
    dash1_pend_s3_r_0017.z = dot(at0_pend_s3_TMP63, gl_MultiTexCoord7.xyz);
    pend_s4_OUT.pend_EyeBitangent.xyz = dash1_pend_s3_r_0017;
    at0_pend_s3_TMP64.x = ViewITXf[0].w;
    at0_pend_s3_TMP64.y = ViewITXf[1].w;
    at0_pend_s3_TMP64.z = ViewITXf[2].w;
    pend_s4_OUT.pend_CamPos.xyz = at0_pend_s3_TMP64;
    gl_TexCoord[3] = pend_s4_OUT.pend_EyeBitangent;
    gl_TexCoord[6] = pend_s4_OUT.pend_CamPos;
    gl_TexCoord[5] = pend_s4_OUT.pend_PosEyespace;
    gl_TexCoord[0].xy = gl_MultiTexCoord0.xy;
    gl_TexCoord[2] = pend_s4_OUT.pend_EyeTangent;
    gl_Position = dash1_pend_s3_r_0009;
    gl_TexCoord[1] = pend_s4_OUT.pend_EyeNormal;
    return;
} // main end
