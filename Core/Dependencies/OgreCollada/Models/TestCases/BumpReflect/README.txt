Simple Bumpy Environment Mapping Example *
******************************************

A small example file showing a bumpReflect - effect (based on NVIDIA's BumpReflect.cgfx [1]).
The shader was modified to use OpenGL matrices only (no seperation of ViewMatrix necessary).

dae/scene_manual_cube.dae:

	This file was created in Maya 8.0 and exported with COLLADAMAYA 3.01 [2]. It contains
	the shader-code in profile_CG of COLLADAFX. To correctly load the translated profile_GLSL
	version of the file, I had to rewrite the initialization of the CUBE-Map surface (as
	OpenSceneGraph does not support DDS-Cubemap compound images). Although this is perfectly
	compliant profile_CG (and profile_GLSL Code), this modification prevents the file being 
	loaded in current profile_CG loaders.
		
	Also note:
		   
		.) Tangent/Bitangent space is not exported with the COLLADA-file. Although the COLLADAMAYA
		   3.01 has the option to Export "Texture Tangents" it seemed not to work correctly, so 
		   it's the loader's job to generate tangent space.
	
dae/scene_glsl.dae
	The scene_manual_cube.dae with profile_CG converted into profile_GLSL version of the COLLADAFX Shaders. 
	It was generated with the COLLADA CgToGLSL Conditioner [3] and is loadable with the experimental
	profile_GLSL reader of OpenSceneGraph [4]. You can get the patch for the COLLADA reader 
	at the home of the COLLADA CgToGLSL Conditioner [3].
	
cgfx/:
	contains the modified CGFX - file of a bumpy shiny effect.
	
textures/: 
	Textures for the sample. 

References:

	[1]	http://developer.nvidia.com
	[2] http://www.feelingsoftware.com
	[3] http://sourceforge.net/projects/collada-cg2glsl
	[4] http://www.openscenegraph.net