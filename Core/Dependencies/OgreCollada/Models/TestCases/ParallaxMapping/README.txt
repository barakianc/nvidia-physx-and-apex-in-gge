Simple Parallax Mapping Example *
*********************************

This example shows the popular parallax mapping, which is a small extension to the 
usual normal mapping.

dae/scene.dae:
	This file was created in Maya 8.0 and exported with COLLADAMAYA 3.01 [1]. It contains
	the parallax code in profile_CG of COLLADAFX. Note that I have not touched file after
	the export (no manual modifications), so this file has some issues that are related to
	the COLLADAMAYA 3.01 Exporter:
		
		.) Each material has it's own <effect>-element, even it's based on the same effect. 
		   I was not able to figure out correctly instancing a material in COLLADAMAYA. 

		.) The <bind>-array contains symbols, that are actually not used in the shader. In the 
		   CgToGLSL Conditioner turn the option "DBG: Check Bind" to "false" to force the conversion
		   proces not to validate the bind-symbols.
		   
		.) Tangent/Bitangent space is not exported with the COLLADA-file. Although the COLLADAMAYA
		   3.01 has the option to Export "Texture Tangents" it seemed not to work correctly, so 
		   it's the loader's job to generate tangent space.
	
dae/scene_glsl.dae
	The scene.dae with an additional profile_GLSL version of the parallax shader-code. It was 
	generated with the COLLADA CgToGLSL Conditioner [2] and is loadable with the experimental
	profile_GLSL reader of OpenSceneGraph [3]. You can get the patch for the COLLADA reader 
	at the home of the COLLADA CgToGLSL Conditioner [2].
	
cgfx/:
	contains the CGFX - file for the Parallax Example Shader.
	
textures/: 
	Textures for the sample. 

References:

	[1] http://www.feelingsoftware.com
	[2] http://sourceforge.net/projects/collada-cg2glsl
	[3] http://www.openscenegraph.net