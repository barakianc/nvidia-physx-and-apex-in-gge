/*
CgToGLSL-Log:

Cg Preprocessing:
	Replaced the Matrix mvpMatrx with gl_ModelViewProjectionMatrixTranspose having the semantic WORLDVIEWPROJECTION using the transposed version. .
	The original declaration of mvpMatrx was removed.

	Replaced the Matrix mvMatrx with gl_ModelViewMatrixTranspose having the semantic WORLDVIEW using the transposed version. .
	The original declaration of mvMatrx was removed.


GLSL PostProcessing: 
	Restored uniform pend_s2_viewMatrix with viewMatrix 14 times.
	Restored the matrix declaration of float4x4 viewMatrix.
	Restored uniform pend_s2_lightpos with lightpos 5 times.
	Replaced all "__" with "_".

Cg Transposing Matrices: 
	Transposed the matrix viewMatrix with simple Cg-transpose.

*/
// transl output by Cg compiler
// cgc version 1.5.0014, build date Sep 18 2006 20:41:01
// command line args: -q -profile glslv -entry objectToEye_VP
//vendor NVIDIA Corporation
//version 1.5.0.14
//profile glslv
//program objectToEye_VP
//semantic gl_ModelViewMatrixTranspose : STATE.MATRIX.MODELVIEW
//semantic gl_ModelViewProjectionMatrixTranspose : STATE.MATRIX.MVP
//semantic ColorMap
//semantic SpecularMap
//semantic ParallaxMap
//semantic SpecExpon
//semantic depth
//semantic LightIntensity
//semantic LightColor
//semantic AmbientColor
//semantic SpecularColor
//semantic viewMatrix : VIEW
//semantic lightpos : LIGHTPOS1
//var float4x4 gl_ModelViewMatrixTranspose : STATE.MATRIX.MODELVIEW : gl_ModelViewMatrixTranspose[0], 4 : -1 : 1
//var float4x4 gl_ModelViewProjectionMatrixTranspose : STATE.MATRIX.MVP : gl_ModelViewProjectionMatrixTranspose[0], 4 : -1 : 1
//var sampler2D ColorMap :  :  : -1 : 0
//var sampler2D SpecularMap :  :  : -1 : 0
//var sampler2D ParallaxMap :  :  : -1 : 0
//var float SpecExpon :  :  : -1 : 0
//var float depth :  :  : -1 : 0
//var float LightIntensity :  :  : -1 : 0
//var float4 LightColor :  :  : -1 : 0
//var float4 AmbientColor :  :  : -1 : 0
//var float4 SpecularColor :  :  : -1 : 0
//var float4x4 viewMatrix : VIEW : viewMatrix[0], 4 : -1 : 1
//var float3 lightpos : LIGHTPOS1 : lightpos : -1 : 1
//var float3 IN.Position : $vin.POSITION : POSITION : 0 : 1
//var float4 IN.UV : $vin.TEXCOORD0 : TEXCOORD0 : 0 : 1
//var float4 IN.Normal : $vin.NORMAL : NORMAL : 0 : 1
//var float3 IN.tangent : $vin.TEXCOORD6 : TEXCOORD6 : 0 : 1
//var float3 IN.bitangent : $vin.TEXCOORD7 : TEXCOORD7 : 0 : 1
//var float4 objectToEye_VP.HPosition : $vout.POSITION : POSITION : -1 : 1
//var float4 objectToEye_VP.TexCoord : $vout.TEXCOORD0 : TEXCOORD0 : -1 : 1
//var float4 objectToEye_VP.EyeNormal : $vout.TEXCOORD1 : TEXCOORD1 : -1 : 1
//var float4 objectToEye_VP.EyeTangent : $vout.TEXCOORD2 : TEXCOORD2 : -1 : 1
//var float4 objectToEye_VP.EyeBitangent : $vout.TEXCOORD3 : TEXCOORD3 : -1 : 1
//var float4 objectToEye_VP.PosEyespace : $vout.TEXCOORD4 : TEXCOORD4 : -1 : 1
//var float4 objectToEye_VP.LightVec : $vout.TEXCOORD5 : TEXCOORD5 : -1 : 1
//default SpecExpon = 30
//default depth = 0.1
//default LightIntensity = 1
//default LightColor = 1 1 1 1
//default AmbientColor = 0.2 0.2 0.2 1
//default SpecularColor = 1 1 1 1

struct appdataBump {
    vec3 pend_Position;
    vec4 pend_UV;
    vec4 pend_Normal;
    vec3 pend_tangent;
    vec3 pend_bitangent;
};

struct vertexOutputBump {
    vec4 pend_HPosition;
    vec4 pend_TexCoord;
    vec4 pend_EyeNormal;
    vec4 pend_EyeTangent;
    vec4 pend_EyeBitangent;
    vec4 pend_PosEyespace;
    vec4 pend_LightVec;
};

vec3 dash1_pend_s3_r_0009;
vec3 dash1_pend_s3_r_0011;
vec3 dash1_pend_s3_r_0013;
vec4 dash1_pend_s3_r_0015;
vec4 dash1_pend_s3_r_0017;
vec4 dash1_pend_s3_r_0019;
vec4 dash1_pend_s3_v_0019;
vec3 at0_pend_s3_TMP62;
vec3 at0_pend_s3_TMP63;
vec3 at0_pend_s3_TMP64;
vec3 at0_pend_s3_TMP65;
vec3 at0_pend_s3_TMP66;
vec3 at0_pend_s3_TMP67;
vec3 at0_pend_s3_TMP68;
vec3 at0_pend_s3_TMP69;
vec3 at0_pend_s3_TMP70;
vec4 at0_pend_s3_TMP71;
vec4 at0_pend_s3_TMP72;
vec4 at0_pend_s3_TMP73;
uniform mat4 viewMatrix;
uniform vec3 lightpos;

 // main procedure, the original name was objectToEye_VP
void main()
{

    vertexOutputBump pend_s4_OUT;
    vec4 pend_s4_posObjectSpace;

    at0_pend_s3_TMP62.x = gl_ModelViewMatrixTranspose[0].x;
    at0_pend_s3_TMP62.y = gl_ModelViewMatrixTranspose[0].y;
    at0_pend_s3_TMP62.z = gl_ModelViewMatrixTranspose[0].z;
    dash1_pend_s3_r_0009.x = dot(at0_pend_s3_TMP62, gl_Normal);
    at0_pend_s3_TMP63.x = gl_ModelViewMatrixTranspose[1].x;
    at0_pend_s3_TMP63.y = gl_ModelViewMatrixTranspose[1].y;
    at0_pend_s3_TMP63.z = gl_ModelViewMatrixTranspose[1].z;
    dash1_pend_s3_r_0009.y = dot(at0_pend_s3_TMP63, gl_Normal);
    at0_pend_s3_TMP64.x = gl_ModelViewMatrixTranspose[2].x;
    at0_pend_s3_TMP64.y = gl_ModelViewMatrixTranspose[2].y;
    at0_pend_s3_TMP64.z = gl_ModelViewMatrixTranspose[2].z;
    dash1_pend_s3_r_0009.z = dot(at0_pend_s3_TMP64, gl_Normal);
    pend_s4_OUT.pend_EyeNormal.xyz = dash1_pend_s3_r_0009;
    at0_pend_s3_TMP65.x = gl_ModelViewMatrixTranspose[0].x;
    at0_pend_s3_TMP65.y = gl_ModelViewMatrixTranspose[0].y;
    at0_pend_s3_TMP65.z = gl_ModelViewMatrixTranspose[0].z;
    dash1_pend_s3_r_0011.x = dot(at0_pend_s3_TMP65, gl_MultiTexCoord6.xyz);
    at0_pend_s3_TMP66.x = gl_ModelViewMatrixTranspose[1].x;
    at0_pend_s3_TMP66.y = gl_ModelViewMatrixTranspose[1].y;
    at0_pend_s3_TMP66.z = gl_ModelViewMatrixTranspose[1].z;
    dash1_pend_s3_r_0011.y = dot(at0_pend_s3_TMP66, gl_MultiTexCoord6.xyz);
    at0_pend_s3_TMP67.x = gl_ModelViewMatrixTranspose[2].x;
    at0_pend_s3_TMP67.y = gl_ModelViewMatrixTranspose[2].y;
    at0_pend_s3_TMP67.z = gl_ModelViewMatrixTranspose[2].z;
    dash1_pend_s3_r_0011.z = dot(at0_pend_s3_TMP67, gl_MultiTexCoord6.xyz);
    pend_s4_OUT.pend_EyeTangent.xyz = dash1_pend_s3_r_0011;
    at0_pend_s3_TMP68.x = gl_ModelViewMatrixTranspose[0].x;
    at0_pend_s3_TMP68.y = gl_ModelViewMatrixTranspose[0].y;
    at0_pend_s3_TMP68.z = gl_ModelViewMatrixTranspose[0].z;
    dash1_pend_s3_r_0013.x = dot(at0_pend_s3_TMP68, gl_MultiTexCoord7.xyz);
    at0_pend_s3_TMP69.x = gl_ModelViewMatrixTranspose[1].x;
    at0_pend_s3_TMP69.y = gl_ModelViewMatrixTranspose[1].y;
    at0_pend_s3_TMP69.z = gl_ModelViewMatrixTranspose[1].z;
    dash1_pend_s3_r_0013.y = dot(at0_pend_s3_TMP69, gl_MultiTexCoord7.xyz);
    at0_pend_s3_TMP70.x = gl_ModelViewMatrixTranspose[2].x;
    at0_pend_s3_TMP70.y = gl_ModelViewMatrixTranspose[2].y;
    at0_pend_s3_TMP70.z = gl_ModelViewMatrixTranspose[2].z;
    dash1_pend_s3_r_0013.z = dot(at0_pend_s3_TMP70, gl_MultiTexCoord7.xyz);
    pend_s4_OUT.pend_EyeBitangent.xyz = dash1_pend_s3_r_0013;
    pend_s4_posObjectSpace = vec4(gl_Vertex.x, gl_Vertex.y, gl_Vertex.z, 1.0);
    dash1_pend_s3_r_0015.x = dot(gl_ModelViewMatrixTranspose[0], pend_s4_posObjectSpace);
    dash1_pend_s3_r_0015.y = dot(gl_ModelViewMatrixTranspose[1], pend_s4_posObjectSpace);
    dash1_pend_s3_r_0015.z = dot(gl_ModelViewMatrixTranspose[2], pend_s4_posObjectSpace);
    pend_s4_OUT.pend_PosEyespace.xyz = dash1_pend_s3_r_0015.xyz;
    dash1_pend_s3_r_0017.x = dot(gl_ModelViewProjectionMatrixTranspose[0], pend_s4_posObjectSpace);
    dash1_pend_s3_r_0017.y = dot(gl_ModelViewProjectionMatrixTranspose[1], pend_s4_posObjectSpace);
    dash1_pend_s3_r_0017.z = dot(gl_ModelViewProjectionMatrixTranspose[2], pend_s4_posObjectSpace);
    dash1_pend_s3_r_0017.w = dot(gl_ModelViewProjectionMatrixTranspose[3], pend_s4_posObjectSpace);
    dash1_pend_s3_v_0019 = vec4(lightpos.x, lightpos.y, lightpos.z, 1.0);
    at0_pend_s3_TMP71.x = viewMatrix[0].x;
    at0_pend_s3_TMP71.y = viewMatrix[1].x;
    at0_pend_s3_TMP71.z = viewMatrix[2].x;
    at0_pend_s3_TMP71.w = viewMatrix[3].x;
    dash1_pend_s3_r_0019.x = dot(at0_pend_s3_TMP71, dash1_pend_s3_v_0019);
    at0_pend_s3_TMP72.x = viewMatrix[0].y;
    at0_pend_s3_TMP72.y = viewMatrix[1].y;
    at0_pend_s3_TMP72.z = viewMatrix[2].y;
    at0_pend_s3_TMP72.w = viewMatrix[3].y;
    dash1_pend_s3_r_0019.y = dot(at0_pend_s3_TMP72, dash1_pend_s3_v_0019);
    at0_pend_s3_TMP73.x = viewMatrix[0].z;
    at0_pend_s3_TMP73.y = viewMatrix[1].z;
    at0_pend_s3_TMP73.z = viewMatrix[2].z;
    at0_pend_s3_TMP73.w = viewMatrix[3].z;
    dash1_pend_s3_r_0019.z = dot(at0_pend_s3_TMP73, dash1_pend_s3_v_0019);
    pend_s4_OUT.pend_LightVec.xyz = dash1_pend_s3_r_0019.xyz - dash1_pend_s3_r_0015.xyz;
    gl_TexCoord[3] = pend_s4_OUT.pend_EyeBitangent;
    gl_TexCoord[5] = pend_s4_OUT.pend_LightVec;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[2] = pend_s4_OUT.pend_EyeTangent;
    gl_TexCoord[4] = pend_s4_OUT.pend_PosEyespace;
    gl_Position = dash1_pend_s3_r_0017;
    gl_TexCoord[1] = pend_s4_OUT.pend_EyeNormal;
    return;
} // main end
