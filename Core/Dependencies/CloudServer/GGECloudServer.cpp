#include "StdAfx.h"
#include "GGECloudServer.h"

#include "RakNetObjects.h"
#include "NetworkIDManager.h"

namespace GamePipe
{
	//char DEFAULT_SERVER_ADDRESS [512];
	const unsigned short DEFAULT_SERVER_PORT = 60000;
	bool didRebalance=false; // So we only reconnect to a lower load server once, for load balancing

	//////////////////////////
	// Neutral methods
	//////////////////////////
	GGECloudServer::GGECloudServer()
	{
		
	}

	GGECloudServer::~GGECloudServer()
	{
	}

	int GGECloudServer::createGGECloudServer()
	{
	
		char **test = NULL;
		if (!RakNet::CloudServerHelper::ParseCommandLineParameters(0,test))
			return 1;
	

		rakPeer=RakNet::RakPeerInterface::GetInstance();

		rakPeer->AttachPlugin(&cloudServer);
		rakPeer->AttachPlugin(&cloudClient);
		rakPeer->AttachPlugin(&fullyConnectedMesh2);
		rakPeer->AttachPlugin(&twoWayAuthentication);
		rakPeer->AttachPlugin(&connectionGraph2);

	if (!RakNet::CloudServerHelper::StartRakPeer(rakPeer))
			return 1;

		sampleFilter.serverGuid=rakPeer->GetMyGUID();
		RakNet::CloudServerHelper::SetupPlugins(&cloudServer, 
												&sampleFilter, 
												&cloudClient, 
												&fullyConnectedMesh2, 
												&twoWayAuthentication, 
												&connectionGraph2,
												RakNet::CloudServerHelper::serverToServerPassword);


		int ret;
		do 
		{
			ret = RakNet::CloudServerHelper::JoinCloud(rakPeer, 
													   &cloudServer, 
													   &cloudClient, 
													   &fullyConnectedMesh2, 
													   &twoWayAuthentication, 
													   &connectionGraph2, 
													   &dynDNS);
		} 
		while (ret==2);

		if (ret==1)
			return 1;
			
		// Should now be connect to the cloud, using authentication and FullyConnectedMesh2
		printf("GGE Cloud Server Running.\n");

		return 0;
	}

	int GGECloudServer::Update() {
	
			for (packet=rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet=rakPeer->Receive())
			{
				RakNet::CloudServerHelper::OnPacket(packet, rakPeer, &cloudClient, &cloudServer, &fullyConnectedMesh2, &twoWayAuthentication, &connectionGraph2, &dynDNS);
			}	
			
		return 1;

	}

	/* Send the Physics objects to the Client to be created localy by the CLient*/
	void GGECloudServer::SendToClient(ObjectData *gameObject, RakNet::RakNetGUID guid)
	{
		float yaw = 0;
		float pitch = 0;
		float roll = 0;
		RakNet::BitStream bs;
		bs = NULL;
		bs.Write((RakNet::MessageID)ID_GGE_GAME_OBJECT);
		bs.Write(gameObject->posX);
		bs.Write(gameObject->posY);	
		bs.Write(gameObject->posZ);				
		bs.Write(yaw);	
		bs.Write(pitch);
		bs.Write(roll);
				
		bs.Write(gameObject->name);
		bs.Write(gameObject->meshName);
						
		bs.Write(GRAPHICS_OBJECT);
		bs.Write(false);
		bs.Write(false);
		bs.Write(true);		

		rakPeer->Send(&bs,HIGH_PRIORITY, RELIABLE_ORDERED, 0,  guid, false);

	}

	/* Send the updated positions and name of the object to be updated to the Client */
	void GGECloudServer::SendUpdateToClient(ObjectData *gameObject, RakNet::RakNetGUID guid, std::map<std::string, GameObject *> guidPhysics)
	{
		if(gameObject->havokName != "the_ground.hkx") {
			float yaw = 0;
			GameObject *temp = guidPhysics[gameObject->name];
			Ogre::Vector3 position = temp->getPosition();
			
			RakNet::BitStream bs;
			bs = NULL;
			bs.Write((RakNet::MessageID)ID_GGE_GAME_OBJECT_UPDATE);
		    bs.Write(position.x);
			bs.Write(position.y);
			bs.Write(position.z);
							
			bs.Write(gameObject->name);
	
			bs.Write(true);		

			rakPeer->Send(&bs,HIGH_PRIORITY, RELIABLE_ORDERED, 0, guid, false);
		}
	}

}
