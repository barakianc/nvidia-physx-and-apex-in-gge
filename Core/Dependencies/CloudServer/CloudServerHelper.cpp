#include "StdAfx.h"
#include "CloudServerHelper.h"
#include "RakSleep.h"
#include "ObjectData.h"
#include "RakNetObjects.h"

#include "Gets.h"
#include "CloudMessageIdentifiers.h"
#include "CloudServerGlobal.h"
#include "BitStream.h"
#include "FullyConnectedMesh2.h"
#include "TwoWayAuthentication.h"
#include "CloudClient.h"
#include "DynDNS.h"
#include "SocketLayer.h"
#include "RakPeerInterface.h"
#include "ConnectionGraph2.h"
#include <sstream>
//#include <stdlib.h>


using namespace RakNet;

const char *CloudServerHelper::dnsHost;
const char *CloudServerHelper::usernameAndPassword;
const char *CloudServerHelper::serverToServerPassword;
unsigned short CloudServerHelper::serverPort;
unsigned short CloudServerHelper::allowedIncomingConnections;
unsigned short CloudServerHelper::allowedOutgoingConnections;

#define CLOUD_SERVER_CONNECTION_COUNT_PRIMARY_KEY "CloudConnCount"

bool CloudServerHelperFilter::OnPostRequest(RakNetGUID clientGuid, SystemAddress clientAddress, CloudKey key, uint32_t dataLength, const char *data)
{
	if (clientGuid!=serverGuid)
	{
		if (key.primaryKey==CLOUD_SERVER_CONNECTION_COUNT_PRIMARY_KEY)
			return false;
	}
	return true;
}
bool CloudServerHelperFilter::OnReleaseRequest(RakNetGUID clientGuid, SystemAddress clientAddress, DataStructures::List<CloudKey> &cloudKeys) {return true;}
bool CloudServerHelperFilter::OnGetRequest(RakNetGUID clientGuid, SystemAddress clientAddress, CloudQuery &query, DataStructures::List<RakNetGUID> &specificSystems) {return true;}
bool CloudServerHelperFilter::OnUnsubscribeRequest(RakNetGUID clientGuid, SystemAddress clientAddress, DataStructures::List<CloudKey> &cloudKeys, DataStructures::List<RakNetGUID> &specificSystems) {return true;}


bool CloudServerHelper::ParseCommandLineParameters(int argc, char **argv)
{
	//const char *DEFAULT_DNS_HOST="test.dnsalias.net";
	const char *DEFAULT_DNS_HOST="";
	const char *DEFAULT_USERNAME_AND_PASSWORD="test:test";
	const char *DEFAULT_SERVER_TO_SERVER_PASSWORD="qwerty1234";
	const unsigned short DEFAULT_SERVER_PORT=60000;
	const unsigned short DEFAULT_ALLOWED_INCOMING_CONNECTIONS=1024;
	const unsigned short DEFAULT_ALLOWED_OUTGOING_CONNECTIONS=64;
	//return true;

//#ifndef _DEBUG
	// Only allow insecure defaults for debugging
	/*
	if (argc<4)
	{
		PrintHelp();
		return false;
	}
	*/
	//dnsHost=argv[1];
	//usernameAndPassword=argv[2];
	//serverToServerPassword=argv[3];
//#else
	if (argc<2) dnsHost=DEFAULT_DNS_HOST;
	//else dnsHost=argv[1];

	if (argc<3) usernameAndPassword=DEFAULT_USERNAME_AND_PASSWORD;
	//else usernameAndPassword=argv[2];

	if (argc<4) serverToServerPassword=DEFAULT_SERVER_TO_SERVER_PASSWORD;
	//else serverToServerPassword=argv[3];
//#endif

	if (argc<5) serverPort=DEFAULT_SERVER_PORT;
	//else serverPort=atoi(argv[4]);

	if (argc<6) allowedIncomingConnections=DEFAULT_ALLOWED_INCOMING_CONNECTIONS;
	//else allowedIncomingConnections=atoi(argv[5]);

	if (argc<7) allowedOutgoingConnections=DEFAULT_ALLOWED_OUTGOING_CONNECTIONS;
	//else allowedOutgoingConnections=atoi(argv[6]);

	return true;
}

void CloudServerHelper::PrintHelp(void)
{	
	printf("Distributed authenticated CloudServer using DNS based host migration.\n");
	printf("Query running servers with CloudClient::Get() on key CloudServerList,0\n\n");
	printf("Query load with key CloudConnCount,0. Read row data as unsigned short.\n\n");
	printf("Usage:\n");
	printf("CloudServer.exe DNSHost Username:Password S2SPWD [Port] [ConnIn] [ConnOut]\n\n");
	printf("Parameters:\n");
	printf("DNSHost - Free DNS hostname from http://www.dyndns.com/\n");
	printf("Username:Password - Account settings from http://www.dyndns.com/\n");
	printf("S2SPWD - Server to server cloud password. Anything random.\n");
	printf("Port - RakNet listen port. Default is 60000\n");
	printf("ConnIn - Max incoming connections for clients. Default is 1024\n");
	printf("ConnIn - Max outgoing connections, used for server to server. Default 64\n\n");
	printf("Example:\n");
	printf("CloudServer.exe test.dnsalias.net test:test qwerty1234 60000 1024 64\n\n");
	
}

bool CloudServerHelper::StartRakPeer(RakNet::RakPeerInterface *rakPeer)
{
	RakNet::SocketDescriptor sd(RakNet::CloudServerHelper::serverPort,0);
	RakNet::StartupResult sr = rakPeer->Startup(RakNet::CloudServerHelper::allowedIncomingConnections+RakNet::CloudServerHelper::allowedOutgoingConnections,&sd,1);
	if (sr!=RakNet::RAKNET_STARTED)
	{
		printf("Startup failed. Reason=%i\n", (int) sr);
		return false;
	}
	rakPeer->SetMaximumIncomingConnections(RakNet::CloudServerHelper::allowedIncomingConnections);
	rakPeer->SetTimeoutTime(60000,UNASSIGNED_SYSTEM_ADDRESS);
	return true;
}

Packet *CloudServerHelper::ConnectToRakPeer(const char *host, unsigned short port, RakPeerInterface *rakPeer)
{
	printf("Connecting to %s\n", host);
	ConnectionAttemptResult car;
	car = rakPeer->Connect(host, port, 0, 0);
	if (car!=CONNECTION_ATTEMPT_STARTED)
	{
		printf("Connect() call failed\n");
		if (car==CANNOT_RESOLVE_DOMAIN_NAME) printf("Cannot resolve domain name\n");
		return 0;
	}

	Packet *packet;
	while (1)
	{
		for (packet=rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet=rakPeer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_CONNECTION_ATTEMPT_FAILED:
				return packet;
			case ID_REMOTE_SYSTEM_REQUIRES_PUBLIC_KEY:
			case ID_OUR_SYSTEM_REQUIRES_SECURITY:
			case ID_PUBLIC_KEY_MISMATCH:
			case ID_CONNECTION_BANNED:
			case ID_INVALID_PASSWORD:
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				return packet;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			case ID_IP_RECENTLY_CONNECTED:
				printf("Remote system full. Retrying...");
				car = rakPeer->Connect(host, port, 0, 0);
				if (car!=CONNECTION_ATTEMPT_STARTED)
				{
					printf("Connect() call failed\n");
					if (car==CANNOT_RESOLVE_DOMAIN_NAME) printf("Cannot resolve domain name\n");
					return 0;
				}
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				if (packet->guid==rakPeer->GetMyGUID())
				{
					// Just connected to myself! Host must be pointing to our own IP address.
					rakPeer->CloseConnection(packet->guid,false);
					RakSleep(30); // Let the thread clear out
					packet->data[0]=ID_ALREADY_CONNECTED;

					return packet;
				}
			case ID_ALREADY_CONNECTED:
				return packet; // Not initial host
			}
		}

		RakSleep(30);
	}
}

bool CloudServerHelper::UpdateHostDNS(RakNet::DynDNS *dynDNS)
{
	dynDNS->UpdateHostIP(
		RakNet::CloudServerHelper::dnsHost,
		0,
		RakNet::CloudServerHelper::usernameAndPassword);

	// Wait for the DNS update to complete
	while (1)
	{
		dynDNS->Update();

		if (dynDNS->IsCompleted())
		{
			printf("%s\n", dynDNS->GetCompletedDescription());
			return dynDNS->WasResultSuccessful();
		}

		RakSleep(30);
	}
	return false;
}

MessageID CloudServerHelper::AuthenticateRemoteServerBlocking(RakPeerInterface *rakPeer, TwoWayAuthentication *twoWayAuthentication, RakNetGUID remoteSystem)
{
	twoWayAuthentication->Challenge("CloudServerHelperS2SPassword", remoteSystem);

	MessageID messageId;
	Packet *packet;
	while (1)
	{
		for (packet=rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet=rakPeer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_CONNECTION_LOST:
			case ID_DISCONNECTION_NOTIFICATION:
				if (packet->guid==remoteSystem)
				{
					messageId=packet->data[0];
					rakPeer->DeallocatePacket(packet);
					return messageId;
				}
				break;
			case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_SUCCESS:
			case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_TIMEOUT:
			case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_FAILURE:
				{
					messageId=packet->data[0];
					rakPeer->DeallocatePacket(packet);
					return messageId;
				}
				break;
			}
		}			

		RakSleep(30);
	}
}
void CloudServerHelper::SetupPlugins(
	RakNet::CloudServer *cloudServer,
	RakNet::CloudServerHelperFilter *sampleFilter,
	RakNet::CloudClient *cloudClient,
	RakNet::FullyConnectedMesh2 *fullyConnectedMesh2,
	RakNet::TwoWayAuthentication *twoWayAuthentication,
	RakNet::ConnectionGraph2 *connectionGraph2,
	const char *serverToServerPassword
	)
{
	cloudServer->AddQueryFilter(sampleFilter);
	// Connect to all systems told about via ConnectionGraph2::AddParticpant(). We are only told about servers that have already been authenticated
	fullyConnectedMesh2->SetConnectOnNewRemoteConnection(true, "");
	// Do not add to the host trracking system all connections, only those designated as servers
	fullyConnectedMesh2->SetAutoparticipateConnections(false);
	// Shared password
	twoWayAuthentication->AddPassword("CloudServerHelperS2SPassword",serverToServerPassword);
	// Do not add systems to the graph unless first validated as a server through the TwoWayAuthentication plugin
	connectionGraph2->SetAutoProcessNewConnections(false);
}
void CloudServerHelper::OnPacket(Packet *packet, RakPeerInterface *rakPeer, CloudClient *cloudClient, RakNet::CloudServer *cloudServer, RakNet::FullyConnectedMesh2 *fullyConnectedMesh2, TwoWayAuthentication *twoWayAuthentication, ConnectionGraph2 *connectionGraph2, DynDNS *dynDNS)
{
	std::list<ObjectData *>::iterator it;
	switch (packet->data[0])
	{
	case ID_FCM2_NEW_HOST:
		RakNet::CloudServerHelper::OnFCMNewHost(packet, rakPeer, dynDNS);
		break;
	case ID_CONNECTION_REQUEST_ACCEPTED:
		twoWayAuthentication->Challenge("CloudServerHelperS2SPassword", packet->guid);
		// Fallthrough

	//On Incoming Connection, Create all Game Objects to send to Client and add them to the Server ObjectList
	case ID_NEW_INCOMING_CONNECTION:
		printf("Got connection to %s\n", packet->systemAddress.ToString(true));
		RakNet::CloudServerHelper::OnConnectionCountChange(rakPeer, cloudClient);
		
		if(packet != NULL) 
		{
			float yaw = (float)0;
			float pitch = (float)0;
			float roll =(float)0;
		
			float posX = -100.0;
			for(int i=0; i<5; i++) {
			
				ObjectData *tempData = new ObjectData;
				
				RakNet::BitStream bs;
				GamePipeGame::BasicReplica *basicObject = new GamePipeGame::BasicReplica();
				bs.Write((RakNet::MessageID)ID_GGE_GAME_OBJECT);
				
				bs.Write(posX+(i*50));
				bs.Write(basicObject->GetPosition().y);
				bs.Write(basicObject->GetPosition().z);
				
				bs.Write(yaw);	
				bs.Write(pitch);
				bs.Write(roll);

				string name = "OgreHead";//basicObject->GetObjectName();
				std::stringstream temp;
				temp << i;
				name += temp.str();
				bs.Write(name);
				
				bs.Write(basicObject->GetMeshName());
				bs.Write(GRAPHICS_OBJECT);
				bs.Write(false);
				bs.Write(false);
				bs.Write(false);			

				//Add to List of All Server Files
				CloudServerHelper::AddToServerObjects(posX+(i*50),basicObject->GetPosition().y,basicObject->GetPosition().z,
													  name,basicObject->GetMeshName(),GRAPHICS_OBJECT,
													  false,false,false,packet->guid, "");
				 
				rakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0,  packet->systemAddress, false);	
			}
			
			
			//Create a unique name for each havokBall using timestamp
			clock_t c1 = clock() / (CLOCKS_PER_SEC / 1000);
			for(int i=0; i<4; i++) {
				std::string ballName = "havokBall";
				std::stringstream temp2;
				temp2 << c1 + i;
				ballName += temp2.str();
				float pos = 50 * (float)i;
				CloudServerHelper::AddToServerObjects(0.0,pos,50.0,ballName,"star.mesh",PHYSICS_DYNAMIC,false,false,false,packet->guid,"star.hkx" );
			}

				std::string ground = "theGround";
				std::stringstream temp3;
				temp3 << "test";
				ground += temp3.str();
				CloudServerHelper::AddToServerObjects(0.0,-30.0,0.0,ground,"the_ground.mesh",PHYSICS_DYNAMIC,false,false,false,packet->guid,"the_ground.hkx");
				
		}
		break;
	case ID_CONNECTION_LOST:
	case ID_DISCONNECTION_NOTIFICATION:
		printf("Lost connection to %s\n", packet->systemAddress.ToString(true));
		RakNet::CloudServerHelper::OnConnectionCountChange(rakPeer, cloudClient);

		/*
		//Remove Objects from the Client's Game Object List
		if(!serverGameObjects[packet->guid].empty()) {
			for(it = serverGameObjects[packet->guid].begin(); it != serverGameObjects[packet->guid].end(); ++it) {
				delete (*it);
			}

		}
		*/


		break;
	case ID_TWO_WAY_AUTHENTICATION_INCOMING_CHALLENGE_SUCCESS:
		printf("New server connected to us from %s\n", packet->systemAddress.ToString(true));
		// Fallthrough
	case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_SUCCESS:
		if (packet->data[0]==ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_SUCCESS)
			printf("We connected to server %s\n", packet->systemAddress.ToString(true));
		cloudServer->AddServer(packet->guid);
		fullyConnectedMesh2->AddParticipant(packet->guid);
		connectionGraph2->AddParticipant(packet->systemAddress, packet->guid);
		break;
	case ID_TWO_WAY_AUTHENTICATION_INCOMING_CHALLENGE_FAILURE:
	case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_FAILURE:
	case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_TIMEOUT:
		rakPeer->CloseConnection(packet->guid,false);
		break;
	case ID_GGE_GAME_OBJECT:
		break;

	//This MessageIdentifier is to handle KeyPress Events from the Client
	case ID_KEY_INPUT:
		if(packet != NULL) {
			int key = 0;
			float x = (float)0.0;
			float y = (float)0.0;
			float z = (float)0.0;
			float yaw = (float)0;
			float pitch = (float)0;
			float roll = (float)0;
			std::string name = "OgreHead2";
			std::string meshName = "ogrehead.mesh";
			
			RakNet::BitStream bs(packet->data, packet->length, false);	
			bs.IgnoreBytes(sizeof(RakNet::MessageID));
			bs.Read(key);
			bs.Read(x);
			bs.Read(y);
			bs.Read(z);

			RakNet::BitStream bs2;
			bs2.Write((RakNet::MessageID)ID_GGE_GAME_OBJECT_UPDATE);
			bs2.Write(x);
			y += 5;
			bs2.Write(y);
			bs2.Write(z);
	
			bs2.Write(name);
		
			bs2.Write(false);
			bs2.Write(true);

			rakPeer->Send(&bs2, HIGH_PRIORITY, RELIABLE_ORDERED, 0,  packet->systemAddress, false);	
		}

		break;
	//MessageIdentifier to update Static Graphics objects from Client.
	case ID_GGE_ENV_OBJECTS:
		if(packet != NULL) {
			int key = 0;
			float x = (float)0.0;
			float y = (float)0.0;
			float z = (float)0.0;
			float yaw = (float)0;
			float pitch = (float)0;
			float roll = (float)0;
			int o=0;
			std::string name = "OgreHead0";
			std::string meshName = "ogrehead.mesh";
		
			int flag = 0;
			std::list<ObjectData *>::iterator object_it;
			
			/*
			if(o==0)
				name = "OgreHead0";
			else if(o==4)
				name = "OgreHead4";
			*/
			//Look through Client's Objects and update specified Object
			//Note this is hardcoded for sake of the Demo, but the Client could specify which Object to update by
			//Sending info to the Server
			for(object_it = serverGameObjects[packet->guid].begin(); object_it != serverGameObjects[packet->guid].end(); ++object_it) {
				if((*object_it)->name == "OgreHead0" || (*object_it)->name == "OgreHead4") {
					RakNet::BitStream bs2;
					bs2.Write((RakNet::MessageID)ID_GGE_GAME_OBJECT_UPDATE);
					bs2.Write((*object_it)->posX);	
					
					if((*object_it)->posY < 20) {
						(*object_it)->posY += 2;	
					}
					bs2.Write((*object_it)->posY);
					bs2.Write((*object_it)->posZ);
					
					bs2.Write((*object_it)->name);
			
					bs2.Write(true);

					rakPeer->Send(&bs2, HIGH_PRIORITY, RELIABLE_ORDERED, 0,  packet->systemAddress, false);	
				}
			}
		}


		break;


	}
}
bool CloudServerHelper::Update(DynDNS *dynDNS)
{
	// Keep DNS updated if needed
	if (dynDNS->IsRunning())
	{
		dynDNS->Update();
		if (dynDNS->IsCompleted())
		{
			printf("%s.\n", dynDNS->GetCompletedDescription());
			if (dynDNS->WasResultSuccessful()==false)
				return false;
			printf("Note: The DNS cache update takes about 60 seconds.\n");
		}
	}
	return true;
}
void CloudServerHelper::OnFCMNewHost(Packet *packet, RakPeerInterface *rakPeer, DynDNS *dynDNS)
{
	RakAssert(packet->data[0]==ID_FCM2_NEW_HOST);
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(MessageID));
	RakNetGUID oldHost;
	bsIn.Read(oldHost);
	RakNetGUID newHost = packet->guid;
	if (newHost==rakPeer->GetMyGUID() && oldHost!=newHost)
	{
		printf("Assuming host. Updating DNS\n");

		// Change dynDNS to point to us
		dynDNS->UpdateHostIP(
			RakNet::CloudServerHelper::dnsHost,
			0,
			RakNet::CloudServerHelper::usernameAndPassword);
	}
}
void CloudServerHelper::OnConnectionCountChange(RakPeerInterface *rakPeer, CloudClient *cloudClient)
{
	RakNet::BitStream bs;
	CloudKey cloudKey(CLOUD_SERVER_CONNECTION_COUNT_PRIMARY_KEY,0);
	unsigned short numberOfSystems;
	rakPeer->GetConnectionList(0, &numberOfSystems);
	bs.Write(numberOfSystems);
	cloudClient->Post(&cloudKey, bs.GetData(), bs.GetNumberOfBytesUsed(), rakPeer->GetMyGUID());
}
int CloudServerHelper::JoinCloud(
	RakNet::RakPeerInterface *rakPeer,
	RakNet::CloudServer *cloudServer,
	RakNet::CloudClient *cloudClient,
	RakNet::FullyConnectedMesh2 *fullyConnectedMesh2,
	RakNet::TwoWayAuthentication *twoWayAuthentication,
	RakNet::ConnectionGraph2 *connectionGraph2,
	DynDNS *dynDNS
	)
{
	RakNet::MessageID result;
	SystemAddress packetAddress;
	RakNetGUID packetGuid;
	Packet *packet;
	char myPublicIP[32];

	// Reset plugins
	cloudServer->Clear();
	fullyConnectedMesh2->Clear();

	// ---- CONNECT TO EXISTING SERVER ----
	packet = RakNet::CloudServerHelper::ConnectToRakPeer(RakNet::CloudServerHelper::dnsHost, RakNet::CloudServerHelper::serverPort, rakPeer);
	if (packet==0)
		return 1;
	result = packet->data[0];
	packetAddress = packet->systemAddress;
	packetGuid = packet->guid;
	rakPeer->DeallocatePacket(packet);

	if (result==ID_CONNECTION_REQUEST_ACCEPTED)
	{
		printf("Connected to host %s.\n", RakNet::CloudServerHelper::dnsHost);

		// We connected through a public IP.
		// Our external IP should also be public
		rakPeer->GetExternalID(packetAddress).ToString(false, myPublicIP);

		// Log in to the remote server using two way authentication
		result = RakNet::CloudServerHelper::AuthenticateRemoteServerBlocking(rakPeer, twoWayAuthentication, packetGuid);
		if (result==ID_CONNECTION_LOST || result==ID_DISCONNECTION_NOTIFICATION)
		{
			printf("Connection lost while authenticating.\n");
			printf("Waiting 60 seconds then restarting.\n");
			RakSleep(60000);
			return 2;
		}
		else if (result==ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_TIMEOUT)
		{
			// Other system is not running plugin? Fail
			printf("Remote server did not respond to challenge.\n");
			return 1;
		}
		else if (result==ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_FAILURE)
		{
			printf("Failed remote server challenge.\n");
			return 1;
		}

		RakAssert(result==ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_SUCCESS);

		// Add this system as a server, and to FullyConnectedMesh2 as a participant
		cloudServer->AddServer(packetGuid);
		fullyConnectedMesh2->AddParticipant(packetGuid);
		connectionGraph2->AddParticipant(packetAddress, packetGuid);
	}
	else if (result==ID_ALREADY_CONNECTED)
	{
		printf("Connected to self. DNS entry already points to this server.\n");

		// dnsHost is always public, so if I can connect through it that's my public IP
		strcpy_s( myPublicIP, ( char* ) SocketLayer::DomainNameToIP( RakNet::CloudServerHelper::dnsHost ));
	}
	else if (result==ID_CONNECTION_ATTEMPT_FAILED)
	{
		printf("Failed connection. Changing DNS to point to this system.\n");

		if (RakNet::CloudServerHelper::UpdateHostDNS(dynDNS)==false)
			return 1;

		// dynDNS gets our public IP when it succeeds
		strcpy_s( myPublicIP, dynDNS->GetMyPublicIP());
	}
	else
	{
		// Another server is running but we cannot connect to them
		printf("Critical failure\n");
		printf("Reason: ");
		switch (result)
		{
		case ID_REMOTE_SYSTEM_REQUIRES_PUBLIC_KEY:
		case ID_OUR_SYSTEM_REQUIRES_SECURITY:
		case ID_PUBLIC_KEY_MISMATCH:
			printf("Other system is running security code.\n");
			break;
		case ID_CONNECTION_BANNED:
			printf("Banned from the other system.\n");
			break;
		case ID_INVALID_PASSWORD:
			printf("Other system has a password.\n");
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("Different major RakNet version.\n");
			break;
		default:
			printf("N/A\n");
			break;
		}
		return 1;
	}

	// Force the external server address for queries. Otherwise it would report 127.0.0.1 since the client is on localhost
	SystemAddress forceAddress;
	forceAddress.FromString(myPublicIP,(int)RakNet::CloudServerHelper::serverPort);
	cloudServer->ForceExternalSystemAddress(forceAddress);

	if (result==ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_SUCCESS)
	{
		OnConnectionCountChange(rakPeer, cloudClient);
	}
	else
	{
		RakNet::BitStream bs;
		CloudKey cloudKey(CLOUD_SERVER_CONNECTION_COUNT_PRIMARY_KEY,0);
		bs.WriteCasted<unsigned short>(0);
		cloudClient->Post(&cloudKey, bs.GetData(), bs.GetNumberOfBytesUsed(), rakPeer->GetMyGUID());
	}
	return 0;
}

/*This Function Sets the necessary parameters into and ObjectData Struct and then
* Adds the struct to the global list of game Objects for the server. The key for 
* the list is the clients RakNet::RakNetGUID
*/
int CloudServerHelper::AddToServerObjects(
		float posX,
		float posY,
		float posZ,
		std::string name,
		std::string meshName,
		GameObjectType objectType,
		bool isAlive,
		bool isCreated,
		bool isUpdated,
		RakNet::RakNetGUID guid,
		std::string havokName
		)
{
	ObjectData *tempData = new ObjectData;

	tempData->posX = posX;
	tempData->posY = posY;
	tempData->posZ = posZ;
	tempData->yaw = 0.0;
	tempData->pitch = 0.0;
	tempData->roll = 0.0;
	tempData->name = name;
	tempData->meshName = meshName;
	tempData->objectType = objectType;
	tempData->havokName = havokName;
	tempData->isAlive = isAlive;
	tempData->isCreated = isCreated;
	tempData->isUpdated = isUpdated;

	serverGameObjects[guid].push_back(tempData);

	return 0;

}
