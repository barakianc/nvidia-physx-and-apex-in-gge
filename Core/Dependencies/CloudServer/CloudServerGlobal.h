#ifndef CLOUD_SERVER_GLOBAL_H
#define CLOUD_SERVER_GLOBAL_H

#include "ObjectData.h"
#include "RakNetTypes.h"
#include <map>

extern std::map<RakNet::RakNetGUID,std::list<ObjectData *>> serverGameObjects;

#endif