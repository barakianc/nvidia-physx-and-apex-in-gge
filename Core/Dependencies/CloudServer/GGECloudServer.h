#ifndef __GGE_CLOUD_SERVER_H
#define __GGE_CLOUD_SERVER_H

#pragma once
#include "GameScreen.h"
#include "Engine.h"
#include "CloudServerHelper.h"
#include "CloudMessageIdentifiers.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include "FullyConnectedMesh2.h"
#include "TwoWayAuthentication.h"
#include "CloudClient.h"
#include "DynDNS.h"
#include "RakPeerInterface.h"
#include "RakSleep.h"
#include "ConnectionGraph2.h"
#include <list>
#include <stdio.h>

#define CLOUD_CLIENT_PRIMARY_KEY "CC_Sample_PK"

//using namespace CM;
namespace GamePipe
{
	class GGECloudServer
	{
		public:
			// ---- RAKPEER -----
			RakNet::RakPeerInterface *rakPeer;
			
			// ---- PLUGINS -----
			// Used to load balance clients, allow for client to client discovery
			RakNet::CloudServer cloudServer;
			// Used to update the local cloudServer
			RakNet::CloudClient cloudClient;
			// Used to determine the host of the server fully connected mesh, as well as to connect servers automatically
			RakNet::FullyConnectedMesh2 fullyConnectedMesh2;
			// Used for servers to verify each other - otherwise any system could pose as a server
			// Could also be used to verify and restrict clients if paired with the MessageFilter plugin
			RakNet::TwoWayAuthentication twoWayAuthentication;
			// Used to update DNS
			RakNet::DynDNS dynDNS;
			// Used to tell servers about each other
			RakNet::ConnectionGraph2 connectionGraph2;
			RakNet::Packet *packet;
			RakNet::CloudServerHelperFilter sampleFilter; // Keeps clients from updating stuff to the server they are not supposed to

			GameObject* gameObject;
		
			RakNet::SystemAddress serverAddressObject;
			unsigned short serverPort;
			
			GGECloudServer();
			~GGECloudServer();


			int createGGECloudServer();
			int Update();
			void SendToClient(ObjectData *gameObject,RakNet::RakNetGUID guid);
			void SendUpdateToClient(ObjectData *gameObject, RakNet::RakNetGUID guid,std::map<std::string, GameObject *> guidPhysics);
		
	};
}

#endif