#pragma once

// Engine
#include "Engine.h"

// GameScreen
#include "GameScreen.h"

class GameScreen;
class TiXmlElement;
class TiXmlNode;


namespace GamePipe
{
	class DotScenePlusLoader
	{
	private:
		Ogre::SceneManager* m_pSceneManager;
		std::string m_strResourceGroupName;
		GameScreen* m_pGameScreen;

		Ogre::Vector3 ParseVector3(TiXmlElement* pXmlElement);
		Ogre::Quaternion ParseQuaternion(TiXmlElement* pXmlElement);
		Ogre::Quaternion ParseRotation(TiXmlElement* pXmlElement);
		Ogre::ColourValue ParseColor(TiXmlElement* pXmlElement);
		Ogre::Matrix4 ParseMatrix(TiXmlElement* pXmlElement);

		std::string GetAttribute(TiXmlElement* pXmlElement, std::string attributeName, std::string defaultValue = "");
		Ogre::Real GetAttributeAsReal(TiXmlElement* pXmlElement, std::string attributeName, Ogre::Real defaultValue = 0.0);
		bool GetAttributeAsBool(TiXmlElement* pXmlElement, std::string attributeName, bool defaultValue = false);
		
		void ProcessScene(TiXmlElement* pRootXmlElement, Ogre::SceneNode* pSceneNodeToAttachTo);
		void ProcessXmlNode(TiXmlNode* pXmlNode, Ogre::SceneNode* pSceneNodeToAttachTo);
		
		
	public:
		void Load(std::string strSceneFileName, GameScreen* pGameScreen, std::string strResourceGroupName, Ogre::SceneManager *pSceneManager, Ogre::SceneNode *pSceneNodeToAttachTo = NULL);
		static Ogre::Entity* FindFirstEntity(Ogre::SceneNode *pSceneNode);

	public:
		DotScenePlusLoader();
		~DotScenePlusLoader() {}
	};
}