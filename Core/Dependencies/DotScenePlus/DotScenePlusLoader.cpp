#include "StdAfx.h"
// DotScenePlusLoader
#include "DotScenePlusLoader.h"

// Base
//#include "Base.h"

// TinyXML
#include "tinyxml.h"

//OgreCollada
#include "OgreCollada.h"

#ifdef HAVOK
#include "HavokWrapper.h"
#endif

#ifdef PHYSX
#include "PhysXObject.h"
#endif

#include "Ogre.h"
#include "AIWorldManager.h"

using namespace std;

namespace GamePipe
{
	DotScenePlusLoader::DotScenePlusLoader()
	{
		m_pSceneManager = NULL;
		m_pGameScreen = NULL;
		m_strResourceGroupName = "";
	}

	void DotScenePlusLoader::ProcessXmlNode(TiXmlNode* pXmlNode, Ogre::SceneNode* pSceneNodeToAttachTo)
	{
		if (pXmlNode->Type() == TiXmlNode::ELEMENT)
		{
			if (Ogre::String(pXmlNode->Value()) == "node")
			{
				std::string sceneNodeName = GetAttribute(pXmlNode->ToElement(), "name", "");

				// check for the skybox, skydome or skyplane nodes
				if(sceneNodeName == "SkyBoxSystem")
				{
					std::string config = GetAttribute(pXmlNode->ToElement(), "configuration");
					std::stringstream intToString;
					int messageval;
					if(config == "SkyBox")
						messageval = (int)GLE_SkyBox_Create;
					if(config == "SkyDome")
						messageval = (int)GLE_SkyDome_Create;
					if(config == "SkyPlane")
						messageval = (int)GLE_SkyPlane_Create;

					intToString << messageval;
					config = intToString.str();
					config += "||0||" + GetAttribute(pXmlNode->ToElement(), "MaterialName") + 
							"||" + GetAttribute(pXmlNode->ToElement(), "Distance") + 
							"||" + GetAttribute(pXmlNode->ToElement(), "Display") + 
							"||" + GetAttribute(pXmlNode->ToElement(), "Curvature") + 
							"||" + GetAttribute(pXmlNode->ToElement(), "Tiling") +
							"||" + GetAttribute(pXmlNode->ToElement(), "Size") + 
							"||" + GetAttribute(pXmlNode->ToElement(), "SegmentsX") + 
							"||" + GetAttribute(pXmlNode->ToElement(), "SegmentsY") + ";;";
					
					EnginePtr->GetEditor()->Editor_Parser(0, config);
					return;
				}
            
				if (sceneNodeName != "Ogre/SceneRoot")
				{
					if (sceneNodeName == "")
					{
						pSceneNodeToAttachTo = pSceneNodeToAttachTo->createChildSceneNode();         //creates an unnamed object
					}
					else
					{
						//1st check if a node was already created thru code to avoid conflict between hard coded node and node saved in the scene file
						if(!m_pSceneManager->hasSceneNode(sceneNodeName))
							pSceneNodeToAttachTo = pSceneNodeToAttachTo->createChildSceneNode(sceneNodeName);//pSceneNodeToAttachTo = m_pSceneManager->getSceneNode(sceneNodeName);
						else
							pSceneNodeToAttachTo = m_pSceneManager->getSceneNode(sceneNodeName);
					}
				}

				TiXmlElement* pXmlElement = NULL;

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("position");
				if (pXmlElement)
				{
					pSceneNodeToAttachTo->setPosition(ParseVector3(pXmlElement));
					pSceneNodeToAttachTo->setInitialState();
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("scale");
				if (pXmlElement)
				{
					pSceneNodeToAttachTo->setScale(ParseVector3(pXmlElement));
					pSceneNodeToAttachTo->setInitialState();
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("quaternion");
				if (pXmlElement)
				{
					pSceneNodeToAttachTo->setOrientation(ParseQuaternion(pXmlElement));
					pSceneNodeToAttachTo->setInitialState();
				}
				else
				{
					pXmlElement = pXmlNode->ToElement()->FirstChildElement("rotation");
					if (pXmlElement)
					{
						pSceneNodeToAttachTo->setOrientation(ParseRotation(pXmlElement));
						pSceneNodeToAttachTo->setInitialState();
					}
					else
					{
						pXmlElement = pXmlNode->ToElement()->FirstChildElement("xform");
						if (pXmlElement)
						{
							Ogre::Matrix4 m4LocalTranformation = ParseMatrix(pXmlElement);

							//pSceneNodeToAttachTo->setPosition(m4LocalTranformation.getTrans());
							//pSceneNodeToAttachTo->setInitialState();
							pSceneNodeToAttachTo->setOrientation(m4LocalTranformation.extractQuaternion());
							pSceneNodeToAttachTo->setInitialState();
						}
					}
				}
			}
			else if (Ogre::String(pXmlNode->Value()) == "entity")
			{
				TiXmlElement* pXmlElement = pXmlNode->ToElement();

				std::string name = GetAttribute(pXmlElement, "name");
				std::string id = GetAttribute(pXmlElement, "id");
				std::string meshFile = GetAttribute(pXmlElement, "meshFile");
				std::string material = GetAttribute(pXmlElement, "material");
				bool bIsStatic = GetAttributeAsBool(pXmlElement, "static", false);
				bool bCastShadows = GetAttributeAsBool(pXmlElement, "castShadows", true);
				bool bIsVisible = GetAttributeAsBool(pXmlElement, "visible", true);

				std::string havokFile = GetAttribute(pXmlElement, "havokFile");
				std::string havokName = GetAttribute(pXmlElement, "havokName");   //not used so far
				std::string collisionShape = GetAttribute(pXmlElement,"CollisionShapeType");
				std::string objectType = GetAttribute(pXmlElement,"ObjectType");

                std::string luaFile = GetAttribute(pXmlElement, "luaFile");
				

				GGETRACELOG("Loaded");
				GGETRACELOG(objectType.c_str());

				GameObjectType eObjectType = DEFAULT_GAME_OBJECT_TYPE;	//initialize it to a default value for compatibility

				if(objectType.compare("Default Game Object") == 0)
					eObjectType = DEFAULT_GAME_OBJECT_TYPE;
				else if(objectType.compare("Graphics Object") == 0)
					eObjectType = GRAPHICS_OBJECT;
				else if(objectType.compare("Physics Fixed") == 0)
					eObjectType = PHYSICS_FIXED;
				else if(objectType.compare("Physics Dynamic") == 0)
					eObjectType = PHYSICS_DYNAMIC;
				else if(objectType.compare("Physics KeyFramed") == 0)
					eObjectType = PHYSICS_KEYFRAMED;
				else if(objectType.compare("Physics System") == 0)
					eObjectType = PHYSICS_SYSTEM;
				else if(objectType.compare("Physics Character Proxy") == 0)
					eObjectType = PHYSICS_CHARACTER_PROXY;
				else if(objectType.compare("Physics Character RigidBody") == 0)
					eObjectType = PHYSICS_CHARACTER_RIGIDBODY;
				else if(objectType.compare("Animated Character Proxy") == 0)
					eObjectType = ANIMATED_CHARACTER_PROXY;
				else if(objectType.compare("Animated Character RigidBody") == 0)
					eObjectType = ANIMATED_CHARACTER_RIGIDBODY;
				else if(objectType.compare("Phantom Shape") == 0)
					eObjectType = PHANTOM_SHAPE;
				else if(objectType.compare("Phantom AABB") == 0)
					eObjectType = PHANTOM_AABB;
				else if(objectType.compare("Vehicle Car") == 0)
					eObjectType = VEHICLE_CAR;
				else if(objectType.compare("Vehicle Motorcycle") == 0)
					eObjectType = VEHICLE_MOTORCYCLE;

				CollisionShapeType eCollisionShapeType = DEFAULT_COLLISION_SHAPE;	//initialize it to a default value for compatibility

				if(collisionShape.compare("Default Collision Shape") == 0)
					eCollisionShapeType = DEFAULT_COLLISION_SHAPE;
				else if(collisionShape.compare("XY Plane") == 0)
					eCollisionShapeType = COLLISION_SHAPE_XYPLANE;
				else if(collisionShape.compare("Sphere") == 0)
					eCollisionShapeType = COLLISION_SHAPE_SPHERE;
				else if(collisionShape.compare("Capsule") == 0)
					eCollisionShapeType = COLLISION_SHAPE_CAPSULE;
				else if(collisionShape.compare("Cylinder") == 0)
					eCollisionShapeType = COLLISION_SHAPE_CYLINDER;
				else if(collisionShape.compare("Box") == 0)
					eCollisionShapeType = COLLISION_SHAPE_BOX;
				else if(collisionShape.compare("Convex Hull") == 0)
					eCollisionShapeType = COLLISION_SHAPE_CONVEX_HULL;
				// Process vertex buffer
				// Process index buffer

				Ogre::Entity* pEntity = NULL;
				GameObject* pGameObject = NULL;
				if (false)	// will never be executed
				{
					try
					{
						if (meshFile == "PhysicsUnitPlane.mesh")		//i think this part if entities r created from Ogre base types
						{
							Ogre::StaticGeometry* pStaticGeometry;
							pStaticGeometry = m_pSceneManager->createStaticGeometry("PhysicsUnitPlaneStaticGeometry");
							pEntity = m_pSceneManager->createEntity(name, meshFile);
							pStaticGeometry->addEntity(pEntity, pSceneNodeToAttachTo->getPosition(), pSceneNodeToAttachTo->getOrientation(), pSceneNodeToAttachTo->getScale());
							pStaticGeometry->build();
						}
						else if (meshFile == "PhysicsUnitCube.mesh")			//check why r they not creating the ojects over here
						{
							Ogre::Vector3 v3Scale = pSceneNodeToAttachTo->getScale();
						}
						else if (meshFile == "PhysicsUnitSphere.mesh")
						{
							Ogre::Vector3 v3Scale = pSceneNodeToAttachTo->getScale();
						}
						else if (meshFile == "PhysicsUnitCylinder.mesh")
						{
							Ogre::Vector3 v3Scale = pSceneNodeToAttachTo->getScale();
						}
						else
						{
							Ogre::Vector3 v3Scale = pSceneNodeToAttachTo->getScale();
						}
					}
					catch (...)
					{
						GGETRACELOG("Loader error creating entity or physics body for: %s", meshFile.c_str());
					}
				}
				else
				{
					if (havokFile != "" || havokName != "")
					{
                        if(luaFile != ""){
							AIWorldManager::useRecast = true;
                            pGameObject = new GameObject((char*)name.c_str(),
                                                        (char*)meshFile.c_str(),
                                                        (char*)havokFile.c_str(),
                                                        eObjectType,
                                                        DEFAULT_COLLISION_SHAPE,
                                                        luaFile,true);
                        }
                        else{
						    pGameObject = new GameObject((char*)name.c_str(),		//y ignore the collision shape over here and y is physics dynamic? i think they use the default value
													     (char*)meshFile.c_str(),	// i think they r writing PHYSICS_DYNAMICS cuz they r not saving the object type and so lose track of the the type
													     (char*)havokFile.c_str(),
													      eObjectType,
														  DEFAULT_COLLISION_SHAPE
														  );
                        }
						
						//check if both the SceneNode names are different; else u'll get an exception because of trying to access an unallocated memory location
						// this is to handle the nodes which are saved with SceneNode substring in them as SceneNode is also added to create a scene node for an entity in GraphicsObject.cpp 
						if(pGameObject->m_pGraphicsObject->m_pOgreSceneNode->getName().compare(pSceneNodeToAttachTo->getName()) != 0)	
							pGameObject->changeSceneNode(pSceneNodeToAttachTo);
                        pEntity = pGameObject->m_pGraphicsObject->m_pOgreEntity;
						Ogre::Vector3 parentPosition = pSceneNodeToAttachTo->getPosition();
						pGameObject->setPosition(parentPosition.x,parentPosition.y,parentPosition.z);
                        Ogre::Quaternion rot = pSceneNodeToAttachTo->getOrientation();

#ifdef HAVOK
                        pGameObject->m_pPhysicsObject->getRigidBody()->setRotation(hkQuaternion(rot.x,rot.y,rot.z,rot.w));
#endif

#ifdef PHYSX
						physx::PxVec3 Pos = physx::PxVec3(parentPosition.x, parentPosition.y, parentPosition.z);
						physx::PxQuat Quat = physx::PxQuat(rot.x, rot.y, rot.z, rot.w);
						physx::PxRigidDynamic *dynBody = pGameObject->m_pPhysicsObject->getDynamicRigidBody();
						if(dynBody != NULL)
							dynBody->setGlobalPose(physx::PxTransform(Pos, Quat));
#endif
					}
					else
					{
						// Check the filename to see if the Entity comes from a mesh file or a Collada file
						string::size_type lastPos = meshFile.find_last_of(".", meshFile.size());
						if (lastPos != std::string::npos)
						{
							std::string ext = meshFile.substr(lastPos, meshFile.size() - lastPos);
							if (ext == ".mesh")
							{
								//before creating check if an entity of the same name was created thru code
								if(!m_pSceneManager->hasEntity(name))
								{
									if(luaFile != "")
									{
										AIWorldManager::useRecast = true;
										pGameObject = new GameObject((char*)name.c_str(),
																 (char*)meshFile.c_str(),
																 (char*)havokFile.c_str(), //make it ""
																 eObjectType,
																 eCollisionShapeType,
																 luaFile,true);
									}
									else
									{
										pGameObject = new GameObject((char*)name.c_str(),
																 (char*)meshFile.c_str(),
																 (char*)havokFile.c_str(), //make it ""
																 eObjectType,
																 eCollisionShapeType,
																 luaFile);
									}

									if(pGameObject->m_pGraphicsObject->m_pOgreSceneNode->getName().compare(pSceneNodeToAttachTo->getName()) != 0)	
										pGameObject->changeSceneNode(pSceneNodeToAttachTo);
									pEntity = pGameObject->m_pGraphicsObject->m_pOgreEntity;

									//Ogre::Vector3 parentPosition = pSceneNodeToAttachTo->getPosition();
									Ogre::Vector3 parentPosition = pEntity->getParentSceneNode()->_getDerivedPosition();
									Ogre::Vector3 derivedScale = pEntity->getParentSceneNode()->_getDerivedScale();
									//Ogre::Vector3 vctParentScale = pSceneNodeToAttachTo->getScale();

									/*if(name == "house1")
									{
										pGameObject->scale(4.0f, 4.0f, 4.0f);
										pGameObject->setPosition(40.0f, 1.0f, 0.0f);
									}
									else*/
									{
										pGameObject->scale(1, 1, 1);
										pGameObject->setPosition(parentPosition.x,parentPosition.y,parentPosition.z);
									}
								}
								else
									pEntity = m_pSceneManager->getEntity(name);

							}
							else if (ext == ".dae")
							{
								//TODO make it dynamic loading.
								Ogre::SceneNode *pColladaNode = NULL;
								OgreCollada::ImpExp *pImpExp = NULL;
								pImpExp = OgreCollada::CreateImpExp(Ogre::Root::getSingletonPtr(), m_pSceneManager);
								std::string strpath = "../../" + EnginePtr->gameFolder + "/Media/Collada/" + meshFile;
								//Ogre::StringVectorPtr info = Ogre::ResourceGroupManager::getSingletonPtr()->findGroupContainingResource(meshFile);
								//list<Ogre::ResourceGroupManager::LocationList>::iterator temp = Ogre::ResourceGroupManager::getSingletonPtr()->getResourceGroup("General")->locationList.begin();

								const OgreCollada::char_t *path = (const OgreCollada::char_t*)strpath.c_str();
								bool rtn = pImpExp->importColladaGetSceneNode(&pColladaNode, path);
								OgreCollada::DestroyImpExp(pImpExp);

								if (pColladaNode != NULL)
								{
									if (Ogre::Entity *pFirstFoundEntity = FindFirstEntity(pColladaNode))
									{
										dynamic_cast<Ogre::SceneNode*>(pColladaNode->getParent())->removeAndDestroyChild(pColladaNode->getName());
										pFirstFoundEntity->setUserAny(static_cast<Ogre::Any>(meshFile));
										std::string colladaFile = Ogre::any_cast<string>(pFirstFoundEntity->getUserAny());
										pSceneNodeToAttachTo->attachObject(pFirstFoundEntity);
									}
								}
							}
						}
					}
				}

				if (pEntity)
				{
					pEntity->setCastShadows(bCastShadows);
					pEntity->setVisible(bIsVisible);

					if (material != "")
					{
						pEntity->setMaterialName(material);
					}

					for (TiXmlNode* pSubEntityXmlNode = pXmlNode->FirstChild(); pSubEntityXmlNode != NULL; pSubEntityXmlNode = pSubEntityXmlNode->NextSibling())
					{
						if (pSubEntityXmlNode->Type() == TiXmlNode::ELEMENT)
						{
							if (Ogre::String(pSubEntityXmlNode->Value()) == "subentity")
							{
								unsigned int index = static_cast<unsigned int>(GetAttributeAsReal(pSubEntityXmlNode->ToElement(), "index", 0));
								std::string material = GetAttribute(pSubEntityXmlNode->ToElement(), "materialName");
								bool bIsVisible = GetAttributeAsBool(pSubEntityXmlNode->ToElement(), "visible", true);

								if (pEntity->getSubEntity(index))
								{
									pEntity->getSubEntity(index)->setMaterialName(material);
									pEntity->getSubEntity(index)->setVisible(bIsVisible);
								}
							}
						}
					}
				}
			}
			else if (Ogre::String(pXmlNode->Value()) == "light")
			{
				TiXmlElement* pXmlElement = pXmlNode->ToElement();

				std::string name = GetAttribute(pXmlElement, "name");
				std::string id = GetAttribute(pXmlElement, "id");
				bool bIsVisible = GetAttributeAsBool(pXmlElement, "visible", true);
				bool bCastShadows = GetAttributeAsBool(pXmlElement, "castShadows", true);

				Ogre::Light* pLight;
				if(m_pSceneManager->hasLight(name))
				{
					pLight = m_pSceneManager->getLight(name);
				}
				else
				{
					pLight = m_pSceneManager->createLight(name);
					pSceneNodeToAttachTo->attachObject(pLight);
				}

				std::string type = GetAttribute(pXmlElement, "type");

				if (type == "point")
				{
					pLight->setType(Ogre::Light::LT_POINT);
				}
				else if (type == "directional")
				{
					pLight->setType(Ogre::Light::LT_DIRECTIONAL);
				}
				else if (type == "spot")
				{
					pLight->setType(Ogre::Light::LT_SPOTLIGHT);
				}
				else if (type == "radPoint")
				{
					pLight->setType(Ogre::Light::LT_POINT);
				}

				pLight->setVisible(bIsVisible);
				pLight->setCastShadows(bCastShadows);

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("position");
				if (pXmlElement)
				{
					pLight->setPosition(ParseVector3(pXmlElement));
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("normal");
				if (pXmlElement)
				{
					pLight->setDirection(pLight->getParentSceneNode()->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z);
					//pLight->setDirection(ParseVector3(pXmlElement));
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("colourDiffuse");
				if (pXmlElement)
				{
					pLight->setDiffuseColour(ParseColor(pXmlElement));
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("colourSpecular");
				if (pXmlElement)
				{
					pLight->setSpecularColour(ParseColor(pXmlElement));
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("lightRange");
				if (pXmlElement)
				{
					Ogre::Real inner = GetAttributeAsReal(pXmlElement, "inner");
					Ogre::Real outer = GetAttributeAsReal(pXmlElement, "outer");
					Ogre::Real falloff = GetAttributeAsReal(pXmlElement, "falloff", 1.0);

					if (pLight->getType() == Ogre::Light::LT_SPOTLIGHT)
					{
						pLight->setSpotlightRange(Ogre::Angle(inner), Ogre::Angle(outer), falloff);
					}
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("lightAttenuation");
				if (pXmlElement)
				{
					Ogre::Real range = GetAttributeAsReal(pXmlElement, "range");
					Ogre::Real constant = GetAttributeAsReal(pXmlElement, "constant");
					Ogre::Real linear = GetAttributeAsReal(pXmlElement, "linear");
					Ogre::Real quadratic = GetAttributeAsReal(pXmlElement, "quadratic");

					pLight->setAttenuation(range, constant, linear, quadratic);
				}
			}
			else if (Ogre::String(pXmlNode->Value()) == "camera")
			{
				TiXmlElement* pXmlElement = pXmlNode->ToElement();

				std::string name = GetAttribute(pXmlElement, "name");
				std::string id = GetAttribute(pXmlElement, "id");
				Ogre::Real fov = GetAttributeAsReal(pXmlElement, "fov", 45);
				Ogre::Real aspectRatio = GetAttributeAsReal(pXmlElement, "aspectRatio", (Ogre::Real)1.3333);
				std::string projectionType = GetAttribute(pXmlElement, "projectionType", "perspective");

				if (m_pSceneManager->hasCamera(name) == false)
				{
					Ogre::Camera* pCamera = m_pSceneManager->createCamera(name);
					pSceneNodeToAttachTo->attachObject(pCamera);

					pCamera->setFOVy(Ogre::Degree(fov));
					pCamera->setAspectRatio(aspectRatio);

					if (projectionType == "perspective")
					{
						pCamera->setProjectionType(Ogre::PT_PERSPECTIVE);
					}
					else if (projectionType == "orthographic")
					{
						pCamera->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
					}

					pXmlElement = pXmlNode->ToElement()->FirstChildElement("clipping");
					if (pXmlElement)
					{
						Ogre::Real nearDistance;
						Ogre::Real farDistance;

						if (pXmlElement->Attribute("near"))
						{
							nearDistance = GetAttributeAsReal(pXmlElement, "near");
						}
						else if (pXmlElement->Attribute("nearPlaneDist"))
						{
							nearDistance = GetAttributeAsReal(pXmlElement, "nearPlaneDist");
						}

						if (pXmlElement->Attribute("far"))
						{
							farDistance = GetAttributeAsReal(pXmlElement, "far");
						}
						else if (pXmlElement->Attribute("farPlaneDist"))
						{
							farDistance = GetAttributeAsReal(pXmlElement, "farPlaneDist");
						}

						pCamera->setNearClipDistance(nearDistance);
						pCamera->setFarClipDistance(farDistance);
					}

					pXmlElement = pXmlNode->ToElement()->FirstChildElement("position");
					if (pXmlElement)
					{
						pCamera->setPosition(ParseVector3(pXmlElement));
					}

					pXmlElement = pXmlNode->ToElement()->FirstChildElement("quaternion");
					if (pXmlElement)
					{
						pCamera->setOrientation(ParseQuaternion(pXmlElement));
					}
					else
					{
						pXmlElement = pXmlNode->ToElement()->FirstChildElement("xform");
						if (pXmlElement)
						{
							Ogre::Matrix4 m4LocalTranformation = ParseMatrix(pXmlElement);

							//pCamera->setPosition(m4LocalTranformation.getTrans());
							pCamera->setOrientation(m4LocalTranformation.extractQuaternion());
						}
					}

					pXmlElement = pXmlNode->ToElement()->FirstChildElement("normal");
					if (pXmlElement)
					{
						pCamera->setDirection(ParseVector3(pXmlElement));
					}

					pXmlElement = pXmlNode->ToElement()->FirstChildElement("lookTarget");
					if (pXmlElement)
					{
						// Look target - is it a vector or a node to look at?
						pCamera->lookAt(ParseVector3(pXmlElement));
					}

					pXmlElement = pXmlNode->ToElement()->FirstChildElement("trackTarget");
					if (pXmlElement)
					{
						// Track target - offset not implemented, attribute name must be checked.
						pCamera->setAutoTracking(true, m_pSceneManager->getSceneNode(GetAttribute(pXmlElement, "node", "Nodes")), Ogre::Vector3::ZERO);
					}

					m_pGameScreen->SetActiveCamera(pCamera);
				}
				else
				{
					// Report that the camera was not created because a camera with the same name already exists in the scene
					GGETRACELOG("Could not create camera %s, a camera with the same name already exist", name.c_str());
				}
			}
			else if (Ogre::String(pXmlNode->Value()) == "environment")
			{
				TiXmlElement* pXmlElement = NULL;

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("skyBox");
				if (pXmlElement)
				{
					std::string material = GetAttribute(pXmlElement, "material");
					Ogre::Real distance = GetAttributeAsReal(pXmlElement, "distance", 5000);
					bool bDrawFirst = GetAttributeAsBool(pXmlElement, "drawFirst", true);

					TiXmlElement* pXmlElementInner = NULL;

					Ogre::Quaternion rotation = Ogre::Quaternion::IDENTITY;
					pXmlElementInner = pXmlElement->FirstChildElement("quaternion");
					if (pXmlElementInner)
					{
						rotation = ParseQuaternion(pXmlElementInner);
					}
					else
					{
						pXmlElementInner = pXmlElement->FirstChildElement("xform");
						if (pXmlElementInner)
						{
							Ogre::Matrix4 m4LocalTranformation = ParseMatrix(pXmlElementInner);
							rotation = m4LocalTranformation.extractQuaternion();
						}
					}

					//rotation = Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y);

					m_pSceneManager->setSkyBox(true, material, distance, bDrawFirst, rotation, m_strResourceGroupName);
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("colourAmbient");
				if (pXmlElement)
				{
					m_pSceneManager->setAmbientLight(ParseColor(pXmlElement));
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("colourBackground");
				if (pXmlElement)
				{
					m_pGameScreen->SetBackgroundColor(ParseColor(pXmlElement));
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("fog");
				if (pXmlElement)
				{
					Ogre::Real expDensity = GetAttributeAsReal(pXmlElement, "expDensity", (Ogre::Real)0.001);
					Ogre::Real linearStart = GetAttributeAsReal(pXmlElement, "linearStart", 0.0);
					Ogre::Real linearEnd = GetAttributeAsReal(pXmlElement, "linearEnd", 1.0);

					Ogre::FogMode fogMode = Ogre::FOG_NONE;
					std::string strMode = GetAttribute(pXmlElement, "mode");

					if (strMode == "none")
					{
						fogMode = Ogre::FOG_NONE;
					}
					else if (strMode == "exp")
					{
						fogMode = Ogre::FOG_EXP;
					}
					else if (strMode == "exp2")
					{
						fogMode = Ogre::FOG_EXP2;
					}
					else if (strMode == "linear")
					{
						fogMode = Ogre::FOG_LINEAR;
					}

					TiXmlElement* pXmlElementInner = NULL;

					Ogre::ColourValue colorDiffuse = Ogre::ColourValue::White;
					pXmlElementInner = pXmlElement->FirstChildElement("colorDiffuse");
					if (pXmlElementInner)
					{
						colorDiffuse = ParseColor(pXmlElementInner);
					}

					m_pSceneManager->setFog(fogMode, colorDiffuse, expDensity, linearStart, linearEnd);
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("skyDome");			// as of now the code only processes for skybox
				if (pXmlElement)
				{
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("skyPlane");
				if (pXmlElement)
				{
				}

				pXmlElement = pXmlNode->ToElement()->FirstChildElement("clipping");
				if (pXmlElement)
				{
				}
			}
			else if (Ogre::String(pXmlNode->Value()) == "terrain")
			{
			}
			else if (Ogre::String(pXmlNode->Value()) == "externals")
			{
			}
			else if (Ogre::String(pXmlNode->Value()) == "userDataReference")
			{
			}
			else if (Ogre::String(pXmlNode->Value()) == "octree")
			{
			}
			else if (Ogre::String(pXmlNode->Value()) == "collada")
			{
				//TiXmlElement* pXmlElement = pXmlNode->ToElement();
				//
				//std::string name = GetAttribute(pXmlElement, "name");
				//
				//std::string gameFolder = GamePipe::Engine::GetInstancePointer()->gameFolder;
				//std::string source = "../../";
				//source.append(gameFolder);
				//source.append("/Media/Collada/");
				//source.append(GetAttribute(pXmlElement, "source"));
				//
				//const OgreCollada::char_t* csource = (const OgreCollada::char_t*)(L"../../Game/Media/Collada/sphere.dae"); //TODO-SV: path cant be hardcoded
				//OgreCollada::ImpExp* pImpExp = NULL;
				//pImpExp = OgreCollada::CreateImpExp(Ogre::Root::getSingletonPtr(), m_pSceneManager);
				////bool rtn = pImpExp->importCollada((const OgreCollada::char_t*)("../../Game/Media/Collada/sphere.dae"));
				//bool rtn = pImpExp->importCollada(csource);
				//OgreCollada::DestroyImpExp(pImpExp);
			}
		}
		else if (pXmlNode->Type() == TiXmlNode::TEXT)
		{
		}

		for (pXmlNode = pXmlNode->FirstChild(); pXmlNode != NULL; pXmlNode = pXmlNode->NextSibling())
		{
			ProcessXmlNode(pXmlNode, pSceneNodeToAttachTo);
		}
	}

	void DotScenePlusLoader::ProcessScene(TiXmlElement* pRootXmlElement, Ogre::SceneNode* pSceneNodeToAttachTo)
	{
		GGETRACELOG("Version: %s, Id: %s, Scene manager: %s, Min Ogre version: %s, Author: %s",
			GetAttribute(pRootXmlElement, "formatVersion", "unknown").c_str(),
			GetAttribute(pRootXmlElement, "ID", "unknown").c_str(),
			GetAttribute(pRootXmlElement, "pSceneManager", "unknown").c_str(),
			GetAttribute(pRootXmlElement, "minOgreVersion", "unknown").c_str(),
			GetAttribute(pRootXmlElement, "author", "unknown").c_str());

		ProcessXmlNode(pRootXmlElement, pSceneNodeToAttachTo);

		return;
	}

	Ogre::Vector3 DotScenePlusLoader::ParseVector3(TiXmlElement* pXmlElement)
	{
		return Ogre::Vector3(
			Ogre::StringConverter::parseReal(pXmlElement->Attribute("x")),
			Ogre::StringConverter::parseReal(pXmlElement->Attribute("y")),
			Ogre::StringConverter::parseReal(pXmlElement->Attribute("z")));
	}

	Ogre::Quaternion DotScenePlusLoader::ParseQuaternion(TiXmlElement* pXmlElement)
	{
		Ogre::Quaternion qOrientation;

		if (pXmlElement->Attribute("x") && pXmlElement->Attribute("y") && pXmlElement->Attribute("z") && pXmlElement->Attribute("w"))
		{
			qOrientation.x = Ogre::StringConverter::parseReal(pXmlElement->Attribute("x"));
			qOrientation.y = Ogre::StringConverter::parseReal(pXmlElement->Attribute("y"));
			qOrientation.z = Ogre::StringConverter::parseReal(pXmlElement->Attribute("z"));
			qOrientation.w = Ogre::StringConverter::parseReal(pXmlElement->Attribute("w"));
		}
		if (pXmlElement->Attribute("qx") && pXmlElement->Attribute("qy") && pXmlElement->Attribute("qz") && pXmlElement->Attribute("qw"))
		{
			qOrientation.x = Ogre::StringConverter::parseReal(pXmlElement->Attribute("qx"));
			qOrientation.y = Ogre::StringConverter::parseReal(pXmlElement->Attribute("qy"));
			qOrientation.z = Ogre::StringConverter::parseReal(pXmlElement->Attribute("qz"));
			qOrientation.w = Ogre::StringConverter::parseReal(pXmlElement->Attribute("qw"));
		}

		return qOrientation;
	}

	Ogre::Quaternion DotScenePlusLoader::ParseRotation(TiXmlElement* pXmlElement)
	{
		Ogre::Quaternion qOrientation = Ogre::Quaternion::IDENTITY;

		if (pXmlElement->Attribute("x") && pXmlElement->Attribute("y") && pXmlElement->Attribute("z"))
		{
			qOrientation = Ogre::Quaternion(Ogre::Degree(Ogre::StringConverter::parseReal(pXmlElement->Attribute("x"))), Ogre::Vector3::UNIT_X) *
				Ogre::Quaternion(Ogre::Degree(Ogre::StringConverter::parseReal(pXmlElement->Attribute("y"))), Ogre::Vector3::UNIT_Y) *
				Ogre::Quaternion(Ogre::Degree(Ogre::StringConverter::parseReal(pXmlElement->Attribute("z"))), Ogre::Vector3::UNIT_Z);
		}

		return qOrientation;
	}

	Ogre::ColourValue DotScenePlusLoader::ParseColor(TiXmlElement* pXmlElement)
	{
		return Ogre::ColourValue(
			Ogre::StringConverter::parseReal(pXmlElement->Attribute("r")),
			Ogre::StringConverter::parseReal(pXmlElement->Attribute("g")),
			Ogre::StringConverter::parseReal(pXmlElement->Attribute("b")),
			pXmlElement->Attribute("a") == NULL ? 1 : Ogre::StringConverter::parseReal(pXmlElement->Attribute("a")));
	}

	Ogre::Matrix4 DotScenePlusLoader::ParseMatrix(TiXmlElement* pXmlElement)
	{
		Ogre::Matrix4 m4LocalTransformation;
		for (unsigned int row = 0; row < 4; ++row)
		{
			for (unsigned int column = 0; column < 4; ++column)
			{
				m4LocalTransformation[row][column] =
					GetAttributeAsReal(pXmlElement, "v" + Ogre::StringConverter::toString(row * 4 + column), 1.0);
			}
		}

		return m4LocalTransformation.transpose();
	}

	std::string DotScenePlusLoader::GetAttribute(TiXmlElement* pXmlElement, std::string attributeName, std::string defaultValue)
	{
		if (pXmlElement->Attribute(attributeName.c_str()))
		{
			return pXmlElement->Attribute(attributeName.c_str());
		}
		else
		{
			return defaultValue;
		}
	}

	Ogre::Real DotScenePlusLoader::GetAttributeAsReal(TiXmlElement* pXmlElement, std::string attributeName, Ogre::Real defaultValue)
	{
		if (pXmlElement->Attribute(attributeName.c_str()))
		{
			return Ogre::StringConverter::parseReal(pXmlElement->Attribute(attributeName.c_str()));
		}
		else
		{
			return defaultValue;
		}
	}

	bool DotScenePlusLoader::GetAttributeAsBool(TiXmlElement* pXmlElement, std::string attributeName, bool defaultValue)
	{
		if (!pXmlElement->Attribute(attributeName.c_str()))
		{
			return defaultValue;
		}
		else if (Ogre::String(pXmlElement->Attribute(attributeName.c_str())) == "true" ||
			Ogre::String(pXmlElement->Attribute(attributeName.c_str())) == "True" ||
			Ogre::String(pXmlElement->Attribute(attributeName.c_str())) == "1")
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void DotScenePlusLoader::Load(std::string strSceneFileName, GameScreen* pGameScreen, std::string strResourceGroupName, Ogre::SceneManager *pSceneManager, Ogre::SceneNode *pSceneNodeToAttachTo)
	{
		GGETRACELOG("Loader initializing");

		TiXmlDocument* pXmlDocument = NULL;
		TiXmlElement* pRootXmlElement = NULL;
		Ogre::DataStreamPtr pDataStream;

		try
		{
			pDataStream = Ogre::ResourceGroupManager::getSingleton().openResource(strSceneFileName, strResourceGroupName);
			Ogre::String data = pDataStream->getAsString();

			pXmlDocument = new TiXmlDocument();
			pXmlDocument->Parse(data.c_str());
			pDataStream->close();
			pDataStream.setNull();

			if (pXmlDocument->Error())
			{
				GGETRACELOG("Loader cannot parse scene file: %s because: %s", strSceneFileName, pXmlDocument->ErrorDesc());
				delete pXmlDocument;
				pXmlDocument = NULL;

				return;
			}
		}
		catch(...)
		{
			GGETRACELOG("Loader error creating or parsing xml scene: %s because: %s", strSceneFileName, pXmlDocument->ErrorDesc());
			delete pXmlDocument;
			pXmlDocument = NULL;

			return;
		}

		pRootXmlElement = pXmlDocument->RootElement();
		if (Ogre::String(pRootXmlElement->Value()) != "scene")
		{
			GGETRACELOG("Loader expected root node <scene> in .scene file: %s", strSceneFileName);
			delete pXmlDocument;
			pXmlDocument = NULL;

			return;
		}

		if (!pSceneNodeToAttachTo)
		{
			pSceneNodeToAttachTo = pSceneManager->getRootSceneNode();
		}

		m_pSceneManager = pSceneManager;
		m_strResourceGroupName = strResourceGroupName;
		m_pGameScreen = pGameScreen;
		ProcessScene(pRootXmlElement, pSceneNodeToAttachTo);

		delete pXmlDocument;
		pXmlDocument = NULL;

		GGETRACELOG("Load complete");
	}

	Ogre::Entity* DotScenePlusLoader::FindFirstEntity(Ogre::SceneNode *pSceneNode)
	{
		// See if the current SceneNode has an attached Entity
		Ogre::SceneNode::ObjectIterator objectIterator = pSceneNode->getAttachedObjectIterator();
		while (objectIterator.hasMoreElements())
		{
			if (Ogre::Entity *tempEnt = dynamic_cast<Ogre::Entity*>(objectIterator.getNext()))
			{
				// We found an entity, return it
				//Ogre::String name = tempEnt->getName();
				return tempEnt;
			}
		}

		// Since this SceneNode didnt had an Entity, run FindFirstEntity on each of this SceneNode's childs if they exist
		Ogre::SceneNode::ChildNodeIterator childNodeIterator = pSceneNode->getChildIterator();
		while (childNodeIterator.hasMoreElements())
		{
			if (Ogre::SceneNode *childSceneNode = dynamic_cast<Ogre::SceneNode*>(childNodeIterator.getNext()))
			{
				if (Ogre::Entity *tempEnt = FindFirstEntity(childSceneNode))
				{
					// An Entity was found in one of the branches. Return it
					return tempEnt;
				}
			}
		}

		return NULL;
	}
}