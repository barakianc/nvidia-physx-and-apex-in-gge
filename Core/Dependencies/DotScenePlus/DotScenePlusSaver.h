#pragma once

#include <string>
// GameScreen
#include "GameScreen.h"
//#include "GameObject.h"

class GameScreen;
class TiXmlElement;
class TiXmlNode;


namespace GamePipe
{
	class DotScenePlusSaver
	{
	private:
		Ogre::SceneManager* m_pSceneManager;
		GameScreen* m_pGameScreen;
		
		void WriteVector3(TiXmlElement* pXmlElement, Ogre::Vector3 v3Vector);
		void WriteQuaternion(TiXmlElement* pXmlElement, Ogre::Quaternion qQuaternion);
		void WriteColor(TiXmlElement* pXmlElement, Ogre::ColourValue cvColor);
		void WriteMatrix(TiXmlElement* pXmlElement, Ogre::Matrix4 m4Matrix);

		std::string GetAttribute(TiXmlElement* pXmlElement, std::string attributeName, std::string defaultValue = "");
		Ogre::Real GetAttributeAsReal(TiXmlElement* pXmlElement, std::string attributeName, Ogre::Real defaultValue = 0.0);
		bool GetAttributeAsBool(TiXmlElement* pXmlElement, std::string attributeName, bool defaultValue = false);
		
		void ProcessScene(TiXmlElement* pSceneXmlElement, Ogre::Node* pNodeToSave);
		void ProcessNode(TiXmlElement* pSceneXmlElement, Ogre::Node* pNodeToSave);

	public:
		void Save(std::string strSceneFileName, GameScreen* pGameScreen, Ogre::SceneManager *pSceneManager, Ogre::Node* pNodeToSave);

	public:
		DotScenePlusSaver();
		~DotScenePlusSaver() {}
	};
}