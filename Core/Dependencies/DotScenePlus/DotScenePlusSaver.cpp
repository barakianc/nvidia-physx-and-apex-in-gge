#include "StdAfx.h"
// DotScenePlusSaver
#include "DotScenePlusSaver.h"

// Engine
#include "Engine.h"

// TinyXML
#include "tinyxml.h"

#ifdef HAVOK
#include "HavokWrapper.h"
#endif

using namespace std;

namespace GamePipe
{
	DotScenePlusSaver::DotScenePlusSaver()
	{
		m_pSceneManager = NULL;
		m_pGameScreen = NULL;
	}

	void DotScenePlusSaver::ProcessNode(TiXmlElement* pSceneXmlElement, Ogre::Node* pNodeToSave)
	{
		// Process the nodes from the skybox system if they are part of it
		if(pNodeToSave == EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyBoxNode() ||
			pNodeToSave == EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyDomeNode() ||
			pNodeToSave == EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyPlaneNode())
		{
			TiXmlElement* pXmlNodeElement = new TiXmlElement("node");
			pSceneXmlElement->LinkEndChild(pXmlNodeElement);
			pXmlNodeElement->SetAttribute("name", "SkyBoxSystem");
			GGETRACELOG(pNodeToSave->getName().c_str());

			/*TiXmlElement* pXmlNameElement = new TiXmlElement("Name");
			pXmlNodeElement->LinkEndChild(pXmlNameElement);
			pXmlNameElement->SetAttribute("Designation",pNodeToSave->getName().c_str());
			GGETRACELOG(pNodeToSave->getName().c_str());*/

			std::vector<Ogre::String> tokens;
			EnginePtr->GetEditor()->AltTokenize(EnginePtr->GetEditor()->AskStatusMessage(GLE_SkyBox_Create).c_str(), tokens, "||");

			// select the configuration
			Ogre::String configuration;
			int conf = atoi(tokens[0].c_str());
			switch(conf)
			{
			case GLE_SkyBox_Create:
				configuration = "SkyBox";
				break;
			case GLE_SkyDome_Create:
				configuration = "SkyDome";
				break;
			case GLE_SkyPlane_Create:
				configuration = "SkyPlane";
				break;
			default:
				configuration = "SkyBox";
			}

			pXmlNodeElement->SetAttribute("configuration", configuration.c_str());
			pXmlNodeElement->SetAttribute("MaterialName", tokens[2].c_str());
			pXmlNodeElement->SetAttribute("Distance", tokens[3].c_str());
			pXmlNodeElement->SetAttribute("Display", tokens[4].c_str());
			pXmlNodeElement->SetAttribute("Curvature", tokens[5].c_str());
			pXmlNodeElement->SetAttribute("Tiling", tokens[6].c_str());
			pXmlNodeElement->SetAttribute("Size", tokens[7].c_str());
			pXmlNodeElement->SetAttribute("SegmentsX", tokens[8].c_str());
			pXmlNodeElement->SetAttribute("SegmentsY", tokens[9].c_str());
			GGETRACELOG(pNodeToSave->getName().c_str());
			return;
		}

		//Don't save nodes if they are part of the Dynamic Editing Tool
		if( pNodeToSave == EnginePtr->GetEditor()->m_DynamicToolXNode ||
			pNodeToSave == EnginePtr->GetEditor()->m_DynamicToolYNode ||
			pNodeToSave == EnginePtr->GetEditor()->m_DynamicToolZNode ||
			pNodeToSave == EnginePtr->GetEditor()->m_FreeDynamicToolNode)
		{
			return;
		}


		// TODO-SV: MainMenuScreenCameraSceneNode is not generic
		if (pNodeToSave->getName() != "MainMenuScreenCameraSceneNode" &&
			pNodeToSave->getName() != "Editor_CameraPerspective_SceneNode" &&
			pNodeToSave->getName() != "Editor_CameraFront_SceneNode" &&
			pNodeToSave->getName() != "Editor_CameraTop_SceneNode" &&
			pNodeToSave->getName() != "Editor_CameraSide_SceneNode")
		{
			GGETRACELOG("Saving Node Tags");

			TiXmlElement* pXmlNodeElement = new TiXmlElement("node");
			pSceneXmlElement->LinkEndChild(pXmlNodeElement);
			pXmlNodeElement->SetAttribute("name", pNodeToSave->getName().c_str());
			GGETRACELOG(pNodeToSave->getName().c_str());

			TiXmlElement* pPositionXmlElement = new TiXmlElement("position");
			pXmlNodeElement->LinkEndChild(pPositionXmlElement);
			WriteVector3(pPositionXmlElement, pNodeToSave->getPosition());

			TiXmlElement* pQuaternionXmlElement = new TiXmlElement("quaternion");
			pXmlNodeElement->LinkEndChild(pQuaternionXmlElement);
			WriteQuaternion(pQuaternionXmlElement, pNodeToSave->getOrientation());

			TiXmlElement* pScaleXmlElement = new TiXmlElement("scale");
			pXmlNodeElement->LinkEndChild(pScaleXmlElement);
			WriteVector3(pScaleXmlElement, pNodeToSave->getScale());

			Ogre::Matrix4 m4TransformationMatrix;
			m4TransformationMatrix.makeTransform(pNodeToSave->getPosition(), pNodeToSave->getScale(), pNodeToSave->getOrientation());
			TiXmlElement* pTransformXmlElement = new TiXmlElement("xform");
			pXmlNodeElement->LinkEndChild(pTransformXmlElement);
			WriteMatrix(pTransformXmlElement, m4TransformationMatrix);

			Ogre::SceneManager::CameraIterator cameraIterator = m_pSceneManager->getCameraIterator();    
			while (cameraIterator.hasMoreElements())
			{

				Ogre::Camera* pCamera = cameraIterator.getNext();
				if (pCamera->getParentSceneNode() == static_cast<Ogre::SceneNode*>(pNodeToSave))
				{
					TiXmlElement* pCameraXmlElement = new TiXmlElement("camera");
					pXmlNodeElement->LinkEndChild(pCameraXmlElement);
					pCameraXmlElement->SetAttribute("name", pCamera->getName().c_str());
					if (pCamera->getName() == m_pGameScreen->GetActiveCamera()->getName())
					{
						pCameraXmlElement->SetAttribute("active", "true");
					}
					else
					{
						pCameraXmlElement->SetAttribute("active", "false");
					}

					TiXmlElement* pPositionXmlElement = new TiXmlElement("position");
					pCameraXmlElement->LinkEndChild(pPositionXmlElement);
					WriteVector3(pPositionXmlElement, pCamera->getPosition());

					TiXmlElement* pQuaternionXmlElement = new TiXmlElement("quaternion");
					pCameraXmlElement->LinkEndChild(pQuaternionXmlElement);
					WriteQuaternion(pQuaternionXmlElement, pCamera->getOrientation());

					Ogre::String strProjectionType = "";
					if (pCamera->getProjectionType() == Ogre::PT_PERSPECTIVE)
					{
						strProjectionType = "perspective";
					}
					else
					{
						strProjectionType = "orthographic";
					}
					pCameraXmlElement->SetAttribute("projectionType", strProjectionType.c_str());
					pCameraXmlElement->SetAttribute("fov", Ogre::StringConverter::toString(pCamera->getFOVy()).c_str());
					pCameraXmlElement->SetAttribute("aspectRatio", Ogre::StringConverter::toString(pCamera->getAspectRatio()).c_str());

					TiXmlElement* pClippingXmlElement = new TiXmlElement("clipping");
					pCameraXmlElement->LinkEndChild(pClippingXmlElement);
					pClippingXmlElement->SetAttribute("nearPlaneDist", Ogre::StringConverter::toString(pCamera->getNearClipDistance()).c_str());
					pClippingXmlElement->SetAttribute("farPlaneDist", Ogre::StringConverter::toString(pCamera->getFarClipDistance()).c_str());

					// Auto track
				}
			}
            
			GGETRACELOG("Saving Entity");
			
			Ogre::SceneManager::MovableObjectIterator entityIterator = m_pSceneManager->getMovableObjectIterator("Entity");
			while (entityIterator.hasMoreElements())
			{
				

				Ogre::MovableObject* pMovableObject = entityIterator.getNext();
				if (pMovableObject->getParentSceneNode() == static_cast<Ogre::SceneNode*>(pNodeToSave))
				{
					TiXmlElement* pMovableObjectXmlElement = new TiXmlElement("entity");
					pXmlNodeElement->LinkEndChild(pMovableObjectXmlElement);
					pMovableObjectXmlElement->SetAttribute("name", pMovableObject->getName().c_str());
					GGETRACELOG(pMovableObject->getName().c_str());


					Ogre::Entity* pEntity = dynamic_cast<Ogre::Entity*>(pMovableObject);
				
					Ogre::Any myAny = pEntity->getUserAny();
					if (myAny.isEmpty() == false)
					{
						string colladaFile = Ogre::any_cast<string>(myAny);
						pMovableObjectXmlElement->SetAttribute("meshFile", colladaFile.c_str());
					}
					else
					{
						pMovableObjectXmlElement->SetAttribute("meshFile", pEntity->getMesh()->getName().c_str());
					}
					pMovableObjectXmlElement->SetAttribute("castShadows", pEntity->getCastShadows() == true ? "true" : "false");
					

					
					// Check if the entity has asociated physics, if it does, add the havokFile and havokName attributes
				//	list<GameObject*> gameObjectList = m_pGameScreen->GetGameObjectManager()->m_pGameObjectList;   //m_pGameOjbectList is private!! actually no need to write a loop
				
				//	for (list<GameObject*>::iterator i = gameObjectList.begin(); i != gameObjectList.end(); i++)
				//	{
					int numObj = m_pGameScreen->GetGameObjectManager()->GetNumOfGameObjects();
					char str[33];
					_itoa_s(numObj,str,10);
					
					GGETRACELOG("Number of Entities");
					GGETRACELOG(str);
					
					
						GameObject* temp;
						temp = m_pGameScreen->GetGameObjectManager()->GetGameObject(pMovableObject->getName());
						
						if(temp != NULL)
						{
							GGETRACELOG("Saving Physics");
							if(temp->m_sHKXFileName.compare("") != 0)
							{
								pMovableObjectXmlElement->SetAttribute("havokFile", temp->m_sHKXFileName.c_str());
							}
							else
							{
								switch(temp->m_eCollisionShapeType)
								{
								case DEFAULT_COLLISION_SHAPE:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","Default Collision Shape");
									break;
								case COLLISION_SHAPE_XYPLANE:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","XY Plane");
									break;
								case COLLISION_SHAPE_SPHERE:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","Sphere");
									break;
								case COLLISION_SHAPE_CAPSULE:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","Capsule");
									break;
								case COLLISION_SHAPE_CYLINDER:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","Cylinder");
									break;
								case COLLISION_SHAPE_BOX:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","Box");
									break;
								case COLLISION_SHAPE_CONVEX_HULL:
									pMovableObjectXmlElement->SetAttribute("CollisionShapeType","Convex Hull");
									break;
								default:
									GGETRACELOG("Unknown Collision Shape");
								}
							}

							switch(temp->m_eObjectType)
							{
							case  DEFAULT_GAME_OBJECT_TYPE:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Default Game Object");
									break;
							case GRAPHICS_OBJECT:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Graphics Object");
								break;
							case PHYSICS_FIXED:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Physics Fixed");
								break;
							case PHYSICS_DYNAMIC:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Physics Dynamic");
								break;
							case PHYSICS_KEYFRAMED:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Physics KeyFramed");
								break;
							case PHYSICS_SYSTEM:
							    pMovableObjectXmlElement->SetAttribute("ObjectType","Physics System");
								break;
							case PHYSICS_CHARACTER_PROXY:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Physics Character Proxy");
								break;
							case PHYSICS_CHARACTER_RIGIDBODY:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Physics Character RigidBody");
								break;
							case ANIMATED_CHARACTER_PROXY:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Animated Character Proxy");
								break;
							case ANIMATED_CHARACTER_RIGIDBODY:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Animated Character RigidBody");
								break;
							case PHANTOM_SHAPE:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Phantom Shape");
								break;
							case PHANTOM_AABB:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Phantom AABB");
								break;
							case VEHICLE_CAR:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Vehicle Car");
								break;
							case VEHICLE_MOTORCYCLE:
								pMovableObjectXmlElement->SetAttribute("ObjectType","Vehicle Motorcycle");
								break;
							}
						}										

					//	if (temp->m_pGraphicsObject->m_sOgreUniqueName == pMovableObject->getName())
					//	{
					//		pMovableObjectXmlElement->SetAttribute("havokFile", temp->m_sHKXFileName.c_str());
					//		break;
					//	}
				//	} 
					

					Ogre::String materialName = "";
					for (size_t s = 0; s < pEntity->getNumSubEntities(); ++s)
					{
						Ogre::SubEntity* pSubEntity = pEntity->getSubEntity(s);
						if (materialName == "")
						{
							materialName = pSubEntity->getMaterialName();
						}
						else
						{
							if (materialName != pSubEntity->getMaterialName())
							{
								materialName = "";
							}
						}
					}

					if (materialName != "")
					{
						pMovableObjectXmlElement->SetAttribute("material", materialName.c_str());
					}
					else
					{
						for (size_t s = 0; s < pEntity->getNumSubEntities(); ++s)
						{
							Ogre::SubEntity* pSubEntity = pEntity->getSubEntity(s);

							TiXmlElement* pSubEntityXmlElement = new TiXmlElement("subentity");
							pMovableObjectXmlElement->LinkEndChild(pSubEntityXmlElement);
							pSubEntityXmlElement->SetAttribute("index", Ogre::StringConverter::toString(s).c_str());
							pSubEntityXmlElement->SetAttribute("materialName", pSubEntity->getMaterialName().c_str());
						}
					}
				}
			}

			Ogre::SceneManager::MovableObjectIterator lightIterator = m_pSceneManager->getMovableObjectIterator("Light");
			while (lightIterator.hasMoreElements())
			{
				Ogre::MovableObject* pMovableObject = lightIterator.getNext();
				if (pMovableObject->getParentSceneNode() == static_cast<Ogre::SceneNode*>(pNodeToSave))
				{
					TiXmlElement* pMovableObjectXmlElement = new TiXmlElement("light");
					pXmlNodeElement->LinkEndChild(pMovableObjectXmlElement);
					pMovableObjectXmlElement->SetAttribute("name", pMovableObject->getName().c_str());
					GGETRACELOG(pMovableObject->getName().c_str());

					Ogre::Light* pLight = dynamic_cast<Ogre::Light*>(pMovableObject);
					Ogre::String strType = "";
					switch (pLight->getType())
					{
					case Ogre::Light::LT_POINT:
						strType = "point";
						break;
					case Ogre::Light::LT_DIRECTIONAL:
						strType = "directional";
						break;
					case Ogre::Light::LT_SPOTLIGHT:
						strType = "spot";
						break;
					}
					pMovableObjectXmlElement->SetAttribute("type", strType.c_str());
					pMovableObjectXmlElement->SetAttribute("visible", pLight->isVisible() ? "true" : "false");

					TiXmlElement* pPositionXmlElement = new TiXmlElement("position");
					pMovableObjectXmlElement->LinkEndChild(pPositionXmlElement);
					WriteVector3(pPositionXmlElement, pLight->getPosition());

					TiXmlElement* pNormalXmlElement = new TiXmlElement("normal");
					pMovableObjectXmlElement->LinkEndChild(pNormalXmlElement);
					WriteVector3(pNormalXmlElement, pLight->getDirection());

					TiXmlElement* pColourDiffuseXmlElement = new TiXmlElement("colourDiffuse");
					pMovableObjectXmlElement->LinkEndChild(pColourDiffuseXmlElement);
					WriteColor(pColourDiffuseXmlElement, pLight->getDiffuseColour());

					TiXmlElement* pColourSpecularXmlElement = new TiXmlElement("colourSpecular");
					pMovableObjectXmlElement->LinkEndChild(pColourSpecularXmlElement);
					WriteColor(pColourSpecularXmlElement, pLight->getSpecularColour());

					TiXmlElement* pLightAttenuationXmlElement = new TiXmlElement("lightAttenuation");
					pMovableObjectXmlElement->LinkEndChild(pLightAttenuationXmlElement);
					pLightAttenuationXmlElement->SetAttribute("range", Ogre::StringConverter::toString(pLight->getAttenuationRange()).c_str());
					pLightAttenuationXmlElement->SetAttribute("constant", Ogre::StringConverter::toString(pLight->getAttenuationConstant()).c_str());
					pLightAttenuationXmlElement->SetAttribute("linear", Ogre::StringConverter::toString(pLight->getAttenuationLinear()).c_str());
					pLightAttenuationXmlElement->SetAttribute("quadratic", Ogre::StringConverter::toString(pLight->getAttenuationQuadric()).c_str());

					if (pLight->getType() == Ogre::Light::LT_SPOTLIGHT)
					{
						TiXmlElement* pLightRangeXmlElement = new TiXmlElement("lightRange");
						pMovableObjectXmlElement->LinkEndChild(pLightRangeXmlElement);
						pLightRangeXmlElement->SetAttribute("inner", Ogre::StringConverter::toString(pLight->getSpotlightInnerAngle()).c_str());
						pLightRangeXmlElement->SetAttribute("outer", Ogre::StringConverter::toString(pLight->getSpotlightOuterAngle()).c_str());
						pLightRangeXmlElement->SetAttribute("falloff", Ogre::StringConverter::toString(pLight->getSpotlightFalloff()).c_str());
					}
				}
			}			

			Ogre::Node::ChildNodeIterator childNodeIterator = pNodeToSave->getChildIterator();
			while (childNodeIterator.hasMoreElements())
			{
				Ogre::Node* pChildNodeToSave = childNodeIterator.getNext();	
			//	string nodeName = pChildNodeToSave->getName();	//put this part in the loader so that we dont load the hard coded nodes from the scene file (gets loaded twice)
			//	int iFoundIndex = nodeName.find("_GLE");         //make more robust by making _GLE the ending of the string in case someone uses _GLE in the middle

			//	if(iFoundIndex != string::npos)			// == will fail cuz it will load the objects twice - 1st from the scene file, and then from the code
					ProcessNode(pXmlNodeElement, pChildNodeToSave);
			}
		}
	}

	void DotScenePlusSaver::ProcessScene(TiXmlElement* pSceneXmlElement, Ogre::Node* pNodeToSave)
	{
		ProcessNode(pSceneXmlElement, pNodeToSave);

		return;		
	}

	void DotScenePlusSaver::WriteVector3(TiXmlElement* pXmlElement, Ogre::Vector3 v3Vector)
	{
		pXmlElement->SetAttribute("x", Ogre::StringConverter::toString(v3Vector.x).c_str());
		pXmlElement->SetAttribute("y", Ogre::StringConverter::toString(v3Vector.y).c_str());
		pXmlElement->SetAttribute("z", Ogre::StringConverter::toString(v3Vector.z).c_str());
	}

	void DotScenePlusSaver::WriteQuaternion(TiXmlElement* pXmlElement, Ogre::Quaternion qQuaternion)
	{
		pXmlElement->SetAttribute("x", Ogre::StringConverter::toString(qQuaternion.x).c_str());
		pXmlElement->SetAttribute("y", Ogre::StringConverter::toString(qQuaternion.y).c_str());
		pXmlElement->SetAttribute("z", Ogre::StringConverter::toString(qQuaternion.z).c_str());
		pXmlElement->SetAttribute("w", Ogre::StringConverter::toString(qQuaternion.w).c_str());
	}

	void DotScenePlusSaver::WriteColor(TiXmlElement* pXmlElement, Ogre::ColourValue cvColor)
	{
		pXmlElement->SetAttribute("r", Ogre::StringConverter::toString(cvColor.r).c_str());
		pXmlElement->SetAttribute("g", Ogre::StringConverter::toString(cvColor.g).c_str());
		pXmlElement->SetAttribute("b", Ogre::StringConverter::toString(cvColor.b).c_str());
		pXmlElement->SetAttribute("a", Ogre::StringConverter::toString(cvColor.a).c_str());
	}

	void DotScenePlusSaver::WriteMatrix(TiXmlElement* pXmlElement, Ogre::Matrix4 m4Matrix)
	{
		pXmlElement->SetAttribute("v0", Ogre::StringConverter::toString(m4Matrix[0][0]).c_str());
		pXmlElement->SetAttribute("v1", Ogre::StringConverter::toString(m4Matrix[0][1]).c_str());
		pXmlElement->SetAttribute("v2", Ogre::StringConverter::toString(m4Matrix[0][2]).c_str());
		pXmlElement->SetAttribute("v12", Ogre::StringConverter::toString(m4Matrix[0][3]).c_str());
		pXmlElement->SetAttribute("v4", Ogre::StringConverter::toString(m4Matrix[1][0]).c_str());
		pXmlElement->SetAttribute("v5", Ogre::StringConverter::toString(m4Matrix[1][1]).c_str());
		pXmlElement->SetAttribute("v6", Ogre::StringConverter::toString(m4Matrix[1][2]).c_str());
		pXmlElement->SetAttribute("v13", Ogre::StringConverter::toString(m4Matrix[1][3]).c_str());
		pXmlElement->SetAttribute("v8", Ogre::StringConverter::toString(m4Matrix[2][0]).c_str());
		pXmlElement->SetAttribute("v9", Ogre::StringConverter::toString(m4Matrix[2][1]).c_str());
		pXmlElement->SetAttribute("v10", Ogre::StringConverter::toString(m4Matrix[2][2]).c_str());
		pXmlElement->SetAttribute("v14", Ogre::StringConverter::toString(m4Matrix[2][3]).c_str());
		pXmlElement->SetAttribute("v3", Ogre::StringConverter::toString(m4Matrix[3][0]).c_str());
		pXmlElement->SetAttribute("v7", Ogre::StringConverter::toString(m4Matrix[3][1]).c_str());
		pXmlElement->SetAttribute("v11", Ogre::StringConverter::toString(m4Matrix[3][2]).c_str());
		pXmlElement->SetAttribute("v15", Ogre::StringConverter::toString(m4Matrix[3][3]).c_str());
	}
	
/*	void DotScenePlusSaver::WriteCollisionEnumeration(TiXmlElement* pXmlElement, enum CollisionShapeType enCollisionShape)
	{
		pXmlElement->SetAttribute("CollisionShape",CollisionShapeTypeNames[enCollisionShape]);
	} 
*/

	string DotScenePlusSaver::GetAttribute(TiXmlElement* pXmlElement, string attributeName, string defaultValue)
	{
		if (pXmlElement->Attribute(attributeName.c_str()))
		{
			return pXmlElement->Attribute(attributeName.c_str());
		}
		else
		{
			return defaultValue;
		}
	}

	Ogre::Real DotScenePlusSaver::GetAttributeAsReal(TiXmlElement* pXmlElement, string attributeName, Ogre::Real defaultValue)
	{
		if (pXmlElement->Attribute(attributeName.c_str()))
		{
			return Ogre::StringConverter::parseReal(pXmlElement->Attribute(attributeName.c_str()));
		}
		else
		{
			return defaultValue;
		}
	}

	bool DotScenePlusSaver::GetAttributeAsBool(TiXmlElement* pXmlElement, string attributeName, bool defaultValue)
	{
		if (!pXmlElement->Attribute(attributeName.c_str()))
		{
			return defaultValue;
		}
		else if (Ogre::String(pXmlElement->Attribute(attributeName.c_str())) == "true" ||
			Ogre::String(pXmlElement->Attribute(attributeName.c_str())) == "True" ||
			Ogre::String(pXmlElement->Attribute(attributeName.c_str())) == "1")
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void DotScenePlusSaver::Save(string strSceneFileName, GameScreen* pGameScreen, Ogre::SceneManager *pSceneManager, Ogre::Node* pNodeToSave)
	{
		GGETRACELOG("Saver initializing");

		TiXmlDocument* pXmlDocument = new TiXmlDocument();

		TiXmlElement* pSceneXmlElement = new TiXmlElement("scene");
		pXmlDocument->LinkEndChild(pSceneXmlElement);

		TiXmlElement* pNodesXmlElement = new TiXmlElement("nodes");
		pSceneXmlElement->LinkEndChild(pNodesXmlElement);

		if (!pNodeToSave)
		{
			pNodeToSave = pSceneManager->getRootSceneNode();
		}
		m_pSceneManager = pSceneManager;
		m_pGameScreen = pGameScreen;
		ProcessScene(pNodesXmlElement, pNodeToSave);

		pXmlDocument->SaveFile(strSceneFileName.c_str());

		delete pXmlDocument;
		pXmlDocument = NULL;

		GGETRACELOG("Save complete");
	}
}