#include <GISKinect.h>
#include <KinectAudioStream.h>
#include <sphelper.h>


#define KINECT_RULE 1001

using namespace GIS;

bool Kinect::InitSpeechRecog(bool useDefaultAudioDevice)
{
	if(speechInit)
		return false;//Already Initialized

	HRESULT hr = E_FAIL;
	if (SUCCEEDED(hr = ::CoInitialize(NULL)))
	{
		hr = g_cpEngine.CoCreateInstance(CLSID_SpInprocRecognizer);
		if(FAILED(hr))
		{
			printf("Create Engine Error");
			return false;
		}
		if(useDefaultAudioDevice)//set intput to default audio input device
		{
			hr = SpGetDefaultTokenFromCategoryId(SPCAT_AUDIOIN, &cpObjectToken);
			if(FAILED(hr))
			{
				printf("Unable to get Default Token");
				return false;
			}
			hr = g_cpEngine->SetInput(cpObjectToken, TRUE);
			if(FAILED(hr))
			{
				printf("Unable to set Input for the Engine");
				return false;
			}
		}
		else//set intput to  Microphone Array
		{
			KinectAudioStream* audioStream=new KinectAudioStream();
			if(!audioStream->initAudioStream())
			{
				printf("Unable to set Input for the Engine");
				return false;
			}
			WAVEFORMATEX wfxOut = {WAVE_FORMAT_PCM, 1, 16000, 32000, 2, 16, 0};
			hr = cpInputStream.CoCreateInstance(CLSID_SpStream);
			cpInputStream->SetBaseStream(audioStream,SPDFID_WaveFormatEx,&wfxOut);
			hr = g_cpEngine->SetInput(cpInputStream, TRUE);
			if(FAILED(hr))
			{
				printf("Unable to set Input for the Engine");
				return false;
			}
		}	
			
		hr=g_cpEngine->CreateRecoContext(&g_cpRecoCtxt);
		if(FAILED(hr))
		{
			printf("Create Context Error");
			return false;
		}

		hr = g_cpRecoCtxt->SetInterest(SPFEI(SPEI_RECOGNITION) , SPFEI(SPEI_RECOGNITION) );
		if(FAILED(hr))
		{
			printf("Error in setting speech recog interest");
			return false;
		}
		hr = g_cpRecoCtxt->CreateGrammar(0,&g_cpGrammar);
		if(FAILED(hr))
		{		
			printf("Error in grammar creation");
			return false;
		}
		//hr = g_cpRecoCtxt->SetNotifyWin32Event();
		hr = g_cpRecoCtxt->SetNotifyCallbackFunction(speechRecognized,(WPARAM)this,NULL);
		if(FAILED(hr))
		{
			printf("Error in setting windows notification");
			return false;
		}
		//RecoEvent=g_cpRecoCtxt->GetNotifyEventHandle();
		//Defining the languague used..change this to change the only languagues recognized by SAPI to Japanese or simplified Chinese as required
		LANGID lang=MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US);
		hr = g_cpGrammar->ResetGrammar(lang);//English - United States.
		if(FAILED(hr))
		{
			printf("Error in resetting the grammar");
			return false;
		}
		//Initialize grammar
		g_cpGrammar->GetRule(NULL,KINECT_RULE,SPRAF_TopLevel | SPRAF_Active | SPRAF_Dynamic,TRUE, &hDynamicRuleHandle);
		g_cpGrammar->ClearRule(hDynamicRuleHandle);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Fire", L" ", SPWT_LEXICAL, 1, NULL);			
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Close", L" ", SPWT_LEXICAL, 1, NULL);		
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Red", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Blue", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Yellow", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"White", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Pink", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Green", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Orange", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Purple", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Left", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Right", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Up", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Down", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Load", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Back", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"One", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Two", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Three", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Four", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Five", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Six", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Seven", L" ", SPWT_LEXICAL, 1, NULL);
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Eight", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Nine", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Ten", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Next", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Previous", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Jump", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, L"Shoot", L" ", SPWT_LEXICAL, 1, NULL);	
		g_cpGrammar->Commit(0);
		g_cpGrammar->SetRuleIdState( KINECT_RULE, SPRS_ACTIVE ); 
		speechInit=true;//Speech part is successfully initialized
		return true;
	}
	return false;	
}

//std::wstring Kinect::RunSpeechRecog()
//bool Kinect::RunSpeechRecog()
//{
//	int RecoEventIndex=WaitForSingleObject(RecoEvent,0);
//	CSpEvent RecoE;
//		if(RecoEventIndex==0)
//		{
//			RecoE.GetFrom(g_cpRecoCtxt);
//			CComPtr<ISpRecoResult> cpResult;
//			cpResult=RecoE.RecoResult();
//			CSpDynamicString dstrText;
//			WCHAR * redS=L"Red";
//			cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, 
//                                                    TRUE, &dstrText, NULL);
//			BSTR* bs;
//			dstrText.CopyToBSTR(bs);
//			std::wstring ws(*bs);
//			kinectListener->speechRecognized(ws);
// 			//return ws;	
//		}
//}

//Dynamically add a speech command to grammar
void Kinect::AddGrammar(LPCWSTR str)
{
	if(!speechInit)
		return;//speech part is not initialized
	g_cpGrammar->SetRuleIdState( KINECT_RULE, SPRS_INACTIVE ); 
	g_cpGrammar->AddWordTransition(hDynamicRuleHandle, NULL, str, L" ", SPWT_LEXICAL, 1, NULL);	
	g_cpGrammar->Commit(0);
	g_cpGrammar->SetRuleIdState( KINECT_RULE, SPRS_ACTIVE );
}

//Handle the callback function. This function itself is in fact a callback function
void __stdcall Kinect::speechRecognized(WPARAM wParam, LPARAM lParam)
{
	//Speech Recognized!
	Kinect *pthis=(Kinect *) wParam;
	CSpEvent RecoE;
	RecoE.GetFrom(pthis->g_cpRecoCtxt);
	CComPtr<ISpRecoResult> cpResult;
	cpResult=RecoE.RecoResult();
	CSpDynamicString dstrText;
	SPPHRASE* pPhrase;
	cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, 
                                            TRUE, &dstrText, NULL);
	cpResult->GetPhrase(&pPhrase);
	float confidence=pPhrase->pElements->SREngineConfidence;
	std::wstring ws;
	ws=dstrText.Copy();
	std::string word(ws.begin(),ws.end());
	pthis->kinectListener->speechRecognized(word,confidence);
}
