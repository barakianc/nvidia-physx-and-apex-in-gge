#include "GISMoveMe.h"
#include <bitset>

namespace GIS{

	MoveMe::~MoveMe(){
		if(connected)
			moveClient->movemeDisconnect();
		delete moveClient;
		connected = false;

	}

	void MoveMe::moveMeConnect(PCSTR lpRemoteAddress, PCSTR lpPort){
		int retValue = moveClient->movemeConnect(lpRemoteAddress, lpPort);
		connected = true;
	}

	void MoveMe::moveMeDisconnect(){
		if(connected)
			moveClient->movemeDisconnect();
		connected = false;
	}

	bool MoveMe::isButtonDown(MoveMeButtonID button){ //TODO:  update argumentss
		return false;
	}

	const MoveState MoveMe::getMoveStateForController(int c){
		//moveClient->movemeGetPacket(serverData);

		return serverData.state[c];
	}

	const MoveServerPacket MoveMe::getMoveServerPacket(){
		return serverData;
	}

	const Vector4 MoveMe::getPositionForController(int c){
		moveClient->movemeGetPacket(serverData);
		
		Vector4 temp;
		temp.x = serverData.state[c].pos[0];
		temp.y = serverData.state[c].pos[1];
		temp.z = serverData.state[c].pos[2];
		temp.w = serverData.state[c].pos[3];

		return temp;
	}

	const Vector4 MoveMe::getQuaternionForController(int c){
		moveClient->movemeGetPacket(serverData);

		Vector4 temp;
		temp.x = serverData.state[c].quat[0];
		temp.y = serverData.state[c].quat[1];
		temp.z = serverData.state[c].quat[2];
		temp.w = serverData.state[c].quat[3];

		return temp;
	}

	const Vector4 MoveMe::getVelocityForController(int c){
		moveClient->movemeGetPacket(serverData);

		Vector4 temp;
		temp.x = serverData.state[c].vel[0];
		temp.y = serverData.state[c].vel[1];
		temp.z = serverData.state[c].vel[2];
		temp.w = serverData.state[c].vel[3];

		return temp;
	}

	const Vector4 MoveMe::getHandlePositionForController(int c){
		moveClient->movemeGetPacket(serverData);

		Vector4 temp;
		temp.x = serverData.state[c].handle_pos[0];
		temp.y = serverData.state[c].handle_pos[1];
		temp.z = serverData.state[c].handle_pos[2];
		temp.w = serverData.state[c].handle_pos[3];

		return temp;
	}

	const MovePadData MoveMe::getMovePadForController(int c){
		return serverData.state[c].pad;
	}

	const Vector4 MoveMe::getHandleVelocityForController(int c){
		moveClient->movemeGetPacket(serverData);

		Vector4 temp;
		temp.x = serverData.state[c].handle_vel[0];
		temp.y = serverData.state[c].handle_vel[1];
		temp.z = serverData.state[c].handle_vel[2];
		temp.w = serverData.state[c].handle_vel[3];

		return temp;
	}

	void MoveMe::setBuffered(bool buffered){
		mBuffered = buffered;
	}

	void MoveMe::_initialize(){
		moveClient = new MoveClient();
		connected = false;
	}

	void MoveMe::capture(){

		moveClient->movemeGetPacket(serverData);
		
		//compare lastCaptureServerData to current server data
		/*std::bitset<8>   bits(serverData.state[0].pad.digital_buttons);
		
		bool squareOn = bits.test(7);
		bool TriOn = bits.test(4);
		bool xOn = bits.test(6);
		bool oOn = bits.test(5);
		bool squigOn = bits.test(3);
		bool trigOn = bits.test(1);

		std::bitset<8> padBits(serverData.pad_data[0].button[2]);

		bool downOn = padBits.test(6);
		bool rightOn = padBits.test(5);
		bool leftOn = padBits.test(7);
		bool upOn = padBits.test(4);


		std::bitset<8> padButtonBits(serverData.pad_data[0].button[3]);

		bool padXOn = padButtonBits.test(6);
		bool padOOn = padButtonBits.test(5);
		bool L1On = padButtonBits.test(2);
		bool L2On = padButtonBits.test(0);*/
	
		//go one by one and test values

		bool currentButtonValue, oldButtonValue;
		
		//new bitset values
		std::bitset<8> moveButtons(serverData.state[0].pad.digital_buttons);
		std::bitset<8> navPadArrows(serverData.pad_data[0].button[2]);
		std::bitset<8> navPadButtons(serverData.pad_data[0].button[3]);

		//old bitset values
		std::bitset<8> oldMoveButtons(lastCaptureServerData.state[0].pad.digital_buttons);
		std::bitset<8> oldNavPadArrows(lastCaptureServerData.pad_data[0].button[2]);
		std::bitset<8> oldNavPadButtons(lastCaptureServerData.pad_data[0].button[3]);


		//1) hanlde pad

		//select button
		currentButtonValue = moveButtons.test(bSelectOffset);
		oldButtonValue = oldMoveButtons.test(bSelectOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_SEL); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_SEL);
			}
		}

		//trigger
		currentButtonValue = moveButtons.test(bTriggerOffset);
		oldButtonValue = oldMoveButtons.test(bTriggerOffset);
		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_TRIGGER); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_TRIGGER);
			}
		}

		//Squiggle
		currentButtonValue = moveButtons.test(bSquiggleOffest);
		oldButtonValue = oldMoveButtons.test(bSquiggleOffest);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_SQUIGGLE); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_SQUIGGLE);
			}
		}

		//start
		currentButtonValue = moveButtons.test(bStartOffset);
		oldButtonValue = oldMoveButtons.test(bStartOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_START); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_START);
			}
		}


		//triangle
		currentButtonValue = moveButtons.test(bTriangleOffset);
		oldButtonValue = oldMoveButtons.test(bTriangleOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_TRI); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_TRI);
			}
		}

		//circle
		currentButtonValue = moveButtons.test(bCircleOffset);
		oldButtonValue = oldMoveButtons.test(bCircleOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_O); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_O);
			}
		}

		//cross
		currentButtonValue = moveButtons.test(bCrossOffset);
		oldButtonValue = oldMoveButtons.test(bCrossOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_X); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_X);
			}
		}

		//square
		currentButtonValue = moveButtons.test(bSquareOffset);
		oldButtonValue = oldMoveButtons.test(bSquareOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_SQR); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_SQR);
			}
		}

		//2) handle navigation arrows

		//up arrow
		currentButtonValue = navPadArrows.test(bUpOffset);
		oldButtonValue = oldNavPadArrows.test(bUpOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_UP); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_UP);
			}
		}

		//right arrow
		currentButtonValue = navPadArrows.test(bRightOffset);
		oldButtonValue = oldNavPadArrows.test(bRightOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_RIGHT); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_RIGHT);
			}
		}

		//down arrow
		currentButtonValue = navPadArrows.test(bDownOffest);
		oldButtonValue = oldNavPadArrows.test(bDownOffest);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_DOWN); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_DOWN);
			}
		}

		//left arrow
		currentButtonValue = navPadArrows.test(bLeftOffset);
		oldButtonValue = oldNavPadArrows.test(bLeftOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_LEFT); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_LEFT);
			}
		}

		//3) handle navigation buttons

		//L2
		currentButtonValue = navPadButtons.test(bL2Offset);
		oldButtonValue = oldNavPadButtons.test(bL2Offset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_L2); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_L2);
			}
		}

		//L1
		currentButtonValue = navPadButtons.test(bL1Offset);
		oldButtonValue = oldNavPadButtons.test(bL1Offset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_L1); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_L1);
			}
		}

		//circle
		currentButtonValue = navPadButtons.test(bCircleOffset);
		oldButtonValue = oldNavPadButtons.test(bCircleOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_O); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_O);
			}
		}

		//cross
		currentButtonValue = navPadButtons.test(bCrossOffset);
		oldButtonValue = oldNavPadButtons.test(bCrossOffset);

		if( currentButtonValue ^ oldButtonValue){
			if(currentButtonValue){
				//broadcast button pressed
				mmListener->buttonPressed(MoveMeEvent(this, serverData), MM_NAV_X); 
			}
			else{
				//broadcast button released
				mmListener->buttonReleased(MoveMeEvent(this, serverData), MM_NAV_X);
			}
		}

		lastCaptureServerData = serverData;

	}
	
}