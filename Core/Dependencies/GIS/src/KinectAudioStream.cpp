#include "KinectAudioStream.h"

#define SAFE_ARRAYDELETE(p) {if (p) delete[] (p); (p) = NULL;}
#define SAFE_RELEASE(p) {if (NULL != p) {(p)->Release(); (p) = NULL;}}

KinectAudioStream::KinectAudioStream()
{
	_refcount = 1;
	_pRead=0;
	_pWrite=0;
	_pEnd=0;
	_isAhead=false;
	hr = ::CoInitialize(NULL);
	if(FAILED(hr))
	{
		printf_s("Cannot nitialize the COM in KinectAudioStream\n");
	}
}

KinectAudioStream::~KinectAudioStream()
{
	SAFE_ARRAYDELETE(_pBuffer);    
	SAFE_RELEASE(_pDMO);
    SAFE_RELEASE(_pPS);

    AvRevertMmThreadCharacteristics(_mmHandle);
	CoUninitialize();
}

HRESULT STDMETHODCALLTYPE KinectAudioStream::QueryInterface(REFIID iid, void ** ppvObject)
{ 
    if (iid == __uuidof(IStream))
    {
        *ppvObject = static_cast<IStream*>(this);
        AddRef();
        return S_OK;
    } else if (iid == __uuidof(IUnknown))
	{
		 *ppvObject = static_cast<IUnknown*>(this);
        AddRef();
        return S_OK;
	}
	else if (iid == __uuidof(ISequentialStream))
	{
		*ppvObject = static_cast<ISequentialStream*>(this);
        AddRef();
        return S_OK;
	}
	else
	{
        return E_NOINTERFACE; 
	}
}

ULONG STDMETHODCALLTYPE KinectAudioStream::AddRef(void) 
{ 
    return (ULONG)InterlockedIncrement(&_refcount); 
}

ULONG STDMETHODCALLTYPE KinectAudioStream::Release(void) 
{
    ULONG res = (ULONG) InterlockedDecrement(&_refcount);
    if (res == 0)
	{
        delete this;
	}
    return res;
}

HRESULT STDMETHODCALLTYPE KinectAudioStream::Read(void* pv, ULONG cb, ULONG* pcbRead)
{
	HRESULT hr;
	DWORD dwStatus;
	while(!_isDataEnough(cb))
	{
		//get data from Kinect sensor
		hr = _pDMO->ProcessOutput(0, 1, &_OutputBufferStruct, &dwStatus);
		if(FAILED(hr))
		{
			printf_s("Cannot process output data from KinectAudioStream\n");
			return HRESULT_FROM_WIN32(GetLastError());
		}
		hr = _outputBuffer->GetBufferAndLength(NULL, &_pWrite);
		if(FAILED(hr))
		{
			printf_s("Cannot get buffer data and length from KinectAudioBuffer\n");
			return HRESULT_FROM_WIN32(GetLastError());
		}
		if(_pWrite>_BufLen/2)//The buffer is "full" to ensure there always enough space to accept the data from DMO
		{
			_outputBuffer->Restart();//New data will then be stored at the beginning of buffer
			_isAhead=true;
			_pEnd=_pWrite;
			_pWrite=0;
		}
	}

	//Copy the data to the buffer of speech recognizer
	if(_isAhead)
	{
		if(_pRead+cb>_pEnd)
		{
			//NOTE: Data will be stored at the beginning of the buffer if the buffer is "full"
			memcpy(pv,&(_outputBuffer->m_pData[_pRead]),(_pEnd-_pRead));//First copy the valid bytes at the end of buffer
			memcpy(&(((BYTE*)pv)[_pEnd-_pRead]),&(_outputBuffer->m_pData),cb-(_pEnd-_pRead));//Copy remaining bytes from the beginning of buffer
			_isAhead=false;
			_pRead=cb-(_pEnd-_pRead);
		}
		else
		{
			memcpy(pv,&(_outputBuffer->m_pData[_pRead]),cb);
			_pRead+=cb;
		}
	}
	else
	{
		memcpy(pv,&(_outputBuffer->m_pData[_pRead]),cb);
		_pRead+=cb;
	}
	*pcbRead=cb;
	return S_OK; 
}

HRESULT STDMETHODCALLTYPE KinectAudioStream::Seek(LARGE_INTEGER liDistanceToMove, DWORD dwOrigin,ULARGE_INTEGER* lpNewFilePointer)
{ 
	switch(dwOrigin)
	{
	case STREAM_SEEK_SET:
		lpNewFilePointer->LowPart=0;//Set the start point of audio stream
		break;
	case STREAM_SEEK_CUR:
		break;
	case STREAM_SEEK_END:
		break;
	default:   
        return STG_E_INVALIDFUNCTION;
        break;
	}
    return S_OK;
}

bool KinectAudioStream::_isDataEnough(ULONG cb)
{
	if(!_isAhead)
	{
		if(_pRead+cb>_pWrite)
			return false;
		else
			return true;
	}
	else
	{
		if(_pRead+cb>_pWrite+_pEnd)
			return false;
		else
			return true;
	}
}

bool KinectAudioStream::initAudioStream()
{

	int  iMicDevIdx = -1;	//Index for input device.
	int	 iSpkDevIdx=-1;		//Index for output device. It is of no special use here because echo cancellation is not implemented

	DWORD mmTaskIndex = 0;
	_mmHandle = AvSetMmThreadCharacteristics("Audio", &mmTaskIndex);
	if(_mmHandle==NULL)
	{
		printf_s("Failed to set thread priority for KinecAudioStream\n");
		return false;
	}

	hr=_pDMO->QueryInterface(IID_IMediaObject, (void**)&_pDMO);
	if(FAILED(hr))
	{
		printf_s("Failed to create DMO instance for KinecAudioStream\n");
		return false;
	}

	hr=_pDMO->QueryInterface(IID_IPropertyStore, (void**)&_pPS);
	if(FAILED(hr))
	{
		printf_s("Failed to create PropertyStore instance for KinecAudioStream\n");
		return false;
	}

	PROPVARIANT pvSysMode;
    PropVariantInit(&pvSysMode);
    pvSysMode.vt = VT_I4;
	pvSysMode.lVal = (LONG)(2);
    hr=_pPS->SetValue(MFPKEY_WMAAECMA_SYSTEM_MODE, pvSysMode);
	if(FAILED(hr))
	{
		printf_s("Failed to set source mode for KinecAudioStream\n");
		return false;
	}
	PropVariantClear(&pvSysMode);

	hr = GetMicArrayDeviceIndex(&iMicDevIdx);
	if(FAILED(hr))
	{
		printf_s("Failed to find microphone array device for KinecAudioStream\n");
		return false;
	}

	//Set device Index
	PROPVARIANT pvDeviceId;
    PropVariantInit(&pvDeviceId);
    pvDeviceId.vt = VT_I4;
	//Speaker index is the two high order bytes and the mic index the two low order ones
    pvDeviceId.lVal = (unsigned long)(iSpkDevIdx<<16) | (unsigned long)(0x0000ffff & iMicDevIdx);
    hr=_pPS->SetValue(MFPKEY_WMAAECMA_DEVICE_INDEXES, pvDeviceId);
	if(FAILED(hr))
	{
		printf_s("Failed to set device index for KinecAudioStream\n");
		return false;
	}
    PropVariantClear(&pvDeviceId);

	//Set output formmat of DMO
	WAVEFORMATEX wfxOut = {WAVE_FORMAT_PCM, 1, 16000, 32000, 2, 16, 0};
	DMO_MEDIA_TYPE mt = {0};
	hr = MoInitMediaType(&mt, sizeof(WAVEFORMATEX));
	if(FAILED(hr))
	{
		printf_s("Failed to initialize media type for KinecAudioStream\n");
		return false;
	}
	mt.majortype = MEDIATYPE_Audio;
    mt.subtype = MEDIASUBTYPE_PCM;
    mt.lSampleSize = 0;
    mt.bFixedSizeSamples = TRUE;
    mt.bTemporalCompression = FALSE;
    mt.formattype = FORMAT_WaveFormatEx;	
    memcpy(mt.pbFormat, &wfxOut, sizeof(WAVEFORMATEX));
	hr = _pDMO->SetOutputType(0, &mt, 0); 
    if(FAILED(hr))
	{
		printf_s("Failed to set output type for KinecAudioStream\n");
		return false;
	}
    MoFreeMediaType(&mt);

	hr = _pDMO->AllocateStreamingResources();
    if(FAILED(hr))
	{
		printf_s("Failed to allocate stream resources for KinecAudioStream\n");
		return false;
	}

	int iFrameSize;
    PROPVARIANT pvFrameSize;
    PropVariantInit(&pvFrameSize);
    hr=_pPS->GetValue(MFPKEY_WMAAECMA_FEATR_FRAME_SIZE, &pvFrameSize);
	if(FAILED(hr))
	{
		printf_s("Failed to get frame size for KinecAudioStream\n");
		return false;
	}
    iFrameSize = pvFrameSize.lVal;
    PropVariantClear(&pvFrameSize);

	//set buffer size
	_BufLen = wfxOut.nSamplesPerSec * wfxOut.nBlockAlign*4;
	_pBuffer=new BYTE[_BufLen];
	if(_pBuffer==NULL)
	{
		printf_s("Failed to allocate buffer for KinecAudioStream\n");
		return false;
	}
	_outputBuffer=new KinectAudioBuffer();
	_outputBuffer->Init((byte*)_pBuffer, _BufLen, 0);
	_OutputBufferStruct.dwStatus = 0;
	_OutputBufferStruct.pBuffer=_outputBuffer;

	hr = _pDMO->QueryInterface(IID_INuiAudioBeam, (void**)&_pSC);
	if(FAILED(hr))
	{
		printf_s("Failed to get interface of ISoundSourceLocalizer for KinecAudioStream\n");
		return false;
	}


	return true;
}

// Gets the subtype of the jack that the specified endpoint device is plugged
// into.  E.g. if the endpoint is for an array mic, then we would expect the
// subtype of the jack to be KSNODETYPE_MICROPHONE_ARRAY
HRESULT KinectAudioStream::GetJackSubtypeForEndpoint(IMMDevice* pEndpoint, GUID* pgSubtype)
{
    IDeviceTopology*    spEndpointTopology = NULL;
    IConnector*         spPlug = NULL;
    IConnector*         spJack = NULL;
    IPart*            spJackAsPart = NULL;
    
    if (pEndpoint == NULL)
        return E_POINTER;
   
    // Get the Device Topology interface
    hr=pEndpoint->Activate(__uuidof(IDeviceTopology), CLSCTX_INPROC_SERVER, NULL, (void**)&spEndpointTopology);
	if(FAILED(hr))
	{
		goto exit;
	}

    hr=spEndpointTopology->GetConnector(0, &spPlug);
	if(FAILED(hr))
	{
		goto exit;
	}

    hr=spPlug->GetConnectedTo(&spJack);
	if(FAILED(hr))
	{
		goto exit;
	}

	hr=spJack->QueryInterface(__uuidof(IPart), (void**)&spJackAsPart);
	if(FAILED(hr))
	{
		goto exit;
	}

    hr = spJackAsPart->GetSubType(pgSubtype);

exit:
   KINECTAUDIOSTREAM_SAFE_RELEASE(spEndpointTopology);
   KINECTAUDIOSTREAM_SAFE_RELEASE(spPlug);    
   KINECTAUDIOSTREAM_SAFE_RELEASE(spJack);    
   KINECTAUDIOSTREAM_SAFE_RELEASE(spJackAsPart);
   return hr;
}//GetJackSubtypeForEndpoint()


// Obtain device index corresponding to microphone array device.
HRESULT KinectAudioStream::GetMicArrayDeviceIndex(int *piDevice)
{
    UINT index, dwCount;
    IMMDeviceEnumerator* spEnumerator;
    IMMDeviceCollection* spEndpoints;

    *piDevice = -1;

    hr=CoCreateInstance(__uuidof(MMDeviceEnumerator),  NULL, CLSCTX_ALL, __uuidof(IMMDeviceEnumerator), (void**)&spEnumerator);
	if(FAILED(hr))
	{
		goto exit;
	}

    hr=spEnumerator->EnumAudioEndpoints(eCapture, DEVICE_STATE_ACTIVE, &spEndpoints);
	if(FAILED(hr))
	{
		goto exit;
	}

    hr=spEndpoints->GetCount(&dwCount);
	if(FAILED(hr))
	{
		goto exit;
	}

    // Iterate over all capture devices until finding one that is a microphone array
    for (index = 0; index < dwCount; index++)
    {
        IMMDevice* spDevice;

        hr=spEndpoints->Item(index, &spDevice);
        
        GUID subType = {0};
        hr=GetJackSubtypeForEndpoint(spDevice, &subType);
        if (subType == KSNODETYPE_MICROPHONE_ARRAY)
        {
            *piDevice = index;
            break;
        }
    }

    hr = (*piDevice >=0) ? S_OK : E_FAIL;

exit:
	KINECTAUDIOSTREAM_SAFE_RELEASE(spEnumerator);
    KINECTAUDIOSTREAM_SAFE_RELEASE(spEndpoints);    
    return hr;
}