#include <windows.h>
#include <iostream>
#include <sstream>



#include "GIS.h"

using namespace GIS;

// Useful print functions!


void PrintXPadButtons(int buttons){
	int bID;
	bID = XP_UP;
	if ((buttons & bID) == bID) std::cout << " U";
	bID = XP_DOWN;
	if ((buttons & bID) == bID) std::cout << " D";
	bID = XP_LEFT;
	if ((buttons & bID) == bID) std::cout << " L";
	bID = XP_RIGHT;
	if ((buttons & bID) == bID) std::cout << " R";
	bID = XP_START;
	if ((buttons & bID) == bID) std::cout << " START";
	bID = XP_BACK;
	if ((buttons & bID) == bID) std::cout << " BACK";
	bID = XP_LTHUMB;
	if ((buttons & bID) == bID) std::cout << " LTH";
	bID = XP_RTHUMB;
	if ((buttons & bID) == bID) std::cout << " RTH";
	bID = XP_LSHOULDER;
	if ((buttons & bID) == bID) std::cout << " LSH";
	bID = XP_RSHOULDER;
	if ((buttons & bID) == bID) std::cout << " RSH";
	bID = XP_A;
	if ((buttons & bID) == bID) std::cout << " A";
	bID = XP_B;
	if ((buttons & bID) == bID) std::cout << " B";
	bID = XP_X;
	if ((buttons & bID) == bID) std::cout << " X";
	bID = XP_Y;
	if ((buttons & bID) == bID) std::cout << " Y";
	std::cout << std::endl;
}

void PrintButtonStatus(int buttons){
	std::cout << "Buttons Pressed:";
	int bID;
	bID = WR_A;
	if ((buttons & bID) == bID) std::cout << " A";
	bID = WR_B;
	if ((buttons & bID) == bID) std::cout << " B";
	bID = WR_ONE;
	if ((buttons & bID) == bID) std::cout << " ONE";
	bID = WR_TWO;
	if ((buttons & bID) == bID) std::cout << " TWO";
	bID = WR_PLUS;
	if ((buttons & bID) == bID) std::cout << " +";
	bID = WR_MINUS;
	if ((buttons & bID) == bID) std::cout << " -";
	bID = WR_HOME;
	if ((buttons & bID) == bID) std::cout << " HOME";
	bID = WR_LEFT;
	if ((buttons & bID) == bID) std::cout << " L";
	bID = WR_RIGHT;
	if ((buttons & bID) == bID) std::cout << " R";
	bID = WR_UP;
	if ((buttons & bID) == bID) std::cout << " U";
	bID = WR_DOWN;
	if ((buttons & bID) == bID) std::cout << " D";
	bID = NC_C;
	if ((buttons & bID) == bID) std::cout << " C";
	bID = NC_Z;
	if ((buttons & bID) == bID) std::cout << " Z";

	std::cout << std::endl;

}

void PrintAccelerometerStatus(Vector3 vec){
	std::cout << "Accelerometer data: ";
	std::cout << "X=" << vec.x << ",";
	std::cout << "Y=" << vec.y << ",";
	std::cout << "Z=" << vec.z << std::endl;
}

void PrintIRStatus(Vector2 lc, Vector2 rc){
	std::cout << "LeftIR: (" << lc.x << "," << lc.y << ")" << std::endl;
	std::cout << "RightIR: (" << rc.x << "," << rc.y << ")" << std::endl;
}

void PrintAnalogStatus( Vector2 as ){
	std::cout << "Analog Stick: ( " << as.x << ", " << as.y << " )" << std::endl;
}

void PrintAxis(Axis ax){
	std::cout << "Axis (abs, rel): (" << ax.abs << ", " << ax.rel << ")" << std::endl;
}

/* Listener test 
   TestListener is supposed to represent some of the code that would
   be placed within GamePipe::Input.
  
*/
class TestListener : public WiiRemoteListener, public XPadListener, public KeyListener, public MouseListener{
public:
	// KeyListener virtual functions
	bool keyPressed( const KeyEvent &arg );
	bool keyReleased( const KeyEvent &arg );
	// MouseListener virtual functions
	bool mouseMoved( const MouseEvent &arg );
	bool mousePressed( const MouseEvent &arg, MouseButtonID id );
	bool mouseReleased( const MouseEvent &arg, MouseButtonID id );
	// WiiRemoteListener virtual functions
	bool buttonPressed( const WiiRemoteEvent &wrEvent, WiiButtonID button );
	bool buttonReleased( const WiiRemoteEvent &wrEvent, WiiButtonID button );
	bool analogStickMoved( const WiiRemoteEvent &wrEvent );
	bool remoteAccelerometersMoved( const WiiRemoteEvent &wrEvent);
	bool nunchuckAccelerometersMoved( const WiiRemoteEvent &wrEvent);
	bool irMoved( const WiiRemoteEvent &wrEvent);
	// XPadListener virtual functions
	virtual bool buttonPressed( const XPadEvent &xpEvent, int button);
	virtual bool buttonReleased( const XPadEvent &xpEvent, int button);
	virtual bool lTriggerPressed( const XPadEvent &xpEvent );
	virtual bool rTriggerPressed( const XPadEvent &xpEvent );
	virtual bool lThumbStickMoved( const XPadEvent &xpEvent );
	virtual bool rThumbStickMoved( const XPadEvent &xpEvent );
	
	// New functions!
	TestListener();
	int InitializeManager();
	int InitializeWiiRemote();
	int InitializeMouse();
	int InitializeKeyboard();
	int InitializeXPad();
	void Update();
	void SetEventCallbacks();
	void capture();
private:
	InputManager* lIM;
	
	Keyboard* lKeyboard;
	Mouse* lMouse;
	WiiRemote* lRemote;
	XPad* lXPad;

	WiiRemoteState lState;
};

TestListener::TestListener(){
	
}

int TestListener::InitializeManager(){
	ParamList paramList;
 
	
	char title[70];
	if (GetConsoleTitle(title, 70)){
		std::cout << "Console Title: " << title << std::endl;
	}
	else{
		std::cout << "ERROR" << std::endl;
	}

	std::ostringstream windowHandleString;
	windowHandleString << reinterpret_cast<size_t>(FindWindow(NULL, title));
	paramList.insert(std::make_pair(std::string("WINDOW"), windowHandleString.str()));
	paramList.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
	paramList.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
	paramList.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
	paramList.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
	lIM = InputManager::createInputSystem(paramList);
	return 0;
}

int TestListener::InitializeKeyboard(){
	try
		{
			lKeyboard = static_cast<GIS::Keyboard*>(lIM->createInputObject(GISKeyboard, true));
		}
		catch (...)
		{
			std::cout << "ERROR!" <<std::endl;
			return 1;
		}
		return 0;
}

int TestListener::InitializeMouse(){
	try
		{
			lMouse = static_cast<GIS::Mouse*>(lIM->createInputObject(GISMouse, true));
		}
		catch (...)
		{
			std::cout << "ERROR!" <<std::endl;
			return 1;
		}
		return 0;
}

int TestListener::InitializeWiiRemote(){
	try
		{
			lRemote = static_cast<GIS::WiiRemote*>(lIM->createInputObject(GISWiiRemote, true));
		}
		catch (...)
		{
			std::cout << "ERROR!" <<std::endl;
			return 1;
		}

	if (lRemote->Connect()){
		std::cout << "Remote Connected!" << std::endl;
	}
	lState.clear();
	lRemote->setNunchuckRequire(true);
	return 0;
}

int TestListener::InitializeXPad(){
	try
		{
			lXPad = static_cast<GIS::XPad*>(lIM->createInputObject(GISXPad, true));
		}
		catch (...)
		{
			std::cout << "ERROR!" <<std::endl;
			return 1;
		}
	return 0;
}

void TestListener::Update(){
	// Here we'd call Input::Capture(), which then
	// calls Capture() for each individual instantiated input device. 
	// Right now it does nothing, since the Wii Remote uses a thread 
	// to update asynchronously.
	lKeyboard->capture();
	lMouse->capture();
	lXPad->capture();
}

void TestListener::SetEventCallbacks(){
	// Here we call setEventCallback on each device instance.
	//if (lRemote != NULL){
	
		lRemote->setEventCallback(this);
		lXPad->setEventCallback(this);
		lKeyboard->setEventCallback(this);
		lMouse->setEventCallback(this);

	//}
}

// KEYBOARD
bool TestListener::keyPressed( const KeyEvent &arg ){
	std::cout << "keyPressed: " << arg.key << std::endl;
	return true;
}
bool TestListener::keyReleased( const KeyEvent &arg ){
	std::cout << "keyReleased: " << arg.key << std::endl;
	return true;
}

// MOUSE
bool TestListener::mouseMoved( const MouseEvent &arg ){
	std::cout << "X ";
	PrintAxis(arg.state.X);
	std::cout << "Y ";
	PrintAxis(arg.state.Y);
	std::cout << "Z ";
	PrintAxis(arg.state.Z);
	return true;
}
bool TestListener::mousePressed( const MouseEvent &arg, MouseButtonID id ){
	std::cout << "mousePressed: " << id << std::endl;
	return true;
}
bool TestListener::mouseReleased( const MouseEvent &arg, MouseButtonID id ){
	std::cout << "mouseReleased: " << id << std::endl;
	return true;
}

bool TestListener::buttonPressed(const WiiRemoteEvent &wrEvent, WiiButtonID button){
	// Update internal state
	lState = wrEvent.state; 
	PrintButtonStatus( lState.buttons );
	if (lState.wrButtonsDown(WR_ONE | WR_TWO)){ std::cout << "Disconnected." << std::endl; lRemote->~WiiRemote();  }
	return true;
}

// Wii Remote
bool TestListener::buttonReleased(const WiiRemoteEvent &wrEvent, WiiButtonID button){
	// Update internal state
	lState = wrEvent.state;
	PrintButtonStatus( lState.buttons );
	return true;
}

bool TestListener::analogStickMoved( const WiiRemoteEvent &wrEvent ){ 
	lState = wrEvent.state; 
	if (lState.wrButtonsDown(WR_MINUS)){
		PrintAnalogStatus(lState.stick);
	}
	return true; 
}

bool TestListener::remoteAccelerometersMoved( const WiiRemoteEvent &wrEvent) { 
	lState = wrEvent.state;
	if (lState.wrButtonsDown(WR_B)){
		PrintAccelerometerStatus( lState.remote );
	}
	return true; 
}
bool TestListener::nunchuckAccelerometersMoved( const WiiRemoteEvent &wrEvent) { 
	lState = wrEvent.state;
	if (lState.wrButtonsDown(WR_PLUS)){
		PrintAccelerometerStatus( lState.nunchuck );
	}
	return true;	
}

bool TestListener::irMoved( const WiiRemoteEvent &wrEvent) { 
	lState = wrEvent.state;
	if (lState.wrButtonsDown(WR_A)){
		PrintIRStatus( lState.lCam, lState.rCam );
	}
	return true;
}

// XPadListener Virtual Functions
bool TestListener::buttonPressed( const XPadEvent &xpEvent, int button){
	std::cout << "xPad buttonPressed: ";
	PrintXPadButtons(xpEvent.state.buttons);
	return true;
}

bool TestListener::buttonReleased( const XPadEvent &xpEvent, int button){
	std::cout << "xPad buttonReleased: ";
	PrintXPadButtons(xpEvent.state.buttons);
	return true;
}

bool TestListener::lTriggerPressed( const XPadEvent &xpEvent ){
	std::cout << "Left Trigger: " << xpEvent.state.XP_lTrigger << std::endl;
	return true;
}

bool TestListener::rTriggerPressed( const XPadEvent &xpEvent ){
	std::cout << "Right Trigger: " << xpEvent.state.XP_rTrigger << std::endl;
	return true;
}
bool TestListener::lThumbStickMoved( const XPadEvent &xpEvent ){
	std::cout << "Left Stick: (" << xpEvent.state.XP_lThumbX << "," << xpEvent.state.XP_lThumbY << ")" << std::endl; 
	return true;
}
bool TestListener::rThumbStickMoved( const XPadEvent &xpEvent ){
	std::cout << "Right Stick: (" << xpEvent.state.XP_rThumbX << "," << xpEvent.state.XP_rThumbY << ")" << std::endl; 
	return true;
}


/*
	Main represents the Engine in which the Input instance is called.
*/
void main()
{
	TestListener tl;
	tl.InitializeManager();
	tl.InitializeKeyboard();
	tl.InitializeMouse();
	tl.InitializeWiiRemote();
	tl.InitializeXPad();
	tl.SetEventCallbacks();
	
	while(true){
		tl.Update();
	}

}

	/* WiiRemote Test
	WiiRemote myRemote;
	myRemote.Connect();

	bool keepGoing = true;
	while ( ! myRemote.mState.wrButtonsDown(WR_ONE)){}
	*/

	/* Thread Test 1/2
	HANDLE thArray[2];
	HANDLE h1 = CreateThread( NULL, 0, &fun1, NULL, 0, NULL);  
	if (h1 == NULL){ std::cout << "F1 failed" << std::endl; }
	HANDLE h2 = CreateThread( NULL, 0, &fun2, NULL, 0, NULL);  
	if (h2 == NULL){ std::cout << "F2 failed" << std::endl; }
	thArray[0] = h1;
	thArray[1] = h2;

	WaitForMultipleObjects( 2, 
        thArray, TRUE, INFINITE);
	CloseHandle(h1);
	CloseHandle(h2);
	std::cout << "THREADS CLOSED" << std::endl;
	for (int i = 0; i < 10000000000; i++){}
	*/

/* Thread Test 2/2
DWORD WINAPI fun1(LPVOID nada){
	for (int i = 0; i < 10000; i++){}
	std::cout << "Hello world1!" << std::endl;
	return 0;
}

DWORD WINAPI fun2(LPVOID nada){
	for (int i = 0; i < 10000; i++){}
	std::cout << "Hello world2!" << std::endl;
	return 0;
}
*/