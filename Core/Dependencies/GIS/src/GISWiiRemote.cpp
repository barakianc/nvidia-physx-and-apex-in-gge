#include "GISWiiRemote.h"
#include <iostream>

using namespace GIS;

WiiRemote::~WiiRemote(){
	// Close thread handle if active.
	if (isConnected == true){
		isConnected = false;
		WaitForSingleObject(mThread, INFINITE);
		CloseHandle(mThread);
		mRemote.StopDataStream();
		mRemote.Disconnect();
	}
}

void WiiRemote::setBuffered(bool buffered = true){
	mBuffered = buffered;
}

void WiiRemote::_initialize(){
	Connect();
}

bool WiiRemote::Connect(){
	int attempt = 0;
	while(!isConnected && attempt < 20) {
		if (mRemote.ConnectToDevice() &&
			mRemote.StartDataStream()){
			isConnected = true;
			mThread = CreateThread(NULL, 0, Update, this, 0, NULL);
		} else {
			isConnected = false;
		}
		attempt++;
	}

	mState.clear(); // This may be unnecessary

	//cWiiMote::tExpansionReport report = mRemote.GetLastExpansionReport();

	//mState.nunchuckAttached = report.mAttachmentPluggedIn;

	//mRemote.SetLEDs(true, false, false, true);
	return isConnected;
}

// -1: Button raised; 1: Button Pressed, 0: no change
inline int buttonCompare(WiiButtonID bID, int oldB, int newB){
	return ((bID & oldB) == (bID & newB))? 0 : (((bID & oldB) < (bID & newB))? 1 : -1 );
}



// This function needs some cleanin'
void WiiRemote::capture(){
	/*if ( ! mRemote.GetLastChuckReport(). && reqNunchuck){
		// Nunchuck is not attached, do something.
		return;
	}
    */
	// Buttons first
	WiiButtonID bID;

	bID = WR_A;
	if (mRemote.GetLastButtonStatus().mA){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_B;
	if (mRemote.GetLastButtonStatus().mB){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_ONE;
	if (mRemote.GetLastButtonStatus().m1){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_TWO;
	if (mRemote.GetLastButtonStatus().m2){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_PLUS;
	if (mRemote.GetLastButtonStatus().mPlus){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_MINUS;
	if (mRemote.GetLastButtonStatus().mMinus){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_HOME;
	if (mRemote.GetLastButtonStatus().mHome){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_UP;
	if (mRemote.GetLastButtonStatus().mUp){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_DOWN;
	if (mRemote.GetLastButtonStatus().mDown){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_LEFT;
	if (mRemote.GetLastButtonStatus().mLeft){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = WR_RIGHT;
	if (mRemote.GetLastButtonStatus().mRight){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = NC_C;
	if (mRemote.GetLastChuckReport().mButtonC){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	bID = NC_Z;
	if (mRemote.GetLastChuckReport().mButtonZ){
		if (mState.wrButtonsUp(bID)) { 
			mState.buttons |= bID;
			mListener->buttonPressed( WiiRemoteEvent(this, mState), bID );
			return;
		}
	}
	else if (mState.wrButtonsDown(bID)){
		mState.buttons &= (~ bID);
		mListener->buttonReleased( WiiRemoteEvent(this, mState), bID );
		return;
	}

	float rmX,rmY,rmZ;
	// Remote Accelerometer
	if (useRemoteAccel){
		
		rmX = mState.remote.x;
		rmY = mState.remote.y;
		rmZ = mState.remote.z;
		mRemote.GetCalibratedAcceleration(mState.remote.x,mState.remote.y,mState.remote.z);
	}

	// IR
	float lX, lY, rX, rY;
	if (useIR){
		lX = mState.lCam.x;
		lY = mState.lCam.y;
		rX = mState.rCam.x;
		rY = mState.rCam.y;

		mRemote.GetIRP1(mState.lCam.x, mState.lCam.y);
		mRemote.GetIRP2(mState.rCam.x, mState.rCam.y);
	}
	// Nunchuck stuff
	float ncX,ncY,ncZ;
	if (reqNunchuck && useNunchuckAccel){
		
		ncX = mState.nunchuck.x;
		ncY = mState.nunchuck.y;
		ncZ = mState.nunchuck.z;
		mRemote.GetCalibratedChuckAcceleration(mState.nunchuck.x,mState.nunchuck.y,mState.nunchuck.z);
	}

	float sX, sY;
	if(reqNunchuck && useAnalogStick){
		
		sX = mState.stick.x;
		sY = mState.stick.y;
		mRemote.GetCalibratedChuckStick( mState.stick.x, mState.stick.y );
	}

	if ( useRemoteAccel ){
		if (rmX != mState.remote.x || rmY != mState.remote.y || rmZ != mState.remote.z){
			mListener->remoteAccelerometersMoved( WiiRemoteEvent(this, mState) );
		}
	}

	if (useIR){
		if (lX != mState.lCam.x || lY != mState.lCam.y ||
			rX != mState.rCam.x || rY != mState.rCam.y ){
				mListener->irMoved( WiiRemoteEvent(this, mState) );
		}
	}

	if ( reqNunchuck && useAnalogStick ){
		if ( sX != mState.stick.x || sY != mState.stick.y ){
			mListener->analogStickMoved( WiiRemoteEvent(this, mState) );
		}
	}

	if ( reqNunchuck && useNunchuckAccel ){
		if ( ncX != mState.nunchuck.x || ncY != mState.nunchuck.y || ncZ != mState.nunchuck.z ){
			mListener->nunchuckAccelerometersMoved( WiiRemoteEvent(this, mState) );
		}
	}
}

DWORD WINAPI GIS::Update(LPVOID self){
	WiiRemote* ptr = (WiiRemote*) self;
	while (ptr->isConnected){
		ptr->mRemote.HeartBeat();
	}
	return 0;
}

/* This may not be needed... : void WiiRemote::_read(){

}*/

/*
OLD WiiRemote::Capture function
// This function needs some cleanin'
void WiiRemote::capture(){
	int currButtons = 0;
	bool wasButtonPressed = false;
	bool wasButtonReleased = false;
	int cmp = 0;
	if (mRemote.GetLastButtonStatus().mA){
			currButtons |= WR_A;
			cmp = buttonCompare(WR_A, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	else if (mState.wrButtonsDown(WR_A)){
		wasButtonReleased = true;
	}
	if (mRemote.GetLastButtonStatus().mB){
			currButtons |= WR_B;
			cmp = buttonCompare(WR_B, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().m1){
			currButtons |= WR_ONE;
			cmp = buttonCompare(WR_ONE, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().m2){
			currButtons |= WR_TWO;
			cmp = buttonCompare(WR_TWO, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mPlus){
			currButtons |= WR_PLUS;
			cmp = buttonCompare(WR_PLUS, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mMinus){
			currButtons |= WR_MINUS;
			cmp = buttonCompare(WR_MINUS, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mHome){
			currButtons |= WR_HOME;
			cmp = buttonCompare(WR_HOME, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mUp){
			currButtons |= WR_UP;
			cmp = buttonCompare(WR_UP, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mDown){
			currButtons |= WR_DOWN;
			cmp = buttonCompare(WR_DOWN, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mLeft){
			currButtons |= WR_LEFT;
			cmp = buttonCompare(WR_LEFT, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastButtonStatus().mRight){
			currButtons |= WR_RIGHT;
			cmp = buttonCompare(WR_RIGHT, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastChuckReport().mButtonC){
			currButtons |= NC_C;
			cmp = buttonCompare(NC_C, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}
	if (mRemote.GetLastChuckReport().mButtonZ){
			currButtons |= NC_Z;
			cmp = buttonCompare(NC_Z, mState.buttons, currButtons);
			if (cmp == 1){ wasButtonPressed = true; }
			else if (cmp == -1){ wasButtonReleased = true; }
	}

	if (mState.buttons != currButtons){
		mState.buttons = currButtons;
		if (mListener){
			if (wasButtonPressed){
				mListener->buttonPressed( WiiRemoteEvent(this, mState) );
			}
			if (wasButtonReleased){
				mListener->buttonReleased( WiiRemoteEvent(this, mState) );
			}
		}
	}

	// Accelerometer stuff
	mRemote.GetCalibratedAcceleration(mState.remote.x,mState.remote.y,mState.remote.z);
	//PrintRemoteAccelerometerStatus(mState.remote);

	// IR Stuff
	mRemote.GetIRP1(mState.lCam.x, mState.lCam.y);
	mRemote.GetIRP2(mState.rCam.x, mState.rCam.y);
	//PrintIRStatus(mState.lCam, mState.rCam);

	// Nunchuck stuff
	//if (mRemote.GetLastExpansionReport().mAttachmentPluggedIn){
	mState.stick.x = mRemote.GetLastChuckReport().mStickX;
	mState.stick.y = mRemote.GetLastChuckReport().mStickY;
	mState.nunchuck.x = mRemote.GetLastChuckReport().mAccelX;
	mState.nunchuck.y = mRemote.GetLastChuckReport().mAccelY;
	mState.nunchuck.z = mRemote.GetLastChuckReport().mAccelZ;

	//std::cout << mState.stick.x << " " << mState.stick.y << std::endl;
	PrintButtonStatus(currButtons);
	//PrintRemoteAccelerometerStatus(mState.nunchuck);
	//}

}
*/

/*
void PrintButtonStatus(int buttons){
	std::cout << "Buttons Pressed:";
	int bID;
	bID = WR_A;
	if ((buttons & bID) == bID) std::cout << " A";
	bID = WR_B;
	if ((buttons & bID) == bID) std::cout << " B";
	bID = WR_ONE;
	if ((buttons & bID) == bID) std::cout << " ONE";
	bID = WR_TWO;
	if ((buttons & bID) == bID) std::cout << " TWO";
	bID = WR_PLUS;
	if ((buttons & bID) == bID) std::cout << " +";
	bID = WR_MINUS;
	if ((buttons & bID) == bID) std::cout << " -";
	bID = WR_HOME;
	if ((buttons & bID) == bID) std::cout << " HOME";
	bID = WR_LEFT;
	if ((buttons & bID) == bID) std::cout << " L";
	bID = WR_RIGHT;
	if ((buttons & bID) == bID) std::cout << " R";
	bID = WR_UP;
	if ((buttons & bID) == bID) std::cout << " U";
	bID = WR_DOWN;
	if ((buttons & bID) == bID) std::cout << " D";
	bID = NC_C;
	if ((buttons & bID) == bID) std::cout << " C";
	bID = NC_Z;
	if ((buttons & bID) == bID) std::cout << " Z";

	std::cout << std::endl;

}

void PrintAccelerometerStatus(Vector3 vec){
	std::cout << "Accelerometer data: ";
	std::cout << "X=" << vec.x << ",";
	std::cout << "Y=" << vec.y << ",";
	std::cout << "Z=" << vec.z << std::endl;
}

void PrintIRStatus(Vector2 lc, Vector2 rc){
	std::cout << "LeftIR: (" << lc.x << "," << lc.y << ")" << std::endl;
	std::cout << "RightIR: (" << rc.x << "," << rc.y << ")" << std::endl;
}

void PrintAnalogStatus( Vector2 as ){
	std::cout << "Analog Stick: ( " << as.x << ", " << as.y << " )" << std::endl;
}
*/