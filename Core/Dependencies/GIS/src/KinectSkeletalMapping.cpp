#include "GISKinect.h"

using namespace GIS;


//Structure define the hierarchical relation between intermediate joints
typedef struct _IntermediateJointDescription
	{
		Ogre::Vector3 bonedir;
		NUI_SKELETON_POSITION_INDEX  startNuiJoint;          
        NUI_SKELETON_POSITION_INDEX  endNuiJoint; 
		INTERMEDIATE_JOINT_INDEX parentIdx;


	}IntermediateJointDescription;


//The intial position of intermedia joints. Actually they form a T-Pose skeleton
const Ogre::Vector3 RootDir=            Ogre::Vector3(1.0,   0.0f,  0.0f);
const Ogre::Vector3 HipCenterDir =      Ogre::Vector3(0.0f,   1.0f,  0.0f);
const Ogre::Vector3 SpineDir =          Ogre::Vector3(0.0f,  .985f,-.174f);
const Ogre::Vector3 CollarLeftDir =     Ogre::Vector3(-1.0f,   0.0f,  0.0f);
const Ogre::Vector3 ShoulderLeftDir =   Ogre::Vector3(-1.0f,   0.0f,  0.0f);
const Ogre::Vector3 ElbowLeftDir =      Ogre::Vector3(-1.0f,   0.0f,  0.0f);
const Ogre::Vector3 WristLeftDir =      Ogre::Vector3(-1.0f,   0.0f,  0.0f);
const Ogre::Vector3 ShoulderRightDir =  Ogre::Vector3( 1.0f,   0.0f,  0.0f);
const Ogre::Vector3 ElbowRightDir =     Ogre::Vector3(1.0f,   0.0f,  0.0f);
const Ogre::Vector3 WristRightDir =     Ogre::Vector3(1.0f,   0.0f,  0.0f);
const Ogre::Vector3 HipLeftDir =        Ogre::Vector3(0.0f,  -1.0f,  0.0f);
const Ogre::Vector3 KneeLeftDir =       Ogre::Vector3(0.0f,  -1.0f,  0.0f);
const Ogre::Vector3 HipRightDir =       Ogre::Vector3( 0.0f,  -1.0f,  0.0f);
const Ogre::Vector3 KneeRightDir =      Ogre::Vector3(0.0f,  -1.0f,  0.0f);


Int_Joint m_IntJoints[INTERMEDIATE_JOINT_COUNT];

//Define the hierarchical relation between intermediate joints
const IntermediateJointDescription g_IntJointDesc[ INTERMEDIATE_JOINT_COUNT ] =
{
	{RootDir,			(NUI_SKELETON_POSITION_INDEX)(-1),        NUI_SKELETON_POSITION_HIP_CENTER,		   (INTERMEDIATE_JOINT_INDEX)(-1),},
	{ShoulderLeftDir,	NUI_SKELETON_POSITION_SHOULDER_LEFT,      NUI_SKELETON_POSITION_ELBOW_LEFT,        (INTERMEDIATE_JOINT_INDEX)(-1),},
	{ElbowLeftDir,		NUI_SKELETON_POSITION_ELBOW_LEFT,         NUI_SKELETON_POSITION_WRIST_LEFT,        INT_SHOULDER_LEFT,},
	{WristLeftDir,		NUI_SKELETON_POSITION_WRIST_LEFT,         NUI_SKELETON_POSITION_HAND_LEFT,         INT_ELBOW_LEFT,},
	{ShoulderRightDir,	NUI_SKELETON_POSITION_SHOULDER_RIGHT,     NUI_SKELETON_POSITION_ELBOW_RIGHT,       (INTERMEDIATE_JOINT_INDEX)(-1),},
	{ElbowRightDir,		NUI_SKELETON_POSITION_ELBOW_RIGHT,        NUI_SKELETON_POSITION_WRIST_RIGHT,       INT_SHOULDER_RIGHT,},
	{WristRightDir,		NUI_SKELETON_POSITION_WRIST_RIGHT,        NUI_SKELETON_POSITION_HAND_RIGHT,        INT_ELBOW_RIGHT,},
    {HipLeftDir,        NUI_SKELETON_POSITION_HIP_LEFT ,          NUI_SKELETON_POSITION_KNEE_LEFT,         (INTERMEDIATE_JOINT_INDEX)(-1),},
	{KneeLeftDir ,	    NUI_SKELETON_POSITION_KNEE_LEFT ,         NUI_SKELETON_POSITION_ANKLE_LEFT,        INT_HIP_LEFT,},
    {HipRightDir ,      NUI_SKELETON_POSITION_HIP_RIGHT,          NUI_SKELETON_POSITION_KNEE_RIGHT,        (INTERMEDIATE_JOINT_INDEX)(-1),},
	{KneeRightDir ,     NUI_SKELETON_POSITION_KNEE_RIGHT,         NUI_SKELETON_POSITION_ANKLE_RIGHT,       INT_HIP_RIGHT, },
};

//Calculation joints rotation using skeleton positions from Kinect
void Kinect::CalculateJointRotations()
{
	//No active skeleton
	if(skeletonTarget==-1)
		return;


	for( int i = 0; i < INTERMEDIATE_JOINT_COUNT; ++i)
    {
		m_IntJoints[i].local = Ogre::Quaternion::IDENTITY;
    }
	
	
	Ogre::Vector3 hipDir(pSkeletonData[skeletonTarget].SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].x-pSkeletonData[skeletonTarget].SkeletonPositions[NUI_SKELETON_POSITION_HIP_LEFT].x,
		pSkeletonData[skeletonTarget].SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].y-pSkeletonData[skeletonTarget].SkeletonPositions[NUI_SKELETON_POSITION_HIP_LEFT].y,
		pSkeletonData[skeletonTarget].SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].z-pSkeletonData[skeletonTarget].SkeletonPositions[NUI_SKELETON_POSITION_HIP_LEFT].z);
	hipDir.y=0;
	hipDir.normalise();
	Ogre::Quaternion hipRot=Ogre::Vector3(1,0,0).getRotationTo(hipDir);
	hipRot.x=-hipRot.x;
	hipRot.y=-hipRot.y;
	hipRot.z=-hipRot.z;
	m_IntJoints[INT_ROOT].local=hipRot;
	m_IntJoints[INT_ROOT].world=hipRot;

	for( int i = 1; i < INTERMEDIATE_JOINT_COUNT; ++i )
	{
		Ogre::Quaternion ParentRot=m_IntJoints[g_IntJointDesc[i].parentIdx].world;//The object-space rotation of its parent
		Ogre::Matrix3 ParentRotMatrix;
		ParentRot.ToRotationMatrix(ParentRotMatrix);
		if( pSkeletonData[skeletonTarget].eSkeletonPositionTrackingState[ g_IntJointDesc[i].endNuiJoint] == NUI_SKELETON_POSITION_NOT_TRACKED ||
            pSkeletonData[skeletonTarget].eSkeletonPositionTrackingState[ g_IntJointDesc[i].startNuiJoint] == NUI_SKELETON_POSITION_NOT_TRACKED )
        {
			m_IntJoints[ i ].local = Ogre::Quaternion::IDENTITY;//If this bone(joint) is not tracked, do not rotate it.
        }
		else
		{
			
			Ogre::Vector3 vDir(pSkeletonData[skeletonTarget].SkeletonPositions[g_IntJointDesc[i].endNuiJoint].x-pSkeletonData[skeletonTarget].SkeletonPositions[g_IntJointDesc[i].startNuiJoint].x,
		pSkeletonData[skeletonTarget].SkeletonPositions[g_IntJointDesc[i].endNuiJoint].y-pSkeletonData[skeletonTarget].SkeletonPositions[g_IntJointDesc[i].startNuiJoint].y,
		-pSkeletonData[skeletonTarget].SkeletonPositions[g_IntJointDesc[i].endNuiJoint].z+pSkeletonData[skeletonTarget].SkeletonPositions[g_IntJointDesc[i].startNuiJoint].z);
			vDir.normalise();
			Ogre::Vector3 vBoneDir = g_IntJointDesc[i].bonedir*ParentRotMatrix;
		
			Ogre::Quaternion qRot=g_IntJointDesc[i].bonedir.getRotationTo(vDir);
		
			qRot.normalise();
			m_IntJoints[ i ].local=qRot;//Local rotation
         }
		m_IntJoints[ i ].world = m_IntJoints[ i ].local*ParentRot;//World rotation
	}
}

void Kinect::GetCharacterRotations(Ogre::Skeleton* skeleton,Ogre::Entity* character,JOINTS_MAP* jointMap)
{
	//No active skeleton
	if(skeletonTarget==-1)
		return;

	//Apply the calculated rotation to character model
	for (  int i =1 ; i<INTERMEDIATE_JOINT_COUNT ; i++ )
	{
		Ogre::Quaternion q=m_IntJoints[ i ].local;
		Ogre::Quaternion qI=skeleton->getBone(jointMap[i].m_characterJoint)->getInitialOrientation();
		skeleton->getBone(jointMap[i].m_characterJoint)->resetOrientation();
		q=skeleton->getBone(jointMap[i].m_characterJoint)->convertWorldToLocalOrientation(q);
		skeleton->getBone(jointMap[i].m_characterJoint)->setOrientation(q*qI);
		
	}

	//Rotate the whole character based on the rotation of hip.
	character->getParentSceneNode()->setOrientation(m_IntJoints[ INT_ROOT ].local);

}


//Manually control the bone and rotate it.
void Kinect::setupBone(Ogre::Entity* character,const Ogre::String& name,const Ogre::Quaternion& q)
{
	Ogre::Bone* bone = character->getSkeleton()->getBone(name);
	bone->setManuallyControlled(true);
	bone->setInheritOrientation(false);
		
	bone->resetOrientation();
	bone->setOrientation(q);
	
	bone->setInitialState();
}

//Manually control the bone and rotate it.
void Kinect::setupBone(Ogre::Entity* character,const Ogre::String& name,const Ogre::Radian& angle, const Ogre::Vector3 axis)
{
		
	Ogre::Quaternion q;
	q.FromAngleAxis(angle,axis);	 
	setupBone(character,name, q);

}

//Manually control the bone and rotate it.
void Kinect::setupBone(Ogre::Entity* character, const Ogre::String& name,const Ogre::Degree& yaw,const Ogre::Degree& pitch,const Ogre::Degree& roll)
{
	Ogre::Bone* bone = character->getSkeleton()->getBone(name);
	bone->setManuallyControlled(true);
	bone->setInheritOrientation(false);
	bone->resetOrientation();
	bone->yaw(yaw);
	bone->pitch(pitch);
	bone->roll(roll);
	bone->setInitialState();

}

//Update the character skeleton. This function should be called inside the update loop of game scene.
void Kinect::mapSkeleton(Ogre::Entity* character,Ogre::Skeleton* skeleton,JOINTS_MAP* jointMap)
{
	KINECT_NOT_INIT(initResult)
 	if(pSkeletonData==NULL)
	{
		return;
	}
	else
	{
		CalculateJointRotations();
		GetCharacterRotations(skeleton,character,jointMap);
	}
}