#include "GISKinect.h"

using namespace GIS;


void Kinect::initializeCamera()
{
	if(!m_hNextVideoFrameEvent) m_hNextVideoFrameEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
    if(!m_hNextDepthFrameEvent) m_hNextDepthFrameEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	videoEvent=m_hNextVideoFrameEvent;
	depthEvent=m_hNextDepthFrameEvent;
}

//Initialize the position, size and resolution of video window
void Kinect::initializeVideo(Ogre::Real left,Ogre::Real top,Ogre::Real width,Ogre::Real height,KINECT_RESOLUTION res)
{
	KINECT_NOT_INIT(initResult)
	if(videoInit)
	{
		return; //Video is already init
	}
	else
	{
		videoInit=true;
	}
		
	HRESULT hr = getCurrentKinect()->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR,(NUI_IMAGE_RESOLUTION)res,0,2,m_hNextVideoFrameEvent,&m_pVideoStreamHandle );
	if(FAILED(hr))
	{
		return;
	}
	if(videoTex.isNull())
	{
		videoTex = Ogre::TextureManager::getSingleton().createManual("colorKinect",
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, 640, 480, 0, Ogre::PF_X8R8G8B8, Ogre::TU_DYNAMIC_WRITE_ONLY);
	}
	Ogre::OverlayManager* overlayManager = Ogre::OverlayManager::getSingletonPtr();
	videoPanel =static_cast<Ogre::OverlayContainer*>(overlayManager->createOverlayElement("Panel","kinectVideoPanel"));
	videoPanel->setMaterialName("Kinect/DummyMaterial");
	videoPanel->setPosition(left,top);
	videoPanel->setDimensions(width,height);
	videoOverlay=overlayManager->create("KinectVideoOverlay");
	videoOverlay->add2D(videoPanel);
	videoOverlay->setZOrder(500);
}

//Update video stream
void Kinect::updateVideo()
{
	KINECT_NOT_INIT(initResult)
	if(videoEvent==NULL)
		return;
	if(displayVideo)
	{		
		// Wait for an event to be signalled
		int nEventIdx=WaitForSingleObject(videoEvent,10);
		// If the stop event, stop looping and exit
		if(nEventIdx==0 )
		{
					
			NUI_IMAGE_FRAME pImageFrame;

			HRESULT hr = getCurrentKinect()->NuiImageStreamGetNextFrame(
				m_pVideoStreamHandle,
				0,
				&pImageFrame );
			if( FAILED( hr ) )
			{
			//return;
			}
				
			INuiFrameTexture * pTexture = pImageFrame.pFrameTexture;
			_NUI_LOCKED_RECT LockedRect;
			pTexture->LockRect( 0, &LockedRect, NULL, 0 );
				
			if( LockedRect.Pitch != 0 )
			{	
				BYTE * pBuffer = (BYTE*) LockedRect.pBits;
				Ogre::PixelBox pb(640,480,1,Ogre::PF_X8R8G8B8,pBuffer);
				videoTex->getBuffer()->blitFromMemory(pb);
				videoPanel->setMaterialName("Kinect/ColorMaterial");
			}
			else
			{
			//OutputDebugString( L"Buffer length of received texture is bogus\r\n" );
			}

			getCurrentKinect()->NuiImageStreamReleaseFrame( m_pVideoStreamHandle, &pImageFrame );
		}
	}
}

//Set video window to be visible or not
void Kinect::setVideoVisible(bool isVisible)
{
	KINECT_NOT_INIT(initResult)
	if(isVisible)
	{
		videoOverlay->show();
		displayVideo=true;
	}
	else
	{
		videoOverlay->hide();
		displayVideo=false;
	}
}

//Set depth window to be visible or not
void Kinect::setDepthVisible(bool isVisible)
{
	KINECT_NOT_INIT(initResult)
	if(isVisible)
	{
		depthOverlay->show();
		displayDepth=true;
	}
	else
	{
		depthOverlay->hide();
		displayDepth=false;
	}
}




//Initialize the position, size and resolution of depth stream.
void Kinect::initializeDepth(Ogre::Real left,Ogre::Real top,Ogre::Real width,Ogre::Real height,KINECT_RESOLUTION res)
{
	KINECT_NOT_INIT(initResult)
	if(depthInit)
	{
		return; //Video is already init
	}
	else
	{
		depthInit=true;
	}
	HRESULT hr = getCurrentKinect()->NuiImageStreamOpen(NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX,(NUI_IMAGE_RESOLUTION)res,0,2,m_hNextDepthFrameEvent,&m_pDepthStreamHandle );
	if(FAILED(hr))
	{
		return;
	}
	if(depthTex.isNull())
	{
		depthTex = Ogre::TextureManager::getSingleton().createManual("depthKinect",
	Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, 320, 240, 0, Ogre::PF_B8G8R8, Ogre::TU_DYNAMIC_WRITE_ONLY);
	}
	Ogre::OverlayManager* overlayManager = Ogre::OverlayManager::getSingletonPtr();
	depthPanel =static_cast<Ogre::OverlayContainer*>(overlayManager->createOverlayElement("Panel","kinectDepthPanel"));
	depthPanel->setMaterialName("Kinect/DummyMaterial");
	depthPanel->setPosition(left,top);
	depthPanel->setDimensions(width,height);
	depthOverlay=overlayManager->create("KinectDepthOverlay");
	depthOverlay->add2D(depthPanel);
	depthOverlay->setZOrder(500);
}

//Update depth stream
void Kinect::updateDepth()
{
	KINECT_NOT_INIT(initResult)
	if(depthEvent==NULL)
	{
		return;
	}
	if(displayDepth)
	{		
		// Wait for an event to be signalled
		int nEventIdx=WaitForSingleObject(depthEvent,0);
		// If the stop event, stop looping and exit
		if(nEventIdx==0)
		{
			//printf("DEPTH EVENT OK - %d\n", nEventIdx);
			NUI_IMAGE_FRAME pImageFrame2;
			HRESULT hr = getCurrentKinect()->NuiImageStreamGetNextFrame(
			m_pDepthStreamHandle,
			0,
			&pImageFrame2 );
			if( FAILED( hr ) )
			{
				printf("DEPTH FAIL\n");
				//return;
			}
				
			INuiFrameTexture * pTexture2 = pImageFrame2.pFrameTexture;
			_NUI_LOCKED_RECT LockedRect;
			pTexture2->LockRect( 0, &LockedRect, NULL, 0 );
				
			if( LockedRect.Pitch != 0 )
			{
				BYTE * pBuffer2 = (BYTE*) LockedRect.pBits;

				RGBQUAD * rgbrun = m_rgbWk;
				USHORT * pBufferRun = (USHORT*) pBuffer2;
				for( int y = 0 ; y < 240 ; y++ )
				{
					for( int x = 0 ; x < 320 ; x++ )
					{
						RGBQUAD quad = Nui_ShortToQuad_Depth( *pBufferRun );
						pBufferRun++;
						*rgbrun = quad;
						rgbrun++;
					}
				}
				Ogre::PixelBox pb2(320,240,1,Ogre::PF_B8G8R8,(BYTE*) m_rgbWk);
				depthTex->getBuffer()->blitFromMemory(pb2);
				depthPanel->setMaterialName("Kinect/DepthMaterial");
			}
			else
			{
				printf("DEPTH FAIL 2\n");
				//OutputDebugString( L"Buffer length of received texture is bogus\r\n" );
			}

			getCurrentKinect()->NuiImageStreamReleaseFrame( m_pDepthStreamHandle, &pImageFrame2 );
		}else{
			//printf("DEPTH EVENT FAIL - %d\n", nEventIdx);
		}
	}
}



//Process the depth image. Partition the player's image from depth image.
GIS::RGBQUAD Kinect::Nui_ShortToQuad_Depth( USHORT s )
{
    USHORT RealDepth = (s & 0xfff8) >> 3;
    USHORT Player = s & 7;

    // transform 13-bit depth information into an 8-bit intensity appropriate
    // for display (we disregard information in most significant bit)
    BYTE l = 255 - (BYTE)(256*RealDepth/0x0fff);

    RGBQUAD q;
    q.rgbRed = q.rgbBlue = q.rgbGreen = 0;

    switch( Player )
    {
    case 0:
        q.rgbRed = l / 2;
        q.rgbBlue = l / 2;
        q.rgbGreen = l / 2;
        break;
    case 1:
        q.rgbRed = l;
        break;
    case 2:
        q.rgbGreen = l;
        break;
    case 3:
        q.rgbRed = l / 4;
        q.rgbGreen = l;
        q.rgbBlue = l;
        break;
    case 4:
        q.rgbRed = l;
        q.rgbGreen = l;
        q.rgbBlue = l / 4;
        break;
    case 5:
        q.rgbRed = l;
        q.rgbGreen = l / 4;
        q.rgbBlue = l;
        break;
    case 6:
        q.rgbRed = l / 2;
        q.rgbGreen = l / 2;
        q.rgbBlue = l;
        break;
    case 7:
        q.rgbRed = 255 - ( l / 2 );
        q.rgbGreen = 255 - ( l / 2 );
        q.rgbBlue = 255 - ( l / 2 );
    }

    return q;
}