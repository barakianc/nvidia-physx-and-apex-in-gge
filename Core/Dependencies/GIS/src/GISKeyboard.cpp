#include "GISKeyboard.h"

//#ifdef WRAPPER
using namespace GIS;

// Destructor 
Keyboard::~Keyboard(){
	if (m_keyboard != NULL){
		delete m_keyboard;
	}
}

void Keyboard::_initialize(){
	m_keyboard = static_cast<OIS::Keyboard*> (mCreator->createOISObject( mType, mBuffered ));
	m_keyboard->setEventCallback(this);
}

// Interface functions for communicating between 
// the program implementing GIS and the OIS Object
void Keyboard::capture(){
	m_keyboard->capture();
}

bool Keyboard::isKeyDown( KeyCode key )
{
	return (GIS::KeyCode)m_keyboard->isKeyDown((OIS::KeyCode)key);
}

void Keyboard::setTextTranslation( TextTranslationMode mode ){
	m_keyboard->setTextTranslation((OIS::Keyboard::TextTranslationMode) mode);
}

Keyboard::TextTranslationMode Keyboard::getTextTranslation(){
	return (GIS::Keyboard::TextTranslationMode) m_keyboard->getTextTranslation();
}

const std::string& Keyboard::getAsString( KeyCode kc ){
	return m_keyboard->getAsString((OIS::KeyCode)kc);
}

bool Keyboard::isModifierDown( Modifier mod ){
	return m_keyboard->isModifierDown((OIS::Keyboard::Modifier)mod);
}

void Keyboard::copyKeyStates( char keys[256] ){
	m_keyboard->copyKeyStates(keys);
}

// Inherited Functions: OIS::KeyListener
bool Keyboard::keyPressed( const OIS::KeyEvent &arg ){
	return mListener->keyPressed(KeyEvent(this, (GIS::KeyCode)arg.key, arg.text));
}
bool Keyboard::keyReleased( const OIS::KeyEvent &arg ){
	return mListener->keyReleased(KeyEvent(this, (GIS::KeyCode)arg.key, arg.text));
}

void Keyboard::setBuffered(bool buffered = true){
	mBuffered = buffered;
}


//#endif