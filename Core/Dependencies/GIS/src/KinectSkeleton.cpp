#include "GISKinect.h"

using namespace GIS;

//Separate thread to handle heavy skeleton-related computation (gesture recognition and gesture record)
DWORD WINAPI Kinect::ProcessSkeletonEvent(LPVOID pParam)
{
	Kinect *pthis=(Kinect *) pParam;
	HANDLE                hEvents[2];
	hEvents[0]=pthis->newSkeletonEvent;
	hEvents[1]=pthis->m_SkeletonThreadStop;
	while(true)
	{
		int nEventIdx=WaitForMultipleObjects(sizeof(hEvents)/sizeof(hEvents[0]),hEvents,FALSE,INFINITE);//wait for new skeleton frame
		if(nEventIdx==0)
		{
			//do gesture recognition
			pthis->processNewFrame();
		}
		else if(nEventIdx == 1)
		{
			break;
		}
	}
	return 0;
}

void Kinect::initializeSkeletonManual()
{
	pskeletonManual=new Ogre::ManualObject("SkeletonWindow");
	//pskeletonManual=pManual;
	// Use identity view/projection matrices
	pskeletonManual->setUseIdentityProjection(true);
	pskeletonManual->setUseIdentityView(true);
	// Use infinite AAB to always stay visible
	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	pskeletonManual->setBoundingBox(aabInf);
	// Render just before overlays
	pskeletonManual->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY+1);
	pskeletonManual->setQueryFlags(0);
}

//do whatever is necessary so you can initialize the skeleton on a new Kinect
//but don't necessarily start displaying yet
void Kinect::switchSkeletonKinect()
{
	if(skeletonInit)
	{
		isSkeletonVisible = false;
		doGestureReco=false;
		doRecord=false;
		pskeletonManual->setVisible(false);
		pskeletonManual->detachFromParent();
		delete pskeletonManual; //stop memory leaking???
		pskeletonManual = NULL;
		skeletonInit = false;
		//I *THINK* that's all we need to do.
	}
}

//Initialize skeleton tracking
void Kinect::initializeSkeleton(Ogre::SceneManager* pSceneManager, std::string gameFolder, float left,float top,float width,float height)
{
	KINECT_NOT_INIT(initResult)
	if(skeletonInit)
	{
		return; //Skeleton is already initialized
	}
	else
	{
		skeletonInit=true;
	}

	 if(pSceneManager!=NULL&&pskeletonManual!=NULL)
	{
		// Attach to scene
		pskeletonManual->detachFromParent();
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pskeletonManual);
		pskeletonManual->setVisible(true);
	}
	 //enable skeleton tracking
	HRESULT hr = getCurrentKinect()->NuiSkeletonTrackingEnable( hSkeletonEvent, 0 );
	if( FAILED( hr ) )
	{
		//MessageBoxResource(m_hWnd,IDS_ERROR_SKELETONTRACKING,MB_OK | MB_ICONHAND);
		//return hr;
	}
	isSkeletonVisible=true;
	doGestureReco=false;
	doRecord=false;
	initSkeletonName();
	this->gameFolder=gameFolder;
	getAllGestures(); //read all gesture data;
	SWindowLeft=left*2-1;
	SWindowRight=SWindowLeft+width*2;
	SWindowTop=-top*2+1;
	SWindowBottom=SWindowTop-height*2;
	isSkeletonNew=false;
}

//Enable gesture recognizion
void Kinect::enableGestureReco(bool enable)
{
	doGestureReco=enable;
}

//Display tracked skeleton from Kinect
void Kinect::displaySkeleton(Ogre::SceneManager* pSceneManager)
{
	KINECT_NOT_INIT(initResult)
	if(pSceneManager!=NULL&&pskeletonManual!=NULL)
	{
		pskeletonManual->detachFromParent();
		//pskeletonManual=pManual;
		// Use identity view/projection matrices
		pskeletonManual->setUseIdentityProjection(true);
		pskeletonManual->setUseIdentityView(true);
		// Use infinite AAB to always stay visible
		Ogre::AxisAlignedBox aabInf;
		aabInf.setInfinite();
		pskeletonManual->setBoundingBox(aabInf);
	 
		// Render just before overlays
		pskeletonManual->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY+1);
		pskeletonManual->setQueryFlags(0);
	 
		// Attach to scene
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pskeletonManual);
		pskeletonManual->setVisible(true);
	}
}


//Update skeleton
void  Kinect::updateSkeleton()
{
	KINECT_NOT_INIT(initResult)
	if(hSkeletonEvent==NULL)
		return;
	int nEventIdx=WaitForSingleObject(hSkeletonEvent,0);
	if(nEventIdx==0)
	{
		storeNewSkeletonFrame();
		if(isSkeletonVisible&&pskeletonManual!=NULL)
		{
			DrawSkeleton();
		}
		if(doGestureReco||doRecord)
		{
			//do gesture reco
			SetEvent(newSkeletonEvent);
		}
	}	
}

//Store the new data and track the one active skeleton
void Kinect::storeNewSkeletonFrame()
{
	NUI_SKELETON_FRAME SkeletonFrame= {0};

	HRESULT hr = getCurrentKinect()->NuiSkeletonGetNextFrame( 0, &SkeletonFrame );

	getCurrentKinect()->NuiTransformSmooth(&SkeletonFrame,NULL);


	for( int i = 0 ; i < NUI_SKELETON_COUNT ; i++ )
	{
		pSkeletonData[i]=SkeletonFrame.SkeletonData[i];
		
	}
	isSkeletonNew=true;//indicate new skeleton data is available

	if(skeletonTarget==-1)
	{
		for( int i = 0 ; i < NUI_SKELETON_COUNT ; i++ )
		{
			if(pSkeletonData[i].eTrackingState == NUI_SKELETON_TRACKED)
			{
				skeletonTarget=i;
				break;
			}//find a skeleton target to track
		}
	}
	else if(pSkeletonData[skeletonTarget].eTrackingState!=NUI_SKELETON_TRACKED)
	{
		skeletonTarget=-1;
	}//track another skeleton target from next frame
}

//Set skeleton tracking visible or not
void  Kinect::setSkeletonVisible(bool isVisible)
{
	KINECT_NOT_INIT(initResult)
	pskeletonManual->setVisible(isVisible);
	isSkeletonVisible=isVisible;;
}

//Drew the black background
void Kinect::DrawBackground()
{
	pskeletonManual->clear();
	// Use identity view/projection matrices
	pskeletonManual->setUseIdentityProjection(true);
	pskeletonManual->setUseIdentityView(true);
	// Use infinite AAB to always stay visible
	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	pskeletonManual->setBoundingBox(aabInf);
	//Render just before overlays
	pskeletonManual->begin("Kinect/SkeletonMaterial", Ogre::RenderOperation::OT_TRIANGLE_FAN);
	pskeletonManual->position(Ogre::Real(SWindowLeft), Ogre::Real(SWindowBottom), Ogre::Real(-1));
	pskeletonManual->colour(Ogre::ColourValue::Black);
	pskeletonManual->position( Ogre::Real(SWindowRight), Ogre::Real(SWindowBottom), Ogre::Real(-1));
	pskeletonManual->colour(Ogre::ColourValue::Black);
	pskeletonManual->position(Ogre::Real(SWindowRight),  Ogre::Real(SWindowTop), Ogre::Real(-1));
	pskeletonManual->colour(Ogre::ColourValue::Black);
	pskeletonManual->position(Ogre::Real(SWindowLeft),  Ogre::Real(SWindowTop), Ogre::Real(-1));
	pskeletonManual->colour(Ogre::ColourValue::Black);

	pskeletonManual->index(0);
	pskeletonManual->index(1);
	pskeletonManual->index(2);
	pskeletonManual->index(3);
	pskeletonManual->end();
}

//Draw all the skeleton
void Kinect::DrawSkeleton()
{
	bool bFoundSkeleton = true;
	for( int i = 0 ; i < NUI_SKELETON_COUNT ; i++ )
	{;
		if( pSkeletonData[i].eTrackingState == NUI_SKELETON_TRACKED )
		{
			bFoundSkeleton = false;
		}
	}

	// no skeletons!
	//
	if( bFoundSkeleton )
	{
		DrawBackground();
	}
	else
	{
		DrawBackground();
		for( int i = 0 ; i < NUI_SKELETON_COUNT ; i++ )
		{
			if( pSkeletonData[i].eTrackingState == NUI_SKELETON_TRACKED )
			{
				DrawIndividualSkeleton(&pSkeletonData[i]);
			}
		}
	}
}

//Draw individual skeleton
void Kinect::DrawIndividualSkeleton(NUI_SKELETON_DATA* skeleton)
{
	float fx=0,fy=0;
	int i;
	for (i = 0; i < NUI_SKELETON_POSITION_COUNT; i++)
	{
		NuiTransformSkeletonToDepthImage( skeleton->SkeletonPositions[i], &fx, &fy );
		fx/=320.0f;
		fy/=240.0f;
		fy=1-fy;
		xposition[i] =  SWindowLeft+fx*(SWindowRight-SWindowLeft);
		if(xposition[i]>SWindowRight)
			xposition[i]=SWindowRight;
		if(xposition[i]<SWindowLeft)
			xposition[i]=SWindowLeft;
		
		yposition[i] = SWindowBottom+fy*(SWindowTop-SWindowBottom);
		if(yposition[i]>SWindowTop)
			yposition[i]=SWindowTop;
		if(yposition[i]<SWindowBottom)
			yposition[i]=SWindowBottom;
		//printf("left:%f ,right:%f\n",SWindowLeft,SWindowRight);
	}
	DrawSkeletonSegment(skeleton,4,NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_SPINE, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_HEAD);
	DrawSkeletonSegment(skeleton,5,NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT);
	DrawSkeletonSegment(skeleton,5,NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT);
	DrawSkeletonSegment(skeleton,5,NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_LEFT, NUI_SKELETON_POSITION_KNEE_LEFT, NUI_SKELETON_POSITION_ANKLE_LEFT, NUI_SKELETON_POSITION_FOOT_LEFT);
	DrawSkeletonSegment(skeleton,5,NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_RIGHT, NUI_SKELETON_POSITION_KNEE_RIGHT, NUI_SKELETON_POSITION_ANKLE_RIGHT, NUI_SKELETON_POSITION_FOOT_RIGHT);
	pskeletonManual->begin("Kinect/SkeletonMaterial", Ogre::RenderOperation::OT_POINT_LIST);
	//draw joint
	for (i = 0; i < NUI_SKELETON_POSITION_COUNT ; i++)
	{
		pskeletonManual->position(Ogre::Real(xposition[i]),Ogre::Real(yposition[i]),Ogre::Real(-1));
		pskeletonManual->colour(Ogre::ColourValue::Blue);
	}
	pskeletonManual->end();
}

void Kinect::DrawSkeletonSegment( NUI_SKELETON_DATA * pSkel, int numJoints, ... )
{
	va_list vl;
	va_start(vl,numJoints);
	int preIndex = va_arg(vl,NUI_SKELETON_POSITION_INDEX);
	for (int i=1;i<numJoints; i++)
	{
		NUI_SKELETON_POSITION_INDEX jointIndex = va_arg(vl,NUI_SKELETON_POSITION_INDEX);
		float dx;
		float dy;
		Ogre::Radian angle=Ogre::Math::ATan2((yposition[jointIndex]-yposition[preIndex]),(xposition[jointIndex]-xposition[preIndex]));
		dy=(float)0.01*Ogre::Math::Cos(angle)/2;
		dx=(float)0.01*Ogre::Math::Sin(angle)/2;
		pskeletonManual->begin("Kinect/SkeletonMaterial", Ogre::RenderOperation::OT_LINE_STRIP);
		pskeletonManual->position(Ogre::Real(xposition[preIndex]-dx),Ogre::Real(yposition[preIndex]+dy),Ogre::Real(-1));
		pskeletonManual->colour(Ogre::ColourValue::Red);
		pskeletonManual->position(Ogre::Real(xposition[preIndex]+dx),Ogre::Real(yposition[preIndex]-dy),Ogre::Real(-1));
		pskeletonManual->colour(Ogre::ColourValue::Red);
		pskeletonManual->position(Ogre::Real(xposition[jointIndex]+dx),Ogre::Real(yposition[jointIndex]-dy),Ogre::Real(-1));
		pskeletonManual->colour(Ogre::ColourValue::Red);
		pskeletonManual->position(Ogre::Real(xposition[jointIndex]-dx),Ogre::Real(yposition[jointIndex]+dy),Ogre::Real(-1));
		pskeletonManual->colour(Ogre::ColourValue::Red);

		pskeletonManual->index(0);
		pskeletonManual->index(1);
		pskeletonManual->index(2);
		pskeletonManual->index(3);
		pskeletonManual->index(0);
		pskeletonManual->end();
		preIndex=jointIndex;
	}
	va_end(vl);
}

//Get skeleton data from Kinect
NUI_SKELETON_DATA* Kinect::getSkeletonData()
{
	isSkeletonNew=false;
	return pSkeletonData;
}

//Retrive the new skeleton data structure
NUI_SKELETON_DATA* Kinect::getNewSkeletonData()
{
	if(isSkeletonNew)
	{
		isSkeletonNew=false;
		return pSkeletonData;
	}
	else
	{
		return NULL;
	}
}


//Retrieve the position of a joint of current track target
void Kinect::getSkeletonPosition(int skeletonNum, float* x, float* y, float* z)
{
	KINECT_NOT_INIT(initResult)
	*x=pSkeletonData[skeletonTarget].SkeletonPositions[skeletonNum].x;
	*y=pSkeletonData[skeletonTarget].SkeletonPositions[skeletonNum].y;
	*z=pSkeletonData[skeletonTarget].SkeletonPositions[skeletonNum].z;
}

//Process new skeleton frame, that recognition or record
void Kinect::processNewFrame()
{
	//No active skeleton
	if(skeletonTarget==-1)
		return;


	std::map<std::string,GestureData>::iterator iter;
	float xp[NUI_SKELETON_POSITION_COUNT],yp[NUI_SKELETON_POSITION_COUNT],zp[NUI_SKELETON_POSITION_COUNT];

	for(int i=0;i<NUI_SKELETON_POSITION_COUNT;i++)
	{
		xp[i]=pSkeletonData[skeletonTarget].SkeletonPositions[i].x;
		yp[i]=pSkeletonData[skeletonTarget].SkeletonPositions[i].y;
		zp[i]=pSkeletonData[skeletonTarget].SkeletonPositions[i].z;
	}//get the new skeleton position

	float cx=(xp[NUI_SKELETON_POSITION_HIP_LEFT]+xp[NUI_SKELETON_POSITION_HIP_RIGHT])/2;
	float cy=(yp[NUI_SKELETON_POSITION_HIP_LEFT]+yp[NUI_SKELETON_POSITION_HIP_RIGHT])/2;
	float cz=(zp[NUI_SKELETON_POSITION_HIP_LEFT]+zp[NUI_SKELETON_POSITION_HIP_RIGHT])/2;

	for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++)
	{
		xp[i]-=cx;
		yp[i]-=cy;
		zp[i]-=cz;
	}

	float hipDist = (float)std::sqrt(std::pow(xp[NUI_SKELETON_POSITION_HIP_LEFT]-xp[NUI_SKELETON_POSITION_HIP_RIGHT],2)+std::pow(yp[NUI_SKELETON_POSITION_HIP_LEFT]-yp[NUI_SKELETON_POSITION_HIP_RIGHT],2)+std::pow(zp[NUI_SKELETON_POSITION_HIP_LEFT]-zp[NUI_SKELETON_POSITION_HIP_RIGHT],2));
		
	for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++)
	{
		xp[i] /= hipDist;
		yp[i] /= hipDist;
		zp[i] /= hipDist;

	}// normalise the skeleton data
	if(doGestureReco)
	{
		
		for(iter=gestureMap.begin();iter!=gestureMap.end();iter++)
		{
			GestureData* pgd=&iter->second;
			SkeletonPosition sp;
			std::vector<SkeletonPosition> frame;
			frame.clear();
			if(!pgd->isActive)
				continue;
			for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++)
			{
				if(pgd->isSkeletonSelected[i])
				{
					sp.x=xp[i];
					sp.y=yp[i];
					sp.z=zp[i];
					frame.push_back(sp);
				}
			}
			pgd->seq.push_back(frame);
		}//store the new frame for every gesture

		for(iter=gestureMap.begin();iter!=gestureMap.end();iter++)
		{
			GestureData* pgd=&iter->second;
			if(!pgd->isActive)
				continue;
			if((int)pgd->seq.size()>pgd->model.size()*2)
				pgd->seq.erase(pgd->seq.begin());//if the number of frame of a gesture is larger than minRecoFrame*2, then delete the first frame, so that the total of frames remains unchanged
			if(recognize(pgd)==true)
			{
				//Gesture recognized!
				pgd->seq.clear();
				std::string recoName=iter->first;
				//printf_s("%s\n",recoName.c_str());
				kinectListener->gestureRecognized(recoName);
				
			}
		}
	}

	//Record the new normalized skeleton data into xml file
	if(doRecord)
	{
		TiXmlElement *gestureFrame= new TiXmlElement("GestureFrame");
		gestureData->LinkEndChild(gestureFrame);
		for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++)
		{
			if(isSkeletonSelected[i]==false)
				continue;
			//outfile<<xp[i]<<endl<<yp[i]<<endl;
			TiXmlElement *skeletonPosition=new TiXmlElement("SkeletonPosition");
			gestureFrame->LinkEndChild(skeletonPosition);
			skeletonPosition->SetAttribute("ID",Ogre::StringConverter::toString(i).data());
			skeletonPosition->SetAttribute("x",Ogre::StringConverter::toString(xp[i]).data());
			skeletonPosition->SetAttribute("y",Ogre::StringConverter::toString(yp[i]).data());
			skeletonPosition->SetAttribute("z",Ogre::StringConverter::toString(zp[i]).data());
		}
	}
}


void Kinect::startRecordGesture(std::string gestureName, float globalThreshold, float firstThreshold, float minRecoTime,bool *isSkeletonSelected)
{
	this->gestureName=gestureName;
	this->globalThreshold=globalThreshold;
	this->firstThreshold=firstThreshold;
	this->minRecoTime=minRecoTime;
	this->isSkeletonSelected=isSkeletonSelected;
	createGestureXML();
	doRecord=true;
}

void Kinect::stopRecordGesture()
{
	doRecord=false;
	std::string GesturePath = "";
	GesturePath.append("..\\..\\");		
	GesturePath.append(gameFolder.c_str());
	GesturePath.append("\\Media\\Kinect\\");
	GesturePath.append(gestureName);
	GesturePath.append(".gesture");
	myDocument->SaveFile(GesturePath.c_str());
}