#include "GIS360.h"
//#include <iostream>

using namespace GIS;

XPad::~XPad(){}

void XPad::_initialize(){}

void XPad::capture(){
	XINPUT_STATE xState;
	ZeroMemory(&xState, sizeof(XINPUT_STATE));

	DWORD Result = XInputGetState(mControllerNumber, &xState);

	if(Result == ERROR_SUCCESS)
	{
		if (mState.buttons != xState.Gamepad.wButtons)
		{
			int bPressed = 0, bReleased = 0;
			int bID;

			bPressed =  (~(mState.buttons & xState.Gamepad.wButtons)) & xState.Gamepad.wButtons;
			bReleased = (~((mState.buttons | bPressed) & xState.Gamepad.wButtons)) & mState.buttons;
			//std::cout << "bPressed: " << bPressed << ", bReleased: " << bReleased << std::endl;
			switch(bPressed)
			{
			case 0:
				break;
			case XP_UP:
				bID = XP_UP;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_DOWN:
				bID = XP_DOWN;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_LEFT:
				bID = XP_LEFT;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_RIGHT:
				bID = XP_RIGHT;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_START:
				bID = XP_START;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_BACK:
				bID = XP_BACK;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_LTHUMB:
				bID = XP_LTHUMB;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_RTHUMB:
				bID = XP_RTHUMB;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_LSHOULDER:
				bID = XP_LSHOULDER;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_RSHOULDER:
				bID = XP_RSHOULDER;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_A:
				bID = XP_A;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_B:
				bID = XP_B;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_X:
				bID = XP_X;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			case XP_Y:
				bID = XP_Y;
				mState.buttons |= bID;
				mListener->buttonPressed(XPadEvent(this, mState), bID);
				return;
			default:
				for (int x = 0; x < MAX_BUTTONS; x++){
					bID = 1<<x;
					if (bPressed & bID){
						mState.buttons |= bID;
						mListener->buttonPressed(XPadEvent(this, mState), bID);
						return;
					}
				}
			}

			switch(bReleased)
			{
			case 0:
				break;
			case XP_UP:
				bID = XP_UP;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_DOWN:
				bID = XP_DOWN;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_LEFT:
				bID = XP_LEFT;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_RIGHT:
				bID = XP_RIGHT;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_START:
				bID = XP_START;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_BACK:
				bID = XP_BACK;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_LTHUMB:
				bID = XP_LTHUMB;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_RTHUMB:
				bID = XP_RTHUMB;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_LSHOULDER:
				bID = XP_LSHOULDER;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_RSHOULDER:
				bID = XP_RSHOULDER;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_A:
				bID = XP_A;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_B:
				bID = XP_B;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_X:
				bID = XP_X;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			case XP_Y:
				bID = XP_Y;
				mState.buttons &= ~bID;
				mListener->buttonReleased(XPadEvent(this, mState), bID);
				return;
			default:
				for (int x = 0; x < MAX_BUTTONS; x++){
					bID = 1<<x;
					if (bReleased & bID){
						mState.buttons &= ~bID;
						mListener->buttonReleased(XPadEvent(this, mState), bID);
						return;
					}
				}
			}
		}
		if (mState.XP_lTrigger != xState.Gamepad.bLeftTrigger)
		{
			mState.XP_lTrigger = xState.Gamepad.bLeftTrigger;
			mListener->lTriggerPressed(XPadEvent(this, mState));
		}
		if (mState.XP_rTrigger != xState.Gamepad.bRightTrigger)
		{
			mState.XP_rTrigger = xState.Gamepad.bRightTrigger;
			mListener->rTriggerPressed(XPadEvent(this, mState));
		}
		if ((mState.XP_lThumbX  != xState.Gamepad.sThumbLX) ||
			(mState.XP_lThumbY  != xState.Gamepad.sThumbLY)){
			mState.XP_lThumbX  = xState.Gamepad.sThumbLX;
			mState.XP_lThumbY  = xState.Gamepad.sThumbLY;
			mListener->lThumbStickMoved(XPadEvent(this, mState));
		}
		if ((mState.XP_rThumbX  != xState.Gamepad.sThumbRX) ||
			(mState.XP_rThumbY  != xState.Gamepad.sThumbRY)){
			mState.XP_rThumbX  = xState.Gamepad.sThumbRX;
			mState.XP_rThumbY  = xState.Gamepad.sThumbRY;
			mListener->rThumbStickMoved(XPadEvent(this, mState));
		}
	}
}

