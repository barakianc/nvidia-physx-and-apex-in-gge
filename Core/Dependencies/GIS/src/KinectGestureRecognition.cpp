//This file implements the DTW algorithm for gesture recognition and gesture file related function

#include "GISKinect.h"

using namespace GIS;

std::vector<std::vector<SkeletonPosition>> seq;
//Read all the gesture file under /Media/Kinect
void Kinect::getAllGestures()
{
	std::string GesturePath = "";
	GesturePath.append("..\\..\\");		
	GesturePath.append(gameFolder.c_str());
	GesturePath.append("\\Media\\Kinect\\");

	WIN32_FIND_DATAA wfd;
	HANDLE hHandle;

	std::string SearchPath = "";
	SearchPath=GesturePath+"*.gesture";
	hHandle = FindFirstFileA(SearchPath.c_str(), &wfd);//Searches a directory for a file or subdirectory
		  //with a name that matches a specific name

	 if (INVALID_HANDLE_VALUE == hHandle)
	{
		 printf_s("Cannot open gesture folder\n");
	}

	 do
	{
		 //. or ..
		  if (wfd.cFileName[0] == '.')
		 {
			  continue;
		  }
		  // if exists sub-directory
		  if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)//dwFileAttributes:The file attributes of a file
		{             
		  //FILE_ATTRIBUTE_DIRECTORY:The handle identifies a directory
			 continue;
		 }
		else//if file
		{
			std::string FileName;
			FileName = GesturePath  + wfd.cFileName;
			readGestureFile(FileName);
		 }
	}while (FindNextFileA(hHandle, &wfd));
	FindClose(hHandle);//Closes a file search handle opened by the FindFirstFile, FindFirstFileEx,
}

 //Read the gesture data from a gesture file
void Kinect::readGestureFile(std::string fileName)
{
	GestureData gd;
	TiXmlDocument *myDocument = new TiXmlDocument(fileName.c_str());
	myDocument->LoadFile();
	TiXmlElement *RootElement = myDocument->RootElement();
	if(RootElement==NULL)
	{
		return;
	}
	if(std::strcmp(RootElement->Value(),"KinectGesture")!=0)
	{
		//report error
		return;
	}

	TiXmlAttribute *fisrtThreshold=RootElement->FirstAttribute();;
	gd.firstThreshold=(float)fisrtThreshold->DoubleValue();

	TiXmlAttribute *globalThreshold=fisrtThreshold->Next();
	gd.globalThreshold=(float)globalThreshold->DoubleValue();

	TiXmlAttribute *minRecoTime=globalThreshold->Next();
	gd.minRecoFrame=(int)floor(((float)minRecoTime->DoubleValue())*30);

	TiXmlAttribute *gestureName=minRecoTime->Next();
	Ogre::String gn=gestureName->Value();

	TiXmlElement *jointsList=RootElement->FirstChildElement();
	TiXmlElement *jointElement=jointsList->FirstChildElement();


	int i=0;
	for(i=0;i<NUI_SKELETON_POSITION_COUNT;i++)
	{
		gd.isSkeletonSelected[i]=false;
	}
	i=0;
	while(jointElement!=NULL)
	{
		for(;i<NUI_SKELETON_POSITION_COUNT;i++)
		{
			if(std::strcmp(jointElement->FirstChild()->Value(),skeletonName.at(i).data())==0)
			{
				gd.isSkeletonSelected[i]=true;
				i++;
				break;
			}
		}
		jointElement=jointElement->NextSiblingElement();
	}

	TiXmlElement *gestureData=jointsList->NextSiblingElement();
	TiXmlElement *gestureFrame=gestureData->FirstChildElement();
	std::vector<SkeletonPosition> frame;
	//std::vector<std::vector<SkeletonPosition>> model;
	gd.model.clear();
	while(gestureFrame!=NULL)
	{
		frame.clear();
		TiXmlElement *skeletonPosition=gestureFrame->FirstChildElement();
		while(skeletonPosition!=NULL)
		{
			//int ID;
			TiXmlAttribute *attribute=skeletonPosition->FirstAttribute();
			attribute=attribute->Next();
			SkeletonPosition sp;
			sp.x=(float)atof(attribute->Value());
			attribute=attribute->Next();
			sp.y=(float)atof(attribute->Value());
			attribute=attribute->Next();
			sp.z=(float)atof(attribute->Value());
			frame.push_back(sp);
			skeletonPosition=skeletonPosition->NextSiblingElement();
		}
		gd.model.push_back(frame);
		gestureFrame=gestureFrame->NextSiblingElement();
	}
	gd.isActive=true;
	gestureMap.insert(std::pair<std::string, GestureData>(gn,gd));
}

void Kinect::initSkeletonName()
{
	std::string sn;
	sn="Hip";
	skeletonName.push_back(sn);
	sn="Spine";
	skeletonName.push_back(sn);
	sn="Center Shoulder";
	skeletonName.push_back(sn);
	sn="Head";
	skeletonName.push_back(sn);
	sn="Left Shoulder";
	skeletonName.push_back(sn);
	sn="Left Elbow";
	skeletonName.push_back(sn);
	sn="Left Wrist";
	skeletonName.push_back(sn);
	sn="Left Hand";
	skeletonName.push_back(sn);
	sn="Right Shoulder";
	skeletonName.push_back(sn);
	sn="Right Elbow";
	skeletonName.push_back(sn);
	sn="Right Wrist";
	skeletonName.push_back(sn);
	sn="Right Hand";
	skeletonName.push_back(sn);
	sn="Left Hip";
	skeletonName.push_back(sn);
	sn="Left Knee";
	skeletonName.push_back(sn);
	sn="Left Ankle";
	skeletonName.push_back(sn);
	sn="Left Foot";
	skeletonName.push_back(sn);
	sn="Right Hip";
	skeletonName.push_back(sn);
	sn="Right Knee";
	skeletonName.push_back(sn);
	sn="Right Ankle";
	skeletonName.push_back(sn);
	sn="Right Foot";
	skeletonName.push_back(sn);
}

std::vector<std::string> Kinect::getSkeletonName()
{
	return skeletonName;
}

//Using DTW to recognize gesture
bool Kinect::recognize(GestureData* gd)
{

	float f=dist(gd->seq[gd->seq.size()-1],gd->model[gd->model.size()-1]);
	gd->computefirst=f;
	if(f>gd->firstThreshold)
	{
		gd->computeglobal=-1;
		return false;
	}
	else
	{
		float d=dtw(gd->seq,gd->model,gd->minRecoFrame);
		gd->computeglobal=d;
		if(d<gd->globalThreshold)
			return true;
	}

	return false;
}

//Distance between two gesture frame
float Kinect::dist(std::vector<SkeletonPosition> a,std::vector<SkeletonPosition> b)
{
	float d=0.0;
	size_t dim=a.size();
	for(size_t i=0;i<dim;i++)
	{
		d=d+std::pow(a[i].x-b[i].x,2)+std::pow(a[i].y-b[i].y,2);
	}
	return std::sqrt(d);
}

//DTW main body. Compute the minimal distance from gesture model and on-live gestreu data.
float Kinect::dtw(std::vector<std::vector<SkeletonPosition>> seq,std::vector<std::vector<SkeletonPosition>> model,int minLength)
{
	int dim1=(int)seq.size();
	int dim2=(int)model.size();
	std::vector<std::vector<float>> tab;
	for(int i=dim1;i>=0;i--)
	{
		std::vector<float> t(dim2+1,std::numeric_limits <float> ::max());
		tab.push_back(t);
		/*for(int j=dim2;j>=0;j--)
		{
			tab[i*(dim2+1)+j]=std::numeric_limits <double> ::max();
		}*/
	}
	tab[dim1][dim2]=0;
	for(int i=dim1-1;i>=0;i--)
	{
		for(int j=dim2-1;j>=0;j--)
		{
			float d=dist(seq[i],model[j]);
			if(tab[i+1][j]<tab[i][j+1]&&tab[i+1][j]<tab[i+1][j+1])
			{
				tab[i][j]=d+tab[i+1][j];
			}
			else if(tab[i][j+1]<tab[i+1][j]&&tab[i][j+1]<tab[i+1][j+1])
			{
				tab[i][j]=d+tab[i][j+1];
			}
			else
			{
				tab[i][j]=d+tab[i+1][j+1];
			}
		}
	}
	float bestMatch=std::numeric_limits <float> ::max();
	for(int i=(int)dim1-1-minLength;i>=0;i--)
	{
		if(tab[i][0]<bestMatch)
			bestMatch=tab[i][0];
	}
	return bestMatch;
}

void Kinect::createGestureXML()
{
	myDocument = new TiXmlDocument();
	RootElement = new TiXmlElement("KinectGesture");
	RootElement->SetAttribute("FirtstThreshold",Ogre::StringConverter::toString(firstThreshold).data());
	RootElement->SetAttribute("GlobalThreshold",Ogre::StringConverter::toString(globalThreshold).data());
	RootElement->SetAttribute("MinRecognitionTime",Ogre::StringConverter::toString(minRecoTime).data());
	RootElement->SetAttribute("Name",gestureName.data());
	TiXmlElement *JointsListElement=new TiXmlElement("JointsList");
	myDocument->LinkEndChild(RootElement);
	RootElement->LinkEndChild(JointsListElement);
	for(int i=0;i<NUI_SKELETON_POSITION_COUNT;i++)
	{
		if(isSkeletonSelected[i]==true)
		{
			TiXmlElement *JointElement=new TiXmlElement("Joint");
			Ogre::String jn=skeletonName.at(i);
			TiXmlText *JointName = new TiXmlText(jn.data());
			JointElement->LinkEndChild(JointName);
			JointsListElement->LinkEndChild(JointElement);
		}
	}
	gestureData= new TiXmlElement("GestureData");
	RootElement->LinkEndChild(gestureData);
}

void Kinect::getGlobalandFirstThreshold(std::string gestureName,float* global,float *first)
{
	std::map<std::string,GestureData>::iterator iter;
	iter=gestureMap.find(gestureName);
	if(iter!=gestureMap.end())
	{
		*global=iter->second.computeglobal;
		*first=iter->second.computefirst;
	}
}

void Kinect::setGestureActive(std::string gestureName,bool isActive)
{
	std::map<std::string,GestureData>::iterator iter;
	iter=gestureMap.find(gestureName);
	if(iter!=gestureMap.end())
	{
		iter->second.isActive=isActive;
	}
}