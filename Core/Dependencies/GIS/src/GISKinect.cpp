//General Part for Kinect intialization and unintialization
#include "GISKinect.h"

#define SAFE_RELEASE(p) {if (NULL != p) {(p)->Release(); (p) = NULL;}}

using namespace GIS;

Kinect::~Kinect(){}


void Kinect::_zero()
{
	if(m_pNuiSensor == (INuiSensor*)0xcdcdcdcd||m_pNuiSensor==NULL)
	{
		kinectCount = 1;
		kinectsInUse = 1;
		nearOn = false;
		imgStreamFlags = 0;
	}
	newSkeletonEvent =  NULL;
	m_SkeletonThreadStop = NULL;
	m_SkeletonProcess=NULL;
	hSkeletonEvent=NULL;
	m_hNextVideoFrameEvent=NULL;
	m_hNextDepthFrameEvent=NULL;
	speechInit=false;
	depthInit=false;
	videoInit=false;
	skeletonInit=false;
	displayDepth=false;
	displayVideo=false;
	isSkeletonVisible=false;
	doGestureReco=false;
	skeletonTarget=-1;
}

void CALLBACK StatusProc(HRESULT hrStatus, const OLECHAR* instanceName, const OLECHAR* uDN)
{
	printf("STATUS PROC\n");
}

//Initialize Kinect
void Kinect::_initialize()
{
	_zero();

	if(m_pNuiSensor == (INuiSensor*)0xcdcdcdcd||m_pNuiSensor==NULL)
	{
		initResult = NuiCreateSensorByIndex(0, &m_pNuiSensor1);
		if(FAILED(initResult))
		{
			//throw exception
			return;
		}
		initResult = NuiCreateSensorByIndex(1, &m_pNuiSensor2);
		if(!FAILED(initResult))
		{
			++kinectCount;
		}
		initResult = NuiCreateSensorByIndex(2, &m_pNuiSensor3);
		if(!FAILED(initResult))
		{
			++kinectCount;
		}
		initResult = NuiCreateSensorByIndex(3, &m_pNuiSensor4);
		if(!FAILED(initResult))
		{
			++kinectCount;
		}
	
		m_pNuiSensor = m_pNuiSensor1;
	}

	initResult=m_pNuiSensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX | NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR);
	if(FAILED(initResult))
    {
		//throw exception
		return;
	}//Kinect now initialized
	
	hSkeletonEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	newSkeletonEvent =  CreateEvent(NULL, FALSE, FALSE, NULL);
	m_SkeletonThreadStop = CreateEvent(NULL,FALSE,FALSE,NULL);
	//initialize windows event for displaying skeleton and recognizing gesture
	m_SkeletonProcess=CreateThread(NULL,0,ProcessSkeletonEvent,this,0,NULL);
	initializeSkeletonManual();//create the Manual Object for display skeleton

	initializeCamera();

	
}


//Release resource used by Kinect
void Kinect::_destroy()
{
	//Close working thread
	if(m_SkeletonThreadStop!=NULL)
    {
        // Signal the thread
        SetEvent(m_SkeletonThreadStop);

        // Wait for thread to stop
        if(m_SkeletonProcess!=NULL)
        {
            WaitForSingleObject(m_SkeletonProcess,INFINITE);
            CloseHandle(m_SkeletonProcess);
        }
        CloseHandle(m_SkeletonThreadStop);
    }
	
	m_pNuiSensor->NuiShutdown();

	//depthTex.setNull();
	//Ogre::TextureManager::getSingleton().remove("depthKinect");

	//Close Handle Resource
	if( hSkeletonEvent && ( hSkeletonEvent != INVALID_HANDLE_VALUE ) )
    {
        CloseHandle( hSkeletonEvent );
        hSkeletonEvent = NULL;
    }
    if( m_hNextDepthFrameEvent && ( m_hNextDepthFrameEvent != INVALID_HANDLE_VALUE ) )
    {
        CloseHandle( m_hNextDepthFrameEvent );
        m_hNextDepthFrameEvent = NULL;
    }
    if( m_hNextVideoFrameEvent && ( m_hNextVideoFrameEvent != INVALID_HANDLE_VALUE ) )
    {
        CloseHandle( m_hNextVideoFrameEvent );
        m_hNextVideoFrameEvent = NULL;
    }
	if( newSkeletonEvent && ( newSkeletonEvent != INVALID_HANDLE_VALUE ) )
    {
        CloseHandle( newSkeletonEvent );
        newSkeletonEvent = NULL;
    }

	//Unintitialize COM
	if(speechInit)
	{
		CoUninitialize();
	}
}

INuiSensor* Kinect::getCurrentKinect()
{
	return m_pNuiSensor;
}

void Kinect::toggleNear()
{
	nearOn = !nearOn;
	
	imgStreamFlags = (nearOn ? NUI_IMAGE_STREAM_FLAG_ENABLE_NEAR_MODE : 0);

	m_pNuiSensor->NuiImageStreamSetImageFrameFlags(m_pDepthStreamHandle, imgStreamFlags);
}

bool Kinect::switchKinects(int index)
{
	INuiSensor * newSensor = m_pNuiSensor;
	if(index < 1) return false;
	if(index > kinectCount) return false;
	if(index == 1)
	{
		newSensor = m_pNuiSensor1;
	}
	if(index == 2)
	{
		newSensor = m_pNuiSensor2;
	}
	if(index == 3)
	{
		newSensor = m_pNuiSensor3;
	}
	if(index == 4)
	{
		newSensor = m_pNuiSensor4;
	}
	
	if(newSensor != m_pNuiSensor)
	{
		
		printf("switching to Kinect %d\n", index);
		//switchSkeletonKinect();
		NuiSetDeviceStatusCallback(reinterpret_cast<NuiStatusProc>(&StatusProc), NULL);
		_destroy();
		printf("shutdown\n");
		//initResult = m_pNuiSensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX | NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR);
		Ogre::OverlayManager* overlayManager = Ogre::OverlayManager::getSingletonPtr();
		overlayManager->destroy("KinectVideoOverlay");
		overlayManager->destroy("KinectDepthOverlay");
		overlayManager->destroyOverlayElement("kinectVideoPanel");
		overlayManager->destroyOverlayElement("kinectDepthPanel");
		m_pNuiSensor = newSensor;
		_initialize();
		printf("init\n");
		//initializeSkeletonManual(); //create the Manual Object for display skeleton
		return true;
	}

	//what the hell how did you even get here
	return false;
}

//Update skeleton data, video image and depth image for kinect
void Kinect::capture()
{
	updateSkeleton();
	updateVideo();
	updateDepth();
}

//Set the angle of Kinect Sensor
void Kinect::setSensorAngle(float angle)
{
	KINECT_NOT_INIT(initResult)
	if(angle<-28)
		angle=-28;
	if(angle>28)
		angle=28;
	m_pNuiSensor->NuiCameraElevationSetAngle((LONG)angle);
}