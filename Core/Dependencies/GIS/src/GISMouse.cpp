#include "GISMouse.h"

using namespace GIS;

Axis convertToGAxis(OIS::Axis oA){
	Axis gA;
	gA.abs = oA.abs;
	gA.absOnly = oA.absOnly;
	gA.cType = (GISComponentType)oA.cType;
	gA.rel = oA.rel;
	return gA;
}

MouseState convertToGMouseState(OIS::MouseState oMS){
	MouseState gMS;
	gMS.buttons = oMS.buttons;
	gMS.height = oMS.height;
	gMS.width = oMS.width;
	gMS.X = convertToGAxis(oMS.X);
	gMS.Y = convertToGAxis(oMS.Y);
	gMS.Z = convertToGAxis(oMS.Z);
	return gMS;
}


//Destructor 
Mouse::~Mouse(){
	if (m_mouse != NULL){ 
		delete m_mouse;
	}
}

void Mouse::_initialize(){
	m_mouse = static_cast<OIS::Mouse*> (mCreator->createOISObject( mType, mBuffered ));
	m_mouse->setEventCallback(this);
}

// Interface functions for communicating between 
// the program implementing GIS and the OIS Object
void Mouse::capture(){
	m_mouse->capture();
}

// Inherited functions: MouseListener
bool Mouse::mouseMoved( const OIS::MouseEvent &arg ){
	mState = convertToGMouseState(arg.state);
	return mListener->mouseMoved(MouseEvent(this, mState));
}

bool Mouse::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id ){
	mState = convertToGMouseState(arg.state);
	return mListener->mousePressed(MouseEvent(this, mState), (MouseButtonID) id);
}

bool Mouse::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id ){
	mState = convertToGMouseState(arg.state);
	return mListener->mouseReleased(MouseEvent(this, mState), (MouseButtonID) id);
}



