//#ifdef WRAPPER
#include "GISInputManager.h"
#include "GISKeyboard.h"
#include "GISMouse.h"
#include "GISWiiRemote.h"
#include "GIS360.h"
#include "GISKinect.h"
#include "GISObject.h"
#include "GISMoveMe.h"
#include "OIS.h"
#include <iostream>
using namespace GIS;

InputManager::InputManager(){
	// TODO: Do stuff
}

void InputManager::_initialize(ParamList &paramList){
	// OIS-specific param list
	
	ParamList::iterator i = paramList.find("WINDOW");
	if( i != paramList.end() ) {
		OIS::ParamList oisList;
		bool initOIS = false;
		oisList.insert(std::make_pair(i->first, i->second));

		ParamList::iterator i = paramList.begin(), e = paramList.end();
		for( ; i != e; ++i ) 
		{
			if( i->first == "w32_keyboard" || i->first == "w32_mouse" || i->first == "w32_joystick")
			{
				oisList.insert(std::make_pair(i->first, i->second));
				initOIS = true;
			}
		}

		if (initOIS){
			try{
			m_pInputManager = OIS::InputManager::createInputSystem(oisList);
			}
			catch(OIS::Exception &e){
				std::cout << e.eText << std::endl;
			}
		}
	}
	
	// TODO: Create input systems that are GIS-exclusive.
}

InputManager* InputManager::createInputSystem( ParamList &paramList )
{
	InputManager* im = new InputManager();
	try{
		im->_initialize(paramList);
	}
	catch(...){
		delete im;
		std::cout << "Couldn't initialize IM." << std::endl;
		throw "GISInputManager.cpp: Could not initialize.\n";
	}
	return im;
}

void InputManager::destroyInputSystem(InputManager* manager)
{
	if (manager == NULL)
		return;
	OIS::InputManager::destroyInputSystem(manager->m_pInputManager);
	// TODO: GIS-specific cleanup here!
	delete manager;
}

	
const std::string& InputManager::inputSystemName(){
	return m_pInputManager->inputSystemName();
}

int InputManager::getNumberOfDevices( Type iType )
{
	if (iType <= 4){ // If the Type is one of OIS's initially supported types...
		return m_pInputManager->getNumberOfDevices((OIS::Type)iType); // OIS call
	}
	else{
		switch(iType)
		{
		case GISWiiRemote:
		case GISXPad:
		case GISKinect:
		case GISMoveMe:
			return 1;
		}
	}
	return 0;
}

DeviceList dlConverter(OIS::DeviceList oisList){
	DeviceList gisList;
	OIS::DeviceList::iterator i = oisList.begin(), e = oisList.end();
	for( ; i != e; ++i ) {
		gisList.insert(std::make_pair((Type)i->first, i->second));
	}
	return gisList;
}

DeviceList InputManager::listFreeDevices(){
	// Retrieve m_pInputManager's device list, then add any additional devices to the list.
	DeviceList list = dlConverter(m_pInputManager->listFreeDevices());
	// TODO: GIS-specific free devices
	return list;
}

Object* InputManager::createInputObject( Type iType, bool bufferMode, const std::string &vendor)
{
	Object* obj = NULL;
	switch (iType){
	case GISKeyboard:
		// TODO: Fill in parameters
		obj = new Keyboard(this, bufferMode);
		obj->_initialize();
		break;

	case GISMouse:
		obj = new Mouse(this, bufferMode);
		obj->_initialize();
		break;
	case GISWiiRemote:
		obj = new WiiRemote(this, bufferMode);
		obj->_initialize();
		break;
	case GISXPad:
		obj = new XPad(this, bufferMode);
		obj->_initialize();
		break;
	case GISKinect:
		obj=new Kinect(this, bufferMode);
		obj->_initialize();
		break;
	case GISMoveMe:
		obj = new MoveMe(this, bufferMode);
		obj->_initialize();
		break;
	default:
		break;
	}

	if (obj == NULL){
		// TODO: Throw an exception here
	}
	return obj;
}

OIS::Object* InputManager::createOISObject( Type iType, bool bufferMode, const std::string &vendor){
	return m_pInputManager->createInputObject((OIS::Type)iType, bufferMode, vendor);
}


void InputManager::destroyInputObject(Object* object)
{
	if (object == NULL || object->getCreator() != this)
		return;
	if(object->type()==GISKinect)
	{
		((Kinect*)object)->_destroy();
	}
	delete object;
}


//#endif