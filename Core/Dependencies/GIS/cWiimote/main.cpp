//cWiimote 0.2 by Kevin Forbes (http://simulatedcomicproduct.com)
//This code is public domain, and comes with no warranty. The user takes full responsibility for anything that happens as a result from using this code.

#include "wiimote.h"
#include <stdio.h>
#include <math.h>
#include <fstream>

using namespace std;

//Features:
// Read Accelerometer, button values from the wiimote
// Read Accelerometer, stick, and button values from the nunchuck
// Preliminary IR support

//Known issues:
// The IR support is spotty at best. It tends to kick out if you plug your 'chuck in and out too many times
// Reading 'chuck calibration data doesn't seem to work, so the code just uses defaults
// Multiple Wiimote support not yet tested
// May only work with Bluesoleil stack?

//Instructions:
// See below for how to connect to a device and start the data stream.
// It is up to the user to call heartbeat fast enough - if you're too slow, you will loose data. Ideally, this would be done in a separate thread
// There are several public functions for getting the values from the wiimote. Look in cWiiMote::PrintStatus for examples.

//Version History:
//0.1 Preliminary Release
//0.2 Added nunchuck, IR support


int main(int nargs, const char * cargs)
{
	cWiiMote wiimote;
	
	int count = 0;
	int hitcount = 0;
	bool hit = false;
	bool zHit = false;
	bool yHit = false;
	bool xHit = false;
	bool zSwitch, ySwitch, xSwitch;

	ofstream output;
	output.open("Output.txt", ios::out);

	if (wiimote.ConnectToDevice() &&
		wiimote.StartDataStream())
	{
		for (;;)
		{
			wiimote.HeartBeat();
			wiimote.PrintStatus();
			count ++;
			
			printf("  %d ", count);

			float x, y, z;

			wiimote.GetCalibratedAcceleration(x, y, z);

			output << "W: " << x << "\t" << y << "\t" << z;

			if (!hit && count > 10)
			{
				//Check for hit start
				if (z < -1.0)
				{
					zHit = true;
					hit = true;
					zSwitch = false;

					output << "\tHit Z Down";
					printf("\tHit Z Down", count);
				}
				/*
				else if (z > 4.0)
				{
					zHit = true;
					hit = true;
					zSwitch = true;

					output << "\tHit Z Up";
				}
				else if (y < -1.5)
				{
					yHit = true;
					hit = true;
					ySwitch = false;
					output << "\tHit Y Down";
				}
				else if (y > 1.5)
				{
					yHit = true;
					hit = true;
					ySwitch = true;
					output << "\tHit Y Up";
				}
				else if (x < -1.5)
				{
					xHit = true;
					hit = true;
					xSwitch = false;

					output << "\tHit X Down";
				}
				else if (x > 1.5)
				{
					xHit = true;
					hit = true;
					xSwitch = true;

					output << "\tHit X Up";
				}
				*/
			}
			else
			{
				//Check for hit end
				if (zHit)
				{
					if (zSwitch)
					{
						if (z < -4.0)
						{
							hit = false;
							zHit = false;
							output << "\tHit Z Done";
							printf("\tHit Z Done", count);
						}
					}
					else
					{
						if (z > 1.0)
						{
							hit = false;
							zHit = false;

							output << "\tHit Z Done";
							printf("\tHit Z Done", count);
						}
					}
				}
				else if (yHit)
				{
					if (ySwitch)
					{
						if (y < 1.0)
						{
							hit = false;
							yHit = false;

							output << "\tHit Y Done";
						}
					}
					else
					{
						if (y > -1.0)
						{
							hit = false;
							yHit = false;

							output << "\tHit Y Done";
						}
					}
				}
				else if (xHit)
				{
					if (xSwitch)
					{
						if (x < 1.0)
						{
							hit = false;
							xHit = false;

							output << "\tHit X Done";
						}
					}
					else
					{
						if (x > -1.0)
						{
							hit = false;
							xHit = false;

							output << "\tHit X Done";
						}
					}
				}
			}

			output << "\n";
			printf (" \n");

			/*
			if (sqrt (x*x + y*y + z*z) - 1 > 1)
			{
				if (!hit)
				{
					hit = true;
					hitcount = 0;
					printf("HIT Started! \n");
				}
				else
				{
					printf("  HIT! \n");
				}

				output << "\tHIT!";
			}
			else if (hit)
			{
				if (hitcount > 5)
				{
					hit = false;
					hitcount = 0;

					printf("Hit Ended! \n");
				}
				else
				{
					hitcount ++;
					printf ("  Counting to End\n");
				}
			}
			else
			{
				printf (" \n");
			}
			*/

			

			if (wiimote.GetLastButtonStatus().mA)
				wiimote.SetVibration(true);

			if (wiimote.GetLastButtonStatus().mB)
				wiimote.SetVibration(false);

			if (wiimote.GetLastButtonStatus().m1)
				wiimote.SetLEDs(true, false, true, false);

			if (wiimote.GetLastButtonStatus().m2)
				wiimote.SetLEDs(false, true, false, true);

			if (wiimote.GetLastButtonStatus().mA && wiimote.GetLastButtonStatus().mB)
				break;
		}
	}

	output.close();

	wiimote.StopDataStream();
	wiimote.Disconnect();

	return 0;
}