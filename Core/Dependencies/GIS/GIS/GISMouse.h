
namespace GIS
{

	//! Button ID for mouse devices
	// Uses the same values as OIS for simpler integration.
	enum MouseButtonID
	{
		MB_Left = 0, MB_Right, MB_Middle,
		MB_Button3, MB_Button4,	MB_Button5, MB_Button6,	MB_Button7
	};

}