#ifndef GIS360_H
#define GIS360_H

// External Includes
#include <windows.h>
#include <XInput.h>

// Internal Includes
#include "GISObject.h"
#include "GISEvents.h"
#include "GISInputManager.h"

namespace GIS
{
	static const int MAX_BUTTONS = 16;

	//! Button ID for Xbox 360 controllers
	enum XPadButtonID
	{
		XP_UP        = XINPUT_GAMEPAD_DPAD_UP,          // 0x0001
		XP_DOWN      = XINPUT_GAMEPAD_DPAD_DOWN,        // 0x0002
		XP_LEFT      = XINPUT_GAMEPAD_DPAD_LEFT,        // 0x0004
		XP_RIGHT     = XINPUT_GAMEPAD_DPAD_RIGHT,       // 0x0008
		XP_START     = XINPUT_GAMEPAD_START,            // 0x0010
		XP_BACK      = XINPUT_GAMEPAD_BACK,             // 0x0020
		XP_LTHUMB    = XINPUT_GAMEPAD_LEFT_THUMB,       // 0x0040
		XP_RTHUMB    = XINPUT_GAMEPAD_RIGHT_THUMB,      // 0x0080
		XP_LSHOULDER = XINPUT_GAMEPAD_LEFT_SHOULDER,    // 0x0100
		XP_RSHOULDER = XINPUT_GAMEPAD_RIGHT_SHOULDER,   // 0x0200
		XP_A         = XINPUT_GAMEPAD_A,                // 0x1000
		XP_B         = XINPUT_GAMEPAD_B,                // 0x2000
		XP_X         = XINPUT_GAMEPAD_X,                // 0x4000
		XP_Y         = XINPUT_GAMEPAD_Y                 // 0x8000
	};

	/* CLASS XPadState */
	/**
		Represents the state of the Xbox 360 controller
		All members are valid for both buffered and non buffered mode
	*/
	class _GISExport XPadState{
	public:

		//! Represents all buttons - bit position indicates button down
		int buttons;

		//! Represents the displacement of left trigger. Takes values in the range of 0 to 255.
		unsigned char XP_lTrigger;

		//! Represents the displacement of right trigger. Takes values in the range of 0 to 255.
		unsigned char XP_rTrigger;

		//! Represents X component of left thumb stick. Takes values in the range of -32768 to 32767.
		short XP_lThumbX;

		//! Represents Y component of left thumb stick. Takes values in the range of -32768 to 32767.
		short XP_lThumbY;

		//! Represents X component of right thumb stick. Takes values in the range of -32768 to 32767.
		short XP_rThumbX;

		//! Represents Y component of right thumb stick. Takes values in the range of -32768 to 32767.
		short XP_rThumbY;

		//! Button down test
		inline bool xpButtonsDown(int bID) const{
			return ((buttons & bID) == bID);
		}

		//! Button up test
		inline bool xpButtonsUp(int bID) const{
			return ((buttons | ~bID) == ~bID);
		}

		//! Clear all the values
		void clear(){
			buttons = 0;
			XP_lTrigger = XP_rTrigger = 0;
			XP_lThumbX = XP_lThumbY = XP_rThumbX = XP_rThumbY = 0;
		}
	};

	/* CLASS XPadEvent */
	/** Specialised for Xbox 360 controller events */
	class _GISExport XPadEvent : public EventArg
	{
	public:
		XPadEvent( Object* obj, const XPadState &xps): EventArg(obj), state(xps) {}
		virtual ~XPadEvent() {};

		//! The state of the Xbox 360 controller - including buttons, triggers and thumb sticks
	    const XPadState &state;
	
	};

	/* CLASS XPadListener */
	/**
		To receive buffered Xbox 360 controller input, derive a class from this, and implement the
		methods here. Then set the call back to your XPad instance with XPad::setEventCallback
	*/
	class _GISExport XPadListener
	{
	public:
		virtual ~XPadListener() {};
		virtual bool buttonPressed( const XPadEvent &xpEvent, int button) =0;
		virtual bool buttonReleased( const XPadEvent &xpEvent, int button) =0;
		virtual bool lTriggerPressed( const XPadEvent &xpEvent ) =0;
		virtual bool rTriggerPressed( const XPadEvent &xpEvent ) =0;
		virtual bool lThumbStickMoved( const XPadEvent &xpEvent ) =0;
		virtual bool rThumbStickMoved( const XPadEvent &xpEvent ) =0;
	};

	/* CLASS XPad */
	/**
		Xbox 360 controller base class.
	*/
	class _GISExport XPad : public Object
	{
	public:
		XPad(InputManager *manager = NULL, bool buffered = true) :
			Object(GISXPad, buffered, manager),
			isConnected(false),
			mControllerNumber(0),
			mListener(NULL) 
			{}
		~XPad();
		
		void setPortNumber(int num) { if (num >=0 && num <= 3) mControllerNumber = num; }

		/** @remarks Returns the state of the Xbox 360 controller - is valid for both buffered and non buffered mode */
		const XPadState& getXPadState() const { return mState; }

		/** @remarks Returns currently set callback.. or 0 */
		void setEventCallback( XPadListener *padListener ) { mListener = padListener;}
		
		/** @remarks Returns the state of the Xbox 360 controller - is valid for both buffered and non buffered mode */
		XPadListener* getEventCallback() { return mListener; }
		

		// Inherited Functions:
		void setBuffered(bool buffered){ mBuffered = buffered; }
		void capture();
		void _initialize();
		// Type type() const { return mType; }
		// virtual bool buffered() const { return mBuffered; }
		//InputManager* getCreator() { return mCreator; }
		////virtual Interface* queryInterface(Interface::IType type) = 0;

	protected:

		//! True if Xbox 360 controller is connected, false otherwise
		bool isConnected;

		//! Used to represent the controller number
		int mControllerNumber;

		//! The state of the Xbox 360 controller
		XPadState mState;

		//! Used for buffered/actionmapping callback
		XPadListener* mListener;

		/* Inherited values:
		   Type mType;             - mType is "fully implemented", so to speak
		   bool mBuffered          - mBuffered is doing nothing
		   InputManager* mCreator  - mCreator is currently commented out
		                             Needs to be implemented at some point.
		*/
	};


}

#endif