#ifndef GISInterface_H
#define GISInterface_H

#include "GISPrereqs.h"

namespace GIS
{
	/**
		An Object's interface is a way to gain write access to devices which support
		it. For example, force feedack.
	*/
	class _GISExport Interface
	{
	public:
		virtual ~Interface() {};

		//! Type of Interface
		enum IType
		{
			ForceFeedback,
			Reserved
		};
	};
}
#endif