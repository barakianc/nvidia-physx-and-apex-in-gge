
//Basic header files
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <atlbase.h>

// For configuring DMO properties
#include <wmcodecdsp.h>

// For discovering microphone array device
#include <MMDeviceApi.h>
#include <devicetopology.h>
#include <functiondiscoverykeys_devpkey.h>

// For functions and definitions used to create output file
#include <dmo.h> // Mo*MediaType
#include <uuids.h> // FORMAT_WaveFormatEx and such
#include <mfapi.h> // FCC

// For string input,output and manipulation
#include <tchar.h>
#include <strsafe.h>
#include <conio.h>

// For CLSID_CMSRKinectAudio GUID
#include "NuiApi.h"

#define KINECTAUDIOSTREAM_SAFE_RELEASE(p) {if (NULL != p) {(p)->Release(); (p) = NULL;}}

class KinectAudioBuffer : public IMediaBuffer 
{
public:
	KinectAudioBuffer() {}
	KinectAudioBuffer(BYTE *pData, ULONG ulSize, ULONG ulData) :
		m_pData(pData), m_ulSize(ulSize), m_ulData(ulData), m_cRef(1) {}
		
	//IUnKnown Interface
public:

	STDMETHODIMP_(ULONG) AddRef() { return 2; }
	STDMETHODIMP_(ULONG) Release() { return 1; }
	STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
		if (riid == IID_IUnknown) {
			AddRef();
			*ppv = (IUnknown*)this;
			return NOERROR;
		}
		else if (riid == IID_IMediaBuffer) {
			AddRef();
			*ppv = (IMediaBuffer*)this;
			return NOERROR;
		}
		else
			return E_NOINTERFACE;
	}

	//IMediaBuffer interface
	STDMETHODIMP SetLength(DWORD ulLength) {m_ulData = ulLength; return NOERROR;}
	STDMETHODIMP GetMaxLength(DWORD *pcbMaxLength) {*pcbMaxLength = m_ulSize; return NOERROR;}
	STDMETHODIMP GetBufferAndLength(BYTE **ppBuffer, DWORD *pcbLength) {
		if (ppBuffer) *ppBuffer = m_pData;
		if (pcbLength) *pcbLength = m_ulData;
		return NOERROR;
	}


	void Init(BYTE *pData, ULONG ulSize, ULONG ulData) {
		m_pData = pData;
		m_ulSize = ulSize;
		m_ulData = ulData;
	}

	void Restart()
	{
		m_ulData=0;
	}

public:
	BYTE *m_pData;//Pointer of the buffer
	ULONG m_ulSize;//Size of the buffer
	ULONG m_ulData;//Points to the position next to last valid byte in buffer
	ULONG m_cRef;
};

/**	@remarks KinectAudioStream is the input audio stream used by sppech recognition engine*/
class KinectAudioStream: public IStream
{
private:
	LONG _refcount;
	ULONG _pWrite;//_pWrite points to position after the last valid byte in the buffer
	ULONG _pRead;//_pRead points to the starting position of valid byte in the buffer
	ULONG _pEnd;//_pEnd points to the position of last valid byte before new data is stored at the beginning of the buffer
	bool _isAhead;//This variable is true if the buffer is "full". Then new data will be stored at the beginning of the buffer

	IMediaObject* _pDMO;
	IPropertyStore* _pPS;
	HANDLE _mmHandle;
	INuiAudioBeam* _pSC;
	KinectAudioBuffer* _outputBuffer;
	DMO_OUTPUT_DATA_BUFFER _OutputBufferStruct;
	BYTE* _pBuffer;//Real buffer

	DWORD _BufLen;//Buffer length

	HRESULT hr;


public:
	KinectAudioStream();
	~KinectAudioStream();

	//IUnKnown Interface
public :
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void ** ppvObject);
	virtual ULONG STDMETHODCALLTYPE AddRef(void);
	virtual ULONG STDMETHODCALLTYPE Release(void);

	//ISequentialStream Interface
public:
	/**	@remarks Speech engine use this function to read input data*/
	virtual HRESULT STDMETHODCALLTYPE Read(void* pv, ULONG cb, ULONG* pcbRead);//Read audio stream from Kinect DMO
	virtual HRESULT STDMETHODCALLTYPE Write(void const* pv, ULONG cb, ULONG* pcbWritten){return E_NOTIMPL; }//Not implemented

	// IStream Interface
public:
	virtual HRESULT STDMETHODCALLTYPE SetSize(ULARGE_INTEGER){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE CopyTo(IStream*, ULARGE_INTEGER, ULARGE_INTEGER*,ULARGE_INTEGER*){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE Commit(DWORD){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE Revert(void){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE LockRegion(ULARGE_INTEGER, ULARGE_INTEGER, DWORD){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE UnlockRegion(ULARGE_INTEGER, ULARGE_INTEGER, DWORD){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE Clone(IStream **){return E_NOTIMPL; }//Not implemented
	virtual HRESULT STDMETHODCALLTYPE Seek(LARGE_INTEGER liDistanceToMove, DWORD dwOrigin,ULARGE_INTEGER* lpNewFilePointer);
	virtual HRESULT STDMETHODCALLTYPE Stat(STATSTG* pStatstg, DWORD grfStatFlag){return E_NOTIMPL; }//Not implemented

public:
	/**	@remarks Initialize audio stream*/
	bool initAudioStream();

private:
	bool _isDataEnough(ULONG cb);//To see if we need fetch more data from Kinect audio stream into local buffer

	//Functions help to get the index of audio device
	HRESULT GetJackSubtypeForEndpoint(IMMDevice* pEndpoint, GUID* pgSubtype);
	HRESULT GetMicArrayDeviceIndex(int *piDevice);
};
