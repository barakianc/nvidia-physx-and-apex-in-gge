#ifndef GISMoveMe_H
#define GISMoveMe_H


#include "GISObject.h"
#include "GISEvents.h"
#include "GISInputManager.h"
#include <windows.h>
#include "moveclient.h"
#include <vector>


#pragma comment(lib, "Ws2_32.lib")


/*
ReadMe:

1) There are currently action listeners for all button and trigger presses.  If for some reason you want more than that, just let me know.  Input.cpp in our Game class is what listens for it.  Input is a singleton, so you can access it from anywhere in the class.  If you want to figure out how to pass down input from action listeners just look at how our current input.cpp does it from keyPressed.  The listeners in Input.cpp that are called are:

bool Input::buttonPressed(const GIS::MoveMeEvent &mmEvent, GIS::MoveMeButtonID button){
		return true;
	}

	bool Input::buttonReleased(const GIS::MoveMeEvent &mmEvent, GIS::MoveMeButtonID button){
		return true;
	}

2)  For pull data, access the input singleton and call GetMoveMe()->getMoveStateForController(int c) 
default case c is zero.  That will return a struct to you that looks like this

typedef struct _MoveState {
	float4 pos;
	float4 vel;
	float4 accel;
	float4 quat;
	float4 angvel;
	float4 angaccel;
	float4 handle_pos;
	float4 handle_vel;
	float4 handle_accel;
	MovePadData pad;
	system_time_t timestamp;
	float temperature;
	float camera_pitch_angle;
	uint32_t tracking_flags;

} MoveState, *LPMoveState;

you shouldn't need to access more than pos and quat


For pulling the nav pad  analog call GetMoveMe()->getMoveServerPacket().pad_data[0].button[ value]

to get up/down value is 7 (up = 0, down = 255)

to get left right value is 6 (left = 0, right = 255)

3)  Navpad data is currently hardcoded so to get the navigation controller to work, you have to set it to the controller 1 slot.  Sorry, couldn't think of a better way :/ (cause if you do it differently, data from the motion controller goes into the nav pad controller data)

4)  When you restart the Playstation 3 Moveme program the server address changes.  You have to enter it into input.cpp on line 125 to connect successfully

5)  To access the input singleton use (GamePipe::Input::GetInstancePointer())


*/

namespace GIS{
	
	const int bSelectOffset= 0;
	const int bL2Offset = 0;
	const int bTriggerOffset = 1;
	const int bL1Offset = 2;
	const int bSquiggleOffest = 2;
	const int bStartOffset = 3;
	const int bTriangleOffset = 4;
	const int bCircleOffset = 5;
	const int bCrossOffset = 6;
	const int bSquareOffset = 7;

	const int bUpOffset = 4;
	const int bRightOffset = 5;
	const int bDownOffest = 6;
	const int bLeftOffset = 7;

	
	enum MoveMeButtonID{ //no idea what the actual values are
		MM_SQR		= 1,
		MM_X		= 2,
		MM_TRI		= 3,
		MM_O		= 4,
		MM_SEL		= 5,
		MM_START	= 6,
		MM_SQUIGGLE	= 7,
		MM_TRIGGER	= 8,
		MM_N		= 9,
		MM_NAV_X	= 10,
		MM_NAV_O	= 11,
		MM_NAV_UP   = 12,
		MM_NAV_DOWN = 13,
		MM_NAV_RIGHT = 14,
		MM_NAV_LEFT	 = 15,
		MM_NAV_L1	 = 16,
		MM_NAV_L2	 = 17,
		MM_NAV_PS	 = 18,

	};

	struct Vector4{
		float x;
		float y;
		float z;
		float w;
	};

	class _GISExport MoveMeEvent: public EventArg
	{
	public:
		MoveMeEvent( Object* obj, const MoveServerPacket &mms): EventArg(obj), state(mms) {}
		virtual ~MoveMeEvent() {};

		const MoveServerPacket &state;
	};


	class _GISExport MoveMeListener
	{
	public:
		virtual ~MoveMeListener() {}
		virtual bool buttonPressed(const MoveMeEvent &mmEvent, MoveMeButtonID button) = 0;
		virtual bool buttonReleased(const MoveMeEvent &mmEvent, MoveMeButtonID button) = 0;

		//add in an analog stick moved?

		//add in a Position moved?

	};

	

	class _GISExport MoveMe : public Object {
				
	protected:
		MoveMeListener* mmListener;

	public:
		MoveMe(InputManager *manager = NULL, bool buffered = true) :
			Object(GISKinect, buffered, manager),
				mmListener(NULL){}
		~MoveMe();

		//initialize and setup MoveMe
		void moveMeConnect(PCSTR lpRemoteAddress, PCSTR lpPort);
		void moveMeDisconnect();

		//used to compare current state to old state and call necessary callbacks to listener
		void capture();
		void setBuffered(bool buffered);
		void _initialize();

		bool isButtonDown(MoveMeButtonID button);//TODO:  add enum and put it as input in parantheses

		//input for Move controller
		const MoveServerPacket getMoveServerPacket();
		const MoveState getMoveStateForController(int c);
		const Vector4 getPositionForController(int c);
		const Vector4 getVelocityForController(int c);
		const Vector4 getHandlePositionForController(int c);
		const Vector4 getHandleVelocityForController(int c);
		const Vector4 getQuaternionForController(int c);

		/*
		@remarks
			Register/unregister a Mouse Listener - Only one allowed for simplicity. If broadcasting
			is neccessary, just broadcast from the callback you registered.
		@param mouseListener
			Send a pointer to a class derived from MouseListener or 0 to clear the callback
		*/
		virtual void setEventCallback( GIS::MoveMeListener *moveMeListener ) {mmListener = moveMeListener;}


		const MovePadData getMovePadForController(int c);



	private:
		MoveClient* moveClient;
		mutable MoveServerPacket serverData;
		MoveServerPacket lastCaptureServerData;
		bool connected;

	};

	
}

#endif