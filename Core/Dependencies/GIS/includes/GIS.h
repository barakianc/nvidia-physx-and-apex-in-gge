#ifndef GIS_H
#define GIS_H

#include "GISPrereqs.h"
#include "GISObject.h"
#include "GISInputManager.h"
#include "GISKeyboard.h"
#include "GISMouse.h"
#include "GISWiiRemote.h"
#include "GIS360.h"
#include "GISEvents.h"
#include "GISKinect.h"
#include "GISMoveMe.h"
#include "moveclient.h"

#endif