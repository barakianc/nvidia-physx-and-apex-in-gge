#ifndef GISPrereqs_H
#define GISPrereqs_H

#include <vector>
#include <string>
#include <map>

#if defined(GIS_EXPORTS)
#	undef _GISExport
#   define _GISExport __declspec(dllexport)
#else
#   define _GISExport __declspec(dllimport)
#endif

namespace GIS
{
	/* FORWARD DECLARATIONS ARE GO */
	class Object;
	class InputManager;
	class EventArg;
	class Keyboard;
	class Mouse;
	class WiiRemote;
	class XPad;
	class KeyListener;
	class MouseListener;
	class WiiRemoteListener;
	class XPadListener;
	class Kinect;

    // OIS left in for easier backwards compatibility
#ifdef OIS_COMPATIBILITY
    enum OISType
	{
		OISUnknown   = 0,
		OISKeyboard  = 1,
		OISMouse     = 2,
		OISJoyStick  = 3,
		OISTablet    = 4
	};

	enum OISComponentType
	{
		OIS_Unknown = 0,
		OIS_Button  = 1, //ie. Key, mouse button, joy button, etc
		OIS_Axis    = 2, //ie. A joystick or mouse axis
		OIS_Slider  = 3, //
		OIS_POV     = 4, //ie. Arrow direction keys
		OIS_Vector3 = 5  //ie. WiiMote orientation
	};
#endif

	enum Type{
		GISUnknown     = 0,
		GISKeyboard    = 1,
		GISMouse       = 2,
		GISJoyStick    = 3,
		GISTablet      = 4,
		GISWiiRemote   = 5,
		GISXPad        = 6,
		GISKinect	   = 7
	};

	//--------     Shared common components    ------------------------//

	// Base type for all device components (button, axis, etc)
    
	/* ENUM */
	enum GISComponentType
	{
		GIS_Unknown     = 0,
		GIS_Button      = 1, //ie. Key, mouse button, joy button, etc
		GIS_Axis        = 2, //ie. A joystick or mouse axis
		GIS_Slider      = 3, //
		GIS_POV         = 4, //ie. Arrow direction keys
		GIS_Vector3     = 5, //ie. WiiMote orientation
		GIS_AnalogStick = 6  // ie. an analog stick
	};


	/* TYPEDEF */
	//! Map of device objects connected and their respective vendors
	typedef std::multimap<Type, std::string> DeviceList;

	//! Way to send OS nuetral parameters.. ie OS Window handles, modes, flags
	typedef std::multimap<std::string, std::string> ParamList;

	// List of FactoryCreator's
	// NOT USING THESE: typedef std::vector<FactoryCreator*> FactoryList;

	// Map of FactoryCreator created Objects
	// CHANGE FC to IM? : typedef std::map<Object*, FactoryCreator*> FactoryCreatedObject;

	/* CLASSES */
	//! Base of all device components (button, axis, etc)
	class _GISExport Component
	{
	public:
		Component() : cType(GIS_Unknown) {};
		Component(GISComponentType type) : cType(type) {};
		//! Indicates what type of coponent this is
		GISComponentType cType;
	};

	//! Button can be a keyboard key, mouse button, etc
	class _GISExport Button : public Component
	{
	public:
		Button() {}
		Button(bool bPushed) : Component(GIS_Button), pushed(bPushed) {};
		//! true if pushed, false otherwise
		bool pushed;
	};

	//! Axis component
	class _GISExport Axis : public Component
	{
	public:
		Axis() : Component(GIS_Axis), abs(0), rel(0), absOnly(false) {};

		//! Absoulte and Relative value components
		int abs, rel;

		//! Indicates if this Axis only supports Absoulte (ie JoyStick)
		bool absOnly;

		//! Used internally by GIS
		void clear()
		{
			abs = rel = 0;
		}
	};

	//! Analog stick component, used for analog input.
	class _GISExport AnalogStick : public Component
	{
	public:
		//! X component of analog stick
		float x;

		//! Y component of analog stick
		float y;

		AnalogStick() {}
		AnalogStick(float _x, float _y) : Component(GIS_AnalogStick), x(_x), y(_y){};

		void clear()
		{
			x = y = 0.0f;
		}
	};

	typedef AnalogStick Vector2;

	//! A 3D Vector component (perhaps an orientation, as in the WiiMote)
	class _GISExport Vector3 : public Component
	{
	public:
		Vector3() {}
		Vector3(float _x, float _y, float _z) : Component(GIS_Vector3), x(_x), y(_y), z(_z) {};
		
		//! X component of vector
		float x;
		
		//! Y component of vector
		float y;

		//! Z component of vector
		float z;

		void clear()
		{
			x = y = z = 0.0f;
		}
	};

}
#endif