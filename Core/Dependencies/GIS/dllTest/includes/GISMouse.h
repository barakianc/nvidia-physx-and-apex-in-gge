#ifndef GISMouse_H
#define GISMouse_H

#include "GISObject.h"
#include "GISEvents.h"
#include "GISInputManager.h"
#include "OIS.h"

namespace GIS
{

	//! Button ID for mouse devices
	// Uses the same values as OIS for simpler integration.
	enum MouseButtonID
	{
		MB_Left = 0, MB_Right, MB_Middle,
		MB_Button3, MB_Button4,	MB_Button5, MB_Button6,	MB_Button7
	};

	/**
		Represents the state of the mouse
		All members are valid for both buffered and non buffered mode
	*/
	class _GISExport MouseState{
	public:
		MouseState() : width(50), height(50), buttons(0) {};

		/** Represents the height/width of your display area.. used if mouse clipping
		or mouse grabbed in case of X11 - defaults to 50.. Make sure to set this
		and change when your size changes.. */
		mutable int width, height;

		//! X Axis component
		Axis X;

		//! Y Axis Component
		Axis Y;

		//! Z Axis Component
		Axis Z;

		//! Represents all buttons - bit position indicates button down
		int buttons;

		//! Button down test
		inline bool buttonDown( MouseButtonID button ) const
		{
			return ((buttons & ( 1L << button )) == 0) ? false : true;
		}

		//! Clear all the values
		void clear()
		{
			X.clear();
			Y.clear();
			Z.clear();
			buttons = 0;
		}
	};

	/** Specialised for mouse events */
	class _GISExport MouseEvent : public EventArg{
	public:
		MouseEvent( Object *obj, const MouseState &ms )	: EventArg(obj), state(ms) {}
		virtual ~MouseEvent() {}

		//! The state of the mouse - including buttons and axes
		const MouseState &state;
	};

	/**
		To receive buffered mouse input, derive a class from this, and implement the
		methods here. Then set the call back to your Mouse instance with Mouse::setEventCallback
	*/
	class _GISExport MouseListener
	{
	public:
		virtual ~MouseListener() {}
		virtual bool mouseMoved( const MouseEvent &arg ) = 0;
		virtual bool mousePressed( const MouseEvent &arg, MouseButtonID id ) = 0;
		virtual bool mouseReleased( const MouseEvent &arg, MouseButtonID id ) = 0;
	};

	/**
		Mouse base class.
	*/
	class _GISExport Mouse : public Object, public OIS::MouseListener{
	public:
		/* FUNCTIONS */
		Mouse(InputManager* creator = NULL, bool buffered = true): 
		  Object(GISMouse, buffered, creator), mListener(NULL) {}
		~Mouse();

		/**
		@remarks
			Register/unregister a Mouse Listener - Only one allowed for simplicity. If broadcasting
			is neccessary, just broadcast from the callback you registered.
		@param mouseListener
			Send a pointer to a class derived from MouseListener or 0 to clear the callback
		*/
		virtual void setEventCallback( GIS::MouseListener *mouseListener ) {mListener = mouseListener;}
		
		/** @remarks Returns currently set callback.. or 0 */
		GIS::MouseListener* getEventCallback() { return mListener; }
		
		/** @remarks Returns the state of the mouse - is valid for both buffered and non buffered mode */
		const OIS::MouseState& getMouseState() const { return m_mouse->getMouseState(); }

		// Inherited Functions: Object
		void setBuffered(bool buffered) { mBuffered = buffered; } 
		void capture();
		void _initialize();
		// Type type() const { return mType; }
		// virtual bool buffered() const { return mBuffered; }
		//InputManager* getCreator() { return mCreator; }
		////virtual Interface* queryInterface(Interface::IType type) = 0;

		// Inherited Functions: MouseListener
		virtual bool mouseMoved( const OIS::MouseEvent &arg );
		virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
		virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

	protected:
		/* VARIABLES */
		
		//! Pointer to OIS::Mouse
		OIS::Mouse* m_mouse;

		//! Used for buffered/actionmapping callback
		GIS::MouseListener *mListener;

		//! The state of the mouse
		MouseState mState;
		// Inherited variables: Object
		/*
		Type mType;
		bool mBuffered;
		InputManager* mCreator;
		*/
	};
}
#endif