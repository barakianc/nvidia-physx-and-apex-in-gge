#ifndef GISObject_H
#define GISObject_H

#include <string>
#include "GISPrereqs.h"
#include "GISInterface.h"

namespace GIS
{

	/**	The base class of all input types. */
	/*
		PROTECTED DATA 
		//std::string mVendor;
		//int mDevID;
		Type mType;
		bool mBuffered;
		InputManager* mCreator;

	*/
	class _GISExport Object
	{
	public:
		virtual ~Object() {}

		/**	@remarks Get the type of device	*/
		Type type() const { return mType; }

		/* 
			GIS NOTE: We don't really care about this stuff right now.
					If it becomes important in the future, integration should be
					simple enough.
		*/
		/*	@remarks Get the vender string name	*/
		//const std::string& vendor() const { return mVendor; }
		/*	@remarks This may/may not) differentiate the different controllers based on (for instance) a port number (useful for console InputManagers) */
		//virtual int getID() const {return mDevID;}
		/*
		@remarks
			If available, get an interface to write to some devices.
			Examples include, turning on and off LEDs, ForceFeedback, etc
		@param type
			The type of interface you are looking for
		*/

		//virtual Interface* queryInterface(Interface::IType type) = 0;

		/**	@remarks Get buffered mode - true is buffered, false otherwise */
		virtual bool buffered() const { return mBuffered; }

		/** @remarks Returns this input object's creator */
		InputManager* getCreator() { return mCreator; }

		/** @remarks Sets buffered mode	*/
		virtual void setBuffered(bool buffered) = 0;

		/**	@remarks Used for updating call once per frame before checking state or to update events */
		virtual void capture() = 0;

		/**	@remarks Internal... Do not call this directly. */
		virtual void _initialize() = 0;

	protected:
		/* 
			GIS Note: If you want to re-implement mVendor or mDevID, the OIS
			Object constructor looked like this: 
			(const std::string &vendor, Type iType, bool buffered,
			   int devID, InputManager* creator) 
		*/
		Object(Type iType, bool buffered, InputManager* creator) :
					//mVendor(vendor),
					mType(iType),
					mBuffered(buffered),
					//mDevID(devID)
					mCreator(creator) 
					{}

		//! Type of controller object
		Type mType;

		//! Buffered flag
		bool mBuffered;

		//! The creator who created this object
		InputManager* mCreator;
	};

}
#endif