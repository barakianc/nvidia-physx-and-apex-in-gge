#ifndef GISKinect_H
#define GISKinect_H

#include <windows.h>

//SAPI Header files


#include <sapi.h>
//#include <stdio.h>
#include <atlbase.h>
//#include <sphelper.h>

// External Includes
#include <ole2.h>
#include "nuiapi.h"
#include <Ogre.h>

// Internal Includes
#include "GISObject.h"
#include "GISEvents.h"
#include "GISInputManager.h"
#include "KinectGesture.h"

//XML
#include "tinyxml.h"
#include "tinystr.h"

#define KINECT_NOT_INIT(result) {if (FAILED(result)) {return;}}


namespace GIS
{
	typedef struct tagRGBQUAD 
	{
        BYTE    rgbBlue;
        BYTE    rgbGreen;
        BYTE    rgbRed;
	} RGBQUAD;

	/**	@remarks Resolution options for video and depth image*/
	typedef enum _KINECT_RESOLUTION
	{
	  KINECT_RESOLUTION_INVALID = -1,
	  KINECT_RESOLUTION_80x60 = 0,
	  KINECT_RESOLUTION_320x240,
	  KINECT_RESOLUTION_640x480,
	  KINECT_RESOLUTION_1280x1024                        
	} KINECT_RESOLUTION;

	/**	@remarks Intermediate joints index*/
	typedef enum INTERMEDIATE_JOINT_INDEX
	{
		INT_ROOT=0,
		INT_SHOULDER_LEFT,
		INT_ELBOW_LEFT,
		INT_WRIST_LEFT,
		INT_SHOULDER_RIGHT,
		INT_ELBOW_RIGHT,
		INT_WRIST_RIGHT,
		INT_HIP_LEFT,
		INT_KNEE_LEFT,
		INT_HIP_RIGHT,
		INT_KNEE_RIGHT,
		INTERMEDIATE_JOINT_COUNT
	}INTERMEDIATE_JOINT_INDEX;

	/**	@remarks Structure for mapping intermediate joint to character joint*/
	typedef struct _JOINTS_MAP
	{
		INTERMEDIATE_JOINT_INDEX    m_intJoint;             // Intermediate skeleton joint
		std::string         m_characterJoint;         // Avatar joint corresponding to the intermediate joint

	} JOINTS_MAP;


	/**	@remarks Intermedate joint structure*/
	typedef struct _Int_Joint
	{
		Ogre::Quaternion local;
		Ogre::Quaternion world;
	}Int_Joint;

	/**	@remarks Listener for Kinect used to notify callback function*/
	class _GISExport KinectListener
	{
	public:
		virtual ~KinectListener() {};
		virtual bool gestureRecognized( const std::string gestureName) =0;
		virtual bool speechRecognized( const std::string wordRecognized, const float wordConfidence) =0;
	};
	

	/**	@remarks Kinect Class*/
	class _GISExport Kinect : public Object
	{
	public:
		Kinect(InputManager *manager = NULL, bool buffered = true) :
			Object(GISKinect, buffered, manager)
			{}
		~Kinect();

		void setBuffered(bool buffered){ mBuffered = buffered; }
		/**	@remarks Initialize Kinect*/
		void _initialize();
		/**	@remarks Unintialze Kinect and release resources.*/
		void _destroy();
		/**	@remarks Update skeleton, video and depth from Kinect.*/
		void capture();
		void _zero();
		/**	@remarks Listener used to notify callback funtion.*/
		KinectListener* kinectListener;
		/**	@remarks Set the Kinect listener.*/
		void setEventCallback( KinectListener *Listener ) { kinectListener = Listener;}
		/**	@remarks Tilt the angle of Kinect sensor, the angle should be within the range from -28 degree to +28 degree.*/
		void setSensorAngle(float angle);

	private:
		
		HRESULT initResult;
		
		
	public:
				//Kinect Skeleton Display Part--------------
	public:
		/**	@remarks Initialize skeleton tracking. Left, top, width, height speicify the position and size of skeleton tracking window.User needs to call this function only once in their game scene if they want to display skeleton and recognize gesture.*/
		void initializeSkeleton(Ogre::SceneManager* pSceneManager, std::string gameFolder, float left=0.7,float top=0.0,float width=0.3,float height=0.3);
		/**	@remarks Turn On/Off skeleton tracking window.*/
		void setSkeletonVisible(bool isVisible);
		/**	@remarks Get whole skeleton data from Kinect.*/
		NUI_SKELETON_DATA* Kinect::getSkeletonData();
		/**	@remarks Get new skeleton data from Kinect. If there is no new data available, return NULL.*/
		NUI_SKELETON_DATA* Kinect::getNewSkeletonData();
		/**	@remarks Get the position of a joint by it number.*/
		void getSkeletonPosition(int skeletonNum, float* x, float* y, float* z);
		/**	@remarks Attach the skeleton tracking window to another scene.*/
		void displaySkeleton(Ogre::SceneManager* pSceneManager);

	private:
		/**	@remarks Update skeleton data in capture()*/
		void updateSkeleton();
		/**	@remarks Initialize the manual object used to display skeleton.*/
		void initializeSkeletonManual();
		HANDLE m_SkeletonProcess;
		HANDLE m_hNextSkeletonEvent;
		HANDLE hSkeletonEvent;
		HANDLE newSkeletonEvent;
		Ogre::ManualObject* pskeletonManual;
		bool isSkeletonVisible;
		/**	@remarks Draw all the skeletons from Kinect.*/
		void DrawSkeleton();
		void DrawBackground();
		void DrawIndividualSkeleton(NUI_SKELETON_DATA* skeleton);
		void DrawSkeletonSegment( NUI_SKELETON_DATA * pSkel, int numJoints, ... );
		float xposition[NUI_SKELETON_POSITION_COUNT];
		float yposition[NUI_SKELETON_POSITION_COUNT];
		float SWindowLeft;
		float SWindowRight;
		float SWindowTop;
		float SWindowBottom;
		NUI_SKELETON_DATA pSkeletonData[NUI_SKELETON_COUNT];
		void storeNewSkeletonFrame();
		bool isSkeletonNew;
		int skeletonTarget;

		//Kinect Skeleton Recognition Part--------------
		/**	@remarks Thread used to handle heavy computation of gesture recognition.*/
		static DWORD WINAPI ProcessSkeletonEvent(LPVOID pParam);
		HANDLE m_SkeletonThreadStop;
		bool doGestureReco;//Do gesture recognition or not
		bool skeletonInit;

		/**	@remarks Store all the gesture imformation.*/
		std::map<std::string,GestureData> gestureMap;
		std::string gameFolder;
		std::vector<std::string> skeletonName;
		/**	@remarks Read all the gesture files from Media/Kinect folder.*/
		void getAllGestures();
		/**	@remarks Read individual gesture file.*/
		void readGestureFile(std::string fileName);
		void initSkeletonName();
		void Kinect::processNewFrame();
		/**	@remarks DTW algorithm for gesture recognition.*/
		bool Kinect::recognize(GestureData* gd);
		/**	@remarks Compute distance between two gesture frame.*/
		float Kinect::dist(std::vector<SkeletonPosition> a,std::vector<SkeletonPosition> b);
		/**	@remarks DTW main body*/
		float Kinect::dtw(std::vector<std::vector<SkeletonPosition>> seq,std::vector<std::vector<SkeletonPosition>> model,int minLength);

		//Kinect Gesture Record Part--------------
	private:
		bool doRecord;//Record gesture or not
		float globalThreshold,firstThreshold;
		float minRecoTime;
		TiXmlDocument *myDocument;
		TiXmlElement *RootElement;
		TiXmlElement *gestureData;
		std::string gestureName;
		bool* isSkeletonSelected;

		/**	@remarks Create a XML file for new gesture.*/
		void createGestureXML();

	public:
		/**	@remarks Enable/Disable gesture recognition.*/
		void enableGestureReco(bool enable);
		/**	@remarks Set individual gesture active or not.*/
		void setGestureActive(std::string gestureName,bool isActive);
		/**	@remarks Get the friendly name for all joints.*/
		std::vector<std::string> getSkeletonName();
		/**	@remarks Start recording new gesture.*/
		void startRecordGesture(std::string gestureName, float globalThreshold, float firstThreshold, float minRecoTime ,bool *isSkeletonSelected);
		/**	@remarks Stop recording new gesture.*/
		void stopRecordGesture();
		/**	@remarks Return the thresholds for the gesture specified by gestureName.*/
		void getGlobalandFirstThreshold(std::string gestureName,float* global,float *first);
		


		//Kinect Speech Recog Part--------------
	private:
		/**	@remarks Speech Recogntion engine.*/
		CComPtr<ISpRecognizer>  g_cpEngine;
		/**	@remarks Speech Recogntion context.*/
		CComPtr<ISpRecoContext> g_cpRecoCtxt;
		/**	@remarks Speech Recogntion grammar.*/
		CComPtr<ISpRecoGrammar> g_cpGrammar;
		/**	@remarks Speech Recogntion input device token.*/
		CComPtr<ISpObjectToken>  cpObjectToken;
		/**	@remarks Speech Recogntion input stream.*/
		CComPtr<ISpStream> cpInputStream;
		SPSTATEHANDLE	hDynamicRuleHandle;
		//std::wstring RunSpeechRecog();
		//bool RunSpeechRecog();
		/**	@remarks Handle the callback function for speech.*/
		static void __stdcall Kinect::speechRecognized(WPARAM wParam, LPARAM lParam); //Callback Function for Speech Recognition

		bool speechInit;


	public:
		/**	@remarks Initialize speech recogntion. Is useDefaultAudioDevice is true, use the default input device of OS; otherwise, use KinectAudioStream.*/
		bool InitSpeechRecog(bool useDefaultAudioDevice=false);
		/**	@remarks Dymaically add speech command to grammar*/
		void AddGrammar(LPCWSTR str);
		//HANDLE RecoEvent;

	//	//Kinect Camera Part--------------
	public:
		/**	@remarks Initialize video stream. Left, top, width, height speicify the position and size of video display window, res specify the stream resolution.*/
		void initializeVideo(Ogre::Real left,Ogre::Real top,Ogre::Real width,Ogre::Real height,KINECT_RESOLUTION res=KINECT_RESOLUTION_640x480);
		/**	@remarks Initialize depth stream. Left, top, width, height speicify the position and size of depth display window, res specify the stream resolution.*/
		void initializeDepth(Ogre::Real left,Ogre::Real top,Ogre::Real width,Ogre::Real height,KINECT_RESOLUTION res=KINECT_RESOLUTION_320x240);
		
		/**	@remarks Set video window visible or not*/
		void setVideoVisible(bool isVisible);
		/**	@remarks Set depth window visible or not*/
		void setDepthVisible(bool isVisible);

	private:

		HANDLE        m_hNextDepthFrameEvent;
		HANDLE        m_hNextVideoFrameEvent;
		HANDLE        m_pDepthStreamHandle;
		HANDLE        m_pVideoStreamHandle;
		RGBQUAD       m_rgbWk[640*480];

		Ogre::Image   colorImage;
		Ogre::TexturePtr videoTex;
		Ogre::TexturePtr depthTex;
		Ogre::OverlayContainer* videoPanel;
		Ogre::OverlayContainer* depthPanel;
		Ogre::Overlay* videoOverlay;
		Ogre::Overlay* depthOverlay;
		HANDLE        videoEvent;
		HANDLE		  depthEvent;

		bool displayDepth;
		bool displayVideo;

		RGBQUAD    Nui_ShortToQuad_Depth( USHORT s );
		/**	@remarks Intialize camera related event. This function is called in _initialize.*/
		void initializeCamera();
		/**	@remarks Update video stream. This function is called in capture().*/
		void updateVideo();
		/**	@remarks Update depth stream. This function is called in capture().*/
		void updateDepth();

		bool depthInit;
		bool videoInit;

		//Kinect Animation Mapping Part--------------
	private:
		/**	@remarks Calculate rotation of each intermediate joints using skeleton positions from Kinect.*/
		void CalculateJointRotations();
		/**	@remarks Apply the rotation of intermediate joints to the skeleton of character model.*/
		void GetCharacterRotations(Ogre::Skeleton* skeleton,Ogre::Entity* character,JOINTS_MAP* jointMap);
	
public:
		/**	@remarks Update the character model using skeleton positions from Kinect. This funciton should be called inside the update loop of game scene.*/
	    void mapSkeleton(Ogre::Entity* character,Ogre::Skeleton* skeleton,JOINTS_MAP* jointMap);
		/**	@remarks Manually control the bone specified by name of character. This function also rotate the bone so that character can be set up to T-Pose.*/
		void setupBone(Ogre::Entity* character,const Ogre::String& name,const Ogre::Quaternion& q);
		/**	@remarks Manually control the bone specified by name of character. This function also rotate the bone so that character can be set up to T-Pose.*/
		void setupBone(Ogre::Entity* character,const Ogre::String& name,const Ogre::Radian& angle, const Ogre::Vector3 axis);
		/**	@remarks Manually control the bone specified by name of character. This function also rotate the bone so that character can be set up to T-Pose.*/
		void setupBone(Ogre::Entity* character,const Ogre::String& name,const Ogre::Degree& yaw,const Ogre::Degree& pitch,const Ogre::Degree& roll);

	};

}
#endif