#ifndef GISEvents_H
#define GISEvents_H

#include "GISObject.h"

namespace GIS
{
	/**
		Base class of all events
	*/ 
	class _GISExport EventArg
	{
	public:
		EventArg( Object* obj ) : device(obj) {}
		virtual ~EventArg() {}

		//! Pointer to the Input Device
		const Object* device;
	};
  
}
#endif