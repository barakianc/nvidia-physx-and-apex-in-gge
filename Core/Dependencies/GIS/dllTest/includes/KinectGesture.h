/**	@remarks Skeleton Position structure*/
typedef struct _SkeletonPosition
{
	float x;
	float y;
	float z;
}SkeletonPosition;

/**	@remarks Gesture data structure. This structure stores the gesture model and all the parameters relevant to gesture recognition*/
typedef struct _GestureData
{
	std::vector<std::vector<SkeletonPosition>> model;//Gesture model
	std::vector<std::vector<SkeletonPosition>> seq;//Live gesture data
	bool isActive;//Should this gesture be recognize
	bool isSkeletonSelected[NUI_SKELETON_POSITION_COUNT];//Which skeleton is selected to be tracked
	float globalThreshold;
	float firstThreshold;
	int minRecoFrame;//Minimum gesture frames required to recognized a gesture
	float computefirst;//First distance between model and on-live data
	float computeglobal;//Global distance between model and on-live data
}GestureData;