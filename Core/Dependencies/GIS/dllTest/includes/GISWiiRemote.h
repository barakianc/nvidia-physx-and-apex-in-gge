#ifndef GISWiiRemote_H
#define GISWiiRemote_H

#include <windows.h>

#include "GISObject.h"
#include "GISEvents.h"

#include "wiimote.h"

// WR = Wii Remote. Duh.
namespace GIS
{
	//! Button IDs: WR = Wii Remote, NC = Nunchuck, CC = Classic Controller
	enum WiiButtonID{
		WR_ONE   = 0x0001,
		WR_TWO   = 0x0002,
		WR_A     = 0x0004,
		WR_B     = 0x0008,
		WR_MINUS = 0x0010,
		WR_HOME  = 0x0020,
		WR_PLUS  = 0x0040,
		WR_UP    = 0x0080,
		WR_DOWN  = 0x0100,
		WR_LEFT  = 0x0200,
		WR_RIGHT = 0x0400,
		NC_C     = 0x0800,
		NC_Z     = 0x1000
	};

	/* CLASS WiiRemoteState */
	/**
		Represents the state of the Wii remote
		All members are valid for both buffered and non buffered mode
	*/
	class _GISExport WiiRemoteState
	{
	public: 
		// Wii Remote State Data
		//! Represents all buttons on Wii remote - bit position indicates button down
		int buttons;

		//! XYZ component of accelerometer in Wii remote
		Vector3 remote;

		//! XY position of left IR point
		Vector2 lCam;

		//! XY position of right IR point
		Vector2 rCam;

		//! AnalogStick object to represent nunchuck's analog stick
		AnalogStick stick;

		//! XYZ component of accelerometer in nunchuck
		Vector3 nunchuck;

		// Functions

		//! Clear all the values
		void clear(){
			buttons = 0;
			remote.clear();
			lCam.clear();
			rCam.clear();
			//nunchuckAttacked = false;
			stick.clear();
			nunchuck.clear();
		}

		//! Button down test
		inline bool wrButtonsDown (int bID) const{
			return ((buttons & bID) == bID);
		}

		//! Button up test
		inline bool wrButtonsUp (int bID) const{
			return ((buttons | ~bID) == ~bID);
		}
	};

	/* CLASS WiiRemoteEvent */
	/** Specialised for Xbox 360 controller events */
	class _GISExport WiiRemoteEvent : public EventArg
	{
	public:
		WiiRemoteEvent( Object* obj, const WiiRemoteState &wrs): EventArg(obj), state(wrs) {}
		virtual ~WiiRemoteEvent() {};

		//! The state of the Wii Remote
	    const WiiRemoteState &state;
	
	};

	/* CLASS WiiRemoteListener */
	/**
		To receive buffered Wii remote input, derive a class from this, and implement the
		methods here. Then set the call back to your WiiRemote instance with WiiRemote::setEventCallback
	*/
	class _GISExport WiiRemoteListener
	{
	public:
		virtual ~WiiRemoteListener() {}
		virtual bool buttonPressed( const WiiRemoteEvent &wrEvent, WiiButtonID button ) = 0;
		virtual bool buttonReleased( const WiiRemoteEvent &wrEvent, WiiButtonID button ) = 0;
		// If we ever add Classic Controller support, we'd want to update
		// this function to indicate WHICH stick has been moved. But for
		// right now, we can simply grab the data from the state.
		virtual bool analogStickMoved( const WiiRemoteEvent &wrEvent ) = 0; 
		virtual bool remoteAccelerometersMoved( const WiiRemoteEvent &wrEvent) = 0;
		virtual bool nunchuckAccelerometersMoved( const WiiRemoteEvent &wrEvent) = 0;
		virtual bool irMoved( const WiiRemoteEvent &wrEvent) = 0;
	};

	/* CLASS WiiRemote */
	/**
		WiiRemote base class.
	*/
	class _GISExport WiiRemote : public Object
	{
	public: 
		// FUNCTIONS
		WiiRemote(InputManager *manager = NULL, bool buffered = true) : 
				Object(GISWiiRemote, buffered, manager), 
				isConnected(false),
				useIR(true),
				useRemoteAccel(true),
				reqNunchuck(false),
				useNunchuckAccel(true),
				useAnalogStick(true)
				{}
		~WiiRemote();

		/** @remarks Starts the data stream of the Wii Remote */
		bool Connect();

		/**
		@remarks
			Set control variable reqNunchuck
		@param req
			Send a boolean to set the control variable
		*/
		void setNunchuckRequire( bool req ){ reqNunchuck = req; }

		/**
		@remarks
			Set control variable useIR
		@param use
			Send a boolean to set the control variable
		*/
		void setIRUse( bool use ) { useIR = use; } 

		/**
		@remarks
			Set control variable useRemoteAccel
		@param use
			Send a boolean to set the control variable
		*/
		void setRemoteAccelUse( bool use ) { useRemoteAccel = use; }

		/**
		@remarks
			Set control variable useNunchuckAccel
		@param use
			Send a boolean to set the control variable
		*/
		void setNunchuckAccelUse( bool use ) { useNunchuckAccel = use; }

		/**
		@remarks
			Set control variable useAnalogStick
		@param use
			Send a boolean to set the control variable
		*/
		void setAnalogStickUse( bool use ) { useAnalogStick = use; }
		
		

		/**
		@remarks
			Register/unregister a Wii remote Listener - Only one allowed for simplicity. If broadcasting
			is neccessary, just broadcast from the callback you registered.
		@param wrListener
			Send a pointer to a class derived from WiiRemoteListener or 0 to clear the callback
		*/
		void setEventCallback( WiiRemoteListener *wrListener ){ mListener = wrListener; }
		
		/** @remarks Returns currently set callback.. or 0 */
		WiiRemoteListener* getEventCallback() {return mListener;}

		/** @remarks Returns the state of the Wii Remote - is valid for both buffered and non buffered mode */
		const WiiRemoteState& getWiiRemoteState() const { return mState; }
		//const cWiiMote& 

		friend DWORD WINAPI Update(LPVOID self);
		
		// Inherited Functions:
		void setBuffered(bool buffered);
		void capture();
		void _initialize();
		// Type type() const { return mType; }
		// virtual bool buffered() const { return mBuffered; }
		//InputManager* getCreator() { return mCreator; }
		////virtual Interface* queryInterface(Interface::IType type) = 0;

		
	
	protected:
		// VARIABLES
		cWiiMote mRemote;

		//! True if Wii remote is connected, false otherwise
		bool isConnected;

		HANDLE mThread;
		
		//! True if Nunchuck is required, false otherwise
		bool reqNunchuck;
		
		/** Control Variable - Allows you to ignore IR input for potential speed-up. 
			These default to on.
		*/
		bool useIR;

		/** Control Variable - Allows you to ignore accelerometer input for potential speed-up. 
			These default to on.
		*/
		bool useRemoteAccel;

		/** Control Variable - Allows you to ignore Nunchuck accelerometer input for potential speed-up. 
			These default to on.
		*/
		bool useNunchuckAccel;

		/** Control Variable - Allows you to ignore Analog stick input for potential speed-up. 
			These default to on.
		*/
		bool useAnalogStick;
		
		//! The state of the mouse
		WiiRemoteState mState; 

		//! Used for buffered/actionmapping callback
		WiiRemoteListener* mListener;
		/* Inherited values:
		   Type mType;             - mType is "fully implemented", so to speak
		   bool mBuffered          - mBuffered is doing nothing
		   InputManager* mCreator  - mCreator is currently commented out
		                             Needs to be implemented at some point.
		*/
		// FUNCTIONS
		// May not be needed... void _read();
		
	};

	DWORD WINAPI Update(LPVOID self);

}
#endif