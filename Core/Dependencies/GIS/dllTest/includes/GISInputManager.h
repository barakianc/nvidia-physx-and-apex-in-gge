//#ifdef WRAPPER
#ifndef GISInputManager_H
#define GISInputManager_H
#include "GISPrereqs.h"
#include "OIS.h"

namespace GIS
{
	/**
		Base Manager class.
	*/
	class _GISExport InputManager
	{
	protected:
		/* FUNCTIONS */
		
		InputManager();
		
		/**
		@remarks
			Called from createInputSystem, gives derived input class a chance to setup after it is created
		*/
		void _initialize(ParamList &paramList);
		
		/* VARIABLES */
		
		//! Pointer to OIS::InputManager
		OIS::InputManager* m_pInputManager;
		// TODO: GIS-specific stuff goes here
		

	public:
		/* FUNCTIONS */

		/**
		@remarks
			Creates appropriate input system dependent on platform. 
		@param paramList
			ParamList contains OS specific info (such as HWND and HINSTANCE for window apps),
			and access mode.
		@returns
			A pointer to the created manager, or raises Exception
		*/
		static InputManager* createInputSystem( ParamList &paramList );

		/**
		@remarks
			Destroys the InputManager
		@param manager
			Manager to destroy
		*/
		static void destroyInputSystem(InputManager* manager);

		/**
		@remarks Gets the name of the current platform input system
		*/
		const std::string& inputSystemName();

		/**
		@remarks
			Returns the number of the specified GIS::Type devices discovered by GIS
		@param iType
			Type that you are interested in
		*/
		int getNumberOfDevices( Type iType );

		/**
		@remarks
			Lists all unused devices
		@returns
			DeviceList which contains Type and vendor of device
		*/
		DeviceList listFreeDevices();

		/**
		@remarks
			Tries to create an object with the specified vendor. If you have no
			preference of vendor, leave vendor as default (""). Raises exception on failure
		*/
		Object* createInputObject( Type iType, bool bufferMode, const std::string &vendor = "");


		/**
		@remarks
			Tries to create an OIS object with the specified vendor. If you have no
			preference of vendor, leave vendor as default (""). Raises exception on failure
		*/
		OIS::Object* createOISObject( Type iType, bool bufferMode, const std::string &vendor="");

		/**
		@remarks Destroys Input Object
		*/
		void destroyInputObject(Object* object);
	};
  
}
#endif
//#endif