#pragma once

//ResourceManager
#include "ResourceManager.h"

// Graphics
#include "OgreTextAreaOverlayElement.h"

// Input
#include "GIS.h"

#include "OgreConsole.h"

class GameObjectManager;

namespace GamePipe
{
	class GameScreen
	{
	private:
		std::string m_Name;
		bool m_bBackgroundUpdate;
		bool m_bBackgroundDraw;
		bool m_bIsPopup;

		Ogre::Viewport* m_pViewport;
		Ogre::Camera* m_pActiveCamera;
		Ogre::ColourValue m_BackgroundColor;

		GameScreen* m_pCallingGameScreen;
	protected:
		GameObjectManager* m_pGameObjectManager;
		bool m_bSupportsTerrain;


	public:

		/**
		 *	Create Default SceneManager in Ogre, this has to be called only once
		 */
		Ogre::SceneManager* CreateDefaultSceneManager();
		void DestroyDefaultSceneManager();
		void CreateDefaultPhysicsScene(bool f_bVisualDebuggerActive=false);
		void DestroyDefaultPhysicsScene();

		Ogre::Camera* CreateDefaultCamera();


		/************************************************************************/
		/* Default Camera Parameters                                            */
		/************************************************************************/
		Ogre::Vector3 m_v3CameraPosition;
		Ogre::Quaternion m_qCameraOrientation;
		float m_fCameraMovementSpeed;
		float m_fCameraRotationSpeed;

	public:
		GameScreen(std::string name, bool supportsTerrain = false);
		virtual ~GameScreen();

		std::string GetName() { return m_Name; }
		void SetName(std::string name) { m_Name = name; }

		inline Ogre::SceneManager* GameScreen::GetDefaultSceneManager();
		inline GameObjectManager* GetGameObjectManager() { return m_pGameObjectManager; }

		inline bool BackgroundUpdate() { return m_bBackgroundUpdate; }
		inline void SetBackgroundUpdate(bool bBackgroundUpdate) { m_bBackgroundUpdate = bBackgroundUpdate; }
		inline bool BackgroundDraw() { return m_bBackgroundDraw; }
		inline void SetBackgroundDraw(bool bBackgroundDraw) { m_bBackgroundDraw = bBackgroundDraw; }
		inline bool IsBackgroundScreen();

		inline bool IsPopup() { return m_bIsPopup; }
		inline void SetIsPopup(bool bIsPopup) { m_bIsPopup = bIsPopup; }
	
		inline Ogre::ColourValue GetBackgroundColor() { return m_BackgroundColor; }
		inline void SetBackgroundColor(Ogre::ColourValue backgroundColor) { m_BackgroundColor = backgroundColor; }
		
		/************************************************************************/
		/* Resource Management                                                  */
		/************************************************************************/
		void SaveScene(Ogre::String fileName);
		void LoadScene(Ogre::String fileName);

		virtual bool LoadResources();
		virtual bool UnloadResources();

		bool LoadTerrain(std::string cfgFileName, std::string resourceGroupName = "General");

		/************************************************************************/
		/* Camera & Viewport                                                    */
		/************************************************************************/
		inline Ogre::Viewport* GetViewport() { return m_pViewport; }
		inline void SetViewport(Ogre::Viewport* pViewport) { m_pViewport = pViewport; }

		inline Ogre::Camera* GameScreen::GetDefaultCamera();
		inline Ogre::SceneNode* GameScreen::GetDefaultCameraSceneNode();

		inline Ogre::Camera* GetActiveCamera() { return m_pActiveCamera; }
		inline void SetActiveCamera(Ogre::Camera* pCamera);

		inline Ogre::SceneNode* GetActiveCameraSceneNode() { return m_pActiveCamera->getParentSceneNode(); }

		/************************************************************************/
		/* GameScreen Management                                                */
		/************************************************************************/
		inline GameScreen* GetCallingGameScreen() { return m_pCallingGameScreen; }

		inline void SetCallingGameScreen(GameScreen* pCallingGameScreen) { m_pCallingGameScreen = pCallingGameScreen; }

		void AddGameScreen(GameScreen* pGameScreen);
		void RemoveGameScreen(GameScreen* pGameScreen);

		/************************************************************************/
		/* Initialize, Show, Update & Draw                                      */
		/************************************************************************/
		bool _Show();
		virtual bool Show();
		bool _Draw();
		virtual bool Draw();
		
		bool _BaseInitialize(bool isLoadingScreen=false);
		virtual bool Initialize()=0;

		bool _BaseDestroy(bool isLoadingScreen=false);
		virtual bool Destroy()=0;		
		
		bool _BaseUpdate();
		virtual bool Update()=0;

		virtual bool Close();

		/************************************************************************/
		/* Input Handling                                                       */
		/************************************************************************/
		bool FreeCameraMovement();
		bool DefaultOnMouseMove(const GIS::MouseEvent& mouseEvent);

		virtual bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		virtual bool OnKeyRelease(const GIS::KeyEvent &keyEvent);
		virtual bool OnMouseMove(const GIS::MouseEvent &mouseEvent);
		virtual bool OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);
		virtual bool OnMouseRelease(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);

		virtual bool OnWiiRemoteButtonPressed( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button );
		virtual bool OnWiiRemoteButtonReleased( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button );
		virtual bool OnWiiAnalogStickMoved( const GIS::WiiRemoteEvent &wrEvent ); 
		virtual bool OnWiiRemoteAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent);
		virtual bool OnWiiNunchuckAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent);
		virtual bool OnWiiIRMoved( const GIS::WiiRemoteEvent &wrEvent);

		virtual bool OnXpadButtonPressed( const GIS::XPadEvent &xpEvent, int button);
		virtual bool OnXpadButtonReleased( const GIS::XPadEvent &xpEvent, int button);
		virtual bool OnXpadLTriggerPressed( const GIS::XPadEvent &xpEvent );
		virtual bool OnXpadRTriggerPressed( const GIS::XPadEvent &xpEvent );
		virtual bool OnXpadLThumbStickMoved( const GIS::XPadEvent &xpEvent );
		virtual bool OnXpadRThumbStickMoved( const GIS::XPadEvent &xpEvent );

		virtual bool gestureRecognized( const std::string gestureName);
		virtual bool speechRecognized( const std::string wordRecognized,const float wordConfidence);
		
	};
}
