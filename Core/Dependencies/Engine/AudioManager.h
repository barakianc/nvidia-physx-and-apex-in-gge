#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#pragma once
#include "stdio.h"
#include "GameScreen.h"
#include <string>
#include <sstream>
//FMOD
#include "fmod.hpp"
#include "fmod_event.hpp"
//Wwise
#include <AK/SoundEngine/Common/AkSoundEngine.h>
#include <AK/SoundEngine/Common/AkModule.h>
#include <AK/MusicEngine/Common/AkMusicEngine.h>
#include <AkSoundEngineDLL.h>
#include <AkDefaultIOHookBlocking.h>
#ifndef AK_OPTIMIZED
    #include <AK/Comm/AkCommunication.h>
#endif // AK_OPTIMIZED
using namespace AK;
namespace GamePipe
{	
	/*!
		@remark AudioManager Class
	*/
	class AudioManager
	{
	public:		
		/*!
			@remark AudioManager Constructor: no use for this semester
		*/
		AudioManager::AudioManager();
		/*!
			@remark AudioManager Destructor: no use for this semester
		*/
		AudioManager::~AudioManager();
		
		/////////////////////////////////////////////////////////////////////////////////////
		// Wwise functions
		//
		///
		/*!
			@remark boolean flag tells whether or not wwise has been initialized
		*/
		static bool AudioManager::initializeWwiseFlag;
		/*!
			@remark initialize Wwise module
		*/
		static void AudioManager::InitializeWwise();
		/*!
			@remark load Wwise one sound bank
			@param[in] bankName must be a String that represents the name of the .bnk file that you are attempting to load
		*/
		static void AudioManager::LoadWwiseBank(std::string bankName);
		/*!
			@remark unload Wwise one sound bank
			@param[in] bankName must be a String that represents the name of the .bnk file that you are attempting to unload
		*/
		static void AudioManager::UnLoadWwiseBank(std::string bankName);
		/*!
			@remark unload all Wwise sound banks			
		*/
		static void AudioManager::UnLoadWwiseAudio();
		/*!
			@remark set up the position of 3D sound of Game Object
			@param[in] GGE Game Object ID
			@param[in] position is position of the sound
			@param[in] orientation is orientation of the sound
		*/
		static void AudioManager::SetWwiseGameObjectPosition(int gameObject, Ogre::Vector3 position, Ogre::Quaternion orientation);
		/*!
			@remark set up the position of listener that would like to listen 3D sound
			@param[in] position is position of the listener
			@param[in] orientation is orientation of the listener
		*/
		static void AudioManager::SetWwiseListenerPosition(Ogre::Vector3 position, Ogre::Quaternion orientation);
		/*!
			@remark Play the music defined in the argument. This function will post the Play_musicName event, and then render the audio
			@param[in] There must be a Play_musicName event created in Wwise Sound Designer
		*/
		static void AudioManager::PlayWwiseMusic(std::string musicName);
		/*!
			@remark Pause the music defined in the argument. This function will post the Pause_musicName event, and then render the audio
			@param[in] There must be a Pause_musicName event created in Wwise Sound Designer
		*/
		static void AudioManager::PauseWwiseMusic(std::string soundName);
		/*!
			@remark Stop the music defined in the argument. This function will post the Stop_musicName event, and then render the audio
			@param[in] There must be a Stop_musicName event created in Wwise Sound Designer
		*/
		static void AudioManager::StopWwiseMusic(std::string soundName);
		/*!
			@remark Resume the music defined in the argument. This function will post the Resume_musicName event, and then render the audio
			@param[in] There must be a Resume_musicName event created in Wwise Sound Designer
		*/
		static void AudioManager::ResumeWwiseMusic(std::string soundName);
		/*!
			@remark Play the sound defined in the argument with GGE Game Object ID. This function will post the Play_soundName event, and then render the audio
			@param[in] There must be a Play_soundName event created in Wwise Sound Designer
			@param[in] gameObj is a GGE Game Object ID declared in GGE Game Object. Please declare a Game Object ID first before using this function
		*/
		static void AudioManager::PlayWwiseSound(std::string soundName, int gameObj);
		/*!
			@remark Play the sound defined in the argument with GGE Game Object ID and RTCValue. This function will post the Play_soundName event, and then render the audio
			@param[in] There must be a Play_soundName event created in Wwise Sound Designer
			@param[in] gameObj is a GGE Game Object ID declared in GGE Game Object. Please declare a Game Object ID first before using this function
			@param[in] Min and Max RTPCValues are declared in Wwise Sound Deginer. Please use the parameter in the range of RTPCValues
		*/
		static void AudioManager::PlayWwiseSound(std::string soundName, int gameObj, float RTPCValue);
		/*!
			@remark Stop the music defined in the argument. This function will post the Stop_musicName event, and then render the audio
			@param[in] There must be a Stop_musicName event created in Wwise Sound Designer
		*/
		static void AudioManager::StopWwiseSound(std::string soundName);
		/*!
			@remark get Wwise sound or music status like playing, paused, stopping
			@param[in] soundName must be event name created in Wwise Sound Designer not including action_ like Play_, Stop_, and so on
		*/
		static Ogre::String AudioManager::getWwiseState(std::string soundName);

		/////////////////////////////////////////////////////////////////////////////////////
		// FMOD functions
		//
		/*!
			@remark FMOD event sysem variable
		*/
		static FMOD::EventSystem* AudioManager::FMODeventSystem;
		/*!
			@remark FMOD system variable
		*/
		static FMOD::System* AudioManager::FMODsys;
		/*!
			@remark boolean flag tells whether or not FMOD has been initialized
		*/
		static bool AudioManager::FMODInitializeFlag;	
		/*!
			@remark boolean flag tells whether or not FMOD has been loaded
		*/
		static bool AudioManager::FMODLoadFlag;		
		/*!
			@remark initialize FMOD module
		*/
		static void AudioManager::FMODInitializeAudio();
		/*!
			@remark load FMOD sound bank depended on AudioPath.txt
			@param[in] sectionName is which section you want to load from AudioPath.txt
		*/
		static void AudioManager::FMODLoadAudio(char *sectionName);
		/*!
			@remark load FMOD sound bank depended on AudioPath.txt
			@param[in] sectionName is which section you want to unload from AudioPath.txt
		*/
		static void AudioManager::FMODUnLoadAudio(char *sectionName);
		/*!
			@remark add DSP to FMOD system.
			@param[in] DSP type can be chosen in FMOD_DSP_type. Some DSP effects need to set up other parameters
		*/
		static void AudioManager::FMODaddDsp(FMOD_DSP_TYPE  type);
		/*!
			@remark remove one DSP effect from first one of the DSP list.
		*/
		static void AudioManager::FMODremoveDsp();		
		/*!
			@remark set up FMOD event sound
			@param[in] eventpath is a path where you want to play sound from the loaded bank
			@param[in] name is a event name created for future purpose
		*/
		static void AudioManager::FMODSetEventSound(std::string eventpath,  std::string name);
		/*!
			@remark set FMOD event 3D sound
			@param[in] eventpath is a path where you want to play 3D sound from the loaded bank
			@param[in] name is a event name created for future purpose
		*/
		static void AudioManager::FMODSet3DEventSound(std::string eventpath, std::string name);
		/*!
			@remark set up attributes of 3D sound 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
			@param[in] pos is position of the 3D sound
			@param[in] vel is orientation of the 3D sound
		*/
		static void AudioManager::FMODSet3DAttributes(std::string name, FMOD_VECTOR pos, FMOD_VECTOR vel);
		/*!
			@remark set up the position of the listener
			@param[in] position is the position of the listener
			@param[in] orientation is orientation of the listener
		*/
		static void AudioManager::FMODUpdateListener(Ogre::Vector3 position, Ogre::Quaternion orientation);
		/*!
			@remark play Fmod sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
		*/
		static void AudioManager::FMODEventPlay(std::string name);
		/*!
			@remark pause Fmod sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
		*/
		static void AudioManager::FMODEventPaused(std::string name);
		/*!
			@remark set up RPM sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
			@param[in] setVolume is RPM pitch of the event sound. The RPM pitch is set in FMOD sound designer
		*/
		static void AudioManager::FMODSetEventRPMVolume(std::string name, float setVolume);
		/*!
			@remark set up RPM sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
			@param[in] setVolume is LOAD pitch of the event sound. The LOAD pitch is set in FMOD sound designer
		*/
		static void AudioManager::FMODSetEventLOADVolume(std::string name, float setVolume);
		/*!
			@remark set up volume of Fmod sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
			@param[in] setVolume is volume of the event sound you would like to set up 
		*/
		static void AudioManager::FMODSetEventVolume(std::string name, float setVolume);
		/*!
			@remark stop Fmod sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
		*/
		static void AudioManager::FMODEventStop(std::string name);
		/*!
			@remark replay Fmod sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
		*/
		static void AudioManager::FMODEventReplay(std::string name);
		/*!
			@remark mute Fmod sound with the event name 
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
		*/
		static void AudioManager::FMODSetEventMute(std::string name);
		/*!
			@remark pause all current fmod sounds
		*/
		static void AudioManager::FMODPlayPauseEvents();
		/*!
			@remark check whether FMOD function runs correctly or not 
			@param[in] result is gotten from FMOD function 
		*/
		static void AudioManager::FMODErrorCheck(FMOD_RESULT result);
		/*!
			@remark get FMOD sound or music status like playing, paused, stopping
			@param[in] name is set in FMODSetEventSound() or FMODSet3DEventSound()
		*/
		static Ogre::String AudioManager::FMODGetState(std::string name);

		// Reverb functions don't work, they never have.  Fix these if you want.  Not a high priority
		//
		//static FMOD::EventReverb* AudioManager::FMODreverbPreset;
		//static void AudioManager::FMODactivateReverb(const char* name);
		//static void AudioManager::FMODdeactivateReverb(const char* name);
	private:		
		////////////////////////////////////////////////////////////////////////////////////
		// Wwise function
		//
		/*!
			@remark get Wwise Media path 
		*/
		static std::string AudioManager::GetWwiseAudioEventsPath();

		////////////////////////////////////////////////////////////////////////////////////
		// FMOD function
		//
		/*!
			@remark get the path to where AudioPaths.txt file is.
		*/
		static std::string AudioManager::FMODGetAudioPath();
		/*!
			@remark get Fmode Media Path
		*/
		static std::string AudioManager::FMODGetAudioEventsPath();
	};
}

#endif //AUDIO_MANAGER_H