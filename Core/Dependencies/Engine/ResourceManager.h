#pragma once

#ifndef ResourceManager_h__
#define ResourceManager_h__

#include <string>
#include <map>
#include "Base.h"

namespace GamePipe
{
	class ResourceManager
	{
	public:

		/**
		 *	Get Singleton Pointer
		 */
		static ResourceManager* GetInstancePtr()
		{
			if (0 == m_pResourceManager)
			{
				m_pResourceManager = new ResourceManager();
			}

			return m_pResourceManager;
		}

		/**
		 *	Get Singleton Reference
		 */
		static ResourceManager& GetInstanceRef()
		{
			if (0 == m_pResourceManager)
			{
				m_pResourceManager = new ResourceManager();
			}

			return *m_pResourceManager;
		}

		static void DestroyInstance()
		{
			delete m_pResourceManager;
			m_pResourceManager = 0;
		}

		void LoadResourceGroup(std::string resGroupName);
		void UnloadResourceGroup(std::string resGroupName);

	protected:
		ResourceManager() {}
		~ResourceManager() {}
	private:
		static ResourceManager* m_pResourceManager;
		std::map<std::string, int> resourceTracker;

		void DoLoad(std::string resGroupName);
		void DoUnload(std::string resGroupName);
	};
}

#endif // ResourceManager_h__