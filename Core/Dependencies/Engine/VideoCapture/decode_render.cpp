//
//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 2005-2011 Intel Corporation. All Rights Reserved.
//
#include "StdAfx.h"
#include <windowsx.h>

#include <dwmapi.h>
#include <mmsystem.h>
#include <math.h>
#include <minmax.h>

#include "decode_render.h"
#include "sample_defs.h"

#pragma warning(disable : 4100)

const UINT DWM_BUFFER_COUNT  = 4;

CD3D9Device::CD3D9Device()
{
    m_pD3D9 = NULL;
    m_pD3DD9 = NULL;
    m_pDeviceManager9 = NULL;
    MSDK_ZERO_MEMORY(m_D3DPP);
    m_resetToken = 0;

    m_nViews = 0;
    m_pS3DControl = NULL;

    MSDK_ZERO_MEMORY(m_backBufferDesc);
    m_pDXVAVPS = NULL;
    m_pDXVAVP_Left = NULL;
    m_pDXVAVP_Right = NULL;
    m_pRenderSurface = NULL;


    MSDK_ZERO_MEMORY(m_targetRect);

    MSDK_ZERO_MEMORY(m_VideoDesc);
    MSDK_ZERO_MEMORY(m_BltParams);
    MSDK_ZERO_MEMORY(m_Sample);

    // Initialize DXVA structures

    DXVA2_AYUVSample16 color = {         
        0x8000,          // Cr
        0x8000,          // Cb
        0x1000,          // Y
        0xffff           // Alpha
    };

    DXVA2_ExtendedFormat format =   {           // DestFormat
        DXVA2_SampleProgressiveFrame,           // SampleFormat
        DXVA2_VideoChromaSubsampling_MPEG2,     // VideoChromaSubsampling
        DXVA_NominalRange_0_255,                // NominalRange
        DXVA2_VideoTransferMatrix_BT709,        // VideoTransferMatrix
        DXVA2_VideoLighting_bright,             // VideoLighting
        DXVA2_VideoPrimaries_BT709,             // VideoPrimaries
        DXVA2_VideoTransFunc_709                // VideoTransferFunction            
    };

    // init m_VideoDesc structure
    memcpy(&m_VideoDesc.SampleFormat, &format, sizeof(DXVA2_ExtendedFormat));
    m_VideoDesc.SampleWidth                         = 0;
    m_VideoDesc.SampleHeight                        = 0;    
    m_VideoDesc.InputSampleFreq.Numerator           = 60;
    m_VideoDesc.InputSampleFreq.Denominator         = 1;
    m_VideoDesc.OutputFrameFreq.Numerator           = 60;
    m_VideoDesc.OutputFrameFreq.Denominator         = 1;
    
    // init m_BltParams structure
    memcpy(&m_BltParams.DestFormat, &format, sizeof(DXVA2_ExtendedFormat));
    memcpy(&m_BltParams.BackgroundColor, &color, sizeof(DXVA2_AYUVSample16));  
      
    // init m_Sample structure
    m_Sample.Start = 0;
    m_Sample.End = 1;
    m_Sample.SampleFormat = format;
    m_Sample.PlanarAlpha.Fraction = 0;
    m_Sample.PlanarAlpha.Value = 1;   

    m_overlaySupported = false;
}

mfxStatus CD3D9Device::Init(
    WindowHandle hWindow,
    mfxU16 nViews)
{
    if (0 == nViews || 2 < nViews)
        return MFX_ERR_UNSUPPORTED;
  
    m_nViews = nViews;
    bool isOverlayRequired = nViews > 1;

    HRESULT hr = Direct3DCreate9Ex(D3D_SDK_VERSION, &m_pD3D9);
    if (!m_pD3D9 || FAILED(hr))
        return MFX_ERR_DEVICE_FAILED;

    ZeroMemory(&m_D3DPP, sizeof(m_D3DPP));
    m_D3DPP.Windowed = true;
    m_D3DPP.hDeviceWindow = (HWND)hWindow;

    m_D3DPP.Flags                      = D3DPRESENTFLAG_VIDEO;
    m_D3DPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
    m_D3DPP.PresentationInterval       = D3DPRESENT_INTERVAL_ONE;
    m_D3DPP.BackBufferCount            = 1;
    m_D3DPP.BackBufferFormat           = D3DFMT_X8R8G8B8;

    m_D3DPP.BackBufferWidth  = GetSystemMetrics(SM_CXSCREEN);
    m_D3DPP.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);

    //
    // Mark the back buffer lockable if software DXVA2 could be used.
    // This is because software DXVA2 device requires a lockable render target
    // for the optimal performance.
    //
    {
        m_D3DPP.Flags |= D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
    }

    // try to init overlay
    m_overlaySupported = true;
    if(isOverlayRequired)
    {
        D3DCAPS9                d3d9caps;
        D3DOVERLAYCAPS          d3doverlaycaps = {0};
        IDirect3D9ExOverlayExtension *d3d9overlay = NULL;
        memset(&d3d9caps, 0, sizeof(d3d9caps));
        hr = m_pD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &d3d9caps);
        if (FAILED(hr) || !(d3d9caps.Caps & D3DCAPS_OVERLAY))
        {
            m_overlaySupported = false;            
        }
        else
        {
            hr = m_pD3D9->QueryInterface(IID_PPV_ARGS(&d3d9overlay));
            if (FAILED(hr) || (d3d9overlay == NULL))
            {
                m_overlaySupported = false;
            }
            else
            {
                hr = d3d9overlay->CheckDeviceOverlayType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
                    m_D3DPP.BackBufferWidth,
                    m_D3DPP.BackBufferHeight,
                    m_D3DPP.BackBufferFormat, NULL,
                    D3DDISPLAYROTATION_IDENTITY, &d3doverlaycaps);
                MSDK_SAFE_RELEASE(d3d9overlay);

                if (FAILED(hr))
                {
                    m_overlaySupported = false;
                }
            }
        }
    }
    
    m_D3DPP.SwapEffect = (isOverlayRequired && m_overlaySupported) ? D3DSWAPEFFECT_OVERLAY : D3DSWAPEFFECT_DISCARD;

    hr = m_pD3D9->CreateDeviceEx(
        D3DADAPTER_DEFAULT,
        D3DDEVTYPE_HAL,
        (HWND)hWindow,
        D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
        &m_D3DPP,
        NULL,
        &m_pD3DD9);
    if (FAILED(hr))
        return MFX_ERR_NULL_PTR;

    hr = m_pD3DD9->ResetEx(&m_D3DPP, NULL);
    if (FAILED(hr))
        return MFX_ERR_UNDEFINED_BEHAVIOR;    

    hr = m_pD3DD9->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
    if (FAILED(hr))
        return MFX_ERR_UNDEFINED_BEHAVIOR;    

    UINT resetToken = 0;

    hr = DXVA2CreateDirect3DDeviceManager9(&resetToken, &m_pDeviceManager9);
    if (FAILED(hr)) 
        return MFX_ERR_NULL_PTR;

    hr = m_pDeviceManager9->ResetDevice(m_pD3DD9, resetToken);
    if (FAILED(hr))
        return MFX_ERR_UNDEFINED_BEHAVIOR;

    m_resetToken = resetToken;

    return MFX_ERR_NONE;
}

mfxStatus CD3D9Device::Reset()
{
    HRESULT hr = NO_ERROR;

    D3DPRESENT_PARAMETERS d3dpp=m_D3DPP;
    ZeroMemory(&d3dpp, sizeof(d3dpp));

    D3DDISPLAYMODE d3dmodeTemp;
    m_pD3DD9->GetDisplayMode(0, &d3dmodeTemp);

    d3dpp.BackBufferWidth  = d3dmodeTemp.Width;
    d3dpp.BackBufferHeight = d3dmodeTemp.Height;

    // Reset will change the parameters, so use a copy instead.
    D3DPRESENT_PARAMETERS d3dppTmp=d3dpp;
    hr = m_pD3DD9->ResetEx(&d3dppTmp, NULL);
    if (FAILED(hr))
        return MFX_ERR_UNDEFINED_BEHAVIOR;
    m_D3DPP = d3dpp;

    hr = m_pDeviceManager9->ResetDevice(m_pD3DD9, m_resetToken);
    if (FAILED(hr))
        return MFX_ERR_UNDEFINED_BEHAVIOR;

    return MFX_ERR_NONE;
}

void CD3D9Device::Close()
{
    MSDK_SAFE_RELEASE(m_pRenderSurface);
    MSDK_SAFE_RELEASE(m_pDXVAVP_Left);
    MSDK_SAFE_RELEASE(m_pDXVAVP_Right);
    MSDK_SAFE_RELEASE(m_pDXVAVPS);

    MSDK_SAFE_RELEASE(m_pDeviceManager9);
    MSDK_SAFE_RELEASE(m_pD3DD9);
    MSDK_SAFE_RELEASE(m_pD3D9);
    m_pS3DControl = NULL;
}

CD3D9Device::~CD3D9Device()
{
    Close();
}

mfxStatus CD3D9Device::GetHandle(mfxHandleType type, mfxHDL *pHdl)
{
    if (MFX_HANDLE_DIRECT3D_DEVICE_MANAGER9 == type && pHdl != NULL)
    {
        *pHdl = m_pDeviceManager9;

        return MFX_ERR_NONE;
    }
    else if (MFX_HANDLE_GFXS3DCONTROL == type && pHdl != NULL)
    {
        *pHdl = m_pS3DControl;

        return MFX_ERR_NONE;
    }
    return MFX_ERR_UNSUPPORTED;
}

mfxStatus CD3D9Device::SetHandle(mfxHandleType type, mfxHDL hdl)
{
    if (MFX_HANDLE_GFXS3DCONTROL == type && hdl != NULL)
    {
        m_pS3DControl = (IGFXS3DControl*)hdl;

        return MFX_ERR_NONE;
    }
    return MFX_ERR_UNSUPPORTED;
}

mfxStatus CD3D9Device::RenderFrame(mfxFrameSurface1 * pSurface, mfxFrameAllocator * pmfxAlloc)
{
    HRESULT hr = S_OK;
    mfxStatus sts = MFX_ERR_NONE;

    if (2 == m_nViews && NULL == m_pS3DControl)
        return MFX_ERR_UNDEFINED_BEHAVIOR;

    if (!m_overlaySupported)    
        return MFX_ERR_DEVICE_FAILED;    

    MSDK_CHECK_POINTER(pSurface, MFX_ERR_NULL_PTR);
    MSDK_CHECK_POINTER(m_pDeviceManager9, MFX_ERR_NOT_INITIALIZED);
    MSDK_CHECK_POINTER(pmfxAlloc, MFX_ERR_NULL_PTR);
    
    // don't try to render second view if output rect changed since first view
    if ((2 == m_nViews && (0 != pSurface->Info.FrameId.ViewId) && NULL == m_pRenderSurface))
        return MFX_ERR_NONE;

    // First call to create video processors
    if (NULL == m_pDXVAVP_Right)
    {
        sts = CreateVideoProcessors();
        MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
    }

    hr = m_pD3DD9->TestCooperativeLevel();    

    if (SUCCEEDED(hr))
    {
        if (1 == m_nViews || 0 == pSurface->Info.FrameId.ViewId)
        {
            GetClientRect(m_D3DPP.hDeviceWindow, &m_targetRect);
            // Ensure target rect fits back buffer (when window bigger than monitor).
            if (NULL != m_pDeviceManager9 &&
               (m_targetRect.right - m_targetRect.left > (LONG)m_backBufferDesc.Width || 
                m_targetRect.bottom - m_targetRect.top > (LONG)m_backBufferDesc.Height))
            {
                m_targetRect.right = min(m_targetRect.right, m_targetRect.left + (LONG)m_backBufferDesc.Width);
                m_targetRect.bottom = min(m_targetRect.bottom, m_targetRect.top + (LONG)m_backBufferDesc.Height);
            }
        }

        // source rectangle
        RECT source = { pSurface->Info.CropX, pSurface->Info.CropY, 
            pSurface->Info.CropX + pSurface->Info.CropW,
            pSurface->Info.CropY + pSurface->Info.CropH };

        m_BltParams.TargetRect = m_targetRect;
        m_Sample.SrcRect = source;
        m_Sample.DstRect = m_targetRect;

        // get IDirect3DSurface9 handle
        mfxHDL surfHDL = {0};
        pmfxAlloc->GetHDL(pmfxAlloc->pthis, pSurface->Data.MemId, &surfHDL);
        m_Sample.SrcSurface = (IDirect3DSurface9*)surfHDL;

        // select processor based on the view id
        IDirectXVideoProcessor* pVideoProcessor = m_pDXVAVP_Left;
        if (1 == m_nViews || 1 == pSurface->Info.FrameId.ViewId)
        {
            pVideoProcessor = m_pDXVAVP_Right;            
        }

        // this is the key moment: 
        // a new rendering surface must be retrieved for every s3d frame (L+R) and then released
        if (1 == m_nViews || 0 == pSurface->Info.FrameId.ViewId)
        {
            hr = m_pD3DD9->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &m_pRenderSurface);
        }  
        // process the surface
        hr = pVideoProcessor->VideoProcessBlt(m_pRenderSurface, 
            &m_BltParams,
            &m_Sample,
            1,
            NULL); 
        if (1 == m_nViews || 1 == pSurface->Info.FrameId.ViewId)
        {
            MSDK_SAFE_RELEASE(m_pRenderSurface);
        }
    }
    if (SUCCEEDED(hr)&& (1 == m_nViews || pSurface->Info.FrameId.ViewId == 1))
    {
        hr = m_pD3DD9->Present(&m_targetRect, &m_targetRect, NULL, NULL);
    }        

    return SUCCEEDED(hr) ? MFX_ERR_NONE : MFX_ERR_DEVICE_FAILED;
}

mfxStatus CD3D9Device::CreateVideoProcessors()
{
    if (2 == m_nViews && NULL == m_pS3DControl)
        return MFX_ERR_UNDEFINED_BEHAVIOR;

   MSDK_SAFE_RELEASE(m_pDXVAVP_Left);
   MSDK_SAFE_RELEASE(m_pDXVAVP_Right);
   MSDK_SAFE_RELEASE(m_pRenderSurface);

   HRESULT hr ;

   if (2 == m_nViews && NULL != m_pS3DControl)
   {
       hr = m_pS3DControl->SetDevice(m_pDeviceManager9);
       if (FAILED(hr))
       {      
           return MFX_ERR_DEVICE_FAILED;
       }
   }

   ZeroMemory(&m_backBufferDesc, sizeof(m_backBufferDesc));
   IDirect3DSurface9 *backBufferTmp = NULL;
   hr = m_pD3DD9->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBufferTmp);
   if (NULL != backBufferTmp)
       backBufferTmp->GetDesc(&m_backBufferDesc);
   MSDK_SAFE_RELEASE(backBufferTmp);

   if (SUCCEEDED(hr))
   {
       // Create DXVA2 Video Processor Service.
       hr = DXVA2CreateVideoService(m_pD3DD9,
           IID_IDirectXVideoProcessorService,
           (void**)&m_pDXVAVPS);
   }

   if (2 == m_nViews)
   {
        // Activate L channel
        if (SUCCEEDED(hr))
        {
           hr = m_pS3DControl->SelectLeftView();       
        }

        if (SUCCEEDED(hr))
        {
           // Create VPP device for the L channel
           hr = m_pDXVAVPS->CreateVideoProcessor(DXVA2_VideoProcProgressiveDevice,
               &m_VideoDesc,
               OVERLAY_BACKBUFFER_FORMAT,
               1,
               &m_pDXVAVP_Left);
        }

        // Activate R channel
        if (SUCCEEDED(hr))
        {
           hr = m_pS3DControl->SelectRightView();       
        }

   }
   if (SUCCEEDED(hr))
   {
       hr = m_pDXVAVPS->CreateVideoProcessor(DXVA2_VideoProcProgressiveDevice,
           &m_VideoDesc,
           OVERLAY_BACKBUFFER_FORMAT,
           1,
           &m_pDXVAVP_Right);        
   } 

   return SUCCEEDED(hr) ? MFX_ERR_NONE : MFX_ERR_DEVICE_FAILED;   
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
#ifdef WIN64
    CDecodeD3DRender* pRender = (CDecodeD3DRender*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
#else
    CDecodeD3DRender* pRender = (CDecodeD3DRender*)LongToPtr(GetWindowLongPtr(hWnd, GWL_USERDATA));
#endif
    if (pRender)
    {
        switch(message)
        {
            HANDLE_MSG(hWnd, WM_DESTROY, pRender->OnDestroy);
            HANDLE_MSG(hWnd, WM_KEYUP,   pRender->OnKey);
        }
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

CDecodeD3DRender::CDecodeD3DRender()
{
    m_bDwmEnabled = false;
    QueryPerformanceFrequency(&m_Freq);
    MSDK_ZERO_MEMORY(m_LastInputTime);
    m_nFrames = 0;
    m_nMonitorCurrent = 0;

    m_hwdev = NULL;
    MSDK_ZERO_MEMORY(m_sWindowParams);
    m_Hwnd = 0;
    MSDK_ZERO_MEMORY(m_rect);
    m_style = 0;
}

BOOL CALLBACK CDecodeD3DRender::MonitorEnumProc(HMONITOR /*hMonitor*/,
                                           HDC /*hdcMonitor*/,
                                           LPRECT lprcMonitor,
                                           LPARAM dwData)
{
    CDecodeD3DRender * pRender = reinterpret_cast<CDecodeD3DRender *>(dwData);
    RECT r = {0};
    if (NULL == lprcMonitor)
        lprcMonitor = &r;

    if (pRender->m_nMonitorCurrent++ == pRender->m_sWindowParams.nAdapter)
    {
        pRender->m_RectWindow = *lprcMonitor;
    }
    return TRUE;
}

CDecodeD3DRender::~CDecodeD3DRender()
{
    if (m_Hwnd)
        DestroyWindow(m_Hwnd);

    //DestroyTimer();
}

mfxStatus CDecodeD3DRender::Init(sWindowParams pWParams)
{
    // window part
    m_sWindowParams = pWParams;

    WNDCLASS window;
    MSDK_ZERO_MEMORY(window);

    window.lpfnWndProc= (WNDPROC)WindowProc;
    window.hInstance= GetModuleHandle(NULL);;
    window.hCursor= LoadCursor(NULL, IDC_ARROW);
    window.lpszClassName= m_sWindowParams.lpClassName;

    if (!RegisterClass(&window))
        return MFX_ERR_UNKNOWN;

    ::RECT displayRegion = {CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT};

    //right and bottom fields consist of width and height values of displayed reqion
    if (0 != m_sWindowParams.nx )
    {
        EnumDisplayMonitors(NULL, NULL, &CDecodeD3DRender::MonitorEnumProc, (LPARAM)this);

        displayRegion.right  = (m_RectWindow.right - m_RectWindow.left)  / m_sWindowParams.nx;
        displayRegion.bottom = (m_RectWindow.bottom - m_RectWindow.top)  / m_sWindowParams.ny;
        displayRegion.left   = displayRegion.right * (m_sWindowParams.ncell % m_sWindowParams.nx) + m_RectWindow.left;
        displayRegion.top    = displayRegion.bottom * (m_sWindowParams.ncell / m_sWindowParams.nx) + m_RectWindow.top;
    }
    else
    {
        m_sWindowParams.nMaxFPS = 10000;//hypotetical maximum
    }

    //no title window style if required
    DWORD dwStyle = NULL == m_sWindowParams.lpWindowName ?  WS_POPUP|WS_BORDER|WS_MAXIMIZE : WS_OVERLAPPEDWINDOW;

    m_Hwnd = CreateWindow(window.lpszClassName,
                          m_sWindowParams.lpWindowName,
                          dwStyle,
                          displayRegion.left,
                          displayRegion.top,
                          displayRegion.right,
                          displayRegion.bottom,
                          NULL,
                          NULL,
                          GetModuleHandle(NULL),
                          NULL);

    m_Hwnd = CreateWindowEx(NULL,
        m_sWindowParams.lpClassName,
        m_sWindowParams.lpWindowName,
        !m_sWindowParams.bFullScreen ? dwStyle : (WS_POPUP),
        !m_sWindowParams.bFullScreen ? displayRegion.left : 0,
        !m_sWindowParams.bFullScreen ? displayRegion.top : 0,
        !m_sWindowParams.bFullScreen ? displayRegion.right : GetSystemMetrics(SM_CXSCREEN),
        !m_sWindowParams.bFullScreen ? displayRegion.bottom : GetSystemMetrics(SM_CYSCREEN),
        m_sWindowParams.hWndParent,
        m_sWindowParams.hMenu,
        m_sWindowParams.hInstance,
        m_sWindowParams.lpParam);

    if (!m_Hwnd)
        return MFX_ERR_UNKNOWN;

    ShowWindow(m_Hwnd, SW_SHOWDEFAULT);
    UpdateWindow(m_Hwnd);

#ifdef WIN64
    SetWindowLongPtr(m_Hwnd, GWLP_USERDATA, PtrToLong(this));
#else
    SetWindowLong(m_Hwnd, GWL_USERDATA, PtrToLong(this));
#endif
    return MFX_ERR_NONE;
}


mfxStatus CDecodeD3DRender::RenderFrame(mfxFrameSurface1 *pSurface, mfxFrameAllocator *pmfxAlloc)
{
    
    MSG msg;
    MSDK_ZERO_MEMORY(msg);
    while (msg.message != WM_QUIT && PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {        
        TranslateMessage(&msg);
        DispatchMessage(&msg);        
    }    

    RECT rect;
    GetClientRect(m_Hwnd, &rect);
    if (IsRectEmpty(&rect))
        return MFX_ERR_UNKNOWN;

    EnableDwmQueuing();

    mfxStatus sts = m_hwdev->RenderFrame(pSurface, pmfxAlloc);
    MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

    if (NULL != m_sWindowParams.lpWindowName)
    {
        double dfps = 0.;

        if (0 == m_LastInputTime.QuadPart)
        {
            QueryPerformanceCounter(&m_LastInputTime);
        }
        else
        {
            LARGE_INTEGER timeEnd;
            QueryPerformanceCounter(&timeEnd);
            dfps = ++m_nFrames * (double)m_Freq.QuadPart / ((double)timeEnd.QuadPart - (double)m_LastInputTime.QuadPart);
        }
        
        TCHAR str[20];
        _stprintf_s(str, 20, _T("fps=%.2lf"), dfps );
        SetWindowText(m_Hwnd, str);
    }

    return sts;
}

VOID CDecodeD3DRender::OnDestroy(HWND /*hwnd*/)
{
    PostQuitMessage(0);
}

VOID CDecodeD3DRender::OnKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
    if (TRUE == fDown)
        return;

    if ('1' == vk && false == m_sWindowParams.bFullScreen)
        ChangeWindowSize(true);
    else if (true == m_sWindowParams.bFullScreen)
        ChangeWindowSize(false);
}

void CDecodeD3DRender::AdjustWindowRect(RECT *rect)
{
    int cxmax = GetSystemMetrics(SM_CXMAXIMIZED);
    int cymax = GetSystemMetrics(SM_CYMAXIMIZED);
    int cxmin = GetSystemMetrics(SM_CXMINTRACK);
    int cymin = GetSystemMetrics(SM_CYMINTRACK);
    int leftmax = cxmax - cxmin;
    int topmax = cymax - cxmin;
    if (rect->left < 0)
        rect->left = 0;
    if (rect->left > leftmax)
        rect->left = leftmax;
    if (rect->top < 0)
        rect->top = 0;
    if (rect->top > topmax)
        rect->top = topmax;

    if (rect->right < rect->left + cxmin)
        rect->right = rect->left + cxmin;
    if (rect->right - rect->left > cxmax)
        rect->right = rect->left + cxmax;

    if (rect->bottom < rect->top + cymin)
        rect->bottom = rect->top + cymin;
    if (rect->bottom - rect->top > cymax)
        rect->bottom = rect->top + cymax;
}

VOID CDecodeD3DRender::ChangeWindowSize(bool bFullScreen)
{
    WINDOWINFO wndInfo;
    wndInfo.cbSize = sizeof(WINDOWINFO);
    GetWindowInfo(m_Hwnd, &wndInfo);

    if(!m_sWindowParams.bFullScreen)
    {
        m_rect = wndInfo.rcWindow;
        m_style = wndInfo.dwStyle;
    }

    m_sWindowParams.bFullScreen = bFullScreen;

    if(!bFullScreen)
    {
        AdjustWindowRect(&m_rect);
        SetWindowLong(m_Hwnd, GWL_STYLE, m_style);
        SetWindowPos(m_Hwnd, HWND_NOTOPMOST, 
            m_rect.left, m_rect.top, 
            m_rect.right - m_rect.left, m_rect.bottom - m_rect.top, 
            SWP_SHOWWINDOW);
    }
    else
    {
        SetWindowLong(m_Hwnd, GWL_STYLE, WS_POPUP);
        SetWindowPos(m_Hwnd, HWND_NOTOPMOST, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_SHOWWINDOW);
    }
}

bool CDecodeD3DRender::EnableDwmQueuing()
{
    HRESULT hr;

    // DWM queuing is enabled already.
    if (m_bDwmEnabled)
    {
        return true;
    }

    // Check to see if DWM is currently enabled.
    BOOL bDWM = FALSE;

    hr = DwmIsCompositionEnabled(&bDWM);

    if (FAILED(hr))
    {
        _tprintf(_T("DwmIsCompositionEnabled failed with error 0x%x.\n"), hr);
        return false;
    }

    // DWM queuing is disabled when DWM is disabled.
    if (!bDWM)
    {
        m_bDwmEnabled = false;
        return false;
    }

    // Retrieve DWM refresh count of the last vsync.
    DWM_TIMING_INFO dwmti = {0};

    dwmti.cbSize = sizeof(dwmti);

    hr = DwmGetCompositionTimingInfo(NULL, &dwmti);

    if (FAILED(hr))
    {
        _tprintf(_T("DwmGetCompositionTimingInfo failed with error 0x%x.\n"), hr);
        return false;
    }

    // Enable DWM queuing from the next refresh.
    DWM_PRESENT_PARAMETERS dwmpp = {0};

    dwmpp.cbSize                    = sizeof(dwmpp);
    dwmpp.fQueue                    = TRUE;
    dwmpp.cRefreshStart             = dwmti.cRefresh + 1;
    dwmpp.cBuffer                   = 8; //maximum depth of DWM queue
    dwmpp.fUseSourceRate            = TRUE;
    dwmpp.cRefreshesPerFrame        = 1;
    dwmpp.eSampling                 = DWM_SOURCE_FRAME_SAMPLING_POINT;
    dwmpp.rateSource.uiDenominator  = 1;
    dwmpp.rateSource.uiNumerator    = m_sWindowParams.nMaxFPS; 


    hr = DwmSetPresentParameters(m_Hwnd, &dwmpp);

    if (FAILED(hr))
    {
        _tprintf(_T("DwmSetPresentParameters failed with error 0x%x.\n"), hr);
        return false;
    }

    // DWM queuing is enabled.
    m_bDwmEnabled = true;

    return true;
}