#pragma once

#include <d3d9.h>
#include <vector>
#include "Engine.h"
#include "GIS.h"

#include <tchar.h>
#include "pipeline_decode.h"
#include <sstream>

#include "mfxmvc.h"
#include "mfxvideo.h"
#include "mfxvideo++.h"
#include "pipeline_encode.h"
#include "pipeline_user.h"

#include "sample_defs.h"

// Easy access
#define VideoCapturePtr (GamePipe::VideoCapture::GetInstancePointer())
#define VideoCaptureRef (GamePipe::VideoCapture::GetInstanceReference())

//using namespace std;

namespace GamePipe
{
	class VideoCapture : public Singleton<VideoCapture>
	{
	private:
		void* d3dDevice;
		IDirect3DDevice9* castedDXContext;
		IDirect3DSwapChain9* swapChain;
		IDirect3DSurface9* surface;
		Ogre::RenderWindow* rWind;
		//IDirect3DSurface9 surface;
		//vector<IDirect3DSurface9*> surfaceVector;
		bool isRecording;
		int frameNum;

		IDirect3DSurface9* GetFrontBufferData();
		int GetFrame();



		std::auto_ptr<CSmplBitstreamReader>  m_FileReader;
		bool m_bIsCompleteFrame;

		MFXVideoSession     m_mfxSession;
		MFXVideoDECODE*     m_pmfxDEC;
		mfxVideoParam       m_mfxVideoParams; 

		MFXFrameAllocator*      m_pMFXAllocator; 
		mfxAllocatorParams*     m_pmfxAllocatorParams;
		bool                    m_bd3dAlloc; // use d3d surfaces
		bool                    m_bExternalAlloc; // use memory allocator as external for Media SDK
		mfxFrameSurface1*       m_pmfxSurfaces; // frames array
		mfxFrameAllocResponse   m_mfxResponse;  // memory allocation response for decoder
		

	public:
		VideoCapture();
		int Initialize(Ogre::RenderWindow* renderWindow);
		~VideoCapture(void);
		int Destroy();
		bool Update();
		bool OnKeyPress(const GIS::KeyEvent& keyEvent);
		//MSDK
		mfxStatus Decode();
		mfxStatus InitDecoder();
		mfxStatus Encode();
	};

}