#include "StdAfx.h"
#include "VideoCapture.h"
#include "Engine.h"

#include <tchar.h>
#include "pipeline_decode.h"
#include <sstream>


using namespace std;

namespace GamePipe
{
	//////////////////////////////////////////////////////////////////////////
	// VideoCapture(Ogre::RenderWindow* renderWindow)
	//////////////////////////////////////////////////////////////////////////
	VideoCapture::VideoCapture()
	{		
		d3dDevice = NULL;
		castedDXContext = NULL;
		surface = NULL;
		swapChain = NULL;
		isRecording = false;
		frameNum = 0;
		rWind = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize(Ogre::RenderWindow* renderWindow)
	//////////////////////////////////////////////////////////////////////////
	int VideoCapture::Initialize(Ogre::RenderWindow* renderWindow)
	{
		renderWindow->getCustomAttribute("D3DDEVICE", &d3dDevice);
		castedDXContext = reinterpret_cast<IDirect3DDevice9*>(d3dDevice);
		//surfaceVector.reserve(90);		// enough for 3 secs at 30 FPS
		isRecording = false;
		frameNum = 0;
		rWind = renderWindow;

		UtilitiesPtr->Add("VideoCap", new DebugText("Recording frames: no", (Ogre::Real)0.005, (Ogre::Real)0.005, (Ogre::Real)2.5, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));

		return S_OK;
	}


	//////////////////////////////////////////////////////////////////////////
	// ~VideoCapture()
	//////////////////////////////////////////////////////////////////////////
	VideoCapture::~VideoCapture(void)
	{
		Destroy();
	}

	//////////////////////////////////////////////////////////////////////////
	// GetFrontBufferData()
	// Gets front buffer data and places a copy into a IDirect3DSurface9 pointer
	//////////////////////////////////////////////////////////////////////////
	IDirect3DSurface9* VideoCapture::GetFrontBufferData()
	{
		if(castedDXContext == NULL) {		// If NULL, didn't get the context properly
			GGETRACELOG("Did not get DX context properly! Unable to capture front buffer data!");
			return NULL;	// return null for failure
		}

		else {

			//castedDXContext->GetFrontBufferData(1, surface);
			//UINT testNum = castedDXContext->GetNumberOfSwapChains();

			castedDXContext->GetSwapChain(0, &swapChain);

			swapChain->GetFrontBufferData(surface);

			return surface;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// GetFrame()
	// Writes one frame out to frames folder in Core/Bin
	//////////////////////////////////////////////////////////////////////////
	int VideoCapture::GetFrame()
	{

#if 0
		stringstream s;
		char buffer[100];

		sprintf_s(buffer, "frames/frame_%i.jpg", frameNum);
		string num(buffer);

		rWind->writeContentsToFile(buffer);

#else
		string title = "frame.jpg";
		rWind->writeContentsToFile(title);

#endif
		
		frameNum++;

		return S_OK;
	}


	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	int VideoCapture::Destroy()
	{
		d3dDevice = NULL;
		castedDXContext = NULL;
		surface = NULL;
		isRecording = false;
		frameNum = NULL;

		return S_OK;
	}


	//////////////////////////////////////////////////////////////////////////
	// MSDK Decode()
	//////////////////////////////////////////////////////////////////////////
	mfxStatus VideoCapture::Decode()
	{
		dInputParams Params;
		CDecodingPipeline Pipeline;

		// start session
		mfxStatus sts = MFX_ERR_NONE;

		//----starting setting parameters
		Params.videoType = MFX_CODEC_JPEG;
		Params.bd3dAlloc = false;
		Params.bCalLatency = false;
		Params.bIsMVC = false;
		Params.bNoTitle = 0;
		Params.bOutput = true;
		Params.bRendering = false;
		Params.bUseHWLib = false;
		//Params.bUseHWLib = true;
		
		wcscpy_s(Params.strSrcFile, _T("frame.jpg"));  // setting input file
		wcscpy_s(Params.strDstFile, _T("temp_raw.yuv"));  // setting output file

		sts = Pipeline.Init(&Params);
		MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);   

		for (;;)
		{
			sts = Pipeline.RunDecoding();

			if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == sts || MFX_ERR_DEVICE_LOST == sts || MFX_ERR_DEVICE_FAILED == sts)
			{
				if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == sts)
				{
					_tprintf(_T("\nERROR: Incompatible video parameters detected. Recovering...\n"));
				}
				else
				{
					_tprintf(_T("\nERROR: Hardware device was lost or returned unexpected error. Recovering...\n"));
					sts = Pipeline.ResetDevice();
					MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
				}           

				sts = Pipeline.ResetDecoder(&Params);
				MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);            
				continue;
			}        
			else
			{
				MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
				break;
			}
    }
    
    _tprintf(_T("\nDecoding finished\n"));

    Pipeline.Close();
	return MFX_ERR_NONE;
	}


	//////////////////////////////////////////////////////////////////////////
	//MSDK Encode()
	//////////////////////////////////////////////////////////////////////////
	mfxStatus VideoCapture::Encode()
	{
		//std::auto_ptr<CEncodingPipeline>  pPipeline;
		CEncodingPipeline pPipeline;
		eInputParams Params;

		mfxStatus sts = MFX_ERR_NONE; // return value check

		//---- start setting parameters
		unsigned short inputwidth = 800;
		unsigned short inputheight = 600;

		Params.CodecId = MFX_CODEC_MPEG2; // encode into mpeg2
		Params.bd3dAlloc = false; // true - frames in video memory (d3d surfaces)
		Params.bIsMVC = 0;
		Params.bUseHWLib = false;
		Params.nWidth = (mfxU16)inputwidth; // inputwidth need to be specified
		Params.nHeight = (mfxU16)inputheight; // inputheight need to be specified
		Params.dFrameRate = 30;
		Params.ColorFormat = MFX_FOURCC_NV12; // default as NV12
		Params.nPicStruct = MFX_PICSTRUCT_PROGRESSIVE;
		Params.nTargetUsage = MFX_TARGETUSAGE_BALANCED;


		wcscpy_s(Params.strSrcFile, _T("temp_raw.yuv")); // input file is a problem...
		wcscpy_s(Params.strDstFile, _T("output.mpg"));

		// set default value
		mfxU32 nviews = (mfxU32)Params.srcFileBuff.size();
		Params.numViews = nviews;
		Params.nTargetUsage = MFX_TARGETUSAGE_BALANCED; //MFX_TARGETUSAGE_BEST_QUALITY, MFX_TARGETUSAGE_BEST_SPEED
		// if no destination picture width or height wasn't specified set it to the source picture size
			Params.nDstWidth = Params.nWidth;

			Params.nDstHeight = Params.nHeight;
		// calculate default bitrate based on the resolution (a parameter for encoder, so Dst resolution is used)

		Params.nBitRate = CalculateDefaultBitrate(Params.CodecId, Params.nTargetUsage, Params.nDstWidth,
            Params.nDstHeight, Params.dFrameRate);

		//---- Finish setting values

		sts = pPipeline.Init(&Params);
		MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts); 

		for (;;){
			sts = pPipeline.Run();

			if (MFX_ERR_DEVICE_LOST == sts || MFX_ERR_DEVICE_FAILED == sts){            
				_tprintf(_T("\nERROR: Hardware device was lost or returned an unexpected error. Recovering...\n"));
				sts = pPipeline.ResetDevice();
				MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);         

				sts = pPipeline.ResetMFXComponents(&Params);
				MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
				continue;
		    }        
			else
			{            
				MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);
				break;
			}
		}    
		_tprintf(_T("\nEncoding finished\n"));
		pPipeline.Close(); 
		return MFX_ERR_NONE;
	}


	//mfxStatus VideoCapture::InitDecoder()
	//{
	//	eInputParams *pParams;
	//	pParams->videoType = MFX_CODEC_JPEG;

	//	CDecodingPipeline Pipeline;

	//	mfxStatus sts = MFX_ERR_NONE;

	//	// prepare input stream file reader
	//	// creating reader that support completeframe mode
	//	if (pParams->bCalLatency)
	//	{
	//		switch (pParams->videoType)
	//		{
	//		case MFX_CODEC_JPEG:
	//			m_FileReader.reset(new CJPEGFrameReader());
	//			m_bIsCompleteFrame = true;
	//			break;
	//		default:
	//			return MFX_ERR_UNSUPPORTED; // latency mode is supported only for H.264 and JPEG codecs
	//		}
 //       
	//	}
	//	else
	//	{
	//		m_FileReader.reset(new CSmplBitstreamReader());
	//	}

	//	sts = m_FileReader->Init(pParams->strSrcFile);
	//	MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

	//	// init session
	//	//mfxIMPL impl = pParams->bUseHWLib ? MFX_IMPL_HARDWARE : MFX_IMPL_SOFTWARE;
	//	mfxIMPL impl = MFX_IMPL_SOFTWARE;

	//	// API version
	//	mfxVersion ver10 = {0, 1};
	//	mfxVersion ver13 = {3, 1};    
	//	mfxVersion version;
	//	//version = (m_bIsMVC || pParams->bCalLatency || pParams->videoType == MFX_CODEC_JPEG) ? ver13 : ver10; 
	//	version = ver13;

	//	sts = m_mfxSession.Init(impl, &version);
	//	MSDK_CHECK_RESULT(sts, MFX_ERR_NONE, sts);

	//	// create decoder
	//	m_pmfxDEC = new MFXVideoDECODE(m_mfxSession);
	//	MSDK_CHECK_POINTER(m_pmfxDEC, MFX_ERR_MEMORY_ALLOC);

	//	// set video type in parameters
	//	m_mfxVideoParams.mfx.CodecId = pParams->videoType; 
	//	// set memory type
	//	m_bd3dAlloc = pParams->bd3dAlloc;


	//}





	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool VideoCapture::Update()
	{
		if(InputPtr->IsKeyDown(GIS::KC_F8)) {
			isRecording = true;
			//GetFrame();
			UtilitiesPtr->GetDebugText("VidCap")->SetText("Recording frames: yes");
		}

		else if(InputPtr->IsKeyDown(GIS::KC_F9)) {
			isRecording = false;
			frameNum = 0;
			UtilitiesPtr->GetDebugText("VidCap")->SetText("Recording frames: no");
			Decode();
			Encode();
		}

		if(isRecording) {

			//GetFrontBufferData();
			GetFrame();
		}


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	/*bool OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_F8:
			{
				GGETRACE("I'M PRESSING F8");
				break;
			}
		}

		return true;
	}*/
}
