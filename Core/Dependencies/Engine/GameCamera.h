
#pragma once

// Engine
#include "Engine.h"

enum CameraType 
{
	FPS,TPS,RTS,SUR,DEF
};
namespace GamePipeGame
{
class GameCamera
{
	// Attributes ------------------------------------------------------------------------------
	protected:
		Ogre::SceneNode *mTargetNode; // The camera target
        Ogre::SceneNode *mCameraNode; // The camera itself
		Ogre::Camera *mCamera; // Ogre camera
		Ogre::SceneNode *mNextCameraNode;
		Ogre::SceneManager *mSceneMgr;
		Ogre::String mName;
		Ogre::String mType;
		bool mOwnCamera; // To know if the ogre camera binded has been created outside or inside of this class
		float fract;
		Ogre::Real mTightness; // Determines the movement of the camera - 1 means tight movement, while 0 means no movement
		CameraType movingto;
		Ogre::SceneNode *FPSNode;
		Ogre::SceneNode *TPSNode;
		Ogre::Vector3 pos;
	public:
// Methods ---------------------------------------------------------------------------------
	protected:
	public:
		GameCamera (Ogre::String name, Ogre::SceneManager *sceneMgr, Ogre::Camera *camera=0, CameraType type=DEF)
		{
			// Basic member references setup
			fract=0;
			mName = name;
			mSceneMgr = sceneMgr;
			// Create the camera's node structure
			mTargetNode = mSceneMgr->getRootSceneNode ()->createChildSceneNode (mName+ "_target");
			mCameraNode = mTargetNode->createChildSceneNode (mName );
			mNextCameraNode=mTargetNode->createChildSceneNode();
			FPSNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
			FPSNode->setPosition(Ogre::Vector3(0,0,0));
			TPSNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
			TPSNode->setPosition(Ogre::Vector3(-50,0,800));
			mNextCameraNode->setPosition(0,0,0);
			
			pos = Ogre::Vector3(-50,0,800);

			if(type==FPS)
			{
				mCameraNode->setPosition(0,0,0);
			//	mCameraNode = FPSNode;
				//mCameraNode
				movingto=FPS;
				mCameraNode->setAutoTracking (true, mTargetNode);
			}
			else if(type==TPS)
			{
				mCameraNode->setAutoTracking (true, mTargetNode);
				 // The camera will always look at the camera target
				mCameraNode->setFixedYawAxis (true); // Needed because of auto tracking
				mCameraNode->setPosition(-50,0,800);
		//		mCameraNode = TPSNode;
				//mCameraNode->lookAt(Vector3(-200,-300,0));
				//mCameraNode->setNearClipDistance(10);
				movingto=TPS;
			}
			else if(type==RTS)
			{
				mCameraNode->setPosition(0,1500,0);
				movingto=RTS;
			}
			else if(type==SUR)
			{
				mCameraNode->setPosition(-100,200,100);

			}
			// Create our camera if it wasn't passed as a parameter
			if (camera == 0) 
			{
				mCamera = mSceneMgr->createCamera (mName);
				mOwnCamera = true;
			}
			else
			{
				mCamera = camera;
			    // just to make sure that mCamera is set to 'origin' (same position as the mCameraNode)
		        mCamera->setPosition(0.0,0.0,0.0);
				mCamera->lookAt(mTargetNode->getPosition());
				mCamera->setNearClipDistance(10);
				mOwnCamera = false;
			}
			// ... and attach the Ogre camera to the camera node
			mCameraNode->attachObject (mCamera);
			mCamera->lookAt(0,0,0);
			// Default tightness
			mTightness = 0.01f;
		}
		~GameCamera()
		{
			mCameraNode->detachAllObjects();
			if (mOwnCamera)
				delete mCamera;
			mSceneMgr->destroySceneNode (mName);
			mSceneMgr->destroySceneNode (mName + "_target");
		}

		void GameCamera::Update()
		{
			mNextCameraNode->setPosition(pos);
			mNextCameraNode->lookAt(mTargetNode->getPosition(), Ogre::Node::TS_WORLD);
			//mCamera->lookAt(0,0,0);
			//mCameraNode->setPosition(Vector3(0,0,0));
			if(fract<1)
			{	fract+=0.2f;
			interpolate(fract,mCameraNode,mNextCameraNode);
			}
			if(fract>=1)
				fract=0;
		}
		
		void GameCamera::interpolate(Ogre::Real fraction, Ogre::SceneNode *startScene, Ogre::SceneNode *finalScene)
		{
			Ogre::Quaternion inirot=startScene->getOrientation();//m_qCameraOrientation; 
			Ogre::Quaternion destrot=finalScene->getOrientation();
			Ogre::Vector3 inipos=startScene->getPosition();//m_v3CameraPosition;
			Ogre::Vector3 destpos=finalScene->getPosition();
			Ogre::Quaternion nrot=Ogre::Quaternion::nlerp(fraction,inirot,destrot,true);
			Ogre::Vector3 direction=destpos-inipos;
			if(direction.length()>2)
			{
				direction.normalise();
				direction*=2;
			}
			Ogre::Vector3 npos=inipos+direction;

			mCameraNode->setPosition(npos);
			mCameraNode->setOrientation(nrot);

		}

		void setTightness (Ogre::Real tightness)
		{
			mTightness = tightness;
		}

		Ogre::Real getTightness ()
		{
			return mTightness;
		}


		Ogre::Vector3 getCameraPosition ()
		{
			return mCameraNode->getPosition ();
		}

		void instantUpdate (Ogre::Vector3 cameraPosition, Ogre::Vector3 targetPosition)
		{
			mCameraNode->setPosition (cameraPosition);
			mTargetNode->setPosition (targetPosition);
		}


		void update (Ogre::Real elapsedTime, Ogre::Vector3 cameraPosition, Ogre::Vector3 targetPosition)
		{
			// Handle movement
			Ogre::Vector3 displacement;
			displacement = (cameraPosition - mCameraNode->getPosition ()) * mTightness;
			mCameraNode->translate (displacement);
			displacement = (targetPosition - mTargetNode->getPosition ()) * mTightness;
			mTargetNode->translate (displacement);
		}

		void attachEntity(Ogre::Entity * targetEntity)
		{
			mTargetNode->attachObject(targetEntity);
		}

		void moveToFPS()
		{
			pos = Ogre::Vector3(0,0,0);
		//	mNextCameraNode->setPosition(0,0,0);
			//mCamera->lookAt(0,0,0);
			//mCameraNode->setPosition(Vector3(0,0,0));
			
		//	if(fract<1)
		//	{	fract+=0.01;
		//		interpolate(fract,mCameraNode,mNextCameraNode);
		//	}
		//	if(fract>=1)
		//		fract=0;
		}

		void moveToTPS()
		{
            pos = Ogre::Vector3(-50,0,800);		
		//	mNextCameraNode->setPosition(-50,0,800);
			//mCamera->lookAt(0,0,0);
			//mCameraNode->setPosition(Vector3(0,0,0));
		//	if(fract<1)
		//	{	fract+=0.01;
		//	interpolate(fract,mCameraNode,mNextCameraNode);
		//	}
		//	if(fract>=1)
		//		fract=0;
		}

		void moveToRTS()
		{
		// mCameraNode->setPosition(Vector3(0,1500,0));
			
            pos = Ogre::Vector3(0,1500,0);		
		//	mNextCameraNode->setPosition(0,1500,0);
		 //mCameraNode->setPosition(Vector3(0,0,0));
		 //mCamera->lookAt(0,0,0);
		 //if(fract<1)
		// {	fract+=0.01;
		 //interpolate(fract,mCameraNode,mNextCameraNode);
		//	}
	//	 if(fract>=1)
	//		 fract=0;
		}
};
}