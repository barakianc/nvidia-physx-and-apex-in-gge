#include "StdAfx.h"
#include "GameObject.h"

#ifdef HAVOK
#include "HavokObject.h"
#endif

#ifdef PHYSX
#include "PhysXObject.h"
#endif

#include "AnimationManager.h"
#include "..\AI\AIAgent.h"
#include "..\AI\AIAgentManager.h"
#include "AIWorldManager.h"
#include "dtAIAgent.h"

#ifdef TBB_PARALLEL_OPTION
#include "TaskTypes.h"
#include "AllClear.h"
#include "CreatePhantomAabbObjectTask.h"
#include "CreateCharacterProxyObjectTask.h"
#include "CreateCharacterRigidBodyObjectTask.h"
#include "CreateHavokAnimationObjectTask.h"
#include "CreatePhantomShapeTask.h"

#endif

using namespace std;

///Deconstructor of the GameObject
GameObject::~GameObject()
{
    // AI requires working animation, physics, graphisc etc to delete.  It must be deleted first.
    if (m_AIAgent!=NULL)
        delete(m_AIAgent);
	//RDS_LOOK this function needs to be revised..I think it should call the deconstructors of ifs objects if they are not NULL.
	if (m_pAnimatedEntity)
	{
		AnimationManager::Manager::GetSingleton()->DeleteAnimationObject(m_pAnimatedEntity->GetName());
	}
#ifdef HAVOK
	if (m_pCollisionListener!=NULL)
		delete(m_pCollisionListener);
	if (m_pPhysicsObject!=NULL)
		m_pPhysicsObject->destroy();
#endif	
	if (m_pGraphicsObject!=NULL)
		delete(m_pGraphicsObject);
}

/*************************************************GameObjectDefinitionsSTART************************************************/
///This function initializes all class member variables to NULL
void GameObject::initializeGameObject() {
	m_bRemoveGameObjectBool = false;	///To be able to remove GameObject at any time for efficiency purposes
#ifdef HAVOK
	m_pPhysicsObject		= NULL;		///An instance of the PhysicsObject
	m_pCollisionListener	= NULL;		///For collision detection purposes
#endif
#ifdef PHYSX
	m_pPhysicsObject		= NULL;
#endif
	m_pGraphicsObject		= NULL;		///An instance of the GraphicsObject
	m_pAnimatedEntity		= NULL;		///An instance of the AnimatedEntity
    m_AIAgent               = NULL;     ///An instance of the AI Agent
	m_dtAIAgent				= NULL;		///AN INSTANCE OF DETOUR AI Agent
	m_sHKXFileName			= "";		///If we will scale a havok object we have to kill it and re-create so we will need this

    m_eObjectType = DEFAULT_GAME_OBJECT_TYPE;
    m_eCollisionShapeType = DEFAULT_COLLISION_SHAPE;

#ifdef TBB_PARALLEL_OPTION
	createPhysicsNow		= 0;
	createAnimNow			= 0;
	physObject				= NULL;
	animObject				= NULL;
	physicsRdy				= false;
	animRdy					= false;
#endif
}

//CONSTRUCTOR_GAMEOBJECT_0-->if you search for this line in GameObject.cpp it takes you to the start of this constructor
GameObject::GameObject(
						   std::string				f_sOgreUniqueName,
						   std::string				f_sMeshFileName,
						   std::string				f_sHKXFileName,			//="",
						   GameObjectType			f_eObjectType,			//=DEFAULT_GAME_OBJECT_TYPE,
						   CollisionShapeType		f_eCollisionShapeType,	//=DEFAULT_COLLISION_SHAPE,
                           std::string				f_sLUAFileName,			//=""
                           bool                     f_addAI
					   )
{
	//first initialize the GameObject--> make every class member NULL
	initializeGameObject();

	//now get the new values which are passed through constructor
	m_eObjectType		=	f_eObjectType;
	m_sHKXFileName		=	f_sHKXFileName;
	m_eCollisionShapeType = f_eCollisionShapeType;

	//Grab the current gameObjectManager and sceneManager from Engine
	GamePipe::GameScreen* f_pCurrentGameScreen = EnginePtr->GetForemostGameScreen();
	GameObjectManager* f_pGameObjectManager = f_pCurrentGameScreen->GetGameObjectManager();
    GamePipe::AIAgentManager *aiAgentManager = f_pGameObjectManager->getAIAgentManager();
	//-----------------------
	AIWorldManager *recastManager = f_pGameObjectManager->getAIWorldManager();
	this->indexInRecastWorld = -1;

	//check for a valid hkx file name
	int f_iHkxFilenameController = f_sHKXFileName.find(".hkx");
	if (f_iHkxFilenameController == string::npos)
		f_iHkxFilenameController = f_sHKXFileName.find(".HKX");
	if (f_iHkxFilenameController == string::npos)
		f_iHkxFilenameController = f_sHKXFileName.find(".xml");
	if (f_iHkxFilenameController == string::npos)
		f_iHkxFilenameController = f_sHKXFileName.find(".XML");

	//check for a valid mesh or dae file name-->not used below for now because not needed for now
	int f_iMeshFilenameController = f_sMeshFileName.find(".mesh");
	if (f_iMeshFilenameController == string::npos)
		f_iMeshFilenameController = f_sMeshFileName.find(".MESH");
	if (f_iMeshFilenameController == string::npos)
		f_iMeshFilenameController = f_sMeshFileName.find(".dae");
	if (f_iMeshFilenameController == string::npos)
		f_iMeshFilenameController = f_sMeshFileName.find(".DAE");

	//first lets check if there are neither hkx file nor mesh file
	if (f_iHkxFilenameController == string::npos)
	{
		//if I am in this if statement
		//this means havok fileName is not given --> I understand this by looking if the string passed to me has .hkx file
		//so look at the object type. if it is not passed, then it has to be a graphics object
		if ((f_eObjectType==DEFAULT_GAME_OBJECT_TYPE)&&(f_eCollisionShapeType==DEFAULT_COLLISION_SHAPE))
			f_eObjectType = GRAPHICS_OBJECT;
		m_eObjectType = f_eObjectType;

		//according to GameObject type use different constructors to create PhysicsObject of the GameObject
		switch (m_eObjectType)
		{
			case  DEFAULT_GAME_OBJECT_TYPE:
				//most probably a CollisionShape is passed with a mesh and no object type is mentioned
				break;
			case  GRAPHICS_OBJECT:
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				if (m_pGraphicsObject->m_pOgreSkeleton!=NULL)
					m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateOgreAnimationObject(m_pGraphicsObject, f_sMeshFileName.c_str(), f_sOgreUniqueName.c_str()));
				break;
			case PHYSICS_FIXED://these are for creating a rigid body from a mesh shape according to f_eCollisionShapeType
			case PHYSICS_DYNAMIC:
			case PHYSICS_KEYFRAMED:
			case PHYSICS_SYSTEM:
#ifdef HAVOK
				m_pPhysicsObject = new PhysicsPrimitiveObject(f_sOgreUniqueName,f_sMeshFileName,f_sHKXFileName,f_eObjectType,f_eCollisionShapeType,&m_pGraphicsObject);
				//now if the GameObject type was not passed to me, I would get it from the created PhysicsObject
				//if (m_pPhysicsObject!=NULL)
				////	m_eObjectType = ((PhysicsPrimitiveObject* )m_pPhysicsObject)->getObjectType();
#endif
#ifdef PHYSX
				m_pPhysicsObject = new PhysicsPrimitiveObject(f_sOgreUniqueName,f_sMeshFileName,f_sHKXFileName,f_eObjectType,f_eCollisionShapeType,&m_pGraphicsObject);
#endif
				break;
			case ANIMATED_CHARACTER_PROXY:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				//if it is a ANIMATED_CHARACTER_PROXY then first create animated entity and then DON'T PUT a break point and create
				//PHYSICS_CHARACTER_PROXY instance too
#ifdef TBB_PARALLEL_OPTION
				if(!EnginePtr->initializing)
				{
					std::string path = f_sHKXFileName;
					std::string name = f_sOgreUniqueName;
					CreateCharacterProxyObjectTask* newTask2 = new CreateCharacterProxyObjectTask(this, m_pGraphicsObject);
					physObject = new TaskContainer((void*)newTask2,Create_CharacterProxyObject_Task);
					createPhysicsNow++;
					AllClear* newTask3 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask3,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
					m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateOgreAnimationObject(m_pGraphicsObject, f_sMeshFileName.c_str(), f_sOgreUniqueName.c_str()));
				}
				else
				{
					m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateOgreAnimationObject(m_pGraphicsObject, f_sMeshFileName.c_str(), f_sOgreUniqueName.c_str()));
					m_pPhysicsObject = new CharacterProxyObject(m_pGraphicsObject);
				}
#else
				m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateOgreAnimationObject(m_pGraphicsObject, f_sMeshFileName.c_str(), f_sOgreUniqueName.c_str()));
				m_pPhysicsObject = new CharacterProxyObject(m_pGraphicsObject);
#endif
#endif
#ifdef PHYSX
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				m_pPhysicsObject = new CharacterObject(m_pGraphicsObject);
#endif
				break;
			case PHYSICS_CHARACTER_PROXY:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
#ifdef TBB_PARALLEL_OPTION
				if(!EnginePtr->initializing)
				{
					CreateCharacterProxyObjectTask* newTask = new CreateCharacterProxyObjectTask(this, m_pGraphicsObject);
					physObject = new TaskContainer((void*)newTask,Create_CharacterProxyObject_Task);
					createPhysicsNow++;
					AllClear* newTask2 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask2,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
				}
				else
					m_pPhysicsObject = new CharacterProxyObject(m_pGraphicsObject);
#else
				m_pPhysicsObject = new CharacterProxyObject(m_pGraphicsObject);
#endif
#endif
#ifdef PHYSX
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				m_pPhysicsObject = new CharacterObject(m_pGraphicsObject);
#endif
				break;
			case ANIMATED_CHARACTER_RIGIDBODY:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				//if it is a ANIMATED_CHARACTER_RIGIDBODY then first create animated entity
				//PHYSICS_CHARACTER_RIGIDBODY instance too
				m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateOgreAnimationObject(m_pGraphicsObject, f_sMeshFileName.c_str(), f_sOgreUniqueName.c_str()));
#ifdef TBB_PARALLEL_OPTION
				if(!EnginePtr->initializing)
				{
					CreateCharacterRigidBodyObjectTask* newTask = new CreateCharacterRigidBodyObjectTask(this, m_pGraphicsObject);
					physObject = new TaskContainer((void*)newTask,Create_CharacterRigidBodyObject_Task);
					AllClear* newTask2 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask2,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
				}
				else
					m_pPhysicsObject = new CharacterRigidBodyObject(m_pGraphicsObject);
#else
				m_pPhysicsObject = new CharacterRigidBodyObject(m_pGraphicsObject);
#endif
#endif
#ifdef PHYSX
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				m_pPhysicsObject = new CharacterObject(m_pGraphicsObject);
#endif
				break;
			case PHYSICS_CHARACTER_RIGIDBODY:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
#ifdef TBB_PARALLEL_OPTION
				if(!EnginePtr->initializing)
				{
					CreateCharacterRigidBodyObjectTask* newTask = new CreateCharacterRigidBodyObjectTask(this, m_pGraphicsObject);
					physObject = new TaskContainer((void*)newTask,Create_CharacterRigidBodyObject_Task);
					AllClear* newTask2 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask2,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
				}
				else
					m_pPhysicsObject = new CharacterRigidBodyObject(m_pGraphicsObject);
#else
				m_pPhysicsObject = new CharacterRigidBodyObject(m_pGraphicsObject);
#endif
#endif
#ifdef PHYSX
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				m_pPhysicsObject = new CharacterObject(m_pGraphicsObject);
#endif
				break;
			case PHANTOM_AABB:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
#ifdef TBB_PARALLEL_OPTION
			if(!EnginePtr->initializing)
			{
				CreatePhantomAabbObjectTask* newTask = new CreatePhantomAabbObjectTask(this,m_pGraphicsObject);
				physObject = new TaskContainer((void*)newTask,Create_PhantomAabbObject_Task);
				createPhysicsNow++;
				AllClear* newTask2 = new AllClear(this);
				TaskContainer* container = new TaskContainer((void*)newTask2,All_Clear);
				EnginePtr->taskQueue.push(container);
				EnginePtr->taskQueueSize++;
			}
			else
				m_pPhysicsObject = new PhantomAabbObject(m_pGraphicsObject);
#else
				m_pPhysicsObject = new PhantomAabbObject(m_pGraphicsObject);
#endif
#endif
				break;
			case PHANTOM_SHAPE:
#ifdef HAVOK
#ifdef TBB_PARALLEL_OPTION
				m_pGraphicsObject = new GraphicsObject(f_sOgreUniqueName,f_eCollisionShapeType);
				if(!EnginePtr->initializing)
				{
					CreatePhantomShapeTask* newTask = new CreatePhantomShapeTask(this,f_sOgreUniqueName,f_eCollisionShapeType,m_pGraphicsObject);
					physObject = new TaskContainer((void*)newTask,Create_Phantom_Shape_Task);
					createPhysicsNow++;
					AllClear* newTask2 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask2,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
				}
				else
					m_pPhysicsObject = new PhantomShapeObject(f_sOgreUniqueName,f_eCollisionShapeType,&m_pGraphicsObject);
#else
				m_pPhysicsObject = new PhantomShapeObject(f_sOgreUniqueName,f_eCollisionShapeType,&m_pGraphicsObject);
#endif
#endif
				break;
			case VEHICLE_CAR:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				m_pPhysicsObject = new VehicleObject(VEHICLE_CAR, m_pGraphicsObject);
#endif
				break;
			case VEHICLE_MOTORCYCLE:
#ifdef HAVOK
				m_pGraphicsObject = new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
				m_pPhysicsObject = new VehicleObject(VEHICLE_MOTORCYCLE,m_pGraphicsObject);
#endif
				break;
				//GGETRACELOG("HavokError in CreatePhysicsObject Function.VehicleMotorCycle Type has not been implemented yet.");
				//exit(1);
			default :
				GGETRACELOG("HavokError in CreatePhysicsObject Function");
				exit(1);
		};

#ifdef HAVOK
		if ((m_pPhysicsObject==NULL)&&(m_eObjectType != GRAPHICS_OBJECT))
		{
			m_pPhysicsObject = new PhysicsPrimitiveObject(f_sOgreUniqueName,f_sMeshFileName,f_sHKXFileName,f_eObjectType,f_eCollisionShapeType,&m_pGraphicsObject);
			//now if the GameObject type was not passed to me, I would get it from the created PhysicsObject
		//	if (m_pPhysicsObject!=NULL)
		//		m_eObjectType = ((PhysicsPrimitiveObject* )m_pPhysicsObject)->getObjectType();
		}
#endif

#ifdef HAVOK
        if((f_sLUAFileName != "" || f_addAI) && !AIWorldManager::useRecast){
            m_AIAgent = new GamePipe::AIAgent(f_sLUAFileName, m_pGraphicsObject, m_pPhysicsObject, m_pAnimatedEntity);
            m_AIAgent->initialize();
            aiAgentManager->addAgent(m_AIAgent);
        }
#endif
		//now every aspect of GameObject has been created
		//so add this gameObject to list and return
		f_pGameObjectManager->AddGameObject(this);

		if (!(f_sLUAFileName != "" || f_addAI) && AIWorldManager::useRecast)
			recastManager->AddToWorld(this);
		
		if (f_addAI && AIWorldManager::useRecast)
		{
			float pos[] = {0.0f,0.0f, 0.0f};
			if(f_eObjectType == ANIMATED_CHARACTER_RIGIDBODY)
				m_dtAIAgent = recastManager->addNewAgent(pos,f_sMeshFileName,f_sHKXFileName,f_sMeshFileName,(char*)f_sLUAFileName.c_str(),"none","ANIMATED_CHARACTER_RIGIDBODY",this);
			else if(f_eObjectType == GRAPHICS_OBJECT)
				m_dtAIAgent = recastManager->addNewAgent(pos,f_sMeshFileName,f_sHKXFileName,f_sMeshFileName,(char*)f_sLUAFileName.c_str(),"none","GRAPHICS_OBJECT",this);
			aiAgentManager->addDtAgent(m_dtAIAgent);
		}
		
		return;
	}
	else {
		//if I am in this else statement this means I am given a .hkx file to read in and this is not an animated entity
		//so I have to check if the object type is mentioned
		//if it is mentioned this means I will have to overWrite the rigidBody info according to the type which is passed to me

		switch (f_eObjectType)
		{
			case DEFAULT_GAME_OBJECT_TYPE://if no object type defined --> use PhysicsObject because a .hkx file is passed and I am here
			case PHYSICS_FIXED:
			case PHYSICS_DYNAMIC:
			case PHYSICS_KEYFRAMED:
			case PHYSICS_SYSTEM:
#ifdef HAVOK
				m_pPhysicsObject = new PhysicsPrimitiveObject(f_sOgreUniqueName,f_sMeshFileName,f_sHKXFileName,f_eObjectType,f_eCollisionShapeType,&m_pGraphicsObject);
				//now if the GameObject type was not passed to me, I would get it from the created PhysicsObject
			//	if (m_pPhysicsObject!=NULL)
			//		m_eObjectType = ((PhysicsPrimitiveObject* )m_pPhysicsObject)->getObjectType();
#endif
#ifdef PHYSX
				m_pPhysicsObject = new PhysicsPrimitiveObject(f_sOgreUniqueName,f_sMeshFileName,f_sHKXFileName,f_eObjectType,f_eCollisionShapeType,&m_pGraphicsObject);
#endif
				break;
			case ANIMATED_CHARACTER_PROXY:
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
#ifdef TBB_PARALLEL_OPTION
				if(!EnginePtr->initializing)
				{
					std::string path = f_sHKXFileName;
					std::string name = f_sOgreUniqueName;
					CreateHavokAnimationObjectTask* newTask = new CreateHavokAnimationObjectTask(this,path,name,m_pGraphicsObject);
					CreateCharacterProxyObjectTask* newTask2 = new CreateCharacterProxyObjectTask(this, m_pGraphicsObject);
					animObject = new TaskContainer((void*)newTask,Create_HavokAnimationObject_Task);
					physObject = new TaskContainer((void*)newTask,Create_CharacterProxyObject_Task);
					createPhysicsNow++;
					createAnimNow++;
					AllClear* newTask3 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask3,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
				}
				else
				{
					m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateHavokAnimationObject(m_pGraphicsObject,f_sHKXFileName.c_str(), f_sOgreUniqueName.c_str()));
					m_pPhysicsObject = new CharacterProxyObject(m_pGraphicsObject);
				}
#else
				m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateHavokAnimationObject(m_pGraphicsObject,f_sHKXFileName.c_str(), f_sOgreUniqueName.c_str()));
				m_pPhysicsObject = new CharacterProxyObject(m_pGraphicsObject);
#endif
#endif
				break;
			case ANIMATED_CHARACTER_RIGIDBODY :
#ifdef HAVOK
				m_pGraphicsObject =	new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eObjectType);
#ifdef TBB_PARALLEL_OPTION
				if(!EnginePtr->initializing)
				{
					std::string path = f_sHKXFileName;
					std::string name = f_sOgreUniqueName;
					CreateHavokAnimationObjectTask* newTask = new CreateHavokAnimationObjectTask(this,path,name,m_pGraphicsObject);
					CreateCharacterRigidBodyObjectTask* newTask2 = new CreateCharacterRigidBodyObjectTask(this, m_pGraphicsObject);
					physObject = new TaskContainer((void*)newTask2,Create_CharacterRigidBodyObject_Task);
					animObject = new TaskContainer((void*)newTask,Create_HavokAnimationObject_Task);
					createPhysicsNow++;
					createAnimNow++;
					AllClear* newTask3 = new AllClear(this);
					TaskContainer* container = new TaskContainer((void*)newTask3,All_Clear);
					EnginePtr->taskQueue.push(container);
					EnginePtr->taskQueueSize++;
				}
				else
				{
					m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateHavokAnimationObject(m_pGraphicsObject,f_sHKXFileName.c_str(), f_sOgreUniqueName.c_str()));
					m_pPhysicsObject = new CharacterRigidBodyObject(m_pGraphicsObject);
				}
#else
				m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateHavokAnimationObject(m_pGraphicsObject,f_sHKXFileName.c_str(), f_sOgreUniqueName.c_str()));
				m_pPhysicsObject = new CharacterRigidBodyObject(m_pGraphicsObject);
#endif
#endif
				break;
			default :
				//GGETRACELOG
				break;
		};
#ifdef HAVOK
        if((f_sLUAFileName != "" ) && !AIWorldManager::useRecast){
            m_AIAgent = new GamePipe::AIAgent(f_sLUAFileName, m_pGraphicsObject, m_pPhysicsObject, m_pAnimatedEntity);
            m_AIAgent->initialize();
            aiAgentManager->addAgent(m_AIAgent);
        }
#endif

		//now every aspect of GameObject has been created
		//so add this gameObject to list and return
		f_pGameObjectManager->AddGameObject(this);
		if (!f_addAI && AIWorldManager::useRecast)
			recastManager->AddToWorld(this);

		if (f_addAI && AIWorldManager::useRecast)
		{
			float pos[] = {0.0f,0.0f, 0.0f};
			if(f_eObjectType == ANIMATED_CHARACTER_RIGIDBODY)
				m_dtAIAgent = recastManager->addNewAgent(pos,f_sMeshFileName,f_sHKXFileName,f_sMeshFileName,(char*)f_sLUAFileName.c_str(),"none","ANIMATED_CHARACTER_RIGIDBODY",this);
			if(f_eObjectType == GRAPHICS_OBJECT)
				m_dtAIAgent = recastManager->addNewAgent(pos,f_sMeshFileName,f_sHKXFileName,f_sMeshFileName,(char*)f_sLUAFileName.c_str(),"none","GRAPHICS_OBJECT",this);
		}

		return;
	}
	GGETRACELOG("Problem in GameObjectConstructor.Nothing has been constructed in the GameObjectConstructor.");
	exit(2);
}

void GameObject::setAkGameObjectID(int gameObjID)
{
	m_AkGameObjectID = gameObjID;
}

void GameObject::setPosition(float x,float y,float z) {

	#ifdef HAVOK
		if (this->m_pPhysicsObject!=NULL)
		{
			hkVector4 newObjPos(x,y,z);
			this->m_pPhysicsObject->setPosition(newObjPos);
		}
	#endif 

	#ifdef PHYSX
		if (this->m_pPhysicsObject!=NULL)
		{
			physx::PxVec3 newObjPos(x,y,z);
			this->m_pPhysicsObject->setPosition(newObjPos);
		}
	#endif
		else if (this->m_pAnimatedEntity!=NULL)
		{
			this->m_pAnimatedEntity->SetPosition(x,y,z);
		}
		else {//just a graphics object
			this->m_pGraphicsObject->setPosition(x,y,z);
		}
		if(AIWorldManager::useRecast)
		{
			GamePipe::GameScreen* f_pCurrentGameScreen = EnginePtr->GetForemostGameScreen();
			GameObjectManager* f_pGameObjectManager = f_pCurrentGameScreen->GetGameObjectManager();
			AIWorldManager *f_AIWorldManager = f_pGameObjectManager->getAIWorldManager();
			f_AIWorldManager->TranslateMesh(Ogre::Vector3(x,y,z),this->indexInRecastWorld,this,false);
		}

}

Ogre::Vector3 GameObject::getPosition()
{
	return Ogre::Vector3(m_pGraphicsObject->m_pOgreSceneNode->getPosition());
}
void GameObject::setVisible(bool f_bVisibilityBool){
	this->m_pGraphicsObject->setVisible(f_bVisibilityBool);
}

void GameObject::changeSceneNode(Ogre::SceneNode* f_pNewSceneNode)
{
	m_pGraphicsObject->changeSceneNode(f_pNewSceneNode);
}
#ifdef TBB_PARALLEL_OPTION
void GameObject::DelayedCreation()
{
	if (createPhysicsNow > 0 && physicsRdy)
	{
		physObject->Process();
		createPhysicsNow--;
	}
	if(createAnimNow > 0&& animRdy)
	{
		animObject->Process();
		createAnimNow--;
	}
}
#endif

bool GameObject::update()
{
	//sometimes physics representation of the gameObject can get out of bounds the physics world
	//then this will be set to false so that GameObjectManager will delete that gameobject from the list
	bool f_bObjectIsStillInPhysicsScene=true;

#ifdef TBB_PARALLEL_OPTION
	if(createAnimNow > 0 || createPhysicsNow > 0)
		DelayedCreation();
#endif
	if (m_pAnimatedEntity!=NULL)
	{
		m_pAnimatedEntity->Update(EnginePtr->GetDeltaTime());
	}
#ifdef HAVOK
	if (m_pPhysicsObject!=NULL)
	{
		f_bObjectIsStillInPhysicsScene = m_pPhysicsObject->update();
	}
#endif

#ifdef PHYSX
	if (m_pPhysicsObject!=NULL)
	{
		f_bObjectIsStillInPhysicsScene = m_pPhysicsObject->update();
	}
#endif
	return f_bObjectIsStillInPhysicsScene;
}

#ifdef HAVOK
void GameObject::addCollisionListener()
{
	if (m_pPhysicsObject!=NULL)
	{
		if (m_pPhysicsObject->getRigidBody()!=NULL)
			m_pCollisionListener = new CollisionListener(this,m_pPhysicsObject->getRigidBody());
	}
}
#endif

#ifdef HAVOK
void GameObject::setOverridableCollisionFuction(CollisionListenerType f_eCollisionFuncType,void* f_pNewOverridableCollisionFuction)
{
	if (m_pCollisionListener==NULL)
	{
		//GGETRACELOG
		exit(1);
		return;
	}
	switch (f_eCollisionFuncType)
	{
		case COLLISION_LISTENER_TYPE1 :
			m_pCollisionListener->setOverridableCollisionFuction_Type1(f_pNewOverridableCollisionFuction);
			break;
		case COLLISION_LISTENER_TYPE2 :
			m_pCollisionListener->setOverridableCollisionFuction_Type2(f_pNewOverridableCollisionFuction);
			break;
		default :
			//GGETRACELOG
			exit(1);
			break;
	}
}
#endif

#ifdef HAVOK
//Raycasting a beam
bool GameObject::castRay(	 Ogre::Vector3		f_vCastTo,
							 hkpRigidBody**		f_pFirstCollidedRigidBody,
							 GameObject**		f_pCollidedGameObject,
							 Ogre::Vector3*		f_vReturnCollisionPoint,
							 const hkVector4	*f_normal
						)
{
	if (m_pPhysicsObject==NULL)
		return false;

	Ogre::Vector3 thisPosition = m_pGraphicsObject->m_pOgreSceneNode->getPosition();
	hkpWorldRayCastInput input;
	input.m_from.set(thisPosition.x, thisPosition.y, thisPosition.z);
	input.m_to.set( f_vCastTo.x, f_vCastTo.y, f_vCastTo.z);

	hkpClosestRayHitCollector output;
	m_pPhysicsObject->getPhysicsWorld()->castRay(input, output );

	// These comments are from HavokDemos_2010
	// To visualise the raycast we make use of a macro defined in "hkDebugDisplay.h" called HK_DISPLAY_LINE.
	// The macro takes three parameters: a start point, an end point and the line color.
	// If a hit is found we display a RED line from the raycast start point to the point of intersection and mark that
	// point with a small RED cross. The intersection point is calculated using: startWorld + (result.m_mindist * endWorld).
	//
	// If no hit is found we simply display a GREY line between the raycast start and end points.

	if( output.hasHit() )
	{
		hkVector4 intersectionPointWorld;
		intersectionPointWorld.setInterpolate4( input.m_from, input.m_to, output.getHit().m_hitFraction );

		//now lets store the collision point to send back to who ever called the function
		hkReal f_fIntersectionPoint[3];
		intersectionPointWorld.store3(f_fIntersectionPoint);
		*f_vReturnCollisionPoint = Ogre::Vector3(f_fIntersectionPoint[0],f_fIntersectionPoint[1],f_fIntersectionPoint[2]);

		//some helper lines for visual debugger
		HK_DISPLAY_LINE( input.m_from, intersectionPointWorld, hkColor::RED);
		HK_DISPLAY_ARROW( intersectionPointWorld, output.getHit().m_normal, hkColor::CYAN);

		f_normal = &(output.getHit().m_normal);

		//get the rigidBody pointer of the collidable (in other words owner of the root collidable)
		//by this line we will also send the collided rigidBody pointer to the function caller
		(*f_pFirstCollidedRigidBody) = (hkpRigidBody *)(output.getHit().m_rootCollidable->getOwner());

		//Now check if the rigidBody has a collision listener.
		//If it has a collisionlistener we can also send the GameObject pointer back to the function caller too
		if ((*f_pFirstCollidedRigidBody)->getContactListeners().getSize()>0)
		{
			//if we are here it means the rigidbody has a collision listener
			//so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
			//this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
			CollisionListener* f_pOtherGameObjectListener = (CollisionListener*)((*f_pFirstCollidedRigidBody)->getContactListeners()[0]);

			//Now get the pointer and set it to the passed parameter so that we are sending back the pointer of the collided gameObject
			*f_pCollidedGameObject = f_pOtherGameObjectListener->m_pGameObject;
		}
		return true;
	}
	else
	{
		// Otherwise draw as GREY
		HK_DISPLAY_LINE(input.m_from, input.m_to, hkColor::rgbFromChars(200, 200, 200));
	}
	return false;
}
#endif 

void GameObject::removeThis()
{
	m_bRemoveGameObjectBool = true;
}
bool GameObject::isActive()
{
	return !m_bRemoveGameObjectBool;
}

void GameObject::scale(float xScale,float yScale,float zScale)
{

		bool f_bPhysicsObjectScaled = true;

		//now get the GraphicsObject's sceneNode component
		Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;

		float prevXScale = f_pTempOgreSceneNode->getScale().x;
		float prevYScale = f_pTempOgreSceneNode->getScale().y;
		float prevZScale = f_pTempOgreSceneNode->getScale().z;

		//first scale the gameObject
		f_pTempOgreSceneNode->setScale(prevXScale*xScale,prevYScale*yScale,prevZScale*zScale);

	#ifdef HAVOK
		//now try to scale the PhysicsObject
		if (m_pPhysicsObject!=NULL)
		{
			f_bPhysicsObjectScaled = m_pPhysicsObject->scale();
		}
	#endif

	#ifdef PHYSX
		if (m_pPhysicsObject!=NULL)
		{
			f_bPhysicsObjectScaled = m_pPhysicsObject->scale();
		}
	#endif

		//if physicsObject could not be scaled then take back scaling
		if (f_bPhysicsObjectScaled==false)
		{
			f_pTempOgreSceneNode->setScale(prevXScale,prevYScale,prevZScale);
		}

		if (m_pAnimatedEntity!=NULL)
		{
			m_pAnimatedEntity->m_xInitScale = xScale;
			m_pAnimatedEntity->m_yInitScale = yScale;
			m_pAnimatedEntity->m_zInitScale = zScale;
		}

		if (f_bPhysicsObjectScaled && AIWorldManager::useRecast)
		{
			GamePipe::GameScreen* f_pCurrentGameScreen = EnginePtr->GetForemostGameScreen();
			GameObjectManager* f_pGameObjectManager = f_pCurrentGameScreen->GetGameObjectManager();
			AIWorldManager *f_AIWorldManager= f_pGameObjectManager->getAIWorldManager();
			f_AIWorldManager->ScaleMesh(Ogre::Vector3(xScale,yScale,zScale),indexInRecastWorld,this);
		}



}

void GameObject::resetScale(float xScale,float yScale,float zScale)
{
	m_pGraphicsObject->m_pOgreSceneNode->setScale(xScale,yScale,zScale);
#ifdef HAVOK
	if (m_pPhysicsObject!=NULL)
	{
		((CharacterProxyObject*)m_pPhysicsObject)->scale();
	}
#endif
}

GamePipe::AIAgent* GameObject::getAIAgent()
{
    return m_AIAgent;
}

/**************************************************GameObjectDefinitionsEND************************************************/
// CollisionListener class inherits from both hkpContactListener and hkpEntityListener and so implementations for each
// of the virtual methods in both base classes must be present.
CollisionListener::CollisionListener(GameObject* f_pGameObject,hkpRigidBody* f_pRigidBody)
{
	m_pGameObject = f_pGameObject;
	m_pRigidBody = f_pRigidBody;
	m_fOverridableCollisionFuction_Type1 = NULL;
	m_fOverridableCollisionFuction_Type2 = NULL;

	m_pRigidBody->addContactListener( this );
	m_pRigidBody->addEntityListener( this );
	m_iUniqueRigidBodyID = this->getUniqueRigidBodyID();
	//RDS_HARDCODED -->later change this number according to frameRate
	//but this can be set by calling a setContactCallBackDelay function
	if (m_pGameObject->m_eObjectType!=PHYSICS_FIXED)
		m_iContactCallBackDelay = 800;
	else
		m_iContactCallBackDelay = 65536;
}
CollisionListener::~CollisionListener(){}
/***********************************************NEW FUNCTIONS FOR COLLISION LISTENERS START**********************************************/
/// This is called for each new contact point between two bodies. It will
/// also be called for all contact points in the contact manifold when the
/// hkpSimpleConstraintContactMgr's m_contactPointCallbackDelay value
/// reaches zero.
void CollisionListener::contactPointCallback( const hkpContactPointEvent& event )
{
	hkpRigidBody* f_pOtherRigidBody = event.m_bodies[0];
	CollisionListener* f_pOtherGameObjectListener = NULL;
	GameObject* f_pOtherGameObject = NULL;

	//first get the contactPoint
	hkVector4 f_rContactPoint = event.m_contactPoint->getPosition();

	//first check the m_bodies[0]. If it has contactListeners then check if this is it
	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		//if we are here it means event.m_bodies[0] has a collision listener
		//so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
		//this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);

		//now look if f_pOtherGameObjectListener is the same with this one
		//if it is not process it else check for event.m_bodies[1];
		if (f_pOtherGameObjectListener!=this)
		{
			//now we know that this collision listener is of the other GameObject
			//now get the gameObject pointer of the listener
			//it shouldn't be null because if it has a collision listener then it mean a GameObject owns it
			f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;

			//the following line will trigger the GameObject's doWhenCollide function by passing the collided gameObjects pointer
			if (f_pOtherGameObject!=NULL)//just a sanity check
			{
				m_pRigidBody->setContactPointCallbackDelay(m_iContactCallBackDelay*10);
				f_pOtherRigidBody->setContactPointCallbackDelay(m_iContactCallBackDelay*10);
				if (m_fOverridableCollisionFuction_Type1!=NULL)
					m_fOverridableCollisionFuction_Type1(this->m_pGameObject,f_pOtherGameObject,f_rContactPoint);
				return;
			}
			//we do this in this if statement because the same event will trigger this function twice (for both gameObjects)
			//so if you want to call for both gameObjects you should call this after if-else statement
		}
	}
	//now event.m_bodies[0] is exctly not this collision listener
	//so event.m_bodies[1] must be the other gameObject
	f_pOtherRigidBody = event.m_bodies[1];
	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		//if we are here it means event.m_bodies[0] has a collision listener
		//so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
		//this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);

		//now look if f_pOtherGameObjectListener is the same with this one
		//if it is not process it else check for event.m_bodies[1];
		if (f_pOtherGameObjectListener!=this)
		{
			//now we know that this collision listener is of the other GameObject
			//now get the gameObject pointer of the listener
			//it shouldn't be null because if it has a collision listener then it mean a GameObject owns it
			f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;

			//the following line will trigger the GameObject's doWhenCollide function by passing the collided gameObjects pointer
			if (f_pOtherGameObject!=NULL)//just a sanity check
			{
				m_pRigidBody->setContactPointCallbackDelay(m_iContactCallBackDelay*10);
				f_pOtherRigidBody->setContactPointCallbackDelay(m_iContactCallBackDelay*10);
				if (m_fOverridableCollisionFuction_Type1!=NULL)
					m_fOverridableCollisionFuction_Type1(this->m_pGameObject,f_pOtherGameObject,f_rContactPoint);
				return;
			}
			//we do this in this if statement because the same event will trigger this function twice (for both gameObjects)
			//so if you want to call for both gameObjects you should call this after if-else statement
		}
	}
	else
	{
		//if we are here it means that the collided rigidBody has no collision Listeners so just let the Object To know about the event
		if (m_fOverridableCollisionFuction_Type2!=NULL)
			m_fOverridableCollisionFuction_Type2(this->m_pGameObject,f_pOtherRigidBody,f_rContactPoint);
	}
	return;
}
/// New call for a new collision between two bodies.
void CollisionListener::collisionAddedCallback( const hkpCollisionEvent& event )
{
	hkpRigidBody* f_pOtherRigidBody=m_pRigidBody;
	CollisionListener* f_pOtherGameObjectListener = NULL;
	GameObject* f_pOtherGameObject = NULL;

	if (event.m_bodies[0]==f_pOtherRigidBody)
		f_pOtherRigidBody = event.m_bodies[1];

	//first check the otherRigidBody. If it has no contactListeners then it means it has no GameObject attached to it
	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
		f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;
		//now if the GameObject of this rigidBody is another gameobject
		if (f_pOtherGameObject!=m_pGameObject)
		{
			/// FOLLOWING LINES HERE ARE FOR DEBUGGING PURPOSES
			std::string f_sOwnerName = m_pGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
			std::string f_sOtherName = f_pOtherGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
			/// Here if the object type is FIXED we want to set the call back delay too much so that we will be informed only once

			if (f_pOtherGameObject->m_eObjectType!=PHYSICS_FIXED)
			{
				m_pRigidBody->setContactPointCallbackDelay(m_iContactCallBackDelay);
				return;
			}
		}
		else
		{
			//We shouldn;t be here.RDS_LOOK put a GGE_TRACELOG here
			int hgj=0;
		}
	}
	m_pRigidBody->setContactPointCallbackDelay(hkUint16(65536));
	return;
}
/// Called when two bodies are no longer colliding.
void CollisionListener::collisionRemovedCallback( const hkpCollisionEvent& event )
{
	/// EVERY LINE HERE IS FOR DEBUGGING PURPOSES
	hkpRigidBody* f_pOtherRigidBody=m_pRigidBody;
	CollisionListener* f_pOtherGameObjectListener = NULL;
	GameObject* f_pOtherGameObject = NULL;
	if (event.m_bodies[0]==f_pOtherRigidBody)
		f_pOtherRigidBody = event.m_bodies[1];
	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
		f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;
		if (f_pOtherGameObject!=m_pGameObject)
		{
			std::string f_sOwnerName = m_pGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
			std::string f_sOtherName = f_pOtherGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
		}
		else
		{
			//We shouldn;t be here.RDS_LOOK put a GGE_TRACELOG here
			int hgj=0;
		}
	}
	return;
}
/************************************************NEW FUNCTIONS FOR COLLISION LISTENERS END***********************************************/
/*******************************************DEPRECATED FUNCTIONS FOR COLLISION LISTENERS START*******************************************/
/// EXPLANATIONS-->
void CollisionListener::contactPointAddedCallback(	hkpContactPointAddedEvent& event )
{
	hkpRigidBody* f_pOtherRigidBody=m_pRigidBody;
	CollisionListener* f_pOtherGameObjectListener = NULL;
	GameObject* f_pOtherGameObject = NULL;

	if (f_pOtherRigidBody==(hkpRigidBody*)(event.m_bodyA->getRootCollidable()->getOwner()))
		f_pOtherRigidBody = (hkpRigidBody*)(event.m_bodyB->getRootCollidable()->getOwner());

	//first check the otherRigidBody. If it has no contactListeners then it means it has no GameObject attached to it
	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
		f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;
		//now if the GameObject of this rigidBody is another gameobject
		if (f_pOtherGameObject!=m_pGameObject)
		{
			/// FOLLOWING LINES HERE ARE FOR DEBUGGING PURPOSES
			std::string f_sOwnerName = m_pGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
			std::string f_sOtherName = f_pOtherGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
			/// Here if the object type is FIXED we want to set the call back delay too much so that we will be informed only once
			if (f_pOtherGameObject->m_eObjectType!=PHYSICS_FIXED)
			{
				m_pRigidBody->setContactPointCallbackDelay(m_iContactCallBackDelay);
				return;
			}
		}
		else
		{
			//We shouldn;t be here.RDS_LOOK put a GGE_TRACELOG here
			int hgj=0;
		}
	}
	// By setting the  ProcessContactCallbackDelay to 0 we will receive callbacks for
	// any collisions processed for this body every frame (simulation step), i.e. the delay between
	// any such callbacks is 0 frames.

	// If you wish to only be notified every N frames simply set the delay to be N-1.

	// The default is 65536, i.e. (for practical purpose) once for the first collision only, until
	// the bodies separate to outside the collision tolerance.
	m_pRigidBody->setContactPointCallbackDelay(hkUint16(65536));

	//	collect all information in our own data structure
	//  as the havok memory manager is really really fast,
	//  allocating lots of small structures is acceptable
	if ( event.m_contactPointProperties->getUserData() == HK_NULL )
	{
		ContactPointInfo* info = new ContactPointInfo;
		event.m_contactPointProperties->setUserData( reinterpret_cast<hkUlong>(info) );
	}
}

/// EXPLANATIONS-->
void CollisionListener::contactPointRemovedCallback( hkpContactPointRemovedEvent& event )
{
	/// EVERY LINE HERE IS FOR DEBUGGING PURPOSES
	hkpRigidBody* f_pOtherRigidBody=(hkpRigidBody*)event.m_entityA;
	CollisionListener* f_pOtherGameObjectListener = NULL;

	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
		if (this!=f_pOtherGameObjectListener)
		{
			f_pOtherRigidBody=(hkpRigidBody*)event.m_entityB;
			if (f_pOtherRigidBody->getContactListeners().getSize()>0)
				f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
			else
				return;
		}
	}
	else
		f_pOtherRigidBody = (hkpRigidBody*)event.m_entityB;

	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
		if (this!=f_pOtherGameObjectListener)
		{
			int jhf=0;
		}
	}
	std::string f_sOwnerName = m_pGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
	std::string f_sOtherName = f_pOtherGameObjectListener->m_pGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes

	//DEBUGGING FINISHED..HERE STARTS SOME USEFUL STUFF
	ContactPointInfo* info = reinterpret_cast<ContactPointInfo*>( event.m_contactPointProperties->getUserData() );
	if ( !info )
	{
		return;
	}

	delete info;
	event.m_contactPointProperties->setUserData( HK_NULL );
}

/// EXPLANATIONS-->
void CollisionListener::contactProcessCallback( hkpContactProcessEvent& event )
{
	hkpRigidBody* f_pOtherRigidBody=m_pRigidBody;
	CollisionListener* f_pOtherGameObjectListener = NULL;
	GameObject* f_pOtherGameObject = NULL;

	if (f_pOtherRigidBody==(hkpRigidBody*)(event.m_collidableA->getRootCollidable()->getOwner()))
		f_pOtherRigidBody = (hkpRigidBody*)(event.m_collidableB->getRootCollidable()->getOwner());

	//first check the otherRigidBody. If it has no contactListeners then it means it has no GameObject attached to it
	if (f_pOtherRigidBody->getContactListeners().getSize()>0)
	{
		f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherRigidBody->getContactListeners()[0]);
		f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;
		//now if the GameObject of this rigidBody is another gameobject
		if (f_pOtherGameObject!=m_pGameObject)
		{
			//the following line will trigger the GameObject's doWhenCollide function by passing the collided gameObjects pointer
			if (f_pOtherGameObject!=NULL)
			{
				//commented out as it is deprecated
				//m_pGameObject->doWhenCollide(f_pOtherGameObject);
			}
			//we do this in this if statement because the same event will trigger this function twice (for both gameObjects)
			//so if you want to call for both gameObjects you should call this after if-else statement
		}
		else
		{
			//We shouldn;t be here.RDS_LOOK put a GGE_TRACELOG here
			int hgj=0;
		}
	}
	else
	{
		//m_pGameObject->doWhenCollide(event);
	}

	hkpProcessCollisionData& result = *event.m_collisionData;
	int size = result.getNumContactPoints();

	for (int i = 0; i < size; i++ )
	{
		hkpProcessCdPoint& cp = result.m_contactPoints[i];
		{
			ContactPointInfo* info = reinterpret_cast<ContactPointInfo*>( event.m_contactPointProperties[i]->getUserData() );
		}

		// draw the contact points and normals in white
		{
			const hkVector4& start = result.m_contactPoints[i].m_contact.getPosition();
			hkVector4 normal       = result.m_contactPoints[i].m_contact.getNormal();

			// For ease of display only, we'll always draw the normal "up" (it points from entity 'B'
			// to entity 'A', but the order of A,B is arbitrary) so that we can see it. Thus, if it's
			// pointing "down", flip its direction (and scale), only for display.
			normal.mul4(5.0f * normal(1));
			HK_DISPLAY_ARROW(start, normal, hkColor::WHITE);
		}
	}
}

/********************************************DEPRECATED FUNCTIONS FOR COLLISION LISTENERS END********************************************/
void CollisionListener::entityDeletedCallback( hkpEntity* entity )
{
	//std::string f_sCallBackFiredFromName = entity->getName();
	// Remove the collision event listener
	entity->removeContactListener( this );
	// Now that we're removed from our entity, we can remove ourselves, too.
	delete this;
}
void CollisionListener::entityRemovedCallback( hkpEntity* entity )
{
	//std::string f_sOwnerName = entity->getName();//just for debugging purposes

	// Do nothing (we'll remove the collision event listener only when the entity is deleted)
	return;
}

void CollisionListener::setOverridableCollisionFuction_Type1(void* f_pNewOverridableCollisionFuction_Type1)
{
	m_fOverridableCollisionFuction_Type1 = (Collision_Function_Type1)f_pNewOverridableCollisionFuction_Type1;
}
void CollisionListener::setOverridableCollisionFuction_Type2(void* f_pNewOverridableCollisionFuction_Type2)
{
	m_fOverridableCollisionFuction_Type2 = (Collision_Function_Type2)f_pNewOverridableCollisionFuction_Type2;
}
GameObjectType getGameObjectType(hkpMotion::MotionType mt) {
    switch (mt) {
        case hkpMotion::MOTION_FIXED :
            return PHYSICS_FIXED;

        case hkpMotion::MOTION_DYNAMIC :
        case hkpMotion::MOTION_BOX_INERTIA :
        case hkpMotion::MOTION_SPHERE_INERTIA :
        case hkpMotion::MOTION_THIN_BOX_INERTIA :
            return PHYSICS_DYNAMIC;

        case hkpMotion::MOTION_KEYFRAMED :
            return PHYSICS_KEYFRAMED;

        case hkpMotion::MOTION_CHARACTER :
            return ANIMATED_CHARACTER_PROXY;

        case hkpMotion::MOTION_INVALID :
            return DEFAULT_GAME_OBJECT_TYPE;
    }
    return DEFAULT_GAME_OBJECT_TYPE;
}
hkpMotion::MotionType getHavokMotionType(GameObjectType gameObjectType) {
    switch (gameObjectType) {
        case PHYSICS_FIXED :
            return hkpMotion::MOTION_FIXED;
        case PHYSICS_DYNAMIC :
            //return hkpMotion::MOTION_BOX_INERTIA;
            //return hkpMotion::MOTION_SPHERE_INERTIA;
            //return hkpMotion::MOTION_THIN_BOX_INERTIA;
            return hkpMotion::MOTION_DYNAMIC;
        case PHYSICS_KEYFRAMED :
            return hkpMotion::MOTION_KEYFRAMED;
        case ANIMATED_CHARACTER_PROXY :
            return hkpMotion::MOTION_CHARACTER;
        case DEFAULT_GAME_OBJECT_TYPE :
            return hkpMotion::MOTION_INVALID;
    }
    return hkpMotion::MOTION_INVALID;
}

/*
CollisionListener* f_pOtherGameObjectListener =(CollisionListener*)(event.m_bodies[0]->getContactListeners()[0]);
GameObject* f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;
if (f_pOtherGameObject==m_pGameObject)
{
f_pOtherGameObjectListener =(CollisionListener*)(event.m_bodies[1]->getContactListeners()[0]);
f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;
}
*/

/*
void CollisionListener::contactPointConfirmedCallback( hkpContactPointConfirmedEvent& event)
{
std::string f_sCallBackFiredFromName = event.m_callbackFiredFrom->getName();//just for debugging purposes
hkpEntity* otherEntity = NULL;
if (event.m_callbackFiredFrom==(hkpEntity *)(event.m_collidableA->getOwner()))
otherEntity = (hkpEntity *)(event.m_collidableB->getOwner());
else
otherEntity = (hkpEntity *)(event.m_collidableA->getOwner());

int f_iSizeOfListeners =otherEntity->getContactListeners().getSize();//just for debugging purposes
CollisionListener* f_pOtherGameObjectListener =(CollisionListener*)otherEntity->getContactListeners()[0];
GameObject* f_pOtherGameObject = f_pOtherGameObjectListener->m_pGameObject;

ContactPointInfo* info = reinterpret_cast<ContactPointInfo*>( event.m_contactPointProperties->getUserData() );
if (!info )
{
return;
}
}
*/
/*
// Here we use the hkpContactProcessEvent structure to access the hkpProcessCollisionOutput which has an array containing all of the contacts
// points. We then simply iterate through each of these points in turn and read the contact point position and normal and display
// a WHITE line at this point. We'd like the normal to always be drawn 'upwards' away from the floor, and since the
// order of the two collidables A and B is arbitrary, we flip the sign of the normal if it points 'downwards'.
*/

