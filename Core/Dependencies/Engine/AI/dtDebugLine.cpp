#include "StdAfx.h"
#include "dtDebugLine.h"

#include "OgreSceneManager.h"
#include "OgreSceneNode.h"
#include "OgreManualObject.h"
#include "OgreVector3.h"



dtDebugLine::dtDebugLine(Ogre::SceneManager *manager) : dtDebugObject(manager)
                     
{

	m_colour = Ogre::ColourValue::Red;
	
};

dtDebugLine::~dtDebugLine()
{
};

void dtDebugLine::initialize()
{
    m_sceneNode = m_manager->getRootSceneNode()->createChildSceneNode();

    m_object = m_manager->createManualObject("line" + getUniqueId());
	
    /*m_object->setRenderQueueGroup(Ogre::RENDER_QUEUE_8);
    m_object->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST);
    m_object->colour(m_colour);
    m_object->position(pos1);
    m_object->position(pos2);
    m_object->end();

    m_object->setCastShadows(false);*/

    m_sceneNode->attachObject(m_object);
};

void dtDebugLine::setColour( Ogre::ColourValue colour)
{
    if (m_object){
        m_manager->destroyManualObject(m_object);
        m_colour = colour;
        initialize();
    }
};

void dtDebugLine::setVisible( bool visible )
{
    dtDebugObject::setVisible(visible);

    m_sceneNode->setVisible(visible);
};

void dtDebugLine::update( float deltaTime)
{
    //m_object->clear();

    /*if(m_visible){
        m_object->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST);
        m_object->position(pos1);
        m_object->position(pos2);
        m_object->end();
    }*/
};
