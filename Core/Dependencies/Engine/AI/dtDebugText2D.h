#ifndef DEBUG_TEXT_2D_H
#define DEBUG_TEXT_2D_H

#include "dtDebugObject.h"

#include "OgreVector2.h"
#include "OgreString.h"



namespace Ogre{
    class SceneManager;
    class SceneNode;
    class MovableObject;
    class Camera;
    class Overlay;
    class OverlayElement;
    class OverlayContainer;
    class Font;
}


    class dtDebugText2D: public dtDebugObject{
    public:
        dtDebugText2D(Ogre::SceneManager*,Ogre::SceneNode* node,Ogre::String);
        virtual ~dtDebugText2D();
        void initialize();
        void update(float);
        virtual void setColour(Ogre::ColourValue);
        void setText(Ogre::String);
		Ogre::String getBehavior() { return behavior;}
		void setBehavior(Ogre::String behSt) { behavior = behSt;}
    protected:
        Ogre::MovableObject *object;
        Ogre::Camera *camera;
        Ogre::Overlay *overlay;
        Ogre::OverlayElement *textArea;
        Ogre::OverlayContainer* container;
        Ogre::Vector2 textDim;
        Ogre::Font *font;
        Ogre::String text;
		Ogre::String behavior;

        Ogre::Vector2 getTextDimensions(Ogre::String);
    private:
    };


#endif
