#include "StdAfx.h"
#include "AIAgent.h"

#include "AIAgentManager.h"
#include "AIAgentBinding.h"

#ifdef HAVOK
#include "HavokObject.h"
#endif

#include "SteeringBehaviors.h"
#include "lua.hpp"
#include "AnimatedEntity.h"
#include "../Resources/TextFileManager.h"
#include "../Resources/TextFile.h"
#include "Debug/DebugCircle2D.h"
#include "Debug/DebugObject.h"
#include "Debug/DebugText2D.h"
#include "GraphicsObject.h"
#include "GraphNode.h"

#include "OgreResourceGroupManager.h"
#include "OgreSceneNode.h"
#include "OgreSceneManager.h"
#include "OgreLogManager.h"
#include "OgreMath.h"

#include "OpenSteer/PolylineSegmentedPathwaySingleRadius.h"
#include "OpenSteer/PolylineSegmentedPathwaySegmentRadii.h"

using namespace GamePipe;
using namespace std;


TextFileManager* AIAgent::m_tfm;

AIAgent::AIAgent(Ogre::String fileName, GraphicsObject *graphics,
                 PhysicsObject *havok, AnimationManager::AnimatedEntity *animation) : m_type(0),
                 m_maxSpeed(0)
{
    m_remove = false;
    m_luaFileName = fileName;
    m_luaStackSize = 0;
    m_graphicsObject = graphics;
    m_physicsObject = havok;

    m_animationEntity = animation;
    GraphicsObject** temp = new GraphicsObject*[1];
    temp[0] = graphics;

    m_text = 0;

    //Steering Behavior Stuff
    m_steeringBehavior = new SteeringBehavior(this);
    m_velocity = Ogre::Vector3(0,0,0);
    m_vHeading = m_velocity.normalisedCopy();
    m_vSide = m_vHeading.perpendicular();
    m_destination = getPosition();
    m_dTimeElapsed = 0;
    m_debug = false;
    m_luaVM = 0;
    m_luaInitilize = false;
    m_enableLua = true;
    m_moveTo = Ogre::Vector3::ZERO;
    m_blendFrame = 0.0f;
};

AIAgent::~AIAgent()
{
    m_remove = true;
    if (m_luaFileName != ""){
        lua_close(m_luaVM);
    }
    for(list<DebugObject*>::iterator it = m_debugObjects->begin();
        it != m_debugObjects->end(); it++){
            delete *it;
    }

    for(list<Ogre::SceneNode*>::iterator it = m_debugPathNodes->begin();
        it != m_debugPathNodes->end(); it++){
            m_graphicsObject->m_pOgreSceneManager->destroySceneNode(*it);
    }

    for(list<DebugObject*>::iterator it = m_debugPathObjects->begin();
        it != m_debugPathObjects->end(); it++){
            delete *it;
    }

    delete m_debugObjects;
    m_debugObjects = 0;

    delete m_debugPathNodes;
    m_debugPathNodes = 0;

    delete m_debugPathObjects;
    m_debugPathObjects = 0;
    m_physicsObject = 0;
    m_graphicsObject = 0;
    m_animationEntity = 0;
};

void AIAgent::addType(int type)
{
    if (type > 0 && type <= AIAgentManager::MAX_TYPES){
        unsigned long mask = 0x1 << (type - 1);
        m_type |= type;
    }
};

void AIAgent::addTypes(unsigned long types)
{
    m_type |= types;
};

void AIAgent::bindAI()
{
    // Add a pointer to the AI Agent to the following functions
    lua_bindAI(&GamePipe::l_getDestination, "getDestination", m_luaVM);
    lua_bindAI(&GamePipe::l_setDestination, "setDestination", m_luaVM);
    lua_bindAI(&GamePipe::l_setMaxLinearSpeed, "setMaxLinearSpeed", m_luaVM);
    lua_bindAI(&GamePipe::l_getPosition, "getPosition", m_luaVM);

    lua_bindAI(&GamePipe::l_setSeekTarget, "setSeekTarget", m_luaVM);
    lua_bindAI(&GamePipe::l_setFleeTarget, "setFleeTarget", m_luaVM);
    lua_bindAI(&GamePipe::l_setArriveTarget, "setArriveTarget", m_luaVM);
    lua_bindAI(&GamePipe::l_setOnBehaviors, "setOnBehaviors", m_luaVM);

    lua_bindAI(&GamePipe::l_playAnimation,"playAnimation",m_luaVM);
    lua_bindAI(&GamePipe::l_stopAnimation,"stopAnimation", m_luaVM);
    lua_bindAI(&GamePipe::l_setDebugText, "setDebugText", m_luaVM);
    lua_bindAI(&GamePipe::l_setDefaultAnimation, "setDefaultAnimation", m_luaVM);
    lua_bindAI(&GamePipe::l_getDefaultAnimation, "getDefaultAnimation", m_luaVM);
    lua_bindAI(&GamePipe::l_setBlendFrame, "setBlendFrame", m_luaVM);
    lua_bindAI(&GamePipe::l_getBlendFrame, "getBlendFrame", m_luaVM);
    lua_bindAI(&GamePipe::l_getActiveAnimation, "getActiveAnimation", m_luaVM);
    lua_bindAI(&GamePipe::l_transitAndPlayAnimation, "transitAndPlayAnimation", m_luaVM);
};

Ogre::String AIAgent::getActiveAnimation()
{
    return m_animationEntity->GetActiveAnimation();
};

std::vector<std::string> AIAgent::getAnimations()
{
    if(m_animationEntity)
    {
        return m_animationEntity->GetAnimationNames();
    }else
    {
        m_log = new Ogre::LogManager();
        Ogre::String message = "ERROR: ";
        message.append(m_name);
        message.append(" does not have an animationEntity.");
        m_log->logMessage(message, Ogre::LML_CRITICAL);

        std::vector<std::string> temp;
        return temp;
    }
};

float AIAgent::getBlendFrame()
{
    return m_blendFrame;
};

Ogre::String AIAgent::getDefaultAnimation()
{
    return m_animationEntity->GetDefaultAnimation();
};

Ogre::Vector3 AIAgent::getDestination()
{
    return Ogre::Vector3(m_destination);
};

int AIAgent::getKnownLuaStackSize()
{
    return m_luaStackSize;
}

Ogre::Real AIAgent::getMaxLinearSpeed()
{
#ifdef HAVOK
    if(m_physicsObject){
        return m_physicsObject->getRigidBody()->getMaxLinearVelocity();
    }
    else {
        return m_maxSpeed;
    }
#endif

#ifdef PHYSX
	return m_maxSpeed;
#endif
}

Ogre::String AIAgent::getName()
{
    if(m_graphicsObject && m_graphicsObject->m_pOgreEntity){
        return m_graphicsObject->m_pOgreEntity->getName();
    }
#ifdef HAVOK
    else if (m_physicsObject){
        return m_physicsObject->getRigidBody()->getName();
    }
#endif
#ifdef PHYSX

#endif
    else{
        return m_name;
    }
};

Ogre::Vector3 AIAgent::getPosition()
{
    // Grab position
#ifdef HAVOK
	if (m_physicsObject){
        hkVector4 havokPosition = m_physicsObject->getPosition();
        return Ogre::Vector3(havokPosition(0), havokPosition(1), havokPosition(2));
    }
    else if (m_graphicsObject){
        return Ogre::Vector3(m_graphicsObject->m_pOgreSceneNode->_getDerivedPosition());
    }
    else {
        return Ogre::Vector3(m_position);
    }
#endif

#ifdef PHYSX
	if (m_graphicsObject){
        return Ogre::Vector3(m_graphicsObject->m_pOgreSceneNode->_getDerivedPosition());
    }
    else {
        return Ogre::Vector3(m_position);
    }
#endif
};

unsigned long AIAgent::getType()
{
    return m_type;
};

vector<int> AIAgent::getTypes()
{
    vector<int> types;
    unsigned long mask = m_type;

    for (int i = 1; i <= AIAgentManager::MAX_TYPES; i++){
        if (mask & 0x01){
            types.push_back(i);
        }
        mask >>= 1;
    }
    return types;
};

Ogre::Vector3 AIAgent::getVelocity(){
    return m_velocity;
}

void AIAgent::initialize()
{
    // Create log manager;
    m_log = Ogre::LogManager::getSingletonPtr();

    AIAgent::m_tfm = TextFileManager::getSingletonPtr();

    // Initialize debug data structures
    m_debugObjects = new list<DebugObject*>();
    m_debugPathNodes = new list<Ogre::SceneNode*>();
    m_debugPathObjects = new list<DebugObject*>();

    if (m_luaFileName != ""){
        reloadLua();
    }

    // Test Debug Graphics
    m_debugSelf = new DebugCircle2D(m_graphicsObject->m_pOgreSceneManager);
    m_debugSelf->attachTo(m_graphicsObject->m_pOgreSceneNode);
    m_debugSelf->initialize();
    m_debugObjects->push_back(m_debugSelf);

    // Debug Destination
    m_debugDestNode = m_graphicsObject->m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
    m_debugDestNode->setPosition(m_destination);
    m_debugPathNodes->push_back(m_debugDestNode);

    m_debugDest = new DebugCircle2D(m_graphicsObject->m_pOgreSceneManager, ((DebugCircle2D*)m_debugSelf)->getRadius(), ((DebugCircle2D*)m_debugSelf)->getThickness());
    m_debugDest->initialize();
    m_debugDest->attachTo(m_debugDestNode);
    m_debugObjects->push_back(m_debugDest);

    setDebug(false);
};

bool AIAgent::isDebug()
{
    return m_debug;
};

void AIAgent::initializeLua()
{
    if(m_luaFileName != ""){
        // Initialize the Lua script if an init function is defined
        m_luaStackSize = lua_gettop(m_luaVM);
        lua_getglobal(m_luaVM, "init");
        if (!lua_isnil(m_luaVM, -1)){
            lua_call(m_luaVM,0,0);
        }
        else{
            lua_remove(m_luaVM, -1);
        }
    }
};
void AIAgent::reloadLua()
{
    m_luaInitilize = false;

    if(m_luaVM){
        lua_close(m_luaVM);
    }
    // Create Lua state
    m_luaVM = lua_open();
    luaL_openlibs(m_luaVM);

    // Load Lua script
    Ogre::ResourceGroupManager::getSingleton().declareResource(m_luaFileName.c_str(), "TextFile");

    if(!Ogre::ResourceGroupManager::getSingletonPtr()->resourceGroupExists("LUA")){
        Ogre::ResourceGroupManager::getSingletonPtr()->createResourceGroup("LUA");
    }

    TextFilePtr textfile = m_tfm->load(m_luaFileName.c_str(), "LUA");

    Ogre::String log = "LUA INFO: Loading lua file ";
    log.append(m_luaFileName);

    Ogre::LogManager::getSingleton().logMessage(log, Ogre::LML_NORMAL);

    luaL_dostring(m_luaVM, textfile->getString().c_str());

    // Init position
    m_position = Ogre::Vector3::ZERO;

    // Bind the AI functions to Lua.
    bindAI();

    // XXX
    // Shouldn't need to unload the file but for some reason unloading the LUA
    // group does not unload the text files and any changes to the file will
    // not be seen by further loads.
    // This is not desirable because multiple AI's using the same lua file have
    // to reload the file instead of using a cached file.
    textfile->unload();

    // Initialize state of the FSM
    lua_getglobal(m_luaVM, "state");
    if (!lua_isnil(m_luaVM, -1)){
        m_state = lua_tostring(m_luaVM, -1);
    }
    else{
        lua_remove(m_luaVM, -1);
        m_state = "";
        Ogre::String message = "ERROR: ";
        message.append(m_luaFileName);
        message.append(" does not define a \"state\" variable containing a ");
        message.append("function name to set the Lua state machine to.");

        m_log->logMessage(message, Ogre::LML_CRITICAL);
    }
}

void AIAgent::setOnBehaviors(Ogre::String name)
{
    SteeringBehavior::behavior_type type = m_steeringBehavior->behaviorType[name];

    switch (type)
    {
    case SteeringBehavior::flee : m_steeringBehavior->FleeOn();
        break;
    case SteeringBehavior::seek : m_steeringBehavior->SeekOn(m_steeringBehavior->m_vSeekTarget);
        break;
    case SteeringBehavior::arrive : m_steeringBehavior->ArriveOn();
        break;
    case SteeringBehavior::wander : m_steeringBehavior->WanderOn();
        break;
    case SteeringBehavior::pursuit : m_steeringBehavior->PursuitOn(m_steeringBehavior->m_pTargetAgent1);
        break;
    case SteeringBehavior::evade : m_steeringBehavior->EvadeOn(m_steeringBehavior->m_pTargetAgent2);
        break;
    case SteeringBehavior::cohesion : m_steeringBehavior->CohesionOn();
        break;
    case SteeringBehavior::separation : m_steeringBehavior->SeparationOn();
        break;
    case SteeringBehavior::allignment : m_steeringBehavior->AlignmentOn();
        break;
    case SteeringBehavior::flock : m_steeringBehavior->FlockingOn();
        break;
    case SteeringBehavior::wall_avoidance : m_steeringBehavior->WallAvoidanceOn();
        break;
    case SteeringBehavior::obstacle_avoidance : m_steeringBehavior->ObstacleAvoidanceOn();
        break;
    }
};

void AIAgent::setArriveTarget(Ogre::Vector3 arriveTarget)
{
    m_steeringBehavior->m_vArriveTarget = arriveTarget;
};

void AIAgent::setFleeTarget(Ogre::Vector3 fleeTarget)
{
    m_steeringBehavior->m_vFleeTarget = fleeTarget;
}

void AIAgent::setSeekTarget(Ogre::Vector3 seekTarget)
{
    m_steeringBehavior->m_vSeekTarget = seekTarget;
}

void AIAgent::setDebug( bool debug)
{
    m_debug = debug;
    for(list<DebugObject*>::iterator it = m_debugObjects->begin();
        it != m_debugObjects->end(); it++){
            (*it)->setVisible(debug);
    }
    m_graphicsObject->m_pOgreSceneNode->showBoundingBox(debug);
};

void AIAgent::setDebugText( Ogre::String text)
{
    if(!m_text){
        m_text = new DebugText2D(m_graphicsObject->m_pOgreSceneManager, m_graphicsObject->m_pOgreSceneNode, text);
        m_text->initialize();
        m_debugObjects->push_back(m_text);
    }
    else {
        ((DebugText2D*)m_text)->setText(text);
    }
}

void AIAgent::setDestination(Ogre::Vector3 destination)
{
    m_destination = Ogre::Vector3(destination);

    m_debugDestNode->setPosition(destination);
};

void AIAgent::setEnableLua(bool enable){
    m_enableLua = enable;
}

void AIAgent::setMaxLinearSpeed( Ogre::Real speed )
{
#ifdef HAVOK
    if(m_physicsObject){
        m_physicsObject->getRigidBody()->setMaxLinearVelocity(speed);
    }
    else {
        m_maxSpeed = speed;
    }
#endif

#ifdef PHYSX
	m_maxSpeed = speed;
#endif
}

void AIAgent::setName( Ogre::String name)
{
    m_name = name;
}

void AIAgent::setType(int type)
{
    if (type > 0 && type <= AIAgentManager::MAX_TYPES){
        unsigned long mask = 0x1 << type;
        m_type = mask;

        if(m_aiAgentManger){
            m_aiAgentManger->reassignAgentType(this, m_type);
        }
    }
};

void AIAgent::setTypes(unsigned long types)
{
    m_type = types;

    if(m_aiAgentManger){
        m_aiAgentManger->reassignAgentType(this, m_type);
    }
};

bool AIAgent::playAnimation(Ogre::String newAnimName,bool loop,Ogre::String transitionAnimation)
{
    if(m_animationEntity)
    {
        m_animationEntity->PlayAnimation(newAnimName.c_str(), transitionAnimation.c_str(), loop, m_blendFrame);
        return true;
    }else
    {
        return false;
    }
};

bool AIAgent::setBlendFrame(float blendFrame)
{
    m_blendFrame = blendFrame;
    return true;
};

bool AIAgent::setDefautAnimation(Ogre::String defaultAnimName)
{
    if(m_animationEntity)
    {
        std::vector<std::string> animNames = m_animationEntity->GetAnimationNames();
        std::vector<std::string>::iterator it;
        for(it = animNames.begin(); it != animNames.end(); it++)
        {
            if (defaultAnimName != *it)
            {
                continue;
            }else
            {
                m_animationEntity->SetDefaultAnimation(defaultAnimName.c_str());
                return true;
            }
        }
        return false;
    }else
    {
        return false;
    }
};

bool AIAgent::stopAnimation()
{
    if (m_animationEntity)
    {
        m_animationEntity->StopAnimation(m_animationEntity->GetActiveAnimation());
        return true;
    }
    else
    {
        return false;
    }
};

void AIAgent::update(float currentTime, float deltaTime)
{
    //don't update if the agent is being removed
    if (m_remove){
        return;
    }

    if (m_enableLua && m_luaFileName != ""){
        if(!m_luaInitilize){
            initializeLua();
            m_luaInitilize = true;
        }

        // Update known lua stack size
        m_luaStackSize = lua_gettop(m_luaVM);

        // Grab current state
        lua_getglobal(m_luaVM, "state");
        if (!lua_isnil(m_luaVM,-1)){
            m_state = lua_tostring(m_luaVM, -1);
        }
        else {
            m_state = "";
            lua_remove(m_luaVM, -1);
        }

        // Execute current state
        if(m_state.length() > 0){
            lua_getglobal(m_luaVM, m_state.c_str());

            if(!lua_isnil(m_luaVM, -1)){
                lua_call(m_luaVM,0,0);
            }
            else{
                m_state = "";
                lua_remove(m_luaVM, -1);
                Ogre::String message = "ERROR: ";
                message.append(m_state);
                message.append(" is not a valid state in ");
                message.append(m_luaFileName);

                m_log->logMessage(message, Ogre::LML_CRITICAL);
            }
        }
    }

    // Update debug if enabled
    for(list<DebugObject*>::iterator it = m_debugObjects->begin();
        it != m_debugObjects->end(); it++){
            (*it)->update(deltaTime);
    }

    // Update self debug object.  The .scene parser likes to move around the
    // object to other scene nodes.
    if (m_debugSelf->m_sceneNode != m_graphicsObject->m_pOgreSceneNode){
        m_debugSelf->attachTo(m_graphicsObject->m_pOgreSceneNode);
    }

    //OPENSTEER

    //keep the ai agent in sync with opensteer
    ((OpenSteer::SimpleVehicle*)this)->setPosition(OpenSteer::Vec3(getPosition().x,0,getPosition().z));
    //((OpenSteer::SimpleVehicle*)this)->setSpeed(m_physicsObject->getRigidBody()->getLinearVelocity().length3());

    OpenSteer::Vec3 force = OpenSteer::Vec3::zero;

    float maxSpeed = ((OpenSteer::SimpleVehicle*)this)->maxSpeed();

    if(m_steeringBehavior->isSeekOn()){
        force += steerForSeek(
            OpenSteer::Vec3(
            m_steeringBehavior->m_vSeekTarget.x,
            m_steeringBehavior->m_vSeekTarget.y,
            m_steeringBehavior->m_vSeekTarget.z)).truncateLength(maxSpeed);
    }
    if (m_steeringBehavior->isEvadeOn()){
        //Make sure the other AI is in sync with the OpenSteer world
        Ogre::Vector3 evadePosition = m_steeringBehavior->m_pTargetAgent2->getPosition();
        ((OpenSteer::SimpleVehicle_1*)m_steeringBehavior->m_pTargetAgent2)->setPosition(OpenSteer::Vec3(evadePosition.x,0,evadePosition.z));

        force += steerForEvasion(*((OpenSteer::SimpleVehicle_1*)m_steeringBehavior->m_pTargetAgent2), 1.0f).truncateLength(maxSpeed);
    }
    if (m_steeringBehavior->isPursuitOn()){
        //Make sure the other AI is in sync with the OpenSteer world
        Ogre::Vector3 pursuePosition = m_steeringBehavior->m_pTargetAgent1->getPosition();
        ((OpenSteer::SimpleVehicle_1*)m_steeringBehavior->m_pTargetAgent1)->setPosition(OpenSteer::Vec3(pursuePosition.x,0,pursuePosition.z));

        force += steerForPursuit(*((OpenSteer::SimpleVehicle_1*)m_steeringBehavior->m_pTargetAgent1), 1.0f).truncateLength(maxSpeed);
    }
    if(m_steeringBehavior->isWanderOn()){
        force += steerForWander(deltaTime);
    }
    if(m_steeringBehavior->isFollowPathOn()){
        // creating the path every frame is very costly and should only be done when a new path is assigned.
        const OpenSteer::PolylineSegmentedPath::size_type pathPointCount = m_steeringBehavior->m_path.size();

        OpenSteer::Vec3 *pathPoints = new OpenSteer::Vec3[pathPointCount];

        int j = 0;
        for (list<GraphNode*>::iterator i = m_steeringBehavior->m_path.begin(); i != m_steeringBehavior->m_path.end(); i++, j++){
            Ogre::Vector3 point = (*i)->getPos();
            pathPoints[j] = OpenSteer::Vec3(point.x, 0, point.z);
        }

        OpenSteer::PolylineSegmentedPathwaySingleRadius *path = new OpenSteer::PolylineSegmentedPathwaySingleRadius (
            pathPointCount,
            pathPoints,
            0.5f,
            false);

        // The prediction time used here affects the speed of the agent.
        // This is not desirable.
        float predictionTime = 0.5f;

        OpenSteer::Vec3 steerToPath = steerToFollowPath (1, predictionTime, *path).truncateLength(maxSpeed);

        float distanceToPredict = OpenSteer::Vec3::distance(((OpenSteer::SimpleVehicle*)this)->position(), path->segmentEnd(path->segmentCount() - 1));

        distanceToPredict = (distanceToPredict > path->radius()) ? 1.0f : 0;

        if (steerToPath.x != 0 || steerToPath.z != 0){
            force += steerToPath.setYtoZero().normalize() * maxSpeed;
        }
        else {
            force += forward().setYtoZero().normalize() * maxSpeed * distanceToPredict;
        }

        delete [] pathPoints;
        delete path;
    }

    if(m_steeringBehavior->isObstacleAvoidanceOn()){
        OpenSteer::Vec3 obstacleSteer = steerToAvoidNeighbors(deltaTime, *neighbors);
        if (obstacleSteer != OpenSteer::Vec3::zero){
            const float leakThrough = 0.1f;
            // Allow other forces to act instead of avoidance 10% of the time.
            // This is beneficial since this steering implementation does not
            // slow the AI but simply steers it away from a future collision.
            if (leakThrough < OpenSteer::frandom01()){
                // sets the steering force of obstacle avoidance to the max AI
                // speed.
                force = obstacleSteer.setYtoZero().normalize() * ((OpenSteer::SimpleVehicle*)this)->maxSpeed();
            }
        }
    }

    if(m_steeringBehavior->isWallAvoidanceOn()){
        //OpenSteer::Vec3 wallSteer = steerToAvoidObstacles(1.0f, )
    }

    // make sure the AI doesn't run faster than possible.
    force = force.truncateLength(((OpenSteer::SimpleVehicle*)this)->maxSpeed());

    applySteeringForce(force, deltaTime);
    // OPENSTEER controls velocity not havok.  Friction is applied by opensteer.
    if(force == OpenSteer::Vec3::zero){
        applyBrakingForce(0.75f, deltaTime);
    }

#ifdef HAVOK
    // keep havok's falling or jumping velocity.
    hkVector4 newVel = hkVector4(
        velocity().x,
        m_physicsObject->getRigidBody()->getLinearVelocity()(1),
        velocity().z) ;
#endif

    Ogre::Vector3 nextMoveTo = Ogre::Vector3(forward().x,0,forward().z);

    //END OF OPENSTEER calculations
#ifdef HAVOK
    if(m_physicsObject){
        if(m_animationEntity){
            Ogre::Vector3 origin = Ogre::Vector3(0,0,1);
            float deltaAngleZ = origin.angleBetween(nextMoveTo).valueDegrees();
            float realDeltaAngle = (Ogre::Math::ATan2(origin.y, origin.x) - Ogre::Math::ATan2(nextMoveTo.y, nextMoveTo.x)).valueDegrees();

            if (nextMoveTo.x || nextMoveTo.y || nextMoveTo.z){
                float degreeRot = ((deltaAngleZ) * (realDeltaAngle ? -1 : 1) + 90.0f);

                ((CharacterRigidBodyObject*)m_physicsObject)->setInputs(0,0,false,Ogre::Math::DegreesToRadians(degreeRot),true);
            }
        }
        if (newVel(0) != 0 || newVel(2) != 0){
            // setting the velocity will not check if the AI is jump or on the
            // ground.  This may have weird behavior if the AI is in the air.
            m_physicsObject->getRigidBody()->setLinearVelocity(newVel);
        }
    }

    // assign new velocity
    m_velocity = Ogre::Vector3(newVel(0), newVel(1), newVel(2));
#endif
};