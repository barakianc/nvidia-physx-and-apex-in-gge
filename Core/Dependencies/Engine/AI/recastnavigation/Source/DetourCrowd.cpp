//
// Copyright (c) 2009-2010 Mikko Mononen memon@inside.org
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//

#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <float.h>
#include <stdlib.h>
#include <new>
#include "DetourCrowd.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshQuery.h"
#include "DetourObstacleAvoidance.h"
#include "DetourCommon.h"
#include "DetourAssert.h"
#include "DetourAlloc.h"
#include "Sample.h"
#include "DetourDraw.h"
#include "AIWorldManager.h"
#include "Ogre.h"

#define RAND_FLOAT (((float) rand()) / (float) RAND_MAX)

static const int MAX_ITERS_PER_UPDATE = 10;
static const int MAX_PATHQUEUE_NODES = 4096;
static const int MAX_COMMON_NODES = 512;

// TODO: replace 128 with maxAgentNum, maybe static member
dtAIAgent** dtCrowd::sm_agentList = (dtAIAgent**)dtAlloc(sizeof(dtAIAgent*)*128, DT_ALLOC_PERM);
int dtCrowd::sm_agentCount = 0;

dtCrowd* dtAllocCrowd()
{
	void* mem = dtAlloc(sizeof(dtCrowd), DT_ALLOC_PERM);
	if (!mem) return 0;
	return new(mem) dtCrowd;
}

void dtFreeCrowd(dtCrowd* ptr)
{
	if (!ptr) return;
	ptr->~dtCrowd();
	dtFree(ptr);
}

inline float tween(const float t, const float t0, const float t1)
{
	return dtClamp((t-t0) / (t1-t0), 0.0f, 1.0f);
}

static int addNeighbour(const int idx, const float dist,
	dtCrowdNeighbour* neis, const int nneis, const int maxNeis)
{
	// Insert neighbour based on the distance.
	dtCrowdNeighbour* nei = 0;
	if (!nneis)
	{
		nei = &neis[nneis];
	}
	else if (dist >= neis[nneis-1].dist)
	{
		if (nneis >= maxNeis)
			return nneis;
		nei = &neis[nneis];
	}
	else
	{
		int i;
		for (i = 0; i < nneis; ++i)
			if (dist <= neis[i].dist)
				break;

		const int tgt = i+1;
		const int n = dtMin(nneis-i, maxNeis-tgt);

		dtAssert(tgt+n <= maxNeis);

		if (n > 0)
			memmove(&neis[tgt], &neis[i], sizeof(dtCrowdNeighbour)*n);
		nei = &neis[i];
	}

	memset(nei, 0, sizeof(dtCrowdNeighbour));

	nei->idx = idx;
	nei->dist = dist;

	return dtMin(nneis+1, maxNeis);
}

static int getNeighbours(const float* pos, const float height, const float range,
	dtAIAgent* skip, dtCrowdNeighbour* result, const int maxResult,
	dtAIAgent** agents, const int /*nagents*/, dtProximityGrid* grid)
{
	int n = 0;

	static const int MAX_NEIS = 32;
	unsigned short ids[MAX_NEIS];
	int nids = grid->queryItems(pos[0]-range, pos[2]-range,
		pos[0]+range, pos[2]+range,
		ids, MAX_NEIS);

	for (int i = 0; i < nids; ++i)
	{
		dtAIAgent* ag = agents[ids[i]];

		if (ag == skip) continue;

		// Check for overlap.
		float diff[3];
		dtVsub(diff, pos, ag->getAgentPosition());
		if (fabsf(diff[1]) >= (height+ag->getAgentParams()->height)/2.0f)
			continue;
		diff[1] = 0;
		const float distSqr = dtVlenSqr(diff);
		if (distSqr > dtSqr(range))
			continue;

		n = addNeighbour(ids[i], distSqr, result, n, maxResult);
	}
	return n;
}

/************************************************************************/
/* DETOUR CROWD CLASS                                                   */
/************************************************************************/
dtCrowd::dtCrowd() :
m_maxAgents(0),
	m_agents(0),
	m_activeAgents(0),
	m_agentAnims(0),
	m_obstacleQuery(0),
	m_grid(0),
	m_pathResult(0),
	m_maxPathResult(0),
	m_maxAgentRadius(0),
	m_moveRequests(0),
	m_moveRequestCount(0),
	m_navquery(0),
	m_agentsOnTarget(0),
	m_manualObj(0),
	mTargets(0)
	{resetActiveAgentsCount();}

dtCrowd::~dtCrowd()
{
	purge();
}

void dtCrowd::purge()
{
	for (int i = 0; i < m_maxAgents; ++i)
		m_agents[i].~dtAIAgent();
	dtFree(m_agents);
	m_agents = 0;
	m_maxAgents = 0;
	resetActiveAgentsCount();

	dtFree(m_activeAgents);
	m_activeAgents = 0;

	dtFree(m_agentAnims);
	m_agentAnims = 0;

	dtFree(m_pathResult);
	m_pathResult = 0;

	dtFree(m_moveRequests);
	m_moveRequests = 0;
	m_moveRequestCount = 0;

	dtFreeProximityGrid(m_grid);
	m_grid = 0;

	dtFreeObstacleAvoidanceQuery(m_obstacleQuery);
	m_obstacleQuery = 0;

	dtFreeNavMeshQuery(m_navquery);
	m_navquery = 0;

	dtTarget* currentTarget = this->mTargets, *nextTarget = this->mTargets;
	while(currentTarget)
	{
		nextTarget = currentTarget->getNextTarget();
		delete currentTarget;
		currentTarget = nextTarget;
	}
}

bool dtCrowd::InitNavQueryForPrevAgents(dtNavMesh* nav)
{

	if (!m_pathq.init(m_maxPathResult, MAX_PATHQUEUE_NODES, nav))
		return false;
	/*
	for (int i = 0; i < m_maxAgents; ++i)
	{
		//m_agents[i].getAgentCorridor()->~dtPathCorridor();
		if (!m_agents[i].getAgentCorridor()->init(m_maxPathResult))
			return false;
	}*/
	delete m_navquery;	
	m_navquery = dtAllocNavMeshQuery();
	if (!m_navquery)
		return false;
	if (dtStatusFailed(m_navquery->init(nav, MAX_COMMON_NODES)))
		return false;
	return true;
}
// TODO Change agent radius according to used agents
bool dtCrowd::init(int crowdID, const int maxAgents, const float maxAgentRadius, char *agentMaterial, dtNavMesh* nav)
{
	purge();
	this->mCrowdID = crowdID;
	this->m_crowdMaterial = _strdup(agentMaterial);

	Ogre::SceneManager *mSceneMgr = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	m_manualObj = mSceneMgr->createManualObject("crowdManual" + this->mCrowdID);
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(m_manualObj);
	
	m_maxAgents = maxAgents;
	m_maxAgentRadius = maxAgentRadius;

	//dtVset(m_ext, m_maxAgentRadius*2.0f,m_maxAgentRadius*1.5f,m_maxAgentRadius*2.0f);

	//m_grid = dtAllocProximityGrid();
	//if (!m_grid)
		//return false;
	//if (!m_grid->init(m_maxAgents*4, maxAgentRadius*3))
		//return false;
	m_obstacleQuery = dtAllocObstacleAvoidanceQuery();
	if (!m_obstacleQuery)
		return false;
	if (!m_obstacleQuery->init(6, 8))
		return false;

	// Init obstacle query params.
	memset(m_obstacleQueryParams, 0, sizeof(m_obstacleQueryParams));
	for (int i = 0; i < DT_CROWD_MAX_OBSTAVOIDANCE_PARAMS; ++i)
	{
		dtObstacleAvoidanceParams* params = &m_obstacleQueryParams[i];
		params->velBias = 0.4f;
		params->weightDesVel = 2.0f;
		params->weightCurVel = 0.75f;
		params->weightSide = 0.75f;
		params->weightToi = 2.5f;
		params->horizTime = 2.5f;
		params->gridSize = 33;
		params->adaptiveDivs = 7;
		params->adaptiveRings = 2;
		params->adaptiveDepth = 5;
	}

	// Allocate temp buffer for merging paths.
	m_maxPathResult = 256;
	m_pathResult = (dtPolyRef*)dtAlloc(sizeof(dtPolyRef)*m_maxPathResult, DT_ALLOC_PERM);
	if (!m_pathResult)
		return false;

	m_moveRequests = (MoveRequest*)dtAlloc(sizeof(MoveRequest)*m_maxAgents, DT_ALLOC_PERM);
	if (!m_moveRequests)
		return false;
	m_moveRequestCount = 0;

	if(nav != NULL)
	{
		if (!m_pathq.init(m_maxPathResult, MAX_PATHQUEUE_NODES, nav))
			return false;
	}

	m_agents = (dtAIAgent*)dtAlloc(sizeof(dtAIAgent)*m_maxAgents, DT_ALLOC_PERM);
	if (!m_agents)
		return false;

	m_activeAgents = (dtAIAgent**)dtAlloc(sizeof(dtAIAgent*)*m_maxAgents, DT_ALLOC_PERM);
	if (!m_activeAgents)
		return false;

	m_agentAnims = (dtCrowdAgentAnimation*)dtAlloc(sizeof(dtCrowdAgentAnimation)*m_maxAgents, DT_ALLOC_PERM);
	if (!m_agentAnims)
		return false;

	for (int i = 0; i < m_maxAgents; ++i)
	{
		new(&m_agents[i]) dtAIAgent(this, i, Ogre::Vector3(0, 0, 0) );
		//m_agents[i].setActive(false);
		/*
		if(nav != NULL)
		{
		*/
			if (!m_agents[i].getAgentCorridor()->init(m_maxPathResult))
				return false;
		//}
	}

	for (int i = 0; i < m_maxAgents; ++i)
	{
		m_agentAnims[i].active = 0;
		m_agents[i].setActive(false);
	}

	// The navquery is mostly used for local searches, no need for large node pool.
	if(nav != NULL)
	{
		m_navquery = dtAllocNavMeshQuery();
		if (!m_navquery)
			return false;
		if (dtStatusFailed(m_navquery->init(nav, MAX_COMMON_NODES)))
			return false;
	}
	// Make polygons with 'disabled' flag invalid.
	this->getEditableFilter()->setExcludeFlags(SAMPLE_POLYFLAGS_DISABLED);

	// Setup local avoidance params to different qualities.
	dtObstacleAvoidanceParams params;
	// Use mostly default settings, copy from crowd.
	memcpy(&params, this->getObstacleAvoidanceParams(0), sizeof(dtObstacleAvoidanceParams));

	// Low (11)
	params.velBias = 0.5f;
	params.adaptiveDivs = 5;
	params.adaptiveRings = 2;
	params.adaptiveDepth = 1;
	this->setObstacleAvoidanceParams(0, &params);

	// Medium (22)
	params.velBias = 0.5f;
	params.adaptiveDivs = 5;
	params.adaptiveRings = 2;
	params.adaptiveDepth = 2;
	this->setObstacleAvoidanceParams(1, &params);

	// Good (45)
	params.velBias = 0.5f;
	params.adaptiveDivs = 7;
	params.adaptiveRings = 2;
	params.adaptiveDepth = 3;
	this->setObstacleAvoidanceParams(2, &params);

	// High (66)
	params.velBias = 0.5f;
	params.adaptiveDivs = 7;
	params.adaptiveRings = 3;
	params.adaptiveDepth = 3;

	this->setObstacleAvoidanceParams(3, &params);

	return true;
}

void dtCrowd::setObstacleAvoidanceParams(const int idx, const dtObstacleAvoidanceParams* params)
{
	if (idx >= 0 && idx < DT_CROWD_MAX_OBSTAVOIDANCE_PARAMS)
		memcpy(&m_obstacleQueryParams[idx], params, sizeof(dtObstacleAvoidanceParams));
}

const dtObstacleAvoidanceParams* dtCrowd::getObstacleAvoidanceParams(const int idx) const
{
	if (idx >= 0 && idx < DT_CROWD_MAX_OBSTAVOIDANCE_PARAMS)
		return &m_obstacleQueryParams[idx];
	return 0;
}

void dtCrowd::update(const float dt, Sample* sample, dtCrowdAgentDebugInfo* debug)
{
	if (!this || !this->getActiveAgentsCount()) return;
	dtAIAgent** agents = m_activeAgents;
	int nagents = getActiveAgents(agents, m_maxAgents);

	InputGeom* inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();
	for (int i = 0 ; i < nagents ; i++)
	{
		dtAIAgent* ag = agents[i];
		if (ag->isAgentActive())
		{
			ag->updateLua();
			//ag->updateSteering(dt, this->m_navquery, this->getFilter(), this->getQueryExtents(), this->mTargets, inputGeom);
		}
	}

	for (int i = 0 ; i < this->getAgentCount() ; ++i)
	{
		if (this->getAgent(i)->isAgentActive())
			this->getAgent(i)->update(-1, dt, inputGeom);
	}

	// Check that all agents still have valid paths.
	checkPathValidty(agents, nagents, dt);

	// Update async move request and path finder.
	updateMoveRequest(dt);

	// Optimize path topology.
	updateTopologyOptimization(agents, nagents, dt);

	// Register agents to proximity grid.
	m_grid->clear();
	for (int i = 0; i < sm_agentCount; ++i)
	{
		dtAIAgent* agCrowds = sm_agentList[i];
		const float* p = agCrowds->getAgentPosition();
		const float r = agCrowds->getAgentParams()->radius;
		m_grid->addItem((unsigned short)i, p[0]-r, p[2]-r, p[0]+r, p[2]+r);
	}

	//Weiwan Liu
	// Get nearby navmesh segments and agents to collide with.
	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];

		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING)
			continue;

		// Update the collision boundary after certain distance has been passed or
		// if it has become invalid.
		const float updateThr = ag->getAgentParams()->collisionQueryRange*0.25f;
		if (dtVdist2DSqr(ag->getAgentPosition(), ag->getAgentBoundary().getCenter()) > dtSqr(updateThr) ||
			!ag->getAgentBoundary().isValid(m_navquery, &m_filter))
		{
			ag->getAgentBoundary().update(ag->getAgentCorridor()->getFirstPoly(), ag->getAgentPosition(), ag->getAgentParams()->collisionQueryRange,
				m_navquery, &m_filter);
		}

		ag->setAgentNNeis( getNeighbours(ag->getAgentPosition(), ag->getAgentParams()->height, ag->getAgentParams()->collisionQueryRange,
			ag, ag->getAgentNeis(), DT_CROWDAGENT_MAX_NEIGHBOURS,
			sm_agentList, sm_agentCount, m_grid) );
	}

	// Find next corner to steer to.
	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];

		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING || ag->isIdle())
			continue;

		// Find corners for steering
		ag->setAgentNCorners(ag->getAgentCorridor()->findCorners(ag->cornerVerts, ag->cornerFlags, ag->cornerPolys,
			DT_CROWDAGENT_MAX_CORNERS, m_navquery, &m_filter));

		// Check to see if the corner after the next corner is directly visible,
		// and short cut to there.
		if ((ag->getAgentParams()->updateFlags & DT_CROWD_OPTIMIZE_VIS) && ag->getNCorners() > 0)
		{
			const float* target = &ag->cornerVerts[dtMin(1, ag->getNCorners()-1)*3];
			ag->getAgentCorridor()->optimizePathVisibility(target, ag->getAgentParams()->pathOptimizationRange, m_navquery, &m_filter);
		}
	}

#ifdef OFF_MESH_CONNECTIONS
	// Trigger off-mesh connections (depends on corners).
	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];

		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING)
			continue;

		// Check 
		const float triggerRadius = ag->getAgentParams()->radius*2.25f;
		if (ag->overOffmeshConnection(triggerRadius))
		{
			// Prepare to off-mesh connection.
			const int idx = ag - m_agents;
			dtCrowdAgentAnimation* anim = &m_agentAnims[idx];

			// Adjust the path over the off-mesh connection.
			dtPolyRef refs[2];
			if (ag->getAgentCorridor()->moveOverOffmeshConnection(ag->cornerPolys[ag->getNCorners()-1], refs,
				anim->startPos, anim->endPos, m_navquery))
			{
				dtVcopy(anim->initPos, ag->getAgentPosition());
				anim->polyRef = refs[1];
				anim->active = 1;
				anim->t = 0.0f;
				anim->tmax = (dtVdist2D(anim->startPos, anim->endPos) / ag->getMaxSpeed()) * 0.5f;

				ag->setState(DT_CROWDAGENT_STATE_OFFMESH);
				ag->setAgentNCorners(0);
				ag->setAgentNNeis(0);
				continue;
			}
			else
			{
				// TODO: Off-mesh connection failed, replan.
			}

		}
	}
#endif

	// Calculate steering.
	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];

		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING)
			continue;

		float *dvel = ag->getAgentDVel();

		if (ag->getNCorners())
		{
			// Calculate steering direction.
			if (ag->getAgentParams()->updateFlags & DT_CROWD_ANTICIPATE_TURNS)
				ag->calcSmoothSteerDirection(dvel);
			else
				ag->calcStraightSteerDirection(dvel);
		}
		// Calculate speed scale, which tells the agent to slowdown at the end of the path.
		const float slowDownRadius = ag->getAgentParams()->radius*2;	// TODO: make less hacky.
		const float speedScale = ag->getDistanceToCorner(slowDownRadius) / slowDownRadius;

		ag->setAgentDesiredSpeed(ag->getMaxSpeed());
		if (ag->hasPathFindingSteeringBehavior())
		{
			dtVscale(dvel, dvel, ag->getAgentSpeed() * speedScale);
		}
		// Separation
		if (ag->getAgentParams()->updateFlags & DT_CROWD_SEPARATION)
		{
			//TODO Check separation const float separationDist = ag->getAgentParams()->collisionQueryRange; 
			const float separationDist = ag->getAgentParams()->collisionQueryRange*0.2f; 
			const float invSeparationDist = 1.0f / separationDist; 
			const float separationWeight = ag->getAgentParams()->separationWeight;

			float w = 0;
			float disp[3] = {0,0,0};
			
			//didn't change anything
			for (int j = 0; j < ag->getAgentdNNeis(); ++j)
			{
				dtAIAgent* nei = &m_agents[ag->getAgentNeis(j).idx];


				float diff[3];
				dtVsub(diff, ag->getAgentPosition(), nei->getAgentPosition());
				diff[1] = 0;


				const float distSqr = dtVlenSqr(diff);
				if (distSqr < 0.00001f)
					continue;
				if (distSqr > dtSqr(separationDist))
					continue;
				const float dist = sqrtf(distSqr);
				const float weight = separationWeight * (1.0f - dtSqr(dist*invSeparationDist));


				dtVmad(disp, disp, diff, weight/dist);
				w += 1.0f;
			}

			if (w > 0.0001f)
			{
				// Adjust desired velocity.
				dtVmad(dvel, dvel, disp, 1.0f/w);
				// Clamp desired velocity to desired speed.
				const float speedSqr = dtVlenSqr(dvel);
				const float desiredSqr = dtSqr(ag->getAgentSpeed());
				if (speedSqr > desiredSqr)
					dtVscale(dvel, dvel, desiredSqr/speedSqr);
			}
		}

		// Set the desired velocity.
		dtVcopy(ag->getAgentDVel(), dvel);
	}

	// Velocity planning.	
	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];
		
		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING || !ag->hasPathFindingSteeringBehavior()/*ag->isIdle()*/)
			continue;

		if (ag->getAgentParams()->updateFlags & DT_CROWD_OBSTACLE_AVOIDANCE)
		{
			m_obstacleQuery->reset();

			// Add neighbours as obstacles
			for (int j = 0; j < ag->getAgentdNNeis(); ++j)
			{
				dtAIAgent* nei = sm_agentList[ag->getAgentNeis(j).idx];
				float neighbourVelocity[3];
				if (nei->hasPursuitSteeringBehaviorIdle())
					dtVset(neighbourVelocity, 0.0f, 0.0f, 0.0f);
				else
					dtVset(neighbourVelocity, nei->getAgentDVel()[0], nei->getAgentDVel()[1], nei->getAgentDVel()[2]);

				m_obstacleQuery->addCircle(nei->getAgentPosition(), nei->getAgentParams()->radius, nei->getAgentVel(), neighbourVelocity);
			}

			// Append neighbour segments as obstacles.
			for (int j = 0; j < ag->getAgentBoundary().getSegmentCount(); ++j)
			{
				const float* s = ag->getAgentBoundary().getSegment(j);
				if (dtTriArea2D(ag->getAgentPosition(), s, s+3) < 0.0f)
					continue;
				m_obstacleQuery->addSegment(s, s+3);
			}

			// Sample new safe velocity.
			bool adaptive = true;
			int ns = 0;

			const dtObstacleAvoidanceParams* params = &m_obstacleQueryParams[ag->getAgentParams()->obstacleAvoidanceType];

			if (adaptive)
			{
				ns = m_obstacleQuery->sampleVelocityAdaptive(ag->getAgentPosition(), ag->getAgentParams()->radius, ag->getAgentSpeed(),
					ag->getAgentVel(), ag->getAgentDVel(), ag->getAgentNVel(), params, NULL);
			}
			else
			{
				ns = m_obstacleQuery->sampleVelocityGrid(ag->getAgentPosition(), ag->getAgentParams()->radius, ag->getAgentSpeed(),
					ag->getAgentVel(), ag->getAgentDVel(), ag->getAgentNVel(), params, NULL);
			}
		}
		else
		{
			// If not using velocity planning, new velocity is directly the desired velocity.
			dtVcopy(ag->getAgentNVel(), ag->getAgentDVel());
		}
	}

	// Integrate.
	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];
		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING || ag->isIdle()/* || !ag->hasPathFindingSteeringBehavior()*/)
			continue;
		ag->integrateNewSpeed(dt);
	}

	// Handle collisions.
	static const float COLLISION_RESOLVE_FACTOR = 0.7f;

	for (int iter = 0; iter < 4; ++iter)
	{
		for (int i = 0; i < nagents; ++i)
		{
			dtAIAgent* ag = agents[i];
			const int idx0 = getAgentIndex(ag);

			// TODO Check necessity
			if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING /*|| !ag->hasPathFindingSteeringBehavior()*/)
				continue;

			dtVset(ag->getAgentDisp(), 0,0,0);

			float w = 0;

			for (int j = 0; j < ag->getAgentdNNeis(); ++j)
			{
				dtAIAgent* nei = sm_agentList[ag->getAgentNeis(j).idx];
				const int idx1 = getAgentIndex(nei);

				float diff[3];
				dtVsub(diff, ag->getAgentPosition(), nei->getAgentPosition());
				diff[1] = 0;

				float dist = dtVlenSqr(diff);
				if (dist > dtSqr(ag->getAgentParams()->radius + nei->getAgentParams()->radius))
					continue;
				dist = sqrtf(dist);
				float pen = (ag->getAgentParams()->radius + nei->getAgentParams()->radius) - dist;
				if (dist < 0.0001f)
				{
					// Agents on top of each other, try to choose diverging separation directions.
					if (idx0 > idx1)
					{
						dtVset(diff, -ag->getAgentDVel(2), 0, ag->getAgentDVel(0));
					}
					else
						dtVset(diff, ag->getAgentDVel(2), 0, -ag->getAgentDVel(0));
					pen = 0.01f;
				}
				else
				{
					pen = (1.0f/dist) * (pen*0.5f) * COLLISION_RESOLVE_FACTOR;
				}

				dtVmad(ag->getAgentDisp(), ag->getAgentDisp(), diff, pen);			

				w += 1.0f;
			}

			if (w > 0.0001f)
			{
				const float iw = 1.0f / w;
				dtVscale(ag->getAgentDisp(), ag->getAgentDisp(), iw);
			}
		}

		for (int i = 0; i < nagents; ++i)
		{
			dtAIAgent* ag = agents[i];
			if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING/* || !ag->hasPathFindingSteeringBehavior()*/)
				continue;

			dtVadd(ag->getAgentPosition(), ag->getAgentPosition(), ag->getAgentDisp());
			ag->setAgentPositionAbsolute(ag->getAgentPosition());
		}
	}

	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];
		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING || !ag->hasPathFindingSteeringBehavior())
			continue;

		// Move along navmesh.
		ag->getAgentCorridor()->movePosition(ag->getAgentPosition(), m_navquery, &m_filter);
		// Get valid constrained position back.
		ag->setAgentPositionAbsolute((float *) ag->getAgentCorridor()->getPos());
	}

#ifdef OFF_MESH_CONNECTIONS
	// Update agents using off-mesh connection.
	for (int i = 0; i < m_maxAgents; ++i)
	{
		dtCrowdAgentAnimation* anim = &m_agentAnims[i];
		dtAIAgent* ag = agents[i];
		if (!anim->active || !ag->hasPathFindingSteeringBehavior())
			continue;

		anim->t += dt;
		if (anim->t > anim->tmax)
		{
			// Reset animation
			anim->active = 0;
			// Prepare agent for walking.
			ag->setState(DT_CROWDAGENT_STATE_WALKING);
			continue;
		}

		// Update position
		const float ta = anim->tmax*0.15f;
		const float tb = anim->tmax;
		if (anim->t < ta)
		{
			const float u = tween(anim->t, 0.0, ta);
			dtVlerp(ag->getAgentPosition(), anim->initPos, anim->startPos, u);
			ag->setAgentPositionAbsolute(ag->getAgentPosition());
		}
		else
		{
			const float u = tween(anim->t, ta, tb);
			dtVlerp(ag->getAgentPosition(), anim->startPos, anim->endPos, u);
			ag->setAgentPositionAbsolute(ag->getAgentPosition());
		}

		// Update velocity.
		ag->setAgentVelocity(0,0,0);
		dtVset(ag->getAgentDVel(), 0,0,0);
	}
#endif

	float totalTime = EnginePtr->GetTotalGameTime();
	//InputGeom* inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();		
	for (int i = 0; i < this->getAgentCount(); ++i)
	{
		if (this->getAgent(i)->isAgentActive())
			//this->getAgent(i)->update(totalTime, dt, inputGeom);
			//this->getAgent(i)->updateGameObject(dt);
			this->getAgent(i)->update(1, dt, inputGeom);
	}
}

void dtCrowd::render()
{
	if (!this) return;

	this->m_manualObj->clear();

	Ogre::String text;
	// Show all targets	
	dtTarget* currentTarget = this->mTargets;
	while(currentTarget)
	{
		detourDrawCross(this->m_manualObj, currentTarget->getTargetPosF()[0],currentTarget->getTargetPosF()[1]+0.1f,currentTarget->getTargetPosF()[2], 1.0, Ogre::ColourValue(1.0f,1.0f,1.0f,0.75f), 2.0f, "");
		currentTarget = currentTarget->getNextTarget();
	}


	for (int i = 0 ; i < this->m_maxAgents ; i++)
		this->getAgent(i)->render(this->m_manualObj, this->m_crowdMaterial);

}

void dtCrowd::FindNearestPositionForPrevAgents(dtAIAgent* ag)
{

	ag->setAgentPositionRelative(ag->getAgentPosition()[0], ag->getAgentPosition()[1], ag->getAgentPosition()[2]);

		/*float nearest[3];
		dtPolyRef ref;
		InputGeom* inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();	
		this->getRelativeMeshPosition(ag->getAgentPosition()[0], ag->getAgentPosition()[1], ag->getAgentPosition()[2], &ref, nearest, inputGeom);

		ag->getAgentCorridor()->reset(ref, nearest);
		ag->getAgentBoundary().reset();
	
		//updateAgentParameters(ag->getAgentID(), ag->getAgentParams());

	
		ag->setAgentPosition(nearest);
	
		if (ref)
			ag->setState(DT_CROWDAGENT_STATE_WALKING);
		else
			ag->setState(DT_CROWDAGENT_STATE_INVALID);*/

		//ag->setAgentNCorners(0);
		ag->initializeLua();
}

int dtCrowd::addAgent(const float* pos, const dtCrowdAgentParams* params, dtTarget* activeTarget,char* type, Ogre::String meshName, Ogre::String hkxName , Ogre::String luaFile, Ogre::String characterName, GameObject* gObj)
{
	// Find empty slot.
	int idx = -1;
	for (int i = 0; i < m_maxAgents; ++i)
	{
		if (!m_agents[i].isAgentActive())
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return -1;

	dtAIAgent* ag = &m_agents[idx];

	setCrowdRadius(params->radius,(this->activeAgentsCount) ? idx : -1);

	ag->updateAgentParameters(params);
	ag->setAgentTopologyOptTime(0.0);
	ag->setAgentNNeis(0);

	dtVset(ag->getAgentDVel(), 0,0,0);
	dtVset(ag->getAgentNVel(), 0,0,0);
	ag->setAgentVelocity(0,0,0);

	ag->setMaxSpeed(ag->getAgentParams()->maxSpeed);
	ag->setMaxAcceleration(params->maxAcceleration);
	ag->setAgentDesiredSpeed(0);

	// Find nearest position on navmesh and place the agent there.
	
	if(EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->IsNavMeshBuilt())
	{

/*		ag->setAgentRelativePosition(pos[0], pos[1], pos[2]);

		float nearest[3];
		dtPolyRef ref;
		InputGeom* inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();	
		ag->getRelativeMeshPosition(pos[0], pos[1], pos[2], &ref, nearest, inputGeom);
		this->getRelativeMeshPosition(pos[0], pos[1], pos[2], &ref, nearest, inputGeom);

		ag->getAgentCorridor()->reset(ref, nearest);
		ag->getAgentBoundary().reset();
		ag->setAgentPosition(nearest);
	
		if (ref)
			ag->setState(DT_CROWDAGENT_STATE_WALKING);
		else
			ag->setState(DT_CROWDAGENT_STATE_INVALID);
	*/
	}


	float p[] = {0,0,0};
	ag->getAgentCorridor()->reset(-1, p);
	ag->setState(DT_CROWDAGENT_STATE_INVALID);
	ag->getAgentBoundary().reset();

	ag->setAgentNCorners(0);
	ag->readAnimationFile("AI_Animation.txt", characterName.c_str());
	ag->activateAgent(meshName, hkxName, type, luaFile, m_crowdMaterial, gObj);
	// TODO check size of global agents list
	sm_agentList[sm_agentCount++] = ag;

	return idx;
}

void dtCrowd::removeAgent(const int idx)
{
	if (idx >= 0 && idx < m_maxAgents)
		m_agents[idx].deactivateAgent();
}


dtTarget* dtCrowd::setNewTarget( float* targetPos, dtPolyRef targetRef )
{
	dtTarget* currentTarget = this->mTargets, *nextTarget = NULL;

	while(currentTarget)
	{
		nextTarget = currentTarget->getNextTarget();
		delete currentTarget;
		currentTarget = nextTarget;
	}

	dtTarget* newTarget = new dtTarget(targetPos, targetRef);
	this->mTargets = newTarget;

	m_agentsOnTarget = 0;

	return newTarget;
}

dtTarget* dtCrowd::addNewTarget( float* targetPos, dtPolyRef targetRef )
{
	dtTarget* newTarget = new dtTarget(targetPos, targetRef);
	dtTarget* currentTarget = this->mTargets, *nextTarget = NULL;

	while(currentTarget && (nextTarget = currentTarget->getNextTarget()))
		currentTarget = nextTarget;

	if (currentTarget)
		currentTarget->setNextTarget(newTarget);
	else
		this->mTargets = newTarget;

	return newTarget;
}

const dtCrowd::MoveRequest* dtCrowd::getActiveMoveTarget(const int idx) const
{
	if (idx < 0 || idx > m_maxAgents)
		return 0;

	MoveRequest* req = 0;
	// Check if there is existing request and update that instead.
	for (int i = 0; i < m_moveRequestCount; ++i)
	{
		if (m_moveRequests[i].idx == idx)
		{
			req = &m_moveRequests[i];
			break;
		}
	}

	return req;
}

bool dtCrowd::requestMoveTargetReplan(const int idx, dtPolyRef ref, const float* pos)
{
	if (idx < 0 || idx > m_maxAgents)
		return false;
	if (!ref)
		return false;

	MoveRequest* req = 0;
	// Check if there is existing request and update that instead.
	for (int i = 0; i < m_moveRequestCount; ++i)
	{
		if (m_moveRequests[i].idx == idx)
		{
			req = &m_moveRequests[i];
			break;
		}
	}
	if (!req)
	{
		if (m_moveRequestCount >= m_maxAgents)
			return false;
		req = &m_moveRequests[m_moveRequestCount++];
		memset(req, 0, sizeof(MoveRequest));
	}

	// Initialize request.
	req->idx = idx;
	req->ref = ref;
	dtVcopy(req->pos, pos);
	req->pathqRef = DT_PATHQ_INVALID;
	req->state = MR_TARGET_REQUESTING;
	req->replan = true;

	req->temp[0] = ref;
	req->ntemp = 1;

	return true;
}


bool dtCrowd::requestMoveTarget(const int idx, dtPolyRef ref, const float* pos)
{
	if (idx < 0 || idx > m_maxAgents)
		return false;
	if (!ref)
		return false;

	MoveRequest* req = 0;
	// Check if there is existing request and update that instead.
	for (int i = 0; i < m_moveRequestCount; ++i)
	{
		if (m_moveRequests[i].idx == idx)
		{
			req = &m_moveRequests[i];
			break;
		}
	}
	if (!req)
	{
		if (m_moveRequestCount >= m_maxAgents)
			return false;
		req = &m_moveRequests[m_moveRequestCount++];
		memset(req, 0, sizeof(MoveRequest));
	}

	// Initialize request.
	req->idx = idx;
	req->ref = ref;
	dtVcopy(req->pos, pos);
	req->pathqRef = DT_PATHQ_INVALID;
	req->state = MR_TARGET_REQUESTING;
	req->replan = false;

	req->temp[0] = ref;
	req->ntemp = 1;

	return true;
}


bool dtCrowd::adjustMoveTarget(const int idx, dtPolyRef ref, const float* pos)
{
	if (idx < 0 || idx > m_maxAgents)
		return false;
	if (!ref)
		return false;

	MoveRequest* req = 0;
	// Check if there is existing request and update that instead.
	for (int i = 0; i < m_moveRequestCount; ++i)
	{
		if (m_moveRequests[i].idx == idx)
		{
			req = &m_moveRequests[i];
			break;
		}
	}
	if (!req)
	{
		if (m_moveRequestCount >= m_maxAgents)
			return false;
		req = &m_moveRequests[m_moveRequestCount++];
		memset(req, 0, sizeof(MoveRequest));

		// New adjust request
		req->state = MR_TARGET_ADJUST;
		req->idx = idx;
	}

	// Set adjustment request.
	req->aref = ref;
	dtVcopy(req->apos, pos);

	return true;
}

int dtCrowd::getActiveAgents(dtAIAgent** agents, const int maxAgents)
{
	int n = 0;
	for (int i = 0; i < m_maxAgents; ++i)
	{
		if ( !m_agents[i].isAgentActive() ) continue;
		if ( n < maxAgents )
			agents[n++] = &m_agents[i];
	}
	return n;
}


void dtCrowd::updateMoveRequest(const float /*dt*/)
{
	// Fire off new requests.
	for (int i = 0; i < m_moveRequestCount; ++i)
	{
		MoveRequest* req = &m_moveRequests[i];
		dtAIAgent* ag = &m_agents[req->idx];

		// Agent not active anymore, kill request.
		if (!ag->isAgentActive())
			req->state = MR_TARGET_FAILED;

		// Adjust target
		if (req->aref)
		{
			if (req->state == MR_TARGET_ADJUST)
			{
				// Adjust existing path.
				ag->getAgentCorridor()->moveTargetPosition(req->apos, m_navquery, &m_filter);
				req->state = MR_TARGET_VALID;
			}
			else
			{
				// Adjust on the flight request.
				float result[3];
				static const int MAX_VISITED = 16;
				dtPolyRef visited[MAX_VISITED];
				int nvisited = 0;
				m_navquery->moveAlongSurface(req->temp[req->ntemp-1], req->pos, req->apos, &m_filter,
					result, visited, &nvisited, MAX_VISITED);
				req->ntemp = dtMergeCorridorEndMoved(req->temp, req->ntemp, MAX_TEMP_PATH, visited, nvisited);
				dtVcopy(req->pos, result);

				// Reset adjustment.
				dtVset(req->apos, 0,0,0);
				req->aref = 0;
			}
		}


		if (req->state == MR_TARGET_REQUESTING)
		{
			// If agent location is invalid, try to recover.
			if ( ag->getAgentState() == DT_CROWDAGENT_STATE_INVALID )
			{
				float agentPos[3];
				dtVcopy(agentPos, ag->getAgentPosition());
				dtPolyRef agentRef = ag->getAgentCorridor()->getFirstPoly();
				if (!m_navquery->isValidPolyRef(agentRef, &m_filter))
				{
					// Current location is not valid, try to reposition.
					// TODO: this can snap agents, how to handle that?
					float nearest[3];
					agentRef = 0;
					m_navquery->findNearestPoly(ag->getAgentPosition(), m_ext, &m_filter, &agentRef, nearest);
					dtVcopy(agentPos, nearest);
				}
				if (!agentRef)
				{
					// Current location still outside navmesh, fail the request.
					req->state = MR_TARGET_FAILED;
					continue;
				}

				// Could relocation agent on navmesh again.
				ag->setState(DT_CROWDAGENT_STATE_WALKING);
				ag->getAgentCorridor()->reset(agentRef, agentPos);
			}

			// Calculate request position.

			if (req->replan)
			{
				// Replan from the end of the current path.
				dtPolyRef reqRef = ag->getAgentCorridor()->getLastPoly();
				float reqPos[3];
				dtVcopy(reqPos, ag->getAgentCorridor()->getTarget());
				req->pathqRef = m_pathq.request(reqRef, req->ref, reqPos, req->pos, &m_filter);
				if (req->pathqRef != DT_PATHQ_INVALID)
				{
					req->state = MR_TARGET_WAITING_FOR_PATH;
				}
			}
			else
			{
				// If there is a lot of latency between requests, it is possible to
				// project the current position ahead and use raycast to find the actual
				// location and path.
				const dtPolyRef* path = ag->getAgentCorridor()->getPath();
				const int npath = ag->getAgentCorridor()->getPathCount();
				dtAssert(npath);
				// Here we take the simple approach and set the path to be just the current location.
				float reqPos[3];
				dtVcopy(reqPos, ag->getAgentCorridor()->getPos());	// The location of the request
				dtPolyRef reqPath[8];					// The path to the request location
				reqPath[0] = path[0];
				int reqPathCount = 1;

				req->pathqRef = m_pathq.request(reqPath[reqPathCount-1], req->ref, reqPos, req->pos, &m_filter);
				if (req->pathqRef != DT_PATHQ_INVALID)
				{
					ag->getAgentCorridor()->setCorridor(reqPos, reqPath, reqPathCount);
					req->state = MR_TARGET_WAITING_FOR_PATH;
				}
			}
		}
	}


	// Update requests.
	m_pathq.update(MAX_ITERS_PER_UPDATE);

	// Process path results.
	for (int i = 0; i < m_moveRequestCount; ++i)
	{
		MoveRequest* req = &m_moveRequests[i];
		dtAIAgent* ag = &m_agents[req->idx];

		if (req->state == MR_TARGET_WAITING_FOR_PATH)
		{
			// Poll path queue.
			dtStatus status = m_pathq.getRequestStatus(req->pathqRef);
			if (dtStatusFailed(status))
			{
				req->pathqRef = DT_PATHQ_INVALID;
				req->state = MR_TARGET_FAILED;
			}
			else if (dtStatusSucceed(status))
			{
				const dtPolyRef* path = ag->getAgentCorridor()->getPath();
				const int npath = ag->getAgentCorridor()->getPathCount();
				dtAssert(npath);

				// Apply results.
				float targetPos[3];
				dtVcopy(targetPos, req->pos);

				dtPolyRef* res = m_pathResult;
				bool valid = true;
				int nres = 0;
				dtStatus status = m_pathq.getPathResult(req->pathqRef, res, &nres, m_maxPathResult);
				if (dtStatusFailed(status) || !nres)
					valid = false;

				// Merge with any target adjustment that happened during the search.
				if (req->ntemp > 1)
				{
					nres = dtMergeCorridorEndMoved(res, nres, m_maxPathResult, req->temp, req->ntemp);
				}

				// Merge result and existing path.
				// The agent might have moved whilst the request is
				// being processed, so the path may have changed.
				// We assume that the end of the path is at the same location
				// where the request was issued.

				// The last ref in the old path should be the same as
				// the location where the request was issued..
				if (valid && path[npath-1] != res[0])
					valid = false;

				if (valid)
				{
					// Put the old path infront of the old path.
					if (npath > 1)
					{
						// Make space for the old path.
						if ((npath-1)+nres > m_maxPathResult)
							nres = m_maxPathResult - (npath-1);

						memmove(res+npath-1, res, sizeof(dtPolyRef)*nres);
						// Copy old path in the beginning.
						memcpy(res, path, sizeof(dtPolyRef)*(npath-1));
						nres += npath-1;

						// Remove trackbacks
						for (int j = 0; j < nres; ++j)
						{
							if (j-1 >= 0 && j+1 < nres)
							{
								if (res[j-1] == res[j+1])
								{
									memmove(res+(j-1), res+(j+1), sizeof(dtPolyRef)*(nres-(j+1)));
									nres -= 2;
									j -= 2;
								}
							}
						}

					}

					// Check for partial path.
					if (res[nres-1] != req->ref)
					{
						// Partial path, constrain target position inside the last polygon.
						float nearest[3];
						if (m_navquery->closestPointOnPoly(res[nres-1], targetPos, nearest) == DT_SUCCESS)
							dtVcopy(targetPos, nearest);
						else
							valid = false;
					}
				}

				if (valid)
				{
					// Set current corridor.
					ag->getAgentCorridor()->setCorridor(targetPos, res, nres);
					// Force to update boundary.
					ag->getAgentBoundary().reset();
					req->state = MR_TARGET_VALID;
				}
				else
				{
					// Something went wrong.
					req->state = MR_TARGET_FAILED;
				}
			}
		}

		// Remove request when done with it.
		if (req->state == MR_TARGET_VALID || req->state == MR_TARGET_FAILED)
		{
			m_moveRequestCount--;
			if (i != m_moveRequestCount)
				memcpy(&m_moveRequests[i], &m_moveRequests[m_moveRequestCount], sizeof(MoveRequest));
			--i;
		}
	}

}

void dtCrowd::updateTopologyOptimization(dtAIAgent** agents, const int nagents, const float dt)
{
	if (!nagents)
		return;

	const float OPT_TIME_THR = 0.5f; // seconds
	const int OPT_MAX_AGENTS = 1;
	dtAIAgent* queue[OPT_MAX_AGENTS];
	int nqueue = 0;

	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];
		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING)
			continue;
		if ((ag->getAgentParams()->updateFlags & DT_CROWD_OPTIMIZE_TOPO) == 0)
			continue;
		ag->setAgentTopologyOptTime( ag->getAgentTopologyOptTime() + dt ) ;
		if (ag->getAgentTopologyOptTime() >= OPT_TIME_THR)
			nqueue = ag->addToOptQueue(queue, nqueue, OPT_MAX_AGENTS);
	}

	for (int i = 0; i < nqueue; ++i)
	{
		dtAIAgent* ag = queue[i];
		ag->getAgentCorridor()->optimizePathTopology(m_navquery, &m_filter);
		ag->setAgentTopologyOptTime(0);
	}

}

void dtCrowd::checkPathValidty(dtAIAgent** agents, const int nagents, const float dt)
{
	static const int CHECK_LOOKAHEAD = 10;

	for (int i = 0; i < nagents; ++i)
	{
		dtAIAgent* ag = agents[i];

		if (!ag->hasPathFindingSteeringBehavior()) continue;

		if (ag->getAgentState() != DT_CROWDAGENT_STATE_WALKING)
			continue;

		// Skip if the corridor is valid
		if (ag->getAgentCorridor()->isValid(CHECK_LOOKAHEAD, m_navquery, &m_filter))
			continue;

		// The current path is bad, try to recover.

		// Store current state.
		float agentPos[3];
		dtPolyRef agentRef = 0;
		dtVcopy(agentPos, ag->getAgentPosition());
		agentRef = ag->getAgentCorridor()->getFirstPoly();

		float targetPos[3];
		dtPolyRef targetRef = 0;
		dtVcopy(targetPos, ag->getAgentCorridor()->getTarget());
		targetRef = ag->getAgentCorridor()->getLastPoly();
		// If has active move target, get target location from that instead.
		const int idx = ag->getAgentID();
		const MoveRequest* req = getActiveMoveTarget(idx);
		if (req)
		{
			if (req->state == MR_TARGET_REQUESTING ||
				req->state == MR_TARGET_WAITING_FOR_PATH ||
				req->state == MR_TARGET_VALID)
			{
				targetRef = req->ref;
				dtVcopy(targetPos, req->pos);
			}
			else if (req->state == MR_TARGET_ADJUST)
			{
				targetRef = req->aref;
				dtVcopy(targetPos, req->apos);
			}
		}

		// First check that the current location is valid.
		if (!m_navquery->isValidPolyRef(agentRef, &m_filter))
		{
			// Current location is not valid, try to reposition.
			// TODO: this can snap agents, how to handle that?
			float nearest[3];
			agentRef = 0;
			m_navquery->findNearestPoly(ag->getAgentPosition(), m_ext, &m_filter, &agentRef, nearest);
			dtVcopy(agentPos, nearest);
		}

		if (!agentRef)
		{
			// Count not find location in navmesh, set state to invalid.
			ag->getAgentCorridor()->reset(0, agentPos);
			ag->getAgentBoundary().reset();
			ag->setState(DT_CROWDAGENT_STATE_INVALID);
			continue;
		}

		// TODO: make temp path by raycasting towards current velocity.

		ag->getAgentCorridor()->trimInvalidPath(agentRef, agentPos, m_navquery, &m_filter);
		ag->getAgentBoundary().reset();
		ag->setAgentPositionAbsolute(agentPos);


		// Check that target is still reachable.
		if (!m_navquery->isValidPolyRef(targetRef, &m_filter))
		{
			// Current target is not valid, try to reposition.
			float nearest[3];
			targetRef = 0;
			m_navquery->findNearestPoly(targetPos, m_ext, &m_filter, &targetRef, nearest);
			dtVcopy(targetPos, nearest);
		}

		if (!targetRef)
		{
			// Target not reachable anymore, cannot replan.
			// TODO: indicate failure!
			continue;
		}

		// Try to replan path to goal.
		requestMoveTargetReplan(idx, targetRef, targetPos);
	}
}

bool dtCrowd::setCrowdRadius(float radius,int agent_ID)
{
	float max = m_maxAgentRadius;
	if( max < radius || agent_ID == -1)
	{
		m_maxAgentRadiusIndex = agent_ID;
		max = radius;
	
		this->m_maxAgentRadius = max;
		

		dtVset(m_ext, m_maxAgentRadius*2.0f,m_maxAgentRadius*1.5f,m_maxAgentRadius*2.0f);
		if(!EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->IsNavMeshBuilt())
			EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->setBuildRadius(max);

		if (m_grid)
		{
			dtFreeProximityGrid(m_grid);
			m_grid = 0;
		}

		m_grid = dtAllocProximityGrid();
		if (!m_grid)
			return false;
		if (!m_grid->init(m_maxAgents*4, this->m_maxAgentRadius*3))
			return false;
	}

	return true;
}

void dtCrowd::updateCrowdRadius(int agent_ID)
{
	if(m_maxAgentRadiusIndex == agent_ID)
	{
		float max  = 0;
		for(int i = 0 ; i < m_maxAgents; i++)
		{
			if(!m_agents[i].isAgentActive())
				break;
			if(m_agents[i].getAgentParams()->radius > max)
			{

				max = m_agents[i].getAgentParams()->radius;
				m_maxAgentRadiusIndex = i;
			}
		}
		this->m_maxAgentRadius = 0;
		setCrowdRadius(max,m_maxAgentRadiusIndex);
	}
}

bool dtCrowd::getRelativeMeshPosition( float x, float y, float z, dtPolyRef* nearestRef, float* nearestPt, InputGeom* inputGeom)
{
	Ogre::Vector3 newPosition (x, y, z);
	Ogre::Ray pRay(newPosition, Ogre::Vector3::NEGATIVE_UNIT_Y);
	if (!inputGeom) inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();
	float pHitPointDistance;	
	if (inputGeom->raycastMesh(pRay, pHitPointDistance, true))
	{
		float pHitPoint[3];
		pHitPoint[0] = pRay.getOrigin().x + pRay.getDirection().x * pHitPointDistance;
		pHitPoint[1] = pRay.getOrigin().y + pRay.getDirection().y * pHitPointDistance;
		pHitPoint[2] = pRay.getOrigin().z + pRay.getDirection().z * pHitPointDistance;

		// TODO check navquery usage with return values
		float negativeYext[3];
		negativeYext[0] = this->getQueryExtents()[0]; negativeYext[1] = (y - pHitPoint[1]); negativeYext[2] = this->getQueryExtents()[2];

		
		this->getNavMeshQuery()->findNearestPolyNegativeY(pHitPoint, negativeYext, this->getFilter(), nearestRef, nearestPt);

		if (!(*nearestRef))
			this->getNavMeshQuery()->findNearestPoly(pHitPoint, this->getQueryExtents(), this->getFilter(), nearestRef, nearestPt);
			//this->getNavMeshQuery()->findNearestPoly(pHitPoint, negativeYext, this->getFilter(), nearestRef, nearestPt);

		return true;
	}

	return false;
}

dtTarget* dtCrowd::getRandomMeshTarget()
{
	int iMaxTriesLeft = 2000;
	while (iMaxTriesLeft > 0)
	{
		AIWorldManager *aiWorldManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager();
	
		const float* minBounds = aiWorldManager->getSample()->getBoundsMin();
		const float* maxBounds = aiWorldManager->getSample()->getBoundsMax();

		float rMinBounds[3], rMaxBounds[3];
		for (int i = 0 ; i < 3 ; i++) 
		{
			rMinBounds[i] = minBounds[i] + 1E-5f;
			rMaxBounds[i] = maxBounds[i] - 1E-5f;
		}

		float xRange = rMaxBounds[0] - rMinBounds[0];
		float yRange = rMaxBounds[1] - rMinBounds[1];
		float zRange = rMaxBounds[2] - rMinBounds[2];

		if (xRange < 1E-5f) { xRange = 0; }
		if (yRange < 1E-5f) { yRange = 0; }
		if (zRange < 1E-5f) { zRange = 0; }

		while (iMaxTriesLeft-- % 100)
		{
			float x = rMinBounds[0] + RAND_FLOAT*xRange;
			float y = rMinBounds[1] + RAND_FLOAT*yRange;
			float z = rMinBounds[2] + RAND_FLOAT*zRange;

			float targetPos[3];
			dtPolyRef targetRef;
			InputGeom* inputGeom = aiWorldManager->getInputGeom();
			if (this->getRelativeMeshPosition(x, y, z, &targetRef, targetPos, inputGeom))
			{
				if (targetRef)
					return new dtTarget(targetPos, targetRef);
			}
		}
	}

	return NULL;
}

dtTarget* dtCrowd::getRandomMeshTarget(dtAIAgent* ag)
{
	float *pos = ag->getAgentPosition();
	float radius = ag->getRadius();
	Ogre::Vector3 agVel = ag->getAgentVelV();
	int iMaxTriesLeft = 2000;

	if (agVel.length() == 0) 
	{
		agVel.x = RAND_FLOAT;
		agVel.y = RAND_FLOAT;
		agVel.z = RAND_FLOAT;
	}

	while (iMaxTriesLeft > 0)
	{
		Ogre::Vector3 vel = Ogre::Quaternion(Ogre::Degree((Ogre::Real)(((double) (rand() % 180)) - 90.0)), Ogre::Vector3::UNIT_Y) * agVel;
		vel.normalise();
		float newVelocity[3];
		newVelocity[0] = vel.x; newVelocity[1] = abs(2*vel.y); newVelocity[2] = vel.z;

		AIWorldManager *aiWorldManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager();
	
		while (iMaxTriesLeft-- % 100)
		{
			int probability = rand() % 100;
			float range;
			if (probability < 45)
				range = 4 * radius;
			else if (probability < 70)
				range = 7 * radius;
			else if (probability < 85)
				range = 11 * radius;
			else if (probability < 95)
				range = 16 * radius;
			else
				range = 22 * radius;
			
			float newPos[3];
			dtVmad(newPos, pos, newVelocity, range);

			float targetPos[3];
			dtPolyRef targetRef;
			InputGeom* inputGeom = aiWorldManager->getInputGeom();
			if (this->getRelativeMeshPosition(newPos[0], newPos[1], newPos[2], &targetRef, targetPos, inputGeom))
			{
				if (targetRef)
					return new dtTarget(targetPos, targetRef);
			}
		}
	}
	return NULL;
}

void dtCrowd::resetStaticMembers()
{
	dtCrowd::sm_agentList = (dtAIAgent**)dtAlloc(sizeof(dtAIAgent*)*128, DT_ALLOC_PERM);
	dtCrowd::sm_agentCount = 0;
}

/************************************************************************/
/* DETOUR TARGET                                                        */
/************************************************************************/
dtTarget::dtTarget(float targetPos[3], dtPolyRef targetRef, dtTarget *nextTarget)
{
	this->setTargetPos(targetPos);
	this->setTargetRef(targetRef);
	this->setNextTarget(nextTarget);
}
dtTarget::dtTarget(float targetPosX, float targetPosY, float targetPosZ, dtPolyRef targetRef, dtTarget *nextTarget)
{
	this->setTargetPos(targetPosX, targetPosY, targetPosZ);
	this->setTargetRef(targetRef);
	this->setNextTarget(nextTarget);
}

/************************************************************************/
/* DETOUR CROWD HANDLER CLASS                                           */
/************************************************************************/
dtCrowdHandler::dtCrowdHandler( dtCrowd* crowd /*= NULL*/, dtCrowdHandler* nextCrowdHandler /*= NULL*/ ) : mCrowd(0), mNextCrowdHandler(0)
{
	this->setCrowd(crowd); this->setNextCrowdHandler(nextCrowdHandler);
}

dtCrowdHandler::~dtCrowdHandler()
{
	dtFreeCrowd(mCrowd); this->mCrowd = NULL;
}

void dtCrowdHandler::setCrowd( dtCrowd* crowd /*= NULL*/ )
{
	dtFreeCrowd(mCrowd); this->mCrowd = crowd;
}

void dtCrowdHandler::setNextCrowdHandler( dtCrowdHandler* nextCrowdHandler /*= NULL*/ )
{
	this->mNextCrowdHandler = nextCrowdHandler;
}
