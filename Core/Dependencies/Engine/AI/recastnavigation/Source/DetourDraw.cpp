//
// Copyright (c) 2009-2010 Mikko Mononen memon@inside.org
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//

#include "StdAfx.h"
#include <math.h>
#include "DebugDraw.h"
#include "ParentDraw.h"
#include "DetourDraw.h"
#include "DetourNavMesh.h"
#include "DetourCommon.h"
#include "DetourNode.h"
#include "DetourDebugDraw.h"
#include "DebugDraw.h"
#include "OgreRenderOperation.h"

static void drawPolyBoundaries(Ogre::ManualObject* dd, const dtMeshTile* tile, Ogre::ColourValue col, const float linew, bool inner, Ogre::String materialName) /*"BaseWhiteNoLighting"*/
{
	static const float thr = 0.01f*0.01f;

	dd->begin(materialName,Ogre::RenderOperation::OT_LINE_LIST);

	for (int i = 0; i < tile->header->polyCount; ++i)
	{
		const dtPoly* p = &tile->polys[i];
		
		if (p->getType() == DT_POLYTYPE_OFFMESH_CONNECTION) continue;
		
		const dtPolyDetail* pd = &tile->detailMeshes[i];
		
		for (int j = 0, nj = (int)p->vertCount; j < nj; ++j)
		{
			Ogre::ColourValue c = col;
			Ogre::ColourValue col33;
			if (inner)
			{
				if (p->neis[j] == 0) continue;
				if (p->neis[j] & DT_EXT_LINK)
				{
					bool con = false;
					for (unsigned int k = p->firstLink; k != DT_NULL_LINK; k = tile->links[k].next)
					{
						if (tile->links[k].edge == j)
						{
							con = true;
							break;
						}
					}
					if (con)
						col33.setAsRGBA(duRGBA(255,255,255,48));
					else
						col33.setAsRGBA(duRGBA(0,0,0,48));
				}
				else
					col33.setAsRGBA(duRGBA(0,48,64,32));
			}
			else
			{
				if (p->neis[j] != 0) continue;
			}
			
			const float* v0 = &tile->verts[p->verts[j]*3];
			const float* v1 = &tile->verts[p->verts[(j+1) % nj]*3];
			
			// Draw detail mesh edges which align with the actual poly edge.
			// This is really slow.
			for (int k = 0; k < pd->triCount; ++k)
			{
				const unsigned char* t = &tile->detailTris[(pd->triBase+k)*4];
				const float* tv[3];
				for (int m = 0; m < 3; ++m)
				{
					if (t[m] < p->vertCount)
						tv[m] = &tile->verts[p->verts[t[m]]*3];
					else
						tv[m] = &tile->detailVerts[(pd->vertBase+(t[m]-p->vertCount))*3];
				}
				for (int m = 0, n = 2; m < 3; n=m++)
				{
					if (((t[3] >> (n*2)) & 0x3) == 0) continue;	// Skip inner detail edges.
					if (distancePtLine2d(tv[n],v0,v1) < thr &&
						distancePtLine2d(tv[m],v0,v1) < thr)
					{
						dd->position(tv[n][0], tv[n][1],tv[n][2]);
						dd->colour(col33);
						dd->position(tv[m][0], tv[m][1],tv[m][2]);
						dd->colour(col33);
					}
				}
			}
		}
	}
	dd->end();
}

static void drawMeshTile(Ogre::ManualObject* dd, const dtNavMesh& mesh, const dtNavMeshQuery* query, const dtMeshTile* tile, unsigned char flags, Ogre::String materialName)
{
	dtPolyRef base = mesh.getPolyRefBase(tile);

	int tileNum = mesh.decodePolyIdTile(base);

	dd->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);
	for (int i = 0; i < tile->header->polyCount; ++i)
	{
		const dtPoly* p = &tile->polys[i];
		if (p->getType() == DT_POLYTYPE_OFFMESH_CONNECTION)	// Skip off-mesh links.
			continue;
			
		const dtPolyDetail* pd = &tile->detailMeshes[i];

		 Ogre::ColourValue col;
		if (query && query->isInClosedList(base | (dtPolyRef)i))
			col.setAsRGBA(duRGBA(255,196,0,64));
		else
		{
			if (flags & DU_DRAWNAVMESH_COLOR_TILES)
			{
				col.setAsRGBA(duIntToCol(tileNum, 128));
			}
			else
			{
				if (p->getArea() == 0) // Treat zero area type as default.
					col.setAsRGBA(duRGBA(0,192,255,64));
				else
					col.setAsRGBA(duIntToCol(p->getArea(), 64));
			}
		}
		
		for (int j = 0; j < pd->triCount; ++j)
		{
			const unsigned char* t = &tile->detailTris[(pd->triBase+j)*4];
			for (int k = 0; k < 3; ++k)
			{
				if (t[k] < p->vertCount)
				{
					const float * tt = &tile->verts[p->verts[t[k]]*3];
					dd->position(tt[0], tt[1], tt[2]);
					dd->colour(col);
				}
				else
				{
					const float * tt = &tile->detailVerts[(pd->vertBase+t[k]-p->vertCount)*3];
					dd->position(tt[0], tt[1], tt[2]);
					dd->colour(col);
				}
			}
		}
	}
	dd->end();
	
	// Draw inter poly boundaries
	//drawPolyBoundaries(dd, tile, duRGBA(0,48,64,32), 1.5f, true);
	
	// Draw outer poly boundaries
	//drawPolyBoundaries(dd, tile, duRGBA(0,48,64,220), 2.5f, false);

	/*if (flags & DRAWNAVMESH_OFFMESHCONS)
	{
		dd->begin(materialName); // 2.0 f
		for (int i = 0; i < tile->header->polyCount; ++i)
		{
			const dtPoly* p = &tile->polys[i];
			if (p->getType() != DT_POLYTYPE_OFFMESH_CONNECTION)	// Skip regular polys.
				continue;
			
			Ogre::ColourValue col;
			unsigned int colInt;
			if (query && query->isInClosedList(base | (dtPolyRef)i))
			{
				col.setAsRGBA(duRGBA(255,196,0,220));
				colInt = duRGBA(255,196,0,220);
			}
			else
			{
				col.setAsRGBA(duDarkenCol(duIntToCol(p->getArea(), 220)));
				colInt = duDarkenCol(duIntToCol(p->getArea(), 220));
			}
			
			const dtOffMeshConnection* con = &tile->offMeshCons[i - tile->header->offMeshBase];
			const float* va = &tile->verts[p->verts[0]*3];
			const float* vb = &tile->verts[p->verts[1]*3];

			// Check to see if start and end end-points have links.
			bool startSet = false;
			bool endSet = false;
			for (unsigned int k = p->firstLink; k != DT_NULL_LINK; k = tile->links[k].next)
			{
				if (tile->links[k].edge == 0)
					startSet = true;
				if (tile->links[k].edge == 1)
					endSet = true;
			}
			
			// End points and their on-mesh locations. 
			if (startSet)
			{
				dd->position(va[0],va[1],va[2]);
				dd->colour(col);
				dd->position(con->pos[0],con->pos[1],con->pos[2]);
				dd->colour(col);
				duAppendCircle(dd, con->pos[0],con->pos[1]+0.1f,con->pos[2], con->rad, duRGBA(0,48,64,196));
			}
			if (endSet)
			{
				dd->position(vb[0],vb[1],vb[2]);
				dd->colour(col);
				dd->position(con->pos[3],con->pos[4],con->pos[5]);
				dd->colour(col);
				duAppendCircle(dd, con->pos[3],con->pos[4]+0.1f,con->pos[5], con->rad, duRGBA(0,48,64,196));
			}	
			
			// End point vertices.
			Ogre::ColourValue col2;
			dd->position(con->pos[0],con->pos[1],con->pos[2]);
			col2.setAsRGBA(duRGBA(0,48,64,196));
			dd->colour(col2);
			dd->position(con->pos[0],con->pos[1]+0.2f,con->pos[2]);
			col2.setAsRGBA(duRGBA(0,48,64,196));
			dd->colour(col2);
			
			dd->position(con->pos[3],con->pos[4],con->pos[5]);
			col2.setAsRGBA(duRGBA(0,48,64,196));
			dd->colour(col2);
			dd->position(con->pos[3],con->pos[4]+0.2f,con->pos[5]);
			col2.setAsRGBA(duRGBA(0,48,64,196));
			dd->colour(col2);
			
			// Connection arc.
			duAppendArc(dd, con->pos[0],con->pos[1],con->pos[2], con->pos[3],con->pos[4],con->pos[5], 0.25f,
						(con->flags & 1) ? 0.6f : 0, 0.6f, colInt);
		}
		dd->end();
	}
	
	const unsigned int vcol = duRGBA(0,0,0,196);
	dd->begin(materialName);//thicker!!!
	for (int i = 0; i < tile->header->vertCount; ++i)
	{
		Ogre::ColourValue col3;
		const float* v = &tile->verts[i*3];
		dd->position(v[0], v[1], v[2]);
		col3.setAsRGBA(vcol);
		dd->colour(col3);
	}
	dd->end();*/

	
}

void detourDrawNavMeshPoly(Ogre::ManualObject* dd, const dtNavMesh& mesh, dtPolyRef ref, const Ogre::ColourValue col, Ogre::String materialName)
{
	if (!dd) return;
	
	const dtMeshTile* tile = 0;
	const dtPoly* poly = 0;
	if (dtStatusFailed(mesh.getTileAndPolyByRef(ref, &tile, &poly)))
		return;
	
	float* fCol = duDivCol((getColorValue(col) & 0x00ffffff) | (64 << 24));
	Ogre::ColourValue c(fCol[0],fCol[1], fCol[2], fCol[3]);
	const unsigned int ip = (unsigned int)(poly - tile->polys);

	if (poly->getType() == DT_POLYTYPE_OFFMESH_CONNECTION)
	{
		dtOffMeshConnection* con = &tile->offMeshCons[ip - tile->header->offMeshBase];

		dd->begin(materialName, Ogre::RenderOperation::OT_LINE_LIST);//2.0 f

		// Connection arc.
		detourAppendArc(dd, con->pos[0],con->pos[1],con->pos[2], con->pos[3],con->pos[4],con->pos[5], 0.25f, (con->flags & 1) ? 0.6f : 0, 0.6f, c);
		
		dd->end();
	}
	else
	{
		const dtPolyDetail* pd = &tile->detailMeshes[ip];
		Ogre::ColourValue col(c);

		dd->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);
		for (int i = 0; i < pd->triCount; ++i)
		{
			const unsigned char* t = &tile->detailTris[(pd->triBase+i)*4];
			for (int j = 0; j < 3; ++j)
			{
				if (t[j] < poly->vertCount)
				{
					const float* tt = &tile->verts[poly->verts[t[j]]*3];
					dd->position(tt[0],tt[1],tt[2]);
					dd->colour(col);
				}
				else
				{
					const float* tt = &tile->detailVerts[(pd->vertBase+t[j]-poly->vertCount)*3];
					dd->position(tt[0],tt[1],tt[2]);
					dd->colour(col);
				}
			}
		}
		dd->end();
	}
	
	

}

void detourDrawNavMeshWithClosedList(Ogre::ManualObject* dd, const dtNavMesh& mesh, const dtNavMeshQuery& query, unsigned char flags, Ogre::String materialName)
{
	if (!dd) return;

	const dtNavMeshQuery* q = (flags & DU_DRAWNAVMESH_CLOSEDLIST) ? &query : 0;
	
	for (int i = 0; i < mesh.getMaxTiles(); ++i)
	{
		const dtMeshTile* tile = mesh.getTile(i);
		if (!tile->header) continue;
		drawMeshTile(dd, mesh, q, tile, flags, materialName);
	}
}
void detourDrawNavMeshPolysWithFlags(Ogre::ManualObject* dd, const dtNavMesh& mesh, const unsigned short polyFlags, Ogre::ColourValue col, Ogre::String materialName)
{
	if (!dd) return;
	
	for (int i = 0; i < mesh.getMaxTiles(); ++i)
	{
		const dtMeshTile* tile = mesh.getTile(i);
		if (!tile->header) continue;
		dtPolyRef base = mesh.getPolyRefBase(tile);

		for (int j = 0; j < tile->header->polyCount; ++j)
		{
			const dtPoly* p = &tile->polys[j];
			if ((p->flags & polyFlags) == 0) continue;
			detourDrawNavMeshPoly(dd, mesh, base|(dtPolyRef)j, col, materialName);
		}
	}
}


void detourDrawNavMeshNodes(Ogre::ManualObject* dd, const dtNavMeshQuery& query, Ogre::String materialName)
{
	if (!dd) return;

	const dtNodePool* pool = query.getNodePool();
	if (pool)
	{
		const float off = 0.5f;
		dd->begin(materialName, Ogre::RenderOperation::OT_POINT_LIST);
		Ogre::ColourValue col;
		col.setAsRGBA(duRGBA(255,192,0,255));
		for (int i = 0; i < pool->getHashSize(); ++i)
		{
			for (dtNodeIndex j = pool->getFirst(i); j != DT_NULL_IDX; j = pool->getNext(j))
			{
				const dtNode* node = pool->getNodeAtIdx(j+1);
				if (!node) continue;
				dd->position(node->pos[0],node->pos[1]+off,node->pos[2]);
				dd->colour(col);
			}
		}
		dd->end();

		dd->begin(materialName, Ogre::RenderOperation::OT_LINE_LIST);
		col.setAsRGBA(duRGBA(255,192,0,128));
		for (int i = 0; i < pool->getHashSize(); ++i)
		{
			for (dtNodeIndex j = pool->getFirst(i); j != DT_NULL_IDX; j = pool->getNext(j))
			{
				const dtNode* node = pool->getNodeAtIdx(j+1);
				if (!node) continue;
				if (!node->pidx) continue;
				const dtNode* parent = pool->getNodeAtIdx(node->pidx);
				if (!parent) continue;

				dd->position(node->pos[0],node->pos[1]+off,node->pos[2]);
				dd->colour(col);
				dd->position(parent->pos[0],parent->pos[1]+off,parent->pos[2]);
				dd->colour(col);
			}
		}
		dd->end();
	}
}

void detourDrawCross( Ogre::ManualObject* dd, const float x, const float y, const float z, const float size, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName )
{
	if (!dd) return;

	dd->begin(materialName,Ogre::RenderOperation::OT_LINE_LIST);
	detourAppendCross(dd, x,y,z, size, col);
	dd->end();
}

void detourAppendCross(Ogre::ManualObject* dd, const float x, const float y, const float z, const float s, Ogre::ColourValue col)
{
	if (!dd) return;
	dd->position(x-s,y,z);
	//dd->colour(col);
	dd->position(x+s,y,z);
	//dd->colour(col);
	dd->position(x,y-s,z);
	//dd->colour(col);
	dd->position(x,y+s,z);
	//dd->colour(col);
	dd->position(x,y,z-s);
	//dd->colour(col);
	dd->position(x,y,z+s);
	//dd->colour(col);
}

void detourDrawCircle(Ogre::ManualObject* dd, const float x, const float y, const float z, const float r, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName)
{
	if (!dd) return;

	dd->begin(materialName,Ogre::RenderOperation::OT_LINE_LIST);
	detourAppendCircle(dd, x,y,z, r, col);
	dd->end();
}

void detourAppendCircle(Ogre::ManualObject* dd, const float x, const float y, const float z, const float r, Ogre::ColourValue col)
{
	if (!dd) return;
	static const int NUM_SEG = 40;
	static float dir[40*2];
	static bool init = false;
	if (!init)
	{
		init = true;
		for (int i = 0; i < NUM_SEG; ++i)
		{
			const float a = (float)i/(float)NUM_SEG*DU_PI*2;
			dir[i*2] = cosf(a);
			dir[i*2+1] = sinf(a);
		}
	}

	for (int i = 0, j = NUM_SEG-1; i < NUM_SEG; j = i++)
	{
		dd->position(x+dir[j*2+0]*r, y, z+dir[j*2+1]*r);
		//dd->colour(col);
		dd->position(x+dir[i*2+0]*r, y, z+dir[i*2+1]*r);
		//dd->colour(col);
	}
}

void detourDrawArrow(Ogre::ManualObject* dd, const float x0, const float y0, const float z0, 	const float x1, const float y1, const float z1, const float as0, const float as1, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName)
{
	if (!dd) return;

	dd->begin(materialName, Ogre::RenderOperation::OT_LINE_LIST);
	detourAppendArrow(dd, x0,y0,z0, x1,y1,z1, as0, as1, col);
	dd->end();
}

void detourAppendArrow(Ogre::ManualObject* dd, const float x0, const float y0, const float z0, const float x1, const float y1, const float z1, const float as0, const float as1, Ogre::ColourValue col)
{
	if (!dd) return;

	dd->position(x0,y0,z0);
	//dd->colour(col);
	dd->position(x1,y1,z1);
	//dd->colour(col);

	// End arrows
	const float p[3] = {x0,y0,z0}, q[3] = {x1,y1,z1};
	if (as0 > 0.001f)
		appendArrowHead(dd, p, q, as0, col);
	if (as1 > 0.001f)
		appendArrowHead(dd, q, p, as1, col);
}

void appendArrowHead(Ogre::ManualObject* dd, const float* p, const float* q, const float s, Ogre::ColourValue col)
{
	const float eps = 0.001f;
	if (!dd) return;
	if (vdistSqr(p,q) < eps*eps) return;
	float ax[3], ay[3] = {0,1,0}, az[3];
	vsub(az, q, p);
	vnormalize(az);
	vcross(ax, ay, az);
	vcross(ay, az, ax);
	vnormalize(ay);
	
	dd->position(p[0],p[1],p[2]);
	//dd->colour(col);
	dd->position(p[0]+az[0]*s+ax[0]*s/3, p[1]+az[1]*s+ax[1]*s/3, p[2]+az[2]*s+ax[2]*s/3);
	//dd->colour(col);

	dd->position(p[0],p[1],p[2]);
	//dd->colour(col);
	dd->position(p[0]+az[0]*s-ax[0]*s/3, p[1]+az[1]*s-ax[1]*s/3, p[2]+az[2]*s-ax[2]*s/3);
	//dd->colour(col);
}

void detourDrawCylinder(Ogre::ManualObject* dd, float minx, float miny, float minz, float maxx, float maxy, float maxz, Ogre::ColourValue col, Ogre::String materialName)
{
	if (!dd) return;

	dd->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_STRIP);
	detourAppendCylinder(dd, minx,miny,minz, maxx,maxy,maxz, col);
	dd->end();
}

void detourAppendCylinder(Ogre::ManualObject* dd, float minx, float miny, float minz, float maxx, float maxy, float maxz, Ogre::ColourValue col)
{
	if (!dd) return;

	static const int NUM_SEG = 16;
	static float dir[NUM_SEG*2];
	static bool init = false;
	if (!init)
	{
		init = true;
		for (int i = 0; i < NUM_SEG; ++i)
		{
			const float a = (float)i/(float)NUM_SEG*DU_PI*2;
			dir[i*2] = cosf(a);
			dir[i*2+1] = sinf(a);
		}
	}

	float* fCol = duDivCol(duMultCol(getColorValue(col), 160));
	Ogre::ColourValue col2(fCol[0], fCol[1], fCol[2], fCol[3]);
	const float cx = (maxx + minx)/2;
	const float cz = (maxz + minz)/2;
	const float rx = (maxx - minx)/2;
	const float rz = (maxz - minz)/2;

	for (int i = 2; i < NUM_SEG; ++i)
	{
		const int a = 0, b = i-1, c = i;

		dd->position(cx+dir[a*2+0]*rx, miny, cz+dir[a*2+1]*rz);
		dd->colour(col2);
		dd->position(cx+dir[b*2+0]*rx, miny, cz+dir[b*2+1]*rz);
		dd->colour(col2);
		dd->position(cx+dir[c*2+0]*rx, miny, cz+dir[c*2+1]*rz);
		dd->colour(col2);
	}
	for (int i = 2; i < NUM_SEG; ++i)
	{
		const int a = 0, b = i, c = i-1;
		dd->position(cx+dir[a*2+0]*rx, maxy, cz+dir[a*2+1]*rz);
		dd->colour(col);
		dd->position(cx+dir[b*2+0]*rx, maxy, cz+dir[b*2+1]*rz);
		dd->colour(col);
		dd->position(cx+dir[c*2+0]*rx, maxy, cz+dir[c*2+1]*rz);
		dd->colour(col);
	}
	for (int i = 0, j = NUM_SEG-1; i < NUM_SEG; j = i++)
	{
		dd->position(cx+dir[i*2+0]*rx, miny, cz+dir[i*2+1]*rz);
		dd->colour(col2);
		dd->position(cx+dir[j*2+0]*rx, miny, cz+dir[j*2+1]*rz);
		dd->colour(col2);
		dd->position(cx+dir[j*2+0]*rx, maxy, cz+dir[j*2+1]*rz);
		dd->colour(col);

		dd->position(cx+dir[i*2+0]*rx, miny, cz+dir[i*2+1]*rz);
		dd->colour(col2);
		dd->position(cx+dir[j*2+0]*rx, maxy, cz+dir[j*2+1]*rz);
		dd->colour(col);
		dd->position(cx+dir[i*2+0]*rx, maxy, cz+dir[i*2+1]*rz);
		dd->colour(col);
	}
}

void detourAppendArc(Ogre::ManualObject* dd, const float x0, const float y0, const float z0, const float x1, const float y1, const float z1, const float h, const float as0, const float as1, Ogre::ColourValue col)
{
	if (!dd) return;
	static const int NUM_ARC_PTS = 8;
	static const float PAD = 0.05f;
	static const float ARC_PTS_SCALE = (1.0f-PAD*2) / (float)NUM_ARC_PTS;
	const float dx = x1 - x0;
	const float dy = y1 - y0;
	const float dz = z1 - z0;
	const float len = sqrtf(dx*dx + dy*dy + dz*dz);
	float prev[3];
	evalArc(x0,y0,z0, dx,dy,dz, len*h, PAD, prev);
	for (int i = 1; i <= NUM_ARC_PTS; ++i)
	{
		const float u = PAD + i * ARC_PTS_SCALE;
		float pt[3];
		evalArc(x0,y0,z0, dx,dy,dz, len*h, u, pt);
		dd->position(prev[0],prev[1],prev[2]);
		dd->colour(col);
		dd->position(pt[0],pt[1],pt[2]);
		dd->colour(col);
		prev[0] = pt[0]; prev[1] = pt[1]; prev[2] = pt[2];
	}

	// End arrows
	if (as0 > 0.001f)
	{
		float p[3], q[3];
		evalArc(x0,y0,z0, dx,dy,dz, len*h, PAD, p);
		evalArc(x0,y0,z0, dx,dy,dz, len*h, PAD+0.05f, q);
		appendArrowHead(dd, p, q, as0, col);
	}

	if (as1 > 0.001f)
	{
		float p[3], q[3];
		evalArc(x0,y0,z0, dx,dy,dz, len*h, 1-PAD, p);
		evalArc(x0,y0,z0, dx,dy,dz, len*h, 1-(PAD+0.05f), q);
		appendArrowHead(dd, p, q, as1, col);
	}
}

 unsigned int getColorValue(Ogre::ColourValue col)
{
	const unsigned int r = (unsigned int)(255 *col.r) & 0xff;
	const unsigned int g = (unsigned int)(255 *col.g) & 0xff;
	const unsigned int b = (unsigned int)(255 *col.b) & 0xff;
	const unsigned int a = (unsigned int)(255 *col.a) & 0xff;
	return duRGBA(r >> 8, g >> 8, b >> 8, a);
}