//
// Copyright (c) 2009-2010 Mikko Mononen memon@inside.org
//


#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Ogre.h"
#include "DebugDraw.h"
#include "RecastDraw.h"
#include "Recast.h"



void recastDrawTriMeshSlope(Ogre::ManualObject* dd, const float* verts, int /*nverts*/, const int* tris, const float* normals, int ntris, const float walkableSlopeAngle, const float texScale, Ogre::String materialName)
{
	if (!dd) return;
	if (!verts) return;
	if (!tris) return;
	if (!normals) return;

	float uva[2];
	float uvb[2];
	float uvc[2];

	const float walkableThr = cosf(walkableSlopeAngle/180.0f*DU_PI);

	const unsigned int unwalkable = duRGBA(192,128,0,255);

	//dd->texture(true);

	dd->begin(materialName);
	for (int i = 0; i < ntris*3; i += 3)
	{
		const float* norm = &normals[i];
		//unsigned int color;
		unsigned char a = (unsigned char)(220*(2+norm[0]+norm[1])/4);
		 Ogre::ColourValue color;
		if ((norm[1] < walkableThr))
			color.setAsRGBA(duLerpCol(duRGBA(a,a,a,255), unwalkable, 64));
		else
			color.setAsRGBA(duRGBA(a,a,a,255));

		const float* va = &verts[tris[i+0]*3];
		const float* vb = &verts[tris[i+1]*3];
		const float* vc = &verts[tris[i+2]*3];
		
		int ax = 0, ay = 0;
		if (rcAbs(norm[1]) > rcAbs(norm[ax]))
			ax = 1;
		if (rcAbs(norm[2]) > rcAbs(norm[ax]))
			ax = 2;
		ax = (1<<ax)&3; // +1 mod 3
		ay = (1<<ax)&3; // +1 mod 3

		uva[0] = va[ax]*texScale;
		uva[1] = va[ay]*texScale;
		uvb[0] = vb[ax]*texScale;
		uvb[1] = vb[ay]*texScale;
		uvc[0] = vc[ax]*texScale;
		uvc[1] = vc[ay]*texScale;
		
		
		color = color.White;
		dd->position(va[0],va[1],va[2]);
		dd->colour(color);
		dd->textureCoord(uva[0],uva[1]);
		dd->position(vb[0],vb[1],vb[2]);
		dd->colour(color);
		dd->textureCoord(uvb[0],uvb[1]);
		dd->position(vc[0],vc[1],vc[2]);
		dd->colour(color);
		dd->textureCoord(uvc[0],uvc[1]);

	
		
	}
	dd->end();
	//dd->texture(false);
}