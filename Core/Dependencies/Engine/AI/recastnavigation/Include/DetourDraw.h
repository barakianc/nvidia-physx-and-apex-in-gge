//
// Copyright (c) 2009-2010 Mikko Mononen memon@inside.org
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//

#ifndef DETOURDRAW_H
#define DETOURDRAW_H

#include "Ogre.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshQuery.h"
#include "DetourTileCacheBuilder.h"

//void duDebugDrawNavMesh(struct duDebugDraw* dd, const dtNavMesh& mesh, unsigned char flags);
void detourDrawNavMeshWithClosedList(Ogre::ManualObject* dd, const dtNavMesh& mesh, const dtNavMeshQuery& query, unsigned char flags, Ogre::String materialName);
void detourDrawNavMeshPoly(Ogre::ManualObject* dd, const dtNavMesh& mesh, dtPolyRef ref, Ogre::ColourValue col, Ogre::String materialName);
void detourDrawNavMeshPolysWithFlags(Ogre::ManualObject* dd, const dtNavMesh& mesh, const unsigned short polyFlags, Ogre::ColourValue col, Ogre::String materialName);
void detourDrawNavMeshNodes(Ogre::ManualObject* dd, const dtNavMeshQuery& query, Ogre::String materialName);
void detourDrawCross(Ogre::ManualObject* dd, const float x, const float y, const float z, const float size, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName);
void detourAppendCross(Ogre::ManualObject* dd, const float x, const float y, const float z, const float s, Ogre::ColourValue col);
void detourDrawCircle(Ogre::ManualObject* dd, const float x, const float y, const float z, const float r, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName);
void detourAppendCircle(Ogre::ManualObject* dd, const float x, const float y, const float z, const float r, Ogre::ColourValue col);
void detourAppendArrow(Ogre::ManualObject* dd, const float x0, const float y0, const float z0, const float x1, const float y1, const float z1, const float as0, const float as1, Ogre::ColourValue col);
void detourDrawArrow(Ogre::ManualObject* dd, const float x0, const float y0, const float z0, 	const float x1, const float y1, const float z1, const float as0, const float as1, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName);
void appendArrowHead(Ogre::ManualObject* dd, const float* p, const float* q, const float s, Ogre::ColourValue col);
void detourDrawCylinder(Ogre::ManualObject* dd, float minx, float miny, float minz, float maxx, float maxy, float maxz, Ogre::ColourValue col, Ogre::String materialName);
void detourAppendCylinder(Ogre::ManualObject* dd, float minx, float miny, float minz, float maxx, float maxy, float maxz, Ogre::ColourValue col);
void detourAppendArc(Ogre::ManualObject* dd, const float x0, const float y0, const float z0, const float x1, const float y1, const float z1, const float h, const float as0, const float as1, Ogre::ColourValue col);
 unsigned int getColorValue(Ogre::ColourValue col);
#endif // DETOURDEBUGDRAW_H