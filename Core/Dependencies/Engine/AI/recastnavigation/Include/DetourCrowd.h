//
// Copyright (c) 2009-2010 Mikko Mononen memon@inside.org
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//

#ifndef DETOURCROWD_H
#define DETOURCROWD_H

#include "DetourNavMeshQuery.h"
#include "DetourObstacleAvoidance.h"
#include "DetourLocalBoundary.h"
#include "DetourPathCorridor.h"
#include "DetourProximityGrid.h"
#include "DetourPathQueue.h"
#include "DetourNavMesh.h"
#include "dtAIAgent.h"
#include "Engine.h"

static const int DT_CROWD_MAX_OBSTAVOIDANCE_PARAMS = 10;
static const Ogre::String CROWD_NODE_PREFIX = "Crowd_Node_";

static const int MAX_CROWD_AGENTS = 64;

/*
struct dtCrowdNeighbour
{
	int idx;
	float dist;
};
*/
enum CrowdAgentState
{
	DT_CROWDAGENT_STATE_INVALID,
	DT_CROWDAGENT_STATE_WALKING,
	DT_CROWDAGENT_STATE_OFFMESH,
};
/*
struct dtCrowdAgentParams
{
	float radius;
	float height;
	float maxAcceleration;
	float maxSpeed;
	float collisionQueryRange;
	float pathOptimizationRange;
	float separationWeight;
	unsigned char updateFlags;
	unsigned char obstacleAvoidanceType;
	void* userData;
};
*/

class dtTarget{
public:
	float m_targetPos[3];
	dtPolyRef m_targetRef;
	dtTarget *m_nextTarget;

	dtTarget(float targetPos[3], dtPolyRef targetRef = 0, dtTarget *nextTarget = NULL);
	dtTarget(float targetPosX = 0.0f, float targetPosY = 0.0f, float targetPosZ = 0.0f, dtPolyRef targetRef = 0, dtTarget *nextTarget = NULL);
	~dtTarget() { this->m_nextTarget = NULL; }

	void setTargetPos(float targetPos[3]) { this->m_targetPos[0] = targetPos[0];  this->m_targetPos[1] = targetPos[1];  this->m_targetPos[2] = targetPos[2]; }
	void setTargetPos(float targetPosX = 0.0f, float targetPosY = 0.0f, float targetPosZ = 0.0f) { this->m_targetPos[0] = targetPosX;  this->m_targetPos[1] = targetPosY;  this->m_targetPos[2] = targetPosZ; }
	void setTargetRef(dtPolyRef targetRef) { this->m_targetRef = targetRef; }
	void setNextTarget(dtTarget *nextTarget) { this->m_nextTarget = nextTarget; }

	float* getTargetPosF() { return this->m_targetPos; }
	Ogre::Vector3 getTargetPosV() { return Ogre::Vector3(this->m_targetPos[0], this->m_targetPos[1], this->m_targetPos[2]); }
	dtPolyRef getTargetRef() { return this->m_targetRef; }
	dtTarget *getNextTarget() { return this->m_nextTarget; }
};

enum UpdateFlags
{
	DT_CROWD_ANTICIPATE_TURNS = 1,
	DT_CROWD_OBSTACLE_AVOIDANCE = 2,
	DT_CROWD_SEPARATION = 4,
	DT_CROWD_OPTIMIZE_VIS = 8,
	DT_CROWD_OPTIMIZE_TOPO = 16,
};

struct dtCrowdAgentDebugInfo
{
	int idx;
	float optStart[3], optEnd[3];
	dtObstacleAvoidanceDebugData* vod;
};

class dtCrowd
{
	dtAIAgent* m_agents;
	dtAIAgent** m_activeAgents;
	int m_maxAgents;
	int activeAgentsCount;
	dtCrowdAgentAnimation* m_agentAnims;
	int mCrowdID;
	int m_agentsOnTarget;
	
	dtPathQueue m_pathq;

	Ogre::ManualObject* m_manualObj;

	dtObstacleAvoidanceParams m_obstacleQueryParams[DT_CROWD_MAX_OBSTAVOIDANCE_PARAMS];
	dtObstacleAvoidanceQuery* m_obstacleQuery;
	
	dtProximityGrid* m_grid;
	
	dtPolyRef* m_pathResult;
	int m_maxPathResult;
	
	float m_ext[3];
	dtQueryFilter m_filter;
	
	float m_maxAgentRadius;
	int m_maxAgentRadiusIndex;

	// static members
	static dtAIAgent** sm_agentList;
	static int sm_agentCount;

	enum MoveRequestState
	{
		MR_TARGET_NONE,
		MR_TARGET_FAILED,
		MR_TARGET_VALID,
		MR_TARGET_REQUESTING,
		MR_TARGET_WAITING_FOR_PATH,
		MR_TARGET_ADJUST,
	};
	
	static const int MAX_TEMP_PATH = 32;

	struct MoveRequest
	{
		unsigned char state;			///< State of the request
		int idx;						///< Agent index
		dtPolyRef ref;					///< Goal ref
		float pos[3];					///< Goal position
		dtPathQueueRef pathqRef;		///< Path find query ref
		dtPolyRef aref;					///< Goal adjustment ref
		float apos[3];					///< Goal adjustment pos
		dtPolyRef temp[MAX_TEMP_PATH];	///< Adjusted path to the goal
		int ntemp;
		bool replan;
	};
	MoveRequest* m_moveRequests;
	int m_moveRequestCount;
	
	dtNavMeshQuery* m_navquery;

	char* m_crowdMaterial;
	dtTarget *mTargets;

	void updateTopologyOptimization(dtAIAgent** agents, const int nagents, const float dt);
	void updateMoveRequest(const float dt);
	void checkPathValidty(dtAIAgent** agents, const int nagents, const float dt);

	inline int getAgentIndex(const dtAIAgent* agent) const  { return agent - m_agents; }
	const MoveRequest* getActiveMoveTarget(const int idx) const;

	bool requestMoveTargetReplan(const int idx, dtPolyRef ref, const float* pos);

	void purge();

public:
	friend class dtAIAgent;

	dtCrowd();
	~dtCrowd();
	
	bool init(int crowdID, const int maxAgents, const float maxAgentRadius, char *agentMaterial, dtNavMesh* nav);

	void setObstacleAvoidanceParams(const int idx, const dtObstacleAvoidanceParams* params);
	const dtObstacleAvoidanceParams* getObstacleAvoidanceParams(const int idx) const;
	
	int addAgent(const float* pos, const dtCrowdAgentParams* params, dtTarget* activeTarget, char* type, Ogre::String meshName, Ogre::String hkxName ,Ogre::String luaFile, Ogre::String characterName,GameObject* gObj);

	void removeAgent(const int idx);

	dtTarget* addNewTarget(float* targetPos, dtPolyRef targetRef);
	dtTarget* setNewTarget(float* targetPos, dtPolyRef targetRef);

	bool requestMoveTarget(const int idx, dtPolyRef ref, const float* pos);
	bool adjustMoveTarget(const int idx, dtPolyRef ref, const float* pos);

	bool getRelativeMeshPosition( float x, float y, float z, dtPolyRef* nearestRef, float* nearestPt, InputGeom* inputGeom = NULL);

	void update(const float dt, class Sample* sample, dtCrowdAgentDebugInfo* debug);
	void render();
	
	const dtQueryFilter* getFilter() const { return &m_filter; }
	dtQueryFilter* getEditableFilter() { return &m_filter; }
	const float* getQueryExtents() const { return m_ext; }
	
	const dtProximityGrid* getGrid() const { return m_grid; }
	const dtPathQueue* getPathQueue() const { return &m_pathq; }
	const dtNavMeshQuery* getNavMeshQuery() const { return m_navquery; }

	dtAIAgent* getAgent(const int idx) { return &m_agents[idx]; }
	const int getAgentCount() const { return this->m_maxAgents; }
	static int getAgentCountAllCrowds() { return sm_agentCount; }
	int getActiveAgents(dtAIAgent** agents, const int maxAgents);
	const int getActiveAgentsCount() const { return activeAgentsCount; }
	void increaseActiveAgents() { activeAgentsCount++; }
	void decreaseActiveAgents() { activeAgentsCount--; }
	void resetActiveAgentsCount() { activeAgentsCount = 0; }

	dtTarget *getTargets() { return this->mTargets; }
	const int getCrowdID() { return this->mCrowdID; }
	Ogre::String getCrowdNodeName() { return CROWD_NODE_PREFIX + Ogre::StringConverter::toString(this->getCrowdID()); }
	bool setCrowdRadius(float radius, int agent_ID);
	void updateCrowdRadius(int agent_ID);

	int getAgentsOnTarget(void) { return m_agentsOnTarget; }
	void setAgentsOnTarget(int count) { m_agentsOnTarget = count; }

	bool InitNavQueryForPrevAgents(dtNavMesh* nav);
	void FindNearestPositionForPrevAgents(dtAIAgent* ag);
	dtTarget* getRandomMeshTarget();
	dtTarget* getRandomMeshTarget(dtAIAgent* ag);
	static void resetStaticMembers();
};

dtCrowd* dtAllocCrowd();
void dtFreeCrowd(dtCrowd* ptr);

/****************************************************************************************
* DETOUR CROWD HANDLER CLASS															*
****************************************************************************************/
class dtCrowdHandler{
	dtCrowd* mCrowd;
	dtCrowdHandler* mNextCrowdHandler;

public:
	dtCrowdHandler(dtCrowd* crowd = NULL, dtCrowdHandler* nextCrowdHandler = NULL);
	~dtCrowdHandler();

	void setCrowd(dtCrowd* crowd = NULL);
	void setNextCrowdHandler(dtCrowdHandler* nextCrowdHandler = NULL);

	dtCrowd* getCrowd() { return this->mCrowd; }
	dtCrowdHandler* getNextCrowdHandler() { return this->mNextCrowdHandler; }
};

#endif // DETOURCROWD_H
