#pragma once
#include "StdAfx.h"
#include <stdio.h>
#include "dtAIAgent.h"
#include "dtDebugText2D.h"
#include "dtDebugLine.h"
#include "AIAgentLuaBinding.h"
#include "DetourCrowd.h"
#include "DetourCommon.h"
#include "Engine.h"
#include "Sample.h"
#include "InputGeom.h"
#include "HavokObject.h"
#include "DetourAssert.h"
#include "DetourDraw.h"
#include "AIWorldManager.h"
#include "HavokObject.h"
#include "SteeringBehaviors.h"
#include "lua.hpp"
#include "AnimatedEntity.h"
#include <cmath>
#include "../../Core/Dependencies/Engine/Resources/TextFileManager.h"
#include "../../Core/Dependencies/Engine/Resources/TextFile.h"

enum SamplePolyFlags;
TextFileManager* dtAIAgent::m_tfm;
/*
OpenSteer::Vec3 gVel(0.0f, 0.0f, 0.0f);
OpenSteer::Vec3 gForce(0.0f, 0.0f, 0.0f);
OpenSteer::Vec3 gEVel(0.0f, 0.0f, 0.0f);
OpenSteer::Vec3 gEPos(0.0f, 0.0f, 0.0f);
*/
dtAIAgent::dtAIAgent(dtCrowd* crowd,  int id, Ogre::Vector3 pos)
{
	this->m_debug = false;
	this->m_debugPosition = false;
	this->m_yOffset = 0;
	this->m_isAgentActive = false;
	this->m_agentGameObject = NULL;
	this->m_activeTarget = NULL;
	dtVset(this->disp, 0.0f, 0.0f, 0.0f);
	this->m_AISteeringBehavior = new AISteeringBehavior();
	this->setAgentID(id);
	this->setAgentPositionAbsolute(pos);
	this->m_crowd = crowd;
	Ogre::String st = this->getAgentEntityName();
	// Make polygons with 'disabled' flag invalid.
	if (m_crowd)
	{
		m_crowd->getEditableFilter()->setExcludeFlags(SAMPLE_POLYFLAGS_DISABLED);
		// Setup local avoidance params to different qualities.
		dtObstacleAvoidanceParams params;
		// Use mostly default settings, copy from dtm_crowd.
		memcpy(&params, m_crowd->getObstacleAvoidanceParams(0), sizeof(dtObstacleAvoidanceParams));
		// Low (11)
		params.velBias = 0.5f;
		params.adaptiveDivs = 5;
		params.adaptiveRings = 2;
		params.adaptiveDepth = 1;
		m_crowd->setObstacleAvoidanceParams(0, &params);
		// Medium (22)
		params.velBias = 0.5f;
		params.adaptiveDivs = 5;
		params.adaptiveRings = 2;
		params.adaptiveDepth = 2;
		m_crowd->setObstacleAvoidanceParams(1, &params);
		// Good (45)
		params.velBias = 0.5f;
		params.adaptiveDivs = 7;
		params.adaptiveRings = 2;
		params.adaptiveDepth = 3;
		m_crowd->setObstacleAvoidanceParams(2, &params);
		// High (66)
		params.velBias = 0.5f;
		params.adaptiveDivs = 7;
		params.adaptiveRings = 3;
		params.adaptiveDepth = 3;
		m_crowd->setObstacleAvoidanceParams(3, &params);

	}
	m_text = NULL;
	m_debug = false;
}

dtAIAgent::~dtAIAgent()
{

	//if (this->isAgentActive())
	//	deactivateAgent();
	if(m_text != NULL)
		delete m_text;

}

void dtAIAgent::scaleAgent(float xScale,float yScale,float zScale)
{
	m_agentGameObject->scale(xScale, yScale, zScale);
	
	if(this->params.radiusXOrZ)
		this->params.radius = 	this->params.radius * xScale;
	else
		this->params.radius = 	this->params.radius * zScale;
	this->params.height = this->params.height * yScale;
	if(this->params.radius * 4 > 3.5)
		this->params.maxSpeed = this->params.radius * 4;
	//this->params.maxSpeed = this->params.radius * 5;
	//this->params.maxAcceleration = this->params.radius * 7;
	//this->params.collisionQueryRange = this->params.radius * (7.2 / this->params.radius);
	//this->params.pathOptimizationRange = this->params.radius * (18 / this->params.radius);
	this->params.collisionQueryRange = this->params.radius * 12.0f;
	this->params.pathOptimizationRange = this->params.radius * 30.0f;
	this->m_crowd->updateCrowdRadius(this->m_agentID);
	EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->updateAgentHeight(m_agentID, m_crowd->getCrowdID(),false);
	//TODO: what about crowd radius?t
}

void dtAIAgent::activateAgent( Ogre::String meshName, Ogre::String hkxName, Ogre::String type, Ogre::String luaFile, Ogre::String materialName, GameObject * gObj)
{
	m_agentGameObject = gObj;
	// Create graphics object
	if ( type == "GRAPHICS_OBJECT" )
	{
		//m_agentGameObject = new GameObject( this->getAgentEntityName(), meshName, hkxName, GRAPHICS_OBJECT, DEFAULT_COLLISION_SHAPE, luaFile, true);

		// Set default clothes color to white
		if ( meshName == "Sinbad.mesh" )
		{
			setSinbadMaterial(materialName);
		}
	}
	else if ( type == "ANIMATED_CHARACTER_RIGIDBODY" )
	{
		//m_agentGameObject = new GameObject( this->getAgentEntityName(), meshName, hkxName, ANIMATED_CHARACTER_RIGIDBODY, DEFAULT_COLLISION_SHAPE, "", true);
		m_agentGameObject->m_pAnimatedEntity->SetDefaultAnimation(m_animationIdle.c_str());
		//((CharacterRigidBodyObject*) this->m_agentGameObject->m_pPhysicsObject)->setInputs(this->getAgentNPos()[0], this->getAgentNPos()[2], true, 0, true);
		//m_agentGameObject->setPosition(0, -10, 0);
		//m_agentGameObject->scale(1.2f, 1.2f, 1.2f);
		//this->scaleAgent(1.2,1.2,1.2);
		Ogre::Vector3 size = m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getBoundingBox().getSize();
		// Used because havok animated gameObjects are being centered at
		// 0,0,0 which is incorrect.

		if(meshName.find("stormtrooper") != string::npos )
		{
			float aboveGround = 1;
#ifdef HAVOK
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->scaleAndOffset(1, 1, 1, 0/*-(this->params.radius - 0.1f)*/, aboveGround,0/* -(this->params.radius / 2 - 0.2f)*/);
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->setBaseLookingDirection(Ogre::Math::DegreesToRadians(-85.0f));
			this->params.height += aboveGround;
#endif
		}
		if( meshName.find("soulblade") != string::npos)
		{
#ifdef HAVOK
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->scaleAndOffset(size.x, size.y, size.z, -(this->params.radius - 0.0f), 0, -(this->params.radius / 2 - 0.0f));
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->setBaseLookingDirection(Ogre::Math::DegreesToRadians(-85.0f));
#endif
		}
		if( meshName.find("MAGICIAN") != string::npos)
		{
#ifdef HAVOK
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->scaleAndOffset(size.x, size.y, size.z, -(this->params.radius - 0.0f), 0, -(this->params.radius  + 0.0f));
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->setBaseLookingDirection(Ogre::Math::DegreesToRadians(180.0f));
#endif
		}
		if( meshName.find("merc") != string::npos)
		{
#ifdef HAVOK
			//this->scaleAgent(1/6,1/6,1/6);
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->scaleAndOffset(size.x/6, size.y/6 , size.z/6, 0 /*-(this->params.radius + 0.25f)*/, size.y/3 ,0/* -(this->params.radius  - 2.0f)*/);
			((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->setBaseLookingDirection(Ogre::Math::DegreesToRadians(-85.0f));
#endif
		}
		//m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
		// Used when havok animated gameObjects are being centered at their
		// actual center.
		//((CharacterRigidBodyObject*)tempObject->m_pPhysicsObject)->scaleAndOffset(size.x/6,size.y/6,size.z/6,0.0f,-size.y/3,0.0f);
		//((CharacterRigidBodyObject*)m_agentGameObject->m_pPhysicsObject)->setBaseLookingDirection(Ogre::Math::DegreesToRadians(-85.0f));
	}
	
	m_remove = false;
	m_luaFileName = luaFile;
	m_luaStackSize = 0;
	m_radPrev = 0;
	// TODO Fix initial Animation
	//m_animationEntity->PlayAnimation("IdleTop");
	m_luaVM = 0;
	m_luaInitilize = false;
	m_enableLua = true;
	m_moveTo = Ogre::Vector3::ZERO;
	m_blendFrame = 0.4f;
	m_hasSlowedDown = false;

	m_dtLines = new list<dtDebugLine*>();
	behaviorName = "idle";
	//
	setDebug(false, false);

	if(luaFile != "")
		this->initialize();

	// Set behavior
	this->makeIdle();
	dtVset(this->gvel, 0.f, 0.f, 0.f);
	this->m_isAgentActive = true;
	this->m_crowd->increaseActiveAgents();

	int randVal = (unsigned)time(NULL) ;
	randVal += this->m_agentID;
	randVal += (int) (this->npos[0] + this->npos[1] + this->npos[2]);
	srand( randVal );
}

/************************************************************************/
/* DEACTIVATE AGENT                                                     */
/************************************************************************/
void dtAIAgent::deactivateAgent()
{
	this->m_isAgentActive = false;
	this->m_crowd->decreaseActiveAgents();

	m_text = 0;

	// TODO destroy game object	
	if (this->m_agentGameObject)
	{
		GraphicsObject* graphicsObject = this->m_agentGameObject->m_pGraphicsObject;
		if (graphicsObject)
		{
			Ogre::SceneNode* sceneNode = graphicsObject->m_pOgreSceneNode;
			Ogre::String entityNodeName = sceneNode->getName();;
			sceneNode = sceneNode->getParentSceneNode();
			sceneNode->detachObject(entityNodeName);
			EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->destroySceneNode(sceneNode);
		}
		this->m_agentGameObject->removeThis();
		this->m_agentGameObject = NULL;
	}
}

void dtAIAgent::setDebug( bool debug, bool debugPos)
{
	m_debug = debug;
	m_debugPosition = debugPos;
	//for(list<dtDebugObject*>::iterator it = m_dtdebugObjects->begin();
	//	it != m_dtdebugObjects->end(); it++)
	//{
	//		(*it)->setVisible(debug);
	//}

	/*
	if (debug && !m_text)
		this->setDebugBehavior("none");
	else
		delete m_text;
	*/

	m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(debug);
};

void dtAIAgent::setDebugText( Ogre::String text)
{
	if(!m_text){
		m_text = new dtDebugText2D(m_agentGameObject->m_pGraphicsObject->m_pOgreSceneManager, m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode, text);
		m_text->initialize();

	}
	else {
		((dtDebugText2D*)m_text)->setText(text);
	}
}

void dtAIAgent::setDebugBehavior( Ogre::String text)
{
	if(!m_text){
		m_text = new dtDebugText2D(m_agentGameObject->m_pGraphicsObject->m_pOgreSceneManager, m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode, text);
		m_text->initialize();
		((dtDebugText2D*)m_text)->setColour(Ogre::ColourValue(0.3f, 0.5f, 0.5f));
		((dtDebugText2D*)m_text)->setBehavior(text);

	}
	else {
		((dtDebugText2D*)m_text)->setText(text);
	}

}

// TODO Check all params and set all that are needed in AISteeringBehavior
void dtAIAgent::updateAgentParameters(const dtCrowdAgentParams* params)
{
	memcpy(this->getAgentParams(), params, sizeof(dtCrowdAgentParams));
}

inline float RandomClamped()
{
	return ((float)rand()/(float)RAND_MAX) * 2.0f - 1.0f;
}

void dtAIAgent::update(const float currentTime, const float deltaTime)
{
	InputGeom* inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();
	this->update(currentTime, deltaTime, inputGeom);
}

void dtAIAgent::updateLua()
{
	if (m_remove){
		return;
	}
	if (m_enableLua && m_luaFileName != ""){
		if(!m_luaInitilize){
			initializeLua();
			m_luaInitilize = true;
		}
		// Update known lua stack size
		m_luaStackSize = lua_gettop(m_luaVM);
		// Grab current state
		lua_getglobal(m_luaVM, "state");
		if (!lua_isnil(m_luaVM,-1)){
			m_state = lua_tostring(m_luaVM, -1);
		}
		else {
			m_state = "";
			lua_remove(m_luaVM, -1);
		}
		// Execute current state
		if(m_state.length() > 0){
			lua_getglobal(m_luaVM, m_state.c_str());
			if(!lua_isnil(m_luaVM, -1)){
				lua_call(m_luaVM,0,0);
			}
			else{
				m_state = "";
				lua_remove(m_luaVM, -1);
				Ogre::String message = "ERROR: ";
				message.append(m_state);
				message.append(" is not a valid state in ");
				message.append(m_luaFileName);
				m_log->logMessage(message, Ogre::LML_CRITICAL);
			}
		}
	}
}

// TOODO remove currentTime
void dtAIAgent::update(const float currentTime, const float deltaTime, InputGeom* inputGeom)
{
	Ogre::LogManager::getSingleton().logMessage(behaviorName);
	if (currentTime < 0)
		this->updateSteering(deltaTime, m_crowd->m_navquery, m_crowd->getFilter(), m_crowd->getQueryExtents(), m_crowd->mTargets, inputGeom);
	else
		this->updateGameObject(deltaTime);
}

void dtAIAgent::updateGameObject(float dt)
{
	if (!this->m_isAgentActive) return;

	// Update debug if enabled
	if(m_debug)
	{
		if(m_debugPosition)
		{
			float x = this->getAgentPosition()[0];
			x = ((float)floor(100*x + 0.5) / 100);
			float y = this->getAgentPosition()[1];
			y = ((float)floor(100*y + 0.5) / 100);
			float z = this->getAgentPosition()[2];
			z = ((float)floor(100*z + 0.5) / 100);
			Ogre::String posSt = Ogre::StringConverter::toString(x) + " " + Ogre::StringConverter::toString(y) + " " + Ogre::StringConverter::toString(z);
			((dtDebugText2D*)(m_text))->setText(behaviorName + "\n" + posSt);
			//UtilitiesPtr->GetDebugText("AgentInfo")->SetText(posSt);
		}
		else
			((dtDebugText2D*)(m_text))->setText(behaviorName);		
		m_text->update(dt);		
	}
	else
	{
		((dtDebugText2D*)(m_text))->setText("");
		m_text->update(dt);
	}

	if (m_agentGameObject)
	{
		const float* pos = this->getAgentPosition();
		if ( m_agentGameObject->m_eObjectType == GRAPHICS_OBJECT )
		{
			this->m_agentGameObject->setPosition( pos[0], pos[1] + m_yOffset , pos[2] );
		}
		else if ( m_agentGameObject->m_eObjectType == ANIMATED_CHARACTER_RIGIDBODY )
		{
			this->m_agentGameObject->setPosition( pos[0], pos[1] + m_yOffset , pos[2] );
		}

		Ogre::Radian rad = (gvel[2] != 0) ? Ogre::Math::ATan(gvel[0]/gvel[2]) : Ogre::Radian(0.0f);
		if ( gvel[2] < 0 )
			rad = rad.valueRadians() - Ogre::Math::PI;

		hkReal currentAngle (0.0f), angleDiff(0.0f);
		if (this->m_agentGameObject->m_eObjectType == ANIMATED_CHARACTER_RIGIDBODY)
		{
			if (rad.valueRadians() != 0)
			{
#ifdef HAVOK
				currentAngle = ((CharacterRigidBodyObject*) this->m_agentGameObject->m_pPhysicsObject)->getCurrentAngle();

				angleDiff =  rad.valueRadians() + Ogre::Math::PI/2;
				((CharacterRigidBodyObject*) this->m_agentGameObject->m_pPhysicsObject)->setInputs(0, 0, false, angleDiff, true);
#endif
			}
		}
		else
		{
			Ogre::SceneNode* entitySceneNode = this->m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode;
			entitySceneNode->setInheritOrientation(false);
			entitySceneNode->setOrientation(Ogre::Quaternion(rad, Ogre::Vector3::UNIT_Y));
		}
	}
}

void dtAIAgent::switchToAnimation( Ogre::String newAnimationName, float transitionTime )
{
	bool b = this->behaviorName == "pursuit";
	if (m_agentGameObject && this->m_agentGameObject->m_pAnimatedEntity)
	{
		Ogre::String activeAnimation = this->m_agentGameObject->m_pAnimatedEntity->GetActiveAnimation();
		//cout << "SwitchToAnimation from " << activeAnimation << " to " << newAnimationName;
		if (activeAnimation != "" && activeAnimation != newAnimationName)
		{
			if ( newAnimationName == m_animationRun )
				this->m_agentGameObject->StopAnimation(activeAnimation.c_str());
			
			m_agentGameObject->m_pAnimatedEntity->PlayAnimation(newAnimationName.c_str(), "", true, transitionTime);
			//this->playAnimation(newAnimationName.c_str(), true);
			//const char* animState = m_agentGameObject->m_pAnimatedEntity->GetActiveAnimation();

			if(this->behaviorName == "pursuit")
			{
				if (newAnimationName == m_animationRun)
					cout<<"\npursuit girl set to run\n"
					<<m_agentGameObject->m_pAnimatedEntity->GetActiveAnimation()<<endl;
				//if (newAnimationName == m_animationIdle)
				//	cout<<"\npursuit girl idle idle idle idle idle\n"<<endl;
			}
		}
	}
	if( newAnimationName == m_animationRun && m_hasSlowedDown == true )
	{
		m_hasSlowedDown = false;
	}
}

void dtAIAgent::updateSteering(float dt, dtNavMeshQuery* navquery, const dtQueryFilter* filter, const float* ext, dtTarget *targets, InputGeom *inputGeom)
{
	if (!this->m_isAgentActive) return;
	dtVset(this->dvel, 0.f, 0.f, 0.f);
	if (this->isIdle()) return;
	OpenSteer::Vec3 force = OpenSteer::Vec3::zero;
	//if (hasPathFinidngSteeringBehavior())
	if (hasPathFindingSteeringBehavior(false))
	{
		//if (this->hasPursuitSteeringBehavior() && !this->isAtGoal())
		if (this->hasPursuitSteeringBehavior(false))
		{
			//Ogre::Vector3 newPosition (this->m_AISteeringBehavior->m_pTargetAgent1->getAgentPositionV());
			Ogre::Vector3 newPosition (this->m_AISteeringBehavior->m_pPursuitTargetAgent->getAgentPositionV());
			// Find nearest point on navmesh and set move request to that location.
			float targetPos[3];
			dtPolyRef targetRef;
			//if (this->getRelativeMeshPosition(newPosition.x, newPosition.y, newPosition.z, &targetRef, targetPos, inputGeom))
			if (this->m_crowd->getRelativeMeshPosition(newPosition.x, newPosition.y, newPosition.z, &targetRef, targetPos, inputGeom))
			{
				this->setAgentActiveTarget(new dtTarget(targetPos, targetRef));
				//this->m_crowd->requestMoveTarget(m_agentID, targetRef, targetPos);
					
				if (!this->isAtGoal())
				{
					this->m_crowd->requestMoveTarget(m_agentID, targetRef, targetPos);
					switchToAnimation(m_animationRun, 0);
					this->m_AISteeringBehavior->FakeIdleOff();
				}
				else
					this->fakeIdle();
			}
			else
				this->fakeIdle();
		}
		else
		{
			if (!this->getAgentActiveTarget()) return;
		//if (this->hasPathFollowingSteeringBehavior() && this->isNearGoal(this->getAgentParams()->radius*6))
		//{
		//	this->setAgentActiveTarget(this->getAgentActiveTarget()->getNextTarget()); 
		//	if (!this->getAgentActiveTarget())
		//		this->setAgentActiveTarget(targets);

		//	if (this->getAgentActiveTarget())
			// TODO Adjust target radius
			// TODO Check correct behavior for path following
			if (this->hasPathFollowingSteeringBehavior() && this->isNearGoal(this->getAgentParams()->radius*2.5f))
			{
		//		Ogre::Vector3 target = this->getAgentActiveTarget()->getTargetPosV();
		//		this->m_AISteeringBehavior->SeekOn(target);
		//		this->m_crowd->adjustMoveTarget(m_agentID, this->getAgentActiveTarget()->getTargetRef(), this->getAgentActiveTarget()->getTargetPosF());
				this->setAgentActiveTarget(this->getAgentActiveTarget()->getNextTarget()); 
				if (!this->getAgentActiveTarget())
					this->setAgentActiveTarget(targets);

				if (this->getAgentActiveTarget())
				{
					Ogre::Vector3 target = this->getAgentActiveTarget()->getTargetPosV();
					this->m_AISteeringBehavior->SeekOn(target);
					this->m_crowd->requestMoveTarget(m_agentID, this->getAgentActiveTarget()->getTargetRef(), this->getAgentActiveTarget()->getTargetPosF());
				}				
			}
			else if (this->hasSeekAndArriveSteeringBehavior() && this->m_crowd->getActiveMoveTarget(this->m_agentID) == NULL)
			{
				if(this->isNearGoal(this->getAgentParams()->radius) && m_hasSlowedDown == false)
				{
					if(behaviorName == "attack")
					{
						this->slowDownToAttack();
					}
					else
					{
						this->slowDownToIdle();
					}
					m_hasSlowedDown = true;
				}
				if (this->isAtGoal() )
				{
			//		this->makeIdle();
			//		int count = 1 + m_crowd->getAgentsOnTarget();
			//		m_crowd->setAgentsOnTarget(count );
					/* TODO During pursuit Make Idle when near target */
					//if (!this->hasPursuitSteeringBehavior() && !this->hasSeekSteeringBehavior())
					if(behaviorName == "attack")
					{
						this->makeAttack();
					}
					else
					{
						this->makeIdle();
					}
					int count = 1 + m_crowd->getAgentsOnTarget();
					m_crowd->setAgentsOnTarget(count);

					std::cout<<"count: "<<count<<std::endl;
				}
			}
		}
	}
	else
	{
		Ogre::Vector3 prevPosition (this->getAgentPosition());
	//	this->m_AISteeringBehavior->getSteeringForce(dt);
	//	this->setAgentPosition(this->m_AISteeringBehavior->position().x, this->m_AISteeringBehavior->position().y, this->m_AISteeringBehavior->position().z);
	//	this->setAgentVelocity(this->m_AISteeringBehavior->velocity().x, this->m_AISteeringBehavior->velocity().y, this->m_AISteeringBehavior->velocity().z);
	//	this->setAgentActiveTarget(new dtTarget(this->getAgentPosition()));
	//	Ogre::Vector3 newPosition (this->getAgentPosition());
	//	newPosition.y = prevPosition.y + 0.4f*this->getAgentParams()->height;
	//	// Find nearest point on navmesh and set move request to that location.
	//	float targetPos[3];
	//	dtPolyRef targetRef;
	//	if (this->getRelativeMeshPosition(newPosition.x, newPosition.y, newPosition.z, &targetRef, targetPos, inputGeom))
		this->m_AISteeringBehavior->getSteeringForce(dt, flock, this->m_agentID);
		
		dtVset(this->dvel, this->m_AISteeringBehavior->velocity().x, this->m_AISteeringBehavior->velocity().y, this->m_AISteeringBehavior->velocity().z);
/*		if (this->m_agentID == 1)
			dtVset(this->dvel, this->m_crowd->getAgent(0)->getAgentDVel()[0], this->m_AISteeringBehavior->velocity().y, this->m_crowd->getAgent(0)->getAgentDVel()[2]);

		if (this->m_agentID == 0)
		{		
			//this->setAgentPosition(this->m_AISteeringBehavior->position().x, this->m_AISteeringBehavior->position().y, this->m_AISteeringBehavior->position().z);
			this->setAgentVelocity(this->m_AISteeringBehavior->velocity().x, this->m_AISteeringBehavior->velocity().y, this->m_AISteeringBehavior->velocity().z);
			//cout << this->dvel[0] << ", " << this->dvel[1] << this->dvel[2] << endl;
			cout << "[" << this->m_agentID << "] vel: (" << vel[0] << "," << vel[1] << "," << vel[2] << ")";
		
			float newPosition[3];
			dtVmad(newPosition, this->getAgentPosition(), this->getAgentVel(), dt);
			newPosition[1] = prevPosition.y + 0.4f*this->getAgentParams()->height;
			//this->setAgentActiveTarget(new dtTarget(this->getAgentPosition()));
			// Find nearest point on navmesh and set move request to that location.
			float targetPos[3];
			dtPolyRef targetRef;
			if (this->m_crowd->getRelativeMeshPosition(newPosition[0], newPosition[1], newPosition[2], &targetRef, targetPos, inputGeom))
			{
				// TODO fix agents positioning
				//targetPos[1] += 1.5f;
				this->setAgentPosition(targetPos, dt);
				this->setAgentActiveTarget(new dtTarget(targetPos, targetRef));
				cout << "\t pos: (" << npos[0] << "," << npos[1] << "," << npos[2] << ")\n";
				if ((fabs(prevPosition.x - targetPos[0])  < 1E-5) && (fabs(prevPosition.z - targetPos[2]) < 1E-5))
					this->setAgentVelocity(-this->getAgentVelV());
			}
		}*/
	}
}

void dtAIAgent::integrateNewSpeed( const float dt )
{	
	if (this->hasPathFindingSteeringBehavior())
	{
		// Fake dynamic constraint.
		const float maxDelta = this->getAgentParams()->maxAcceleration * dt;
		float dv[3];
		dtVsub(dv, this->getAgentNVel(), this->getAgentVel());
		float ds = dtVlen2D(dv);
		if (ds > maxDelta)
			dtVscale(dv, dv, maxDelta/ds);

		this->setAgentVelocity(this->getAgentVel()[0] + dv[0], this->getAgentVel()[1] + dv[1], this->getAgentVel()[2] + dv[2], dt);
		this->setAgentPositionAbsolute(this->getAgentPosition());

		// Integrate
		if (dtVlen2D(this->getAgentVel()) > 0.0001f)
		{
			dtVmad(this->getAgentPosition(), this->getAgentPosition(), this->getAgentVel(), dt);
			this->setAgentPositionAbsolute(this->getAgentPosition());
		}
		else
			this->setAgentVelocity(0,0,0);
	}
	else if(!this->isIdle() && !this->hasPursuitSteeringBehaviorIdle()/*hasPursuitSteeringBehavior(false)*/)
	{
		//this->setAgentVelocity(this->nvel);
		// Fake dynamic constraint.
		this->setAgentVelocity(this->dvel);
/*
		const float maxDelta = this->getAgentParams()->maxAcceleration * dt;
		float dv[3];
		dtVsub(dv, this->getAgentDVel(), this->getAgentVel());
		float ds = dtVlen(dv);
		if (ds > maxDelta)
			dtVscale(dv, dv, maxDelta/ds);

		this->setAgentVelocity(this->getAgentVel()[0] + dv[0], this->getAgentVel()[1] + dv[1], this->getAgentVel()[2] + dv[2], dt);
*/	
		//cout << "[" << this->m_agentID << "] vel: (" << vel[0] << "," << vel[1] << "," << vel[2] << ")";
		// Integrate
		if (dtVlen2D(this->getAgentVel()) > 0.0001f)
		{
			float targetPosition[3];

			Ogre::Vector3 oldPosition (this->getAgentPosition());
			dtVmad(targetPosition, this->getAgentPosition(), this->getAgentVel(), dt);
				
			/*this->setAgentPosition(this->getAgentPosition()[0], this->getAgentPosition()[1], this->getAgentPosition()[2]);	
			cout << this->m_agentID << "V:" << Ogre::Vector3(vel) << "\t";*/
		
			if(!this->setAgentPositionRelative(targetPosition, dt))
			{
				//cout << "Reset to old position";
				this->setAgentPositionAbsolute(oldPosition.x, oldPosition.y, oldPosition.z);
			}
			else
			{
				Ogre::Vector3 newVelocity (this->getAgentVel());
				bool bInverse = false;
				if ((fabs(oldPosition.x - this->getAgentPosition()[0])  < 1E-5) && (fabs(this->getAgentVel()[0]) > 1E-4))
				{
					newVelocity.x = -newVelocity.x;
					bInverse = true;
				}
				if ((fabs(oldPosition.z - this->getAgentPosition()[2])  < 1E-5) && (fabs(this->getAgentVel()[2]) > 1E-4))
				{
					newVelocity.z = -newVelocity.z;
					bInverse = true;
				}
				
				if (bInverse) this->setAgentVelocity(newVelocity);
				/*
				if ((fabs(oldPosition.x - this->getAgentPosition()[0])  < 1E-5) && (fabs(oldPosition.z - this->getAgentPosition()[2]) < 1E-5))
				{
					this->setAgentVelocity(-this->getAgentVelV());
					cout << "Got stack - inverse velocity";
				}
				*/
			}
/*
			cout << "\t pos: (" << npos[0] << "," << npos[1] << "," << npos[2] << ")\n";
			Ogre::String text = Ogre::StringConverter::toString(this->getAgentPositionV());
			text += "\n" + Ogre::StringConverter::toString(this->getAgentVelV() * dt);
				
			if (!this->hasEvadeSteeringBehavior())
			{
				if (this->m_agentID)
				{
					UtilitiesPtr->GetDebugText("AgentInfo2")->SetText(text);
				}
				else
				{
					UtilitiesPtr->GetDebugText("AgentInfo")->SetText(text);
				}
			}
*/
		}
		else
			this->setAgentVelocity(0,0,0);
	}
}

void dtAIAgent::render(Ogre::ManualObject* dd, char* materialName)
{
	if (!this->m_isAgentActive) return;
	
//#ifdef DEBUG_AI
	// TODO: Show target for wandering agents
	
	//if (this->hasWanderSteeringBehavior() && this->m_activeTarget)
	//	detourDrawCross(dd, this->m_activeTarget->getTargetPosF()[0], this->m_activeTarget->getTargetPosF()[1]+0.1f,this->m_activeTarget->getTargetPosF()[2], 1.0, Ogre::ColourValue(1.0f,1.0f,1.0f,0.75f), 2.0f, materialName);
	float height = this->getAgentParams()->height;
	const float radius = this->getAgentParams()->radius;
	const float* pos = this->getAgentPosition();
	const float* vel = this->getAgentVel();
	unsigned int col = duRGBA(220,220,220,192);
	float* fCol = duDivCol(col);
	height += this->getYOffset();


	if ( m_drawDebugInfo == true )
	{
		detourDrawCircle(dd,pos[0], pos[1]+height, pos[2], radius, Ogre::ColourValue(fCol[0], fCol[1], fCol[2], fCol[3]), 2.0f,"");
		detourDrawArrow(dd, pos[0],pos[1]+height,pos[2], pos[0]+vel[0],pos[1]+height+vel[1],pos[2]+vel[2], 0.0f, 0.4f, Ogre::ColourValue(1.0f,1.0f,1.0f,1.0f), 1.0f, "");
		m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);	
	}
	else
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(false);	
	}


//#endif
}

void dtAIAgent::setWanderSteeringBehavior()
{
	this->m_AISteeringBehavior->WanderOn();
	this->m_activeTarget = NULL;

	behaviorName = "wander";
	switchToAnimation(m_animationRun);
}
/*
void dtAIAgent::setAttackBehavior(float x, float y, float z)
{
	this->setSeekAndArriveSteeringBehavior(x,y,z);
}
*/
void dtAIAgent::makeIdle()
{
	//cout << "Make Idle\n";
	this->m_AISteeringBehavior->AllOf();
	
	if (m_agentGameObject && m_hasSlowedDown == false )
	{
		this->switchToAnimation(m_animationIdle);
		//this->m_animationEntity->PlayAnimation(m_animationIdle.c_str(), "", true, 0.4f);
	}

	dtVset(gvel, vel[0], vel[1], vel[2]);
	this->setAgentVelocity(0.f, 0.f, 0.f);

	behaviorName = "idle";
	this->m_activeTarget = NULL;
}

void dtAIAgent::makeAttack()
{
	//cout << "Make Idle\n";
	this->m_AISteeringBehavior->AllOf();

	dtVset(gvel, vel[0], vel[1], vel[2]);
	this->setAgentVelocity(0.f, 0.f, 0.f);

	//behaviorName = "attacking";
	this->switchToAnimation("SliceHorizontal", 0.8f);
	this->m_activeTarget = NULL;
}

void dtAIAgent::slowDownToIdle()
{
	if (m_agentGameObject)
	{
		this->switchToAnimation(m_animationIdle, 0.8f);
	}
}

void dtAIAgent::slowDownToAttack()
{
	if (m_agentGameObject)
	{
		this->switchToAnimation("DrawSwords", 0.8f);
	}
}

void dtAIAgent::fakeIdle()
{
	if (m_agentGameObject)
	{
		//cout << "Fake Idle\t"<<endl;
		this->switchToAnimation(m_animationIdle, 0);
	}

	if (!this->m_AISteeringBehavior->isFakeIdleOn())
	{
		dtVset(gvel, vel[0], vel[1], vel[2]);
		this->m_AISteeringBehavior->FakeIdleOn();
	}
	this->setAgentVelocity(0.f, 0.f, 0.f);	
}

void dtAIAgent::setPathFollowingSteeringBehavior(dtTarget* target)
{
	if (!this->m_AISteeringBehavior->isFollowPathOn())
	{
		if (this->isIdle()) 
			this->setAgentVelocity(0,0,0,0);
		else
			this->m_AISteeringBehavior->AllOf();

		this->m_AISteeringBehavior->FollowPathOn();
		if (this->getAgentActiveTarget() == NULL)
		{
			this->setAgentActiveTarget(target);
			this->m_AISteeringBehavior->SeekOn(target->getTargetPosV());
			this->m_crowd->requestMoveTarget(this->m_agentID, target->getTargetRef(), target->getTargetPosF());
		}
	}
	
	behaviorName = "Follow Path";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::setSeekAndArriveSteeringBehavior(float pos[3])
{
	//cout << "Set seek and arrive f*\n";
	this->setSeekAndArriveSteeringBehavior(pos[0], pos[1], pos[2]);
}

void dtAIAgent::setSeekAndArriveSteeringBehavior(float x, float y, float z)
{
	//cout << "Set seek and arrive f3\n";
	float targetPos[3];
	dtPolyRef targetRef;
	// TODO check navquery usage with return values
	this->m_crowd->getRelativeMeshPosition(x, y, z, &targetRef, targetPos);
	this->setSeekAndArriveSteeringBehavior(new dtTarget(targetPos, targetRef));	
}

void dtAIAgent::setSeekAndArriveSteeringBehavior(dtTarget* target)
{
	//cout << "Set seek and arrive t*\n";
	if (this->isIdle()) 
		this->setAgentVelocity(0,0,0,0);
	else
		this->m_AISteeringBehavior->AllOf();

	this->setAgentActiveTarget(target);
	this->m_AISteeringBehavior->SeekOn(target->getTargetPosV());
	this->m_AISteeringBehavior->ArriveOn();
	this->m_crowd->requestMoveTarget(this->m_agentID, target->getTargetRef(), target->getTargetPosF());
	this->m_crowd->setAgentsOnTarget(0);

	behaviorName = "arrive";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::setSeekAndAttackSteeringBehavior(float x, float y, float z)
{
	//cout << "Set seek and arrive f3\n";
	float targetPos[3];
	dtPolyRef targetRef;
	// TODO check navquery usage with return values
	this->m_crowd->getRelativeMeshPosition(x, y, z, &targetRef, targetPos);
	this->setSeekAndAttackSteeringBehavior(new dtTarget(targetPos, targetRef));	
}

void dtAIAgent::setSeekAndAttackSteeringBehavior(dtTarget* target)
{
	//cout << "Set seek and arrive t*\n";
	if (this->isIdle()) 
		this->setAgentVelocity(0,0,0,0);
	else
		this->m_AISteeringBehavior->AllOf();

	this->setAgentActiveTarget(target);
	this->m_AISteeringBehavior->SeekOn(target->getTargetPosV());
	this->m_AISteeringBehavior->ArriveOn();
	this->m_crowd->requestMoveTarget(this->m_agentID, target->getTargetRef(), target->getTargetPosF());
	this->m_crowd->setAgentsOnTarget(0);

	behaviorName = "attack";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::setFleeSteeringBehavior(Ogre::Vector3 targetPos)
{
	if (this->isIdle()) 
		this->setAgentVelocity(0,0,0,0);
	else
		this->m_AISteeringBehavior->AllOf();

	this->m_AISteeringBehavior->FleeOn(targetPos);
	this->m_activeTarget = NULL;

	behaviorName = "flee";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::setSeekSteeringBehavior(float pos[3])
{
	this->setSeekSteeringBehavior(pos[0], pos[1], pos[2]);
}

void dtAIAgent::setSeekSteeringBehavior(float x, float y, float z)
{
	float targetPos[3];
	dtPolyRef targetRef;
	// TODO check navquery usage with return values
	//this->m_crowd->m_navquery->findNearestPoly(pos, m_crowd->getQueryExtents(), this->m_crowd->getFilter(), &targetRef, targetPos);
	this->m_crowd->getRelativeMeshPosition(x, y, z, &targetRef, targetPos);
	this->setSeekSteeringBehavior(new dtTarget(targetPos, targetRef));
}

void dtAIAgent::setSeekSteeringBehavior(dtTarget* target)
{
	//if (this->isIdle()) 
	//	this->setAgentVelocity(0,0,0,0);
	//else
		this->m_AISteeringBehavior->AllOf();

	this->setAgentActiveTarget(target);
	this->m_AISteeringBehavior->SeekOn(target->getTargetPosV());
	this->m_crowd->requestMoveTarget(this->m_agentID, target->getTargetRef(), target->getTargetPosF());
	
	setMaxSpeed(4);
	behaviorName = "seek";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::setFleeSteeringBehavior(dtTarget* target)
{
	if (this->isIdle()) 
		this->setAgentVelocity(0,0,0,0);
	else
		this->m_AISteeringBehavior->AllOf();

	if (this->isIdle()) this->setAgentVelocity(0,0,0,0);
	this->m_AISteeringBehavior->FleeOn(target->getTargetPosV());
	this->m_activeTarget = target;

	behaviorName = "flee";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::setPursuitSteeringBehavior(int targetAgentID)
{
	if (targetAgentID != this->getAgentID())
		this->setPursuitSteeringBehavior(this->m_crowd->getAgent(targetAgentID));
}

void dtAIAgent::setPursuitSteeringBehavior(dtAIAgent *targetAgent)
{
	//cout << "Set purusit\n";
	if (!(targetAgent && targetAgent->isAgentActive())) return;

	if (this->isIdle()) 
		this->setAgentVelocity(0,0,0,0);
	else
		this->m_AISteeringBehavior->AllOf();

	this->m_AISteeringBehavior->PursuitOn(targetAgent);
	this->m_activeTarget = NULL;
	this->m_crowd->setAgentsOnTarget(0);

	behaviorName = "pursuit";
	switchToAnimation(m_animationRun);	
}

void dtAIAgent::setEvadeSteeringBehavior(int targetAgentID)
{
	if (targetAgentID != this->getAgentID())
		this->setEvadeSteeringBehavior(this->m_crowd->getAgent(targetAgentID));
}

void dtAIAgent::setEvadeSteeringBehavior(dtAIAgent *targetAgent)
{
	if (!(targetAgent && targetAgent->isAgentActive())) return;
	// TODO disable to smooth transition
	//if (this->isIdle()) 
	//	this->setAgentVelocity(0,0,0,0);
	//else
		this->m_AISteeringBehavior->AllOf();

	this->m_AISteeringBehavior->EvadeOn(targetAgent);
	this->m_activeTarget = NULL;

	behaviorName = "evade";
	switchToAnimation(m_animationRun);	
}

void dtAIAgent::setFlockingSteeringBehavior()
{
	if (this->isIdle()) 
		this->setAgentVelocity(0,0,0,0);
	else
		this->m_AISteeringBehavior->AllOf();

	this->m_AISteeringBehavior->FlockingOn();
	this->m_activeTarget = NULL;

	behaviorName = "flock";
	switchToAnimation(m_animationRun);
}

void dtAIAgent::removeWanderSteeringBehavior()
{
	this->m_AISteeringBehavior->WanderOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removeSeekAndArriveSteeringBehavior()
{
	this->m_AISteeringBehavior->SeekOff();
	this->m_AISteeringBehavior->ArriveOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removeSeekSteeringBehavior()
{
	this->m_AISteeringBehavior->SeekOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removeFleeSteeringBehavior()
{
	this->m_AISteeringBehavior->FleeOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removeFlockingSteeringBehavior()
{
	this->m_AISteeringBehavior->FlockingOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removetPursuitSteeringBehavior()
{
	this->m_AISteeringBehavior->PursuitOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removeEvadeSteeringBehavior()
{
	this->m_AISteeringBehavior->EvadeOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

void dtAIAgent::removePathFollowingSteeringBehavior()
{
	this->m_AISteeringBehavior->FollowPathOff();

	if (this->isIdle())
	{
		this->m_activeTarget = NULL;
		this->switchToAnimation(m_animationIdle);
	}
	behaviorName = "idle";
}

bool dtAIAgent::hasWanderSteeringBehavior()
{
	return this->m_AISteeringBehavior->isWanderOn();
}

bool dtAIAgent::isIdle()
{
	return this->m_AISteeringBehavior->isIdle();
}

bool dtAIAgent::hasPathFollowingSteeringBehavior()
{
	return this->m_AISteeringBehavior->isFollowPathOn();
}

bool dtAIAgent::hasSeekAndArriveSteeringBehavior()
{
	return this->m_AISteeringBehavior->isSeekOn() && this->m_AISteeringBehavior->isArriveOn();

}

bool dtAIAgent::hasFleeSteeringBehavior()
{
	return this->m_AISteeringBehavior->isFleeOn();
}

bool dtAIAgent::hasPursuitSteeringBehavior(bool checkFakeIdle)
{
	return this->m_AISteeringBehavior->isPursuitOn() && (!checkFakeIdle || (checkFakeIdle && !this->m_AISteeringBehavior->isFakeIdleOn()));
}

bool dtAIAgent::hasPursuitSteeringBehaviorIdle()
{
	return this->m_AISteeringBehavior->isPursuitOn() && this->m_AISteeringBehavior->isFakeIdleOn();
}

bool dtAIAgent::hasEvadeSteeringBehavior()
{
	return this->m_AISteeringBehavior->isEvadeOn();
}

bool dtAIAgent::hasSeekSteeringBehavior()
{
	return this->m_AISteeringBehavior->isSeekOn();
}

bool dtAIAgent::hasPathFindingSteeringBehavior(bool checkFakeIdle)
{
	return (this->hasPursuitSteeringBehavior(checkFakeIdle) || this->hasSeekSteeringBehavior() || 
		this->hasPathFollowingSteeringBehavior() || this->hasSeekAndArriveSteeringBehavior());
}

bool dtAIAgent::hasFlockingSteeringBehavior()
{
	return this->m_AISteeringBehavior->isFlockingOn();
}

Ogre::String dtAIAgent::getAgentEntityName() 
{ 
	return AGENT_ENTITY_PREFIX + Ogre::StringConverter::toString(this->m_crowd->getCrowdID()) + "_" + Ogre::StringConverter::toString(this->m_agentID); 
}

Ogre::String dtAIAgent::getAgentNodeName() 
{ 
	return AGENT_NODE_PREFIX + Ogre::StringConverter::toString(this->m_crowd->getCrowdID()) + "_" + Ogre::StringConverter::toString(this->m_agentID); 
}

bool dtAIAgent::isAtGoal()
{	
	int agentsArrived = this->m_crowd->getAgentsOnTarget();
	float targetRadius;

	if (this->hasPursuitSteeringBehavior(false))
		targetRadius = (this->getAgentParams()->radius + this->m_AISteeringBehavior->m_pPursuitTargetAgent->getRadius())*1.25f;
	else
		targetRadius = this->getAgentParams()->radius * 0.5f;

	
    if ( agentsArrived == 0 )
	{
		targetRadius = targetRadius;
	}
	//else if ( agentsArrived == 1 )
	//{
	//	targetRadius = targetRadius * 4.0f;
	//}
	//else if ( agentsArrived == 2 )
	//{
	//	targetRadius = targetRadius * 5.0f;
	//}
	//else if ( agentsArrived == 3 )
	//{
	//	targetRadius = targetRadius * 6.0f;
	//}
	//if ( agentsArrived < 4 )
	//{
	//	targetRadius = /*(agentsArrived < 2) ? targetRadius :*/ targetRadius * 4.0f;
	//}
	else
	{
		// Radius of each agent, including separation
		float agentRadius = this->getAgentParams()->radius + this->getAgentParams()->separationWeight;
		// Area taken up by one agent
		float agentArea = agentRadius*agentRadius*Ogre::Math::PI;
		// Total area taken up by all the agents that has already arrived at the target
		float agentTotalArea = agentArea * agentsArrived;
		// Adjust the target radius
		targetRadius = Ogre::Math::Sqrt(agentTotalArea/Ogre::Math::PI);
	}
	float distance = this->getDistanceToGoal(targetRadius);

	//std::cout<<this->getAgentID()<<"   Target Radius: "<<targetRadius<<"   Distance: "<<distance<<std::endl;

	if ( distance >= targetRadius )
		return false;
	else
		return true;
}

bool dtAIAgent::isNearGoal( const float range )
{
	return this->getDistanceToGoal( range ) < range;
}

float dtAIAgent::getDistanceToGoal( const float range )
{
	if (!this->getAgentActiveTarget())
	{
		return range;
	}

	const bool endOfPath = (this->cornerFlags[this->getNCorners()-1] & DT_STRAIGHTPATH_END) ? true : false;
	if (endOfPath || hasPursuitSteeringBehaviorIdle())
	{
		float dist = dtVdist2D(this->getAgentPosition(), this->getAgentActiveTarget()->getTargetPosF());
		//cout << "D(" << dist << "/" << range << ")R\t";
		return dtMin(dist, range);
	}
	return range;
}

float dtAIAgent::getDistanceToCorner( const float range )
{
	if (!this->getNCorners())
		return range;
	const bool endOfPath = (this->cornerFlags[this->getNCorners()-1] & DT_STRAIGHTPATH_END) ? true : false;
	if (endOfPath)
		return dtMin(dtVdist2D(this->getAgentPosition(), &this->cornerVerts[(this->getNCorners()-1)*3]), range);
	return range;
}

bool dtAIAgent::overOffmeshConnection( const float radius )
{	
	if (!this->getNCorners())
		return false;
	const bool offMeshConnection = (this->cornerFlags[this->getNCorners()-1] & DT_STRAIGHTPATH_OFFMESH_CONNECTION) ? true : false;
	if (offMeshConnection)
	{
		const float distSq = dtVdist2DSqr(this->getAgentPosition(), &this->cornerVerts[(this->getNCorners()-1)*3]);
		if (distSq < radius*radius)
			return true;
	}
	return false;
}

void dtAIAgent::calcSmoothSteerDirection(float* dir)
{
	if (!this->getNCorners())
	{
		dtVset(dir, 0,0,0);
		return;
	}
	const int ip0 = 0;
	const int ip1 = dtMin(1, this->getNCorners()-1);
	const float* p0 = &this->cornerVerts[ip0*3];
	const float* p1 = &this->cornerVerts[ip1*3];
	float dir0[3], dir1[3];
	dtVsub(dir0, p0, this->getAgentPosition());
	dtVsub(dir1, p1, this->getAgentPosition());
	dir0[1] = 0;
	dir1[1] = 0;
	float len0 = dtVlen(dir0);
	float len1 = dtVlen(dir1);
	if (len1 > 0.001f)
		dtVscale(dir1,dir1,1.0f/len1);
	dir[0] = dir0[0] - dir1[0]*len0*0.5f;
	dir[1] = 0;
	dir[2] = dir0[2] - dir1[2]*len0*0.5f;
	dtVnormalize(dir);
}

void dtAIAgent::calcStraightSteerDirection(float* dir)
{
	if (!this->getNCorners())
	{
		dtVset(dir, 0,0,0);
		return;
	}
	dtVsub(dir, &this->cornerVerts[0], this->getAgentPosition());
	dir[1] = 0;
	dtVnormalize(dir);
}

void dtAIAgent::getAgentBounds(float* bmin, float* bmax)
{
	const float* p = this->getAgentPosition();
	const float r = this->getAgentParams()->radius;
	const float h = this->getAgentParams()->height;
	bmin[0] = p[0] - r;
	bmin[1] = p[1];
	bmin[2] = p[2] - r;
	bmax[0] = p[0] + r;
	bmax[1] = p[1] + h;
	bmax[2] = p[2] + r;
}

int dtAIAgent::addToOptQueue(dtAIAgent** agents, const int nagents, const int maxAgents)
{
	// Insert neighbour based on greatest time.
	int slot = 0;
	if (!nagents)
	{
		slot = nagents;
	}
	else if (this->getAgentTopologyOptTime() <= agents[nagents-1]->getAgentTopologyOptTime() )
	{
		if (nagents >= maxAgents)
			return nagents;
		slot = nagents;
	}
	else
	{
		int i;
		for (i = 0; i < nagents; ++i)
			if (this->getAgentTopologyOptTime()  >= agents[i]->getAgentTopologyOptTime() )
				break;
		const int tgt = i+1;
		const int n = dtMin(nagents-i, maxAgents-tgt);
		dtAssert(tgt+n <= maxAgents);
		if (n > 0)
			memmove(&agents[tgt], &agents[i], sizeof(dtAIAgent*)*n);
		slot = i;
	}
	agents[slot] = this;
	return dtMin(nagents+1, maxAgents);
}

void dtAIAgent::setAgentPositionAbsolute( float *pos, float dt )
{
	this->setAgentPositionAbsolute(pos[0], pos[1], pos[2], dt);
}

void dtAIAgent::setAgentPositionAbsolute( Ogre::Vector3 pos, float dt )
{
	this->setAgentPositionAbsolute(pos.x, pos.y, pos.z, dt);
}

void dtAIAgent::setAgentPositionAbsolute( float x, float y, float z, float dt )
{
	this->npos[0] = x;
	this->npos[1] = y;
	this->npos[2] = z;
	
	this->setState(DT_CROWDAGENT_STATE_WALKING);
	
	this->m_AISteeringBehavior->updatePosition(x, y, z, dt);
}

bool dtAIAgent::setAgentPositionRelative( float *pos, float dt )
{
	return this->setAgentPositionRelative(pos[0], pos[1], pos[2], dt);
}

bool dtAIAgent::setAgentPositionRelative( Ogre::Vector3 pos, float dt )
{
	return this->setAgentPositionRelative(pos.x, pos.y, pos.z, dt);
}

bool dtAIAgent::setAgentPositionRelative( float x, float y, float z, float dt )
{
	static int counter = 0;
	// Find nearest point on navmesh and set move request to that location.
	Ogre::Vector3 oldPosition(this->getAgentPosition());
	float targetPos[3];
	dtPolyRef targetRef;
	InputGeom* inputGeom = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager()->getInputGeom();
	if (this->m_crowd->getRelativeMeshPosition(x, y + this->getAgentParams()->height, z, &targetRef, targetPos, inputGeom))
	{
		if (targetRef)
		{
			// TODO add boundary resetting
			this->setState(DT_CROWDAGENT_STATE_WALKING);

			// TODO fix agents positioning
			this->setAgentPositionAbsolute(targetPos, dt);
			this->getAgentCorridor()->reset(targetRef, targetPos);
			this->getAgentBoundary().reset();
	
			return true;
		}
		else
		{
			float p[] = {0,0,0};
			this->setState(DT_CROWDAGENT_STATE_INVALID);
			this->getAgentCorridor()->reset(-1, p);
			this->getAgentBoundary().reset();
		}
	}

	//this->setState(DT_CROWDAGENT_STATE_INVALID);
	return false;
}

void dtAIAgent::setAgentVelocity( float *vel, float dt )
{
	this->setAgentVelocity(vel[0], vel[1], vel[2], dt);
}

void dtAIAgent::setAgentVelocity( Ogre::Vector3 vel, float dt )
{
	this->setAgentVelocity(vel.x, vel.y, vel.z, dt);
}

void dtAIAgent::setAgentVelocity( float x, float y, float z, float dt )
{
	this->m_AISteeringBehavior->setSpeed(x,y,z, dt);
	OpenSteer::Vec3 newVel = this->m_AISteeringBehavior->velocity();

	dtVset(vel, newVel.x, newVel.y, newVel.z);
	if (newVel.length() != 0) dtVset(gvel, newVel.x, newVel.y, newVel.z);
}

void dtAIAgent::setMaxSpeed(float maxSpeed)
{
	this->params.maxSpeed = maxSpeed;
	//m_crowd->updateAgentParameters(m_agentID,&(this->params));
	this->m_AISteeringBehavior->setMaxSpeed(maxSpeed);
}

void dtAIAgent::setMaxAcceleration(float maxAcceleration)
{
	this->params.maxAcceleration = maxAcceleration;
	this->m_AISteeringBehavior->setMaxForce(maxAcceleration);
}

void dtAIAgent::initialize()
{
	// Create log manager;
	m_log = Ogre::LogManager::getSingletonPtr();
	dtAIAgent::m_tfm = TextFileManager::getSingletonPtr();
	m_switchAttack = true;
	// Initialize debug data structures

	if (m_luaFileName != ""){
		reloadLua();
	}

	setDebug(false,false);
}

void dtAIAgent::bindAI()
{
	// Add a pointer to the AI Agent to the following functions
	lua_bindAI(&GamePipe::l_makeIdle, "makeIdle", m_luaVM);
	lua_bindAI(&GamePipe::l_getDestination, "getDestination", m_luaVM);
	lua_bindAI(&GamePipe::l_setMaxSpeed, "setMaxSpeed", m_luaVM);
	lua_bindAI(&GamePipe::l_getPosition, "getPosition", m_luaVM);
	lua_bindAI(&GamePipe::l_setPosition, "setPosition", m_luaVM);
	lua_bindAI(&GamePipe::l_setWander, "setWander", m_luaVM);
	lua_bindAI(&GamePipe::l_setSeekTarget, "setSeekTarget", m_luaVM);
	lua_bindAI(&GamePipe::l_setSeekAndArriveTarget, "setSeekAndArriveTarget", m_luaVM);
	lua_bindAI(&GamePipe::l_setSeekAndAttackTarget, "setSeekAndAttackTarget", m_luaVM);
	lua_bindAI(&GamePipe::l_setPursuitAgent, "setPursuitAgent", m_luaVM);
	lua_bindAI(&GamePipe::l_setFleeTarget, "setFleeTarget", m_luaVM);
	lua_bindAI(&GamePipe::l_setEvadeAgent, "setEvadeAgent", m_luaVM);
	lua_bindAI(&GamePipe::l_playAnimation,"playAnimation",m_luaVM);
	lua_bindAI(&GamePipe::l_stopAnimation,"stopAnimation", m_luaVM);
	lua_bindAI(&GamePipe::l_setDebugText, "setDebugText", m_luaVM);
	lua_bindAI(&GamePipe::l_setDefaultAnimation, "setDefaultAnimation", m_luaVM);
	lua_bindAI(&GamePipe::l_getDefaultAnimation, "getDefaultAnimation", m_luaVM);
	lua_bindAI(&GamePipe::l_setBlendFrame, "setBlendFrame", m_luaVM);
	lua_bindAI(&GamePipe::l_getBlendFrame, "getBlendFrame", m_luaVM);
	lua_bindAI(&GamePipe::l_getActiveAnimation, "getActiveAnimation", m_luaVM);
	lua_bindAI(&GamePipe::l_transitAndPlayAnimation, "transitAndPlayAnimation", m_luaVM);
}

Ogre::String dtAIAgent::getActiveAnimation()
{
	return m_agentGameObject->m_pAnimatedEntity->GetActiveAnimation();
}

std::vector<std::string> dtAIAgent::getAnimations()
{
	if(m_agentGameObject->m_pAnimatedEntity)
	{
		return m_agentGameObject->m_pAnimatedEntity->GetAnimationNames();
	}else
	{
		m_log = new Ogre::LogManager();
		Ogre::String message = "ERROR: ";
		message.append(m_name);
		message.append(" does not have an animationEntity.");
		m_log->logMessage(message, Ogre::LML_CRITICAL);
		std::vector<std::string> temp;
		return temp;
	}
}

float dtAIAgent::getBlendFrame()
{
	return m_blendFrame;
}

Ogre::String dtAIAgent::getDefaultAnimation()
{
	return m_agentGameObject->m_pAnimatedEntity->GetDefaultAnimation();
}

Ogre::Vector3 dtAIAgent::getDestination()
{
	if (!this->isAgentActive()) return Ogre::Vector3::ZERO;
	if (this->m_activeTarget)
		return this->m_activeTarget->getTargetPosV();
	else
		return this->getAgentPositionV();
}

int dtAIAgent::getKnownLuaStackSize()
{
	return m_luaStackSize;
}

void dtAIAgent::initializeLua()
{
	if(m_luaFileName != ""){
		// Initialize the Lua script if an init function is defined
		m_luaStackSize = lua_gettop(m_luaVM);
		lua_getglobal(m_luaVM, "init");
		if (!lua_isnil(m_luaVM, -1)){
			lua_call(m_luaVM,0,0);
		}
		else{
			lua_remove(m_luaVM, -1);
		}
	}
}

void dtAIAgent::reloadLua()
{
	m_luaInitilize = false;
	if(m_luaVM){
		lua_close(m_luaVM);
	}
	// Create Lua state
	m_luaVM = lua_open();
	luaL_openlibs(m_luaVM);
	// Load Lua script
	Ogre::ResourceGroupManager::getSingleton().declareResource(m_luaFileName.c_str(), "TextFile");
	if(!Ogre::ResourceGroupManager::getSingletonPtr()->resourceGroupExists("LUA")){
		Ogre::ResourceGroupManager::getSingletonPtr()->createResourceGroup("LUA");
	}
	TextFilePtr textfile = m_tfm->load(m_luaFileName.c_str(), "LUA");
	Ogre::String log = "LUA INFO: Loading lua file ";
	log.append(m_luaFileName);
	Ogre::LogManager::getSingleton().logMessage(log, Ogre::LML_NORMAL);
	luaL_dostring(m_luaVM, textfile->getString().c_str());
	// Init position
	m_position = Ogre::Vector3::ZERO;
	// Bind the AI functions to Lua.
	bindAI();
	// XXX
	// Shouldn't need to unload the file but for some reason unloading the LUA
	// group does not unload the text files and any changes to the file will
	// not be seen by further loads.
	// This is not desirable because multiple AI's using the same lua file have
	// to reload the file instead of using a cached file.
	textfile->unload();
	// Initialize state of the FSM
	lua_getglobal(m_luaVM, "state");
	if (!lua_isnil(m_luaVM, -1)){
		m_state = lua_tostring(m_luaVM, -1);
	}
	else{
		lua_remove(m_luaVM, -1);
		m_state = "";
		Ogre::String message = "ERROR: ";
		message.append(m_luaFileName);
		message.append(" does not define a \"state\" variable containing a ");
		message.append("function name to set the Lua state machine to.");
		m_log->logMessage(message, Ogre::LML_CRITICAL);
	}
}

void dtAIAgent::setOnBehaviors(Ogre::String name)
{
	AISteeringBehavior::behavior_type type = m_AISteeringBehavior->behaviorType[name];
	switch (type)
	{
	case AISteeringBehavior::flee : this->setFleeSteeringBehavior(m_AISteeringBehavior->m_vFleeTarget);
		break;
	case AISteeringBehavior::seek : this->setSeekAndArriveSteeringBehavior(this->getAgentActiveTarget());
		break;
	case AISteeringBehavior::wander : this->setWanderSteeringBehavior();
		break;
	case AISteeringBehavior::pursuit : this->setPursuitSteeringBehavior(m_AISteeringBehavior->m_pPursuitTargetAgent);
		break;
	case AISteeringBehavior::evade :  m_AISteeringBehavior->EvadeOn(m_AISteeringBehavior->m_pEvadeTargetAgent);
		break;
	case AISteeringBehavior::flock : this->setFlockingSteeringBehavior();
		//	break;
		/*case AISteeringBehavior::cohesion : m_AISteeringBehavior->CohesionOn();
		break;
		case AISteeringBehavior::separation : m_AISteeringBehavior->SeparationOn();
		break;
		case AISteeringBehavior::allignment : m_AISteeringBehavior->AlignmentOn();
		break;
		case AISteeringBehavior::flock : m_AISteeringBehavior->FlockingOn();
		break;*/
	}
}

void dtAIAgent::setArriveTarget(Ogre::Vector3 arriveTarget)
{
	//m_AISteeringBehavior->m_vArriveTarget = arriveTarget;
}

void dtAIAgent::setFleeTarget(Ogre::Vector3 fleeTarget)
{
	m_AISteeringBehavior->m_vFleeTarget = fleeTarget;
}

void dtAIAgent::setSeekTarget(Ogre::Vector3 seekTarget)
{
	m_AISteeringBehavior->m_vSeekTarget = seekTarget;
}

void dtAIAgent::setEnableLua(bool enable){
	m_enableLua = enable;
}

bool dtAIAgent::playAnimation(Ogre::String newAnimName, bool loop, Ogre::String transitionAnimation)
{
	if(m_agentGameObject->m_pAnimatedEntity)
	{
		m_agentGameObject->m_pAnimatedEntity->PlayAnimation(newAnimName.c_str(), transitionAnimation.c_str(), loop, m_blendFrame);
		//m_animationEntity->PlayAnimation(newAnimName.c_str(), "", true, 0.4f);
		return true;
	}else
	{
		return false;
	}
}

bool dtAIAgent::setBlendFrame(float blendFrame)
{
	m_blendFrame = blendFrame;
	return true;
}

bool dtAIAgent::setDefautAnimation(Ogre::String defaultAnimName)
{
	if(m_agentGameObject->m_pAnimatedEntity)
	{
		std::vector<std::string> animNames = m_agentGameObject->m_pAnimatedEntity->GetAnimationNames();
		std::vector<std::string>::iterator it;
		for(it = animNames.begin(); it != animNames.end(); it++)
		{
			if (defaultAnimName != *it)
			{
				continue;
			}else
			{
				m_agentGameObject->m_pAnimatedEntity->SetDefaultAnimation(defaultAnimName.c_str());
				return true;
			}
		}
		return false;
	}else
	{
		return false;
	}
}

bool dtAIAgent::stopAnimation()
{
	if (m_agentGameObject->m_pAnimatedEntity)
	{
		m_agentGameObject->m_pAnimatedEntity->StopAnimation(m_agentGameObject->m_pAnimatedEntity->GetActiveAnimation());
		return true;
	}
	else
	{
		return false;
	}
}


void dtAIAgent::readAnimationFile( char* filename, const char* character )
{
	char line[100];
	m_animationIdle = ""; m_animationRun = "";
	//TODO: move the text file from Core\Bin to Media
	ifstream animFile(filename);
	if (animFile.good())
	{
		while ( !animFile.eof() )
		{
			animFile.getline(line, 100);
			if ( strcmp(line, character) == 0 )
			{
				animFile.getline(line, 100);
				m_animationIdle = line;
				animFile.getline(line, 100);
				m_animationRun = line;
				break;
			}
		}
	}
}

void dtAIAgent::clearLines()
{

	std::list<dtDebugLine*>::iterator it;
	for(it = m_dtLines->begin() ; it != m_dtLines->end(); it++)
	{
		this->m_agentGameObject->m_pGraphicsObject->m_pOgreSceneManager->destroySceneNode((*it)->m_sceneNode);
		delete *it;
	}
	m_dtLines->clear();

}

Ogre::ManualObject* dtAIAgent::addToLines()
{

	dtDebugLine* line = new dtDebugLine(m_agentGameObject->m_pGraphicsObject->m_pOgreSceneManager);
	line->initialize();
	m_dtLines->push_back(line);
	return line->getManual();
}

void dtAIAgent::setAnimationNameIdle(Ogre::String sAnimationIdle) 
{ 
	this->m_animationIdle = sAnimationIdle; 

	if (isIdle())
		this->switchToAnimation(m_animationIdle);
}

void dtAIAgent::setAnimationNameRun(Ogre::String sAnimationRun) 
{ 
	this->m_animationRun = sAnimationRun; 

	if (!isIdle())
		this->switchToAnimation(m_animationRun);
}

void dtAIAgent::setSinbadMaterial( Ogre::String materialName )
{

	if ( materialName == "" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_white");
	}
	else if ( materialName == "CyanMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_cyan");
	}
	else if ( materialName == "LimeMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_lime");
	}
	else if ( materialName == "GrayMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_gray");
	}
	else if ( materialName == "MagentaMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_magenta");
	}
	else if ( materialName == "OrangeMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_orange");
	}
	else if ( materialName == "GreenMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_green");
	}
	else if ( materialName == "RedMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_red");
	}
	else if ( materialName == "BlueMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_blue");
	}
	else if ( materialName == "YellowMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_yellow");
	}
	else if ( materialName == "WhiteMaterial" )
	{
		m_agentGameObject->m_pGraphicsObject->m_pOgreEntity->getSubEntity(6)->setMaterialName("Sinbad/Clothes_white");
	}

		//if ( materialName == "" )
		//{
		//	EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//		->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName(materialName);
		//}
		//else
		//{
		//	//Change material according to crowd ID
		//	switch( m_crowd->getCrowdID()%10 )
		//	{
		//	case 0:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_yellow");
		//		break;
		//	case 1:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_blue");
		//		break;
		//	case 2:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_lime");
		//		break;
		//	case 3:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_orange");
		//		break;
		//	case 4:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_red");
		//		break;
		//	case 5:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_magenta");
		//		break;
		//	case 6:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_green");
		//		break;
		//	case 7:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_cyan");
		//		break;
		//	case 8:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_gray");
		//		break;
		//	case 9:
		//		EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()
		//			->getEntity(getAgentEntityName())->getSubEntity(6)->setMaterialName("Sinbad/Clothes_white");
		//		break;
		//	default:
		//		break;
		//	}
		//}	
}

bool dtAIAgent::toggleDebugInfo( bool flag  )
{
		m_drawDebugInfo = flag;

		return m_drawDebugInfo;
}

