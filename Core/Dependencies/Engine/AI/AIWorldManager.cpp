
#include "StdAfx.h"
#include "AIWorldManager.h"
#include "InputGeom.h"
#include "Ogre.h"
#include "Sample.h"
#include "HavokWrapper.h"
#include "Ogre.h"
#include "DetourCommon.h"
#include "NavMeshTesterTool.h"
#include "DetourDraw.h"

/************************************************************************/
/* GENERAL METHODS                                                      */
/************************************************************************/
bool AIWorldManager::useRecast = false;
AIWorldManager::AIWorldManager() :
	mActiveCrowdId (0),
	mActiveAgentId(-1),
	mCrowdsHandler(0),
	mCrowdsNo(0),
	mSample (0),
	staticWorld(0),
	mManualObj(0)
	{}

/************************************************************************/
/* AI WORLD MANAGER DESTRUCTOR                                          */
/************************************************************************/
AIWorldManager::~AIWorldManager()
{
	this->destroyCrowds();
}

/************************************************************************/
/* DESTROY CROWDS                                                       */
/************************************************************************/
void AIWorldManager::destroyCrowds()
{
	dtCrowdHandler* crowdHandler = this->mCrowdsHandler, *nextCrowdHandler;
	while(crowdHandler)
	{
		//for( int i = 0 ; i < crowdHandler->getCrowd()->getAgentCount() ; i++)
		//{
		//	
		//	delete crowdHandler->getCrowd()->getAgent(i);
		//}
		//delete[] crowdHandler->getCrowd()->getAgent(0);
		nextCrowdHandler = crowdHandler->getNextCrowdHandler();
		delete crowdHandler;
		crowdHandler = nextCrowdHandler;
	}	
	this->mCrowdsHandler = NULL;
	this->mCrowdsNo = 0;
	
	dtCrowd::resetStaticMembers();

}

/************************************************************************/
/* INITIALIZE                                                           */
/************************************************************************/
void AIWorldManager::initialize()
{
	staticObstacles = std::list<InputGeom*>();
	dynamicObstacle = std::list<InputGeom*>();

	// Initialize the parameters for building navigation mesh
	m_hasBuiltNaviMesh = false;
	m_cellSize = 0.3f;
	m_cellHeight = 0.2f;
	//m_agentHeight = 2.0f;
	m_agentRadius = 0.6f;
	m_agentMaxClimb = 0.9f;
	m_agentMaxSlope = 45.0f;
	m_regionMinSize = 8;
	m_regionMergeSize = 20;
	m_monotonePartitioning = false;
	m_edgeMaxLen = 12.0f;
	m_edgeMaxError = 1.3f;
	m_vertsPerPoly = 6.0f;
	m_detailSampleDist = 6.0f;
	m_detailSampleMaxError = 1.0f;
	m_debug = false;
	m_drawNaviMesh = false;
	mActiveSign = 0;
}

bool AIWorldManager::isDynamic(GameObject* gObj)
{
	if(gObj->m_eObjectType == PHYSICS_FIXED )
		return false;
	else
		return true;
}
void AIWorldManager::TranslateMesh(Ogre::Vector3 pos,int index,GameObject* gObj, bool translateOrset)
{
	if (index < 0) return;

	rcContext ctx;
	std::list<InputGeom*>::iterator it;
	if(isDynamic(gObj))
	{
		it = dynamicObstacle.begin();
	}
	else
	{
		it = staticObstacles.begin();
	}

	std::advance(it, index);
	const Ogre::MeshPtr cMeshPtr = gObj->m_pGraphicsObject->m_pOgreEntity->getMesh();
	Ogre::MeshPtr ptr = ((Ogre::MeshPtr)cMeshPtr);

	(*it)->setMeshTONull();
	if(!translateOrset)
		(*it)->setPosition(pos);
	else
		(*it)->setPosition((*it)->getPosition() + pos);
	(*it)->loadMesh(&ctx,ptr);
	if(mSample && isDynamic(gObj))
		mSample->updateTempObstacleNahid(index,(*it));

}

void AIWorldManager::OrientMesh(Ogre::Quaternion quad,int index,GameObject* gObj)
{
	if (index < 0) return;
	rcContext ctx;
	std::list<InputGeom*>::iterator it;
	if(isDynamic(gObj))
		it = dynamicObstacle.begin();
	else
		it = staticObstacles.begin();

	std::advance(it, index);
	const Ogre::MeshPtr cMeshPtr = gObj->m_pGraphicsObject->m_pOgreEntity->getMesh();
	Ogre::MeshPtr ptr = ((Ogre::MeshPtr)cMeshPtr);

	(*it)->setMeshTONull();
	Ogre::Quaternion preOrient = (*it)->getOrient();
	(*it)->setOrient(preOrient * quad);
	(*it)->loadMesh(&ctx,ptr);
	if(mSample && isDynamic(gObj))
		mSample->updateTempObstacleNahid(index,(*it));

}

void AIWorldManager::ScaleMesh(Ogre::Vector3 scale,int index,GameObject* gObj)
{
	if (index < 0) return;
	rcContext ctx;

	std::list<InputGeom*>::iterator it;
	if(isDynamic(gObj))
		it = dynamicObstacle.begin();
	else
		it = staticObstacles.begin();
	std::advance(it, index);

	const Ogre::MeshPtr cMeshPtr = gObj->m_pGraphicsObject->m_pOgreEntity->getMesh();
	Ogre::MeshPtr ptr = ((Ogre::MeshPtr)cMeshPtr);

	(*it)->setMeshTONull();
	Ogre::Vector3 preScale = (*it)->getScale();
	(*it)->setScale(preScale * scale);
	(*it)->loadMesh(&ctx,ptr);
	if(mSample && isDynamic(gObj))
		mSample->updateTempObstacleNahid(index,(*it));

}

void AIWorldManager::UpdateWorld(float dt)
{
	if(this->mSample && this->m_hasBuiltNaviMesh)
	{
		this->mSample->handleUpdate(dt);

		if (this->mCrowdsHandler)
		{
			dtNavMesh* nav = this->mSample->getNavMesh();
			if (!nav) return;

			dtCrowdHandler* crowdHandler = this->mCrowdsHandler;
			while(crowdHandler)
			{
				crowdHandler->getCrowd()->update(dt, this->mSample, NULL);
				crowdHandler = crowdHandler->getNextCrowdHandler();
			}
		}
	}

	//Update the position of the active sign
	updateActiveSign();
}

void AIWorldManager::drawAgentPath()
{

	dtAIAgent* agent;
	dtCrowd* crowd = this->getActiveCrowd();
	for(int i = 0 ; i < crowd->getAgentCount() ; i++)
	{
		if(crowd->getAgent(i)->isAgentActive())
		{
			
			agent = crowd->getAgent(i);
			agent->clearLines();
			NavMeshTesterTool* test = new NavMeshTesterTool;
			test->init(mSample);
			test->setStartPoint(agent->getAgentPositionV());
			if(agent->getAgentActiveTarget() != NULL)
			{
				Ogre::Vector3 targetPos = agent->getAgentActiveTarget()->getTargetPosV();
				test->setEndPoint(targetPos);
				test->handleMenu();
				Ogre::ManualObject* man = agent->addToLines();
				test->handleRender(man);
			}
		}
	}
}

void AIWorldManager::RenderWorld()
{
	//return;
	if(this->mSample)
	{
		mManualObj->clear();


		this->mSample->handleRender(mManualObj, m_drawNaviMesh);

		dtNavMesh* nav = this->mSample->getNavMesh();
		if (!nav) return;

		dtCrowdHandler* crowdHandler = this->mCrowdsHandler;
		while(crowdHandler)
		{
			crowdHandler->getCrowd()->render();
			crowdHandler = crowdHandler->getNextCrowdHandler();
		}
/*
		if(m_debug)
			this->drawAgentPath();
		else
		{
			if (this->mCrowdsNo)
			{
				dtAIAgent* agent;
				dtCrowd* crowd = this->getActiveCrowd();
				for(int i = 0 ; i < crowd->getAgentCount() ; i++)
				{
					if(crowd->getAgent(i)->isAgentActive())
					{			
						agent = crowd->getAgent(i);
						agent->clearLines();
					}
				}
			}
		}
*/
	}
}

bool AIWorldManager::CreateStaticWorld()
{
	std::list<InputGeom*>::iterator it;
	this->mSample = new Sample();
	
	// Set the parameters for building navigation mesh
	mSample->resetCommonSettings(m_cellSize, m_cellHeight, m_agentHeight, m_agentRadius, m_agentMaxClimb, m_agentMaxSlope, 
		m_regionMinSize, m_regionMergeSize, m_monotonePartitioning, m_edgeMaxLen, m_edgeMaxError, m_vertsPerPoly, 
		m_detailSampleDist, m_detailSampleMaxError );

	// TODO Fix case when there are no static objects
	if (staticObstacles.size() != 0)
	{
		rcContext ctx ;
		staticWorld = new InputGeom;
		for(it = staticObstacles.begin() ; it != staticObstacles.end(); it++)
			//TODO: combine should be fixed
			staticWorld->CombineInputGeoms((*it));

		staticWorld->SetBounds(&ctx);

		BuildContext ctx2;
		this->mSample->setContext(&ctx2);
		this->mSample->handleMeshChanged(staticWorld);
		this->mSample->handleSettings();
		this->mSample->handleBuild();

	}	

	for(it = dynamicObstacle.begin() ; it != dynamicObstacle.end(); it++)
		this->mSample->addTempObstacle(Ogre::Vector3::ZERO,Ogre::Vector3::UNIT_SCALE, *it);
	Ogre::SceneManager *mSceneMgr = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	mManualObj = mSceneMgr->createManualObject("manual");
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(mManualObj);

	// After nevi mesh is generated, the parameters cannot be changed afterwards
	m_hasBuiltNaviMesh = true;

	
	for(int  i = 0 ; i < this->mCrowdsNo ; i++)
		{
			this->getCrowd(i)->InitNavQueryForPrevAgents(mSample->getNavMesh());
			for(int j = 0 ; j < this->getCrowd(i)->getAgentCount() ; j++)
			{
				if(this->getCrowd(i)->getAgent(j)->isAgentActive())
					this->getCrowd(i)->FindNearestPositionForPrevAgents(this->getCrowd(i)->getAgent(j));
			}
		}
	this->updateAgentHeight(-1,-1,true);
	return mSample->getInputGeom() != NULL;
}

void AIWorldManager::AddToWorld(GameObject* gObj)
{
	//static
	if(!isDynamic(gObj))
	{
		rcContext* ctx = new rcContext();
		const Ogre::MeshPtr cMeshPtr = gObj->m_pGraphicsObject->m_pOgreEntity->getMesh();
		InputGeom* inputGeom = new InputGeom();
		Ogre::MeshPtr ptr = ((Ogre::MeshPtr)cMeshPtr);
		inputGeom->loadMesh(ctx,ptr);
		staticObstacles.push_back(inputGeom);
		gObj->indexInRecastWorld = staticObstacles.size() - 1;
	}
	else
	{
		rcContext* ctx = new rcContext();
		const Ogre::MeshPtr cMeshPtr = gObj->m_pGraphicsObject->m_pOgreEntity->getMesh();
		InputGeom* inputGeom = new InputGeom();
		Ogre::MeshPtr ptr = ((Ogre::MeshPtr)cMeshPtr);
		inputGeom->loadMesh(ctx,ptr);
		dynamicObstacle.push_back(inputGeom);
		gObj->indexInRecastWorld = dynamicObstacle.size() - 1;
	}
	//dynamic obstacle*/
}

void AIWorldManager::deleteWorld()
{
		GamePipe::GameScreen* f_pCurrentGameScreen = EnginePtr->GetForemostGameScreen();
		GameObjectManager* f_pGameObjectManager = f_pCurrentGameScreen->GetGameObjectManager();
		//f_pGameObjectManager->DeleteGameObjects();
		destroyCrowds();

		delete mSample;
		for(list<InputGeom*>::iterator it = staticObstacles.begin();
        it != staticObstacles.end(); it++){
            delete *it;
		}
		for(list<InputGeom*>::iterator it = dynamicObstacle.begin();
        it != dynamicObstacle.end(); it++){
            delete *it;
		}
}

/************************************************************************/
/* CROWD RELATED METHODS                                                */
/************************************************************************/
dtCrowd * AIWorldManager::addNewCrowd( char *agentMaterial /*= "WhiteMaterial"*/, float agentRadius /*= 2.0f*/, float agentHeight /*= 2.0f*/ )
{
	// Navigate to last crowd
	dtCrowdHandler* pCurrentCrowd = this->mCrowdsHandler;
	while (pCurrentCrowd && pCurrentCrowd->getNextCrowdHandler())
		pCurrentCrowd = pCurrentCrowd->getNextCrowdHandler();

	// Initialize new crowd
	dtCrowd* crowd = dtAllocCrowd();
	dtCrowdHandler* pCrowdHandler = new dtCrowdHandler(crowd);

	if (pCurrentCrowd)
		pCurrentCrowd->setNextCrowdHandler(pCrowdHandler);
	else
		this->mCrowdsHandler = pCrowdHandler;

	this->mActiveCrowdId = this->mCrowdsNo;
	if(this->mSample != NULL)
		crowd->init(this->mCrowdsNo++, MAX_CROWD_AGENTS, agentRadius, agentMaterial, this->mSample->getNavMesh());
	else
		crowd->init(this->mCrowdsNo++, MAX_CROWD_AGENTS, agentRadius, agentMaterial, NULL);

	return crowd;
}

/************************************************************************/
/* AGENT RELATED METHODS                                                */
/************************************************************************/
/************************************************************************/
/* ADD NEW TARGET                                                       */
/************************************************************************/
dtTarget* AIWorldManager::addNewTarget( const float* hitpoint, Ogre::String behavior /*= "arrive"*/ )
{
	if (!this->mSample) 
		return NULL;

	InputGeom* geom = this->mSample->getInputGeom();
	if (!geom) 
		return NULL;
	
	dtCrowd *crowd = this->getActiveCrowd();

	if (!crowd) 
		return NULL;

	// Find nearest point on navmesh and set move request to that location.
	dtNavMeshQuery* navquery = this->mSample->getNavMeshQuery();
	const dtQueryFilter* filter = crowd->getFilter();
	const float* ext = crowd->getQueryExtents();

	float targetPos[3];
	dtPolyRef targetRef;
	navquery->findNearestPoly(hitpoint, ext, filter, &targetRef, targetPos);
	
	dtTarget* newTarget = NULL;
	// Add new target to path
	if (behavior == "follow_path")
		newTarget = crowd->addNewTarget(targetPos, targetRef);
	else if (behavior == "arrive") // Set a new target for seek and arrive behavior
		newTarget = crowd->setNewTarget(targetPos, targetRef);
	
	// Update target for agents
	for (int i = 0; i < crowd->getAgentCount(); ++i)
	{
		dtAIAgent* ag = crowd->getAgent(i);
		if (ag->isAgentActive() && (ag->isIdle() || (ag->hasPathFindingSteeringBehavior() && !ag->hasPursuitSteeringBehavior())))
		{
			// TODO Fix behavior when there is already a target
			if (behavior == "follow_path")
				ag->setPathFollowingSteeringBehavior(newTarget);
			else if (behavior == "arrive")
				ag->setSeekAndArriveSteeringBehavior(newTarget);
		}
	}

	if (behavior == "flee" && this->mActiveAgentId != -1)
	{
		this->getActiveAgent()->setFleeSteeringBehavior(new dtTarget(targetPos, targetRef));
	}

	return newTarget;
}

/************************************************************************/
/* SET PURSUIT TARGET                                                   */
/************************************************************************/
void AIWorldManager::setPursuitTarget(int iTarget)
{
	this->setPursuitTarget(this->getActiveCrowd()->getAgent(iTarget));
}

/************************************************************************/
/* SET EVADE TARGET                                                     */
/************************************************************************/
void AIWorldManager::setEvadeTarget(int iTarget)
{
	this->setEvadeTarget(this->getActiveCrowd()->getAgent(iTarget));
}
/************************************************************************/
/* SET PURSUIT TARGET                                                   */
/************************************************************************/
void AIWorldManager::setPursuitTarget(dtAIAgent* target)
{
	if (target && target->isAgentActive())
		this->getActiveAgent()->setPursuitSteeringBehavior(target);
}

/************************************************************************/
/* SET EVADE TARGET                                                     */
/************************************************************************/
void AIWorldManager::setEvadeTarget(dtAIAgent* target)
{
	if (target && target->isAgentActive())
		this->getActiveAgent()->setEvadeSteeringBehavior(target);
}

float AIWorldManager::CalcAgentRadius(float* bmin, float* bmax, dtCrowdAgentParams* ap)
{
	//if (!this->mSample) return false;

	float radius  = 0;
	float dx = bmax[0] - bmin[0];
	float dz = bmax[2] - bmin[2];
	if(radius < dx/2)
	{
		radius = dx / 2;
		ap->radiusXOrZ = true;
	}
	if(radius < dz/ 2)
	{
		radius = dz / 2;
		ap->radiusXOrZ = false;
	}
	ap->radius = radius;

	return radius;

}

/************************************************************************/
/* ADD NEW AGENT                                                        */
/************************************************************************/
dtAIAgent* AIWorldManager::addNewAgent( const float* hitpoint, string meshName /*= ""*/, string hkxName /*= ""*/,string characterName /*= ""*/, char* luaFile /*= ""*/, Ogre::String behavior /*= "none"*/, char* type /*= "GRAPHICS_OBJECT"*/, GameObject* gobj )
{
	//if (!this->mSample) return NULL;
	//InputGeom* geom = this->mSample->getInputGeom();
	//if (!geom) return NULL;

	dtCrowdHandler* crowdHandler = this->mCrowdsHandler;
	if (!crowdHandler || !crowdHandler->getCrowd())
		this->addNewCrowd();

	dtCrowd* crowd = this->getActiveCrowd();

	if(meshName == "")
	{
		meshName = "";
		hkxName = "";
		if ( type == "GRAPHICS_OBJECT" )
		{
			meshName = "Sinbad.mesh";
			hkxName = "";
		}
		else if ( type == "ANIMATED_CHARACTER_RIGIDBODY" )
		{
#ifdef STORMTROOPER
			meshName = "stormtrooper_withgun_bound_LOD.mesh";
			hkxName = "storm_trooper_ragdoll_complete.hkx";
#elif defined HAVOK_GIRL
			std::string meshName = "havokGirl3.mesh";
			std::string hkxName = "havokGirl3.hkx";
#else
			meshName = "merc.mesh";
			hkxName = "merc.hkx";
#endif
		}
	}

	//update the Radius , always set it to the maximum radius among agents in a crowd
	//------------------------------------------------------
	InputGeom* agentGeom = new InputGeom();
	rcContext* ctx = new rcContext();
	//Ogre::MeshManager* meshMn = Ogre::MeshManager.getSingletonPtr();
	Ogre::MeshPtr ptr = Ogre::MeshManager::getSingletonPtr()->load(meshName,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME); 
	agentGeom->loadMesh(ctx,ptr);
	//crowd->setCrowdRadius(((float*)agentGeom->getMeshBoundsMin()),((float*)agentGeom->getMeshBoundsMax()));
	dtCrowdAgentParams ap;
	float agentRadius = CalcAgentRadius(((float*)agentGeom->getMeshBoundsMin()),((float*)agentGeom->getMeshBoundsMax()), &ap);
	//-------------------------------------------------------
	// Add

	memset(&ap, 0, sizeof(ap));
	//ap.radius = this->mSample->getAgentRadius();
	ap.radius = agentRadius;
	ap.height = agentGeom->getMeshBoundsMax()[1] - agentGeom->getMeshBoundsMin()[1];
	ap.maxAcceleration = 8;
	ap.maxSpeed = 3.5;
	//ap.collisionQueryRange = ap.radius * 12.0f;
	//ap.pathOptimizationRange = ap.radius * 30.0f;
	ap.collisionQueryRange = ap.radius * 12.0f;
	ap.pathOptimizationRange = ap.radius * 30.0f;

	ap.updateFlags = 0; 			
	ap.updateFlags |= DT_CROWD_ANTICIPATE_TURNS;
	ap.updateFlags |= DT_CROWD_OPTIMIZE_VIS;
	ap.updateFlags |= DT_CROWD_OPTIMIZE_TOPO;
	ap.updateFlags |= DT_CROWD_OBSTACLE_AVOIDANCE;
	ap.updateFlags |= DT_CROWD_SEPARATION;
	ap.obstacleAvoidanceType = (unsigned char) 3.0f;
	//THis may cuz the problem of getting too close
	ap.separationWeight = 1.5f;

	int idx = crowd->addAgent(hitpoint, &ap, crowd->getTargets(), type, meshName, hkxName, luaFile, characterName, gobj);

	//setting the m_agentHeight
	if(mSample != NULL && mSample->getAgentHeight() < ap.height)
	{
		mSample->setAgentHeight(ap.height);
		mSample->setAgentHeightIndex(idx);
		mSample->setAgentHeightCrowd(crowd->getCrowdID());
	}
	// Set agent physics parameters

	// Set agent behavior
	if (idx != -1)
	{
		if ((behavior == "arrive") || (behavior == "follow_path"))
		{
			dtTarget* target = crowd->getTargets();
			// Create agent with default behavior
			if (target)
			{
				if ((behavior == "follow_path") && target->getNextTarget() && target != target->getNextTarget())
					crowd->getAgent(idx)->setPathFollowingSteeringBehavior(target);
				else if (behavior == "arrive") 
					crowd->getAgent(idx)->setSeekAndArriveSteeringBehavior(target);
			}
		}
		else if(behavior == "pursuit")
			crowd->getAgent(idx)->setPursuitSteeringBehavior(crowd->getAgent(0));
		else if(behavior == "evade")
			crowd->getAgent(idx)->setPursuitSteeringBehavior(crowd->getAgent(0));
		else if(behavior == "wander")
			crowd->getAgent(idx)->setWanderSteeringBehavior();
	}
	else
		return NULL;

	return crowd->getAgent(idx);
}

/************************************************************************/
/* GET CROWD HANDLER                                                    */
/************************************************************************/
dtCrowdHandler* AIWorldManager::getCrowdHandler(int crowdID)
{
	dtCrowdHandler* crowdHandler = this->mCrowdsHandler;

	while(crowdHandler)
	{
		if(crowdHandler->getCrowd()->getCrowdID() == crowdID)
			return crowdHandler;

		crowdHandler = crowdHandler->getNextCrowdHandler();
	}

	return NULL;
}

/************************************************************************/
/* GET CROWD                                                            */
/************************************************************************/
dtCrowd* AIWorldManager::getCrowd(int crowdID)
{
	dtCrowdHandler* crowdHandler = this->getCrowdHandler(crowdID);

	if (crowdHandler)
		return crowdHandler->getCrowd();
	
	return NULL;
}

void AIWorldManager::updateActiveSign( void )
{
	if ( mActiveAgentId != -1 && mActiveSign )
	{
		dtAIAgent* ag = this->getActiveCrowd()->getAgent(mActiveAgentId);

		if (ag)
		{
			//Get the position of currently active agent
			float* pos = ag->getAgentPosition();
			float height = ag->getAgentParams()->height + 0.3f;

			//Set the active sign at the same place
			mActiveSign->setPosition(pos[0], pos[1] + height, pos[2]);
		}

		ag->setDebug(true, true);
	}
}

void AIWorldManager::setNaviParameters( float cellSize, float cellHeight, float agentHeight, float agentRadius, 
	float agentMaxClimb, float agentMaxSlope, float regionMinSize, float regionMergeSize, bool monotonePartitioning, 
	float edgeMaxLen, float edgeMaxError, float vertsPerPoly, float detailSampleDist, float detailSampleMaxError )
{				
	if ( m_hasBuiltNaviMesh == false )
	{
		m_cellSize = cellSize;
		m_cellHeight = cellHeight;
		m_agentHeight = agentHeight;
		m_agentRadius = agentRadius;
		m_agentMaxClimb = agentMaxClimb;
		m_agentMaxSlope = agentMaxSlope;
		m_regionMinSize = regionMinSize;
		m_regionMergeSize = regionMergeSize;
		m_monotonePartitioning = monotonePartitioning;
		m_edgeMaxLen = edgeMaxLen;
		m_edgeMaxError = edgeMaxError;
		m_vertsPerPoly = vertsPerPoly;
		m_detailSampleDist = detailSampleDist;
		m_detailSampleMaxError = detailSampleMaxError;
	}	
}

void AIWorldManager::setActiveAgentPosition(const float* pos) 
{
	dtAIAgent* ag = this->getActiveCrowd()->getAgent(mActiveAgentId);
	
	if (ag && ag->isAgentActive())
		ag->setAgentPositionAbsolute(Ogre::Vector3(pos)); 
}

void AIWorldManager::updateAgentHeight(int m_agentID,int m_crowdID, bool checkAll)
{

	if(checkAll || (mSample->getAgentHeightCrowd() == m_crowdID && mSample->getAgentHeightIndex() == m_agentID))
	{
		mSample->setAgentHeight(0);
		for(int  i = 0 ; i < mCrowdsNo ; i++)
		{

			dtCrowd* temp = this->getCrowd(i);
			for(int j = 0 ; j < temp->getAgentCount() ; j++)
			{
				if(temp->getAgent(j)->getAgentParams()->height > mSample->getAgentHeight())
				{
					mSample->setAgentHeight(temp->getAgent(j)->getAgentParams()->height);
					mSample->setAgentHeightIndex(j);
					mSample->setAgentHeightCrowd(i);
				}
			}
		}
		this->updateDynamicObstacles();
	}
}
void AIWorldManager::updateDynamicObstacles()
{
	if(!mSample)
		return;
	std::list<InputGeom*>::iterator it;
	int index = 0;
	for(it = dynamicObstacle.begin() ; it != dynamicObstacle.end(); it++)
	{
		mSample->updateTempObstacleNahid(index,*it);
		index++;
		
	}
	
}

std::string AIWorldManager::getNextAgentName(std::string prefix)
{
	std::string name = prefix;
	int crowdID = this->getActiveCrowdId();
	int agentID = this->getActiveCrowd()->getActiveAgentsCount();
	std::ostringstream ID;
	ID<<crowdID<<"_"<<agentID;
	name += ID.str();

	return name;
}
