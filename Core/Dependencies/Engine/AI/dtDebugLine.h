#ifndef DEBUG_LINE_H
#define DEBUG_LINE_H

#include "dtDebugObject.h"
#include "OgreVector3.h"

namespace Ogre {
    class SceneManager;
    class SceneNode;
    class ManualObject;
	class Vector3;
}


    class dtDebugLine : public dtDebugObject{
    public:
        Ogre::Vector3 pos1,pos2;
		Ogre::ManualObject* getManual() { return m_object;}

        dtDebugLine(Ogre::SceneManager*);
        virtual ~dtDebugLine();
        virtual void initialize();
        virtual void setColour(Ogre::ColourValue);
        virtual void setVisible(bool);
        virtual void update(float);
    protected:
        
        Ogre::ManualObject *m_object;
    private:
    };


#endif