#pragma once

#include <list>
#include "InputGeom.h"
#include "Ogre.h"
#include "Sample.h"
#include "DetourCrowd.h"
#include "dtAIAgent.h"

/************************************************************************/
/* AI World Manager                                                     */
/************************************************************************/
class AIWorldManager 
{
private:
	InputGeom* staticWorld;
	std::list<InputGeom*> staticObstacles;
	std::list<InputGeom*> dynamicObstacle;

	Sample* mSample;
	bool m_debug;

	Ogre::ManualObject* mManualObj;

	dtCrowdHandler* mCrowdsHandler;
	bool m_drawNaviMesh;

	int mCrowdsNo;
	int mActiveCrowdId;
	int mActiveAgentId;
	Ogre::SceneNode* mActiveSign;

	// Parameters for building  navi mesh
	bool m_hasBuiltNaviMesh;
	float m_cellSize, m_cellHeight, m_agentHeight, m_agentRadius, m_agentMaxClimb, m_agentMaxSlope, m_regionMinSize, 
		m_regionMergeSize, m_edgeMaxLen, m_edgeMaxError, m_vertsPerPoly, m_detailSampleDist, m_detailSampleMaxError;
	bool m_monotonePartitioning;

public:
	// General methods
	static bool useRecast;
	AIWorldManager();
	~AIWorldManager();

	void initialize();
	void destroyCrowds();
	void deleteWorld();

	void UpdateWorld(float DELTA_TIME);
	void RenderWorld();
	void drawAgentPath();

	// World Related methods
	void AddToWorld(GameObject* gObj);
	bool CreateStaticWorld();
	bool isDynamic(GameObject* gObj);

	void TranslateMesh(Ogre::Vector3 pos,int index,GameObject* gObj, bool translateOrSet);
	void OrientMesh(Ogre::Quaternion quad,int index,GameObject* gObj);
	void ScaleMesh(Ogre::Vector3 scale,int index,GameObject* gObj);

	// Crowd Related methods
	dtCrowd *addNewCrowd(char *agentMaterial = "WhiteMaterial", float agentRadius = 2.0f, float agentHeight = 2.0f);

	// Agents Related methods
	dtTarget* addNewTarget(const float* hitpoint, Ogre::String behavior = "arrive");
	dtAIAgent* addNewAgent( const float* hitpoint, string meshName = "", string hkxName = "", string characterName = "", char* luaFile = "", Ogre::String behavior = "none", char* type = "GRAPHICS_OBJECT", GameObject* gObj=NULL);

	void setPursuitTarget(dtAIAgent* target);
	void setEvadeTarget(dtAIAgent* target);

	void setPursuitTarget(int iTarget);
	void setEvadeTarget(int iTarget);

	// Sets' and Gets'
	void setActiveCrowd(int crowdID) { this->mActiveCrowdId = crowdID % this->mCrowdsNo; }
	int getActiveCrowdId() { return this->mActiveCrowdId; }
	dtCrowd* getActiveCrowd(void) { return this->getCrowdHandler(mActiveCrowdId)->getCrowd(); }
	
	dtAIAgent* getActiveAgent(void) { return this->getCrowdHandler(this->mActiveCrowdId)->getCrowd()->getAgent(mActiveAgentId); }
	int getActiveAgentId(void) { return mActiveAgentId; }
	void setActiveAgent(int idx) { mActiveAgentId = idx; }
	void updateActiveAgentParams(dtCrowdAgentParams* params) 
	{ this->getActiveAgent()->updateAgentParameters(params); };
	void updateActiveSign(void);
	Ogre::SceneNode* getActiveSign(void) { return mActiveSign; }
	void setActiveSign(Ogre::SceneNode* activeSign) { mActiveSign = activeSign; }
	void setActiveAgentPosition(const float* pos);

	dtCrowdHandler* getCrowdHandler() { return this->mCrowdsHandler; }
	class InputGeom* getInputGeom() { return (this->mSample) ? this->mSample->getInputGeom() : NULL ; }

	dtCrowdHandler* getCrowdHandler(int crowdID);
	dtCrowd* getCrowd(int crowdID);
	float CalcAgentRadius(float* bmin, float* bmax, dtCrowdAgentParams* ap);
	void setBuildRadius(float radius) { m_agentRadius = radius;}

	Sample* getSample() { return this->mSample; }
	
	void updateAgentHeight(int m_agentID, int m_crowdID, bool checkAll);
	void updateDynamicObstacles();

	void setDebug(bool debug){ m_debug= debug;}

	void setDrawNaviMesh(bool flag){ m_drawNaviMesh = flag; }
	bool getDrawNaviMesh(void) { return m_drawNaviMesh; }

	std::string getNextAgentName(std::string prefix);

	int getCrowdsCount(void) { return mCrowdsNo; }
	Ogre::ManualObject* getManualObject(void) { return mManualObj; }

	// Set the parameters used to generate navigation mesh 
	void setNaviParameters( float cellSize = 0.3f, float cellHeight = 0.2f, float agentHeight = 2.0f, float agentRadius = 0.6f, 
		float agentMaxClimb = 0.9f, float agentMaxSlope = 45.0f, float regionMinSize = 8, float regionMergeSize = 20, bool monotonePartitioning = false, 
		float edgeMaxLen = 12.0f, float edgeMaxError = 1.3f, float vertsPerPoly = 6.0f, float detailSampleDist = 6.0f, float detailSampleMaxError = 1.0f);
	bool IsNavMeshBuilt() { return m_hasBuiltNaviMesh;}
};
