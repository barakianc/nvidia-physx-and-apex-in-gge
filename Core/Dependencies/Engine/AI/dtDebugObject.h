#ifndef DEBUG_OBJECT_H
#define DEBUG_OBJECT_H

#include "OgreString.h"
#include "OgreColourValue.h"

namespace Ogre{
    class SceneManager;
    class SceneNode;
}

   class dtDebugObject{
    public:
        Ogre::SceneNode *m_sceneNode;

        dtDebugObject(Ogre::SceneManager*);
        virtual ~dtDebugObject(){};
        virtual void initialize() = 0;
        virtual void update(float) = 0;
        virtual void attachTo(Ogre::SceneNode*);
        virtual void detach();
        virtual void setVisible(bool);

        virtual bool isVisible();
        virtual void setColour(Ogre::ColourValue) = 0;
    protected:
        Ogre::SceneNode *m_parent;
        Ogre::SceneManager *m_manager;
        bool m_visible;

        Ogre::ColourValue m_colour;

        static Ogre::String getUniqueId();
    private:
        static int m_uniqueId;
    };


#endif