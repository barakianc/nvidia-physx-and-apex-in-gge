#ifndef AIGroup_H
#define AIGroup_H

#include "Ogre.h"

#include <map>
#include <list>


namespace GamePipe
{
    class AIAgent;

};

class AIGroup
{
private:

    ///<summary>
    ///Hash Map that contains all the groups
    ///</summary>
    std::map<Ogre::String, std::list<GamePipe::AIAgent *>> m_all;

    Ogre::LogManager *m_log;


public:
    AIGroup();
    void addAgent(Ogre::String name,GamePipe::AIAgent *agent);
    std::list<GamePipe::AIAgent*> getAIGroup(Ogre::String name);
    void removeFromAIGroup(Ogre::String name,Ogre::String agentName);
};
#endif
