#include "StdAfx.h"
#include "AISteeringBehavior.h"
#include "dtAIAgent.h"
#include "OpenSteer/SimpleVehicle.h"
#include "OpenSteer/Utilities.h"
#include <ctime>
/*

extern OpenSteer::Vec3 gVel;
extern OpenSteer::Vec3 gForce;

extern OpenSteer::Vec3 gEVel;
extern OpenSteer::Vec3 gEPos;
*/

AISteeringBehavior::AISteeringBehavior(void)
{
	m_iFlags = 0;

	m_pPursuitTargetAgent = NULL;
	m_pEvadeTargetAgent = NULL;

	m_vSeekTarget = Ogre::Vector3::ZERO;
	m_vFleeTarget = Ogre::Vector3::ZERO;

	this->setMaxSpeed(8.0f);
	this->setMaxForce(50.0f);

	behaviorType["none"] = none;
	behaviorType["evade"] = evade;
	behaviorType["flee"] = flee;
	behaviorType["seek"] = seek;
	behaviorType["wander"] = wander;
	behaviorType["cohesion"] = cohesion;
	behaviorType["separation"] = separation;
	behaviorType["arrive"] = arrive;
	behaviorType["allignment"] = allignment;
	behaviorType["pursuit"] = pursuit;
	behaviorType["interpose"] = interpose;
	behaviorType["flock"] = flock;
	behaviorType["wall_avoidance"] = wall_avoidance;
	behaviorType["obstacle_avoidance"] = obstacle_avoidance;
	behaviorType["follow_path"] = follow_path;
	behaviorType["hide"] = hide;
	behaviorType["offset_pursuit"] = offset_pursuit;
}

AISteeringBehavior::~AISteeringBehavior(void)
{
}

bool AISteeringBehavior::On(behavior_type bt)
{
	int onFlag = (m_iFlags & bt);
	return onFlag == bt;
}
Ogre::Vector3 AISteeringBehavior::getSteeringForce( float deltaTime, OpenSteer::AVGroup group, int agentID )
{
	OpenSteer::Vec3 force (0.0f, 0.0f, 0.0f);

	float maxSpeed = this->maxSpeed();
	//srand((unsigned)time(NULL));
	
	if (this->isEvadeOn()){
		//Make sure the other AI is in sync with the OpenSteer world
		float* evadePosition = this->m_pEvadeTargetAgent->getAgentPosition();
		force += steerForEvasion(*((OpenSteer::SimpleVehicle_1*)this->m_pEvadeTargetAgent->m_AISteeringBehavior), 1.0f).truncateLength(maxSpeed);
/*
		OpenSteer::Vec3 ev(((OpenSteer::SimpleVehicle_1*)this->m_pEvadeTargetAgent->m_AISteeringBehavior)->velocity());
		gEVel.set(ev.x, ev.y, ev.z);
		OpenSteer::Vec3 ep(((OpenSteer::SimpleVehicle_1*)this->m_pEvadeTargetAgent->m_AISteeringBehavior)->position());
		gEPos.set(ep.x, ep.y, ep.z);
		*/
	}

	if(this->isWanderOn()){
		int pos[3];
		pos[0] = (int) (position().x * 3);
		pos[1] = (int) (position().y * 3);
		pos[2] = (int) (position().z * 3);

		//srand( (unsigned)time(NULL) + agentID + pos[0] + pos[1] + pos[2] );
		force += steerForWander(deltaTime).truncateLength(maxSpeed);
	}

	if (this->isCohesionOn())
	{
		force += steerForCohesion(1000, 0, group);
	}

	if (this->isAlignmentOn())
	{
		force += steerForAlignment(1000, 0, group);
	}

	if (this->isSeparationOn())
	{
		force += steerForSeparation(1000, 0, group);
	}

	if (this->isFleeOn())
	{
		force += steerForFlee(
			OpenSteer::Vec3(
			this->m_vFleeTarget.x,
			this->m_vFleeTarget.y,
			this->m_vFleeTarget.z)).truncateLength(maxSpeed);
	}

	//force.y = 0.f;

	//applySteeringForce(force, deltaTime);
	smoothSpeed(force, deltaTime);

	if(force == OpenSteer::Vec3::zero)
		applyBrakingForce(0.75f, deltaTime);
/*

	gVel.set(this->velocity().x, this->velocity().y, this->velocity().z);
	gForce.set(force.x, force.y, force.z);
*/

	return Ogre::Vector3(this->velocity().x, this->velocity().y, this->velocity().z);
}

void AISteeringBehavior::smoothSpeed(const OpenSteer::Vec3& force, const float elapsedTime)
{
	const OpenSteer::Vec3 adjustedForce = adjustRawSteeringForce (force, elapsedTime);

	// enforce limit on magnitude of steering force
	const OpenSteer::Vec3 clippedForce = adjustedForce.truncateLength (maxForce ());

	// compute acceleration and velocity
	OpenSteer::Vec3 newAcceleration = (clippedForce / mass());
	OpenSteer::Vec3 newVelocity = velocity();

	// damp out abrupt changes and oscillations in steering acceleration
	// (rate is proportional to time step, then clipped into useful range)
	if (elapsedTime > 0)
	{
		const float smoothRate = OpenSteer::clip(9 * elapsedTime, 0.15f, 0.4f);
		OpenSteer::Vec3 smoothedAcceleration(smoothedAcceleration());
		OpenSteer::blendIntoAccumulator (smoothRate, newAcceleration, smoothedAcceleration);
		resetSmoothedAcceleration(smoothedAcceleration);
	}

	// Euler integrate (per frame) acceleration into velocity
	newVelocity += smoothedAcceleration() * elapsedTime;

	// enforce speed limit
	newVelocity = newVelocity.truncateLength (maxSpeed ());

	// update Speed
	((OpenSteer::SimpleVehicle*)this)->setSpeed(newVelocity.length());

	// regenerate local space (by default: align vehicle's forward axis with
	// new velocity, but this behavior may be overridden by derived classes.)
	regenerateLocalSpace (newVelocity, elapsedTime);
}

// ----------------------------------------------------------------------------
// apply a given steering force to our momentum,
// adjusting our orientation to maintain velocity-alignment.

void AISteeringBehavior::updatePosition (const float x, const float y, const float z, const float elapsedTime)
{
	OpenSteer::Vec3 pos(x,y,z);
	// Euler integrate (per frame) velocity into position
	setPosition (pos/*position() + (this->velocity() * elapsedTime)*/);

	// maintain path curvature information
	measurePathCurvature (elapsedTime);

	// running average of recent positions
	OpenSteer::Vec3 smoothedPosition(smoothedPosition());
	OpenSteer::blendIntoAccumulator (elapsedTime * 0.06f, pos, smoothedPosition);
	resetSmoothedPosition(smoothedPosition);
}

void AISteeringBehavior::setSpeed(float speedX, float speedY, float speedZ, float dt)
{
	OpenSteer::Vec3 osSpeed (speedX, speedY, speedZ);
	osSpeed.truncateLength(((OpenSteer::SimpleVehicle*)this)->maxSpeed());
	((OpenSteer::SimpleVehicle*)this)->setSpeed(osSpeed.length());	
	
	this->regenerateLocalSpace(osSpeed, dt);
}

void AISteeringBehavior::setSpeed(Ogre::Vector3 speed, float dt)
{
	this->setSpeed(speed.x, speed.y, speed.z, dt);
}

void AISteeringBehavior::setSpeed(float *speed, float dt)
{
	this->setSpeed(speed[0], speed[1], speed[2], dt);
}
