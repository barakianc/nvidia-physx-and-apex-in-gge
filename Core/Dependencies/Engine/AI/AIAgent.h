#ifndef AIAGENT_H
#define AIAGENT_H

#define lua_bindAI(functionPointer, functionName, luaVM) \
    lua_pushlightuserdata(luaVM, this);\
    lua_pushcclosure(luaVM, functionPointer, 1);\
    lua_setglobal(luaVM, functionName);

#define lua_unBindAI(functionName, luaVM) \
    lua_pushnil(luaVM); \
    lua_setglobal(luaVM, functionName);

// Ogre
#include "OgreVector3.h"
#include "OgrePrerequisites.h"
#include "OgreString.h"

#include "AIGroup.h"

#include "OpenSteer\SimpleVehicle.h"

// Std
#include <list>
#include <vector>

class PhysicsObject;
class CollisionListener;
class TextFileManager;
class GraphicsObject;
struct lua_State;

namespace Ogre{
    class SceneNode;
    class LogManager;
    class Entity;
}

namespace AnimationManager{
    class AnimatedEntity;
}

namespace GamePipe
{
    class DebugObject;
    class AIAgentManager;
    class SteeringBehavior;

    /// <summary>
    /// AI Agent class.  Represents an AI which is binded to Lua, Havok, and
    /// Havok/Ogre animations.
    /// </summary>
    /// <author>David Young dayoung@goliathdesigns.com</author>
    class AIAgent : public OpenSteer::SimpleVehicle
    {
    public:
        friend class AIAgentManager;
        /// <summary>Graphics rendering object, may be null.</summary>
        GraphicsObject *m_graphicsObject;
        /// <summary>Physics object, may be null.</summary>
        PhysicsObject *m_physicsObject;
        /// <summary>Animation object, may be null.</summary>
        AnimationManager::AnimatedEntity *m_animationEntity;

        /// <summary>Phantom Shape object, may be null.</summary>
        //PhantomAabbObject *m_phantomAabbObject;

        /// <summary>Collision Listener, may be null.</summary>
        CollisionListener *m_collisionListener;

        /// <summary>Steering Behavior</summary>
        SteeringBehavior *m_steeringBehavior;

        /// <summary>
        /// The AIAgentManager the agent belongs to.  This may be null if the
        /// AI is not part of any AIAgentManager.
        /// This is only set by the AIAgentManager when the AI is added to it.
        /// </summary>
        AIAgentManager *m_aiAgentManger;

        /// <summary>
        /// Velocity of the AI Agent.
        /// </summary>
        Ogre::Vector3 m_velocity;

        /// <summary>
        /// Heading of the AI Agent.
        /// </summary>
        Ogre::Vector3 m_vHeading;

        /// <summary>
        /// Side Component of the AI Agent.
        /// </summary>
        Ogre::Vector3 m_vSide;

        /// <summary>
        /// Time Elapsed each update func call of the AI Agent.
        /// </summary>
        double m_dTimeElapsed;

        //the maximum force this entity can produce to power itself
        //(think rockets and thrust)
        double        m_dMaxForce;

        //the maximum rate (radians per second)this vehicle can rotate
        double       m_dMaxTurnRate;

        /// <summary>
        /// AI Agent constructor.
        /// </summary>
        /// <param name='fileName'>Lua script file</param>
        /// <param name='graphics'>Ogre renderable graphics object</param>
        /// <param name='havok'>Havok physics object</param>
        /// <param name='animation'>Generic animation object</param>
        AIAgent(Ogre::String, GraphicsObject*, PhysicsObject*, AnimationManager::AnimatedEntity*);

        /// <summary>
        /// AIAgent destructor.
        /// </summary>
        virtual ~AIAgent();

        void addDestination(Ogre::Vector3);

        /// <summary>
        /// Adds a type to the current AI's type.
        /// </summary>
        /// <param name='type'>Represents a number from 1 to 64 specifying which
        /// bit the new type is.
        /// </param>
        void addType(int);

        /// <summary>
        /// Add multiple types at once to the AI's type.
        /// </summary>
        /// <param name='types'>Multiple types indicated by 1 bits.</param>
        void addTypes(unsigned long);

        /// <summary>
        /// Returns the destination the AIAgent is attempting to reach.
        /// </summary>
        Ogre::Vector3 getDestination();

        /// <summary>
        /// Returns the known Lua stack size.
        /// </summary>
        /// <returns>Lua stack size.</returns>
        int getKnownLuaStackSize();

        /// <summary>
        /// Returns the max speed of the agent.
        /// </summary>
        /// <returns>Speed of the agent.</returns>
        Ogre::Real getMaxLinearSpeed();

        /// <summary>
        /// Returns the name of the agent.
        /// </summary>
        /// <returns>Name of the agent.</returns>
        Ogre::String getName();

        /// <summary>
        /// Returns the position of the AIAgent.
        /// </summary>
        /// <returns>Position of the agent.</returns>
        Ogre::Vector3 getPosition();

        /// <summary>
        /// Returns a unsigned long representing 64 possible types the AI may be
        /// unsigned long to.  Each toggled bit of the unsigned long represent
        /// a different type.
        /// </summary>
        /// <returns>Types of the AI.</returns>
        unsigned long getType();

        /// <summary>
        /// Convenience method to return a vector of ints representing each of
        /// the types the AI beunsigned longs to.
        /// <returns>
        /// Vector of ints ranging from 1 to 64, otherwise a vector containing
        /// the int 0 representing that the AI is not part of any type.
        /// </returns>
        std::vector<int> getTypes();

        /// <summary>
        /// Returns the AI's current velocity.
        /// </summary>
        /// <returns>Current velocity</returns>
        Ogre::Vector3 getVelocity();

        /// <summary>
        /// Initialize the AIAgent, called when the scene is initialized.
        /// </summary>
        void initialize();

        /// <summary>
        /// Initialize the Lua stack.
        /// </summary>
        void initializeLua();

        /// <summary>
        /// Return whether debug information is being shown.
        /// </summary>
        /// <returns>Debug toggle.</returns>
        bool isDebug();

        /// <summary>
        /// Reloads the lua stack based on the lua filename.
        /// </summary>
        void reloadLua();

        /// <summary>
        /// Sets the position the AIAgent will attempt to reach.
        /// </summary>
        void setDestination(Ogre::Vector3);

        /// <summary>
        /// Set whether to show debug information about the AI Agent.
        /// </summary>
        /// <param name='enable'>Enable or disable debug information.</param>
        void setDebug(bool);

        /// <summary>
        /// Sets the debug text that hovers above the AI Agent.
        /// </summary>
        /// <param name='text'>Text to set.</param>
        void setDebugText(Ogre::String);

        /// <summary>
        /// Enable or disable lua support.
        /// </summary>
        /// <param name='enable'>Whether to enable lua support.</param>
        void setEnableLua(bool);

        /// <summary>
        /// Sets the max linear speed the AI Agent may move at.
        /// </summary>
        void setMaxLinearSpeed(Ogre::Real);

        /// <summary>
        /// Sets the AI name.
        /// </summary>
        void setName(Ogre::String);

        /// <summary>
        /// Overrides the current type of the AI.
        /// </summary>
        /// <param name='type'>
        /// Set the AI type based on a number from 1 to 64.
        /// </param>
        void setType(int);

        /// <summary>
        /// Overrides the current types of the AI.
        /// </summary>
        /// <param name='types'>
        /// Set the AI type based on toggled bits on the param.
        /// </param>
        void setTypes(unsigned long);

        /// <summary>
        /// Update loop called during each update loop.
        /// </summary>
        void update(const float, const float);

        /// <summary>
        /// Set the default animation
        /// </summary>
        /// <param name = 'defaultAnimName'>the name of the animation want to play</param>
        bool setDefautAnimation(Ogre::String defaultAnimName);

        /// <summary>
        /// Get the name of the default animation
        /// </summary>
        Ogre::String getDefaultAnimation();

        /// <summary>
        /// Play the current animation.
        /// </summary>
        /// <param name = 'newAnimName'>the name of the animation</param>
        /// <param name = 'loop'>whether the animation play once or forever</param>
        /// <param name = 'transitionAnimation'>set the transition animation between the active one and the new animation</param>
        bool playAnimation(Ogre::String newAnimName,bool loop,Ogre::String transitionAnimation = "");

        /// <summary>
        /// stop the current animation
        /// </summary>
        bool stopAnimation();

        /// <summary>
        /// return names of all the animations this agent has
        /// </summary>
        std::vector<std::string> getAnimations();

        /// <summary>
        /// set the default number of frames used when blending animations
        /// </summary>
        /// <param name = 'blendFrame'>the number user want to set</param>
        bool setBlendFrame(float blendFrame);

        /// <summary>
        ///  return the default number of frames used when blending animations
        /// </summary>
        float getBlendFrame();

        /// <summary>
        /// get current active animation
        /// </summary>
        /// <returns>Returns the name of the active animation.</returns>
        Ogre::String getActiveAnimation();

        /// <summary>
        /// Sets the Seek Target
        /// </summary>
        /// <author>hrudesh.doke@gmail.com</author>
        void setSeekTarget(Ogre::Vector3 seekTarget);

        /// <summary>
        /// Sets the Flee Target
        /// </summary>
        void setFleeTarget(Ogre::Vector3 fleeTarget);

        /// <summary>
        /// Sets the Arrive Target
        /// </summary>
        void setArriveTarget(Ogre::Vector3 arriveTarget);

        /// <summary>
        /// Switching ON the behaviors
        /// </summary>
        void setOnBehaviors(Ogre::String name);

        OpenSteer::AVGroup *neighbors;
        std::vector<Ogre::Entity*> *obstacles;

    protected:
        /// <summary>
        /// The AIAgentManager the agent belongs to.  This may be null if the
        /// AI is not part of any AIAgentManager.
        /// This is only set by the AIAgentManager when the AI is added to it.
        /// </summary>
       // AIAgentManager *m_aiAgentManger;

        /// <summary>
        /// Whether debugging is enabled or disabled for this object.
        /// </summary>
        bool m_debug;

        /// <summary>
        /// Debug graphic objects.
        /// </summary>
        std::list<DebugObject*> *m_debugObjects;

        /// <summary>
        /// Nodes used for debugging purposes only.
        /// </summary>
        std::list<Ogre::SceneNode*> *m_debugPathNodes;
        std::list<DebugObject*> *m_debugPathObjects;

        Ogre::SceneNode* m_debugDestNode;
        DebugObject *m_text, *m_debugDest, *m_debugSelf;

        /// <summary>
        /// Destination the AI is attempting to reach, may be the AI's current
        /// position if the AI has no current destination.
        /// </summary>
        Ogre::Vector3 m_destination;

        Ogre::LogManager *m_log;

        /// <summary>
        /// Filename of the lua script.
        /// </summary>
        Ogre::String m_luaFileName;

        /// <summary>
        /// Lua state, represents the interface between this AI Agent and Lua.
        /// </summary>
        lua_State* m_luaVM;

        bool m_enableLua;

        bool m_luaInitilize;

        int m_luaStackSize;

        Ogre::Real m_maxSpeed;

        /// <summary>
        /// Name of the AI Agent.
        /// </summary>
        Ogre::String m_name;

        /// <summary>
        /// Position of the AI, updated every update call.  Based off of physics
        /// if present, otherwise based off the graphics if present, otherwise
        /// kept internally by the AI Agent.
        /// </summary>
        Ogre::Vector3 m_position;

        /// <summary>
        /// Current state the AI is in.
        /// </summary>
        Ogre::String m_state;
        /// <summary>
        /// Type/Types which this AI Agent is a part of.  Each toggled bit
        /// represents a different type the AI is.  The maximum
        /// </summary>
        unsigned long m_type;
    private:
        /// <summary>
        /// Text file manager which is used to load in Lua scripts.
        /// </summary>
        static TextFileManager *m_tfm;

        /// <summary>
        ///  this member stores the direction the AIAgent is moving to now
        /// </summary>
        Ogre::Vector3 m_moveTo;

        /// <summary>
        /// Binds the AI to the Lua stack.
        /// </summary>
        void bindAI();

        /// <summary>
        /// The number of frames used when blending the animation.
        /// </summary>
        float m_blendFrame;

        /// <summary>
        /// Flag to remove the AI.  The AI stops updating if true.
        /// </summary>
        bool m_remove;
    };
};

#endif