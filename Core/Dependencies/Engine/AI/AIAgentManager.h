#ifndef AIAGENT_MANAGER_H
#define AIAGENT_MANAGER_H

#include <list>
#include <map>
#include <vector>

#include "Ogre.h"
#include "OgreString.h"
#include "OgreVector3.h"
#include "OgrePrerequisites.h"

#include "tbb\blocked_range.h"
#include "AIAgent.h"
#include "dtAIAgent.h"
#include "DetourCrowd.h"

struct lua_State;

namespace GamePipe
{
    //class AIAgent;
    class GridWorld;
    class GraphWorld;

    static int l_searchByType(lua_State*);
    static int l_searchByTypeAndLimit(lua_State*);
    static int l_searchByLimit(lua_State*);

    static void g_agentManager_debug( std::vector<Ogre::String>& );
    static void g_agentManager_reload( std::vector<Ogre::String>& );

    class AIAgentManagerUpdate{
    public:
        float m_totalTime, m_deltaTime;
        AIAgentManagerUpdate(float totalTime, float deltaTime){
            m_totalTime= totalTime;
            m_deltaTime = deltaTime;
        }
        void operator()( AIAgent* const r ) const {
                r->update(m_totalTime, m_deltaTime);
        }
    };

    class AIAgentManager{
    public:
        static const unsigned long MAX_TYPES = 64;

        std::list<AIAgent*> m_all_agents;
		std::list<dtAIAgent*> m_all_dtAgents;

        AIAgentManager();
        ~AIAgentManager();

        void addAgent(AIAgent*);
		void addDtAgent(dtAIAgent*);
        AIAgent* getAgent(Ogre::String);
        std::list<AIAgent*>::iterator getAgentIterator();

        std::vector<int> getTypes();

        void initialize();
        bool isDebug();

        bool reassignAgentType(AIAgent*, unsigned long);

        void reloadLua();

        bool removeAgent(Ogre::String);

        std::list<AIAgent*> searchByType(unsigned long, bool = false);
        std::list<AIAgent*> searchByTypeAndLimit(unsigned long, Ogre::Vector3, Ogre::Real, bool = false);
        std::list<AIAgent*> searchByLimit(Ogre::Vector3, Ogre::Real);

        void setDebug(bool);

        void setWorld(GraphWorld*);
        void setWorld(GridWorld*);

        void update(float);

    protected:
        GridWorld *m_gridWorld;
        GraphWorld *m_graphWorld;

        std::map<unsigned long, std::list<AIAgent*>> m_agents_list;
        bool m_debug;

        void bindAI(AIAgent*);
        void unbindAI(AIAgent*);
    private:
        Ogre::Vector3 m_position;
        std::vector<int> m_types;

        bool removeAgentFromList(std::list<AIAgent*>,AIAgent*);
        void sortByDistance(std::vector<AIAgent*>&,Ogre::Vector3);
    };
};

#endif