#include "StdAfx.h"

#include "AIAgentManager.h"

#include "World/GridWorld.h"
#include "World/GraphWorld.h"
#include "lua.hpp"
#include "AIAgent.h"

#include "GameScreen.h"
#include "Engine.h"
#include "GameObjectManager.h"

#include "OgreLog.h"
#include "OgreLogManager.h"
#include "OgreResourceGroupManager.h"

#include "tbb/parallel_for.h"
#include "tbb/parallel_for_each.h"

using namespace GamePipe;

static void GamePipe::g_agentManager_debug( std::vector<Ogre::String>& commands )
{
    GameScreen *screen = EnginePtr->GetForemostGameScreen();
    if (screen){
        GameObjectManager *objectManager = screen->GetGameObjectManager();
        if (objectManager){
            AIAgentManager *aiManager = objectManager->getAIAgentManager();

            if(aiManager){
                if(commands.size() > 1){
                    if (commands[1] == "true"){
                        aiManager->setDebug(true);
                    }
                    else if (commands[1] == "false"){
                        aiManager->setDebug(false);
                    }
                }
            }
        }
    }
};

void GamePipe::g_agentManager_reload( std::vector<Ogre::String>& )
{
    GameScreen *screen = EnginePtr->GetForemostGameScreen();
    if (screen){
        GameObjectManager *objectManager = screen->GetGameObjectManager();
        if (objectManager){
			AIAgentManager *aiManager = objectManager->getAIAgentManager();
            if(aiManager){
				aiManager->reloadLua();
            }
        }
    }
};

static int GamePipe::l_searchByType(lua_State* luaVM)
{
    AIAgentManager* manager = (AIAgentManager*)lua_topointer(luaVM,lua_upvalueindex(1));

    const char *types = lua_tostring(luaVM, -1);
    unsigned long type = strtol(types, NULL, 0);

    std::list<AIAgent*> agents = manager->searchByType(type);

    lua_createtable(luaVM, agents.size(), 0);
    int tableIndex = lua_gettop(luaVM);

    unsigned int i = 1;

    for(std::list<AIAgent*>::iterator it = agents.begin(); it != agents.end(); it++, i++){
        lua_pushlightuserdata(luaVM, (*it));
        lua_rawseti(luaVM, tableIndex, i);
    }

    return 1;
};

static int GamePipe::l_searchByTypeAndLimit(lua_State* luaVM)
{
    AIAgentManager* manager = (AIAgentManager*)lua_topointer(luaVM,lua_upvalueindex(1));

    const char *types = lua_tostring(luaVM, -5);
    unsigned long type = strtol(types, NULL, 0);

    Ogre::Vector3 position = Ogre::Vector3((float)lua_tonumber(luaVM, -4),
        (float)lua_tonumber(luaVM, -3),
        (float)lua_tonumber(luaVM, -2));

    Ogre::Real limit = (float)lua_tonumber(luaVM, -1);

    std::list<AIAgent*> agents = manager->searchByTypeAndLimit(type, position, limit);

    lua_createtable(luaVM, agents.size(), 0);
    int tableIndex = lua_gettop(luaVM);

    unsigned int i = 1;

    for(std::list<AIAgent*>::iterator it = agents.begin(); it != agents.end(); it++, i++){
        lua_pushlightuserdata(luaVM, (*it));
        lua_rawseti(luaVM, tableIndex, i);
    }

    return 1;
};

static int GamePipe::l_searchByLimit(lua_State* luaVM)
{
    AIAgentManager* manager = (AIAgentManager*)lua_topointer(luaVM,lua_upvalueindex(1));

    Ogre::Vector3 position = Ogre::Vector3((float)lua_tonumber(luaVM, -4),
        (float)lua_tonumber(luaVM, -3),
        (float)lua_tonumber(luaVM, -2));

    Ogre::Real limit = (float)lua_tonumber(luaVM, -1);

    std::list<AIAgent*> agents = manager->searchByLimit(position, limit);

    lua_createtable(luaVM, agents.size(), 0);
    int tableIndex = lua_gettop(luaVM);

    unsigned int i = 1;

    for(std::list<AIAgent*>::iterator it = agents.begin(); it != agents.end(); it++, i++){
        lua_pushlightuserdata(luaVM, (*it));
        lua_rawseti(luaVM, tableIndex, i);
    }

    return 1;
};

AIAgentManager::AIAgentManager()
{
    m_position = Ogre::Vector3();
    m_gridWorld = 0;
    m_graphWorld = 0;
};

void AIAgentManager::addAgent(AIAgent *agent)
{
    unsigned long types = agent->getType();

    m_all_agents.push_back(agent);

    //Add the agent to each type list it belongs to.
    for (int i = 1; i <= AIAgentManager::MAX_TYPES; i++){
        if (types & 0x01){
            m_agents_list[i].push_back(agent);
        }
        types >>= 1;
    }

    agent->m_aiAgentManger = this;
    if (agent->m_luaFileName != ""){
        bindAI(agent);
    }
};

void AIAgentManager::addDtAgent(dtAIAgent *agent)
{
    m_all_dtAgents.push_back(agent);
};

void AIAgentManager::bindAI(AIAgent* agent)
{
    lua_bindAI(&GamePipe::l_searchByType, "searchByType", agent->m_luaVM);
    lua_bindAI(&GamePipe::l_searchByTypeAndLimit, "searchByTypeAndLimit", agent->m_luaVM);
    lua_bindAI(&GamePipe::l_searchByLimit, "searchByLimit", agent->m_luaVM);
};

std::list<AIAgent*>::iterator AIAgentManager::getAgentIterator()
{
    return m_all_agents.begin();
};

std::vector<int> AIAgentManager::getTypes()
{
    unsigned long types = 0;

    std::map<unsigned long, std::list<AIAgent*>>::iterator it;

    for (it = m_agents_list.begin(); it != m_agents_list.end(); it++){
        types |= (*it).first;
    }

    std::vector<int> type_list;

    for (int i = 1; i <= AIAgentManager::MAX_TYPES; i++){
        if (types & 0x01){
            type_list.push_back(i);
        }
        types >>= 1;
    }

    return type_list;
};

void AIAgentManager::initialize()
{
    m_all_agents = std::list<AIAgent*>();
    m_agents_list = std::map<unsigned long, std::list<AIAgent*>>();

    for (int i = 1; i <= AIAgentManager::MAX_TYPES; i++){
        m_agents_list[i] = std::list<AIAgent*>();
    }

    OgreConsole::getSingletonPtr()->addCommand("ai_set_debug", &GamePipe::g_agentManager_debug);
    OgreConsole::getSingletonPtr()->addCommand("ai_reload", &GamePipe::g_agentManager_reload);
};

bool AIAgentManager::isDebug()
{
    return m_debug;
};

bool AIAgentManager::reassignAgentType(AIAgent* agent, unsigned long type)
{
    bool found = false;
    found = removeAgent(agent->getName());
    if (found){
        agent->setType(type);
        addAgent(agent);
    }
    return found;
};

void AIAgentManager::reloadLua()
{
    Ogre::LogManager::getSingleton().logMessage("LUA INFO: Reloading lua files.",
        Ogre::LML_NORMAL);

    if(Ogre::ResourceGroupManager::getSingletonPtr()->isResourceGroupInGlobalPool("LUA")){
        Ogre::ResourceGroupManager::getSingletonPtr()->unloadResourceGroup("LUA");
        Ogre::ResourceGroupManager::getSingletonPtr()->clearResourceGroup("LUA");
        Ogre::ResourceGroupManager::getSingletonPtr()->destroyResourceGroup("LUA");
    }

    Ogre::LogManager::getSingleton().logMessage("LUA INFO: Removing LUA resources.",
        Ogre::LML_NORMAL);

    for(std::list<AIAgent*>::iterator agent = m_all_agents.begin(); agent != m_all_agents.end(); agent++){
        (*agent)->reloadLua();
    }

	for(std::list<dtAIAgent*>::iterator agent = m_all_dtAgents.begin(); agent != m_all_dtAgents.end(); agent++){
        (*agent)->reloadLua();
    }
};

bool AIAgentManager::removeAgent(Ogre::String name)
{
    AIAgent *agent = 0;
    std::list<AIAgent*>::iterator it;
    bool found = false;

    //Remove the agent from the all agent list.
    for (it = m_all_agents.begin(); it != m_all_agents.end(); it++){
        if ((*it)->getName().compare(name)){
            agent = (*it);
            break;
        }
    }

    //See if the AI was found, if found remove from the list and map.
    if (agent){
        m_all_agents.erase(it);

        unsigned long types = agent->getType();

        //Search for a list of agents over all known agent types.
        for (int i = 1; i <= AIAgentManager::MAX_TYPES; i++){
            if (types & 0x01){
                std::map<unsigned long, std::list<AIAgent*>>::iterator mapIt = m_agents_list.find(agent->getType());

                //See if a list of this agent type exists.
                if(mapIt != m_agents_list.end()){
                    std::list<AIAgent*> listIt = mapIt->second;

                    //Search the list for the specific AI.
                    for (it = listIt.begin(); it != listIt.end(); it++){
                        if ((*it)->getName().compare(name)){
                            break;
                        }
                    }

                    //See if the AI was found.
                    if (it != listIt.end()){
                        listIt.erase(it);
                        found = true;
                        //Remove the AI Type list if it is empty.
                        if (listIt.size() <= 0){
                            m_agents_list.erase(mapIt);
                        }
                    }
                }
            }
            //Search for next AI type.
            types >>= 1;
        }
    }

    agent->m_aiAgentManger = NULL;
    unbindAI(agent);

    return found;
};

std::list<AIAgent*> AIAgentManager::searchByLimit(Ogre::Vector3 position, Ogre::Real distance)
{
    std::vector<AIAgent*> distanceAgents;
    Ogre::Real squareDistance = distance * distance;

    if(m_all_agents.size() > 0){
        for(std::list<AIAgent*>::iterator it = m_all_agents.begin(); it != m_all_agents.end(); it++){
            if ((*it)->getPosition().squaredDistance(position) <= squareDistance ){
                distanceAgents.push_back(*it);
            }
        }
        sortByDistance(distanceAgents,position);
        return std::list<AIAgent*>(distanceAgents.begin(), distanceAgents.end());
    }
    return std::list<AIAgent*>();
};

std::list<AIAgent*> AIAgentManager::searchByType(unsigned long type, bool reverse)
{
    std::list<AIAgent*> agents;
    std::map<unsigned long, std::list<AIAgent*>>::iterator typeIt = m_agents_list.find(type);

    if (!reverse){
        if(typeIt != m_agents_list.end()){
            agents = std::list<AIAgent*>(typeIt->second);
        }
    }
    else {
        std::map<unsigned long, std::list<AIAgent*>>::iterator otherIt = m_agents_list.begin();
        for (;otherIt != m_agents_list.end(); otherIt++){
            if (otherIt != typeIt){
                agents.insert(agents.begin(),
                    otherIt->second.begin(),
                    otherIt->second.end());
            }
        }
    }

    return agents;
};

std::list<AIAgent*> AIAgentManager::searchByTypeAndLimit(unsigned long type, Ogre::Vector3 position, Ogre::Real distance, bool reverse){
    std::list<AIAgent*> typeAgents = searchByType(type, reverse);

    Ogre::Real squareDistance = distance * distance;

    if(typeAgents.size() > 0){
        std::vector<AIAgent*> distanceAgents;

        for(std::list<AIAgent*>::iterator it = typeAgents.begin(); it != typeAgents.end(); it++){
            if ((*it)->getPosition().squaredDistance(position) <= squareDistance ){
                distanceAgents.push_back(*it);
            }
        }

        sortByDistance(distanceAgents, position);

        return std::list<AIAgent*>(distanceAgents.begin(), distanceAgents.end());
    }
    return typeAgents;
};

void AIAgentManager::setDebug( bool debug )
{
    m_debug = debug;

    for(std::list<AIAgent*>::iterator it = m_all_agents.begin();
        it != m_all_agents.end(); it++){
            (*it)->setDebug(debug);
    }

    if (m_gridWorld){
        m_gridWorld->setDebug(debug);
    }
    if (m_graphWorld){
        m_graphWorld->setDebug(debug);
    }
};

void AIAgentManager::setWorld( GraphWorld* graphWorld)
{
    m_graphWorld = graphWorld;
};

void AIAgentManager::setWorld( GridWorld* gridWorld)
{
    m_gridWorld = gridWorld;
};

void AIAgentManager::sortByDistance(std::vector<AIAgent*> &agents, Ogre::Vector3 position){
    unsigned int left, right, i = 1, j, k;

    unsigned int tempSize = agents.size()/2;
    unsigned int tempSpace = 0x80000000;

    //impossible for the first bit to be half the size of agents.
    tempSpace <<= 1;

    //temp space will be the next 2^n above half the agent size.
    while (!(tempSize & 0x80000000)){
        tempSize <<= 1;
        tempSpace >>= 1;
    }

    //temp space used in merge sort.
    AIAgent** tempAgents = new AIAgent*[tempSpace];

    if (agents.size() >= 1){
        //sorts in blocks starting from 1, 2, 4, 8, 16, etc.
        for(i = 1; i <= agents.size()/2; i *= 2){
            //sort each block .
            for (j = 0; j < agents.size(); j += 2*i){
                //split each block into two.
                left = j;
                right = j + i;

                //merge both sorted blocks into a temp array.
                for(k = 0; k < 2*i; k++){
                    if(left < j+1 && (right > agents.size() ||
                        agents[left]->getPosition().squaredDistance(position) <
                        agents[right]->getPosition().squaredDistance(position))){
                            tempAgents[k] = agents[left];
                            left++;
                    }
                    else {
                        tempAgents[k] = agents[right];
                        right++;
                    }
                }

                //copy the pointers from the temp array to the vector.
                for(k = 0; k < (left + right - 1); k++){
                    agents[j+k] = tempAgents[k];
                }
            }
        }
    }

    delete [] tempAgents;
};

void AIAgentManager::unbindAI(AIAgent* agent)
{
    lua_unBindAI("searchByType", agent->m_luaVM);
    lua_unBindAI("searchByTypeAndLimit", agent->m_luaVM);
    lua_unBindAI("searchByLimit", agent->m_luaVM);
};
void AIAgentManager::update(float deltaTime)
{
    const float totalTime = EnginePtr->GetTotalGameTime();

    std::list<AIAgent*>::iterator agent = m_all_agents.begin();

    AIAgentManagerUpdate updateManager(totalTime, deltaTime);
    tbb::parallel_for_each(m_all_agents.begin(), m_all_agents.end(), updateManager);
};