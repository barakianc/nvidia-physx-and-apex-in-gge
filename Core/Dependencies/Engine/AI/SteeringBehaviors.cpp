#include "StdAfx.h"
#include "SteeringBehaviors.h"

using namespace GamePipe;

SteeringBehavior::SteeringBehavior(AIAgent *agent)
{
	m_pVehicle = agent;
	m_iFlags = 0;

    m_pTargetAgent1 = NULL;
    m_pTargetAgent2 = NULL;

    behaviorType["evade"] = evade;
	behaviorType["flee"] = flee;
	behaviorType["seek"] = seek;
	behaviorType["wander"] = wander;
	behaviorType["cohesion"] = cohesion;
	behaviorType["separation"] = separation;
	behaviorType["arrive"] = arrive;
	behaviorType["allignment"] = allignment;
	behaviorType["pursuit"] = pursuit;
	behaviorType["interpose"] = interpose;
	behaviorType["flock"] = flock;
	behaviorType["wall_avoidance"] = wall_avoidance;
	behaviorType["obstacle_avoidance"] = obstacle_avoidance;
}

void SteeringBehavior::setParams(Ogre::String name,Ogre::Real value)
{
	params[name] = value;	
}

Ogre::Real SteeringBehavior::getParams(Ogre::String name)
{
	return params[name];
}

SteeringBehavior::~SteeringBehavior()
{

}
