#include "StdAfx.h"
#include "dtDebugObject.h"
#include "OgreSceneManager.h"
#include "OgreSceneNode.h"


int dtDebugObject::m_uniqueId = 0;

Ogre::String dtDebugObject::getUniqueId(){
    char buff[sizeof(m_uniqueId)*8+1];
    _itoa_s(m_uniqueId,buff,10);

    m_uniqueId++;

    Ogre::String num = buff;

    return num;
};

dtDebugObject::dtDebugObject(Ogre::SceneManager* manager): m_manager(manager), m_sceneNode(0), 
m_parent(0), m_visible(true){
    if (m_manager){
        m_sceneNode = m_manager->getRootSceneNode()->createChildSceneNode();
    }

    m_colour = Ogre::ColourValue();
};

void dtDebugObject::attachTo(Ogre::SceneNode* parent){
    m_parent = parent;
};

void dtDebugObject::detach(){
    if (m_parent){
        m_parent = 0;
    }
};

void dtDebugObject::setVisible(bool visible){
    m_visible = visible;
};

bool dtDebugObject::isVisible(){
    return m_visible;
};