#ifndef DEBUG_CIRCLE_2D_H
#define DEBUG_CIRCLE_2D_H

#include "ogre.h"
#include "DebugObject.h"

using namespace GamePipe;

static int m_uniqueId = 0;

namespace GamePipe{
    class DebugCircle2D: public DebugObject{
    public:
        DebugCircle2D(Ogre::SceneManager*);

        ///<summary>
        ///Create a one pixel thick white circle object based radius not attached to
        ///any object.
        ///</summary>
        ///<param name='agent'>AI Agent used for graphics creation.</param>
        ///<param name='radius'>Radius of the circle.</param>
        DebugCircle2D(Ogre::SceneManager*, float);

        ///<summary>
        ///Creates a white circle object based radius and thickness not attached to
        ///any object.
        ///</summary>
        ///<param name='agent'>AI Agent used for graphics creation.</param>
        ///<param name='radius'>Radius of the circle.</param>
        ///<param name='thickness'>Thickness of the circle.</param>
        DebugCircle2D(Ogre::SceneManager*, float, float);
        virtual ~DebugCircle2D();
        virtual void initialize();
        virtual void setColour(Ogre::ColourValue);
        virtual void setVisible(bool);
        virtual void update(float);
        float getRadius() const { return radius; }
        float getThickness() const { return thickness; }
    protected:
        Ogre::MovableObject *m_model;
        Ogre::ManualObject *temp_model;

        Ogre::ColourValue m_colour;

        float radius;
        float thickness;

        void initializeSize();
    private:
        ///<summary>
        ///Accuracy of circle representation.
        ///Higher values generate better representations of a circle but create
        ///more polygons or line segments.
        ///</summary>
        static const float ACCURACY;
    };
};

#endif