#include "StdAfx.h"
#include "DebugText2D.h"

#include "OgreFontManager.h"
#include "OgreSceneManager.h"
#include "OgreSceneNode.h"
#include "OgreMovableObject.h"
#include "OgreCamera.h"
#include "OgreOverlay.h"
#include "OgreOverlayManager.h"
#include "OgreOverlayElement.h"
#include "OgreOverlayContainer.h"
#include "OgreFont.h"

DebugText2D::DebugText2D(Ogre::SceneManager* manager, Ogre::SceneNode* node, Ogre::String text): DebugObject(manager), text(text){
    Ogre::SceneNode::ObjectIterator objects = node->getAttachedObjectIterator();
    object = objects.peekNextValue();
    
    //camera = EnginePtr->GetForemostGameScreen()->GetActiveCamera();
    Ogre::SceneManager::CameraIterator cameras = manager->getCameraIterator();
    camera = cameras.peekNextValue();
};

Ogre::Vector2 DebugText2D::getTextDimensions(Ogre::String text){
    //Ogre::Real charHeight = Ogre::StringConverter::parseReal(font->getParameter("size"));
    Ogre::Real charHeight = Ogre::StringConverter::parseReal(textArea->getParameter("char_height"));

    Ogre::Vector2 result(0, 0);
 
    for(Ogre::String::iterator i = text.begin(); i < text.end(); i++)
    {   
        if (*i == 0x0020)
            result.x += font->getGlyphAspectRatio(0x0030);
        else
            result.x += font->getGlyphAspectRatio(*i);
    }
 
    Ogre::Viewport* viewport = camera->getViewport();

    result.x = (result.x * charHeight) / (float)camera->getViewport()->getActualWidth();
    result.y = charHeight / (float)camera->getViewport()->getActualHeight();
 
    return result;
};

void DebugText2D::initialize(){
    overlay = Ogre::OverlayManager::getSingleton().create("debug_text2d_" + getUniqueId());
    container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", "container_" + getUniqueId());
    overlay->add2D(container);

    textArea = Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea","text_area_" + getUniqueId());
    textArea->setDimensions(0.8f, 0.8f);
    textArea->setMetricsMode(Ogre::GMM_PIXELS);
    textArea->setPosition(0.1f, 0.1f);

    Ogre::FontManager::getSingleton().load("EngineFont", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    font = (Ogre::Font*)Ogre::FontManager::getSingleton().getByName("EngineFont").getPointer();
    textArea->setParameter("font_name", "EngineFont");
    //textArea->setParameter("char_height", font->getParameter("size"));
    textArea->setParameter("char_height", "12");
    textArea->setParameter("horz_align", "left");
    //textArea->setColour(color);
    textArea->setCaption(text);

    container->addChild(textArea);
    overlay->show();

    //textDim = getTextDimensions(text);
    //container->setDimensions(textDim.x, textDim.y);
};

void DebugText2D::setText(Ogre::String text){
    textArea->setCaption(text);
    textDim = getTextDimensions(text);
    container->setDimensions(textDim.x, textDim.y);
};

void DebugText2D::setColour( Ogre::ColourValue colour)
{
    textArea->setColour(colour);
};

void DebugText2D::update(float deltaTime){
    if(!object->isInScene() || !m_visible)
    {
        overlay->hide();
        return;
    }

    textDim = getTextDimensions(text);
    container->setDimensions(textDim.x, textDim.y);

    // Derive the average point between the top-most corners of the object's bounding box
    const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox(true);
    Ogre::Vector3 point = (AABB.getCorner(Ogre::AxisAlignedBox::FAR_LEFT_TOP)
        + AABB.getCorner(Ogre::AxisAlignedBox::FAR_RIGHT_TOP)
        + AABB.getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP)
        + AABB.getCorner(Ogre::AxisAlignedBox::NEAR_RIGHT_TOP)) / 4.0f;

    // Is the camera facing that point? If not, hide the overlay and return.
    Ogre::Plane cameraPlane = Ogre::Plane(Ogre::Vector3(camera->getDerivedOrientation().zAxis()), camera->getDerivedPosition());
    if(cameraPlane.getSide(point) != Ogre::Plane::NEGATIVE_SIDE)
    {
        overlay->hide();
        return;
    }

    // Derive the 2D screen-space coordinates for that point
    point = camera->getProjectionMatrix() * (camera->getViewMatrix() * point);

    // Transform from coordinate space [-1, 1] to [0, 1]
    Ogre::Real x = (point.x / 2.0f) + 0.5f;
    Ogre::Real y = 1.0f - ((point.y / 2.0f) + 0.5f);

    // Update the position (centering the text)
    container->setPosition(x - (textDim.x / 2.0f), y);
    overlay->show();
};

DebugText2D::~DebugText2D()
{
    container->removeChild(textArea->getName());

    Ogre::OverlayManager::getSingleton().destroyOverlayElement(textArea);

    overlay->remove2D(container);
    Ogre::OverlayManager::getSingleton().destroyOverlayElement(container);
    Ogre::OverlayManager::getSingleton().destroy(overlay);
};
