#include "StdAfx.h"
#include "DebugObject.h"
#include "OgreSceneManager.h"
#include "OgreSceneNode.h"

using namespace GamePipe;

int DebugObject::m_uniqueId = 0;

Ogre::String DebugObject::getUniqueId(){
    char buff[sizeof(m_uniqueId)*8+1];
    _itoa_s(m_uniqueId,buff,10);

    m_uniqueId++;

    Ogre::String num = buff;

    return num;
};

DebugObject::DebugObject(Ogre::SceneManager* manager): m_manager(manager), m_sceneNode(0), 
m_parent(0), m_visible(true){
    if (m_manager){
        m_sceneNode = m_manager->getRootSceneNode()->createChildSceneNode();
    }

    m_colour = Ogre::ColourValue();
};

void DebugObject::attachTo(Ogre::SceneNode* parent){
    m_parent = parent;
};

void DebugObject::detach(){
    if (m_parent){
        m_parent = 0;
    }
};

void DebugObject::setVisible(bool visible){
    m_visible = visible;
};

bool DebugObject::isVisible(){
    return m_visible;
};