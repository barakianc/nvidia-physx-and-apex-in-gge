#include "StdAfx.h"
#include "DebugCircle2D.h"

const float DebugCircle2D::ACCURACY = 35;

DebugCircle2D::DebugCircle2D( Ogre::SceneManager* manager):
DebugObject(manager), radius(0), thickness(0)
{
};

DebugCircle2D::DebugCircle2D(Ogre::SceneManager* manager, float radius, float thickness):
DebugObject(manager), radius(radius), thickness(thickness){
};

DebugCircle2D::DebugCircle2D(Ogre::SceneManager* manager, float radius):
DebugObject(manager), radius(radius), thickness(0){
};

DebugCircle2D::~DebugCircle2D(){
};

void DebugCircle2D::initialize(){
    temp_model = m_manager->createManualObject("circle" + getUniqueId());
    temp_model->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

    temp_model->colour(m_colour);

    unsigned point_index = 0;

    if (thickness == 0 && radius == 0){
        initializeSize();
    }

    if (thickness > 0){
        for(float theta = 0; theta <= 2 * Ogre::Math::PI; theta += Ogre::Math::PI / DebugCircle2D::ACCURACY) {
            temp_model->position(radius * cos(theta),
                0,
                radius * sin(theta));
            temp_model->position(radius * cos(theta - Ogre::Math::PI / DebugCircle2D::ACCURACY),
                0,
                radius * sin(theta - Ogre::Math::PI / DebugCircle2D::ACCURACY));
            temp_model->position((radius - thickness) * cos(theta - Ogre::Math::PI / DebugCircle2D::ACCURACY),
                0,
                (radius - thickness) * sin(theta - Ogre::Math::PI / DebugCircle2D::ACCURACY));
            temp_model->position((radius - thickness) * cos(theta),
                0,
                (radius - thickness) * sin(theta));
            // Join the 4 vertices created above to form a quad.
            temp_model->quad(point_index, point_index + 1, point_index + 2, point_index + 3);
            point_index += 4;
        }
    }
    else{
        for(float theta = 0; theta <= 2 * Ogre::Math::PI; theta += Ogre::Math::PI / DebugCircle2D::ACCURACY) {
            temp_model->position(radius * cos(theta), 0, radius * sin(theta));
            temp_model->index(point_index++);
        }
    }

    temp_model->index(0); // Rejoins the last point to the first.
    temp_model->end();

    temp_model->setCastShadows(false);

    m_model = temp_model;
    if(m_sceneNode){
        m_sceneNode->attachObject(m_model);
    }
};

void DebugCircle2D::initializeSize()
{
    Ogre::SceneNode::ObjectIterator objects = m_parent->getAttachedObjectIterator();

    Ogre::Real maxX = (Ogre::Real)INT_MIN, maxZ = (Ogre::Real)INT_MIN;
    Ogre::Real minX = (Ogre::Real)INT_MAX, minZ = (Ogre::Real)INT_MAX;

    while(objects.hasMoreElements()){
        Ogre::MovableObject* object = objects.peekNextValue();

        const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox();

        Ogre::Vector3 max = AABB.getMaximum();
        maxX = (maxX > max.x) ? maxX : max.x;
        maxZ = (maxZ > max.z) ? maxZ : max.z;

        Ogre::Vector3 min = AABB.getMinimum();
        minX = (minX < min.x) ? minX : min.x;
        minZ = (minZ < min.z) ? minZ : min.z;

        objects.moveNext();
    }

    if (maxX == INT_MIN || maxZ == INT_MIN ||
        minX == INT_MAX || minZ == INT_MAX){
            thickness = 1;
            radius = 5;
    }
    else{
        Ogre::Real averageX = (maxX - minX);
        Ogre::Real averageZ = (maxZ - minZ);

        radius = (averageX > averageZ) ? averageX : averageZ;

        thickness = radius * 0.1f;
    }
}

void DebugCircle2D::setColour( Ogre::ColourValue colour)
{
    if(temp_model){
        m_colour = colour;

        m_manager->destroyManualObject(temp_model);
        initialize();
    }
};

void DebugCircle2D::setVisible( bool debug )
{
    DebugObject::setVisible(debug);

    Ogre::SceneNode::ObjectIterator it = m_sceneNode->getAttachedObjectIterator();

    Ogre::SceneNode::ObjectMap::iterator map = it.begin();

    for(;map != it.end(); map++){
        ((*map).second)->setVisible(debug);
    }
};

void DebugCircle2D::update(float deltaTime){
    if(m_visible && m_parent){
        Ogre::SceneNode::ObjectIterator objects = m_parent->getAttachedObjectIterator();

        Ogre::Real maxX = (Ogre::Real)INT_MIN, maxY = (Ogre::Real)INT_MIN, maxZ = (Ogre::Real)INT_MIN;
        Ogre::Real minX = (Ogre::Real)INT_MAX, minY = (Ogre::Real)INT_MAX, minZ = (Ogre::Real)INT_MAX;

        while(objects.hasMoreElements()){
            Ogre::MovableObject* object = objects.peekNextValue();

            const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox();

            Ogre::Vector3 max = AABB.getMaximum();
            maxX = (maxX > max.x) ? maxX : max.x;
            maxY = (maxY > max.y) ? maxY : max.y;
            maxZ = (maxZ > max.z) ? maxZ : max.z;

            Ogre::Vector3 min = AABB.getMinimum();
            minX = (minX < min.x) ? minX : min.x;
            minY = (minY < min.y) ? minY : min.y;
            minZ = (minZ < min.z) ? minZ : min.z;

            objects.moveNext();
        }

        if (maxX == INT_MIN && maxY == INT_MIN && maxZ == INT_MIN &&
            minX == INT_MAX && minY == INT_MAX && minZ == INT_MAX){
                m_sceneNode->setPosition(m_parent->getPosition());
        }
        else {
            //hover the circle 5% above the bottom.
            m_sceneNode->setPosition((minX + maxX)/2.0f, minY + (maxY - minY) * 0.05f, (minZ + maxZ)/2.0f);
        }
    }
};
