#ifndef DEBUG_LINE_H
#define DEBUG_LINE_H

#include "DebugObject.h"

namespace Ogre {
    class SceneManager;
    class SceneNode;
    class ManualObject;
}

namespace GamePipe {
    class DebugLine : public DebugObject{
    public:
        Ogre::SceneNode *m_nodeOne, *m_nodeTwo;

        DebugLine(Ogre::SceneManager*, Ogre::SceneNode*, Ogre::SceneNode*);
        virtual ~DebugLine();
        virtual void initialize();
        virtual void setColour(Ogre::ColourValue);
        virtual void setVisible(bool);
        virtual void update(float);
    protected:
        
        Ogre::ManualObject *m_object;
    private:
    };
};

#endif