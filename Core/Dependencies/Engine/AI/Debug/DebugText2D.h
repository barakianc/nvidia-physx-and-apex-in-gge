#ifndef DEBUG_TEXT_2D_H
#define DEBUG_TEXT_2D_H

#include "DebugObject.h"

#include "OgreVector2.h"
#include "OgreString.h"

using namespace GamePipe;

namespace Ogre{
    class SceneManager;
    class SceneNode;
    class MovableObject;
    class Camera;
    class Overlay;
    class OverlayElement;
    class OverlayContainer;
    class Font;
}

namespace GamePipe{

    class DebugText2D: public DebugObject{
    public:
        DebugText2D(Ogre::SceneManager*,Ogre::SceneNode* node,Ogre::String);
        virtual ~DebugText2D();
        void initialize();
        void update(float);
        virtual void setColour(Ogre::ColourValue);
        void setText(Ogre::String);

    protected:
        Ogre::MovableObject *object;
        Ogre::Camera *camera;
        Ogre::Overlay *overlay;
        Ogre::OverlayElement *textArea;
        Ogre::OverlayContainer* container;
        Ogre::Vector2 textDim;
        Ogre::Font *font;
        Ogre::String text;

        Ogre::Vector2 getTextDimensions(Ogre::String);
    private:
    };
};

#endif
