#include "StdAfx.h"
#include "DebugLine.h"

#include "OgreSceneManager.h"
#include "OgreSceneNode.h"
#include "OgreManualObject.h"

using namespace GamePipe;

DebugLine::DebugLine(Ogre::SceneManager *manager, Ogre::SceneNode* nodeOne,
                     Ogre::SceneNode* nodeTwo) : DebugObject(manager), m_nodeOne(nodeOne),
                     m_nodeTwo(nodeTwo)
{
};

DebugLine::~DebugLine()
{
};

void DebugLine::initialize()
{
    m_sceneNode = m_manager->getRootSceneNode()->createChildSceneNode();

    m_object = m_manager->createManualObject("line" + getUniqueId());

    m_object->setRenderQueueGroup(Ogre::RENDER_QUEUE_8);
    m_object->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST);
    m_object->colour(m_colour);
    m_object->position(m_nodeOne->_getDerivedPosition());
    m_object->position(m_nodeTwo->_getDerivedPosition());
    m_object->end();

    m_object->setCastShadows(false);

    m_sceneNode->attachObject(m_object);
};

void DebugLine::setColour( Ogre::ColourValue colour)
{
    if (m_object){
        m_manager->destroyManualObject(m_object);
        m_colour = colour;
        initialize();
    }
};

void DebugLine::setVisible( bool visible )
{
    DebugObject::setVisible(visible);

    m_sceneNode->setVisible(visible);
};

void DebugLine::update( float deltaTime)
{
    m_object->clear();

    if(m_visible){
        m_object->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST);
        m_object->position(m_nodeOne->_getDerivedPosition());
        m_object->position(m_nodeTwo->_getDerivedPosition());
        m_object->end();
    }
};
