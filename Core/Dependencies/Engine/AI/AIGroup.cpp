#include "StdAfx.h"
#include "AIGroup.h"

#include "AIAgent.h"

using namespace GamePipe;

AIGroup::AIGroup()
{
    m_all = std::map<Ogre::String, std::list<AIAgent*>>();
	// Create log manager;
    m_log = Ogre::LogManager::getSingletonPtr();
}

void AIGroup::addAgent(Ogre::String name, GamePipe::AIAgent *agent)
{
	
	std::list<AIAgent*> agents = m_all[name];

	if(agents.size() > 0)
	{
		agents = m_all[name];
		agents.push_back(agent);
	}
	else
	{
		agents = std::list<AIAgent*>();
		agents.push_back(agent);
	}
	
	
}

std::list<AIAgent*> AIGroup::getAIGroup(Ogre::String name)
{
	std::list<AIAgent*> agents = m_all[name];
	if(agents.size() > 0)
	{
		return agents;
	}
	else
	{
		m_log->logMessage("Name of the AIGroup does not exist", Ogre::LML_CRITICAL);
	}
	std::list<AIAgent*> zeroAgent;
	return zeroAgent;
}


void AIGroup::removeFromAIGroup(Ogre::String name,Ogre::String agentName)
{
	std::list<AIAgent*> agents = m_all[name];
	std::list<AIAgent*>::iterator it;
	
	for(it = agents.begin();it != agents.end();it++ )
	{
		if((*it)->getName() == agentName)
		{
			agents.erase(it);
		}
	}
}