#ifndef GENERATE_SCENE_H
#define GENERATE_SCENE_H

#include "OgreString.h"

class TextFileManager;
class GameObject;

namespace Ogre
{
    class SceneNode;
};

namespace GamePipe
{
    class GridWorld;

	class GenerateScene
	{
	public:
		GenerateScene();
		~GenerateScene();

		///<summary>
		/// generate the scene according to the file
		///</summary>
		///<returns>if the scene is generated successfully, then return true</returns>
		static bool getScene(Ogre::String fileName);

		///<summary>
		/// generate the scene according to the file and return the pointer to gridWorld
		///</summary>
		///<returns>if the scene is generated successfully, then return the pointer to the gridWorld which represents the scene</returns>
		static GamePipe::GridWorld* getSceneWithGridWorld(Ogre::String fileName);
        
        static GameObject* createCharacter(int,Ogre::String = "");

        static void createOffsetCameraNode(Ogre::SceneNode*,Ogre::SceneNode*);

	private:
		///<summary>
		///Text file manager which is used to load in map file.
		///</summary>
		static TextFileManager *m_tfm;

		static int m_uniqueId;

		static Ogre::String getUniqueId();
	};
};
#endif