#include "StdAfx.h"
#include "GraphWorld.h"
#include "GameObject.h"
#include "GraphNode.h"
#include "GraphEdge.h"
#include "DebugObject.h"
#include "DebugLine.h"
#include "DebugCircle2D.h"
#include "OgreSceneManager.h"

using namespace std;

namespace GamePipe
{
    GraphWorld::GraphWorld()
    {
        NodeList = list<GraphNode*>();
        EdgeList = list<GraphEdge*>();
        m_lineDebugObjects = new list<DebugObject*>();
        m_pathDebugObjects = new list<DebugObject*>();
    };

    bool GraphWorld::addNode(GraphNode* newNode)
    {
        std::list<GraphNode*>::iterator nodeIt;
        std::list<GraphEdge*>::iterator edgeIt;

        for(nodeIt = this->NodeList.begin(); nodeIt != this->NodeList.end(); nodeIt++)
        {
            if((*nodeIt)->getPos() == newNode->getPos())
            {
                return false;
            }
        }

        if((int)this->NodeList.size() > 0)
        {
            for(nodeIt = this->NodeList.begin(); nodeIt != this->NodeList.end(); nodeIt++)
            {
                EdgeList.push_back(new GraphEdge((*nodeIt), newNode));
            }
        }
        this->NodeList.push_back(newNode);
        return true;
    };

    bool GraphWorld::deleteNode(GraphNode* delNode)
    {
        std::list<GraphNode*>::iterator nodeIt;
        std::list<GraphEdge*>::iterator edgeIt;

        for(edgeIt = this->EdgeList.begin(); edgeIt != this->EdgeList.end(); edgeIt++)
        {
            if((*edgeIt)->getStartNode()->getPos() == delNode->getPos() || (*edgeIt)->getEndNode()->getPos() == delNode->getPos())
            {
                EdgeList.erase(edgeIt);
                break;
            }
        }
        for(nodeIt = NodeList.begin(); nodeIt != NodeList.end(); nodeIt++)
        {
            if((*nodeIt)->getPos() == delNode->getPos())
            {
                NodeList.erase(nodeIt);
                break;
            }
        }
        return true;
    };

    bool GraphWorld::deleteNodeByIndex(int i)
    {
        return deleteNode(getNodeByIndex(i));
    };

    void GraphWorld::drawPath( Ogre::SceneManager* manager, list<GraphNode*> nodes)
    {
        DebugLine *line;

        std::list<GraphNode*>::iterator it = nodes.begin(), it2 = nodes.begin();
        it2++;

        for(; it != nodes.end(); it++){
            for(std::list<DebugObject*>::iterator itd = m_pathDebugObjects->begin(); itd != m_pathDebugObjects->end(); itd++){
                if((*itd)->m_sceneNode->getPosition().positionEquals((*it)->getPos())){
                    (*itd)->setColour(Ogre::ColourValue(0.0f,1.0f,0.0f));
                    break;
                }
            }
            for(std::list<DebugObject*>::iterator itl = m_lineDebugObjects->begin(); itl != m_lineDebugObjects->end(); itl++){
                line = (DebugLine*)(*itl);
                
                if(line->m_nodeOne->getPosition().positionEquals((*it)->getPos())) {

                    if (it2 != nodes.end()){
                        if(line->m_nodeTwo->getPosition().positionEquals((*it2)->getPos())) {
                            line->setColour(Ogre::ColourValue(0.0f,1.0f,0.0f));
                            break;
                        }
                    }
                }
                else if (line->m_nodeTwo->getPosition().positionEquals((*it)->getPos())){

                    if (it2 != nodes.end()){
                        if(line->m_nodeOne->getPosition().positionEquals((*it2)->getPos())) {
                            line->setColour(Ogre::ColourValue(0.0f,1.0f,0.0f));
                            break;
                        }
                    }
                }
            }
            if(it2 != nodes.end()){
                it2++;
            }
        }
    };

    GraphNode* GraphWorld::getNodeByIndex(int i)
    {
        std::list<GraphNode*>::iterator nodeIt;
        nodeIt = NodeList.begin();
        advance(nodeIt, i);
        return *nodeIt;
    };

    GraphEdge* GraphWorld::getEdgeByIndex(int i)
    {
        std::list<GraphEdge*>::iterator edgeIt;
        edgeIt = EdgeList.begin();
        advance(edgeIt, i);
        return *edgeIt;
    };

    std::list<GraphEdge*> GraphWorld::getEdgeList(std::list<GraphNode*> givenNodeList)
    {
        std::list<GraphEdge*> returnEdgeList;
        std::list<GraphNode*>::iterator startNodeIt = givenNodeList.begin();
        std::list<GraphNode*>::iterator endNodeIt = ++startNodeIt;
        startNodeIt--;
        std::list<GraphEdge*>::iterator edgeIt = EdgeList.begin();

        for(; endNodeIt != givenNodeList.end(); startNodeIt++, endNodeIt++)
        {
            for(; edgeIt != EdgeList.end(); edgeIt++)
            {
                if((*startNodeIt)->getPos() == (*edgeIt)->getStartNode()->getPos() && (*endNodeIt)->getPos() == (*edgeIt)->getEndNode()->getPos() || ((*startNodeIt)->getPos() == (*edgeIt)->getEndNode()->getPos() && (*endNodeIt)->getPos() == (*edgeIt)->getStartNode()->getPos()))
                {
                    returnEdgeList.push_back(*edgeIt);
                    break;
                }else
                {
                    continue;
                }
            }
        }
        return returnEdgeList;
    };

    GraphNode* GraphWorld::getNodeByPos(Ogre::Vector3 pos)
    {
        std::list<GraphNode*>::iterator nodeIt;
        nodeIt = NodeList.begin();
        for(; nodeIt != NodeList.end(); nodeIt++)
        {
            if((*nodeIt)->getPos() == pos)
            {
                return *nodeIt;
            }
        }
        return new GraphNode(Ogre::Vector3::Vector3(0,0,0));
    };

    std::list<GraphEdge*> GraphWorld::getEdgeList(GraphNode* node)
    {
        std::list<GraphEdge*> returnEdgeList;
        std::list<GraphEdge*>::iterator edgeIt = EdgeList.begin();

        for(; edgeIt != EdgeList.end(); edgeIt++)
        {
            if((*edgeIt)->getCost() > 0)
            {
                if(node->getPos() == (*edgeIt)->getStartNode()->getPos() || node->getPos() == (*edgeIt)->getEndNode()->getPos())
                {
                    returnEdgeList.push_back(*edgeIt);
                }
            }
        }
        return returnEdgeList;
    };

    GraphNode* GraphWorld::getNearestNode(Ogre::Vector3 pos)
    {
        std::list<GraphNode*>::iterator nodeIt = NodeList.begin();
        GraphNode* nearestNode = (*NodeList.begin());
        float lowestCost = FLT_MAX;
        float tempCost = 0;

        for(; nodeIt != NodeList.end(); nodeIt++)
        {
            tempCost = (*nodeIt)->getPos().squaredDistance(pos);
            if( tempCost < lowestCost)
            {
                nearestNode = *nodeIt;
                lowestCost = tempCost;
            }
        }
        return nearestNode;
    };

    void GraphWorld::initializeDebug( Ogre::SceneManager* manager )
    {
        Ogre::SceneNode *tempNode, *tempNode2;

        DebugCircle2D *circle;
        DebugLine *line;

        for(list<DebugObject*>::iterator it = m_pathDebugObjects->begin(); it != m_pathDebugObjects->end(); it++){
            delete *it;
        }
        for(list<DebugObject*>::iterator it = m_lineDebugObjects->begin(); it != m_lineDebugObjects->end(); it++){
            delete *it;
        }

        m_pathDebugObjects->clear();
        m_lineDebugObjects->clear();

        for(list<GraphNode*>::iterator it = NodeList.begin(); it != NodeList.end(); it++){
            tempNode = manager->getRootSceneNode()->createChildSceneNode();
            tempNode->setPosition((*it)->getPos());

            circle = new DebugCircle2D(manager,1,0.1f);
            circle->attachTo(tempNode);
            circle->initialize();
            circle->update(0);

            m_pathDebugObjects->push_back(circle);
        }

        for(list<GraphEdge*>::iterator it = EdgeList.begin(); it != EdgeList.end(); it++){
            if((*it)->getCost() < 0){
                continue;
            }
            tempNode = manager->getRootSceneNode()->createChildSceneNode();
            tempNode->setPosition((*it)->getStartNode()->getPos());

            tempNode2 = manager->getRootSceneNode()->createChildSceneNode();
            tempNode2->setPosition((*it)->getEndNode()->getPos());

            line = new DebugLine(manager, tempNode, tempNode2);
            line->initialize();
            line->update(0);

            m_lineDebugObjects->push_back(line);
        }
    };

    void GraphWorld::setDebug( bool debug)
    {
        m_debug = debug;

        for(list<DebugObject*>::iterator it = m_pathDebugObjects->begin();
            it != m_pathDebugObjects->end(); it++){
                (*it)->setVisible(debug);
        }
        for(list<DebugObject*>::iterator it = m_lineDebugObjects->begin();
            it != m_lineDebugObjects->end(); it++){
                (*it)->setVisible(debug);
        }
    };

    bool GraphWorld::updateGraphWorld()
    {
        GameObject* myGameObject = NULL;
#ifdef HAVOK
        hkpRigidBody* myRigidBody = NULL;

		Ogre::Vector3 collisionPoint;
        std::list<GraphEdge*>::iterator edgeIt = EdgeList.begin();
        for(;edgeIt != EdgeList.end();edgeIt++)
        {
            if(EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->castRay((*edgeIt)->getStartNode()->getPos(),(*edgeIt)->getEndNode()->getPos(), &myRigidBody, &myGameObject,  &collisionPoint))
            {
                (*edgeIt)->setCost(-1);
            }else
            {
                if((*edgeIt)->getCost() < 0)
                {
                    (*edgeIt)->setCost((*edgeIt)->getStartNode()->getPos().distance((*edgeIt)->getEndNode()->getPos()));
                }
            }
        }
#endif
        return true;
    };

    void GraphWorld::updateDebug( float deltaTime)
    {
        for(list<DebugObject*>::iterator it = m_pathDebugObjects->begin();
            it != m_pathDebugObjects->end(); it++){
                (*it)->update(deltaTime);
        }
        for(list<DebugObject*>::iterator it = m_lineDebugObjects->begin();
            it != m_lineDebugObjects->end(); it++){
                (*it)->update(deltaTime);
        }
    };

    void GraphWorld::removePath()
    {
        for(list<DebugObject*>::iterator it = m_pathDebugObjects->begin();
            it != m_pathDebugObjects->end(); it++){
                (*it)->setColour(Ogre::ColourValue(1.0f,1.0f,1.0f));
        }
        for(list<DebugObject*>::iterator it = m_lineDebugObjects->begin();
            it != m_lineDebugObjects->end(); it++){
                (*it)->setColour(Ogre::ColourValue(1.0f,1.0f,1.0f));
        }
    };

	GraphEdge* GraphWorld::getEdge(Ogre::Vector3 node1, Ogre::Vector3 node2)
	{
		std::list<GraphEdge*>::iterator edgeIt = EdgeList.begin();

		for(; edgeIt != EdgeList.end(); edgeIt++)
		{
			if(node1 == (*edgeIt)->getStartNode()->getPos() && node2 == (*edgeIt)->getEndNode()->getPos() || (node1 == (*edgeIt)->getEndNode()->getPos() && node2 == (*edgeIt)->getStartNode()->getPos()))
			{
				return *edgeIt;
			}else
			{
				continue;
			}
		}
		return NULL;
	};
}