#include "StdAfx.h"
#include "GenerateScene.h"

#include "OgreString.h"
#include "OgreResourceManager.h"
#include "OgreSceneNode.h"

#include "GameObject.h"

#ifdef HAVOK
#include "HavokObject.h"
#include "HavokWrapper.h"
#endif

#include "GridWorld.h"
#include "../Resources/TextFileManager.h"
#include "../Resources/TextFile.h"
#include "AIAgent.h"

namespace GamePipe
{
    TextFileManager* GenerateScene::m_tfm;
    int GenerateScene::m_uniqueId=0;

    bool GenerateScene::getScene(Ogre::String fileName)
    {
        getSceneWithGridWorld(fileName);
        return true;
    };

    GamePipe::GridWorld* GenerateScene::getSceneWithGridWorld(Ogre::String fileName)
    {
        int width, height;
        size_t step;
        size_t preFound;
        size_t found;
#ifdef HAVOK
        GameObject *ball;
#endif

        m_tfm = TextFileManager::getSingletonPtr();

        // Load map file
        Ogre::ResourceGroupManager::getSingleton().declareResource(fileName.c_str(), "TextFile");

        TextFilePtr textfile = m_tfm->load(fileName.c_str(), Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        std::string mapString= textfile->getString();

        found = mapString.find('\n');
        step = found;

        width = atoi(mapString.substr(0,step).c_str());

        preFound = found;
        found = mapString.find('\n', found+1);
        step = found - preFound;

        height = atoi(mapString.substr(preFound,step).c_str());

        GamePipe::GridWorld* gridWorld = new GamePipe::GridWorld(width,height);

        found++;
        for(int i = 0; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                if(gridWorld->getCost(j,i) == -1)
                {
                    found++;
                    continue;
                }else
                {
                    switch(mapString[found])
                    {
                    case 'O':
                        {
                            gridWorld->setCost(j,i,0);
                            found++;
                            break;
                        }
                    case '2':
                        {
#ifdef HAVOK
                            ball = new GameObject(getUniqueId(),"crate2m.mesh","crate2m.hkx",PHYSICS_DYNAMIC);
                            ball->setPosition((j-width/2.0f-1.0f)*1.0f,-19.0f,(i-height/2.0f-1.0f)*1.0f);
                            ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
#endif

                            gridWorld->setCost(j,i,-1);
                            gridWorld->setCost(j+1,i,-1);
                            gridWorld->setCost(j,i+1,-1);
                            gridWorld->setCost(j+1,i+1,-1);
                            found++;
                            break;
                        }
                    case '1':
                        {
                            gridWorld->setCost(j,i,-1);
#ifdef HAVOK
                            ball = new GameObject(getUniqueId(),"crate1m.mesh","crate1m.hkx",PHYSICS_DYNAMIC);
                            ball->setPosition(j*1.0f-(width+3.0f)*1/2.0f,-19.5f,i*1.0f-(width+3.0f)*1.0f/2.0f);
                            ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
#endif

                            found++;
                            break;
                        }
                    case '4':
                        {
#ifdef HAVOK
                            ball = new GameObject(getUniqueId(),"crate4m.mesh","crate4m.hkx",PHYSICS_DYNAMIC);
                            ball->setPosition(j*1.0f-width*1.0f/2.0f,-18.0f,i*1.0f-width*1.0f/2.0f);
                            ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
#endif
                            for(int m =0; m<4; m++)
                                for(int n =0; n<4; n++)
                                {
                                    gridWorld->setCost(j+m,i+n,-1);
                                }
                                found++;
                                break;
                        }
                    }
                }
            }
            found++;
            found++;
        }

        return gridWorld;
    };

    Ogre::String GenerateScene::getUniqueId(){
        char buff[sizeof(m_uniqueId)*8+1];
        _itoa_s(m_uniqueId,buff,10);

        m_uniqueId++;

        Ogre::String num = buff;

        return num;
    };

    GameObject* GenerateScene::createCharacter( int character, Ogre::String luaFile )
    {
        GameObject* tempObject = NULL;
#ifdef HAVOK
        Ogre::String name = "merc";
        name.append(getUniqueId());
        name = getUniqueId();

        switch(character){
            case 2:
                if(luaFile != ""){
                    tempObject = new GameObject(name,"merc2.mesh","merc.hkx",ANIMATED_CHARACTER_RIGIDBODY,DEFAULT_COLLISION_SHAPE,luaFile);
                }
                else {
                    tempObject = new GameObject(name,"merc.mesh","merc.hkx",ANIMATED_CHARACTER_RIGIDBODY);
                }
                break;

            default:
                if(luaFile != ""){
                    tempObject = new GameObject(name,"merc.mesh","merc.hkx",ANIMATED_CHARACTER_RIGIDBODY,DEFAULT_COLLISION_SHAPE,luaFile);
                }
                else {
                    tempObject = new GameObject(name,"merc.mesh","merc.hkx",ANIMATED_CHARACTER_RIGIDBODY);
                }
                break;
        }

        if(tempObject){
            if(luaFile != ""){
                tempObject->getAIAgent()->playAnimation("merc_runfront", true);
                tempObject->getAIAgent()->setMaxLinearSpeed(20.0);
                tempObject->getAIAgent()->m_physicsObject->getRigidBody()->setFriction(0.25f);
            }
            //tempObject->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
            tempObject->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
            Ogre::Vector3 size = tempObject->m_pGraphicsObject->m_pOgreEntity->getBoundingBox().getSize();

            // Used because havok animated gameObjects are being centered at
            // 0,0,0 which is incorrect.
            ((CharacterRigidBodyObject*)tempObject->m_pPhysicsObject)->scaleAndOffset(size.x/6,size.y/6,size.z/6,0.0f,size.y/6,0);
            
            // Used when havok animated gameObjects are being centered at their
            // actual center.
            //((CharacterRigidBodyObject*)tempObject->m_pPhysicsObject)->scaleAndOffset(size.x/6,size.y/6,size.z/6,0.0f,-size.y/3,0.0f);
            ((CharacterRigidBodyObject*)tempObject->m_pPhysicsObject)->setBaseLookingDirection(Ogre::Math::DegreesToRadians(-85.0f));

            ((OpenSteer::SimpleVehicle*)tempObject->getAIAgent())->setMaxSpeed (8.0f);
            ((OpenSteer::SimpleVehicle*)tempObject->getAIAgent())->setMaxForce (50.0f);
            ((OpenSteer::SimpleVehicle*)tempObject->getAIAgent())->setRadius(size.x/5);

            // initially stopped
            ((OpenSteer::SimpleVehicle*)tempObject->getAIAgent())->setSpeed (0);
        }
#endif
        return tempObject;
    };

    void GenerateScene::createOffsetCameraNode( Ogre::SceneNode *offsetNode, Ogre::SceneNode *parentNode )
    {
        // Use when havok animated gameObjects are centered at 0,0,0
        // This is XXX coding.
        offsetNode->setPosition(0.0f,3.0f,-5.0f);

        // Use when havok animated gameObjects are centered properly
        //offsetNode->setPosition(0.0f,6.0f,-5.0f);

        Ogre::Quaternion rot = Ogre::Quaternion(Ogre::Radian(Ogre::Degree(20.0f).valueRadians()),Ogre::Vector3::NEGATIVE_UNIT_X);

        offsetNode->setOrientation(offsetNode->getOrientation() * Ogre::Quaternion(0,0,1.0f,0) * rot);
        parentNode->addChild(offsetNode);
    };
};