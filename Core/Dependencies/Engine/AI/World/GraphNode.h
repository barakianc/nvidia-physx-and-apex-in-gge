#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include "OgreVector3.h"

namespace GamePipe
{
	///<summary>
	///This class contains the information of one node in world
	///</summary>
	///<author>Lin Tian tina.tian1987@gmail.com</author>
	class GraphNode
	{
	public:
	  ///<summary>
	  ///GraphNode constructor
	  ///</summary>
	  ///<param name = 'newPos'>vector3 contains the position information of the node</param>
	  GraphNode(Ogre::Vector3 newPos);
	 /* ~GraphNode();*/

	  ///<summary>
	  ///get the vector3 contains the position of node
	  ///</summary>
	  ///<returns>return vector3 contains the position</returns>
	  Ogre::Vector3 getPos()const{return pos;}

	  ///<summary>
	  ///set the position of the node
	  ///</summary>
	  ///<param name = 'newPos'>vector3 contains the position information</param>
	  void  setPos(Ogre::Vector3 newPos){pos.swap(newPos);}

      bool equal(GraphNode* right){return pos.positionEquals(right->pos);}

	private:
		Ogre::Vector3 pos;
	};
}
#endif