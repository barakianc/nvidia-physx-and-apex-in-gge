#ifndef GRAPHEDGE_H
#define GRAPHEDGE_H

namespace GamePipe
{
    class GraphNode;

	///<summary>
	///contain Edge information in world
	///</summary>
	///<author>Lin Tian tina.tian1987@gmail.com</author>
	class GraphEdge
	{
	public:
		///<summary>
		///GraphEdge constructor
		///</summary>
		///<param name = 'start'>pointer to the start node in this edge</param>
		///<param name = 'end'>pointer to the end node in this edge</param>
		GraphEdge(GraphNode* start, GraphNode* end);
		~GraphEdge();

		///<summary>
		///set start node in this edge
		///</summary>
		///<param name = 'start'>pointer to the start node in this edge</param>
		void setStartNode(GraphNode* start){startNode = start;}

		///<summary>
		///get the start node in this edge
		///</summary>
		///<returns>return pointer to the start node </returns>
		GraphNode* getStartNode(){return startNode;}

		///<summary>
		///set end node in this edge
		///</summary>
		///<param name = 'end'>pointer to the end node in this edge</param>
		void setEndNode(GraphNode* end){endNode = end;}

		///<summary>
		///get the end node in this edge
		///</summary>
		///<returns>return pointer to the end node </returns>
		GraphNode* getEndNode(){return endNode;}

		///<summary>
		///set the cost value of this edge
		///</summary>
		///<param name = 'newCost'>cost value of this edge</param>
		void setCost(float newCost){cost = newCost;}

		///<summary>
		///get the cost to go through this edge
		///</summary>
		///<returns>return cost</returns>
		float getCost(){return cost;}

	private:
		///<summary>startNode contains the pointer to the start node</summary>
		GraphNode* startNode;

		///<summary>endNode contains the pointer to the end node</summary>
		GraphNode* endNode;

		///<summary>contain cost value of this edge</summary>
		float cost;
	};
}

#endif