#ifndef GRAPHWORLD_H
#define GRAPHWORLD_H

#include <list>
#include "OgreVector3.h"

namespace Ogre
{
    class SceneManager;
};

namespace GamePipe
{
    class GraphNode;
    class GraphEdge;
    class DebugObject;

	/// <summary>
	/// use graph to represent the world for AI
	/// </summary>
	/// <author>Lin Tian tina.tian1987@gmail.com</author>
	class GraphWorld
	{
		public:
			GraphWorld();
			~GraphWorld();

			/// <summary>
			/// add node to the world and also add edges containing the node to the world
			/// </summary>
			/// <param name = 'newNode'>the pointer to the node to add</param>
			/// <returns>return true if add successfully</returns>
			bool addNode(GraphNode* newNode);

			/// <summary>
			/// delete node
			/// </summary>
			/// <param name = 'delNode'>the pointer to the node to delete</param>
			/// <returns>return true if delete successfully</returns>
			bool deleteNode(GraphNode* delNode);

			/// <summary>
			/// delete node
			/// </summary>
			/// <param name = 'i'>the index of the node to delete in Nodelist</param>
			/// <returns>return true if delete successfully</returns>
			bool deleteNodeByIndex(int i);

            void drawPath(Ogre::SceneManager*, std::list<GraphNode*>);

            /// <summary>
            /// input a givenNodeList, this function will return the edges containing
            /// adjacent nodes in the givenNodeList in given sequence
            /// </summary>
            /// <param name = 'givenNodeList'>nodeList contains the sequence of the nodes</param>
            /// <returns>return the edge in given sequence</returns>
            std::list<GraphEdge*> getEdgeList(std::list<GraphNode*> givenNodeList);

            /// <summary>
            ///  input the pointer to a node and return all the edge containing this node
            /// </summary>
            /// <param name = 'node'>pointer to the node</param>
            /// <returns>a list of pointer to the edges containing the node</returns>
            std::list<GraphEdge*> getEdgeList(GraphNode* node);

            /// <summary>
            /// get the EdgeList in GraphWorld
            /// </summary>
            /// <returns>return a list which contains pointer to all the Edge in GraphWorld</returns>
            std::list<GraphEdge*> getEdgeList(){return EdgeList;}

            /// <summary>
            ///  this function will return the pointer to the node which is nearest to the position
            /// </summary>
            /// <param name = 'pos'>input the position</param>
            /// <returns>return the pointer to the node</returns>
            GraphNode* getNearestNode(Ogre::Vector3 pos);

            /// <summary>
            /// get node by index in NodeList
            /// </summary>
            /// <param name = 'i'>index in NodeList</param>
            /// <returns>pointer to the node</returns>
            GraphNode* getNodeByIndex(int i);

            /// <summary>
            ///  input a position, this function will return the pointer to the node
            /// </summary>
            /// <param name = 'pos'>the position of the node</param>
            /// <returns>if the node exists return the pointer to the node,
            /// or else return pointer to the node with position (0,0,0)</returns>
            GraphNode* getNodeByPos(Ogre::Vector3 pos);

            /// <summary>
            /// get edge by index in EdgeList
            /// </summary>
            /// <param name = 'i'>index in EdgeList</param>
            /// <returns>pointer to the edge</returns>
            GraphEdge* getEdgeByIndex(int i);

            /// <summary>
			/// get the NodeList in GraphWorld
			/// </summary>
			/// <returns>return a list which contains pointer to all the node in GraphWorld</returns>
			std::list<GraphNode*> getNodeList(){return NodeList;}

			/// <summary>
			/// get the edge by the 2 end points
			/// </summary>
			/// <param name = 'node1'>the coordinate of one of the end points</param>
			/// <param name = 'node2'>the coordinate of the other end point</param>
			/// <returns>return the pointer to the edge. If there is no edge between these 2 points, then return an edge with -1 cost. If this 2 nodes are not in the world, then return null</returns>
			GraphEdge* getEdge(Ogre::Vector3 node1, Ogre::Vector3 node2);


            void removePath();

			/// <summary>
			///  use this function to update the graph world every frame
			/// </summary>
			bool updateGraphWorld();

            void initializeDebug(Ogre::SceneManager*);

            void setDebug(bool);

            void updateDebug(float);

        protected:
            std::list<DebugObject*> *m_pathDebugObjects;
            std::list<DebugObject*> *m_lineDebugObjects;

		private:
			/// <summary>NodeList contains pointers to all the node in the world</summary>
			std::list<GraphNode*> NodeList;

			/// <summary>EdgeList contains pointers to all the edge in the world</summary>
			std::list<GraphEdge*> EdgeList;

            bool m_debug;
	};
}
#endif