#include "StdAfx.h"
#include "GridWorld.h"
#include "OgreVector2.h"
#include <math.h>
#include "GraphNode.h"
#include "../Debug/DebugCircle2D.h"
#include "../Debug/DebugLine.h"
#include "../Debug/DebugObject.h"

namespace GamePipe
{
    GridWorld::GridWorld()
    {
        start_position = Ogre::Vector3::ZERO;
        m_debug = false;
        m_height = 256;
        m_width = 256;
        m_heightPerGrid = 1;
        m_widthPerGrid = 1;
        grid = new float *[m_height];
        for(int i=0; i<m_height; i++)
        {
            grid[i] = new float[m_width];
        }
        for(int i=0; i<m_height; i++)
        {
            for (int j=0; j<m_width; j++)
            {
                grid[i][j] = 0;
            }
        }
    }

    GridWorld::GridWorld(int width, int height)
    {
        start_position = Ogre::Vector3::ZERO;
        m_debug = false;
        this->m_width = width;
        this->m_height = height;
        this->m_heightPerGrid = 1;
        this->m_widthPerGrid = 1;

        grid = new float *[height];
        for(int i=0; i<height; i++)
        {
            grid[i] = new float[width];
        }
        for(int i=0; i<height; i++)
        {
            for (int j=0; j<width; j++)
            {
                grid[i][j] = 0;
            }
        }
    };

    GridWorld::GridWorld(int width, int height, float widthPerGrid, float heightPerGrid)
    {
        start_position = Ogre::Vector3::ZERO;
        m_debug = false;
        this->m_heightPerGrid = heightPerGrid;
        this->m_widthPerGrid = widthPerGrid;

        this->m_width = width;
        this->m_height = height;

        grid = new float *[height];
        for(int i=0; i<height; i++)
        {
            grid[i] = new float[width];
        }
        for(int i=0; i<height; i++)
        {
            for (int j=0; j<width; j++)
            {
                grid[i][j] = 0;
            }
        }
    };

    GridWorld::~GridWorld()
    {
        for(int i=0; i<m_width; i++)
        {
            delete [m_width]grid[i];
            grid[i] = NULL;
        }
        delete [m_height]grid;
        grid = NULL;


    }

    Ogre::Vector2 GridWorld::changePositionToIndex(Ogre::Vector3 pos)
    {
        Ogre::Vector2 index;
        index.x = ceil(pos.x/m_widthPerGrid)+m_width/2-1;
        index.y = ceil(pos.z/m_heightPerGrid)+m_height/2-1;
        return index;
    };

    void GridWorld::colorPath( Ogre::ColourValue color)
    {
        for(std::list<DebugObject*>::iterator it = m_pathDebugObjects->begin(); it != m_pathDebugObjects->end(); it++){
            (*it)->setColour(color);
        }
    };

    void GridWorld::drawPath( Ogre::SceneManager* manager, std::list<GraphNode*> nodes)
    {
        Ogre::SceneNode* node;
        DebugCircle2D* circle;

        for(std::list<DebugObject*>::iterator it = m_pathDebugObjects->begin(); it != m_pathDebugObjects->end(); it++){
            (*it)->setColour(Ogre::ColourValue(0.0f,0.5f,0.0f));
        }

        for(std::list<GraphNode*>::iterator it = nodes.begin(); it != nodes.end(); it++){
            node = manager->getRootSceneNode()->createChildSceneNode();
            m_pathNodes->push_front(node);

            float xStart = m_widthPerGrid/2.0f;
            float yStart = m_heightPerGrid/2.0f;

            node->setPosition(start_position + Ogre::Vector3((*it)->getPos().x * m_widthPerGrid + xStart,0,(*it)->getPos().y * m_heightPerGrid + yStart));

            circle = new DebugCircle2D(manager, m_widthPerGrid/2.0f , m_widthPerGrid/10.0f);
            circle->attachTo(node);
            circle->initialize();
            circle->setVisible(true);
            circle->update(0.0f);
            circle->setColour(Ogre::ColourValue(0.0f,1.0f,0.0f));

            m_pathDebugObjects->push_front(circle);
        }
    };
    float GridWorld::getCost(int X, int Y)
    {
        return grid[Y][X];
    }

    float GridWorld::getCost(Ogre::Vector3 pos)
    {
        int x,y;
        Ogre::Vector2 index = changePositionToIndex(pos);
        x = int(index.x);
        y = int(index.y);

        if(x<0 || x>=m_width )
        {
            return FLT_MAX;
        }

        if(y<0 || y>=m_height )
        {
            return FLT_MAX;
        }
        return grid[y][x];
    };

    void GridWorld::initializeDebug(Ogre::SceneManager *manager)
    {
        m_sceneManager = manager;

        Ogre::SceneNode *tempNode, *tempNode2;

        DebugObject *line;

        m_debugObjects = new std::list<DebugObject*>();
        m_pathNodes = new std::list<Ogre::SceneNode*>();
        m_pathDebugObjects = new std::list<DebugObject*>();

        for(int i = 0; i <= m_width; i++){

            tempNode = manager->getRootSceneNode()->createChildSceneNode();
            tempNode->setPosition(start_position + 
                Ogre::Vector3(i * m_widthPerGrid, 0, 0));


            tempNode2 = manager->getRootSceneNode()->createChildSceneNode();
            tempNode2->setPosition(start_position + 
                Ogre::Vector3(i * m_widthPerGrid, 0, m_height * m_heightPerGrid));

            line = new DebugLine(manager, tempNode, tempNode2);
            line->initialize();
            m_debugObjects->push_back(line);
        }

        for(int i = 0; i <= m_height; i++){

            tempNode = manager->getRootSceneNode()->createChildSceneNode();
            tempNode->setPosition(start_position + 
                Ogre::Vector3(0 * m_widthPerGrid, 0, i * m_heightPerGrid));


            tempNode2 = manager->getRootSceneNode()->createChildSceneNode();
            tempNode2->setPosition(start_position + 
                Ogre::Vector3(m_width * m_widthPerGrid, 0, i * m_heightPerGrid));

            line = new DebugLine(manager, tempNode, tempNode2);
            line->initialize();

            m_debugObjects->push_back(line);
        }

        Ogre::SceneNode *node;
        DebugCircle2D* circle;

        for(int i = 0; i < m_height; i++){
            for (int j = 0; j < m_width; j++){
                if (getCost(i, j) < 0){

                    node = manager->getRootSceneNode()->createChildSceneNode();
                    m_pathNodes->push_front(node);

                    float xStart = m_widthPerGrid/2.0f;
                    float yStart = m_heightPerGrid/2.0f;

                    node->setPosition(start_position + Ogre::Vector3(i * m_widthPerGrid + xStart,0,j * m_heightPerGrid + yStart));

                    circle = new DebugCircle2D(manager, m_widthPerGrid/2.0f , m_widthPerGrid/10.0f);
                    circle->attachTo(node);
                    circle->initialize();
                    circle->setVisible(true);
                    circle->update(0.0f);
                    circle->setColour(Ogre::ColourValue(1.0f,0.0f,0.0f));

                    m_debugObjects->push_front(circle);
                }
            }
        }
    };

    void GridWorld::removePath()
    {
        for(std::list<DebugObject*>::iterator it = m_pathDebugObjects->begin(); it != m_pathDebugObjects->end(); it++){
            delete (*it);
        }

        m_pathDebugObjects->clear();

        for(std::list<Ogre::SceneNode*>::iterator it = m_pathNodes->begin(); it != m_pathNodes->end(); it++){
            m_sceneManager->destroySceneNode(*it);
        }

        m_pathNodes->clear();
    }

    bool GridWorld::setCost(Ogre::Vector3 pos,float cost)
    {
        int x,y;
        Ogre::Vector2 index = changePositionToIndex(pos);
        x = int(index.x);
        y = int(index.y);

        if(x<0 || x>=m_width )
        {
            return false;
        }

        if(y<0 || y>=m_height )
        {
            return false;
        }
        grid[y][x] = cost;
        return true;
    };

    bool GridWorld::setCost(int X, int Y, float cost)
    {
        grid[Y][X] = cost;
        return true;
    }

    void GridWorld::setDebug( bool debug )
    {
        m_debug = debug;

        for(std::list<DebugObject*>::iterator it = m_debugObjects->begin();
            it != m_debugObjects->end(); it++){

                (*it)->setVisible(m_debug);
        }

        for(std::list<DebugObject*>::iterator it = m_pathDebugObjects->begin(); it != m_pathDebugObjects->end(); it++){
            (*it)->setVisible(m_debug);
        }
    };

    void GridWorld::updateDebug( float deltaTime )
    {
        for(std::list<DebugObject*>::iterator it = m_debugObjects->begin();
            it != m_debugObjects->end(); it++){

                (*it)->update(deltaTime);
        }
    };

};