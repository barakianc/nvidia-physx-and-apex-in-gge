#include "StdAfx.h"
#include "GraphEdge.h"
#include "GraphNode.h"

namespace GamePipe
{
	GraphEdge::GraphEdge(GraphNode* start, GraphNode* end)
	{
		this->startNode = start;
		this->endNode = end;
		this->cost = start->getPos().distance(end->getPos());
		return;
	}
}