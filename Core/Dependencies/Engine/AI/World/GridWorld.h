#ifndef GRIDWORLD
#define GRIDWORLD

#include "OgreVector3.h"
#include <list>

namespace GamePipe
{
    class GraphNode;
    class DebugObject;

	/// <summary>
	/// represent the world by grid
	/// </summary>
	/// <author>Lin Tian tina.tian1987@gmail.com</author>
	class GridWorld
	{
	public:
		GridWorld();

		/// <summary>
		/// constructor for grid world
		/// </summary>
		/// <param name = 'width'>width of the grids world</param>
		/// <param name = 'height'>height of the grids world</param>
		GridWorld(int, int);

		/// <summary>
		/// constructor for grid world
		/// </summary>
		/// <param name = 'width'>width of the grids world</param>
		/// <param name = 'height'>height of the grids world</param>
		/// <param name = 'widthPerGrid'>width per grid</param>
		/// <param name = 'heightPerGrid'>height per grid</param>
		GridWorld(int,int,float,float);
		~GridWorld();

		/// <summary>
		///  set the cost of grid in position(x,y)
		/// </summary>
		/// <param name = 'X'>the index of the grid in horizontal</param>
		/// <param name = 'Y'>the index of the grid in vertical</param>
		/// <param name = 'cost'>the value of the cost</param>
		/// <returns>if set the cost successfully then return true</returns>
		bool setCost(int, int, float);

		/// <summary>
		///  get the cost of grid in position(x,y)
		/// </summary>
		/// <param name = 'X'>the index of the grid in horizontal</param>
		/// <param name = 'Y'>the index of the grid in vertical</param>
		/// <returns>the value of the cost</returns>
		float getCost(int,int);

		/// <summary>
		///  set the cost of grid in position(x,y)
		/// </summary>
		/// <param name = 'pos'>the position in game world coordinate</param>
		/// <param name = 'cost'>the value of the cost</param>
		/// <returns>if set the cost successfully then return true</returns>
		bool setCost(Ogre::Vector3 pos,float cost);

		/// <summary>
		///  get the cost of grid in position(x,y)
		/// </summary>
		/// <param name = 'pos'>the position in game world coordinate</param>
		/// <returns>the value of the cost</returns>
		float getCost(Ogre::Vector3 pos);

		/// <summary>
		///  set the width of the grid
		/// </summary>
		bool setWidth(int newWidth){m_width = newWidth; return true;};

		/// <summary>
		///  set the height of the grids
		/// </summary>
		bool setHeight(int newHight){m_height = newHight; return true;};

		/// <summary>
		///  get the height of the grids
		/// </summary>
        int getHeight(){ return m_height; }

		/// <summary>
		///  get the width of the grid
		/// </summary>
        int getWidth(){ return m_width; }

		/// <summary>
		///  set the width per grid
		/// </summary>
		bool setWidthPerGrid(float newWidth){m_widthPerGrid = newWidth; return true;}

		/// <summary>
		///  get the width per grid
		/// </summary>
		float getWidthPerGrid(){return m_widthPerGrid;}

		/// <summary>
		///  set the height per grid
		/// </summary>
		bool setHeightPerGrid(float newHeight){m_heightPerGrid = newHeight; return true;}

		/// <summary>
		///  get the height per grid
		/// </summary>
		float getHeightPerGrid(){return m_heightPerGrid;}

        void setInitialPosition(Ogre::Vector3 position){start_position = position;}
        Ogre::Vector3 getInitialPosition(){return start_position;}

        void setDebug(bool);

        void initializeDebug(Ogre::SceneManager*);

        void updateDebug(float);

        void drawPath(Ogre::SceneManager*, std::list<GraphNode*>);
        void colorPath(Ogre::ColourValue);
        void removePath();

	private:
		/// <summary>store the value of width and height of the grids, the value is the number of grids</summary>
		int m_width,m_height;

        Ogre::Vector3 start_position;

		float m_widthPerGrid, m_heightPerGrid;

        std::list<Ogre::SceneNode*> *m_pathNodes;
        std::list<DebugObject*> *m_pathDebugObjects;

        Ogre::SceneManager *m_sceneManager;

		/// <summary>pointer storing the grid</summary>
		float **grid;

        bool m_debug;

        std::list<DebugObject*> *m_debugObjects;

		/// <summary>
		///  change the position in the game world coordinate to index in grids world
		/// </summary>
		/// <param name = 'pos'>the position in game world coordinate</param>
		/// <returns>return a vector2 containing the x,y index in grids world</returns>
		Ogre::Vector2 changePositionToIndex(Ogre::Vector3 pos);
	};
}
#endif