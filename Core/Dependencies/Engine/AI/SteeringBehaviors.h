#ifndef STEERINGBEHAVIORS_H
#define STEERINGBEHAVIORS_H

// std
#include <map>
#include <list>

// ogre
#include "OgrePrerequisites.h"
#include "OgreVector3.h"
#include "OgreString.h"

namespace GamePipe{
    class AIAgent;
    class AIAgentManager;
    class GraphNode;

    class SteeringBehavior
    {
    public:
        enum behavior_type
        {
            none               = 0x00000,
            seek               = 0x00002,
            flee               = 0x00004,
            arrive             = 0x00008,
            wander             = 0x00010,
            cohesion           = 0x00020,
            separation         = 0x00040,
            allignment         = 0x00080,
            obstacle_avoidance = 0x00100,
            wall_avoidance     = 0x00200,
            follow_path        = 0x00400,
            pursuit            = 0x00800,
            evade              = 0x01000,
            interpose          = 0x02000,
            hide               = 0x04000,
            flock              = 0x08000,
            offset_pursuit     = 0x10000,
        };

        //Map Used to define all the behaviors
        std::map<Ogre::String, behavior_type> behaviorType;

        //these can be used to keep track of friends, pursuers, or prey
        AIAgent* m_pTargetAgent1;
        AIAgent* m_pTargetAgent2;

        std::list<GraphNode*> m_path;

    protected:	
        AIAgent *m_pVehicle;

        //binary flags to indicate whether or not a behavior should be active
        int m_iFlags;

        //this function tests if a specific bit of m_iFlags is set
        bool On(behavior_type bt){return (m_iFlags & bt) == bt;}

        //Map Used to define all the parameters
        std::map<Ogre::String, Ogre::Real> params;

    public: 
        //Setting the Parameters
        void setParams(Ogre::String name,Ogre::Real value);
        Ogre::Real getParams(Ogre::String name);

        //Targets for Steering Behaviors
        Ogre::Vector3 m_vSeekTarget;
        Ogre::Vector3 m_vFleeTarget;
        Ogre::Vector3 m_vArriveTarget;

        SteeringBehavior(AIAgent *agent);

        void SetTargetAgent1(AIAgent* Agent){m_pTargetAgent1 = Agent;}
        void SetTargetAgent2(AIAgent* Agent){m_pTargetAgent2 = Agent;}

        void FleeOn(){m_iFlags |= flee;}
        void SeekOn(Ogre::Vector3 dest){m_iFlags |= seek; m_vSeekTarget = dest;}
        void ArriveOn(){m_iFlags |= arrive;}
        void WanderOn(){m_iFlags |= wander;}
        void PursuitOn(AIAgent* v){m_iFlags |= pursuit; m_pTargetAgent1 = v;}
        void EvadeOn(AIAgent* v){m_iFlags |= evade; m_pTargetAgent2 = v;}
        void CohesionOn(){m_iFlags |= cohesion;}
        void SeparationOn(){m_iFlags |= separation;}
        void AlignmentOn(){m_iFlags |= allignment;}
        void ObstacleAvoidanceOn(){m_iFlags |= obstacle_avoidance;}
        void WallAvoidanceOn(){m_iFlags |= wall_avoidance;}
        void FollowPathOn(std::list<GraphNode*> path){m_iFlags |= follow_path; m_path = path;}
        void InterposeOn(AIAgent* v1, AIAgent* v2){m_iFlags |= interpose; m_pTargetAgent1 = v1; m_pTargetAgent2 = v2;}
        void HideOn(AIAgent* v){m_iFlags |= hide; m_pTargetAgent1 = v;}
        void FlockingOn(){CohesionOn(); AlignmentOn(); SeparationOn(); WanderOn();}

        void FleeOff()  {if(On(flee))   m_iFlags ^=flee;}
        void SeekOff()  {if(On(seek))   m_iFlags ^=seek;}
        void ArriveOff(){if(On(arrive)) m_iFlags ^=arrive;}
        void WanderOff(){if(On(wander)) m_iFlags ^=wander;}
        void PursuitOff(){if(On(pursuit)) m_iFlags ^=pursuit;}
        void EvadeOff(){if(On(evade)) m_iFlags ^=evade;}
        void CohesionOff(){if(On(cohesion)) m_iFlags ^=cohesion;}
        void SeparationOff(){if(On(separation)) m_iFlags ^=separation;}
        void AlignmentOff(){if(On(allignment)) m_iFlags ^=allignment;}
        void ObstacleAvoidanceOff(){if(On(obstacle_avoidance)) m_iFlags ^=obstacle_avoidance;}
        void WallAvoidanceOff(){if(On(wall_avoidance)) m_iFlags ^=wall_avoidance;}
        void FollowPathOff(){if(On(follow_path)) m_iFlags ^=follow_path;}
        void InterposeOff(){if(On(interpose)) m_iFlags ^=interpose;}
        void HideOff(){if(On(hide)) m_iFlags ^=hide;}
        void OffsetPursuitOff(){if(On(offset_pursuit)) m_iFlags ^=offset_pursuit;}
        void FlockingOff(){CohesionOff(); AlignmentOff(); SeparationOff(); WanderOff();}

        bool isFleeOn(){return On(flee);}
        bool isSeekOn(){return On(seek);}
        bool isArriveOn(){return On(arrive);}
        bool isWanderOn(){return On(wander);}
        bool isPursuitOn(){return On(pursuit);}
        bool isEvadeOn(){return On(evade);}
        bool isCohesionOn(){return On(cohesion);}
        bool isSeparationOn(){return On(separation);}
        bool isAlignmentOn(){return On(allignment);}
        bool isObstacleAvoidanceOn(){return On(obstacle_avoidance);}
        bool isWallAvoidanceOn(){return On(wall_avoidance);}
        bool isFollowPathOn(){return On(follow_path);}
        bool isInterposeOn(){return On(interpose);}
        bool isHideOn(){return On(hide);}
        bool isOffsetPursuitOn(){return On(offset_pursuit);}

        virtual ~SteeringBehavior();
    };
}
#endif