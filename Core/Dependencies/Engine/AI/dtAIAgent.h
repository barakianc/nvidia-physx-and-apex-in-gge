#pragma once

#define lua_bindAI(functionPointer, functionName, luaVM) \
	lua_pushlightuserdata(luaVM, this);\
	lua_pushcclosure(luaVM, functionPointer, 1);\
	lua_setglobal(luaVM, functionName);

#define lua_unBindAI(functionName, luaVM) \
	lua_pushnil(luaVM); \
	lua_setglobal(luaVM, functionName);

#include "OgreVector3.h"
#include "OgrePrerequisites.h"
#include "OgreString.h"

#include "AIGroup.h"

#include "Ogre.h"
#include "Engine.h"
#include "DetourNavMeshQuery.h"
#include "DetourObstacleAvoidance.h"
#include "DetourLocalBoundary.h"
#include "DetourPathCorridor.h"
#include "DetourProximityGrid.h"
#include "DetourPathQueue.h"
#include "DetourNavMesh.h"
#include "InputGeom.h"

#include "opensteer\Vec3.h"
#include "AISteeringBehavior.h"
#include <list>
#include <vector>
#include "GraphicsObject.h"

static const Ogre::String AGENT_ENTITY_PREFIX = "Agent_Entity_";
static const Ogre::String AGENT_NODE_PREFIX = "Agent_Node_";
static const int AGENT_MAX_TRAIL = 64;
static const int DT_CROWDAGENT_MAX_NEIGHBOURS = 6;
static const int DT_CROWDAGENT_MAX_CORNERS = 4;

class dtCrowd;
class dtTarget;
struct lua_State;
class PhysicsObject;
class CollisionListener;
class TextFileManager;
class GraphicsObject;
class AISteeringBehavior;
class dtDebugObject;
class dtDebugLine;

namespace GamePipe
{
	class DebugObject;
}

namespace Ogre{
	class SceneNode;
	class LogManager;
	class Entity;
}

namespace AnimationManager{
	class AnimatedEntity;
}





//struct AgentTrail
//{
//	float trail[AGENT_MAX_TRAIL*3];
//	int htrail;
//};

struct dtCrowdNeighbour
{
	int idx;
	float dist;
};

struct dtCrowdAgentAnimation
{
	unsigned char active;
	float initPos[3], startPos[3], endPos[3];
	dtPolyRef polyRef;
	float t, tmax;
};

struct dtCrowdAgentParams
{
	float radius;
	bool radiusXOrZ;
	float height;
	float maxAcceleration;
	float maxSpeed;
	float collisionQueryRange;
	float pathOptimizationRange;
	float separationWeight;
	unsigned char updateFlags;
	unsigned char obstacleAvoidanceType;
	void* userData;
};

using namespace GamePipe;

class dtAIAgent
{
public:


	CollisionListener *m_collisionListener;

	AISteeringBehavior *m_steeringBehavior;
	dtDebugObject *m_text;

protected:
	bool m_debug;
	bool m_debugPosition;
	bool m_drawDebugInfo;
	std::list<dtDebugLine*> *m_dtLines;


private:
	Ogre::Radian m_radPrev;
	int m_agentID;
	class dtCrowd* m_crowd;
	GameObject* m_agentGameObject;

	float m_yOffset;

	int direction;

	//Ogre::String* m_agentMaterial;
	bool m_isAgentActive;
	bool m_hasSlowedDown;
	bool m_switchAttack;
	Ogre::String behaviorName;
	//from struct dtCrowdAgent
	unsigned char state;
	dtTarget *m_activeTarget;

	dtPathCorridor m_agentCorridor;
	dtLocalBoundary m_agentBoundary;
	float topologyOptTime;
	dtCrowdNeighbour neis[DT_CROWDAGENT_MAX_NEIGHBOURS];
	int nneis;
	float desiredSpeed;
	float npos[3];
	float disp[3];
	float dvel[3];
	float nvel[3];
	float vel[3];
	float gvel[3];
	dtCrowdAgentParams params;

	Ogre::LogManager *m_log;

	
	Ogre::String m_luaFileName;

	
	lua_State* m_luaVM;

	bool m_enableLua;

	bool m_luaInitilize;

	int m_luaStackSize;
	  Ogre::String m_name;

	  Ogre::Vector3 m_position;

	  Ogre::String m_state;
	  unsigned long m_type;

	static TextFileManager *m_tfm;


	Ogre::Vector3 m_moveTo;


	float m_blendFrame;
	

	bool m_remove;

	int ncorners;
	
	AISteeringBehavior* m_AISteeringBehavior;

	Ogre::String m_animationIdle;
	Ogre::String m_animationRun;

public:
	friend AISteeringBehavior;
	OpenSteer::AVGroup flock;
	float cornerVerts[DT_CROWDAGENT_MAX_CORNERS*3];
	unsigned char cornerFlags[DT_CROWDAGENT_MAX_CORNERS];
	dtPolyRef cornerPolys[DT_CROWDAGENT_MAX_CORNERS];

	dtAIAgent( dtCrowd* crowd = NULL, int idx = -1, Ogre::Vector3 pos = Ogre::Vector3::ZERO );
	~dtAIAgent();

	void activateAgent( Ogre::String meshName, Ogre::String hkxName, Ogre::String type, Ogre::String luaFile="", Ogre::String materialName="",GameObject* gObj=NULL);
	void deactivateAgent();

	void update(const float currentTime, const float deltaTime);
	void updateLua();
	void update(const float currentTime, const float deltaTime, class InputGeom *inputGeom);
	void updateSteering(float dt, dtNavMeshQuery* navquery, const dtQueryFilter* filter, const float* ext, dtTarget *targets, InputGeom *inputGeom);
	void updateGameObject(float dt);
	void render(Ogre::ManualObject* dd, char* materialName);

	bool isAgentActive() { return this->m_isAgentActive; }

	void readAnimationFile( char*, const char * );

	void setAgentPositionAbsolute( Ogre::Vector3 pos, float dt = 0 );
	void setAgentPositionAbsolute( float *pos, float dt = 0);
	void setAgentPositionAbsolute( float x, float y, float z, float dt = 0 );
	bool setAgentPositionRelative( Ogre::Vector3 pos, float dt = 0 );
	bool setAgentPositionRelative( float *pos, float dt = 0 );
	bool setAgentPositionRelative( float x, float y, float z, float dt = 0 );

	void setAgentVelocity( Ogre::Vector3 vel, float dt = 1);
	void setAgentVelocity( float *vel, float dt = 1);
	void setAgentVelocity( float x, float y, float z, float dt = 1 );
	void updateAgentParameters(const dtCrowdAgentParams* params);

	float* getAgentPosition() { return npos; }
	GraphicsObject* getAgentGraphicsObject(){ return m_agentGameObject->m_pGraphicsObject;}
	Ogre::Vector3 getAgentPositionV() { return Ogre::Vector3(npos[0], npos[1], npos[2]); }
	void setActive ( bool active ) { m_isAgentActive = active; }
	// Get functions
	//Ogre::String* getAgentMaterial() { return m_agentMaterial; }
	//AgentTrail getTrails() { return m_agentTrails; }
	int getNCorners() { return ncorners; }
	dtPathCorridor* getAgentCorridor() { return &m_agentCorridor; }
	int getAgentID() { return m_agentID; }
	dtLocalBoundary getAgentBoundary() { return m_agentBoundary; }
	dtCrowdAgentParams* getAgentParams() { return &params; }
	float getAgentTopologyOptTime() { return topologyOptTime; }
	float* getAgentNVel() { return nvel; }
	float* getAgentDVel() { return dvel; }
	float getAgentDVel( int i ) { return dvel[i]; }
	float* getAgentVel() { return vel; }
	Ogre::Vector3 getAgentVelV() { return Ogre::Vector3(vel[0], vel[1], vel[2]); }
	int getAgentdNNeis() { return nneis; }
	unsigned char getAgentState() { return state; }
	dtCrowdNeighbour* getAgentNeis() { return neis; }
	dtCrowdNeighbour getAgentNeis(int index) { return neis[index]; }
	float getAgentSpeed() { return desiredSpeed; }
	float* getAgentDisp() { return disp; }
	dtTarget* getAgentActiveTarget() { return m_activeTarget; }
	bool hasActiveTarget();
	float getMaxSpeed() { return this->getAgentParams()->maxSpeed; }
	float getRadius() { return this->getAgentParams()->radius; }
	float getYOffset(){ return m_yOffset;}
	void clearLines();
	Ogre::ManualObject* addToLines();
	//unsigned char getAgentCornerFlags() { return cornerFlags; }
	bool toggleDebugInfo(bool flag );
	AISteeringBehavior* getAISteeringBehavior() { return m_AISteeringBehavior; }

	// Set functions
	void setState( unsigned char new_state ) { state = new_state; }
	void setAgentTopologyOptTime( float time ) { topologyOptTime = time; }
	void setAgentNNeis( int n ) { nneis = n; }
	void setAgentDesiredSpeed( float speed ) { desiredSpeed = speed; }
	void setAgentActiveTarget( dtTarget* target ) { m_activeTarget = target; }
	void setAgentNCorners( int nc ) { ncorners = nc; }
	void setAgentID(int id) { this->m_agentID = id; }
	void setMaxSpeed(float maxSpeed);
	void setMaxAcceleration(float maxAcceleration);
	//void setAgentMaterial( Ogre::String* name ){ m_agentMaterial = name; }
	void setYOffset(float yOffset) { m_yOffset = yOffset;}
	void setSinbadMaterial(Ogre::String materialName);

	void makeIdle();
	void slowDownToIdle();
	void makeAttack();
	void slowDownToAttack();
	void fakeIdle();
	void switchToAnimation(Ogre::String newAnimationName, float transitionTime = 0.5f);

	void setWanderSteeringBehavior();
	void setSeekAndArriveSteeringBehavior(dtTarget* target);
	void setSeekAndArriveSteeringBehavior(float pos[3]);
	void setSeekAndArriveSteeringBehavior(float x, float y, float z);
	void setSeekAndAttackSteeringBehavior(dtTarget* target);
	void setSeekAndAttackSteeringBehavior(float x, float y, float z);
	void setSeekSteeringBehavior(dtTarget* target);
	void setSeekSteeringBehavior(float pos[3]);
	void setSeekSteeringBehavior(float x, float y, float z);
	void setFleeSteeringBehavior(dtTarget* target);
	void setFleeSteeringBehavior(Ogre::Vector3 targetPos);
	void setFlockingSteeringBehavior();
	void setPursuitSteeringBehavior(dtAIAgent *targetAgent);
	void setEvadeSteeringBehavior(dtAIAgent *targetAgent);	
	void setPursuitSteeringBehavior(int targetAgentID);
	void setEvadeSteeringBehavior(int targetAgentID);
	void setPathFollowingSteeringBehavior(dtTarget* target);

	void removeWanderSteeringBehavior();
	void removeSeekAndArriveSteeringBehavior();
	void removeSeekSteeringBehavior();
	void removeFleeSteeringBehavior();
	void removeFlockingSteeringBehavior();
	void removetPursuitSteeringBehavior();
	void removeEvadeSteeringBehavior();	
	void removePathFollowingSteeringBehavior();

	bool isIdle();
	bool hasWanderSteeringBehavior();
	bool hasSeekAndArriveSteeringBehavior();
	bool hasSeekSteeringBehavior();
	bool hasFleeSteeringBehavior();
	bool hasFlockingSteeringBehavior();
	bool hasPursuitSteeringBehavior(bool checkFakeIdle = true);
	bool hasPursuitSteeringBehaviorIdle();
	bool hasEvadeSteeringBehavior();
	bool hasPathFollowingSteeringBehavior();

	bool hasPathFindingSteeringBehavior(bool checkFakeIdle = true);

	GraphicsObject* getGraphicsObject() { return this->m_agentGameObject->m_pGraphicsObject; }

	Ogre::String getAgentEntityName();
	Ogre::String getAgentNodeName();
	void integrateNewSpeed( const float dt );
	float getDistanceToGoal( const float range );
	float getDistanceToCorner( const float range );
	void getAgentBounds(float* bmin, float* bmax);
	bool isAtGoal( );
	bool isNearGoal( const float range );
	bool overOffmeshConnection(const float radius);
	void calcSmoothSteerDirection(float* dir);
	void calcStraightSteerDirection(float* dir);
	int addToOptQueue(dtAIAgent** agents, const int nagents, const int maxAgents);

	void bindAI();
	Ogre::String getActiveAnimation();
	std::vector<std::string> getAnimations();
	float getBlendFrame();
	Ogre::String getDefaultAnimation();
	Ogre::Vector3 getDestination();
	int getKnownLuaStackSize();
	void initializeLua();
	void initialize();
	void reloadLua();
	void setOnBehaviors(Ogre::String name);
	void setArriveTarget(Ogre::Vector3 arriveTarget);
	void setFleeTarget(Ogre::Vector3 fleeTarget);
	void setSeekTarget(Ogre::Vector3 seekTarget);
	void setEnableLua(bool enable);
	bool playAnimation(Ogre::String newAnimName,bool loop,Ogre::String transitionAnimation="");
	bool setBlendFrame(float blendFrame);
	bool setDefautAnimation(Ogre::String defaultAnimName);

	void setDebugText( Ogre::String text);
	void setDebugBehavior( Ogre::String text );
	void setDebug( bool debug, bool debugPos);
	bool stopAnimation();
	
	//scale
	void scaleAgent(float xScale,float yScale,float zScale);

	void setAnimationNameIdle(Ogre::String sAnimationIdle);
	void setAnimationNameRun(Ogre::String sAnimationRun);
};

//}
