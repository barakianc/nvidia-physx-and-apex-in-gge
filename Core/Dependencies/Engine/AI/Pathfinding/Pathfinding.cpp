#include "StdAfx.h"
 #include "OgreVector3.h"

#include "Pathfinding.h"
#include "GridWorld.h"
#include "GraphEdge.h"
#include "GraphNode.h"
#include "GraphWorld.h"
#include <stack>
#include <math.h>

using namespace GamePipe;
using namespace std;

Ogre::Real Pathfinding::cost( GraphNode* left, GraphNode* right, GridWorld* world)
{
    Ogre::Vector3 leftPosition = left->getPos();
    Ogre::Vector3 rightPosition = right->getPos();

    return leftPosition.squaredDistance(rightPosition);
    //return fabs((leftPosition.x - rightPosition.x)) + fabs((leftPosition.y - rightPosition.y)) ;
};

bool Pathfinding::evaluateMove( closedSet &closed, openSet &open, int x, int y, 
                               node *current, GraphNode* goal, GridWorld* world)
{
    Ogre::Vector3 tempPosition = current->current->getPos();

    bool added = false;

    node *tempNode = new node();
    tempNode->cost = current->cost + world->getCost((int)tempPosition.x + x, (int)tempPosition.y + y) + 1;
    tempNode->parent = current;
    tempNode->current = new GraphNode(Ogre::Vector3(tempPosition.x + x, tempPosition.y + y, tempPosition.z));
    tempNode->cost += cost(current->current,goal,world);
    if(!closed[Pathfinding::toString(tempNode->current)] || 
        closed[Pathfinding::toString(tempNode->current)]->cost > tempNode->cost){
            open.push(tempNode);
            added = true;
    }

    return added;
};

list<GraphNode*> Pathfinding::findPath( GraphNode *start, GraphNode *goal, GridWorld *world, bool diagonal)
{
    map<Ogre::String, node*>closed;
    priority_queue<node*,vector<node*>,node_less>open;

    int width = world->getWidth();
    int height = world->getHeight();
    Ogre::Vector3 tempPosition;
    node *current;

    Ogre::Vector3 goalPosition = goal->getPos();

    node *startNode = new node();
    startNode->cost = 0;
    startNode->parent = NULL;
    startNode->current = start;

    open.push(startNode);

    while(open.size() > 0){
        current = open.top();
        open.pop();

        Ogre::String toString = Pathfinding::toString(current->current);

        // processed this node before, skipping.
        if(closed[toString]){
            continue;
        }

        closed[toString] = current;

        tempPosition = current->current->getPos();

        // goal node is the best current node, found a path.
        if(goalPosition.x == tempPosition.x && goalPosition.y == tempPosition.y){
            break;
        }

        //North
        if (tempPosition.y < (height - 1) && (world->getCost((int)tempPosition.x, (int)tempPosition.y + 1) >= 0)){
            Pathfinding::evaluateMove(closed, open, 0, 1, current, goal, world);

            //North East
            if (diagonal && tempPosition.x < (width - 1) && 
                (world->getCost((int)tempPosition.x + 1, (int)tempPosition.y + 1) >= 0)){
                    Pathfinding::evaluateMove(closed, open, 1, 1, current, goal, world);
            }
        }
        
        //East
        if (tempPosition.x < (width - 1) && (world->getCost((int)tempPosition.x + 1, (int)tempPosition.y) >= 0)){
            Pathfinding::evaluateMove(closed, open, 1, 0, current, goal, world);
            //South East
            if (diagonal && tempPosition.y > 0 && 
                (world->getCost((int)tempPosition.x + 1, (int)tempPosition.y - 1) >= 0)){
                    Pathfinding::evaluateMove(closed, open, 1, -1, current, goal, world);
            }
        }
        
        //South
        if (tempPosition.y > 0 && (world->getCost((int)tempPosition.x, (int)tempPosition.y - 1) >= 0)){
            Pathfinding::evaluateMove(closed, open, 0, -1, current, goal, world);
            //South West
            if (diagonal && tempPosition.y > 0 && 
                (world->getCost((int)tempPosition.x - 1, (int)tempPosition.y - 1) >= 0)){
                    Pathfinding::evaluateMove(closed, open, -1, -1, current, goal, world);
            }
        }
        
        //West
        if (tempPosition.x > 0 && (world->getCost((int)tempPosition.x - 1, (int)tempPosition.y) >= 0)){
            Pathfinding::evaluateMove(closed, open, -1, 0, current, goal, world);

            //North West
            if (diagonal && tempPosition.y < (height - 1) && 
                (world->getCost((int)tempPosition.x - 1, (int)tempPosition.y + 1) >= 0)){
                    Pathfinding::evaluateMove(closed, open, -1, +1, current, goal, world);
            }
        }
    }

    list<GraphNode*> nodes;

    while(current){
        nodes.push_front(current->current);
        current = current->parent;
    }

    return nodes;
};

list<GraphNode*> Pathfinding::findPath( GraphNode* start, GraphNode* goal, GraphWorld* world)
{
    map<Ogre::String, node*>closed;
    priority_queue<node*,vector<node*>,node_less>open;

    Ogre::Vector3 tempPosition;
    node *currentNode;
    std::list<GraphEdge*> tempEdges;
    std::list<GraphEdge*>::iterator tempIt;
    GraphNode *tempGraphNode;

    Ogre::Vector3 goalPosition = goal->getPos();

    node *startNode = new node();
    startNode->cost = 0;
    startNode->parent = NULL;
    startNode->current = start;

    node *tempNode;

    open.push(startNode);

    while(open.size() > 0){
        currentNode = open.top();
        open.pop();

        Ogre::String toString = Pathfinding::toString(currentNode->current);

        closed[toString] = currentNode;

        tempPosition = currentNode->current->getPos();

        // goal node is the best current node, found a path.
        if(goalPosition.x == tempPosition.x && goalPosition.y == tempPosition.y && goalPosition.z == tempPosition.z){
            break;
        }

        tempEdges = world->getEdgeList(currentNode->current);

        for(tempIt = tempEdges.begin(); tempIt != tempEdges.end(); tempIt++){
            tempGraphNode = (*tempIt)->getStartNode()->equal(currentNode->current) ? 
                (*tempIt)->getEndNode(): (*tempIt)->getStartNode();

            tempNode = new node();
            tempNode->cost = currentNode->cost + (*tempIt)->getCost() + 1;
            tempNode->parent = currentNode;
            tempNode->current = tempGraphNode;

            if(!closed[Pathfinding::toString(tempNode->current)] || 
                closed[Pathfinding::toString(tempNode->current)]->cost > tempNode->cost){

                    open.push(tempNode);
            }
        }
    }

    list<GraphNode*> nodes;

    while(currentNode){
        nodes.push_front(currentNode->current);
        currentNode = currentNode->parent;
    }

    return nodes;
};

Ogre::String Pathfinding::toString( GraphNode* currentNode )
{
    Ogre::Vector3 position = currentNode->getPos();

    int max = (position.x > position.y) ? (int)position.x : (int)position.y;
    max = (max > position.z) ? max : (int)position.z;

    char buff[sizeof(max)*8+1];
    _itoa_s((int)position.x,buff,10);

    Ogre::String positionString = "";
    positionString.append(buff);
    positionString.append("x");

    _itoa_s((int)position.y,buff,10);
    positionString.append(buff);
    positionString.append("y");

    _itoa_s((int)position.z,buff,10);
    positionString.append(buff);
    positionString.append("z");

    return positionString;
};
