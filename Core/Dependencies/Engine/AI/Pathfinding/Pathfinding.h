#ifndef PATHFINDING_H
#define PATHFINDING_H

#include "OgreString.h"
#include "OgrePrerequisites.h"

#include <list>
#include <map>
#include <queue>
#include <vector>

namespace GamePipe{
    class GraphNode;
    class GridWorld;
    class GraphEdge;
    class GraphWorld;

    class Pathfinding{
    public:
        static std::list<GraphNode*> findPath(GraphNode*, GraphNode*, GridWorld*, bool = false);
        static std::list<GraphNode*> findPath(GraphNode*, GraphNode*, GraphWorld*);
    protected:
        struct node{
            node* parent;
            GraphNode* current;
            Ogre::Real cost;
        };

        struct node_less{
            bool operator()(const node* left, const node* right) const {
                return left->cost > right->cost;
            };
        };

        static Ogre::Real cost(GraphNode*, GraphNode*, GridWorld*);
        static Ogre::Real cost(GraphNode*, GraphNode*, GraphWorld*);
    private:
        typedef std::map<Ogre::String, node*> closedSet;
        typedef std::priority_queue<node*,std::vector<node*>,node_less> openSet;

        static Ogre::String toString(GraphNode*);
        static bool evaluateMove(closedSet&, openSet&, int, int, node*, GraphNode*,
            GridWorld*);
    };
};

#endif