#pragma once

#include "Ogre.h"
#include "OpenSteer\SimpleVehicle.h"

class AISteeringBehavior : public OpenSteer::SimpleVehicle
{
private:
	//binary flags to indicate whether or not a behavior should be active
	int m_iFlags;

	//these can be used to keep track of friends, pursuers, or prey
	class dtAIAgent* m_pPursuitTargetAgent;
	class dtAIAgent* m_pEvadeTargetAgent;
	
	//Targets for Steering Behaviors
	Ogre::Vector3 m_vSeekTarget;
	Ogre::Vector3 m_vFleeTarget;

protected:

public:
	enum behavior_type
	{
		none               = 0x00000,
		seek               = 0x00002,
		flee               = 0x00004,
		arrive             = 0x00008,
		wander             = 0x00010,
		cohesion           = 0x00020,
		separation         = 0x00040,
		allignment         = 0x00080,
		obstacle_avoidance = 0x00100,
		wall_avoidance     = 0x00200,
		follow_path        = 0x00400,
		pursuit            = 0x00800,
		evade              = 0x01000,
		interpose          = 0x02000,
		hide               = 0x04000,
		flock              = 0x08000,
		offset_pursuit     = 0x10000,
		fake_idle		   = 0x20000,
	};

	//Map Used to define all the behaviors
	std::map<Ogre::String, behavior_type> behaviorType;

	//this function tests if a specific bit of m_iFlags is set
	bool On(behavior_type bt);

	friend class dtAIAgent;

	AISteeringBehavior(void);
	~AISteeringBehavior(void);

	Ogre::Vector3 getSteeringForce( float deltaTime , OpenSteer::AVGroup group, int agentID = 0 );

	void applySteeringForce (const OpenSteer::Vec3& force,	const float elapsedTime);
	void smoothSpeed(const OpenSteer::Vec3& force, const float elapsedTime);
	void updatePosition(const float x, const float y, const float z, const float elapsedTime);

	void setSpeed(Ogre::Vector3 speed, float dt);
	void setSpeed(float *speed, float dt);
	void setSpeed(float speedX, float speedY, float speedZ, float dt);
	void update(const float currentTime, const float deltaTime){}

	void FleeOn(Ogre::Vector3 target){m_iFlags |= flee; m_vFleeTarget = target; }
	void SeekOn(Ogre::Vector3 dest){m_iFlags |= seek; m_vSeekTarget = dest;}
	void ArriveOn(){m_iFlags |= arrive;}
	void WanderOn(){m_iFlags |= wander;}
	void PursuitOn(class dtAIAgent* v){m_iFlags |= pursuit; m_pPursuitTargetAgent = v;}
	void EvadeOn(class dtAIAgent* v){m_iFlags |= evade; m_pEvadeTargetAgent = v;}
	void CohesionOn(){m_iFlags |= cohesion;}
	void SeparationOn(){m_iFlags |= separation;}
	void AlignmentOn(){m_iFlags |= allignment;}
	void ObstacleAvoidanceOn(){m_iFlags |= obstacle_avoidance;}
	void WallAvoidanceOn(){m_iFlags |= wall_avoidance;}
	void FollowPathOn(){m_iFlags |= follow_path;}
	void InterposeOn(class dtAIAgent* v1, class dtAIAgent* v2){m_iFlags |= interpose; m_pPursuitTargetAgent = v1; m_pEvadeTargetAgent = v2;}
	void HideOn(class dtAIAgent* v){m_iFlags |= hide; m_pPursuitTargetAgent = v;}
	void FlockingOn(){CohesionOn(); AlignmentOn(); SeparationOn(); WanderOn();}
	void FakeIdleOn() { m_iFlags |= fake_idle; }

	void FleeOff()  {if(On(flee))   m_iFlags ^=flee;}
	void SeekOff()  {if(On(seek))   m_iFlags ^=seek;}
	void ArriveOff(){if(On(arrive)) m_iFlags ^=arrive;}
	void WanderOff(){if(On(wander)) m_iFlags ^=wander;}
	void PursuitOff(){if(On(pursuit)) m_iFlags ^=pursuit;}
	void EvadeOff(){if(On(evade)) m_iFlags ^=evade;}
	void CohesionOff(){if(On(cohesion)) m_iFlags ^=cohesion;}
	void SeparationOff(){if(On(separation)) m_iFlags ^=separation;}
	void AlignmentOff(){if(On(allignment)) m_iFlags ^=allignment;}
	void ObstacleAvoidanceOff(){if(On(obstacle_avoidance)) m_iFlags ^=obstacle_avoidance;}
	void WallAvoidanceOff(){if(On(wall_avoidance)) m_iFlags ^=wall_avoidance;}
	void FollowPathOff(){if(On(follow_path)) m_iFlags ^=follow_path;}
	void InterposeOff(){if(On(interpose)) m_iFlags ^=interpose;}
	void HideOff(){if(On(hide)) m_iFlags ^=hide;}
	void OffsetPursuitOff(){if(On(offset_pursuit)) m_iFlags ^=offset_pursuit;}
	void FlockingOff(){CohesionOff(); AlignmentOff(); SeparationOff(); WanderOff();}
	void FakeIdleOff(){m_iFlags ^=fake_idle;}
	void AllOf() { this->m_iFlags = none; }

	bool isFleeOn(){return On(flee);}
	bool isSeekOn(){return On(seek);}
	bool isArriveOn(){return On(arrive);}
	bool isWanderOn(){return On(wander);}
	bool isPursuitOn(){return On(pursuit);}
	bool isEvadeOn(){return On(evade);}
	bool isCohesionOn(){return On(cohesion);}
	bool isSeparationOn(){return On(separation);}
	bool isAlignmentOn(){return On(allignment);}
	bool isObstacleAvoidanceOn(){return On(obstacle_avoidance);}
	bool isWallAvoidanceOn(){return On(wall_avoidance);}
	bool isFollowPathOn(){return On(follow_path);}
	bool isInterposeOn(){return On(interpose);}
	bool isHideOn(){return On(hide);}
	bool isOffsetPursuitOn(){return On(offset_pursuit);}
	bool isFlockingOn(){ return On(cohesion) & On(allignment) & On(separation) & On(wander);}
	bool isFakeIdleOn(){return On(fake_idle);}
	bool isIdle() { return (m_iFlags == 0); }
};

