#ifndef AIAGENT_BINDING_H
#define AIAGENT_BINDING_H

#include "lua.hpp"
#include "AIAgent.h"

#include "Ogre.h"
#include "OgreVector3.h"
#include "OgreString.h"
#include "OgreLogManager.h"
#include "OgreLog.h"

namespace GamePipe
{
    static int l_getDestination(lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

        int stackDiff = ai->getKnownLuaStackSize() - lua_gettop(luaVM);

        Ogre::Vector3 position;

        if (!stackDiff){
            // requesting own destination
            position = ai->getDestination();
        }
        else if(stackDiff == 1){
            // request another AI's destination
            if(lua_islightuserdata(luaVM, -1)){
                AIAgent *otherAI = (AIAgent*)lua_topointer(luaVM, -1);
                position = otherAI->getDestination();
            }
            else{
                Ogre::String error = "LUA ERROR: C++ getDestination expected a ";
                error.append("AIAgent pointer");

                Ogre::LogManager::getSingleton().logMessage(error, 
                    Ogre::LML_CRITICAL);

                lua_remove(luaVM, -1);
            }
        }
        else{
            char buff[sizeof(stackDiff)*8+1];
            _itoa_s(stackDiff,buff,10);
            // error
            Ogre::String error = "LUA ERROR: C++ getDestination expected at most ";
            error.append("1 argument but received ");
            error.append(buff);
            error.append(" arguments");

            Ogre::LogManager::getSingleton().logMessage(error, 
                Ogre::LML_CRITICAL);

            // return lua stack to stable state.
            for (int i = 0; i < stackDiff; i++){
                lua_remove(luaVM, -1);
            }

            return 0;
        }

        lua_pushnumber(luaVM, position.x);
        lua_pushnumber(luaVM, position.y);
        lua_pushnumber(luaVM, position.z);

        return 3;
    };

    static int l_setDebugText(lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

        ai->setDebugText(lua_tostring(luaVM, -1));

        return 0;
    };

    static int l_setDestination(lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

        ai->setDestination(Ogre::Vector3(
            (float)lua_tonumber(luaVM, -3),
            (float)lua_tonumber(luaVM, -2),
            (float)lua_tonumber(luaVM, -1)));

        return 0;
    };

    static int l_setMaxLinearSpeed( lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

        ai->setMaxLinearSpeed((float)lua_tonumber(luaVM, -1));

        return 0;
    };

    static int l_getPosition( lua_State *luaVM )
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));


        Ogre::Vector3 position = ai->getPosition();

        lua_pushnumber(luaVM, position.x);
        lua_pushnumber(luaVM, position.y);
        lua_pushnumber(luaVM, position.z);

        return 3;
    };

	static int l_setSeekTarget( lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		 ai->setSeekTarget(Ogre::Vector3(
            (float)lua_tonumber(luaVM, -3),
            (float)lua_tonumber(luaVM, -2),
            (float)lua_tonumber(luaVM, -1)));


        return 0;
    };

	static int l_setFleeTarget( lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		 ai->setFleeTarget(Ogre::Vector3(
            (float)lua_tonumber(luaVM, -3),
            (float)lua_tonumber(luaVM, -2),
            (float)lua_tonumber(luaVM, -1)));


        return 0;
    };

	static int l_setOnBehaviors( lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		 ai->setOnBehaviors(Ogre::String(lua_tostring(luaVM,-1)));


        return 0;
    };

	static int l_setArriveTarget( lua_State *luaVM)
    {
        AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		 ai->setArriveTarget(Ogre::Vector3(
            (float)lua_tonumber(luaVM, -3),
            (float)lua_tonumber(luaVM, -2),
            (float)lua_tonumber(luaVM, -1)));

        return 0;
    };

	static int l_playAnimation( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		bool loop = lua_toboolean(luaVM, -1) == 0? false : true;
		ai->playAnimation(lua_tostring(luaVM,-2),loop);

		return 0;
	};

	static int l_transitAndPlayAnimation( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM, lua_upvalueindex(1));

		bool loop = lua_toboolean(luaVM, -1) == 0? false : true;
		ai->playAnimation(lua_tostring(luaVM, -3),loop, lua_tostring(luaVM, -2));

		return 0;
	};
    
	static int l_stopAnimation( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		ai->stopAnimation();
		return 0;
	};

	static int l_setDefaultAnimation( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		ai->setDefautAnimation(lua_tostring(luaVM,-1));
		return 0;
	};

	static int l_getDefaultAnimation( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		Ogre::String animName = ai->getDefaultAnimation();
		lua_pushstring(luaVM, animName.c_str());
		return 1;
	};

	static int l_setBlendFrame( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		ai->setBlendFrame((float)lua_tonumber(luaVM,-1));
		return 0;
	};

	static int l_getBlendFrame( lua_State *luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		float blendFrame = ai->getBlendFrame();
		lua_pushnumber(luaVM, blendFrame);
		return 1;
	};

	static int l_getActiveAnimation( lua_State* luaVM)
	{
		AIAgent *ai = (AIAgent*)lua_topointer(luaVM,lua_upvalueindex(1));

		Ogre::String animName = ai->getActiveAnimation();
		lua_pushstring(luaVM, animName.c_str());
		return 1;
	}
};

#endif