#include "StdAfx.h"
#ifdef GLE_EDITOR
// Base Includes
#include "Engine.h"
//#include "GameScreen.h"

// Other
#include <OgreMath.h>

// Editor Includes
#include "Editor.h"

#ifdef HAVOK
#include "HavokWrapper.h"
#endif

#ifdef PHYSX
#include "PhysXObject.h"
#endif

#include <vector>

// -------------------------------------------------------
// Chain of command includes
// -------------------------------------------------------
#include "EditorMsgManager/IMEssageHandle.h"
#include "EditorMsgManager/EditorMsgManager_ScreenActivationKeyboardInput.h"
#include "EditorMsgManager/EditorMsgManager_SceneNodePRoperties.h"
#include "EditorMsgManager/EditorMsgManager_SceneNodeAndManager.h"
#include "EditorMsgManager/EditorMsgManager_ParticleSystem.h"
#include "EditorMsgManager/EditorMsgManager_MouseInput.h"
#include "EditorMsgManager/EditorMsgManager_LightComponents.h"
#include "EditorMsgManager/EditorMsgManager_GLEBaseProperties.h"
#include "EditorMsgManager/EditorMsgManager_GLE_Zoom.h"
#include "EditorMsgManager/EditorMsgManager_GLE_Set_Object_Properties.h"
#include "EditorMsgManager/EditorMsgManager_GLE_Query_SceneNode_Children.h"
#include "EditorMsgManager/EditorMsgManager_GLE_Query_Object_Properties.h"
#include "EditorMsgManager/EditorMsgManager_GLE_Focus.h"
#include "EditorMsgManager/EditorMsgManager_GLE_Entity_Create.h"
#include "EditorMsgManager/EditorMsgManager_GLE_AttachObject.h"
#include "EditorMsgManager/EditorMsgManager_AnimationSystem.h"
#include "EditorMsgManager/EditorMsgManager_SkyBoxSubSystem.h"

// Debug output
#define GLE_DEBUG_NETWORK
using namespace std;

namespace GamePipe
{
	/*! Constructor
	*/
	Editor::Editor()
	{
		// Init some variables that the editor needs
		m_pSceneNodeSelected	= NULL;

		// Default values for the editor
		SetEditorMode(false);
		SetUseEditorInput(false);
		SetEditorConnectionStatus (GGE_DISCONNECTED);

		// Input
		ed_MouseState.clear();
		ed_KeyCode				= 100000;
		ed_KeyPress				= false;

		// Editor cameras speed
		m_fCameraMovementSpeed	= 0.05f;
		m_fCameraRotationSpeed	= 0.13f;

		// Animations
		m_CurrentAnimationName = "";
		m_sMVCurrentGameObjectName = "";
		m_pMVGameObject			= NULL;

		// Init message handles
		InitMessageHandels();
	}

	/*! Destructor
	*/
	Editor::~Editor()
	{

	}

	/*! This function initializes the chain of command system that handles the messages as they income
	*/
	void Editor::InitMessageHandels()
	{
		m_vMessageHandles.push_back(new EditorMsgManager_ScreenActivationKeyboardInput(this));
		m_vMessageHandles.push_back(new EditorMsgManager_SceneNodePRoperties(this));
		m_vMessageHandles.push_back(new EditorMsgManager_SceneNodeAndManager(this));
		m_vMessageHandles.push_back(new EditorMsgManager_ParticleSystem(this));

		m_vMessageHandles.push_back(new EditorMsgManager_MouseInput(this));
		m_vMessageHandles.push_back(new EditorMsgManager_LightComponents(this));
		m_vMessageHandles.push_back(new EditorMsgManager_GLE_Zoom(this));
		m_vMessageHandles.push_back(new EditorMsgManager_GLE_Set_Object_Properties(this));

		m_vMessageHandles.push_back(new EditorMsgManager_GLE_Query_SceneNode_Children(this));
		m_vMessageHandles.push_back(new EditorMsgManager_GLE_Query_Object_Properties(this));
		m_vMessageHandles.push_back(new EditorMsgManager_GLE_Focus(this));
		m_vMessageHandles.push_back(new EditorMsgManager_GLE_Entity_Create(this));

		m_vMessageHandles.push_back(new EditorMsgManager_GLE_AttachObject(this));
		m_vMessageHandles.push_back(new EditorMsgManager_AnimationSystem(this));
		m_vMessageHandles.push_back(new EditorMsgManager_GLEBaseProperties(this));

		m_vMessageHandles.push_back(new EditorMsgManager_SkyBoxSubSystem(this));
	}

	/*! This function is called every frame, it is actually in charge of changing the connection states.
	*	@return	S_OK if everything went well
	*/
	int Editor::UpdateEditor()
	{
		// In charge of changing the states of the app
		switch( GetEditorConnectionStatus() )
		{
		case GGE_DISCONNECTED:
			Editor_StartServer();
			break;
		case GGE_READY_TO_ACCEPT:
			Editor_AcceptConnection();
			break;
		case GGE_CONNECTED:
			Editor_Receive();
			break;
		case GGE_ERROR:
			// Reset the connection and 
			closesocket( m_sockListen );
			closesocket( m_sockMessages );
			WSACleanup();
			// Restart the server
			SetEditorConnectionStatus( GGE_DISCONNECTED );
			break;
		}

		//If there is an active animation
		if(m_pVecGameScreens->size())
		{			
			//if (m_pMVGameObject != NULL)
			//{
				//m_pMVGameObject->update();
			//}
		}

		return S_OK;
	}
	
	Ogre::Entity*	Editor::GetMVEntity()
	{
		if (m_pMVGameObject==NULL)
		{
			return NULL;
		}
		else
		{
			if (m_pMVGameObject->m_pGraphicsObject == NULL)
			{
				return NULL;
			}
			else
			{
				return m_pMVGameObject->m_pGraphicsObject->m_pOgreEntity;
			}
		}
	}
	Ogre::SceneNode* Editor::GetMVSceneNode()
	{
		if (m_pMVGameObject==NULL)
		{
			return NULL;
		}
		else
		{
			if (m_pMVGameObject->m_pGraphicsObject == NULL)
			{
				return NULL;
			}
			else
			{
				return m_pMVGameObject->m_pGraphicsObject->m_pOgreSceneNode;
			}
		}
	}
	/*! Start a server in port 19000 if it is the editor or 19001 if it is the modelviewer
	*	@return	S_OK if everything went well
	*/
	int Editor::Editor_StartServer()
	{
		// Connecting
		WSADATA					wsaData;
		int						socket_type		= SOCK_STREAM;
//HACK ports are hard coded here, but not in GLE
#ifdef GLE_EDITOR_MODELVIEW
		unsigned short			port			= 19001;
#else
		unsigned short			port			= 19000;
#endif
		char					*pInterface		= NULL;
		struct	sockaddr_in		local;


		// Start winsock
		if( WSAStartup(0x202,&wsaData ) == SOCKET_ERROR ) 
		{
			GGETRACELOG("WSAStartup failed with error %d", WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}
		local.sin_family		= AF_INET;
		local.sin_addr.s_addr	= (!pInterface)?INADDR_ANY:inet_addr(pInterface); 
		local.sin_port			= htons(port);

		// Start the socket
		m_sockListen = socket(AF_INET, socket_type, 0); // TCP socket
		if(m_sockListen == INVALID_SOCKET)
		{
			GGETRACELOG("socket() failed with error %d",WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}

		// bind() associates a local address and port combination with the socket just created.
		if(::bind(m_sockListen,(struct sockaddr*)&local,sizeof(local) ) == SOCKET_ERROR) 
		{
			GGETRACELOG("bind() failed with error %d, on port: %d",WSAGetLastError(), port);
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}

		// Since the connection is TCP we can LISTEN
		if (listen(m_sockListen, 1) == SOCKET_ERROR) 
		{
			GGETRACELOG("listen() failed with error %d",WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}

		// Change the status, from now on we are going to check if GLE want to connect to GGE
		SetEditorConnectionStatus( GGE_READY_TO_ACCEPT );

		return 1;
	}

	/*! Check if there is a client (non-blocking)
	*	@return	S_OK if everything went well
	*/
	int Editor::Editor_AcceptConnection()
	{
		FD_SET					set;
		timeval					waitTimeStr;
		struct	sockaddr_in		from;
		int						fromlen	= 0;
		int						retval	= 0;

		// Check if there is a message to read (non-blocking)
		FD_ZERO( &set);
		FD_SET( m_sockListen, &set);
		waitTimeStr.tv_sec = 0;
		waitTimeStr.tv_usec =0;
		retval = select( FD_SETSIZE, &set, NULL, NULL, &waitTimeStr );
		if( retval == 0)				return 0;
		if( retval == SOCKET_ERROR) 	
		{
			GGETRACELOG("select() error %d",WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}

		// Accept a client
		fromlen = sizeof(from);
		m_sockMessages = accept(m_sockListen,(struct sockaddr*)&from, &fromlen);
		if(m_sockMessages == INVALID_SOCKET) 
		{
			GGETRACELOG("accept() error %d",WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}

		int flag = 1;
		int result = setsockopt(m_sockMessages,            /* socket affected */
			IPPROTO_TCP,     /* set option at TCP level */
			TCP_NODELAY,     /* name of option */
			(char *) &flag,  /* the cast is historical
							 cruft */
							 sizeof(int)); 

		GGETRACELOG("GLE connected from %s, port %d", inet_ntoa(from.sin_addr), htons(from.sin_port)) ;

		// Update the status
		SetEditorConnectionStatus (GGE_CONNECTED);

		// Send Ready to GLE so that we can start editing !
		Editor_GenericCommand(GGE_Ready, "0");// Sends the GGE_Ready message

		
		//Dynamic object editing tool
		
		CUBE_SIZE = 10;
		Ogre::MaterialPtr red = Ogre::MaterialManager::getSingleton().create("RedMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		// Get a material pass
		Ogre::Pass *m_pPass = red->getTechnique(0)->getPass(0);
		// Creating texture unit
		m_pPass->setDiffuse(1,0,0,1);
		m_pPass->setAmbient(1,0,0);
		m_pPass->setDepthCheckEnabled(false);
		m_pPass->setLightingEnabled(true);
		red->load();
		
		
		Ogre::MaterialPtr blue = Ogre::MaterialManager::getSingleton().create("BlueMaterial", "General", true);
		blue->setDiffuse(0,0,1,1);
		blue->setAmbient(0,0,1);
		blue->setReceiveShadows(false);
		blue->setDepthCheckEnabled(false);
		Ogre::MaterialPtr green = Ogre::MaterialManager::getSingleton().create("GreenMaterial", "General", true);
		green->setDiffuse(0,1,0,1);
		green->setAmbient(0,1,0);
		green->setReceiveShadows(false);
		green->setDepthCheckEnabled(false);
		Ogre::MaterialPtr white = Ogre::MaterialManager::getSingleton().create("WhiteMaterial", "General", true);
		white->setDiffuse(1,1,1,1);
		white->setAmbient(1,1,1);
		white->setReceiveShadows(false);
		white->setDepthCheckEnabled(false);
		

		//Note that while Cubes are used as the default. They will be swapped out with spheres and cones when running the dynamic editing tool.
		m_DynamicCubeXMarker = new Ogre::ManualObject("DynamicCubeXMarker");
		m_DynamicSphereXMarker = new Ogre::ManualObject("DynamicSphereXMarker");
		m_DynamicConeXMarker = new Ogre::ManualObject("DynamicConeXMarker");
		m_DynamicToolXNode = GetSceneManager()->getRootSceneNode()->createChildSceneNode( m_DynamicCubeXMarker->getName() );
		SetupCube(m_DynamicCubeXMarker, red->getName());
		SetupSphere(m_DynamicSphereXMarker, red->getName());
		SetupCone(m_DynamicConeXMarker, red->getName());
		b_DynamicToolXMarkerSelected = false;
		m_DynamicToolXNode->attachObject(m_DynamicCubeXMarker);
		m_DynamicToolXNode->setVisible(false);
		m_DynamicToolXNode->roll( Ogre::Radian(Ogre::Real(-1.57)) );
		
		m_DynamicCubeYMarker = new Ogre::ManualObject("DynamicCubeYMarker");
		m_DynamicSphereYMarker = new Ogre::ManualObject("DynamicSphereYMarker");
		m_DynamicConeYMarker = new Ogre::ManualObject("DynamicConeYMarker");
		m_DynamicToolYNode = GetSceneManager()->getRootSceneNode()->createChildSceneNode( m_DynamicCubeYMarker->getName() );
		SetupCube(m_DynamicCubeYMarker,  blue->getName());
		SetupSphere(m_DynamicSphereYMarker, blue->getName());
		SetupCone(m_DynamicConeYMarker, blue->getName());
		b_DynamicToolYMarkerSelected = false;
		m_DynamicToolYNode->attachObject(m_DynamicCubeYMarker);
		m_DynamicToolYNode->setVisible(false);

		m_DynamicCubeZMarker = new Ogre::ManualObject("DynamicCubeZMarker");
		m_DynamicSphereZMarker = new Ogre::ManualObject("DynamicSphereZMarker");
		m_DynamicConeZMarker = new Ogre::ManualObject("DynamicConeZMarker");
		m_DynamicToolZNode = GetSceneManager()->getRootSceneNode()->createChildSceneNode( m_DynamicCubeZMarker->getName() );
		SetupCube(m_DynamicCubeZMarker,  green->getName());
		SetupSphere(m_DynamicSphereZMarker, green->getName());
		SetupCone(m_DynamicConeZMarker, green->getName());
		b_DynamicToolZMarkerSelected = false;
		m_DynamicToolZNode->attachObject(m_DynamicCubeZMarker);
		m_DynamicToolZNode->setVisible(false);
		m_DynamicToolZNode->pitch( Ogre::Radian(Ogre::Real(1.57)));

		m_FreeDynamicCubeMarker = new Ogre::ManualObject("FreeDynamicCubeMarker");
		m_FreeDynamicSphereMarker = new Ogre::ManualObject("FreeDynamicSphereMarker");
		m_FreeDynamicToolNode = GetSceneManager()->getRootSceneNode()->createChildSceneNode( m_FreeDynamicCubeMarker->getName() );
		SetupCube(m_FreeDynamicCubeMarker,  white->getName());
		SetupSphere(m_FreeDynamicSphereMarker, white->getName());
		b_FreeDynamicToolMarkerSelected = false;
		m_FreeDynamicToolNode->attachObject(m_FreeDynamicCubeMarker);
		m_FreeDynamicToolNode->setVisible(false);

		
		
		//The dynamic editing tool is off by default
		b_DynamicScalingMode = false;
		b_DynamicPositioningMode = false;
		b_DynamicRotationMode = false;

		b_DynamicToolActive = false;
		

		return 1;
	}

	/*! Check if there is any new packet (non-blocking)
	*	@return	1 if everything went well
	*/
	int Editor::Editor_Receive()
	{
		int		retval	= 0;
		FD_SET	set;
		timeval	waitTimeStr;
		char	buffer[MAX_PACKET_SIZE];
		memset(buffer, '\0', MAX_PACKET_SIZE);


		// Check if there is a message to read (non-blocking)
		FD_ZERO( &set);
		FD_SET( m_sockMessages, &set);
		waitTimeStr.tv_sec = 0;
		waitTimeStr.tv_usec =0;
		retval = select( FD_SETSIZE, &set, NULL, NULL, &waitTimeStr );
		if( retval == 0)				return 0;
		if( retval == SOCKET_ERROR) 	
		{
			GGETRACELOG("select() error %d",WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}

		// If we are here is because there is something to read
		retval = recv(m_sockMessages, buffer, sizeof (buffer),0 );		
		if(retval == SOCKET_ERROR) 
		{
			GGETRACELOG("recv() failed: error %d", WSAGetLastError());
			SetEditorConnectionStatus( GGE_ERROR );
			return -1;
		}
		if(retval == 0) 
		{
			GGETRACELOG("Client closed connection");
			SetEditorConnectionStatus( GGE_ERROR );
			return 0;
		}

		// Parse the message and do something
		// HACK commented out code
		//Editor_Parser( retval, buffer );
		
		// Parse the message and do something
		Ogre::String sBuffer = Ogre::String(buffer);
		if (sBuffer == ";") return 0;
		try
		{
			if (sBuffer.substr(sBuffer.length() - 2, 2) == ";;")
			{
				if (m_msgLeftOvers.length() > 0)
				{

					m_msgLeftOvers.append(buffer);
					//GGETRACELOG("LONG PACKET HANDLED: %s", m_msgLeftOvers.c_str());
					Editor_Parser( retval, m_msgLeftOvers );
					m_msgLeftOvers.clear();
				}
				else
					Editor_Parser( retval, sBuffer);
			}
			else
			{
				
				//GGETRACELOG("OH SNAP TOO LONG PACKET: %s", buffer);
				m_msgLeftOvers.clear();
				m_msgLeftOvers.append(sBuffer);
			}
			

			return 1;
		}
		catch (...)
		{
			return 0;
		}
	}

	/*! Send a packet to GLE
	*	@return The number of bytes actually sent
	*/
	int	Editor::Editor_Send( const char * cpBuffer )
	{
#ifdef GLE_DEBUG_NETWORK
		GGETRACELOG("Sending message: [%s]", cpBuffer);
#endif

		int iRet;
		iRet = send(m_sockMessages, cpBuffer, strlen(cpBuffer), 0);
		// Check errors
		if(iRet == SOCKET_ERROR) 
			GGETRACELOG("send() failed: error %d", WSAGetLastError());

		return iRet;
	}

	/*! Build and send a packet to GLE
	*	@param[in]	packedID	The ID of the packet that we want to send
	*	@param[in]	uniqueID	ID generated automatically that identifies any transaction
	*	@param[in]	args		Extra arguments that might be needed by the packet
	*	@return		The number of bytes actually sent
	*/
	int Editor::Editor_GenericCommand(int packetID, int uniqueID, std::string args)
	{
		stringstream ss;
		ss << packetID << "||" << uniqueID << "||" << args << ";;";

		return Editor_Send(ss.str().c_str());

	}
	/*! Build and send a packet to GLE
	*	@param[in]	packedID	The ID of the packet that we want to send
	*	@param[in]	args		Extra arguments that might be needed by the packet
	*	@return		The number of bytes actually sent
	*/
	int Editor::Editor_GenericCommand(int packetID, std::string args)
	{
		return Editor_GenericCommand(packetID, -1, args);
	}

	/*! Build and send a packet to GLE
	*	@param[in]	string		Packet received
	*	@param[out]	tokens		Vector of strings extracted from the string. It will contain the packetID, uniqueID, arguments.
	*	@param[in]	delimiters	Delimiters
	*/
	void Editor::Tokenize(const Ogre::String& str, std::vector<Ogre::String>& tokens, const Ogre::String& delimiters)
	{
		// Skip delimiters at beginning.
		string::size_type lastPos = str.find_first_not_of(delimiters, 0);
		// Find first "non-delimiter".
		string::size_type pos = str.find_first_of(delimiters, lastPos);

		while (string::npos != pos || string::npos != lastPos)
		{
			// Found a token, add it to the vector.
			Ogre::String token = str.substr(lastPos, pos - lastPos);
			if (token.length() > 0)
				tokens.push_back(token);
			// Skip delimiters.  Note the "not_of"
			lastPos = str.find_first_not_of(delimiters, pos);
			// Find next "non-delimiter"
			pos = str.find_first_of(delimiters, lastPos);
		}
	}

	void Editor::AltTokenize(const Ogre::String& str, std::vector<Ogre::String>& tokens, const Ogre::String& delimiters)
	{
		Ogre::String tempString = str;
		Ogre::String outString;
		int tokenIdx = 0;
		while(tempString.size() > 0 && tokenIdx != -1)
		{
			tokenIdx = tempString.find(delimiters);
			outString = "";
			outString += tempString.substr(0, tokenIdx);
			tempString = tempString.substr(tokenIdx + delimiters.size());
			tokens.push_back(outString);
		}

		if(tempString.size() > 0)
			tokens.push_back(tempString);
		else
			tokens.push_back("");
	}

	/*! Convert a string to a float
	*	@param[in]	str	String to convert
	*	@return		Float
	*/
	float Editor::ParseFloat(const string& str)
	{
		float r = 0;
		istringstream iss(str);
		if (! (iss >> r))
			GGETRACELOG(string("Error parsing to float: " + str).c_str());
		return r;
	}

	/*! Convert a string to a int
	*	@param[in]	str	String to convert
	*	@return		Int
	*/
	int Editor::ParseInt(const string& str)
	{
		int r = -1;
		istringstream iss(str);
		if (! (iss >> r))
			GGETRACELOG(string("Error parsing to int: " + str).c_str());
		return r;
	}

	/*! Convert a string to a bool
	*	@param[in]	str	String to convert
	*	@return		Boolean
	*/
	bool Editor::ParseBool(const string& str)
	{
		bool r = false;
		if (StrCompareNoCase(str, "false") || (StrCompareNoCase(str, "0")))
			r = false;
		else if (StrCompareNoCase(str, "true") || (StrCompareNoCase(str, "1")))
			r = true;
		else		
			GGETRACELOG(string("Error parsing to bool: " + str).c_str());

		return r;
	}

	/*! Compare two strings
	*	@param[in]	String1	String to compare
	*	@param[in]	String2	String to compare
	*	@return		True if the string are the same
	*/
	bool Editor::StrCompareNoCase( string String1, string String2 )
	{
		transform(String1.begin(), String1.end(), String1.begin(),	tolower);
		transform(String2.begin(), String2.end(), String2.begin(),	tolower);
		return String1 == String2;
	}

	/*! Convert a string to a vector of tuples with properties and values
	*	@param[in]	str	String to convert
	*	@return		Vector of properties and values
	*/
	vector<GLE_PropTuple> Editor::ParseTuples(const string& str)
	{
		string cstr = str;

		vector<GLE_PropTuple> tuples;		

		while (true)
		{
			int first = cstr.find("[[");
			int last = cstr.find("]]");
			int mid = cstr.find("][");

			if (first >= 0 && last >= 0 && mid >= 0)
			{
				int propType = ParseInt(cstr.substr(first + 2, mid - first - 2));
				string value = cstr.substr(mid + 2, last - mid - 2);								
				cstr = cstr.erase(0, last + 2);

				tuples.push_back(GLE_PropTuple(propType, value));
			}
			else
				break;
		}


		return tuples;
	}

	/*! Convert an integer to string
	*	@param[in]	i	Integer to convert
	*	@return		String 
	*/
	string Editor::Int2Str(int i)
	{
		stringstream ss; 
		ss << i;
		return ss.str();
	}
	
	/*! Convert an float to string
	*	@param[in]	f	Float to convert
	*	@return		String 
	*/
	string Editor::Float2Str(float f)
	{
		stringstream ss; 
		ss << fixed << setprecision(6) << f;
		return ss.str();
	}

	/*! Send a message to GLE reporting an error
	*	@param[in]	sMessage	Message to send to GLE
	*	@return		The number of bytes sent
	*/
	int Editor::Editor_ReportErrorToGLE( const Ogre::String& sMessage )
	{
		// Report
		GGETRACELOG("Error: [%s] sending to GLE", sMessage.c_str());

		// Send the string		
		return Editor_GenericCommand(GGE_Error, sMessage);
	}

	/*! Send an exception to GLE reporting an error
	*	@param[in]	ex	Exception to send to GLE
	*	@return		The number of bytes sent
	*/
	int Editor::Editor_ReportErrorToGLE( const Ogre::Exception& ex )
	{
		// Send the string		
		return Editor_ReportErrorToGLE(ex.getDescription());
	}
	/*! Send a message to GLE reporting the use of a deprecated function
	*	@param[in]	packedID	ID of the packet being used
	*	@return		The number of bytes sent
	*/
	int Editor::Editor_ReportDeprecated( int packetID )
	{
		return Editor_ReportErrorToGLE(Ogre::String("Method deprecated: " + Int2Str(packetID)));
	}

	/*! Send a message to GLE reporting the use of an unimplemented function
	*	@param[in]	packedID	ID of the packet
	*	@return		The number of bytes sent
	*/
	int Editor::Editor_ReportUnimplemented( int packetID )
	{
		return Editor_ReportErrorToGLE(Ogre::String("Method not implemented: " + Int2Str(packetID)));
	}

	/*! Send a message to GLE with the properties of an object
	*	@return		The number of bytes sent
	*/
	int Editor::Editor_GGE_Response_Object_Properties(string nodeName, int nodeType, vector<GLE_PropTuple>& tuples)
	{
		stringstream ss;

		ss << nodeName << "||";
		ss << nodeType << "||";

		for(unsigned int i = 0; i < tuples.size(); i++)
		{
			GLE_PropTuple t = tuples[i];

			ss << "[[" << t.property << "][" << t.value << "]]";
		}

		return Editor_GenericCommand(GGE_Response_Object_Properties, ss.str());
	}

	/*! Send a basic message to the GLE, message will be showed as a
	*	message box on the GLE side.
	*	@param[in] the message to be displayed in the message box
	*	@return the number of bytes sent
	*/

	int Editor::SendSimpleMessage(std::string smessage)
	{
		return Editor_GenericCommand(GLE_StandardMessage, smessage);
	}

	/*! Return a saved message depending of the input protocol selected
	*	some classes (as the skybox) need to save several definition elements
	*	this way, other objects can access those definitions through the string message that created them
	*	@param[in] the protocol which has to be accessed
	*	@return the most relevant message for that protocol
	*/
	std::string Editor::AskStatusMessage(EditorProtocol protocolDefinition)
	{
		bool retval = false;
		string message;
		for(unsigned int con = 0; con < m_vMessageHandles.size(); con++)
		{
			if(m_vMessageHandles[con]->AskCurrentStatusMessage(protocolDefinition, message))
			{
				retval = true;
				break;
			}
		}

		if(!retval)
		{
			message = "";
		}

		return message;
	}

	/*! Main function. Called when receiving packets, it will parse the packet and execute the associated functionality
	*	@param[in]	iNumBytesSent	Number of bytes received from GLE
	*	@param[in]	cpBuffer		Message received from GLE
	*	@return		-1 if error
	*/

	int Editor::Editor_Parser( int iNumBytesSent, Ogre::String cpBuffer )
	{
		Ogre::String packet = Ogre::String(cpBuffer);

		#ifdef GLE_DEBUG_NETWORK
				GGETRACELOG("Received %d bytes, data: [%s] from client", iNumBytesSent, packet.c_str());
		#endif

		// Separate all messages
		vector<Ogre::String> packets;
		AltTokenize(packet, packets, ";;");//Tokenize(packet, packets, ";;");

		// process each message
		for (unsigned int i = 0; i < packets.size(); i++)
		{
			int iPacketID = -1;
			int iUniqueID = -1;

			// Update the text on the console
			#ifdef GLE_DEBUG_NETWORK
						GGETRACELOG("Message from client: [%s]", packets[i].c_str());
			#endif

			vector<Ogre::String> tokens;

			AltTokenize(packets[i], tokens, "||");//Tokenize(packets[i], tokens, "||");
			if (tokens.size() < 2)
				continue;

			bool retval = false;
			for(unsigned int con = 0; con < m_vMessageHandles.size(); con++)
			{
				if(m_vMessageHandles[con]->ProcessMessage(tokens))
				{
					retval = true;
					break;
				}
			}

			if(retval == false)
				return -1;
		}

		return 1;
	}

	/*! Close Winsock and both sockets.
	*	@return		S_OK if everything correct
	*/
	int Editor::FinalizeEditor()
	{
		closesocket( m_sockListen );
		closesocket( m_sockMessages );
		WSACleanup();

		return S_OK;
	}

	/*! Obtain all the information from a node and create a vector with it
	*	@param[in]	node	Node to extract information
	*	@return		Vector with the properties and values of the scene node
	*/
	vector<GLE_PropTuple> Editor::GetSceneNodeProperties(Ogre::SceneNode* node)
	{
		vector<GLE_PropTuple> tuples;

		Ogre::Vector3 Pos = node->getPosition();
		Ogre::Vector3 Scale = node->getScale();
		Ogre::Quaternion Orientation = node->getOrientation();

		tuples.push_back(GLE_PropTuple(PROP_Pos_X, Pos.x));
		tuples.push_back(GLE_PropTuple(PROP_Pos_Y, Pos.y));
		tuples.push_back(GLE_PropTuple(PROP_Pos_Z, Pos.z));

// 		tuples.push_back(GLE_PropTuple(PROP_RotationQuat_X, Orientation.x));
// 		tuples.push_back(GLE_PropTuple(PROP_RotationQuat_Y, Orientation.y));
// 		tuples.push_back(GLE_PropTuple(PROP_RotationQuat_Z, Orientation.z)));
// 		tuples.push_back(GLE_PropTuple(PROP_RotationQuat_W, Orientation.w));

		tuples.push_back(GLE_PropTuple(PROP_Rotation_X, Orientation.getPitch().valueDegrees()));
		tuples.push_back(GLE_PropTuple(PROP_Rotation_Y, Orientation.getYaw().valueDegrees()));		
		tuples.push_back(GLE_PropTuple(PROP_Rotation_Z, Orientation.getRoll().valueDegrees()));

		tuples.push_back(GLE_PropTuple(PROP_Scale_X, Scale.x));
		tuples.push_back(GLE_PropTuple(PROP_Scale_Y, Scale.y));
		tuples.push_back(GLE_PropTuple(PROP_Scale_Z, Scale.z));

		return tuples;
	}

	/*! Obtain all the information from an entity and create a vector with it
	*	@param[in]	entity	Entity to extract information
	*	@return		Vector with the properties and values of the entity
	*/
	vector<GLE_PropTuple> Editor::GetEntityProperties( Ogre::Entity* entity )
	{
		// TODO-SV: return havokFile and havokName
								
		vector<GLE_PropTuple> tuples;

		tuples.push_back(GLE_PropTuple(PROP_MovableType, entity->getMovableType()));
		tuples.push_back(GLE_PropTuple(PROP_Material, entity->getSubEntity(0)->getMaterialName()));
		tuples.push_back(GLE_PropTuple(PROP_Shadow, entity->getCastShadows()));
		

		return tuples;
	}

	/*! Obtain all the information from a light and create a vector with it
	*	@param[in]	light	Light to extract information
	*	@return		Vector with the properties and values of the light
	*/
	vector<GLE_PropTuple> Editor::GetLightProperties( Ogre::Light* light )
	{
		vector<GLE_PropTuple> tuples;

		Ogre::ColourValue spec = light->getSpecularColour();
		Ogre::ColourValue diff = light->getDiffuseColour();
		Ogre::Vector3 dir = light->getDirection();
		Ogre::Light::LightTypes type = light->getType();

		tuples.push_back(GLE_PropTuple(PROP_Light_Type, float(type)));

		tuples.push_back(GLE_PropTuple(PROP_Direction_X, dir.x));
		tuples.push_back(GLE_PropTuple(PROP_Direction_Y, dir.y));
		tuples.push_back(GLE_PropTuple(PROP_Direction_Z, dir.z));

		tuples.push_back(GLE_PropTuple(PROP_Color_Diff_R, diff.r));
		tuples.push_back(GLE_PropTuple(PROP_Color_Diff_G, diff.g));
		tuples.push_back(GLE_PropTuple(PROP_Color_Diff_B, diff.b));

		tuples.push_back(GLE_PropTuple(PROP_Color_Spec_R, spec.r));
		tuples.push_back(GLE_PropTuple(PROP_Color_Spec_G, spec.g));
		tuples.push_back(GLE_PropTuple(PROP_Color_Spec_B, spec.b));

		return tuples;

	}

	/*! Set the scene node properties from a tuple
	*	@param[in]	tuple	Property-Value to set
	*	@param[in]	node	Node that will receive the information
	*/
	void Editor::SetSceneNodeProperty( GLE_PropTuple &tuple, Ogre::SceneNode* node )
	{
		try
		{
			
			switch (tuple.property)
			{
			case  PROP_Pos_X:
				{
					node->setPosition(ParseFloat(tuple.value), node->getPosition().y, node->getPosition().z);
					break;
				}
			case  PROP_Pos_Y:
				{
					node->setPosition(node->getPosition().x, ParseFloat(tuple.value), node->getPosition().z);
					break;
				}
			case  PROP_Pos_Z:
				{
					node->setPosition(node->getPosition().x, node->getPosition().y, ParseFloat(tuple.value));
					break;
				}
			case  PROP_RotationQuat_X:
				{
					node->setOrientation( node->getOrientation().w, ParseFloat(tuple.value), node->getOrientation().y, node->getOrientation().z );
					break;
				}
			case  PROP_RotationQuat_Y:
				{
					node->setOrientation( node->getOrientation().w, node->getOrientation().x, ParseFloat(tuple.value), node->getOrientation().z );
					break;
				}
			case  PROP_RotationQuat_Z:
				{
					node->setOrientation( node->getOrientation().w, node->getOrientation().x, node->getOrientation().y, ParseFloat(tuple.value) );
					break;
				}
			case  PROP_RotationQuat_W:
				{
					node->setOrientation( ParseFloat(tuple.value), node->getOrientation().x, node->getOrientation().y, node->getOrientation().z );
					break;
				}
			case PROP_Rotation_Y:
				{
					Ogre::Quaternion q;
					q.FromAngleAxis( Ogre::Degree(ParseFloat(tuple.value)), Ogre::Vector3::UNIT_Y );
					node->setOrientation(q);
					break;
				}
			case PROP_Rotation_X:
				{
					Ogre::Quaternion q;
					q.FromAngleAxis( Ogre::Degree(ParseFloat(tuple.value)), Ogre::Vector3::UNIT_X );
					node->setOrientation(q);
					break;
				}
			case PROP_Rotation_Z:
				{
					Ogre::Quaternion q;
					q.FromAngleAxis( Ogre::Degree(ParseFloat(tuple.value)), Ogre::Vector3::UNIT_Z );
					node->setOrientation(q);
					break;
				}
			case PROP_Scale_X:
				{
					node->setScale(ParseFloat(tuple.value),node->getScale().y,node->getScale().z);
					break;
				}
			case PROP_Scale_Y:
				{
					node->setScale(node->getScale().x,ParseFloat(tuple.value),node->getScale().z);
					break;
				}
			case PROP_Scale_Z:
				{
					node->setScale(node->getScale().x,node->getScale().y,ParseFloat(tuple.value));
					break;
				}
			}

			
			// If the node contains an entity with physics the physics object needs to be updated
			Ogre::SceneNode::ObjectIterator childNodeIterator = node->getAttachedObjectIterator();
			while (childNodeIterator.hasMoreElements())
			{
				Ogre::MovableObject* childNode = childNodeIterator.getNext();
				if (m_pEngine->GetForemostGameScreen(true)->GetDefaultSceneManager()->hasEntity(childNode->getName()))
				{
					if (m_pEngine->GetForemostGameScreen(true)->GetGameObjectManager()->HasGameObject(childNode->getName()))
					{
						GameObject* gameObject = m_pEngine->GetForemostGameScreen(true)->GetGameObjectManager()->GetGameObject(childNode->getName());
						bool bStandard = false;
#ifdef HAVOK
						hkVector4 newPosition(node->getPosition().x, node->getPosition().y, node->getPosition().z);
						hkQuaternion newRotation(node->getOrientation().x, node->getOrientation().y, node->getOrientation().z, node->getOrientation().w);
						PhysicsPrimitiveObject* f_pTempObject = (PhysicsPrimitiveObject*)gameObject->m_pPhysicsObject;
						bStandard = true;
						if (f_pTempObject!=NULL)
						{
							if(f_pTempObject->getRigidBody() != NULL)
							{
								f_pTempObject->getRigidBody()->setPositionAndRotation(newPosition, newRotation);
								f_pTempObject->getRigidBody()->setLinearVelocity(hkVector4::getZero());
								f_pTempObject->getRigidBody()->setAngularVelocity(hkVector4::getZero());
								bStandard = false;
							}
						}
#endif

#ifdef PHYSX
						physx::PxVec3 Pos(node->getPosition().x, node->getPosition().y, node->getPosition().z);
						physx::PxQuat Quat(node->getOrientation().x, node->getOrientation().y, node->getOrientation().z, node->getOrientation().w);

						PhysicsPrimitiveObject* f_pTempObject = (PhysicsPrimitiveObject*)gameObject->m_pPhysicsObject;

						if (f_pTempObject != NULL) {
							if(f_pTempObject->getGameObjectType() == PHYSICS_DYNAMIC) {
								f_pTempObject->getDynamicRigidBody()->setGlobalPose(physx::PxTransform(Pos, Quat));
								f_pTempObject->getDynamicRigidBody()->setLinearVelocity(physx::PxVec3(0,0,0));
								f_pTempObject->getDynamicRigidBody()->setAngularVelocity(physx::PxVec3(0,0,0));
							}
							else if (f_pTempObject->getGameObjectType() == PHYSICS_FIXED) {
								f_pTempObject->getStaticRigidBody()->setGlobalPose(physx::PxTransform(Pos, Quat));
							}
						}
#endif

						if(bStandard)
						{
							gameObject = m_pEngine->GetForemostGameScreen(true)->GetGameObjectManager()->GetGameObject(childNode->getName());
							gameObject->setPosition(node->getPosition().x, node->getPosition().y, node->getPosition().z);
						}
						gameObject->resetScale(node->getScale().x, node->getScale().y, node->getScale().z);
					}
				}
			}
			
		}
		catch(...)
		{
		}
	}

	/*! Set the light properties from a tuple
	*	@param[in]	tuple	Property-Value to set
	*	@param[in]	light	Light that will receive the information
	*/	
	void Editor::SetLightProperty( GLE_PropTuple &tuple, Ogre::Light* light)
	{
		switch (tuple.property)
		{		
		case PROP_Light_Type:
			{
				int type = ParseInt(tuple.value);
				if (type >= 0)
					light->setType((Ogre::Light::LightTypes)ParseInt(tuple.value));
				break;
			}
		case PROP_Direction_X:
			{
				light->setDirection(ParseFloat(tuple.value), light->getDirection().y, light->getDirection().z);
				break;
			}
		case PROP_Direction_Y:
			{
				light->setDirection(light->getDirection().x, ParseFloat(tuple.value), light->getDirection().z);
				break;
			}
		case PROP_Direction_Z:
			{
				light->setDirection(light->getDirection().x, light->getDirection().y, ParseFloat(tuple.value));
				break;
			}
		case PROP_Color_Diff_R:
			{
				light->setDiffuseColour(ParseFloat(tuple.value), light->getDiffuseColour().g, light->getDiffuseColour().b);
				break;
			}
		case PROP_Color_Diff_G:
			{
				light->setDiffuseColour(light->getDiffuseColour().r, ParseFloat(tuple.value), light->getDiffuseColour().b);
				break;
			}
		case PROP_Color_Diff_B:
			{
				light->setDiffuseColour(light->getDiffuseColour().r, light->getDiffuseColour().g, ParseFloat(tuple.value));
				break;
			}
		case PROP_Color_Spec_R:
			{
				light->setSpecularColour(ParseFloat(tuple.value), light->getSpecularColour().g, light->getSpecularColour().b);
				break;
			}
		case PROP_Color_Spec_G:
			{
				light->setSpecularColour(light->getSpecularColour().r, ParseFloat(tuple.value), light->getSpecularColour().b);
				break;
			}
		case PROP_Color_Spec_B:
			{
				light->setSpecularColour(light->getSpecularColour().r, light->getSpecularColour().g, ParseFloat(tuple.value));
				break;
			}

		}
	}

	void Editor::SetEntityProperty( GLE_PropTuple &tuple, Ogre::Entity* entity)
	{
		try
		{

			switch(tuple.property)
			{
			case PROP_Material:
				GGETRACELOG("Entered PROP_Material case");
				entity->setMaterialName(tuple.value);
				break;
			case PROP_Shadow:
				GGETRACELOG("Entered PROP_Shadow case");
				entity->setCastShadows(ParseBool(tuple.value));
				break; 
			}
		}catch(...)
		{
		}
	}

	/*! Toogle the grid of the scene manager (only one by scene manager)
	*	@param[in]	tokens	List of tokens
	*/	
	void Editor::GridToggle( vector<Ogre::String> tokens )
	{
		Ogre::String nm;

		// Parse the packet
		nm=tokens[2];

		// Check if we are activating or deactivating the grid
		// if we are activating then we need to receive other information ...
		if (nm == "1" && 
			!GetSceneManager()->hasSceneNode("Editor_GridSceneNode") && 
			!GetSceneManager()->hasManualObject("Editor_Grid"))
		{
			int iCount = 0 ;
			// Get the information from the packet
			int		m_iGridMaxX			= ParseInt(tokens[3]);
			int		m_iGridMaxZ			= ParseInt(tokens[4]);
			int		m_iGridSeparation	= ParseInt(tokens[5]);
			Ogre::Real	iGridAxesColorR		= Ogre::Real(ParseFloat(tokens[6]));
			Ogre::Real	iGridAxesColorG		= Ogre::Real(ParseFloat(tokens[7]));
			Ogre::Real	iGridAxesColorB		= Ogre::Real(ParseFloat(tokens[8]));
			Ogre::Real	iGridLineColorR		= Ogre::Real(ParseFloat(tokens[9]));
			Ogre::Real	iGridLineColorG		= Ogre::Real(ParseFloat(tokens[10]));
			Ogre::Real	iGridLineColorB		= Ogre::Real(ParseFloat(tokens[11]));

			// Create the grids
			// extra grids are used for orthogonal cameras
			Ogre::ManualObject * mGrid = GetSceneManager()->createManualObject("Editor_Grid");
			mGrid->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

			Ogre::ManualObject * mGridSide = GetSceneManager()->createManualObject("Editor_Grid_Side");
			mGridSide->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

			Ogre::ManualObject * mGridFront = GetSceneManager()->createManualObject("Editor_Grid_Front");
			mGridFront->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

			for( int i = -m_iGridMaxZ ; i <= m_iGridMaxZ ; i += m_iGridSeparation)
			{
				mGrid->position(-Ogre::Real(m_iGridMaxX),	0.0f,	Ogre::Real(i));
				mGrid->colour(iGridLineColorR,iGridLineColorG,iGridLineColorB);
				if( i == 0 )	mGrid->colour(iGridAxesColorR,iGridAxesColorG,iGridAxesColorB);
				mGrid->position(Ogre::Real(m_iGridMaxX),	0.0f,	Ogre::Real(i));
				//
				mGridSide->position(0.0f,	-Ogre::Real(m_iGridMaxX),	Ogre::Real(i));
				mGridSide->colour(iGridLineColorR,iGridLineColorG,iGridLineColorB);
				if( i == 0 )	mGridSide->colour(iGridAxesColorR,iGridAxesColorG,iGridAxesColorB);
				mGridSide->position(0.0f,	Ogre::Real(m_iGridMaxX),	Ogre::Real(i));
				//
				mGridFront->position(-Ogre::Real(m_iGridMaxX),	Ogre::Real(i),	0.0f);
				mGridFront->colour(iGridLineColorR,iGridLineColorG,iGridLineColorB);
				if( i == 0 )	mGridFront->colour(iGridAxesColorR,iGridAxesColorG,iGridAxesColorB);
				mGridFront->position(Ogre::Real(m_iGridMaxX),	Ogre::Real(i),	0.0f);
				//
				iCount++;
			}
			iCount = 0;
			for( int i = -m_iGridMaxX ; i <= m_iGridMaxX ; i += m_iGridSeparation)
			{
				mGrid->position(Ogre::Real(i),	0.0f, -Ogre::Real(m_iGridMaxZ)	);
				mGrid->colour(iGridLineColorR,iGridLineColorG,iGridLineColorB);
				if( i == 0 )	mGrid->colour(iGridAxesColorR,iGridAxesColorG,iGridAxesColorB);
				mGrid->position(Ogre::Real(i),  0.0f,	Ogre::Real(m_iGridMaxZ)	);
				//
				mGridSide->position(0.0f,	Ogre::Real(i), -Ogre::Real(m_iGridMaxZ)	);
				mGridSide->colour(iGridLineColorR,iGridLineColorG,iGridLineColorB);
				if( i == 0 )	mGridSide->colour(iGridAxesColorR,iGridAxesColorG,iGridAxesColorB);
				mGridSide->position(0.0f,  Ogre::Real(i),	Ogre::Real(m_iGridMaxZ)	);
				//
				mGridFront->position(Ogre::Real(i),	-Ogre::Real(m_iGridMaxZ),	0.0f);
				mGridFront->colour(iGridLineColorR,iGridLineColorG,iGridLineColorB);
				if( i == 0 )	mGridFront->colour(iGridAxesColorR,iGridAxesColorG,iGridAxesColorB);
				mGridFront->position(Ogre::Real(i),	Ogre::Real(m_iGridMaxZ),	0.0f);
				//
				iCount++;
			}
			mGrid->end(); 
			mGridSide->end();
			mGridFront->end();
			// Add it to a new scene node
			GetSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode")->attachObject(mGrid);

			//if older orthogonal grids still exist then destroy them
			if(GetSceneManager()->hasSceneNode("Editor_GridSceneNode_Side") && 
						GetSceneManager()->hasManualObject("Editor_Grid_Side") &&
						GetSceneManager()->hasSceneNode("Editor_GridSceneNode_Front") && 
						GetSceneManager()->hasManualObject("Editor_Grid_Front")) {
				GetSceneManager()->destroyManualObject("Editor_Grid_Side");
				GetSceneManager()->destroySceneNode("Editor_GridSceneNode_Side");
				GetSceneManager()->destroyManualObject("Editor_Grid_Front");
				GetSceneManager()->destroySceneNode("Editor_GridSceneNode_Front");
			}
			//add the new orthogonal new ones
			GetSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode_Side")->attachObject(mGridSide);
			GetSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode_Front")->attachObject(mGridFront);
			
			//get the current camera
			const Ogre::String cName = m_pEngine->GetForemostGameScreen()->GetActiveCamera()->getName();
			
			//hide grids based on current camera
			if(cName == "Editor_CameraPerspective" || cName == "Editor_CameraTop") {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(true);
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(false);
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(false);
			}
			else if(cName == "Editor_CameraSide") {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(false);
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(true);
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(false);
			}
			else if(cName == "Editor_CameraFront") {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(false);
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(false);
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(true);
			}
		}else if( nm == "0" && 
			GetSceneManager()->hasSceneNode("Editor_GridSceneNode") && 
			GetSceneManager()->hasManualObject("Editor_Grid") &&
			//orthogonal grids
			GetSceneManager()->hasSceneNode("Editor_GridSceneNode_Side") && 
			GetSceneManager()->hasManualObject("Editor_Grid_Side") &&
			GetSceneManager()->hasSceneNode("Editor_GridSceneNode_Front") && 
			GetSceneManager()->hasManualObject("Editor_Grid_Front"))
		{
			// Destroy them
			GetSceneManager()->destroyManualObject("Editor_Grid");
			GetSceneManager()->destroySceneNode("Editor_GridSceneNode");
			// Destroy orthogonal grids
			GetSceneManager()->destroyManualObject("Editor_Grid_Side");
			GetSceneManager()->destroySceneNode("Editor_GridSceneNode_Side");
			GetSceneManager()->destroyManualObject("Editor_Grid_Front");
			GetSceneManager()->destroySceneNode("Editor_GridSceneNode_Front");
		}
	}

#ifdef GLE_EDITOR_MODELVIEW
	// TODO remove deprecated feature
	/*! Set the active camera (DEPRECATED)
	*	@param[in]	tokens	List of tokens
	*/		
	void Editor::MVCameraSet( vector<Ogre::String> tokens )
	{ 
		// Parse the packet
		int iCameraType = ParseInt(tokens[2]);
		int iSizeX		= ParseInt(tokens[3]);
		int iSizeY		= ParseInt(tokens[4]);
		float fFov		= ParseFloat(tokens[5]);

		if (iCameraType==0)
			m_pEngine->GetGameScreen("BlankScreen")->SetActiveCamera( m_pRenderer->getSceneManager("BlankScreenSceneManager")->getCamera("CameraPerspective"));
		if (iCameraType==1)
			m_pEngine->GetGameScreen("BlankScreen")->SetActiveCamera( m_pRenderer->getSceneManager("BlankScreenSceneManager")->getCamera("CameraTop"));
		if (iCameraType==2)
			m_pEngine->GetGameScreen("BlankScreen")->SetActiveCamera( m_pRenderer->getSceneManager("BlankScreenSceneManager")->getCamera("CameraSide"));
		if (iCameraType==3)
			m_pEngine->GetGameScreen("BlankScreen")->SetActiveCamera( m_pRenderer->getSceneManager("BlankScreenSceneManager")->getCamera("CameraFront"));

		// HACK FOV commented out
		// m_pEngine->GetGameScreen("BlankScreen")->GetActiveCamera()->setFOVy(Ogre::Radian(fFov));
	}
#endif

	/*! Set the active camera in editor mode
	*	@param[in]	tokens	List of tokens
	*/	
	void Editor::CameraSet( vector<Ogre::String> tokens )
	{
		// Parse the packet
		int iCameraType = ParseInt(tokens[2]);
		int iSizeX		= ParseInt(tokens[3]);
		int iSizeY		= ParseInt(tokens[4]);
		float fFov		= ParseFloat(tokens[5]);
		
		// Initialize the camera just in case the scene manager doesn't have them
		InitEditorCameras(GetSceneManager(), false);

		//Check if the grids are present
		bool hastop = GetSceneManager()->hasManualObject("Editor_Grid");
		bool hasside = GetSceneManager()->hasManualObject("Editor_Grid_Side");
		bool hasfront = GetSceneManager()->hasManualObject("Editor_Grid_Front");

		// Set the camera and make sure the relevant grid is visible
		if (iCameraType==0) {
			m_pEngine->GetForemostGameScreen()->SetActiveCamera( GetSceneManager()->getCamera("Editor_CameraPerspective"));
			if(hastop) {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(true);
			}
			if(hasside) {
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(false);
			}
			if(hasfront) {
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(false);
			}
		}
		if (iCameraType==1) {
			m_pEngine->GetForemostGameScreen()->SetActiveCamera( GetSceneManager()->getCamera("Editor_CameraTop"));
			if(hastop) {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(true);
			}
			if(hasside) {
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(false);
			}
			if(hasfront) {
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(false);
			}
		}
		if (iCameraType==2){
			m_pEngine->GetForemostGameScreen()->SetActiveCamera( GetSceneManager()->getCamera("Editor_CameraSide"));
			if(hastop) {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(false);
			}
			if(hasside) {
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(true);
			}
			if(hasfront) {
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(false);
			}
		}
		if (iCameraType==3){
			m_pEngine->GetForemostGameScreen()->SetActiveCamera( GetSceneManager()->getCamera("Editor_CameraFront"));
			if(hastop) {
				GetSceneManager()->getManualObject("Editor_Grid")->setVisible(false);
			}
			if(hasside) {
				GetSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(false);
			}
			if(hasfront) {
				GetSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(true);
			}
		}		
		
		// FOV
		// Size	
		//m_pEngine->GetForemostGameScreen()->GetActiveCamera()->setFOVy(Ogre::Radian(fFov));
		

		
	}

	/*! Create an editor camera
	*	@param[in]	pSceneManager			Scene manager (the camera will only be created once in every scene manager)
	*	@param[in]	sCameraSceneNodeName	Name of the scene node that will contain the camera
	*	@param[in]	sCameraName				Name of the camera
	*	@param[out]	bNeedToInitCamera		True, we need to initialize the camera because it has been created
	*	@return		Pointer to the camera created
	*/	
	Ogre::Camera* Editor::CreateEditorCamera( Ogre::SceneManager* pSceneManager, Ogre::String sCameraSceneNodeName, Ogre::String sCameraName, bool * bNeedToInitCamera)
	{
		Ogre::Camera *		pCamera;
		Ogre::SceneNode*	pCameraSN;

		// Check if the scene node was created
		if( !pSceneManager->hasSceneNode(sCameraSceneNodeName) ) {
			pCameraSN = pSceneManager->getRootSceneNode()->createChildSceneNode(sCameraSceneNodeName);
			*bNeedToInitCamera = true;
		}else{
			pCameraSN = pSceneManager->getSceneNode(sCameraSceneNodeName);
			*bNeedToInitCamera = false;
		}

		// Check if the camera was created
		if( !pSceneManager->hasCamera(sCameraName) )
		{
			pCamera	= pSceneManager->createCamera(sCameraName);
			// Attach the new camera
			pCameraSN->attachObject(pCamera);

			*bNeedToInitCamera = true;

		}else{
			pCamera	= pSceneManager->getCamera(sCameraName);
			*bNeedToInitCamera = false;
		}

		return pCamera;
	}

	/*! Initialize all the editor cameras
	*	@param[in]	pSceneManager			Scene manager (the cameras will only be created once in every scene manager)
	*	@param[in]	bNeedToInitCamera		True, we need to initialize the camera
	*/	
	void Editor::InitEditorCameras( Ogre::SceneManager* pSceneManager, bool bNeedToResetCameras )
	{
		Ogre::Camera *		pCamera;
		bool				bRes = false;

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Perspective camera
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			pCamera = CreateEditorCamera(pSceneManager, "Editor_CameraPerspective_SceneNode", "Editor_CameraPerspective", &bRes );

			// Check if we have to initialize the camera
			if(bRes || bNeedToResetCameras)
			{
				InputPtr->m_fRadius			= 40.0;
				InputPtr->m_fYawValue		= 0.0;
				InputPtr->m_fPitchValue		= -Ogre::Math::PI;

				// Set camera parameters
				pCamera->setNearClipDistance((Ogre::Real)0.05);
				pCamera->setPosition		(Ogre::Vector3(0.0000, 0.0000, 0.0000));
				pCamera->setOrientation		(Ogre::Quaternion::IDENTITY);
				pCamera->yaw				(Ogre::Degree(InputPtr->m_fYawValue));
				pCamera->pitch				(Ogre::Degree(InputPtr->m_fPitchValue));
				pCamera->moveRelative		(Ogre::Vector3(0.0,0.0, InputPtr->m_fRadius));
			}
			bRes = false;
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Top camera
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			pCamera = CreateEditorCamera(pSceneManager, "Editor_CameraTop_SceneNode", "Editor_CameraTop", &bRes );
			
			// Check if we have to initialize the camera
			if(bRes || bNeedToResetCameras)
			{
				// Set camera parameters
				pCamera->getParentSceneNode()->setPosition( 0, 50000, 0); //was 1, caused clipping issues -11/2/09
				pCamera->getParentSceneNode()->setDirection(Ogre::Vector3(0,-1,0));
				pCamera->setProjectionType( Ogre::PT_ORTHOGRAPHIC );
				pCamera->setNearClipDistance((Ogre::Real)0.01);
				pCamera->setFarClipDistance((Ogre::Real)100000);
				pCamera->setOrthoWindow(Ogre::Real(m_pEngine->GetRenderWindow()->getWidth()/2),  Ogre::Real(m_pEngine->GetRenderWindow()->getHeight()/2));
			}
			bRes = false;
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Init Side Camera
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			pCamera = CreateEditorCamera(pSceneManager, "Editor_CameraSide_SceneNode", "Editor_CameraSide", &bRes );

			// Check if we have to initialize the camera
			if(bRes || bNeedToResetCameras)
			{
				// Set camera parameters
				pCamera->getParentSceneNode()->setPosition( 50000, 0, 0); //was 1, caused clipping issues -11/2/09 
				pCamera->getParentSceneNode()->setDirection(Ogre::Vector3(-1,0,0));
				pCamera->setProjectionType( Ogre::PT_ORTHOGRAPHIC );
				pCamera->setNearClipDistance((Ogre::Real)0.01);
				pCamera->setFarClipDistance(100000);
				pCamera->setOrthoWindow(Ogre::Real(m_pEngine->GetRenderWindow()->getWidth()/2),  Ogre::Real(m_pEngine->GetRenderWindow()->getHeight()/2));
			}
			bRes = false;
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Init Front Camera
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			pCamera = CreateEditorCamera(pSceneManager, "Editor_CameraFront_SceneNode", "Editor_CameraFront", &bRes );

			// Check if we have to initialize the camera
			if(bRes || bNeedToResetCameras)
			{
				// Set camera parameters
				pCamera->getParentSceneNode()->setPosition( 0, 0, 50000); //was 1, caused clipping issues -11/2/09
				pCamera->getParentSceneNode()->setDirection(Ogre::Vector3(0,0,-1));
				pCamera->setProjectionType( Ogre::PT_ORTHOGRAPHIC );
				pCamera->setNearClipDistance((Ogre::Real)0.01);
				pCamera->setFarClipDistance(100000);
				pCamera->setOrthoWindow(Ogre::Real(m_pEngine->GetRenderWindow()->getWidth()/2),  Ogre::Real(m_pEngine->GetRenderWindow()->getHeight()/2));
			}
			bRes = false;
	}
#ifdef GLE_EDITOR_MODELVIEW
	/*! Add a Resource Location
	*/	
	void Editor::MVLoadResLocation( vector<Ogre::String> tokens )
	{
		Ogre::String nm;
		Ogre::String randGroupName;

		randGroupName = Int2Str(rand()*1000);
		// Parse the packet
		nm=tokens[2];


		Ogre::ResourceGroupManager *rgm = Ogre::ResourceGroupManager::getSingletonPtr();
		/* Reload resources */
		Ogre::ResourceGroupManager::ResourceManagerIterator it = rgm->getResourceManagerIterator();
		while(it.hasMoreElements())
		{
			Ogre::ResourceManager* man = it.getNext();
			man->reloadAll();
		}
		rgm->addResourceLocation(nm,"FileSystem",randGroupName);
		rgm->initialiseResourceGroup(randGroupName);
		//rgm->initialiseAllResourceGroups();

	}
#endif
#ifdef GLE_EDITOR_MODELVIEW
	/*! Recursive clearing of a SceneNode
	*/
	void  Editor::MVDestoryMovables(Ogre::SceneNode *sp) {
		unsigned short numObj = sp->numAttachedObjects();
		unsigned short numChild = sp->numChildren();

		GGETRACELOG("%d num attachedObjects, %d numChildren\n",numObj,numChild);

		Ogre::MovableObject *currentObj;

		for(unsigned short i = 0; i < numObj; i++) {
			currentObj = sp->getAttachedObject(i);
			m_pRenderer->getSceneManager("BlankScreenSceneManager")->destroyMovableObject(currentObj);
			GGETRACELOG("Movable Object #%d destroyed\n",i);
		}
		for(unsigned short i = 0; i < numChild; i++) {
			MVDestoryMovables((Ogre::SceneNode *)sp->getChild(i));
		}
	}
	/*! Load a mesh
	*/	
	void Editor::MVLoadMesh( vector<Ogre::String> tokens )
	{
		Ogre::String nm;

		// Parse the packet
		nm=tokens[2];

		// Just in case we had an animation previously
		m_CurrentAnimationName = "";
		m_sMVCurrentGameObjectName = GLE_MODEL_EDITOR_OBJECT_NAME;
		GameObjectManager* currentObjectManager = m_pEngine->GetGameScreen("BlankScreen")->GetGameObjectManager();
		if (m_pMVGameObject != NULL)
		{
			currentObjectManager->RemoveGameObject(m_pMVGameObject);
			currentObjectManager->RefreshGameObjectsList(false);
			m_pMVGameObject = NULL;
			m_sMVCurrentGameObjectName.append("_NewModel");
		}
		m_pMVGameObject = new GameObject(m_sMVCurrentGameObjectName,nm);
		currentObjectManager->AddGameObject(m_pMVGameObject);
		currentObjectManager->RefreshGameObjectsList(false);
		if (m_pMVGameObject != NULL)
		{
			m_pMVGameObject->setPosition(0,0,0);
			if (m_pMVGameObject->m_pGraphicsObject->m_pOgreEntity != NULL)
			{
				// Set the camera initial position 
				Ogre::Vector3 maxVector = m_pMVGameObject->m_pGraphicsObject->m_pOgreEntity->getBoundingBox().getMaximum();
				Ogre::Vector3 minVector = m_pMVGameObject->m_pGraphicsObject->m_pOgreEntity->getBoundingBox().getMinimum();   
				Ogre::Vector3 v = maxVector-minVector;
			
				InputPtr->m_fRadius		= v.length();
				InputPtr->m_fYawValue	= 0.0;
				InputPtr->m_fPitchValue	= -Ogre::Math::PI;
				Ogre::Camera * pCamera;
				pCamera = m_pEngine->GetGameScreen("BlankScreen")->GetDefaultSceneManager()->getCamera("Editor_CameraPerspective");
				pCamera->setPosition	(0.0,0.0,0.0);
				pCamera->setOrientation(Ogre::Quaternion::IDENTITY);
				pCamera->yaw			(Ogre::Degree(InputPtr->m_fYawValue));
				pCamera->pitch			(Ogre::Degree(InputPtr->m_fPitchValue));
				pCamera->moveRelative	(Ogre::Vector3(0.0,0.0, InputPtr->m_fRadius));
				// This is going to be the active camera
				m_pEngine->GetGameScreen("BlankScreen")->SetActiveCamera(pCamera);	
			}
		}
	}
#endif
#ifdef GLE_EDITOR_MODELVIEW
	/*! Load a mesh
	*/	
	void Editor::MVLoadColladaFile( vector<Ogre::String> tokens )
	{
		Ogre::String nm;

		// Parse the packet
		nm=tokens[2];

		// Just in case we had an animation previously
		m_CurrentAnimationName = "";
		Ogre::SceneNode * p;
		// check if there is a mesh already in memory, if so it will unload it first and then load
		if (!m_pRenderer->getSceneManager("BlankScreenSceneManager")->hasSceneNode("MySN"))
		{
			p =	m_pRenderer->getSceneManager("BlankScreenSceneManager")->getRootSceneNode()->createChildSceneNode("MySN");						
		}else{
			p = m_pRenderer->getSceneManager("BlankScreenSceneManager")->getSceneNode("MySN");

			MVDestoryMovables(p);

			p->removeAndDestroyAllChildren();
			m_pRenderer->getSceneManager("BlankScreenSceneManager")->destroySceneNode(p);
			//m_pRenderer->getSceneManager("BlankScreenSceneManager")->destroyAllEntities();
			p =	m_pRenderer->getSceneManager("BlankScreenSceneManager")->getRootSceneNode()->createChildSceneNode("MySN");
		}

		OgreCollada::ImpExp* pImpExp = 0;
		bool rtn = false;
		//pImpExp = OgreCollada::CreateImpExp( pRoot, pSceneManager );
		
		pImpExp = OgreCollada::CreateImpExp( m_pRenderer, m_pRenderer->getSceneManager("BlankScreenSceneManager") );
		
		Ogre::SceneNode * colladaNode;
		rtn = pImpExp->importColladaGetSceneNode(&colladaNode,
			(const OgreCollada::char_t*)nm.c_str() );

		//Dettach the collada node and reattach it to "MySN" node
		Ogre::Entity *colladaEntity = DotScenePlusLoader::FindFirstEntity(colladaNode);
		//colladaEntity->detatchFromParent();
		dynamic_cast<Ogre::SceneNode*>(colladaNode->getParent())->removeAndDestroyChild(colladaNode->getName());
		//p->addChild(colladaNode);
		
		p->attachObject(colladaEntity);

		//Scene file output for debugging
		//DotScenePlusSaver *sceneSave = new DotScenePlusSaver();
		//sceneSave->Save("testscene.scene", m_pEngine->GetForemostGameScreen(), m_pRenderer->getSceneManager("BlankScreenSceneManager"), m_pRenderer->getSceneManager("BlankScreenSceneManager")->getRootSceneNode());

		OgreCollada::DestroyImpExp(pImpExp);
		
		InputPtr->m_fYawValue	= 0.0;
		InputPtr->m_fPitchValue	= -Ogre::Math::PI;
		Ogre::Camera * pCamera;
		pCamera = m_pEngine->GetGameScreen("BlankScreen")->GetDefaultSceneManager()->getCamera("Editor_CameraPerspective");
		pCamera->setPosition	(0.0,0.0,0.0);
		pCamera->setOrientation(Ogre::Quaternion::IDENTITY);
		pCamera->yaw			(Ogre::Degree(InputPtr->m_fYawValue));
		pCamera->pitch			(Ogre::Degree(InputPtr->m_fPitchValue));
		pCamera->moveRelative	(Ogre::Vector3(0.0,0.0, InputPtr->m_fRadius));

		// This is going to be the active camera
		m_pEngine->GetGameScreen("BlankScreen")->SetActiveCamera(pCamera);
	}
#endif
	/*! Load a particle system
	*/
	void Editor::MVLoadPS( vector<Ogre::String> tokens )
	{
	#ifdef GLE_Particle_Universe
		Ogre::String nm;

		// Parse the packet
		nm=tokens[2];

		Ogre::SceneNode * p;
		// check if there is a mesh already in memory, if so it will unload it first and then load
		if (!m_pRenderer->getSceneManager("BlankScreenSceneManager")->hasSceneNode("MySN_PS"))
		{
			p =	m_pRenderer->getSceneManager("BlankScreenSceneManager")->getRootSceneNode()->createChildSceneNode("MySN_PS");						
		}else{
			p = m_pRenderer->getSceneManager("BlankScreenSceneManager")->getSceneNode("MySN_PS");
		}

		ParticleUniverse::ParticleSystemManager::getSingleton().destroyParticleSystem("MyPS", m_pRenderer->getSceneManager("BlankScreenSceneManager") );

		// Load the particle system
		ParticleUniverse::ParticleSystem *e = ParticleUniverse::ParticleSystemManager::getSingleton().createParticleSystem("MyPS", nm, m_pRenderer->getSceneManager("BlankScreenSceneManager"));
		p->attachObject(e);
		e->prepare();
		e->start();
	#endif
	}

	/*! Obtain the list of particle systems in memory
	*/
	bool Editor::MVGetParticleSystemsList()
	{
		static Ogre::String					sParticleSystemList = "";
		static bool initialized = false;
#ifdef GLE_Particle_Universe
		Ogre::StringVector				v;

		if (!initialized)
		{
			ParticleUniverse::ParticleSystemManager::getSingleton().particleSystemTemplateNames(v);
			
			for (unsigned int i = 0 ; i < v.size() ; i++)
				sParticleSystemList += v[i] + "||";
		}
#endif
		// Send back to GLE
		Editor_GenericCommand(MV_GLE_ParticleSystemsList, sParticleSystemList);

		return true;
	}

#ifdef GLE_EDITOR_MODELVIEW
	// UNDONE show bones of a skeleton
	/*! Show the bones of a skeleton (UNIMPLEMENTED)
	*/
	bool Editor::MVShowBones( vector<Ogre::String> tokens )
	{
		try
		{
			Ogre::String nm;

			// Parse the packet
			nm=tokens[2];

			// Check if a mesh is loaded
			if (m_pRenderer->getSceneManager("BlankScreenSceneManager")->hasSceneNode("MySN") &&
				m_pRenderer->getSceneManager("BlankScreenSceneManager")->hasEntity("MyEnt"))
			{

				// Check if there are animation in the entity
				if( !m_pRenderer->getSceneManager("BlankScreenSceneManager")->getEntity("MyEnt")->hasSkeleton() ) 
				{
					Editor_ReportErrorToGLE("No skeleton");
					return false;
				}

				if(nm == "1")
					m_pRenderer->getSceneManager("BlankScreenSceneManager")->getEntity("MyEnt")->setDisplaySkeleton(true);
				else
					m_pRenderer->getSceneManager("BlankScreenSceneManager")->getEntity("MyEnt")->setDisplaySkeleton(false);

			}else{
				Editor_ReportErrorToGLE("No mesh loaded");
				return false;
			}

		}
		catch (Ogre::Exception& e)
		{
			Editor_ReportErrorToGLE(e);
		}

		return true;
	}
#endif

	/*! Plays an animation
	*/
	bool Editor::MVPlayAnimation( vector<Ogre::String> tokens )
	{
		Ogre::String nm;

		// Parse the packet
		nm=tokens[2];
		bool ans = false;
		if (m_pMVGameObject != NULL && m_pMVGameObject->m_pAnimatedEntity != NULL)
		{
			if( m_pMVGameObject->HasAnimationName(nm)) 
			{
				m_CurrentAnimationName = nm;
				m_pMVGameObject->PlayAnimation(nm);
				ans = true;
			}
			else
			{
				Editor_ReportErrorToGLE("Animation \"" + nm + "\" does not exist");
			}
		}
		else
		{
			Editor_ReportErrorToGLE("Attempt to run animation without a selected entity");
		}
		return ans;
	}


	/*! Get the list of animations
	*/
	bool Editor::MVGetListAnimations(vector<Ogre::String> tokens)
	{
		std::vector<std::string> animationNames;
		// Check if a mesh is loaded
		if (tokens.size() > 3) // There is no MV mesh loaded, but there is a name appended to the packet
		{
			Ogre::String name = tokens[2];
			int type = ParseInt(tokens[3]);
			GGETRACELOG("MV_GetListAnimations: %d %s", type, name.c_str());
			m_pMVGameObject = NULL;
			switch (type)
			{
				case OBJTYPE_SceneNode: 
					{
						//find scene node properties and respond to GLE: GGE_Response_SceneNode_Properties				
						Ogre::SceneNode* node = GetSceneManager()->getSceneNode(name);

						// test the scene node so that is not part of the skybox system
						// scene nodes that have an skybox attached don't have an entity to use and report a bug
						if(node == GetSceneManager()->getSkyBoxNode() || 
							node == GetSceneManager()->getSkyDomeNode() || 
							node == GetSceneManager()->getSkyPlaneNode())
							break;

						Ogre::SceneNode::ObjectIterator itO = node->getAttachedObjectIterator();
						bool found = false;
						while (itO.hasMoreElements() && !found)
						{
							Ogre::MovableObject* mObj = itO.getNext();
							if ( mObj->getMovableType().compare("Entity") == 0 ) 
							{
								m_pMVEntity = static_cast<Ogre::Entity*>(mObj);
								found = true;
							}
						}
						if (m_pMVEntity != NULL)
						{
							m_pMVGameObject = m_pEngine->GetForemostGameScreen(true)->GetGameObjectManager()->GetGameObject(m_pMVEntity->getName());
						}
						break;
					}
				case OBJTYPE_Entity: 
					{
						m_pMVEntity = GetSceneManager()->getEntity(name);
						if (m_pMVEntity != NULL)
						{
							m_pMVGameObject = m_pEngine->GetForemostGameScreen(true)->GetGameObjectManager()->GetGameObject(m_pMVEntity->getName());
						}
						break;
					}
				case OBJTYPE_Light: 
					{
						//Editor_ReportErrorToGLE("Cannot Load Animations for Lights");
						break;
					}
				case OBJTYPE_Camera: 
					{
						//Editor_ReportErrorToGLE("Cannot Load Animations for Cameras");
						break;
					}
			}
			if (m_pMVGameObject != NULL)
			{
				animationNames = m_pMVGameObject->GetAnimationNames();				
			}
		}
		else if (m_pEngine->GetGameScreen("BlankScreen") != NULL)
		{
			if (m_pEngine->GetGameScreen("BlankScreen")->GetGameObjectManager()->HasGameObject(m_sMVCurrentGameObjectName))
			{
				GGETRACELOG("MV_GetListAnimations: ModelViewer");
				m_pMVGameObject = m_pEngine->GetGameScreen("BlankScreen")->GetGameObjectManager()->GetGameObject(m_sMVCurrentGameObjectName);
				if (m_pMVGameObject != NULL)
				{
					animationNames = m_pMVGameObject->GetAnimationNames();
				}
			}
		}
		else // error case
		{
			Editor_ReportErrorToGLE("No mesh loaded");
			return false;
		}

		// If the animation set was updated
		if(animationNames.size() != 0) 
		{
			Ogre::String	sPacketToGLE = "";

			// Send the list of animations
			for(std::vector<std::string>::iterator 	it = animationNames.begin(); it != animationNames.end();it++)
			{
				sPacketToGLE.append(*it);
				sPacketToGLE.append("||");
			} 
			// Send back to GLE
			Editor_GenericCommand(MV_GLE_AnimationList, sPacketToGLE);
		}
		return true;
	}

	/*! Stops an animation
	*/
	int Editor::MVStopAnimation(vector<Ogre::String> tokens)
	{
		// Check if a mesh is loaded and if an animation is being played
		bool result = false;
		if (m_pMVGameObject != NULL)
		{
			if (m_CurrentAnimationName != "")
			{
				if (m_pMVGameObject->HasAnimationName(m_CurrentAnimationName))
				{
					m_pMVGameObject->StopAnimation(m_CurrentAnimationName);
					result = true;
				}
			}
		}
		return result;
	}

	/*! Show/Hide the bounding box
	*/
	bool Editor::MVBoundingBoxToggle( vector<Ogre::String> tokens )
	{
		bool ret_val = false;
		if (m_pMVGameObject != NULL && m_pMVGameObject->m_pGraphicsObject != NULL)
		{
			Ogre::SceneNode* node = m_pMVGameObject->m_pGraphicsObject->m_pOgreSceneNode;
			// set it to its opposite
			node->showBoundingBox(!(node->getShowBoundingBox()));		
			ret_val = true;
		}

		return ret_val;
	}



	/*! Send the name of the object that has been picked to GLE
	*	@param[in]	sName	Name of the node
	*	@return		1
	*/
	int	Editor::SendPickedToGLE( Ogre::String sName )
	{
		Editor_GenericCommand(GGE_Picked, sName);

		return 1;
	}

	/*! Send whether or not the dynamic movement tool is selected
	* @param[in] toolSelected  Whether or not the movement tool is selected
	*/
	int Editor::SendMovementToolSelected( bool toolSelected )
	{
		
		if(toolSelected)
		{
			Editor_GenericCommand(GGE_Activate_Dynamic_Tool, "");
		}
		else
		{
			b_DynamicToolXMarkerSelected = false;
			b_DynamicToolYMarkerSelected = false;
			b_DynamicToolZMarkerSelected = false;
			b_FreeDynamicToolMarkerSelected = false;

			Editor_GenericCommand(GGE_Deactivate_Dynamic_Tool, "");
		}
		

		return 1;
	}


	/*! Set the bounding box to a scene node
	*	@param[in]	sSceneNodeName	Name of the node
	*/
	void Editor::SelectSceneNode( Ogre::String sSceneNodeName )
	{
		// If we just want to unselect the selected node without doing anything else
		if( sSceneNodeName == "NULL" )
		{
			if(m_pSceneNodeSelected!= NULL)
			{
				m_pSceneNodeSelected->showBoundingBox(false);
				m_pSceneNodeSelected = NULL;

				
				if(b_DynamicToolActive)
				{
					//Turn off the dynamic movement tool as well.
					m_DynamicToolXNode->setVisible(false);
					m_DynamicToolYNode->setVisible(false);
					m_DynamicToolZNode->setVisible(false);
					m_FreeDynamicToolNode->setVisible(false);
					SendMovementToolSelected(false);
				}
				
				
				
			}
			return;
		}

		if( GetSceneManager()->hasSceneNode( sSceneNodeName ) )
		{
			// If there was an object previously selected we unselect it
			if(m_pSceneNodeSelected!=NULL && m_pSceneNodeSelected->getName() != sSceneNodeName)
			{
				m_pSceneNodeSelected->showBoundingBox(false);
				SendMovementToolSelected(false);
			}

			// Assign the new selected object
			m_pSceneNodeSelected = GetSceneManager()->getSceneNode(sSceneNodeName);
			// Activate the BB
			m_pSceneNodeSelected->showBoundingBox(true);

			
			//Reposition the dynamic movement tool to the currently selectedNode.
			if(b_DynamicToolActive)
			{
				RepositionMarkers();
				m_DynamicToolXNode->setVisible(true);
				m_DynamicToolYNode->setVisible(true);
				m_DynamicToolZNode->setVisible(true);
				m_FreeDynamicToolNode->setVisible(true);
			}
			
			
			
		}
		else
		{
			Editor_ReportErrorToGLE("Scene node does not exist");
		}
		
	}

	/*! Create a ray on the mouse position an check collisions against it
	*	@param[in]	mouseEvene	This is from we extract the information of the mouse coords
	*	@return		Object that has been picked
	*/
	Ogre::MovableObject* Editor::Picking( const GIS::MouseEvent &mouseEvent )
	{
		try{
			GIS::MouseState m_state;
			m_state = InputPtr->GetMouseState();

			Ogre::Ray mRayMouse = m_pEngine->GetForemostGameScreen(true)->GetActiveCamera()->getCameraToViewportRay
				(
				Ogre::Real(float(m_state.X.abs)/float(m_pEngine->GetRenderWindow()->getWidth())), 
				Ogre::Real(float(m_state.Y.abs)/float(m_pEngine->GetRenderWindow()->getHeight())) 
				);
#ifdef GLE_DEBUG_INPUT
			GGETRACELOG("Picking begins");
#endif

			// Create RaySceneQuery
			Ogre::RaySceneQuery*  mRaySceneQuery = m_pEngine->GetForemostGameScreen(true)->GetDefaultSceneManager()->createRayQuery(Ogre::Ray());
			mRaySceneQuery->setRay(mRayMouse);
			mRaySceneQuery->setSortByDistance(true);
			Ogre::RaySceneQueryResult &result = mRaySceneQuery->execute();
#ifdef GLE_DEBUG_INPUT
			GGETRACELOG("  Picking - Number Objects Detected : %d", result.size());
#endif
			GGETRACELOG("  Picking - Number Objects Detected : %d", result.size());
			// execute the query, returns a vector of hits
			if (result.size() <= 0)
			{
				// raycast did not hit an objects bounding box
				return NULL;
			}

			Ogre::MovableObject *	closestObject = NULL;
			Ogre::Real				closest_distance = -1.0f;
			Ogre::Vector3			closest_result;

			//Ogre::RaySceneQueryResult &query_result = m_pray_scene_query->getLastResults();
			for (size_t qr_idx = 0; qr_idx < result.size(); qr_idx++)
			{
				// stop checking if we have found a raycast hit that is closer
				// than all remaining entities
				/*
				if ((closest_distance >= 0.0f) &&
					(closest_distance < result[qr_idx].distance))
				{
					break;
				}
				*/
				
				
				//check for the dynamic movement tool first
				//Only select the dynamic movement tool if we have a scene node selected
				if( (result[qr_idx].movable != NULL) &&
					(result[qr_idx].movable->getMovableType().compare("ManualObject") == 0) &&
					(m_pSceneNodeSelected != NULL) &&
					b_DynamicToolActive )
				{
					Ogre::ManualObject *pManualObject = static_cast<Ogre::ManualObject*>(result[qr_idx].movable);
					
					//Determine which axis the player clicked on.
					if(pManualObject->getParentSceneNode() == m_DynamicToolXNode)
					{
						b_DynamicToolXMarkerSelected = true;
					}
					else if(pManualObject->getParentSceneNode() == m_DynamicToolYNode)
					{
						b_DynamicToolYMarkerSelected = true;
					}
					else if(pManualObject->getParentSceneNode() == m_DynamicToolZNode)
					{
						b_DynamicToolZMarkerSelected = true;
					}
					else if(pManualObject->getParentSceneNode() == m_FreeDynamicToolNode)
					{
						b_FreeDynamicToolMarkerSelected = true;
					}

					SendMovementToolSelected(true);
					SendPickedToGLE(m_pSceneNodeSelected->getName());
					SelectSceneNode(m_pSceneNodeSelected->getName());

					//If we are doing scaling, we also need the mouse's start point and the marker's start point.
					if(b_DynamicScalingMode)
					{
						m_ScalingMarkerStartPoint = result[qr_idx].movable->getParentNode()->getPosition(); 
						m_ScalingMouseStartPoint = Ogre::Vector2(Ogre::Real(float(m_state.X.abs)), Ogre::Real(float(m_state.Y.abs)));
						m_StartingScale = m_pSceneNodeSelected->getScale();
					}
					//If we are doing rotation, we need the marker's start point
					else if(b_DynamicRotationMode)
					{
						m_RotationMarkerStartPoint = result[qr_idx].movable->getParentNode()->getPosition();
					}

					//The Dynamic Movement tool has the highest priority. So break here as it is time to move the object.
					return NULL;

				}
				
				
				// only check this result if its a hit against an entity
				if ((result[qr_idx].movable != NULL) &&
					(result[qr_idx].movable->getMovableType().compare("Entity") == 0)  )
				{
					// get the entity to check
					Ogre::Entity *pentity = static_cast<Ogre::Entity*>(result[qr_idx].movable);           

					GGETRACELOG("  Picking - Object Detected : %s", pentity->getParentSceneNode()->getName().c_str());
					// mesh data to retrieve         
					size_t vertex_count;
					size_t index_count;
					Ogre::Vector3 *vertices;
					unsigned long *indices;

					// get the mesh information
					GetMeshInformation(pentity->getMesh(), vertex_count, vertices, index_count, indices,             
						pentity->getParentSceneNode()->getPosition(),
						pentity->getParentSceneNode()->getOrientation(),
						pentity->getParentSceneNode()->getScale());

					// test for hitting individual triangles on the mesh
					bool new_closest_found = false;
					for (int i = 0; i < static_cast<int>(index_count); i += 3)
					{
						// check for a hit against this triangle
						pair<bool, Ogre::Real> hit = Ogre::Math::intersects(mRayMouse, vertices[indices[i]],
							vertices[indices[i+1]], vertices[indices[i+2]], true, false);

						// if it was a hit check if its the closest
						if (hit.first)
						{
							if ((closest_distance < 0.0f) ||
								(hit.second < closest_distance))
							{
								// this is the closest so far, save it off
								closestObject = result[qr_idx].movable;
								closest_distance = hit.second;
								new_closest_found = true;
							}
						}
					}

					// free the verticies and indicies memory
					delete[] vertices;
					delete[] indices;

					// if we found a new closest raycast for this object, update the
					// closest_result before moving on to the next object.
					if (new_closest_found)
					{
						closestObject = result[qr_idx].movable;
						closest_result = mRayMouse.getPoint(closest_distance);               
					}
				}       
			}

			// return the result
			if (closest_distance >= 0.0f)
			{
#ifdef GLE_DEBUG_INPUT
				GGETRACELOG("  Picked - %s", closestObject->getParentSceneNode()->getName().c_str());
#endif
				// raycast success
				SendPickedToGLE(closestObject->getParentSceneNode()->getName());	
				SelectSceneNode(closestObject->getParentSceneNode()->getName());

				if (closestObject->getParentSceneNode()->getName() == "NULL")
				{
					m_pMVGameObject = NULL;
				}
			}
			else
			{
#ifdef GLE_DEBUG_INPUT
				GGETRACELOG("  Nothing picked");
#endif
				SendPickedToGLE("NULL");
				SelectSceneNode("NULL");
			} 
#ifdef GLE_DEBUG_INPUT
			GGETRACELOG("Picking ends");
#endif
			
		}catch (Ogre::Exception& e){
			Editor_ReportErrorToGLE(e);
		}

		return NULL;
	}
	
	Ogre::SceneManager*	Editor::GetSceneManager()
	{
		return m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager();
	}

	void Editor::PreprocessSave(EditorProtocol protocolHandle)
	{
		string message;
		for(unsigned int con = 0; con < m_vMessageHandles.size(); con++)
		{
			if(m_vMessageHandles[con]->PreprocessSave(protocolHandle))
			{
				return;
			}
		}
	}
	void Editor::PostprocessSave(EditorProtocol protocolHandle)
	{
		string message;
		for(unsigned int con = 0; con < m_vMessageHandles.size(); con++)
		{
			if(m_vMessageHandles[con]->PostprocessSave(protocolHandle))
			{
				return;
			}
		}
	}
	
	/*! Get the mesh information for the given mesh. Part of the Picking method (Code found in Wiki: www.ogre3d.org/wiki/index.php/RetrieveVertexData)
	*	@param[in]	sName	Name of the node
	*/ 
	void Editor::GetMeshInformation(const Ogre::MeshPtr mesh,
		size_t &vertex_count,
		Ogre::Vector3* &vertices,
		size_t &index_count,
		unsigned long* &indices,
		const Ogre::Vector3 &position,
		const Ogre::Quaternion &orient,
		const Ogre::Vector3 &scale)
	{
		bool added_shared = false;
		size_t current_offset = 0;
		size_t shared_offset = 0;
		size_t next_offset = 0;
		size_t index_offset = 0;

		vertex_count = index_count = 0;

		// Calculate how many vertices and indices we're going to need
		for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
		{
			Ogre::SubMesh* submesh = mesh->getSubMesh( i );

			// We only need to add the shared vertices once
			if(submesh->useSharedVertices)
			{
				if( !added_shared )
				{
					vertex_count += mesh->sharedVertexData->vertexCount;
					added_shared = true;
				}
			}
			else
			{
				vertex_count += submesh->vertexData->vertexCount;
			}

			// Add the indices
			index_count += submesh->indexData->indexCount;
		}


		// Allocate space for the vertices and indices
		vertices = new Ogre::Vector3[vertex_count];
		indices = new unsigned long[index_count];

		added_shared = false;

		// Run through the submeshes again, adding the data into the arrays
		for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
		{
			Ogre::SubMesh* submesh = mesh->getSubMesh(i);

			Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

			if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared))
			{
				if(submesh->useSharedVertices)
				{
					added_shared = true;
					shared_offset = current_offset;
				}

				const Ogre::VertexElement* posElem =
					vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

				Ogre::HardwareVertexBufferSharedPtr vbuf =
					vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

				unsigned char* vertex =
					static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

				// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
				//  as second argument. So make it float, to avoid trouble when Ogre::Real will
				//  be comiled/typedefed as double:
				//      Ogre::Real* pReal;
				float* pReal;

				for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
				{
					posElem->baseVertexPointerToElement(vertex, &pReal);

					Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);

					vertices[current_offset + j] = (orient * (pt * scale)) + position;
				}

				vbuf->unlock();
				next_offset += vertex_data->vertexCount;
			}


			Ogre::IndexData* index_data = submesh->indexData;
			size_t numTris = index_data->indexCount / 3;
			Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

			bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

			unsigned long*  pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
			unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);


			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;

			if ( use32bitindexes )
			{
				for ( size_t k = 0; k < numTris*3; ++k)
				{
					indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
				}
			}
			else
			{
				for ( size_t k = 0; k < numTris*3; ++k)
				{
					indices[index_offset++] = static_cast<unsigned long>(pShort[k]) +
						static_cast<unsigned long>(offset);
				}
			}

			ibuf->unlock();
			current_offset = next_offset;
		}
	} 


	//Creates a cube for the player to click on when they are using the dynamic scaling tool
	void Editor::SetupCube(Ogre::ManualObject* c, std::string s)
	{
		// tell OGRE we use the OT_TRIANGLE_STRIP to draw
		c->begin( s ,Ogre::RenderOperation::OT_TRIANGLE_STRIP);

		// fill the manual object with vertex, texcoord, normal ...

		// Front face
		c->position(CUBE_SIZE,0,0); c->textureCoord(0,0);
		c->position(CUBE_SIZE,CUBE_SIZE,0); c->textureCoord(1,0);
		c->position(0,0,0); c->textureCoord(1,1);
		c->position(0,CUBE_SIZE,0); c->textureCoord(0,1);

		// Left face
		c->position(0,0,-CUBE_SIZE); c->textureCoord(1,0);
		c->position(0,CUBE_SIZE,-CUBE_SIZE); c->textureCoord(1,1);

		// Back face
		c->position(CUBE_SIZE,0,-CUBE_SIZE); c->textureCoord(0,0);
		c->position(CUBE_SIZE,CUBE_SIZE,-CUBE_SIZE); c->textureCoord(0,1);

		// Right face
		c->position(CUBE_SIZE,0,0); c->textureCoord(1,0);
		c->position(CUBE_SIZE,CUBE_SIZE,0); c->textureCoord(1,1);

		c->end(); // end of a TRIANGLE_STRIP

		// begin the second TRIANGLE_STRIP ( top face )
		c->begin( s ,Ogre::RenderOperation::OT_TRIANGLE_STRIP);
		      
		c->position(CUBE_SIZE,CUBE_SIZE,0); c->textureCoord(0,0);
		c->position(CUBE_SIZE,CUBE_SIZE,-CUBE_SIZE); c->textureCoord(1,0);
		c->position(0,CUBE_SIZE,0); c->textureCoord(1,1);
		c->position(0,CUBE_SIZE,-CUBE_SIZE); c->textureCoord(0,1);
		
		c->end();

		// begin the third TRIANGLE_STRIP ( bottom face )
		c->begin( s ,Ogre::RenderOperation::OT_TRIANGLE_STRIP);
		      
		c->position(0,0,0); c->textureCoord(0,0);
		c->position(0,0,-CUBE_SIZE); c->textureCoord(1,0);
		c->position(CUBE_SIZE,0,0); c->textureCoord(1,1);
		c->position(CUBE_SIZE,0,-CUBE_SIZE); c->textureCoord(0,1);
		
		c->end();

		c->setRenderQueueGroup(90);
	}

	//Creates a sphere for the player to click on when they are using the dynamic rotation tool.
	void Editor::SetupSphere(Ogre::ManualObject* manual, std::string s, const int nRings, const int nSegments)
	{
		 float r = CUBE_SIZE / 2;
		 manual->begin(s, Ogre::RenderOperation::OT_TRIANGLE_LIST);
 
		 float fDeltaRingAngle = (Ogre::Math::PI / nRings);
		 float fDeltaSegAngle = (2 * Ogre::Math::PI / nSegments);
		 unsigned short wVerticeIndex = 0 ;
 
		 // Generate the group of rings for the sphere
		 for( int ring = 0; ring <= nRings; ring++ ) {
			 float r0 = r * sinf (ring * fDeltaRingAngle);
			 float y0 = r * cosf (ring * fDeltaRingAngle);
 
			 // Generate the group of segments for the current ring
			 for(int seg = 0; seg <= nSegments; seg++) {
				 float x0 = r0 * sinf(seg * fDeltaSegAngle);
				 float z0 = r0 * cosf(seg * fDeltaSegAngle);
 
				 // Add one vertex to the strip which makes up the sphere
				 manual->position( x0, y0, z0);
				 manual->normal(Ogre::Vector3(x0, y0, z0).normalisedCopy());
				 manual->textureCoord((float) seg / (float) nSegments, (float) ring / (float) nRings);
 
				 if (ring != nRings) {
					 // each vertex (except the last) has six indicies pointing to it
					 manual->index(wVerticeIndex + nSegments + 1);
					 manual->index(wVerticeIndex);               
					 manual->index(wVerticeIndex + nSegments);
					 manual->index(wVerticeIndex + nSegments + 1);
					 manual->index(wVerticeIndex + 1);
					 manual->index(wVerticeIndex);
					 wVerticeIndex ++;
					 }
			 }; // end for seg
		 } // end for ring
		 manual->end();
		 manual->setRenderQueueGroup(90);
	}

	void Editor::SetupCone(Ogre::ManualObject* manual, std::string s)
	{
		float height = CUBE_SIZE;
		float radius = CUBE_SIZE/2;
		float numSegBase = 16.0f;
		float numSegHeight = 16.0f;
		float uTile = 1.0f;
		float vTile = 1.0f;
		unsigned short  offset = 0;

		Ogre::Real deltaAngle = (Ogre::Math::TWO_PI / numSegBase);
		Ogre::Real deltaHeight = height/(Ogre::Real)numSegHeight;

		Ogre::Vector3 refNormal = Ogre::Vector3(radius, height, 0.f).normalisedCopy();
		Ogre::Quaternion q;

		manual->begin(s, Ogre::RenderOperation::OT_TRIANGLE_LIST);

		for (int i = 0; i <=numSegHeight; i++)
		{
			Ogre::Real r0 = radius * (1 - i / (Ogre::Real)numSegHeight);
			for (int j = 0; j<=numSegBase; j++)
			{
				Ogre::Real x0 = r0* cosf(j*deltaAngle);
				Ogre::Real z0 = r0 * sinf(j*deltaAngle);
				manual->position(x0, i*deltaHeight, z0);
				q.FromAngleAxis(Ogre::Radian(-j*deltaAngle), Ogre::Vector3::UNIT_Y);
				manual->normal(q*refNormal);
				//if (i != numSegHeight)
				manual->textureCoord(j/(Ogre::Real)numSegBase*uTile, i/(Ogre::Real)numSegHeight*vTile);
				/*else
				manual->textureCoord(0.f, i/(Ogre::Real)numSegHeight*vTile);*/

				if (i != numSegHeight&& j != numSegBase)
				{
					manual->index(Ogre::uint32(offset + numSegBase + 2));
					manual->index(offset);
					manual->index(Ogre::uint32(offset + numSegBase+1));
					manual->index(Ogre::uint32(offset + numSegBase + +2));
					manual->index(offset + 1);
					manual->index(offset);
				}

				offset ++;
			}
		}

		//low cap
		int centerIndex = offset;
		manual->position(0,0,0);
		manual->normal(Ogre::Vector3::NEGATIVE_UNIT_Y);
		manual->textureCoord(0.0,vTile);
		offset++;
		for (int j=0; j<=numSegBase; j++)
		{
			Ogre::Real x0 = radius * cosf(j*deltaAngle);
			Ogre::Real z0 = radius * sinf(j*deltaAngle);

			manual->position(x0, 0.0f, z0);
			manual->normal(Ogre::Vector3::NEGATIVE_UNIT_Y);
			manual->textureCoord(j/(Ogre::Real)numSegBase*uTile,0.0);
			if (j!=numSegBase)
			{
				manual->index(centerIndex);
				manual->index(offset);
				manual->index(offset+1);
			}
			offset++;
		}

		manual->end();
		manual->setRenderQueueGroup(90);
	}

	void Editor::CreateMaterial(Ogre::MaterialPtr p, float r, float g, float b, string name)
	{
		p = Ogre::MaterialManager::getSingleton().create(name, "General", true);
		p->setDiffuse(r,g,b,1);
		p->setAmbient(r,g,b);
		p->setReceiveShadows(false);
	}

	//This function takes the dynamic tool markers and repositions them around the currently selected scene node.
	void Editor::RepositionMarkers(void)
	{
		
		if(m_pSceneNodeSelected != NULL)
		{
			if(b_DynamicRotationMode)
			{
				m_DynamicToolXNode->setPosition(m_pSceneNodeSelected->getPosition() + Ogre::Vector3(CUBE_SIZE * 3, 0, 0) );
				m_DynamicToolYNode->setPosition(m_pSceneNodeSelected->getPosition() + Ogre::Vector3(0, CUBE_SIZE * 3, 0));
				m_DynamicToolZNode->setPosition(m_pSceneNodeSelected->getPosition() + Ogre::Vector3(0, 0, CUBE_SIZE * 3));
				m_FreeDynamicToolNode->setPosition(m_pSceneNodeSelected->getPosition());
			}
			else if(b_DynamicPositioningMode)
			{
				//The offset is needed for drawing the free position cube correctly. 
				//This is due to its center location not being the center of the cube, but actually one of the corners.
				Ogre::Vector3 offset = Ogre::Vector3(CUBE_SIZE/2,CUBE_SIZE/2, -CUBE_SIZE/2);
				m_DynamicToolXNode->setPosition(m_pSceneNodeSelected->getPosition() + Ogre::Vector3(CUBE_SIZE * 3, 0, 0) );
				m_DynamicToolYNode->setPosition(m_pSceneNodeSelected->getPosition() + Ogre::Vector3(0, CUBE_SIZE * 3, 0));
				m_DynamicToolZNode->setPosition(m_pSceneNodeSelected->getPosition() + Ogre::Vector3(0, 0, CUBE_SIZE * 3));
				m_FreeDynamicToolNode->setPosition(m_pSceneNodeSelected->getPosition() - offset);
			}
			else
			{
				//The offset is needed for drawing the cubes correctly. 
				//This is due to their center location not being the center of the cube, but actually one of the corners.
				Ogre::Vector3 offset = Ogre::Vector3(CUBE_SIZE/2,CUBE_SIZE/2, -CUBE_SIZE/2);
				Ogre::Vector3 offset2 = Ogre::Vector3(CUBE_SIZE/2,CUBE_SIZE/2, CUBE_SIZE/2);
				m_DynamicToolXNode->setPosition(m_pSceneNodeSelected->getPosition() + offset2 + Ogre::Vector3(CUBE_SIZE * 2, 0, 0) );
				m_DynamicToolYNode->setPosition(m_pSceneNodeSelected->getPosition() - offset + Ogre::Vector3(0, CUBE_SIZE * 3, 0));
				m_DynamicToolZNode->setPosition(m_pSceneNodeSelected->getPosition() - offset + Ogre::Vector3(0, 0, CUBE_SIZE * 2));
				m_FreeDynamicToolNode->setPosition(m_pSceneNodeSelected->getPosition() - offset);
			}
		}
		
		
		
	}

	//This function sets up what marker shapes should show up depending on what mode the dynamic editor tool is in.
	void Editor::SetMarkerShapes(void)
	{
		//Only do something if the tool is active
		if(b_DynamicToolActive)
		{
			//unload the current objects
			m_DynamicToolXNode->detachAllObjects();
			m_DynamicToolYNode->detachAllObjects();
			m_DynamicToolZNode->detachAllObjects();
			m_FreeDynamicToolNode->detachAllObjects();

			if(b_DynamicPositioningMode)
			{
				//Use cones
				m_DynamicToolXNode->attachObject(m_DynamicConeXMarker);
				m_DynamicToolYNode->attachObject(m_DynamicConeYMarker);
				m_DynamicToolZNode->attachObject(m_DynamicConeZMarker);
				m_FreeDynamicToolNode->attachObject(m_FreeDynamicCubeMarker);
			}
			else if(b_DynamicRotationMode)
			{
				//Use Spheres
				m_DynamicToolXNode->attachObject(m_DynamicSphereXMarker);
				m_DynamicToolYNode->attachObject(m_DynamicSphereYMarker);
				m_DynamicToolZNode->attachObject(m_DynamicSphereZMarker);
				m_FreeDynamicToolNode->attachObject(m_FreeDynamicSphereMarker);
			}
			else if(b_DynamicScalingMode)
			{
				//Use cubes
				m_DynamicToolXNode->attachObject(m_DynamicCubeXMarker);
				m_DynamicToolYNode->attachObject(m_DynamicCubeYMarker);
				m_DynamicToolZNode->attachObject(m_DynamicCubeZMarker);
				m_FreeDynamicToolNode->attachObject(m_FreeDynamicCubeMarker);
			}

			RepositionMarkers();
		}
	}
}
#endif