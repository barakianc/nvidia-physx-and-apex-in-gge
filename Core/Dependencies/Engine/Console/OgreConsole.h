#ifndef OGRE_CONSOLE_H
#define OGRE_CONSOLE_H

#include <OgreFrameListener.h>
#include <Ogre.h>
#include <list>
#include <vector>
#include "GIS.h"

#ifdef TBB_PARALLEL_OPTION
#include "ConsoleUpdateTask.h"
#endif
namespace GamePipe{
    class OgreConsole: public Ogre::Singleton<OgreConsole>, Ogre::FrameListener, Ogre::LogListener
    {
    public:
        void addCommand(const Ogre::String &command, void (*)(std::vector<Ogre::String>&));
        virtual bool frameEnded(const Ogre::FrameEvent&);
        virtual bool update(Ogre::Real);

        virtual bool frameStarted(const Ogre::FrameEvent&);
        static OgreConsole& getSingleton();

        static OgreConsole* getSingletonPtr();
        void init(Ogre::Root*, Ogre::SceneManager*);
        bool isVisible(){return m_visible;}

        //log
		void messageLogged( const Ogre::String& message, Ogre::LogMessageLevel lml, bool maskDebug, const Ogre::String &logName, bool& skipThisMessage ) { if (!skipThisMessage) { print(logName+": "+message);} }
        void onKeyPressed(const GIS::KeyEvent &arg);

        void print(const Ogre::String &text);

        void removeCommand(const Ogre::String &command);

        void setVisible(bool visible);
        void shutdown();

    private:
        std::map<Ogre::String,void (*)(std::vector<Ogre::String>&)> m_commands;

        float m_height;
        bool m_initialized;
        std::list<Ogre::String> m_lines;
        Ogre::SceneNode *m_node;
        Ogre::Overlay *m_overlay;

        Ogre::String m_prompt;
        Ogre::Rectangle2D *m_rectangle;
        Ogre::Root *m_root;
        Ogre::SceneManager *m_scene;
        unsigned int m_start_line;
        Ogre::OverlayElement *m_textbox;
        bool m_update_overlay;
        bool m_visible;
        OgreConsole();
        OgreConsole(OgreConsole&){};
        ~OgreConsole();
    };
};

#endif