#include "StdAfx.h"
#include "OgreConsole.h"

#define CONSOLE_LINE_LENGTH 145
#define CONSOLE_LINE_COUNT 25

using namespace GamePipe;

template<> OgreConsole *Ogre::Singleton<OgreConsole>::msSingleton=0;

void OgreConsole::addCommand(const Ogre::String &command, void (*func)(std::vector<Ogre::String>&)){
    m_commands[command]=func;
};

bool OgreConsole::frameEnded(const Ogre::FrameEvent &evt){
    return true;
};

bool OgreConsole::update( Ogre::Real deltaTime)
{
    if(!m_initialized)
        return false;

    if(m_visible&&m_height<1){
        m_height+=deltaTime*2;
        m_textbox->show();
        if(m_height>=1){
            m_height=1;
        }
    }
    else if(!m_visible&&m_height>0){
        m_height-=deltaTime*2;
        if(m_height<=0){
            m_height=0;
            m_textbox->hide();
        }
    }

#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		ConsoleUpdatePositionTask* newTask = new ConsoleUpdatePositionTask(m_rectangle,m_textbox,m_height);
		TaskContainer* container = new TaskContainer((void*)newTask,Console_Update_Position_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		m_textbox->setPosition(0,(m_height-1)*0.5);
		m_rectangle->setCorners(-1,1+m_height,1,1-m_height);
		m_rectangle->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
	}
#else
	m_textbox->setPosition(0, Ogre::Real((m_height-1)*0.5f));
    m_rectangle->setCorners(-1,1+m_height,1,1-m_height);
    m_rectangle->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
#endif

    if(m_update_overlay){
        Ogre::String text;
        std::list<Ogre::String>::iterator i,start,end;

        //make sure is in range
        if(m_start_line>m_lines.size())
            m_start_line=m_lines.size();

        int lcount=0;
        start=m_lines.begin();
        for(unsigned int c=0; c <m_start_line;c++)
            start++;
        end=start;
        for(int c=0;c<CONSOLE_LINE_COUNT;c++){
            if(end==m_lines.end())
                break;
            end++;
        }
        for(i=start;i!=end;i++)
            text+=(*i)+"\n";

        //add the prompt
        text+="] "+m_prompt;

#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		ConsoleUpdateTextTask* newTask = new CConsoleUpdateTextTask(m_textbox,text);
		TaskContainer* container = new TaskContainer((void*)newTask,Console_Update_Text_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		m_textbox->setCaption(text);
	}
#else
        m_textbox->setCaption(text);
#endif

        m_update_overlay=false;
    }
    return true;
};

bool OgreConsole::frameStarted(const Ogre::FrameEvent &evt){
    if(m_visible&&m_height<1){
        m_height+=evt.timeSinceLastFrame*2;
        m_textbox->show();
        if(m_height>=1){
            m_height=1;
        }
    }
    else if(!m_visible&&m_height>0){
        m_height-=evt.timeSinceLastFrame*2;
        if(m_height<=0){
            m_height=0;
            m_textbox->hide();
        }
    }

#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		ConsoleUpdatePositionTask* newTask = new ConsoleUpdatePositionTask(m_rectangle,m_textbox,m_height);
		TaskContainer* container = new TaskContainer((void*)newTask,Console_Update_Position_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		m_textbox->setPosition(0,(m_height-1)*0.5);
		m_rectangle->setCorners(-1,1+m_height,1,1-m_height);
		m_rectangle->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
	}
#else
	m_textbox->setPosition(Ogre::Real(0.0f),Ogre::Real((m_height-1)*0.5f));
    m_rectangle->setCorners(-1,1+m_height,1,1-m_height);
    m_rectangle->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
#endif

    if(m_update_overlay){
        Ogre::String text;
        std::list<Ogre::String>::iterator i,start,end;

        //make sure is in range
        if(m_start_line>m_lines.size())
            m_start_line=m_lines.size();

        int lcount=0;
        start=m_lines.begin();
        for(unsigned int c=0; c <m_start_line;c++)
            start++;
        end=start;
        for(int c=0;c<CONSOLE_LINE_COUNT;c++){
            if(end==m_lines.end())
                break;
            end++;
        }
        for(i=start;i!=end;i++)
            text+=(*i)+"\n";

        //add the prompt
        text+="] "+m_prompt;

#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		ConsoleUpdateTextTask* newTask = new CConsoleUpdateTextTask(m_textbox,text);
		TaskContainer* container = new TaskContainer((void*)newTask,Console_Update_Text_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		m_textbox->setCaption(text);
	}
#else
		try
		{
			m_textbox->setCaption(text);
		}
		catch (...)
		{
		}
        
#endif

        m_update_overlay=false;
    }
    return true;
};

OgreConsole &OgreConsole::getSingleton()
{  
    if(!msSingleton){
		
        msSingleton = new OgreConsole();
    }
    assert(msSingleton);  
    return(*msSingleton);
}

OgreConsole *OgreConsole::getSingletonPtr()
{
    if(!msSingleton){
		
        msSingleton = new OgreConsole();
    }
    return msSingleton;
}

void OgreConsole::init(Ogre::Root *root, Ogre::SceneManager *scene){
    if (m_initialized){
        shutdown();
    }

    if(!root->getSceneManagerIterator().hasMoreElements())
        OGRE_EXCEPT( Ogre::Exception::ERR_INTERNAL_ERROR, "No scene manager found!", "init" );

    m_root=root;
    
    root->addFrameListener(this);

    m_visible = false;

    m_height=1;

    Ogre::StringVector groups = Ogre::ResourceGroupManager::getSingletonPtr()->getResourceGroups();

    if(find(groups.begin(), groups.end(), "ConsoleGroup") == groups.end()){
        Ogre::ResourceGroupManager::getSingleton().createResourceGroup("ConsoleGroup");
    }

    Ogre::ResourceGroupManager::getSingleton().declareResource("console/background", "Material", "ConsoleGroup");
    Ogre::ResourceGroupManager::getSingleton().loadResourceGroup("ConsoleGroup");
    Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("ConsoleGroup");

    // Create background rectangle covering the whole screen
    m_rectangle = new Ogre::Rectangle2D(true);
    m_rectangle->setCorners(-1, 1, 1, 1-m_height);
    m_rectangle->setMaterial("console/background");
    m_rectangle->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY);
    m_rectangle->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    m_node = scene->getRootSceneNode()->createChildSceneNode();
    m_node->attachObject(m_rectangle);

    if(Ogre::OverlayManager::getSingleton().hasOverlayElement("ConsoleText"))
        m_textbox=Ogre::OverlayManager::getSingleton().getOverlayElement("ConsoleText");
    else
        m_textbox=Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea","ConsoleText");
    m_textbox->setCaption("");
    m_textbox->setMetricsMode(Ogre::GMM_RELATIVE);
    m_textbox->setPosition(0,0);
    m_textbox->setParameter("font_name","EngineFont");
    m_textbox->setParameter("colour_top","1 1 1");
    m_textbox->setParameter("colour_bottom","1 1 1");
    m_textbox->setParameter("char_height","0.02");

    m_overlay=Ogre::OverlayManager::getSingleton().getByName("Console");
    if(!m_overlay)
        m_overlay=Ogre::OverlayManager::getSingleton().create("Console");   
    m_overlay->add2D((Ogre::OverlayContainer*)m_textbox);
    m_overlay->show();
    Ogre::LogManager::getSingleton().getDefaultLog()->addListener(this);

    m_initialized = true;
};

OgreConsole::OgreConsole(){
    m_start_line=0;
    m_height = 0;
    m_initialized = false;
};

OgreConsole::~OgreConsole(){
    shutdown();
};

void OgreConsole::onKeyPressed(const GIS::KeyEvent &arg){
    if(!m_visible)
        return;
    if (arg.key == GIS::KC_RETURN)
    {
        //split the parameter list
        const char *str=m_prompt.c_str();
        std::vector<Ogre::String> params;
        Ogre::String param="";
        for(unsigned int c=0; c < m_prompt.length();c++){
            if(str[c]==' '){
                if(param.length())
                    params.push_back(param);
                param="";
            }
            else
                param+=str[c];
        }
        if(param.length())
            params.push_back(param);

        print(m_prompt);
        m_prompt="";

        //try to execute the command
        std::map<Ogre::String,void(*)(std::vector<Ogre::String>&)>::iterator i;
        for(i=m_commands.begin();i!=m_commands.end();i++){
            if((*i).first==params[0]){
                if((*i).second)
                    (*i).second(params);
                break;
            }
        }
    }
    if (arg.key == GIS::KC_BACK)
        m_prompt=m_prompt.substr(0,m_prompt.length()-1);
    if (arg.key == GIS::KC_PGUP)
    {
        if(m_start_line>0)
            m_start_line--;
    }
    if (arg.key == GIS::KC_PGDOWN)
    {
        if(m_start_line < m_lines.size())
            m_start_line++;
    }
    else
    {
        char legalchars[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890+!\"#%&/()=?[]\\*-_.:,; ";
        for(int c=0;c<sizeof(legalchars) - 1;c++){
            if(legalchars[c]==arg.text){
                m_prompt+=arg.text;
                break;
            }
        }
    }
    m_update_overlay=true;
};
void OgreConsole::print(const Ogre::String &text){
    //subdivide it into lines
    const char *str=text.c_str();
    int start=0,count=0;
    int len=text.length();
    Ogre::String line;
    for(int c=0;c<len;c++){
        if(str[c]=='\n'||line.length() >= CONSOLE_LINE_LENGTH){
            m_lines.push_back(line);
            line="";
        }
        if(str[c]!='\n')
            line+=str[c];
    }
    if(line.length())
        m_lines.push_back(line);
    if(m_lines.size() > CONSOLE_LINE_COUNT)
        m_start_line=m_lines.size() - CONSOLE_LINE_COUNT;
    else
        m_start_line=0;
    m_update_overlay=true;
};

void OgreConsole::removeCommand(const Ogre::String &command){
    m_commands.erase(m_commands.find(command));
};
void OgreConsole::setVisible(bool visible){
    m_visible=visible;
};

void OgreConsole::shutdown(){
    if(!m_initialized)
        return;
    delete m_rectangle;

    m_overlay->clear();
    Ogre::OverlayManager::getSingleton().destroyOverlayElement(m_textbox);
    Ogre::OverlayManager::getSingleton().destroy(m_overlay);
    m_root->removeFrameListener(this);
    Ogre::LogManager::getSingleton().getDefaultLog()->removeListener(this);

    m_initialized = false;
};

