#include "StdAfx.h"
#include "AnimatedHavokEntity.h"

//#include "Windows.h"
#include <stdio.h>
#include <fstream>

const float eclipse = 0.00000001f;

#ifdef TBB_PARALLEL_OPTION
#include "SkeletonUpdateBoneTask.h"
#include "SkeletonUpdateRootTask.h"
#include "TaskContainer.h"
#endif

using namespace AnimationManager;

AnimatedHavokEntity::AnimatedHavokEntity()
{
	//////////////////////////////////
	// initialize pointers to NULL
	//////////////////////////////////

	m_pGraphicsObject = NULL;
	m_hkSkeletonInstance = NULL;
	m_hkLoader = NULL;
	m_hkSkeleton = NULL;
	m_ogSkeleton = NULL;
	m_worldFromModel = NULL;
	ragDollEntity = NULL;

	ragDollAttachedToken = false;
	ragDollPoseToken = false;
}

AnimatedHavokEntity::~AnimatedHavokEntity()
{
	//////////////////////////////////////
	// set pointers to NULL
	//////////////////////////////////////

	m_pGraphicsObject = NULL;
	m_ogSkeleton = NULL;
	m_hkSkeleton = NULL;

	//////////////////////////////////////
	// delete objects whose memory we own
	//////////////////////////////////////

	if (ragDollEntity)
	{
		delete ragDollEntity;
	}

	if (m_hkSkeletonInstance)
	{
		delete m_hkSkeletonInstance;
	}

	if (m_hkLoader)
	{
		delete m_hkLoader;
	}

	if (m_worldFromModel)
	{
		delete m_worldFromModel;
	}

	//////////////////////////////////////
	// clear hashmaps we don't need to
	// delete their contents, because we
	// do not own their memory
	//////////////////////////////////////

	m_hkAnimations.clear();
	m_hkBindings.clear();
	m_hkControls.clear();
}

AnimatedHavokEntity::AnimatedHavokEntity(GraphicsObject* f_pGraphicsObject, const char* f_sRigPath, const char* f_sName)
{
	//////////////////////////////////
	// initialize pointers to NULL
	//////////////////////////////////

	m_pGraphicsObject = NULL;
	m_hkSkeletonInstance = NULL;
	m_hkLoader = NULL;
	m_hkSkeleton = NULL;
	m_ogSkeleton = NULL;
	m_worldFromModel = NULL;
	ragDollEntity = NULL;

	//////////////////////////////////
	// initialize bools to false
	//////////////////////////////////
	ragDollAttachedToken = false;
	ragDollPoseToken = false;

	//////////////////////////////////
	// initialize strings to "None"
	//////////////////////////////////
	m_sActiveAnim = _strdup("None");
	m_sDefaultAnim = _strdup("None");

	defaultBlendDuration = 0.5f;

	transitionLoop = false;
	transition = false;
	overlay = false;

	m_worldFromModel = new hkQsTransform();

	// initialize the worldFromModel matrix
	m_worldFromModel->setIdentity();

	if (f_pGraphicsObject->m_pOgreEntity->hasSkeleton())
	{
		f_pGraphicsObject->m_pOgreSkeleton = f_pGraphicsObject->m_pOgreEntity->getSkeleton();
		f_pGraphicsObject->setManualControlOgreSkeleton();
	}

    ragDollAttachedToken = false;

	// set member variables
	m_sName = _strdup(f_sName);
	m_pGraphicsObject = f_pGraphicsObject;

	// initialize the loader
	m_hkLoader = new hkLoader();

	std::string afolder;
	afolder = _strdup(f_sRigPath);
	afolder = afolder.substr(0, afolder.find(".hkx"));

	// get the rig
	{
		// get the path to the rig
		std::string rpath;
		rpath.append("..\\..\\");
		rpath = rpath.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		rpath = rpath.append("\\Media\\Havok\\");
		rpath = rpath.append(afolder);
		rpath = rpath.append("\\");
		rpath = rpath.append(f_sRigPath);

		// get the rig's container from the binary .hkx file
		// and assign it to m_hkSkeleton
		hkRootLevelContainer* container = m_hkLoader->load(rpath.c_str());
        // commented out the assertion to allow havok character proxies without animations.
		//HK_ASSERT2(0x27343437, container != HK_NULL , "Could not load asset -- rig!");
        if(!container)
            return;
		hkaAnimationContainer* ac = reinterpret_cast<hkaAnimationContainer*>(container->findObjectByType(hkaAnimationContainerClass.getName()));
		HK_ASSERT2(0x27343435, ac && (ac->m_skeletons.getSize() > 0), "No skeleton loaded when loading rig file!");
		m_hkSkeleton = ac->m_skeletons[0];
	}

	m_hkSkeletonInstance = new hkaAnimatedSkeleton(m_hkSkeleton);

	// get the animations
	{
		std::string line;

		// get the path to the list of animations
		std::string apath;
		apath = apath.append("..\\..\\");
		apath = apath.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		apath = apath.append("\\Media\\Havok\\");
		apath = apath.append(afolder);
		apath = apath.append("\\");
		apath = apath.append("animations.txt");

		std::ifstream fin(apath.c_str());

		while(fin >> line)
		{
			// add the animation name to the array
			m_aAnimationNames.push_back(line);

			// get the path to the animation file
			std::string aline;
			aline.append("..\\..\\");
			aline = aline.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
			aline = aline.append("\\Media\\Havok\\");
			aline = aline.append(afolder);
			aline = aline.append("\\");
			aline = aline.append(line);
			aline = aline.append(".hkx");

			// get the animation
			hkRootLevelContainer* container = m_hkLoader->load(aline.c_str());
			HK_ASSERT2(0x27343437, container != HK_NULL , "Could not load asset -- animation");
			hkaAnimationContainer* ac = reinterpret_cast<hkaAnimationContainer*>( container->findObjectByType( hkaAnimationContainerClass.getName() ));
			HK_ASSERT2(0x27343435, ac && (ac->m_animations.getSize() > 0 ), "No animation loaded!");
			HK_ASSERT2(0x27343435, ac && (ac->m_bindings.getSize() > 0), "No binding loaded!");

			// THIS DOES NOTHING! 
			// indicate that either a rig was loaded when
			// it shouldn't have been, or that you properly
			// loaded an animation without a rig
	/*		if (ac->m_skeletons.getSize() == 0)
			{
				GGETRACELOG("Animation loaded without rig!");
			}
			else
			{
				GGETRACELOG("Warning: no rig should be loaded!");
			}*/

			// store the animation, binding, and animated skeleton instance in our hashmaps
			m_hkAnimations.insert(TStrAnimPair(line, ac->m_animations[0]));
			m_hkBindings.insert(TStrBindPair(line, ac->m_bindings[0]));

			// set reference pose weight on the animated skeleton instance
			m_hkSkeletonInstance->setReferencePoseWeightThreshold(0.5f);
		}
	}

	// get the ogre skeleton from the GraphicsObject's Ogre::Entity
	m_ogSkeleton = m_pGraphicsObject->m_pOgreEntity->getSkeleton();

	// initialize the scale to identity
	m_fScaleFactor = 1.0;
}

void AnimatedHavokEntity::Update(float f_fTimeStep)
{
    if (!m_hkSkeletonInstance)
        return;

#ifdef GGE_PROFILER2
	float starttime;
	starttime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
	GGE_Profiler2Ptr->SetAnimationStartTime(starttime);
#endif

	// get the current pose of the skeleton at this frame
	hkaAnimatedSkeleton* hkAnimatedSkeleton = new hkaAnimatedSkeleton(m_hkSkeleton);
	hkaPose pose(hkAnimatedSkeleton->getSkeleton());
// if there is a ragdoll and ragdoll is triggered off or ragdoll is triggered off
	// so if ragdoll isn't enabled
	if ((ragDollAttachedToken && !ragDollPoseToken) || !ragDollAttachedToken) 
	{
		std::string f_sActiveAnim = _strdup(m_sActiveAnim);
		int startInx = 0;
		int endInx = 0;
		// if Active animation has an _OB_ suffix, get rid of it....
		if (f_sActiveAnim.find("_OB_") != std::string::npos) // for when there's something to do with Animation Overlay
		{
			endInx = f_sActiveAnim.find("_OB_");
			f_sActiveAnim = _strdup(f_sActiveAnim.substr(startInx, endInx).c_str());
		}
		// play default animation
		// transition false (no transition animation passed) and there is no active animation and there is a default (default animation is not
		// none", play animation
		if (!transition && strcmp(f_sActiveAnim.c_str(), "None") == 0 && strcmp(m_sDefaultAnim, "None") != 0)
		{
			PlayAnimation(m_sDefaultAnim, "", true, m_mRampDownValues[m_sDefaultAnim]);
		}


		// play transition destination
		// if transistion is true  (there is a transition animation passed) and (there is no active animation) or m_hkControls[active aniamtin] exists & had a playback speed that is less then eclipse
		if (transition && (strcmp(f_sActiveAnim.c_str(), "None") == 0 || (m_hkControls.count(f_sActiveAnim) && m_hkControls[f_sActiveAnim]->getPlaybackSpeed() < eclipse)))
		{
			// if there is no overlay
			if (!overlay)
			{
				PlayAnimation(transitionTo, "", transitionLoop, m_mRampDownValues[transitionTo]);
			}
			// if overlay
			else
			{
				PlayAnimationOverlay(transitionTo, "", transitionLoop, m_mRampDownValues[transitionTo]);
			}
			transition = false;
			transitionTo = "";
		}

		// if an animation is not playing, remove its control from both skeleton instance and hash map
		TStrCtrlIt i = m_hkControls.begin(); //first(keys) are the animation names, seconde(values) are the ocntrols
		TStrCtrlIt end = m_hkControls.end();
		std::vector<std::string> f_sInactiveAnim;
		while ( i != end )
		{
			if (i->second == NULL) // if there is no control push name onto inactive animations
			{
				f_sInactiveAnim.push_back(i->first);
			}
			else
			{
				// if the playback speed is less then ellipse and the animatin control is eased in, ease this animation out...?
				if (i->second->getPlaybackSpeed() < eclipse &&
					i->second->getEaseStatus() == hkaDefaultAnimationControl::EASED_IN)
				{
					//i->second->setEaseInCurve(0, 0, 1, 1);
					//i->second->setEaseOutCurve(1, 1, 0, 0);
					//i->second->setMasterWeight(0.0f);
					//i->second->setPlaybackSpeed( 1.0f );
					i->second->easeOut(m_mRampDownValues[i->first]);
				}
				// if the ease status of the animatin control is eased out
				if (i->second->getEaseStatus() == hkaDefaultAnimationControl::EASED_OUT)
				{
					// set active animation to none
					if (strcmp(f_sActiveAnim.c_str(), i->first.c_str()) == 0)
					{
						m_sActiveAnim = "None"; // sets active animation to none?
					}
					f_sInactiveAnim.push_back(i->first);
				}
			}
			++i;
		}

		// erase inactive controls from hash map
		std::vector<std::string>::iterator j = f_sInactiveAnim.begin();
		std::vector<std::string>::iterator endj = f_sInactiveAnim.end();
		while (j != endj)
		{
			m_hkSkeletonInstance->removeAnimationControl(m_hkControls[j->data()]);
			m_hkControls.erase(j->data());
			j++;
		}
		f_sInactiveAnim.clear();
	}

	// if ragdoll is enabled
	if(ragDollAttachedToken)
	{
		hkaPose ragdollPose(ragDollEntity->m_ragdollSkeleton);
		//hkaPose upsampledPose( hkAnimatedSkeleton->getSkeleton() );

		ragdollPose.setToReferencePose();

		if(ragDollPoseToken)
		{
			ragDollEntity->m_ragdoll->getPoseWorldSpace(ragdollPose.accessUnsyncedPoseModelSpace().begin());
			ragdollPose.syncAll();
			ragDollEntity->m_animationFromRagdoll->mapPose(ragdollPose.getSyncedPoseModelSpace().begin(), m_hkSkeletonInstance->getSkeleton()->m_referencePose.begin(), pose.accessSyncedPoseModelSpace().begin(), hkaSkeletonMapper::CURRENT_POSE);
		}
		else
		{
			// drive using havok animations
			m_hkSkeletonInstance->sampleAndCombineAnimations(pose.accessUnsyncedPoseLocalSpace().begin(), pose.getFloatSlotValues().begin());

			hkQsTransform rootTransform = pose.getSyncedPoseLocalSpace()[0];
			rootTransform.setMul(ragDollEntity->m_worldFromModel, rootTransform);
			pose.accessSyncedPoseLocalSpace()[0] = rootTransform;

			//now here the rigidbodies are not controlled by us
			//if we do not do anything on ragdollEntity we will have switched to ragdoll
			//which means bones are controlled by havok according to rigidbody properties
			//ragDollEntity->m_ragdollFromAnimation->mapPose(pose, ragdollPose);
			ragDollEntity->m_ragdollFromAnimation->mapPose( pose.getSyncedPoseModelSpace().begin(), ragDollEntity->m_ragdollSkeleton->m_referencePose.begin(), ragdollPose.accessUnsyncedPoseModelSpace().begin(), hkaSkeletonMapper::CURRENT_POSE );
			ragDollEntity->m_ragdoll->setPoseModelSpace( ragdollPose.getSyncedPoseModelSpace().begin(),hkQsTransform::IDENTITY);

			//ragDollEntity->m_ragdollRigidBodyController->driveToPose( f_fTimeStep, ragdollPose.accessSyncedPoseModelSpace().begin(), *m_worldFromModel, HK_NULL );
		}
	}
	else
	{
		m_hkSkeletonInstance->sampleAndCombineAnimations(pose.accessUnsyncedPoseLocalSpace().begin(), pose.getFloatSlotValues().begin());

		// get the root transform of the pose
		hkQsTransform rootTransform = pose.getSyncedPoseLocalSpace()[0];
		rootTransform.setMul(*m_worldFromModel, rootTransform);
		pose.accessSyncedPoseLocalSpace()[0] = rootTransform;
	}

	const hkArray<hkQsTransform>& localSpaceTransformArray = pose.getSyncedPoseLocalSpace();

	const hkQsTransform& transRoot = localSpaceTransformArray[0];

	const hkQuadReal& pos = transRoot.m_translation.getQuad();
	const hkQuadReal& quat = transRoot.m_rotation.m_vec.getQuad();

	Ogre::Quaternion q;
	q.x = quat.v[0];q.y = quat.v[1];q.z = quat.v[2];q.w = quat.v[3];

#ifdef TBB_PARALLEL_OPTION
	m_pGraphicsObject->setOrientation(q);
	SkeletonUpdateRootTask* newTask = new SkeletonUpdateRootTask(this,q);
	TaskContainer* container = new TaskContainer((void*)newTask,Skeleton_Update_Root_Task);
	EnginePtr->taskQueue.push(container);
	EnginePtr->taskQueueSize++;
#else

	m_pGraphicsObject->m_pOgreSceneNode->setPosition(pos.v[0],pos.v[1],pos.v[2]);
	//m_pGraphicsObject->m_pOgreSceneNode->setOrientation(q);

	m_ogSkeleton->getRootBone()->setPosition(pos.v[0], pos.v[1], pos.v[2]);
	m_ogSkeleton->getRootBone()->setOrientation(q);
	//m_ogSkeleton->getRootBone()->setPosition(Ogre::Vector3::ZERO);
	//m_ogSkeleton->getRootBone()->setOrientation(Ogre::Quaternion::IDENTITY);
	m_ogSkeleton->getRootBone()->setScale(Ogre::Vector3::UNIT_SCALE);

#endif

	// map Havok rig to Ogre rig
	// by iterating through all bones in the Havok skeleton,
	// finding the corresponding bone in the Ogre skeleton,
	// and setting the orientation of the Ogre bone to
	// match the orientation of the Havok bone
	for (int i = 1; i < m_hkSkeleton->m_bones.getSize(); ++i)
	{
		hkaBone hkBone = m_hkSkeleton->m_bones[i];

		// get the name of the Havok bone
		Ogre::String boneName(hkBone.m_name);

		// initialize the Ogre bone
		Ogre::Bone* ogreBone;

		// get the corresponding bone in the Ogre skeleton
		try
		{
			ogreBone = m_ogSkeleton->getBone(boneName);
		}
		catch (...)
		{
			continue;
		}

		// update the Ogre bone's position
		const hkQsTransform& trans = localSpaceTransformArray[i];
		const hkQuadReal& pos = trans.m_translation.getQuad();
		Ogre::Vector3 vectorPos(pos.v[0]*m_fScaleFactor,pos.v[1]*m_fScaleFactor,pos.v[2]*m_fScaleFactor);

#ifdef TBB_PARALLEL_OPTION
		SkeletonUpdateBoneTask* newTask2 = new SkeletonUpdateBoneTask(ogreBone,vectorPos);
		container = new TaskContainer((void*)newTask2,Skeleton_Update_Bone_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
#else
		ogreBone->setPosition(vectorPos);
#endif

		// update the Ogre bone's orientation
		const hkQuadReal& quat = trans.m_rotation.m_vec.getQuad();
		Ogre::Quaternion q;
		q.x = quat.v[0];q.y = quat.v[1];q.z = quat.v[2];q.w = quat.v[3];

#ifdef TBB_PARALLEL_OPTION
		newTask2 = new SkeletonUpdateBoneTask(ogreBone,q);
		container = new TaskContainer((void*)newTask2,Skeleton_Update_Bone_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
#else
		ogreBone->setOrientation(q);
#endif
	}

	m_hkSkeletonInstance->stepDeltaTime(f_fTimeStep);

#ifdef GGE_PROFILER2
	float endtime;
	endtime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
	GGE_Profiler2Ptr->SetAnimationEndTime(endtime);
#endif
}

void AnimatedHavokEntity::PlayAnimation(const char* f_sDestinationAnimationName, const char* f_sTransitionAnimationName, const bool f_bLoop, const float f_fBlendDuration)
{
	// Don't transition into an animation that's already playing!
	if (strcmp(m_sActiveAnim, f_sDestinationAnimationName) == 0 && (strcmp(f_sTransitionAnimationName, "") == 0 || f_sTransitionAnimationName == NULL || strcmp(f_sTransitionAnimationName, "None") == 0)) {
		return;
	} 
	if (strcmp(m_sActiveAnim, f_sTransitionAnimationName) == 0) {
		return;
	}

	// if h_hkAnimations has no animation value at f_sDestinationAnimationName key
	// if this aimation doesn't exist for this entity
	if (!m_hkAnimations.count(f_sDestinationAnimationName))
	{
		return;
	}
	// if m_hkAnimation has no animation of name TransistionAnimationName
	// if this animation doesn't exist for this entity
	if (!m_hkAnimations.count(f_sTransitionAnimationName))
	{
		f_sTransitionAnimationName = "";
	}
	overlay = false;

	// if the destination animation is already the playing animation

	// stop all the other animations
	// If I get rid of this then i don't have the t-pos in the middle which is weird becasue used straight elsewhere stopAnimation stops all but the default
	// and it's bascially the same as overlay
	//if (f_fBlendDuration == 0.0)
	/*StopAnimation(); */

	// check if we have already stored a rampDown value for this animation
	if (m_mRampDownValues.count(f_sDestinationAnimationName))
	{
		m_mRampDownValues[f_sDestinationAnimationName] = f_fBlendDuration/2; // if we already had a value, overwrite it
	}
	else // otherwise, insert a new pair for this rampDown value
	{
		m_mRampDownValues.insert(TStrRampPair(_strdup(f_sDestinationAnimationName), f_fBlendDuration/2));
	}
	// this animation does exist for this entity
	if (m_hkAnimations.count(m_sActiveAnim))
	{
		// if we already had a value, overwrite it
		if (m_mRampDownValues.count(m_sActiveAnim))
		{
			m_mRampDownValues[m_sActiveAnim] = f_fBlendDuration/2; // if we already had a value, overwrite it
		}
		else // otherwise, insert a new pair for this rampDown value
		{
			m_mRampDownValues.insert(TStrRampPair(_strdup(m_sActiveAnim), f_fBlendDuration/2));
		}
	}
	StopAnimation(m_sActiveAnim);
	// get control for current/active animation
	hkaDefaultAnimationControl* original_control;
	//TStrCtrlIt i = m_hkControls.begin();
	//TStrCtrlIt end = m_hkControls.end();
	//while (i != end)
	//{
	//	// if the control is not null and (the animationname passed is null or "" or the name is same as the key
	//	// this is the current playing animation and will be stopped
	//	if ( i->second != NULL && strcmp(i->first.c_str(), m_sActiveAnim) == 0)
	//	{
	//		original_control = i->second;
	//	}
	//	++i;
	//}



	// MaxCycles
	int f_iMaxCycles = -1; // INFINATE CYCLES
	// create an animation control
	
	const char* f_sCurrentAnimation = _strdup(m_sActiveAnim);
	original_control = new hkaDefaultAnimationControl( m_hkBindings[m_sActiveAnim], false, f_iMaxCycles);
	hkaDefaultAnimationControl* control;
	// No transistional animation passed 
	if (f_sTransitionAnimationName == NULL || strcmp(f_sTransitionAnimationName, "") == 0)
	{
		// set the current active animation name to destination animation
		m_sActiveAnim = _strdup(f_sDestinationAnimationName);
		// don't loop, so only one cycle
		if (!f_bLoop)
		{
			f_iMaxCycles = 1;
		}
		
		control = new hkaDefaultAnimationControl ( m_hkBindings[f_sDestinationAnimationName], false, f_iMaxCycles );

	}
	// transitional animation passed
	else
	{
		// set the current active animation name
		m_sActiveAnim = _strdup(f_sTransitionAnimationName);
		control = new hkaDefaultAnimationControl (m_hkBindings[f_sTransitionAnimationName], false, 1);
		transition = true;
		transitionLoop = f_bLoop;
		transitionTo = _strdup(f_sDestinationAnimationName);

		// check if we have already stored a rampDown value for this animation
		if (m_mRampDownValues.count(f_sTransitionAnimationName))
		{
			m_mRampDownValues[f_sTransitionAnimationName] = f_fBlendDuration/2; // if we already had a value, overwrite it
		}
		else // otherwise, insert a new pair for this rampDown value
		{
			m_mRampDownValues.insert(TStrRampPair(_strdup(f_sTransitionAnimationName), f_fBlendDuration/2));
		}
	}
	//m_hkControls.insert(TStrCtrlPair(f_sCurrentAnimation, original_control));
	m_hkControls.insert(TStrCtrlPair(m_sActiveAnim, control));
	//original_control = m_hkControls.find(f_sCurrentAnimation)->second;
	// get currently playing (how?) animation's control to blend out
	//original_control = (hkaDefaultAnimationControl*) m_hkSkeletonInstance->getAnimationControl(m_hkSkeletonInstance->getNumAnimationControls() - 1);
	//original_control = new hkaDefaultAnimationControl (m_hkBindings[f_sCurrentAnimation], false, f_iMaxCycles);
	// Bind the control to the skeleton
	m_hkSkeletonInstance->addAnimationControl(original_control);
	m_hkSkeletonInstance->addAnimationControl(control);

	original_control->setPlaybackSpeed(eclipse);
	control->setPlaybackSpeed( 1.0f );

	original_control->setEaseInCurve(0, 0, 1, 1);
	original_control->setEaseOutCurve(1, 1, 0, 0);
	// Set ease curves explicitly
	control->setEaseInCurve(0, 0, 1, 1);	// Smooth
	control->setEaseOutCurve(1, 1, 0, 0);	// Smooth

	original_control->removeReference();
	// the animated skeleton now owns the control
	control->removeReference();

	//original_control->setMasterWeight(0.0f);
	// Set control weight of the skeleton instance
	control->setMasterWeight(1.0f); 
	// ease in the animation controller for the specified animation
	original_control->easeOut(f_fBlendDuration/2);
	control->easeIn(f_fBlendDuration/2);

	return;
}

void AnimatedHavokEntity::PlayAnimationOverlay(	const char* f_sOverlayAnimationName, const char* f_sTransitionAnimationName, const bool	f_bLoop, const float f_fBlendDuration)
{
	//// Don't overlay an animation withitself!
	if (strcmp(m_sActiveAnim, f_sOverlayAnimationName) == 0) {
		return;
	}
	if (strcmp(m_sActiveAnim, f_sTransitionAnimationName) == 0) {
		return;
	}
	//if (strcmp(f_sTransitionAnimationName, "") != 0 && strcmp(transitionTo, f_sOverlayAnimationName) == 0) {
	//	//transition = false;
	//	return;
	//}
	// This animation is already overlayed! Don't reoverlay.
	std::string f_sActiveAnim;
	f_sActiveAnim = _strdup(m_sActiveAnim);
	if (f_sActiveAnim.find("_OB_") != std::string::npos ) {
		if (f_sActiveAnim.find(f_sOverlayAnimationName) != std::string::npos) {
			return;
		}
		int start = 0;
		int end = f_sActiveAnim.find("_OB_") - 1;
		std::string temp = _strdup(f_sActiveAnim.substr(start, end).c_str());
		if (strcmp(temp.c_str(), f_sOverlayAnimationName) == 0) {
			return;
		}
		//if (f_sActiveAnim.find(f_sTransitionAnimationName) != std::string::npos && (strcmp(f_sTransitionAnimationName, "") != 0 || f_sTransitionAnimationName != NULL || strcmp(f_sTransitionAnimationName, "None") != 0)) {
		//	return;
		//}
	}
	//if (f_sActiveAnim.find(f_sOverlayAnimationName) != std::string::npos && (strcmp(f_sTransitionAnimationName, "") == 0 || f_sTransitionAnimationName == NULL || strcmp(f_sTransitionAnimationName, "None") == 0)) {
	//	return;
	//} 
	//if (f_sActiveAnim.find(f_sTransitionAnimationName) != std::string::npos && (strcmp(f_sTransitionAnimationName, "") != 0 || f_sTransitionAnimationName != NULL || strcmp(f_sTransitionAnimationName, "None") != 0)) {
	//	return;
	//}


	// This animation doesn't exist for this entity
	if (!m_hkAnimations.count(f_sOverlayAnimationName))
	{
		return;
	}
	if (!m_hkAnimations.count(f_sTransitionAnimationName))
	{
		f_sTransitionAnimationName = "";
	}
	// there is no active animation to overlay with so just play regular
	if (strcmp(m_sActiveAnim, "None") == 0)
	{
		PlayAnimation(f_sOverlayAnimationName, f_sTransitionAnimationName, f_bLoop, f_fBlendDuration);
		return;
	}
	overlay = true; // overlay flag for update

	// set the current active animation names
	// animations to be overlayed
	char* f_sAnimationName1;
	char* f_sAnimationName2;

	// MaxCycles
	int f_iMaxCycles = -1;

	// check if we have already stored a rampDown value for this animation
	if (m_mRampDownValues.count(f_sOverlayAnimationName))
	{
		m_mRampDownValues[f_sOverlayAnimationName] = f_fBlendDuration/2; // if we already had a value, overwrite it
	}
	else // otherwise, insert a new pair for this rampDown value
	{
		m_mRampDownValues.insert(TStrRampPair(_strdup(f_sOverlayAnimationName), f_fBlendDuration/2));
	}

	// create an animation control
	hkaDefaultAnimationControl* control;
	//std::string f_sActiveAnim;
	int start=0, end=0;
	// If no transition animation passed
	if (f_sTransitionAnimationName == NULL || strcmp(f_sTransitionAnimationName, "") == 0)
	{
		// set the animation to be overlayed name
		f_sAnimationName1 = _strdup(f_sOverlayAnimationName);
		// current animation name
		f_sActiveAnim = _strdup(m_sActiveAnim);
		if (f_sActiveAnim.find("_OB_") != std::string::npos)
		{
			start = f_sActiveAnim.find("_OB_") + 4;
			end = f_sActiveAnim.size()-1;
			f_sActiveAnim = _strdup(f_sActiveAnim.substr(start, end).c_str());
			//// Don't overlay with yourself!
			//if (strcmp(f_sActiveAnim.c_str(), f_sOverlayAnimationName)) {
			//	return;
			//}
		}
		// set current animation name
		if (m_hkAnimations.count(f_sActiveAnim))
		{
			f_sAnimationName2 = _strdup(f_sActiveAnim.c_str());
		}
		else
		{
			// If no active animation and no default animation play regular
			if (strcmp(m_sDefaultAnim, "None") == 0)
			{
				PlayAnimation(f_sOverlayAnimationName, f_sTransitionAnimationName, f_bLoop, f_fBlendDuration);
				return;
			}
			f_sAnimationName2 = _strdup(m_sDefaultAnim);
		}
		// set name for combined animation

		strcat_s(f_sAnimationName1, strlen(f_sAnimationName1)+6, "_OB_");
		strcat_s(f_sAnimationName1, strlen(f_sAnimationName1)+strlen(f_sAnimationName2) + 1, f_sAnimationName2);
		m_sActiveAnim = _strdup(f_sAnimationName1);
		f_sAnimationName1 = _strdup(f_sOverlayAnimationName);
		// no loop if false
		if (!f_bLoop)
		{
			f_iMaxCycles = 1;
		}
		// new control for combined animation name
		control = new hkaDefaultAnimationControl ( m_hkBindings[f_sAnimationName1], false, f_iMaxCycles );
	}
	// Transition animation passed
	else
	{
		// set first animation to be overlayed (transitory)
		f_sAnimationName1 = _strdup(f_sTransitionAnimationName);
		f_sActiveAnim = _strdup(m_sActiveAnim);
		if (f_sActiveAnim.find("_OB_") != std::string::npos)
		{
			start = f_sActiveAnim.find("_OB_") + 4;
			end = f_sActiveAnim.size()-1;
			f_sActiveAnim = _strdup(f_sActiveAnim.substr(start, end).c_str());
		}
		if (m_hkAnimations.count(f_sActiveAnim))
		{
			f_sAnimationName2 = _strdup(f_sActiveAnim.c_str());
		}
		else
		{
			// if no active or default animation to be overlayed, call regular animation
			if (strcmp(m_sDefaultAnim, "None") == 0)
			{
				PlayAnimation(f_sOverlayAnimationName, f_sTransitionAnimationName, f_bLoop, f_fBlendDuration);
				return;
			}
			f_sAnimationName2 = _strdup(m_sDefaultAnim);
		}
		strcat_s(f_sAnimationName1, strlen(f_sAnimationName1) + strlen("_OB_") + 1, "_OB_");
		strcat_s(f_sAnimationName1, strlen(f_sAnimationName1) + strlen(f_sAnimationName2) + 1, f_sAnimationName2);
		m_sActiveAnim = _strdup(f_sAnimationName1);
		f_sAnimationName1 = _strdup(f_sTransitionAnimationName);
		control = new hkaDefaultAnimationControl (m_hkBindings[f_sAnimationName1], false, 1);
		transition = true;
		transitionLoop = f_bLoop;
		transitionTo = _strdup(f_sOverlayAnimationName); // store overlay animation for call in update
		// check if we have already stored a rampDown value for this animation
		if (m_mRampDownValues.count(f_sTransitionAnimationName))
		{
			m_mRampDownValues[f_sTransitionAnimationName] = f_fBlendDuration/2; // if we already had a value, overwrite it
		}
		else // otherwise, insert a new pair for this rampDown value
		{
			m_mRampDownValues.insert(TStrRampPair(_strdup(f_sTransitionAnimationName), f_fBlendDuration/2));
		}
	}

	if (m_hkControls.count(f_sAnimationName1))
	{
		m_hkControls.erase(f_sAnimationName1);
	}
	m_hkControls.insert(TStrCtrlPair(f_sAnimationName1, control));

	// Bind the control to the skeleton
	m_hkSkeletonInstance->addAnimationControl(control);

	// Set the animation weights
	//float anim1Weight = hkMath::min2(beta, 1.0f);
	//anim1Weight = hkMath::max2(anim1Weight, 0.0f); // Bounding
	float anim1Weight = 0.5f;
	float anim2Weight = 1-anim1Weight;
	m_hkControls[f_sAnimationName1]->setMasterWeight(anim1Weight);
	m_hkControls[f_sAnimationName2]->setMasterWeight(anim2Weight);

	// Sync playback speeds
	const float anim1Period = m_hkControls[f_sAnimationName1]->getAnimationBinding()->m_animation->m_duration;
	const float anim2Period = m_hkControls[f_sAnimationName2]->getAnimationBinding()->m_animation->m_duration;

	const float totalWeight = anim1Weight + anim2Weight + 1e-6f;
	const float anim1Portion = anim1Weight / totalWeight;
	const float anim2Portion = anim2Weight / totalWeight;
	const float anim12Ratio = anim1Period / anim2Period;
	const float anim21Ratio = anim2Period / anim1Period;

	m_hkControls[f_sAnimationName1]->setPlaybackSpeed( (1-anim1Portion) * anim12Ratio + anim1Portion );
	m_hkControls[f_sAnimationName2]->setPlaybackSpeed( (1-anim2Portion) * anim21Ratio + anim2Portion );

	m_hkControls[f_sAnimationName1]->easeIn(f_fBlendDuration/2);
	m_hkControls[f_sAnimationName2]->easeIn(f_fBlendDuration/2);

	f_sActiveAnim.clear();
}

bool AnimatedHavokEntity::IsAnimationPlaying(const char* f_sAnimationName)
{
	if (m_hkControls[f_sAnimationName] != NULL &&
		m_hkBindings[f_sAnimationName]->m_animation->m_duration > m_hkControls[f_sAnimationName]->getLocalTime() ||
		(m_hkControls[f_sAnimationName]->getPlaybackSpeed() > eclipse &&
		m_hkControls[f_sAnimationName]->getEaseStatus() == hkaDefaultAnimationControl::EASED_IN))
	{
		return true;
	}
	else
	{
		return false;
	}
}

// This stops all currently playing animation,
void AnimatedHavokEntity::StopAnimation(const char* f_sAnimationName) // if animationname is "" this should stop all currently playing animations
{
	//const char* f_sActiveAnim;
	//if (f_sAnimationName == NULL || strcmp("", f_sAnimationName) == 0) {
	//	f_sAnimationName = GetActiveAnimation();
	//}

	TStrCtrlIt i = m_hkControls.begin();
	TStrCtrlIt end = m_hkControls.end();
	while (i != end)
	{
		// if the control is not null and (the animationname passed is null or "" or the name is same as the key
		// this is the current playing animation and will be stopped
		if ( i->second != NULL &&
			(f_sAnimationName == NULL || strcmp("", f_sAnimationName) == 0 || strcmp(i->first.c_str(), f_sAnimationName) == 0))
		{
			// set the current active animation name
			m_sActiveAnim = _strdup("None");
			transitionTo = _strdup("None");
			//i->second->setMasterWeight(0.0f);
			//i->second->setPlaybackSpeed(eclipse);
			i->second->setEaseInCurve(0, 0, 1, 1);
			i->second->setEaseOutCurve(1, 1, 0, 0);
			i->second->setMasterWeight(0.0f);
			i->second->setPlaybackSpeed( 1.0f );
			i->second->easeOut(m_mRampDownValues[i->first]);
		}
		++i;
	}
}

// This will stop all animation, returns all to T pos
void AnimatedHavokEntity::StopAllAnimation() {
	m_sActiveAnim = _strdup("None");
	transitionTo = _strdup("None");
	m_sDefaultAnim = _strdup("None");
	transition = false;
	
	// NULL out all the controls
	TStrCtrlIt i = m_hkControls.begin();
	TStrCtrlIt end = m_hkControls.end();
	while (i != end)
	{
			if (i->second != NULL) {
				i->second->setMasterWeight(0.0f);
				//i->second->setPlaybackSpeed(eclipse);
				i->second->easeOut(m_mRampDownValues[i->first]);
			}
		++i;
	}

}

const char* AnimatedHavokEntity::GetName()
{
	return m_sName;
}

const char* AnimatedHavokEntity::GetActiveAnimation()
{
	return m_sActiveAnim;
}

void AnimatedHavokEntity::SetDefaultAnimation(const char* f_sDefaultAnim)
{
	m_sDefaultAnim = _strdup(f_sDefaultAnim);
	// check if we have already stored a rampDown value for this animation
	if (m_mRampDownValues.count(f_sDefaultAnim))
	{
		m_mRampDownValues[f_sDefaultAnim] = defaultBlendDuration; // if we already had a value, overwrite it
	}
	else // otherwise, insert a new pair for this rampDown value
	{
		m_mRampDownValues.insert(TStrRampPair(_strdup(f_sDefaultAnim), defaultBlendDuration));
	}
}

const char* AnimatedHavokEntity::GetDefaultAnimation()
{
	return m_sDefaultAnim;
}

std::vector<std::string> AnimatedHavokEntity::GetAnimationNames()
{
	return m_aAnimationNames;
}

void AnimatedHavokEntity::ToggleToRagDollPose(bool token)
{
	if(ragDollAttachedToken)
	{
		ragDollPoseToken=token;
	}
}

void AnimatedHavokEntity::SetRagdollPosition(hkVector4& f_v4hkPos)
{
	if(ragDollAttachedToken)
	{
		/*for ( int i = 0; i < ragDollEntity->m_ragdoll->m_rigidBodies.getSize(); i++ )
		{
			hkpRigidBody* rb = ragDollEntity->m_ragdoll->m_rigidBodies[i];
			rb->setPosition(f_v4hkPos);
		}*/
		ragDollEntity->m_worldFromModel.setTranslation(f_v4hkPos);
	}
}

void AnimatedHavokEntity::SetPosition(float x, float y, float z)
{
	m_xInitPos = x;
	m_yInitPos = y;
	m_zInitPos = z;
	m_worldFromModel->setTranslation(hkVector4(x,y,z));
	SetRagdollPosition(hkVector4(x,y,x));
}

void AnimatedHavokEntity::AddRagDoll(const std::string f_sRagdollPath)
{
	std::string afolder = f_sRagdollPath;
	int end = afolder.find(".hkx");
	afolder = afolder.substr(0, end);
	afolder = afolder.substr(8); 
	std::string f_sHavokFullLink="";
	f_sHavokFullLink.append("..\\..\\");
	f_sHavokFullLink.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
	f_sHavokFullLink.append("\\Media\\Havok\\");
	f_sHavokFullLink.append(afolder);
	f_sHavokFullLink.append("\\");
	f_sHavokFullLink.append(f_sRagdollPath);

	ragDollEntity = new RagdollObject(f_sHavokFullLink.c_str(), 1.0f);

	ragDollAttachedToken = true;
	ragDollPoseToken = false;

	// Set the initial pose of the character
	{
		hkaPose animationPose( m_hkSkeleton );
		hkaPose ragdollPose( ragDollEntity->m_ragdollSkeleton );

		//// Sample the character's current animated pose
		m_hkSkeletonInstance->sampleAndCombineAnimations( animationPose.accessUnsyncedPoseLocalSpace().begin(), HK_NULL );

		//// Convert the animation pose to a ragdoll pose
		ragDollEntity->m_ragdollFromAnimation->mapPose( animationPose.getSyncedPoseModelSpace().begin(), ragDollEntity->m_ragdollSkeleton->m_referencePose.begin(), ragdollPose.accessUnsyncedPoseModelSpace().begin(), hkaSkeletonMapper::CURRENT_POSE );

		//// Set the pose of the ragdoll
		ragDollEntity->m_ragdoll->setPoseModelSpace( ragdollPose.getSyncedPoseModelSpace().begin(), *m_worldFromModel );
	}

	// Set the initial pose of the character
	{
		hkaPose animationPose( m_hkSkeleton );
		hkaPose ragdollPose( ragDollEntity->m_ragdollSkeleton );

		//// Sample the character's current animated pose
		m_hkSkeletonInstance->sampleAndCombineAnimations( animationPose.accessUnsyncedPoseLocalSpace().begin(), HK_NULL );

		//// Convert the animation pose to a ragdoll pose
		ragDollEntity->m_ragdollFromAnimation->mapPose( animationPose.getSyncedPoseModelSpace().begin(), ragDollEntity->m_ragdollSkeleton->m_referencePose.begin(), ragdollPose.accessUnsyncedPoseModelSpace().begin(), hkaSkeletonMapper::CURRENT_POSE );

		//// Set the pose of the ragdoll
		//ragDollEntity->m_ragdoll->setPoseModelSpace( ragdollPose.getSyncedPoseModelSpace().begin(), m_worldFromModel );
	}
	ragDollEntity->m_worldFromModel.setIdentity();
}