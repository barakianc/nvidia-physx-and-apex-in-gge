#ifdef GLE_EDITOR
#pragma once

/*! Basic engine includes */
//#include "Base.h"
//#include "Ogre.h"
#include "Input.h"
#include "GameScreen.h"
#include "Utilities.h"
#include "DotScenePlusLoader.h"
#include <OgreCollada.h>
#include "GameObject.h"

/*! Editor Includes */
#include "Editor_Packets.h" 
// cannot move this include to the .cpp file because other 
// files use the packet infomation - including Engine.h
// Changing other parts of the system to call specific functions
// as opposed to using the generic command feature would remove
// the need for its inclusion.

/*! Other useful includes */
#include <string>
#include <vector>
#include <iostream>
#include <sstream>

//#define GLE_DEBUG_INPUT
//#define GLE_DEBUG_NETWORK
#define GLE_MODEL_EDITOR_OBJECT_NAME "ModelViewerObject"
class GameObject;


namespace GamePipe
{
	class IMessageHandle;
	/*! Defines a class used to send properties */
	class GLE_PropTuple
	{
	public:
		/*! Property */
		int property;
		/*! Value */
        std::string value;

		/*! Sets the values for a tuple
		*   @param[in]	prop Property
		*	@param[in]  val Value
		*/
		GLE_PropTuple(int prop, std::string val)
		{ property = prop; value = val; }

		/*! Sets the values for a tuple
		*   @param[in]	prop Property
		*	@param[in]  val Value
		*/
		GLE_PropTuple(int prop, float val)
		{ property = prop; value = Float2Str(val); }

		GLE_PropTuple(int prop, bool val)
		{ property = prop; value = Bool2Str(val); }

		/*! Convert float to string
		*   @param[in]	f	Value to convert to string
		*	@return		String with the float
		*/
		std::string Float2Str(float f)
		{
			Ogre::stringstream ss; 
			ss << std::fixed << std::setprecision(6) << f;
			return ss.str();
		}

		//convert bool to string
		std::string Bool2Str(bool b)
		{
			if(b == true)
				return "true";
			else
				return "false";
		} 
	};

	/*! Defines the main class in charge of the communication between GGE and GGE Live Editor */
	class Engine;
	class Editor 
	{
	private: 
		std::vector<IMessageHandle*>	m_vMessageHandles;
		void InitMessageHandels();

	public:
		Editor();
		~Editor();

		/*! Initialize the editor. Collect basic information about the engine and the screens.
		*	@param[in]	e				Pointer to the engine being used
		*	@param[in]	pRenderer		Pointer to the renderer
		*	@param[in]	pVecGameScreens	Pointer to the screens of the game
		*/
		void Init( Engine* e, Ogre::Root* pRenderer, std::vector<GameScreen*> * pVecGameScreens )
		{ m_pEngine = e; m_pRenderer = pRenderer; m_pVecGameScreens = pVecGameScreens; }

		/*! Possible states for the editor */
		enum  eEditorConnectionStatus {				
			GGE_DISCONNECTED, 
			GGE_READY_TO_ACCEPT, 
			GGE_CONNECTED,
			GGE_ERROR
		};

		//Original as private change required due to new system
	public:
		
		/*! Socket to receive information from the client */
		SOCKET					m_sockMessages;			
		/*! Socket used to receive the client */
		SOCKET					m_sockListen;		
		/*! Used to go to Game mode and go back to Editor mode, if true the controls and loops are controlled by the editor */
		bool					m_bIsEditorMode;
		/*! Used to switch the control inputs between GLE and keyboard*/
		bool					m_bUseEditorInput;
		/*! Status of the application */
		eEditorConnectionStatus	m_eEditorConnectionStatus;
		/*! Save "left overs" from other packets, this is useful when receiving packets that are splitted in more than one TCP packet*/
		Ogre::String			m_msgLeftOvers;

		/*! Ogre renderer */
		Ogre::Root*					m_pRenderer;
		/*! Engine GameScreens */
		std::vector<GameScreen*>*	m_pVecGameScreens;
		/*! Engine */
		Engine*						m_pEngine;

		/*! Active scene node, I am using this to know which scene node has the bounding box */
		Ogre::SceneNode*		m_pSceneNodeSelected;
		
		/*! INPUT */
		GIS::MouseState			ed_MouseState;
		bool					ed_KeyStateBuffer[250];
		int						ed_KeyCode;
		bool					ed_KeyPress;

		/*! Active animation, this is useful to update the animations */
		Ogre::AnimationStateSet* pMyAnimationSet;
		Ogre::Entity*			m_pMVEntity;

		GameObject*				m_pMVGameObject;
		std::string	m_CurrentAnimationName;
		std::string	m_sMVCurrentGameObjectName;
		

		// Objects needed for the dynamic editing tool in general
		Ogre::SceneNode*		m_DynamicToolXNode;
		Ogre::SceneNode*		m_DynamicToolYNode;
		Ogre::SceneNode*		m_DynamicToolZNode;
		Ogre::SceneNode*		m_FreeDynamicToolNode;
		
		bool					b_DynamicToolXMarkerSelected;
		bool					b_DynamicToolYMarkerSelected;
		bool					b_DynamicToolZMarkerSelected;
		bool					b_FreeDynamicToolMarkerSelected;

		bool					b_DynamicToolActive;

		// Data needed to allow for dynamic movement of objects/sceneNodes
		Ogre::ManualObject*		m_DynamicConeXMarker;
		Ogre::ManualObject*		m_DynamicConeYMarker;
		Ogre::ManualObject*		m_DynamicConeZMarker;
		Ogre::ManualObject*		m_FreeDynamicCubeMarker;
		bool					b_DynamicPositioningMode;

		// Data needed for dynamic scaling of objects/sceneNodes
		bool					b_DynamicScalingMode;
		Ogre::ManualObject*		m_DynamicCubeXMarker;
		Ogre::ManualObject*		m_DynamicCubeYMarker;
		Ogre::ManualObject*		m_DynamicCubeZMarker;
		Ogre::Vector2			m_ScalingMouseStartPoint;
		Ogre::Vector3			m_ScalingMarkerStartPoint;
		Ogre::Vector3			m_StartingScale;
		//The size of the tool's hooks
		Ogre::Real				CUBE_SIZE;

		// Data needed for the dynamic rotation of objects/sceneNodes
		bool					b_DynamicRotationMode;
		Ogre::ManualObject*		m_DynamicSphereXMarker;
		Ogre::ManualObject*		m_DynamicSphereYMarker;
		Ogre::ManualObject*		m_DynamicSphereZMarker;
		Ogre::ManualObject*		m_FreeDynamicSphereMarker;
		Ogre::Vector3			m_RotationMarkerStartPoint;


	public:
		Ogre::Entity*			GetMVEntity();
		Ogre::SceneNode*		GetMVSceneNode();
		/*! Camera movement speed in editor mode */
		float					m_fCameraMovementSpeed;
		/*! Camera rotation speed in editor mode */
		float					m_fCameraRotationSpeed;

	public:
		/////////////////////////////////////
		// Function comments in the cpp file
		/////////////////////////////////////
		inline GameObject*				CurrentGameObject() { return m_pMVGameObject;}
		// Control editor mode
		inline bool                     UseEditorInput() { return m_bUseEditorInput;}
		inline void                     SetUseEditorInput(bool bSetEditorInput) { m_bUseEditorInput = bSetEditorInput;}
		inline	bool					IsEditorMode() { return m_bIsEditorMode; }
		inline	void					SetEditorMode(bool bIsEditorMode) { m_bIsEditorMode = bIsEditorMode; }

		// Input
		inline GIS::MouseState			Editor_GetEditorMouse() {return ed_MouseState;}
		inline int						Editor_GetKeyCode() {return ed_KeyCode;}
		inline bool						Editor_GetKeyStatus() {return ed_KeyPress;}
		inline bool						Editor_GetKeyStateBuffer(int index) {return ed_KeyStateBuffer[index];}

		// Basic functionalities
		inline	eEditorConnectionStatus	GetEditorConnectionStatus() { return m_eEditorConnectionStatus; }
		inline	void					SetEditorConnectionStatus(eEditorConnectionStatus eConnectionStatus) 
		{ m_eEditorConnectionStatus = eConnectionStatus; 
		  if (eConnectionStatus == GGE_CONNECTED)
			SetUseEditorInput(true); 
		  else 
			SetUseEditorInput(false); 
		}
		int								Editor_StartServer();
		int								Editor_AcceptConnection();
		int								Editor_Receive();
		int								Editor_Send(const char * cpBuffer );
		int								Editor_Parser( int iNumBytesSent, Ogre::String );
		int								Editor_GenericCommand(int packetID, int uniqueID, std::string args);
		int								Editor_GenericCommand(int packetID, std::string args);
		int								Editor_ReportDeprecated( int packetID );
		int								Editor_ReportUnimplemented( int packetID );
		int								Editor_ReportErrorToGLE( const Ogre::String& sMessage );
		int								Editor_ReportErrorToGLE( const Ogre::Exception& ex );
		int								Editor_GGE_Response_Object_Properties(std::string nodeName, int nodeType, std::vector<GLE_PropTuple>& tuples);
		
		//Simple messages are used to communicate a situation to the GLE user
		//this situations doesn't have to be errors
		int								SendSimpleMessage(std::string smessage);

		// Access stats messages depending ofthe object
		std::string						AskStatusMessage(EditorProtocol protocolDefinition);

		int								UpdateEditor();
		int								FinalizeEditor();

		// ModelViewer
#ifdef GLE_EDITOR_MODELVIEW
		void							MVDestoryMovables(Ogre::SceneNode *sp) ;
		void							MVCameraSet( std::vector<Ogre::String> tokens );
		void							MVLoadMesh( std::vector<Ogre::String> tokens );
		void							MVLoadColladaFile( std::vector<Ogre::String> tokens );
		void							MVLoadResLocation( std::vector<Ogre::String> tokens );
#endif
		bool							MVBoundingBoxToggle( std::vector<Ogre::String> tokens );
		int								MVStopAnimation( std::vector<Ogre::String> tokens);
		bool							MVGetListAnimations(std::vector<Ogre::String> tokens);
		bool							MVPlayAnimation( std::vector<Ogre::String> tokens );
		void							MVLoadPS( std::vector<Ogre::String> tokens );
		bool							MVShowBones( std::vector<Ogre::String> tokens );
		bool							MVGetParticleSystemsList();

		// Editor cameras
		Ogre::Camera*					CreateEditorCamera( Ogre::SceneManager* pSceneManager, Ogre::String sCameraSceneNodeName, Ogre::String sCameraName, bool * bNeedToInitCamera);
		void							InitEditorCameras( Ogre::SceneManager* pSceneManager , bool bNeedToResetCameras);
		void							CameraSet( std::vector<Ogre::String> tokens );

		// Picking from the screens
		void							SelectSceneNode(Ogre::String sSceneNodeName);
		Ogre::SceneNode *				GetSceneNodeSelected(										) { return m_pSceneNodeSelected; }
		void							SetSceneNodeSelected( Ogre::SceneNode * pSceneNodeSelected	) { m_pSceneNodeSelected = pSceneNodeSelected; }
		Ogre::MovableObject*			Picking( const GIS::MouseEvent &mouseEvent );
		void							GetMeshInformation(const Ogre::MeshPtr mesh,
															size_t &vertex_count,
															Ogre::Vector3* &vertices,
															size_t &index_count,
															unsigned long* &indices,
															const Ogre::Vector3 &position,
															const Ogre::Quaternion &orient,
															const Ogre::Vector3 &scale);
		int								SendPickedToGLE( Ogre::String sName );		

		// Properties
		std::vector<GLE_PropTuple>			GetEntityProperties( Ogre::Entity* entity );
		std::vector<GLE_PropTuple>			GetLightProperties( Ogre::Light* light );
		std::vector<GLE_PropTuple>			GetSceneNodeProperties( Ogre::SceneNode* node );
		void							SetSceneNodeProperty( GLE_PropTuple &tuple, Ogre::SceneNode* node );
		void							SetLightProperty( GLE_PropTuple &tuple, Ogre::Light* light);
		void							SetEntityProperty( GLE_PropTuple &tuple, Ogre::Entity* entity);

		// Other packets
		void							GridToggle( std::vector<Ogre::String> tokens );

		// Utility functions
		void							Tokenize(const Ogre::String& str, std::vector<Ogre::String>& tokens, const Ogre::String& delimiters = "||");
		void							AltTokenize(const Ogre::String& str, std::vector<Ogre::String>& tokens, const Ogre::String& delimiters = "||");
		float							ParseFloat(const std::string& str);
		int								ParseInt(const std::string& str);
		std::vector<GLE_PropTuple>			ParseTuples(const std::string& str);
		bool							ParseBool(const std::string& str);
		std::string							Int2Str(int i);
		std::string							Float2Str(float f);
		bool							StrCompareNoCase( std::string String1, std::string String2 );
		Ogre::SceneManager*             GetSceneManager();
		void							PreprocessSave(EditorProtocol protocolHandle);
		void							PostprocessSave(EditorProtocol protocolHandle);


		//Functions for dynamic positioning tools
		void							RepositionMarkers(void);
		void							SetMarkerShapes(void);
		void							SetupCube(Ogre::ManualObject* c, std::string s);
		void							SetupSphere(Ogre::ManualObject* manual, std::string s, const int nRings = 16, const int nSegments = 16);
		void							SetupCone(Ogre::ManualObject* manual, std::string s);
		void							CreateMaterial(Ogre::MaterialPtr p, float r, float g, float b, std::string name);
		int								SendMovementToolSelected(bool toolSelected);
		
	};
}
#endif
