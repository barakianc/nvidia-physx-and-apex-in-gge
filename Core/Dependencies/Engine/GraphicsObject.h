#ifndef GRAPHICS_OBJECT_H
#define GRAPHICS_OBJECT_H

#include "GameObjectManager.h"
enum CollisionShapeType;
enum GameObjectType;

/*************************************************GraphicsObjectDeclarationsSTART*********************************************/
///This is a class to hold the information about the renderable object which is created by using OGRE Graphics Engine.
class GraphicsObject
{
public:

	///This is the OGRE SceneManager this GraphicsObject instance is associated with.
	Ogre::SceneManager* m_pOgreSceneManager;

	///This is the unique name of this GraphicsObject instance which is given by the Game programmer.
	///This variable is set while creating the renderable.
	Ogre::String		m_sOgreUniqueName;

	///This is the pointer to created ogre entity.
	///This pointer is just for reducing the indirections used in the code.
	///e.g. GraphicsObject->m_pOgreEntity
	///e.g. GraphicsObject->m_pOgreSceneManager->getEntity(GraphicsObject->m_sOgreUniqueName)
	///e.g. these above lines are doing exactly the same thing
	Ogre::Entity*		m_pOgreEntity;

	///This is the pointer to the OgreSceneNode which the m_pOgreEntity of this GraphicsObject instance is attached to.
	///This pointer is just for reducing the indirections used in the code.
	///e.g. GraphicsObject->m_pOgreSceneNode
	///e.g. GraphicsObject->m_pOgreSceneManager->getEntity(GraphicsObject->m_sOgreUniqueName)->getParentSceneNode()
	///e.g. these above lines are doing exactly the same thing
	Ogre::SceneNode*	m_pOgreSceneNode;	//Scene node of the object

	///This is the pointer to the Ogre Skeleton entity if the HavokObjectType is a SKELETON.
	///This pointer is just for reducing the indirections used in the code.
	///e.g. GraphicsObject->m_pOgreSceneNode
	///e.g. GraphicsObject->m_pOgreSceneManager->getEntity(GraphicsObject->m_sOgreUniqueName)->getParentSceneNode()
	///e.g. these above lines are doing exactly the same thing
	//If the type is a skeleton this will be a pointer to skeleton entity of the object
	Ogre::Skeleton*		m_pOgreSkeleton;

	///If the type is a skeleton this will show how many bones skeleton has
	int					m_iNumberOfSkeletonBones;

	//CONSTRUCTOR_GARPHICSOBJECT_1-->if you search for this line in GraphicsObject.cpp it takes you to the start of this constructor
	///Constructor which creates a Graphics Object by using a .mesh file
	GraphicsObject	(
						Ogre::String		f_sOgreUniqueName,
						Ogre::String		f_sMeshFileName,
						GameObjectType		f_eObjectType,
						Ogre::SceneNode*	f_pOgreSceneNode=NULL
					);

	//CONSTRUCTOR_GARPHICSOBJECT_2-->if you search for this line in GraphicsObject.cpp it takes you to the start of this constructor
	///Constructor which creates a Graphics Object by using Ogre Base types
	GraphicsObject	(
						Ogre::String		f_sOgreUniqueName,
						CollisionShapeType	f_eCollisionShapeType,
						Ogre::SceneNode*	f_pOgreSceneNode=NULL
					);

	///Destructor of Graphics Object
	~GraphicsObject();

	//some functions which needed to be implemented
	void setPosition(float x,float y,float z);
	void setOrientation(float x,float y,float z,float w=0);
	Ogre::Vector3 getPosition();
	void setVisible(bool visibilityBool);
	void changeSceneNode(Ogre::SceneNode* f_pNewSceneNode);
	void destroy();
	Ogre::Entity* createOgreEntity(
										Ogre::String			f_sOgreUniqueName,
										Ogre::String			f_sMeshFileName
								   );
	Ogre::Entity* createOgreEntity(
										Ogre::String			f_sOgreUniqueName,
										CollisionShapeType		f_eCollisionShapeType
									);
	///This is used for the OOGRE skeleton
	void setManualControlOgreSkeleton();
	///To add levels of detail
	void addLODs(std::vector<std::string> &f_eMeshFileNames, std::vector<float> &f_eDistances);
};
/**************************************************GraphicsObjectDeclarationsEND**********************************************/
#endif // GRAPHICS_OBJECT_H