
#ifndef __NET_PLAYER_H__
#define __NET_PLAYER_H__

#include "HavokObject.h"
#include "GameScreen.h"
#include "MultiplayerOnline\RakNet\HavokNetManager.h"
#include "MultiplayerOnline\RakNet\GameObjectReplica.h"

namespace GamePipeGame
{

enum packetID
{
	STATIC_OBJECT_PACKET = ID_USER_PACKET_ENUM,
	OBJECT_PACKET,
	UPDATE_PACKET
};

class CreatePacket
{
public:
	CreatePacket();
	CreatePacket(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type);
	CreatePacket(std::string objectName, Ogre::Vector3 position, Ogre::Quaternion rotation);
	bool LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket);
	bool LoadFromRakNetPacket2(const RakNet::Packet* const rakNetPacket);
	const char* GetObjectName() const { return m_objectName.c_str(); }
	const char* GetMeshName() const { return m_meshName.c_str(); }
	const char* GetHKXFileName() const { return m_hkxFileName.c_str(); }
	Ogre::Vector3 GetPosition() const { return m_position; }
	Ogre::Quaternion GetRotation()const { return m_rotation; }
	GameObjectType GetObjectType() const { return m_type; }
	packetID m_id;
	bool WriteToBitStream(RakNet::BitStream& bitStream) const;
		
private:
	std::string m_objectName;
	std::string m_meshName;
	std::string m_hkxFileName;
	Ogre::Vector3 m_position;
	Ogre::Quaternion m_rotation;
	GameObjectType m_type;
	std::string m_message;
};

class NetPlayer
{
private:

	CharacterProxyObject *charProxy;
	VehicleObject *vehicle;
	bool isServer;
	char serverIP[30];
	int port;
	Ogre::ConfigFile m_configFile;
	HavokNetManager* m_pHavokNetMgr;
	std::deque<LogMessage *> m_messageLog;
	GamePipe::GameScreen *player_gameScreen;
	EntityMapType m_entityMap;
	std::vector<CreatePacket *> staticObjects;

public:

	void *gameData;
	NetPlayer(bool , bool, CharacterProxyObject *, VehicleObject *, GamePipe::GameScreen *gameScreen = NULL, char ip[30] = "", int port = 0, bool (*EventCB)(const RakNet::Packet *const) = 0);
	bool initHavokNetMgr(bool (*EventHandler)(const RakNet::Packet *const));
	bool processMessages();
	bool loadConfigFile(const char* configFileName = "networkedhavokdemo.config");
	void update();
	void addNewStaticObject(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type);
	void sendStaticObjects();
	void createNewObject(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type);
	void updateObject(std::string objectName, Ogre::Vector3 position, Ogre::Quaternion rotation);
	HavokNetManager *getHavokNetManager(){ return m_pHavokNetMgr; }

	static const int DEFAULT_SERVER_PORT = 60000;
	static const int DEFAULT_MAX_CONNECTIONS = 32;
	static const int DEFAULT_SLEEP_INTERVAL = 100;
	static const char* DEFAULT_REMOTE_HOST;
	static const int DEFAULT_PORT = 0;
};

}

#endif