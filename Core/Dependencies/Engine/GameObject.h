#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include "Ogre.h"
#include "GameObjectManager.h"

#include "AnimationManager.h"
#include "AnimatedEntity.h"
#include "AnimatedHavokEntity.h"
#include "AnimatedOgreEntity.h"
#include "GraphicsObject.h"
#include "AudioManager.h"

//Collision listeners need some headers
#include <Common/Base/hkBase.h>
#include <Common/Base/Types/Color/hkColor.h>
#include <Common/Visualize/hkDebugDisplay.h>
#include <Physics/Dynamics/Collide/ContactListener/hkpContactListener.h>
#include <Physics/Dynamics/Entity/hkpEntityListener.h>

#ifdef TBB_PARALLEL_OPTION
#include "TaskContainer.h"
#endif

class PhysicsObject;
class CollisionListener;
class dtAIAgent;

namespace GamePipe{
    class AIAgent;

};


///Enumeration for describing the PhysicsObject Type - used while creating a GameObject
///This enumeration helps us to choose different portion of the code while creating/updating/deleting the GameObject
enum GameObjectType
{
    DEFAULT_GAME_OBJECT_TYPE		= 0,//,//

    GRAPHICS_OBJECT					= 90,//,//

    PHYSICS_FIXED					= 11,//,//
    PHYSICS_DYNAMIC					= 12,//,//
    PHYSICS_KEYFRAMED				= 13,//,//
    PHYSICS_SYSTEM					= 14,//,//
    PHYSICS_CHARACTER_PROXY			= 51,//,//
    PHYSICS_CHARACTER_RIGIDBODY		= 52,//,//

    //THESE TWO ARE ABOUT ANIMATION. SO THE GAMEOBJECT WILL HAVE AN ANIMATED ENTITY FOR THESE TWO OBJECT TYPES
    //THE ANSWER TO QUESTION IS IT HAVOK OR OGRE ANIMATION WILL DEPEND ON IF THERE IS AN HKX FILE ATTACHED OR NOT
    ANIMATED_CHARACTER_PROXY		= 21,//,//PHYSICS_CHARACTER_PROXY WITH ANIMATION
    ANIMATED_CHARACTER_RIGIDBODY	= 22,//,//PHYSICS_CHARACTER_RIGIDBODY WITH ANIMATION

    PHANTOM_SHAPE			= 31,//,//
    PHANTOM_AABB			= 32,//,//

    VEHICLE_CAR				= 41,//,//
    VEHICLE_MOTORCYCLE		= 42,//,//
};
GameObjectType getGameObjectType(hkpMotion::MotionType mt);
hkpMotion::MotionType getHavokMotionType(GameObjectType gameObjectType);

///Enumeration for creating HavokObjects with basic types
///All of the type names are enough to explain which basic type they belong to
enum CollisionShapeType {
    ///if no shape predefined e.g.-->already has a mesh
    DEFAULT_COLLISION_SHAPE			= 0,//,//
    COLLISION_SHAPE_XYPLANE			= 1,//,//can be used for creating scalable plane grounds
    COLLISION_SHAPE_SPHERE			= 2,//,//
    COLLISION_SHAPE_CAPSULE			= 3,//,//
    COLLISION_SHAPE_CYLINDER		= 4,//,//
    COLLISION_SHAPE_BOX				= 5,//,//
    COLLISION_SHAPE_CONVEX_HULL		= 6
};


///Enumeration for creating collision layers between HavokObjects
enum CollisionListenerType
{
    COLLISION_LISTENER_TYPE1			= 1,//,//
    COLLISION_LISTENER_TYPE2			= 2//
};
/*************************************************GameObjectDeclarationsSTART************************************************/
///This is the main wrapper class which can either contain
///a GraphicsObject
///a PhysicsObject and a GraphicsObject
///an AnimationObject and a GraphicsObject
class GameObject
{
private :
	void initializeGameObject();					//initializer
	///If at any time GameObject needs to be deleted
	bool m_bRemoveGameObjectBool;
	//Wwise Game Object ID for using Wwise sound purpose
	AkGameObjectID m_AkGameObjectID;
public:
	///An instance of the PhysicsObject
	PhysicsObject*							m_pPhysicsObject;

	///Contact Listener for implementing ray casting and extra collision detection features
	CollisionListener*						m_pCollisionListener;

	///An instance of the GraphicsObject
	GraphicsObject*							m_pGraphicsObject;

	///An instance of the AnimatedEntity
	AnimationManager::AnimatedEntity*		m_pAnimatedEntity;

    //An instance of the AIAgent
    GamePipe::AIAgent*						m_AIAgent;

	///This holds the Object type
	GameObjectType							m_eObjectType;

	///This holds the PhysicsObject shape type
	CollisionShapeType						m_eCollisionShapeType;

	///This holds the hkx file name which contains the information
	///about the PhysicsObject instance of this GameObject instance
	std::string								m_sHKXFileName;

	//index in the obstacle list
	int										indexInRecastWorld;

	//AN instance of detour AI Agent
	dtAIAgent*								m_dtAIAgent;
	

#ifdef TBB_PARALLEL_OPTION
	int	createPhysicsNow;
	bool physicsRdy;
	int	createAnimNow;
	bool animRdy;
#endif
	//CONSTRUCTOR_GAMEOBJECT_99-->if you search for this line in GameObject.cpp it takes you to the start of this constructor
	GameObject(
					std::string				f_sOgreUniqueName,
					std::string				f_sMeshFileName,
					std::string				f_sHKXFileName="",
					GameObjectType			f_eObjectType=DEFAULT_GAME_OBJECT_TYPE,
					CollisionShapeType		f_eCollisionShapeType=DEFAULT_COLLISION_SHAPE,
                    std::string				f_sLUAFileName="",
                    bool                    f_addAI=false
			);

	///DeConstructor of GameObject
	~GameObject();

	///Sets the position of the GameObject according to it's active members.
	void setPosition(float x,float y,float z);

	///Sets the position of the GameObject according to it's active members.
	Ogre::Vector3 getPosition();

	///To enable and disable the GameObject for debugging purposes
	void setVisible(bool f_bVisibilityBool);

	///When grouping of GameObjects needed
	void changeSceneNode(Ogre::SceneNode* f_pNewSceneNode);

	///This function is for updating each active member of the GameObject
	bool update();

	///Functions for collision listeners
	void addCollisionListener();
	void setOverridableCollisionFuction(CollisionListenerType f_eCollisionFuncType,void* f_pNewOverridableCollisionFuction);

	///Function for casting a ray beam
	bool castRay(Ogre::Vector3 f_vCastTo,hkpRigidBody** f_pFirstCollidedRigidBody,GameObject** f_pCollidedGameObject,Ogre::Vector3* f_vReturnCollisionPoint,const hkVector4 *f_normal = NULL);

	///Functions for setting and getting the object current property --> remove from game and is it alive
	void removeThis();
	bool isActive();
	void scale(float xScale,float yScale,float zScale);
	void resetScale(float xScale,float yScale,float zScale);
///Gets a list of playable animations for this game object
	virtual std::vector<std::string> GetAnimationNames()
	{
		if (m_pAnimatedEntity == NULL)
		{
			std::vector<std::string> emptyList;
			return emptyList;
		}
		else
		{
			return m_pAnimatedEntity->GetAnimationNames();
		}
	}

	bool HasAnimationName(std::string animationNameToCheck)
	{
		if (m_pAnimatedEntity == NULL)
		{
			return false;
		}
		else
		{
			return m_pAnimatedEntity->HasAnimationName(animationNameToCheck);
		}
	}

	void PlayAnimation(std::string f_sAnimationName)
	{
		if (m_pAnimatedEntity != NULL)
		{
			m_pAnimatedEntity->PlayAnimation(f_sAnimationName.c_str());
		}
	}
	void StopAnimation(std::string f_sAnimationName)
	{
		if (m_pAnimatedEntity != NULL)
		{
			m_pAnimatedEntity->StopAnimation(f_sAnimationName.c_str());
		}
	}
#ifdef TBB_PARALLEL_OPTION
	void DelayedCreation();
	TaskContainer* physObject;
	TaskContainer* animObject;
#endif

    GamePipe::AIAgent* getAIAgent();

	dtAIAgent* getdtAIAgent() { return m_dtAIAgent;}

	AkGameObjectID getAkGameObjectID()
	{
		return m_AkGameObjectID;
	}

	void setAkGameObjectID(int gameObjID);
};
/****************************************************GameObjectDeclarationsEND************************************************/
//Different Collision Listener Functions For Allowing the Programmer To Write Different Types Of Collision Listener Functions
typedef	void (*Collision_Function_Type1)(GameObject* f_pOwnerGameObject,GameObject* f_pCollidedGameObject,hkVector4 &f_vCollisionPoint);	/* pointer to over ridable collision_function */
typedef	void (*Collision_Function_Type2)(GameObject* f_pOwnerGameObject,hkpRigidBody* f_pCollidedRigidBody,hkVector4 &f_vCollisionPoint);/* pointer to over ridable collision_function */
/// The collision listener class which triggers the doWhenCollide function of a gameObject
/// by passing the other collided objects pointer
class CollisionListener : public hkReferencedObject, public hkpContactListener, public hkpEntityListener
{
public:

	HK_DECLARE_CLASS_ALLOCATOR( HK_MEMORY_CLASS_DEMO );

	///Create the collision listener for a gameObject and it's rigidBody
	///So that when any collision is detected we can play with both colliding gameObjects
	CollisionListener(GameObject* f_pGameObject,hkpRigidBody* f_pRigidBody);

	~CollisionListener();

	/// Collision events.
	/// If hkpWorldCinfo.m_fireCollisionCallbacks is set to true, then these are fired when
	/// the constraint representing the collision is added or removed from the world.
	/// These callbacks are fired single-threaded but not necessarily in deterministic order.
	/// Note that a collisionAddedCallback does not guarantee that a collision has occurred if a
	/// collisionRemovedCallback is also fired in the same frame. (Of course, contactPointEvents
	/// will be fired if a collision did occur during the frame).
	/***********************************************NEW FUNCTIONS FOR COLLISION LISTENERS START**********************************************/
	/// This is called for each new contact point between two bodies. It will
	/// also be called for all contact points in the contact manifold when the
	/// hkpSimpleConstraintContactMgr's m_contactPointCallbackDelay value
	/// reaches zero.
	virtual void contactPointCallback( const hkpContactPointEvent& event );//NOT IMPLEMENTED YET
	/// New call for a new collision between two bodies.
	virtual void collisionAddedCallback( const hkpCollisionEvent& event );
	/// Called when two bodies are no longer colliding.
	virtual void collisionRemovedCallback( const hkpCollisionEvent& event );
	/************************************************NEW FUNCTIONS FOR COLLISION LISTENERS END***********************************************/
	/*******************************************DEPRECATED FUNCTIONS FOR COLLISION LISTENERS START*******************************************/
	/// Deprecated call for new collision between two rigidBodies
	virtual void contactPointAddedCallback(	hkpContactPointAddedEvent& event );
	/// Deprecated call just before the collisionResult is passed to the constraint system (solved).
	virtual void contactProcessCallback( hkpContactProcessEvent& event );
	/// Deprecated call before a contact point gets removed. We do not implement this for this demo.
	virtual void contactPointRemovedCallback( hkpContactPointRemovedEvent& event );
	/*******************************************DEPRECATED FUNCTIONS FOR COLLISION LISTENERS END*******************************************/
	///Members from base class hkpEntityListener which must be implemented:

	/// Called when the entity is deleted. Important to use this event to remove ourselves as a listener.
	void entityDeletedCallback( hkpEntity* entity );
	/// Called when the entity is removed from the hkpWorld
	void entityRemovedCallback( hkpEntity* entity );
protected:

	// a small structure, which gets attached to each contact point
	struct  ContactPointInfo
	{
		HK_DECLARE_NONVIRTUAL_CLASS_ALLOCATOR( HK_MEMORY_CLASS_DEMO,  ContactPointInfo);
		int m_uniqueId;
	};
	int m_iUniqueRigidBodyID;
	int m_iContactCallBackDelay;

public:

	int m_iReportLevel;
	GameObject* m_pGameObject;
	hkpRigidBody* m_pRigidBody;

	int getUniqueRigidBodyID()
	{
		return m_iUniqueRigidBodyID;
	}

	void setContactCallBackDelay(int f_iNewContactCallBackDelay)
	{
		m_iContactCallBackDelay = f_iNewContactCallBackDelay;
	}

	Collision_Function_Type1 m_fOverridableCollisionFuction_Type1;
	void setOverridableCollisionFuction_Type1(void* f_pNewOverridableCollisionFuction_Type1);
	Collision_Function_Type2 m_fOverridableCollisionFuction_Type2;
	void setOverridableCollisionFuction_Type2(void* f_pNewOverridableCollisionFuction_Type2);
};

#endif