#pragma once

// Utilities
#include "Utilities.h"

#ifdef GLE_Particle_Universe
// ParticleUniverse
#include "ParticleUniverseSystemManager.h"
#endif

// FlashGUI
#include "FlashGUI.h"

#include "VideoCapture.h"

#include "AudioManager.h"

// Configuration
#include "Configuration.h"

// DotScenePlus
#include "DotScenePlusLoader.h"
#include "DotScenePlusSaver.h"

// Editor
#ifdef GLE_EDITOR
#include "Editor.h"
#endif
// Threading
#ifdef TBB_PARALLEL_OPTION
#include "tbb/task_scheduler_init.h"
#include "tbb/task.h"
#include "tbb/spin_mutex.h"
#include "tbb/concurrent_queue.h"
#include "tbb/tbb_thread.h"
#include "GGETimer.h"
class TaskContainer;
#endif

// Editor
#ifdef GLE_EDITOR
#include "Editor.h"
#endif

#include "Profiler.h"

// Easy access
#define EnginePtr (GamePipe::Engine::GetInstancePointer())
#define EngineRef (GamePipe::Engine::GetInstanceReference())

//PhysX
#ifdef PHYSX
//PhysX header files
#include "PxPhysicsAPI.h"
#include "PxDefaultErrorCallback.h"
#include "PxDefaultAllocator.h"
#include "PxExtensionsAPI.h"
#include "PxCudaContextManager.h"
#include "PxTask.h"
//using namespace physx;
#endif

// Windows stuff
#define IDS_APP_TITLE           103
#define IDR_MAINFRAME           128
#define IDD_TUTORIAL1_DIALOG    102
#define IDD_ABOUTBOX            103
#define IDM_ABOUT               104
#define IDM_EXIT                105
#define IDI_TUTORIAL1           107
#define IDI_SMALL               108
#define IDC_TUTORIAL1           109
#define IDC_MYICON              2
#define IDC_STATIC              -1
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                 130
#define _APS_NEXT_RESOURCE_VALUE    129
#define _APS_NEXT_COMMAND_VALUE     32771
#define _APS_NEXT_CONTROL_VALUE     1000
#define _APS_NEXT_SYMED_VALUE       110
#endif
#endif

namespace GamePipe
{
    class Engine : public Singleton<Engine>
    {
    private:
        HINSTANCE m_hInst;
        HWND m_hWnd;

        Ogre::Root* m_pRenderer;
        Ogre::RenderWindow* m_pRenderWindow;
#ifdef TBB_PARALLEL_OPTION
        GGETimer* m_pTimer;
#else
        Ogre::Timer* m_pTimer;
#endif
        float m_fDeltaTime;

        float m_fTotalGameTime;

        GameScreen* m_pLoadingScreen;
        //GGE_Profiler2 *profilerPtr;
        std::vector<GameScreen*> m_vecGameScreens;
        std::vector<GameScreen*> m_vecGameScreensToDelete;
		std::vector<GameScreen*> m_vecGameScreensToAdd;

        unsigned int		m_uiViewportIndex;
		
#ifdef PHYSX
		//foundation
		physx::PxFoundation*						m_PhysXFoundation;
		PVD::PvdConnection*		m_PVDConnection;
#endif

#ifdef GLE_EDITOR
        Editor			m_Editor;	
#endif

    protected:

    public:
        std::string gameFolder; // Used to tell the GGE where the mediaPaths.txt file is. 
        inline Ogre::Root* GetRenderer() { return m_pRenderer; }
        inline Ogre::RenderWindow* GetRenderWindow() { return m_pRenderWindow; }
        inline HWND GetWindowHandle() { return m_hWnd; }
        inline void SetWindowHandle(HWND hWnd) { m_hWnd = hWnd; }



        int GetViewportIndex() { return m_uiViewportIndex; }
        void SetViewportIndex(const unsigned int uiViewportIndex) { m_uiViewportIndex = uiViewportIndex; }
        int GetFreeViewportIndex() { return m_uiViewportIndex++; }

        inline GameScreen* GetLoadingScreen() { return m_pLoadingScreen; }
        inline void SetLoadingScreen(GameScreen* pLoadingScreen) { m_pLoadingScreen = pLoadingScreen; }

        void AddGameScreen(GameScreen* pGameScreen, GameScreen* pCallingGameScreen = NULL);
		void LoadPendingGameScreens();
        void RemoveGameScreen(GameScreen* pGameScreen);

        GameScreen* GetGameScreen(std::string name);
        inline GameScreen* GetForemostGameScreen(bool bInputPtrHandlingScreensOnly = false);

        void BringScreenToFront(std::string name);
        void SendScreenToBack(std::string name);

        inline void ResetViewports();

        inline float GetDeltaTime() { return m_fDeltaTime; }
        inline float GetTotalGameTime() { return m_fTotalGameTime; }
#ifdef PHYSX
		//return the PhysX foundation for the engine
		physx::PxFoundation* GetPhysXFoundation();
		PVD::PvdConnection* GetPVDConnection();
		void				SetPVDConnection(physx::PxPhysics* phy,physx::PxVisualDebuggerConnectionFlags cf);
#endif

        //Threading
#ifdef TBB_PARALLEL_OPTION
		/**
		* bool public variable.
		* keepRendering is set to true at the initialization of the renderer update thread and set to false when the Engine is finalizing.  RenderingLoop
		* checks against keepRendering to continue operation; when it is set false RenderingLoop exits.
		*/
        bool keepRendering;
		
		/**
		* int function ParallelUpdate()
		* ParallelUpdate is the parallel version of int Engine::Update().  It creates a list of tbb::tasks defined in EngineUpdateTasks.h, spawns
		* them and then waits for the tasks to complete before calling UpdateGraphics().
		*/
        int ParallelUpdate();
        
		/**
		* inline int function GetScreensToDelete().
		* GetScreensToDelete() is a utility function to get the number of pending GameScreens to be deleted.  If this is greater than zero, then
		* it might be dangerous to perform updates to GameObjects in RenderingLoop.  Deprecated for now.
		*/
		inline int GetScreensToDelete () { return m_vecGameScreensToDelete.size(); }

		/**
		* inline void function RenderUpdate().
		* RenderUpdate() is the main function that RenderingLoop calls to render the next frame.  RenderingLoop does not initialize and does not own
		* Ogre::Root but it manages updates to Ogre::Root between initialization and finalization steps and calls render updates through this method.
		*/
        inline void RenderUpdate() { m_pRenderer->renderOneFrame(); }

		/**
		* GameScreenDeleteMutexType public variable.
		* GameScreenLock is a tbb::spin_mutex that manages mutual exclusion between RenderingLoop and the main Engine thread during GameScreen deletion.
		*/
        typedef tbb::spin_mutex GameScreenDeleteMutexType;
        GameScreenDeleteMutexType GameScreenLock;

		/**
		* ObjectMutexType public variable.
		* ObjectLock is a tbb::spin_mutex that manages mutual exclusion between RenderingLoop and the main Engine thread during GameScreen deletion.
		* Currently deprecated.
		*/
        typedef tbb::spin_mutex ObjectMutexType;
        ObjectMutexType ObjectLock;

		/**
		* tbb::concurrent_queue<TaskContainer*> public variable.
		* tbb::concurrent_queue<TaskContainer*> is a concurrent container provided by the Intel TBB library.  It implements concurrent versions
		* of most of the STL queue interface without use of explicit locks to manage concurrent access to the queue.  A concurrent_queue was necessary
		* because of the potential for RenderingLoop and the main thread/Havok worker threads to access the global task queue at the same time.
		*/
        tbb::concurrent_queue<TaskContainer*> taskQueue;

		/**
		* int public variable.
		* taskQueueSize holds the current size of the taskQueue.
		*/
        int taskQueueSize;

		/**
		* bool public variable.
		* initializing signals RenderingLoop when the Engine is initializing GameScreens.  If true, RenderingLoop stop all operations and busy waits
		* until it is set to false.
		*/
        bool initializing;

		/**
		* bool public variable.
		* screenTransition signals RenderingLoop when the Engine is transitioning between GameScreens.  This prevents RenderingLoop from calling 
		* updates on tasks that are no longer valid (ie for the old GameScreen).
		*/
        bool screenTransition;	
#endif

#ifdef GLE_EDITOR
        inline Editor*	GetEditor() { return &m_Editor; }
#endif

    private:

    public:
        int Run();

#ifndef DLLRELEASE
        int Initialize(int argc, char* argv[], std::string gFolder);
#else
		int Initialize(int argc, char* argv[], std::string gFolder, std::string strDLLCoreBinPath);
#endif
        int InitializeTime();
        int InitializeInput();
        int InitializePhysics();
        int InitializeGraphics();
		int InitializeVideoCapture();
        int InitializeGUI();

#ifdef DLLRELEASE
		void SetGameFolder(std::string gFolder);
		std::string GetGameFolder() { return gameFolder; }
#endif

        int Update();
        bool UpdateTime();
        int UpdateInput();
        int UpdatePhysics();
        int UpdateGraphics();
		int UpdateVideoCapture();

        int Show();
        int Draw();

        int Finalize();
        int FinalizeTime();
        int FinalizeInput();
        int FinalizePhysics();
        int FinalizeGraphics();
		int FinalizeVideoCapture();
        int Engine::FinalizeAudio();

    public:
        friend GamePipe::Singleton<Engine>;
        GGE_PROFILER;
    private:
        Engine();
        ~Engine() {}
    };
}
