//you need this. it creates pointer to characters which Havok Engine uses
//if you put this line into wrapper.h you will get re-definition errors
//because all cpp files which includes wrapper.h will try to create these character pointers in their .obj copies
//and this will create Linking redefinition problems
#include "StdAfx.h"

#ifdef PHYSX
//if you comment the next line out you will get unresolved symbols error
//this file is needed for linking with pre-compiled havok libraries
#include <Common/Base/Config/hkProductFeatures.cxx>
#endif

#ifdef HAVOK

#include <Common/Base/keycode.cxx>

//this is needed for memory linker purposes. I do not know it in detail.
//if you comment it out it will crash at compile time
#ifndef HK_EXCLUDE_LIBRARY_hkgpConvexDecomposition
#define HK_EXCLUDE_LIBRARY_hkgpConvexDecomposition
#endif

//if you comment the following lines out, it will crash on run time at hkResource.inl (line 17)
//because it will not be able to load resources
#ifndef HK_FEATURE_REFLECTION_PHYSICS
#define HK_FEATURE_REFLECTION_PHYSICS
#endif

//if you comment the next line out you will get unresolved symbols error
//this file is needed for linking with pre-compiled havok libraries
#include <Common/Base/Config/hkProductFeatures.cxx>

#include "HavokWrapper.h"

#endif