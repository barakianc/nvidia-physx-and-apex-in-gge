#include "StdAfx.h"
#include "GraphicsObject.h"
#include <sstream>
#ifdef TBB_PARALLEL_OPTION
#include "TaskTypes.h"
#include "TaskContainer.h"
#include "GraphicsObjectUpdateTask.h"
#include "CreateDefaultGraphicsObjectTask.h"
#include "ChangeSceneNodeTask.h"
#include "SetVisibleTask.h"
#include "CreateHavokShapeTask.h"
#endif

using namespace std;

Ogre::String getUniqueEntityName(	Ogre::SceneManager*	f_pOgreSceneManager,
								 Ogre::String		f_sOgreUniqueName
								 )
{
	int i=0;//for loop purposes
	int startChar=0,lengthChar=1;
	bool entityAlreadyCreated=true;

	Ogre::String f_sTempUniqueName ="";
	Ogre::Entity* f_pTempOgreEntity=NULL;

	while (entityAlreadyCreated) {
		f_sTempUniqueName = f_sOgreUniqueName;
		if (i!=0)
		{
			f_sTempUniqueName.append("_");
			std::stringstream numberBuffer;
			numberBuffer << i;
			//if (i<10) { startChar += 1; lengthChar = 1;}
			//else if (i<100) { startChar += 2; lengthChar = 2;}
			f_sTempUniqueName.append(numberBuffer.str());
		}
		entityAlreadyCreated = f_pOgreSceneManager->hasEntity(f_sTempUniqueName);
		i+=1;
	}
	//if (i!=1) {
	//	GGETRACELOG("Warning.The unique Ogre name you mentioned [%s] Already exists.\n",f_sOgreUniqueName.c_str());
	//	GGETRACELOG("Instead of breaking the programme it has been replaced with [%s] and\n",f_sTempUniqueName.c_str());
	//	GGETRACELOG("GameObject->m_pGraphicsObject->f_sOgreUniqueName is set to [%s]",f_sTempUniqueName.c_str());
	//}
	return f_sTempUniqueName;
}

Ogre::Entity* GraphicsObject::createOgreEntity(
	Ogre::String			f_sOgreUniqueName,
	Ogre::String			f_sMeshFileName
	)
{
	Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	f_sOgreUniqueName = getUniqueEntityName(f_pOgreSceneManager,f_sOgreUniqueName);
	return f_pOgreSceneManager->createEntity(f_sOgreUniqueName,f_sMeshFileName);
}

Ogre::Entity* GraphicsObject::createOgreEntity(
	Ogre::String		f_sOgreUniqueName,
	CollisionShapeType		f_eCollisionShapeType
	)
{
	Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	f_sOgreUniqueName = getUniqueEntityName(f_pOgreSceneManager,f_sOgreUniqueName);
	switch (f_eCollisionShapeType) {
		case COLLISION_SHAPE_XYPLANE :
			return f_pOgreSceneManager->createEntity(f_sOgreUniqueName, Ogre::SceneManager::PT_CUBE);
		case COLLISION_SHAPE_SPHERE :
			return f_pOgreSceneManager->createEntity(f_sOgreUniqueName, Ogre::SceneManager::PT_SPHERE);
		case COLLISION_SHAPE_BOX :
			return f_pOgreSceneManager->createEntity(f_sOgreUniqueName, Ogre::SceneManager::PT_CUBE);
		case COLLISION_SHAPE_CONVEX_HULL :
		default :
			break;
	}
	return NULL;
}

///////////////////////////////////////////////GraphicsObjectDefinitionsSTART/////////////////////////////////////////////////////////////////////////////
GraphicsObject::GraphicsObject(
							   Ogre::String			f_sOgreUniqueName,
							   Ogre::String			f_sMeshFileName,
							   GameObjectType		f_eObjectType,
							   Ogre::SceneNode*		f_pOgreSceneNode
							   )
{
	//initialize parameters to NULL
	m_pOgreEntity				= NULL;
	m_pOgreSkeleton				= NULL;
	m_iNumberOfSkeletonBones	= 0;

	//get the currentSceneManager
	m_pOgreSceneManager			= EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();

	//set the parameters that has been passed by the function
	m_sOgreUniqueName			= f_sOgreUniqueName;
	m_pOgreSceneNode			= f_pOgreSceneNode;

	//RDS_LOOK should I support such a function that creates only 1 scene node but no entity??
	//RDS_LOOK what kindof error checking can be done here??
	//for example :

	//first check if it is a collada file
	int fileNameController = f_sMeshFileName.find(".dae");
	if (fileNameController!=string::npos)
	{
		//if I am here it means that the file name is a Collada file name
		//so construct a GameObject accordingh to Collada file name
		Ogre::Root* myNewRoot = Ogre::Root::getSingletonPtr();
		OgreCollada::ImpExp*  m_pImpExp = OgreCollada::CreateImpExp( myNewRoot , m_pOgreSceneManager );
		if (f_pOgreSceneNode==NULL)
		{
			if ( m_pOgreSceneManager->hasSceneNode(m_sOgreUniqueName+"SceneNode") )
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getSceneNode(m_sOgreUniqueName+"SceneNode");
			}
			else
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(m_sOgreUniqueName+"SceneNode");
			}
		}
		else
		{
			m_pOgreSceneNode = f_pOgreSceneNode;
		}
		// m_pOgreSceneManager->getSceneNode("duck2_1");
		bool f_bLoadStatus = m_pImpExp->importColladaGetSceneNode(&m_pOgreSceneNode, (const OgreCollada::char_t*)f_sMeshFileName.c_str());
		if (!f_bLoadStatus)
		{
			GGETRACELOG("Collada file could not be loaded = %s",f_sMeshFileName);
			exit(1);
		}
		OgreCollada::DestroyImpExp(m_pImpExp);
		return;
	}
	//if the file is not a cvollada file then it should be a mesh file
	fileNameController = f_sMeshFileName.find(".mesh");
	if (fileNameController==string::npos) {
		GGETRACELOG("Error.The mesh file name does not involve '.mesh' in content.Name you sent = %s",f_sMeshFileName);
		exit(1);
		return;
	}
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CreateDefaultGraphicsObjectTask* newTask = new CreateDefaultGraphicsObjectTask(this, f_eObjectType, f_sMeshFileName);
			TaskContainer* container = new TaskContainer((void*)newTask,Create_Default_Graphics_Object_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			if (f_eObjectType==PHYSICS_SYSTEM)
			{
				//this means that this function is called create a scene node
				//so create only a scene node and return
				if (f_pOgreSceneNode==NULL)
				{
					if ( m_pOgreSceneManager->hasSceneNode(m_sOgreUniqueName+"SceneNode") )
					{
						m_pOgreSceneNode = m_pOgreSceneManager->getSceneNode(m_sOgreUniqueName+"SceneNode");
					}
					else
					{
						m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(m_sOgreUniqueName+"SceneNode");
					}
				}
				else
				{
					m_pOgreSceneNode = f_pOgreSceneNode;
				}
			}
			else
			{
				//RDS_LOOK do we need to check if the stuff is not created for some reason??
				m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,f_sMeshFileName);

				//if ((f_eObjectType==ANIMATION_ENTITY_HAVOK)||(f_eObjectType==ANIMATION_ENTITY_HAVOK_AND_OGRE))//RDS_PREVDEFINITION || hType==RAGDOLL)
				//{
				//	m_pOgreSkeleton = m_pOgreEntity->getSkeleton();
				//	setManualControlOgreSkeleton();
				//}
				if (m_pOgreSceneNode!=NULL)
				{
					m_pOgreSceneNode->attachObject(m_pOgreEntity);
				}
				else
				{
					if ( m_pOgreSceneManager->hasSceneNode(m_sOgreUniqueName+"SceneNode") )
					{
						m_pOgreSceneNode = m_pOgreSceneManager->getSceneNode(m_sOgreUniqueName+"SceneNode");
					}
					else
					{
						m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(m_sOgreUniqueName+"SceneNode");
					}
					m_pOgreSceneNode->attachObject(m_pOgreEntity);
				}
			}
		}
#else
	if (f_eObjectType==PHYSICS_SYSTEM)
	{
		//this means that this function is called create a scene node
		//so create only a scene node and return
		if (f_pOgreSceneNode==NULL)
		{
			if ( m_pOgreSceneManager->hasSceneNode(m_sOgreUniqueName+"SceneNode") )
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getSceneNode(m_sOgreUniqueName+"SceneNode");
			}
			else
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(m_sOgreUniqueName+"SceneNode");
			}
		}
		else
		{
			m_pOgreSceneNode = f_pOgreSceneNode;
		}
	}
	else
	{
		//RDS_LOOK do we need to check if the stuff is not created for some reason??
		m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,f_sMeshFileName);

		if (m_pOgreEntity->hasSkeleton())
		{
			m_pOgreSkeleton = m_pOgreEntity->getSkeleton();
		}
		if (m_pOgreSceneNode!=NULL)
		{
			m_pOgreSceneNode->attachObject(m_pOgreEntity);
		}
		else
		{
			if ( m_pOgreSceneManager->hasSceneNode(m_sOgreUniqueName+"SceneNode") )
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getSceneNode(m_sOgreUniqueName+"SceneNode");
			}
			else
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(m_sOgreUniqueName+"SceneNode");
			}
			m_pOgreSceneNode->attachObject(m_pOgreEntity);
		}
	}
#endif
}

//CONSTRUCTOR_GARPHICSOBJECT_2
///Constructor which creates a Graphics Object by using Ogre Base types
GraphicsObject::GraphicsObject	(
								 Ogre::String		f_sOgreUniqueName,
								 CollisionShapeType	f_eCollisionShapeType,
								 Ogre::SceneNode*	f_pOgreSceneNode
								 )
{
	//initialize parameters to NULL
	m_pOgreEntity				= NULL;
	m_pOgreSkeleton				= NULL;
	m_iNumberOfSkeletonBones	= 0;

	//get the currentSceneManager
	m_pOgreSceneManager			= EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();

	//set the parameters that has been passed by the function
	m_sOgreUniqueName			= f_sOgreUniqueName;
	m_pOgreSceneNode			= f_pOgreSceneNode;

	//this is just for debugging purposes
	Ogre::Vector3 halfSize;

	switch (f_eCollisionShapeType) {
		case COLLISION_SHAPE_XYPLANE :
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CreateHavokShapeTask* newTask = new CreateHavokShapeTask(this, COLLISION_SHAPE_XYPLANE);
			TaskContainer* container = new TaskContainer((void*)newTask,Create_Havok_Shape_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,COLLISION_SHAPE_XYPLANE);
			halfSize = m_pOgreEntity->getBoundingBox().getHalfSize();

			if (m_pOgreSceneNode!=NULL)
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			else
			{
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			}
			//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
			m_pOgreSceneNode->scale(100.0f/halfSize.x,0.01f/halfSize.y,100.0f/halfSize.z);
		}
#else
			m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,COLLISION_SHAPE_XYPLANE);
			halfSize = m_pOgreEntity->getBoundingBox().getHalfSize();

			if (m_pOgreSceneNode!=NULL)
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			else {
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			}
			//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
			m_pOgreSceneNode->scale(100.0f/halfSize.x,0.01f/halfSize.y,100.0f/halfSize.z);
#endif
			break;
		case COLLISION_SHAPE_SPHERE :
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CreateHavokShapeTask* newTask = new CreateHavokShapeTask(this, COLLISION_SHAPE_SPHERE);
			TaskContainer* container = new TaskContainer((void*)newTask,Create_Havok_Shape_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,COLLISION_SHAPE_SPHERE);
			halfSize = m_pOgreEntity->getBoundingBox().getHalfSize();

			if (m_pOgreSceneNode!=NULL)
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			else {
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			}
			//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
			m_pOgreSceneNode->scale(1/halfSize.x,1/halfSize.y,1/halfSize.z);
		}
#else
			m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,COLLISION_SHAPE_SPHERE);
			halfSize = m_pOgreEntity->getBoundingBox().getHalfSize();

			if (m_pOgreSceneNode!=NULL)
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			else {
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			}
			//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
			m_pOgreSceneNode->scale(1/halfSize.x,1/halfSize.y,1/halfSize.z);
#endif
			break;
		case COLLISION_SHAPE_CAPSULE :

			break;
		case COLLISION_SHAPE_CYLINDER :

			break;
		case COLLISION_SHAPE_BOX :
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CreateHavokShapeTask* newTask = new CreateHavokShapeTask(this, COLLISION_SHAPE_BOX);
			TaskContainer* container = new TaskContainer((void*)newTask,Create_Havok_Shape_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,COLLISION_SHAPE_BOX);
			halfSize = m_pOgreEntity->getBoundingBox().getHalfSize();

			if (m_pOgreSceneNode!=NULL)
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			else {
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			}
			//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
			m_pOgreSceneNode->scale(1/halfSize.x,1/halfSize.y,1/halfSize.z);
		}
#else
			m_pOgreEntity = createOgreEntity(m_sOgreUniqueName,COLLISION_SHAPE_BOX);
			halfSize = m_pOgreEntity->getBoundingBox().getHalfSize();

			if (m_pOgreSceneNode!=NULL)
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			else {
				m_pOgreSceneNode = m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
				m_pOgreSceneNode->attachObject(m_pOgreEntity);
			}
			//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
			m_pOgreSceneNode->scale(1/halfSize.x,1/halfSize.y,1/halfSize.z);
#endif
			break;
		case COLLISION_SHAPE_CONVEX_HULL :

			break;
		default :

			break;
	}
	//RDS_LOOK do we need to check if the stuff is not created for some reason??
}

GraphicsObject::~GraphicsObject()
{
	//RDS_LOOK what kindof error checking can be done here??
	if (m_pOgreSceneNode!=NULL)
	{
		if(m_pOgreEntity!=NULL)
		{
            if (m_pOgreSceneNode->getChildIterator().hasMoreElements())
                m_pOgreSceneNode->removeAllChildren();
			m_pOgreSceneNode->detachObject(m_pOgreEntity);
			m_pOgreSceneManager->destroyEntity(m_pOgreEntity);
		}
		//if the entity deleted was the only one that was attached to the scene node then delete it too
		if (m_pOgreSceneNode->numAttachedObjects()<1)
		{
			m_pOgreSceneNode->removeAndDestroyAllChildren();
			m_pOgreSceneManager->destroySceneNode(m_pOgreSceneNode);
		}
	}
}

void GraphicsObject::destroy()
{
	this->~GraphicsObject();
}
void GraphicsObject::setPosition(float x,float y,float z)
{
#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		GraphicsObjectUpdateTask* newTask = new GraphicsObjectUpdateTask(this,x,y,z);
		TaskContainer* container = new TaskContainer((void*)newTask,Graphics_Object_Update_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		if (m_pOgreSceneNode) m_pOgreSceneNode->setPosition(x,y,z);
	}
#else
	if (m_pOgreSceneNode) m_pOgreSceneNode->setPosition(x,y,z);
#endif
}

void GraphicsObject::setOrientation(float x,float y,float z,float w)
{
#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		GraphicsObjectUpdateTask* newTask = new GraphicsObjectUpdateTask(this,w,x,y,z);
		TaskContainer* container = new TaskContainer((void*)newTask,Graphics_Object_Update_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		if (m_pOgreSceneNode) m_pOgreSceneNode->setOrientation(w,x,y,z);
	}
#else
	if (m_pOgreSceneNode) m_pOgreSceneNode->setOrientation(w,x,y,z);
#endif
}

Ogre::Vector3 GraphicsObject::getPosition()
{
	Ogre::Vector3 returnPosition = Ogre::Vector3(.0,.0,.0);
	if (m_pOgreSceneNode) returnPosition = m_pOgreSceneNode->getPosition();
	return returnPosition;
}

void GraphicsObject::setVisible(bool visibilityBool)
{
#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		SetVisibleTask* newTask = new SetVisibleTask(this,visibilityBool);
		TaskContainer* container = new TaskContainer((void*)newTask,Set_Visible_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		if (m_pOgreEntity)
			m_pOgreEntity->setVisible(visibilityBool);
	}
#else
	if (m_pOgreEntity)
		m_pOgreEntity->setVisible(visibilityBool);
#endif
}

void GraphicsObject::changeSceneNode(Ogre::SceneNode* f_pNewSceneNode)
{
#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		ChangeSceneNodeTask* newTask = new ChangeSceneNodeTask(this,f_pNewSceneNode);
		TaskContainer* container = new TaskContainer((void*)newTask,Change_Scene_Node_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
		if (m_pOgreSceneNode)
		{
			m_pOgreSceneNode->detachObject(m_pOgreEntity);
			f_pOgreSceneManager->destroySceneNode(m_pOgreSceneNode);
			m_pOgreSceneNode = f_pNewSceneNode;
			m_pOgreSceneNode->attachObject(m_pOgreEntity);
		}
	}
#else
	Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	if (m_pOgreSceneNode)
	{
		m_pOgreSceneNode->detachObject(m_pOgreEntity);
		f_pOgreSceneManager->destroySceneNode(m_pOgreSceneNode);
		m_pOgreSceneNode = f_pNewSceneNode;
		m_pOgreSceneNode->attachObject(m_pOgreEntity);
	}
#endif
}

void GraphicsObject::setManualControlOgreSkeleton()
{
	Ogre::Bone* f_pOgreBone;//for iteration of bones

	Ogre::Skeleton::BoneIterator f_pBoneIterator = m_pOgreSkeleton->getBoneIterator();

	m_iNumberOfSkeletonBones = m_pOgreSkeleton->getNumBones();

	for(int i = 0 ; i<m_iNumberOfSkeletonBones; ++i)
	{
		f_pOgreBone = f_pBoneIterator.peekNext();
		f_pOgreBone->setManuallyControlled(true);
		f_pBoneIterator.moveNext();
	}
}

void GraphicsObject::addLODs(std::vector<std::string> &f_eMeshFileNames, std::vector<float> &f_eDistances)
{
    // TODO - THERE IS AN ISSUE WITH LOD IF YOU ARE USING A NON LOD MESH OF THE
    // SAME TYPE.

	Ogre::String lodName = "..\\..\\Trunk_AI\\Media\\Models\\lod_";
	lodName.append(f_eMeshFileNames[0]);

	if (Ogre::MeshManager::getSingleton().getByName(lodName).isNull() == true)
	{
		Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().load(f_eMeshFileNames[0], "General"); // level 0, 100 percent

		for(unsigned int i = 1; i < f_eMeshFileNames.size(); i++)
		{
			mesh->createManualLodLevel(f_eDistances[i - 1], f_eMeshFileNames[i]); // level 1, 50 percent
		}

		Ogre::MeshSerializer ser;
		ser.exportMesh(mesh.get(), lodName);
	}

	m_pOgreSceneNode->detachObject(m_pOgreEntity);
	m_pOgreSceneManager->destroyEntity(m_pOgreEntity);

	m_pOgreEntity = createOgreEntity(m_sOgreUniqueName, lodName);
	m_pOgreSceneNode->attachObject(m_pOgreEntity);
}

///////////////////////////////////////////////GraphicsObjectDefinitionsEND/////////////////////////////////////////////////////////////////////////////