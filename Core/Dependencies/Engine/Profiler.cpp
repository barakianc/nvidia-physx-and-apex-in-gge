#include "StdAfx.h"

//#include "OgreStableHeaders.h"
//#include "Profiler.h"
#include "GameScreen.h"
#include "Engine.h"
#include "VTuneApi.h"

namespace GamePipe {

    //---------------------------------------------------------------
	//---------------------------------------------------------------
	//OGRE PROFILER FUNCTIONS
	//---------------------------------------------------------------
	//---------------------------------------------------------------
    GGE_Profiler::GGE_Profiler() {

        // init some variables
        mTimer = 0;
        mTotalFrameTime = 0;
        mUpdateDisplayFrequency = 0;
        mCurrentFrame = 0;
        mEnabled = mNewEnableState = false; // the profiler starts out as disabled
        mEnableStateChangePending = false;
        mInitialized = false;
        maxProfiles = 50;
		
        // by default the display will be updated every 10 frames
        mUpdateDisplayFrequency = 10;

    }
    //-----------------------------------------------------------------------
    GGE_Profiler::~GGE_Profiler() {

        if (!mProfileHistory.empty()) {
            // log the results of our profiling before we quit
            logResults();
        }

        // clear all our lists
        mProfiles.clear();
        mProfileFrame.clear();
        mProfileHistoryMap.clear();
        mProfileHistory.clear();
        mDisabledProfiles.clear();
        mProfileBars.clear();

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::initialize() {

        // init some gui characteristics
        mBarHeight = 10; //0.02;
        mGuiBorderWidth = 10; //0.02;
        mGuiHeight = 25; //0.05;
        mGuiWidth = 250; //0.15;
        mBarIndent = mGuiWidth;
        mBarLineWidth = 2;

		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("Bootstrap");

        // create a new overlay to hold our Profiler display
		mOverlay = Ogre::OverlayManager::getSingleton().create("Profiler");
        mOverlay->setZOrder(500);

        // this panel will be the main container for our profile bars
        mProfileGui = createContainer();

        Ogre::OverlayElement* element;

        // we create the little "ticks" above the profiles
        for (int k = 1; k < 10; ++k) { // we don't want a tick at 0% or 100%

            if (k != 5) { // we don't want a tick at 50%
				element = createTextArea("ProfileKeyLine" + Ogre::StringConverter::toString(k), Ogre::Real(20), Ogre::Real(10), Ogre::Real(2), Ogre::Real(mGuiWidth * (1 + k * .1)), 9, "|");
                mProfileGui->addChild(element);
            }

        }

        // we create a 0% marker
		element = createTextArea("ProfileKey0", Ogre::Real(50), Ogre::Real(10), Ogre::Real(2), Ogre::Real(mGuiWidth * 0.99), 9, "0%");
        mProfileGui->addChild(element);

        // we create a 50% marker
        element = createTextArea("ProfileyKey50", Ogre::Real(50), Ogre::Real(10), Ogre::Real(2), Ogre::Real(mGuiWidth * 1.48), 9, "50%");
        mProfileGui->addChild(element);

        // we create a 100% marker
        element = createTextArea("ProfileKey100", Ogre::Real(50), Ogre::Real(10), Ogre::Real(2), Ogre::Real(mGuiWidth * 1.98), 9, "100%");
        mProfileGui->addChild(element);

        // we create an initial pool of 50 profile bars
        for (unsigned int i = 0; i < maxProfiles; ++i) {

            // this is for the profile name and the number of times it was called in a frame
            element = createTextArea("profileText" + Ogre::StringConverter::toString(i), 90, mBarHeight, mGuiBorderWidth + (mBarHeight * 2) * i, 0, 14, "", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the current frame time
            element = createPanel("currBar" + Ogre::StringConverter::toString(i), 0, mBarHeight, mGuiBorderWidth + (mBarHeight * 2) * i, mBarIndent, "Core/ProfilerCurrent", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the minimum frame time
            element = createPanel("minBar" + Ogre::StringConverter::toString(i), mBarLineWidth, mBarHeight, mGuiBorderWidth + (mBarHeight * 2) * i, 0, "Core/ProfilerMin", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the maximum frame time
            element = createPanel("maxBar" + Ogre::StringConverter::toString(i), mBarLineWidth, mBarHeight, mGuiBorderWidth + (mBarHeight * 2) * i, 0, "Core/ProfilerMax", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

            // this indicates the average frame time
            element = createPanel("avgBar" + Ogre::StringConverter::toString(i), mBarLineWidth, mBarHeight, mGuiBorderWidth + (mBarHeight * 2) * i, 0, "Core/ProfilerAvg", false);
            mProfileGui->addChild(element);
            mProfileBars.push_back(element);

        }

        // throw everything all the GUI stuff into the overlay and display it
        mOverlay->add2D(mProfileGui);
        mOverlay->show();

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::setTimer(Ogre::Timer* t) {
		t->reset();
        mTimer = t;

    }
    //-----------------------------------------------------------------------
    Ogre::Timer* GGE_Profiler::getTimer() {

        assert(mTimer && "Timer not set!");
        return mTimer;

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::setEnabled(bool enabled) {

        if (!mInitialized && enabled) {

            // the user wants to enable the Profiler for the first time
            // so we initialize the GUI stuff
            initialize();
            mInitialized = true;
            mEnabled = true;

        }
        else {
            // We store this enable/disable request until the frame ends
            // (don't want to screw up any open profiles!)
            mEnableStateChangePending = true;
            mNewEnableState = enabled;
        }

    }
    //-----------------------------------------------------------------------
    bool GGE_Profiler::getEnabled() const {

        return mEnabled;

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::disableProfile(const Ogre::String& profileName) {

        // make sure the profile isn't already disabled
        DisabledProfileMap::iterator iter;
        iter = mDisabledProfiles.find(profileName);

        // make sure you don't disable a profile in the middle of that profile
        ProfileStack::iterator pIter;
        for (pIter = mProfiles.begin(); pIter != mProfiles.end(); ++pIter) {

            if (profileName == (*pIter).name)
                break;

        }

        // if those two conditions are met, disable the profile
        if ( (iter == mDisabledProfiles.end()) && (pIter == mProfiles.end()) ) {

			mDisabledProfiles.insert(std::pair<Ogre::String, bool>(profileName, true));

        }

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::enableProfile(const Ogre::String& profileName) {

        // make sure the profile is actually disabled
        DisabledProfileMap::iterator iter;
        iter = mDisabledProfiles.find(profileName);

        // make sure you don't enable a profile in the middle of that profile
        ProfileStack::iterator pIter;
        for (pIter = mProfiles.begin(); pIter != mProfiles.end(); ++pIter) {

            if (profileName == (*pIter).name)
                break;

        }

        // if those two conditions are met, enable the profile by removing it from
        // the disabled list
        if ( (iter != mDisabledProfiles.end()) && (pIter == mProfiles.end()) ) {

            mDisabledProfiles.erase(iter);

        }

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::beginProfile(const Ogre::String& profileName) {

        // if the profiler is enabled
        if (!mEnabled) {

            return;

        }

        // empty string is reserved for the root
        assert ((profileName != "") && ("Profile name can't be an empty string"));

        ProfileStack::iterator iter;
        for (iter = mProfiles.begin(); iter != mProfiles.end(); ++iter) {

            if ((*iter).name == profileName) {

                break;

            }

        }

        // make sure this profile isn't being used more than once
        assert ((iter == mProfiles.end()) && ("This profile name is already being used"));

        // we only process this profile if isn't disabled
        DisabledProfileMap::iterator dIter;
        dIter = mDisabledProfiles.find(profileName);
        if ( dIter != mDisabledProfiles.end() ) {

            return;

        }

        ProfileInstance p;
		p.hierarchicalLvl = static_cast<unsigned int>(mProfiles.size());

        // this is the root, it has no parent
        if (mProfiles.empty()) {

            p.parent = "";

        }
        // otherwise peek at the stack and use the top as the parent
        else {

            ProfileInstance parent = mProfiles.back();
            p.parent = parent.name;

        }

        // need a timer to profile!
        assert (mTimer && "Timer not set!");

        ProfileFrameList::iterator fIter;
        ProfileHistoryList::iterator hIter;

        // we check to see if this profile has been called in the frame before
        for (fIter = mProfileFrame.begin(); fIter != mProfileFrame.end(); ++fIter) {

            if ((*fIter).name == profileName)
                break;

        }
        // if it hasn't been called before, set its position in the stack
        if (fIter == mProfileFrame.end()) {

            ProfileFrame f;
            f.name = profileName;
            f.frameTime = 0;
            f.calls = 0;
            f.hierarchicalLvl = (unsigned int) mProfiles.size();
            mProfileFrame.push_back(f);

        }

        // we check to see if this profile has been called in the app before
        ProfileHistoryMap::iterator histMapIter;
        histMapIter = mProfileHistoryMap.find(profileName);

        // if not we add a profile with just the name into the history
        if (histMapIter == mProfileHistoryMap.end()) {

            ProfileHistory h;
            h.name = profileName;
            h.numCallsThisFrame = 0;
            h.totalTime = 0;
            h.totalCalls = 0;
            h.maxTime = 0;
            h.minTime = 1;
            h.hierarchicalLvl = p.hierarchicalLvl;
            h.currentTime = 0;

            // we add this to the history
            hIter = mProfileHistory.insert(mProfileHistory.end(), h);

            // for quick look-ups, we'll add it to the history map as well
            mProfileHistoryMap.insert(std::pair<Ogre::String, ProfileHistoryList::iterator>(profileName, hIter));

        }

        // add the stats to this profile and push it on the stack
        // we do this at the very end of the function to get the most
        // accurate timing results
        p.name = profileName;
        p.currTime = mTimer->getMicroseconds();
        p.accum = 0;
        mProfiles.push_back(p);

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::endProfile(const Ogre::String& profileName) {

		// if the profiler received a request to be enabled or disabled
		// we reached the end of the frame so we can safely do this
		if (mEnableStateChangePending) {

			changeEnableState();

		}

     // if the profiler is enabled
        if(!mEnabled) {

            return;

        }

        // need a timer to profile!
        assert (mTimer && "Timer not set!");

        // get the end time of this profile
        // we do this as close the beginning of this function as possible
        // to get more accurate timing results
        Ogre::ulong endTime = mTimer->getMicroseconds();

        // empty string is reserved for designating an empty parent
        assert ((profileName != "") && ("Profile name can't be an empty string"));

        // we only process this profile if isn't disabled
        DisabledProfileMap::iterator dIter;
        dIter = mDisabledProfiles.find(profileName);
        if ( dIter != mDisabledProfiles.end() ) {

            return;

        }

        // stack shouldnt be empty
        assert (!mProfiles.empty());

        // get the start of this profile
        ProfileInstance bProfile;
        bProfile = mProfiles.back();
        mProfiles.pop_back();

        // calculate the elapsed time of this profile
        Ogre::ulong timeElapsed = endTime - bProfile.currTime;

        // update parent's accumalator if it isn't the root
        if (bProfile.parent != "") {

            // find the parent
            ProfileStack::iterator iter;
            for(iter = mProfiles.begin(); iter != mProfiles.end(); ++iter) {

                if ((*iter).name == bProfile.parent)
                    break;

            }

            // the parent should be found 
            assert(iter != mProfiles.end());

            // add this profile's time to the parent's accumlator
            (*iter).accum += timeElapsed;

        }

        // we find the profile in this frame
        ProfileFrameList::iterator iter;
        for (iter = mProfileFrame.begin(); iter != mProfileFrame.end(); ++iter) {

            if ((*iter).name == bProfile.name)
                break;

        }

        // we subtract the time the children profiles took from this profile
        (*iter).frameTime += timeElapsed - bProfile.accum;
        (*iter).calls++;

        // the stack is empty and all the profiles have been completed
        // we have reached the end of the frame so process the frame statistics
        if (mProfiles.empty()) {

            // we know that the time elapsed of the main loop is the total time the frame took
            mTotalFrameTime = timeElapsed;

            // we got all the information we need, so process the profiles
            // for this frame
            processFrameStats();

            // clear the frame stats for next frame
            mProfileFrame.clear();

            // we display everything to the screen
            displayResults();

        }

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::processFrameStats() {

        ProfileFrameList::iterator frameIter;
        ProfileHistoryList::iterator historyIter;

        // we set the number of times each profile was called per frame to 0
        // because not all profiles are called every frame
        for (historyIter = mProfileHistory.begin(); historyIter != mProfileHistory.end(); ++historyIter) {

            (*historyIter).numCallsThisFrame = 0;

        }

        // iterate through each of the profiles processed during this frame
        for (frameIter = mProfileFrame.begin(); frameIter != mProfileFrame.end(); ++frameIter) {

            Ogre::String s = (*frameIter).name;

            // use our map to find the appropriate profile in the history
            historyIter = (*mProfileHistoryMap.find(s)).second;

            // extract the frame stats
            Ogre::ulong frameTime = (*frameIter).frameTime;
            unsigned int calls = (*frameIter).calls;
            unsigned int lvl = (*frameIter).hierarchicalLvl;

            // calculate what percentage of frame time this profile took
            Ogre::Real framePercentage = (Ogre::Real) frameTime / (Ogre::Real) mTotalFrameTime;

            // update the profile stats
            (*historyIter).currentTime = framePercentage;
            (*historyIter).totalTime += framePercentage;
            (*historyIter).totalCalls++;
            (*historyIter).numCallsThisFrame = calls;
            (*historyIter).hierarchicalLvl = lvl;

            // if we find a new minimum for this profile, update it
            if ((framePercentage) < ((*historyIter).minTime)) {

                (*historyIter).minTime = framePercentage;

            }

            // if we find a new maximum for this profile, update it
            if ((framePercentage) > ((*historyIter).maxTime)) {

                (*historyIter).maxTime = framePercentage;

            }

        }

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::displayResults() {

        if (!mEnabled) {

            return;

        }

        // if its time to update the display
        if (mCurrentFrame >= mUpdateDisplayFrequency) {

            mCurrentFrame = 0;

            ProfileHistoryList::iterator iter;
            ProfileBarList::iterator bIter;

			Ogre::OverlayElement* g;

            Ogre::Real newGuiHeight = mGuiHeight;

            int temp = 0; // dummy variable for weird Ogre issue

            // go through each profile and display it
            for (iter = mProfileHistory.begin(), bIter = mProfileBars.begin(); 
				iter != mProfileHistory.end() && bIter != mProfileBars.end(); 
				++iter, ++bIter) 
			{

                // display the profile's name and the number of times it was called in a frame
                g = *bIter;
                g->show();
				g->setCaption(Ogre::String((*iter).name + " (" + Ogre::StringConverter::toString((*iter).numCallsThisFrame) + ")"));
				g->setLeft(Ogre::Real(10 + (*iter).hierarchicalLvl * 15));

                // display the main bar that show the percentage of the frame time that this
                // profile has taken
                bIter++;
                g = *bIter;
                g->show();
                // most of this junk has been set before, but we do this to get around a weird
                // Ogre gui issue (bug?)
			
				g->setMetricsMode(Ogre::GMM_PIXELS);
                g->setHeight(mBarHeight);
                g->setWidth(((*iter).currentTime) * mGuiWidth);
                g->setLeft(mGuiWidth);
                g->setTop(mGuiBorderWidth + temp * mBarHeight * 2);

                // display line to indicate the minimum frame time for this profile
                bIter++;
                g = *bIter;
                g->show();
                g->setLeft(mBarIndent + (*iter).minTime * mGuiWidth);

                // display line to indicate the maximum frame time for this profile
                bIter++;
                g = *bIter;
                g->show();
                g->setLeft(mBarIndent + (*iter).maxTime * mGuiWidth);

                // display line to indicate the average frame time for this profile
                bIter++;
                g = *bIter;
                g->show();
                if ((*iter).totalCalls != 0)
                    g->setLeft(mBarIndent + ((*iter).totalTime / (*iter).totalCalls) * mGuiWidth);
                else
                    g->setLeft(mBarIndent);
                // we set the height of the display with respect to the number of profiles displayed
                newGuiHeight += mBarHeight * 2;

                temp++;

            }

            // set the main display dimensions
			mProfileGui->setMetricsMode(Ogre::GMM_PIXELS);
            mProfileGui->setHeight(newGuiHeight);
            mProfileGui->setWidth(mGuiWidth * 2 + 15);
            mProfileGui->setTop(5);
            mProfileGui->setLeft(5);

            // we hide all the remaining pre-created bars
            for (; bIter != mProfileBars.end(); ++bIter) {

                (*bIter)->hide();

            }

        }

        // not time to update the display yet
        else {

            mCurrentFrame++;

        }

    }
    //-----------------------------------------------------------------------
    bool GGE_Profiler::watchForMax(const Ogre::String& profileName) {

        ProfileHistoryMap::iterator mapIter;
        ProfileHistoryList::iterator iter;

        mapIter = mProfileHistoryMap.find(profileName);

        // if we don't find the profile, return false
        if (mapIter == mProfileHistoryMap.end())
            return false;

        iter = (*mapIter).second;

        return ((*iter).currentTime == (*iter).maxTime);

    }
    //-----------------------------------------------------------------------
    bool GGE_Profiler::watchForMin(const Ogre::String& profileName) {

        ProfileHistoryMap::iterator mapIter;
        ProfileHistoryList::iterator iter;

        mapIter = mProfileHistoryMap.find(profileName);

        // if we don't find the profile, return false
        if (mapIter == mProfileHistoryMap.end())
            return false;

        iter = (*mapIter).second;

        return ((*iter).currentTime == (*iter).minTime);

    }
    //-----------------------------------------------------------------------
    bool GGE_Profiler::watchForLimit(const Ogre::String& profileName, Ogre::Real limit, bool greaterThan) {

        ProfileHistoryMap::iterator mapIter;
        ProfileHistoryList::iterator iter;

        mapIter = mProfileHistoryMap.find(profileName);

        // if we don't find the profile, return false
        if (mapIter == mProfileHistoryMap.end())
            return false;

        iter = (*mapIter).second;

        if (greaterThan)
            return ((*iter).currentTime > limit);
        else
            return ((*iter).currentTime < limit);

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::logResults() {

        ProfileHistoryList::iterator iter;

        Ogre::LogManager::getSingleton().logMessage("----------------------Profiler Results----------------------");

        for (iter = mProfileHistory.begin(); iter != mProfileHistory.end(); ++iter) {

            // create an indent that represents the hierarchical order of the profile
            Ogre::String indent = "";
            for (unsigned int i = 0; i < (*iter).hierarchicalLvl; ++i) {

                indent = indent + "   ";

            }

            Ogre::LogManager::getSingleton().logMessage(indent + "Name " + (*iter).name + " | Min " + Ogre::StringConverter::toString((*iter).minTime) + " | Max " + Ogre::StringConverter::toString((*iter).maxTime) + " | Avg "+ Ogre::StringConverter::toString((*iter).totalTime / (*iter).totalCalls));

        }

        Ogre::LogManager::getSingleton().logMessage("------------------------------------------------------------");

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::reset() {

        ProfileHistoryList::iterator iter;
        for (iter = mProfileHistory.begin(); iter != mProfileHistory.end(); ++iter) {
        
            (*iter).currentTime = (*iter).maxTime = (*iter).totalTime = 0;
            (*iter).numCallsThisFrame = (*iter).totalCalls = 0;

            (*iter).minTime = 1;

        }

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::setUpdateDisplayFrequency(unsigned int freq) {

        mUpdateDisplayFrequency = freq;

    }
    //-----------------------------------------------------------------------
    unsigned int GGE_Profiler::getUpdateDisplayFrequency() const {

        return mUpdateDisplayFrequency;

    }
    //-----------------------------------------------------------------------
    void GGE_Profiler::changeEnableState() {

        if (mNewEnableState) {

            mOverlay->show();

        }
        else {

            mOverlay->hide();

        }
        mEnabled = mNewEnableState;
        mEnableStateChangePending = false;

    }
    //-----------------------------------------------------------------------
    Ogre::OverlayContainer* GGE_Profiler::createContainer() {

        Ogre::OverlayContainer* container = (Ogre::OverlayContainer*) 
			Ogre::OverlayManager::getSingleton().createOverlayElement(
				"BorderPanel", "profiler");
        container->setMetricsMode(Ogre::GMM_PIXELS);
        container->setMaterialName("Core/StatsBlockCenter");
        container->setHeight(mGuiHeight);
        container->setWidth(mGuiWidth * 2 + 15);
        container->setParameter("border_size", "1 1 1 1");
        container->setParameter("border_material", "Core/StatsBlockBorder");
        container->setParameter("border_topleft_uv", "0.0000 1.0000 0.0039 0.9961");
        container->setParameter("border_top_uv", "0.0039 1.0000 0.9961 0.9961");
        container->setParameter("border_topright_uv", "0.9961 1.0000 1.0000 0.9961");
        container->setParameter("border_left_uv","0.0000 0.9961 0.0039 0.0039");
        container->setParameter("border_right_uv","0.9961 0.9961 1.0000 0.0039");
        container->setParameter("border_bottomleft_uv","0.0000 0.0039 0.0039 0.0000");
        container->setParameter("border_bottom_uv","0.0039 0.0039 0.9961 0.0000");
        container->setParameter("border_bottomright_uv","0.9961 0.0039 1.0000 0.0000");
        container->setLeft(5);
        container->setTop(5);

        return container;

    }
    //-----------------------------------------------------------------------
    Ogre::OverlayElement* GGE_Profiler::createTextArea(const Ogre::String& name, Ogre::Real width, Ogre::Real height, Ogre::Real top, Ogre::Real left, 
                                         unsigned int fontSize, const Ogre::String& caption, bool show) {


        Ogre::OverlayElement* textArea = 
			Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea", name);
        textArea->setMetricsMode(Ogre::GMM_PIXELS);
        textArea->setWidth(width);
        textArea->setHeight(height);
        textArea->setTop(top);
        textArea->setLeft(left);
        textArea->setParameter("font_name", "BlueHighway");
        textArea->setParameter("char_height", Ogre::StringConverter::toString(fontSize));
        textArea->setCaption(caption);
        textArea->setParameter("colour_top", "1 1 1");
        textArea->setParameter("colour_bottom", "1 1 1");

        if (show) {
            textArea->show();
        }
        else {
            textArea->hide();
        }

        return textArea;

    }
    //-----------------------------------------------------------------------
    Ogre::OverlayElement* GGE_Profiler::createPanel(const Ogre::String& name, Ogre::Real width, Ogre::Real height, Ogre::Real top, Ogre::Real left, 
                                      const Ogre::String& materialName, bool show) {

        Ogre::OverlayElement* panel = 
			Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", name);
        panel->setMetricsMode(Ogre::GMM_PIXELS);
        panel->setWidth(width);
        panel->setHeight(height);
        panel->setTop(top);
        panel->setLeft(left);
        panel->setMaterialName(materialName);

        if (show) {
            panel->show();
        }
        else {
            panel->hide();
        }

        return panel;
		
    }




	//---------------------------------------------------------------
	//---------------------------------------------------------------
	//PROFILER 2 FUNCTIONS
	//---------------------------------------------------------------
	//---------------------------------------------------------------

	//Previously these functions were used for profiling individual frames
	//If not defined, GGE will throw errors due to usage in Engine::Run
	void GGE_Profiler2::ResumeProfiling() {};
	void GGE_Profiler2::PauseProfiling() {};

	//Creates the menus used for Profiling statistics
	void GGE_Profiler2::InitializeMenu()
	{
		profilerenabled = false;
		//Initialize a new timer for profiling, as some functions reset m_pTimer during their Update loops
#ifdef TBB_PARALLEL_OPTION
		m_pTimer2 = new GGETimer();
#else
		m_pTimer2 = new Ogre::Timer();
#endif
		m_pTimer2->reset();
		
		//Construct a new variable to take advantage of CPU Benchmarking
		cpumeasurer = new Instrumentation();
		//NumCounters is #CPUS+1 (CPU Total as well)
		cpucount = cpumeasurer->getNumCounters();
		//allocate CPUPercent for as many CPUCounters there will be
		CPUPercent = new double[cpucount];
		UtilitiesPtr->Add("AvgFPS", new GamePipe::DebugText("", 0.80f, 0.05f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("WorstFPS", new GamePipe::DebugText("", 0.80f, 0.07f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("BestFPS", new GamePipe::DebugText("", 0.80f, 0.09f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("RenderFPS", new GamePipe::DebugText("", 0.80f, 0.13f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("UpdateFPS", new GamePipe::DebugText("", 0.80f, 0.15f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("UpdateTotal", new GamePipe::DebugText("", 0.80f, 0.19f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("GameLogicFPS", new GamePipe::DebugText("", 0.80f, 0.21f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("InputFPS", new GamePipe::DebugText("", 0.80f, 0.23f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("HavokFPS", new GamePipe::DebugText("", 0.80f, 0.25f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("AnimationFPS", new GamePipe::DebugText("", 0.80f, 0.27f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("CPUCores", new GamePipe::DebugText("", 0.04f, 0.40f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("FPSLine", new GamePipe::DebugText("", 0.30f, 0.10f, 2.25f, Ogre::ColourValue(0, 1, 0, 1), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("TopFPS", new GamePipe::DebugText("", 0.010f, 0.12f, 2.25f, Ogre::ColourValue(0, 1, 0, 1), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("BottomFPS", new GamePipe::DebugText("", 0.015f, 0.30f, 2.25f, Ogre::ColourValue(0, 1, 0, 1), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("CPULine", new GamePipe::DebugText("", 0.25f, 0.68f, 2.25f, Ogre::ColourValue(1, 1, 1, 1), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("TopCPU", new GamePipe::DebugText("", 0.008f, 0.70f, 2.25f, Ogre::ColourValue(1, 1, 1, 1), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("BottomCPU", new GamePipe::DebugText("", 0.014f, 0.88f, 2.25f, Ogre::ColourValue(1, 1, 1, 1), Ogre::TextAreaOverlayElement::Left));

		//this starting pos value is also used in dynamically creating the menus based on # of CPUs in system 
		pos = 0.42f;
		float localPos = pos;
		for (unsigned int i = 0; i < cpucount; i++)
		{
			int j = i+1;
			if (j != cpucount)
			{
				UtilitiesPtr->Add("CPU" + Ogre::StringConverter::toString(j), new GamePipe::DebugText("", 0.04f, localPos, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
			}
			else {
				UtilitiesPtr->Add("CPUTotal", new GamePipe::DebugText("", 0.04f, localPos, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
			}
			localPos = localPos + 0.02f;
		}
		//create FPS and CPU line graph streams
		createOverlays();
		//create FPS and CPU data overlays (blue rectangles)
		LoadGraphics(); 

		debugKeyReleased = true;
	}

	void GGE_Profiler2::createOverlays()
	{
		mLineStreamFactory = new Ogre::LineStreamOverlayElementFactory();
		Ogre::OverlayManager::getSingleton().addOverlayElementFactory(mLineStreamFactory);

		debugOverlay = Ogre::OverlayManager::getSingleton().create("Core/CPUOverlay");

		//Make the material partially transparent
		Ogre::MaterialPtr backMat = Ogre::MaterialManager::getSingleton().getByName("Core/StatsBlockBorder", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		backMat->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setAlphaOperation(Ogre::LBX_MODULATE, Ogre::LBS_TEXTURE, Ogre::LBS_MANUAL, 1.0f, 0.5f);

		//create the FPS graph
		createFPSLineStreamOverlay(backMat, debugOverlay);
		//create the CPU graph 
		createCPULineStreamOverlay(backMat, debugOverlay);
	}

	//Enable/Disable Profiler when key is pressed
	void GGE_Profiler2::ProfilerHotkey()
	{
		if(InputPtr->IsKeyDown(GIS::KC_F1))
		{
			if(debugKeyReleased)
			{
				offsetX = -.15f;
				offsetY = -.18f;
				debugKeyReleased = false;
				mouseCentered = false;
				if (!profilerenabled)
					profilerenabled = true;
				else
					profilerenabled = false;
			}	
		}
		else
			debugKeyReleased = true;

		if(InputPtr->IsKeyDown(GIS::KC_F2))
		{
			unsigned int width, height, depth;
			int cornerLeft, cornerTop;
			EnginePtr->GetRenderWindow()->getMetrics(width, height, depth, cornerLeft, cornerTop);
			LPPOINT lpPoint = new POINT;
			GetCursorPos(lpPoint);
			float x = float(lpPoint->x + cornerLeft)/float(width)+offsetX;
			float y = float(lpPoint->y + cornerTop)/float(height)+offsetY;
			if(!mouseCentered)
			{
				InputPtr->CenterMouse();
				GetCursorPos(lpPoint);
				x = 0.5f;
				y = 0.5f;
				offsetX = 0.5f - float(lpPoint->x + cornerLeft)/float(width);
				offsetY = 0.5f - float(lpPoint->y + cornerTop)/float(height);
			}
			if(InputPtr->IsKeyDown(GIS::KC_1))
			{
				mouseCentered = true;
				container2->setPosition(x,y-0.03f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("CurrentFPS"))->SetPosition(x,y-0.03f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("AvgFPS"))->SetPosition(x,y);
				((GamePipe::DebugText*)UtilitiesPtr->Get("WorstFPS"))->SetPosition(x,y+0.02f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("BestFPS"))->SetPosition(x,y+0.04f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("RenderFPS"))->SetPosition(x,y+0.08f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("UpdateFPS"))->SetPosition(x,y+0.1f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("UpdateTotal"))->SetPosition(x,y+0.14f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("GameLogicFPS"))->SetPosition(x,y+0.16f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("InputFPS"))->SetPosition(x,y+0.18f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("HavokFPS"))->SetPosition(x,y+0.2f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("AnimationFPS"))->SetPosition(x,y+0.22f);
			}
			else if(InputPtr->IsKeyDown(GIS::KC_2))
			{
				mouseCentered = true;
				debugLineStreamPanel1->setPosition(x,y);
				debugLineStream1->_setPosition(x,y);
				((GamePipe::DebugText*)UtilitiesPtr->Get("TopFPS"))->SetPosition(x-0.020f,y);
				((GamePipe::DebugText*)UtilitiesPtr->Get("BottomFPS"))->SetPosition(x-0.015f,y+0.18f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("FPSLine"))->SetPosition(x+0.27f,y-0.02f);
			}
			else if(InputPtr->IsKeyDown(GIS::KC_3))
			{
				mouseCentered = true;
				((GamePipe::DebugText*)UtilitiesPtr->Get("CPUCores"))->SetPosition(x,y-0.02f);
				container->setPosition(x,y-0.02f);
				float localPos = y;
				for (unsigned int i = 0; i < cpucount; i++)
				{
					int j = i+1;
					if (j != cpucount)
					{
						((GamePipe::DebugText*)UtilitiesPtr->Get("CPU" + Ogre::StringConverter::toString(j)))->SetPosition(x,localPos);
					}
					else {
						((GamePipe::DebugText*)UtilitiesPtr->Get("CPUTotal"))->SetPosition(x,localPos);
					}
					localPos = localPos + 0.02f;
				}
				
			}
			else if(InputPtr->IsKeyDown(GIS::KC_4))
			{
				mouseCentered = true;
				debugLineStreamPanel->setPosition(x,y);
				debugLineStream->_setPosition(x,y);
				((GamePipe::DebugText*)UtilitiesPtr->Get("CPULine"))->SetPosition(x+0.22f,y-0.02f);
				((GamePipe::DebugText*)UtilitiesPtr->Get("TopCPU"))->SetPosition(x-0.02f,y);
				((GamePipe::DebugText*)UtilitiesPtr->Get("BottomCPU"))->SetPosition(x-0.013f,y+0.18f);
			}
			else if(InputPtr->IsKeyDown(GIS::KC_5))
			{	
				mouseCentered = true;
				ResetOverlayPosition();
			}
		}

	}

	void GGE_Profiler2::ResetOverlayPosition()
	{
		container2->setPosition(0.80f,0.02f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("CurrentFPS"))->SetPosition(0.80f,0.02f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("AvgFPS"))->SetPosition(0.80f,0.05f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("WorstFPS"))->SetPosition(0.80f,0.07f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("BestFPS"))->SetPosition(0.80f,0.09f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("RenderFPS"))->SetPosition(0.80f,0.13f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("UpdateFPS"))->SetPosition(0.80f,0.15f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("UpdateTotal"))->SetPosition(0.80f,0.19f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("GameLogicFPS"))->SetPosition(0.80f,0.21f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("InputFPS"))->SetPosition(0.80f,0.23f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("HavokFPS"))->SetPosition(0.80f,0.25f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("AnimationFPS"))->SetPosition(0.80f,0.27f);

		debugLineStreamPanel->setPosition(0.03f,  0.70f);
		debugLineStream->_setPosition(0.03f,  0.70f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("CPULine"))->SetPosition(0.25f, 0.68f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("TopCPU"))->SetPosition(0.008f, 0.70f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("BottomCPU"))->SetPosition(0.014f, 0.88f);
		
		debugLineStreamPanel1->setPosition(0.03f,  0.12f);
		debugLineStream1->_setPosition(0.03f,  0.12f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("TopFPS"))->SetPosition(0.010f, 0.12f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("BottomFPS"))->SetPosition(0.015f, 0.30f);
		((GamePipe::DebugText*)UtilitiesPtr->Get("FPSLine"))->SetPosition(0.30f, 0.10f);
		
		
		((GamePipe::DebugText*)UtilitiesPtr->Get("CPUCores"))->SetPosition(0.04f,0.4f);
		container->setPosition(0.04f,0.4f);
		float localPos = 0.42f;
		for (unsigned int i = 0; i < cpucount; i++)
		{
			int j = i+1;
			if (j != cpucount)
			{
				((GamePipe::DebugText*)UtilitiesPtr->Get("CPU" + Ogre::StringConverter::toString(j)))->SetPosition(0.04f,localPos);
			}
			else {
				((GamePipe::DebugText*)UtilitiesPtr->Get("CPUTotal"))->SetPosition(0.04f,localPos);
			}
			localPos = localPos + 0.02f;
		}
	}

	//to be used in the future for adding text to the overlays.  Right now just the UtilitiesPtr is being used
	void GGE_Profiler2::addOverlayLabel(Ogre::String labelName, Ogre::Real left, Ogre::Real top, Ogre::OverlayContainer * debugLineStreamPanel, Ogre::ColourValue color  )
	{
		Ogre::TextAreaOverlayElement* t = (Ogre::TextAreaOverlayElement*)Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea", labelName);
		t->setMetricsMode(Ogre::GMM_RELATIVE);
		t->setCaption(labelName);
		t->setPosition(left, top);
		t->setFontName("BlueHighway");
		t->setDimensions(0.2f, 0.5f);
		t->setCharHeight(1.5f);
		t->setColour(color);
		debugLineStreamPanel->addChild(t);
	}

	
	//creates the CPU line graph
	void GGE_Profiler2::createCPULineStreamOverlay(Ogre::MaterialPtr backMat, Ogre::Overlay* debugOverlay )
	{
		debugLineStream = (Ogre::OverlayContainer*)(Ogre::OverlayManager::getSingleton().createOverlayElement("LineStream","debugLineStream2"));
		
		//Change these to change the size and placement of the graph.  Must be between 0 and 1
		debugLineStream->_setPosition(0.03f,  0.70f);
		debugLineStream->_setDimensions(0.35f, 0.20f);
		debugLineStream->setMaterialName(backMat->getName());

		mCPULineStream = dynamic_cast<Ogre::LineStreamOverlayElement *>(debugLineStream);
		//this changes the amount of samples used
		mCPULineStream->setNumberOfSamplesForTrace(400);
		//you can add more lines to the same graph, just update the number of traces
		mCPULineStream->setNumberOfTraces(1);
		mCPULineStream->createVertexBuffer();
		mCPULineStream->setGraphScale(100.0);
		//change this to change the color of the line
		//if you add more traces above, change 0 to whatever the proper index is
		mCPULineStream->setTraceColor(0, Ogre::ColourValue::White);

		//Panel is the Ogre Entity you want to use, the second argument is the unique ogrename
		debugLineStreamPanel = (Ogre::OverlayContainer *) (Ogre::OverlayManager::getSingleton().createOverlayElement("Panel","debugLineStreamPanel"));
		//change this to change the position/size of the actual panel
		debugLineStreamPanel->_setPosition(0.03f,  0.70f);
		debugLineStreamPanel->_setDimensions(0.35f, 0.2f);
		debugLineStreamPanel->setMaterialName(backMat->getName());
		debugLineStreamPanel->hide();

		//show the panel and stream on the display
		debugOverlay->add2D(debugLineStreamPanel);
		debugOverlay->add2D(debugLineStream);
	}

	void GGE_Profiler2::createFPSLineStreamOverlay(Ogre::MaterialPtr backMat, Ogre::Overlay* debugOverlay )
	{
		debugLineStream1 = (Ogre::OverlayContainer*)(Ogre::OverlayManager::getSingleton().createOverlayElement("LineStream","debugLineStream"));
		//Change this to change size and pos of the graph
		debugLineStream1->_setPosition(0.03f,  0.12f);
		debugLineStream1->_setDimensions(0.35f, 0.20f);
		debugLineStream1->setMaterialName(backMat->getName());

		mFPSLineStream = dynamic_cast<Ogre::LineStreamOverlayElement *>(debugLineStream1);
		//change amount of samples used for the stream
		mFPSLineStream->setNumberOfSamplesForTrace(400);
		//Change amount of linse being drawn on the scene
		mFPSLineStream->setNumberOfTraces(1);
		mFPSLineStream->createVertexBuffer();
		
		mFPSLineStream->setGraphScale(60.0);
		//Changes color for the respective line
		mFPSLineStream->setTraceColor(0, Ogre::ColourValue::Green);

		debugLineStreamPanel1 = (Ogre::OverlayContainer *) (Ogre::OverlayManager::getSingleton().createOverlayElement("Panel","debugLineStreamPanel1"));
		//Change size and pos of FPS panel
		debugLineStreamPanel1->_setPosition(0.03f,  0.12f);
		debugLineStreamPanel1->_setDimensions(0.35f, 0.2f);
		debugLineStreamPanel1->setMaterialName(backMat->getName());
		debugLineStreamPanel1->hide();

		debugOverlay->add2D(debugLineStreamPanel1);
		debugOverlay->add2D(debugLineStream1);
	}
	void GGE_Profiler2::UpdateMenu()
	{
		 //For VS2010
		if (profilerenabled)
		{
			debugOverlay->show();
			cpudata->show();
			fpsdata->show();
			debugLineStreamPanel->show();
			debugLineStreamPanel1->show();
			//Set the end time of the update loop and calculate the total time
			m_fUpdateEndTime = (float)m_pTimer2->getMicroseconds();
			//Calculate the total time used by Animation
			m_fAnimationTotalTime = m_fAnimationEndTime - m_fAnimationStartTime;

			//Calculate the total time used by Physics
			m_fPhysicsTotalTime = m_fPhysicsEndTime - m_fPhysicsStartTime;
			//Subtract out Animation Time, as that's measured in the same UpdateWorld loop
			m_fPhysicsTotalTime -= m_fAnimationTotalTime;

			
			m_fUpdateTotalTime = m_fUpdateEndTime - m_fUpdateStartTime;

			float m_fDrawPercentage, m_fUpdatePercentage;
			float m_fInputPercentage, m_fPhysicsPercentage, m_fGameLogicPercentage, m_fAnimationPercentage;
			//Calculate Draw and Update as percentages of Run Time
			if (m_fRunTotalTime != 0)
			{
				m_fDrawPercentage = m_fDrawTotalTime / m_fRunTotalTime;
				m_fDrawPercentage *= 100;
				m_fUpdatePercentage = m_fUpdateTotalTime / m_fRunTotalTime;
				m_fUpdatePercentage *= 100;
			}
			//get the total number of CPU counters
			cpumeasurer->getCPUCounters(CPUPercent);
			UtilitiesPtr->GetDebugText("CPUCores")->SetText("# CPU Cores: " + Ogre::StringConverter::toString(cpumeasurer->getCPUCount()));

			//Dynamically creates the menu and stats based on amount of CPUs used.
			for (unsigned int i = 0; i < cpucount; i++)
			{
				//used to see if we're at the last position to see if we're using the CPU total or not
				int j = i+1;
				if (j != cpucount)
				{
					UtilitiesPtr->GetDebugText("CPU" + Ogre::StringConverter::toString(j))->SetText("CPU " + Ogre::StringConverter::toString(j) + ": " + Ogre::StringConverter::toString((float)CPUPercent[i]) + "%");
				}
				else {
					UtilitiesPtr->GetDebugText("CPUTotal")->SetText("CPU Total: " + Ogre::StringConverter::toString((float)CPUPercent[i]) + "%");
				}
			}

			float currentcpu = (float)CPUPercent[cpumeasurer->getCPUCount()];
			//Sets the information on the FPS Graph
			UtilitiesPtr->GetDebugText("FPSLine")->SetText("FPS: " + Ogre::StringConverter::toString(1 / EnginePtr->GetDeltaTime()));
			UtilitiesPtr->GetDebugText("TopFPS")->SetText("60");
			UtilitiesPtr->GetDebugText("BottomFPS")->SetText("0");

			//Sets the information on the CPU Graph
			UtilitiesPtr->GetDebugText("CPULine")->SetText("CPU Total: " + Ogre::StringConverter::toString(currentcpu) + "%");
			UtilitiesPtr->GetDebugText("TopCPU")->SetText("100");
			UtilitiesPtr->GetDebugText("BottomCPU")->SetText("0");

			//Start of FPS Data statistics
			UtilitiesPtr->GetDebugText("AvgFPS")->SetText("Average FPS: " + Ogre::StringConverter::toString(EnginePtr->GetRenderWindow()->getAverageFPS()));
			UtilitiesPtr->GetDebugText("WorstFPS")->SetText("Worst  FPS: " + Ogre::StringConverter::toString(EnginePtr->GetRenderWindow()->getWorstFPS()));
			UtilitiesPtr->GetDebugText("BestFPS")->SetText("Best   FPS: " + Ogre::StringConverter::toString(EnginePtr->GetRenderWindow()->getBestFPS()));
			
			//Display Render as a percentage of Total Run Time
			UtilitiesPtr->GetDebugText("RenderFPS")->SetText("Render FPS: " + Ogre::StringConverter::toString(m_fDrawPercentage) + "%");

			//Display Update as a percentage of Total Run Time
			UtilitiesPtr->GetDebugText("UpdateFPS")->SetText("Update FPS: " + Ogre::StringConverter::toString(m_fUpdatePercentage) + "%");
			
			//Calculate Update percentages
			if (m_fUpdateTotalTime != 0)
			{
				m_fInputPercentage = m_fInputTotalTime / m_fUpdateTotalTime;
				m_fInputPercentage *= 100;
				m_fPhysicsPercentage = m_fPhysicsTotalTime / m_fUpdateTotalTime;
				m_fPhysicsPercentage *= 100;
				m_fGameLogicPercentage = m_fGameLogicTotalTime / m_fUpdateTotalTime;
				m_fGameLogicPercentage *= 100;
				m_fAnimationPercentage = m_fAnimationTotalTime / m_fUpdateTotalTime;
				m_fAnimationPercentage *= 100;
			}
			//Display Input as a percentage of Update Time
			UtilitiesPtr->GetDebugText("InputFPS")->SetText("Input FPS: " + Ogre::StringConverter::toString(m_fInputPercentage) + "%");

			//Display Physics as a percentage of Update Time
			UtilitiesPtr->GetDebugText("HavokFPS")->SetText("Havok FPS: " + Ogre::StringConverter::toString(m_fPhysicsPercentage) + "%");

			//Display GameLogic as a percentage of Update Time
			UtilitiesPtr->GetDebugText("GameLogicFPS")->SetText("GameLogic FPS: " + Ogre::StringConverter::toString(m_fGameLogicPercentage) + "%");

			//Display Animation as a percentage of Update Time
			UtilitiesPtr->GetDebugText("AnimationFPS")->SetText("Animation FPS: " + Ogre::StringConverter::toString(m_fAnimationPercentage) + "%");

			//Change these values if you want to trace something different
			mFPSLineStream->setTraceValue(0, (1.0f / EnginePtr->GetDeltaTime()));
			mCPULineStream->setTraceValue(0, Ogre::Real((CPUPercent[cpumeasurer->getCPUCount()])));
		}
		else
		{
			debugOverlay->hide();
			cpudata->hide();
			fpsdata->hide();
			debugLineStreamPanel->hide();
			debugLineStreamPanel1->hide();
			UtilitiesPtr->GetDebugText("CPUCores")->SetText("");
			for (unsigned int i = 0; i < cpucount; i++)
			{
				//used to see if we're at the last position to see if we're using the CPU total or not
				int j = i+1;
				if (j != cpucount)
				{
					UtilitiesPtr->GetDebugText("CPU" + Ogre::StringConverter::toString(j))->SetText("");
				}
				else {
					UtilitiesPtr->GetDebugText("CPUTotal")->SetText("");
				}
			}

			//Sets the information on the FPS Graph
			UtilitiesPtr->GetDebugText("FPSLine")->SetText("");
			UtilitiesPtr->GetDebugText("TopFPS")->SetText("");
			UtilitiesPtr->GetDebugText("BottomFPS")->SetText("");

			//Sets the information on the CPU Graph
			UtilitiesPtr->GetDebugText("CPULine")->SetText("");
			UtilitiesPtr->GetDebugText("TopCPU")->SetText("");

			//Start of FPS Data statistics
			UtilitiesPtr->GetDebugText("AvgFPS")->SetText("");
			UtilitiesPtr->GetDebugText("WorstFPS")->SetText("");
			UtilitiesPtr->GetDebugText("BestFPS")->SetText("");
			
			//Display Render as a percentage of Total Run Time
			UtilitiesPtr->GetDebugText("RenderFPS")->SetText("");

			//Display Update as a percentage of Total Run Time
			UtilitiesPtr->GetDebugText("UpdateFPS")->SetText("");
			//Display Input as a percentage of Update Time
			UtilitiesPtr->GetDebugText("InputFPS")->SetText("");

			//Display Physics as a percentage of Update Time
			UtilitiesPtr->GetDebugText("HavokFPS")->SetText("");

			//Display GameLogic as a percentage of Update Time
			UtilitiesPtr->GetDebugText("GameLogicFPS")->SetText("");

			//Display Animation as a percentage of Update Time
			UtilitiesPtr->GetDebugText("AnimationFPS")->SetText("");
		}
	}
	

	//---------------------------------------------------------------
	//Initialize the CPU and FPS Backgrounds
	//---------------------------------------------------------------
	void GGE_Profiler2::LoadGraphics()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("Bootstrap");

		//Start of CPU Overlay
		//code used to dynamically resize the menu based on # CPUs in use
		cpucount = cpumeasurer->getNumCounters();
		//add one to take into consideration the # CPUs line
		int cpudrawcount = cpucount+1;
		container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingleton().createOverlayElement("Panel","CPUbackground");
		//GMM_RELATIVE automatically adjusts the sizing with the window
		container->setMetricsMode(Ogre::GMM_RELATIVE);
		container->setPosition(.04f,.40f);
		//Each line of text takes up 0.02 amount
		container->setDimensions(Ogre::Real(0.13), Ogre::Real(0.02*cpudrawcount));

		//The material name is in OgrePacks.zip
		container->setMaterialName("LoadingBar/Background");
		container->setParameter("border_size", "1 1 1 1");
		container->setParameter("border_material", "Core/StatsBlockBorder");
		container->setParameter("border_topleft_uv", "0.0000 1.0000 0.0039 0.9961");
		container->setParameter("border_top_uv", "0.0039 1.0000 0.9961 0.9961");
		container->setParameter("border_topright_uv", "0.9961 1.0000 1.0000 0.9961");
		container->setParameter("border_left_uv","0.0000 0.9961 0.0039 0.0039");
		container->setParameter("border_right_uv","0.9961 0.9961 1.0000 0.0039");
		container->setParameter("border_bottomleft_uv","0.0000 0.0039 0.0039 0.0000");
		container->setParameter("border_bottom_uv","0.0039 0.0039 0.9961 0.0000");
		container->setParameter("border_bottomright_uv","0.9961 0.0039 1.0000 0.0000");

		//Start of FPS Overlay
		container2 = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingleton().createOverlayElement("Panel","FPSbackground");
		container2->setMetricsMode(Ogre::GMM_RELATIVE);
		container2->setPosition(0.8f,0.02f);
		container2->setDimensions(0.17f, 0.27f);

		container2->setMaterialName("LoadingBar/Background");
		container2->setParameter("border_size", "1 1 1 1");
		container2->setParameter("border_material", "Core/StatsBlockBorder");
		container2->setParameter("border_topleft_uv", "0.0000 1.0000 0.0039 0.9961");
		container2->setParameter("border_top_uv", "0.0039 1.0000 0.9961 0.9961");
		container2->setParameter("border_topright_uv", "0.9961 1.0000 1.0000 0.9961");
		container2->setParameter("border_left_uv","0.0000 0.9961 0.0039 0.0039");
		container2->setParameter("border_right_uv","0.9961 0.9961 1.0000 0.0039");
		container2->setParameter("border_bottomleft_uv","0.0000 0.0039 0.0039 0.0000");
		container2->setParameter("border_bottom_uv","0.0039 0.0039 0.9961 0.0000");
		container2->setParameter("border_bottomright_uv","0.9961 0.0039 1.0000 0.0000");

		fpsdata = Ogre::OverlayManager::getSingleton().create("TheOverlay2");
		fpsdata->add2D(container);

		cpudata = Ogre::OverlayManager::getSingleton().create("TheOverlay3");
		cpudata->add2D(container2);

	}

	//---------------------------------------------------------------
	//---------------------------------------------------------------
	//VTUNE PROFILER FUNCTIONS
	//---------------------------------------------------------------
	//---------------------------------------------------------------
	void GGE_PROFILER_VTUNE::ResumeProfiling()
	{
		if (InputPtr->IsKeyDown(GIS::KC_F1) && ignoreF1 == 0)			//F1 is down
		{
			//Profile this frame
			startProfile=1;
			ignoreF1=1;
			//---------------------------------------------------------------
			//Necessary to prevent errors on machines without VTune Installed
			#if GGE_PROFILER3
			VTResume();
			#endif
			//---------------------------------------------------------------
		}
		else if (!InputPtr->IsKeyDown(GIS::KC_F1))						//F1 is released
		{
			//Note: F1 presses will be ignored until you release the key. That way
			//you can't hold the key down and profile multiple frames.
			ignoreF1=0;
		}

		if (InputPtr->IsKeyDown(GIS::KC_F2))							//F2 is down
		{
			//Profile this frame (you can hold down F2 and profile multiple frames)
			startProfile=1;
			//---------------------------------------------------------------
			//Necessary to prevent errors on machines without VTune Installed
			#if GGE_PROFILER3
			VTResume();
			#endif
			//---------------------------------------------------------------
			if (F2mode == 0)
			{
				//Start profiling multiple frames
				F2mode=1;
			}
		}
		else if (!InputPtr->IsKeyDown(GIS::KC_F2) && F2mode == 1)		//F2 is released
		{
			//Finished profiling multiple frames
			F2mode=0;
		}
	}

	void GGE_PROFILER_VTUNE::PauseProfiling()
	{
		if (startProfile == 1)
		{
			//Get time from resume -> pause
			startProfile=0;
			//---------------------------------------------------------------
			//Necessary to prevent errors on machines without VTune Installed
			#if GGE_PROFILER3
			VTPause();
			#endif
			//---------------------------------------------------------------
		}
	}
    //-----------------------------------------------------------------------

}
