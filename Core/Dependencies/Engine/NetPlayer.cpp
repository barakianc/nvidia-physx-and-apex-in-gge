#include "StdAfx.h"
#include "NetPlayer.h"

namespace GamePipeGame
{
	
	extern ofstream outputLog;
	const char* NetPlayer::DEFAULT_REMOTE_HOST = "127.0.0.1";

	NetPlayer::NetPlayer(bool isMP, bool isServ, CharacterProxyObject *charProx, VehicleObject *veh, GamePipe::GameScreen *gameScreen, char ip[30], int prt, bool (*EventCB)(const RakNet::Packet *const))
	{
		//Mesh name should be passed
		//vehicleObjectType should be passed
		if(vehicle != NULL)
		{
			bool isPlayer = true;
		}

		charProxy = charProx;
		vehicle = veh;
		//Call GameObject(params,isPlayer,charProxy,vehicle)
		//GameObject creates either Vehiclecar/vehiclemotorcycle/characterProxy and sets the reference back to parameter vehicle of caller function
		//check for null values before using charProxy or vechicle object .Either one of them is NULL.

		isServer = isServ;
		player_gameScreen = gameScreen;
		if(isMP && !isServer)
		{
			strcpy(serverIP, ip);
			port = prt;
		}
		printf("\n%s\n", serverIP);
		printf("\nPort: %d\n",port);
		loadConfigFile();
		initHavokNetMgr(EventCB);
	}

	void NetPlayer::update()
	{
		if (!m_pHavokNetMgr->NotStarted())
		{
			m_pHavokNetMgr->ProcessMessages();
			m_pHavokNetMgr->UpdateNetworkObjectsInSceneWithHavok(player_gameScreen->GetDefaultSceneManager(), m_entityMap, player_gameScreen->GetGameObjectManager());
			m_pHavokNetMgr->UpdateNetworkObjectsAfterHavokStep();
		}
	}

	bool NetPlayer::loadConfigFile(const char * configFileName)
	{
		std::string path = "";
		path.append("../../");
		path.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		path.append("/");
		path.append(configFileName);

		ifstream ifs (path.c_str(), ifstream::in);

		if (ifs.good())
		{
			m_configFile.load(path.c_str(), "\x09:=", true);
		}
		else
		{
			cout << "Could not find configuration file: '" << configFileName << "'" << endl;
			return false;
		}

		return true;
	}

	bool NetPlayer::initHavokNetMgr(bool (*EventHandler)(const RakNet::Packet * const))
	{
		m_pHavokNetMgr = new HavokNetManager(EventHandler);
		if (!m_pHavokNetMgr)
			return false;
		m_pHavokNetMgr->SetMessageLog(&m_messageLog);
		m_pHavokNetMgr->SetConfigFile(&m_configFile);

		if(isServer)
		{
			m_pHavokNetMgr->SetTopology(SERVER);
			m_pHavokNetMgr->CreateNetworkIDManager(true);

			Ogre::String configStrLocalSocketPort = m_configFile.getSetting("server.localport");
			Ogre::String configStrMaxConnections = m_configFile.getSetting("server.maxconnections");
			Ogre::String configStrSleepInterval = m_configFile.getSetting("server.sleepinterval");

			unsigned short configLocalSocketPort = (configStrLocalSocketPort.empty()) ? DEFAULT_SERVER_PORT : ((unsigned short)Ogre::StringConverter::parseInt(configStrLocalSocketPort));
			int configMaxConnections = (configStrMaxConnections.empty()) ? DEFAULT_MAX_CONNECTIONS : Ogre::StringConverter::parseInt(configStrMaxConnections);
			int configSleepInterval = (configStrSleepInterval.empty()) ? DEFAULT_SLEEP_INTERVAL : Ogre::StringConverter::parseInt(configStrSleepInterval);

			m_pHavokNetMgr->SetLocalSocketPort(configLocalSocketPort);
			m_pHavokNetMgr->SetMaxConnections(configMaxConnections);
			m_pHavokNetMgr->SetSleepInterval(configSleepInterval);

			m_pHavokNetMgr->Initialize();

			m_pHavokNetMgr->EnableRakNetPlugin("ReplicaManager3");

			outputLog.open("MultiplayerOnlineDemo_Server.txt");

			outputLog << "server socket port: " << configLocalSocketPort << endl;
			outputLog << "server max connections: " << configMaxConnections << endl;
			outputLog << "server sleep interval: " << configSleepInterval << endl;
		}

		else
		{
			m_pHavokNetMgr->SetTopology(CLIENT);
			m_pHavokNetMgr->CreateNetworkIDManager(false);

			Ogre::String configStrLocalSocketPort = m_configFile.getSetting("client.localport");
			Ogre::String configStrMaxConnections = m_configFile.getSetting("client.maxconnections");
			Ogre::String configStrSleepInterval = m_configFile.getSetting("client.sleepinterval");
			Ogre::String configStrRemoteHost = m_configFile.getSetting("client.remotehost");
			Ogre::String configStrRemotePort = m_configFile.getSetting("client.remoteport");
			Ogre::String configStrPassword = m_configFile.getSetting("client.password");

			unsigned short configLocalSocketPort = (configStrLocalSocketPort.empty())? DEFAULT_PORT : ((unsigned short)Ogre::StringConverter::parseInt(configStrLocalSocketPort));
			int configMaxConnections = (configStrMaxConnections.empty())? DEFAULT_MAX_CONNECTIONS : Ogre::StringConverter::parseInt(configStrMaxConnections);
			int configSleepInterval = (configStrSleepInterval.empty())? DEFAULT_SLEEP_INTERVAL : Ogre::StringConverter::parseInt(configStrSleepInterval);
			Ogre::String configRemoteHost = (configStrRemoteHost.empty())? DEFAULT_REMOTE_HOST : configStrRemoteHost;
			int configRemotePort = (configStrRemotePort.empty())? DEFAULT_SERVER_PORT : Ogre::StringConverter::parseInt(configStrRemotePort);
			Ogre::String configPassword = configStrPassword;

			m_pHavokNetMgr->SetLocalSocketPort(configLocalSocketPort);
			m_pHavokNetMgr->SetMaxConnections(configMaxConnections);
			m_pHavokNetMgr->SetSleepInterval(configSleepInterval);
			// use the explicitly provided serverIP if we have one. otherwise just use the default from the config file.
			printf("\ninit: %s\n",serverIP);
			std::string remoteHost = serverIP ? serverIP: configRemoteHost;
			m_pHavokNetMgr->SetRemoteHost(remoteHost.c_str());
			// use the explicitly provided port if we have one. otherwise just use the default from the config file.
			m_pHavokNetMgr->SetRemotePort((-1 != port) ? /*port*/configRemotePort : configRemotePort);
			m_pHavokNetMgr->SetPasswordOptions(configStrPassword.c_str(), configStrPassword.length());

			m_pHavokNetMgr->Initialize();

			m_pHavokNetMgr->EnableRakNetPlugin("ReplicaManager3");
			if(m_pHavokNetMgr->Connect() == NETMANAGER_SUCCESS)
				printf("Connected");
			else
				printf("Not Connected");
			outputLog.open("MultiplayerOnlineDemo_Client.txt");

			outputLog << "client socket port: " << configLocalSocketPort << endl;
			outputLog << "client max connections: " << configMaxConnections << endl;
			outputLog << "client sleep interval: " << configSleepInterval << endl;
			outputLog << "client remote host: " << configRemoteHost << endl;
			outputLog << "client remote port: " << configRemotePort << endl;
			outputLog << "client password: " << configPassword << endl;
		}

		return true;
	}

	void NetPlayer::addNewStaticObject(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type)
	{
		CreatePacket *packet = new CreatePacket(objectName, meshName, hkxFileName, position, type);
		packet->m_id = STATIC_OBJECT_PACKET;
		staticObjects.push_back(packet);
		printf("\n%d\n", packet->m_id);
	}

	void NetPlayer::sendStaticObjects()
	{
		RakNet::BitStream bitStream;
		for (unsigned idx = 0; idx < staticObjects.size(); idx++)
		{
			if (staticObjects[idx]->WriteToBitStream(bitStream))
			{
				if(m_pHavokNetMgr->SendPacket(bitStream)!=NETMANAGER_SUCCESS)
					printf("\nPacket Not Sent\n");
				else
					printf("\nSent\n");
				bitStream.Reset();
			}
		}
	}

	void NetPlayer::createNewObject(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type)
	{
		RakNet::BitStream bitStream;
		CreatePacket *packet = new CreatePacket(objectName, meshName, hkxFileName, position, type);
		packet->m_id = OBJECT_PACKET;
		packet->WriteToBitStream(bitStream);
		m_pHavokNetMgr->SendPacket(bitStream);
		bitStream.Reset();
	}

	void NetPlayer::updateObject(std::string objectName, Ogre::Vector3 position, Ogre::Quaternion rotation)
	{
		RakNet::BitStream bitStream;
		CreatePacket *packet = new CreatePacket(objectName, position, rotation);
		packet->m_id = UPDATE_PACKET;
		packet->WriteToBitStream(bitStream);
		m_pHavokNetMgr->SendPacket(bitStream);
		bitStream.Reset();
	}

	CreatePacket::CreatePacket()
	{
	}

	CreatePacket::CreatePacket(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type)
	{
		char buffer[1024];
		m_objectName.assign(objectName);
		m_meshName.assign(meshName);
		m_hkxFileName.assign(hkxFileName);
		m_position.x = position.x;
		m_position.y = position.y;
		m_position.z = position.z;
		m_type = type;
		std::sprintf(buffer, "'%s' '%s' '%s' %d %f %f %f", m_objectName.c_str(), m_meshName.c_str(), m_hkxFileName.c_str(), m_type, m_position.x, m_position.y, m_position.z);
		buffer[sizeof(buffer) - 1] = 0;
		m_message = buffer;
		printf("\n%s\n", m_message.c_str());
	}

	CreatePacket::CreatePacket(std::string objectName, Ogre::Vector3 position, Ogre::Quaternion rotation)
	{
		char buffer[1024];
		m_objectName.assign(objectName);
		m_position.x = position.x;
		m_position.y = position.y;
		m_position.z = position.z;
		m_rotation.x = rotation.x;
		m_rotation.y = rotation.y;
		m_rotation.z = rotation.z;
		m_rotation.w = rotation.w;
		std::sprintf(buffer, "'%s' %f %f %f %f %f %f %f", m_objectName.c_str(), m_position.x, m_position.y, m_position.z, m_rotation.x, m_rotation.y, m_rotation.z, m_rotation.w);
		m_message = buffer;
	}

	bool CreatePacket::LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket)
	{
		// if the packet only has one byte in it, it only contains the mesasge type.
		// we can't parse that.
		if (1 == rakNetPacket->length)
			return false;

		// we are not interested in the first 3 bytes of data we received.
		// ignore them.
		unsigned char buffer[1024];
		memcpy(buffer, &rakNetPacket->data[3], rakNetPacket->length - 3);
		buffer[rakNetPacket->length - 3] = 0;
		std::string msg = std::string(reinterpret_cast<const char*>(buffer));

		//-----------------------------------
		// Parse object name
		//-----------------------------------
		size_t pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_objectName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);

		//-----------------------------------
		// Parse mesh name
		//-----------------------------------
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_meshName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);

		//-----------------------------------
		// Parse hkx file name
		//-----------------------------------
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_hkxFileName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);

		// the hkx file name is followed by a space. get rid of it.
		if (msg[0] == ' ')
			msg = msg.substr(1);

		//-----------------------------------
		// Parse object type
		//-----------------------------------
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string objType = msg.substr(0, pos);
		m_type = (GameObjectType)atoi(objType.c_str());
		msg = msg.substr(pos + 1);

		//-----------------------------------
		// Parse object position
		//-----------------------------------
		// x coord
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string xCoord = msg.substr(0, pos);
		m_position.x = (float)atof(xCoord.c_str());
		msg = msg.substr(pos + 1);
		
		// y coord
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string yCoord = msg.substr(0, pos);
		m_position.y = (float)atof(yCoord.c_str());
		msg = msg.substr(pos + 1);

		// z coord
		// there should be no more spaces at this point
		pos = msg.find(" ");
		if (string::npos != pos)
			return false;
		std::string zCoord = msg;
		m_position.z = (float)atof(zCoord.c_str());

		return true;
	}

	bool CreatePacket::LoadFromRakNetPacket2(const RakNet::Packet* const rakNetPacket)
	{
		// if the packet only has one byte in it, it only contains the mesasge type.
		// we can't parse that.
		if (1 == rakNetPacket->length)
			return false;

		// we are not interested in the first 3 bytes of data we received.
		// ignore them.
		unsigned char buffer[1024];
		memcpy(buffer, &rakNetPacket->data[3], rakNetPacket->length - 3);
		buffer[rakNetPacket->length - 3] = 0;
		std::string msg = std::string(reinterpret_cast<const char*>(buffer));

		//-----------------------------------
		// Parse object name
		//-----------------------------------
		size_t pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_objectName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);
		printf("\n%s\n", m_objectName.c_str());

		if(msg[0] == ' ')
			msg = msg.substr(1);
		pos = msg.find(" ");
		if(string::npos == pos)
			return false;
		std::string xCoord = msg.substr(0, pos);
		m_position.x = (float)atof(xCoord.c_str());
		msg = msg.substr(pos + 1);
		printf("%f\n", m_position.x);
		// y coord
		pos = msg.find(" ");
		if(string::npos == pos)
			return false;
		std::string yCoord = msg.substr(0, pos);
		m_position.y = (float)atof(yCoord.c_str());
		msg = msg.substr(pos + 1);
		printf("%f\n", m_position.y);
		// z coord
		// there should be no more spaces at this point
		pos = msg.find(" ");
		if(string::npos == pos)
			return false;
		std::string zCoord = msg;
		m_position.z = (float)atof(zCoord.c_str());
		msg = msg.substr(pos + 1);
		printf("%f\n", m_position.z);

		pos = msg.find(" ");
		if(string::npos == pos)
			return false;
		std::string xCoordR = msg.substr(0, pos);
		m_rotation.x = (float)atof(xCoordR.c_str());
		msg = msg.substr(pos + 1);
		printf("%f\n", m_rotation.x);
		// y coord
		pos = msg.find(" ");
		if(string::npos == pos)
			return false;
		std::string yCoordR = msg.substr(0, pos);
		m_rotation.y = (float)atof(yCoordR.c_str());
		msg = msg.substr(pos + 1);
		printf("%f\n", m_rotation.y);
		// z coord
		// there should be no more spaces at this point
		pos = msg.find(" ");
		if(string::npos == pos)
			return false;
		std::string zCoordR = msg;
		m_rotation.z = (float)atof(zCoordR.c_str());
		msg = msg.substr(pos + 1);
		printf("%f\n", m_rotation.z);

		pos = msg.find(" ");
		if(string::npos != pos)
			return false;
		std::string wCoordR = msg;
		m_rotation.w = (float)atof(wCoordR.c_str());
		printf("%f\n", m_rotation.w);

		return true;
	}

	bool CreatePacket::WriteToBitStream(RakNet::BitStream& bitStream) const
	{
		printf("\nPacket: %d %s\n",m_id,m_message.c_str());
		bitStream.Write((unsigned char)m_id);
		if (m_message.length() > 0)
			bitStream.Write(m_message.c_str());
		return true;
	}

}