#include "StdAfx.h"


#ifdef PHYSX
#ifdef PHYSX_APEX
#include "GGEApexRenderResourceManager.h"
#include "GGEApexRenderResources.h"

#include <NxApexRenderContext.h>
#include <NxUserRenderIndexBufferDesc.h>
#include <NxUserRenderInstanceBuffer.h>
#include <NxUserRenderResourceDesc.h>
#include <NxUserRenderBoneBufferDesc.h>
#include <NxUserRenderSpriteBufferDesc.h>
#include <NxUserRenderSurfaceBufferDesc.h>
#include <NxUserRenderVertexBufferDesc.h>

GGEApexResourceManager::GGEApexResourceManager(){
	m_numVertexBuffers = 0;
	m_numIndexBuffers = 0;
	m_numInstanceBuffers = 0;
	m_numSurfaceBuffers = 0;
	m_numBoneBuffers = 0;
	m_numSpriteBuffers = 0;
	m_numResources = 0;
	m_nameCount = 0;
}

GGEApexResourceManager::~GGEApexResourceManager(){
	
}

physx::apex::NxUserRenderVertexBuffer* GGEApexResourceManager::createVertexBuffer(const physx::apex::NxUserRenderVertexBufferDesc& desc){
	PX_ASSERT(desc.isValid());
	if(!desc.isValid()){

		return NULL;
	}
	else{
		GGEApexRendererVertexBuffer* vb = NULL;

		unsigned int numSemantics = 0;

		for(unsigned int i = 0; i < physx::apex::NxRenderVertexSemantic::NUM_SEMANTICS; i++){
			numSemantics += desc.buffersRequest[i] != physx::apex::NxRenderDataFormat::UNSPECIFIED ? 1 : 0;
		}

		if(numSemantics > 0){
			vb = new GGEApexRendererVertexBuffer(desc);
			m_numVertexBuffers++;
		}

		return vb;
	}
}

physx::apex::NxUserRenderIndexBuffer*		GGEApexResourceManager::createIndexBuffer(const physx::apex::NxUserRenderIndexBufferDesc& desc){
	GGEApexRendererIndexBuffer* ib = NULL;
	PX_ASSERT(desc.isValid());

	if(desc.isValid()){
		ib = new GGEApexRendererIndexBuffer(desc);
		m_numIndexBuffers++;
		return ib;
	}
	return NULL;
}


physx::apex::NxUserRenderSurfaceBuffer*		GGEApexResourceManager::createSurfaceBuffer(const physx::apex::NxUserRenderSurfaceBufferDesc& desc){
	PX_ASSERT(desc.isValid());
	physx::apex::NxUserRenderSurfaceBuffer* sb = NULL;
	m_numSurfaceBuffers++;
	return sb;
}

physx::apex::NxUserRenderInstanceBuffer*	GGEApexResourceManager::createInstanceBuffer(const physx::apex::NxUserRenderInstanceBufferDesc& desc){
	PX_ASSERT(desc.isValid());
	physx::apex::NxUserRenderInstanceBuffer* ib = NULL;
	m_numInstanceBuffers++;
	return ib;
}

physx::apex::NxUserRenderSpriteBuffer*		GGEApexResourceManager::createSpriteBuffer(const physx::apex::NxUserRenderSpriteBufferDesc& desc){
	PX_ASSERT(desc.isValid());
	physx::apex::NxUserRenderSpriteBuffer* sb = NULL;
	m_numSpriteBuffers++;
	return sb;
}

physx::apex::NxUserRenderResource*			GGEApexResourceManager::createResource(const physx::apex::NxUserRenderResourceDesc& desc){
	PX_ASSERT(desc.isValid());
	GGEApexRendererMesh* mesh = NULL;
	if(desc.isValid()){
		char buffer[32];
		std::string meshName = "ApexOgreMesh";
		itoa(m_nameCount,buffer,10);
		meshName.append((std::string)buffer);
		std::string Entityname = "ApexOgreEntity";
		Entityname.append((std::string)buffer);
		m_nameCount++;
		//create a new Ogre entity for the newly created mesh
		mesh = new GGEApexRendererMesh(desc,meshName,Entityname);
		m_numResources++;
	}
	return mesh;
}

physx::apex::NxUserRenderBoneBuffer*		GGEApexResourceManager::createBoneBuffer(const physx::apex::NxUserRenderBoneBufferDesc& desc){
	PX_ASSERT(desc.isValid());
	physx::apex::NxUserRenderBoneBuffer* bb = new GGEApexRendererBoneBuffer(desc);
	m_numBoneBuffers++;
	return bb;
}

physx::PxU32								GGEApexResourceManager::getMaxBonesForMaterial(void* material){
	return 0;
}

void										GGEApexResourceManager::releaseVertexBuffer(physx::apex::NxUserRenderVertexBuffer& buffer){
	PX_ASSERT(m_numVertexBuffers > 0);
	m_numVertexBuffers--;
	delete &buffer;

}

void										GGEApexResourceManager::releaseIndexBuffer(physx::apex::NxUserRenderIndexBuffer& buffer){
	PX_ASSERT(m_numIndexBuffers > 0);
	m_numIndexBuffers--;
	delete &buffer;

}

void										GGEApexResourceManager::releaseSurfaceBuffer(physx::apex::NxUserRenderSurfaceBuffer& buffer){
	PX_ASSERT(m_numSurfaceBuffers > 0);
	m_numSurfaceBuffers--;
	delete &buffer;

}

void										GGEApexResourceManager::releaseBoneBuffer(physx::apex::NxUserRenderBoneBuffer& buffer){
	PX_ASSERT(m_numBoneBuffers > 0);
	m_numBoneBuffers--;
	delete &buffer;
}

void										GGEApexResourceManager::releaseInstanceBuffer(physx::apex::NxUserRenderInstanceBuffer& buffer){
	PX_ASSERT(m_numInstanceBuffers > 0);
	m_numInstanceBuffers--;
	delete &buffer;

}

void										GGEApexResourceManager::releaseSpriteBuffer(physx::apex::NxUserRenderSpriteBuffer& buffer){
	PX_ASSERT(m_numSpriteBuffers > 0);
	m_numSpriteBuffers--;
	delete &buffer;

}

void										GGEApexResourceManager::releaseResource(physx::apex::NxUserRenderResource& resource){
	PX_ASSERT(m_numResources > 0);
	m_numResources--;
	delete &resource;

}

#endif
#endif