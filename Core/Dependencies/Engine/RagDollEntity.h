
#ifndef RAGDOLL_ENTITY_H
#define RAGDOLL_ENTITY_H

/// Include files for Math and Base
#include <Common/Base/hkBase.h>
#include <Common/Base/Container/String/hkString.h>
#include <Common/Base/Container/String/hkStringBuf.h>

/// Include files for Skeletal Animation
#include <Animation/Animation/Playback/hkaAnimatedSkeleton.h>
#include <Animation/Animation/hkaAnimationContainer.h>
#include <Animation/Animation/Playback/Control/Default/hkaDefaultAnimationControl.h>

// FALL2010 Include files for loading hkx files
#include <Common/Serialize/Util/hkRootLevelContainer.h>
#include <Common/Serialize/Resource/hkResource.h>
#include <Common/Serialize/Util/hkLoader.h>
#include <Common/Serialize/Util/hkSerializeUtil.h>

// FALL2010 Include files for hkx file serialization
#include <Common/Serialize/Packfile/hkPackfileData.h>
#include <Common/Compat/Deprecated/Packfile/hkPackfileReader.h>
#include <Physics/Utilities/Serialize/hkpHavokSnapshot.h>

#include <Animation/Animation/Rig/hkaPose.h>

#include <Animation/Ragdoll/Instance/hkaRagdollInstance.h>

#include <Animation/Ragdoll/Controller/RigidBody/hkaRagdollRigidBodyController.h>
#include <Physics/Dynamics/Phantom/hkpAabbPhantom.h>

#include <Physics/Utilities/Dynamics/Inertia/hkpInertiaTensorComputer.h>
#include <Animation/Animation/Mapper/hkaSkeletonMapper.h>
#include <Animation/Ragdoll/Utils/hkaRagdollUtils.h>

#include "GameObjectManager.h"

class RagdollObject
{

public:
	/// This object sets the properties of a ragdoll.
	hkaRagdollInstance* m_ragdoll;
	///This is a container for the simulation's physical objects.
#ifdef HAVOK
	hkpWorld* m_physicsWorld;
#endif
#ifdef PHYSX

#endif
	/// This object is used to drive a ragdoll to a given animation pose. 
	hkaRagdollRigidBodyController* m_ragdollRigidBodyController;

	/// hkaSkeleton object used as low res skeleton
	const hkaSkeleton* m_ragdollSkeleton;
	/// hkaSkeleton object used as high res skeleton
	const hkaSkeleton* m_animationSkeleton;
	/// animated skeleton -> not used though
	hkaAnimatedSkeleton* m_character;
	/// Used for low bone to high bone mapping
	hkaSkeletonMapper* m_ragdollFromAnimation;
	/// Used for high to low mapping 
	hkaSkeletonMapper* m_animationFromRagdoll;
	///Describes how an animation is bound to a skeleton
	hkaAnimationBinding* m_binding;
	///This object represents a simple animation control that 
	///provides basic support for varying playback speed, fading and looping
	hkaDefaultAnimationControl* m_control;

	hkpAabbPhantom* m_ragdollPhantom;

	hkQsTransform m_worldFromModel;

public:
	/// Constructor for creation of rigid bodies given file path of havok ragdoll
	RagdollObject(const char* hkx_Path, hkReal scale); 
	~RagdollObject();
	/// Drive pose every frame
	void DrivePose(float f_fTimeStep);
	// constructor for creation of rigid bodies from havok content tools
	//hkaPose* GetPose();

};

#endif // RAGDOLL_ENTITY_H