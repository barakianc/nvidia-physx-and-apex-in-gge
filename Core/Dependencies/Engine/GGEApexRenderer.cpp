#include "StdAfx.h"

#include "Ogre.h"

#ifdef PHYSX
#ifdef PHYSX_APEX

#include "GGEApexRenderer.h"
#include "GGEApexRenderResources.h"

GGEApexRenderer::GGEApexRenderer(){

}

GGEApexRenderer::~GGEApexRenderer(){

}

void GGEApexRenderer::renderResource(const physx::apex::NxApexRenderContext& context){
	if(context.renderResource){
		static_cast<GGEApexRendererMesh*>(context.renderResource)->renderToOgreMesh(m_materialName);
	}

}


#endif // PHYSX_APEX
#endif // PHYSX