#include "StdAfx.h"
// Input
#include "Input.h"

// Engine
#include "Engine.h"

#include "Editor.h"
#include "GISMoveMe.h"


namespace GamePipe
{
	//////////////////////////////////////////////////////////////////////////
	// Input()
	//////////////////////////////////////////////////////////////////////////
	Input::Input()
	{
		m_pInputManager = NULL;
		m_pKeyboard = NULL;
		m_pMouse = NULL;
		m_pRemote = NULL;
		m_pXPad = NULL;

		m_MouseState.clear();
		m_WiiRemoteState.clear();
		m_XPadState.clear();

#ifdef GLE_EDITOR
		// Editor cameras
		m_fRadius				= 40.0;
		m_fYawValue				= 0.0;
		m_fPitchValue			= -Ogre::Math::PI;
		// Control editor cameras
		m_bKeyAlt				= false;
		m_bMouseLeft			= false;
		m_bMouseMiddle			= false;
		m_bMouseMoved			= false;
#endif

	}

	//////////////////////////////////////////////////////////////////////////
	// ~InputPtr()
	//////////////////////////////////////////////////////////////////////////
	Input::~Input()
	{
		Destroy();
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	int Input::Initialize()
	{
		if (!EnginePtr->GetRenderer()->isInitialised())
		{
			GGETRACELOG("Expecting initialized Ogre.");
			return E_FAIL;
		}

#if 1
		GIS::ParamList paramList;
		std::ostringstream windowHandleString;
		windowHandleString << reinterpret_cast<size_t>(EnginePtr->GetWindowHandle());
		paramList.insert(std::make_pair(std::string("WINDOW"), windowHandleString.str()));
		paramList.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
		paramList.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
		paramList.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
		paramList.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
		m_pInputManager = GIS::InputManager::createInputSystem(paramList);
#else
		m_pInputManager = GIS::InputManager::createInputSystem(reinterpret_cast<size_t>(EnginePtr->GetWindowHandle()));
#endif

		try
		{
			m_pKeyboard = static_cast<GIS::Keyboard*>(m_pInputManager->createInputObject(GIS::GISKeyboard, true));
		}
		catch (...)
		{
			GGETRACELOG("GIS Error on keyboard creation.\n");
		}

		try
		{
			m_pMouse = static_cast<GIS::Mouse*>(m_pInputManager->createInputObject(GIS::GISMouse, true));
		}
		catch (...)
		{
			GGETRACELOG("GIS Error on mouse creation.\n");
		}

		try
		{
			m_pRemote = static_cast<GIS::WiiRemote*>(m_pInputManager->createInputObject(GIS::GISWiiRemote, true));
			m_pRemote->Connect();
		}
		catch (...)
		{
			GGETRACELOG("GIS Error on Wii Remote creation.\n");
		}

		try
		{
			m_pXPad = static_cast<GIS::XPad*>(m_pInputManager->createInputObject(GIS::GISXPad, true));
		}
		catch (...)
		{
			GGETRACELOG("GIS Error on XBox 360 Pad creation.\n");
		}

		try
		{
			m_pKinect = static_cast<GIS::Kinect*>(m_pInputManager->createInputObject(GIS::GISKinect, true));
		}
		catch (...)
		{
			GGETRACELOG("GIS Error on Kinect creation.\n");
		}

		try
		{
			m_pMoveMe = static_cast<GIS::MoveMe*>(m_pInputManager->createInputObject(GIS::GISMoveMe, true));
			//set connection here temporarily - this is a hardcoded number that must be fixed to get input to work
			//m_pMoveMe->moveMeConnect("204.140.159.1", "7899");
			m_pMoveMe->moveMeConnect("0.0.0.0", "7899");
		}
		catch (...)
		{
			GGETRACELOG("GIS Error on MoveMe creation.\n");
		}


		unsigned int uiWidth, uiHeight, uiDepth;
		int iTop, iLeft;
		EnginePtr->GetRenderWindow()->getMetrics(uiWidth, uiHeight, uiDepth, iLeft, iTop);
		SetMouseArea(uiWidth, uiHeight);

		SetEventCallbacks();

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	int Input::Destroy()
	{
		if (m_pInputManager)
		{
			m_pInputManager->destroyInputObject(m_pKeyboard);
			m_pInputManager->destroyInputObject(m_pMouse);
			m_pInputManager->destroyInputObject(m_pRemote);
			m_pInputManager->destroyInputObject(m_pXPad);
			m_pInputManager->destroyInputObject(m_pKinect);
			m_pInputManager->destroyInputObject(m_pMoveMe); //may cause a crash!?
			//GIS::InputManager::destroyInputSystem(m_pInputManager);

			m_pInputManager = NULL;
			m_pKeyboard = NULL;
			m_pMouse = NULL;
			m_pRemote = NULL;
			m_pXPad = NULL;
			m_pKinect = NULL;
			m_pMoveMe = NULL;
		}

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// SetMouseArea(unsigned int uiWidth, unsigned int uiHeight)
	//////////////////////////////////////////////////////////////////////////	
	void Input::SetMouseArea(unsigned int uiWidth, unsigned int uiHeight)
	{
		if (m_pMouse)
		{
			const OIS::MouseState& mouseState = m_pMouse->getMouseState();
			//m_pMouse->mState.width = uiWidth;
			//m_pMouse->mState.height = uiHeight;
			mouseState.width = uiWidth;
			mouseState.height = uiHeight;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// CenterMouse()
	//////////////////////////////////////////////////////////////////////////	
	void Input::CenterMouse()
	{
		unsigned int uiWidth, uiHeight, uiDepth;
		int iLeft, iTop;
		
		EnginePtr->GetRenderWindow()->getMetrics(uiWidth, uiHeight, uiDepth, iLeft, iTop);
		SetCursorPos(iLeft + (uiWidth / 2), iTop + (uiHeight / 2));
	}

	//////////////////////////////////////////////////////////////////////////
	// SetEventCallbacks()
	//////////////////////////////////////////////////////////////////////////
	void Input::SetEventCallbacks()
	{
		if (m_pInputManager->getNumberOfDevices(GIS::GISKeyboard) > 0)
		{
			if (m_pKeyboard != NULL)
			{
				m_pKeyboard->setEventCallback(this);
			}
		}

		if (m_pInputManager->getNumberOfDevices(GIS::GISMouse) > 0)
		{
			if (m_pMouse != NULL)
			{
				m_pMouse->setEventCallback(this);
			}
		}

		if (m_pInputManager->getNumberOfDevices(GIS::GISWiiRemote) > 0)
		{
			if (m_pRemote != NULL)
			{
				m_pRemote->setEventCallback(this);
			}
		}
		if (m_pInputManager->getNumberOfDevices(GIS::GISXPad) > 0)
		{
			if (m_pXPad != NULL)
			{
				m_pXPad->setEventCallback(this);
			}
		}
		if (m_pInputManager->getNumberOfDevices(GIS::GISKinect) > 0)
		{
			if (m_pKinect != NULL)
			{
				m_pKinect->setEventCallback(this);
			}
		}
		if (m_pInputManager->getNumberOfDevices(GIS::GISMoveMe) > 0){
			if (m_pMoveMe != NULL)
			{
				m_pMoveMe->setEventCallback(this);
			}

		}

	}

	//////////////////////////////////////////////////////////////////////////
	// Capture()
	//////////////////////////////////////////////////////////////////////////
	void Input::Capture()
	{
		if (m_pInputManager->getNumberOfDevices(GIS::GISKeyboard) > 0)
		{
			if (m_pKeyboard != NULL)
			{
				m_pKeyboard->capture();

			}
		}

		if (m_pInputManager->getNumberOfDevices(GIS::GISMouse) > 0)
		{
			if (m_pMouse != NULL)
			{
				m_pMouse->capture();

			}
		}

		if (m_pInputManager->getNumberOfDevices(GIS::GISWiiRemote) > 0)
		{
			if (m_pRemote != NULL)
			{
				m_pRemote->capture();

			}
		}
		
		if (m_pInputManager->getNumberOfDevices(GIS::GISXPad) > 0)
		{
			if (m_pXPad != NULL)
			{
				m_pXPad->capture();

			}
		}
		if (m_pInputManager->getNumberOfDevices(GIS::GISKinect) > 0)
		{
			if (m_pKinect != NULL)
			{
				m_pKinect->capture();

			}
		}

		if (m_pInputManager->getNumberOfDevices(GIS::GISMoveMe) > 0)
		{
			if(m_pMoveMe != NULL)
			{
				m_pMoveMe->capture();
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	void Input::Update()
	{
		if (m_pInputManager)
		{
			m_MouseState.clear();
			m_WiiRemoteState.clear();
			m_XPadState.clear();
			Capture();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// IsKeyDown(GIS::KeyCode keyCode)
	//////////////////////////////////////////////////////////////////////////
	bool Input::IsKeyDown(GIS::KeyCode keyCode)
	{
#ifdef GLE_EDITOR

		if (EnginePtr->GetEditor()->UseEditorInput())
			return EnginePtr->GetEditor()->Editor_GetKeyStateBuffer(keyCode);

#endif
		if (m_pInputManager->getNumberOfDevices(GIS::GISKeyboard) > 0)
		{
			if (m_pKeyboard != NULL)
			{						
				return m_pKeyboard->isKeyDown(keyCode);
			}
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	// IsMouseButtonDown(GIS::MouseButtonID mouseButtonId)
	//////////////////////////////////////////////////////////////////////////
	bool Input::IsMouseButtonDown(OIS::MouseButtonID mouseButtonId)
	{
		if (m_pInputManager->getNumberOfDevices(GIS::GISMouse) > 0)
		{
			if (m_pMouse != NULL)
			{
				return m_pMouse->getMouseState().buttonDown(mouseButtonId);
			}
		}

		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	// IsWRButtonDown(GIS::WiiButtonID wrButtonId)
	//////////////////////////////////////////////////////////////////////////
	bool Input::IsWRButtonDown(GIS::WiiButtonID wrButtonId){
		if (m_pInputManager->getNumberOfDevices(GIS::GISWiiRemote) > 0)
		{
			if (m_pRemote != NULL)
			{
				return m_pRemote->getWiiRemoteState().wrButtonsDown(wrButtonId);
			}
		}

		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	// IsXPButtonDown(GIS::XPadButtonID xpButtonId)
	//////////////////////////////////////////////////////////////////////////
	bool Input::IsXPButtonDown(GIS::XPadButtonID xpButtonId){
		if (m_pInputManager->getNumberOfDevices(GIS::GISXPad) > 0)
		{
			if (m_pXPad != NULL)
			{
				return m_pXPad->getXPadState().xpButtonsDown(xpButtonId);
			}
		}

		return false;

	}

	//////////////////////////////////////////////////////////////////////////
	// GetMouseState()
	//////////////////////////////////////////////////////////////////////////
	GIS::MouseState Input::GetMouseState()
	{
		// HACK commented out code
		/*
#ifdef GLE_EDITOR
		if (EnginePtr->GetEditor()->IsEditorMode())
		{
			m_MouseState.clear();
			
			m_MouseState.X.absOnly = false;
			m_MouseState.X.abs = 0;
			m_MouseState.X.rel = 0;
			
			m_MouseState.Y.absOnly = false;
			m_MouseState.Y.abs = 0;
			m_MouseState.Y.rel = 0;

			m_MouseState.buttons = 0;			

		}  
#endif*/
		return m_MouseState;
	}

	//////////////////////////////////////////////////////////////////////////
	// GetWiiRemoteState()
	//////////////////////////////////////////////////////////////////////////
	GIS::WiiRemoteState Input::GetWiiRemoteState(){
		return m_WiiRemoteState;	
	}
	//////////////////////////////////////////////////////////////////////////
	// GetXPadState()
	//////////////////////////////////////////////////////////////////////////
	GIS::XPadState Input::GetXPadState(){
		return m_XPadState;
	}
	//////////////////////////////////////////////////////////////////////////
	// keyPressed(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool Input::keyPressed(const GIS::KeyEvent& keyEvent)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
#ifdef GLE_EDITOR
			if( EnginePtr->GetEditor()->IsEditorMode() )
			{
				switch(keyEvent.key)
				{
				case GIS::KC_LMENU:
				case GIS::KC_RMENU:
					{
						m_bKeyAlt = true;
						break;
					}
				}
			}else
#endif			
			{
                bool visible = OgreConsole::getSingletonPtr()->isVisible();

                switch(keyEvent.key){
                    case GIS::KC_GRAVE:
                    case GIS::KC_KANJI:
                        {
                            OgreConsole::getSingletonPtr()->setVisible(!visible);
                            break;
                        }
                }
                if ((keyEvent.key != GIS::KC_KANJI && keyEvent.key != GIS::KC_GRAVE) && OgreConsole::getSingletonPtr()->isVisible()){
                    OgreConsole::getSingletonPtr()->onKeyPressed(keyEvent);
                }
                else{
				    EnginePtr->GetForemostGameScreen(true)->OnKeyPress(keyEvent);
                }
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// keyReleased(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool Input::keyReleased(const GIS::KeyEvent& keyEvent)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
#ifdef GLE_EDITOR
			if( EnginePtr->GetEditor()->IsEditorMode() )
			{
				switch(keyEvent.key)
				{
				case GIS::KC_LMENU:
				case GIS::KC_RMENU:
					{
						m_bKeyAlt = false;
						break;
					}
				}
			}else
#endif			
			{
				EnginePtr->GetForemostGameScreen(true)->OnKeyRelease(keyEvent);
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// mouseMoved(const GIS::MouseEvent& mouseEvent)
	//////////////////////////////////////////////////////////////////////////
	bool Input::mouseMoved(const GIS::MouseEvent& mouseEvent)
	{
		// Inject mouse move to the FlashGUI manager
		FlashGUIPtr->FlashGUIManager->injectMouseMove(mouseEvent.state.X.abs, mouseEvent.state.Y.abs) || FlashGUIPtr->FlashGUIManager->injectMouseWheel(mouseEvent.state.Z.rel);
		m_MouseState = mouseEvent.state;

		if (EnginePtr->GetForemostGameScreen(true))
		{
#ifdef GLE_EDITOR
			if( EnginePtr->GetEditor()->IsEditorMode() )
			{
				GameScreen   * pGameScreen		= EnginePtr->GetForemostGameScreen(true);
				Editor       * pEditor          = EnginePtr->GetEditor();
				Ogre::Camera * pCamera			= pGameScreen->GetActiveCamera();
				Ogre::Vector3 v3RotationPoint	= pGameScreen->GetActiveCameraSceneNode()->getPosition();
				Ogre::Vector3 v3CameraPosition	= pCamera->getPosition();

				// Apply the modifications
				if(pCamera->getName() == "Editor_CameraPerspective")
				{
					if(m_bMouseMiddle && m_bKeyAlt )
					{
						v3RotationPoint -=  pCamera->getOrientation().xAxis() * pEditor->m_fCameraMovementSpeed * (Ogre::Real)mouseEvent.state.X.rel;
						v3RotationPoint +=  pCamera->getOrientation().yAxis() * pEditor->m_fCameraMovementSpeed * (Ogre::Real)mouseEvent.state.Y.rel;
					}

					if(m_bMouseLeft && m_bKeyAlt )
					{	
						m_fYawValue		-= pEditor->m_fCameraRotationSpeed * mouseEvent.state.X.rel;
						m_fPitchValue	-= pEditor->m_fCameraRotationSpeed * mouseEvent.state.Y.rel;
					}

					m_fRadius = pCamera->getPosition().length() - pEditor->m_fCameraMovementSpeed*mouseEvent.state.Z.rel;
					
					if(m_fRadius<0.0) 
						m_fRadius = (Ogre::Real)0.1;

					pGameScreen->GetActiveCameraSceneNode()->setPosition	(v3RotationPoint);
					pCamera->setPosition		(0.0,0.0,0.0);
					pCamera->setOrientation		(Ogre::Quaternion::IDENTITY);
					pCamera->yaw				(Ogre::Degree(m_fYawValue));
					pCamera->pitch				(Ogre::Degree(m_fPitchValue));
					pCamera->lookAt				(v3RotationPoint);
					pCamera->moveRelative		(Ogre::Vector3(0.0,0.0, m_fRadius));
					
				}
				else if (	pCamera->getName()		== "Editor_CameraSide"	|| 
							pCamera->getName()		== "Editor_CameraTop"	|| 
							pCamera->getName()		== "Editor_CameraFront"	)
				{
					// Calculate the position
					if(m_bMouseMiddle && m_bKeyAlt )	
					{
						v3CameraPosition.x -=  pEditor->m_fCameraMovementSpeed * mouseEvent.state.X.rel;
						v3CameraPosition.y +=  pEditor->m_fCameraMovementSpeed * mouseEvent.state.Y.rel;
					}

					// Calculate the zoom
					int iWidth	=	(int)pCamera->getOrthoWindowWidth();
					int iHeight	=	(int)pCamera->getOrthoWindowHeight();
					int deltaWidth  = 5 * (int)pEditor->m_fCameraMovementSpeed * mouseEvent.state.Z.rel;
					int deltaHeight = 5 * (int)pEditor->m_fCameraMovementSpeed * mouseEvent.state.Z.rel;

					if (deltaWidth < 1 && deltaWidth > 0)
						deltaWidth = 1;
					if (deltaWidth > -1 && deltaWidth < 0)
						deltaWidth = -1;

					if (deltaHeight < 1 && deltaHeight > 0)
						deltaHeight = 1;
					if (deltaHeight > -1 && deltaHeight < 0) //second deltaHeight was width before. Typo? -11/2/09
						deltaHeight = -1;

					
					iWidth		-=	deltaWidth;
					iHeight		-=	deltaHeight;

					//GGETRACELOG("iWidth %i  iHeight %i  camx %f  camy %f",iWidth,iHeight, v3CameraPosition.x, v3CameraPosition.y); //Debugline for value tracking
					if(iHeight < 5 || iWidth < 5) {
						//The camera gets stuck at iWidth = 7 iHeight = 4 for unknown reason. As a temp fix I'm not allowing it to zoom that far. -11/2/09
					}
					else {

						//if(iWidth < 1) iWidth = 1; //redundant atm
						//if(iHeight< 1) iHeight= 1; //redundant atm

						// Zoom and position
						pCamera->setOrthoWindowWidth( (Ogre::Real)iWidth );
						pCamera->setOrthoWindowHeight( (Ogre::Real)iHeight );
						pCamera->setPosition(v3CameraPosition);
					}
				}

				// In order to detect if it was just a click or if the user want to rotate or something we do this
				if( mouseEvent.state.X.rel + mouseEvent.state.Y.rel > 10)
					m_bMouseMoved		= true;

			}else
#endif			
			{
				EnginePtr->GetForemostGameScreen(true)->OnMouseMove(mouseEvent);
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// mousePressed(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	//////////////////////////////////////////////////////////////////////////
	bool Input::mousePressed(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		// Inject mouse pressed to the FlashGUI manager
		FlashGUIPtr->FlashGUIManager->injectMouseDown(mouseButtonId);
		m_MouseState = mouseEvent.state;

		if (EnginePtr->GetForemostGameScreen(true))
		{
#ifdef GLE_EDITOR
			if( EnginePtr->GetEditor()->IsEditorMode() )
			{
				if		( mouseButtonId == GIS::MB_Left		)	
				{
					// No picking in the model viewer
#ifndef GLE_EDITOR_MODELVIEW
					// If the mouse has not been moved ...
					if( !m_bMouseMoved )
					{
						EnginePtr->GetEditor()->Picking(mouseEvent);
						m_bMouseMoved = true;
					}
#endif
						m_bMouseLeft	= true;
				}
				else if ( mouseButtonId == GIS::MB_Middle	)	m_bMouseMiddle	= true;
			}else
#endif
			{
				EnginePtr->GetForemostGameScreen(true)->OnMousePress(mouseEvent, mouseButtonId);
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// mouseReleased(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	//////////////////////////////////////////////////////////////////////////
	bool Input::mouseReleased(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		// Inject mouse release to the FlashGUI manager
		FlashGUIPtr->FlashGUIManager->injectMouseUp(mouseButtonId);
		
		m_MouseState = mouseEvent.state;

		if (EnginePtr->GetForemostGameScreen(true))
		{
#ifdef GLE_EDITOR
			if( EnginePtr->GetEditor()->IsEditorMode() )
			{
				if ( mouseButtonId == GIS::MB_Left			)
				{
					EnginePtr->GetEditor()->RepositionMarkers();
					EnginePtr->GetEditor()->SendMovementToolSelected(false);
					m_bMouseLeft	= false;
				}
				else if ( mouseButtonId == GIS::MB_Middle	)		m_bMouseMiddle	= false;
				// To detect just mouse clicks
				m_bMouseMoved = false;
			}else
#endif		
			{
				EnginePtr->GetForemostGameScreen(true)->OnMouseRelease(mouseEvent, mouseButtonId);
			}
		}

		return true;
	}


#ifdef GLE_EDITOR
	/////////////////////////////////////////////////
	//editorMouseMoved()
	////////////////////////////////////
	bool Input::EditorMouseMoved()
	{
		m_MouseState = EnginePtr->GetEditor()->Editor_GetEditorMouse();
		GIS::MouseEvent M_event = GIS::MouseEvent::MouseEvent(m_pMouse,m_MouseState);
		mouseMoved(M_event);

		return true;
	}

	bool Input::EditorKeyRead()
	{
		GIS::KeyCode K_code;
		K_code = EditorKeyCodeTranslation(EnginePtr->GetEditor()->Editor_GetKeyCode());
		
		int tx = 0;
		GIS::KeyEvent K_event = GIS::KeyEvent::KeyEvent(m_pKeyboard,K_code,tx);

		#ifdef GLE_DEBUG_INPUT
			GGETRACELOG("Key %d:",K_code);
		#endif

		if (EnginePtr->GetEditor()->Editor_GetKeyStatus())
		{
			#ifdef GLE_DEBUG_INPUT
				GGETRACELOG("button pressed");
			#endif
			
			
			keyPressed(K_event);
				
			
		}
		else 
		{
			#ifdef GLE_DEBUG_INPUT
				GGETRACELOG("button released");
			#endif

			keyReleased(K_event);
		}
		
		return true;
	}
	
	bool Input::EditorMouseButtonRead(int mouseButtonID, bool mouseButtonState)
	{
		GIS::MouseButtonID M_btn;
		M_btn = EditorMouseButtonTranslation(mouseButtonID);
		m_MouseState = EnginePtr->GetEditor()->Editor_GetEditorMouse();

		GIS::MouseEvent M_event = GIS::MouseEvent::MouseEvent(m_pMouse, m_MouseState);

		if(mouseButtonState)
		{
			mousePressed(M_event,M_btn);
		}
		else
		{
			mouseReleased(M_event,M_btn);
		}
		
		return true;
	}


	GIS::MouseButtonID Input::EditorMouseButtonTranslation(int buttonID)
	{
		GIS::MouseButtonID emptyMouse;
		emptyMouse = GIS::MouseButtonID (100);

		switch (buttonID)
		{
		case 1048576:
			return GIS::MB_Left;
		case 2097152:
			return GIS::MB_Right;
		case 4194304:
			return GIS::MB_Middle;
		case 8388608:
			//XButton1
			return GIS::MB_Button3;
		case 16777216:
			//XButton2
			return GIS::MB_Button4;
		case 0:
		default:
			return emptyMouse;
		}

	}

	GIS::KeyCode Input::EditorKeyCodeTranslation(int key)
	{
		GIS::KeyCode emptyKey;
		emptyKey = GIS::KeyCode (500);
		switch(key)
		{
			//numeric
			case 48:
				return GIS::KC_0;
			case 49:
				return GIS::KC_1;
			case 50:
				return GIS::KC_2;
			case 51:
				return GIS::KC_3;
			case 52:
				return GIS::KC_4;
			case 53:
				return GIS::KC_5;
			case 54:
				return GIS::KC_6;
			case 55:
				return GIS::KC_7;
			case 56:
				return GIS::KC_8;
			//alphabetic
			case 57:
				return GIS::KC_9;
			case 65:
				return GIS::KC_A;
			case 66:
				return GIS::KC_B;
			case 67:
				return GIS::KC_C;
			case 68:
				return GIS::KC_D;
			case 69:
				return GIS::KC_E;
			case 70:
				return GIS::KC_F;
			case 71:
				return GIS::KC_G;
			case 72:
				return GIS::KC_H;
			case 73:
				return GIS::KC_I;
			case 74:
				return GIS::KC_J;
			case 75:
				return GIS::KC_K;
			case 76:
				return GIS::KC_L;
			case 77:
				return GIS::KC_M;
			case 78:
				return GIS::KC_N;
			case 79:
				return GIS::KC_O;
			case 80:
				return GIS::KC_P;
			case 81:
				return GIS::KC_Q;
			case 82:
				return GIS::KC_R;
			case 83:
				return GIS::KC_S;
			case 84:
				return GIS::KC_T;
			case 85:
				return GIS::KC_U;
			case 86:
				return GIS::KC_V;
			case 87:
				return GIS::KC_W;
			case 88:
				return GIS::KC_X;
			case 89:
				return GIS::KC_Y;
			case 90:
				return GIS::KC_Z;
			//keys
			case 8:
				return GIS::KC_BACK;
			case 9:
				return GIS::KC_TAB;
			case 13:
				return GIS::KC_RETURN;
			case 16:
				return GIS::KC_LSHIFT; //GIS::KC_RSHIFT also exists
			case 17:
				return GIS::KC_LCONTROL; //GIS::KC_RCONTROL also exists
			case 18:
				return GIS::KC_LMENU;	//GIS::KC_RMENU also exists
			case 19:
				return GIS::KC_PAUSE;
			case 20:
				return GIS::KC_CAPITAL; 
			case 27:
				return GIS::KC_ESCAPE;
			case 32:
				return GIS::KC_SPACE;
			case 33:
				return GIS::KC_PGUP;
			case 34:
				return GIS::KC_PGDOWN;
			case 35:
				return GIS::KC_END;
			case 36:
				return GIS::KC_HOME;
			case 37:
				return GIS::KC_LEFT;
			case 38:
				return GIS::KC_UP;
			case 39:
				return GIS::KC_RIGHT;
			case 40:
				return GIS::KC_DOWN;
			case 45:
				return GIS::KC_INSERT;
			case 46:
				return GIS::KC_DELETE;
			case 144:
				return GIS::KC_NUMLOCK;
			case 145:
				return GIS::KC_SCROLL;

			//function keys
			case 112:
				return GIS::KC_F1;
			case 113:
				return GIS::KC_F2;
			case 114:
				return GIS::KC_F3;
			case 115:
				return GIS::KC_F4;
			case 116:
				return GIS::KC_F5;
			case 117:
				return GIS::KC_F6;
			case 118:
				return GIS::KC_F7;
			case 119:
				return GIS::KC_F8;
			case 120:
				return GIS::KC_F9;
			case 121:
				return GIS::KC_F10;
			case 122:
				return GIS::KC_F11;
			case 123:
				return GIS::KC_F12;
			case 124:
				return GIS::KC_F13;
			case 125:
				return GIS::KC_F14;
			case 126:
				return GIS::KC_F15;
			//numpads
			case 96:
				return GIS::KC_NUMPAD0;
			case 97:
				return GIS::KC_NUMPAD1;
			case 98:
				return GIS::KC_NUMPAD2;
			case 99:
				return GIS::KC_NUMPAD3;
			case 100:
				return GIS::KC_NUMPAD4;
			case 101:
				return GIS::KC_NUMPAD5;
			case 102:
				return GIS::KC_NUMPAD6;
			case 103:
				return GIS::KC_NUMPAD7;
			case 104:
				return GIS::KC_NUMPAD8;
			case 105:
				return GIS::KC_NUMPAD9;
			case 106:
				return GIS::KC_MULTIPLY;
			case 107:
				return GIS::KC_ADD;
			case 109:
				return GIS::KC_SUBTRACT;
			case 110:
				return GIS::KC_DECIMAL;
			case 111:
				return GIS::KC_DIVIDE;
			default:
				return emptyKey;			

		}
	}
#endif

	//edit these two functions to have input behavior
	bool Input::buttonPressed(const GIS::MoveMeEvent &mmEvent, GIS::MoveMeButtonID button){
		return true;
	}

	bool Input::buttonReleased(const GIS::MoveMeEvent &mmEvent, GIS::MoveMeButtonID button){
		return true;
	}

	bool Input::buttonPressed( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnWiiRemoteButtonPressed( wrEvent, button);
		}
		return true;
	}

	bool Input::buttonReleased( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnWiiRemoteButtonReleased(wrEvent, button);
		}
		return true;
	}

	bool Input::analogStickMoved( const GIS::WiiRemoteEvent &wrEvent )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnWiiAnalogStickMoved(wrEvent);
		}
		return true;
	}

	bool Input::remoteAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnWiiRemoteAccelerometersMoved(wrEvent);
		}
		return true;
	}

	bool Input::nunchuckAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnWiiNunchuckAccelerometersMoved(wrEvent);
		}
		return true;
	}

	bool Input::irMoved( const GIS::WiiRemoteEvent &wrEvent)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnWiiIRMoved(wrEvent);
		}
		return true;
	}

	bool Input::buttonPressed( const GIS::XPadEvent &xpEvent, int button)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnXpadButtonPressed(xpEvent, button);
		}
		return true;
	}

	bool Input::buttonReleased( const GIS::XPadEvent &xpEvent, int button)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnXpadButtonReleased(xpEvent, button);
		}
		return true;
	}

	bool Input::lTriggerPressed( const GIS::XPadEvent &xpEvent )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnXpadLTriggerPressed(xpEvent);
		}
		return true;
	}

	bool Input::rTriggerPressed( const GIS::XPadEvent &xpEvent )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnXpadRTriggerPressed(xpEvent);
		}
		return true;
	}

	bool Input::lThumbStickMoved( const GIS::XPadEvent &xpEvent )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnXpadLThumbStickMoved(xpEvent);
		}
		return true;
	}

	bool Input::rThumbStickMoved( const GIS::XPadEvent &xpEvent )
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->OnXpadRThumbStickMoved(xpEvent);
		}
		return true;
	}

	GIS::MoveMe* Input::GetMoveMe()
	{
		return m_pMoveMe;
	}


	GIS::Kinect* Input::getKinect()
	{
		return m_pKinect;
	}

	bool Input::gestureRecognized( const std::string gestureName)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->gestureRecognized(gestureName);
		}
		return true;
	}
	bool Input::speechRecognized( const std::string wordRecognized,const float wordConfidence)
	{
		if (EnginePtr->GetForemostGameScreen(true))
		{
			EnginePtr->GetForemostGameScreen(true)->speechRecognized(wordRecognized,wordConfidence);
		}
		return true;
	}

#ifdef DLLRELEASE
	//////////////////////////////////////////////////////////////////////////
	// DLLKeyCodeTranslation(int key):
	// Keycodes for use with the DLL release. The Java applet sends different keycodes
	// depending on if a letter is uppercase or lowercase, and GGE doesn't normally have
	// a problem with those keycodes. 
	//////////////////////////////////////////////////////////////////////////
	GIS::KeyCode Input::DLLKeyCodeTranslation(int key)
	{
		GIS::KeyCode emptyKey;
		emptyKey = GIS::KeyCode (500);
		switch(key)
		{
			//numeric
			case 48:
				return GIS::KC_0;
			case 49:
				return GIS::KC_1;
			case 50:
				return GIS::KC_2;
			case 51:
				return GIS::KC_3;
			case 52:
				return GIS::KC_4;
			case 53:
				return GIS::KC_5;
			case 54:
				return GIS::KC_6;
			case 55:
				return GIS::KC_7;
			case 56:
				return GIS::KC_8;
			case 57:
				return GIS::KC_9;
			
			//alphabetic
			case 65:
				return GIS::KC_A;
			case 66:
				return GIS::KC_B;
			case 67:
				return GIS::KC_C;
			case 68:
				return GIS::KC_D;
			case 69:
				return GIS::KC_E;
			case 70:
				return GIS::KC_F;
			case 71:
				return GIS::KC_G;
			case 72:
				return GIS::KC_H;
			case 73:
				return GIS::KC_I;
			case 74:
				return GIS::KC_J;
			case 75:
				return GIS::KC_K;
			case 76:
				return GIS::KC_L;
			case 77:
				return GIS::KC_M;
			case 78:
				return GIS::KC_N;
			case 79:
				return GIS::KC_O;
			case 80:
				return GIS::KC_P;
			case 81:
				return GIS::KC_Q;
			case 82:
				return GIS::KC_R;
			case 83:
				return GIS::KC_S;
			case 84:
				return GIS::KC_T;
			case 85:
				return GIS::KC_U;
			case 86:
				return GIS::KC_V;
			case 87:
				return GIS::KC_W;
			case 88:
				return GIS::KC_X;
			case 89:
				return GIS::KC_Y;
			case 90:
				return GIS::KC_Z;
			
			// lowercase
			case 97:
				return GIS::KC_A;
			case 98:
				return GIS::KC_B;
			case 99:
				return GIS::KC_C;
			case 100:
				return GIS::KC_D;
			case 101:
				return GIS::KC_E;
			case 102:
				return GIS::KC_F;
			case 103:
				return GIS::KC_G;
			case 104:
				return GIS::KC_H;
			case 105:
				return GIS::KC_I;
			case 106:
				return GIS::KC_J;
			case 107:
				return GIS::KC_K;
			case 108:
				return GIS::KC_L;
			case 109:
				return GIS::KC_M;
			case 110:
				return GIS::KC_N;
			case 111:
				return GIS::KC_O;
			case 112:
				return GIS::KC_P;
			case 113:
				return GIS::KC_Q;
			case 114:
				return GIS::KC_R;
			case 115:
				return GIS::KC_S;
			case 116:
				return GIS::KC_T;
			case 117:
				return GIS::KC_U;
			case 118:
				return GIS::KC_V;
			case 119:
				return GIS::KC_W;
			case 120:
				return GIS::KC_X;
			case 121:
				return GIS::KC_Y;
			case 122:
				return GIS::KC_Z;

			//keys
			/*case 8:
				return GIS::KC_BACK;
			case 9:
				return GIS::KC_TAB;
			case 13:
				return GIS::KC_RETURN;
			case 16:
				return GIS::KC_LSHIFT; //GIS::KC_RSHIFT also exists
			case 17:
				return GIS::KC_LCONTROL; //GIS::KC_RCONTROL also exists
			case 18:
				return GIS::KC_LMENU;	//GIS::KC_RMENU also exists
			case 19:
				return GIS::KC_PAUSE;
			case 20:
				return GIS::KC_CAPITAL; 
			case 27:
				return GIS::KC_ESCAPE;
			case 32:
				return GIS::KC_SPACE;
			case 33:
				return GIS::KC_PGUP;
			case 34:
				return GIS::KC_PGDOWN;
			case 35:
				return GIS::KC_END;
			case 36:
				return GIS::KC_HOME;
			case 37:
				return GIS::KC_LEFT;
			case 38:
				return GIS::KC_UP;
			case 39:
				return GIS::KC_RIGHT;
			case 40:
				return GIS::KC_DOWN;
			case 45:
				return GIS::KC_INSERT;
			case 46:
				return GIS::KC_DELETE;
			case 144:
				return GIS::KC_NUMLOCK;
			case 145:
				return GIS::KC_SCROLL;*/

			//function keys
			case 16777226:
				return GIS::KC_F1;
			case 16777227:
				return GIS::KC_F2;
			case 16777228:
				return GIS::KC_F3;
			case 16777229:
				return GIS::KC_F4;
			case 16777230:
				return GIS::KC_F5;
			case 16777231:
				return GIS::KC_F6;
			case 16777232:
				return GIS::KC_F7;
			case 16777233:
				return GIS::KC_F8;
			case 16777234:
				return GIS::KC_F9;
			case 16777235:
				return GIS::KC_F10;
			/*case 122:
				return GIS::KC_F11;
			case 123:
				return GIS::KC_F12;
			case 124:
				return GIS::KC_F13;
			case 125:
				return GIS::KC_F14;
			case 126:
				return GIS::KC_F15;*/
			
			//numpads
			/*case 96:
				return GIS::KC_NUMPAD0;
			case 97:
				return GIS::KC_NUMPAD1;
			case 98:
				return GIS::KC_NUMPAD2;
			case 99:
				return GIS::KC_NUMPAD3;
			case 100:
				return GIS::KC_NUMPAD4;
			case 101:
				return GIS::KC_NUMPAD5;
			case 102:
				return GIS::KC_NUMPAD6;
			case 103:
				return GIS::KC_NUMPAD7;
			case 104:
				return GIS::KC_NUMPAD8;
			case 105:
				return GIS::KC_NUMPAD9;
			case 106:
				return GIS::KC_MULTIPLY;
			case 107:
				return GIS::KC_ADD;
			case 109:
				return GIS::KC_SUBTRACT;
			case 110:
				return GIS::KC_DECIMAL;
			case 111:
				return GIS::KC_DIVIDE;*/
			default:
				return emptyKey;			

		}
	}
#endif
}
