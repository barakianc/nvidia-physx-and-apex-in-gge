#include "StdAfx.h"
#include "ResourceManager.h"

namespace GamePipe
{
	ResourceManager* GamePipe::ResourceManager::m_pResourceManager;



	void ResourceManager::LoadResourceGroup( std::string resGroupName )
	{
		std::map<std::string, int>::iterator it;
		it = resourceTracker.find(resGroupName);

		//Increase resource counter
		if (it != resourceTracker.end())
		{
			if (resourceTracker[resGroupName] <= 0)
			{
				//Load resource
				resourceTracker[resGroupName] = 0;
				DoLoad(resGroupName);
			}
			resourceTracker[resGroupName]++;
		}
		else
		{
			//Load the resource if it haven't been loaded
			resourceTracker[resGroupName] = 1;
			DoLoad(resGroupName);
		}
	}

	void ResourceManager::UnloadResourceGroup( std::string resGroupName )
	{
		std::map<std::string, int>::iterator it;
		it = resourceTracker.find(resGroupName);

		if (it != resourceTracker.end())
		{
			resourceTracker[resGroupName]--;
			if (resourceTracker[resGroupName] <= 0)
			{
				//remove the reference
				resourceTracker.erase(it);
				DoUnload(resGroupName);
			}
		} 
		else
		{
			//Try to unload resource that not exists
			GGETRACELOG("Resource Group not exits");
		}
	}

	void ResourceManager::DoLoad( std::string resGroupName )
	{
		try
		{
			Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup(resGroupName);
			//Ogre::ResourceGroupManager::getSingleton().loadResourceGroup(resGroupName);
		}
		catch (Ogre::InternalErrorException e)
		{
			GGETRACELOG(e.getFullDescription().c_str());
		}
	}

	void ResourceManager::DoUnload( std::string resGroupName )
	{
		//Ogre::ResourceGroupManager::getSingleton().unloadResourceGroup(resGroupName);
		Ogre::ResourceGroupManager::getSingleton().clearResourceGroup(resGroupName);
	}

}

