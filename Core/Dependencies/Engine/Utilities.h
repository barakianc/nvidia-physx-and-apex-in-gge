#pragma once

// Engine
#include "Engine.h"

// Input
#include "Input.h"

#include "OgreTextAreaOverlayElement.h"

// Easy access
#define UtilitiesPtr (GamePipe::Utilities::GetInstancePointer())
#define UtilitiesRef (GamePipe::Utilities::GetInstanceReference())


namespace GamePipe
{
	class Utility : public GamePipe::Object
	{
	private:
	public:
		Utility() {}
		virtual ~Utility() {}
	};

	class DebugText : public Utility
	{
	private:
        Ogre::Overlay* overlay;

	public:
		void SetText(const Ogre::String text);
		void SetFontName(const Ogre::String text);
		void SetPosition(const Ogre::Real x, const Ogre::Real y);
		void SetCharHeight(const Ogre::Real charHeight);
		void SetColor(const Ogre::ColourValue colourValue);
		void SetMaterialName(const Ogre::String materialName);
        Ogre::Overlay* GetOverlay() { return overlay; }

	public:
		void Create(Ogre::String overlayText, Ogre::Real x = 0.5, Ogre::Real y = 0.5, const Ogre::Real charHeight = 3,
			const Ogre::ColourValue color = Ogre::ColourValue(1, 1, 1, 1), const Ogre::TextAreaOverlayElement::Alignment halignment = Ogre::TextAreaOverlayElement::Left,
			const Ogre::String materialName = "", const Ogre::Real w = 0, const Ogre::Real h = 0);
		void Destroy();

	public:
		DebugText(Ogre::String overlayText, Ogre::Real x = 0.5, Ogre::Real y = 0.5, const Ogre::Real charHeight = 3,
			const Ogre::ColourValue color = Ogre::ColourValue(1, 1, 1, 1), const Ogre::TextAreaOverlayElement::Alignment halignment = Ogre::TextAreaOverlayElement::Left,
			const Ogre::String materialName = "", const Ogre::Real w = 0, const Ogre::Real h = 0);
		~DebugText();
	};

	class Utilities : public Singleton<Utilities>, public ObjectManager<Utility>
	{
	private:

	protected:

	public:
		DebugText* GetDebugText(std::string debugTextKey)
		{
			return static_cast<DebugText*>(Get(debugTextKey));
		}

	public:
		friend GamePipe::Singleton<Utilities>;

	private:
		Utilities() {}
		~Utilities() {}
	};
}