#include "StdAfx.h"
#include "EditorMsgManager_SceneNodePRoperties.h"

namespace GamePipe
{
	EditorMsgManager_SceneNodePRoperties::EditorMsgManager_SceneNodePRoperties(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_SceneNodePRoperties::~EditorMsgManager_SceneNodePRoperties()
	{

	}

	bool EditorMsgManager_SceneNodePRoperties::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if( statement == GLE_SceneNode_Scale ||
			statement == GLE_SceneNode_RotationQuat ||
			statement == GLE_SceneNode_RotationDeg)
		{
			m_pEditor->Editor_ReportDeprecated(statement);
			return true;
		} // Node management

		if(statement == GLE_SceneNode_Translation)
		{
			//The raw passed in mouse coordinates
			float mouseX = (float)atof(tokens[2].c_str());
			float mouseY = (float)atof(tokens[3].c_str());

			//Check what mode the dynamic editing tool is in.
			if(m_pEditor->b_DynamicScalingMode)
			{
				DynamicScaling(mouseX, mouseY);
			}
			else if(m_pEditor->b_DynamicRotationMode)
			{
				DynamicRotation(mouseX, mouseY);
			}
			else if(m_pEditor->b_DynamicPositioningMode)
			{
				DynamicPositioning(mouseX, mouseY);
			}

		}
		if(statement == GLE_SceneNode_Create)
		{
			Ogre::String name = tokens[2];						

			// Create a scene node
			if (!m_pEditor->GetSceneManager()->hasSceneNode(name))
				m_pEditor->GetSceneManager()->getRootSceneNode()->createChildSceneNode(Ogre::String(name));	

			return true;
		} // Node create
		
		if(statement == GLE_SceneNode_Del)
		{
			Ogre::String name = tokens[2];

			//m_pEditor->SendSimpleMessage("Scene Node: " + name);

			// Destroy the node by its name
			if (name != "OGRE_SCENEROOT")
			{
				if (m_pEditor->GetSceneManager()->hasSceneNode(name))
				{
					Ogre::SceneNode* node; 
					node = m_pEditor->GetSceneManager()->getSceneNode(name);
					Ogre::SceneNode::ObjectIterator attachedObjs = node->getAttachedObjectIterator();
					bool deletedAtLeastOneGameObject = false;
					while(attachedObjs.hasMoreElements())
					{
						Ogre::MovableObject* o = attachedObjs.getNext();
						if (o->getMovableType() == Ogre::EntityFactory::FACTORY_TYPE_NAME)
						{
							if (EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->HasGameObject(o->getName()))
							{
								GameObject* currentObject = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetGameObject(o->getName());
								currentObject->removeThis();
								currentObject->m_pGraphicsObject = NULL;

								//m_pEditor->SendSimpleMessage("En el while");
								deletedAtLeastOneGameObject = true;
							}
						}
					}


					if (m_pEditor->GetSceneManager()->hasSceneNode(name))// && !deletedAtLeastOneGameObject)
					{
						//m_pEditor->SendSimpleMessage("En el <if>");
						m_pEditor->GetSceneManager()->destroySceneNode(Ogre::String(name));
					}

					 m_pEditor->m_pMVGameObject = NULL;
				}
			}
			return true;
		} // Node delete

		//Changing the mode of the Dynamic Editing Tool
		if(statement == GLE_Dynamic_Editing_Tool_Select)
		{
			
			Ogre::String selection = tokens[2];

			if(selection == "0")
			{
				//Turn off the editing tool.
				m_pEditor->b_DynamicToolActive = false;
				m_pEditor->b_DynamicPositioningMode = false;
				m_pEditor->b_DynamicRotationMode = false;
				m_pEditor->b_DynamicScalingMode = false;

				m_pEditor->m_DynamicToolXNode->setVisible(false);
				m_pEditor->m_DynamicToolYNode->setVisible(false);
				m_pEditor->m_DynamicToolZNode->setVisible(false);
				m_pEditor->m_FreeDynamicToolNode->setVisible(false);
				m_pEditor->SendMovementToolSelected(false);
			}
			else if(selection == "1")
			{
				//Activate the Positioning Tool
				m_pEditor->b_DynamicPositioningMode = true;
				m_pEditor->b_DynamicToolActive = true;

				//Turn off any other tools
				m_pEditor->b_DynamicRotationMode = false;
				m_pEditor->b_DynamicScalingMode = false;
			}
			else if(selection == "2")
			{
				//Activate the Rotation Tool
				m_pEditor->b_DynamicRotationMode = true;
				m_pEditor->b_DynamicToolActive = true;

				//Turn off any other tools
				m_pEditor->b_DynamicPositioningMode = false;
				m_pEditor->b_DynamicScalingMode = false;
			}
			else if(selection == "3")
			{
				//Activate the Scaling Tool
				m_pEditor->b_DynamicScalingMode = true;
				m_pEditor->b_DynamicToolActive = true;

				//Turn off any other tools
				m_pEditor->b_DynamicPositioningMode = false;
				m_pEditor->b_DynamicRotationMode = false;
			}

			//Switch the in-editor icons that appear for editing the object
			m_pEditor->SetMarkerShapes();

			return true;
		}

		return false;
	}

	void EditorMsgManager_SceneNodePRoperties::DynamicPositioning(float mouseX, float mouseY)
	{
		Ogre::Vector3* offsetVector = new Ogre::Vector3();

		//Now decide on how to translate
		if(m_pEditor->b_FreeDynamicToolMarkerSelected)
		{
			//Free translating around the current screen

			
			Ogre::Camera *pCamera = m_pEditor->m_pEngine->GetForemostGameScreen()->GetActiveCamera();
			
			Ogre::Matrix3 *perspectiveMatrix = new Ogre::Matrix3();
			pCamera->getProjectionMatrix().extract3x3Matrix(*perspectiveMatrix);

			//Need a vector facing towards the camera
			//A new location in 3D space based on the point in the middle of the viewport.
			Ogre::Vector3 vecLocation;
			float pointX = (float)m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualWidth() / 2;
			float pointY = (float)m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualHeight() / 2;
			//Go from mouse coordinates to screen coordinates
			vecLocation.x = ((pointX * 2) / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualWidth()) - 1;
			//Remember to negate the Y value at the end of the equation
			vecLocation.y = (((pointY * 2) / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualHeight()) - 1) * -1;

			//Use the scaling amount found at locations [0][0] and [1][1]
			vecLocation.x /= *perspectiveMatrix[0][0];
			vecLocation.y /= *perspectiveMatrix[1][1];
			vecLocation.z = 1;

			//Now go from perspective to world space
			vecLocation = pCamera->getViewMatrix().inverse() * vecLocation;

			//Now that we have a point, get a vector from that point to the camera's point.
			Ogre::Vector3 translationPlaneNormal = pCamera->getDerivedPosition() - vecLocation;
			translationPlaneNormal.normalise();

			//Create a plane
			Ogre::Plane translationPlane = Ogre::Plane(translationPlaneNormal, m_pEditor->m_pSceneNodeSelected->getPosition());

			//Create the Ray we need. Starting from the camera, going to the location where the mouse cursor was mapped to
			Ogre::Ray theRay = pCamera->getCameraToViewportRay(mouseX / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualWidth(),
																mouseY / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualHeight());

			//See where the intersection point is
			std::pair<bool, Ogre::Real> result = theRay.intersects(translationPlane);
			if( result.first )
			{
				//Move object selected to this point
				m_pEditor->m_pSceneNodeSelected->setPosition(theRay.getPoint(result.second));
			}
		}
		else
		{
			//Translate along one axis

			//Get our offset vector.
			if(CreateOffsetVector(mouseX, mouseY, offsetVector))
			{
				//offset the selected object
				Ogre::Vector3 newPosition = m_pEditor->m_pSceneNodeSelected->getPosition();
				newPosition.x += offsetVector->x;
				newPosition.y += offsetVector->y;
				newPosition.z += offsetVector->z;
				m_pEditor->m_pSceneNodeSelected->setPosition(newPosition);
			}
		}


		delete offsetVector;
		

		//Reposition the movement tool
		m_pEditor->RepositionMarkers();
	}

	void EditorMsgManager_SceneNodePRoperties::DynamicScaling(float mouseX, float mouseY)
	{
		
		//If the user just clicks on the object and does not move it, then it should stay the same scale as when it started
		float newScaleX = m_pEditor->m_StartingScale.x;
		float newScaleY = m_pEditor->m_StartingScale.y;
		float newScaleZ = m_pEditor->m_StartingScale.z;

		//Check to see if we should be scaling by the entire object.
		//This is the easiest scaling to do, we just do it based on how far the user dragged their mouse.
		if(m_pEditor->b_FreeDynamicToolMarkerSelected)
		{
			//This value is used to help convert distance moved into how much to scal the object.
			float scalingFactor = 0.01f;

			//If the user pulls the mouse to the left of the starting point, use negative scaling
			if(mouseX < m_pEditor->m_ScalingMouseStartPoint.x)
			{
				newScaleX += -1 * (std::abs(mouseX - m_pEditor->m_ScalingMouseStartPoint.x) * scalingFactor);
				newScaleY += -1 * (std::abs(mouseX - m_pEditor->m_ScalingMouseStartPoint.x) * scalingFactor);
				newScaleZ += -1 * (std::abs(mouseX - m_pEditor->m_ScalingMouseStartPoint.x) * scalingFactor);
			}
			//Otherwise increase the scale normally
			else
			{
				newScaleX += std::abs(mouseX - m_pEditor->m_ScalingMouseStartPoint.x) * scalingFactor;
				newScaleY += std::abs(mouseX - m_pEditor->m_ScalingMouseStartPoint.x) * scalingFactor;
				newScaleZ += std::abs(mouseX - m_pEditor->m_ScalingMouseStartPoint.x) * scalingFactor;
			}
			//Scale the object
			m_pEditor->m_pSceneNodeSelected->setScale(newScaleX, newScaleY, newScaleZ);
		}
		//Otherwise scale by axis
		else
		{
			//This value is used to help convert distance moved into how much to scale the object.
			float scalingFactor = 0.01f;

			Ogre::Vector3 * offsetVector = new Ogre::Vector3();
			
			if(CreateOffsetVector(mouseX, mouseY, offsetVector))
			{
				//offset the selected object
				if(m_pEditor->b_DynamicToolXMarkerSelected)
				{
					Ogre::Vector3 newPosition = m_pEditor->m_DynamicToolXNode->getPosition();
					newPosition.x += offsetVector->x;
					newPosition.y += offsetVector->y;
					newPosition.z += offsetVector->z;
					m_pEditor->m_DynamicToolXNode->setPosition(newPosition);

					//Now that the tool is offset, figure out the distance traveled since its start point
					//This will act as a number we use to scale the object
					float distance = (float)m_pEditor->m_DynamicToolXNode->getPosition().distance(m_pEditor->m_ScalingMarkerStartPoint);

					//Now check if the current position is less than the starting position on this axis. If so, we scale negatively.
					if(m_pEditor->m_DynamicToolXNode->getPosition().x < m_pEditor->m_ScalingMarkerStartPoint.x)
					{
						newScaleX += -1 * (distance * scalingFactor);
					}
					else
					{
						newScaleX += distance * scalingFactor;
					}

				}
				else if(m_pEditor->b_DynamicToolYMarkerSelected)
				{
					Ogre::Vector3 newPosition = m_pEditor->m_DynamicToolYNode->getPosition();
					newPosition.x += offsetVector->x;
					newPosition.y += offsetVector->y;
					newPosition.z += offsetVector->z;
					m_pEditor->m_DynamicToolYNode->setPosition(newPosition);

					//Now that the tool is offset, figure out the distance traveled since its start point
					//This will act as a number we use to scale the object
					float distance = (float)m_pEditor->m_DynamicToolYNode->getPosition().distance(m_pEditor->m_ScalingMarkerStartPoint);

					//Now check if the current position is less than the starting position on this axis. If so, we scale negatively.
					if(m_pEditor->m_DynamicToolYNode->getPosition().y < m_pEditor->m_ScalingMarkerStartPoint.y)
					{
						newScaleY += -1 * (distance * scalingFactor);
					}
					else
					{
						newScaleY += distance * scalingFactor;
					}
				}
				else
				{
					Ogre::Vector3 newPosition = m_pEditor->m_DynamicToolZNode->getPosition();
					newPosition.x += offsetVector->x;
					newPosition.y += offsetVector->y;
					newPosition.z += offsetVector->z;
					m_pEditor->m_DynamicToolZNode->setPosition(newPosition);

					//Now that the tool is offset, figure out the distance traveled since its start point
					//This will act as a number we use to scale the object
					float distance = (float)m_pEditor->m_DynamicToolZNode->getPosition().distance(m_pEditor->m_ScalingMarkerStartPoint);


					//Now check if the current position is less than the starting position on this axis. If so, we scale negatively.
					if(m_pEditor->m_DynamicToolZNode->getPosition().z < m_pEditor->m_ScalingMarkerStartPoint.z)
					{
						newScaleZ += -1 * (distance * scalingFactor);
					}
					else
					{
						newScaleZ += distance * scalingFactor;
					}
				}

				//Now scale the object the user selected
				m_pEditor->m_pSceneNodeSelected->setScale(newScaleX, newScaleY, newScaleZ);

			}

			delete offsetVector;
		}
	}


	void EditorMsgManager_SceneNodePRoperties::DynamicRotation(float mouseX, float mouseY)
	{
		//If the user just clicks on the object and does not move it, then it should have the same rotation as when it started
		float newRotationX = 0;
		float newRotationY = 0;
		float newRotationZ = 0;

		//Check to see if we should rotate the object freely.
		//In the future it could be a good idea to use an Arcball implementation here.
		if(m_pEditor->b_FreeDynamicToolMarkerSelected)
		{
			//Nothing for now
		}
		//Otherwise rotate by axis
		else
		{
			//This value is used to help convert distance moved into how much to rotate the object.
			float rotationFactor = 0.01f;

			Ogre::Vector3* offsetVector = new Ogre::Vector3();
			
			if(CreateOffsetVector(mouseX, mouseY, offsetVector))
			{
				//offset the selected object
				if(m_pEditor->b_DynamicToolXMarkerSelected)
				{
					Ogre::Vector3 newPosition = m_pEditor->m_DynamicToolXNode->getPosition();
					newPosition.x += offsetVector->x;
					newPosition.y += offsetVector->y;
					newPosition.z += offsetVector->z;
					m_pEditor->m_DynamicToolXNode->setPosition(newPosition);

					//Now that the tool is offset, figure out the distance traveled since its start point
					//This will act as a number we use to scale the object
					float distance = (float)m_pEditor->m_DynamicToolXNode->getPosition().distance(m_pEditor->m_RotationMarkerStartPoint);

					//Now check if the current position is less than the starting position on this axis. If so, we scale negatively.
					if(m_pEditor->m_DynamicToolXNode->getPosition().x < m_pEditor->m_RotationMarkerStartPoint.x)
					{
						newRotationX += -1 * (distance * rotationFactor);
					}
					else
					{
						newRotationX += distance * rotationFactor;
					}

					//Set the new starting position.
					m_pEditor->m_RotationMarkerStartPoint = m_pEditor->m_DynamicToolXNode->getPosition();
				}
				else if(m_pEditor->b_DynamicToolYMarkerSelected)
				{
					Ogre::Vector3 newPosition = m_pEditor->m_DynamicToolYNode->getPosition();
					newPosition.x += offsetVector->x;
					newPosition.y += offsetVector->y;
					newPosition.z += offsetVector->z;
					m_pEditor->m_DynamicToolYNode->setPosition(newPosition);

					//Now that the tool is offset, figure out the distance traveled since its start point
					//This will act as a number we use to scale the object
					float distance = (float)m_pEditor->m_DynamicToolYNode->getPosition().distance(m_pEditor->m_RotationMarkerStartPoint);

					//Now check if the current position is less than the starting position on this axis. If so, we scale negatively.
					if(m_pEditor->m_DynamicToolYNode->getPosition().y < m_pEditor->m_RotationMarkerStartPoint.y)
					{
						newRotationY += -1 * (distance * rotationFactor);
					}
					else
					{
						newRotationY += distance * rotationFactor;
					}
				
					//Set the new starting position.
					m_pEditor->m_RotationMarkerStartPoint = m_pEditor->m_DynamicToolYNode->getPosition();
				}
				else
				{
					Ogre::Vector3 newPosition = m_pEditor->m_DynamicToolZNode->getPosition();
					newPosition.x += offsetVector->x;
					newPosition.y += offsetVector->y;
					newPosition.z += offsetVector->z;
					m_pEditor->m_DynamicToolZNode->setPosition(newPosition);

					//Now that the tool is offset, figure out the distance traveled since its start point
					//This will act as a number we use to scale the object
					float distance = (float)m_pEditor->m_DynamicToolZNode->getPosition().distance(m_pEditor->m_RotationMarkerStartPoint);


					//Now check if the current position is less than the starting position on this axis. If so, we scale negatively.
					if(m_pEditor->m_DynamicToolZNode->getPosition().z < m_pEditor->m_RotationMarkerStartPoint.z)
					{
						newRotationZ += -1 * (distance * rotationFactor);
					}
					else
					{
						newRotationZ += distance * rotationFactor;
					}

					//Set the new starting position.
					m_pEditor->m_RotationMarkerStartPoint = m_pEditor->m_DynamicToolZNode->getPosition();
				}

				//Create a Quaternion
				Ogre::Quaternion rotationQuat = Ogre::Quaternion( 1.0f, newRotationX, newRotationY, newRotationZ); 

				//Now scale the object the user selected
				m_pEditor->m_pSceneNodeSelected->rotate(rotationQuat);
			}

			delete offsetVector;
		}
	}


	//This method does the large amount of math needed to find an offset vector along one axis.
	//It returns false in a few edge cases where we the equation fails to get a real result.
	bool EditorMsgManager_SceneNodePRoperties::CreateOffsetVector(float mouseX, float mouseY, Ogre::Vector3* offsetVector)
	{
		//A new location in 3D space based on the mouse coordinates.
		Ogre::Vector3 newLocation;

		Ogre::Camera *pCamera = m_pEditor->m_pEngine->GetForemostGameScreen()->GetActiveCamera();
			
		//Go from mouse coordinates to screen coordinates
		newLocation.x = ((mouseX * 2) / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualWidth()) - 1;
		//Remember to negate the Y value at the end of the equation
		newLocation.y = (((mouseY * 2) / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualHeight()) - 1) * -1;

		//Now go from screen coordinates to perspective space.
		Ogre::Matrix3 perspectiveMatrix = Ogre::Matrix3();
		pCamera->getProjectionMatrix().extract3x3Matrix(perspectiveMatrix);
		//Use the scaling amount found at locations [0][0] and [1][1]
		newLocation.x /= perspectiveMatrix[0][0];
		newLocation.y /= perspectiveMatrix[1][1];
		newLocation.z = 1;

		//Now go from perspective to world space
		newLocation = pCamera->getViewMatrix().inverse() * newLocation;

		//Translate only on one axis
		//We will need 2 planes
		Ogre::Plane plane1, plane2;
		//And 2 normals, one for each plane
		Ogre::Vector3 plane1Normal, plane2Normal;

		//Determine which planes to use
		if(m_pEditor->b_DynamicToolXMarkerSelected)
		{
			//XY and XZ planes
			plane1Normal = Ogre::Vector3::UNIT_X.crossProduct(Ogre::Vector3::UNIT_Y);
			plane2Normal = Ogre::Vector3::UNIT_X.crossProduct(Ogre::Vector3::UNIT_Z);

			plane1 = Ogre::Plane(plane1Normal, m_pEditor->m_pSceneNodeSelected->getPosition());
			plane2 = Ogre::Plane(plane2Normal, m_pEditor->m_pSceneNodeSelected->getPosition());
		}
		else if(m_pEditor->b_DynamicToolYMarkerSelected)
		{
			//YX and YZ planes
			plane1Normal = Ogre::Vector3::UNIT_Y.crossProduct(Ogre::Vector3::UNIT_X);
			plane2Normal = Ogre::Vector3::UNIT_Y.crossProduct(Ogre::Vector3::UNIT_Z);

			plane1 = Ogre::Plane(plane1Normal, m_pEditor->m_pSceneNodeSelected->getPosition());
			plane2 = Ogre::Plane(plane2Normal, m_pEditor->m_pSceneNodeSelected->getPosition());
		}
		else
		{
			//ZX and ZY planes
			plane1Normal = Ogre::Vector3::UNIT_Z.crossProduct(Ogre::Vector3::UNIT_X);
			plane2Normal = Ogre::Vector3::UNIT_Z.crossProduct(Ogre::Vector3::UNIT_Y);

			plane1 = Ogre::Plane(plane1Normal, m_pEditor->m_pSceneNodeSelected->getPosition());
			plane2 = Ogre::Plane(plane2Normal, m_pEditor->m_pSceneNodeSelected->getPosition());
		}

		//Find the plane most parallel to our mouse vector
		//First we need a normalized mouse vector
		Ogre::Vector3 normalizedMouseVec = newLocation.normalisedCopy();

		//Normalized vector DOT plane's normal
		//Furtherest from zero is the winner, hence the absolute value
		Ogre::Real value1 = Ogre::Math::Abs(normalizedMouseVec.dotProduct(plane1Normal));
		Ogre::Real value2 = Ogre::Math::Abs(normalizedMouseVec.dotProduct(plane2Normal));

		//Store the intersection point
		std::pair<bool, Ogre::Real> intersectionResult;

		//Create the Ray we need for intersection. Starting from the camera, going to the location where the mouse cursor was mapped to
		Ogre::Ray theRay = pCamera->getCameraToViewportRay(mouseX / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualWidth(),
															mouseY / m_pEditor->m_pEngine->GetForemostGameScreen()->GetViewport()->getActualHeight());

		//Now really find the plane most parallel to our mouse vector
		if(value1 > value2)
		{
			//plane1 was the most parallel
			intersectionResult = theRay.intersects(plane1);
		}
		else
		{
			//plane2 was the most parallel
			intersectionResult = theRay.intersects(plane2);
		}

		//Make sure there was an intersection
		if(!intersectionResult.first)
		{
			//IF there was not, break out of this function.
			return false;
		}

		Ogre::Vector3 toolPosition;
		Ogre::Vector3 toolOffset = Ogre::Vector3(m_pEditor->CUBE_SIZE/2,m_pEditor->CUBE_SIZE/2, -m_pEditor->CUBE_SIZE/2);
		if(m_pEditor->b_DynamicToolXMarkerSelected)
		{
			if(m_pEditor->b_DynamicPositioningMode)
			{
				toolPosition = m_pEditor->m_pSceneNodeSelected->getPosition() - toolOffset + Ogre::Vector3(m_pEditor->CUBE_SIZE * 3, 0, 0);
			}
			else
			{
				toolPosition = m_pEditor->m_DynamicToolXNode->getPosition() + toolOffset;
			}
		}
		else if(m_pEditor->b_DynamicToolYMarkerSelected)
		{
			if(m_pEditor->b_DynamicPositioningMode)
			{
				toolPosition = m_pEditor->m_pSceneNodeSelected->getPosition() - toolOffset + Ogre::Vector3( 0, m_pEditor->CUBE_SIZE *3, 0);
			}
			else
			{
				toolPosition = m_pEditor->m_DynamicToolYNode->getPosition() + toolOffset;
			}
		}
		else
		{
			if(m_pEditor->b_DynamicPositioningMode)
			{
				toolPosition = m_pEditor->m_pSceneNodeSelected->getPosition() - toolOffset + Ogre::Vector3( 0, 0,m_pEditor->CUBE_SIZE * 3);
			}
			else
			{
				toolPosition = m_pEditor->m_DynamicToolZNode->getPosition() + toolOffset;
			}
		}

		//Get a vector that goes from the origin of the current selector to the point of intersection
		Ogre::Vector3 vectorToBeMapped = theRay.getPoint(intersectionResult.second) - toolPosition;

		Ogre::Vector3 unitAxis;
		//Now check the dot product of this vector with that of our axis to translate over
		if(m_pEditor->b_DynamicToolXMarkerSelected)
		{
			unitAxis = Ogre::Vector3::UNIT_X;
		}
		else if(m_pEditor->b_DynamicToolYMarkerSelected)
		{
			unitAxis = Ogre::Vector3::UNIT_Y;
		}
		else
		{
			unitAxis = Ogre::Vector3::UNIT_Z;
		}
				
		Ogre::Real dotProductResult = vectorToBeMapped.dotProduct(unitAxis);
		Ogre::Vector3 result;

		//Check our results
		if(dotProductResult == 0)
		{
			//Break out!
			return false;
		}
		else if(dotProductResult < 0)
		{
			//We will use an inverted unit axis for projection
			unitAxis = unitAxis * -1;
			//Now project onto the unit axis.
			result = (unitAxis.dotProduct(vectorToBeMapped)) * unitAxis;
			offsetVector->x = result.x;
			offsetVector->y = result.y;
			offsetVector->z = result.z;
		}
		else
		{
			//Do nothing special, just project
			result = (unitAxis.dotProduct(vectorToBeMapped)) * unitAxis;
			offsetVector->x = result.x;
			offsetVector->y = result.y;
			offsetVector->z = result.z;
		}


	return true;
}
	
}