#include "StdAfx.h"
#include "EditorMsgManager_GLE_AttachObject.h"

namespace GamePipe
{
	EditorMsgManager_GLE_AttachObject::EditorMsgManager_GLE_AttachObject(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_AttachObject::~EditorMsgManager_GLE_AttachObject()
	{

	}

	bool EditorMsgManager_GLE_AttachObject::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_AttachObject)
			return false;
		
		Ogre::String sMovingObject = tokens[2];
		Ogre::String sReceiveingObject = tokens[3];

		GGETRACELOG("GLE_AttachObject: %s to %s", sMovingObject.c_str(), sReceiveingObject.c_str());

		if (!m_pEditor->GetSceneManager()->hasSceneNode(sReceiveingObject) && sReceiveingObject != "OGRE_SCENEROOT")
		{
			m_pEditor->Editor_ReportErrorToGLE("Objects/SceneNodes can only be parented to other SceneNodes.");
			return true;
		}

		Ogre::SceneNode* receiveingObject;
		if (sReceiveingObject == "OGRE_SCENEROOT")
			receiveingObject = m_pEditor->GetSceneManager()->getRootSceneNode();
		else
			receiveingObject = m_pEditor->GetSceneManager()->getSceneNode(sReceiveingObject);



		if (m_pEditor->GetSceneManager()->hasSceneNode(sMovingObject))
		{
			Ogre::SceneNode* movingObject = m_pEditor->GetSceneManager()->getSceneNode(sMovingObject);

			Ogre::SceneNode::ObjectIterator attachedObjs = movingObject->getAttachedObjectIterator();

			movingObject->getParentSceneNode()->removeChild(movingObject->getName());

			receiveingObject->addChild(movingObject);

			while(attachedObjs.hasMoreElements())
			{
				Ogre::MovableObject* o = attachedObjs.getNext();
				//o->detatchFromParent();
				movingObject->attachObject(o);
			}
		}
		else
		{
			Ogre::MovableObject* movingObject; 

			if (m_pEditor->GetSceneManager()->hasMovableObject(sMovingObject, Ogre::EntityFactory::FACTORY_TYPE_NAME))
				movingObject = m_pEditor->GetSceneManager()->getMovableObject(sMovingObject, Ogre::EntityFactory::FACTORY_TYPE_NAME);
			else if (m_pEditor->GetSceneManager()->hasMovableObject(sMovingObject, Ogre::LightFactory::FACTORY_TYPE_NAME))
				movingObject = m_pEditor->GetSceneManager()->getMovableObject(sMovingObject, Ogre::LightFactory::FACTORY_TYPE_NAME);
			else
			{
				m_pEditor->Editor_ReportUnimplemented(GLE_AttachObject);
				return true;
			}

			//movingObject->detatchFromParent();
			receiveingObject->attachObject(movingObject);
		}

		return true;
	}
}