#ifndef IMessageHandle_h
#define IMessageHandle_h

/*
We include the editor class because some instances of this interface will require to use 
functions that belong to the editor system; we do this in order to minimize the impact 
of the new feature on the existing code.
*/

// Base Includes
#include "../Engine.h"
//#include "GameScreen.h"

// Other
#include <OgreMath.h>

// Editor Includes
#include "../Editor.h"

#include "../HavokWrapper.h"
#include <vector>

namespace GamePipe
{

	class IMessageHandle
	{
	protected:

		Editor* m_pEditor;

	public:

		IMessageHandle(){m_pEditor = NULL;}
		IMessageHandle(Editor* pEditor){m_pEditor = pEditor;}
		~IMessageHandle(){m_pEditor = NULL;}

		virtual bool ProcessMessage(std::vector<Ogre::String> &tokens) = 0;

		// access properties saved in the class or message management
		virtual bool AskCurrentStatusMessage(EditorProtocol protocolHandle, std::string &retString){return false;}
		virtual bool PreprocessSave(EditorProtocol protocolHandle){return false;}
		virtual bool PostprocessSave(EditorProtocol protocolHandle){return false;}

	};

}

#endif