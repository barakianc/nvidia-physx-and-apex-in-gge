#ifndef CH_ParticleSystem_H
#define CH_ParticleSystem_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_ParticleSystem : public IMessageHandle
	{
	public: 
		EditorMsgManager_ParticleSystem(Editor* pEditor);
		~EditorMsgManager_ParticleSystem();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif