/*

Since all the properties of the scene node share a common piece of code
we have decided to dedicate only one class tho handle them; however, if 
the code changes in the future, new classes can be added to the system 
to replace the current implementation.

*/

#ifndef CH_SceneNodePRoperties_H
#define CH_SceneNodePRoperties_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_SceneNodePRoperties : public IMessageHandle
	{
	public: 
		EditorMsgManager_SceneNodePRoperties(Editor* pEditor);
		~EditorMsgManager_SceneNodePRoperties();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
		
		
		void DynamicPositioning(float mouseX, float mouseY);
		void DynamicScaling(float mouseX, float mouseY);
		void DynamicRotation(float mouseX, float mouseY);

		bool CreateOffsetVector(float mouseX, float mouseY, Ogre::Vector3* offsetVector);
	};
}

#endif