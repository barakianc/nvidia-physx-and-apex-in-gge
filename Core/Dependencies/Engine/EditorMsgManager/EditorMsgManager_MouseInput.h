#ifndef CH_MouseInput_H
#define CH_MouseInput_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_MouseInput : public IMessageHandle
	{
	public: 
		EditorMsgManager_MouseInput(Editor* pEditor);
		~EditorMsgManager_MouseInput();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}


#endif