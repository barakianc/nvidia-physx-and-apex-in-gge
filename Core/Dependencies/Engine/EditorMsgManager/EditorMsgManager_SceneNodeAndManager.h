/*

Since all of these properties share common code we have decided to 
dedicate only one class tho handle them; however, if the code changes 
in the future, new classes can be added to the system to replace the 
current implementation.

*/

#ifndef CH_SceneNodeAndManager_H
#define CH_SceneNodeAndManager_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_SceneNodeAndManager : public IMessageHandle
	{
	public: 
		EditorMsgManager_SceneNodeAndManager(Editor* pEditor);
		~EditorMsgManager_SceneNodeAndManager();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif