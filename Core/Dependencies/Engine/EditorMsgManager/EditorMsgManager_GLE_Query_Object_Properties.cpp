#include "StdAfx.h"
#include "EditorMsgManager_GLE_Query_Object_Properties.h"

namespace GamePipe
{ 
	EditorMsgManager_GLE_Query_Object_Properties::EditorMsgManager_GLE_Query_Object_Properties(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_Query_Object_Properties::~EditorMsgManager_GLE_Query_Object_Properties()
	{

	}

	bool EditorMsgManager_GLE_Query_Object_Properties::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_Query_Object_Properties)
			return false;

		Ogre::String name = tokens[2];
		int type = atoi(tokens[3].c_str());

		GGETRACELOG("GLE_Query_Object_Properties: %d %s", type, name.c_str());

		switch (type)
		{
		case OBJTYPE_SceneNode: 
			{
				//find scene node properties and respond to GLE: GGE_Response_SceneNode_Properties				
				Ogre::SceneNode* node = m_pEditor->GetSceneManager()->getSceneNode(name);

				m_pEditor->Editor_GGE_Response_Object_Properties(name, OBJTYPE_SceneNode, m_pEditor->GetSceneNodeProperties(node));
				return true;
			}
		case OBJTYPE_Entity: 
			{
				Ogre::Entity* ent = m_pEditor->GetSceneManager()->getEntity(name);

				m_pEditor->Editor_GGE_Response_Object_Properties(name, OBJTYPE_Entity, m_pEditor->GetEntityProperties(ent));
				return true;
			}
		case OBJTYPE_Light: 
			{
				Ogre::Light* light = m_pEditor->GetSceneManager()->getLight(name);

				m_pEditor->Editor_GGE_Response_Object_Properties(name, OBJTYPE_Light, m_pEditor->GetLightProperties(light));
				return true;
			}
		case OBJTYPE_Camera: 
			{
				Ogre::SceneNode* node;

				if (name == "DEFAULT_SCREEN_CAMERA")
					node = m_pEditor->m_pEngine->GetForemostGameScreen()->GetActiveCameraSceneNode();
				else
					node = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getCamera(name)->getParentSceneNode();

				m_pEditor->Editor_GGE_Response_Object_Properties(name, OBJTYPE_Camera, m_pEditor->GetSceneNodeProperties(node));
				return true;
			}
		}

		return true;
	}
}