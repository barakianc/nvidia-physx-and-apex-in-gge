#include "StdAfx.h"
#include "EditorMsgManager_SceneNodeAndManager.h"

namespace GamePipe
{
	EditorMsgManager_SceneNodeAndManager::EditorMsgManager_SceneNodeAndManager(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_SceneNodeAndManager::~EditorMsgManager_SceneNodeAndManager()
	{

	}

	bool EditorMsgManager_SceneNodeAndManager::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement == GLE_Context_SceneManager_Set ||
			statement == GLE_Context_SceneManager_Get ||
			statement == GLE_Context_SceneNode_Set ||
			statement == GLE_Context_SceneNode_Get)
		{
			m_pEditor->Editor_ReportDeprecated(statement);
			return true;
		} // Common properties

		return false;
	}
}