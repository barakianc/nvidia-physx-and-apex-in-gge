#ifndef CH_GLE_AttachObject_H
#define CH_GLE_AttachObject_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_GLE_AttachObject : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLE_AttachObject(Editor* pEditor);
		~EditorMsgManager_GLE_AttachObject();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif