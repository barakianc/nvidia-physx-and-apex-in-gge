#ifndef CH_ScreenActivationKeyboardInput_H
#define CH_ScreenActivationKeyboardInput_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_ScreenActivationKeyboardInput : public IMessageHandle
	{
	public: 
		EditorMsgManager_ScreenActivationKeyboardInput(Editor* pEditor);
		~EditorMsgManager_ScreenActivationKeyboardInput();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif