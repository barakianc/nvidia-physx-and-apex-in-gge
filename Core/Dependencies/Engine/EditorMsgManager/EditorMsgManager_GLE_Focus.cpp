#include "StdAfx.h"
#include "EditorMsgManager_GLE_Focus.h"

namespace GamePipe
{
	EditorMsgManager_GLE_Focus::EditorMsgManager_GLE_Focus(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_Focus::~EditorMsgManager_GLE_Focus()
	{

	}

	bool EditorMsgManager_GLE_Focus::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_Focus)
			return false;
			
		GamePipe::GameScreen *pGameScreen = m_pEditor->m_pEngine->GetForemostGameScreen();

#ifdef GLE_EDITOR_MODELVIEW
		m_pEditor->m_pMVEntity = m_pEditor->GetMVEntity();
		/*
		if (pGameScreen->GetDefaultSceneManager()->hasSceneNode("MySN"))
		{
		Ogre::SceneNode::ObjectIterator entityIterator = pGameScreen->GetDefaultSceneManager()->getSceneNode("MySN")->getAttachedObjectIterator();
		if (entityIterator.hasMoreElements())
		{
		m_pMVEntity = dynamic_cast<Ogre::Entity*>(entityIterator.getNext());
		}
		}*/
#endif

		if (m_pEditor->m_pMVEntity != NULL)
		{

			// Calc the size
			Ogre::AxisAlignedBox boundingBox = m_pEditor->m_pMVEntity->getBoundingBox();		
			Ogre::Camera *pCamera = pGameScreen->GetActiveCamera();

			// define the 8 corners of the box
			//vector<Ogre::Vector3> corners(8);

			const Ogre::Vector3 *corners = boundingBox.getAllCorners();

			// Instead of working off the position of the entity's parent scene node (which can cause offset issues)
			// we calculate the center of the bounding box.
			Ogre::Vector3 entityPositionVector = boundingBox.getCenter();


			// Store the camera position in world coordinates
			Ogre::Vector3 cameraPositionVector = pCamera->getParentSceneNode()->_getDerivedPosition() + pCamera->getPosition();

			// for perspective cameras we calc the distance to the object and use it as a scalar
			if(pCamera->getName() == "Editor_CameraPerspective")
			{	
				float distconst = 1;

				Ogre::Real distance = distconst;

				//////
				//For unclear reasons the perspective camera focus case was
				//dabblilng in camera rotation which was causing strange behavior.
				//Rather than remove it entirely it has been commented out
				//for the time being -11/2/09
				//Removed and code trimmed in general 11/9/09 updates
				//////

				// first attempt at focus
				InputPtr->m_fRadius = distance;
				pCamera->getParentSceneNode()->setPosition(entityPositionVector);
				pCamera->setPosition	(0,0,0);
				pCamera->moveRelative	(Ogre::Vector3(0.0,0.0, InputPtr->m_fRadius));


				bool allVisible = false;

				// zoom out until its all visible
				while (!allVisible)
				{
					// if any are not visible
					allVisible = true;
					for (int i = 0; i < 8 && allVisible; i++)
					{										
						allVisible &= pCamera->isVisible(corners[i]);
					}

#ifdef GLE_DEBUG_NETWORK
					if (allVisible)
					{
						GGETRACELOG("All Visible:");
					}
					else
					{
						GGETRACELOG("Not Visible:");
					}
#endif

					if (!allVisible)
					{
						// increase the distance
						distance += distconst;

						// zoom out
						InputPtr->m_fRadius = distance;
						pCamera->moveRelative	(Ogre::Vector3(0.0,0.0, InputPtr->m_fRadius));
					}
				}
				pCamera->getParentSceneNode()->translate(m_pEditor->m_pMVEntity->getParentNode()->getPosition());

			}
			else // orthographic
			{

#ifdef GLE_DEBUG_NETWORK
				GGETRACELOG("World Min X: %f", boundingBox.getMinimum().x);
				GGETRACELOG("World Max X: %f", boundingBox.getMaximum().x);
				GGETRACELOG("World Min Y: %f", boundingBox.getMinimum().y);
				GGETRACELOG("World Max Y: %f", boundingBox.getMaximum().y);
#endif				
				pCamera->setPosition(0,0,0);

				// Find the difference between min and max
				int diffX =  int(boundingBox.getMaximum().x - boundingBox.getMinimum().x);
				int diffY =  int(boundingBox.getMaximum().y - boundingBox.getMinimum().y);
				int diffZ =  int(boundingBox.getMaximum().z - boundingBox.getMinimum().z);

				int scale;

				// Move the cameras back to their origional positions.

				//Modification of orthoginal camera position inconsistant with how cameras are created. commented out -11/2/09
				//Added static displacement to prevent clipping. -11/2/09

				if(pCamera->getName() ==  "Editor_CameraTop")
				{			
					pCamera->getParentSceneNode()->setPosition(entityPositionVector.x, entityPositionVector.y + 50000, entityPositionVector.z);
				}
				if(pCamera->getName() ==  "Editor_CameraSide")
				{
					pCamera->getParentSceneNode()->setPosition(entityPositionVector.x + 50000, entityPositionVector.y, entityPositionVector.z);
				}	
				if (pCamera->getName() ==   "Editor_CameraFront")
				{
					pCamera->getParentSceneNode()->setPosition(entityPositionVector.x, entityPositionVector.y, entityPositionVector.z + 50000);
				}


				if ( (diffX > diffY) && (diffX > diffZ) )
					scale = diffX;
				else if ( (diffY > diffX) && (diffY > diffZ) )
					scale = diffY;
				else if ( (diffZ > diffX) && (diffZ > diffY) )
					scale = diffZ;

				pCamera->setOrthoWindowHeight(Ogre::Real(scale));

				//hack to prevent focus from zooming in too far and getting the orthoganal cameras stuck
				//for values <= 4 for width/height
				while(pCamera->getOrthoWindowWidth() < 5 || pCamera->getOrthoWindowHeight() < 5) {
					scale++;
					pCamera->setOrthoWindowHeight(Ogre::Real(scale));
				}

				if(pCamera->getName() ==  "Editor_CameraTop")
				{			
					pCamera->getParentSceneNode()->translate(m_pEditor->m_pMVEntity->getParentNode()->getPosition().x, 0, m_pEditor->m_pMVEntity->getParentNode()->getPosition().z);
				}
				if(pCamera->getName() ==  "Editor_CameraSide")
				{
					pCamera->getParentSceneNode()->translate(0, m_pEditor->m_pMVEntity->getParentNode()->getPosition().y, m_pEditor->m_pMVEntity->getParentNode()->getPosition().z);
				}	
				if (pCamera->getName() ==   "Editor_CameraFront")
				{
					pCamera->getParentSceneNode()->translate(m_pEditor->m_pMVEntity->getParentNode()->getPosition().x, m_pEditor->m_pMVEntity->getParentNode()->getPosition().y, 0);
				}
			} // close orthographic
		}
		else
		{
			m_pEditor->Editor_ReportErrorToGLE("Nothing to (F)ocus on");
		}

		return true;

	}

}