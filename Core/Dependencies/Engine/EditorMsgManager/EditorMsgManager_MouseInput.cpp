#include "StdAfx.h"
#include "EditorMsgManager_MouseInput.h"

namespace GamePipe
{
	EditorMsgManager_MouseInput::EditorMsgManager_MouseInput(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_MouseInput::~EditorMsgManager_MouseInput()
	{

	}

	bool EditorMsgManager_MouseInput::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());

		if(statement == GLE_Input_Mouse_Position)
		{
			int x,y;

			x = atoi(tokens[2].c_str());
			y = atoi(tokens[3].c_str());

			GIS::MouseState m_state;

			m_state = InputPtr->GetMouseState();
			m_pEditor->ed_MouseState.X.absOnly = false;
			m_pEditor->ed_MouseState.Y.absOnly = false;

			m_pEditor->ed_MouseState.X.rel = x - m_pEditor->ed_MouseState.X.abs;
			m_pEditor->ed_MouseState.Y.rel = y - m_pEditor->ed_MouseState.Y.abs;

			m_pEditor->ed_MouseState.X.abs = x;
			m_pEditor->ed_MouseState.Y.abs = y;		

			m_pEditor->ed_MouseState.width = 1024;
			m_pEditor->ed_MouseState.height = 768;

#ifdef GLE_DEBUG_INPUT
			GGETRACE("Mouse Abs X %d, rel %d:",m_pEditor->m_state.X.abs, m_pEditor->m_state.X.rel);
			GGETRACE("Mouse height %d, width %d:",m_pEditor->m_state.height , m_pEditor->m_state.width);
#endif

			InputPtr->EditorMouseMoved();

			return true;
		}
		if(statement == GLE_Input_Mouse_Button)
		{
			int MouseButtonID = atoi(tokens[2].c_str());
			bool MouseButtonState = m_pEditor->ParseBool(tokens[3]);

			m_pEditor->ed_MouseState.X.abs = atoi(tokens[4].c_str());
			m_pEditor->ed_MouseState.Y.abs = atoi(tokens[5].c_str());

			InputPtr->EditorMouseButtonRead(MouseButtonID, MouseButtonState);

			return true;
		}
		if(statement == GLE_Input_Mouse_Wheel)
		{
			int d;

			d = atoi(tokens[2].c_str());

			m_pEditor->ed_MouseState.Z.rel = d; //ed_MouseState.Z.abs-d;
			m_pEditor->ed_MouseState.Z.abs = 0;

			InputPtr->EditorMouseMoved();

			m_pEditor->ed_MouseState.Z.rel = 0;


			return true;
		}

		return false;
	}
}