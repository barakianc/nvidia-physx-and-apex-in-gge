#ifndef EditorMsgManager_SkyBoxSubSystem_H
#define EditorMsgManager_SkyBoxSubSystem_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_SkyBoxSubSystem : public IMessageHandle
	{
		string m_sCurrentStatus;
		string m_sSkyBoxName;
		EditorProtocol m_currentProtcol;
		Ogre::SceneNode* node;

		void CloseLastSkyBox(bool closeAll);

	public: 
		EditorMsgManager_SkyBoxSubSystem(Editor* pEditor);
		~EditorMsgManager_SkyBoxSubSystem();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);

		bool AskCurrentStatusMessage(EditorProtocol protocolHandle, std::string &retString);
		bool PreprocessSave(EditorProtocol protocolHandle);
		bool PostprocessSave(EditorProtocol protocolHandle);
	};
}

#endif