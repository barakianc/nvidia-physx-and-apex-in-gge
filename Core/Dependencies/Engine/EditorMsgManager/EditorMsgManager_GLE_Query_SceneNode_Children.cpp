#include "StdAfx.h"
#include "EditorMsgManager_GLE_Query_SceneNode_Children.h"

namespace GamePipe
{ 
	EditorMsgManager_GLE_Query_SceneNode_Children::EditorMsgManager_GLE_Query_SceneNode_Children(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_Query_SceneNode_Children::~EditorMsgManager_GLE_Query_SceneNode_Children()
	{

	}

	bool EditorMsgManager_GLE_Query_SceneNode_Children::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_Query_SceneNode_Children)
			return false;

		Ogre::String nm = tokens[2];
		GGETRACELOG("GLE_Query_SceneNode_Children: %s", nm.c_str());

		Ogre::SceneNode* node; 

		if (nm == "OGRE_SCENEROOT")
			node = m_pEditor->GetSceneManager()->getRootSceneNode();
		else
			node = m_pEditor->GetSceneManager()->getSceneNode(nm);

		// Get children scenenodes
		Ogre::Node::ChildNodeIterator children = node->getChildIterator();
		while(children.hasMoreElements())
		{
			Ogre::Node* o = children.getNext();
			Ogre::SceneNode* newNode;

			string nodeName = o->getName();  
			int iFound = nodeName.find("_Node");   //for nodes that were created in the older version of GLE

			if (iFound != string::npos)
			{								
				nodeName.replace(iFound,5,"_GLE");
				if(m_pEditor->GetSceneManager()->hasSceneNode(nodeName))
					newNode = m_pEditor->GetSceneManager()->getSceneNode(nodeName);
				else
					newNode = m_pEditor->GetSceneManager()->getRootSceneNode()->createChildSceneNode(nodeName);
				
				//set the position, rotation and scale of the node
				newNode->setPosition(m_pEditor->GetSceneManager()->getSceneNode(o->getName())->getPosition());
				newNode->setOrientation(m_pEditor->GetSceneManager()->getSceneNode(o->getName())->getOrientation());
				newNode->setScale(m_pEditor->GetSceneManager()->getSceneNode(o->getName())->getScale());

				// now attach the objects to this new node
				Ogre::SceneNode* itNode = m_pEditor->GetSceneManager()->getSceneNode(o->getName());
				Ogre::SceneNode::ObjectIterator itAttachedObjs = itNode->getAttachedObjectIterator();
				while(itAttachedObjs.hasMoreElements())
				{
					Ogre::MovableObject* obj = itAttachedObjs.getNext();
					obj->detachFromParent();
					newNode->attachObject(obj);									
				} 
				// now delete the old node because we dont want it anymore, else it will also get saved
				m_pEditor->GetSceneManager()->destroySceneNode(o->getName());
			}

			stringstream ss;
			ss << nm << "||";
			ss << nodeName << "||";
			ss << OBJTYPE_SceneNode;

			m_pEditor->Editor_GenericCommand(GGE_Response_SceneNode_Children, ss.str());
		}


		// Get children movableobjects
		Ogre::SceneNode::ObjectIterator attachedObjs = node->getAttachedObjectIterator();
		while(attachedObjs.hasMoreElements())
		{
			Ogre::MovableObject* o = attachedObjs.getNext();

			stringstream ss;
			ss << nm << "||";
			ss << o->getName() << "||";


			if (o->getMovableType() == Ogre::LightFactory::FACTORY_TYPE_NAME)
				ss << OBJTYPE_Light;
			else if (o->getMovableType() == Ogre::EntityFactory::FACTORY_TYPE_NAME)
				ss << OBJTYPE_Entity;
			else if (o->getMovableType() == "Camera")
				ss << OBJTYPE_Camera;
			else
			{
				ss << OBJTYPE_Unknown << "||";
				ss << o->getMovableType();
			}

			m_pEditor->Editor_GenericCommand(GGE_Response_SceneNode_Children, ss.str());
		}
		
		return true;
	}
}