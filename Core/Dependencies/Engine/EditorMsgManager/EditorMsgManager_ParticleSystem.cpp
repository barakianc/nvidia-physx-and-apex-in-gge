#include "StdAfx.h"
#include "EditorMsgManager_ParticleSystem.h"

namespace GamePipe
{
	EditorMsgManager_ParticleSystem::EditorMsgManager_ParticleSystem(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_ParticleSystem::~EditorMsgManager_ParticleSystem()
	{
		
	}

	bool EditorMsgManager_ParticleSystem::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());

		if(statement == MV_LoadParticleSystem)
		{
			m_pEditor->MVLoadPS(tokens);
			return true;
		}
		if(statement == MV_ClearParticleSystem)
		{
#ifdef GLE_Particle_Universe
			// If there is a mesh loaded using MV_LOADMESH, this method unloads it
			if (m_pEditor->m_pRenderer->getSceneManager("BlankScreenSceneManager")->hasSceneNode("MySN_PS")	)
			{
				// Destroy them
				ParticleUniverse::ParticleSystemManager::getSingleton().destroyParticleSystem("MyPS", m_pEditor->m_pRenderer->getSceneManager("BlankScreenSceneManager") );
				m_pEditor->m_pRenderer->getSceneManager("BlankScreenSceneManager")->destroySceneNode("MySN_PS");							
			}
#endif
			return true;
		}
		if(statement == MV_GetListParticleSystems)
		{
			if (!m_pEditor->MVGetParticleSystemsList())
				return false;

			return true;
		}

			return false;
	}
}