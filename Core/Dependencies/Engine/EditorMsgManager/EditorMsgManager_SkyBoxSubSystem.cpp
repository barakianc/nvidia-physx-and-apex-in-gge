#include "StdAfx.h"
#include "EditorMsgManager_SkyBoxSubSystem.h"

namespace GamePipe
{
	void EditorMsgManager_SkyBoxSubSystem::CloseLastSkyBox(bool closeAll = true)
	{
		Ogre::SceneNode* pRootSceneNode = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getRootSceneNode();
		Ogre::Node* pNode;
		switch(m_currentProtcol)
		{
		case GLE_SkyBox_Create:
			pNode = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyBoxNode();
			pRootSceneNode->removeChild(pNode);
			if(closeAll)
				m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->setSkyBox(false, m_sSkyBoxName);
			break;
		case GLE_SkyDome_Create:
			pNode = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyDomeNode();
			pRootSceneNode->removeChild(pNode);
			if(closeAll)
				m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->setSkyDome(false, m_sSkyBoxName);
			break;
		case GLE_SkyPlane_Create:
			Ogre::Plane plane;
			plane.d = 1000;
			plane.normal = Ogre::Vector3::NEGATIVE_UNIT_Y;

			pNode = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyPlaneNode();
			pRootSceneNode->removeChild(pNode);
			if(closeAll)
				m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->setSkyPlane(false, plane, m_sSkyBoxName);
			break;
		}
	}

	EditorMsgManager_SkyBoxSubSystem::EditorMsgManager_SkyBoxSubSystem(Editor* pEditor)
	{
		m_pEditor = pEditor;
		m_currentProtcol = GGE_Ready;
		node = NULL;
	}
	EditorMsgManager_SkyBoxSubSystem::~EditorMsgManager_SkyBoxSubSystem()
	{

	}

	bool EditorMsgManager_SkyBoxSubSystem::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_SkyPlane_Create && statement != GLE_SkyDome_Create && statement != GLE_SkyBox_Create)
			return false;

		CloseLastSkyBox();
		m_sSkyBoxName = tokens[2];

		// save current status for saving
		m_sCurrentStatus = tokens[0] + "||" + tokens[1] + "||" + tokens[2] + "||" + tokens[3] + "||" + tokens[4] + "||" + tokens[5] + "||" + tokens[6] + "||" + tokens[7] + "||" + tokens[8] + "||" + tokens[9];
		if(statement == GLE_SkyBox_Create)
		{
			m_currentProtcol = GLE_SkyBox_Create;
			float dist = (float)atof(tokens[3].c_str());
			int dispI = atoi(tokens[4].c_str());
			m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->setSkyBox(true, tokens[2], dist, (bool)dispI);

			node = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyBoxNode();	
			// Do something to save the current state of the skybox/dome/plane
		}

		if(statement == GLE_SkyDome_Create)
		{
			m_currentProtcol = GLE_SkyDome_Create;
			float curv = (float)atof(tokens[5].c_str());
			float tile = (float)atof(tokens[6].c_str());
			m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->setSkyDome(true, tokens[2], curv, tile);
			node = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyDomeNode();
			// Do something to save the current state of the skybox/dome/plane
		}

		if(statement == GLE_SkyPlane_Create)
		{
			m_currentProtcol = GLE_SkyPlane_Create;
			float curv = (float)atof(tokens[5].c_str());
			float tile = (float)atof(tokens[6].c_str());
			int dispI = atoi(tokens[4].c_str());
			float siz = (float)atof(tokens[7].c_str());
			int segX = atoi(tokens[8].c_str());
			int segY = atoi(tokens[9].c_str());

			Ogre::Plane plane;
			plane.d = 1000;
			plane.normal = Ogre::Vector3::NEGATIVE_UNIT_Y;

			m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->setSkyPlane(true, plane, tokens[2], siz, tile, (bool)dispI, curv, segX, segY);

			node = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getSkyPlaneNode();
			// Do something to save the current state of the skybox/dome/plane
		}

		return true;
	}

	bool EditorMsgManager_SkyBoxSubSystem::AskCurrentStatusMessage(EditorProtocol protocolHandle, std::string &retString)
	{
		if(protocolHandle != GLE_SkyPlane_Create && protocolHandle != GLE_SkyDome_Create && protocolHandle != GLE_SkyBox_Create)
			return false;

		retString = m_sCurrentStatus;
		return true;
	}

	bool EditorMsgManager_SkyBoxSubSystem::PreprocessSave(EditorProtocol protocolHandle)
	{
		if(protocolHandle != GLE_SkyPlane_Create && protocolHandle != GLE_SkyDome_Create && protocolHandle != GLE_SkyBox_Create)
			return false;

		Ogre::SceneNode* Rootnode = m_pEditor->m_pEngine->GetForemostGameScreen()->GetDefaultSceneManager()->getRootSceneNode();
		if(node)
		{
			if(node->getParentSceneNode() != Rootnode)
				Rootnode->addChild((Ogre::Node*)node);
		}
		return true;
	}
	bool EditorMsgManager_SkyBoxSubSystem::PostprocessSave(EditorProtocol protocolHandle)
	{
		if(protocolHandle != GLE_SkyPlane_Create && protocolHandle != GLE_SkyDome_Create && protocolHandle != GLE_SkyBox_Create)
			return false;

		CloseLastSkyBox(false);
		return true;
	}

}