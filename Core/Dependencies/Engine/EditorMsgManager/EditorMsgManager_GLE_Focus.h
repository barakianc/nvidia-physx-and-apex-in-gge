#ifndef CH_GLE_Focus_h
#define CH_GLE_Focus_h

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_GLE_Focus : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLE_Focus(Editor* pEditor);
		~EditorMsgManager_GLE_Focus();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif