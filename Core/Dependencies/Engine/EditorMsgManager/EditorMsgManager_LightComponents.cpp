#include "StdAfx.h"
#include "EditorMsgManager_LightComponents.h"

namespace GamePipe
{
	EditorMsgManager_LightComponents::EditorMsgManager_LightComponents(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_LightComponents::~EditorMsgManager_LightComponents()
	{

	}

	bool EditorMsgManager_LightComponents::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());

		if(statement == GLE_Light_Create)
		{
			string sName = tokens[2];

			GGETRACELOG("GLE_Light_Create: Creating light : %s", sName.c_str());



			Ogre::Light* l;
			if (m_pEditor->GetSceneManager()->hasLight(Ogre::String(sName)))
				l = m_pEditor->GetSceneManager()->getLight(Ogre::String(sName));
			else
				l = m_pEditor->GetSceneManager()->createLight(Ogre::String(sName));

			
			// By request, if a scene node is selected, that scene node should parent the new light
			if(m_pEditor->m_pSceneNodeSelected != NULL)
			{
				l->detachFromParent();
				m_pEditor->m_pSceneNodeSelected->attachObject(l);
				return true;
			}
			
			string sceneNodeName = sName; 
			//sceneNodeName.append("_Node");
			sceneNodeName.append("_GLE");

			Ogre::SceneNode* node;
			if (m_pEditor->GetSceneManager()->hasSceneNode(sceneNodeName))
				node = m_pEditor->GetSceneManager()->getSceneNode(sceneNodeName);
			else
				node = m_pEditor->GetSceneManager()->getRootSceneNode()->createChildSceneNode(sceneNodeName);

			//Ogre::Entity* ent = GetSceneManager()->createEntity(Ogre::String(name),Ogre::String(filename));
			l->detachFromParent();
			node->attachObject(l);
			return true;
		}
		if(statement == GLE_Light_Type)
		{
			string			sName = tokens[2];
			int				iType = atoi(tokens[3].c_str());//ParseInt( tokens[3] );

			GGETRACELOG("GLE_Light_Create: Name : %s Type : %d", sName.c_str(), iType);

			if( m_pEditor->GetSceneManager()->hasLight( Ogre::String(sName) ) )
			{
				Ogre::Light* pLight = m_pEditor->GetSceneManager()->getLight(Ogre::String(sName));
				pLight->setType( (Ogre::Light::LightTypes)iType );
			}else{
				m_pEditor->Editor_ReportErrorToGLE("Light does not exist");
			}

			return true;
		}
		if(statement == GLE_Light_Position)
		{
			string		sName = tokens[2];
			float			fX = (float)atof(tokens[3].c_str()); //ParseFloat( tokens[3] );
			float			fY = (float)atof(tokens[4].c_str()); //ParseFloat( tokens[4] );
			float			fZ = (float)atof(tokens[5].c_str()); //ParseFloat( tokens[5] );

			GGETRACELOG("GLE_Light_Position: Name : %s Position : %f, %f, %f", sName.c_str(), fX, fY, fZ);

			if( m_pEditor->GetSceneManager()->hasLight( Ogre::String(sName) ) )
			{
				Ogre::Light* pLight = m_pEditor->GetSceneManager()->getLight(Ogre::String(sName));
				pLight->setPosition(fX,fY,fZ);
			}else{
				m_pEditor->Editor_ReportErrorToGLE("Light does not exist");
			}
			return true;
		}
		if(statement == GLE_Light_Direction)
		{
			string		sName = tokens[2];
			float			fX = (float)atof(tokens[3].c_str()); //ParseFloat( tokens[3] );
			float			fY = (float)atof(tokens[4].c_str()); //ParseFloat( tokens[4] );
			float			fZ = (float)atof(tokens[5].c_str()); //ParseFloat( tokens[5] );

			GGETRACELOG("GLE_Light_Direction: Name : %s Direction : %f, %f, %f", sName.c_str(), fX, fY, fZ);

			if( m_pEditor->GetSceneManager()->hasLight( Ogre::String(sName) ) )
			{
				Ogre::Light* pLight = m_pEditor->GetSceneManager()->getLight(Ogre::String(sName));
				pLight->setDirection(fX,fY,fZ);
			}else{
				m_pEditor->Editor_ReportErrorToGLE("Light does not exist");
			}
			return true;
		}
		if(statement == GLE_Light_Color_Diffuse)
		{
			string		sName = tokens[2];
			float			fR = (float)atof(tokens[3].c_str()); //ParseFloat( tokens[3] );
			float			fG = (float)atof(tokens[4].c_str()); //ParseFloat( tokens[4] );
			float			fB = (float)atof(tokens[5].c_str()); //ParseFloat( tokens[5] );

			GGETRACELOG("GLE_Light_Color_Diffuse: Name : %s Color : %f, %f, %f", sName.c_str(), fR, fG, fB);

			if( m_pEditor->GetSceneManager()->hasLight( Ogre::String(sName) ) )
			{
				Ogre::Light* pLight = m_pEditor->GetSceneManager()->getLight(Ogre::String(sName));
				pLight->setDiffuseColour(fR,fG,fB);
			}else{
				m_pEditor->Editor_ReportErrorToGLE("Light does not exist");
			}
			return true;
		}
		if(statement ==GLE_Light_Color_Specular)
		{
			Ogre::String	sName = tokens[2];
			float			fR = (float)atof(tokens[3].c_str()); //ParseFloat( tokens[3] );
			float			fG = (float)atof(tokens[4].c_str()); //ParseFloat( tokens[4] );
			float			fB = (float)atof(tokens[5].c_str()); //ParseFloat( tokens[5] );

			GGETRACELOG("GLE_Light_Color_Specular: Name : %s Color : %f, %f, %f", sName.c_str(), fR, fG, fB);

			if( m_pEditor->GetSceneManager()->hasLight( sName ) )
			{
				Ogre::Light* pLight = m_pEditor->GetSceneManager()->getLight(sName);
				pLight->setSpecularColour(fR,fG,fB);
			}else{
				m_pEditor->Editor_ReportErrorToGLE("Light does not exist");
			}
			return true;
		}

		return false;
	}
}