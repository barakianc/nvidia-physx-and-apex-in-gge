#include "StdAfx.h"
#include "EditorMsgManager_GLE_Set_Object_Properties.h"

namespace GamePipe
{
	EditorMsgManager_GLE_Set_Object_Properties::EditorMsgManager_GLE_Set_Object_Properties(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_Set_Object_Properties::~EditorMsgManager_GLE_Set_Object_Properties()
	{

	}

	bool EditorMsgManager_GLE_Set_Object_Properties::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());

		if(statement != GLE_Set_Object_Properties)
			return false;
		
		// Parse the packet
		string name = tokens[2];
		int type = atoi(tokens[3].c_str());

		GGETRACELOG("GLE_Set_Object_Properties: %s type: %d", name.c_str(), type);
		GGETRACELOG("tuple size");
		string stuples = tokens[4];
		vector<GLE_PropTuple> tuples = m_pEditor->ParseTuples(stuples);
		char tempName[10];
		_itoa_s(tuples.size(),tempName,10);
		GGETRACELOG(tempName);

		for (unsigned int i = 0; i < tuples.size(); i++)
		{
			switch (type)
			{
			case OBJTYPE_SceneNode:
				{
					Ogre::SceneNode* node = m_pEditor->GetSceneManager()->getSceneNode(Ogre::String(name));
					m_pEditor->SetSceneNodeProperty(tuples[i], node);
					break;
				}
			case OBJTYPE_Light:
				{
					Ogre::Light* light = m_pEditor->GetSceneManager()->getLight(Ogre::String(name));
					m_pEditor->SetLightProperty(tuples[i], light);
					break;
				}
			case OBJTYPE_Entity:
				{
					Ogre::Entity* entity = m_pEditor->GetSceneManager()->getEntity(Ogre::String(name));
					m_pEditor->SetEntityProperty(tuples[i],entity);
					break;
				}
			}
		}

		return true;

	}
}