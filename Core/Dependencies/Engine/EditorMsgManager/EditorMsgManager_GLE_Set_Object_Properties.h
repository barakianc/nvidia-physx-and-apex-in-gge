#ifndef CH_GLE_Set_Object_Properties_H
#define CH_GLE_Set_Object_Properties_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_GLE_Set_Object_Properties : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLE_Set_Object_Properties(Editor* pEditor);
		~EditorMsgManager_GLE_Set_Object_Properties();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif