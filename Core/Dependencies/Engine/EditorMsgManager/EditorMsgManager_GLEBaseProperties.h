#ifndef CH_GLEBaseProperties_H
#define CH_GLEBaseProperties_H

#include "IMessageHandle.h"

namespace GamePipe
{
	struct InitNodeConfig
	{
		Ogre::String NodeName;
		vector<GLE_PropTuple> tuples;
	};

	class EditorMsgManager_GLEBaseProperties : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLEBaseProperties(Editor* pEditor);
		~EditorMsgManager_GLEBaseProperties();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);

		void SaveInitSceneNodeConfig();
		void RestoreOriginalSceneNodeConfig();

		vector<InitNodeConfig> savedSceneNodeConfig;
	};
}

#endif