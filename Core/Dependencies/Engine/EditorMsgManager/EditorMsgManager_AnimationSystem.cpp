#include "StdAfx.h"
#include "EditorMsgManager_AnimationSystem.h"

namespace GamePipe
{
	EditorMsgManager_AnimationSystem::EditorMsgManager_AnimationSystem(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}	
	EditorMsgManager_AnimationSystem::~EditorMsgManager_AnimationSystem()
	{

	}

	bool EditorMsgManager_AnimationSystem::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());

		if(statement == MV_ShowBones)
		{
			m_pEditor->Editor_ReportUnimplemented(statement);
			// UNDONE Bones
			//MVShowBones(tokens);
			return true;
		}
		if(statement == MV_PlayAnimation)
		{
			if (!m_pEditor->MVPlayAnimation(tokens))
				return false;

			return true;
		}
		if(statement == MV_StopAnimation)
		{
			if(!m_pEditor->MVStopAnimation(tokens))
				return false;

			return true;
		}
		if(statement == MV_GetListAnimations)
		{
			if (!m_pEditor->MVGetListAnimations(tokens))
				return false;

			return true;
		}

		return false;
	}
}