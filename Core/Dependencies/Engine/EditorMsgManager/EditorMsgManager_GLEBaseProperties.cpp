#include "StdAfx.h"
#include "EditorMsgManager_GLEBaseProperties.h"

namespace GamePipe
{
	EditorMsgManager_GLEBaseProperties::EditorMsgManager_GLEBaseProperties(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLEBaseProperties::~EditorMsgManager_GLEBaseProperties()
	{

	}

	bool EditorMsgManager_GLEBaseProperties::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());

		if(statement == GLE_Save_Scene)
		{
			string file = tokens[2];

			GGETRACELOG("GLE_Save_Scene: %s", file.c_str());
			m_pEditor->PreprocessSave(GLE_SkyBox_Create);
			m_pEditor->m_pEngine->GetForemostGameScreen()->SaveScene(file);
			m_pEditor->PostprocessSave(GLE_SkyBox_Create);

			return true;
		}

		if(statement == GLE_Grid_Toogle)
		{
			m_pEditor->GridToggle(tokens);

			return true;
		}

		if(statement == GLE_SetSensibility)
		{
			GGETRACELOG("GLE_SetSensibility: Rotation - %s  Movement - %s", tokens[2].c_str(), tokens[3].c_str() );

			float fRotation = (float)atof(tokens[2].c_str());//ParseFloat(tokens[2]);
			float fMovement = (float)atof(tokens[3].c_str());//ParseFloat(tokens[3]);

			GGETRACELOG("GLE_SetSensibility: Rotation - %f  Movement - %f", fRotation, fMovement );

			m_pEditor->m_fCameraMovementSpeed = fMovement;
			m_pEditor->m_fCameraRotationSpeed = fRotation;

			return true;
		}

		if(statement == MV_Wireframe)
		{
			int iWireframe = atoi(tokens[2].c_str());//ParseInt(tokens[2]);

			GGETRACELOG("GLE_Wireframe: Value - %d", iWireframe);

			if(iWireframe==1)	m_pEditor->m_pEngine->GetForemostGameScreen()->GetActiveCamera()->setPolygonMode(Ogre::PM_WIREFRAME);
			else				m_pEditor->m_pEngine->GetForemostGameScreen()->GetActiveCamera()->setPolygonMode(Ogre::PM_SOLID);

			return true;
		}

		if(statement == GLE_Select)
		{
			string		sName = tokens[2];

			GGETRACELOG("GLE_Select: Name : %s", sName.c_str() );

			m_pEditor->SelectSceneNode( Ogre::String(sName) );
			return true;
		}
		if(statement == GLE_Camera_Set)
		{
			m_pEditor->CameraSet(tokens);
			return true;
		}
		if(statement == GLE_Set_GameMode)
		{
			// Activate default camera
			GGETRACELOG("GLE_Set_GameMode");
			//before setting the camera and changing the mode, save the initial scene node positions
			SaveInitSceneNodeConfig();

			m_pEditor->m_pEngine->GetForemostGameScreen(true)->SetActiveCamera(
				m_pEditor->m_pEngine->GetForemostGameScreen(true)->GetDefaultSceneManager()->getCamera(m_pEditor->m_pEngine->GetForemostGameScreen(true)->GetName() + "Camera"));
			m_pEditor->SetEditorMode(false);
			return true;
		}
		if(statement == GLE_Set_EditorMode)
		{
			GGETRACELOG("GLE_Set_EditorMode");

			// restore the objects to their original positions
			RestoreOriginalSceneNodeConfig();

			// Initialize cameras just in case the user has changed the screen during the game
			m_pEditor->InitEditorCameras(m_pEditor->m_pEngine->GetForemostGameScreen(true)->GetDefaultSceneManager(), true);
			// We should change this line below
			m_pEditor->m_pEngine->GetForemostGameScreen(true)->SetActiveCamera(
				m_pEditor->m_pEngine->GetForemostGameScreen(true)->GetDefaultSceneManager()->getCamera("Editor_CameraPerspective"));
			m_pEditor->SetEditorMode(true);
			return true;
		}
		if(statement == GLE_Window_Resize)
		{
			m_pEditor->m_pEngine->GetRenderWindow()->windowMovedOrResized();
			m_pEditor->m_pEngine->Show();
			return true;
		}
#ifdef GLE_EDITOR_MODELVIEW
		if(statement == MV_LoadResLocation)
		{
			m_pEditor->MVLoadResLocation(tokens);

			return true;
		}
		if(statement == MV_LoadMesh)
		{
			m_pEditor->MVLoadMesh(tokens);

			return true;
		}
		if(statement == MV_LoadColladaFile)
		{
			m_pEditor->MVLoadColladaFile(tokens);

			return true;
		}
		if(statement == MV_Camera_Set)
		{
			m_pEditor->Editor_ReportDeprecated(statement);

			m_pEditor->MVCameraSet(tokens);

			return true;
		}
#endif
		if(statement == MV_Clear)
		{
			// Just in case we had an animation
			m_pEditor->m_CurrentAnimationName = "";
			m_pEditor->m_sMVCurrentGameObjectName  = "";
			m_pEditor->m_pMVGameObject = NULL;
			return true;
		}

		if(statement == MV_BoundingBox_Toggle)
		{
			if (!m_pEditor->MVBoundingBoxToggle(tokens))
				return false;

			return true;
		}

		return false;
	}

	void EditorMsgManager_GLEBaseProperties::SaveInitSceneNodeConfig()
	{
		savedSceneNodeConfig.clear();
		vector<GLE_PropTuple> tuples;

		Ogre::SceneNode* node = m_pEditor->GetSceneManager()->getRootSceneNode();

		Ogre::Node::ChildNodeIterator children = node->getChildIterator();

		//problem with this is the order - we will have to store the name of the scene Node then
		while(children.hasMoreElements())
		{
			Ogre::Node* o = children.getNext();
			Ogre::SceneNode* tempNode;
			tempNode = m_pEditor->GetSceneManager()->getSceneNode(o->getName());
			tuples = m_pEditor->GetSceneNodeProperties(tempNode);
			
			InitNodeConfig temp;
			temp.NodeName = o->getName();
			temp.tuples = tuples;
			savedSceneNodeConfig.push_back(temp);
			//GGETRACELOG(o->getName().c_str());
		} 		
	}

	void EditorMsgManager_GLEBaseProperties::RestoreOriginalSceneNodeConfig()
	{
		for(unsigned int i = 0; i < savedSceneNodeConfig.size(); i++)
		{
			for(unsigned int j = 0; j < savedSceneNodeConfig.at(i).tuples.size(); j++)
			{
				m_pEditor->SetSceneNodeProperty(savedSceneNodeConfig.at(i).tuples[j],m_pEditor->GetSceneManager()->getSceneNode(savedSceneNodeConfig.at(i).NodeName));
			}
		}
	}
}