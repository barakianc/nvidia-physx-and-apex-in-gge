#ifndef CH_GLE_Query_SceneNode_Children_H
#define CH_GLE_Query_SceneNode_Children_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_GLE_Query_SceneNode_Children : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLE_Query_SceneNode_Children(Editor* pEditor);
		~EditorMsgManager_GLE_Query_SceneNode_Children();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif