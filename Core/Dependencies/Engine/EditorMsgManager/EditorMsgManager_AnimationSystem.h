#ifndef CH_AnimationSystem_H
#define CH_AnimationSystem_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_AnimationSystem : public IMessageHandle
	{
	public: 
		EditorMsgManager_AnimationSystem(Editor* pEditor);
		~EditorMsgManager_AnimationSystem();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif