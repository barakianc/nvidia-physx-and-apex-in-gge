#include "StdAfx.h"
#include "EditorMsgManager_ScreenActivationKeyboardInput.h"

namespace GamePipe
{
	EditorMsgManager_ScreenActivationKeyboardInput::EditorMsgManager_ScreenActivationKeyboardInput(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_ScreenActivationKeyboardInput::~EditorMsgManager_ScreenActivationKeyboardInput()
	{

	}

	bool EditorMsgManager_ScreenActivationKeyboardInput::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		
		if(statement == GLE_ActivateScreen)
		{
			string nm = tokens[2];
			GGETRACELOG("GLE_ActivateScreen: %s", nm.c_str());

			m_pEditor->m_pEngine->BringScreenToFront(nm);

			// Initialize cameras just in case the user has changed the screen during the game
			m_pEditor->InitEditorCameras(m_pEditor->m_pEngine->GetForemostGameScreen(true)->GetDefaultSceneManager(), true);

			return true;
		}

		if(statement == GLE_Input_Keyboard_Button)
		{
			bool state = m_pEditor->ParseBool(tokens[3]);
			int bt_id = atoi(tokens[2].c_str());

			m_pEditor->ed_KeyCode = bt_id;
			GIS::KeyCode kC = InputPtr->EditorKeyCodeTranslation(bt_id);						
			m_pEditor->ed_KeyPress = state;
			m_pEditor->ed_KeyStateBuffer[kC] = state;			

			InputPtr->EditorKeyRead();

			return true;

		}

		return false;
	}
}