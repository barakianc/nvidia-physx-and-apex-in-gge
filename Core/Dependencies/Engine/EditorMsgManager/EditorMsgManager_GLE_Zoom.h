#ifndef CH_GLE_Zoom_H
#define CH_GLE_Zoom_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_GLE_Zoom : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLE_Zoom(Editor* pEditor);
		~EditorMsgManager_GLE_Zoom();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif