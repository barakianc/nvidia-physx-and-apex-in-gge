#include "StdAfx.h"
#include "EditorMsgManager_GLE_Zoom.h"

namespace GamePipe
{
	EditorMsgManager_GLE_Zoom::EditorMsgManager_GLE_Zoom(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_Zoom::~EditorMsgManager_GLE_Zoom()
	{

	}

	bool EditorMsgManager_GLE_Zoom::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_Zoom)
			return false;

		// weight the mouse delta when its a recognized 'zoom' event only.
		int d = m_pEditor->ParseInt(tokens[2]);						
		Ogre::Camera *pCamera = m_pEditor->m_pEngine->GetForemostGameScreen()->GetActiveCamera();
		// the amount by which the distance will be scaled
		Ogre::Real scalar = 0;

		// for perspective cameras we calc the distance to the object and use it as a scalar
		if(pCamera->getName() == "Editor_CameraPerspective")
		{
			Ogre::Vector3 camera_loc = pCamera->getPosition();
			Ogre::Vector3 target_loc;



			// Is there an active entity
			if ( m_pEditor->m_pMVGameObject != NULL)
			{
				target_loc = m_pEditor->m_pMVGameObject->getPosition();

			}
			else
			{
				// get distance from camera to the origin
				target_loc = Ogre::Vector3::ZERO;

			}

			// get the squared distance to use as a scalar
			scalar = camera_loc.squaredDistance(target_loc);	

	#ifdef GLE_DEBUG_NETWORK
			GGETRACELOG("Distance: %f", (float)scalar);
	#endif

			// weight the scalar
			scalar *= (Ogre::Real)0.0005;
		}
		else // ortho cameras
		{
			// We cannot scale scrolling based on the distance as we do when dealing with the perspective
			// Instead we base it off of some predefined 'golden standard' that will represent base speed

			// set the golden standard
			Ogre::Real baseWidth = 0;

			// get the current value
			Ogre::Real currWidth = pCamera->getOrthoWindowWidth();

			// get the difference between the baseWidth and currWidth to be the 'distance'
			Ogre::Real widthDiff = currWidth - baseWidth + 10;

			// store diff as scalar
			scalar = widthDiff;

			// orther has issues backing up if zooming out increase it							

	#ifdef GLE_DEBUG_NETWORK
			GGETRACELOG("diff: %f", (float)scalar);
	#endif

			//reasoning behind scalar settings unclear -11/2/09
			scalar *= (Ogre::Real) 0.005;

	#ifdef GLE_DEBUG_NETWORK
			GGETRACELOG("scaled diff: %f", (float)scalar);
	#endif

		}


		//Purpose of scalar bounding unclear -11/2/09
		if ( scalar < .05)
			scalar = (Ogre::Real).05;

		if (scalar > 3.5)
			scalar = 3.5;


	#ifdef GLE_DEBUG_NETWORK
		GGETRACELOG("Zoom scalar: %f", (float)scalar);
	#endif
		// scale the delta based on the distance
		d = int(d * scalar);
					
		m_pEditor->ed_MouseState.Z.rel = d; //ed_MouseState.Z.abs-d;
		m_pEditor->ed_MouseState.Z.abs = 0;

		InputPtr->EditorMouseMoved();
		
		m_pEditor->ed_MouseState.Z.rel = 0;

		return true;
	}
}