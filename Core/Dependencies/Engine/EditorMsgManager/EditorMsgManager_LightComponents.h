#ifndef CH_LightComponents_H
#define CH_LightComponents_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_LightComponents : public IMessageHandle
	{
	public: 
		EditorMsgManager_LightComponents(Editor* pEditor);
		~EditorMsgManager_LightComponents();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif