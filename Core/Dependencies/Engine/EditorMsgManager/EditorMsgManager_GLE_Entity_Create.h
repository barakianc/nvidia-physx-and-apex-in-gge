#ifndef CH_GLE_Entity_Create_H
#define CH_GLE_Entity_Create_H

#include "IMessageHandle.h"

namespace GamePipe
{
	class EditorMsgManager_GLE_Entity_Create : public IMessageHandle
	{
	public: 
		EditorMsgManager_GLE_Entity_Create(Editor* pEditor);
		~EditorMsgManager_GLE_Entity_Create();

		bool ProcessMessage(std::vector<Ogre::String> &tokens);
	};
}

#endif