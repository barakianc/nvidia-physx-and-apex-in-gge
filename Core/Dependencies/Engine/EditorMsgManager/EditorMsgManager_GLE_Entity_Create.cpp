#include "StdAfx.h"
#include "EditorMsgManager_GLE_Entity_Create.h"

namespace GamePipe
{
	EditorMsgManager_GLE_Entity_Create::EditorMsgManager_GLE_Entity_Create(Editor* pEditor)
	{
		m_pEditor = pEditor;
	}
	EditorMsgManager_GLE_Entity_Create::~EditorMsgManager_GLE_Entity_Create()
	{

	}

	bool EditorMsgManager_GLE_Entity_Create::ProcessMessage(std::vector<Ogre::String> &tokens)
	{
		int statement = atoi(tokens[0].c_str());
		if(statement != GLE_Entity_Create)
			return false;

		string name;
		string parentName;
		int type;

		// Parse the packet
		name = tokens[2];
		type = atoi(tokens[3].c_str());
		if (name == "")
		{
			m_pEditor->Editor_ReportErrorToGLE( Ogre::String("Entity type not implemented.") );
			return true;
		}
		

		// Seems that this is the point where the collada file should be supported
		// So this might be a good place to start
		// Andres Ramirez E


		if (type == 2)  // Mesh from file
		{	//amf.MeshFile, amf.HavokFile, ((int)amf.ObjectType).ToString(), ((int)amf.shapeType).ToString(), amf.LuaFile
			// Parse the rest of the packet
			string meshFile = tokens[4];
			string havokFile = "";
			int objectType = atoi(tokens[6].c_str());
			int shapeType = atoi(tokens[7].c_str());
			string luaFile = "";

			havokFile += tokens[5];
			// there doesnt seem to be an implementation of the lua file system yet
			luaFile += tokens[8];
			
			string sceneNodeName = name; 
			//sceneNodeName.append("_Node");
			sceneNodeName.append("_GLE");
			Ogre::SceneManager *pSceneManager = m_pEditor->GetSceneManager();
			if (pSceneManager->hasSceneNode(sceneNodeName))
			{
				//node = pSceneManager->getSceneNode(sceneNodeName); 
				m_pEditor->SendSimpleMessage("Entity can't be created because there\nexists another entity with this name");
				return true;
			}

			Ogre::SceneNode* node = pSceneManager->getRootSceneNode()->createChildSceneNode(sceneNodeName);

			GameObject* pGameObject = new GameObject(name,meshFile,havokFile, (GameObjectType)objectType, (CollisionShapeType) shapeType, luaFile);
			pGameObject->changeSceneNode(node);
			Ogre::Vector3 parentPosition = node->getPosition();
			pGameObject->setPosition(parentPosition.x, parentPosition.y, parentPosition.z);

			GGETRACELOG("Created Entity");
			char str[10];
			_itoa_s(objectType,str,10);
			GGETRACELOG(str);
			_itoa_s(shapeType,str,10);
			GGETRACELOG(str);
			_itoa_s(pGameObject->m_pGraphicsObject->m_pOgreEntity->getNumSubEntities(),str,10);

			GGETRACELOG("Number of SubEntities");
			/*if(pGameObject->m_AIAgent != NULL)
			GGETRACELOG(str);
				m_pEditor->SendSimpleMessage("Entidad Creada " + pGameObject->m_pGraphicsObject->m_pOgreEntity->getName());*/
		}

		return true;
	}
}