#pragma once

#define MAX_PACKET_SIZE	128	

/*! Defines the protocol used by GGE and GLE to talk */

/// <summary>
/// All packets send back and forth between GGE and GLE
/// are identified by a packet ID that can be looked up 
/// in this enum. See docs for further explanation of 
/// each message
/// 
/// Since enums have a large range the protocol has offsets
/// Status Info     - 001
/// Inputs          - 101
/// Model Viewer    - 201
/// Game Editor     - 301
/// SceneNode       - 401
/// Entity          - 501
/// Lights          - 601
/// 
/// </summary>
/*! Packets */
enum EditorProtocol
	{
        // Status Info  - 001
		GGE_Ready = 1,
        GGE_Error = 2,
        GLE_Window_Resize = 3,
		GLE_StandardMessage = 4,
        // Inputs       - 101
		GLE_Input_Mouse_Button = 101,
		GLE_Input_Mouse_Position = 102,
		GLE_Input_Keyboard_Button = 103,
		GLE_Grid_Toogle = 104,
        GLE_Input_Mouse_Wheel = 105,
        GLE_Zoom = 106,
        GLE_Focus = 107,
     // Model Viewer - 201
        MV_LoadMesh = 201,
        MV_Clear = 202,
        MV_PlayAnimation = 203,
        MV_StopAnimation = 204,
        MV_GetListAnimations = 205,
        MV_GLE_AnimationList = 206,
        MV_BoundingBox_Toggle = 207,
        MV_Camera_Set = 208,
        MV_LoadParticleSystem = 209,
        MV_ClearParticleSystem = 210,
        MV_ShowBones = 211,
        MV_GetListParticleSystems = 212,
        MV_GLE_ParticleSystemsList = 213,
        MV_LoadResLocation = 214,
        MV_Wireframe = 215,
		MV_LoadColladaFile = 216,
		// Game Editor  - 301
	    GLE_Context_SceneManager_Set = 301,
	    GLE_Context_SceneManager_Get = 302,
	    GLE_Context_Screen_Set = 303,
	    GLE_Context_Screen_Get = 304,
	    GLE_Context_Game_Set = 305,
	    GLE_Context_Game_Get = 306,
	    GLE_Context_Scene_Set = 307,
	    GLE_Context_Scene_Get = 308,
	    GLE_Context_SceneNode_Set = 309,
	    GLE_Context_SceneNode_Get = 310,
		GLE_Query_Object_Properties = 311,
		GGE_Response_Object_Properties = 312,
        GLE_ActivateScreen = 313,
        GLE_Camera_Set = 314,
        GLE_Set_Object_Properties = 315,
        GLE_Save_Scene = 316,
        GLE_Query_Camera_Properties = 317,
        GGE_Response_Camera_Properties = 318,
        GLE_Set_GameMode = 319,
        GLE_Set_EditorMode = 320,
        GGE_ActivatedScreen = 321,
        GLE_SetSensibility = 322,
        GLE_Select = 324,
        GGE_Picked = 325,
        // Scene Node   - 401
		GLE_SceneNode_Translation = 401,
		GLE_SceneNode_RotationQuat = 402,
		GLE_SceneNode_Scale = 403,
		GLE_SceneNode_RotationDeg = 404,
		GLE_SceneNode_Create = 405,
		GLE_SceneNode_Del = 406,
		GLE_Query_SceneNode_Children = 407,
		GGE_Response_SceneNode_Children = 408, 
		GGE_Activate_Dynamic_Tool = 409,
        GGE_Deactivate_Dynamic_Tool = 410,
		GLE_Dynamic_Editing_Tool_Select = 411,
        GLE_Dynamic_Editing_Tool_Movement = 412,
        // Entity   - 501
		GLE_Entity_Create = 501,
		GLE_Entity_Del = 502,
		GLE_AttachObject = 503,
        // Lights   - 601
		GLE_Light_Create = 601,
		GLE_Light_Type = 602,
		GLE_Light_Position = 603,
		GLE_Light_Color_Diffuse = 604,
		GLE_Light_Color_Specular = 605,
		GLE_Light_Direction = 606,
		// SkyBox
        GLE_SkyBox_Create = 701,
		GLE_SkyDome_Create = 702,
		GLE_SkyPlane_Create = 703,
	};

/*! Object properties */
#define PROP_Name 0
#define PROP_Type 1
#define PROP_Pos_X 2
#define PROP_Pos_Y 3
#define PROP_Pos_Z 4
#define PROP_RotationQuat_X 5
#define PROP_RotationQuat_Y 6
#define PROP_RotationQuat_Z 7
#define PROP_RotationQuat_W 8
#define PROP_Rotation_Y 9
#define PROP_Rotation_X 10
#define PROP_Rotation_Z 11
#define PROP_Scale_X 12
#define PROP_Scale_Y 13
#define PROP_Scale_Z 14
#define PROP_Color_Diff_R 15
#define PROP_Color_Diff_G 16
#define PROP_Color_Diff_B 17
#define PROP_Color_Spec_R 18
#define PROP_Color_Spec_G 19
#define PROP_Color_Spec_B 20
#define PROP_MovableType 21
#define PROP_Light_Type 22
#define PROP_Direction_X 23
#define PROP_Direction_Y 24
#define PROP_Direction_Z 25
#define PROP_Material 26
#define PROP_Shadow 27

/*! Object Types */
#define OBJTYPE_Unknown -1
#define OBJTYPE_SceneNode 0
#define OBJTYPE_Light 1
#define OBJTYPE_Entity 2
#define OBJTYPE_Camera 3
