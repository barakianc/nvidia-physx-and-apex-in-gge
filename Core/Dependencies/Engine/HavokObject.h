
#ifndef _HAVOK_OBJECT_H_
#define _HAVOK_OBJECT_H_

#ifdef HAVOK

#include "GameObjectManager.h"

///Include files for Dynamics includes
#include <Physics/Collide/hkpCollide.h>//STILL										
#include <Physics/Collide/Agent/ConvexAgent/SphereBox/hkpSphereBoxAgent.h>//STILL									
#include <Physics/Collide/Query/CastUtil/hkpWorldRayCastInput.h>//STILL			
#include <Physics/Collide/Query/CastUtil/hkpWorldRayCastOutput.h>//STILL								
#include <Physics/Collide/Shape/HeightField/SampledHeightField/hkpSampledHeightFieldShape.h>//STILL 
#include <Physics/Collide/Shape/HeightField/SampledHeightField/hkpSampledHeightFieldBaseCinfo.h>//STILL 
#include <Physics/Utilities/CharacterControl/CharacterProxy/hkpCharacterProxy.h>//STILL 
#include <Physics/Utilities/CharacterControl/CharacterRigidBody/hkpCharacterRigidBody.h>//STILL 
#include <Physics/Utilities/CharacterControl/StateMachine/hkpDefaultCharacterStates.h>//STILL 
#include <Physics/Collide/Shape/Convex/Capsule/hkpCapsuleShape.h>//STILL
#include <Physics/Collide/Shape/Convex/Cylinder/hkpCylinderShape.h>//STILL
#include <Physics/Collide/Shape/Misc/Transform/hkpTransformShape.h>//STILL
#include <Physics/Dynamics/Phantom/hkpSimpleShapePhantom.h>//STILL
#include <Physics/Collide/Query/Collector/PointCollector/hkpClosestCdPointCollector.h>//STILL
#include <Common/Base/Algorithm/PseudoRandom/hkPseudoRandomGenerator.h>//STILL
#include <Physics/Collide/Agent/hkpProcessCollisionInput.h>//STILL
#include <Physics/Collide/Shape/Misc/PhantomCallback/hkpPhantomCallbackShape.h>//STILL
#include <Physics/Collide/Shape/Misc/Bv/hkpBvShape.h>//STILL
#include <Physics/Collide/Agent/ConvexAgent/BoxBox/hkpBoxBoxAgent.h>//STILL
#include <Physics/Collide/Agent/MiscAgent/Phantom/hkpPhantomAgent.h>//STILL
#include <Physics/Dynamics/Phantom/hkpAabbPhantom.h>//STILL

///Include files for Vehicle includes

///// Vehicle Basics
#include <Physics/Vehicle/hkpVehicle.h>
#include <Physics/Vehicle/hkpVehicleData.h>
#include <Physics/Vehicle/hkpVehicleInstance.h>
/// Vehicle Aerodynamics
#include <Physics/Vehicle/AeroDynamics/Default/hkpVehicleDefaultAerodynamics.h>
/// Vehicle Brakes
#include <Physics/Vehicle/Brake/Default/hkpVehicleDefaultBrake.h>
/// Vehicle Cameras
#include <Physics/Vehicle/Camera/hkp1dAngularFollowCamCinfo.h>
/// Vehicle DriverInput
#include <Physics/Vehicle/DriverInput/Default/hkpVehicleDefaultAnalogDriverInput.h>
/// Vehicle Engine
#include <Physics/Vehicle/Engine/Default/hkpVehicleDefaultEngine.h>
/// Vehicle Friction
#include <Physics/Vehicle/Friction/hkpVehicleFriction.h>
/// Vehicle Manager
#include <Physics/Vehicle/Manager/hkpVehicleCastBatchingManager.h>
#include <Physics/Vehicle/Manager/LinearCastBatchingManager/hkpVehicleLinearCastBatchingManager.h>
#include <Physics/Vehicle/Manager/MultithreadedVehicle/hkpMultithreadedVehicleManager.h>
#include <Physics/Vehicle/Manager/MultithreadedVehicle/hkpVehicleIntegrateJob.h>
#include <Physics/Vehicle/Manager/MultithreadedVehicle/hkpVehicleJobQueueUtils.h>
#include <Physics/Vehicle/Manager/MultithreadedVehicle/hkpVehicleJobs.h>
/// Vehicle Steering
#include <Physics/Vehicle/Steering/Default/hkpVehicleDefaultSteering.h>
/// Vehicle Suspension
#include <Physics/Vehicle/Suspension/Default/hkpVehicleDefaultSuspension.h>
/// Vehicle Transmission
#include <Physics/Vehicle/Transmission/Default/hkpVehicleDefaultTransmission.h>
/// Vehicle TyreMarks
#include <Physics/Vehicle/TyreMarks/hkpTyremarksInfo.h>
/// Vehicle VelocityDamper
#include <Physics/Vehicle/VelocityDamper/Default/hkpVehicleDefaultVelocityDamper.h>
/// Vehicle WheelCollide
#include <Physics/Vehicle/WheelCollide/LinearCast/hkpVehicleLinearCastWheelCollide.h>
#include <Physics/Vehicle/WheelCollide/RayCast/hkpVehicleRayCastWheelCollide.h>
#include <Physics/Vehicle/WheelCollide/RejectChassisListener/hkpRejectChassisListener.h>
///RDS Vehicle added
#include <Physics/Dynamics/Constraint/hkpConstraintInstance.h>
#include <Physics/Dynamics/Constraint/ConstraintKit/hkpGenericConstraintData.h>
#include <Physics/Dynamics/Constraint/ConstraintKit/hkpConstraintConstructionKit.h>
///RDS Mcycle added
#include <Common/Base/Types/Geometry/hkStridedVertices.h>//for hkStridedVertices
//#include <Physics/Collide/Shape/Convex/ConvexVertices/hkpConvexVerticesShape.h>//for hkpConvexVerticesShape and hkStridedVertices
/**********************************************HavokObjectDeclarationsSTART*********************************************/
/// This is the base class from which every Havok entity inherits,these functions will have EMPTY definitions in HavokWrapper.cpp
class PhysicsObject 
{
public:
	//PhysicsObject(){};//Constructor
	virtual void				setPosition(hkVector4& f_rPosition)=0;
	virtual hkVector4			getPosition()=0;
	virtual hkpWorld*			getPhysicsWorld()=0;
	virtual hkpRigidBody*		getRigidBody()=0;
	virtual bool				scale()=0;
	virtual bool				update()=0;
	virtual void				destroy()=0;
};
/**********************************************HavokObjectDeclarationsEND*********************************************/
/**************************************HavokObjectHelpFunctionsDeclarationsSTART**************************************/
void havokPackFileLoader(hkPackfileData** f_pPackfileData,hkpPhysicsData** f_pPhysicsData,std::string f_sHKXFileName,std::string f_sHavokExtraFolderName);
std::string grabHavokEntityNames(std::string f_sHKXFileName,std::string f_sHavokExtraFolderName="");
/*************************************HavokObjectHelpFunctionsDeclarationsEND*************************************/
/******************************************PhysicsObjectDeclarationsSTART*****************************************/
///This class creates a PhysicsObject and adds it to the hkpWorld either directly from code or then by parsing the hkx file
class PhysicsPrimitiveObject :public PhysicsObject
{
private :
	///Destructor
	~PhysicsPrimitiveObject();	

	///A private initializer for setting every member to initial values
	void initializePhysicsPrimitive();

	///The Physics World is obtained from the GameObjectManager
	hkpWorld*			m_pPhysicsWorld;

	///A rigidBody Object that comes either from code or from an hkx file
	hkpRigidBody*		m_pRigidBodyObject;
	
	///A pointer to physics System for complex bridge-like stuff
	hkpPhysicsSystem*	m_pPhysicsSystem;
	
	///A pointer for the graphicsObject of this gameObject-->needed for updating the graphics object
	GraphicsObject*		m_pGraphicsObject;

	///This is for knowing what the GameObject type is assigned to this PhysicsObject
	GameObjectType		m_eGameObjectType;

	///This is for being able to scale the objects which are created through code
	CollisionShapeType	m_eCollisionShapeType;

	///The base orientation of the GraphicsObject that we should use when updating the orientation of it
	Ogre::Quaternion*	m_vBaseOrientation;

	///The base offset of the GraphicsObject that we should use when updating the orientation of it
	hkVector4			m_vCharacterSceneNodeOffset;

	///The offset of rigidbody to adjust the position
	///added in 2011
	hkVector4			m_vBoxShapeOffset;

	///Position for key framed objects
	hkVector4			m_vPosition;

	///Orientation for key framed objects
	hkQuaternion		m_fQuaternion;

public:
	///Base constructor that calls different constructor helper functions according to the parameters passed to it
	PhysicsPrimitiveObject(
								std::string				f_sOgreUniqueName,
								std::string				f_sMeshFileName,
								std::string				f_sHKXFileName,
								GameObjectType			f_eHavokObjectType,
								CollisionShapeType		f_eCollisionShapeType,
								GraphicsObject**		f_pGraphicsObject
		  				); 

	///Base constructor that calls different constructor helper functions according to the parameters passed to it using LOD values and meshes
	PhysicsPrimitiveObject(
								std::string				f_sOgreUniqueName,
								std::vector<std::string>	f_eMeshFileNames,
								std::vector<float>		f_eMeshLODValues,
								std::string				f_sHKXFileName,
								GameObjectType			f_eHavokObjectType,
								CollisionShapeType		f_eCollisionShapeType,
								GraphicsObject**		f_pGraphicsObject
		  				); 

	///This function returns the PhysicsWorld object of this PhysicsObject instance
	hkpWorld* getPhysicsWorld()
	{
		return m_pPhysicsWorld;
	}

	///This function returns the RigidBodyObject member of this instance
	hkpRigidBody* getRigidBody()
	{
		return m_pRigidBodyObject;
	}

	///This function returns the PhysicsSystem member of this instance
	hkpPhysicsSystem* getPhysicsSystem()
	{
		return m_pPhysicsSystem;
	}

	///This function returns the object type of this instance
	GameObjectType getObjectType()
	{
		return m_eGameObjectType;
	}
	///This function returns the current position of the RigidBodyObject of this PhysicsObject instance
	hkVector4 getPosition();

	///This function sets the current position of RigidBodyObject of this PhysicsObject instance
	void setPosition(hkVector4& f_rPosition);

	///This function sets either the rigid body's or all physics systems mass
	void setMass(float mass);

	//Below function and variables are for key framed objects
private :
	
	///The custom function pointer to be called at every update/when applyKeyframed is called
	Custom_Keyframed_Function m_pCustomKeyframedFunction;
	
	///For being able to activate/disable key framing
	bool m_bKeyframeActive;

public :

	///This function is here for Networked havok and Ogre animation
	void setKeyFrame(hkVector4& f_rPosition, hkQuaternion& f_rQuaternion);

	///This function sets the pointer to your custom function
	void setCustomKeyframedFunction(void* f_pNewCustomKeyFramedFunction,bool f_bKeyframedEnabled);

	///For activating/disabling key framing
	void keyframeEnableDisable(bool f_bKeyframedEnabled)
	{
		m_bKeyframeActive = f_bKeyframedEnabled;
	}

	//Below functions are the abstract function of PhysicsObject-->has to be implemented for all child classes

	///The update function that is being called at every update of GameObjectManager
	bool update();

	///The destroy function that is being when a GameObject is destroyed
	void destroy();

	///This function will scale the physics representation of the GameObject if possible(if not created by hkx file)
	///Else it returns false to mention it couldn't apply the changes
	bool scale();

private :

	//Following functions are private constructor helper functions
	//After analyzing the parameters passed to the constructor
	//We will choose the appropriate function to call
	
	//CONSTRUCTOR_PHYSICSPRIMITIVE_1-->if you search for this line in PhysicsObject.cpp it takes you to the start of this constructor
	///This constructor creates and returns a PhysicsObject instance
	///The GraphicsObject in the constructor is needed to save the pointer inside the PhysicsObject
	bool SingleRigidBodyConstructor(//DONE
		std::string				f_sOgreUniqueName,
		std::string				f_sMeshFileName,
		hkpPhysicsSystem*		f_pCurrentPhysicsSystem,
		GameObjectType			f_eHavokObjectType
	);

	//CONSTRUCTOR_PHYSICSPRIMITIVE_2-->if you search for this line in PhysicsObject.cpp it takes you to the start of this constructor
	///This constructor creates a complex GameObject instance, using a single .hkx file (with one or multiple rigid bodies)
	///to instantiate a composite object using a single mesh file with multiple components, assigning each component a rigid body
	///the GameObject type must be a PhysicsSystem
	///The GraphicsObject in the constructor is needed to create the GraphicsObject so that it is a pointer to pointer
	bool MultipleRigidBody_MultipleEntity(							
		std::string				f_sOgreUniqueName,
		std::string				f_sMeshFileName,
		hkpPhysicsSystem*		f_pCurrentPhysicsSystem,
		GameObjectType			f_eHavokObjectType
		);

	//CONSTRUCTOR_PHYSICSPRIMITIVE_3-->if you search for this line in PhysicsObject.cpp it takes you to the start of this constructor
	///This constructor creates a GameObject instance, by creating an Ogre GraphicsObject and a PhysicsObject
	///using Ogre and Havok primitives to create everything from scratch. It doesn't use a hkx file	or a mesh file
	///The GraphicsObject in the constructor is needed to create the GraphicsObject so that it is a pointer to pointer
	bool NoHkx_ShapeRigidBody(
		std::string				f_sOgreUniqueName,
		std::string				f_sMeshFileName,
		CollisionShapeType		f_eCollisionShapeType,
		GameObjectType			f_eHavokObjectType
		);

	//Sub functions for CONSTRUCTOR PHYSICSOBJECT 3. These will be used to create Havok objects from code.

	//These functions will create rigidBodies and add them into PhysicsWorld
	//The size of these shapes will be applied according to the size of the entity under GraphicsObject that is passed as a parameter

	///Creates a collision sphere shape according to the entity created by a mesh file, when there is no .hkx file created
	void addSphere(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType);

	///This function will re-create the collision shape according to the scaled entity
	bool scaleSphere();

	///Creates a collision capsule shape according to the entity created by a mesh file, when there is no .hkx file created
	void addCapsule(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType);

	///This function will re-create the collision shape according to the scaled entity
	bool scaleCapsule();

	///Creates a collision cylinder shape according to the entity created by a mesh file, when there is no .hkx file created
	void addCylinder(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType);

	///This function will re-create the collision shape according to the scaled entity
	bool scaleCylinder();

	///Creates a collision box shape according to the entity created by a mesh file, when there is no .hkx file created
	void addBox(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType);

	///This function will re-create the collision shape according to the scaled entity
	bool scaleBox();

	//These functions will not be used for now.
	//These functions below creates a unit sized (e.g. 1,1,1) shape
	void addGround();
	void addSphere();
	void addCapsule();
	void addCylinder();
	void addBox();
	void addConvexHull();
};
/******************************************PhysicsObjectDeclarationsEND*****************************************/
/**************************************CharacterProxyObjectDeclarationsSTART************************************/
class MyCharacterProxyListener : public hkReferencedObject, public hkpCharacterProxyListener
{
private :
	///Pointer to custom collision call back function.
	Custom_CharacterProxy_Collision_Callback_Function		m_fCustom_contactPointAddedCallback_Function;

	///Pointer to custom collision call back function.
	Custom_CharacterProxy_Collision_Callback_Function		m_fCustom_contactPointRemovedCallback_Function;
public:
	HK_DECLARE_CLASS_ALLOCATOR(HK_MEMORY_CLASS_DEMO);

	MyCharacterProxyListener(CharacterProxyObject* f_pOwnerCharacterProxy);
	CharacterProxyObject* m_pOwnerCharacterProxy;
	// Ladder handling code goes here
	void contactPointAddedCallback( const hkpCharacterProxy* proxy, const hkpRootCdPoint& point );
	void contactPointRemovedCallback( const hkpCharacterProxy* proxy, const hkpRootCdPoint& point);

	void addCustom_contactPointAddedCallback_Function(void* f_fNewCustom_contactPointAddedCallback_Function);
	void addCustom_contactPointRemovedCallback_Function(void* f_fNewCustom_contactPointRemovedCallback_Function);
public:
	hkBool m_bAtLadder;
	hkVector4 m_vLadderNorm;
	hkVector4 m_vLadderVelocity;
};

class CharacterProxyObject :public PhysicsObject
{///This class creates a CharacterProxy and adds it to the physics World
private:
	///Enumeration for creating collision layers between HavokObjects
	enum CharacterShapeStateType
	{
		PROXY_STAND_SHAPE			= 1,//,//
		PROXY_AI_SHAPE				= 2,//,//
		PROXY_CRAWL_SHAPE			= 3//
	};
	bool switchCharacterProxyShape(CharacterShapeStateType f_eNewShapeType);
	///Create capsule
	void createCapsule();
protected:
	hkReal		m_iLeftPlus_RightMinus;				// left or right
	hkReal		m_iForwardMinus_BackPlus;			// forward or backward
	hkReal		m_fRotAngle_LeftPlus_RightMinus;	// changed orientation angle derived from mouse movement
	bool		m_bJUMP;							// whether jumping or not

	///The physics world to which the CharacterProxy is added
	hkpWorld*				m_pPhysicsWorld;

	///An instance of the CharacterProxy
	hkpCharacterProxy*		m_pCharacterProxy;

	///An instance to get collision information for CharcterProxy
	MyCharacterProxyListener* m_pCharacterProxyListener;

	///A pointer for the graphicsObject of this gameObject-->needed for updating the graphics object
	GraphicsObject*			m_pGraphicsObject;

	///The CharacterProxy context
	hkpCharacterContext*	m_pCharacterContext;

	///The phantom shapes used for collision purpose
	CharacterShapeStateType m_eCharacterShapeState;
	hkpShape*				m_pStandShape;
	hkpShape*				m_pHalfSizeShape;
	hkpShape*				m_pAIShape;
	hkpShapePhantom*		m_pPhantomShape;

	///The base properties of a Character, not to hard code the character gravity(mass) and forward direction
	hkVector4				m_vBaseCharacterForward;
	hkReal					m_fCharacterGravityScaleFactor;
	hkVector4				m_vCharacterSceneNodeOffset;

	///This variable will be multiplied by the direction of velocity
	hkReal					m_fVelocity;

	///This stores the current orientation angle
	hkReal					m_fCurrentAngle;

	///This is the base character looking direction. if the character is not looking at x=1 you should set this to sth else
	hkReal					m_fBaseLookingAngle;


	///An hkpNormal
	hkVector4				m_vNormal;

	///This buffer is used to store the state of the CharacterProxy-> just for debugging info purposes
	std::string				m_sStatus;

	bool flycheatflag;

	///Pointer to custom collision call back function.
	///Needed when the character needs to be resized
	Custom_CharacterProxy_Collision_Callback_Function		m_fCustom_contactPointAddedCallback_Function;

	///Pointer to custom collision call back function.
	///Needed when the character needs to be resized
	Custom_CharacterProxy_Collision_Callback_Function		m_fCustom_contactPointRemovedCallback_Function;
public :
	///Constructor which creates a CharacterProxy and adds it to the Physics World
	// GraphicsObject pointer is for setting the GraphicsObject pointer of the CharacterProxyObject and have a cross-reference
	CharacterProxyObject(GraphicsObject* f_pGraphicsObject);

	///Destructor
	~CharacterProxyObject();
	
	//give position and orientation inputs to the character controller//RDS_LOOK NOT USED
	void setInputs(hkReal f_iLeftPlus_RightMinus, hkReal f_iForwardMinus_BackPlus, bool f_bJump,hkReal f_fRotAngle_LeftPlus_RightMinus);

	///This function returns the PhysicsWorld object of this CharacterProxyObject instance
	hkpWorld* getPhysicsWorld()
	{
		return m_pPhysicsWorld;
	}

	///This function returns the CharacterProxy Instance of this CharacterProxyObject instance
	hkpCharacterProxy* getCharacterProxy()
	{
		return m_pCharacterProxy;
	}

	///This function returns NULL because CharacterProxy doesn't have a rigidBody attached to it
	hkpRigidBody* getRigidBody()
	{
		return NULL;
	}

	///This function returns the current position of the CharacterProxy Instance of this CharacterProxyObject instance
	hkVector4 getPosition()
	{
		return m_pCharacterProxy->getPosition();
	}

	///This function sets the current position of CharacterProxy Instance of this CharacterProxyObject instance
	void setPosition(hkVector4& f_rPosition)
	{
		m_pCharacterProxy->setPosition(f_rPosition);
	}


	///This sets the base character looking direction. if the character is not looking at x=1 you should set this to sth else than 0
	void setBaseLookingDirection(hkReal f_fBaseLookingAngle)
	{
		m_fBaseLookingAngle = f_fBaseLookingAngle;
	}

	///This function returns the state of the CharacterProxy among the defined states
	std::string	getCharacterStatus();
	
	///This function performs the physics simulation during update world 	
	void		stepObject(hkReal m_timestep);

	///This function is used to teleport the CharacterProxy according to a certain criterion
	void		teleport(hkVector4& checkPosition,hkVector4& teleportPosition, int bound);

	///This function returns true if the teleport criterion is met but does not actually teleport it.
	///Teleportation using this is left to the user using setPosition()
	int			teleportTell(hkVector4& checkPosition, int bound);
	
	///This functions adds the fly cheat functionality by removing all collisions except those wrt to ground
	void		flyCheat();

	///This function removes the fly cheat functionality and the physics resumes
	void		removeFlyCheat();	

	///The update function that is being called at every update of GameObjectManager
	bool update();
	///The destroy function that is being when a GameObject is destroyed
	void destroy();
	///This function will scale the physics representation of the GameObject if possible
	///Else it returns false to mention it couldn't apply the changes
	bool scale();

	///This function sets the velocity of the CharacterProxy.
	void setVelocity(hkReal f_fNewVelocity);

	///This function changes the character proxy shape to standing position (if it is not in this state and if it is applicable according to environment)
	bool startStanding();
	bool startCrawling();
	bool startAIShape();

	void addCustom_contactPointAddedCallback_Function(void* f_fNewCustom_contactPointAddedCallback_Function);
	void addCustom_contactPointRemovedCallback_Function(void* f_fNewCustom_contactPointRemovedCallback_Function);
protected:
	///This function is used to rotate the Phantom - Currently doesn't work
	hkBool reorientPhantom(const hkRotation& rotation);
};
/******************************************CharacterProxyObjectDeclarationsEND****************************************/
/***************************************CharacterRigidBodyObjectDeclarationsSTART*************************************/
class CharacterRigidBodyObject : public PhysicsObject
{
private :
	///This function is used for the CharacterRigidBody rotation
	hkBool reorientCharacter(const hkRotation& f_rRotation,hkReal f_fTimestep);

	///This function is a helper function for Global Scale function. Will be called internally from scale function
	bool scale(float width, float height, float depth);

	///Create capsule
	void createCapsule();

protected:
	///The physics world
	hkpWorld*				m_pPhysicsWorld;

	///An instance of the CharacterRigidBody
	hkpCharacterRigidBody*	m_pCharacterRigidBody;

	///The rigidBody shape used for collision purpose
	hkpShape*				m_pRBBoundingShape;

	///A pointer for the graphicsObject of this gameObject-->needed for updating the graphics object
	GraphicsObject*			m_pGraphicsObject;

	///A Character Context
	hkpCharacterContext*	m_pCharacterContext;

	///The base properties of a Character, not to hard code the character gravity(mass) and forward direction
	hkVector4				m_vBaseCharacterForward;
	hkReal					m_fCharacterGravityScaleFactor;
	hkVector4				m_vCharacterSceneNodeOffset;

	///This variable will be multiplied by the direction of velocity
	hkReal					m_fVelocity;	

	///This is the CharacterRigidBody's current rotation position 
	hkReal					m_fCurrentAngle;

	///This is the base character looking direction. if the character is not looking at x=1 you should set this to sth else
	hkReal					m_fBaseLookingAngle;

	///This defines the orientation of the Rigid body
	hkQuaternion			m_vCurrentOrientation;

	///Defines the number of frames when the character is in Air
	hkInt32					m_iFramesInAir;

	///To get character status as string for debugging/demonstration purposes
	std::string				m_sStatus;

	hkReal					m_iLeftPlus_RightMinus;				//left or right
	hkReal					m_iForwardMinus_BackPlus;			//forward or backward
	bool					m_bJUMP;							//whether jumping or not 
	hkReal					m_fRotAngle_LeftPlus_RightMinus;	//changed orientation angle based on mouse movement

public:

	///Constructor which creates a CharacterRigidBodyObject and adds it to the Physics World
	// GraphicsObject pointer is for setting the GraphicsObject pointer of the CharacterProxyObject and have a cross-reference
	CharacterRigidBodyObject(GraphicsObject* f_pGraphicsObject);

	///Destructor
	~CharacterRigidBodyObject();

	///This function returns the PhysicsWorld object of this CharacterProxyObject instance
	hkpWorld* getPhysicsWorld()
	{
		return m_pPhysicsWorld;
	}

    hkReal getCurrentAngle()
    {
        return m_fCurrentAngle;
    }

	///This function returns the CharacterProxy Instance of this CharacterRigidBodyObject instance
	hkpCharacterRigidBody* getCharacterRigidBody()
	{
		return m_pCharacterRigidBody;
	}

	///This function returns the CharacterProxy Instance of this CharacterRigidBodyObject instance
	hkpRigidBody* getRigidBody()
	{
		return m_pCharacterRigidBody->getRigidBody();
	}

	///This function returns the current position of the CharacterRigidBody Instance of this CharacterRigidBodyObject instance
	hkVector4 getPosition()
	{
		return m_pCharacterRigidBody->getPosition();
	}

	///This function sets the current position of m_pCharacterRigidBody Instance of this CharacterRigidBodyObject instance
	void setPosition(hkVector4& f_rPosition)
	{
		//this function should be a friend function with the GLE somehow.. I do not know how..
		//nobody else then GLE shouldn't be calling this function
		m_pCharacterRigidBody->getRigidBody()->setPosition(f_rPosition);
	}

	///This sets the base character looking direction. if the character is not looking at x=1 you should set this to sth else than 0
	void setBaseLookingDirection(hkReal f_fBaseLookingAngle)
	{
		m_fBaseLookingAngle = f_fBaseLookingAngle;
	}


	///This function performs the physics simulation during update world
	void stepObject(hkReal f_fTimestep);

	///Set inputs so that Havok can calculate the velocity vector
	void setInputs(hkReal f_iLeftPlus_RightMinus, hkReal f_iForwardMinus_BackPlus, bool f_bJump,hkReal f_fRotAngle_LeftPlus_RightMinus, bool assignRotation = false);

	///This returns the CharacterRigidBody state from among the defined states
	std::string getCharacterStatus();

	///The update function that is being called at every update of GameObjectManager
	bool update();
	///The destroy function that is being when a GameObject is destroyed
	void destroy();
	///This function will scale the physics representation of the GameObject if possible
	///Else it returns false to mention it couldn't apply the changes
	bool scale();

	bool scaleAndOffset(float width, float height,float depth, float floatX = 0.0f,	float floatY = 0.0f, float floatZ = 0.0f);

	///This function sets the velocity of the CharacterRigidBody.
	void setVelocity(float f_fNewVelocity);
};
/****************************************CharacterRigidBodyObjectDeclarationsEND**************************************/
//call back implementation for phantoms
class MyPhantomCallbackShape;
/******************************************PhantomShapeObjectDeclarationsSTART****************************************/
class PhantomShapeObject : public PhysicsObject
{///This class creates a PhantomShapeObject and adds it to the Physics World
protected :
	~PhantomShapeObject();///Destructor

	///The Physics World is obtained from the GameObjectManager
	hkpWorld*				m_pPhysicsWorld;

	///A pointer for the graphicsObject of this gameObject-->needed for updating the graphics object
	GraphicsObject*			m_pGraphicsObject;

	CollisionShapeType		m_eCollisionShapeType;

	///PhantomShapeObject needs this rigidbody representation in the world for collision detection purposes
	hkpRigidBody*			m_pRigidBodyObject;

	///This is where we will implement our custom functions
	MyPhantomCallbackShape*	m_pMyPhantomCallbackShape;

	///Pointer to custom collision call back function.
	///Needed when the phantom needs to be resized
	Custom_PhantomShape_Collision_Callback_Function		m_fCustom_phantomEnterEvent_Function;

	///Pointer to custom collision call back function.
	///Needed when the phantom needs to be resized
	Custom_PhantomShape_Collision_Callback_Function		m_fCustom_phantomLeaveEvent_Function;


	///The phantom pointer
	hkpShapePhantom*		m_pPhantom;

public:
	PhantomShapeObject(///A constructor which creates a PhantomShapeObject and adds it to the Physics World
							std::string				f_sOgreUniqueName,
							CollisionShapeType		f_eCollisionShapeType,
							GraphicsObject**		f_pGraphicsObject
						);
	///This function returns the PhysicsWorld object of this PhantomShapeObject instance
	hkpWorld* getPhysicsWorld()
	{
		return m_pPhysicsWorld;
	}

	///This function is an abstract function that has to be implemented
	hkpRigidBody* getRigidBody()
	{
		return m_pRigidBodyObject;
	}

	///This function returns the current position of the PhantomShapeObject instance
	hkVector4 getPosition();

	///This function sets the current position of RigidBodyObject of this PhantomShapeObject instance
	void setPosition(hkVector4& f_rPosition);

	///The update function that is being called at every update of GameObjectManager
	bool update();
	///The destroy function that is being when a GameObject is destroyed
	void destroy();
	///This function will scale the physics representation of the GameObject if possible
	///Else it returns false to mention it couldn't apply the changes
	bool scale();

	///This function sets the custom callback function for phantom enter event
	void setCustom_phantomEnterEvent_Function(void* f_fNew_Custom_phantomEnterEvent_Function);
	///This function sets the custom callback function for phantom leave event
	void setCustom_phantomLeaveEvent_Function(void* f_fNew_Custom_phantomLeaveEvent_Function);
};
/*****************************************PhantomShapeObjectDeclarationsEND**************************************/
/*****************************************PhantomAabbObjectDeclarationsSTART*************************************/
///This class creates a PhantomAabbObject and adds it to the Physics World
class MyHkpCDPointCollector;
class PhantomAabbObject : public PhysicsObject 
{
protected :
	///A PhantomAabbObject destructor
	~PhantomAabbObject();

	///Used for adding and removing the Phantom
	hkpWorld*			m_pPhysicsWorld;

	///An instance of the hkpAabbPhantom
	hkpAabbPhantom*		m_pAabbPhantom;

	///A pointer for the graphicsObject of this gameObject-->needed for updating the graphics object
	GraphicsObject*		m_pGraphicsObject;

	///RDS_PREVDEFINITION This is used by the stepObject function
	hkReal				m_fTime;

	///This function is used for stepping the simulation
	void stepObject(hkReal f_fTimeStep);

	///The function that calls the custom callback function whenever there is a hit
	Custom_PhantomAabb_Collision_Callback_Function			m_fCustom_Callback_Function;

	MyHkpCDPointCollector*		m_pCastCollector;
	MyHkpCDPointCollector*		m_pStartCollector;
public:
	///A constructor which creates a PhantomAabbObject and adds it to the Physics World according to mesh file inside the GraphicsObject
	PhantomAabbObject(
							GraphicsObject*		f_pGraphicsObject
					  );
	///The Physics World is obtained from the GameObjectManager.

	///This function returns the PhysicsWorld object of this PhantomShapeObject instance
	hkpWorld* getPhysicsWorld()
	{
		return m_pPhysicsWorld;
	}

	///This function returns the hkpAabbPhantomObject of this PhantomAabbObject instance
	hkpAabbPhantom* getPhantom()
	{
		return m_pAabbPhantom;
	}

	///This function returns the rigidBody attached to PhantomAabbObject
	hkpRigidBody* getRigidBody()
	{
		return NULL;
	}

	///This function returns the current position of the RigidBodyObject of this PhantomShapeObject instance
	hkVector4 getPosition();

	///This function sets the current position of PhantomShapeObject
	void setPosition(hkVector4& f_rPosition);

	///This function sets the Custom_PhantomAabb_Collision_Callback_Function
	void setCustom_PhantomAabb_Collision_Callback_Function(void* f_fNew_Custom_Callback_Function);

	///The update function that is being called at every update of GameObjectManager
	bool update();
	///The destroy function that is being when a GameObject is destroyed
	void destroy();
	///This function will scale the physics representation of the GameObject if possible
	///Else it returns false to mention it couldn't apply the changes
	bool scale();

	bool linearCast();
};
/*****************************************PhantomAabbObjectDeclarationsEND****************************************/

/******************************************VehicleObjectDeclarationsBEGIN*****************************************/
///A class which creates either a Car or a Motorcycle for the user - inherits from the PhysicsObject
class VehicleObject : public PhysicsObject
{
protected:
	///This is the Physics world in which the Car or Motorcycle will be added
	hkpWorld*							m_pPhysicsWorld;

	///This is a Vehicle Instance used to create either a car or a Vehicle
	hkpVehicleInstance*					m_pVehicleInstance;

	///A pointer for the graphicsObject of this gameObject-->needed for updating the graphics object
	GraphicsObject*						m_pGraphicsObject;

	///The RigidBody which forms the chassis of the car or the Motorcycle
	hkpRigidBody*						m_pChassisRigidBody;

	///This is a constraint instance used for wheels
	hkpConstraintInstance*				m_constraint;

	///This is used for controlling the movement of the car or the motorcycle
	hkpVehicleDriverInputAnalogStatus*	deviceStatus;
	
	///Variables which tell us if we want a car or a motorcycle - default values are for the car
	int m_numWheels;
	int m_numVehicles;

	//Used for setting up the motorcycle
	int m_basisIndexA;
	int m_basisIndexB;

	///This is used to store the generic constraint data
	hkpGenericConstraintData *m_genericConstraint;

	//Vehicle Steer flags.
	int leftflag;
	int rightflag;
	int accelerateflag;
	int reverseflag;
	bool m_bSlow;

public:
	///A constructor which creates a Car or a Motorcycle
	VehicleObject::VehicleObject(
									GameObjectType		f_eHavokObjectType, 
									GraphicsObject*		f_pGraphicsObject 
								);

	///This function is used to steer the car to the left
	void VehicleObject::steer_left();

	///This function is used to steer the car to the right
	void VehicleObject::steer_right();

	///This function is used to accelerate the car
	void VehicleObject::accelerate();

	///This function is used to reverse the car
	void VehicleObject::reverse();

	///This function is used to decelerate the car
	void VehicleObject::slow();

	//This function is used to go straight.
	void VehicleObject::go_straight();

	///This sets the position of the Vehicle in the world
	void VehicleObject::setPosition(hkVector4 &position);

	///Returns the current position of the chassis  in the world
	hkVector4 VehicleObject::getPosition();

	///This function returns the RigidBody i.e. the chassis 
	hkpRigidBody* VehicleObject::getRigidBody();

	///This function returns the Physics World
	hkpWorld* VehicleObject::getPhysicsWorld();

	//this function sets the dimensions of the vehicle
	void VehicleObject::setDimensions(float dimensions[]);

	//this array will be used to set the dimensions of the vehicle physics object
	float vehicleDimensions[14];
	
	///The update function that is being called at every update of GameObjectManager
	bool update();
	///The destroy function that is being when a GameObject is destroyed
	void destroy();
	///This function will scale the physics representation of the GameObject if possible
	///Else it returns false to mention it couldn't apply the changes
	bool scale();
private :
	///Destructor which removes the VehicleInstance
	VehicleObject::~VehicleObject();

	///This creates a new VehicleInstance for the car
	void VehicleObject::createVehicle();

	///This creates a new VehicleInstance for the motorcycle
	void VehicleObject::createMcycle();

	///This function populates the VehicleInstance with default values of the car
	void VehicleObject::buildVehicle();

	///This function populates the VehicleInstance with default values of the motorcycle
	void VehicleObject::buildMcycle();

	///This function creates a chassis for the car
	void VehicleObject::setupVehicles();

	///This function creates a chassis for the motorcycle
	void VehicleObject::setupMcycle();

	///This function populates the VehicleData instance for the car
	void VehicleObject::setupVehicleData(hkpVehicleData &data);

	///This function populates the VehicleData instance for the motorcycle
	void VehicleObject::setupMcycleData(hkpVehicleData &data);

	///This function sets the Default input for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultAnalogDriverInput &driverInput );

	///This function sets the Default steering settings for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultSteering &steering );

	///This function sets the Default engine settings for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultEngine &engine );

	///This function sets the Default transmission settings for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultTransmission &transmission );

	///This function sets the Default brake settings for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultBrake &brake );

	///This function sets the Default suspension settings for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultSuspension &suspension );

	///This function sets the Default aerodynamics settings for the car
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultAerodynamics &aerodynamics );

	///This function is used for the car's velocity damping
	void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultVelocityDamper &velocityDamper );

	///This function sets the tyre marks on the ground
	void VehicleObject::setupTyremarks( const hkpVehicleData &data, hkpTyremarksInfo &tyremarkscontroller );

	///This function sets the wheel collision settings for the car
	void VehicleObject::setupWheelCollide( hkpVehicleRayCastWheelCollide &wheelCollide );

	///This function sets the Default input for the motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultAnalogDriverInput &driverInput );

	///This function sets the Default steering settings for the motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultSteering &steering );

	///This function sets the Default engine settings for the motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultEngine &engine );

	///This function sets the Default transmission settings for the motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultTransmission &transmission );

	///This function sets the Default brake settings for the car motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultBrake &brake );

	///This function sets the Default suspension settings for the motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultSuspension &suspension );

	///This function sets the Default aerodynamics settings for the motorcycle
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultAerodynamics &aerodynamics );

	///This function is used for the motorcycle's velocity damping
	void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultVelocityDamper &velocityDamper );

	///This function sets the tyre marks on the ground
	void VehicleObject::setupMTyremarks( const hkpVehicleData &data, hkpTyremarksInfo &tyremarkscontroller );

	///This function sets the wheel collision settings for the motorcycle
	void VehicleObject::setupMWheelCollide( hkpVehicleRayCastWheelCollide &wheelCollide );
};
/*******************************************VehicleObjectDeclarationsEND******************************************/
#endif


/*JJUNK
	//CONSTRUCTOR_PHYSICSOBJECT_1-->if you search for this line in PhysicsObject.cpp it takes you to the start of this constructor
	///This constructor creates a complex GameObject instance, that includes a single Ogre GraphicsObject and a single PhysicsObject 
	///(which has 1 rigidBody in it). This constructor needs the correct Havok entity name (which is already inside the hkx file) 
	///to create the object, otherwise the object will not be created because of inconsistency in the names.
	PhysicsObject(
					std::string			f_sOgreUniqueName,
					std::string			f_sMeshFileName,
					std::string			f_sHKXFileName,
					std::string			f_sHavokEntityName,
					GameObjectType			f_eHavokObjectType,
					GraphicsObject**		f_pGraphicsObject		///for setting the GraphicsObject pointer of the GameObject which has this entity
				); 


*/

#endif