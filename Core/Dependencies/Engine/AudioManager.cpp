#include "StdAfx.h"
#include "AudioManager.h"
#include <sstream>
#include <fstream>
#include <exception>
#include "Engine.h"
#include "fmod_errors.h"
#include <map>
#include <utility>
#include <list>
#include <AkFilePackageLowLevelIOBlocking.h>


namespace AK
{
#ifdef WIN32
    void * AllocHook( size_t in_size )
    {
        return malloc( in_size );
    }
    void FreeHook( void * in_ptr )
    {
        free( in_ptr );
    }
    // Note: VirtualAllocHook() may be used by I/O pools of the default implementation
    // of the Stream Manager, to allow "true" unbuffered I/O (using FILE_FLAG_NO_BUFFERING
    // - refer to the Windows SDK documentation for more details). This is NOT mandatory;
    // you may implement it with a simple malloc().
    void * VirtualAllocHook(
        void * in_pMemAddress,
        size_t in_size,
        DWORD in_dwAllocationType,
        DWORD in_dwProtect
        )
    {
        return VirtualAlloc( in_pMemAddress, in_size, in_dwAllocationType, in_dwProtect );
    }
    void VirtualFreeHook( 
        void * in_pMemAddress,
        size_t in_size,
        DWORD in_dwFreeType
        )
    {
        VirtualFree( in_pMemAddress, in_size, in_dwFreeType );
    }
#endif
}

using namespace AK;
namespace GamePipe
{
	//----------------------------------new variables--------------------------------------//	
	//FMOD
	FMOD::EventSystem* AudioManager::FMODeventSystem = NULL;
	FMOD::System* AudioManager::FMODsys = NULL;
	bool AudioManager::FMODInitializeFlag = false;
	bool AudioManager::FMODLoadFlag = false;
	//the lsist of FMOD DSP Effects
	std::list<FMOD::DSP*> FMODdspLists;	
	typedef std::map<std::string, FMOD::Event*> FMODMapType;
	//the sound list of FMOD sound events
	FMODMapType soundListsMap;	
	FMOD_RESULT result;	
	//FMOD::EventReverb* AudioManager::FMODreverbPreset = NULL;

	//////////////////////////////////////////////////////////////////////////////////////////
	//Wwise variables
	//
	struct SoundStatusType {
		bool isPlaying;
		bool isPaused;
		AkGameObjectID ObjectID;
	};	
	//Structure used to store sounds, each entry holds the status of a sound:
	typedef std::map<std::string, SoundStatusType> WwiseMapType;
	
	//An instance of the WwiseMapType structure
    WwiseMapType WwiseSoundFlag;

	//boolean flag tells whether or not wwise has been initialized
	bool AudioManager::initializeWwiseFlag = false;

	//A queue of all the soundbanks for wwise
	std::list<std::string> WwiseBankLists;

	//A default game object, representing a music player
	const AkGameObjectID GAME_OBJECT_ID_MUSIC = 100;

	//variables needed for initialization of wwise
	AkBankID bankID;
	//CAkFilePackageLowLevelIOBlocking g_lowLevelIO;
	AKRESULT retValue;
	//----------------------------------new variable end----------------------------------//

	AudioManager::AudioManager()
	{
	}

	AudioManager::~AudioManager()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// WWISE IMPLEMENTATION
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AudioManager::InitializeWwise()
	{
		// Initialize audio engine
		// Memory Manager
		AkMemSettings memSettings;

		memSettings.uMaxNumPools = 20;

		// I/O Stream Manager
		AkStreamMgrSettings stmSettings;
		StreamMgr::GetDefaultSettings( stmSettings );

		AkDeviceSettings deviceSettings;
		StreamMgr::GetDefaultDeviceSettings( deviceSettings );

		AkInitSettings l_InitSettings;
		AkPlatformInitSettings l_platInitSetings;
		SoundEngine::GetDefaultInitSettings( l_InitSettings );
		SoundEngine::GetDefaultPlatformInitSettings( l_platInitSetings );
		AkMusicSettings musicInit;
		AK::MusicEngine::GetDefaultInitSettings( musicInit );
		// Setting pool sizes for this game.
		// These sizes are tuned for this game, every game should determine its own optimal values.
		l_InitSettings.uDefaultPoolSize				=  4 * 1024 * 1024;
		l_InitSettings.uMaxNumPaths					=  16;
		l_InitSettings.uMaxNumTransitions			=  128;

		l_platInitSetings.uLEngineDefaultPoolSize	=  4*1024*1024;

		//Initialization of everything
		/*
		   if ( AK::MemoryMgr::Init( &memSettings ) != AK_Success )
			{
				assert( ! "Could not create the memory manager." );
				return ;
			}
		   if ( !AK::StreamMgr::Create( stmSettings ) )
			{
				assert( ! "Could not create the Streaming Manager" );
				return;
			}
		   if ( g_lowLevelIO.Init( deviceSettings ) != AK_Success )
			{
				assert( ! "Could not create the streaming device and Low-Level I/O system" );
				return ;
			}

		   if ( AK::SoundEngine::Init( &l_InitSettings, &l_platInitSetings ) != AK_Success )
			{
				assert( ! "Could not initialize the Sound Engine." );
				return;
			}
    
		   if ( AK::MusicEngine::Init( &musicInit ) != AK_Success )
			{
				assert( ! "Could not initialize the Music Engine." );
				return;
			}
		#ifndef NDEBUG
			//
			// Initialize communications (not in release build!)
			//
			AkCommSettings commSettings;
			AK::Comm::GetDefaultInitSettings( commSettings );
			if ( AK::Comm::Init( commSettings ) != AK_Success )
			{
				assert( ! "Could not initialize communication." );
				return;
			}
		#endif // AK_OPTIMIZED
			*/
		//remember to change \ to / for Wwise path and add / in the last because of "L" 	
		std::string AudioPaths = "";
		AudioPaths.append("..\\..\\");		
		AudioPaths.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		AudioPaths.append("\\Media\\audio\\Wwise\\");

		//----------------string to wstring---------------------//
		std::wstring path(AudioPaths.length(), L' ');
		std::copy(AudioPaths.begin(), AudioPaths.end(),path.begin());
		//----------------string to wstring---------------------//
		//SOUNDENGINE_DLL::SetBasePath(path.c_str());// wstring to wchar_t

		AudioManager::LoadWwiseBank("Init.bnk");
		AK::SoundEngine::RegisterGameObj( GAME_OBJECT_ID_MUSIC, "Music" );

		AudioManager::initializeWwiseFlag = true;
	}


	void AudioManager::LoadWwiseBank(std::string bankName)
	{
		//iterate through all of the bank names already existing, if it already exists, nothing happens
		//if the bank name has not already been loaded, load it
		//
		for (std::list<std::string>::iterator it = WwiseBankLists.begin(); it != WwiseBankLists.end(); it++)
		{
			if (*it == bankName)
				return;		
		}		
		WwiseBankLists.push_front(bankName);
		retValue = SoundEngine::LoadBank( bankName.c_str(), AK_DEFAULT_POOL_ID, bankID );
		assert(retValue == AK_Success);
	}

	void AudioManager::UnLoadWwiseBank(std::string bankName)
	{
		//iterate through the existing loaded banks, if it has been loaded, remove it
		//
		for (std::list<std::string>::iterator it = WwiseBankLists.begin(); it != WwiseBankLists.end(); it++)
		{
			if (*it == bankName)
			{
				WwiseBankLists.remove(bankName);
				retValue = SoundEngine::UnloadBank( bankName.c_str(), 0 );
				assert(retValue == AK_Success);
				return;			
			}
		}		
	}

	void AudioManager::UnLoadWwiseAudio()
	{
		if (AudioManager::initializeWwiseFlag == true)
		{
			WwiseBankLists.clear();
			WwiseSoundFlag.clear();
			AK::SoundEngine::UnregisterGameObj( GAME_OBJECT_ID_MUSIC );
			retValue = SoundEngine::ClearPreparedEvents();
			assert(retValue == AK_Success);
			retValue = SoundEngine::ClearBanks();
			assert(retValue == AK_Success);
			//SOUNDENGINE_DLL::Term();
			AudioManager::initializeWwiseFlag = false;
		}
	}

	void AudioManager::SetWwiseGameObjectPosition(int gameObject, Ogre::Vector3 position, Ogre::Quaternion orientation)
	{
		AkSoundPosition soundPos;
		soundPos.Position.X = position.x;
		soundPos.Position.Y = position.y;
		soundPos.Position.Z = position.z;
		soundPos.Orientation.X = orientation.x;//getRoll().valueDegrees();
		soundPos.Orientation.Y = orientation.y;//getPitch().valueDegrees();
		soundPos.Orientation.Z = orientation.z;//getYaw().valueDegrees();
		AK::SoundEngine::SetPosition( gameObject, soundPos );
	}

	void AudioManager::SetWwiseListenerPosition(Ogre::Vector3 position, Ogre::Quaternion orientation)
	{
		AkListenerPosition listener;
		listener.Position.X = position.x;
		listener.Position.Y = position.y;
		listener.Position.Z = position.z;

		Ogre::Real yaw = orientation.getYaw().valueDegrees();
		Ogre::Real pitch = orientation.getPitch().valueDegrees();
		Ogre::Real roll = orientation.getRoll().valueDegrees();

		listener.OrientationFront.X = roll;
		listener.OrientationFront.Y = pitch;
		listener.OrientationFront.Z = yaw;

		//up.x = cos(yaw)*sin(pitch)*sin(roll) - sin(yaw)*cos(roll)
		listener.OrientationTop.X = cos(yaw) * sin(pitch) * sin(roll) - sin(yaw) * cos(roll);

		//up.y = sin(yaw)*sin(pitch)*sin(roll) + cos(yaw)*cos(roll)
		listener.OrientationTop.Y = sin(yaw) * sin(pitch) * sin(roll) + cos(yaw) * cos(roll);

		//up.z = cos(pitch)*sin(roll)
		listener.OrientationTop.Z = cos(pitch) * sin(roll);

		AK::SoundEngine::SetListenerPosition(listener);
	}

	void AudioManager::PlayWwiseMusic(std::string musicName)
	{
		std::string newName;
		
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			//If the music has already been played, then it's status can be found in WwiseSoundFlag
			if (it->first == musicName)
			{
				if (it->second.isPlaying == false && it->second.isPaused == false)
				{					
					newName = "Play_" + musicName;
					WwiseSoundFlag[musicName].isPlaying = true;	
					WwiseSoundFlag[musicName].isPaused = false;	
					SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
					SoundEngine::RenderAudio();					
				}
				else if (it->second.isPlaying == true && it->second.isPaused == true)
				{
					newName = "Resume_" + musicName;
					WwiseSoundFlag[musicName].isPlaying = true;	
					WwiseSoundFlag[musicName].isPaused = false;	
					SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
					SoundEngine::RenderAudio();
				}
				return;
			}
		}

		//if it hasn't been played before, or if it completely stopped, initiate the values in WwiseSoundFlag[musicName]
		newName = "Play_" + musicName;
		WwiseSoundFlag[musicName].isPlaying = true;
		WwiseSoundFlag[musicName].isPaused = false;
		WwiseSoundFlag[musicName].ObjectID = GAME_OBJECT_ID_MUSIC;
		SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
		SoundEngine::RenderAudio();

	}	

	void AudioManager::PauseWwiseMusic(std::string soundName)
	{	
		std::string newName;		
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			if (it->first == soundName)
			{
				if (it->second.isPlaying == true && it->second.isPaused == false)
				{
					newName = "pause_" + soundName;
					WwiseSoundFlag[soundName].isPlaying = true;	
					WwiseSoundFlag[soundName].isPaused = true;	
					SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
					SoundEngine::RenderAudio();					
				}
				else if (it->second.isPlaying == true && it->second.isPaused == true)
				{
					newName = "Resume_" + soundName;
					WwiseSoundFlag[soundName].isPlaying = true;	
					WwiseSoundFlag[soundName].isPaused = false;	
					SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
					SoundEngine::RenderAudio();					
				}
				return;
			}
		}
	}

	void AudioManager::ResumeWwiseMusic(std::string soundName)
	{
		std::string newName;
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			if (it->first == soundName)
			{				
				if (it->second.isPlaying == true && it->second.isPaused == true)
				{
					newName = "Resume_" + soundName;
					WwiseSoundFlag[soundName].isPlaying = true;	
					WwiseSoundFlag[soundName].isPaused = false;	
					SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
					SoundEngine::RenderAudio();
					return;
				}
			}
		}
	}	

	void AudioManager::StopWwiseMusic(std::string soundName)
	{
		std::string newName;
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			if (it->first == soundName)
			{
				std::string newName = "Stop_" + soundName;
				WwiseSoundFlag[soundName].isPlaying = false;	
				WwiseSoundFlag[soundName].isPaused = false;	
				SoundEngine::PostEvent ( newName.c_str(), GAME_OBJECT_ID_MUSIC);
				SoundEngine::RenderAudio();
				return;
			}			
		}		
	}

	void AudioManager::PlayWwiseSound(std::string soundName, int gameObj)
	{
		std::string newName;		
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			//If the sound has already been played, then it's status can be found in WwiseSoundFlag
			if (it->first == soundName)
			{
				if (it->second.isPlaying == false && it->second.isPaused == false)
				{					
					newName = "Play_" + soundName;
					WwiseSoundFlag[soundName].isPlaying = true;	
					WwiseSoundFlag[soundName].isPaused = false;	
					SoundEngine::PostEvent ( newName.c_str(), gameObj);
					SoundEngine::RenderAudio();					
				}
				else if (it->second.isPlaying == true && it->second.isPaused == true)
				{
					newName = "Resume_" + soundName;
					WwiseSoundFlag[soundName].isPlaying = true;	
					WwiseSoundFlag[soundName].isPaused = false;	
					SoundEngine::PostEvent ( newName.c_str(), gameObj);
					SoundEngine::RenderAudio();
				}
				return;
			}
		}

		//if it hasn't been played before, or if it completely stopped, initiate the values in WwiseSoundFlag[soundName]
		newName = "Play_" + soundName;
		WwiseSoundFlag[soundName].isPlaying = true;
		WwiseSoundFlag[soundName].isPaused = false;
		WwiseSoundFlag[soundName].ObjectID = gameObj;
		SoundEngine::PostEvent ( newName.c_str(),gameObj);
		SoundEngine::RenderAudio();
	}

	void AudioManager::PlayWwiseSound(std::string soundName, int gameObj, float RTPCValue)
	{
		std::string newName;
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			if (it->first == soundName)
			{
				newName = "Play_" + soundName;
				AK::SoundEngine::SetRTPCValue( "RPM", RTPCValue, gameObj );
				if (it->second.isPlaying == true && it->second.isPaused == false)
				{
					SoundEngine::PostEvent ( newName.c_str(), gameObj );
					SoundEngine::RenderAudio();
				}	
				else if (it->second.isPlaying == false && it->second.isPaused == false)
				{
					WwiseSoundFlag[soundName].isPlaying = true;
					SoundEngine::PostEvent ( newName.c_str(), gameObj );
					SoundEngine::RenderAudio();
				}
				return;
			}
		}

		//if the sound hasn't been played before, initialize the values in WwiseSoundFlag[soundName]
		newName = "Play_" + soundName;
		WwiseSoundFlag[soundName].isPaused = false;
		WwiseSoundFlag[soundName].isPlaying = true;
		WwiseSoundFlag[soundName].ObjectID = gameObj;
		SoundEngine::PostEvent ( newName.c_str(), gameObj );
		SoundEngine::RenderAudio();
	}

	void AudioManager::StopWwiseSound(std::string soundName)
	{
		std::string newName;
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			if (it->first == soundName)
			{
				std::string newName = "Stop_" + soundName;
				WwiseSoundFlag[soundName].isPlaying = false;	
				WwiseSoundFlag[soundName].isPaused = false;	
				SoundEngine::PostEvent ( newName.c_str(), WwiseSoundFlag[soundName].ObjectID);
				SoundEngine::RenderAudio();
				return;
			}			
		}		
	}

	Ogre::String AudioManager::getWwiseState(std::string soundName)
	{
		Ogre::String result;
		for (WwiseMapType::const_iterator it = WwiseSoundFlag.begin(); it != WwiseSoundFlag.end(); ++it)
		{
			if (it->first == soundName)
			{
				if (WwiseSoundFlag[soundName].isPlaying == false && WwiseSoundFlag[soundName].isPaused == false)
				{
					return result = "stopped";
				}
				else if (WwiseSoundFlag[soundName].isPlaying == true && WwiseSoundFlag[soundName].isPaused == false)
				{
					return result = "playing";
				}
				else if (WwiseSoundFlag[soundName].isPlaying == true && WwiseSoundFlag[soundName].isPaused == true)
				{
					return result = "paused";
				}
			}
		}
		return result ="not playing"; 
	}
	
	//private function
	std::string AudioManager::GetWwiseAudioEventsPath()
	{	
		std::string AudioPaths = "";
		AudioPaths.append("..\\..\\");		
		AudioPaths.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		AudioPaths.append("\\Media\\audio\\Wwise\\");

		return AudioPaths;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// FMOD IMPLEMENTATION
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AudioManager::FMODInitializeAudio()
	{
		if(!AudioManager::FMODInitializeFlag)
		{			
			AudioManager::FMODErrorCheck(result = FMOD::EventSystem_Create(&AudioManager::FMODeventSystem));
			AudioManager::FMODeventSystem->init(64, FMOD_INIT_3D_RIGHTHANDED | FMOD_INIT_NORMAL,0,FMOD_EVENT_INIT_NORMAL);
			AudioManager::FMODeventSystem->setMediaPath(FMODGetAudioEventsPath().c_str());
			AudioManager::FMODInitializeFlag = true;
		}
	}

	void AudioManager::FMODLoadAudio(char *sectionName)
	{
		Ogre::String secName, typeName, archName;
		Ogre::ConfigFile configFile;	
		if (AudioManager::FMODInitializeFlag == true)
		{
			configFile.load(AudioManager::FMODGetAudioPath().c_str());
			Ogre::ConfigFile::SectionIterator sectionIterator = configFile.getSectionIterator();
			char *presentSection;

			while (sectionIterator.hasMoreElements())
			{
				//free(presentSection);
				secName = sectionIterator.peekNextKey();
				
                presentSection=(char *)malloc(sizeof(secName.length()));
                               
			    strcpy_s(presentSection, strlen(secName.c_str()) + 1, secName.c_str());
               
                Ogre::ConfigFile::SettingsMultiMap *settingsMultiMap = sectionIterator.getNext();
				Ogre::ConfigFile::SettingsMultiMap::iterator settingIterator;
				for (settingIterator = settingsMultiMap->begin(); settingIterator != settingsMultiMap->end(); ++settingIterator)
				{
					typeName = settingIterator->first;
					archName = settingIterator->second;
					if(!strcmp(sectionName,presentSection))
					{
						AudioManager::FMODeventSystem->load((char*)archName.c_str(),0,0);
					}
				}
			}
			AudioManager::FMODLoadFlag = true;
		}
	}

	void AudioManager::FMODUnLoadAudio(char *sectionName)
	{
		Ogre::String secName, typeName, archName;
		Ogre::ConfigFile configFile;

		if (AudioManager::FMODLoadFlag == true)
		{
			configFile.load(AudioManager::FMODGetAudioPath().c_str());
			Ogre::ConfigFile::SectionIterator sectionIterator = configFile.getSectionIterator();
			char *presentSection;

			while (sectionIterator.hasMoreElements())
			{
				//free(presentSection);
				secName = sectionIterator.peekNextKey();
				presentSection=(char *)malloc(sizeof(secName.length()));
				strcpy_s(presentSection, strlen(secName.c_str()) + 1, secName.c_str());
				Ogre::ConfigFile::SettingsMultiMap *settingsMultiMap = sectionIterator.getNext();
				Ogre::ConfigFile::SettingsMultiMap::iterator settingIterator;
				for (settingIterator = settingsMultiMap->begin(); settingIterator != settingsMultiMap->end(); ++settingIterator)
				{
					typeName = settingIterator->first;
					archName = settingIterator->second;
					if(!strcmp(sectionName,presentSection))
					{

						if(typeName.compare("fev")==0)
						{
							FMOD::EventProject *proj=NULL;
							archName = archName.substr(0,archName.find("."));
							AudioManager::FMODeventSystem->getProject(archName.c_str(),&proj);		

							if(proj)
								proj->release();
						}
						else if(typeName.compare("fsb")==0)
						{
							AudioManager::FMODeventSystem->unloadFSB(archName.c_str(),NULL);
						}
					}
				}
			}
			FMODdspLists.clear();
			AudioManager::FMODeventSystem->release();
			AudioManager::FMODInitializeFlag = false;
			AudioManager::FMODLoadFlag = false;
		}
	}

	void AudioManager::FMODaddDsp(FMOD_DSP_TYPE type)
	{
		GamePipe::AudioManager::FMODeventSystem->getSystemObject(&AudioManager::FMODsys);
		FMOD::DSP* dspvalue = NULL;
		AudioManager::FMODsys->createDSPByType(type,&dspvalue);
		AudioManager::FMODsys->addDSP(dspvalue,NULL);
		FMODdspLists.push_front(dspvalue);
		GamePipe::AudioManager::FMODeventSystem->update();
	}

	void AudioManager::FMODremoveDsp()
	{
		if (!FMODdspLists.empty())
		{	
			FMODdspLists.back()->remove();
			FMODdspLists.pop_back();
		}
	}

	// These next two functions are a little confusing, because of their similar naming conventions.
	// Honestly, we couldn't think of anything else.  The first function, FMODGetAudioPath() is called when
	// FMODLoadAudio() is called.  The string returned by FMODGetAudioPath() tells FMOD which configuration file to
	// load.  Essentially, it's giving it the path to where AudioPaths.txt file is.
	
	//private function
	std::string AudioManager::FMODGetAudioPath()
	{	
		//  "..\\..\\Demos\\Media\\AudioPaths.txt"
		std::string AudioPaths = "";
		AudioPaths.append("..\\..\\");
		AudioPaths.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		AudioPaths.append("\\AudioPaths.txt");

		return AudioPaths;
	}

	// FMODGetAudioEventsPath() returns a string which represents the path to the directory in which all
	// the FMOD audio assets are stored.  This is called in the Initialization of FMOD.  Note, although both
	// FMODGetAudioPath and FMODGetAudioEventsPath functions are similar, keep in mind that FMODGetAudioEventsPath() 
	// returns the path containing the audio events, while FMODGetAudioPath returns the string representing the
	// path to the AudioPaths.txt file.

	//private function
	std::string AudioManager::FMODGetAudioEventsPath()
	{	
		//  "..\\..\\Demos\\Media\\audio\\events\\"
		std::string AudioPaths = "";
		AudioPaths.append("..\\..\\");		
		AudioPaths.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		AudioPaths.append("\\Media\\audio\\fmod\\");

		return AudioPaths;
	}

	// The FMOD Reverb functions don't work, and they never have.  We didn't bother to fix them, 
	// you guys can if you want.  It's not that important.
	//
	//void AudioManager::FMODactivateReverb(const char* name)
	//{
	//	//Create a reverb object
	//	AudioManager::FMODeventSystem->createReverb(&AudioManager::FMODreverbPreset);
	//	//ERRCHECK(result);

	//	//Declare a reverb definition 
	//	FMOD_REVERB_PROPERTIES prop;

	//	//Set the define to the preset 'scarycave'
	//	AudioManager::FMODeventSystem->getReverbPreset(name, &prop);
	//	//ERRCHECK(result);

	//	//Apply the reverb definition properties to the reverb object
	//	AudioManager::FMODreverbPreset->setProperties(&prop);

	//	//Set the 3D reverb's position, minimum distance and maximum distance
	//	//reverb_scarycave->set3DAttributes(&pos, min_dist, max_dist);	
	//	//ERRCHECK(result);

	//	//Set the reverb to active
	//	AudioManager::FMODreverbPreset->setActive(true);
	//	//ERRCHECK(result);

	//	AudioManager::FMODeventSystem->update();
	//}

	//void AudioManager::FMODdeactivateReverb(const char* name)
	//{

	//	//Create a reverb object
	//	AudioManager::FMODeventSystem->createReverb(&AudioManager::FMODreverbPreset);
	//	//ERRCHECK(result);

	//	//Declare a reverb definition 
	//	FMOD_REVERB_PROPERTIES prop;

	//	//Set the define to the preset 'scarycave'
	//	AudioManager::FMODeventSystem->getReverbPreset(name, &prop);
	//	//ERRCHECK(result);

	//	//Apply the reverb definition properties to the reverb object
	//	AudioManager::FMODreverbPreset->setProperties(&prop);

	//	//Set the 3D reverb's position, minimum distance and maximum distance
	//	//reverb_scarycave->set3DAttributes(&pos, min_dist, max_dist);	
	//	//ERRCHECK(result);

	//	//Set the reverb to active
	//	AudioManager::FMODreverbPreset->setActive(false);
	//	//ERRCHECK(result);

	//	AudioManager::FMODeventSystem->update();
	//}
	
	//---------------------------NEW FMOD FUNCTION----------------------------//
	void AudioManager::FMODSetEventSound(std::string eventpath, std::string name)
	{
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != soundListsMap.end(); ++it)
		{
			if (it->first == name)
				return;
		}
		soundListsMap[name] = new FMOD::Event();
		AudioManager::FMODeventSystem->getEvent(eventpath.c_str(), FMOD_EVENT_DEFAULT, &soundListsMap[name]);
	}

	void AudioManager::FMODSet3DEventSound(std::string eventpath, std::string name)
	{
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != soundListsMap.end(); ++it)
		{
			if (it->first == name)
				return;
		}
		soundListsMap[name] = new FMOD::Event();
		AudioManager::FMODeventSystem->getEvent(eventpath.c_str(), FMOD_3D, &soundListsMap[name]);
	}

	void AudioManager::FMODSet3DAttributes(std::string name, FMOD_VECTOR pos, FMOD_VECTOR vel)
	{
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != soundListsMap.end(); ++it)
		{
			if (it->first == name)
			{
				it->second->get3DAttributes(&pos, &vel);
			}
		}	
	}

	void AudioManager::FMODUpdateListener(Ogre::Vector3 position, Ogre::Quaternion orientation)
	{	
		FMOD_VECTOR listenerPos;
		listenerPos.x = position.x;
		listenerPos.y = position.y;
		listenerPos.z = position.z;

		float xRight = (float)cos(orientation.y * (Ogre::Math::PI / 180.0f));
		float yRight = 0.0f;
		float zRight = (float)sin(orientation.y * (Ogre::Math::PI / 180.0f));

		float xForward = (float)sin(orientation.y * (Ogre::Math::PI / 180.0f)) * cos(orientation.x  * (Ogre::Math::PI / 180.0f));
		float yForward = -(float)sin(orientation.x  * (Ogre::Math::PI / 180.0f));
		float zForward = -(float)cos(orientation.y * (Ogre::Math::PI / 180.0f)) * cos(orientation.x  * (Ogre::Math::PI / 180.0f));

		float xUp = yRight * zForward - zRight * yForward;
		float yUp = zRight * xForward - xRight * zForward;
		float zUp = xRight * yForward - yRight * xForward;

		static FMOD_VECTOR lastpos = { 0.0f, 0.0f, 0.0f };

		FMOD_VECTOR forward;
		FMOD_VECTOR up;
		//FMOD_VECTOR vel;

		forward.x = xForward;
		forward.y = yForward;
		forward.z = zForward;

		up.x = xUp;
		up.y = yUp;
		up.z = zUp;

		// ********* NOTE ******* READ NEXT COMMENT!!!!!
		// vel = how far we moved last FRAME (m/f), then time compensate it to SECONDS (m/s).
		//vel.x = (listenerPos.x - lastpos.x) * (1000.0f / GamePipe::Engine::GetDeltaTime());
		//vel.y = (listenerPos.y - lastpos.y) * (1000.0f / GamePipe::Engine::GetDeltaTime());
		//vel.z = (listenerPos.z - lastpos.z) * (1000.0f / GamePipe::Engine::GetDeltaTime());		
		//static FMOD_VECTOR lastVel = { 0.0f, 0.0f, 0.0f };

		// store pos for next time
		lastpos = listenerPos;

		GamePipe::AudioManager::FMODeventSystem->set3DListenerAttributes(0, &listenerPos, 0, &forward, &up);		
	}

	void  AudioManager::FMODEventPlay(std::string name)
	{	
		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{				
				FMOD_EVENT_STATE state;
				it->second->getState(&state);
				if (!(state & FMOD_EVENT_STATE_PLAYING))
				{
					it->second->start();
					it->second->setPaused(false);
					AudioManager::FMODeventSystem->update();					
				}
				else
				{
					bool isPaused;
					it->second->getPaused(&isPaused);
					if (isPaused == true)
					{
						it->second->setPaused(false);
						AudioManager::FMODeventSystem->update();
					}				
				}
			}
		}
	}

	void  AudioManager::FMODEventPaused(std::string name)
	{
		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				bool isPaused;
				it->second->getPaused(&isPaused);
				if (isPaused == false)
				{
					it->second->setPaused(true);
					AudioManager::FMODeventSystem->update();
				}
				else if (isPaused == true)
				{
					it->second->setPaused(false);
					AudioManager::FMODeventSystem->update();
				}
			}
		}
	}

	void AudioManager::FMODEventStop(std::string name)
	{
		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				it->second->stop();
				AudioManager::FMODeventSystem->update();
			}
		}
	}

	void AudioManager::FMODSetEventRPMVolume(std::string name, float setVolume)
	{
		float rpm_min, rpm_max;
		FMOD::EventParameter* param;		

		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				it->second->getParameter("rpm",&param);
				param->getRange(&rpm_min, &rpm_max);
				if (setVolume >= rpm_max)
					param->setValue(rpm_max);	
				else if (setVolume <= rpm_min)
					param->setValue(rpm_min);
				else
					param->setValue(setVolume);	
				AudioManager::FMODeventSystem->update();
			}
		}
	}

	void AudioManager::FMODSetEventLOADVolume(std::string name, float setVolume)
	{
		float load_min, load_max;
		FMOD::EventParameter* param;

		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				it->second->getParameter("load",&param);
				param->getRange(&load_min, &load_max);
				if (setVolume >= load_max)
					param->setValue(load_max);	
				else if (setVolume <= load_min)
					param->setValue(load_min);
				else
					param->setValue(setVolume);	
				AudioManager::FMODeventSystem->update();
			}
		}	
	}

	void AudioManager::FMODSetEventVolume(std::string name, float setVolume)
	{
		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				it->second->setVolume(setVolume);
				AudioManager::FMODeventSystem->update();
			}
		}
	}

    void AudioManager::FMODEventReplay(std::string name)
	{
		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				it->second->start();
				AudioManager::FMODeventSystem->update();
			}
		}
	}
	
	void AudioManager::FMODSetEventMute(std::string name)
	{	
		bool muted;

		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				it->second->getMute(&muted);
				muted = !muted;
				it->second->setMute(muted);
				AudioManager::FMODeventSystem->update();
			}
		}
	}
	
	//Pause or Replay current events
	void AudioManager::FMODPlayPauseEvents()
	{
		FMOD::EventCategory  *mastercategory;
		bool paused;
		AudioManager::FMODeventSystem->getCategory("master", &mastercategory);
        mastercategory->getPaused(&paused);
        paused = !paused;
        mastercategory->setPaused(paused);
		AudioManager::FMODeventSystem->update();
	}

	void AudioManager::FMODErrorCheck(FMOD_RESULT result)
	{
		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			exit(-1);
		}
	}

	Ogre::String AudioManager::FMODGetState(std::string name)
	{
		Ogre::String result = "";

		FMODMapType::const_iterator end = soundListsMap.end(); 
		for (FMODMapType::const_iterator it = soundListsMap.begin(); it != end; ++it)
		{
			if (it->first == name)
			{
				FMOD_EVENT_STATE state;
				it->second->getState(&state);
				if (state & FMOD_EVENT_STATE_PLAYING)
				{
					bool isPaused;
					it->second->getPaused(&isPaused);
					if (isPaused == false)
						return result = "playing";
					else if (isPaused == true)
						return result = "paused";
				}
				else
				{
					return result = "stopped";				
				}
			}
		}
		return result ="error"; 
	}
}