///////////////////////////////////////
#include "StdAfx.h"

#include "CreateDefaultGraphicsObjectTask.h"

void CreateDefaultGraphicsObjectTask::UpdateTask() {
	
	if (type==PHYSICS_SYSTEM) 
	{
		//this means that this function is called create a scene node
		//so create only a scene node and return
		if (obj->m_pOgreSceneNode==NULL)
			obj->m_pOgreSceneNode = obj->m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(obj->m_sOgreUniqueName+"SceneNode");
	}
	else
	{
		obj->m_pOgreEntity = obj->createOgreEntity(obj->m_sOgreUniqueName,fileName);
		if (obj->m_pOgreSceneNode!=NULL)
		{
			obj->m_pOgreSceneNode->attachObject(obj->m_pOgreEntity);
		}
		else
		{
			obj->m_pOgreSceneNode = obj->m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(obj->m_sOgreUniqueName+"SceneNode");
			obj->m_pOgreSceneNode->attachObject(obj->m_pOgreEntity);
		}
	}
};