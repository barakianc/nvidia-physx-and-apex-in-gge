/////////TaskContainer.cpp////////////////////////////////////

#include "StdAfx.h"

//#ifdef TBB_PARALLEL_OPTION

#include "CameraUpdateTask.h"
#include "GraphicsObjectUpdateTask.h"
#include "CreateDefaultGraphicsObjectTask.h"
#include "ChangeSceneNodeTask.h"
#include "AllClear.h"
#include "SetVisibleTask.h"
#include "CreateHavokShapeTask.h"
#include "CreatePhantomAabbObjectTask.h"
#include "CreateCharacterProxyObjectTask.h"
#include "CreateCharacterRigidBodyObjectTask.h"
#include "CreateHavokAnimationObjectTask.h"
#include "CreateOgreAnimationObjectTask.h"
#include "SkeletonUpdateRootTask.h"
#include "SkeletonUpdateBoneTask.h"
#include "ConsoleUpdateTask.h"
#include "CreatePhantomShapeTask.h"
#include "UtilitiesUpdateTask.h"
#include "TaskContainer.h"

void TaskContainer::Process(){
		switch (taskType){
			case Camera_Update_Task:
				((CameraUpdateTask*)task)->UpdateTask();
				break;
			case Graphics_Object_Update_Task:
				((GraphicsObjectUpdateTask*)task)->UpdateTask();
				break;
			case Create_Default_Graphics_Object_Task:
				((CreateDefaultGraphicsObjectTask*)task)->UpdateTask();
				break;
			case Change_Scene_Node_Task:
				((ChangeSceneNodeTask*)task)->UpdateTask();
				break;
			case Set_Visible_Task:
				((SetVisibleTask*)task)->UpdateTask();
				break;
			case Create_Havok_Shape_Task:
				((CreateHavokShapeTask*)task)->UpdateTask();
				break;
			case Create_PhantomAabbObject_Task:
				((CreatePhantomAabbObjectTask*)task)->UpdateTask();
				break;
			case Create_CharacterProxyObject_Task:
				((CreateCharacterProxyObjectTask*)task)->UpdateTask();
				break;
			case Create_CharacterRigidBodyObject_Task:
				((CreateCharacterRigidBodyObjectTask*)task)->UpdateTask();
				break;
			case Create_HavokAnimationObject_Task:
				((CreateHavokAnimationObjectTask*)task)->UpdateTask();
				break;
			case Create_OgreAnimationObject_Task:
				((CreateOgreAnimationObjectTask*)task)->UpdateTask();
				break;
			case All_Clear:
				((AllClear*)task)->UpdateTask();
				break;
			case Skeleton_Update_Root_Task:
				((SkeletonUpdateRootTask*)task)->UpdateTask();
				break;
			case Skeleton_Update_Bone_Task:		
				((SkeletonUpdateBoneTask*)task)->UpdateTask();
				break;
			case Console_Update_Position_Task:
				((ConsoleUpdatePositionTask*)task)->UpdateTask();
				break;
			case Console_Update_Text_Task:
				((ConsoleUpdateTextTask*)task)->UpdateTask();
				break;
			case Create_Phantom_Shape_Task:
				((CreatePhantomShapeTask*)task)->UpdateTask();
				break;
			case Utilities_Update_Task:
				((UtilitiesUpdateTask*)task)->UpdateTask();
				break;
			default:

				break;
		}
	};

//#endif