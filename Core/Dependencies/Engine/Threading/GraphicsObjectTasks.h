//Define tasks for updating data relating to GraphicsObject objects
#ifndef GRAPHICS_OBJECT_TASKS_H
#define GRAPHICS_OBJECT_TASKS_H
/*
#ifdef TBB_PARRALEL_OPTION
#include "GraphicsObject.h"
#include "QueuedTask.h"
#include "HavokObject.h"
#include "HavokObjectTasks.h"
class GraphicsObject;
class CreatePhantomAabbObjectTask;



class TaskContainer : public QueuedTask
{	
public:
	void* task;
	TASKS taskType;
	TaskContainer(void* incTask, TASKS incTaskType) : task(incTask), taskType(incTaskType) {}
	~TaskContainer() { delete task; }
	void Update(){
		switch (taskType){
			case Camera_Update_Task:
				((CameraUpdateTask*)task)->Update();
				break;
			case Graphics_Object_Update_Task:
				((GraphicsObjectUpdateTask*)task)->Update();
				break;
			case Graphics_Object_Initialize_Task:
				((GraphicsObjectInitializeTask*)task)->Update();
				break;
			case Create_Default_Graphics_Object_Task:
				((CreateDefaultGraphicsObjectTask*)task)->Update();
				break;
			case Change_Scene_Node_Task:
				((ChangeSceneNodeTask*)task)->Update();
				break;
			case Set_Visible_Task:
				((SetVisibleTask*)task)->Update();
				break;
			case Create_Havok_Shape_Task:
				((CreateHavokShapeTask*)task)->Update();
				break;
			case Create_PhantomAabbObject_Task:
				((CreatePhantomAabbObjectTask*)task)->Update();
				break;
			case Create_CharacterProxyObject_Task:
				((CreateCharacterProxyObjectTask*)task)->Update();
				break;
			case Create_CharacterRigidBodyObject_Task:
				((CreateCharacterRigidBodyObjectTask*)task)->Update();
				break;
			case Create_HavokAnimationObject_Task:
				((CreateHavokAnimationObjectTask*)task)->Update();
				break;
			case All_Clear:
				((AllClear*)task)->Update();
				break;
			case Skeleton_Update_Root_Task:
				((SkeletonUpdateRootTask*)task)->Update();
				break;
			case Skeleton_Update_Bone_Task:		
				((SkeletonUpdateBoneTask*)task)->Update();
				break;
			default:

				break;
		}
	}
};
*/
#endif
#endif