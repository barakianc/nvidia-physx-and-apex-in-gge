/////////Enumeration of task types for use by QueuedTask derived classes

#ifndef TASK_TYPES_H
#define TASK_TYPES_H

/** The TASKS enumeration specifies the type of each task that inherits QueuedTask for use by TaskContainer.
 * TASKS enumeration.
 * The TASKS enumeration specifies the type of each task that inherits QueuedTask for use by TaskContainer.  TaskContainer utilizes a void
 * pointer to the particular task it holds and thus needs a way of casting back from a void pointer when Processing tasks.  This is an alternative
 * to using a template class as we discovered that to be problematic with the need to forward declare TaskContainer.
 */
enum TASKS{
	Graphics_Object_Update_Task,
	Create_Default_Graphics_Object_Task,
	Change_Scene_Node_Task,
	Set_Visible_Task,
	Create_Havok_Shape_Task,
	Create_PhantomAabbObject_Task,
	Create_CharacterProxyObject_Task,
	Create_CharacterRigidBodyObject_Task,
	Create_HavokAnimationObject_Task,
	Create_OgreAnimationObject_Task,
	All_Clear,
	Camera_Update_Task,
	Skeleton_Update_Root_Task,
	Skeleton_Update_Bone_Task,
	Console_Update_Position_Task,
	Console_Update_Text_Task,
	Create_Phantom_Shape_Task,
	Utilities_Update_Task
};

#endif
