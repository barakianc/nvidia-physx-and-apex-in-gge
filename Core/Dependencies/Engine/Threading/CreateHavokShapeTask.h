/////////////SetVisibleTask.h - ///////////////////////////////////////

#ifndef CREATE_HAVOK_SHAPE_TASK_H
#define CREATE_HAVOK_SHAPE_TASK_H

#include "QueuedTask.h"
#include "GraphicsObject.h"

/** The CreateDefaultGraphicsObjectTask class implements most of the GraphicsObject constructor for deferred creation.
 * CreateHavokShapeTask class.
 * The CreateHavokShapeTask class implements most of the GraphicsObject constructor particularly the creation of the a SceneNode
 * for the GameObject, the creation of an OgreEntity, and the attachment of the latter to the former.  This version of the GraphicsObject
 * constructor creates primitive shapes based on a specified type.  As with most classes inheriting QueuedTask, instances of this class
 * are queued and processed by the rendering thread so as to prevent writing to Ogre scene data while the renderer is rendering a frame.
 */
class CreateHavokShapeTask : public QueuedTask
{
private:

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject whose constructor is partially implemented/finished here.
	*/
	GraphicsObject* obj;

	/**
	* CollisionShapeType private variable.
	* An enumeration of basic shape types
	*/
	CollisionShapeType type;
	
	/**
	* Ogre::Vector3 private variable.
	* A Ogre::Vector3 that sets scaling of subsequently created SceneNode
	*/
	Ogre::Vector3 halfSize;
public:

	/**
	* CreateHavokShapeTask constructor.
	* CreateHavokShapeTask constructor that takes a GraphicsObject pointer and CollisionShapeType as parameters
	* @param GraphicsObject*
	* @param CollisionShapeType
	*/
	CreateHavokShapeTask(GraphicsObject* incObj, CollisionShapeType incType) : obj(incObj), type(incType) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that finishes the implementation of one of the GraphicsObject
	* constructors and sets corresponding pointers.
	*/
	void UpdateTask();
};

#endif