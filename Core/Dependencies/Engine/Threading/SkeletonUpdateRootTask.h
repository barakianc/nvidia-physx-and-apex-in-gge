////////////SkeletonUpdateRootTask.h - ////////////////////////////

#ifndef SKELETON_UPDATE_ROOT_TASK_H
#define SKELETON_UPDATE_ROOT_TASK_H

#include "QueuedTask.h"
#include "AnimatedHavokEntity.h"


/** The SkeletonUpdateRootTask class updates the position, orientation and scale of the RootBone of an OgreSkeleton.
 * SkeletonUpdateRootTask class.
 * The SkeletonUpdateRootTask class updates the position, orientation and scale of the RootBone of an OgreSkeleton.  As with most 
 * classes inheriting QueuedTask, instances of this class are queued and processed by the rendering thread so as to prevent writing 
 * to Ogre scene data while the renderer is rendering a frame.
 */
class SkeletonUpdateRootTask : public QueuedTask
{
private:

	/**
	* AnimationManager::AnimatedHavokEntity* private variable.
	* A pointer to the animated havok entity that holds the OgreSkeleton that will be manipulated.
	*/
	AnimationManager::AnimatedHavokEntity* anim;

	/**
	* Ogre::Quaternion private variable.
	* A Quaternion that specifies the orientation of the skeleton.
	*/
	Ogre::Quaternion q;
public:

	/**
	* SkeletonUpdateRootTask constructor.
	* SkeletonUpdateRootTask constructor that takes an AnimationManager::AnimatedHavokEntity* and Ogre::Quaternion as parameters.
	* @param AnimationManager::AnimatedHavokEntity*
	* @param  Ogre::Quaternion
	*/
	SkeletonUpdateRootTask(AnimationManager::AnimatedHavokEntity* incAnim, Ogre::Quaternion incQ) : anim(incAnim), q(incQ) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that updates the position/orientation/scale of the OgreSkeleton
	* pointed to by AnimationManager::AnimatedHavokEntity* anim.
	*/
	void UpdateTask(){
		anim->GetSkeleton()->getRootBone()->setPosition(Ogre::Vector3::ZERO);
		anim->GetSkeleton()->getRootBone()->setOrientation(q);
		anim->GetSkeleton()->getRootBone()->setScale(Ogre::Vector3::UNIT_SCALE);
	}
};

#endif