#ifndef ENGINE_UPDATE_TASKS_H
#define ENGINE_UPDATE_TASKS_H

#ifdef TBB_PARALLEL_OPTION
#include "tbb/task_scheduler_init.h"
#include "tbb/task.h"
#include "tbb/spin_mutex.h"
#include "tbb/tbb_thread.h"
#include "TaskTypes.h"
#include "Engine.h"
#include "TaskContainer.h"

using namespace GamePipe;


/** The RenderingLoop class serves two purposes: to process updates to Ogre scene data and to update the Renderer.
 * RenderingLoop class.
 * The RenderingLoop class serves two purposes: to process updates to Ogre scene data and to update the Renderer.  The design goal was to
 * update the renderer in a separate thread as fast as possible and with minimal use of explicit locks.  As implemented here, the way we
 * achieved this was to create a global queue of tasks that make non-thread-safe changes to Ogre scene data, and have the RenderingLoop process
 * those changes before every call to renderOneFrame.  In the current implementation, these tasks are held in a tbb::concurrent_queue that
 * allows for concurrent push/pop operations on the container without locks (previous implementations had to be heavily locked as the container
 * was an STL list or queue).  Once RenderingLoop has processed the number of tasks that were in the task queue at the start of DoUpdates(), it
 * renders the next frame of the scene.  This rendering loop is not locked to the GGE's Engine update loop so it can and will update as fast as
 * possible.  This, however, also allows for race conditions and syncing problems with game logic calls to Ogre Scene data that might precede
 * expected updates to those objects that are in the task queue but have not been processed.  This is an unavoidable issue at present with the
 * current implementation of the GGE GameObject structure.  Individual workarounds have been implemented as needed but a more sweeping change
 * will be needed to deal with the root issues.
 */	
class RenderingLoop{

public:

	/** DoUpdates is the function that processes the task queue held by the instance of Engine.
	* A void function DoUpdates()
	* DoUpdates is the function that processes the task queue held by the instance of Engine.  It utilizes a lock provided by the TBB library
	* to prevent updates during certain scene transitions.  The task queue is implemented with a tbb::concurrent_queue and is iterated through
	* without the need for locks as the try_pop method is a thread-safe operation.
	*/
	void DoUpdates()
	{
		if (EnginePtr->taskQueueSize>0 && !(EnginePtr->screenTransition))
		{
			int size = EnginePtr->taskQueueSize;
			TaskContainer* task;
			for (int i = 0; i < size && !(EnginePtr->screenTransition); i++)
			{
				if(EnginePtr->taskQueue.try_pop(task) && !(EnginePtr->screenTransition))
					task->Process();
			}				
			EnginePtr->taskQueueSize -= size;
		}	
	}

	/** operator is a function that must be implemented for classes that are spawned as threads using the TBB library.
	* A void function operator ()()
	* operator is a function that must be implemented for classes that are spawned as threads using the TBB library.  operator serves as
	* the "main" function for the thread.  In this implementation, we have a while loop that processes the task queue and calls renderOneFrame
	* in as tight a loop as possible.  We utilize a scoped lock provided by the TBB library to prevent updates during screen transitions.
	*/
	void operator ()()
	{
		while (EnginePtr->keepRendering)
		{
			Engine::GameScreenDeleteMutexType::scoped_lock lock(EnginePtr->GameScreenLock);
			if (!EnginePtr->initializing)
			{
				DoUpdates();
				EnginePtr->RenderUpdate();
			}
		}
	}
};


/** 
 * UtilitiesUpdateChildTask class.
 * The UtilitiesUpdateChildTask class inherits tbb::task, a class provided by the TBB library, and serves as a thin wrapper for the
 * UtilitiesPtr->_Update() call in Engine::Update().  This allows for the call to be scheduled by the TBB task scheduler and done
 * concurrently.
 */	
class UtilitiesUpdateChildTask: public tbb::task{
public:
	
	/**
	* UtilitiesUpdateChildTask constructor.
	* UtilitiesUpdateChildTask constructor.
	*/
	UtilitiesUpdateChildTask() {}
	
	/**
	* A tbb::task* function execute()
	* execute is an inherited function from tbb::task that calls UtilitiesPtr->_Update() and returns a null pointer to the TBB task
	* scheduler to signal that the task has finished executing (other options are available for return depending on the implementation).
	*/
	tbb::task* execute () {
		UtilitiesPtr->_Update();
		return NULL;
	}
};

/** 
 * FlashUpdateChildTask class.
 * The FlashUpdateChildTask class inherits tbb::task, a class provided by the TBB library, and serves as a thin wrapper for the
 * FlashGUIPtr->_Update() call in Engine::Update().  This allows for the call to be scheduled by the TBB task scheduler and done
 * concurrently.
 */	
class FlashUpdateChildTask: public tbb::task{
public:

	/**
	* FlashUpdateChildTask constructor.
	* FlashUpdateChildTask constructor.
	*/
	FlashUpdateChildTask() {}

	/**
	* A tbb::task* function execute()
	* execute is an inherited function from tbb::task that calls FlashGUIPtr->_Update() and returns a null pointer to the TBB task
	* scheduler to signal that the task has finished executing (other options are available for return depending on the implementation).
	*/
	tbb::task* execute () {
		FlashGUIPtr->_Update();
		return NULL;
	}
};

/** 
 * GraphicsUpdateChildTask class.
 * The GraphicsUpdateChildTask class inherits tbb::task, a class provided by the TBB library, and serves as a thin wrapper for the
 * EnginePtr->UpdateGraphics() call in Engine::Update().  This allows for the call to be scheduled by the TBB task scheduler and done
 * concurrently.  This is not used in the current implementation.
 */	
class GraphicsUpdateChildTask: public tbb::task{
public:

	/**
	* GraphicsUpdateChildTask constructor.
	* GraphicsUpdateChildTask constructor.
	*/
	GraphicsUpdateChildTask() {}
	
	/**
	* A tbb::task* function execute()
	* execute is an inherited function from tbb::task that calls EnginePtr->UpdateGraphics() and returns a null pointer to the TBB task
	* scheduler to signal that the task has finished executing (other options are available for return depending on the implementation).
	*/
	tbb::task* execute () {
		EnginePtr->UpdateGraphics();
		return NULL;
	}
};

/** 
 * GraphicsUpdateChildTask class.
 * The PhysicsUpdateChildTask class inherits tbb::task, a class provided by the TBB library, and serves as a thin wrapper for the
 * EnginePtr->UpdatePhysics() call in Engine::Update().  This allows for the call to be scheduled by the TBB task scheduler and done
 * concurrently.  EnginePtr->UpdatePhysics() is not used in the current implementation.
 */	
class PhysicsUpdateChildTask: public tbb::task{
public:

	/**
	* PhysicsUpdateChildTask constructor.
	* PhysicsUpdateChildTask constructor.
	*/
	PhysicsUpdateChildTask() {}
	
	/**
	* A tbb::task* function execute()
	* execute is an inherited function from tbb::task that calls EnginePtr->UpdatePhysics() and returns a null pointer to the TBB task
	* scheduler to signal that the task has finished executing (other options are available for return depending on the implementation).
	*/
	tbb::task* execute () {
		EnginePtr->UpdatePhysics();
		return NULL;
	}
};

/** 
 * InputUpdateChildTask class.
 * The InputUpdateChildTask class inherits tbb::task, a class provided by the TBB library, and serves as a thin wrapper for the
 * InputPtr->Update() call in Engine::Update().  This allows for the call to be scheduled by the TBB task scheduler and done
 * concurrently.
 */	
class InputUpdateChildTask: public tbb::task{
public:

	/**
	* InputUpdateChildTask constructor.
	* InputUpdateChildTask constructor.
	*/
	InputUpdateChildTask() {}
	
	/**
	* A tbb::task* function execute()
	* execute is an inherited function from tbb::task that calls InputPtr->Update() and returns a null pointer to the TBB task
	* scheduler to signal that the task has finished executing (other options are available for return depending on the implementation).
	* execute utilizes a lock to prevent it from executing during screen deletion.
	*/
	tbb::task* execute () {
		Engine::GameScreenDeleteMutexType::scoped_lock lock(EnginePtr->GameScreenLock);
		if (EnginePtr->GetScreensToDelete())
		{
			return NULL;
		}
		else
		{
			InputPtr->Update();
			return NULL;
		}	
	}
};


#endif
#endif