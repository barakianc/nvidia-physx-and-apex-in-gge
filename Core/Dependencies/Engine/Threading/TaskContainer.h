////////TaskContainer.h - Container class to keep consistent interface for updating various QueuedTask derived classes
#ifndef TASK_CONTAINER_H
#define TASK_CONTAINER_H

#include "TaskTypes.h"

/** The TaskContainer class is a thin container class for classes inheriting QueuedTask.
 * TaskContainer class.
 * The TaskContainer class is a thin container class for classes inheriting QueuedTask.  It's purpose is facilitate use of containers like the
 * tbb::concurrent_queue that serve as the global queue of tasks to be performed by the renderer.  It acts like a Node class in a C style linked
 * list implementation, taking a void* and enumerated task type as parameters allowing it to cast and call the appropriate instance of UpdateTask()
 * when Process() is invoked.
 */
class TaskContainer
{	
public:

	/**
	* void* public variable.
	* A pointer to an instance of a queued task.
	*/
	void* task;

	/**
	* TASKS public variable.
	* An enumeration specifying what type of task is pointed to by void* task.  For casting purposes.
	*/
	TASKS taskType;

	/**
	* TaskContainer constructor.
	* TaskContainer constructor that takes a void* and TASKS type as parameters.
	* @param void*
	* @param TASKS
	*/
	TaskContainer(void* incTask, TASKS incTaskType) : task(incTask), taskType(incTaskType) {}
	
	/**
	* TaskContainer deconstructor.
	* TaskContainer deconstructor that cleans up the pointed to task.
	* @param void*
	* @param TASKS
	*/
	~TaskContainer() { delete task; }

	/**
	* A void function Process()
	* Process casts the void* task back to its original class and then invokes that classes implementation of UpdateTask()
	*/
	void Process();
};
#endif
