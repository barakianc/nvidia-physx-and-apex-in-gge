/////////////////////////////////////////////
#include "StdAfx.h"

#include "CreateHavokShapeTask.h"

void CreateHavokShapeTask::UpdateTask() {
	if (type == COLLISION_SHAPE_XYPLANE)
		obj->m_pOgreEntity = obj->createOgreEntity(obj->m_sOgreUniqueName,COLLISION_SHAPE_XYPLANE);
	else if (type == COLLISION_SHAPE_SPHERE)
		obj->m_pOgreEntity = obj->createOgreEntity(obj->m_sOgreUniqueName,COLLISION_SHAPE_SPHERE);
	else if (type == COLLISION_SHAPE_BOX)
		obj->m_pOgreEntity = obj->createOgreEntity(obj->m_sOgreUniqueName,COLLISION_SHAPE_BOX);

	halfSize = obj->m_pOgreEntity->getBoundingBox().getHalfSize();

	if (obj->m_pOgreSceneNode!=NULL)
		obj->m_pOgreSceneNode->attachObject(obj->m_pOgreEntity);
	else {
		obj->m_pOgreSceneNode = obj->m_pOgreSceneManager->getRootSceneNode()->createChildSceneNode();
		obj->m_pOgreSceneNode->attachObject(obj->m_pOgreEntity);
	}
	//-->this scale parameter is set here in order to make the HavokShape and OgreShape the same size
	if (type != COLLISION_SHAPE_XYPLANE)
		obj->m_pOgreSceneNode->scale(1/halfSize.x,1/halfSize.y,1/halfSize.z);
	else
		obj->m_pOgreSceneNode->scale(100.0f/halfSize.x,0.01f/halfSize.y,100.0f/halfSize.z);
};
