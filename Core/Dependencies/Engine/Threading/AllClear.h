/////////////AllClear.h - ///////////////////////////////////////

#ifndef ALL_CLEAR_H
#define ALL_CLEAR_H

#include "QueuedTask.h"
#include "GameObject.h"

 
/** The AllClear class is used in conjunction with CreateDefaultGraphicsObjectTask or any QueuedTask that requires that a GraphicsObject that has been queued to be created.
 * AllClear class.
 * The AllClear class is used in conjunction with CreateDefaultGraphicsObjectTask or any QueuedTask that requires that a GraphicsObject that has been
 * queued to be created.  The motivation is that rather than add the creation of a PhysicsObject or AnimationObject to the TaskQueue, we push the
 * AllClear task after the queued GraphicsObject creation task to signal back to the GameObject that the GraphicsObject has successfully created when
 * it (the AllClear task) gets processed.  At that point, the GameObject can create the required objects on its next Update call.  This workaround
 * was required by problems allocating Havok objects accross threads (the TaskQueue is processed from the Rendering thread at present, not the main thread
 * that created and holds the HavokWorld and corresponding memory pool).
 */
class AllClear : public QueuedTask
{
private:

	/**
	* GameObject* private variable.
	* A pointer to the GameObject whose boolean values AllClear will set when its processed to signal that it can create a PhysicsObject and/or
	* AnimationObject on its next Update call.
	*/
	GameObject* obj;
public:

	/**
	* AllClear constructor.
	* AllClear constructor that takes the GameObject it will signal as a parameter
	* @param GameObject*
	*/
	AllClear(GameObject* incObj) : obj(incObj) {}

	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that sets two boolean values when it is called.
	*/
	void UpdateTask() {
#ifdef TBB_PARALLEL_OPTION
		obj->animRdy = true;
		obj->physicsRdy = true;
#endif	
	}
};


#endif