////////////ConsoleUpdateTask.h - ///////////////////////////////////

#ifndef CONSOLE_UPDATE_TASK_H
#define CONSOLE_UPDATE_TASK_H

#include "QueuedTask.h"

/** The ConsoleUpdatePositionTask class handles the updating of positions and bounding boxes for Ogre::Rectangle2d and Ogre::OverlayElements managed by OgreConsole with TBB_PARALLEL_OPTION enabled.
 * ConsoleUpdatePositionTask class.
 * The ConsoleUpdatePositionTask class handles the updating of positions and bounding boxes for Ogre::Rectangle2d and Ogre::OverlayElements
 * managed by OgreConsole with TBB_PARALLEL_OPTION enabled.
 */

class ConsoleUpdatePositionTask : public QueuedTask
{
private:

	/**
	* float private variable.
	* The height for the textbox OverlayElement and corresponding Rectangle2d
	*/
	float m_height;

	/**
	* Ogre::Rectangle2D* private variable.
	* Overlay container for a textbox
	*/
	Ogre::Rectangle2D *m_rectangle;

	/**
	* Ogre::OverlayElement* private variable.
	* OverlayElement that is used as a textbox
	*/
	Ogre::OverlayElement *m_textbox;
public:

	/**
	* ConsoleUpdatePositionTask constructor.
	* ConsoleUpdatePositionTask constructor that takes as paremeters Ogre overlay elements whose position will be determined as a
	* function of the height parameter.
	* @param Ogre::Rectangle2D*
	* @param Ogre::OverlayElement*
	* @param float
	*/
	ConsoleUpdatePositionTask(Ogre::Rectangle2D *incRectangle, Ogre::OverlayElement *incTextbox, float incHeight) : m_rectangle(incRectangle), m_textbox(incTextbox), m_height(incHeight) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that sets the position of the specified overlay elements.
	*/
	void UpdateTask(){
		m_textbox->setPosition(0.0f,(m_height-1)*0.5f);
		m_rectangle->setCorners(-1,1+m_height,1,1-m_height);
		m_rectangle->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
	}
};

/** 
 * ConsoleUpdateTextTask class.
 * The ConsoleUpdateTextTask class handles the updating of text for existing textbox OverlayElements.
 */

class ConsoleUpdateTextTask : public QueuedTask
{
private:

	/**
	* Ogre::OverlayElement* private variable.
	* OverlayElement that is used as a textbox.
	*/
	Ogre::OverlayElement *m_textbox;

	/**
	* Ogre::String private variable.
	* String that holds the new caption for the specified textbox.
	*/
	Ogre::String text;
public:

	/**
	* ConsoleUpdateTextTask constructor.
	* ConsoleUpdateTextTask constructor that takes as paremeters an Ogre OverlayElement and text String that will serve as the
	* OverlayElement's new caption.
	* @param Ogre::OverlayElement*
	* @param Ogre::String
	*/
	ConsoleUpdateTextTask(Ogre::OverlayElement *incTextbox, Ogre::String incText) : m_textbox(incTextbox), text(incText) {}

	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that sets the text of the specified overlay elements.
	*/
	void UpdateTask() {  m_textbox->setCaption(text); }
};

#endif