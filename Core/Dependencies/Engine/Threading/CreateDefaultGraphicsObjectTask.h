///////CreateDefaultGraphicsObjectTask.h - ////////////////////

#ifndef CREATE_DEFAULT_GRAPHICS_OBJECT_TASK_H
#define CREATE_DEFAULT_GRAPHICS_OBJECT_TASK_H

#include "QueuedTask.h"
#include "GraphicsObject.h"


/** The CreateDefaultGraphicsObjectTask class implements most of the GraphicsObject constructor for deferred creation.
 * CreateDefaultGraphicsObjectTask class.
 * The CreateDefaultGraphicsObjectTask class implements most of the GraphicsObject constructor particularly the creation of the a SceneNode
 * for the GameObject, the creation of an OgreEntity, and the attachment of the latter to the former.  As with most classes inheriting QueuedTask,
 * instances of this class are queued and processed by the rendering thread so as to prevent writing to Ogre scene data while the renderer is
 * rendering a frame.
 */
class CreateDefaultGraphicsObjectTask : public QueuedTask
{
private:

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject whose constructor CreateDefaultGraphicsObjectTask partially implements.
	*/
	GraphicsObject* obj;

	/**
	* GameObjectType private variable.
	* An enumeration of GameObject types
	*/
	GameObjectType type;

	/**
	* Ogre::String private variable.
	* A Ogre::String that holds the file name/path of the mesh for this object
	*/
	Ogre::String fileName;
public:

	/**
	* CreateDefaultGraphicsObjectTask constructor.
	* CreateDefaultGraphicsObjectTask constructor that takes a GraphicsObject pointer, GameObjectType and path to mesh file as parameters
	* @param GraphicsObject*
	* @param GameObjectType
	* @param Ogre::String
	*/
	CreateDefaultGraphicsObjectTask(GraphicsObject* incObj, GameObjectType incType, Ogre::String incFileName) : obj(incObj), type(incType), fileName(incFileName) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that partially implments the GraphicObject constructor, finishing the
	* creation of SceneNodes and Entities correpsonding to the passed in GraphicsObject
	*/
	void UpdateTask();
};



#endif