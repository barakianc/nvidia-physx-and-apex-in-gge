/////QueuedTask.h - Abstract base class for GameObject tasks that are queued in order to support independent rendering thread
#ifndef QUEUED_TASK_H
#define QUEUED_TASK_H

/** The QueuedTask class is an abstract base class that is inherited by all tasks that are queued for updates by the Rendering thread.
 * QueuedTask abstract base class.
 * The QueuedTask class is an abstract base class that is inherited by all tasks that are queued for updates by the Rendering thread.
 * It specifies a minimal common interface and provides a way to, in the future, easily inherit true TBB tasks without changing the task
 * interface/classes already written.
 */
class QueuedTask{
public:
	/**
	* A virtual void function UpdateTask()
	* UpdateTask is a pure virtual function that specifies the interface all classes that inherit QueuedTask must use and implement.
	*/
	virtual void UpdateTask() = 0;
};

#endif