/////////////////////CreateCharacterProxyObjectTask.h - /////////////////////////////////

#ifndef CREATE_CHARACTER_PROXY_OBJECT_TASK_H
#define CREATE_CHARACTER_PROXY_OBJECT_TASK_H

#include "QueuedTask.h"
#include "GameObject.h"
#include "GraphicsObject.h"

#ifdef HAVOK
#include "HavokObject.h"
#endif


/** The CreateCharacterProxyObjectTask class is a wrapper for the allocation of a new CharacterProxyObject.
 * CreateCharacterProxyObjectTask class.
 * The CreateCharacterProxyObjectTask class is a wrapper for the allocation of a new CharacterProxyObject.  The class exists so as to queue the
 * creation of the PhyicsObject until a later time at which it can be guaranteed that the GraphicsObject which it references and that has also
 * been queued has been created.  Otherwise, the constructor of CharacterProxyObject will reference elements of the GraphicsObject it is passed
 * that have not been initialized/properly allocated.  As with most classes inheriting QueuedTask, instances of this class are queued and processed
 * by the rendering thread so as to prevent writing to Ogre scene data while the renderer is rendering a frame.
 */
class CreateCharacterProxyObjectTask : public QueuedTask
{
private:
	
	/**
	* GameObject* private variable.
	* A pointer to the GameObject which holds the PhysicsObject to be created.
	*/
	GameObject* obj;

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that is a parameter for the constructor of CharacterProxyObject.
	*/
	GraphicsObject* 	f_pGraphicsObject;
public:

	/**
	* CreateCharacterProxyObjectTask constructor.
	* CreateCharacterProxyObjectTask constructor that takes a GameObject and GraphicsObject pointer as parameters.
	* @param GameObject*
	* @param GraphicsObject*
	*/
	CreateCharacterProxyObjectTask(GameObject* incObj, GraphicsObject* incGraphObj) : obj(incObj), f_pGraphicsObject(incGraphObj) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that allocates a new CharacterProxyObject and sets the appropriate
	* pointer in GameObject to the new PhysicsObject.
	*/
	void UpdateTask(){
#ifdef HAVOK
		PhysicsObject* physObj = new CharacterProxyObject(f_pGraphicsObject);
		obj->m_pPhysicsObject = physObj;
#endif
	}
};

#endif
