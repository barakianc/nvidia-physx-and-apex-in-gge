/////////////SetVisibleTask.h - ///////////////////////////////////////

#ifndef SET_VISIBLE_TASK_H
#define SET_VISIBLE_TASK_H

#include "QueuedTask.h"
#include "GraphicsObject.h"

/** The SetVisibleTask class sets the visibility of an Ogre entity.
 * SetVisibleTask class.
 * The SetVisibleTask class sets the visibility of an Ogre entity.  As with most classes inheriting QueuedTask, instances of this 
 * class are queued and processed by the rendering thread so as to prevent writing to Ogre scene data while the renderer is rendering 
 * a frame.
 */
class SetVisibleTask : public QueuedTask
{
private:

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that points to the Ogre::Entity that will be made visible/invisible.
	*/
	GraphicsObject* obj;

	/**
	* Boolean private variable.
	* A boolean that specifies whethere the Ogre::Entity will be made visible/invisible.
	*/
	bool visibility;
public:

	/**
	* SetVisibleTask constructor.
	* SetVisibleTask constructor that takes a GraphicsObject pointer and boolean as parameters.
	* @param GraphicsObject*
	* @param bool
	*/
	SetVisibleTask(GraphicsObject* incObj, bool incBool) : obj(incObj), visibility(incBool) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that sets the visibility of the pointed to Ogre::Entity
	* based on the passed in boolean parameter.
	*/
	void UpdateTask() {
		if (obj->m_pOgreEntity)
			obj->m_pOgreEntity->setVisible(visibility);
	}
};

#endif