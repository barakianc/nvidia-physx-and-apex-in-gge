///////CameraUpdateTask.h - ////////////////////////////////

#ifndef CAMERA_UPDATE_TASK_H
#define CAMERA_UPDATE_TASK_H
#include "OgreSceneNode.h"
#include "OgreVector3.h"
#include "OgreQuaternion.h"
#include "QueuedTask.h"

/** 
 * enum CameraTaskType.
 * Enumeration of the different camera updates that can be invoked by CameraUpdateTask
 */

enum CameraTaskType{
	Update_Pos_Rot,
	Update_Pos_LookAt,
	Update_Pos_Target,
	Translate_Pos_Target,
	Attach
};

/** CameraUpdateTask implements several common Ogre camera update routines that are not otherwise threadsafe and must therefore be queued in order to be processed at a predictable time.
 * CameraUpdateTask class.
 * CameraUpdateTask implements several common Ogre camera update routines that are not otherwise threadsafe and must therefore be queued in order
 * to be processed at a predictable time.  The type of update is specified by an enumeration CameraTaskType and the overloaded constructor.  Every
 * method of GameCamera that writes to Ogre scene data has a corresponding entry here.
 */

class CameraUpdateTask : public QueuedTask
{
private:
	/**
	* Ogre::SceneNode* private variable.
	* Pointer to a GameCamera SceneNode that is used in several of the update calls 
	*/
	Ogre::SceneNode* camNode;

	/**
	* Ogre::SceneNode* private variable.
	* Pointer to a GameCamera target node that is used in several of the update calls.  The target scene node is the look at point of the camera.
	*/
	Ogre::SceneNode* tarNode;

	/**
	* Ogre::Entity* private variable.
	* Pointer to an Ogre entity to which a GameCamera SceneNode is attached
	*/
	Ogre::Entity*	ent;

	/**
	* Ogre::Vector3 private variable.
	* Ogre::Vector3 that holds the camera's position for certain updates
	*/
	Ogre::Vector3 camVect;

	/**
	* Ogre::Vector3 private variable.
	* Ogre::Vector3 that holds the target's position for certain updates
	*/
	Ogre::Vector3 tarVect;

	/**
	* Ogre::Quaternion private variable.
	* Ogre::Quaternion that holds the camera's orientation for certain updates
	*/
	Ogre::Quaternion rot;

	/**
	* CameraTaskType private variable.
	* Enumeration that specifies the type of update to be done
	*/
	CameraTaskType type;
public:

	/**
	* CameraUpdateTask constructor 1.
	* CameraUpdateTask constructor for postion/orientation update of camera node
	* @param Ogre::SceneNode*
	* @param Ogre::Vector3
	* @param Ogre::Quaternion
	* @param CameraTaskType
	*/
	CameraUpdateTask(Ogre::SceneNode* incNode, Ogre::Vector3 incVect, Ogre::Quaternion incRot, CameraTaskType incType) : camNode(incNode), camVect(incVect), rot(incRot), type(incType) {}
	
	/**
	* CameraUpdateTask constructor 2.
	* CameraUpdateTask constructor for postion/look at point update of camera node
	* @param Ogre::SceneNode*
	* @param Ogre::SceneNode*
	* @param Ogre::Vector3
	* @param CameraTaskType
	*/
	CameraUpdateTask(Ogre::SceneNode* incNode, Ogre::SceneNode* incNode2, Ogre::Vector3 incVect, CameraTaskType incType) : camNode(incNode), tarNode(incNode2), camVect(incVect), type(incType) {}
	
	/**
	* CameraUpdateTask constructor 3.
	* CameraUpdateTask constructor for postion update of camera/target nodes or translation of camera/target nodes as specified by type
	* @param Ogre::SceneNode*
	* @param Ogre::SceneNode*
	* @param Ogre::Vector3
	* @param Ogre::Vector3
	* @param CameraTaskType
	*/
	CameraUpdateTask(Ogre::SceneNode* incNode, Ogre::SceneNode* incNode2, Ogre::Vector3 incVect, Ogre::Vector3 incVect2, CameraTaskType incType) : camNode(incNode), tarNode(incNode2), camVect(incVect), tarVect(incVect2), type(incType) {}
	
	/**
	* CameraUpdateTask constructor 4.
	* CameraUpdateTask constructor for camera node entity attachment
	* @param Ogre::SceneNode*
	* @param Ogre::Entity*
	* @param CameraTaskType
	*/
	CameraUpdateTask(Ogre::SceneNode* incNode, Ogre::Entity* incEnt, CameraTaskType incType) : camNode(incNode), ent(incEnt), type(incType) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that makes the specified camera updates
	*/
	void UpdateTask() {
		if(type == Update_Pos_Rot){
			if (camNode){
				camNode->setPosition(camVect);
				camNode->setOrientation(rot);
			}
		}
		else if (type == Update_Pos_LookAt){
			camNode->setPosition(camVect);
			camNode->lookAt(tarNode->getPosition(), Ogre::Node::TS_WORLD);
		}
		else if (type == Update_Pos_Target){
			camNode->setPosition (camVect);
			tarNode->setPosition (tarVect);
		}
		else if (type == Translate_Pos_Target){
			camNode->translate (camVect);
			tarNode->translate (tarVect);
		}
		/*else if (type == Attach){
			camNode->attachObject(ent);
		}*/
	}
};

#endif
