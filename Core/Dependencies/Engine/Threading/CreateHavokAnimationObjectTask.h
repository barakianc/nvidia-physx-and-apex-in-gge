///////////////////CreateHavokAnimationObjectTask.h - ////////////////////////////////

#ifndef CREATE_HAVOK_ANIMATION_OBJECT_TASK_H
#define CREATE_HAVOK_ANIMATION_OBJECT_TASK_H

#include "QueuedTask.h"
#include "GameObject.h"
#include "GraphicsObject.h"

#ifdef HAVOK
#include "HavokObject.h"
#endif

#include "AnimatedHavokEntity.h"


/** The CreateHavokAnimationObjectTask class is a wrapper for the CreateHavokAnimationObject constructor.
 * CreateHavokAnimationObjectTask class.
 * The CreateHavokAnimationObjectTask class is a wrapper for the CreateHavokAnimationObject constructor.  The class exists so as to queue the
 * creation of the AnimationObject until a later time at which it can be guaranteed that the GraphicsObject which it references and that has also
 * been queued has been created.  Otherwise, the constructor of CreateHavokAnimationObject will reference elements of the GraphicsObject it is passed
 * that have not been initialized/properly allocated.  As with most classes inheriting QueuedTask, instances of this class are queued but
 * CreateHavokAnimationObjectTask is processed by the main thread during the next update call to the associated GameObject.
 */
class CreateHavokAnimationObjectTask : public QueuedTask
{
private:

	/**
	* GameObject* private variable.
	* A pointer to the GameObject which holds the AnimationObject to be created.
	*/
	GameObject* obj;

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that is a parameter for the constructor of CreateHavokAnimationObject.
	*/
	GraphicsObject* 	f_pGraphicsObject;

	/**
	* std::string private variable.
	* A std::string	of the path to the .hkx file for this object.
	*/
	std::string			path;

	/**
	* std::string private variable.
	* A std::string	of the unique name for this object.
	*/
	std::string			name;
public:

	/**
	* CreateHavokAnimationObjectTask constructor.
	* CreateHavokAnimationObjectTask constructor that takes a GameObject, path name to a .hkx file, unique name, and GraphicsObject 
	* pointer as parameters.
	* @param GameObject*
	* @param std::string
	* @param std::string
	* @param GraphicsObject*
	*/
	CreateHavokAnimationObjectTask(GameObject* incObj, std::string incPath, std::string incName, GraphicsObject* incGraphObj) : obj(incObj), path(incPath), name(incName), f_pGraphicsObject(incGraphObj) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that allocates a new CreateHavokAnimationObject and sets the appropriate
	* pointer in GameObject to the new AnimationObject
	*/
	void UpdateTask() {
		obj->m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateHavokAnimationObject(f_pGraphicsObject,path.c_str(), name.c_str()));
	}
};


#endif 