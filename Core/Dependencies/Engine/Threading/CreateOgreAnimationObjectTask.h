///////////////////CreateHavokAnimationObjectTask.h - ////////////////////////////////

#ifndef CREATE_OGRE_ANIMATION_OBJECT_TASK_H
#define CREATE_OGRE_ANIMATION_OBJECT_TASK_H

#include "QueuedTask.h"
#include "GameObject.h"
#include "GraphicsObject.h"
#include "AnimatedHavokEntity.h"


/** The CreateOgreAnimationObjectTask class is a wrapper for the CreateOgreAnimationObjectTask constructor.
 * CreateOgreAnimationObjectTask class.
 * The CreateOgreAnimationObjectTask class is a wrapper for the CreateOgreAnimationObjectTask constructor.  The class exists so as to queue the
 * creation of the AnimationObject until a later time at which it can be guaranteed that the GraphicsObject which it references and that has also
 * been queued has been created.  Otherwise, the constructor of CreateOgreAnimationObjectTask will reference elements of the GraphicsObject it is passed
 * that have not been initialized/properly allocated.  As with most classes inheriting QueuedTask, instances of this class are queued but
 * CreateOgreAnimationObjectTask is processed by the main thread during the next update call to the associated GameObject.
 */
class CreateOgreAnimationObjectTask : public QueuedTask
{
private:

	/**
	* GameObject* private variable.
	* A pointer to the GameObject which holds the AnimationObject to be created.
	*/
	GameObject* obj;

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that is a parameter for the constructor of CreateHavokAnimationObject.
	*/
	GraphicsObject* 	m_pGraphicsObject;

	/**
	* std::string private variable.
	* A std::string	of the unique name for this object.
	*/
	std::string			name;

	/**
	* std::string private variable.
	* A std::string	of the mesh name for this object.
	*/
	std::string			meshName;
public:

	/**
	* CreateOgreAnimationObjectTask constructor.
	* CreateOgreAnimationObjectTask constructor that takes a GameObject, unique name, mesh name, and GraphicsObject 
	* pointer as parameters.
	* @param GameObject*
	* @param std::string
	* @param std::string
	* @param GraphicsObject*
	*/
	CreateOgreAnimationObjectTask(GameObject* incObj, std::string incPath, std::string incName, GraphicsObject* incGraphObj) : obj(incObj), meshName(incPath), name(incName), m_pGraphicsObject(incGraphObj) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that allocates a new CreateOgreAnimationObject and sets the appropriate
	* pointer in GameObject to the new AnimationObject
	*/
	void UpdateTask() {
		obj->m_pAnimatedEntity = static_cast<AnimationManager::AnimatedEntity*>(AnimationManager::Manager::GetSingleton()->CreateOgreAnimationObject(m_pGraphicsObject, meshName.c_str(), name.c_str()));
	}
};


#endif 