///////////////////////////////////////////////
#include "StdAfx.h"

#include "ChangeSceneNodeTask.h"

void ChangeSceneNodeTask::UpdateTask() {
	Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	if (obj->m_pOgreSceneNode)
	{
		obj->m_pOgreSceneNode->detachObject(obj->m_pOgreEntity);
		f_pOgreSceneManager->destroySceneNode(obj->m_pOgreSceneNode);
		obj->m_pOgreSceneNode = node;
		obj->m_pOgreSceneNode->attachObject(obj->m_pOgreEntity);
	}
};