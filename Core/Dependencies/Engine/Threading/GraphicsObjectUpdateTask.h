//////////GraphicsObjectUpdateTask.h - //////////////////////////////

#ifndef GRAPHICS_OBJECT_UPDATE_TASK_H
#define GRAPHICS_OBJECT_UPDATE_TASK_H

#include "GraphicsObject.h"
#include "QueuedTask.h"


/** The GraphicsObjectUpdateTask class is a utility class for updating Ogre scene data.
 * GraphicsObjectUpdateTask class.
 * The GraphicsObjectUpdateTask class is a utility class for updating Ogre scene data.  It can be used to either update the position/rotation of
 * Ogre Nodes that are not explicitly tied to a GGE GraphicsObject or perform the same operations for Nodes that are already tied to a GraphicsObject.
 * As with most classes inheriting QueuedTask, instances of this class are queued and processed by the rendering thread so as to prevent writing 
 * to Ogre scene data while the renderer is rendering a frame.
 */
class GraphicsObjectUpdateTask : public QueuedTask
{
private:

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that holds the Ogre::Node being manipulated.
	*/
	GraphicsObject* obj;

	/**
	* float private variables.
	* Floats holding alternatively the position or rotation components of Node manipulations.
	*/
	float x,y,z,w;

	/**
	* boolean private variables.
	* Bool set at construction that designates whether UpdateTask will perform a position or rotation update.
	*/
	bool isPosUpdate;

	/**
	* Ogre::Node* private variables.
	* Ogre::Node* that stores a node to be manipulated (used in cases where a GraphicsObject is not passed as a parameter).
	*/
	Ogre::Node* node;
public:

	/**
	* GraphicsObjectUpdateTask constructor.
	* GraphicsObjectUpdateTask constructor that takes a pointer to a GraphicsObject and three floats as position vector components as
	* parameters.
	* @param GraphicsObject*
	* @param float
	* @param float
	* @param float
	*/
	GraphicsObjectUpdateTask(GraphicsObject* incObj, float incX, float incY, float incZ) : obj(incObj), x(incX), y(incY), z(incZ), isPosUpdate(true), node(NULL) {}
	
	/**
	* GraphicsObjectUpdateTask constructor.
	* GraphicsObjectUpdateTask constructor that takes a pointer to a GraphicsObject and four floats as Quaternion components as
	* parameters. 
	* @param GraphicsObject*
	* @param float
	* @param float
	* @param float
	* @param float
	*/
	GraphicsObjectUpdateTask(GraphicsObject* incObj, float incW, float incX, float incY, float incZ) : obj(incObj), x(incX), y(incY), z(incZ), w(incW), isPosUpdate(false), node(NULL)  {}
	
	/**
	* GraphicsObjectUpdateTask constructor.
	* GraphicsObjectUpdateTask constructor that takes a pointer to an Ogre::Node and three floats as position vector components as
	* parameters.
	* @param Ogre::Node*
	* @param float
	* @param float
	* @param float
	*/
	GraphicsObjectUpdateTask(Ogre::Node* incNode, float incX, float incY, float incZ) : node(incNode), x(incX), y(incY), z(incZ), isPosUpdate(true), obj(NULL)  {}
	
	/**
	* GraphicsObjectUpdateTask constructor.
	* GraphicsObjectUpdateTask constructor that takes a pointer to an Ogre::Node and four floats as Quaternion components as
	* parameters.  
	* @param Ogre::Node*
	* @param float
	* @param float
	* @param float
	* @param float
	*/
	GraphicsObjectUpdateTask(Ogre::Node* incNode, float incW, float incX, float incY, float incZ) : node(incNode), x(incX), y(incY), z(incZ), w(incW), isPosUpdate(false), obj(NULL)  {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that updates the position or rotation of an Ogre::Node
	*/
	void UpdateTask() {
		if(obj){
			if(isPosUpdate){
				if (obj->m_pOgreSceneNode) obj->m_pOgreSceneNode->setPosition(x,y,z); }
			else{
				if (obj->m_pOgreSceneNode) obj->m_pOgreSceneNode->setOrientation(w,x,y,z);}
		}
		else{
			if(isPosUpdate){
				if (node) node->setPosition(x,y,z); }
			else{
				if (node) node->setOrientation(w,x,y,z);}
		}
	}
};


#endif