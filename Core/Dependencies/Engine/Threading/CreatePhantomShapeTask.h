//////////CreateCharacterRigidBodyObjectTask.h - //////////////////////////

#ifndef CREATE_PHANTOM_SHAPE_TASK_H
#define CREATE_PHANTOM_SHAPE_TASK_H

#include "QueuedTask.h"
#include "GameObject.h"
#include "GraphicsObject.h"
#include "HavokObject.h"


/** The CreatePhantomShapeTask class is a wrapper for the allocation of a new CharacterProxyObject.
 * CreatePhantomShapeTask class.
 * The CreatePhantomShapeTask class is a wrapper for the allocation of a new CharacterProxyObject.  The class exists so as to queue the
 * creation of the PhyicsObject until a later time at which it can be guaranteed that the GraphicsObject which it references and that has also
 * been queued has been created.  Otherwise, the constructor of PhantomShapeObject will reference elements of the GraphicsObject it is passed
 * that have not been initialized/properly allocated.
 */
class CreatePhantomShapeTask : public QueuedTask
{
private:

	/**
	* GameObject* private variable.
	* A pointer to the GameObject which holds the PhysicsObject to be created.
	*/
	GameObject*						  obj;

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that is a parameter for the constructor of  PhantomShapeObject.
	*/
	GraphicsObject*		f_pGraphicsObject;

	/**
	* std::string private variable.
	* A string that holds the unique Ogre name of the object to be created.
	*/
	std::string					 ogreName;

	/**
	* CollisionShapeType private variable.
	* A enumeration that specifies what type of shape to be created.
	*/
	CollisionShapeType			shapeType;
public:

	/**
	* CreatePhantomShapeTask constructor.
	* CreatePhantomShapeTask constructor that takes a GameObject*, GraphicsObject*, std::string, and enumateration as parameters
	* @param GameObject*
	* @param std::string
	* @param CollisionShapeType
	* @param GraphicsObject*
	*/
	CreatePhantomShapeTask(GameObject* incObj, std::string incName, CollisionShapeType incType, GraphicsObject* incGraphObj) : obj(incObj), ogreName(incName), shapeType(incType), f_pGraphicsObject(incGraphObj) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that allocates a new PhantomShapeObject and sets the appropriate
	* pointer in GameObject to the new PhysicsObject
	*/
	void UpdateTask() {
#ifdef HAVOK
		obj->m_pPhysicsObject = new PhantomShapeObject(ogreName,shapeType,&f_pGraphicsObject);
#endif
	}
};

#endif