////////CreatePhantomAabbObjectTask.h - ////////////////////////////////////
#ifndef CREATE_PHANTOM_AABB_OBJECT_TASK_H
#define CREATE_PHANTOM_AABB_OBJECT_TASK_H

#include "QueuedTask.h"
#include "GameObject.h"
#include "GraphicsObject.h"

#ifdef HAVOK
#include "HavokObject.h"
#endif

/** The CreatePhantomAabbObjectTask class is a wrapper for the allocation of a new PhantomAabbObject.
 * CreatePhantomAabbObjectTask class.
 * The CreatePhantomAabbObjectTask class is a wrapper for the allocation of a new PhantomAabbObject.  The class exists so as to queue the
 * creation of the PhyicsObject until a later time at which it can be guaranteed that the GraphicsObject which it references and that has also
 * been queued has been created.  Otherwise, the constructor of PhantomAabbObject will reference elements of the GraphicsObject it is passed
 * that have not been initialized/properly allocated.
 */
class CreatePhantomAabbObjectTask : public QueuedTask
{
private:

	/**
	* GameObject* private variable.
	* A pointer to the GameObject which holds the PhysicsObject to be created.
	*/
	GameObject* obj;

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that is a parameter for the constructor of PhantomAabbObject.
	*/
	GraphicsObject* 	f_pGraphicsObject;
public:

	/**
	* CreatePhantomAabbObjectTask constructor.
	* CreatePhantomAabbObjectTask constructor that takes a GameObject and GraphicsObject pointer as parameters
	* @param GameObject*
	* @param GraphicsObject*
	*/
	CreatePhantomAabbObjectTask(GameObject* incObj, GraphicsObject* incGraphObj) : obj(incObj), f_pGraphicsObject(incGraphObj) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that allocates a new PhantomAabbObject and sets the appropriate
	* pointer in GameObject to the new PhysicsObject
	*/
	void UpdateTask() {
#ifdef HAVOK
		obj->m_pPhysicsObject = new PhantomAabbObject(f_pGraphicsObject);
#endif
	}
};

#endif