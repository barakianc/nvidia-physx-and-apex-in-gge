/////////////UtilitiesUpdateTask.h - ///////////////////////////////////////

#ifndef UTILITIES_UPDATE_TASK_H
#define UTILITIES_UPDATE_TASK_H

#include "QueuedTask.h"
#include "OgreTextAreaOverlayElement.h"
#include "OgreOverlayContainer.h"
#include "OgreString.h"
#include "OgreColourValue.h"

/** The UtilitiesUpdateTask class implements all of the updates made by the Utilities class during run time (as opposed to initialization).
 * UtilitiesUpdateTask class.
 * The UtilitiesUpdateTask class implements all of the updates made by the Utilities class during run time (as opposed to initialization).
 * The type of updated is specified by the choice of overloaded constructor.  As with most classes inheriting QueuedTask, instances of this
 * class are queued and processed by the rendering thread so as to prevent writing to Ogre scene data while the renderer is rendering a frame.
 */
class UtilitiesUpdateTask : public QueuedTask
{
private:

	/**
	* int private variable.
	* An integer that specifies the type of update to be done.  Set by type of constructor called.
	*/
	int updateType;

	/**
	* Ogre::TextAreaOverlayElement* private variable.
	* A pointer to an Ogre::TextAreaOverlayElement* that serves as a text area.
	*/
	Ogre::TextAreaOverlayElement* textArea;

	/**
	* const Ogre::String private variable.
	* An Ogre::String that serves as new text/font to be updated in a text area.
	*/
	const Ogre::String text;
	
	/**
	* boolean private variable.
	* A bool that specifies whether the text itself or the font will be updated in a text area.
	*/
	bool isSetText;
	
	/**
	* Ogre::OverlayContainer* private variable.
	* A pointer to an Ogre::OverlayContainer*.
	*/
	Ogre::OverlayContainer* panel;
	
	/**
	* const Ogre::Real private variable.
	* An Ogre::Real that specifies the x position of OverlayContainers or alternatively character height of text.
	*/
	const Ogre::Real x;
	
	/**
	* const Ogre::Real private variable.
	* An Ogre::Real that specifies the y position of OverlayContainers.
	*/
	const Ogre::Real y;
	
	/**
	* Ogre::ColourValue private variable.
	* An Ogre::ColourValue that specifies the new colour value of text.
	*/
	const Ogre::ColourValue colourValue;
public:

	/**
	* UtilitiesUpdateTask constructor.
	* UtilitiesUpdateTask constructor that takes an Ogre::TextAreaOverlayElement*, Ogre::String, and boolean as parameters.
	* @param Ogre::TextAreaOverlayElement*
	* @param const Ogre::String
	* @param bool
	*/
	UtilitiesUpdateTask(Ogre::TextAreaOverlayElement* incTextArea, const Ogre::String incText, bool setText) : textArea(incTextArea), text(incText), isSetText(setText), x(0.0), y(0.0), updateType (0) {}
	
	/**
	* UtilitiesUpdateTask constructor.
	* UtilitiesUpdateTask constructor that takes an Ogre::OverlayContainer* and two const Ogre::Real as parameters.
	* @param Ogre::OverlayContainer*
	* @param const Ogre::Real
	* @param const Ogre::Real
	*/
	UtilitiesUpdateTask(Ogre::OverlayContainer* incPanel, const Ogre::Real incX,const Ogre::Real incY) : panel(incPanel), x(incX), y(incY), updateType (1) {}
	
	/**
	* UtilitiesUpdateTask constructor.
	* UtilitiesUpdateTask constructor that takes an Ogre::TextAreaOverlayElement* and one const Ogre::Real as parameters.
	* @param Ogre::TextAreaOverlayElement*
	* @param const Ogre::Real
	*/
	UtilitiesUpdateTask(Ogre::TextAreaOverlayElement* incTextArea, const Ogre::Real charHeight) : textArea(incTextArea), x(charHeight), y(0.0), updateType (2) {}
	
	/**
	* UtilitiesUpdateTask constructor.
	* UtilitiesUpdateTask constructor that takes an Ogre::TextAreaOverlayElement* and a const Ogre::ColourValue as parameters.
	* @param const Ogre::ColourValue
	*/
	UtilitiesUpdateTask(Ogre::TextAreaOverlayElement* incTextArea, const Ogre::ColourValue incColour) : textArea(incTextArea), colourValue(incColour), x(0.0), y(0.0), updateType (3) {}
	
	/**
	* UtilitiesUpdateTask constructor.
	* UtilitiesUpdateTask constructor that takes an Ogre::OverlayContainer* and a Ogre::String as parameters.
	* @param Ogre::OverlayContainer*
	* @param const Ogre::String
	*/
	UtilitiesUpdateTask(Ogre::OverlayContainer* incPanel, const Ogre::String incText) : panel(incPanel), text(incText), x(0.0), y(0.0), updateType (4) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that updates the various Overlay elements handled by Utilities.  The
	* type of update performed is determined by the constructor used.
	*/
	void UpdateTask() { 
		switch(updateType){
			case 0:
				if(isSetText)
					textArea->setCaption(text);
				else
					textArea->setFontName(text);
				break;
			case 1:
				panel->setPosition(x, y);
				break;
			case 2:
				textArea->setCharHeight(x / 100.0f);
				break;
			case 3:
				textArea->setColour(colourValue);
				break;
			case 4:
				panel->setMaterialName(text);
				break;
			default:

				break;
		}
	}
};


#endif