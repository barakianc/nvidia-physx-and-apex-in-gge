//////////CreateCharacterRigidBodyObjectTask.h - //////////////////////////

#ifndef CREATE_CHARACTER_RIGID_BODY_OBJECT_TASK_H
#define CREATE_CHARACTER_RIGID_BODY_OBJECT_TASK_H

#include "QueuedTask.h"
#include "GameObject.h"
#include "GraphicsObject.h"

#ifdef HAVOK
#include "HavokObject.h"
#endif

/** The CreateCharacterRigidBodyObjectTask class is a wrapper for the allocation of a new CharacterRigidBodyObject.
 * CreateCharacterRigidBodyObjectTask class.
 * The CreateCharacterRigidBodyObjectTask class is a wrapper for the allocation of a new CharacterRigidBodyObject.  The class exists so as to queue the
 * creation of the PhyicsObject until a later time at which it can be guaranteed that the GraphicsObject which it references and that has also
 * been queued has been created.  Otherwise, the constructor of CharacterRigidBodyObject will reference elements of the GraphicsObject it is passed
 * that have not been initialized/properly allocated.
 */
class CreateCharacterRigidBodyObjectTask : public QueuedTask
{
private:

	/**
	* GameObject* private variable.
	* A pointer to the GameObject which holds the PhysicsObject to be created.
	*/
	GameObject* obj;

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that is a parameter for the constructor of CharacterRigidBodyObject.
	*/
	GraphicsObject* 	f_pGraphicsObject;
public:

	/**
	* CreateCharacterRigidBodyObjectTask constructor.
	* CreateCharacterRigidBodyObjectTask constructor that takes a GameObject and GraphicsObject pointer as parameters
	* @param GameObject*
	* @param GraphicsObject*
	*/
	CreateCharacterRigidBodyObjectTask(GameObject* incObj, GraphicsObject* incGraphObj) : obj(incObj), f_pGraphicsObject(incGraphObj) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that allocates a new CharacterRigidBodyObject and sets the appropriate
	* pointer in GameObject to the new PhysicsObject
	*/
	void UpdateTask() {
#ifdef HAVOK
		obj->m_pPhysicsObject = new CharacterRigidBodyObject(f_pGraphicsObject);
#endif
	}
};

#endif