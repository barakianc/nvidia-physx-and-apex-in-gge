///////////ChangeSceneNodeTask.h - //////////////////////

#ifndef CHANGE_SCENE_NODE_TASK_H
#define CHANGE_SCENE_NODE_TASK_H

#include "QueuedTask.h"
#include "GraphicsObject.h"


/** The ChangeSceneNodeTask class is used to change the scene node to which the Ogre::Entity that is pointed to by the passed in GraphicsObject* is attached.
 * ChangeSceneNodeTask class.
 * The ChangeSceneNodeTask class is used to change the scene node to which the Ogre::Entity that is pointed to by the passed in GraphicsObject*
 * is attached. When UpdateTask() is called, the entity is detached from the old scene node, the old scene node is destroyed, and the entity is
 * attached to the new scene node
 */
class ChangeSceneNodeTask : public QueuedTask
{
private:

	/**
	* GraphicsObject* private variable.
	* A pointer to the GraphicsObject that holds the Entity and SceneNode that will be manipulated
	*/
	GraphicsObject* obj;
	
	/**
	* Ogre::SceneNode* private variable.
	* A pointer to the Ogre::SceneNode to which the Entity will be attached
	*/
	Ogre::SceneNode* node;
public:

	/**
	* ChangeSceneNodeTask constructor.
	* ChangeSceneNodeTask constructor that takes pointers to the GraphicsObject it will manipulate and the SceneNode that the entity will be
	* attached to
	* @param GraphicsObject*
	* @param Ogre::SceneNode*
	*/
	ChangeSceneNodeTask(GraphicsObject* incObj, Ogre::SceneNode* incNode) : obj(incObj), node(incNode) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that changes the SceneNode to which the Entity held by the GraphicsObject
	* is attached.
	*/
	void UpdateTask();
};


#endif
