////////////////////SkeletonUpdateBoneTask.h - ////////////////////////////

#ifndef SKELETON_UPDATE_BONE_TASK_H
#define SKELETON_UPDATE_BONE_TASK_H

#include "QueuedTask.h"
#include "OgreBone.h"
#include "OgreVector3.h"
#include "OgreQuaternion.h"

/** The SkeletonUpdateBoneTask class updates either the position or orientation of an indivdual bone.
 * SkeletonUpdateBoneTask class.
 * The SkeletonUpdateBoneTask class updates either the position or orientation of an indivdual bone.  This is most often called in Animation
 * Object routines.  As with most classes inheriting QueuedTask, instances of this class are queued and processed by the rendering thread so 
 * as to prevent writing to Ogre scene data while the renderer is rendering a frame.
 */
class SkeletonUpdateBoneTask : public QueuedTask
{
private:

	/**
	* Ogre::Bone* private variable.
	* A pointer to the bone that will be manipulated.
	*/
	Ogre::Bone* bone;

	/**
	* Ogre::Vector3 private variable.
	* A vector that specifies the new position of the bone.
	*/
	Ogre::Vector3 vect;

	/**
	* Ogre::Quaternion private variable.
	* A Quaternion that specifies the new orientation of the bone.
	*/
	Ogre::Quaternion q;

	/**
	* boolean private variable.
	* A bool that specifies whether UpdateTaskw will update the position or orientation of the bone.  Set by type of constructor called.
	*/
	bool isVectUpdate;
public:

	/**
	* SkeletonUpdateBoneTask constructor.
	* SkeletonUpdateBoneTask constructor that takes an Ogre::Bone* and Ogre::Vector3 as parameters and sets isVectUpdate true.
	* @param Ogre::Bone*
	* @param Ogre::Vector3
	*/
	SkeletonUpdateBoneTask(Ogre::Bone* incBone, Ogre::Vector3 incVect) : bone(incBone) , vect(incVect), isVectUpdate(true) {}
	
	/**
	* SkeletonUpdateBoneTask constructor.
	* SkeletonUpdateBoneTask constructor that takes an Ogre::Bone* and Ogre::Quaternion as parameters and sets isVectUpdate false.
	* @param Ogre::Bone*
	* @param Ogre::Quaternion
	*/
	SkeletonUpdateBoneTask(Ogre::Bone* incBone, Ogre::Quaternion incQ) : bone(incBone) , q(incQ) , isVectUpdate(false) {}
	
	/**
	* A void function UpdateTask()
	* UpdateTask is an inherited function from the interface QueuedTask that updates the position or orientation of an Ogre::Bone
	*/
	void UpdateTask() {
		if (isVectUpdate) bone->setPosition(vect);
		else bone->setOrientation(q);
	}
};

#endif