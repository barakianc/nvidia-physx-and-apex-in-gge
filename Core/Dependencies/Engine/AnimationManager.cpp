	
#include "StdAfx.h"
#include "AnimationManager.h"

using namespace AnimationManager;

// initialize the singleton pointer to NULL
Manager* Manager::m_pInstance = NULL;

Manager::~Manager()
{
	// initialize the iterator over the map
	TStrEntIt it;

	// iterate over all entries in the map
	for (it = m_mAnimatedEntities.begin(); it != m_mAnimatedEntities.end(); it++)
	{
		// use RTTI to determine type of pointer
		if (dynamic_cast<AnimatedOgreEntity*>(it->second))
		{
			delete dynamic_cast<AnimatedOgreEntity*>(it->second);	// call AnimatedOgreEntity's destructor
		}
		else if (dynamic_cast<AnimatedHavokEntity*>(it->second))
		{
			delete dynamic_cast<AnimatedHavokEntity*>(it->second);	// call AnimatedHavokEntity's destructor
		}
		else
		{
			// TODO: this is problematic
		}

		it->second = NULL;
	}

	// erase all of the entries in the map
	m_mAnimatedEntities.clear();

	// delete the singleton
	if (m_pInstance)
	{
		delete Manager::m_pInstance;
		Manager::m_pInstance = NULL;
	}
}

Manager* Manager::GetSingleton()
{
	// initialize the singleton pointer if it hasn't already been initialized
	// this should only happen on the first call to GetSingleton()
	// and ensure that only one instance is created
	if (!m_pInstance)
	{
		m_pInstance = new Manager();
	}

	// return the singleton pointer
	return m_pInstance;
}

AnimatedHavokEntity* Manager::CreateHavokAnimationObject(GraphicsObject* f_pGraphicsObject, const char* path, const char* name)
{
	// instantiate the new AnimatedHavokEntity and insert it into the map
	m_mAnimatedEntities.insert(TStrEntPair(name, dynamic_cast<AnimatedEntity*>(new AnimatedHavokEntity(f_pGraphicsObject, path, name))));

	// return the AnimatedHavokEntity by retrieving it from the map
	return dynamic_cast<AnimatedHavokEntity*>(m_mAnimatedEntities[name]);
}

AnimatedOgreEntity* Manager::CreateOgreAnimationObject(GraphicsObject* f_pGraphicsObject, const char* path, const char* name)
{
	// instantiate the new AnimatedHavokEntity and insert it into the map
	m_mAnimatedEntities.insert(TStrEntPair(name, dynamic_cast<AnimatedEntity*>(new AnimatedOgreEntity(f_pGraphicsObject, path, name))));

	// return the AnimatedHavokEntity by retrieving it from the map
	return dynamic_cast<AnimatedOgreEntity*>(m_mAnimatedEntities[name]);
}

void Manager::DeleteAnimationObject(const char* name)
{
	// find the entity in the hashmap
	TStrEntIt it = m_mAnimatedEntities.find(name);
	if (it == m_mAnimatedEntities.end())
	{
		return;
	}

	// use RTTI to determine type of pointer
	if (dynamic_cast<AnimatedOgreEntity*>(it->second))
	{
		delete dynamic_cast<AnimatedOgreEntity*>(it->second);	// call AnimatedOgreEntity's destructor
	}
	else if (dynamic_cast<AnimatedHavokEntity*>(it->second))
	{
		delete dynamic_cast<AnimatedHavokEntity*>(it->second);	// call AnimatedHavokEntity's destructor
	}
	else
	{
		// TODO: this is problematic
	}

	it->second = NULL;

	// remove the entity from the hashmap
	m_mAnimatedEntities.erase(it);
}
