/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright  2000-2005 The OGRE Team
Also see acknowledgements in Readme.html

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

You may alternatively use this source under the terms of a specific version of
the OGRE Unrestricted License provided you have obtained such a license from
Torus Knot Software Ltd.
-----------------------------------------------------------------------------
*/
#include "StdAfx.h"
#include "OgreStableHeaders.h"

#include "OgreLineStreamOverlayElement.h"
#include "OgreMaterial.h"
#include "OgreTechnique.h"
#include "OgrePass.h"
#include "OgreStringConverter.h"
#include "OgreHardwareBufferManager.h"
#include "OgreRoot.h"
#include "OgreRenderSystem.h"

namespace Ogre {
    //---------------------------------------------------------------------
    String LineStreamOverlayElement::msTypeName = "LineStream";
    //---------------------------------------------------------------------
    LineStreamOverlayElement::LineStreamOverlayElement(const String& name)
		: OverlayContainer(name), 
		mNumberOfTraces(1),
		mNumberOfSamplesForTrace(100),
		mPosInStream(0),
		mDataChanged(false),
		scale(0.0)
    {
		// Setup render op in advance
		mRenderOp.vertexData = OGRE_NEW VertexData();
		 mRenderOp.vertexData->vertexCount = 0;
	}
    //---------------------------------------------------------------------
    LineStreamOverlayElement::~LineStreamOverlayElement()
    {
        OGRE_DELETE mRenderOp.vertexData;
    }
    //---------------------------------------------------------------------
    void LineStreamOverlayElement::initialise(void)
    {
		bool init = !mInitialised;

		OverlayContainer::initialise();
		if (init)
		{
			mInitialised = true;
		}
    }
    //---------------------------------------------------------------------
    const String& LineStreamOverlayElement::getTypeName(void) const
    {
        return msTypeName;
    }
    //---------------------------------------------------------------------
    void LineStreamOverlayElement::getRenderOperation(RenderOperation& op)
    {
		updateVtxBuffer();
        op = mRenderOp;
    }
    //---------------------------------------------------------------------
    void LineStreamOverlayElement::setMaterialName(const String& matName)
    {
        OverlayContainer::setMaterialName(matName);
    }
    //---------------------------------------------------------------------
    void LineStreamOverlayElement::_updateRenderQueue(RenderQueue* queue)
    {
        if (mVisible)
        {

            if (!mMaterial.isNull())
            {
                OverlayElement::_updateRenderQueue(queue);
            }

            // Also add children
            ChildIterator it = getChildIterator();
            while (it.hasMoreElements())
            {
                // Give children ZOrder 1 higher than this
                it.getNext()->_updateRenderQueue(queue);
            }
        }
    }
    //---------------------------------------------------------------------
    void LineStreamOverlayElement::updatePositionGeometry(void)
    {
		mDataChanged = true;
		updateVtxBuffer();
    }
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::updateTextureGeometry( void )
	{
		// no need
	}
	//---------------------------------------------------------------------
	uint32 LineStreamOverlayElement::getNumberOfTraces() const
	{
		return mNumberOfTraces;
	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::setNumberOfTraces( const uint32 val )
	{
		mNumberOfTraces = val;
	}
	//---------------------------------------------------------------------
	uint32 LineStreamOverlayElement::getNumberOfSamplesForTrace() const
	{
		return mNumberOfSamplesForTrace;
	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::setNumberOfSamplesForTrace( const uint32 val )
	{
		mNumberOfSamplesForTrace = val;
	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::setGraphScale( float incScale )
	{
		scale = incScale;
	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::createVertexBuffer()
	{

		uint32 numberOfVertexs = mNumberOfTraces * mNumberOfSamplesForTrace * 2;
		if (numberOfVertexs != mRenderOp.vertexData->vertexCount)
		{
			// Vertex declaration: 1 position, add texcoords later depending on #layers
			// Create as separate buffers so we can lock & discard separately
			VertexDeclaration* decl = mRenderOp.vertexData->vertexDeclaration;
			size_t offset = 0;
			offset += decl->addElement(0, 0, VET_FLOAT3, VES_POSITION).getSize();
			offset += decl->addElement(0, offset, VET_COLOUR_ARGB, VES_DIFFUSE).getSize();

			// Basic vertex data
			mRenderOp.vertexData->vertexStart = 0;
			// No indexes & issue as a strip
			mRenderOp.useIndexes = false;
			mRenderOp.operationType = RenderOperation::OT_LINE_LIST;	


			mTraceSamples.resize(numberOfVertexs / 2);
			mTraceColors.resize(mNumberOfTraces);
	
			mRenderOp.vertexData->vertexCount = numberOfVertexs;

			// Vertex buffer #1
			mCurrentVtxBuffer =
				HardwareBufferManager::getSingleton().createVertexBuffer(
				decl->getVertexSize(0), mRenderOp.vertexData->vertexCount,
				HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY// mostly static except during resizing
				);
			// Bind buffer
			mRenderOp.vertexData->vertexBufferBinding->setBinding(0, mCurrentVtxBuffer);

			ZeroMemory(mCurrentVtxBuffer->lock(HardwareBuffer::HBL_DISCARD), decl->getVertexSize(0) * mRenderOp.vertexData->vertexCount);
			ZeroMemory(&mTraceSamples[0], sizeof(Real) * mTraceSamples.size());
			mCurrentVtxBuffer->unlock();

			mPosInStream = 0;
		}
	}

	//---------------------------------------------------------------------
	void LineStreamOverlayElement::updateVtxBuffer()
	{

		if (!mDataChanged)
		{
			return;
		}
		mDataChanged = false;

		struct fTraceVertex
		{
			Vector3 pos;
			ARGB color;
		};





		// Use the furthest away depth value, since materials should have depth-check off
		// This initialised the depth buffer for any 3D objects in front
		Real zValue = Root::getSingleton().getRenderSystem()->getMaximumDepthInputValue();
		Real maxValue = 0.0f;
		if ( !scale )
		{
			//find max value
			for (uint32 trace = 0 ; trace < mNumberOfTraces ; trace++)
			{
				for (uint32 sample = 0 ; sample < mNumberOfSamplesForTrace ; sample++)
				{
					Real curSample = mTraceSamples[trace * mNumberOfSamplesForTrace + sample];
					if (maxValue < curSample)
					{
						maxValue = curSample;
					}

				}
			}

			if (maxValue == 0.0f)
			{
				return;
			}
		}
		else
			maxValue = scale;

		Real left, right, top, bottom;
		left = _getDerivedLeft() * 2 - 1;
		right = left + (mWidth * 2);
		top = -((_getDerivedTop() * 2) - 1);
		bottom =  top -  (mHeight * 2);

		fTraceVertex * currentVtxBufferData = 
			static_cast<fTraceVertex *>(mCurrentVtxBuffer->lock(HardwareBuffer::HBL_DISCARD));

		// write vertexes
		for (uint32 trace = 0 ; trace < mNumberOfTraces ; trace++)
		{
			for (uint32 sample = 1 ; sample < mNumberOfSamplesForTrace ; sample++)
			{
				fTraceVertex & lineStartVtx = currentVtxBufferData[(trace * mNumberOfSamplesForTrace + sample) * 2];
				lineStartVtx.pos.x = left + (mWidth * 2) * ( (Real)sample  / ((Real)mNumberOfSamplesForTrace + 1) );
				Real startSampleQunt = ( mTraceSamples[trace * mNumberOfSamplesForTrace + (sample + mPosInStream) % mNumberOfSamplesForTrace] / maxValue );
				lineStartVtx.pos.y = bottom + (mHeight * 2) * startSampleQunt;
				lineStartVtx.pos.z = zValue;


				fTraceVertex & lineEndVtx = currentVtxBufferData[(trace * mNumberOfSamplesForTrace + sample) * 2 + 1];
				lineEndVtx.pos.x = left + (mWidth * 2) * (Real)(sample + 1) / (Real)(mNumberOfSamplesForTrace + 1) ;
				Real EndSampleQunt = ( mTraceSamples[trace * mNumberOfSamplesForTrace + (sample + mPosInStream + 1) % mNumberOfSamplesForTrace] / maxValue );
				lineEndVtx.pos.y = bottom +(mHeight * 2) * EndSampleQunt;
				lineEndVtx.pos.z = zValue;

				lineStartVtx.color = mTraceColors[trace].getAsARGB();
				lineEndVtx.color = mTraceColors[trace].getAsARGB();

			}
			
		}
		mCurrentVtxBuffer->unlock();

		mPosInStream++;
	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::setTraceValue( const uint32 traceIndex, const Real traceValue )
	{
		mTraceSamples[traceIndex * mNumberOfSamplesForTrace + mPosInStream % mNumberOfSamplesForTrace] = traceValue;
		mDataChanged = true;

	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElement::setTraceColor( const uint32 traceIndex, const ColourValue & traceValue )
	{
		mTraceColors[traceIndex] = traceValue;
	}
	//---------------------------------------------------------------------
	//---------------------------------------------------------------------
	//---------------------------------------------------------------------
	//---------------------------------------------------------------------
	const String& LineStreamOverlayElementFactory::getTypeName( void ) const
	{
		static String name = "LineStream";
		return name;
	}
	//---------------------------------------------------------------------
	OverlayElement* LineStreamOverlayElementFactory::createOverlayElement( const String& instanceName )
	{
		return OGRE_NEW LineStreamOverlayElement(instanceName);
	}
	//---------------------------------------------------------------------
	void LineStreamOverlayElementFactory::destroyOverlayElement( OverlayElement* pElement )
	{
		OGRE_DELETE pElement;
	}
}



