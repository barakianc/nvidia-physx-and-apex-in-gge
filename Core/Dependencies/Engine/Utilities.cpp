#include "StdAfx.h"
// Utilities
#include "Utilities.h"
#ifdef TBB_PARALLEL_OPTION
#include "UtilitiesUpdateTask.h"
#include "TaskContainer.h"
#include "TaskTypes.h"
#endif

namespace GamePipe
{
	//////////////////////////////////////////////////////////////////////////
	// DebugText()
	//////////////////////////////////////////////////////////////////////////
	DebugText::DebugText(Ogre::String overlayText, Ogre::Real x, Ogre::Real y, const Ogre::Real charHeight,
		const Ogre::ColourValue color, const Ogre::TextAreaOverlayElement::Alignment halignment, const Ogre::String materialName, const Ogre::Real w, const Ogre::Real h)
	{
		SetType("DebugText");
		Create(overlayText, x, y, charHeight, color, halignment, materialName, w, h);
	}

	//////////////////////////////////////////////////////////////////////////
	// ~DebugText()
	//////////////////////////////////////////////////////////////////////////
	DebugText::~DebugText()
	{
		if (IsValid())
		{
			Destroy();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Create an overlay with a text display.
	//////////////////////////////////////////////////////////////////////////
	void DebugText::Create(Ogre::String overlayText, Ogre::Real x, Ogre::Real y, const Ogre::Real charHeight,
		const Ogre::ColourValue color, const Ogre::TextAreaOverlayElement::Alignment halignment, const Ogre::String materialName, const Ogre::Real w, const Ogre::Real h)
	{
		// Get a handle
		Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();

		// Prepare an overlay container panel
		Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "Panel" + GetName()));
		panel->setMetricsMode(Ogre::GMM_RELATIVE);
		SetPosition(x, y);
		panel->setDimensions(w, h);
		SetMaterialName(materialName);

		if (halignment == Ogre::TextAreaOverlayElement::Center)
		{
			panel->setLeft(Ogre::Real(w/-2.0f));
			panel->setHorizontalAlignment(Ogre::GHA_CENTER);
		}

		// Prepare a text area overlay element
		Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(overlayManager.createOverlayElement("TextArea", "TextArea" + GetName()));
		textArea->setMetricsMode(Ogre::GMM_RELATIVE);
		textArea->setPosition(0, 0);
		//textArea->setDimensions(100, 100);
		SetText(overlayText);
		SetCharHeight(charHeight);
		SetFontName("EngineFont");
		SetColor(color);
		textArea->setAlignment(halignment);
		textArea->setLeft(Ogre::Real(w/2.0f));

		// Create an overlay containing the panel and the text area
		overlay = overlayManager.create("Overlay" + GetName());
		overlay->add2D(panel);
		panel->addChild(textArea);

		// Show the overlay
		overlay->show();

		// Update the screen
		EnginePtr->GetRenderWindow()->update();

		// Set the Object valid
		IsValid(true);
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy an overlay created by CreateOverlayText().
	//////////////////////////////////////////////////////////////////////////
	void DebugText::Destroy()
	{
		// Get a handle
		Ogre::OverlayManager* overlayManager = Ogre::OverlayManager::getSingletonPtr();

		// Destroy the overlay elements
		overlayManager->destroyOverlayElement("TextArea" + GetName());
		overlayManager->destroyOverlayElement("Panel" + GetName());

		// Destroy the overlay itself
		overlayManager->destroy("Overlay" + GetName());

		// Object is not valid anymore
		IsValid(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// SetText(const Ogre::String text)
	//////////////////////////////////////////////////////////////////////////
	void DebugText::SetText(const Ogre::String text)
	{
		Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(Ogre::OverlayManager::getSingleton().getOverlayElement("TextArea" + GetName()));
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			UtilitiesUpdateTask* newTask = new UtilitiesUpdateTask(textArea,text,true);
			TaskContainer* container = new TaskContainer((void*)newTask,Utilities_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			textArea->setCaption(text);
		}
#else
		textArea->setCaption(text);
#endif	
	}

	//////////////////////////////////////////////////////////////////////////
	// SetFontName(const Ogre::String text)
	//////////////////////////////////////////////////////////////////////////
	void DebugText::SetFontName(const Ogre::String text)
	{
		Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(Ogre::OverlayManager::getSingleton().getOverlayElement("TextArea" + GetName()));
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			UtilitiesUpdateTask* newTask = new UtilitiesUpdateTask(textArea,text,false);
			TaskContainer* container = new TaskContainer((void*)newTask,Utilities_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			textArea->setFontName(text);
		}
#else
		textArea->setFontName(text);
#endif	
	}

	//////////////////////////////////////////////////////////////////////////
	// SetPosition(const Ogre::Real x, const Ogre::Real y)
	//////////////////////////////////////////////////////////////////////////
	void DebugText::SetPosition(const Ogre::Real x, const Ogre::Real y)
	{
		Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(Ogre::OverlayManager::getSingleton().getOverlayElement("Panel" + GetName()));
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			UtilitiesUpdateTask* newTask = new UtilitiesUpdateTask(panel,x,y);
			TaskContainer* container = new TaskContainer((void*)newTask,Utilities_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			panel->setPosition(x, y);
		}
#else
		panel->setPosition(x, y);
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// SetCharHeight(const Ogre::Real charHeight)
	//////////////////////////////////////////////////////////////////////////
	void DebugText::SetCharHeight(const Ogre::Real charHeight)
	{
		Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(Ogre::OverlayManager::getSingleton().getOverlayElement("TextArea" + GetName()));
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			UtilitiesUpdateTask* newTask = new UtilitiesUpdateTask(textArea,charHeight / 100.0f);
			TaskContainer* container = new TaskContainer((void*)newTask,Utilities_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			textArea->setCharHeight(charHeight / 100.0f);
		}
#else
		textArea->setCharHeight(charHeight / 100.0f);
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// SetColor(const Ogre::ColourValue colourValue)
	//////////////////////////////////////////////////////////////////////////
	void DebugText::SetColor(const Ogre::ColourValue colourValue)
	{
		Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(Ogre::OverlayManager::getSingleton().getOverlayElement("TextArea" + GetName()));
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			UtilitiesUpdateTask* newTask = new UtilitiesUpdateTask(textArea,colourValue);
			TaskContainer* container = new TaskContainer((void*)newTask,Utilities_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			textArea->setColour(colourValue);
		}
#else
		textArea->setColour(colourValue);
#endif	
	}

	//////////////////////////////////////////////////////////////////////////
	// SetMaterialName(const Ogre::String materialName)
	//////////////////////////////////////////////////////////////////////////
	void DebugText::SetMaterialName(const Ogre::String materialName)
	{
		if (materialName != "")
		{
			Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(Ogre::OverlayManager::getSingleton().getOverlayElement("Panel" + GetName()));
#ifdef TBB_PARALLEL_OPTION
			if(!EnginePtr->initializing)
			{
				UtilitiesUpdateTask* newTask = new UtilitiesUpdateTask(panel,materialName);
				TaskContainer* container = new TaskContainer((void*)newTask,Utilities_Update_Task);
				EnginePtr->taskQueue.push(container);
				EnginePtr->taskQueueSize++;
			}
			else
			{
				panel->setMaterialName(materialName);
			}
#else
			panel->setMaterialName(materialName);
#endif			
		}
	}
}