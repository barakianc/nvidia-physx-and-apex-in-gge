#include "StdAfx.h"
#ifdef DLLRELEASE
/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
This code is part of the open source project WaveIt-Web-Player found on GitHub at the following address: https://github.com/maylson/Waveit-Web-Player
The license agreement may be found in the GGE "ogreaddons/Waveit-Web-Player/" folder in the README file
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

#include "WaveitJavaInputListener.h"
#include "WaveitJavaLauncher.h"
#include "Engine.h"
#include "LoadingScreen\LoadingScreen.h"
#include "MainMenuScreen\MainMenuScreen.h"
#include <jni.h>

int keypress[10];

JNIEXPORT void JNICALL Java_Launcher_initialise
	(JNIEnv *env, jobject obj, jstring jStr)
{
	jboolean iscopy;
	const char *mfile = env->GetStringUTFChars(jStr, &iscopy);
	
	printf("Initialize in C++!\n");
	
	// Initialize the engine since I am not sure how to call the main function
	EnginePtr->Initialize(0, (char**)"", EnginePtr->GetGameFolder(), mfile);
	EnginePtr->SetLoadingScreen(new GamePipeGame::LoadingScreen("LoadingScreen"));
	EnginePtr->AddGameScreen(new GamePipeGame::MainMenuScreen("MainMenuScreen"));
	int returnValue = EnginePtr->Run();
	
}

JNIEXPORT jboolean JNICALL Java_Launcher_update
  (JNIEnv *env, jobject obj)
{

	//printf("I'm updating in C++!\n");
	return true;
}

JNIEXPORT void JNICALL Java_Launcher_shutdown
  (JNIEnv *env, jobject obj)
{
	
}

/* * * * Java_Launcher_setCanvas * * * * * *
 * Sets the window handle for the GGE game. The applet should know to grab the GGE
 * window and place it in the canvas as long as the externalWindowHandle in GGE is 
 * the same as what the applet sends (I believe)
 * * * * * * * * * * * * * * * * * * * * * *
 */
JNIEXPORT void JNICALL Java_Launcher_setCanvas
  (JNIEnv *env, jobject obj, jint hwnd, jint width, jint height)
{
	EnginePtr->SetWindowHandle(reinterpret_cast<HWND>(hwnd));
}


/* * * * Java_Launcher_setResourcePath * * * * * *
 * Sets the resource path so that GGE knows where to find the media files.
 * * * * * * * * * * * * * * * * * * * * * * * * *
 */
JNIEXPORT void JNICALL Java_Launcher_setResourcePath
  (JNIEnv *env, jobject obj, jstring jStr)
{
	jboolean iscopy;
	const char *mfile = env->GetStringUTFChars(jStr, &iscopy);
	EnginePtr->SetGameFolder(mfile);
}




/*
 * Class:     InputListener
 * Method:    sendKeyPressed
 * Signature: (I)V
 */
/* * * * Java_InputListener_sendKeyPressed * * * * * *
 * Creates a key event using the keycode sent by the applet and then
 * invokes the current game screen's OnKeyPress function.
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
JNIEXPORT void JNICALL Java_InputListener_sendKeyPressed
  (JNIEnv *env, jobject obj, jint key)
{
	
	if(InputPtr) {
		
		GIS::KeyEvent ke = GIS::KeyEvent(InputPtr->GetKeyboard(), InputPtr->DLLKeyCodeTranslation(key), 0);
		EnginePtr->GetForemostGameScreen(false)->OnKeyPress(ke);
	}
}

/*
 * Class:     InputListener
 * Method:    sendMultiplePressed
 * Signature: ([Ljava/lang/Integer;)V
 */

JNIEXPORT void JNICALL Java_InputListener_sendMultiplePressed
  (JNIEnv *env, jobject obj, jobjectArray ja)
{
	
}


/*
 * Class:     InputListener
 * Method:    sendKeyReleased
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendKeyReleased
  (JNIEnv *env, jobject obj, jint key)
{
	if(InputPtr) {
		GIS::KeyEvent ke = GIS::KeyEvent(InputPtr->GetKeyboard(), InputPtr->DLLKeyCodeTranslation(key), 0);
		EnginePtr->GetForemostGameScreen(false)->OnKeyRelease(ke);
	}
}

/*
 * Class:     InputListener
 * Method:    sendMouseMoved
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMouseMoved
  (JNIEnv *env, jobject obj, jint x, jint y)
{
	GIS::MouseState ms = GIS::MouseState();
	ms.X.rel = x;
	ms.Y.rel = y;
	ms.Z.rel = 0;
	GIS::MouseEvent me = GIS::MouseEvent(InputPtr->GetMouse(), ms);
	
	cout << x << endl;
	cout << y << endl;
	EnginePtr->GetForemostGameScreen()->DefaultOnMouseMove(me);
}

/*
 * Class:     InputListener
 * Method:    sendMouseScrolled
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMouseScrolled
  (JNIEnv *env, jobject obj, jint z)
{
	//mManager.javaMouseScrolled(z);
}

/*
 * Class:     InputListener
 * Method:    sendMousePressed
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMousePressed
  (JNIEnv *env, jobject obj, jint id)
{
	/*if (id == 1)
		mManager.javaMousePressed(Waveit::MouseButtonID::MB_Left);
	else
		if (id == 3)
			mManager.javaMousePressed(Waveit::MouseButtonID::MB_Right);
		else
			if (id == 2)
				mManager.javaMousePressed(Waveit::MouseButtonID::MB_Middle);
			else
				mManager.javaMousePressed((Waveit::MouseButtonID)id);*/
}

/*
 * Class:     InputListener
 * Method:    sendMouseReleased
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMouseReleased
  (JNIEnv *env, jobject obj, jint id)
{
	/*if (id == 1)
		mManager.javaMouseReleased(Waveit::MouseButtonID::MB_Left);
	else
		if (id == 3)
			mManager.javaMouseReleased(Waveit::MouseButtonID::MB_Right);
		else
			if (id == 2)
				mManager.javaMouseReleased(Waveit::MouseButtonID::MB_Middle);
			else
				mManager.javaMouseReleased((Waveit::MouseButtonID)id);*/
}

#endif