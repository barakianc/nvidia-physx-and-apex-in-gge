#ifdef DLLRELEASE
/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
This code is part of the open source project WaveIt-Web-Player found on GitHub at the following address: https://github.com/maylson/Waveit-Web-Player
The license agreement may be found in the GGE "ogreaddons/Waveit-Web-Player/" folder in the README file
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class InputListener */

#ifndef _Included_InputListener
#define _Included_InputListener
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     InputListener
 * Method:    sendKeyPressed
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendKeyPressed
  (JNIEnv *, jobject, jint);

/*
 * Class:     InputListener
 * Method:    sendMultiplePressed
 * Signature: ([Ljava/lang/Integer;)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMultiplePressed
  (JNIEnv *, jobject, jobjectArray);

/*
 * Class:     InputListener
 * Method:    sendKeyReleased
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendKeyReleased
  (JNIEnv *, jobject, jint);

/*
 * Class:     InputListener
 * Method:    sendMouseMoved
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMouseMoved
  (JNIEnv *, jobject, jint, jint);

/*
 * Class:     InputListener
 * Method:    sendMouseScrolled
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMouseScrolled
  (JNIEnv *, jobject, jint);

/*
 * Class:     InputListener
 * Method:    sendMousePressed
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMousePressed
  (JNIEnv *, jobject, jint);

/*
 * Class:     InputListener
 * Method:    sendMouseReleased
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_InputListener_sendMouseReleased
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
#endif