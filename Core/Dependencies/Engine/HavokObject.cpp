#include "StdAfx.h"

#ifdef HAVOK

#include "HavokObject.h"
#include "GameObject.h"
#include <Math.h>
#ifdef TBB_PARALLEL_OPTION
#include "TaskTypes.h"
#include "TaskContainer.h"
#include "GraphicsObjectUpdateTask.h"
#endif

using namespace std;

//RDS_PREVDEFINITION this up vector is required for character rigidbody
//RDS_PREVDEFINITION Attention: Y is HavokStaticUpVector now!
static const hkVector4 HavokStaticUpVector(0,1,0);

/*************************************************HavokObjectDefinitionsSTART*************************************************/
///////////////////////////////////////////////HavokObjectDefinitionsEND/////////////////////////////////////////////////////////////////////////////
string automaticMeshNameCreator(string baseString,string f_sMeshFileName) {
	string createdName="";

	if (f_sMeshFileName.compare("")!=0) {//if meshName is already given just return it
		//but also check if it has .mesh in it
		createdName = f_sMeshFileName;
		if (createdName.find(".mesh")!=string::npos)
			return createdName;
		return createdName.append(".mesh");
	}

	int dotFoundAt = baseString.find(".hkx");

	if (dotFoundAt!=string::npos)
	{
		//it means that we will create the name out of the havok file name
		createdName.append(baseString.c_str(),dotFoundAt);
	}
	else {
		//if we are here it means that we will create the name out of entity name
		//so we have to check if the first character is | or not. because Maya exporter can add a '|' character some times at the beginning
		int startChar = 0,endChar = baseString.length();
		if (baseString.c_str()[0]=='|') startChar++;
		createdName.append(baseString.c_str(),startChar,endChar);
	}

	return createdName.append(".mesh");
}
string automaticOgreUniqueNameCreator(string baseString,string f_sOgreUniqueName) {
	string createdName="";

	if (f_sOgreUniqueName.compare("")!=0) {//if f_sOgreUniqueName is already given just return it
		return f_sOgreUniqueName;
	}

	int dotFoundAt = baseString.find(".hkx");

	if (dotFoundAt!=string::npos)
	{
		//it means that we will create the name out of the havok file name
		createdName.append("sA_");
		createdName.append(baseString.c_str(),dotFoundAt);
	}
	else {
		//if we are here it means that we will create the name out of entity name
		//so we have to check if the first character is | or not. because Maya exporter can add a '|' character some times at the beginning
		int startChar = 0,endChar = baseString.length();
		if (baseString.c_str()[0]=='|') startChar++;
		createdName.append("sA_");
		createdName.append(baseString.c_str(),startChar,endChar);
	}

	//now that we found the base name to create
	//so lets find the unique name suitable for scene manager
	Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	int i=0;//for loop purposes
	int startChar=0,lengthChar=1;
	bool entityAlreadyCreated=f_pOgreSceneManager->hasEntity(createdName);
	Ogre::String f_sTempUniqueName ="";
	while (entityAlreadyCreated) {
		f_sTempUniqueName ="";
		f_sTempUniqueName += createdName;
		if (i!=0)
		{
			f_sTempUniqueName.append("_");
			std::stringstream numberBuffer;
			numberBuffer << i;
			f_sTempUniqueName.append(numberBuffer.str());
		}
		entityAlreadyCreated = f_pOgreSceneManager->hasEntity(f_sTempUniqueName);
		i+=1;
	}
	return createdName;
}
void havokPackFileLoader(hkPackfileData** f_pPackfileData,hkpPhysicsData** f_pPhysicsData,string f_sHKXFileName) {
	string f_sHavokFullLink = "";
	f_sHavokFullLink.append("../../");
	f_sHavokFullLink.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
	f_sHavokFullLink.append("/Media/Havok/");
	f_sHavokFullLink.append(f_sHKXFileName);

	*f_pPackfileData = static_cast<hkPackfileData*>(hkSerializeUtil::load(f_sHavokFullLink.c_str()));

	hkRootLevelContainer* f_pRootContainer = (*f_pPackfileData)->getContents<hkRootLevelContainer>();// Get the top level object in the file, which we know is a hkRootLevelContainer
	HK_ASSERT2(0xa6451543, (*f_pPackfileData) != HK_NULL, "Could not load root level object" );

	// Get the physics data
	*f_pPhysicsData = static_cast<hkpPhysicsData*>( f_pRootContainer->findObjectByType( hkpPhysicsDataClass.getName() ) );
}

string grabHavokEntityNames(string f_sHKXFileName) {
	///For loop purposes inside the function
	int i=0,j=0;
	///The return value to be set
	string		f_sHavokEntityNames = "";
	///Temporary loader of a hkpRigidBody
	hkpRigidBody*   f_pRigidBodyObject;
	///Physics data related to a hkx file
	hkpPhysicsData* f_pPhysicsData;
	///Packfile data related to a hkx file
	hkPackfileData* f_pPackfileData;

	/// Load the data to see what is inside the hkx file
	havokPackFileLoader(&f_pPackfileData,&f_pPhysicsData,f_sHKXFileName);

	/// Some variables needed for loop and setting of names/info
	int f_iPhysicsSystemsSize=0,f_iNumberOfRigidBodies=0;
	hkpRigidBodyCinfo	f_rigidBodyInfo;
	string			f_sRigidBodyName;

	f_iPhysicsSystemsSize = f_pPhysicsData->getPhysicsSystems().getSize();

	for(i=0; i<f_iPhysicsSystemsSize; i++)
	{
		if (i!=0) f_sHavokEntityNames.append(",");
		f_iNumberOfRigidBodies = (f_pPhysicsData->getPhysicsSystems()[i])->getRigidBodies().getSize();
		for (j=0;j<f_iNumberOfRigidBodies;j++) {
			if (j!=0) f_sHavokEntityNames.append(",");
			f_pRigidBodyObject = (f_pPhysicsData->getPhysicsSystems()[i])->getRigidBodies()[j];
			f_pRigidBodyObject->getCinfo(f_rigidBodyInfo);
			f_sRigidBodyName = f_pRigidBodyObject->getName();
			f_sHavokEntityNames.append(f_sRigidBodyName.c_str());
			f_pRigidBodyObject->removeReference();
		}
	}
	f_pPackfileData->removeReference();
	f_pPhysicsData->removeReference();
	return f_sHavokEntityNames;
}
void setRigidBodyMotionType(hkpRigidBody* f_pRigidBodyObject,GameObjectType f_eHavokObjectType) {
	//for debugging purposes
	hkpMotion::MotionType mt;
	mt = f_pRigidBodyObject->getMotionType();
	switch (f_eHavokObjectType)
	{
		case DEFAULT_GAME_OBJECT_TYPE:
		case PHYSICS_SYSTEM:
			//don't overwrite anything
			break;
		case PHYSICS_FIXED:
			f_pRigidBodyObject->setMotionType(hkpMotion::MOTION_FIXED);
			break;
		case PHYSICS_DYNAMIC:
			f_pRigidBodyObject->setMotionType(hkpMotion::MOTION_DYNAMIC);
			break;
		case PHYSICS_KEYFRAMED:
			f_pRigidBodyObject->setMotionType(hkpMotion::MOTION_KEYFRAMED);
			break;
	}
}
void setCollisionLayerInfo(hkpRigidBody* f_pRigidBodyObject,GameObjectType f_eHavokObjectType) {
	//COLLISION_LAYER_GROUND				= 1,//,//
	//	COLLISION_LAYER_PHANTOMS			= 6,//,//
	//	PHANTOM_SHAPE			= 31,//,//
	//	PHANTOM_AABB			= 32,//,//

	switch (f_eHavokObjectType) {
		case PHYSICS_FIXED:
			f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_STATIC_OBJECTS));
			break;
		case PHYSICS_DYNAMIC:
			f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_MOVING_OBJECTS));
			break;
		case PHYSICS_KEYFRAMED:
			f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_KEYFRAMED_OBJECTS));
			break;
		case PHYSICS_CHARACTER_RIGIDBODY:
		case ANIMATED_CHARACTER_RIGIDBODY:
			f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_CHARACTER_NPC));
			break;
		case PHYSICS_CHARACTER_PROXY:
		case ANIMATED_CHARACTER_PROXY:
			f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_CHARACTER_PLAYER));
			break;
		case VEHICLE_CAR:
		case VEHICLE_MOTORCYCLE:
			f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_VEHICLE_OBJECT));
			break;
	}
}

/***********************************************PhantomHelperClassDeclarationsSTART***********************************************/
class MyPhantomCallbackShape: public hkpPhantomCallbackShape
{
// Phantomshape callback class; write your custom functions in here for objects interacting with phantom
private :
	hkVector4 m_vPhantomEnterLinearVelocityEffect;
	hkVector4 m_vPhantomLeaveLinearVelocityEffect;
	PhantomShapeObject* m_pOwnerPhantom;
	Custom_PhantomShape_Collision_Callback_Function m_fCustom_phantomEnterEvent_Function;
	Custom_PhantomShape_Collision_Callback_Function m_fCustom_phantomLeaveEvent_Function;
public:

	MyPhantomCallbackShape()
	{
		m_pOwnerPhantom = NULL;
		m_fCustom_phantomEnterEvent_Function = NULL;
		m_fCustom_phantomLeaveEvent_Function = NULL;
		m_vPhantomEnterLinearVelocityEffect = hkVector4(0,0,0,0);//0,20,0,0//50,20,0,0
		m_vPhantomLeaveLinearVelocityEffect = hkVector4(0,0,0,0);//0,20,0,0//50,20,0,0
	}

	MyPhantomCallbackShape(PhantomShapeObject* f_pOwnerPhantom)
	{
		m_pOwnerPhantom = f_pOwnerPhantom;
		m_fCustom_phantomEnterEvent_Function = NULL;
		m_fCustom_phantomLeaveEvent_Function = NULL;
		m_vPhantomEnterLinearVelocityEffect = hkVector4(0,0,0,0);//0,20,0,0//50,20,0,0
		m_vPhantomLeaveLinearVelocityEffect = hkVector4(0,0,0,0);//0,20,0,0//50,20,0,0
	}

	void setCustom_phantomEnterEvent_Function(void* f_fNew_Custom_phantomEnterEvent_Function)
	{
		m_fCustom_phantomEnterEvent_Function = (Custom_PhantomShape_Collision_Callback_Function)f_fNew_Custom_phantomEnterEvent_Function;
	}

	void setCustom_phantomLeaveEvent_Function(void* f_fNew_Custom_phantomLeaveEvent_Function)
	{
		m_fCustom_phantomLeaveEvent_Function = (Custom_PhantomShape_Collision_Callback_Function)f_fNew_Custom_phantomLeaveEvent_Function;
	}

	// hkpPhantom interface implementation
	virtual void phantomEnterEvent( const hkpCollidable* phantomCollidable, const hkpCollidable* otherCollidable, const hkpCollisionInput& env )
	{
		// the color can only be changed once the entity has been added to the world
		hkpRigidBody* f_pOtherCollidable_Owner = (hkpRigidBody*)otherCollidable->getOwner();
		hkpRigidBody* f_pPhantomCollidable_Owner = (hkpRigidBody*)phantomCollidable->getOwner();

		GameObject* f_pCollidedGameObject = NULL;//will be in function call;

		if (f_pOtherCollidable_Owner->getContactListeners().getSize()>0)
        {
            //if we are here it means the rigidbody has a collision listener
            //so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
            //this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
            CollisionListener* f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherCollidable_Owner->getContactListeners()[0]);

            //Now get the pointer and set it to the passed parameter so that we are sending back the pointer of the collided gameObject
            f_pCollidedGameObject = f_pOtherGameObjectListener->m_pGameObject;
        }

		if ((m_pOwnerPhantom!=NULL)&&(m_fCustom_phantomEnterEvent_Function!=NULL))
		{
			//do the physical interaction in custom function
			m_fCustom_phantomEnterEvent_Function(m_pOwnerPhantom,f_pOtherCollidable_Owner,f_pCollidedGameObject);
		}
		else
		{
			switch (f_pOtherCollidable_Owner->getMotionType())
			{
				case hkpMotion::MOTION_FIXED:
				case hkpMotion::MOTION_KEYFRAMED:
					break;
				default:
					f_pOtherCollidable_Owner->setLinearVelocity(m_vPhantomEnterLinearVelocityEffect);
					break;
			}
			if (f_pPhantomCollidable_Owner->getMotionType()!=hkpMotion::MOTION_FIXED)
			{
				f_pPhantomCollidable_Owner->setLinearVelocity(m_vPhantomEnterLinearVelocityEffect);
			}
		}

		//some code for visual debugger
		switch (f_pOtherCollidable_Owner->getMotionType())
		{
			case hkpMotion::MOTION_FIXED:
				HK_SET_OBJECT_COLOR((hkUlong)f_pOtherCollidable_Owner->getCollidable(), hkColor::rgbFromChars(255,0, 0));
				break;
			case hkpMotion::MOTION_KEYFRAMED:
				HK_SET_OBJECT_COLOR((hkUlong)f_pOtherCollidable_Owner->getCollidable(), hkColor::rgbFromChars(0,255, 0));
				break;
			default:
				HK_SET_OBJECT_COLOR((hkUlong)f_pOtherCollidable_Owner->getCollidable(), hkColor::rgbFromChars(0,0, 255));
				break;
		}
		if (f_pPhantomCollidable_Owner->getMotionType()!=hkpMotion::MOTION_FIXED)
		{
			HK_SET_OBJECT_COLOR((hkUlong)f_pPhantomCollidable_Owner->getCollidable(), hkColor::rgbFromChars(0,255, 0));
		}
		else
		{
			HK_SET_OBJECT_COLOR((hkUlong)f_pPhantomCollidable_Owner->getCollidable(), hkColor::rgbFromChars(255,0, 255));
		}
	}

	// hkpPhantom interface implementation
	virtual void phantomLeaveEvent( const hkpCollidable* phantomCollidable, const hkpCollidable* otherCollidable )
	{
		// the color can only be changed once the entity has been added to the world
		hkpRigidBody* f_pOtherCollidable_Owner = (hkpRigidBody*)otherCollidable->getOwner();
		hkpRigidBody* phantomCollidable_Owner = (hkpRigidBody*)phantomCollidable->getOwner();

		GameObject* f_pCollidedGameObject = NULL;//will be in function call;

		if (f_pOtherCollidable_Owner->getContactListeners().getSize()>0)
        {
            //if we are here it means the rigidbody has a collision listener
            //so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
            //this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
            CollisionListener* f_pOtherGameObjectListener = (CollisionListener*)(f_pOtherCollidable_Owner->getContactListeners()[0]);

            //Now get the pointer and set it to the passed parameter so that we are sending back the pointer of the collided gameObject
            f_pCollidedGameObject = f_pOtherGameObjectListener->m_pGameObject;
        }

		if ((m_pOwnerPhantom!=NULL)&&(m_fCustom_phantomLeaveEvent_Function!=NULL))
		{
			//do the physical interaction in custom function
			m_fCustom_phantomLeaveEvent_Function(m_pOwnerPhantom,f_pOtherCollidable_Owner,f_pCollidedGameObject);
		}
		else
		{
			switch (f_pOtherCollidable_Owner->getMotionType())
			{
			case hkpMotion::MOTION_FIXED:
			case hkpMotion::MOTION_KEYFRAMED:
				break;
			default:
				f_pOtherCollidable_Owner->setLinearVelocity(m_vPhantomLeaveLinearVelocityEffect);
				break;
			}
			if (phantomCollidable_Owner->getMotionType()!=hkpMotion::MOTION_FIXED)
				phantomCollidable_Owner->setLinearVelocity(m_vPhantomLeaveLinearVelocityEffect);
		}

		switch (f_pOtherCollidable_Owner->getMotionType())
		{
			case hkpMotion::MOTION_FIXED:
				HK_SET_OBJECT_COLOR((hkUlong)f_pOtherCollidable_Owner->getCollidable(), hkColor::rgbFromChars(0,0, 255));
				break;
			case hkpMotion::MOTION_KEYFRAMED:
				HK_SET_OBJECT_COLOR((hkUlong)f_pOtherCollidable_Owner->getCollidable(), hkColor::rgbFromChars(0,255, 0));
				break;
			default:
				HK_SET_OBJECT_COLOR((hkUlong)f_pOtherCollidable_Owner->getCollidable(), hkColor::rgbFromChars(255,0, 0));
				break;
		}

		if (phantomCollidable_Owner->getMotionType()!=hkpMotion::MOTION_FIXED)
			HK_SET_OBJECT_COLOR((hkUlong)phantomCollidable_Owner->getCollidable(), hkColor::rgbFromChars(255,0, 255));
		else
			HK_SET_OBJECT_COLOR((hkUlong)phantomCollidable_Owner->getCollidable(), hkColor::rgbFromChars(0,255, 0));
	}
};

class MyHkpCDPointCollector : public hkpCdPointCollector
{
public :
	std::list<hkpRigidBody*> m_pCollidedRigidBodyList;
	MyHkpCDPointCollector() {}// : hkpCdPointCollector() {}

	virtual void addCdPoint(const hkpCdPoint& point)
	{
		hkpRigidBody * ownerRB = (hkpRigidBody * )point.m_cdBodyA.getRootCollidable()->getOwner();
		if (ownerRB!=NULL) m_pCollidedRigidBodyList.push_back(ownerRB);
	}
	void reset()
	{
		m_pCollidedRigidBodyList.clear();
	}
};

class hkMovingRBCollectorPhantom : public hkpAabbPhantom
{
public:
	hkMovingRBCollectorPhantom( const hkAabb& aabb, hkUint32 collisionFilterInfo = 0 );
	virtual void addOverlappingCollidable( hkpCollidable* handle );// Callback implementation
	virtual void removeOverlappingCollidable( hkpCollidable* handle );// Callback implementation
};
hkMovingRBCollectorPhantom::hkMovingRBCollectorPhantom( const hkAabb& aabb, hkUint32 collisionFilterInfo  )
: hkpAabbPhantom(aabb, collisionFilterInfo){
	// This class centers around the implementations of the two virtual methods in the base class, namely
	// addOverlappingCollidable(...) and removeOverlappingCollidable(...). The code for these methods is given below.
	// Note: This (very simple) demo works fine without implementing either of these functions. Deriving an entirely
	// new hkpAabbPhantom class is not strictly necessary either...
}
void hkMovingRBCollectorPhantom::addOverlappingCollidable( hkpCollidable* c )
{
	hkpRigidBody* rb = (hkpRigidBody*)c->getOwner();

	// Ignore other phantoms and fixed rigid bodies.
	if ( (rb != HK_NULL) && !rb->isFixed() )
	{
		hkpAabbPhantom::addOverlappingCollidable( c );
	}
}
void hkMovingRBCollectorPhantom::removeOverlappingCollidable( hkpCollidable* c )
{
	hkpRigidBody* rb = (hkpRigidBody*)c->getOwner();

	// hkpAabbPhantom::removeOverlappingCollidable() protects its m_overlappingCollidables member,
	// regardless of whether collidables are present in it or not. However this check needs to be
	// symmetric with the one above, if we want symmetry in callbacks fired within
	// hkpAabbPhantom::removeOverlappingCollidable() and hkpAabbPhantom::addOverlappingCollidable().
	if ( (rb != HK_NULL) && !rb->isFixed() )
	{
		hkpAabbPhantom::removeOverlappingCollidable( c );
	}
}
/*********************************************PhantomHelperClassDeclarationsEND*********************************************/
/***********************************************PhysicsObjectDefinitionsSTART***********************************************/
void PhysicsPrimitiveObject::initializePhysicsPrimitive() {
	m_pPhysicsWorld		= NULL;
	m_pRigidBodyObject	= NULL;
	m_pPhysicsSystem	= NULL;
	m_pGraphicsObject	= NULL;
	m_vCharacterSceneNodeOffset.set(0.0f,0.0f,0.0f);
	m_pCustomKeyframedFunction = NULL;
	m_bKeyframeActive=false;
	m_eCollisionShapeType = DEFAULT_COLLISION_SHAPE;
}
//CONSTRUCTOR_PHYSICSPRIMITIVE
//Constructor for creation of a rigid body by parsing a hkx file which is created from havok content tools
PhysicsPrimitiveObject::PhysicsPrimitiveObject(
													string				f_sOgreUniqueName,
													string				f_sMeshFileName,
													string				f_sHKXFileName,
													GameObjectType			f_eHavokObjectType,
													CollisionShapeType		f_eCollisionShapeType,
													GraphicsObject**		f_pGraphicsObject
):PhysicsObject()
{
	initializePhysicsPrimitive();
	m_pPhysicsWorld		= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();

	bool f_bPhysicsObjectCreated=false;

	int strSearchValue = f_sHKXFileName.find(".hkx");
	if (strSearchValue == string::npos)
		strSearchValue = f_sHKXFileName.find(".HKX");

	if (strSearchValue != string::npos)
	{
		//if I am in this else statement this means I am given a .hkx file to read in and this is not an animated entity
		//so I have to check if the object type is mentioned
		//if it is mentioned this means I will have to overWrite the rigidBody info according to the type which is passed to me

		//these variables are just to make it easier to debug to code.Also I think it inreases readibility of the code
		//they can be changed in the code and deleted-->
		//[f_iPhysicsSystemsSize<->f_pPhysicsData->getPhysicsSystems().getSize()]
		//[f_iNumberOfRigidBodies<->f_pCurrentPhysicsSystem->getRigidBodies().getSize()]
		int f_iPhysicsSystemsSize=0,f_iNumberOfRigidBodies=0;

		hkpPhysicsSystem*	f_pCurrentPhysicsSystem=NULL;	//A pointer to physicsSytstem which is grabbed out of havokFile
		hkPackfileData*		f_pPackfileData;				//Packfile data related to a hkx file
		hkpPhysicsData*		f_pPhysicsData;					//Physics data related to a hkx file

		havokPackFileLoader(&f_pPackfileData,&f_pPhysicsData,f_sHKXFileName);// Load the data

		f_iPhysicsSystemsSize = f_pPhysicsData->getPhysicsSystems().getSize();
		if (f_iPhysicsSystemsSize==1)
		{//ONLY 1 PHYSICS SYSTEM-->FOR NOW THIS IS THE ONLY STUFF WE CAN GET OUT OF MAYA
			f_pCurrentPhysicsSystem = f_pPhysicsData->getPhysicsSystems()[0];
			f_iNumberOfRigidBodies = f_pCurrentPhysicsSystem->getRigidBodies().getSize();

			if (f_iNumberOfRigidBodies==1)
			{
				//if physicsSystem is wanted but 1 rigidBody is inside the hkx then derive the gameObjectType from rigidBody info
				if (f_eHavokObjectType==PHYSICS_SYSTEM)
					f_eHavokObjectType = getGameObjectType( f_pPhysicsData->getPhysicsSystems()[0]->getRigidBodies()[0]->getMotionType());
				f_bPhysicsObjectCreated = SingleRigidBodyConstructor(f_sOgreUniqueName,f_sMeshFileName,f_pCurrentPhysicsSystem,f_eHavokObjectType);
			}
			else
			{
				f_bPhysicsObjectCreated = MultipleRigidBody_MultipleEntity(f_sOgreUniqueName,f_sMeshFileName,f_pCurrentPhysicsSystem,f_eHavokObjectType);
			}
		}

		else
		{//MORE THAN 1 PHYSICS SYSTEM-->WE WONT HANDLE IT FOR NOW
			GGETRACELOG("Problem in CONSTRUCTOR_PHYSICSPRIMITIVE.\
						The hkx file has more than 1 physicsSystem in it.\
						We do not handle such hkx files.\
						.hkx file passed = %s",f_sHKXFileName.c_str());

			exit(1);
		}
	}
	else {
		//if I am in this if statement
		//this means havok fileName is not given --> I understand this by looking if the string passed to me has .hkx file
		//so look at the object type. if it is not passed then make it a box
		if (f_eCollisionShapeType==DEFAULT_COLLISION_SHAPE)
			f_eCollisionShapeType=COLLISION_SHAPE_BOX;
		f_bPhysicsObjectCreated = NoHkx_ShapeRigidBody(f_sOgreUniqueName,f_sMeshFileName,f_eCollisionShapeType,f_eHavokObjectType);
	}

	if ((m_pRigidBodyObject==NULL)&&(m_pPhysicsSystem==NULL)) {
		GGETRACELOG("Problem in CONSTRUCTOR_PHYSICSPRIMITIVE.\
					No rigidBody has been created.\
					.hkx file passed = %s",f_sHKXFileName.c_str());
		exit(1);
	}

	//send the data back
	*f_pGraphicsObject = m_pGraphicsObject;
	return;
}

//CONSTRUCTOR_PHYSICSPRIMITIVE_1
bool PhysicsPrimitiveObject::SingleRigidBodyConstructor(
															string				f_sOgreUniqueName,
															string				f_sMeshFileName,
															hkpPhysicsSystem*		f_pCurrentPhysicsSystem,
															GameObjectType			f_eHavokObjectType
														)
{
	//first create the graphics object with the f_sOgreUniqueName and f_sMeshFileName
	m_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eHavokObjectType));

	//now get the GraphicsObject's entity for setting the initial position of the rigidBody
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::Vector3 f_vGraphicsObjectPosition = m_pGraphicsObject->getPosition();
	hkVector4 f_vCreatedEntityInitialPosition(f_vGraphicsObjectPosition.x,f_vGraphicsObjectPosition.y,f_vGraphicsObjectPosition.z);

	//as the physics object is created by hkx and mesh file no offset should be applied
	m_vCharacterSceneNodeOffset.set(0.0f,0.0f,0.0f);

	//this info will be used for debugging purposes
	hkpRigidBodyCinfo f_rigidBodyInfo;

	m_pRigidBodyObject = f_pCurrentPhysicsSystem->getRigidBodies()[0];
	m_pRigidBodyObject->setPosition(f_vCreatedEntityInitialPosition);

	//these 2 lines are just for debugging purposes
	m_pRigidBodyObject->getCinfo(f_rigidBodyInfo);
	string f_sRigidBodyName = m_pRigidBodyObject->getName();

	setRigidBodyMotionType(m_pRigidBodyObject,f_eHavokObjectType);
	m_eGameObjectType = getGameObjectType(m_pRigidBodyObject->getMotionType());
	setCollisionLayerInfo(m_pRigidBodyObject,f_eHavokObjectType);

	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);
	m_pRigidBodyObject->removeReference();

	return true;
}

//CONSTRUCTOR_PHYSICSPRIMITIVE_2
bool PhysicsPrimitiveObject::MultipleRigidBody_MultipleEntity(
																	string				f_sOgreUniqueName,
																	string				f_sMeshFileName,
																	hkpPhysicsSystem*		f_pCurrentPhysicsSystem,
																	GameObjectType			f_eHavokObjectType
															 )
{
	//f_sOgreUniqueName is passed but it is used for only setting the GameObjectsSceneNode's name
	//normally it is used for setting the entityName but here we have multiple rigidbodies so that
	//in stead of using the name with counters [f_sOgreUniqueName_1,f_sOgreUniqueName_2]
	//I used the OgreUniqueNames to be for each rigidBody[sA_rigidBodyName]

	//we set the m_pPhysicsSystem so that in update loop we will understand that this is a physicsSystem by checking if this is NULL or not
	m_pPhysicsSystem = f_pCurrentPhysicsSystem;
	//also we need to set m_pRigidBody to NULL so that for a PhysicsSystem we will not deal with m_pRigidBody
	m_pRigidBodyObject = NULL;
	//we will need this parameter to orient the sceneNode position and the rigidBody/physicsSystem position
	hkReal f_vSubRigidBodiesOriginOffset[3];

	//we will get the number of rigidBodies inside the system
	int f_iNumberOfRigidBodies = f_pCurrentPhysicsSystem->getRigidBodies().getSize();

	Ogre::SceneManager* f_pOgreSceneManager = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager();
	Ogre::SceneNode* f_pTempOgreSceneNode = f_pOgreSceneManager->getRootSceneNode()->createChildSceneNode(f_sOgreUniqueName+"ComplexSceneNode");
	m_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,PHYSICS_SYSTEM,f_pTempOgreSceneNode));

	Ogre::Entity* f_pTempOgreEntity = f_pOgreSceneManager->createEntity("entityForGettingNumOfEntities",f_sMeshFileName);
	int f_iNumberOfSubEntities = f_pTempOgreEntity->getNumSubEntities();
	f_pOgreSceneManager->destroyEntity(f_pTempOgreEntity);

	hkpRigidBody* f_pRigidBodyObject;
	GameObjectType f_TempGameObjectType = DEFAULT_GAME_OBJECT_TYPE;
	if (f_iNumberOfSubEntities!=f_iNumberOfRigidBodies)
	{
		//we can not map the rigidBodies to subEntities
		//but we will check if a fixed system is wanted
		//if so we will set the first rigidBody as the m_pRigidBodyObject not to have problems in the update function
		if (f_eHavokObjectType==PHYSICS_FIXED)
		{
			for (int i=0;i<f_iNumberOfRigidBodies;i++)
			{
				//just make all the rigidBodies fixed by overwriting it
				f_pRigidBodyObject = f_pCurrentPhysicsSystem->getRigidBodies()[i];
				f_pRigidBodyObject->setMotionType(hkpMotion::MOTION_FIXED);
				//we will get whatever the rigidBody's motionType is (it can be overwritten!!)
				f_TempGameObjectType = getGameObjectType(f_pRigidBodyObject->getMotionType());
				//and set the collisionLayerInfo according to f_TempGameObjectType
				setCollisionLayerInfo(f_pRigidBodyObject,f_TempGameObjectType);
			}
			//now destroy the m_pGraphicsObject created
			//m_pGraphicsObject->m_pOgreSceneNode->detachObject(m_pGraphicsObject->m_pOgreEntity);
			//m_pGraphicsObject->m_pOgreSceneManager->destroyEntity(m_pGraphicsObject->m_pOgreEntity);
			m_pGraphicsObject->m_pOgreSceneNode->removeAndDestroyAllChildren();
			m_pGraphicsObject->m_pOgreSceneManager->destroySceneNode(m_pGraphicsObject->m_pOgreSceneNode);
			m_pGraphicsObject = NULL;
			//and now create a new GraphicsObject as if we have 1 rigidBody 1Mesh combination
			m_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,PHYSICS_FIXED));
			m_pRigidBodyObject = f_pCurrentPhysicsSystem->getRigidBodies()[0];
			m_pPhysicsSystem = NULL;
			m_eGameObjectType = PHYSICS_FIXED;
			//now check the root rigid body position
			//when creating the system in Maya, if it is not oriented to 0, we will orient the sceneNode of the GraphicsObject here
			m_pRigidBodyObject->getPosition().store3(f_vSubRigidBodiesOriginOffset);
			m_pGraphicsObject->setPosition(f_vSubRigidBodiesOriginOffset[0],f_vSubRigidBodiesOriginOffset[1],f_vSubRigidBodiesOriginOffset[2]);
			//don't add the rigidBodies 1 by 1. Add the whole system into PhysicsWorld
			m_pPhysicsWorld->addPhysicsSystem(f_pCurrentPhysicsSystem);
			return true;
		}
		else
		{
			GGETRACELOG("Problem in MultipleRigidBody_MultipleEntity function.\
						Number of subEntities in mesh file and number of rigidBodies in hkx file do not match.\
						Constructor can not map the hkx file and mesh file together.");
			exit(1);
			return false;
		}
	}

	hkpRigidBodyCinfo f_rigidBodyInfo;
	Ogre::Entity* f_pOgreEntity;

	//store base orientation of each rigidbody
	m_vBaseOrientation = new Ogre::Quaternion[f_iNumberOfRigidBodies];

	//now check the root rigid body position
	//when creating the system in Maya, if it is not oriented to 0, we will orient the sceneNode of the GraphicsObject here
	//f_pCurrentPhysicsSystem->getRigidBodies()[0]->getPosition().store3(f_vSubRigidBodiesOriginOffset);
	//m_pGraphicsObject->setPosition(f_vSubRigidBodiesOriginOffset[0],f_vSubRigidBodiesOriginOffset[1],f_vSubRigidBodiesOriginOffset[2]);

	for (int i=0;i<f_iNumberOfRigidBodies;i++)
	{
		f_pRigidBodyObject = f_pCurrentPhysicsSystem->getRigidBodies()[i];

		//these 3 lines are just for creating ogre unique names for each rigid body
		//because user can only give 1 unique name for 1 gameObject
		//but I need to create 1 entity for each rigid body in the system and find a way to create unique names for them
		f_pRigidBodyObject->getCinfo(f_rigidBodyInfo);
		string f_sRigidBodyName = f_pRigidBodyObject->getName();
		//f_sOgreUniqueName	= automaticOgreUniqueNameCreator(f_sRigidBodyName,"");//sA_f_sRigidBodyName

		string f_sTempOgreUniqueName = "";
		f_sTempOgreUniqueName += f_sOgreUniqueName;
		f_sTempOgreUniqueName.append(Ogre::StringConverter::toString(i).c_str());

		//the sub meshes are not in the center of the real mesh
		//so I need to have 1 orient scene node which I will push the mesh to orient it according to 1 rigid body position
		//then push the real scene node back in negative direction to see the rigid body in the correct position

		//in these two lines, we will create the sceneNodes according to rigidBodyNames
		//so that in the update function we will be able to grab the scene node of the rigidBody according to its name
		f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode->createChildSceneNode(f_sTempOgreUniqueName+"_SceneNode");//sA_f_sRigidBodyName_SceneNode
		Ogre::SceneNode* f_pOrientSceneNode = f_pTempOgreSceneNode->createChildSceneNode(f_sTempOgreUniqueName+"_OrientSceneNode");//sA_f_sRigidBodyName_OrientSceneNode

		//and here is the initialization of the scene node positions
		f_pRigidBodyObject->getPosition().store3(f_vSubRigidBodiesOriginOffset);
		f_pTempOgreSceneNode->setPosition(f_vSubRigidBodiesOriginOffset[0],f_vSubRigidBodiesOriginOffset[1],f_vSubRigidBodiesOriginOffset[2]);
		f_pOrientSceneNode->setPosition(-f_vSubRigidBodiesOriginOffset[0],-f_vSubRigidBodiesOriginOffset[1],-f_vSubRigidBodiesOriginOffset[2]);

		hkReal f_fTmpRotation[4];
		f_pRigidBodyObject->getRotation().m_vec.store4(f_fTmpRotation);
		//m_vBaseOrientation = Ogre::Quaternion(f_fStoredRotation[3], f_fStoredRotation[0], f_fStoredRotation[1], f_fStoredRotation[2] );
		m_vBaseOrientation[i].w = f_fTmpRotation[3];
		m_vBaseOrientation[i].x = f_fTmpRotation[0];
		m_vBaseOrientation[i].y = f_fTmpRotation[1];
		m_vBaseOrientation[i].z = f_fTmpRotation[2];

		m_vBaseOrientation[i] = m_vBaseOrientation[i].Inverse();

		//now create a temporary entity to attach to the GameObject's sceneNode
		f_pOgreEntity = m_pGraphicsObject->createOgreEntity(f_sOgreUniqueName,f_sMeshFileName);

		//this line is just for debugging purposes to see which subMesh am I gonna use for the currentRigidBody
		string f_sSubentityName = f_pOgreEntity->getSubEntity(i)->getMaterialName();

		//now make all other sub entities invisible in order not to see them in OgreScene
		for (int j=0;j<f_iNumberOfRigidBodies;j++)
		{
			if (i!=j) f_pOgreEntity->getSubEntity(j)->setVisible(false);
		}

		f_pOrientSceneNode->attachObject(f_pOgreEntity);
		f_pRigidBodyObject->setMass(200);

		//now if f_eHavokObjectType is not default or physics system, this function will overwrite the
		//rigidBody motionType
		setRigidBodyMotionType(f_pRigidBodyObject,f_eHavokObjectType);
		//we will get whatever the rigidBody's motionType is (it can be overwritten!!)
		f_TempGameObjectType = getGameObjectType(f_pRigidBodyObject->getMotionType());
		//and set the collisionLayerInfo according to f_TempGameObjectType
		setCollisionLayerInfo(f_pRigidBodyObject,f_TempGameObjectType);
		//f_pRigidBodyObject->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(18));

		//don't add the rigidBodies 1 by 1. Add the whole system into PhysicsWorld to be able to have constraints and all other stuff
		//m_pPhysicsWorld->addEntity(f_pRigidBodyObject);
		f_pRigidBodyObject->removeReference();
	}
	//hkpGroupFilter* filter = new hkpGroupFilter();
	//filter->disableCollisionsBetween(18,18);
	//m_pPhysicsWorld->setCollisionFilter(filter);
	//filter->removeReference();

	m_pPhysicsWorld->addPhysicsSystem(f_pCurrentPhysicsSystem);
	m_eGameObjectType = PHYSICS_SYSTEM;
	return true;
}

//CONSTRUCTOR_PHYSICSPRIMITIVE_3
bool PhysicsPrimitiveObject::NoHkx_ShapeRigidBody(
													string				f_sOgreUniqueName,
													string				f_sMeshFileName,
													CollisionShapeType		f_eCollisionShapeType,
													GameObjectType			f_eHavokObjectType
												  )
{
	initializePhysicsPrimitive();

	m_pPhysicsWorld	= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();
	//first create the graphics object with the f_sOgreUniqueName and f_sMeshFileName
	m_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName,f_sMeshFileName,f_eHavokObjectType));

	switch (f_eCollisionShapeType) {
		case COLLISION_SHAPE_SPHERE :
			addSphere(m_pGraphicsObject,f_eHavokObjectType);
			break;
		case COLLISION_SHAPE_CAPSULE :
			addCapsule(m_pGraphicsObject,f_eHavokObjectType);
			break;
		case COLLISION_SHAPE_CYLINDER :
			addCylinder(m_pGraphicsObject,f_eHavokObjectType);
			break;
		case COLLISION_SHAPE_BOX :
			addBox(m_pGraphicsObject,f_eHavokObjectType);
			break;
		default :
			m_pRigidBodyObject	= NULL;
			break;
	}

	if (m_pRigidBodyObject==NULL) {
		GGETRACELOG("HavokError in Constructor Function (NoHkx_ShapeRigidBody).\n\
					Could not create PhysicsPrimitiveObject. Object is NULL because this basic type hasn't been implemented yet.\n"
					);
		exit(1);
		return false;
	}
	//these should be handled in the functions
	//setRigidBodyMotionType(m_pRigidBodyObject,f_eHavokObjectType);
	//setCollisionLayerInfo(m_pRigidBodyObject,f_eHavokObjectType);
	m_eGameObjectType = f_eHavokObjectType;
	return true;
}

PhysicsPrimitiveObject::~PhysicsPrimitiveObject()
{
	if (m_pRigidBodyObject!=NULL)
	{
		m_pRigidBodyObject->removeReference();
		m_pRigidBodyObject=NULL;
	}
	if (m_pPhysicsSystem!=NULL)
	{
		for (int i=0;i<m_pPhysicsSystem->getRigidBodies().getSize();i++)
		{
			if (m_pPhysicsSystem->getRigidBodies()[i]->isAddedToWorld())
				m_pPhysicsWorld->removeEntity(m_pPhysicsSystem->getRigidBodies()[i]);
		}
		m_pPhysicsSystem = NULL;
	}
}
void PhysicsPrimitiveObject::destroy()
{
	this->~PhysicsPrimitiveObject();
}

bool PhysicsPrimitiveObject::update()
{
	bool f_bRigidBodyAlive=true;
	hkReal f_fTimestep =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorldStep();
	if (m_pRigidBodyObject)
	{
		if ((m_eGameObjectType==PHYSICS_KEYFRAMED)&&(m_pCustomKeyframedFunction!=NULL)&&(m_bKeyframeActive))
		{
			m_pCustomKeyframedFunction(this,f_fTimestep,m_vPosition, m_fQuaternion);
			hkpKeyFrameUtility::applyHardKeyFrame(m_vPosition, m_fQuaternion, 1.0f / f_fTimestep, m_pRigidBodyObject);
		}
		//if the object is out of world it will be deleted from havok world
		//so we should return false to delete the game object.
		f_bRigidBodyAlive |= m_pRigidBodyObject->isAddedToWorld();

		hkReal f_fStoredPosition[3];
		hkVector4 f_vOffsetToApply = m_vCharacterSceneNodeOffset;
		f_vOffsetToApply.add4(this->getPosition());
		f_vOffsetToApply.store3(f_fStoredPosition);
		m_pGraphicsObject->setPosition(f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);

		hkReal f_fStoredRotation[4];
		m_pRigidBodyObject->getRotation().m_vec.store4(f_fStoredRotation);
		m_pGraphicsObject->setOrientation(f_fStoredRotation[0],f_fStoredRotation[1],f_fStoredRotation[2],f_fStoredRotation[3]);
	}
	else if (m_pPhysicsSystem)
	{
		//if all of the rigidBodies are out of the world, then we should delete this GameObject
		f_bRigidBodyAlive = false;
		//for dealing with each rigid body in the physicsSystem
		hkpRigidBody* f_pTempRigidBodyObject = NULL;
		//to have a direct sceneNode pointer of the physics system. just help to read the code better
		Ogre::SceneNode* f_pRootSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
		//next two variables are for getting each rigidBody entity pointer from the root scene node of physics system
		//Ogre::SceneNode* f_pRigidBodySceneNode = NULL;
		Ogre::Node* f_pRigidBodySceneNode = NULL;
		string f_sRigidBodySceneNodeName = "";

		//for each rigid body in the physics system
		//first get the root scene node
		//then update the orientation and position
		for (int i=0;i<m_pPhysicsSystem->getRigidBodies().getSize();i++)
		{
			f_pTempRigidBodyObject = m_pPhysicsSystem->getRigidBodies()[i];
			f_sRigidBodySceneNodeName = "";
			f_sRigidBodySceneNodeName+= m_pGraphicsObject->m_sOgreUniqueName;
			f_sRigidBodySceneNodeName.append(Ogre::StringConverter::toString(i).c_str());
			f_sRigidBodySceneNodeName+="_SceneNode";
			f_pRigidBodySceneNode = f_pRootSceneNode->getChild(f_sRigidBodySceneNodeName);

			hkReal f_fStoredPosition[3];
			hkVector4 f_vSubRigidBodyPos = f_pTempRigidBodyObject->getPosition();
			//f_vSubRigidBodyPos.sub4(m_vOffsetPositions[i]);
			f_vSubRigidBodyPos.store3(f_fStoredPosition);
#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		GraphicsObjectUpdateTask* newTask = new GraphicsObjectUpdateTask(f_pRigidBodySceneNode,f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);
		TaskContainer* container = new TaskContainer((void*)newTask,Graphics_Object_Update_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		f_pRigidBodySceneNode->setPosition(f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);
	}
#else
			f_pRigidBodySceneNode->setPosition(f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);
#endif
			//if any of the rigidBody is still in the world we can not delete this GameObject
			//if 1 is alive it will change the boolean value to true
			f_bRigidBodyAlive |= f_pTempRigidBodyObject->isAddedToWorld();

			hkReal f_fStoredRotation[4];
			f_pTempRigidBodyObject->getRotation().m_vec.store4(f_fStoredRotation);
			//first get the mesh file into 0,0,0 for this entity
			//f_pRigidBodySceneNode->setPosition(0,0,0);
			//set the orientation accordingly
#ifdef TBB_PARALLEL_OPTION
	if(!EnginePtr->initializing)
	{
		GraphicsObjectUpdateTask* newTask = new GraphicsObjectUpdateTask(f_pRigidBodySceneNode,f_fStoredRotation[3],f_fStoredRotation[0],f_fStoredRotation[1],f_fStoredRotation[2]);
		TaskContainer* container = new TaskContainer((void*)newTask,Graphics_Object_Update_Task);
		EnginePtr->taskQueue.push(container);
		EnginePtr->taskQueueSize++;
	}
	else
	{
		f_pRigidBodySceneNode->setOrientation(f_fStoredRotation[3],f_fStoredRotation[0],f_fStoredRotation[1],f_fStoredRotation[2]);
	}
#else
			f_pRigidBodySceneNode->setOrientation( Ogre::Quaternion(f_fStoredRotation[3],f_fStoredRotation[0],f_fStoredRotation[1],f_fStoredRotation[2]) * m_vBaseOrientation[i] );
#endif
			//now move the object to its original place in world
		}
	}
	//means if the object is still alive in Havok World
	return f_bRigidBodyAlive;
}
bool PhysicsPrimitiveObject::scale()
{
	bool f_bScaled=false;
	hkVector4 pos = this->getPosition();
	setPosition(hkVector4(0,0,0));

	switch (m_eCollisionShapeType) {
		case COLLISION_SHAPE_SPHERE :
			f_bScaled = scaleSphere();
			break;
		case COLLISION_SHAPE_CAPSULE :
			f_bScaled = scaleCapsule();
			break;
		case COLLISION_SHAPE_CYLINDER :
			f_bScaled = scaleCylinder();
			break;
		case COLLISION_SHAPE_BOX :
			f_bScaled = scaleBox();
			break;
		default :
			break;
	}

	setPosition(pos);
	return f_bScaled;
}
void PhysicsPrimitiveObject::setCustomKeyframedFunction(void* f_pNewCustomKeyFramedFunction,bool f_bKeyframedEnabled)
{
	m_bKeyframeActive = f_bKeyframedEnabled;
	m_pCustomKeyframedFunction = (Custom_Keyframed_Function)f_pNewCustomKeyFramedFunction;
}

void PhysicsPrimitiveObject::setKeyFrame(hkVector4& f_rPosition, hkQuaternion& f_rQuaternion)
{
	//this function should be deleted!!!
	//I just made it private for now
	m_vPosition   = f_rPosition;
	m_fQuaternion = f_rQuaternion;
}

hkVector4 PhysicsPrimitiveObject::getPosition()
{
	if (m_pRigidBodyObject)
		return m_pRigidBodyObject->getPosition();
	return hkVector4(m_pGraphicsObject->m_pOgreSceneNode->getPosition().x,m_pGraphicsObject->m_pOgreSceneNode->getPosition().y,m_pGraphicsObject->m_pOgreSceneNode->getPosition().z);
}

void PhysicsPrimitiveObject::setPosition(hkVector4& f_rPosition)
{
	if (m_pRigidBodyObject){
		if( m_eCollisionShapeType == COLLISION_SHAPE_BOX ) f_rPosition.add4(m_vBoxShapeOffset);
		m_pRigidBodyObject->setPosition(f_rPosition);
	}
	if (m_pPhysicsSystem) {
		hkVector4 f_vSubRigidBodyPos= hkVector4(0,0,0);
		hkpRigidBody* f_pRigidBodyObject = NULL;
		for (int i=0;i<m_pPhysicsSystem->getRigidBodies().getSize();i++)
		{
			f_pRigidBodyObject = m_pPhysicsSystem->getRigidBodies()[i];
			f_vSubRigidBodyPos = f_pRigidBodyObject->getPosition();
			f_vSubRigidBodyPos.add4(f_rPosition);
			f_pRigidBodyObject->setPosition(f_vSubRigidBodyPos);
		}
	}
}

void PhysicsPrimitiveObject::setMass(float mass)
{
	//sets the rigid body mass
	if (m_pRigidBodyObject!=NULL)
		m_pRigidBodyObject->setMass(mass);
	//if it is a physics system sets the mass of each rigid body one by one
	if (m_pPhysicsSystem!=NULL)
	{
		for (int i=0;i<m_pPhysicsSystem->getRigidBodies().getSize();i++)
		{
			if (m_pPhysicsSystem->getRigidBodies()[i]->isAddedToWorld())
				m_pPhysicsSystem->getRigidBodies()[i]->setMass(mass);
		}
	}
}

void PhysicsPrimitiveObject::addGround()
{
//RDS_PREVDEFINITON please edit this function as required
	//half Extent means the half size of the box for each direction. this is defined this way due to User_Guide.pdf/page 222
	hkVector4 halfExtents( 100.0f, 0.01f, 100.0f );
	//the groundPos is defined according to put the box in 0,0,0 position
	//but as it has 1.0 length between the center point and ground I put Y position to 1.0
	hkVector4 groundPos( 0.0f, -0.01f, 0.0f );
	hkpRigidBodyCinfo info;

	//info.m_mass = 1;
	info.m_shape = new hkpBoxShape(halfExtents);
	info.m_position = groundPos;
	info.m_motionType  = hkpMotion::MOTION_FIXED;
	info.m_qualityType = HK_COLLIDABLE_QUALITY_FIXED;

	m_pRigidBodyObject = new hkpRigidBody( info );
	m_pPhysicsWorld->addEntity( m_pRigidBodyObject );

	m_pRigidBodyObject->removeReference();
	info.m_shape->removeReference();
}
void PhysicsPrimitiveObject::addSphere()
{
	//The sphere will be created as a Unit Sphere
	//so that I will create the Ogre Renderable Unit too to make them same size
	hkReal radius = 1.0f;

	hkpConvexShape* sphereShape = new hkpSphereShape(radius);
	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo sphereInfo;

	sphereInfo.m_shape = sphereShape;
	sphereInfo.m_position.set(0.0f, 1.0f, 0.0f);
	sphereInfo.m_restitution = 2.0f;
	sphereInfo.m_motionType = hkpMotion::MOTION_SPHERE_INERTIA;
	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (sphereInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		sphereInfo.m_mass = 1.0f;
		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeSphereVolumeMassProperties(radius, sphereInfo.m_mass, massProperties);
		sphereInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		sphereInfo.m_centerOfMass = massProperties.m_centerOfMass;
		sphereInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(sphereInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	sphereShape->removeReference();

	// Remove reference since the world now "owns" it.
	m_pRigidBodyObject->removeReference();
}
void PhysicsPrimitiveObject::addSphere(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType)
{
	Ogre::Entity* f_pTempOgreEntity =  m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	//The sphere will be created according to the ACTUAL size of the entity

	// Get scale factors
	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	// Calculate actual body height,width and depth
	hkReal f_rWidth	 = xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth	 = zScale*f_vEntityAabb.getSize().z;

	//find the radius to use
	//we need an approximation because :) lol
	hkReal f_rSphereRadius = (f_rWidth+f_rHeight+f_rDepth)/6.0f;

	// pointer to the shape to be created
	hkpConvexShape* f_pSphereShape = new hkpSphereShape(f_rSphereRadius);
	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo sphereInfo;

	//now I will try to approximate the mass of the object
	//as if it has density = 1. so that its volume will change the mass
	//later this should be changed if the user sets a mass to this
	//so a function should be added to set mass property
	hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
	sphereInfo.m_mass = abs(f_rApproximateMass);

	sphereInfo.m_shape = f_pSphereShape;
	sphereInfo.m_position.set(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z);

	//just some random value. To handle this is future work
	sphereInfo.m_restitution = 2.0f;

	//this parameter should be set according to the GameObject type passed
	//if this parameter was default then we will make it Sphere_Inertia due to the shape we are creating
	hkpMotion::MotionType f_MotionType;
	if (f_eHavokObjectType!=DEFAULT_GAME_OBJECT_TYPE)
		f_MotionType = getHavokMotionType(f_eHavokObjectType);
	else
		f_MotionType = hkpMotion::MOTION_FIXED;
	sphereInfo.m_motionType = f_MotionType;

	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (sphereInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeSphereVolumeMassProperties(f_rSphereRadius, sphereInfo.m_mass, massProperties);
		sphereInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		sphereInfo.m_centerOfMass = massProperties.m_centerOfMass;
		sphereInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(sphereInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pSphereShape->removeReference();

	m_eCollisionShapeType = COLLISION_SHAPE_SPHERE;
}
bool PhysicsPrimitiveObject::scaleSphere()
{
	//first delete the first rigid body and get the needed parameters from that
	hkpMotion::MotionType f_MotionType = m_pRigidBodyObject->getMotionType();
	hkReal f_restitution = m_pRigidBodyObject->getRestitution();
	m_pPhysicsWorld->removeEntity(m_pRigidBodyObject);

	Ogre::Entity* f_pTempOgreEntity =  m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	//The sphere will be created according to the ACTUAL size of the entity

	// Get scale factors
	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	// Calculate actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	//find the radius to use
	hkReal f_rSphereRadius;
	f_rSphereRadius = max(f_rWidth,f_rHeight);
	f_rSphereRadius = max(f_rSphereRadius,f_rDepth);
	f_rSphereRadius /= 2.0f;

	// pointer to the shape to be created
	hkpConvexShape* f_pSphereShape = new hkpSphereShape(f_rSphereRadius);
	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo sphereInfo;

	//now I will try to approximate the mass of the object
	//as if it has density = 1. so that its volume will change the mass
	//later this should be changed if the user sets a mass to this
	//so a function should be added to set mass property
	hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
	sphereInfo.m_mass = abs(f_rApproximateMass);

	sphereInfo.m_shape = f_pSphereShape;
	sphereInfo.m_position.set(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z);

	//just some random value. To handle this is future work
	sphereInfo.m_restitution = f_restitution;
	sphereInfo.m_motionType = f_MotionType;

	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (sphereInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeSphereVolumeMassProperties(f_rSphereRadius, sphereInfo.m_mass, massProperties);
		sphereInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		sphereInfo.m_centerOfMass = massProperties.m_centerOfMass;
		sphereInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(sphereInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pSphereShape->removeReference();
	return true;
}
void PhysicsPrimitiveObject::addCapsule(){}
void PhysicsPrimitiveObject::addCapsule(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType)
{
	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	hkReal radius;
	hkReal capsuleHeight;
	hkVector4 vertexA;
	hkVector4 vertexB;

	//m_vCharacterSceneNodeOffset = hkVector4(-f_vCenterOfEntity.x,-f_vCenterOfEntity.y,-f_vCenterOfEntity.z);
	m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);

	if ((f_rHeight>f_rDepth)&&(f_rHeight>f_rWidth))
	{//y oriented
		radius = max(f_rWidth, f_rDepth)/2.0f;
		capsuleHeight = f_rHeight/4.0f;
		/*m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);*/
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*capsuleHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x,-yScale*capsuleHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else if ((f_rWidth>f_rHeight)&&(f_rWidth>f_rDepth))
	{//x oriented
		radius = max(f_rHeight, f_rDepth)/2.0f;
		capsuleHeight = f_rWidth/4.0f;
		//m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set( xScale*capsuleHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(-xScale*capsuleHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else
	{//z oriented
		radius = max(f_rWidth, f_rHeight)/2.0f;
		capsuleHeight = f_rDepth/4.0f;
		//m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*capsuleHeight + f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, -zScale*capsuleHeight + f_vCenterOfEntity.z);
	}

	hkpShape* f_pCapsuleShape;//-->poiter to shape to be created as a capsule

	// Adjust offset to make similar position as using a hkx file.
	//vertexA.add(m_vCharacterSceneNodeOffset);
	//vertexB.add(m_vCharacterSceneNodeOffset);

	// Create a capsule to represent the character standing
	// Havok Physics->Collision Detection->Optimizing and Tuning Collision Detection->Convex Shape Radius.
	// Basically there is a shell around convex objects that keeps them a short distance apart. The reason for this is to
	// minimize the risk of objects penetrating. The convex-convex collision detection algorithm is fastest when objects are
	// not inter penetrating. It you see a graphical artifact where it looks like your graphics mesh is floating above the
	// ground by the convex radius, you need to shrink your physics representation down by the convex radius.
	f_pCapsuleShape = new hkpCapsuleShape(vertexA, vertexB, radius);

	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo capsuleInfo;

	capsuleInfo.m_shape = f_pCapsuleShape;
	capsuleInfo.m_position.set(f_ObjectPosition.x,f_ObjectPosition.y,f_ObjectPosition.z);

	//just some random value. To handle this is future work
	capsuleInfo.m_restitution = 2.0f;

	//this parameter should be set according to the GameObject type passed
	//if this parameter was default then we will make it Sphere_Inertia due to the shape we are creating
	hkpMotion::MotionType f_MotionType;
	if (f_eHavokObjectType!=DEFAULT_GAME_OBJECT_TYPE)
		f_MotionType = getHavokMotionType(f_eHavokObjectType);
	else
		f_MotionType = hkpMotion::MOTION_FIXED;
	capsuleInfo.m_motionType = f_MotionType;

	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (capsuleInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		//now I will try to approximate the mass of the object
		//as if it has density = 1. so that its volume will change the mass
		//later this should be changed if the user sets a mass to this
		//so a function should be added to set mass property
		hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
		capsuleInfo.m_mass = abs(f_rApproximateMass);

		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeCapsuleVolumeMassProperties(vertexA, vertexB, radius, f_rApproximateMass, massProperties);
		capsuleInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		capsuleInfo.m_centerOfMass = massProperties.m_centerOfMass;
		capsuleInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(capsuleInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pCapsuleShape->removeReference();

	m_eCollisionShapeType = COLLISION_SHAPE_CAPSULE;
}

bool PhysicsPrimitiveObject::scaleCapsule()
{
	//first delete the first rigid body and get the needed parameters from that
	hkpMotion::MotionType f_MotionType = m_pRigidBodyObject->getMotionType();
	hkReal f_restitution = m_pRigidBodyObject->getRestitution();

	m_pPhysicsWorld->lock();
	m_pPhysicsWorld->removeEntity(m_pRigidBodyObject);

	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	hkReal radius;
	hkReal capsuleHeight;
	hkVector4 vertexA;
	hkVector4 vertexB;

	if ((f_rHeight>f_rDepth)&&(f_rHeight>f_rWidth))
	{//y oriented
		radius = max(f_rWidth, f_rDepth)/2.0f;
		capsuleHeight = f_rHeight/4.0f;
		m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*capsuleHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x,-yScale*capsuleHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else if ((f_rWidth>f_rHeight)&&(f_rWidth>f_rDepth))
	{//x oriented
		radius = max(f_rHeight, f_rDepth)/2.0f;
		capsuleHeight = f_rWidth/4.0f;
		m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set( xScale*capsuleHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(-xScale*capsuleHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else
	{//z oriented
		radius = max(f_rWidth, f_rHeight)/2.0f;
		capsuleHeight = f_rDepth/4.0f;
		m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*capsuleHeight + f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, -zScale*capsuleHeight + f_vCenterOfEntity.z);
	}

	hkpShape* f_pCapsuleShape;//-->poiter to shape to be created as a capsule

	// Create a capsule to represent the character standing
	// Havok Physics->Collision Detection->Optimizing and Tuning Collision Detection->Convex Shape Radius.
	// Basically there is a shell around convex objects that keeps them a short distance apart. The reason for this is to
	// minimize the risk of objects penetrating. The convex-convex collision detection algorithm is fastest when objects are
	// not inter penetrating. It you see a graphical artifact where it looks like your graphics mesh is floating above the
	// ground by the convex radius, you need to shrink your physics representation down by the convex radius.
	f_pCapsuleShape = new hkpCapsuleShape(vertexA, vertexB, radius);

	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo capsuleInfo;

	capsuleInfo.m_shape = f_pCapsuleShape;

	capsuleInfo.m_position.setNeg4(f_vPrevCharacterSceneNodeOffset);
	capsuleInfo.m_position.add4(hkVector4(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z));

	//just some random value. To handle this is future work
	capsuleInfo.m_restitution = f_restitution;
	capsuleInfo.m_motionType = f_MotionType;

	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (capsuleInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		//now I will try to approximate the mass of the object
		//as if it has density = 1. so that its volume will change the mass
		//later this should be changed if the user sets a mass to this
		//so a function should be added to set mass property
		hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
		capsuleInfo.m_mass = abs(f_rApproximateMass);

		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeCylinderVolumeMassProperties(vertexA, vertexB, f_rHeight/2.0f, f_rApproximateMass, massProperties);
		capsuleInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		capsuleInfo.m_centerOfMass = massProperties.m_centerOfMass;
		capsuleInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(capsuleInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pCapsuleShape->removeReference();

	m_pPhysicsWorld->unlock();
	return true;
}
void PhysicsPrimitiveObject::addCylinder(){}
void PhysicsPrimitiveObject::addCylinder(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType)
{
	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	hkReal radius;
	hkReal cylinderHeight;
	hkVector4 vertexA;
	hkVector4 vertexB;

	//m_vCharacterSceneNodeOffset = hkVector4(-f_vCenterOfEntity.x,-f_vCenterOfEntity.y,-f_vCenterOfEntity.z);
	m_vCharacterSceneNodeOffset = hkVector4(0,0,0);

	if ((f_rHeight>f_rDepth)&&(f_rHeight>f_rWidth))
	{//y oriented
		radius = max(f_rWidth, f_rDepth)/2.0f;
		cylinderHeight = f_rHeight/2.0f;
		//m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*cylinderHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x,-yScale*cylinderHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else if ((f_rWidth>f_rHeight)&&(f_rWidth>f_rDepth))
	{//x oriented
		radius = max(f_rHeight, f_rDepth)/2.0f;
		cylinderHeight = f_rWidth/2.0f;
		//m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set( xScale*cylinderHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(-xScale*cylinderHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else
	{//z oriented
		radius = max(f_rWidth, f_rHeight)/2.0f;
		cylinderHeight = f_rDepth/2.0f;
		//m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*cylinderHeight + f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, -zScale*cylinderHeight + f_vCenterOfEntity.z);
	}

	hkpShape* f_pCylinderShape;

	// adjust offset to get similar position with using hkx
	//vertexA.add(m_vCharacterSceneNodeOffset);
	//vertexB.add(m_vCharacterSceneNodeOffset);

	f_pCylinderShape = new hkpCylinderShape(vertexA, vertexB, radius);

	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo cylinderInfo;

	cylinderInfo.m_shape = f_pCylinderShape;
	cylinderInfo.m_position.set(f_ObjectPosition.x,f_ObjectPosition.y,f_ObjectPosition.z);

	//just some random value. To handle this is future work
	cylinderInfo.m_restitution = 2.0f;

	//this parameter should be set according to the GameObject type passed
	//if this parameter was default then we will make it Sphere_Inertia due to the shape we are creating
	hkpMotion::MotionType f_MotionType;
	if (f_eHavokObjectType!=DEFAULT_GAME_OBJECT_TYPE)
		f_MotionType = getHavokMotionType(f_eHavokObjectType);
	else
		f_MotionType = hkpMotion::MOTION_FIXED;
	cylinderInfo.m_motionType = f_MotionType;

	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (cylinderInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		//now I will try to approximate the mass of the object
		//as if it has density = 1. so that its volume will change the mass
		//later this should be changed if the user sets a mass to this
		//so a function should be added to set mass property
		hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
		cylinderInfo.m_mass = abs(f_rApproximateMass);

		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeCapsuleVolumeMassProperties(vertexA, vertexB, radius, f_rApproximateMass, massProperties);
		cylinderInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		cylinderInfo.m_centerOfMass = massProperties.m_centerOfMass;
		cylinderInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(cylinderInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pCylinderShape->removeReference();

	m_eCollisionShapeType = COLLISION_SHAPE_CYLINDER;
}
bool PhysicsPrimitiveObject::scaleCylinder()
{
		//first delete the first rigid body and get the needed parameters from that
	hkpMotion::MotionType f_MotionType = m_pRigidBodyObject->getMotionType();
	hkReal f_restitution = m_pRigidBodyObject->getRestitution();

	m_pPhysicsWorld->lock();
	m_pPhysicsWorld->removeEntity(m_pRigidBodyObject);

	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	hkReal radius;
	hkReal cylinderHeight;
	hkVector4 vertexA;
	hkVector4 vertexB;

	if ((f_rHeight>f_rDepth)&&(f_rHeight>f_rWidth))
	{//y oriented
		radius = max(f_rWidth, f_rDepth)/2.0f;
		cylinderHeight = f_rHeight/4.0f;
		m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*cylinderHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x,-yScale*cylinderHeight + f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else if ((f_rWidth>f_rHeight)&&(f_rWidth>f_rDepth))
	{//x oriented
		radius = max(f_rHeight, f_rDepth)/2.0f;
		cylinderHeight = f_rWidth/4.0f;
		m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set( xScale*cylinderHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
		vertexB.set(-xScale*cylinderHeight + f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*f_vCenterOfEntity.z);
	}
	else
	{//z oriented
		radius = max(f_rWidth, f_rHeight)/2.0f;
		cylinderHeight = f_rDepth/4.0f;
		m_vCharacterSceneNodeOffset = hkVector4(0.0f,0.0f,0.0f);
		vertexA.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, zScale*cylinderHeight + f_vCenterOfEntity.z);
		vertexB.set(xScale*f_vCenterOfEntity.x, yScale*f_vCenterOfEntity.y, -zScale*cylinderHeight + f_vCenterOfEntity.z);
	}

	hkpShape* f_pCylinderShape;//-->poiter to shape to be created as a capsule

	// Create a capsule to represent the character standing
	// Havok Physics->Collision Detection->Optimizing and Tuning Collision Detection->Convex Shape Radius.
	// Basically there is a shell around convex objects that keeps them a short distance apart. The reason for this is to
	// minimize the risk of objects penetrating. The convex-convex collision detection algorithm is fastest when objects are
	// not inter penetrating. It you see a graphical artifact where it looks like your graphics mesh is floating above the
	// ground by the convex radius, you need to shrink your physics representation down by the convex radius.
	f_pCylinderShape = new hkpCylinderShape(vertexA, vertexB, radius);

	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo cylinderInfo;

	cylinderInfo.m_shape = f_pCylinderShape;

	cylinderInfo.m_position.setNeg4(f_vPrevCharacterSceneNodeOffset);
	cylinderInfo.m_position.add4(hkVector4(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z));

	//just some random value. To handle this is future work
	cylinderInfo.m_restitution = f_restitution;
	cylinderInfo.m_motionType = f_MotionType;

	// If we need to compute mass properties, we'll do this using the hkpInertiaTensorComputer.
	if (cylinderInfo.m_motionType != hkpMotion::MOTION_FIXED)
	{
		//now I will try to approximate the mass of the object
		//as if it has density = 1. so that its volume will change the mass
		//later this should be changed if the user sets a mass to this
		//so a function should be added to set mass property
		hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
		cylinderInfo.m_mass = abs(f_rApproximateMass);

		hkpMassProperties massProperties;
		hkpInertiaTensorComputer::computeCylinderVolumeMassProperties(vertexA, vertexB, f_rHeight/2.0f, f_rApproximateMass, massProperties);
		cylinderInfo.m_inertiaTensor = massProperties.m_inertiaTensor;
		cylinderInfo.m_centerOfMass = massProperties.m_centerOfMass;
		cylinderInfo.m_mass = massProperties.m_mass;
	}
	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(cylinderInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pCylinderShape->removeReference();

	m_pPhysicsWorld->unlock();
	return true;
}
void PhysicsPrimitiveObject::addBox()
{
	//Needed parameteres :
	//1. X_length
	//2. Y_length
	//3. Z_length
	//4. center_position
	//5. mass
	//6. motion_type = hkpMotion::MOTION_BOX_INERTIA
	//7. quality_type = HK_COLLIDABLE_QUALITY_CHARACTER
//RDS_LOOK
	//half Extent means the half size of the box for each direction. this is defined this way due to User_Guide.pdf/page 222
	//hkVector4 halfExtents(1,1,1);
	////the groundPos is defined according to put the box in 0,0,0 position
	////but as it has 1.0 length between the center point and ground I put Y position to 1.0
	//hkVector4 groundPos( 0.0f, 1.0f, 0.0f );
	//hkpRigidBodyCinfo info;
	//info.m_mass = 300;
	//info.m_shape = new hkpBoxShape(halfExtents);
	//info.m_position = groundPos;
	//info.m_motionType  = hkpMotion::MOTION_BOX_INERTIA;
	//info.m_qualityType = HK_COLLIDABLE_QUALITY_CHARACTER;
	//m_pRigidBodyObject = new hkpRigidBody( info );
	//m_pPhysicsWorld->addEntity( m_pRigidBodyObject );
	//m_pRigidBodyObject->removeReference();
	//info.m_shape->removeReference();
}

void PhysicsPrimitiveObject::addBox(GraphicsObject* f_pGraphicsObject,GameObjectType f_eHavokObjectType)
{
	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	hkVector4 halfExtents(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f);
	hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;
	m_vCharacterSceneNodeOffset = hkVector4(-f_vCenterOfEntity.x,-f_vCenterOfEntity.y,-f_vCenterOfEntity.z);
	m_vBoxShapeOffset = hkVector4(f_vCenterOfEntity.x,f_vCenterOfEntity.y,f_vCenterOfEntity.z);
	//m_vCharacterSceneNodeOffset = hkVector4(0,0,0);

	hkpShape* f_pBoxShape = new hkpBoxShape(halfExtents);//-->poiter to shape to be created as a box

	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo f_RigidBodyInfo;

	f_RigidBodyInfo.m_shape = f_pBoxShape;
	f_RigidBodyInfo.m_position.set(f_ObjectPosition.x,f_ObjectPosition.y,f_ObjectPosition.z);
	f_RigidBodyInfo.m_position.sub4(f_vPrevCharacterSceneNodeOffset);

	//now I will try to approximate the mass of the object
	//as if it has density = 1. so that its volume will change the mass
	//later this should be changed if the user sets a mass to this
	//so a function should be added to set mass property
	hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
	f_RigidBodyInfo.m_mass = abs(f_rApproximateMass);

	//just some random value. To handle this is future work
	f_RigidBodyInfo.m_restitution = 2.0f;

	//this parameter should be set according to the GameObject type passed
	//if this parameter was default then we will make it fixed due to the shape we are creating
	hkpMotion::MotionType f_MotionType;
	if (f_eHavokObjectType!=DEFAULT_GAME_OBJECT_TYPE)
		f_MotionType = getHavokMotionType(f_eHavokObjectType);
	else
		f_MotionType = hkpMotion::MOTION_FIXED;
	f_RigidBodyInfo.m_motionType = f_MotionType;

	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(f_RigidBodyInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pBoxShape->removeReference();

	m_eCollisionShapeType = COLLISION_SHAPE_BOX;
}

bool PhysicsPrimitiveObject::scaleBox()
{
	//first delete the first rigid body and get the needed parameters from that
	hkpMotion::MotionType f_MotionType = m_pRigidBodyObject->getMotionType();
	hkReal f_restitution = m_pRigidBodyObject->getRestitution();

	m_pPhysicsWorld->lock();
	m_pPhysicsWorld->removeEntity(m_pRigidBodyObject);

	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition(0,0,0); //f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	hkVector4 halfExtents(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f);
	hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;
	m_vCharacterSceneNodeOffset = hkVector4(-f_vCenterOfEntity.x * xScale,-f_vCenterOfEntity.y * yScale,-f_vCenterOfEntity.z * zScale);

	hkpShape* f_pBoxShape = new hkpBoxShape(halfExtents);;//-->poiter to shape to be created as a box

	// To illustrate using the shape, create a rigid body.
	hkpRigidBodyCinfo f_RigidBodyInfo;

	f_RigidBodyInfo.m_shape = f_pBoxShape;
	f_RigidBodyInfo.m_position.set(f_ObjectPosition.x,f_ObjectPosition.y,f_ObjectPosition.z);
	//f_RigidBodyInfo.m_position.sub4(f_vPrevCharacterSceneNodeOffset);

	//now I will try to approximate the mass of the object
	//as if it has density = 1. so that its volume will change the mass
	//later this should be changed if the user sets a mass to this
	//so a function should be added to set mass property
	hkReal f_rApproximateMass = f_rWidth*f_rHeight*f_rDepth;
	f_RigidBodyInfo.m_mass = abs(f_rApproximateMass);

	f_RigidBodyInfo.m_restitution = f_restitution;
	f_RigidBodyInfo.m_motionType = f_MotionType;

	// Create a rigid body (using the template above)
	m_pRigidBodyObject = new hkpRigidBody(f_RigidBodyInfo);

	// Finally add body so we can see it
	m_pPhysicsWorld->addEntity(m_pRigidBodyObject);

	// Remove reference since the body now "owns" the Shape
	f_pBoxShape->removeReference();
	m_pPhysicsWorld->unlock();
	return true;
}
void PhysicsPrimitiveObject::addConvexHull(){}
/***********************************************PhysicsObjectDefinitionsEND***********************************************/
/*****************************************CharacterProxyObjectDefinitionsSTART*****************************************/
MyCharacterProxyListener::MyCharacterProxyListener(CharacterProxyObject* f_pOwnerCharacterProxy) : m_bAtLadder(false)
{
	m_pOwnerCharacterProxy = f_pOwnerCharacterProxy;
	m_fCustom_contactPointAddedCallback_Function = NULL;
	m_fCustom_contactPointRemovedCallback_Function = NULL;
}
void MyCharacterProxyListener::addCustom_contactPointAddedCallback_Function(void* f_fNewCustom_contactPointAddedCallback_Function)
{
	m_fCustom_contactPointAddedCallback_Function = (Custom_CharacterProxy_Collision_Callback_Function)f_fNewCustom_contactPointAddedCallback_Function;
}
void MyCharacterProxyListener::addCustom_contactPointRemovedCallback_Function(void* f_fNewCustom_contactPointRemovedCallback_Function)
{
	m_fCustom_contactPointRemovedCallback_Function = (Custom_CharacterProxy_Collision_Callback_Function)f_fNewCustom_contactPointRemovedCallback_Function;
}

// Ladder handling code goes here
void MyCharacterProxyListener::contactPointAddedCallback( const hkpCharacterProxy* proxy, const hkpRootCdPoint& point )
{
	hkpRigidBody* f_pCollidedRigidBody = hkpGetRigidBody(point.m_rootCollidableB);
	GameObject* f_pCollidedGameObject = NULL;

	if (f_pCollidedRigidBody==NULL)
		return;

	//Now check if the rigidBody has a collision listener.
	//If it has a collisionlistener we can also send the GameObject pointer back to the function caller too
	if (f_pCollidedRigidBody->getContactListeners().getSize()>0)
	{
		//if we are here it means the rigidbody has a collision listener
		//so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
		//this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
		CollisionListener* f_pOtherGameObjectListener = (CollisionListener*)(f_pCollidedRigidBody->getContactListeners()[0]);

		//Now get the pointer and set it to the passed parameter so that we are sending back the pointer of the collided gameObject
		f_pCollidedGameObject = f_pOtherGameObjectListener->m_pGameObject;
	}

	if (m_fCustom_contactPointAddedCallback_Function!=NULL)
		m_fCustom_contactPointAddedCallback_Function(m_pOwnerCharacterProxy,f_pCollidedRigidBody,f_pCollidedGameObject);

	if ( getGameObjectType(f_pCollidedRigidBody->getMotionType())==PHYSICS_FIXED )
	{
		m_bAtLadder = true;
		m_vLadderNorm = point.m_contact.getNormal();
		f_pCollidedRigidBody->getPointVelocity(point.m_contact.getPosition(), m_vLadderVelocity);
	}
}

void MyCharacterProxyListener::contactPointRemovedCallback( const hkpCharacterProxy* proxy, const hkpRootCdPoint& point)
{
	hkpRigidBody* f_pCollidedRigidBody = hkpGetRigidBody(point.m_rootCollidableB);
	GameObject* f_pCollidedGameObject = NULL;

	if (f_pCollidedRigidBody==NULL)
		return;

	//Now check if the rigidBody has a collision listener.
	//If it has a collisionlistener we can also send the GameObject pointer back to the function caller too
	if (f_pCollidedRigidBody->getContactListeners().getSize()>0)
	{
		//if we are here it means the rigidbody has a collision listener
		//so we are gonna grab it, but we will just grab the first one because normally 1 gameObject should have 1 collision listener
		//this assumption should be correct because I create collision listeners for gameObjects and I only have 1 to 1 attachment
		CollisionListener* f_pOtherGameObjectListener = (CollisionListener*)(f_pCollidedRigidBody->getContactListeners()[0]);

		//Now get the pointer and set it to the passed parameter so that we are sending back the pointer of the collided gameObject
		f_pCollidedGameObject = f_pOtherGameObjectListener->m_pGameObject;
	}

	if (m_fCustom_contactPointRemovedCallback_Function!=NULL)
		m_fCustom_contactPointRemovedCallback_Function(m_pOwnerCharacterProxy,f_pCollidedRigidBody,f_pCollidedGameObject);

	if ( getGameObjectType(f_pCollidedRigidBody->getMotionType())==PHYSICS_FIXED )
	{
		m_bAtLadder = false;
	}
}
CharacterProxyObject::CharacterProxyObject(GraphicsObject*	f_pGraphicsObject) : PhysicsObject()
{
	m_pPhysicsWorld		= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();
	m_pGraphicsObject	= f_pGraphicsObject;

	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	//	Create a capsule shape for character proxy object
	{
		createCapsule();
		//m_pStandShape = new hkpCapsuleShape(vertexA, vertexB, radius);

		//we will also create a smaller shape for further use
		/*vertexA.setZero4();
		m_pHalfSizeShape = new hkpCapsuleShape(vertexA, vertexB, f_rHeight/4);*/

		// Construct a Shape Phantom
		m_pPhantomShape = new hkpSimpleShapePhantom( m_pStandShape, hkTransform::getIdentity(), COLLISION_LAYER_CHARACTER_PLAYER);
		//we will initially start with m_pStandShape so that we will set our shape state to PROXY_STAND_SHAPE
		m_eCharacterShapeState = PROXY_STAND_SHAPE;

		// Add the phantom to the world
		m_pPhysicsWorld->addPhantom(m_pPhantomShape);
		m_pPhantomShape->removeReference();

		// Construct a character proxy
		hkpCharacterProxyCinfo cpci;
		cpci.m_position.set(0.0f,0.0f,0.0f);
		cpci.m_staticFriction = 0.0f;
		cpci.m_dynamicFriction = 1.0f;
		cpci.m_up.setNeg4( m_pPhysicsWorld->getGravity() );
		cpci.m_up.normalize3();

		cpci.m_shapePhantom = m_pPhantomShape;
		m_pCharacterProxy = new hkpCharacterProxy( cpci );

		m_pCharacterProxyListener = new MyCharacterProxyListener(this);
		m_fCustom_contactPointAddedCallback_Function = NULL;
		m_fCustom_contactPointRemovedCallback_Function = NULL;
		m_pCharacterProxy->addCharacterProxyListener(m_pCharacterProxyListener);
	}
	// Create the Character state machine and context; please add your own custom state if needed
	{
		hkpCharacterState* state;
		hkpCharacterStateManager* manager = new hkpCharacterStateManager();

		state = new hkpCharacterStateOnGround();
		manager->registerState( state,	HK_CHARACTER_ON_GROUND);
		state->removeReference();

		state = new hkpCharacterStateInAir();
		manager->registerState( state,	HK_CHARACTER_IN_AIR);
		state->removeReference();

		state = new hkpCharacterStateJumping();
		manager->registerState( state,	HK_CHARACTER_JUMPING);
		state->removeReference();

		state = new hkpCharacterStateClimbing();
		manager->registerState( state,	HK_CHARACTER_CLIMBING);
		state->removeReference();

		m_pCharacterContext = new hkpCharacterContext(manager, HK_CHARACTER_ON_GROUND);
		manager->removeReference();
	}

	m_fBaseLookingAngle = 0.0f;					// Assuming character is facing x=1
	m_fCurrentAngle = 0.0f;						//Character will be started by looking forward
	m_vNormal.set(0,1,0,0);						//This is for if we want to apply changes on the normal of the character

	//initial velocity is 1
	m_fVelocity = 1.0f;

	//character will start in a not moving position
	m_iLeftPlus_RightMinus			= 0.0f;
	m_iForwardMinus_BackPlus		= 0.0f;
	m_fRotAngle_LeftPlus_RightMinus	= 0.0f;
	m_bJUMP							= false;

	flycheatflag = false;

	//we assume here that the base orientation of the character is looking at the X=1 direction
	//but the model creator must create its model facing X=1 direction
	m_vBaseCharacterForward.set(1,0,0);
	//we will set the scale factor to 1 so that character will be affected by exactly world's gravity
	m_fCharacterGravityScaleFactor = 1.0f;
}
bool CharacterProxyObject::switchCharacterProxyShape(CharacterShapeStateType f_eNewShapeType)
{
	if (m_eCharacterShapeState==f_eNewShapeType)
		return false;//we are already with that shape, so do nothing

	m_pPhysicsWorld->lock();
	hkpShape* f_pCurrentShape=NULL;
	m_pPhysicsWorld->removePhantom( m_pPhantomShape );
	switch (f_eNewShapeType)
	{
		case PROXY_STAND_SHAPE :
			if (m_eCharacterShapeState==PROXY_CRAWL_SHAPE)
			{
				hkVector4 firstPosition = hkVector4(1.0f,2.1f,1.0f);
			    firstPosition.mul4(m_pCharacterProxy->getPosition());
				m_pPhantomShape->setPosition(firstPosition);
				m_pGraphicsObject->m_pOgreSceneNode->setScale(2,2,2);
			}
			m_pPhantomShape->setShape(m_pStandShape);
			m_pGraphicsObject->m_pOgreSceneNode->setScale(1,1,1);
			break;
		case PROXY_CRAWL_SHAPE :
			m_pPhantomShape->setShape(m_pHalfSizeShape);
			m_pGraphicsObject->m_pOgreSceneNode->setScale(0.5,0.5,0.5);
			break;
		case PROXY_AI_SHAPE :
			m_pPhantomShape->setShape(m_pAIShape);
	}
	//then add it back with the new shape
	m_pPhysicsWorld->addPhantom( m_pPhantomShape );

	//now check if the new shape is switchable
	//if not switch back to the previous state
	hkpClosestCdPointCollector collector;
	m_pPhantomShape->getClosestPoints( collector );

	// Allow a very slight tolerance (approx 1cm)

	if (collector.hasHit() && collector.getHit().m_contact.getDistance() < .01f)
	{
		// Switch the phantom back to our current shape.
		// N.B. To be safe, we always remove the phantom from the world first, then change the shape,
		// then re-add, in order to refresh the cached agents in any hkCachingShapePhantoms which
		// may also be present in the world.
		// This also forces the display to be rebuilt, which is necessary for us to see the new shape!
		{
			// Note we do not have to add a reference before removing becasue we hold a hkpCharacterProxy
			// which has a reference to this phantom - hence removal from the world cannot cause this phantom to
			// be accidentally deleted.
			m_pPhysicsWorld->removePhantom( m_pPhantomShape );
			switch (m_eCharacterShapeState)
			{
				case PROXY_STAND_SHAPE :
					m_pPhantomShape->setShape(m_pStandShape);
					m_pGraphicsObject->m_pOgreSceneNode->setScale(1,1,1);
					break;
				case PROXY_CRAWL_SHAPE :
					m_pPhantomShape->setShape(m_pHalfSizeShape);
					m_pGraphicsObject->m_pOgreSceneNode->setScale(0.5,0.5,0.5);
					break;
				case PROXY_AI_SHAPE :
					m_pPhantomShape->setShape(m_pAIShape);
					break;
			}
			m_pPhysicsWorld->addPhantom( m_pPhantomShape );
			m_pPhysicsWorld->unlock();
			return false;
		}
	}
	m_pPhysicsWorld->unlock();
	m_eCharacterShapeState = f_eNewShapeType;
	return true;
}

bool CharacterProxyObject::startStanding()
{
	return switchCharacterProxyShape(PROXY_STAND_SHAPE);
}
bool CharacterProxyObject::startCrawling()
{
	return switchCharacterProxyShape(PROXY_CRAWL_SHAPE);
}
bool CharacterProxyObject::startAIShape()
{
	return switchCharacterProxyShape(PROXY_AI_SHAPE);
}
bool CharacterProxyObject::scale()
{
	m_pPhysicsWorld->lock();

	delete(m_pCharacterProxyListener);
	m_pStandShape->removeReference();
	m_pHalfSizeShape->removeReference();
	m_pAIShape->removeReference();
	m_pPhysicsWorld->removePhantom(m_pPhantomShape);
	m_pCharacterProxy->removeReference();

	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;

	createCapsule();

	// Construct a Shape Phantom
	m_pPhantomShape = new hkpSimpleShapePhantom( m_pStandShape, hkTransform::getIdentity(), COLLISION_LAYER_CHARACTER_PLAYER);
	//we will initially start with m_pStandShape so that we will set our shape state to PROXY_STAND_SHAPE
	m_eCharacterShapeState = PROXY_STAND_SHAPE;

	// Add the phantom to the world
	m_pPhysicsWorld->addPhantom(m_pPhantomShape);
	m_pPhantomShape->removeReference();

	// Construct a character proxy
	hkpCharacterProxyCinfo cpci;
	cpci.m_position.setNeg4(f_vPrevCharacterSceneNodeOffset);
	cpci.m_position.add4(hkVector4(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z));
	cpci.m_staticFriction = 0.0f;
	cpci.m_dynamicFriction = 1.0f;
	cpci.m_up.setNeg4( m_pPhysicsWorld->getGravity() );
	cpci.m_up.normalize3();

	cpci.m_shapePhantom = m_pPhantomShape;
	m_pCharacterProxy = new hkpCharacterProxy( cpci );

	m_pCharacterProxyListener = new MyCharacterProxyListener(this);
	m_pCharacterProxyListener->addCustom_contactPointAddedCallback_Function((void*)m_fCustom_contactPointAddedCallback_Function);
	m_pCharacterProxyListener->addCustom_contactPointRemovedCallback_Function((void*)m_fCustom_contactPointRemovedCallback_Function);
	m_pCharacterProxy->addCharacterProxyListener(m_pCharacterProxyListener);

	m_pPhysicsWorld->unlock();
	return true;
}
void CharacterProxyObject::addCustom_contactPointAddedCallback_Function(void* f_fNewCustom_contactPointAddedCallback_Function)
{
	m_fCustom_contactPointAddedCallback_Function = (Custom_CharacterProxy_Collision_Callback_Function)f_fNewCustom_contactPointAddedCallback_Function;
	m_pCharacterProxyListener->addCustom_contactPointAddedCallback_Function((void*)m_fCustom_contactPointAddedCallback_Function);
}
void CharacterProxyObject::addCustom_contactPointRemovedCallback_Function(void* f_fNewCustom_contactPointRemovedCallback_Function)
{
	m_fCustom_contactPointRemovedCallback_Function = (Custom_CharacterProxy_Collision_Callback_Function)f_fNewCustom_contactPointRemovedCallback_Function;
	m_pCharacterProxyListener->addCustom_contactPointRemovedCallback_Function((void*)m_fCustom_contactPointRemovedCallback_Function);
}

CharacterProxyObject::~CharacterProxyObject()
{
	delete(m_pCharacterProxyListener);
	m_pStandShape->removeReference();
	m_pHalfSizeShape->removeReference();
	m_pAIShape->removeReference();
	m_pPhysicsWorld->removePhantom(m_pPhantomShape);
	m_pCharacterProxy->removeReference();
	delete(m_pCharacterContext);
}
void CharacterProxyObject::destroy()
{
	this->~CharacterProxyObject();
}

void CharacterProxyObject::setVelocity(hkReal f_fNewVelocity)
{
	if (f_fNewVelocity<0)
		return;
	if (f_fNewVelocity>10)//RDS_HARDCODED-->I just didn't want to set a velocity of a character more than sth.What is a better way to put threshold??
		f_fNewVelocity=10;
	m_fVelocity = f_fNewVelocity;
}

void CharacterProxyObject::setInputs(hkReal f_iLeftPlus_RightMinus, hkReal f_iForwardMinus_BackPlus, bool f_bJump,hkReal f_fRotAngle_LeftPlus_RightMinus)
{
	Ogre::Vector3 f_vectorNormalizer(f_iLeftPlus_RightMinus,f_iForwardMinus_BackPlus,0);
	f_vectorNormalizer.normalise();

	m_iLeftPlus_RightMinus			= f_vectorNormalizer.x;
	m_iForwardMinus_BackPlus		= f_vectorNormalizer.y;
	m_bJUMP							= f_bJump;
	m_fRotAngle_LeftPlus_RightMinus = f_fRotAngle_LeftPlus_RightMinus;
}

bool CharacterProxyObject::update()
{
	hkReal f_fTimestep =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorldStep();
	this->stepObject(f_fTimestep);

	hkReal f_fStoredPosition[3];
	hkVector4 newPosition = m_vCharacterSceneNodeOffset;
	newPosition.add4(this->getPosition());
	newPosition.store3(f_fStoredPosition);
	if (m_pGraphicsObject) m_pGraphicsObject->setPosition(f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);

	//RDS_PREVDEFINITION does not support rotation of character
#ifdef TBB_PARALLEL_OPTION
	Ogre::Quaternion quat = Ogre::Quaternion(Ogre::Radian(m_fBaseLookingAngle+m_fCurrentAngle),Ogre::Vector3::UNIT_Y);
	m_pGraphicsObject->setOrientation(quat.x,quat.y,quat.z,quat.w);
#else
	m_pGraphicsObject->m_pOgreSceneNode->setOrientation(Ogre::Quaternion(Ogre::Radian(m_fBaseLookingAngle+m_fCurrentAngle),Ogre::Vector3::UNIT_Y));
#endif
	//RDS_LOOK when should we return false to delete the gameObject??

	return true;
}

string CharacterProxyObject::getCharacterStatus()
{
	return m_sStatus;
}

void CharacterProxyObject::stepObject(hkReal f_fTimestep)
{
		//this line is here for character proxy.
		//so that no penetrations will be allowed during the step time of charcter proxy
	m_pPhysicsWorld->lock();

	hkVector4 f_vUp;
	hkQuaternion f_qOrient;
	{
		f_vUp.setNeg4( m_pPhysicsWorld->getGravity() );
		f_vUp.normalize3();
		//so now here f_vUP is = 0,1,0,0. in Havok Demos it is 0,0,1,0 because that z is the up direction

		{
			m_fCurrentAngle += m_fRotAngle_LeftPlus_RightMinus;
			f_qOrient.setAxisAngle(f_vUp, m_fCurrentAngle);
			m_fRotAngle_LeftPlus_RightMinus = 0;
		}

		hkpCharacterInput f_input;
		hkpCharacterOutput f_output;
		hkpSurfaceInfo f_surfaceInfo;
		{
			f_input.m_inputLR	= m_fVelocity*m_iLeftPlus_RightMinus;
			f_input.m_inputUD	= m_fVelocity*m_iForwardMinus_BackPlus;
			f_input.m_wantJump	= m_bJUMP;

			//if you set this true character will try to climb
			f_input.m_atLadder = m_pCharacterProxyListener->m_bAtLadder;

			f_input.m_up = f_vUp;

			f_input.m_forward = hkVector4(m_vBaseCharacterForward);
			f_input.m_forward.setRotatedDir( f_qOrient, f_input.m_forward );

			f_input.m_stepInfo.m_deltaTime = f_fTimestep;
			f_input.m_stepInfo.m_invDeltaTime = 1.0f / f_fTimestep;

			f_input.m_characterGravity.set(m_fCharacterGravityScaleFactor,m_fCharacterGravityScaleFactor,m_fCharacterGravityScaleFactor);
			f_input.m_characterGravity.mul4(m_pPhysicsWorld->getGravity());

			f_input.m_velocity = m_pCharacterProxy->getLinearVelocity();
			f_input.m_position = m_pCharacterProxy->getPosition();

			hkVector4 f_vDown;
			f_vDown.setNeg4(f_input.m_up);

			m_pCharacterProxy->checkSupport(f_vDown, f_input.m_surfaceInfo);

			if (m_pCharacterProxyListener->m_bAtLadder  && ( ( f_input.m_inputUD < 0 ) || ( f_input.m_surfaceInfo.m_supportedState != hkpSurfaceInfo::SUPPORTED ) ) )
			{
				hkVector4 right, ladderUp;

				right.setCross( f_vUp, m_pCharacterProxyListener->m_vLadderNorm );
				ladderUp.setCross( m_pCharacterProxyListener->m_vLadderNorm, right );

				// Calculate the up vector for the ladder
				if (ladderUp.lengthSquared3() > HK_REAL_EPSILON)
				{
					ladderUp.normalize3();
				}

				// Reorient the forward vector so it points up along the ladder
				f_input.m_forward.addMul4( -m_pCharacterProxyListener->m_vLadderNorm.dot3(f_input.m_forward), m_pCharacterProxyListener->m_vLadderNorm);
				f_input.m_forward.add4( ladderUp );
				f_input.m_forward.normalize3();

				f_input.m_surfaceInfo.m_supportedState = hkpSurfaceInfo::UNSUPPORTED;
				f_input.m_surfaceInfo.m_surfaceNormal = m_pCharacterProxyListener->m_vLadderNorm;
				f_input.m_surfaceInfo.m_surfaceVelocity = m_pCharacterProxyListener->m_vLadderVelocity;
			}
		}
		// Apply the character state machine
		{
			HK_TIMER_BEGIN( "update character state", HK_NULL );
			m_pCharacterContext->update(f_input, f_output);
			HK_TIMER_END();
		}
		//Apply the player character controller
		{
			HK_TIMER_BEGIN( "simulate character", HK_NULL );
			// Feed output from state machine into character proxy

			//if (flycheatflag)-->we used this flag for debugging purposes and we will leave it here for next years guys incase they need
			//{
			//	hkVector4 linVel = hkVector4(1,0,1,1);
			//	linVel.mul4(f_output.m_velocity);
			//	m_pCharacterProxy->setLinearVelocity(linVel);
			//}
			//else
			{
				m_pCharacterProxy->setLinearVelocity(f_output.m_velocity);
			}

			hkStepInfo si(hkTime(0.0f),hkTime(f_fTimestep));
			m_pCharacterProxy->integrate( si, m_pPhysicsWorld->getGravity() );
			HK_TIMER_END();
		}
		// Display state
		{
			hkpCharacterStateType state = m_pCharacterContext->getState();
			m_sStatus = "";

			switch (state)
			{
			case HK_CHARACTER_ON_GROUND:
				m_sStatus.append("On Ground");
				break;
			case HK_CHARACTER_JUMPING:
				m_sStatus.append("Jumping");
				break;
			case HK_CHARACTER_IN_AIR:
				m_sStatus.append("In Air");
				break;
			case HK_CHARACTER_CLIMBING:
				m_sStatus.append("Climbing");
				break;
			default:
				m_sStatus.append("Other");
				break;
			}
		}
		// Rotate the character
		//{
		//	hkVector4 f_vOffset;
		//	f_vOffset.set(1,0,0);
		//	f_vOffset.setRotatedDir( f_qOrient , f_vOffset);
		//	hkRotation f_rotation;
		//	hkVector4& col0 = f_rotation.getColumn(0);
		//	hkVector4& col1 = f_rotation.getColumn(1);
		//	hkVector4& col2 = f_rotation.getColumn(2);
		//	hkVector4 f_vSurfaceNorm;
		//	f_vSurfaceNorm = f_surfaceInfo.m_supportedState==hkpSurfaceInfo::SUPPORTED ? f_input.m_surfaceInfo.m_surfaceNormal : f_vUp;
		//	m_vNormal.addMul4( 0.1f, f_vSurfaceNorm );
		//	m_vNormal.normalize3();
		//	col1 = m_vNormal;
		//	col2.setCross( col1, f_vOffset);
		//	col2.normalize3();
		//	col0.setCross( col1, col2 );
		//	reorientPhantom( f_rotation );
		//}
	}
	m_pPhysicsWorld->unlock();
	m_iLeftPlus_RightMinus			= 0.0f;
	m_iForwardMinus_BackPlus		= 0.0f;
	m_fRotAngle_LeftPlus_RightMinus	= 0.0f;
	m_bJUMP							= false;
}

void CharacterProxyObject::teleport(hkVector4 &checkPosition,hkVector4 &teleportPosition, int bound)
{
	hkVector4 currentPosition;
	currentPosition = getPosition();

	hkVector4Comparison a;
	int mask;
	if(bound == TELEPORT_EQUAL)
	{
		a = currentPosition.compareEqual4(checkPosition);
		mask = a.getMask();
		if(mask == 14 || mask == 15)
		{
			setPosition(teleportPosition);
		}
	}
	else if(bound == TELEPORT_GR_Z)
	{
		a = currentPosition.compareGreaterThan4(checkPosition);

		mask = a.getMask();

		if( mask == 4 || mask == 5 || mask == 8 || mask == 9 || mask == 12 || mask == 13)
		{
			setPosition(teleportPosition);
		}
	}

	else if(bound == TELEPORT_GR_Y)
	{
		a = currentPosition.compareGreaterThan4(checkPosition);

		mask = a.getMask();

		if( mask == 2 || mask == 3 || mask == 8 || mask == 9 || mask == 10 || mask == 11)
		{
			setPosition(teleportPosition);
		}
	}
	else if(bound == TELEPORT_GR_X)
	{
		a = currentPosition.compareGreaterThan4(checkPosition);

		mask = a.getMask();

		if( mask == 2 || mask == 3 || mask == 4 || mask == 5 || mask == 6 || mask == 7)
		{
			setPosition(teleportPosition);
		}
	}
	else if(bound == TELEPORT_LS_Z)
	{
		a = currentPosition.compareLessThan4(checkPosition);

		mask = a.getMask();
		if(mask == 4 || mask == 5 || mask == 8 || mask == 9 || mask == 12 || mask == 13)
		{
			setPosition(teleportPosition);
		}
	}
	else if(bound == TELEPORT_LS_Y)
	{
		a = currentPosition.compareLessThan4(checkPosition);

		mask = a.getMask();
		if(mask == 2 || mask == 3 || mask == 8 || mask == 9 || mask == 10 || mask == 11)
		{
			setPosition(teleportPosition);
		}
	}
	else if(bound == TELEPORT_LS_X)
	{
		a = currentPosition.compareLessThan4(checkPosition);

		mask = a.getMask();
		if(mask == 2 || mask == 3 || mask == 4 || mask == 5 || mask == 6 || mask == 7)
		{
			setPosition(teleportPosition);
		}
	}
}
int CharacterProxyObject::teleportTell(hkVector4& checkPosition, int bound)
{
//RDS_LOOK there should be a better way to do this masking thing
	hkVector4 currentPosition;
	currentPosition = getPosition();

	hkVector4Comparison a;
	int mask;
	if(bound == TELEPORT_EQUAL)
	{
		a = currentPosition.compareEqual4(checkPosition);
		mask = a.getMask();
		if(mask == 14 || mask == 15)
		{
			return 1;
		}
	}
	else if(bound == TELEPORT_GR_Z)
	{
		a = currentPosition.compareGreaterThan4(checkPosition);

		mask = a.getMask();

		if( mask == 4 || mask == 5 || mask == 8 || mask == 9 || mask == 12 || mask == 13)
		{
			return 1;
		}
	}

	else if(bound == TELEPORT_GR_Y)
	{
		a = currentPosition.compareGreaterThan4(checkPosition);

		mask = a.getMask();

		if( mask == 2 || mask == 3 || mask == 8 || mask == 9 || mask == 10 || mask == 11)
		{
			return 1;
		}
	}
	else if(bound == TELEPORT_GR_X)
	{
		a = currentPosition.compareGreaterThan4(checkPosition);

		mask = a.getMask();

		if( mask == 2 || mask == 3 || mask == 4 || mask == 5 || mask == 6 || mask == 7)
		{
			return 1;
		}
	}
	else if(bound == TELEPORT_LS_Z)
	{
		a = currentPosition.compareLessThan4(checkPosition);

		mask = a.getMask();
		if(mask == 4 || mask == 5 || mask == 8 || mask == 9 || mask == 12 || mask == 13)
		{
			return 1;
		}
	}
	else if(bound == TELEPORT_LS_Y)
	{
		a = currentPosition.compareLessThan4(checkPosition);

		mask = a.getMask();
		if(mask == 2 || mask == 3 || mask == 8 || mask == 9 || mask == 10 || mask == 11)
		{
			return 1;
		}
	}
	else if(bound == TELEPORT_LS_X)
	{
		a = currentPosition.compareLessThan4(checkPosition);

		mask = a.getMask();
		if(mask == 2 || mask == 3 || mask == 4 || mask == 5 || mask == 6 || mask == 7)
		{
			return 1;
		}
	}
	return 0;
}
void CharacterProxyObject::flyCheat()
{
	//flycheatflag = true;-->we used this flag for debugging purposes and we will leave it here for next years guys incase they need
	hkpGroupFilter* filter = new hkpGroupFilter();
	filter->disableCollisionsUsingBitfield(0xfffffffe, 0xfffffffe);

	filter->enableCollisionsBetween(COLLISION_LAYER_GROUND,COLLISION_LAYER_MOVING_OBJECTS);
	filter->enableCollisionsBetween(COLLISION_LAYER_GROUND,COLLISION_LAYER_STATIC_OBJECTS);
	filter->enableCollisionsBetween(COLLISION_LAYER_GROUND,COLLISION_LAYER_KEYFRAMED_OBJECTS);
	filter->enableCollisionsBetween(COLLISION_LAYER_GROUND,COLLISION_LAYER_CHARACTER_PLAYER);//different than character_rb
	filter->enableCollisionsBetween(COLLISION_LAYER_GROUND,COLLISION_LAYER_VEHICLE_OBJECT);

	filter->disableCollisionsBetween(COLLISION_LAYER_CHARACTER_PLAYER,COLLISION_LAYER_MOVING_OBJECTS);
	filter->disableCollisionsBetween(COLLISION_LAYER_CHARACTER_PLAYER,COLLISION_LAYER_STATIC_OBJECTS);
	filter->disableCollisionsBetween(COLLISION_LAYER_CHARACTER_PLAYER,COLLISION_LAYER_KEYFRAMED_OBJECTS);
	filter->disableCollisionsBetween(COLLISION_LAYER_CHARACTER_PLAYER,COLLISION_LAYER_PHANTOMS);//different than ground
	filter->disableCollisionsBetween(COLLISION_LAYER_CHARACTER_PLAYER,COLLISION_LAYER_VEHICLE_OBJECT);

	filter->disableCollisionsBetween(COLLISION_LAYER_PHANTOMS,COLLISION_LAYER_MOVING_OBJECTS);
	//filter->disableCollisionsUsingBitfield( crState,0xfffffffe);
	m_pPhysicsWorld->setCollisionFilter(filter);
	filter->removeReference();
}
void CharacterProxyObject::removeFlyCheat()
{
	//flycheatflag = false;-->we used this flag for debugging purposes and we will leave it here for next years guys incase they need
	hkpGroupFilter* filter = new hkpGroupFilter();
	//filter->disableCollisionsUsingBitfield( 0xfffffffe, 0xfffffffe );
	filter->enableCollisionsUsingBitfield(0xfffffffe,0xfffffffe);
	m_pPhysicsWorld->setCollisionFilter(filter);
	filter->removeReference();
}

hkBool CharacterProxyObject::reorientPhantom(const hkRotation& f_rotation)
{
	// Set the new orientation for the phantom
	hkTransform oldTransform = m_pPhantomShape->getTransform();
	hkTransform newTransform = oldTransform;
	newTransform.setRotation(f_rotation);
	m_pPhantomShape->setTransform( newTransform );
	// We use getClosestPoints to query our penetration
	hkpClosestCdPointCollector closestCollector;
	m_pPhysicsWorld->getClosestPoints( m_pPhantomShape->getCollidable(), *m_pPhysicsWorld->getCollisionInput(), closestCollector );
	// We allow penetration up to 10 cms.
	// You can tweak this tolerance arbitrarily
	const hkReal tolerance = 0.1f;
	if (closestCollector.hasHit() && (closestCollector.getHit().m_contact.getDistance() < -tolerance) )
	{
		m_pPhantomShape->setTransform( oldTransform );
		return false;
	}
	else
	{
		return true;
	}
}

void CharacterProxyObject::createCapsule()
{
	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	f_pTempOgreEntity->getParentSceneNode()->showBoundingBox(false); //for debug

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();
	Ogre::Vector3 f_ExactCenter = Ogre::Vector3(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f);

	//radius and length for capsule
	hkReal radius;
	hkReal capsuleLength;

	int capsuleCase = 0;
	// 0 for y-axis
	// 1 for x-axis
	// 2 for z-axis

	//calculate for different cases
	if( f_rHeight >= f_rWidth && f_rHeight >= f_rDepth )
	{
		radius = max(f_rWidth, f_rDepth)/2.0f;
		//capsuleLength = f_rHeight/2.0f;
		capsuleLength = f_rHeight - 2 * radius;
		capsuleCase = 0;
	}
	else if ( f_rWidth >= f_rHeight && f_rWidth >= f_rDepth )
	{
		radius = max(f_rHeight, f_rDepth)/2.0f;
		//capsuleLength = f_rWidth/2.0f;
		capsuleLength = f_rWidth - 2 * radius;
		capsuleCase = 1;
	}
	else
	{
		radius = max(f_rHeight, f_rWidth)/2.0f;
		//capsuleLength = f_rDepth/2.0f;
		capsuleLength = f_rDepth - 2 * radius;
		capsuleCase = 2;
	}

	//this offset value will help to orient the graphics object and the rigidbody
	//the position will be given as the bottom position all the time
	//so any displacement in y direction will be a problem if we do not have this offset
	//offset will be added in update function for graphicsObject
	m_vCharacterSceneNodeOffset = hkVector4(f_rWidth/2.0f, -yScale*(f_vEntityAabb.getMinimum().y), f_rDepth/2.0f);

	//	Create a capsule shape for character rigidbody object
	hkVector4 vertexA;
	hkVector4 vertexB;

	switch( capsuleCase )
	{
	case 0:
		vertexA.set(f_ExactCenter.x, f_ExactCenter.y - capsuleLength/2, f_ExactCenter.z);
		vertexB.set(f_ExactCenter.x, f_ExactCenter.y + capsuleLength/2, f_ExactCenter.z);
		break;
	case 1:
		vertexA.set(f_ExactCenter.x - capsuleLength/2, f_ExactCenter.y, f_ExactCenter.z);
		vertexB.set(f_ExactCenter.x + capsuleLength/2, f_ExactCenter.y, f_ExactCenter.z);
		break;
	case 2:
		vertexA.set(f_ExactCenter.x, f_ExactCenter.y, f_ExactCenter.z - capsuleLength/2);
		vertexB.set(f_ExactCenter.x, f_ExactCenter.y, f_ExactCenter.z + capsuleLength/2);
		break;
	}

	// Create a capsule to represent the character standing
	// Havok Physics->Collision Detection->Optimizing and Tuning Collision Detection->Convex Shape Radius.
	// Basically there is a shell around convex objects that keeps them a short distance apart. The reason for this is to
	// minimize the risk of objects penetrating. The convex-convex collision detection algorithm is fastest when objects are
	// not inter penetrating. It you see a graphical artifact where it looks like your graphics mesh is floating above the
	// ground by the convex radius, you need to shrink your physics representation down by the convex radius.
	m_pStandShape = new hkpCapsuleShape(vertexA, vertexB, radius);

	///The old code did this so I added this here
	///don't know why
	///TBD
	vertexA.setZero4();
	m_pHalfSizeShape = new hkpCapsuleShape(vertexA, vertexB, radius);

	//here we will try to make a bigger capsule for AI guys
	//hkVector4 vertexAForAI(-2*f_rBodyWidth,f_vCenterOfEntity.y, -2*f_rBodyWidth);
	//hkVector4 vertexBForAI(2*f_rBodyWidth,f_vCenterOfEntity.y, 2*f_rBodyWidth);
	//hkReal f_rAIRadius = max(2*f_rBodyWidth,f_vEntityAabb.getSize().y/2);//will be used as radius for AI
	//m_pAIShape = new hkpCapsuleShape(vertexA, vertexB, f_rHeight/4);//new hkpCapsuleShape(vertexAForAI,vertexBForAI,f_rAIRadius);
	m_pAIShape = new hkpCapsuleShape(vertexA, vertexB, radius);
}

/********************************************CharacterProxyObjectDefinitionsEND********************************************/
/*****************************************CharacterRigidBodyObjectDefinitionsSTART*****************************************/
CharacterRigidBodyObject::CharacterRigidBodyObject(GraphicsObject*	f_pGraphicsObject):PhysicsObject()
{
	m_pPhysicsWorld		= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();
	m_pGraphicsObject	= f_pGraphicsObject;

	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	f_pTempOgreEntity->getParentSceneNode()->showBoundingBox(false); //for debug

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	// Create a capsule to represent the character standing
	// Havok Physics->Collision Detection->Optimizing and Tuning Collision Detection->Convex Shape Radius.
	// Basically there is a shell around convex objects that keeps them a short distance apart. The reason for this is to
	// minimize the risk of objects penetrating. The convex-convex collision detection algorithm is fastest when objects are
	// not inter penetrating. It you see a graphical artifact where it looks like your graphics mesh is floating above the
	// ground by the convex radius, you need to shrink your physics representation down by the convex radius.

	// put all create process into a function
	createCapsule();
	//m_pRBBoundingShape = new hkpCapsuleShape(vertexA, vertexB, radius);

	// Construct a character rigid body
	hkpCharacterRigidBodyCinfo info;
	info.m_mass			= f_rWidth*f_rHeight*f_rDepth;
	info.m_shape		= m_pRBBoundingShape;

	info.m_maxForce		= f_rWidth*f_rHeight*f_rDepth*5;//aprroximates the strength of the rigidbody according to its volume
	info.m_up			= HavokStaticUpVector;

	info.m_position.set(f_ObjectPosition.x, 0.0f, f_ObjectPosition.z);

	info.m_maxSlope		= HK_REAL_PI/2.0f;	// Only vertical plane is too steep

	info.m_collisionFilterInfo = COLLISION_LAYER_CHARACTER_PLAYER;

	m_pCharacterRigidBody = new hkpCharacterRigidBody( info );
	hkpRigidBody* f_pTempRigidBodyOfCharacter =  m_pCharacterRigidBody->getRigidBody() ;
	m_pPhysicsWorld->addEntity(f_pTempRigidBodyOfCharacter);
	m_pRBBoundingShape->removeReference();
	//}

	// Create the character state machine and context
	{
		hkpCharacterState* state;
		hkpCharacterStateManager* manager = new hkpCharacterStateManager();

		state = new hkpCharacterStateOnGround();
		manager->registerState( state,	HK_CHARACTER_ON_GROUND);
		state->removeReference();

		state = new hkpCharacterStateInAir();
		manager->registerState( state,	HK_CHARACTER_IN_AIR);
		state->removeReference();

		state = new hkpCharacterStateJumping();
		manager->registerState( state,	HK_CHARACTER_JUMPING);
		state->removeReference();

		state = new hkpCharacterStateClimbing();
		manager->registerState( state,	HK_CHARACTER_CLIMBING);
		state->removeReference();

		m_pCharacterContext = new hkpCharacterContext(manager, HK_CHARACTER_ON_GROUND);
		manager->removeReference();

		m_pCharacterContext->setCharacterType(hkpCharacterContext::HK_CHARACTER_RIGIDBODY);
	}

	m_sStatus = "";

	m_fBaseLookingAngle = 0.0f;					// Assuming character is facing x=1
	m_fCurrentAngle		= 0.0f;					// Looking forward initially
	m_fVelocity			= 1.0f;					// Init actual time
	m_vCurrentOrientation = hkQuaternion(0,0,0,1);// Init rigid body normal
	m_iFramesInAir		= 0;

	// Character created just standing
	m_iLeftPlus_RightMinus			= 0.0f;
	m_iForwardMinus_BackPlus		= 0.0f;
	m_bJUMP							= false;
	m_fRotAngle_LeftPlus_RightMinus = 0.0f;

	// Character created looking at x = 1
	m_vBaseCharacterForward.set(1,0,0);

	// Character Affected by normal gravity
	m_fCharacterGravityScaleFactor = 1.0f;
}

CharacterRigidBodyObject::~CharacterRigidBodyObject()
{
    if (m_pCharacterRigidBody->getRigidBody())
	    m_pPhysicsWorld->removeEntity(m_pCharacterRigidBody->getRigidBody());
    if(m_pCharacterRigidBody)
	    m_pCharacterRigidBody->removeReference();
	delete(m_pCharacterContext);
}

void CharacterRigidBodyObject::destroy()
{
	this->~CharacterRigidBodyObject();
}

bool CharacterRigidBodyObject::update()
{
	hkReal f_fTimestep =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorldStep();
	this->stepObject(f_fTimestep);

	hkReal f_fStoredPosition[3];
	hkVector4 newPosition = m_vCharacterSceneNodeOffset;
	newPosition.add4(this->getPosition());
	newPosition.store3(f_fStoredPosition);
	if (m_pGraphicsObject) m_pGraphicsObject->setPosition(f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);

#ifdef TBB_PARALLEL_OPTION
	Ogre::Quaternion quat = Ogre::Quaternion(Ogre::Radian(m_fBaseLookingAngle+m_fCurrentAngle),Ogre::Vector3::UNIT_Y);
	m_pGraphicsObject->setOrientation(quat.x,quat.y,quat.z,quat.w);
#else
	m_pGraphicsObject->m_pOgreSceneNode->setOrientation(Ogre::Quaternion(Ogre::Radian(m_fBaseLookingAngle+m_fCurrentAngle),Ogre::Vector3::UNIT_Y));
#endif
	//RDS_LOOK do we ever need to kill character rigidbody object according to
	//any info from havok world??
	//if yes --> then this part needs to be fixed and
	//when that condition occurs this function needs to return false
	return true;
}

bool CharacterRigidBodyObject::scale(float width, float height, float depth){
    return scaleAndOffset(width, height, depth, 0.0f, 0.0f, 0.0f);
}

bool CharacterRigidBodyObject::scale()
{
	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

    scale(f_rWidth,f_rHeight,f_rDepth);

	return true;
}

bool CharacterRigidBodyObject::scaleAndOffset(float width,
                                              float height,
                                              float depth,
                                              float floatX,
                                              float floatY,
                                              float floatZ)
{
    m_pPhysicsWorld->lock();

    m_pPhysicsWorld->removeEntity(m_pCharacterRigidBody->getRigidBody());
    m_pCharacterRigidBody->removeReference();

    //now get the GraphicsObject component
    Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
    Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
    Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
    hkVector4 f_vPrevCharacterSceneNodeOffset = m_vCharacterSceneNodeOffset;

    //get the bounding box fromOgre to set the capsule shape
    Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();

    // Get actual body height,width and depth
    hkReal f_rWidth		= width;
    hkReal f_rHeight	= height;
    hkReal f_rDepth		= depth;

    Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();
    Ogre::Vector3 f_ExactCenter = Ogre::Vector3(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f);
    hkReal radius = max(f_rWidth, f_rDepth)/2.0f;
    hkReal capsuleHeight = f_rHeight/2.0f;

    //if the radius of the object is bigger than the height/2 it results in the objects hovering position
    //not to have the object as it hovers
    //we have to clamp the radius to height/2
    if (radius>capsuleHeight)
    {
        //this is a special case for stormtrooper
        radius = f_rHeight/4.0f;
        capsuleHeight = f_rHeight/4.0f;//0.0f;
        m_vCharacterSceneNodeOffset = hkVector4(f_rWidth/2.0f + floatX,f_rHeight/2.0f + floatY,f_rDepth/2.0f + floatZ);
    }
    else
    {
        //this offset value will help to orient the graphics object and the rigidbody
        //the position will be given as the bottom position all the time
        //so any displacement in y direction will be a problem if we do not have this offset
        //offset will be added in update function for graphicsObject
        m_vCharacterSceneNodeOffset = hkVector4(f_rWidth/2.0f + floatX,-radius + floatY,f_rDepth/2.0f + floatZ);
    }

    //	Create a capsule shape for character rigidbody object
    {
        hkVector4 vertexA(f_ExactCenter.x, f_ExactCenter.y - capsuleHeight, f_ExactCenter.z);
        hkVector4 vertexB(f_ExactCenter.x, f_ExactCenter.y + capsuleHeight, f_ExactCenter.z);

        // Create a capsule to represent the character standing
        // Havok Physics->Collision Detection->Optimizing and Tuning Collision Detection->Convex Shape Radius.
        // Basically there is a shell around convex objects that keeps them a short distance apart. The reason for this is to
        // minimize the risk of objects penetrating. The convex-convex collision detection algorithm is fastest when objects are
        // not inter penetrating. It you see a graphical artifact where it looks like your graphics mesh is floating above the
        // ground by the convex radius, you need to shrink your physics representation down by the convex radius.
        m_pRBBoundingShape = new hkpCapsuleShape(vertexA, vertexB, radius);

        // Construct a character rigid body
        hkpCharacterRigidBodyCinfo info;
        info.m_mass			= f_rWidth*f_rHeight*f_rDepth;
        info.m_shape		= m_pRBBoundingShape;

        info.m_maxForce		= f_rWidth*f_rHeight*f_rDepth*5;//aprroximates the strength of the rigidbody according to its volume
        info.m_up			= HavokStaticUpVector;

        info.m_position.setNeg4(f_vPrevCharacterSceneNodeOffset);
        info.m_position.add4(hkVector4(f_ObjectPosition.x+floatX, floatY, f_ObjectPosition.z+floatZ));

        info.m_maxSlope		= HK_REAL_PI/2.0f;	// Only vertical plane is too steep

        info.m_collisionFilterInfo = COLLISION_LAYER_CHARACTER_PLAYER;

        m_pCharacterRigidBody = new hkpCharacterRigidBody( info );
        hkpRigidBody* f_pTempRigidBodyOfCharacter =  m_pCharacterRigidBody->getRigidBody() ;
        m_pPhysicsWorld->addEntity(f_pTempRigidBodyOfCharacter);
        m_pRBBoundingShape->removeReference();
    }
    m_pPhysicsWorld->unlock();
    return true;
}

void CharacterRigidBodyObject::stepObject(hkReal f_fTimestep)
{
	m_fCurrentAngle += m_fRotAngle_LeftPlus_RightMinus;
	m_vCurrentOrientation.setAxisAngle(HavokStaticUpVector, m_fCurrentAngle);
	//
	// Detect ladder
	//
	hkBool atLadder = false;
	// Initialize these so gcc doesn't complain
	hkVector4 ladderNorm; ladderNorm.setZero4();
	hkVector4 ladderVelocity; ladderVelocity.setZero4();

	// Process all collisions points to see if there is a collision at a ladder. This could be done in a callback, however
	// it is done this way here to ensure compatibility with SPU simulation.
	hkpLinkedCollidable* f_pLinkedCollidable = m_pCharacterRigidBody->getRigidBody()->getLinkedCollidable();
	hkArray<struct hkpLinkedCollidable::CollisionEntry> collisionEntriesTmp;
	f_pLinkedCollidable->getCollisionEntriesSorted(collisionEntriesTmp);
	const hkArray<struct hkpLinkedCollidable::CollisionEntry>& collisionEntries = collisionEntriesTmp;

	for ( int i = 0; i < collisionEntries.getSize(); ++i )
	{
		hkpRigidBody* rb = hkpGetRigidBody( collisionEntries[i].m_partner );
		if ( rb != HK_NULL && rb->getMotionType()==hkpMotion::MOTION_FIXED )
		{
			if ( collisionEntries[i].m_agentEntry->m_contactMgr->m_type == hkpContactMgr::TYPE_SIMPLE_CONSTRAINT_CONTACT_MGR )
			{
				hkpSimpleConstraintContactMgr* mgr = (hkpSimpleConstraintContactMgr*)(collisionEntries[i].m_agentEntry->m_contactMgr);
				if (mgr->m_contactConstraintData.getNumContactPoints() > 0)
				{
					atLadder = true;
					hkContactPoint* contactPoints = mgr->m_contactConstraintData.m_atom->getContactPoints();
					ladderNorm = contactPoints[0].getNormal();
					rb->getPointVelocity( contactPoints[0].getPosition(), ladderVelocity );
					break;
				}
			}
		}
	}

	HK_TIMER_BEGIN( "set character state", HK_NULL );

	hkpCharacterInput input;
	hkpCharacterOutput output;
	{
		input.m_inputLR = m_fVelocity*m_iLeftPlus_RightMinus;
		input.m_inputUD = m_fVelocity*m_iForwardMinus_BackPlus;

		input.m_wantJump = m_bJUMP;
		input.m_atLadder = atLadder;

		input.m_up = HavokStaticUpVector;
		input.m_forward.set(1,0,0);
		input.m_forward.setRotatedDir( m_vCurrentOrientation, input.m_forward );

		input.m_stepInfo.m_deltaTime = f_fTimestep;
		input.m_stepInfo.m_invDeltaTime = 1.0f/f_fTimestep;

		input.m_characterGravity.set(0,0,0);
		input.m_characterGravity.add4(m_pPhysicsWorld->getGravity());
		input.m_characterGravity.add4(m_pPhysicsWorld->getGravity());//not a typo. I want the character to fall down faster.

		input.m_velocity = m_pCharacterRigidBody->getRigidBody()->getLinearVelocity();
		input.m_position = m_pCharacterRigidBody->getRigidBody()->getPosition();

		m_pCharacterRigidBody->checkSupport(input.m_stepInfo, input.m_surfaceInfo);

		// Only climb the ladder when the character is either unsupported or wants to go up.
		if ( atLadder && ( ( input.m_inputUD < 0 ) || ( input.m_surfaceInfo.m_supportedState != hkpSurfaceInfo::SUPPORTED ) ) )
		{
			hkVector4 right, ladderUp;
			right.setCross( HavokStaticUpVector, ladderNorm );
			ladderUp.setCross( ladderNorm, right );
			// Calculate the up vector for the ladder
			if (ladderUp.lengthSquared3() > HK_REAL_EPSILON)
			{
				ladderUp.normalize3();
			}

			// Reorient the forward vector so it points up along the ladder
			input.m_forward.addMul4( -ladderNorm.dot3(input.m_forward), ladderNorm);
			input.m_forward.add4( ladderUp );
			input.m_forward.normalize3();

			input.m_surfaceInfo.m_supportedState = hkpSurfaceInfo::UNSUPPORTED;
			input.m_surfaceInfo.m_surfaceNormal = ladderNorm;
			input.m_surfaceInfo.m_surfaceVelocity = ladderVelocity;

			HK_SET_OBJECT_COLOR( (hkUlong) m_pCharacterRigidBody->getRigidBody()->getCollidable(), hkColor::rgbFromChars( 255, 255, 0, 100 ) );
		}
		else
		{
			// Change character rigid body color according to its state
			if( input.m_surfaceInfo.m_supportedState == hkpSurfaceInfo::SUPPORTED )
			{
				HK_SET_OBJECT_COLOR( (hkUlong) m_pCharacterRigidBody->getRigidBody()->getCollidable(), hkColor::rgbFromChars( 0, 255, 0, 100 ) );
			}
			else
			{
				HK_SET_OBJECT_COLOR( (hkUlong) m_pCharacterRigidBody->getRigidBody()->getCollidable(), hkColor::BLUE );
			}
		}
		HK_TIMER_END();
	}

	// Apply the character state machine
	{
		HK_TIMER_BEGIN( "update character state", HK_NULL );

		m_pCharacterContext->update( input, output );

		HK_TIMER_END();
	}

	//Apply the player character controller
	{
		HK_TIMER_BEGIN( "simulate character", HK_NULL );

		// Set output velocity from state machine into character rigid body
		m_pCharacterRigidBody->setLinearVelocity(output.m_velocity, f_fTimestep);

		HK_TIMER_END();
	}

	// Display state
	{
		hkpCharacterStateType state = m_pCharacterContext->getState();
		m_sStatus = "";

		switch (state)
		{
		case HK_CHARACTER_ON_GROUND:
			m_sStatus.append("On Ground");
			break;
		case HK_CHARACTER_JUMPING:
			m_sStatus.append("Jumping");
			break;
		case HK_CHARACTER_IN_AIR:
			m_sStatus.append("In Air");
			break;
		case HK_CHARACTER_CLIMBING:
			m_sStatus.append("Climbing");
			break;
		default:
			m_sStatus.append("Other");
			break;
		}
	}
	m_iLeftPlus_RightMinus			= 0.0f;
	m_iForwardMinus_BackPlus		= 0.0f;
	m_fRotAngle_LeftPlus_RightMinus	= 0.0f;
	m_bJUMP							= false;
}

void CharacterRigidBodyObject::setVelocity(float f_fNewVelocity)
{
	if (f_fNewVelocity<0)
		return;
	if (f_fNewVelocity>5)//RDS_HARDCODED-->I just didn't want to set a velocity of a character more than sth.What is a better way to put threshold??
		f_fNewVelocity=5;
	m_fVelocity = f_fNewVelocity;
}

void CharacterRigidBodyObject::setInputs(hkReal f_iLeftPlus_RightMinus, hkReal f_iForwardMinus_BackPlus, bool f_bJump,hkReal f_fRotAngle_LeftPlus_RightMinus, bool assignRotation)
{
	Ogre::Vector3 f_vectorNormalizer(f_iLeftPlus_RightMinus,0,f_iForwardMinus_BackPlus);
	f_vectorNormalizer.normalise();

	m_iLeftPlus_RightMinus			= f_vectorNormalizer.x;
	m_iForwardMinus_BackPlus		= f_vectorNormalizer.z;
	m_bJUMP							= f_bJump;
    m_fRotAngle_LeftPlus_RightMinus = (assignRotation) ? f_fRotAngle_LeftPlus_RightMinus : m_fRotAngle_LeftPlus_RightMinus + f_fRotAngle_LeftPlus_RightMinus;
    if (m_fCurrentAngle){
        m_fCurrentAngle = 0.0f;
    }
}
hkBool CharacterRigidBodyObject::reorientCharacter(const hkRotation& f_rRotation,hkReal f_fTimestep)
{
	// Calculate desired rotation velocity to follow expected orientation
	//
	// angularVel = gain*(desiredOrient - currentOrient)/timestep
	//
	const hkReal gain = 0.5f;
	const hkQuaternion& currentOrient = m_pCharacterRigidBody->getRigidBody()->getRotation();

	hkQuaternion desiredOrient;
	desiredOrient.set(f_rRotation);

	hkVector4 angle;
	currentOrient.estimateAngleTo(desiredOrient,angle);

	hkVector4 angularVelocity;
	angularVelocity.setMul4(gain/f_fTimestep,angle);

	m_pCharacterRigidBody->setAngularVelocity(angularVelocity);

	return true;
}

string CharacterRigidBodyObject::getCharacterStatus()
{
	return m_sStatus;
}

void CharacterRigidBodyObject::createCapsule()
{
	//now get the GraphicsObject component
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	f_pTempOgreEntity->getParentSceneNode()->showBoundingBox(false); //for debug

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();
	Ogre::Vector3 f_ExactCenter = Ogre::Vector3(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f);

	//radius and length for capsule
	hkReal radius;
	hkReal capsuleLength;

	int capsuleCase = 0;
	// 0 for y-axis
	// 1 for x-axis
	// 2 for z-axis

	//calculate for different cases
	if( f_rHeight >= f_rWidth && f_rHeight >= f_rDepth )
	{
		radius = max(f_rWidth, f_rDepth)/2.0f;
		//capsuleLength = f_rHeight/2.0f;
		capsuleLength = f_rHeight - 2 * radius;
		capsuleCase = 0;
	}
	else if ( f_rWidth >= f_rHeight && f_rWidth >= f_rDepth )
	{
		radius = max(f_rHeight, f_rDepth)/2.0f;
		//capsuleLength = f_rWidth/2.0f;
		capsuleLength = f_rWidth - 2 * radius;
		capsuleCase = 1;
	}
	else
	{
		radius = max(f_rHeight, f_rWidth)/2.0f;
		//capsuleLength = f_rDepth/2.0f;
		capsuleLength = f_rDepth - 2 * radius;
		capsuleCase = 2;
	}

	//This is some internal thing in the capsule?
	m_vCharacterSceneNodeOffset = hkVector4(f_rWidth/2.0f, -yScale*(f_vEntityAabb.getMinimum().y), f_rDepth/2.0f);

	//	Create a capsule shape for character rigidbody object
	hkVector4 vertexA;
	hkVector4 vertexB;

	switch( capsuleCase )
	{
	case 0:
		vertexA.set(f_ExactCenter.x, f_ExactCenter.y - capsuleLength/2, f_ExactCenter.z);
		vertexB.set(f_ExactCenter.x, f_ExactCenter.y + capsuleLength/2, f_ExactCenter.z);
		break;
	case 1:
		vertexA.set(f_ExactCenter.x - capsuleLength/2, f_ExactCenter.y, f_ExactCenter.z);
		vertexB.set(f_ExactCenter.x + capsuleLength/2, f_ExactCenter.y, f_ExactCenter.z);
		break;
	case 2:
		vertexA.set(f_ExactCenter.x, f_ExactCenter.y, f_ExactCenter.z - capsuleLength/2);
		vertexB.set(f_ExactCenter.x, f_ExactCenter.y, f_ExactCenter.z + capsuleLength/2);
		break;
	}

	///Create capsule
	m_pRBBoundingShape = new hkpCapsuleShape(vertexA, vertexB, radius);
}

/********************************************CharacterRigidBodyObjectDefinitionsEND********************************************/
/**********************************************PhantomShapeObjectDefinitionsSTART**********************************************/
PhantomShapeObject::PhantomShapeObject(
										string				f_sOgreUniqueName,
										CollisionShapeType		f_eCollisionShapeType,
										GraphicsObject**		f_pGraphicsObject
									   ):PhysicsObject()
{
	m_pPhysicsWorld		= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();
	*f_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName,f_eCollisionShapeType));
	m_pGraphicsObject = *f_pGraphicsObject;
	m_eCollisionShapeType = f_eCollisionShapeType;

	Ogre::Entity* f_pTempOgreEntity =  m_pGraphicsObject->m_pOgreEntity;
	f_pTempOgreEntity->setVisible(false);
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	hkpShape* f_pPhantomShape;
	hkReal sphereRadius;
	switch (f_eCollisionShapeType)
	{
		case COLLISION_SHAPE_SPHERE :
			sphereRadius = max(f_rWidth,f_rHeight);
			sphereRadius = max(sphereRadius,f_rDepth);
			sphereRadius /= 2.0f;
			f_pPhantomShape = new hkpSphereShape(sphereRadius);
			break;
		case COLLISION_SHAPE_BOX :
			f_pPhantomShape = new hkpBoxShape(hkVector4(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f));
			break;
	}
	{
		m_fCustom_phantomEnterEvent_Function = NULL;
		m_fCustom_phantomLeaveEvent_Function = NULL;
		m_pMyPhantomCallbackShape = new MyPhantomCallbackShape(this);
		hkpShape* f_pBvShape = new hkpBvShape( f_pPhantomShape, m_pMyPhantomCallbackShape );

		// Construct a Shape Phantom
		m_pPhantom = new hkpSimpleShapePhantom( f_pBvShape, hkTransform::getIdentity(), COLLISION_LAYER_PHANTOMS);
		m_pPhantom->setPosition(hkVector4(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z));

		hkpRigidBodyCinfo f_rigidBodyCinfo;
		f_rigidBodyCinfo.m_motionType = hkpMotion::MOTION_FIXED;
		f_rigidBodyCinfo.m_position.set(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z);
		f_rigidBodyCinfo.m_collisionFilterInfo = COLLISION_LAYER_PHANTOMS;
		f_rigidBodyCinfo.m_shape = f_pBvShape;

		m_pRigidBodyObject = new hkpRigidBody( f_rigidBodyCinfo );
		setCollisionLayerInfo(m_pRigidBodyObject,PHYSICS_FIXED);
		m_pPhysicsWorld->addEntity(m_pRigidBodyObject);//add the rigidBody to the havok world
		m_pRigidBodyObject->removeReference();

		// Add the phantom to the world
		m_pPhysicsWorld->addPhantom(m_pPhantom);
		m_pPhantom->removeReference();

		//clear unneeded references
		f_pPhantomShape->removeReference();

		f_pBvShape->removeReference();
	}
}
PhantomShapeObject::~PhantomShapeObject()
{
	m_pPhysicsWorld->removeEntity(m_pRigidBodyObject);
	m_pPhysicsWorld->removePhantom(m_pPhantom);
	m_pMyPhantomCallbackShape->removeReference();
}
void PhantomShapeObject::destroy()
{
	this->~PhantomShapeObject();
}
bool PhantomShapeObject::update()
{
	//hkReal f_fTimestep =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorldStep();
	//RDS_LOOK do we ever need to kill GameObject according to
	//any info from havok world??
	//if yes --> then this part needs to be fixed and
	//when that condition occurs this function needs to return false
	return true;
}

bool PhantomShapeObject::scale()
{
	m_pPhysicsWorld->lock();

	m_pPhysicsWorld->removeEntity(m_pRigidBodyObject);
	m_pPhysicsWorld->removePhantom(m_pPhantom);
	m_pMyPhantomCallbackShape->removeReference();

	Ogre::Entity* f_pTempOgreEntity =  m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	// Get actual body height,width and depth
	hkReal f_rWidth		= xScale*f_vEntityAabb.getSize().x;
	hkReal f_rHeight	= yScale*f_vEntityAabb.getSize().y;
	hkReal f_rDepth		= zScale*f_vEntityAabb.getSize().z;

	hkpShape* f_pPhantomShape;
	hkReal sphereRadius;
	switch (m_eCollisionShapeType)
	{
	case COLLISION_SHAPE_SPHERE :
		sphereRadius = max(f_rWidth,f_rHeight);
		sphereRadius = max(sphereRadius,f_rDepth);
		sphereRadius /= 2.0f;
		f_pPhantomShape = new hkpSphereShape(sphereRadius);
		break;
	case COLLISION_SHAPE_BOX :
		f_pPhantomShape = new hkpBoxShape(hkVector4(f_rWidth/2.0f,f_rHeight/2.0f,f_rDepth/2.0f));
		break;
	}
	{
		m_pMyPhantomCallbackShape = new MyPhantomCallbackShape(this);
		m_pMyPhantomCallbackShape->setCustom_phantomEnterEvent_Function((void *)m_fCustom_phantomEnterEvent_Function);
		m_pMyPhantomCallbackShape->setCustom_phantomLeaveEvent_Function((void *)m_fCustom_phantomLeaveEvent_Function);

		hkpShape* f_pBvShape = new hkpBvShape( f_pPhantomShape, m_pMyPhantomCallbackShape );

		// Construct a Shape Phantom
		m_pPhantom = new hkpSimpleShapePhantom( f_pBvShape, hkTransform::getIdentity(), COLLISION_LAYER_PHANTOMS);
		m_pPhantom->setPosition(hkVector4(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z));

		hkpRigidBodyCinfo f_rigidBodyCinfo;
		f_rigidBodyCinfo.m_motionType = hkpMotion::MOTION_FIXED;
		f_rigidBodyCinfo.m_position.set(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z);
		f_rigidBodyCinfo.m_collisionFilterInfo = COLLISION_LAYER_PHANTOMS;
		f_rigidBodyCinfo.m_shape = f_pBvShape;

		m_pRigidBodyObject = new hkpRigidBody( f_rigidBodyCinfo );
		setCollisionLayerInfo(m_pRigidBodyObject,PHYSICS_FIXED);
		m_pPhysicsWorld->addEntity(m_pRigidBodyObject);//add the rigidBody to the havok world
		m_pRigidBodyObject->removeReference();

		// Add the phantom to the world
		m_pPhysicsWorld->addPhantom(m_pPhantom);
		m_pPhantom->removeReference();

		//clear unneeded references
		f_pPhantomShape->removeReference();

		f_pBvShape->removeReference();
	}

	m_pPhysicsWorld->unlock();
	return true;
}
void PhantomShapeObject::setPosition(hkVector4& f_rPosition)
{
	m_pPhysicsWorld->lock();
	m_pGraphicsObject->m_pOgreSceneNode->setPosition(f_rPosition(0),f_rPosition(1),f_rPosition(2));
	m_pPhantom->setPosition(f_rPosition);
	m_pRigidBodyObject->setPosition(f_rPosition);
	m_pPhysicsWorld->unlock();
}
hkVector4 PhantomShapeObject::getPosition()
{
	return m_pRigidBodyObject->getPosition();
}
void PhantomShapeObject::setCustom_phantomEnterEvent_Function(void* f_fNew_Custom_phantomEnterEvent_Function)
{
	m_fCustom_phantomEnterEvent_Function = (Custom_PhantomShape_Collision_Callback_Function)f_fNew_Custom_phantomEnterEvent_Function;
	m_pMyPhantomCallbackShape->setCustom_phantomEnterEvent_Function(f_fNew_Custom_phantomEnterEvent_Function);
}

void PhantomShapeObject::setCustom_phantomLeaveEvent_Function(void* f_fNew_Custom_phantomLeaveEvent_Function)
{
	m_fCustom_phantomLeaveEvent_Function = (Custom_PhantomShape_Collision_Callback_Function)f_fNew_Custom_phantomLeaveEvent_Function;
	m_pMyPhantomCallbackShape->setCustom_phantomLeaveEvent_Function(f_fNew_Custom_phantomLeaveEvent_Function);
}

/***********************************************PhantomShapeObjectDefinitionsEND**********************************************/
/**********************************************PhantomAabbObjectDefinitionsSTART**********************************************/
PhantomAabbObject::PhantomAabbObject(GraphicsObject*	f_pGraphicsObject) : PhysicsObject()
{
	m_fCustom_Callback_Function = NULL;
	m_pGraphicsObject	= f_pGraphicsObject;
	m_pPhysicsWorld		= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();

	//now get the GraphicsObject component and make it invisible
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	f_pTempOgreEntity->setVisible(false);
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	{
		Ogre::Vector3 minAabbVec3 = f_vEntityAabb.getMinimum();
		Ogre::Vector3 maxAabbVec3 = f_vEntityAabb.getMaximum();
		hkAabb infoAABB;

		infoAABB.m_min.set(f_ObjectPosition.x + minAabbVec3.x,f_ObjectPosition.y + minAabbVec3.y,f_ObjectPosition.z + minAabbVec3.z);
		infoAABB.m_max.set(f_ObjectPosition.x + maxAabbVec3.x,f_ObjectPosition.y + maxAabbVec3.y,f_ObjectPosition.z + maxAabbVec3.z);

		m_pAabbPhantom = new hkMovingRBCollectorPhantom(infoAABB,COLLISION_LAYER_PHANTOMS);
		//m_pCastCollector = new MyHkpCDPointCollector();
		//m_pStartCollector = new MyHkpCDPointCollector();
		m_pPhysicsWorld->addPhantom(m_pAabbPhantom);
	}
	m_fTime = 0;
}
PhantomAabbObject::~PhantomAabbObject(){
	m_pPhysicsWorld->removePhantom(m_pAabbPhantom);
}
void PhantomAabbObject::destroy()
{
	this->~PhantomAabbObject();
}
bool PhantomAabbObject::update()
{
	hkReal f_fTimestep =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorldStep();
	this->stepObject(f_fTimestep);
	return true;
}

bool PhantomAabbObject::scale()
{
	m_pPhysicsWorld->lock();
	//first remove the previous one from world
	m_pPhysicsWorld->removePhantom(m_pAabbPhantom);

	//now get the GraphicsObject component and make it invisible
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	{
		Ogre::Vector3 minAabbVec3 = f_vEntityAabb.getMinimum();
		Ogre::Vector3 maxAabbVec3 = f_vEntityAabb.getMaximum();
		hkAabb infoAABB;

		infoAABB.m_min.set(f_ObjectPosition.x + xScale*minAabbVec3.x,f_ObjectPosition.y + yScale*minAabbVec3.y,f_ObjectPosition.z + zScale*minAabbVec3.z);
		infoAABB.m_max.set(f_ObjectPosition.x + xScale*maxAabbVec3.x,f_ObjectPosition.y + yScale*maxAabbVec3.y,f_ObjectPosition.z + zScale*maxAabbVec3.z);

		m_pAabbPhantom = new hkMovingRBCollectorPhantom(infoAABB,COLLISION_LAYER_PHANTOMS);
		m_pPhysicsWorld->addPhantom(m_pAabbPhantom);
	}
	m_pPhysicsWorld->unlock();
	return true;
}
void PhantomAabbObject::stepObject(hkReal f_fTimeStep)
{
	m_fTime += f_fTimeStep;
	m_pAabbPhantom->ensureDeterministicOrder();
	if ((m_fCustom_Callback_Function!=NULL)&&(m_pAabbPhantom->getOverlappingCollidables().getSize()>0))
		m_fCustom_Callback_Function(m_pAabbPhantom,f_fTimeStep,m_fTime);
}
bool PhantomAabbObject::linearCast()
{
	//not functional right now
	//TBD
	hkpLinearCastInput f_pLinearCastInput;
	f_pLinearCastInput.m_maxExtraPenetration = 0.2f;
	f_pLinearCastInput.m_startPointTolerance = 0.2f;
	f_pLinearCastInput.m_to.set(10.0f,0.0f,0.0f);
	f_pLinearCastInput.m_to.add4(this->getPosition());

	//m_pAabbPhantom->linearCast(m_pAabbPhantom->getCollidable(),f_pLinearCastInput,(hkpCdPointCollector &)(*m_pCastCollector),(hkpCdPointCollector *)m_pStartCollector);

	return true;
}
void PhantomAabbObject::setCustom_PhantomAabb_Collision_Callback_Function(void* f_fNew_Custom_Callback_Function)
{
	m_fCustom_Callback_Function = (Custom_PhantomAabb_Collision_Callback_Function)f_fNew_Custom_Callback_Function;
}

void PhantomAabbObject::setPosition(hkVector4& f_rPosition)
{
	m_pPhysicsWorld->lock();
	//first remove the previous one from world
	m_pPhysicsWorld->removePhantom(m_pAabbPhantom);

	//now get the GraphicsObject component and make it invisible
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	//get the bounding box fromOgre to set the capsule shape
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;

	hkReal xScale = f_pTempOgreSceneNode->getScale().x;
	hkReal yScale = f_pTempOgreSceneNode->getScale().y;
	hkReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	//get the bounding box fromOgre to set the capsule shape
	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	{
		Ogre::Vector3 minAabbVec3 = f_vEntityAabb.getMinimum();
		Ogre::Vector3 maxAabbVec3 = f_vEntityAabb.getMaximum();
		hkAabb infoAABB;

		infoAABB.m_min.set(f_rPosition(0) + xScale*minAabbVec3.x,f_rPosition(1) + yScale*minAabbVec3.y,f_rPosition(2) + zScale*minAabbVec3.z);
		infoAABB.m_max.set(f_rPosition(0) + xScale*maxAabbVec3.x,f_rPosition(1) + yScale*maxAabbVec3.y,f_rPosition(2) + zScale*maxAabbVec3.z);

		m_pAabbPhantom = new hkMovingRBCollectorPhantom(infoAABB,COLLISION_LAYER_PHANTOMS);
		m_pPhysicsWorld->addPhantom(m_pAabbPhantom);
	}
	m_pPhysicsWorld->unlock();
	//because that we are not updating the graphics object position at every update
	//we should do it over here
	m_pGraphicsObject->m_pOgreSceneNode->setPosition(f_rPosition(0),f_rPosition(1),f_rPosition(2));
}

hkVector4 PhantomAabbObject::getPosition()
{
	return hkVector4(m_pGraphicsObject->m_pOgreSceneNode->getPosition().x,m_pGraphicsObject->m_pOgreSceneNode->getPosition().y,m_pGraphicsObject->m_pOgreSceneNode->getPosition().z);
}
/***********************************************PhantomAabbObjectDefinitionsEND*********************************************/
VehicleObject::VehicleObject(
								 GameObjectType		f_eHavokObjectType,		///to see if it is single or multiple rigidbody constructor
								 GraphicsObject*	f_pGraphicsObject		//for setting the GraphicsObject pointer of a gameObject
							 )
{
	m_pChassisRigidBody = NULL;
	GameObjectManager *f_pGameObjectManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager();
	m_pGraphicsObject = f_pGraphicsObject;

	m_numVehicles = 1;
	leftflag = 0;
	rightflag = 0;
	accelerateflag = 0;
	reverseflag = 0;

	//default values for car dimensions
	vehicleDimensions[0]=1.75f;
	vehicleDimensions[1]=0.25f;
	vehicleDimensions[2]=1.1f;
	vehicleDimensions[3]=1.9f;
	vehicleDimensions[4]=0.15f;
	vehicleDimensions[5]=1.0f;
	vehicleDimensions[6]=0.4f;
	vehicleDimensions[7]=-1.0f;
	vehicleDimensions[8]=0.7f;
	vehicleDimensions[9]=0.7f;
	vehicleDimensions[10]=0.4f;
	vehicleDimensions[11]=-1.0f;
	vehicleDimensions[12]=0.25f;
	vehicleDimensions[13]=1.2f;

	if(f_eHavokObjectType == VEHICLE_CAR)
	{
		m_numWheels = 4;
		setupVehicles();
	}
	else if(f_eHavokObjectType == VEHICLE_MOTORCYCLE)
	{
		m_numWheels = 2;
		setupMcycle();
	}
	else
	{
		GGETRACELOG("Error in VehicleObject Constructor Function\n\
					Could not create VehicleObject. UnKnownVehicle type = %d\n",f_eHavokObjectType);
		exit(1);
	}
}

///Destructor which removes the VehicleInstance
VehicleObject::~VehicleObject()
{
	if (m_pChassisRigidBody->isAddedToWorld())
	{
		m_pPhysicsWorld->removeAction(m_pVehicleInstance);
	}
	if(m_numWheels == 2)
	{
		m_genericConstraint->removeReference();
		m_constraint->removeReference();
	}
}
void VehicleObject::destroy()
{
	this->~VehicleObject();
}
///This creates a new VehicleInstance for the car
void VehicleObject::createVehicle()
{
	GameObjectManager *f_pGameObjectManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager();
	// Create the basic vehicle.
	{
		m_pPhysicsWorld = f_pGameObjectManager->GetWorld();
		m_pVehicleInstance = new hkpVehicleInstance( m_pChassisRigidBody );

		buildVehicle();
	}

	// Add the vehicle's entities and phantoms to the world
	{
		m_pVehicleInstance->addToWorld( m_pPhysicsWorld );
	}

	// The vehicle is an action
	{
		m_pPhysicsWorld->addAction(m_pVehicleInstance);
	}
}

///This creates a new VehicleInstance for the motorcycle
void VehicleObject::createMcycle()
{
	GameObjectManager *f_pGameObjectManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager();
	// Create the basic vehicle.
	{
		m_pPhysicsWorld = f_pGameObjectManager->GetWorld();
		m_pVehicleInstance = new hkpVehicleInstance( m_pChassisRigidBody );

		buildMcycle();
	}

	// Add the vehicle's entities and phantoms to the world
	{
		m_pVehicleInstance->addToWorld( m_pPhysicsWorld );
	}

	// The vehicle is an action
	{
		m_pPhysicsWorld->addAction(m_pVehicleInstance);
	}
}

///This function populates the VehicleInstance with default values of the car
void VehicleObject::buildVehicle()
{
	//this is the code for build vehicle
	m_pVehicleInstance->m_data = new hkpVehicleData;
	m_pVehicleInstance->m_engine = new hkpVehicleDefaultEngine;
	m_pVehicleInstance->m_transmission = new hkpVehicleDefaultTransmission;
	m_pVehicleInstance->m_driverInput = new hkpVehicleDefaultAnalogDriverInput;
	m_pVehicleInstance->m_steering = new hkpVehicleDefaultSteering;
	m_pVehicleInstance->m_brake = new hkpVehicleDefaultBrake;
	m_pVehicleInstance->m_suspension = new hkpVehicleDefaultSuspension;
	m_pVehicleInstance->m_aerodynamics = new hkpVehicleDefaultAerodynamics;
	m_pVehicleInstance->m_velocityDamper = new hkpVehicleDefaultVelocityDamper;

	// For illustrative purposes we use a custom hkpVehicleRayCastWheelCollide
	// which implements varying 'ground' friction in a very simple way.
	m_pVehicleInstance->m_wheelCollide		= new hkpVehicleRayCastWheelCollide;

	setupVehicleData(*m_pVehicleInstance->m_data);

	m_pVehicleInstance->m_tyreMarks = new hkpTyremarksInfo( *m_pVehicleInstance->m_data, 128 );

	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultAnalogDriverInput* >(m_pVehicleInstance->m_driverInput) );
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultSteering*>(m_pVehicleInstance->m_steering));
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultEngine*>(m_pVehicleInstance->m_engine) );
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultTransmission*>(m_pVehicleInstance->m_transmission) );
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultBrake*>(m_pVehicleInstance->m_brake) );
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultSuspension*>(m_pVehicleInstance->m_suspension) );
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultAerodynamics*>(m_pVehicleInstance->m_aerodynamics) );
	setupComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultVelocityDamper*>(m_pVehicleInstance->m_velocityDamper) );

	//setupWheelCollide( m_pPhysicsWorld, *m_pVehicleInstance, *static_cast< hkpVehicleRayCastWheelCollide*>(m_pVehicleInstance.m_wheelCollide) );
	setupTyremarks( *m_pVehicleInstance->m_data, *static_cast< hkpTyremarksInfo*>(m_pVehicleInstance->m_tyreMarks) );

	// Check that all components are present.

	HK_ASSERT(0x0, m_pVehicleInstance->m_data );
	HK_ASSERT(0x7708674a,  m_pVehicleInstance->m_driverInput );
	HK_ASSERT(0x5a324a2d,  m_pVehicleInstance->m_steering );
	HK_ASSERT(0x7bcb2aff,  m_pVehicleInstance->m_engine );
	HK_ASSERT(0x29bddb50,  m_pVehicleInstance->m_transmission );
	HK_ASSERT(0x2b0323a2,  m_pVehicleInstance->m_brake );
	HK_ASSERT(0x7a7ade23,  m_pVehicleInstance->m_suspension );
	HK_ASSERT(0x6ec4d0ed,  m_pVehicleInstance->m_aerodynamics );
	//HK_ASSERT(0x67161206,  m_pVehicleInstance.m_wheelCollide );
	HK_ASSERT(0x295015f1,  m_pVehicleInstance->m_tyreMarks );

	m_pVehicleInstance->m_deviceStatus = new hkpVehicleDriverInputAnalogStatus;
	deviceStatus = (hkpVehicleDriverInputAnalogStatus*)m_pVehicleInstance->m_deviceStatus;
	//deviceStatus->m_positionY = -0.4f;
	//deviceStatus->m_positionY = 0.1f;
	// Turn.
	//deviceStatus->m_positionX = 0.3f;
	//deviceStatus->m_positionX = -0.3f;

	// Defaults
	deviceStatus->m_handbrakeButtonPressed = false;
	deviceStatus->m_reverseButtonPressed = false;

	m_pVehicleInstance->init();
}

///This function populates the VehicleInstance with default values of the motorcycle
void VehicleObject::buildMcycle()
{
	// All memory allocations are made here.
	m_pVehicleInstance->m_data				= new hkpVehicleData;
	m_pVehicleInstance->m_driverInput		= new hkpVehicleDefaultAnalogDriverInput;
	m_pVehicleInstance->m_steering			= new hkpVehicleDefaultSteering;
	m_pVehicleInstance->m_engine			= new hkpVehicleDefaultEngine;
	m_pVehicleInstance->m_transmission		= new hkpVehicleDefaultTransmission;
	m_pVehicleInstance->m_brake				= new hkpVehicleDefaultBrake;
	m_pVehicleInstance->m_suspension		= new hkpVehicleDefaultSuspension;
	m_pVehicleInstance->m_aerodynamics		= new hkpVehicleDefaultAerodynamics;
	m_pVehicleInstance->m_velocityDamper	= new hkpVehicleDefaultVelocityDamper;

	// For illustrative purposes we use a custom hkpVehicleRayCastWheelCollide
	// which implements varying 'ground' friction in a very simple way.
	m_pVehicleInstance->m_wheelCollide		= new hkpVehicleRayCastWheelCollide;

	setupMcycleData( *m_pVehicleInstance->m_data );

	// The tyremarks controller is initialized with the number of tyremarks to keep.
	m_pVehicleInstance->m_tyreMarks		= new hkpTyremarksInfo( *m_pVehicleInstance->m_data, 128 );

	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultAnalogDriverInput* >(m_pVehicleInstance->m_driverInput) );
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultSteering*>(m_pVehicleInstance->m_steering));
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultEngine*>(m_pVehicleInstance->m_engine) );
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultTransmission*>(m_pVehicleInstance->m_transmission) );
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultBrake*>(m_pVehicleInstance->m_brake) );
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultSuspension*>(m_pVehicleInstance->m_suspension) );
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultAerodynamics*>(m_pVehicleInstance->m_aerodynamics) );
	setupMComponent( *m_pVehicleInstance->m_data, *static_cast< hkpVehicleDefaultVelocityDamper*>(m_pVehicleInstance->m_velocityDamper) );

	//	setupMWheelCollide( world, m_pVehicleInstance, *static_cast< hkpVehicleRayCastWheelCollide*>(m_pVehicleInstance->m_wheelCollide) );
	//setupCamera( m_pVehicleInstance.m_camera );
	setupMTyremarks( *m_pVehicleInstance->m_data, *static_cast< hkpTyremarksInfo*>(m_pVehicleInstance->m_tyreMarks) );

	//
	// Check that all components are present.
	//
	HK_ASSERT(0x0, m_pVehicleInstance->m_data );
	HK_ASSERT(0x7708674a,  m_pVehicleInstance->m_driverInput );
	HK_ASSERT(0x5a324a2d,  m_pVehicleInstance->m_steering );
	HK_ASSERT(0x7bcb2aff,  m_pVehicleInstance->m_engine );
	HK_ASSERT(0x29bddb50,  m_pVehicleInstance->m_transmission );
	HK_ASSERT(0x2b0323a2,  m_pVehicleInstance->m_brake );
	HK_ASSERT(0x7a7ade23,  m_pVehicleInstance->m_suspension );
	HK_ASSERT(0x6ec4d0ed,  m_pVehicleInstance->m_aerodynamics );
	HK_ASSERT(0x67161206,  m_pVehicleInstance->m_wheelCollide );
	HK_ASSERT(0x295015f1,  m_pVehicleInstance->m_tyreMarks );

	// Set up any variables that store cached data.

	// Give driver input default values so that the vehicle (if this input is a default for non
	// player cars) will drive, even if it is in circles!

	// Accelerate.
	m_pVehicleInstance->m_deviceStatus = new hkpVehicleDriverInputAnalogStatus;
	deviceStatus = (hkpVehicleDriverInputAnalogStatus*)m_pVehicleInstance->m_deviceStatus;
	deviceStatus->m_positionY = 0.0f;

	// Turn.
	deviceStatus->m_positionX = 0.0f;

	// Defaults
	deviceStatus->m_handbrakeButtonPressed = false;
	deviceStatus->m_reverseButtonPressed = false;

	// Don't forget to call init! (This function ensures all internal data is consistent)
	m_pVehicleInstance->init();
}

//this function sets the dimensions of the vehicle

void VehicleObject::setDimensions(float dimensions[])
{
	for(int i=0;i<14;i++)
		vehicleDimensions[i]=dimensions[i];
}

///This function creates a chassis for the car
void VehicleObject::setupVehicles()
{
	GameObjectManager *f_pGameObjectManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager();
	//VehicleSetup setup;

	hkReal xSize = vehicleDimensions[0];
	hkReal ySize = vehicleDimensions[1];
	hkReal zSize = vehicleDimensions[2];

	hkReal xBumper = vehicleDimensions[3];
	hkReal yBumper = vehicleDimensions[4];
	hkReal zBumper = vehicleDimensions[5];

	hkReal xRoofFront = vehicleDimensions[6];
	hkReal xRoofBack = vehicleDimensions[7];
	hkReal yRoof = vehicleDimensions[8];
	hkReal zRoof = vehicleDimensions[9];

	hkReal xDoorFront = vehicleDimensions[10];
	hkReal xDoorBack = vehicleDimensions[11];
	hkReal yDoor = vehicleDimensions[12];
	hkReal zDoor = vehicleDimensions[13];

	int numV = 22;

	// 16 = 4 (size of "each float group", 3 for x,y,z, 1 for padding) * 4 (size of float).
	int stride = sizeof(float) * 4;

	float vertices[] = {
		xSize, ySize, zSize, 0.0f,		// v0
		xSize, ySize, -zSize, 0.0f,		// v1
		xSize, -ySize, zSize, 0.0f,		// v2
		xSize, -ySize, -zSize, 0.0f,	// v3
		-xSize, -ySize, zSize, 0.0f,	// v4
		-xSize, -ySize, -zSize, 0.0f,	// v5

		xBumper, yBumper, zBumper, 0.0f,	// v6
		xBumper, yBumper, -zBumper, 0.0f,	// v7
		-xBumper, yBumper, zBumper, 0.0f,	// v8
		-xBumper, yBumper, -zBumper, 0.0f,	// v9

		xRoofFront, yRoof, zRoof, 0.0f,		// v10
		xRoofFront, yRoof, -zRoof, 0.0f,	// v11
		xRoofBack, yRoof, zRoof, 0.0f,		// v12
		xRoofBack, yRoof, -zRoof, 0.0f,		// v13

		xDoorFront, yDoor, zDoor, 0.0f,		// v14
		xDoorFront, yDoor, -zDoor, 0.0f,	// v15
		xDoorFront, -yDoor, zDoor, 0.0f,	// v16
		xDoorFront, -yDoor, -zDoor, 0.0f,	// v17

		xDoorBack, yDoor, zDoor, 0.0f,		// v18
		xDoorBack, yDoor, -zDoor, 0.0f,		// v19
		xDoorBack, -yDoor, zDoor, 0.0f,		// v20
		xDoorBack, -yDoor, -zDoor, 0.0f,	// v21
	};

	hkStridedVertices a;
	a.m_numVertices = numV;
	a.m_striding = stride;
	a.m_vertices = vertices;

	hkpConvexVerticesShape* chassisShape = new hkpConvexVerticesShape(a);

	//Do Same for Vehicles

	int chassisLayer = 1;

	// Create the vehicle.
	{
		// Create the chassis body.
		{
			hkpRigidBodyCinfo chassisInfo;
			chassisInfo.m_mass = 750.0f;
			chassisInfo.m_shape = chassisShape;
			chassisInfo.m_friction = 0.8f;
			chassisInfo.m_motionType = hkpMotion::MOTION_BOX_INERTIA;
			//chassisInfo.m_collisionFilterInfo = 7;
			// Position chassis on the ground.

			// Inertia tensor will be set by VehicleSetup.
			//chassisInfo.m_position.set(-40.0f, -4.5f, 5.0f);
			chassisInfo.m_position.set(-40.0f, 20.5f, 5.0f);
			chassisInfo.m_inertiaTensor.setDiagonal(1.0f, 1.0f, 1.0f);

			chassisInfo.m_centerOfMass.set( -0.037f, 0.143f, 0.0f);
			chassisInfo.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo( 7 );

			m_pChassisRigidBody = new hkpRigidBody(chassisInfo);
		}

		createVehicle();
		//m_vehicles[vehicleId].m_lastRPM = 0.0f;

		// This hkpAction flips the car upright if it turns over.
		/*if (vehicleId == 0)
		{
			hkVector4 rotationAxis(1.0f, 0.0f, 0.0f);
			hkVector4 upAxis(0.0f, 1.0f, 0.0f);
			m_reorientAction = new hkpReorientAction(m_pChassisRigidBody, rotationAxis, upAxis);
		}*/

		m_pChassisRigidBody->removeReference();
	}
	chassisShape->removeReference();
}

///This function creates a chassis for the motorcycle
void VehicleObject::setupMcycle()
{
	GameObjectManager *f_pGameObjectManager = EnginePtr->GetForemostGameScreen()->GetGameObjectManager();
	hkpConvexVerticesShape* chassisShape = HK_NULL;
	{
		hkReal xSize = 0.7f;
		//hkReal ySize = 0.3f;
		hkReal ySizeUp = 0.5f;
		hkReal ySizeDown = -0.3f;
		hkReal zSize = 0.2f;

		hkReal xBumperFront = 0.95f;
		hkReal xBumperBack = -1.1f;
		hkReal yBumper = 0.13f;
		hkReal zBumper = 0.05f;

		hkReal xBodyFront = 0.5f;
		hkReal xBodyBack = -0.5f;
		hkReal yBody = 0.13f;
		hkReal zBody = 0.25f;

		// Data specific to this shape.
		int numVertices = 16;

		// 16 = 4 (size of "each float group", 3 for x,y,z, 1 for padding) * 4 (size of float).
		int stride = sizeof(float) * 4;

		float vertices[] = {
			xSize, ySizeUp, zSize, 0.0f,	// v0
			xSize, ySizeUp, -zSize, 0.0f,	// v1
			xSize, ySizeDown, zSize, 0.0f,	// v2
			xSize, ySizeDown, -zSize, 0.0f, // v3

			-xSize, ySizeUp, zSize, 0.0f,		// v4
			-xSize, ySizeUp, -zSize, 0.0f,		// v5
			-xSize, ySizeDown, zSize, 0.0f,		// v6
			-xSize, ySizeDown, -zSize, 0.0f,	// v7

			xBumperFront, yBumper, zBumper, 0.0f,	// v8
			xBumperFront, yBumper, -zBumper, 0.0f,	// v9
			xBumperBack, yBumper, zBumper, 0.0f,	// v10
			xBumperBack, yBumper, -zBumper, 0.0f,	// v11

			xBodyFront, yBody, zBody, 0.0f,		// v12
			xBodyFront, yBody, -zBody, 0.0f,	// v13
			xBodyBack, yBody, zBody, 0.0f,		// v14
			xBodyBack, yBody, -zBody, 0.0f,		// v15
		};

		// SHAPE CONSTRUCTION.
		{
			hkStridedVertices stridedVerts;
			{
				stridedVerts.m_numVertices = numVertices;
				stridedVerts.m_striding = stride;
				stridedVerts.m_vertices = vertices;
			}

			chassisShape = new hkpConvexVerticesShape(stridedVerts);
		}
	}

	// Create the chassis body.
	{
		hkpRigidBodyCinfo chassisInfo;
		chassisInfo.m_mass = 250.0f;
		chassisInfo.m_shape = chassisShape;
		chassisInfo.m_friction = 0.8f;
		chassisInfo.m_motionType = hkpMotion::MOTION_BOX_INERTIA;
		chassisInfo.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo( 7 );

		// Position chassis on the ground.
		chassisInfo.m_position.set(-40.0f, 4.5f,  5.0f);
		hkpInertiaTensorComputer::setShapeVolumeMassProperties(chassisInfo.m_shape, chassisInfo.m_mass, chassisInfo);

		m_pChassisRigidBody = new hkpRigidBody(chassisInfo);
	}

	createMcycle();

	/////////////////////////////////////////////
	///[SetUpConstaint]
	/// Use the constraint kit to create a 1d angular constraint to hold motorcycle
	/// upright and to tilt it while turning.
	///
	{
		// Up axis.
		hkVector4 upWorldSpace = m_pVehicleInstance->m_data->m_chassisOrientation.getColumn(0);
		// Forward axis.
		hkVector4 forwardChassisSpace = m_pVehicleInstance->m_data->m_chassisOrientation.getColumn(1);
		// Right axis.
		hkVector4 rightChassisSpace = m_pVehicleInstance->m_data->m_chassisOrientation.getColumn(2);

		{
			// Constrain the chassis with respect to the world.
			m_genericConstraint =	new hkpGenericConstraintData();
			hkpConstraintConstructionKit kit;

			// Must always start with this command (the constraint works like a program, every
			// command being executed sequentially).
			kit.begin(m_genericConstraint);
			{
				// Forward axis, x.
				const hkVector4 colX(1.0f, 0.0f, 0.0f);

				// Up axis, y.
				const hkVector4 colY(0.0f, 1.0f, 0.0f);

				// Right axis, z.
				const hkVector4 colZ(0.0f, 0.0f, 1.0f);

				hkMatrix3 constrainB;
				hkMatrix3 constrainA;

				constrainA.setCols(colX, colY, colZ);
				constrainB.setCols(colX, colY, colZ);

				// Set both basis' to Identity at start.
				m_basisIndexA = kit.setAngularBasisA(constrainA);
				m_basisIndexB = kit.setAngularBasisB(constrainB);

				// Let angle vary between -0.01 and 0.01 radians
				kit.setAngularLimit(0, -0.01f, 0.01f);
			}
			// Must always end with this command.
			kit.end();

			m_constraint = m_pPhysicsWorld->createAndAddConstraintInstance(m_pPhysicsWorld->getFixedRigidBody(), m_pChassisRigidBody, m_genericConstraint);
		}
	}
	m_pChassisRigidBody->removeReference();

	chassisShape->removeReference();
}

///This function populates the VehicleData instance for the car
void VehicleObject::setupVehicleData(hkpVehicleData &data)
{
	data.m_gravity = m_pPhysicsWorld->getGravity();

	data.m_chassisOrientation.setCols( hkVector4(0, 1, 0), hkVector4(1, 0, 0), hkVector4(0, 0, 1));

	data.m_frictionEqualizer = 0.5f;

	// Inertia tensor for each axis is calculated by using :
	// (1 / chassis_mass) * (torque(axis)Factor / chassisUnitInertia)
	data.m_torqueRollFactor = 0.625f;
	data.m_torquePitchFactor = 0.5f;
	data.m_torqueYawFactor = 0.35f;

	data.m_chassisUnitInertiaYaw = 1.0f;
	data.m_chassisUnitInertiaRoll = 1.0f;
	data.m_chassisUnitInertiaPitch = 1.0f;

	// Adds or removes torque around the yaw axis
	// based on the current steering angle.  This will
	// affect steering.
	data.m_extraTorqueFactor = -0.5f;
	data.m_maxVelocityForPositionalFriction = 0.0f;

	// Wheel specifications
	data.m_numWheels = 4;

	data.m_wheelParams.setSize( data.m_numWheels );

	data.m_wheelParams[0].m_axle = 0;
	data.m_wheelParams[1].m_axle = 0;
	data.m_wheelParams[2].m_axle = 1;
	data.m_wheelParams[3].m_axle = 1;

	data.m_wheelParams[0].m_friction = 1.5f;
	data.m_wheelParams[1].m_friction = 1.5f;
	data.m_wheelParams[2].m_friction = 1.5f;
	data.m_wheelParams[3].m_friction = 1.5f;

	data.m_wheelParams[0].m_slipAngle = 0.0f;
	data.m_wheelParams[1].m_slipAngle = 0.0f;
	data.m_wheelParams[2].m_slipAngle = 0.0f;
	data.m_wheelParams[3].m_slipAngle = 0.0f;

	for ( int i = 0 ; i < data.m_numWheels ; i++ )
	{
		// This value is also used to calculate the m_primaryTransmissionRatio.
		data.m_wheelParams[i].m_radius = 0.4f;
		data.m_wheelParams[i].m_width = 0.2f;
		data.m_wheelParams[i].m_mass = 10.0f;

		data.m_wheelParams[i].m_viscosityFriction = 0.25f;
		data.m_wheelParams[i].m_maxFriction = 2.0f * data.m_wheelParams[i].m_friction;
		data.m_wheelParams[i].m_forceFeedbackMultiplier = 0.1f;
		data.m_wheelParams[i].m_maxContactBodyAcceleration = hkReal(data.m_gravity.length3()) * 2;
	}
}

///This function populates the VehicleData instance for the motorcycle
void VehicleObject::setupMcycleData( hkpVehicleData &data)
{
	data.m_gravity = m_pPhysicsWorld->getGravity();

	// The vehicleData contains information about the chassis.

	// The coordinates of the chassis system, used for steering the vehicle.
	//										up					forward				right
	data.m_chassisOrientation.setCols( hkVector4(0, 1, 0), hkVector4(1, 0, 0), hkVector4(0, 0, 1));

	data.m_frictionEqualizer = 0.5f;

	data.m_torqueRollFactor = 0.25f;
	data.m_torquePitchFactor = 0.5f;
	data.m_torqueYawFactor = 0.35f;

	data.m_extraTorqueFactor = -0.5f;
	data.m_maxVelocityForPositionalFriction = 10.0f;

	data.m_chassisUnitInertiaYaw = 0.3f;
	data.m_chassisUnitInertiaRoll = 0.3f;
	data.m_chassisUnitInertiaPitch = 0.5f;

	// Wheel specifications
	data.m_numWheels = 2;

	data.m_wheelParams.setSize( data.m_numWheels );

	data.m_wheelParams[0].m_axle = 0;
	data.m_wheelParams[1].m_axle = 1;

	data.m_wheelParams[0].m_friction = 2.5f;
	data.m_wheelParams[1].m_friction = 2.5f;

	for ( int i = 0 ; i < data.m_numWheels ; i++ )
	{
		// This value is also used to calculate the m_primaryTransmissionRatio.
		data.m_wheelParams[i].m_radius = 0.4f;
		data.m_wheelParams[i].m_width = 0.25f;
		data.m_wheelParams[i].m_mass = 15.0f;

		data.m_wheelParams[i].m_viscosityFriction = 0.05f;
		data.m_wheelParams[i].m_slipAngle = 0.0f;
		data.m_wheelParams[i].m_maxFriction = 2.0f * data.m_wheelParams[i].m_friction;
		data.m_wheelParams[i].m_forceFeedbackMultiplier = 30.0f;
		data.m_wheelParams[i].m_maxContactBodyAcceleration = hkReal(data.m_gravity.length3()) * 2;
	}
}

///This function is used to steer the car to the left
void VehicleObject::steer_left()
{
	//The deviceStatus for vehicle should be updated in the vehicles Update function rather than over here.
	deviceStatus->m_positionX = -0.5;
}
void VehicleObject::steer_right()
{
	deviceStatus->m_positionX = 0.5;
}

void VehicleObject::go_straight()
{
	deviceStatus->m_positionX = 0.0;
}

///This function is used to accelerate the car
void VehicleObject::accelerate()
{
	deviceStatus->m_positionY = -0.9f;
}

///This function is used to reverse the car
void VehicleObject::reverse()
{
	deviceStatus->m_positionY = 0.9f;
}

///This function is used to decelerate the car
void VehicleObject::slow()
{
	deviceStatus->m_positionY = 0.0;
}

///This sets the position of the Vehicle in the world
void VehicleObject::setPosition(hkVector4 &position)
{
	m_pChassisRigidBody->setPosition(position);
}

///Returns the current position of the chassis  in the world
hkVector4 VehicleObject::getPosition()
{
	return m_pChassisRigidBody->getPosition();
}

///This function returns the RigidBody i.e. the chassis
hkpRigidBody* VehicleObject::getRigidBody()
{
	return m_pChassisRigidBody;
}

///This function returns the Physics World
hkpWorld* VehicleObject::getPhysicsWorld()
{
	return this->m_pPhysicsWorld;
}

bool VehicleObject::update()
{
	if (m_pChassisRigidBody==NULL)
		return false;

 	hkReal f_fStoredPosition[3];
	m_pChassisRigidBody->getPosition().store3(f_fStoredPosition);
	m_pGraphicsObject->setPosition(f_fStoredPosition[0],f_fStoredPosition[1],f_fStoredPosition[2]);

	hkReal f_fStoredRotation[4];
	m_pChassisRigidBody->getRotation().m_vec.store4(f_fStoredRotation);
	m_pGraphicsObject->setOrientation(f_fStoredRotation[0],f_fStoredRotation[1],f_fStoredRotation[2],f_fStoredRotation[3]);

	//if the object is removed from HavokWorld
	//the gameobject should be deleted too
	//return m_pChassisRigidBody->isAddedToWorld();//this should be returned but for practical purposes we will return true;
	return true;
}
bool VehicleObject::scale()
{
	//this hasn't been implemented yet
	return false;
}
///This function sets the Default input for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultAnalogDriverInput &driverInput )
{
	// We also use an analog "driver input" class to help converting user input to vehicle behavior.
	driverInput.m_slopeChangePointX = 0.8f;
	driverInput.m_initialSlope = 0.7f;
	driverInput.m_deadZone = 0.0f;
	driverInput.m_autoReverse = true;
}

///This function sets the Default steering settings for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultSteering &steering )
{
	steering.m_doesWheelSteer.setSize( data.m_numWheels );

	// degrees
	steering.m_maxSteeringAngle = 35 * ( HK_REAL_PI / 180 );

	// [mph/h] The steering angle decreases linearly
	// based on your overall max speed of the vehicle.
	steering.m_maxSpeedFullSteeringAngle = 70.0f * (1.605f / 3.6f);
	steering.m_doesWheelSteer[0] = true;
	steering.m_doesWheelSteer[1] = true;
	steering.m_doesWheelSteer[2] = false;
	steering.m_doesWheelSteer[3] = false;
}

///This function sets the Default engine settings for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultEngine &engine )
{
	engine.m_maxTorque = 500.0f;

	engine.m_minRPM = 1000.0f;
	engine.m_optRPM = 5500.0f;

	// This value is also used to calculate the m_primaryTransmissionRatio.
	engine.m_maxRPM = 7500.0f;

	engine.m_torqueFactorAtMinRPM = 0.8f;
	engine.m_torqueFactorAtMaxRPM = 0.8f;

	engine.m_resistanceFactorAtMinRPM = 0.05f;
	engine.m_resistanceFactorAtOptRPM = 0.1f;
	engine.m_resistanceFactorAtMaxRPM = 0.3f;
}

///This function sets the Default transmission settings for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultTransmission &transmission )
{
	int numGears = 4;

	transmission.m_gearsRatio.setSize( numGears );
	transmission.m_wheelsTorqueRatio.setSize( data.m_numWheels );

	transmission.m_downshiftRPM = 3500.0f;
	transmission.m_upshiftRPM = 6500.0f;

	transmission.m_clutchDelayTime = 0.0f;
	transmission.m_reverseGearRatio = 1.0f;
	transmission.m_gearsRatio[0] = 2.0f;
	transmission.m_gearsRatio[1] = 1.5f;
	transmission.m_gearsRatio[2] = 1.0f;
	transmission.m_gearsRatio[3] = 0.75f;
	transmission.m_wheelsTorqueRatio[0] = 0.2f;
	transmission.m_wheelsTorqueRatio[1] = 0.2f;
	transmission.m_wheelsTorqueRatio[2] = 0.3f;
	transmission.m_wheelsTorqueRatio[3] = 0.3f;

	const hkReal vehicleTopSpeed = 130.0f;
	const hkReal wheelRadius = 0.4f;
	const hkReal maxEngineRpm = 7500.0f;
	transmission.m_primaryTransmissionRatio = hkpVehicleDefaultTransmission::calculatePrimaryTransmissionRatio(
		vehicleTopSpeed,
		wheelRadius,
		maxEngineRpm,
		transmission.m_gearsRatio[ numGears - 1 ] );
}

///This function sets the Default brake settings for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultBrake &brake )
{
	brake.m_wheelBrakingProperties.setSize( data.m_numWheels );

	const float bt = 1500.0f;
	brake.m_wheelBrakingProperties[0].m_maxBreakingTorque = bt;
	brake.m_wheelBrakingProperties[1].m_maxBreakingTorque = bt;
	brake.m_wheelBrakingProperties[2].m_maxBreakingTorque = bt;
	brake.m_wheelBrakingProperties[3].m_maxBreakingTorque = bt;

	// Handbrake is attached to rear wheels only.
	brake.m_wheelBrakingProperties[0].m_isConnectedToHandbrake = false;
	brake.m_wheelBrakingProperties[1].m_isConnectedToHandbrake = false;
	brake.m_wheelBrakingProperties[2].m_isConnectedToHandbrake = true;
	brake.m_wheelBrakingProperties[3].m_isConnectedToHandbrake = true;
	brake.m_wheelBrakingProperties[0].m_minPedalInputToBlock = 0.9f;
	brake.m_wheelBrakingProperties[1].m_minPedalInputToBlock = 0.9f;
	brake.m_wheelBrakingProperties[2].m_minPedalInputToBlock = 0.9f;
	brake.m_wheelBrakingProperties[3].m_minPedalInputToBlock = 0.9f;
	brake.m_wheelsMinTimeToBlock = 1000.0f;
}

///This function sets the Default suspension settings for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultSuspension &suspension )
{
	suspension.m_wheelParams.setSize( data.m_numWheels );
	suspension.m_wheelSpringParams.setSize( data.m_numWheels );

	suspension.m_wheelParams[0].m_length = 0.35f;
	suspension.m_wheelParams[1].m_length = 0.35f;
	suspension.m_wheelParams[2].m_length = 0.35f;
	suspension.m_wheelParams[3].m_length = 0.35f;

	const float str = 50.0f;
	suspension.m_wheelSpringParams[0].m_strength = str;
	suspension.m_wheelSpringParams[1].m_strength = str;
	suspension.m_wheelSpringParams[2].m_strength = str;
	suspension.m_wheelSpringParams[3].m_strength = str;

	const float wd = 3.0f;
	suspension.m_wheelSpringParams[0].m_dampingCompression = wd;
	suspension.m_wheelSpringParams[1].m_dampingCompression = wd;
	suspension.m_wheelSpringParams[2].m_dampingCompression = wd;
	suspension.m_wheelSpringParams[3].m_dampingCompression = wd;

	suspension.m_wheelSpringParams[0].m_dampingRelaxation = wd;
	suspension.m_wheelSpringParams[1].m_dampingRelaxation = wd;
	suspension.m_wheelSpringParams[2].m_dampingRelaxation = wd;
	suspension.m_wheelSpringParams[3].m_dampingRelaxation = wd;

	// NB: The hardpoints MUST be positioned INSIDE the chassis.
	{
		const hkReal hardPointFrontX = 1.3f;
		const hkReal hardPointBackX = -1.1f;
		const hkReal hardPointY = -0.05f;
		const hkReal hardPointZ = 1.1f;

		suspension.m_wheelParams[0].m_hardpointChassisSpace.set ( hardPointFrontX, hardPointY, -hardPointZ);
		suspension.m_wheelParams[1].m_hardpointChassisSpace.set ( hardPointFrontX, hardPointY,  hardPointZ);
		suspension.m_wheelParams[2].m_hardpointChassisSpace.set ( hardPointBackX, hardPointY, -hardPointZ);
		suspension.m_wheelParams[3].m_hardpointChassisSpace.set ( hardPointBackX, hardPointY,  hardPointZ);
	}

	const hkVector4 downDirection( 0.0f, -1.0f, 0.0f );
	suspension.m_wheelParams[0].m_directionChassisSpace = downDirection;
	suspension.m_wheelParams[1].m_directionChassisSpace = downDirection;
	suspension.m_wheelParams[2].m_directionChassisSpace = downDirection;
	suspension.m_wheelParams[3].m_directionChassisSpace = downDirection;
}

///This function sets the Default aerodynamics settings for the car
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultAerodynamics &aerodynamics )
{
	aerodynamics.m_airDensity = 1.3f;
	// In m^2.
	aerodynamics.m_frontalArea = 1.0f;

	aerodynamics.m_dragCoefficient = 0.7f;
	aerodynamics.m_liftCoefficient = -0.3f;

	// Extra gravity applies in world space (independent of m_chassisCoordinateSystem).
	aerodynamics.m_extraGravityws.set( 0.0f, -5.0f, 0.0f);
}

///This function is used for the car's velocity damping
void VehicleObject::setupComponent( const hkpVehicleData &data, hkpVehicleDefaultVelocityDamper &velocityDamper )
{
	// Caution: setting negative damping values will add energy to system.
	// Setting the value to 0 will not affect the angular velocity.

	// Damping the change of the chassis angular velocity when below m_collisionThreshold.
	// This will affect turning radius and steering.
	velocityDamper.m_normalSpinDamping    = 0.0f;

	// Positive numbers dampen the rotation of the chassis and
	// reduce the reaction of the chassis in a collision.
	velocityDamper.m_collisionSpinDamping = 4.0f;

	// The threshold in m/s at which the algorithm switches from
	// using the normalSpinDamping to the collisionSpinDamping.
	velocityDamper.m_collisionThreshold   = 1.0f;
}

///This function sets the tyre marks on the ground
void VehicleObject::setupTyremarks( const hkpVehicleData &data, hkpTyremarksInfo &tyremarkscontroller )
{
	tyremarkscontroller.m_minTyremarkEnergy = 100.0f;
	tyremarkscontroller.m_maxTyremarkEnergy  = 1000.0f;
}

///This function sets the wheel collision settings for the car
void VehicleObject::setupWheelCollide( hkpVehicleRayCastWheelCollide &wheelCollide )
{
	// Set the wheels to have the same collision filter info as the chassis.
	wheelCollide.m_wheelCollisionFilterInfo = m_pChassisRigidBody->getCollisionFilterInfo();
}

///This function sets the Default input for the motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData &data, hkpVehicleDefaultAnalogDriverInput &driverInput )
{
	// We also use an analog "driver input" class to help converting user input to vehicle behavior.
	driverInput.m_slopeChangePointX = 0.7f;
	driverInput.m_initialSlope = 0.5f;
	driverInput.m_deadZone = 0.0f;
	driverInput.m_autoReverse = true;
}

///This function sets the Default steering settings for the motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultSteering& steering )
{
	steering.m_doesWheelSteer.setSize( data.m_numWheels );

	// degrees
	steering.m_maxSteeringAngle = HK_REAL_PI / 10.0f;
	// [mph/h].
	steering.m_maxSpeedFullSteeringAngle = 130.0f * (1.605f / 3.6f);
	steering.m_doesWheelSteer[0] = true;
	steering.m_doesWheelSteer[1] = false;
}

///This function sets the Default engine settings for the motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultEngine& engine )
{
	// 300.0f hkpVehicleDefaultEngine::m_maxTorque gives reasonable balance of control e.g. not "wheelie-ing"
	// too easily, and fun e.g. can still do donuts and reach about 140mph within reasonable time. Higher
	// values reach top speed sooner but will wheelie (and fall over) easily.
	engine.m_maxTorque = 300.0f;

	engine.m_minRPM = 500.0f;
	engine.m_optRPM = 4500.0f;

	// This value is also used to calculate the m_primaryTransmissionRatio.
	engine.m_maxRPM = 6000.0f;

	engine.m_torqueFactorAtMinRPM = 1.0f;
	engine.m_torqueFactorAtMaxRPM = 1.0f;

	engine.m_resistanceFactorAtMinRPM = 0.05f;
	engine.m_resistanceFactorAtOptRPM = 0.1f;
	engine.m_resistanceFactorAtMaxRPM = 0.3f;
}

///This function sets the Default transmission settings for the motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultTransmission& transmission )
{
	int numGears = 5;

	transmission.m_gearsRatio.setSize( numGears );
	transmission.m_wheelsTorqueRatio.setSize( data.m_numWheels );

	transmission.m_downshiftRPM = 2500.0f;
	transmission.m_upshiftRPM = 5300.0f;

	transmission.m_clutchDelayTime = 0.0f;
	transmission.m_reverseGearRatio = 1.0f;

	transmission.m_gearsRatio[0] = 6.0f;
	transmission.m_gearsRatio[1] = 4.0f;
	transmission.m_gearsRatio[2] = 2.5f;
	transmission.m_gearsRatio[3] = 1.5f;
	transmission.m_gearsRatio[4] = 1.0f;
	transmission.m_wheelsTorqueRatio[0] = 0.2f;
	transmission.m_wheelsTorqueRatio[1] = 0.8f;

	const hkReal vehicleTopSpeed = 180.0f;
	const hkReal wheelRadius = 0.4f;
	const hkReal maxEngineRpm = 6000.0f;
	transmission.m_primaryTransmissionRatio = hkpVehicleDefaultTransmission::calculatePrimaryTransmissionRatio( vehicleTopSpeed,
		wheelRadius,
		maxEngineRpm,
		transmission.m_gearsRatio[ numGears - 1 ] );
}

///This function sets the Default brake settings for the car motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultBrake& brake )
{
	brake.m_wheelBrakingProperties.setSize( data.m_numWheels );

	brake.m_wheelBrakingProperties[0].m_maxBreakingTorque = 200;
	brake.m_wheelBrakingProperties[1].m_maxBreakingTorque = 2000;

	brake.m_wheelBrakingProperties[0].m_isConnectedToHandbrake = false;

	// 'Handbrake' is on back wheel of motorcyle.
	brake.m_wheelBrakingProperties[1].m_isConnectedToHandbrake = true;

	brake.m_wheelBrakingProperties[0].m_minPedalInputToBlock = 0.9f;
	brake.m_wheelBrakingProperties[1].m_minPedalInputToBlock = 0.9f;
	brake.m_wheelsMinTimeToBlock = 1000.0f;
}

///This function sets the Default suspension settings for the motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultSuspension& suspension )
{
	suspension.m_wheelParams.setSize( data.m_numWheels );
	suspension.m_wheelSpringParams.setSize( data.m_numWheels );

	suspension.m_wheelParams[0].m_length = 0.35f;
	suspension.m_wheelParams[1].m_length = 0.35f;

	const float str = 50.0f;
	suspension.m_wheelSpringParams[0].m_strength = str;
	suspension.m_wheelSpringParams[1].m_strength = str;

	const float wd = 3.0f;
	suspension.m_wheelSpringParams[0].m_dampingCompression = wd;
	suspension.m_wheelSpringParams[1].m_dampingCompression = wd;
	suspension.m_wheelSpringParams[0].m_dampingRelaxation = wd;
	suspension.m_wheelSpringParams[1].m_dampingRelaxation = wd;

	// NB: The hard points MUST be positioned INSIDE the chassis.
	{
		const hkReal hardPointX = 0.7f;
		const hkReal hardPointY = -0.05f;
		const hkReal hardPointZ = 0.0f;

		suspension.m_wheelParams[0].m_hardpointChassisSpace.set ( hardPointX, hardPointY, hardPointZ);
		suspension.m_wheelParams[1].m_hardpointChassisSpace.set ( -hardPointX, hardPointY, hardPointZ);
	}

	const hkVector4 downDirection( 0.0f, -1.0f, 0.0f );
	suspension.m_wheelParams[0].m_directionChassisSpace = downDirection;
	suspension.m_wheelParams[1].m_directionChassisSpace = downDirection;
}

///This function sets the Default aerodynamics settings for the motorcycle
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultAerodynamics& aerodynamics )
{
	aerodynamics.m_airDensity = 1.3f;
	// In m^2.
	aerodynamics.m_frontalArea = 1.0f;

	aerodynamics.m_dragCoefficient = 0.7f;
	aerodynamics.m_liftCoefficient = -0.3f;

	// Extra gravity applies in world space (independent of m_chassisCoordinateSystem).
	aerodynamics.m_extraGravityws.set( 0.0f, -5.0f, 0.0f);
}

///This function is used for the motorcycle's velocity damping
void VehicleObject::setupMComponent( const hkpVehicleData& data, hkpVehicleDefaultVelocityDamper& velocityDamper )
{
	velocityDamper.m_normalSpinDamping    = 1.0f;
	velocityDamper.m_collisionSpinDamping = 0.0f;
	velocityDamper.m_collisionThreshold   = 4.0f;
}

///This function sets the tyre marks on the ground
void VehicleObject::setupMTyremarks( const hkpVehicleData& data, hkpTyremarksInfo& tyremarkscontroller )
{
	tyremarkscontroller.m_minTyremarkEnergy = 50.0f;
	tyremarkscontroller.m_maxTyremarkEnergy  = 1000.0f;
}

///This function sets the wheel collision settings for the motorcycle
void VehicleObject::setupMWheelCollide( hkpVehicleRayCastWheelCollide& wheelCollide )
{
	// Set the wheels to have the same collision filter info as the chassis.
	wheelCollide.m_wheelCollisionFilterInfo = m_pChassisRigidBody->getCollisionFilterInfo();
}

/*JJUNK
//CONSTRUCTOR_PHYSICSOBJECT_1
//A constructor which constructs a rigid body from havok content tools by parsing a hkx file according to the entity name given
PhysicsObject::PhysicsObject(
					string			f_sOgreUniqueName,
					string			f_sMeshFileName,
					string			f_sHKXFileName,
					string			f_sHavokEntityName,
					GameObjectType			f_eHavokObjectType,
					GraphicsObject**		f_pGraphicsObject		///for setting the GraphicsObject pointer of the GameObject which has this entity
				) :PhysicsObject()
{
	initializePhysicsObject();

	m_pPhysicsWorld		= f_pGameObjectManager->GetWorld();

	hkpPhysicsData*		f_pPhysicsData; //Physics data related to a hkx file
	hkPackfileData*		f_pPackfileData;//Packfile data related to a hkx file

	havokPackFileLoader(f_pPackfileData,f_pPhysicsData,f_sHKXFileName);// Load the data

//RDS_PREVDEFINITON Spliting the systems just in case - for future use - not required at the moment
//RDS_PREVDEFINITON hkpPhysicsData::SplitPhysicsSystemsOutput * f_pPhysicsSystemsOutput;
//RDS_PREVDEFINITON  malloc is being used and free not used. is it correct??
//RDS_PREVDEFINITON f_pPhysicsSystemsOutput=(hkpPhysicsData::SplitPhysicsSystemsOutput*)malloc(sizeof(hkpPhysicsData::SplitPhysicsSystemsOutput));

	m_pRigidBodyObject = m_pPhysicsData->findRigidBodyByName(f_sHavokEntityName.c_str());
//RDS_LOOK not a good way to do error checking
	if (m_pRigidBodyObject==NULL) {
		GGETRACELOG("HavokError in PhysicsObject Constructor Function (CONSTRUCTOR_PHYSICSOBJECT_1).\n\
					Could not create PhysicsObject.\n\
					Object is NULL because the havok entity name you supported = [%s] couldn't be found int file [%s].\n",
					f_sHavokEntityName,f_sHKXFileName);
		return;
	}
//RDS_PREVDEFINITON here - setCollisionFilterInfo used to create flycheat functionality. We can use such collision filters to
//RDS_PREVDEFINITON create objects that we need to move through and others that we do not require. Future: Put into one other function
//RDS_PREVDEFINITON and call function when flycheat needs to be enabled.

	if (f_eHavokObjectType!=DEFAULT_GAME_OBJECT_TYPE) {
		//RDS_LOOK for Hussain to see how they set the motion type before (most probably through HavokContentTool)
		//dear Hussain please put a break point here and see :) for the_ramp.hkx :)
			hkpMotion::MotionType tempMotionType = m_pRigidBodyObject->getMotionType();

			switch (f_eHavokObjectType) {
				case PHYSICS_FIXED:
					m_pRigidBodyObject->setCollisionFilterInfo(COLLISION_LAYER_STATIC_OBJECTS);
					m_pRigidBodyObject->setMotionType(hkpMotion::MOTION_FIXED);
					break;
				case PHYSICS_DYNAMIC:
					m_pRigidBodyObject->setCollisionFilterInfo(COLLISION_LAYER_MOVING_OBJECTS);
					break;
				case PHYSICS_KEYFRAMED:
					m_pRigidBodyObject->setCollisionFilterInfo(COLLISION_LAYER_KEYFRAMED_OBJECTS);
					m_pRigidBodyObject->setMotionType(hkpMotion::MOTION_KEYFRAMED);
					break;
			}

		//RDS_HARDCODED
			hkVector4 pos(0,1,0,0);
			m_pRigidBodyObject->setPosition(pos);
		//RDS_LOOK I do not know which one is the right way to do.. Both gives me the same effect till now..
		#if 1
			m_pPhysicsWorld->addEntity(m_pRigidBodyObject);
		#else
			m_pPhysicsWorld->addPhysicsSystem(m_pPhysicsData->getPhysicsSystems()[0]);
		#endif
		//	m_pContactListener = new hkpContactListener();
		//	m_pRigidBodyObject->addContactListener(m_pContactListener);

			m_pRigidBodyObject->removeReference();
			*f_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_pOgreSceneManager,f_sOgreUniqueName,f_sMeshFileName,f_eHavokObjectType));
	}
	else {
		//RDS_HARDCODED
		//hkVector4 pos(0,1,0,0);
		//m_pRigidBodyObject->setPosition(pos);
		m_pPhysicsWorld->addEntity(m_pRigidBodyObject);
		m_pRigidBodyObject->removeReference();
		string f_sTempMeshName = automaticMeshNameCreator(f_sHavokEntityName,"");
		f_sOgreUniqueName = automaticOgreUniqueNameCreator(f_sHavokEntityName,"");
		*f_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_pOgreSceneManager,f_sOgreUniqueName,f_sTempMeshName,f_eHavokObjectType));
	}
}
*/

#endif