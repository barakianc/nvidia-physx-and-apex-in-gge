
#ifndef ANIMATED_ENTITY_H
#define ANIMATED_ENTITY_H

#include <string>
#include <vector>
#include <algorithm>
#include <map>
using namespace std;

namespace AnimationManager
{

	/// AnimatedEntity is an abstract class, to be either Havok Animated Entity or Ogre Animated Entity
	class AnimatedEntity
	{
		/// type definitions for Ramp/Blend Duration map
		typedef std::map<const std::string, float> TStrRampMap;
		typedef TStrRampMap::iterator TStrRampIt;
		typedef std::pair<const std::string, float> TStrRampPair;

	protected:
		const char* m_sName;												// Entity name which is unique
		const char* m_sActiveAnim;									// Name of the animation that is currently playing
																							// None if no animation is playing
		const char* m_sDefaultAnim;									// Name of the default animation that is assigned by user for an entity
																							// None if none is assigned
		std::vector<std::string> m_aAnimationNames;	// List of Animations that an animated entity have in the manager
		float m_fDeltaTime;													// Current Delta Time, only used in Ogre for now
		TStrRampMap m_mRampDownValues;					// RampDown values for an animation, now uses BlendDuration/2 for RampDown

	public:

		// no use for ogre entity, just for generalization
		float m_xInitPos;
		float m_yInitPos;
		float m_zInitPos;
		float m_xInitScale;
		float m_yInitScale;
		float m_zInitScale;

		/*!
			@remark Animated Entity Destructor: It is virtual function for destroy Havok animated entity or Ogre animated entity
		*/
        virtual ~AnimatedEntity(){};
		
		/*!
			@remark Animated Entity Update: It is virtual function for update Havok animated entity or Ogre animated entity
			@param[in] f_fTimeStep is the delta time since last frame
		*/
		virtual void Update(float f_fTimeStep) = 0;

		/*!
			@remarks	Set AnimatedEntity's position, only for HavokEntity actually
			@param[in] f_vhkPos is the position user wants to set the skeleton to
		*/
		virtual void SetPosition(float x, float y, float z) = 0;

		/*!
			@remarks	Play Animation given the name(s) once or looping
								Blend through given seconds from current animation
			@param[in] f_sDestinationAnimationName is the name of animation user wants to play
			@param[in] f_sTransitionAnimationName is the name of animation for intermediate transition
									default: no transition
			@param[in] f_bLoop indicates whether to play the animation once or loop the animation
									default: looping
			@param[in] f_fBlendDuration is the seconds transition to destination
		*/
		virtual void PlayAnimation(	const char* f_sDestinationAnimationName,		//
									const char* f_sTransitionAnimationName = "",	//
									const bool	f_bLoop = true,						//
									const float f_fBlendDuration = 0.5f) = 0;			//
		
		/*!
			@remarks	Play Animation given the name(s) once or looping
								Blend through given seconds overlay current animation
			@param[in] f_sOverlayAnimationName is the name of animation user wants to play
			@param[in] f_sTransitionAnimationName is the name of animation for intermediate transition
									default: no transition
			@param[in] f_bLoop indicates whether to play the animation once or loop the animation
									default: looping
			@param[in] f_fBlendDuration is the seconds transition to destination
		*/
		virtual void PlayAnimationOverlay(	const char* f_sOverlayAnimationName,	//
									const char* f_sTransitionAnimationName = "",	//
									const bool	f_bLoop = true,						//
									const float f_fBlendDuration = 0.5f) = 0;			//
		
		/*!
			@remarks	Stops the animation given the name
			@param[in] f_sAnimationName is the name of animation user wants to stop
								default: stop all animations that are playing
		*/
		virtual void StopAnimation(const char* f_sAnimationName = "") = 0;
		virtual void StopAllAnimation() = 0;
		/*!
			@remarks Checks if an animation of given name is playing
			@param[in] f_sAnimationName is the name of animation user wants to check
		*/
		virtual bool IsAnimationPlaying(const char* f_sAnimationName) = 0;
		
		/*!
			@return Unique name of this entity
		*/
		virtual const char* GetName() = 0;
		/*!
			@return Animation names loaded in this entity
		*/
		virtual std::vector<std::string> GetAnimationNames() = 0;
		/*!
			@return Current playing animation(s)
		*/
		virtual const char* GetActiveAnimation() = 0;
		/*!
			@remark Set default animation which will be played when no other animation is playing
			@param[in] Default animation name
		*/
		virtual void SetDefaultAnimation(const char* f_sDefaultAnim) = 0;
		/*!
			@return default animation name
		*/
		virtual const char* GetDefaultAnimation() = 0;

		/*!
			@remark Checks if given animation exists in this entity
		*/
		bool HasAnimationName(std::string animationNameToCheck)
		{
			bool foundAnimationName = false;
			if (m_aAnimationNames.size() > 0)
			{
				for (std::vector<std::string>::iterator it = m_aAnimationNames.begin(); it != m_aAnimationNames.end(); it ++)
				{
					if (animationNameToCheck.compare(*it) == 0)
					{
						foundAnimationName = true;
						break;
					}
				}
			}
			return foundAnimationName;
		}
	};
}

#endif // ANIMATED_ENTITY_H