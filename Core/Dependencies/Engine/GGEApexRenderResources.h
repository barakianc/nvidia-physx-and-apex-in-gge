#ifndef _APEX_RENDER_RESOURCES_H_
#define _APEX_RENDER_RESOURCES_H_

#include <stdio.h>
#include <iostream>
#include <string.h>

#ifdef PHYSX
#ifdef PHYSX_APEX
#include <NxUserRenderer.h>
#include <NxUserRenderResourceManager.h>

#include <NxApexRenderContext.h>
#include <NxUserRenderBoneBuffer.h>
#include <NxUserRenderIndexBuffer.h>
#include <NxUserRenderInstanceBuffer.h>
#include <NxUserRenderResource.h>
#include <NxUserRenderSpriteBuffer.h>
#include <NxUserRenderSurfaceBuffer.h>
#include <NxUserRenderVertexBuffer.h>

class GGEApexRendererVertexBuffer : public physx::apex::NxUserRenderVertexBuffer
{
public:
	GGEApexRendererVertexBuffer(const physx::apex::NxUserRenderVertexBufferDesc& desc);
	virtual ~GGEApexRendererVertexBuffer(void);

	virtual void writeBuffer(const physx::apex::NxApexRenderVertexBufferData& data, physx::PxU32 firstVertex, physx::PxU32 numVerts);

	physx::PxU32 m_numVerts;
	physx::PxU32 m_numUVs;

	struct vertexData
	{
		physx::PxVec3 position;
		physx::PxVec3 normal;
		physx::PxVec2 texCoords0;
		physx::PxVec2 texCoords1;
		physx::PxVec2 texCoords2;
		physx::PxVec2 texCoords3;

	}* vertData;

	struct boneData
	{
		/*bones*/
		physx::PxU8 bonesIndices[4];


		/*bone weights*/
		physx::PxU8 boneWeights[4];

	}* vertexBoneData;

	bool m_writtenTo;
private:
	bool  m_lock;
};

class GGEApexRendererIndexBuffer : public physx::apex::NxUserRenderIndexBuffer
{
	    friend class GGEApexRendererMesh;

public:
		GGEApexRendererIndexBuffer(const physx::apex::NxUserRenderIndexBufferDesc& desc);
		virtual ~GGEApexRendererIndexBuffer(void);

		virtual bool getInteropResourceHandle(CUgraphicsResource& handle);

		virtual void writeBuffer(const void* srcData, physx::PxU32 srcStride, physx::PxU32 firstDestElement, physx::PxU32 numElements);

		enum Format
		{
			FORMAT_UINT16 = 0,
			FORMAT_UINT32 = 1,

			NUM_FORMATS
		}m_format;

		enum Hint
		{
			HINT_STATIC = 0,
			HINT_DYNAMIC = 1,
		}m_hint;

		physx::PxU32 m_maxIndices;

		struct indexData
		{
			physx::PxU32 vertexIndices[3];
		};
		struct indexData *m_indexData;

};

class GGEApexRendererInstanceBuffer : public physx::apex::NxUserRenderInstanceBuffer
{

};

class GGEApexRendererBoneBuffer : public physx::apex::NxUserRenderBoneBuffer
{
	public:

	GGEApexRendererBoneBuffer(const physx::apex::NxUserRenderBoneBufferDesc& desc);
	virtual ~GGEApexRendererBoneBuffer(void);

	physx::PxMat44* getBones() const;

	virtual void writeBuffer(const physx::apex::NxApexRenderBoneBufferData& data, physx::PxU32 firstBone, physx::PxU32 numBones);

	physx::PxMat44*				m_bones;
	physx::PxU32				m_maxBones;
	physx::PxU32				m_numBones;
};

class GGEApexRendererSpriteBuffer : public physx::apex::NxUserRenderSpriteBuffer
{
};

class GGEApexRendererMesh : public physx::apex::NxUserRenderResource
{
public:
	GGEApexRendererMesh(const physx::apex::NxUserRenderResourceDesc& desc, std::string meshName, std::string entityName);
	~GGEApexRendererMesh();


	/** \brief Set vertex buffer range */
	void setVertexBufferRange(physx::PxU32 firstVertex, physx::PxU32 numVerts);
	/** \brief Set index buffer range */
	void setIndexBufferRange(physx::PxU32 firstIndex, physx::PxU32 numIndices);
	/** \brief Set bone buffer range */
	void setBoneBufferRange(physx::PxU32 firstBone, physx::PxU32 numBones);
	/** \brief Set instance buffer range */
	void setInstanceBufferRange(physx::PxU32 firstInstance, physx::PxU32 numInstances);
	/** \brief Set sprite buffer range */
	void setSpriteBufferRange(physx::PxU32 firstSprite, physx::PxU32 numSprites);
	/** \brief Set material */
	void setMaterial(void* material);
	/** \brief Get number of vertices */
	physx::PxU32 getNumVertices(void);
	/** \brief Get number of vertices */
	physx::PxU32 getNumIndices(void);
	/** \brief Get number of vertices */
	physx::PxU32 getNumInstances(void);
	/** \brief Get number of vertex buffers */
	physx::PxU32						getNbVertexBuffers() const;
	/** \brief Get vertex buffer */
	physx::NxUserRenderVertexBuffer*	getVertexBuffer(physx::PxU32 index) const;
	/** \brief Get index buffer */
	physx::NxUserRenderIndexBuffer*		getIndexBuffer() const;
	/** \brief Get bone buffer */
	physx::NxUserRenderBoneBuffer*		getBoneBuffer() const;
	/** \brief Get instance buffer */
	physx::NxUserRenderInstanceBuffer*	getInstanceBuffer() const;
	/** \brief Get sprite buffer */
	physx::NxUserRenderSpriteBuffer*	getSpriteBuffer() const;
	/** \brief Rendering to Ogre mesh */
	void renderToOgreMesh(std::string matname);

	//Ogre Mesh Pointer
	Ogre::MeshPtr							 m_mesh;
	std::string								 m_entityName;

	Ogre::ManualObject*						m_testman;

	

protected:
	

	GGEApexRendererVertexBuffer              **m_vertexBuffers;
	physx::PxU32                             m_firstVertex;
	physx::PxU32                             m_numVertices;
	physx::PxU32							 m_numVertexBuffers;

	GGEApexRendererIndexBuffer				 *m_indexBuffer;
	physx::PxU32							 m_firstIndex;
	physx::PxU32							 m_numIndices;

	GGEApexRendererInstanceBuffer			 *m_instanceBuffer;
	physx::PxU32							 m_firstInstance;
	physx::PxU32							 m_numInstances;

	GGEApexRendererBoneBuffer		     	 *m_boneBuffer;
	physx::PxU32							 m_firstBone;
	physx::PxU32							 m_numBones;

	GGEApexRendererSpriteBuffer				 *m_spriteBuffer;
	
	physx::PxMat44							 m_meshTransform;
	physx::apex::NxRenderCullMode::Enum		 m_cullMode;

	std::string								 m_meshName;

	//sub_mesh
	Ogre::SubMesh *m_subMesh;

	//Vertex Data
	Ogre::VertexData *m_vertexData;

	//VertexDec
	Ogre::VertexDeclaration *m_vertexDecl;

};

#endif
#endif

#endif