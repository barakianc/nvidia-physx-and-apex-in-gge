#define	GGE_PROFILER1 0 //Ogre
#define	GGE_PROFILER2 1	//FPS and CPU Profiling Statistics.  Press F1 to show/hide information.
//Hold F2 + 1, 2, 3, or 4 to move each UI element around, release to set in place.  F2+5 to reset the positions.
#define	GGE_PROFILER3 0 //Vtune. Enable only if VTune Installed else DEBUG MODE will end up with error VTune DLLs not found.
//#define TBB_PARALLEL_OPTION
// Uncomment " #define _MASTER 1" in Profiler.h //It is the hard swith to disable profiling. Use if you have any problems.