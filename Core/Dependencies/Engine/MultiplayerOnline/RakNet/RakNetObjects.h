#include "Engine.h"

#include "RakPeerInterface.h"
#include "ReplicaManager3.h"
#include "GetTime.h"

#ifndef RAKNETOBJECTS_H
#define RAKNETOBJECTS_H

namespace GamePipeGame
{
	enum NetTopology { CLIENT, SERVER, P2P };
	extern NetTopology topology;
	extern NetTopology topology1;

	class BasicReplica : public RakNet::Replica3
	{
	public:
		/// base constructor
		BasicReplica();
		/// constructor taking in 4 parameters
		BasicReplica(Ogre::String newName, Ogre::String newMesh="", Ogre::String newEntity="", Ogre::Real newMass=-1);

		/// destructor
		~BasicReplica();

	protected:
		/// gets replica's name
		virtual RakNet::RakString GetName() const;
		
		//Dustin Farris: RakNet 4.033
		//virtual void WriteAllocationID(RakNet::BitStream *allocationIdBitstream) const;
		virtual void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const;

		virtual void PrintOutput(RakNet::BitStream *bs);

		/// construct the bitstream to be sent over the network
		virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
		/// read from received bitstream
		virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection);

		virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection);
		virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection);

		virtual void DeallocReplica(RakNet::Connection_RM3 *sourceConnection);

		virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters *serializeParameters);
		virtual void Deserialize(RakNet::DeserializeParameters *deserializeParameters);

		virtual void SerializeConstructionRequestAccepted(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *requestingConnection);
		virtual void DeserializeConstructionRequestAccepted(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *acceptingConnection);
		virtual void SerializeConstructionRequestRejected(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *requestingConnection);
		virtual void DeserializeConstructionRequestRejected(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *rejectingConnection);

		virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, RakNet::ReplicaManager3 *replicaManager3);
		virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection);
		virtual RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection); 
		virtual RakNet::RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const;

	public:

		Ogre::String GetObjectName();
		Ogre::String GetMeshName();
		Ogre::String GetEntityName();

		Ogre::Vector3 GetScale();
		Ogre::Real GetMass();

		Ogre::Vector3 GetPosition();
		Ogre::Vector3 GetRealPosition();
		Ogre::Vector3 GetRenderPosition();

		Ogre::Quaternion GetOrientation();
		Ogre::Quaternion GetRealOrientation();
		Ogre::Quaternion GetRenderOrientation();

		Ogre::Vector3 GetVelocity();
		Ogre::Vector3 GetRealVelocity();

		Ogre::Quaternion GetRotVelocity();
		Ogre::Quaternion GetRealRotVelocity();

		bool GetServerCorrected();									
		bool GetClientModified();									

		void SetObjectName(const char *newObjName);
		void SetMeshName(const char *newMeshName);		
		void SetEntityName(const char *newName);

		void SetScale(Ogre::Vector3 newScale);
		void SetMass(Ogre::Real newMass);

		void SetPosition(Ogre::Vector3 newPosition);
		void SetRealPosition(Ogre::Vector3 newRealPosition);
		void SetRenderPosition(Ogre::Vector3 newRenderPosition);

		void SetOrientation(Ogre::Quaternion newOrientation);
		void SetRealOrientation(Ogre::Quaternion newRealOrientation);
		void SetRenderOrientation(Ogre::Quaternion newRenderOrientation);

		void SetVelocity(float, float, float);
		void SetVelocity(Ogre::Vector3 newVelocity);
		void SetRealVelocity(float, float, float);
		void SetRealVelocity(Ogre::Vector3 newRealVelocity);

		void SetRotVelocity(Ogre::Quaternion newRotVelocity);
		void SetRealRotVelocity(Ogre::Quaternion newRealRotVelocity);

		void SetServerCorrected(bool newCorrected);
		void SetClientModified(bool newModified);

		Ogre::Vector3 UpdateRenderPosition(bool isObjectOwner);
		Ogre::Quaternion UpdateRenderOrientation(bool isObjectOwner);

	protected:

		/// object name
		Ogre::String objName;	
		/// object mesh
		Ogre::String meshName;	
		/// name of object's entity in scene
		Ogre::String entityName;				

		/// object mass, used for physics
		Ogre::Real mass;						

		/// object scale
		Ogre::Vector3 scale;					

		/// client-side position
		Ogre::Vector3 position;		
		/// server-side position
		Ogre::Vector3 realPosition;	
		/// client position used for rendering (not serialized, used for rendering)
		Ogre::Vector3 renderPosition;			

		/// client-side orientation
		Ogre::Quaternion orientation;	
		/// server-side orientation
		Ogre::Quaternion realOrientation;	
		/// client orientation used for rendering (not serialized, used for rendering)
		Ogre::Quaternion renderOrientation;		

		/// client-side linear velocity
		Ogre::Vector3 velocity;		
		/// server-side linear velocity
		Ogre::Vector3 realVelocity;				

		/// client-side rotational velocity
		Ogre::Quaternion rotVelocity;	
		/// server-side rotational velocity
		Ogre::Quaternion realRotVelocity;		

		/// flag indicating server-side correction (not serialized, used to determine serialization)
		bool serverCorrected;	
		/// flag indicating client-side correction (not serialized, used to determine serialization)
		bool clientModified;					
		//RakNetTime lastUpdateServer;			// time of last update by/from the server (not serialized, used for interpolation/bounds)
		//RakNetTime lastUpdateClient;			// time of last update by/from the client (not serialized, used for interpolation/bounds)

		// raknet 4.0b8
		/// time of last update by/from the server (not serialized, used for interpolation/bounds)
		RakNet::Time lastUpdateServer;	
		/// time of last update by/from the client (not serialized, used for interpolation/bounds)
		RakNet::Time lastUpdateClient;			
	};

	class GamePipeRMConnection : public RakNet::Connection_RM3
	{
	public:
		//GamePipeRMConnection(SystemAddress _systemAddress, RakNetGUID _guid);

		//raknet 4.0b8
		GamePipeRMConnection(RakNet::SystemAddress _systemAddress, RakNet::RakNetGUID _guid);
		virtual ~GamePipeRMConnection();
		virtual RakNet::Replica3 *AllocReplica(RakNet::BitStream *allocationId, RakNet::ReplicaManager3 *replicaManager3);
	};

	class GamePipeReplicaManager : public RakNet::ReplicaManager3
	{
	public:
		//virtual RakNet::Connection_RM3 *AllocConnection(SystemAddress systemAddress, RakNetGUID rakNetGUID) const;
		
		//raknet 4.0b8
		//virtual RakNet::Connection_RM3 *AllocConnection(RakNet::SystemAddress systemAddress, RakNet::RakNetGUID rakNetGUID) const;
		//RakNet 4.033
		virtual RakNet::Connection_RM3 *AllocConnection(const RakNet::SystemAddress &systemAddress, RakNet::RakNetGUID rakNetGUID) const;
		virtual void DeallocConnection(RakNet::Connection_RM3 *connection) const;
	};
}

#endif
