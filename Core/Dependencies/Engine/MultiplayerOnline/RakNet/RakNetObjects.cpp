#include "StdAfx.h"
#include <string>
#include <iostream>

#include "RakNetObjects.h"
#include "GameObjectReplica.h"
#include "PhysXGameObjectReplica.h"

using namespace std;

namespace GamePipeGame
{
	NetTopology topology1;
	/// BasicReplica - Constructor
	BasicReplica::BasicReplica()
		: 	objName("DefaultGameObjectReplica"), meshName("ogrehead.mesh"), entityName(""), mass(0), 
			scale(Ogre::Vector3::UNIT_SCALE), 
			position(Ogre::Vector3::ZERO), realPosition(Ogre::Vector3::ZERO), renderPosition(Ogre::Vector3::ZERO), 
			orientation(Ogre::Quaternion::IDENTITY), realOrientation(Ogre::Quaternion::IDENTITY), renderOrientation(Ogre::Quaternion::IDENTITY), 
			velocity(Ogre::Vector3::ZERO), realVelocity(Ogre::Vector3::ZERO), 
			rotVelocity(Ogre::Quaternion::IDENTITY), realRotVelocity(Ogre::Quaternion::IDENTITY), 
			serverCorrected(false), clientModified(false),
			lastUpdateServer(0), lastUpdateClient(0)
	{

	}

	/// BasicReplica - Constructor
	///
	/// Parameters:
	/// newName - a name for this object.  Does not have to be unique.
	/// newMesh - the filename of the mesh for this object.  Default value is empty.
	/// newEntity - an entity name for this object, which would likely be determined later anyway.  Default value is empty.
	/// newMass - a value for an object's weight.  Default value is zero.

	BasicReplica::BasicReplica(Ogre::String newName, Ogre::String newMesh, Ogre::String newEntity, Ogre::Real newMass)
		:	objName(newName), meshName(newMesh), entityName(newEntity), mass(newMass), 
			scale(Ogre::Vector3::UNIT_SCALE), 
			position(Ogre::Vector3::ZERO), realPosition(Ogre::Vector3::ZERO), renderPosition(Ogre::Vector3::ZERO), 
			orientation(Ogre::Quaternion::IDENTITY), realOrientation(Ogre::Quaternion::IDENTITY), renderOrientation(Ogre::Quaternion::IDENTITY), 
			velocity(Ogre::Vector3::ZERO), realVelocity(Ogre::Vector3::ZERO), 
			rotVelocity(Ogre::Quaternion::IDENTITY), realRotVelocity(Ogre::Quaternion::IDENTITY), 
			serverCorrected(false), clientModified(false),
			lastUpdateServer(0), lastUpdateClient(0)
	{

	}

	/// BasicReplica - Destructor
	BasicReplica::~BasicReplica()
	{
	
	
	}

	/// GetName - Returns a print out of a name, suitable for framing.
	/// Virtual, redefined from Replica3.

	RakNet::RakString 
	BasicReplica::GetName() const
	{
		Ogre::String ogreString = "Class: BasicReplica, Name: ";
		ogreString += objName;

		return RakNet::RakString(ogreString.c_str());
	}

	/// WriteAllocationID - Writes a unique string to be used by the replica factory.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// allocationIdBitstream - a RakNet BitStream where the allocation string will be written to (and later read from by the factory)

	void
	//Dustin Farris: RakNet 4.033
	//BasicReplica::WriteAllocationID(RakNet::BitStream *allocationIdBitstream) const
	BasicReplica::WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const
	{
		Ogre::String className = "BasicReplica";
		allocationIdBitstream->Write(className.c_str());
	}


	/// PrintOutput - Prints the contents of a BitStream, assumed to be a string type.
	/// Virtual, redefined from Replica3.

	void
	BasicReplica::PrintOutput(RakNet::BitStream *bs)
	{
		if(bs->GetNumberOfBitsUsed() == 0)
			return;
		RakNet::RakString rakString;
		bs->Read(rakString);
	}

	/// SerializeConstruction - Serializes data for a remote object reference/creation call.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// constructionBitstream - a RakNet BitStream containing the data to be used in the object's remote construction
	/// destinationConnection -	the connection the remote object will be created on (in case special serialization is needed)

	void
	BasicReplica::SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		//	cout << "[BasicReplica/SerializeConstruction] in SerializeConstruction()" << endl;
		constructionBitstream->Write(GetName() + RakNet::RakString(" SerializeConstruction"));

		constructionBitstream->Write(RakNet::RakString(objName.c_str()));
		constructionBitstream->Write(RakNet::RakString(meshName.c_str()));

		constructionBitstream->Write(RakNet::RakString(Ogre::StringConverter::toString(mass).c_str()));

		if(topology == SERVER)
		{
			entityName = objName;
			entityName.append(".");
			//entityName.append(Ogre::StringConverter::toString((int)GetNetworkID().localSystemAddress));
			//raknet 4.0b8
			entityName.append(Ogre::StringConverter::toString((int)GetNetworkID()));
		}
		else
		{

		}

	}

	/// DeserializeConstruction - Reads and processes incoming data from a remote object reference/creation call.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// constructionBitstream - a RakNet BitStream containing the data to be used in the object's remote construction
	/// sourceConnection -	the connection calling for the construction of the object represented by this replica

	bool
	BasicReplica::DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		//	cout << "[BasicReplica/DeserializeConstruction] in DeserializeConstruction()" << endl;

		RakNet::RakString rakString;
		RakNet::RakString rakStringObjectName;
		RakNet::RakString rakStringMeshName;
		RakNet::RakString rakStringMass;

		constructionBitstream->Read(rakString);
		constructionBitstream->Read(rakStringObjectName);
		constructionBitstream->Read(rakStringMeshName);
		constructionBitstream->Read(rakStringMass);

		// rakString is just a throwaway string
		objName = rakStringObjectName.C_String();
		meshName = rakStringMeshName.C_String();
		mass = (Ogre::Real)Ogre::StringConverter::parseInt(rakStringMass.C_String());

		if(topology == CLIENT)
		{
			entityName = objName;
			entityName.append(".");
			//entityName.append(Ogre::StringConverter::toString((int)GetNetworkID().localSystemAddress));
			//raknet 4.0b8
			entityName.append(Ogre::StringConverter::toString((int)GetNetworkID()));
		}
		else
		{

		}

		return true;
	}

	/// SerializeDestruction - Writes additional data for use with destroying remote objects.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// destructionBitstream - the RakNet BitStream containing the extra destruction data
	/// destinationConnection - the connection receiving the extra destruction data

	void
	BasicReplica::SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		//	cout << "[BasicReplica/SerializeDestruction] in SerializeDestruction()" << endl;
		destructionBitstream->Write(GetName() + RakNet::RakString(" SerializeDestruction"));
	}

	/// DeserializeDestruction - Reads and processes additional data for use with destroying remote objects.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// destructionBitstream - the RakNet BitStream containing the extra destruction data
	/// sourceConnection - the connection that sent the extra destruction data

	bool
	BasicReplica::DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		cout << "[BasicReplica/DeserializeDestruction] in DeserializeDestruction()" << endl;
		PrintOutput(destructionBitstream);
		return true;
	}

	/// DeallocReplica - Determines what should be done when a replica should be deleted.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// sourceConnection -	the connection calling for the destruction of the object represented by this replica

	void
	BasicReplica::DeallocReplica(RakNet::Connection_RM3 *sourceConnection)
	{
		cout << "[BasicReplica/DeallocReplica] in DeallocReplica()" << endl;
		delete this;
	}

	/// Serialize - Writes data to a Bitstream to be sent to a remote system.
	/// Virtual, redefined from Replica3.
	///
	/// If allowed to by the returned result of QuerySerialize, this function runs every call to replicaManager->Update(),
	///	for every object, for every connection.
	///
	/// Parameters:
	/// serializeParameters - a RakNet SerializeParameters class containing the serialized information to be sent

	RakNet::RM3SerializationResult
	BasicReplica::Serialize(RakNet::SerializeParameters *serializeParameters)
	{
		//	cout << "[BasicReplica/Serialize] In Serialize(), NetworkID = " << GetNetworkID().localSystemAddress << endl;

		if(topology == CLIENT)
		{
			//RakNetTime diffUpdateTime = RakNet::GetTime() - lastUpdateClient;

			//raknet 4.0b8
			RakNet::Time diffUpdateTime = RakNet::GetTime() - lastUpdateClient;
			if(!clientModified && diffUpdateTime < 1000)
			{
				//	cout << "[BasicReplica/Serialize] CLIENT: no local modification, should skip serialization this tick" << endl;
				return RakNet::RM3SR_DO_NOT_SERIALIZE;
			}
			else
			{
				//	cout << "[BasicReplica/Serialize] CLIENT: locally modified, should serialize to server and reset modification flag" << endl;
				clientModified = false;
			}
		}

		//RakNetTime time = RakNet::GetTime()/1000;

		//raknet 4.0b8
		RakNet::Time time = RakNet::GetTime()/1000;

		serializeParameters->outputBitstream[0].Write(time);

		serializeParameters->outputBitstream[0].Write(position.x);
		serializeParameters->outputBitstream[0].Write(position.y);
		serializeParameters->outputBitstream[0].Write(position.z);

		serializeParameters->outputBitstream[0].Write(realPosition.x);
		serializeParameters->outputBitstream[0].Write(realPosition.y);
		serializeParameters->outputBitstream[0].Write(realPosition.z);

		serializeParameters->outputBitstream[0].Write(orientation.x);
		serializeParameters->outputBitstream[0].Write(orientation.y);
		serializeParameters->outputBitstream[0].Write(orientation.z);
		serializeParameters->outputBitstream[0].Write(orientation.w);

		serializeParameters->outputBitstream[0].Write(realOrientation.x);
		serializeParameters->outputBitstream[0].Write(realOrientation.y);
		serializeParameters->outputBitstream[0].Write(realOrientation.z);
		serializeParameters->outputBitstream[0].Write(realOrientation.w);

		serializeParameters->outputBitstream[0].Write(velocity.x);
		serializeParameters->outputBitstream[0].Write(velocity.y);
		serializeParameters->outputBitstream[0].Write(velocity.z);

		serializeParameters->outputBitstream[0].Write(realVelocity.x);
		serializeParameters->outputBitstream[0].Write(realVelocity.y);
		serializeParameters->outputBitstream[0].Write(realVelocity.z);

		serializeParameters->outputBitstream[0].Write(rotVelocity.x);
		serializeParameters->outputBitstream[0].Write(rotVelocity.y);
		serializeParameters->outputBitstream[0].Write(rotVelocity.z);
		serializeParameters->outputBitstream[0].Write(rotVelocity.w);

		serializeParameters->outputBitstream[0].Write(realRotVelocity.x);
		serializeParameters->outputBitstream[0].Write(realRotVelocity.y);
		serializeParameters->outputBitstream[0].Write(realRotVelocity.z);
		serializeParameters->outputBitstream[0].Write(realRotVelocity.w);

		serializeParameters->messageTimestamp = RakNet::GetTime();			// set a non-zero value so the timestamp is passed

		if(topology == CLIENT)
			lastUpdateClient = RakNet::GetTime();
		else
			lastUpdateServer = RakNet::GetTime();

		return RakNet::RM3SR_BROADCAST_IDENTICALLY;
	}

	/// Deserialize - Reads and processes incoming data from a remote serialization.
	///	Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// deserializeParameters - a RakNet DeserializeParameters class containing the serialized information to be read

	void
	BasicReplica::Deserialize(RakNet::DeserializeParameters *deserializeParameters)
	{
		//	cout << "[BasicReplica/Deserialize] Deserialize for object ID = " << GetNetworkID().localSystemAddress << endl;

		//RakNetTime dsTime;

		//raknet 4.0b8
		RakNet::Time dsTime;
		Ogre::Vector3 newPosition, newRealPosition;
		Ogre::Quaternion newOrientation, newRealOrientation;
		Ogre::Vector3 newVelocity, newRealVelocity;
		Ogre::Quaternion newRotVelocity, newRealRotVelocity;

		// Read in the values from the serialization string here.  The order has to match the original serialization order.

		deserializeParameters->serializationBitstream[0].Read(dsTime);

		deserializeParameters->serializationBitstream[0].Read(newPosition.x);
		deserializeParameters->serializationBitstream[0].Read(newPosition.y);
		deserializeParameters->serializationBitstream[0].Read(newPosition.z);

		deserializeParameters->serializationBitstream[0].Read(newRealPosition.x);
		deserializeParameters->serializationBitstream[0].Read(newRealPosition.y);
		deserializeParameters->serializationBitstream[0].Read(newRealPosition.z);

		deserializeParameters->serializationBitstream[0].Read(newOrientation.x);
		deserializeParameters->serializationBitstream[0].Read(newOrientation.y);
		deserializeParameters->serializationBitstream[0].Read(newOrientation.z);
		deserializeParameters->serializationBitstream[0].Read(newOrientation.w);

		deserializeParameters->serializationBitstream[0].Read(newRealOrientation.x);
		deserializeParameters->serializationBitstream[0].Read(newRealOrientation.y);
		deserializeParameters->serializationBitstream[0].Read(newRealOrientation.z);
		deserializeParameters->serializationBitstream[0].Read(newRealOrientation.w);

		deserializeParameters->serializationBitstream[0].Read(newVelocity.x);
		deserializeParameters->serializationBitstream[0].Read(newVelocity.y);
		deserializeParameters->serializationBitstream[0].Read(newVelocity.z);

		deserializeParameters->serializationBitstream[0].Read(newRealVelocity.x);
		deserializeParameters->serializationBitstream[0].Read(newRealVelocity.y);
		deserializeParameters->serializationBitstream[0].Read(newRealVelocity.z);

		deserializeParameters->serializationBitstream[0].Read(newRotVelocity.x);
		deserializeParameters->serializationBitstream[0].Read(newRotVelocity.y);
		deserializeParameters->serializationBitstream[0].Read(newRotVelocity.z);
		deserializeParameters->serializationBitstream[0].Read(newRotVelocity.w);

		deserializeParameters->serializationBitstream[0].Read(newRealRotVelocity.x);
		deserializeParameters->serializationBitstream[0].Read(newRealRotVelocity.y);
		deserializeParameters->serializationBitstream[0].Read(newRealRotVelocity.z);
		deserializeParameters->serializationBitstream[0].Read(newRealRotVelocity.w);

		if(topology == SERVER)
		{
			lastUpdateClient = RakNet::GetTime();
			if(true)												// sanity check the position update sent from the client
			{
				/* Reject changes that were a result of a previous server update being relayed back to the server somehow */

				if(newPosition != newRealPosition)
					position = realPosition = newPosition;

				if(newOrientation != newRealOrientation)
					orientation = realOrientation = newOrientation;

				if(newVelocity != newRealVelocity)
					velocity = realVelocity = newVelocity;

				if(newRotVelocity != newRealRotVelocity)
					rotVelocity = realRotVelocity = newRotVelocity;
			}
			else
				serverCorrected = true;
		}
		else
		{
			lastUpdateServer = RakNet::GetTime();

			if(realPosition != newRealPosition || realOrientation != newRealOrientation || velocity != newRealVelocity || rotVelocity != newRotVelocity)
			{

				realPosition = newRealPosition;
				realOrientation = newRealOrientation;
				realVelocity = newRealVelocity;
				realRotVelocity = newRealRotVelocity;

				//	A client updates the local client values if at least one of the following is true:
				//		1.	the squared distance between the current client position and the position update from the server is greater than 1
				//		2.	the difference between the current client orientation and the orientation update from the server is more than 10 degrees (0.1745 rads)
				//		3.	this client is not the owner of the object (replica->creatingSystemGUID vs replicaManager->GRPI()->GetGUID)

				if(position.squaredDistance(realPosition) > 1 || !orientation.equals(realOrientation, Ogre::Radian((Ogre::Real)0.1745)) || 
					//creatingSystemGUID != replicaManager->GetRakPeerInterface()->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS))
					//raknet 4.0b8
					creatingSystemGUID != replicaManager->GetRakPeerInterface()->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS))
				{
					position = realPosition;
					orientation = realOrientation;
					velocity = realVelocity;
					rotVelocity = realRotVelocity;
				}
			}
		}
	}

	/// SerializeConstructionRequestAccepted - Writes addtional data to be sent to a system whose object creation requestion has been accepted.
	/// Virtual, redefined from Replica3.
	///
	/// This function is a possible response by a remote system from a local system's call to SerializeConstruction()
	///
	/// Parameters:
	/// serializationBitstream - the RakNet BitStream containing the extra data to include in the accept
	/// requestingConnection - a reference to the connection that previously called SerializeConstruction()

	void
	BasicReplica::SerializeConstructionRequestAccepted(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *requestingConnection)
	{
		//	cout << "[BasicReplica/SCRA] in SerializeConstructionRequestAccepted()" << endl;
		serializationBitstream->Write(GetName() + RakNet::RakString(" SerializeConstructionRequestAccepted"));

		entityName = objName;
		entityName.append(".");
		//entityName.append(Ogre::StringConverter::toString((int)GetNetworkID().localSystemAddress));
		//raknet 4.0b8
		entityName.append(Ogre::StringConverter::toString((int)GetNetworkID()));
	}

	/// DeserializeConstructionRequestAccepted - Reads and processes addtional data sent from a system accepting remote object creation.
	/// Virtual, redefined from Replica3.
	///
	/// This function is a possible response by a remote system from a local system's call to SerializeConstruction(), and also returns
	///	a valid network object ID if it is a network ID authority.
	///
	/// Parameters:
	/// serializationBitstream - the RakNet BitStream containing the extra data to include in the accept
	/// acceptingConnection - a reference to the connection that approved the call to SerializeConstruction()

	void
	BasicReplica::DeserializeConstructionRequestAccepted(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *acceptingConnection)
	{
		//	cout << "[BasicReplica/DCRA] in DeserializeConstructionRequestAccepted()" << endl;

		entityName = objName;
		entityName.append(".");
		//entityName.append(Ogre::StringConverter::toString((int)GetNetworkID().localSystemAddress));
		//raknet 4.0b8
		entityName.append(Ogre::StringConverter::toString((int)GetNetworkID()));
		//	PrintOutput(serializationBitstream);
	}

	/// SerializeConstructionRequestRejected - Writes addtional data to be sent to a system whose object creation requestion has been rejected.
	/// Virtual, redefined from Replica3.
	///
	/// This function is a possible response by a remote system from a local system's call to SerializeConstruction()
	///
	/// Parameters:
	/// serializationBitstream - the RakNet BitStream containing the extra data to include in the accept
	/// requestingConnection - a reference to the connection that previously called SerializeConstruction()

	void
	BasicReplica::SerializeConstructionRequestRejected(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *requestingConnection)
	{
		//	cout << "[BasicReplica/SCRR] in SerializeConstructionRequestRejected()" << endl;
		serializationBitstream->Write(GetName() + RakNet::RakString(" SerializeConstructionRequestRejected"));
	}

	/// DeserializeConstructionRequestRejected - Reads and processes addtional data sent from a system rejecting remote object creation.
	/// Virtual, redefined from Replica3.
	///
	/// This function is a possible response by a remote system from a local system's call to SerializeConstruction()
	///
	/// Parameters:
	/// serializationBitstream - the RakNet BitStream containing the extra data to include in the accept
	/// rejectingConnection - a reference to the connection that rejected the call to SerializeConstruction()

	void
	BasicReplica::DeserializeConstructionRequestRejected(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *rejectingConnection)
	{
		//	cout << "[BasicReplica/DCRR] in DeserializeConstructionRequestRejected()" << endl;
		//	PrintOutput(serializationBitstream);
	}

	/// QueryConstruction - Determines whether this object should be created/destroyed on a remote system if it does not/does 
	///	exist on a remote system.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// destinationConnection - the connection the object will be created/destroyed on
	/// replicaManager3 - a reference to the replica manager

	RakNet::RM3ConstructionState
	BasicReplica::QueryConstruction(RakNet::Connection_RM3 *destinationConnection, RakNet::ReplicaManager3 *replicaManager3)
	{
		//	cout << "[BasicReplica/QueryConstruction] in QueryConstruction()" << endl;
		//return QueryConstruction_ClientConstruction(destinationConnection);

		//raknet 4.0b8
		return QueryConstruction_ClientConstruction(destinationConnection, topology==SERVER);
	}

	/// QueryRemoteConstruction - Determines whether this object can be created by a specific connection.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// sourceConnection - the connection attempting to create the shared object.

	bool
	BasicReplica::QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection)
	{
		//	cout << "[BasicReplica/QueryRemoteConstruction] in QueryRemoteConstruction()" << endl;
		//return QueryRemoteConstruction_ClientConstruction(sourceConnection);

		//raknet 4.0b8
		return QueryRemoteConstruction_ClientConstruction(sourceConnection, topology==SERVER);
	}

	/// QuerySerialization - Determine whether this object should be serialized to the specified connection.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// destinationConnection - the connection the object would be serialized to.

	RakNet::RM3QuerySerializationResult
	BasicReplica::QuerySerialization(RakNet::Connection_RM3 *destinationConnection)
	{

		//	cout << "[BasicReplica/QuerySerialization] in QuerySerialization()" << endl;

		//	If this is the client, allow serialization of objects owned by this client (ie., call ClientSerializable())
		//	Otherwise, this is a server or a peer:
		//		If the destination connection isn't the owner of the object, call serialize so it can be relayed
		//		Otherwise, the destination connection IS the owner of the object:
		//			If the object has been corrected by the server:
		//				1.	Reset the server-corrected flag
		//				2.	Call serialize on the object, to its owner
		//	By default, don't serialize.

		if(topology == CLIENT)
		{
			//return QuerySerialization_ClientSerializable(destinationConnection);

			//raknet 4.0b8
			return QuerySerialization_ClientSerializable(destinationConnection, topology==SERVER);
		}

		//	cout << "[BasicReplica/QuerySerialization] SERVER: object = " << entityName << ", serverCorrected = " << serverCorrected << endl;

		if(destinationConnection->GetRakNetGUID() != creatingSystemGUID)
			return RakNet::RM3QSR_CALL_SERIALIZE;

		if(serverCorrected)
		{
			serverCorrected = false;						// reset the server-corrected flag so we don't keep sending "updates"
			return RakNet::RM3QSR_CALL_SERIALIZE;			// serialize it back to the owner
		}

		return RakNet::RM3QSR_DO_NOT_CALL_SERIALIZE;		// default: do not serialize to this connection
	}

	/// QueryActionOnPopConnection - Determines what action to take with replicas belonging to a manager whose connection is lost.
	/// Virtual, redefined from Replica3.
	///
	/// Parameters:
	/// droppedConnection - a reference to the lost connection 

	RakNet::RM3ActionOnPopConnection
	BasicReplica::QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const
	{
		//	cout << "[BasicReplica/QAOPC] in QueryActionOnPopConnection()" << endl;

		//	If this is a server, destroy the corresponding local objects and broadcast destruction of the objects to connected clients
		//	If this is a client, only destroy the corresponding local objects

		if(topology == SERVER) return QueryActionOnPopConnection_Server(droppedConnection);
		if(topology == CLIENT) return QueryActionOnPopConnection_Client(droppedConnection);
		
		return QueryActionOnPopConnection_Client(droppedConnection);
	}

	/// GetObjectName - Gets the replica's associated object name.  This does not have to be a unique name.
	Ogre::String BasicReplica::GetObjectName()	{ return objName; }
	/// GetMeshName - Gets the replica's associated mesh file name (eg., "object.mesh").
	Ogre::String BasicReplica::GetMeshName()	{ return meshName; }
	/// GetEntityName - Gets the replica's associated mesh entity name.  This should be a unique name.
	Ogre::String BasicReplica::GetEntityName()	{ return entityName; }

	/// GetScale - Gets the object's scale size
	Ogre::Vector3 BasicReplica::GetScale() { return scale; }
	/// GetMass - Gets the object's mass
	Ogre::Real BasicReplica::GetMass() { return mass; }

	/// GetPosition - Gets the object's client-side position
	Ogre::Vector3 BasicReplica::GetPosition() { return position; }
	/// GetRealPosition - Gets the object's server-side position
	Ogre::Vector3 BasicReplica::GetRealPosition()	{ return realPosition; }
	/// GetRenderPosition - Gets the object's position used for rendering
	Ogre::Vector3 BasicReplica::GetRenderPosition() { return renderPosition; }

	/// GetOrientation - Gets the object's client-side orientation
	Ogre::Quaternion BasicReplica::GetOrientation() { return orientation; }
	/// GetRealOrientation - Gets the object's server-side orientation
	Ogre::Quaternion BasicReplica::GetRealOrientation() { return realOrientation; }
	/// GetRenderOrientation - Gets the object's orientation used for rendering
	Ogre::Quaternion BasicReplica::GetRenderOrientation() { return renderOrientation; }

	/// GetVelocity - Gets the object's client-side linear velocity
	Ogre::Vector3 BasicReplica::GetVelocity() { return velocity; }
	/// GetRealVelocity - Gets the object's server-side linear velocity
	Ogre::Vector3 BasicReplica::GetRealVelocity() { return realVelocity; }

	/// GetRotVelocity - Gets the object's client-side rotational velocity (quaternion)
	Ogre::Quaternion BasicReplica::GetRotVelocity() { return rotVelocity; }
	/// GetRealRotVelocity - Gets the object's server-side rotational velocity (quaternion)
	Ogre::Quaternion BasicReplica::GetRealRotVelocity() { return realRotVelocity; }

	/// GetServerCorrected - Gets the flag indicating whether the replica was corrected by the server
	bool BasicReplica::GetServerCorrected() { return serverCorrected; }
	/// GetClientModified - Gets the flag indicating whether the replica was modified by the client
	bool BasicReplica::GetClientModified() { return clientModified; }

	/// SetObjectName - Sets the replica's associated object name.  This does not have to be a unique name.
	void BasicReplica::SetObjectName(const char *newObjName) { objName.assign(newObjName); }
	/// SetMeshName - Sets the replica's associated mesh file name (eg., "object.mesh").
	void BasicReplica::SetMeshName(const char *newMesh) { meshName.assign(newMesh); }
	/// SetEntityName - Sets the replica's associated mesh entity name.  This should be a unique name.
	void BasicReplica::SetEntityName(const char *newName) {	entityName.assign(newName);	}

	/// SetScale - Sets the object's scale size
	void BasicReplica::SetScale(Ogre::Vector3 newScale) { scale = newScale; }
	/// SetMass - Sets the object's mass
	void BasicReplica::SetMass(Ogre::Real newMass) { mass = newMass; }

	/// SetPosition - Sets the object's client-side position
	void BasicReplica::SetPosition(Ogre::Vector3 newPosition) { position = newPosition; }
	/// SetRealPosition - Sets the object's server-side position
	void BasicReplica::SetRealPosition(Ogre::Vector3 newRealPosition) { realPosition = newRealPosition; }
	/// SetRenderPosition - Sets the object's position used for rendering
	void BasicReplica::SetRenderPosition(Ogre::Vector3 newRenderPosition) { renderPosition = newRenderPosition; } 

	/// SetOrientation - Sets the object's client-side orientation
	void BasicReplica::SetOrientation(Ogre::Quaternion newOrientation) { orientation = newOrientation; }
	/// SetRealOrientation - Sets the object's server-side orientation
	void BasicReplica::SetRealOrientation(Ogre::Quaternion newRealOrientation) { realOrientation = newRealOrientation; }
	/// SetRenderOrientation - Sets the object's orientation used for rendering
	void BasicReplica::SetRenderOrientation(Ogre::Quaternion newRenderOrientation) { renderOrientation = newRenderOrientation; }

	/// SetVelocity - Sets the object's client-side linear velocity.  Uses 3 floats.
	void BasicReplica::SetVelocity(float x, float y, float z) { velocity = Ogre::Vector3(x, y, z); }
	/// SetVelocity - Sets the object's client-side linear velocity.  Uses a Vector3.
	void BasicReplica::SetVelocity(Ogre::Vector3 newVelocity) { velocity = newVelocity; }

	/// SetRealVelocity - Sets the object's server-side linear velocity.  Uses 3 floats.
	void BasicReplica::SetRealVelocity(float x, float y, float z) { realVelocity = Ogre::Vector3(x, y, z); }
	/// SetRealVelocity - Sets the object's server-side linear velocity.  Uses a Vector3.
	void BasicReplica::SetRealVelocity(Ogre::Vector3 newRealVelocity) { realVelocity = newRealVelocity; }

	/// SetRotVelocity - Sets the object's client-side rotational velocity (quaternion)
	void BasicReplica::SetRotVelocity(Ogre::Quaternion newRotVelocity) { rotVelocity = newRotVelocity; }
	/// SetRealRotVelocity - Sets the object's server-side rotational velocity (quaternion)
	void BasicReplica::SetRealRotVelocity(Ogre::Quaternion newRealRotVelocity) { realRotVelocity = newRealRotVelocity; }

	/// SetServerCorrected - Sets the flag indicating whether the replica was corrected by the server
	void BasicReplica::SetServerCorrected(bool newCorrected) { serverCorrected = newCorrected; }
	/// SetClientModified - Sets the flag indicating whether the replica was modified by the client
	void BasicReplica::SetClientModified(bool newModified) { clientModified = newModified; }

	/// UpdateRenderOrientation - Updates the display position of an entity
	///
	/// Parameters:
	/// isObjectOwner - a flag indicating whether this replica is owned by this system
	///
	/// Returns: an Ogre Vector3 representing the new rendering position

	Ogre::Vector3
	BasicReplica::UpdateRenderPosition(bool isObjectOwner)
	{
		if(isObjectOwner)
			return renderPosition = position;

		// modify the render position to smooth out the transitions
		Ogre::Vector3 renderPositionDifference = renderPosition - realPosition;

		if(renderPositionDifference.squaredLength() > 1.0)
			renderPosition = realPosition;
		else
			renderPosition += renderPositionDifference * 0.1f;

		return renderPosition;
	}

	/// UpdateRenderOrientation - Updates the display orientation of an entity
	///
	/// Parameters:
	/// isObjectOwner - a flag indicating whether this replica is owned by this system
	///
	/// Returns: an Ogre Vector3 representing the new rendering orientation

	Ogre::Quaternion
	BasicReplica::UpdateRenderOrientation(bool isObjectOwner)
	{
		if(isObjectOwner)
			return renderOrientation = orientation;

		// modify the render orientation to smooth out the transitions
		renderOrientation = Ogre::Quaternion::Slerp((Ogre::Real) 0.1f, orientation, realOrientation, true);

		return renderOrientation;
	}


	/// ~GamePipeRMConnection - Constructor
	//GamePipeRMConnection::GamePipeRMConnection(SystemAddress _systemAddress, RakNetGUID _guid) : RakNet::Connection_RM3(_systemAddress, _guid) {}

	//raknet 4.0b8
	GamePipeRMConnection::GamePipeRMConnection(RakNet::SystemAddress _systemAddress, RakNet::RakNetGUID _guid) : RakNet::Connection_RM3(_systemAddress, _guid) {}


	/// ~GamePipeRMConnection - Destructor
	GamePipeRMConnection::~GamePipeRMConnection() {}

	/// AllocReplica - Factory function for creating new replica objects, used by the replica manager.
	/// Virtual, redefined from Connection_RM3.
	///
	/// Parameters:
	/// allocationID - a RakNet BitStream containing the allocation data used to determine what kind of replica to create
	///	replicaManager3 - a reference to the containing replica manager

	
	RakNet::Replica3 *
	GamePipeRMConnection::AllocReplica(RakNet::BitStream *allocationId, RakNet::ReplicaManager3 *replicaManager3)
	{
		//	cout << "[GamePipeRMConnection/AllocReplica] in AllocReplica()" << endl;
		RakNet::RakString typeName;

		allocationId->Read(typeName);
		/*** Dustin-Cloud Gaming: Commented out return statements ***/
		if (typeName=="BasicReplica") return new BasicReplica;
		if (typeName=="GameObjectReplica") return new GameObjectReplica;

			cout << "[GamePipeRMConnection/AllocReplica] read typeName: " << typeName << endl;
		return new BasicReplica;					// we could also create other objects based on the typeName
		return 0;
	}

	/// AllocConnection - Factory function for creating new connection objects, used by the replica manager.
	/// Virtual, redefined from ReplicaManager3.
	///
	/// Parameters:
	/// systemAddress - an object containing the IP address and port number of the remote system
	///	rakNetGUID - a unique GUID for the connection

	RakNet::Connection_RM3 * 
	//GamePipeReplicaManager::AllocConnection(SystemAddress systemAddress, RakNetGUID rakNetGUID) const
	
	//raknet 4.0b8
	//GamePipeReplicaManager::AllocConnection(RakNet::SystemAddress systemAddress, RakNet::RakNetGUID rakNetGUID) const
	//{
		//	cout << "[GamePipeReplicaManager/AllocConnection] in AllocConnection()" << endl;
		//return new GamePipeRMConnection(systemAddress, rakNetGUID);
	//}
	// Dustin Farris: RakNet 4.033
	GamePipeReplicaManager::AllocConnection(const RakNet::SystemAddress &systemAddress, RakNet::RakNetGUID rakNetGUID) const
	{
		//	cout << "[GamePipeReplicaManager/AllocConnection] in AllocConnection()" << endl;
		return new GamePipeRMConnection(systemAddress, rakNetGUID);
	}

	/// DeallocConnection - Runs additional processing for dealing with connections that have been lost or disconnected.
	/// Virtual, redefined from ReplicaManager3.
	///
	/// Parameters:
	/// connection - the connection being destroyed due to disconnection

	void
	GamePipeReplicaManager::DeallocConnection(RakNet::Connection_RM3 *connection) const
	{
		//	cout << "[GamePipeReplicaManager/DeallocConnection] in DeallocConnection()" << endl;
		delete connection;
	}

}	// end namespace GamePipeGame