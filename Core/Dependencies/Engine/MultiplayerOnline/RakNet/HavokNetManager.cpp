#include "StdAfx.h"

#ifdef HAVOK

#include "HavokNetManager.h"

namespace GamePipeGame
{

	HavokNetManager::HavokNetManager(CustomPacketHandler pCustomPacketHandler /*= 0*/)
		: NetManager(pCustomPacketHandler), 
			configFile(NULL)
	{

	}

	HavokNetManager::~HavokNetManager()
	{
		managerStatus = DESTROYING;
	}


	/// SetConfigFile - Sets a pointer to an Ogre ConfigFile object, used for storing values from a configuration file
	/// 
	///	Returns:
	/// NETMANAGER_FAILURE, if the pointer to the ConfigFile is NULL.
	/// NETMANAGER_SUCCESS, otherwise.

	int
	HavokNetManager::SetConfigFile(Ogre::ConfigFile *newConfigFile)
	{
		if(newConfigFile == NULL) return NETMANAGER_FAILURE;

		configFile = newConfigFile;
		return NETMANAGER_SUCCESS;
	}


	/// UpdateNetworkObjectsInSceneWithHavok - Updates the local replicas with Havok information.
	///
	/// This function is used primarily to create new Havok GameObjects for replicas that have already been issued a GUID by the
	/// system that has been designated as the Network ID Authority.  This function also assumed that the related Ogre objects
	/// will be created by the Havok GameObject constructor.
	/// 
	///	Returns:
	/// NETMANAGER_FAILURE, if the pointer to the Ogre scene manager or the Havok GameObjectManager are NULL.
	/// NETMANAGER_SUCCESS, otherwise.

	int
	HavokNetManager::UpdateNetworkObjectsInSceneWithHavok(Ogre::SceneManager *pSceneManager, EntityMapType &entityMap, GameObjectManager *havokManager)
	{
		if(pSceneManager == NULL || havokManager == NULL)
			return NETMANAGER_FAILURE;

		DataStructures::List<RakNet::Replica3*> fullReplicaList;
//		DataStructures::Multilist<ML_STACK, RakNet::Replica3 *> fullReplicaList;
		replicaManager.GetReferencedReplicaList(fullReplicaList);

		//	For each replica in the replica manager:
		//		1.	If the entity name has not yet been set:
		//				If this is the server/peer, create the entity name
		//				Otherwise, this is a client, skip this replica and continue
		//		2.	Check if a Havok GameObject has been created
		//				If not, create one
		//				Otherwise, do nothing (scene updating will occur through the Havok GameObjectManager class later)
		//

		for(unsigned index = 0; index < replicaManager.GetReplicaCount(); index++)
		{
			GameObjectReplica *replica = static_cast<GameObjectReplica *>(replicaManager.GetReplicaAtIndex(index));
			if(replica != NULL)
			{
				Ogre::String objectName = replica->GetObjectName();
				Ogre::String entityName = replica->GetEntityName();

				// If the entity name isn't set and this is a server or a peer, set the entity name now
				// This case only appears to happen for server instances when objects are created before 
				// any client has ever connected.
				//
				// The problem doesn't appear to occur if objects are created after all clients have 
				// been disconnected.
				//
				// We cannot do this for objects that are not NetworkIDAuthorities because the name
				// contains the unique shared network object value, which is invalid for non-authorities.

				if(entityName.empty())
				{
					//if(networkIDManager.IsNetworkIDAuthority())
					
					//{
					//	// Entity base name format:  "objectName.guid"
					//	Ogre::String newEntityName = objectName + ".";
					//	//newEntityName.append(Ogre::StringConverter::toString((int)(replica->GetNetworkID().localSystemAddress)));
					//	//raknet 4.0b8
					//	newEntityName.append(Ogre::StringConverter::toString((int)(replica->GetNetworkID())));
					//	replica->SetEntityName(newEntityName.c_str());			
					//	entityName = replica->GetEntityName();
					//}
					//else
					//{
					//	// If the entity name isn't set and this is a client, skip this object because we don't yet have 
					//	//		a valid network ID to construct an entity name and the object shouldn't be rendered
					//	continue;
					//}

					//raknet 4.0b8
					
						// Entity base name format:  "objectName.guid"
						Ogre::String newEntityName = objectName + ".";
						//newEntityName.append(Ogre::StringConverter::toString((int)(replica->GetNetworkID().localSystemAddress)));
						//raknet 4.0b8
						newEntityName.append(Ogre::StringConverter::toString((int)(replica->GetNetworkID())));
						replica->SetEntityName(newEntityName.c_str());			
						entityName = replica->GetEntityName();
					
				}	

				//	After this point, entityName should not be empty, because if it was before now:
				//		Case 1: The entity name was just set because this is the NetworkIDAuthority (ie., the server)
				//		Case 2: continue was used in the previous loop for anything else (ie., a client)
				//
				//	At this point we want to check and see if a Havok GameObject has already been created for this replica.
				//	Calling the Havok GameObject constructor will do the following:
				//		- create an entity
				//		- create an anonymous scene node child from the scene root
				//		- attach the entity to the created scene node

				if(replica->GetHavokGameObject() == NULL)
				{
					//	Note to Havok guys:  Parameters that are char * (instead of const char * or std::string) and never modified are lame.

					GameObject *newHavokGameObject = new GameObject(
						const_cast<char *>(replica->GetEntityName().c_str()), 
						const_cast<char *>(replica->GetMeshName().c_str()),
						const_cast<char *>(replica->GetHavokObjectFileName().c_str()), 
						replica->GetHavokObjectType()
						);
					

					if(newHavokGameObject->m_eObjectType==DEFAULT_GAME_OBJECT_TYPE || newHavokGameObject->m_eObjectType==PHYSICS_FIXED ||
						newHavokGameObject->m_eObjectType==PHYSICS_DYNAMIC || newHavokGameObject->m_eObjectType==PHYSICS_KEYFRAMED
						|| newHavokGameObject->m_eObjectType==PHYSICS_SYSTEM ||
						newHavokGameObject->m_eObjectType==ANIMATED_CHARACTER_RIGIDBODY || 
						newHavokGameObject->m_eObjectType==PHYSICS_CHARACTER_RIGIDBODY)
					{
						newHavokGameObject->m_pPhysicsObject->getRigidBody()->setQualityType(HK_COLLIDABLE_QUALITY_BULLET);
						newHavokGameObject->m_pPhysicsObject->getRigidBody()->setMotionType(hkpMotion::MOTION_BOX_INERTIA);
						newHavokGameObject->m_pPhysicsObject->getRigidBody()->setFriction(1.0f);
						newHavokGameObject->m_pPhysicsObject->getRigidBody()->setRestitution(1.0f);
					

						//Not sure whether these lines shd be in current else or new one
					PhysicsPrimitiveObject* f_pTempPhysicsPrimitiveObject = (PhysicsPrimitiveObject*)newHavokGameObject->m_pPhysicsObject;
					if(replica->GetMass() > 0)
						if (f_pTempPhysicsPrimitiveObject!=NULL)
							f_pTempPhysicsPrimitiveObject->getRigidBody()->setMass(replica->GetMass());
					}
					if(replica->GetPosition() != Ogre::Vector3::ZERO)
					{
						Ogre::Vector3 ogrePosition = replica->GetPosition();
						newHavokGameObject->setPosition(ogrePosition.x, ogrePosition.y, ogrePosition.z);
					}
					
					if(newHavokGameObject->m_eObjectType==DEFAULT_GAME_OBJECT_TYPE || newHavokGameObject->m_eObjectType==PHYSICS_FIXED ||
						newHavokGameObject->m_eObjectType==PHYSICS_DYNAMIC || newHavokGameObject->m_eObjectType==PHYSICS_KEYFRAMED
						|| newHavokGameObject->m_eObjectType==PHYSICS_SYSTEM ||
						newHavokGameObject->m_eObjectType==ANIMATED_CHARACTER_RIGIDBODY || 
						newHavokGameObject->m_eObjectType==PHYSICS_CHARACTER_RIGIDBODY){
					if(replica->GetVelocity() != Ogre::Vector3::ZERO)
					{
						Ogre::Vector3 ogreVelocity = replica->GetVelocity();
						newHavokGameObject->m_pPhysicsObject->getRigidBody()->setLinearVelocity(hkVector4(ogreVelocity.x, ogreVelocity.y, ogreVelocity.z));
					}
					}
					//	If this is a client, mark it as updated so the server/peer can get an update
					if(topology == CLIENT) replica->SetClientModified(true);

					//	Set the references to the Havok GameObject, the Ogre scene manager, and the Havok GameObjectManager
					replica->SetHavokGameObject(newHavokGameObject);
					replica->SetSceneManager(pSceneManager);
					replica->SetGameObjectManager(havokManager);
				}

				//	Updating the scene nodes is not necessary here since that will be done in a subsequent call to havokManager->UpdateGraphicsObjects()

				entityMap[entityName] = true;
			}
		}

		return NETMANAGER_SUCCESS;
	}

	///	UpdateNetworkObjectsAfterHavokStep - Takes the replicas that this NetManager instance is responsible for and updates 
	/// the information with the results of a previous call to Havok->UpdateWorld()
	///
	///	Returns:
	/// NETMANAGER_FAILURE, if there were no objects to update
	/// NETMANAGER_SUCCESS, otherwise.

	int 
	HavokNetManager::UpdateNetworkObjectsAfterHavokStep()
	{
		topology1= topology; //topology1 is being used because global topology has been hidden by local topology

		DataStructures::List<RakNet::Replica3*> replicaList;
//		DataStructures::Multilist<ML_STACK, RakNet::Replica3 *> replicaList;
		//if(topology == SERVER)													
			replicaManager.GetReferencedReplicaList(replicaList);				// The server is responsible for updating all objects
		//else																	
		//	replicaManager.GetReplicasCreatedByMe(replicaList);					// Clients and peers update only their own objects
		//Above three lines have been commented since we also need to update all the objects... if this is a client/peer replica is not updated
		if(replicaList.Size() == 0) return NETMANAGER_FAILURE;

		for(unsigned index = 0; index < replicaManager.GetReplicaCount(); index++)
		{
			GameObjectReplica *replica = static_cast<GameObjectReplica *>(replicaManager.GetReplicaAtIndex(index));
			if(replica != NULL)
			{
				//replica->ProcessHavokStep(replica->creatingSystemGUID == networkInterface->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS));
				//raknet 4.0b8
				//if(havokGameObject->m_eObjectType!=GRAPHICS_OBJECT)
				//if(replica->GetHavokGameObject()->m_eObjectType!=GRAPHICS_OBJECT)
				replica->ProcessHavokStep(replica->creatingSystemGUID == networkInterface->GetMyGUID(),topology);
			}
		}
		return NETMANAGER_SUCCESS;
	}

}

#endif