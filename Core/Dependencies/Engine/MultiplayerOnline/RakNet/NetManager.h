#ifndef NETMANAGER_H
#define NETMANAGER_H

//raknet 4.0b8
//#include "RakNetworkFactory.h"
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"

#include "ReplicaManager3.h"
#include "NetworkIDManager.h"

#include "RakNetObjects.h"
//raknet 4.0b8
#include "GameObjectReplica.h"
#include "PhysXGameObjectReplica.h"
#include "RakNetTypes.h"
#include "PluginInterface2.h"

//raknet 4.0b8

#include <map>

namespace GamePipeGame
{
	class LogMessage;

	typedef bool (*CustomPacketHandler)(const RakNet::Packet* const rakNetPacket);

	typedef unsigned short GamePipeNetObjectGUID;
	//typedef RakNetGUID GamePipeNetClientGUID;

	//raknet 4.0b8
	typedef RakNet::RakNetGUID GamePipeNetClientGUID;

	typedef std::map<Ogre::String, bool> EntityMapType;
	extern NetTopology topology;

	static const int NETMANAGER_SUCCESS = 0;
	static const int NETMANAGER_FAILURE = 1;

	enum NetManagerStatus { INACTIVE, INITIALIZING, RUNNING, STOPPING, RESTARTING, DESTROYING };
	enum SubsystemType { NATIVE, RAKNET };

	class NetManager
	{

	public:

		NetManager(CustomPacketHandler pCustomPacketHandler = 0);
		virtual ~NetManager();

		virtual int Initialize();
//		virtual int Destroy();

//		virtual int Start();
//		virtual int Stop();
//		virtual int Restart();

		virtual int ProcessMessages();

		//virtual int SendPacket(int messageType, const char *message, int messageLength, int messagePriority=LOW_PRIORITY, int messageReliability=UNRELIABLE_SEQUENCED,
			//int channel=0, SystemAddress destination=UNASSIGNED_SYSTEM_ADDRESS, bool broadcast=true);

		//raknet 4.0b8
		virtual int SendPacket(int messageType, const char *message, int messageLength, int messagePriority=LOW_PRIORITY, int messageReliability=UNRELIABLE_SEQUENCED,
			//int channel=0, RakNet::SystemAddress destination=UNASSIGNED_SYSTEM_ADDRESS, bool broadcast=true);
		//raknet 4.0b8
			int channel=0, RakNet::SystemAddress destination=RakNet::UNASSIGNED_SYSTEM_ADDRESS, bool broadcast=true);

		//virtual int SendPacket(RakNet::BitStream &message, int messagePriority=LOW_PRIORITY, int messageReliability=UNRELIABLE_SEQUENCED,
			//int channel=0, SystemAddress destination=UNASSIGNED_SYSTEM_ADDRESS, bool broadcast=true);

		//raknet 4.0b8
		virtual int SendPacket(RakNet::BitStream &message, int messagePriority=LOW_PRIORITY, int messageReliability=UNRELIABLE_SEQUENCED,
			//int channel=0, RakNet::SystemAddress destination=UNASSIGNED_SYSTEM_ADDRESS, bool broadcast=true);
			//raknet 4.0b8
			int channel=0, RakNet::SystemAddress destination=RakNet::UNASSIGNED_SYSTEM_ADDRESS, bool broadcast=true);

		NetManagerStatus GetStatus();
		NetTopology GetTopology();

		int SetMessageLog(std::deque<LogMessage *> *newLog);			// set the global log
		int	AddLogMessage(std::string newMessage, int priority=0);		// add a message to the global log

		/// set the network topology type (ie., client, server, peer)
		virtual int SetTopology(NetTopology newTopology);	
		/// create the network ID manager and indicate if it's an ID authority
		virtual int CreateNetworkIDManager(bool netIDAuthority);	
		/// set the local port to use for socket binding (0 binds first available)
		virtual int SetLocalSocketPort(unsigned short newLocalPort);
		/// set the maximum number of connections
		virtual int SetMaxConnections(int newMaxConnections);	
		/// set the sleep interval for the socket
		virtual int SetSleepInterval(int newSleepIntervalMS);	
		/// set the remote host IP to connec to (default "127.0.0.1")
		virtual int SetRemoteHost(const char *newRemoteHost);		
		/// set the remote port to connect to
		virtual int SetRemotePort(unsigned short newRemotePort);	
		/// set the password, given the password length
		virtual int SetPasswordOptions(const char *password, int passwordLength);			

		/// enable a RakNet plug-in
		virtual int EnableRakNetPlugin(const char *pluginName);								

		/// as a client, connects to a server
		virtual int Connect();	
		/// as a client, disconnects from the server
		virtual int Disconnect();															

		/// disconnect all connected systems
		virtual int NetShutdown();															

		/// returns whether the system has been started
		virtual bool NotStarted();															

		//virtual int AddObject(BasicReplica *);											
		//raknet 4.0b8
		/// add new local object to the shared object list
		virtual int AddObject(GameObjectReplica *);	

		//virtual int AddObject(BasicReplica *, RakNet::RakNetGUID);											
		//raknet 4.03
		/// add new local object to the shared object list and add details to listGUID
		virtual int AddObject(GameObjectReplica *, RakNet::RakNetGUID);
		//virtual int DestroyObjectByPtr(BasicReplica *);								
		//raknet 4.0b8
		/// destroy a specific object using a pointer reference
		virtual int DestroyObjectByPtr(GameObjectReplica *);
		/// destroy a specific object by its name
		virtual int DestroyObjectByName(Ogre::String name);	
		/// destroy a specific object by its network guid
		virtual int DestroyObjectByGUID(GamePipeNetObjectGUID guid);						

		/// destroy all objects owned by me
		virtual int DestroyMyObjects(std::vector<Ogre::String> &destroyedEntityNames);		
		/// destroy all objects that I can destroy
		virtual int DestroyAllObjects(std::vector<Ogre::String> &destroyedEntityNames);		

		//virtual BasicReplica * QueryByObjectName(Ogre::String &name);					
		//raknet 4.0b8
		/// get the object with the specific object name
		virtual GameObjectReplica * QueryByObjectName(Ogre::String &name);
		//virtual BasicReplica * QueryByEntityName(Ogre::String &name);					
		//raknet 4.0b8
		/// get the object with the specific entity name
		virtual GameObjectReplica * QueryByEntityName(Ogre::String &name);
		//virtual int QueryAllObjects(std::vector<BasicReplica *> &objectList);			

		//raknet 4.0b8
		/// get a list of all known shared objects
		virtual int QueryAllObjects(std::vector<GameObjectReplica *> &objectList);

		//raknet 4.03
		/// get a list of all objects created by guid
		virtual int QueryObjectsByGUID(std::vector<GameObjectReplica *> &objectList,RakNet::RakNetGUID guid);

		//virtual BasicReplica * SelectNextObject(BasicReplica *currentEntityPtr, bool forward = true);
		//raknet 4.0b8
		/// selects the next Object
		virtual GameObjectReplica * SelectNextObject(GameObjectReplica *currentEntityPtr, bool forward = true);

		virtual int UpdateNetworkObjectsInScene(Ogre::SceneManager *pSceneManager, EntityMapType &entityMap);

	protected:

		/// status of the network manager
		NetManagerStatus managerStatus;		
		/// subsystem type (using RakNet or something else)
		SubsystemType subsystem;					

		/// determines whether this is a client, server, or peer
		NetTopology topology;						

		/// maximum number of connections
		int maxConnections;		
		/// sleep interval between polls on the socket(s)
		int sleepInterval;							

		/// port number to bind to locally
		unsigned short localPort;					

		/// IP address of remote host to connect to
		std::string remoteHost;			
		/// port number of remote host to connect to
		unsigned short remotePort;					

		/// password to use for connections
		std::string password;						

		/* Shared Object ID Management */

		//NetworkIDManager networkIDManager;			
		
		//raknet 4.0b8
		/// object responsible for assigning unique network IDs
		RakNet::NetworkIDManager networkIDManager;

		/// whether this connection is responsible for assigning network IDs
		bool isNetworkIDAuthority;					

		/* Network Interface */

		//RakPeerInterface *networkInterface;			

		//raknet 4.0b8
		/// basic RakNet network connection
		RakNet::RakPeerInterface *networkInterface;

		RakNet::PluginInterface2 *networkPluginInterface;
		//SocketDescriptor connectionSD;				
		
		// raknet 4.0b8
		/// basic object containing port (ushort) and hostname (char[32])
		RakNet::SocketDescriptor connectionSD;
		//Packet *rakPacket;

		//raknet 4.0b8
		RakNet::Packet *rakPacket;

		/* Distributed Network Object Management */

		/// handles management of all shared objects
		GamePipeReplicaManager replicaManager;		

		/// reference to global log deque
		std::deque<LogMessage *> *globalLog;
		
		/// method to handle custom game packets not handled by NetManager's ProcessMessages()
		CustomPacketHandler m_pCustomPacketHandler;

		//Vectors to keep track  of which GameObejctReplica was created by which GUID
		vector<RakNet::RakNetGUID> listGUID;
		vector<RakNet::NetworkID> listNetworkID;
	};

	class LogMessage
	{
	public:
		LogMessage();
		LogMessage(std::string newMessage, int newPriority=0);
		~LogMessage();

		float messageAge;
		int messagePriority;
		std::string messageText;
	};

}		// end namespace GamePipeGame

#endif