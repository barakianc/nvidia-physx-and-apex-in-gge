#ifndef HAVOKNETMANAGER_H
#define HAVOKNETMANAGER_H

#ifdef HAVOK

#include "NetManager.h"
#include "GameObjectReplica.h"

namespace GamePipeGame
{
	extern NetTopology topology1;
	class HavokNetManager : public NetManager
	{

	public:
		HavokNetManager(CustomPacketHandler pCustomPacketHandler = 0);
		~HavokNetManager();

		/// set the config file object (containing special settings)
		int SetConfigFile(Ogre::ConfigFile *newConfigFile);				

		//	If I wanted to redefine how the NetManager should handle receives, I can uncomment the line below
		//	int ProcessMessages();											// rewrite of Receive() handling for this class

		/// creates a game object from received replica if it hasn't been created yet
		int UpdateNetworkObjectsInSceneWithHavok(Ogre::SceneManager *pSceneManager, EntityMapType &entityMap, GameObjectManager *havokManager);
		/// update existing game objects on local system with received physics info
		int UpdateNetworkObjectsAfterHavokStep();

	protected:

		Ogre::ConfigFile *configFile;									// reference to startup configuration settings

	};
}

#endif

#endif