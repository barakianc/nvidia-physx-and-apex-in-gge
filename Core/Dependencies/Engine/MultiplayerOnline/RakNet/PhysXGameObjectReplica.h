#pragma once

#ifdef PHYSX

#include "RakNetObjects.h"


#ifndef GAMEOBJECTREPLICA_H
#define GAMEOBJECTREPLICA_H

namespace GamePipeGame
{
	class GameObjectReplica : public BasicReplica
	{
	public:
		GameObjectReplica();
		GameObjectReplica(Ogre::String newName, Ogre::String newMesh="", Ogre::String newEntity="", Ogre::Real newMass=-1);

		~GameObjectReplica();

		/* Virtual functions inherited from GameObjectReplica */

	protected:
		virtual RakNet::RakString GetName() const;

		virtual void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const;
//		virtual void PrintOutput(RakNet::BitStream *bs);

		virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
		virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection);

//		virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection);
//		virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection);

		virtual void DeallocReplica(RakNet::Connection_RM3 *sourceConnection);

		virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters *serializeParameters);
		virtual void Deserialize(RakNet::DeserializeParameters *deserializeParameters);

//		virtual void SerializeConstructionRequestAccepted(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *requestingConnection);
//		virtual void DeserializeConstructionRequestAccepted(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *acceptingConnection);
//		virtual void SerializeConstructionRequestRejected(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *requestingConnection);
//		virtual void DeserializeConstructionRequestRejected(RakNet::BitStream *serializationBitstream, RakNet::Connection_RM3 *rejectingConnection);

//		virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, RakNet::ReplicaManager3 *replicaManager3);
//		virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection);
//		virtual RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection); 
//		virtual RakNet::RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const;

		/* Havok-object related adds */

	public:

		/*void SetHavokGameObject(GameObject *newHavokGameObject);

		void SetHavokObjectFileName(const char *newHavokObjectFileName);
		void SetHavokObjectFileInfo(const char *newHavokObjectFileName, const char *newMeshName);

		void SetHavokObjectType(int newHavokObjectType);*/

		void SetAngularVelocity(float x, float y, float z);
		void SetAngularVelocity(Ogre::Vector3 newAngularVelocity);
		void SetRealAngularVelocity(float x, float y, float z);
		void SetRealAngularVelocity(Ogre::Vector3 newRealAngularVelocity);

		/*GameObject * GetHavokGameObject();

		Ogre::String GetHavokObjectFileName();

		GameObjectType GetHavokObjectType();*/

		Ogre::Vector3 GetAngularVelocity();
		Ogre::Vector3 GetRealAngularVelocity();

		/*void GetHavokRigidBodyInfo(hkVector4 &hkPosition, hkVector4 &hkVelocity, hkVector4 &hkAngularVelocity, hkQuaternion &hkOrientation);
		void SetHavokRigidBodyInfo(Ogre::Vector3 updatePosition, Ogre::Quaternion updateOrientation, 
			Ogre::Vector3 updateVelocity, Ogre::Vector3 updateAngularVelocity);

		void ProcessHavokStep(bool isOwner,NetTopology topology=CLIENT);*/

		//@Animation
		void playAnimation(Ogre::String animationName);
		void stopAnimation(Ogre::String animationName = "");
		bool hasAnimation(Ogre::String animationName);

	protected:

		GameObject *havokGameObject;			// pointer to a Havok "GameObject" class

		Ogre::String m_sHKXFileName;			// Havok object file (.hkx)

		int m_eGameObjectType;					// Havok "main" object type (enum GameObjectType)

		//	Angular velocity in Havok from some documentation:
		//		- Magnitude of the vector determines the speed in degrees per second CCW about the specified axis
		//		- Normal of the vector gives the axis
		//
		//  Example:  (2.0, 0, 0) is a rotation speed of 2 deg/s CCW around the +X axis

		Ogre::Vector3 angularVelocity;			// client-side Havok angular velocity 
		Ogre::Vector3 realAngularVelocity;		// server-side Havok angular velocity

		// Stuff I have to add to handle deletion of Havok objects...sigh.

		//@Animation
		Ogre::String currentAnimation; //if no animation to be played use "none"


	public:

		void SetSceneManager(Ogre::SceneManager *newSceneManager);
		void SetGameObjectManager(GameObjectManager *newManager);
		void SetUseServerPhysicsOnly(bool newUseServerPhysics);

		Ogre::SceneManager * GetSceneManager();
		GameObjectManager * GetGameObjectManager();		
		bool GetUseServerPhysicsOnly();

	protected:

		Ogre::SceneManager *sceneManager;
		GameObjectManager *owningManager;
		bool useServerPhysicsOnly;
	};

}

#endif

#endif