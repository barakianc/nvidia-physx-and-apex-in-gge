#include <AuthService.h>
#include <sc.h>

static const char * SAMPLE_GAMENAME = "atlasSamples";
static const int SAMPLE_GAME_ID = 1649;
static const char * SAMPLE_SECRET_KEY = "Zc0eM6";
static const char * SAMPLE_ACCESS_KEY = "39cf9714e24f421c8ca07b9bcb36c0f5";

static char * SAMPLE_UNIQUENICK[3] = {"gsSampleName", "gsSampleName2", "gsSampleName3"};
static const char * SAMPLE_PASSWORD = "gspy";

extern GSLoginCertificate certificate;
extern GSLoginPrivateData privateData;
extern GSLoginCertificate certificates[3];
extern GSLoginPrivateData privateDatas[3];

gsi_bool AvailabilityCheck();

void SampleAuthenticationCallback(GHTTPResult httpResult, WSLoginResponse * theResponse, void * theUserData);

gsi_bool SampleAuthenticatePlayer(const char * uniqueNick, const char * password);

gsi_bool AuthSetup();

void CoreCleanup();

gsi_bool AtlasReportingSetup(SCInterfacePtr * statsInterface, int playerIndex, char * sessionId);
gsi_bool AtlasQuerySetup(SCInterfacePtr * statsInterface);
void DisplayAtlasPlayerRecords(SCPlayerStatsQueryResponse * response);
void AtlasCleanup(SCInterfacePtr statsInterface);

void WaitForUserInput();