#include "sb_serverbrowsing.h"
#include "qr2regkeys.h"
#include "gsAvailable.h"
#include "natneg.h"
#include "nninternal.h"
#include "vector"
#include "string"

/* Server Browser class definition. See cpp file for method descriptions. */
class ServerBrowsing
{
private:
	ServerBrowser sb;					// Server Browser instance
	char gameName[20];					// Name of our game(as specified on GameSpy. The short name not the full name)
	char secretKey[10];					// Secret Key provided by GameSpy
	int maxConcUpdates;					// Maximum number of concurrent updates allowed
	bool lanBrowse;						// Are we searching for games only on local LAN?
	unsigned char *basicFields;			// Key values which we're going to use to Query the server for details
	int numFields;						// Number of keys used
	bool initialized;					// Has the server browser been initialized(If not don't call update or think)

public:
	bool newServerBrowser();
	void think();
	void update();
	void setGameName(char name[20]);
	void setSecretKey(char key[10]);
	void setMaxConcUpdates(int max);
	void setLanBrowse(bool lan);
	void setBasicFields(unsigned char *fields, int num);
	ServerBrowsing();
};

char * getServerIP();
int getPort();
void ConnectToServer(int index);
void GetServerList(std::vector<std::string> *sl);
