#include <gsCommon.h>
#include <gsCore.h>
#include <gsAvailable.h>
#include <AuthService.h>
#include <sc.h>
#include "GameSpyAtlas.h"
#include "atlas_atlasSamples_v2.h"

// this will make our lives easier since every atlas call returns a local result
#define CHECK_SCRESULT(result) if(result != SCResult_NO_ERROR) { return gsi_false; }

#define NUM_PLAYERS 3

// cars
const int CORVETTE = 0;
const int VIPER = 1;

// tracks
const int DAYTONA_BEACH = 0;
const int MONACO = 1; 

typedef struct SampleMatchData
{
	char sessionId[SC_SESSION_GUID_SIZE];
	int track;
	gsi_u8 corvetteUses;
	gsi_u8 viperUses;

	SCGameStatus status;
} SampleMatchData;

typedef struct SamplePlayerData
{
	GSLoginCertificate * certificate;
	GSLoginPrivateData * privateData;

	SCInterfacePtr statsInterface;
	
	SCReportPtr report;

	gsi_u8 connectionId[SC_CONNECTION_GUID_SIZE];
	
	SCGameResult result;
	int vehicleUsed;
	gsi_u64 raceTime;

	char uniquenick[20]; // 20 is the max length for a GameSpy uniquenick

} SamplePlayerData;

void PlayFakeMatch();
gsi_bool ReportServerSetup(char*,  size_t);
gsi_bool ReportClientSetup(int, char*);
gsi_bool ReportStats(int);
gsi_bool ReportCleanup(int);