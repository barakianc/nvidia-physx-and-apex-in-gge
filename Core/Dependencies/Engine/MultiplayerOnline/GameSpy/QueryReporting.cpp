#include "StdAfx.h"
#include "QueryReporting.h"
#include "gsAvailable.h"

/* Global Varible that should be modified only by the QR callback functions.
   The game should only have permission to read this value and not modify it.
   So declare only in cpp file and not as a static member of the QR class */
bool connected_server = false; // Indicates whether we're connected to GameSpy server.

/* Initialize Static Members */
char QueryReporting::hostName[30] = "";
char QueryReporting::gameVersion[5] = "";
int QueryReporting::hostPort = -1;
char QueryReporting::mapName[30] = "";
char QueryReporting::gameType[30] = "";
int QueryReporting::numPlayers = -1;
int QueryReporting::numTeams = -1;
int QueryReporting::maxPlayers = -1;
char QueryReporting::gameMode[30] = "";
bool QueryReporting::teamPlay = false;
int QueryReporting::fragLimit = -1;

/* Callback function that's called when a client queries the server for information */
void serverkey_callback(int keyid, qr2_buffer_t outbuf, void *userdata)
{
	char value[30];
	switch (keyid)
	{
	case HOSTNAME_KEY:
		QueryReporting::getHostName(value);
		qr2_buffer_add(outbuf, value);
		break;
	case GAMEVER_KEY:
		QueryReporting::getGameVersion(value);
		qr2_buffer_add(outbuf, value);
		break;
	case HOSTPORT_KEY:
		qr2_buffer_add_int(outbuf, QueryReporting::getHostPort());
		break;
	case MAPNAME_KEY:
		QueryReporting::getMapName(value);
		qr2_buffer_add(outbuf, value);
		break;
	case GAMETYPE_KEY:
		QueryReporting::getGameType(value);
		qr2_buffer_add(outbuf, value);
		break;
	case NUMPLAYERS_KEY:
		qr2_buffer_add_int(outbuf, QueryReporting::getNumPlayers());
		break;
	case NUMTEAMS_KEY:
		qr2_buffer_add_int(outbuf, QueryReporting::getNumTeams());
		break;
	case MAXPLAYERS_KEY:
		qr2_buffer_add_int(outbuf, QueryReporting::getMaxPlayers());
		break;
	case GAMEMODE_KEY:
		QueryReporting::getGameMode(value);
		qr2_buffer_add(outbuf, value);
		break;
	case TEAMPLAY_KEY:
		qr2_buffer_add_int(outbuf, QueryReporting::getTeamPlay());
		break;
	case FRAGLIMIT_KEY:
		qr2_buffer_add_int(outbuf, QueryReporting::getFragLimit());
		break;
	default:
		qr2_buffer_add(outbuf, _T(""));
	}
	
	GSI_UNUSED(userdata);
}

/* Specify the list of keys that clients can use to Query the server.
   If you're planning on using custom keys, you should add them here */
void keylist_callback(qr2_key_type keytype, qr2_keybuffer_t keybuffer, void *userdata)
{
	switch (keytype)
	{
	case key_server:
		qr2_keybuffer_add(keybuffer, HOSTNAME_KEY);
		qr2_keybuffer_add(keybuffer, GAMEVER_KEY);
		qr2_keybuffer_add(keybuffer, HOSTPORT_KEY);
		qr2_keybuffer_add(keybuffer, MAPNAME_KEY);
		qr2_keybuffer_add(keybuffer, GAMETYPE_KEY);
		qr2_keybuffer_add(keybuffer, NUMPLAYERS_KEY);
		qr2_keybuffer_add(keybuffer, NUMTEAMS_KEY);
		qr2_keybuffer_add(keybuffer, MAXPLAYERS_KEY);
		qr2_keybuffer_add(keybuffer, GAMEMODE_KEY);
		qr2_keybuffer_add(keybuffer, TEAMPLAY_KEY);
		qr2_keybuffer_add(keybuffer, FRAGLIMIT_KEY);
		qr2_keybuffer_add(keybuffer, TIMELIMIT_KEY);
		break;
	case key_player:
		qr2_keybuffer_add(keybuffer, PLAYER__KEY);
		qr2_keybuffer_add(keybuffer, SCORE__KEY);
		qr2_keybuffer_add(keybuffer, DEATHS__KEY);
		qr2_keybuffer_add(keybuffer, PING__KEY);
		qr2_keybuffer_add(keybuffer, TEAM__KEY);
		break;
	case key_team:
		qr2_keybuffer_add(keybuffer, TEAM_T_KEY);
		qr2_keybuffer_add(keybuffer, SCORE_T_KEY);
		break;
	default: break;
	}
	
	GSI_UNUSED(userdata);
}

/* Callback function that's called when NAT negotiation state changes.
   Called on both server and client. */
void natProgressCBServer(NegotiateState state, void *user)
{
	switch(state)
	{
		case ns_initack:
			printf("ns_initack, Init Packets Acknowledged.\n");
			break;
		case ns_connectping:
			printf("ns_connectping, Starting connection and pinging other machine.\n");
			break;
		default:
			break;
	}
}

/* Callback function that's called when NAT negotiation is complete.
   Called on both server and client. */
void natCompleteCBServer(NegotiateResult result, SOCKET gamesocket, struct sockaddr_in *remoteaddr, void *userdata)
{
	if(result != nr_success)
		printf("\nFailed!!\n");
	else
	{
		printf("NAT Negotiation Complete!\n");
		connected_server = true;
	}
}

/* Callback function that's called when client initiates NAT negotiation.
   Called only on Server. */
void natNegCB(int cookie, void *user)
{
	printf("\nRequest Received\n");
	NNBeginNegotiation(cookie, 1, natProgressCBServer, natCompleteCBServer, NULL);
	gsi_time start = current_time();
	while((current_time() - start <= 60000) && !connected_server)
	{
		NNThink();
	}
}

/* Some callbacks that are used when player keys, team keys etc., are requested by client
   They're similar to serverkey callback. These are currently not being used by us. */
void playerkey_callback(int keyid, int index, qr2_buffer_t outbuf, void *userdata)
{

}

void teamkey_callback(int keyid, int index, qr2_buffer_t outbuf, void *userdata)
{

}	

int count_callback(qr2_key_type keytype, void *userdata)
{
	return 0;
}

void adderror_callback(qr2_error_t error, gsi_char *errmsg, void *userdata)
{

}

QueryReporting::QueryReporting()
{
	qrec = NULL;
	basePort = 11111;
	strcpy_s(gameName, "USCGGEpc");
	strcpy_s(secretKey, "SqTXPs");
	isPublic = 1;
	natNegotiate = 1;
}

/* List of get/set methods for class members */
void QueryReporting::setFragLimit(int value)
{
	fragLimit = value;
}

int QueryReporting::getFragLimit()
{
	return fragLimit;
}

void QueryReporting::setGameMode(char value[30])
{
	strcpy_s(gameMode, value);
}

void QueryReporting::getGameMode(char value[30])
{
	strcpy_s(value, 30, gameMode);
}

void QueryReporting::setGameType(char value[30])
{
	strcpy_s(gameType, value);
}

void QueryReporting::getGameType(char value[30])
{
	strcpy_s(value, 30, gameType);
}

void QueryReporting::setGameVersion(char value[5])
{
	strcpy_s(gameVersion, value);
}

void QueryReporting::getGameVersion(char value[5])
{
	strcpy_s(value, 5, gameVersion);
}

void QueryReporting::setHostName(char value[30])
{
	strcpy_s(hostName, value);
}

void QueryReporting::getHostName(char value[30])
{
	strcpy_s(value, 30, hostName);
}

void QueryReporting::setHostPort(int value)
{
	hostPort = value;
}

int QueryReporting::getHostPort()
{
	return hostPort;
}

void QueryReporting::setMapName(char value[30])
{
	strcpy_s(mapName, value);
}

void QueryReporting::getMapName(char value[30])
{
	strcpy_s(value, 30, mapName);
}

void QueryReporting::setMaxPlayers(int value)
{
	maxPlayers = value;
}

int QueryReporting::getMaxPlayers()
{
	return maxPlayers;
}

void QueryReporting::setNumPlayers(int value)
{
	numPlayers = value;
}

int QueryReporting::getNumPlayers()
{
	return numPlayers;
}

void QueryReporting::setNumTeams(int value)
{
	numTeams = value;
}

int QueryReporting::getNumTeams()
{
	return numTeams;
}

void QueryReporting::setTeamPlay(bool value)
{
	teamPlay = value;
}

bool QueryReporting::getTeamPlay()
{
	return teamPlay;
}

void QueryReporting::setGameName(char name[20])
{
	strcpy_s(gameName, name);
}

void QueryReporting::setSecretKey(char key[10])
{
	strcpy_s(secretKey, key);
}

void QueryReporting::setIsPublic(bool pub)
{
	isPublic = pub;
}

void QueryReporting::setNatNegotiate(bool nat)
{
	natNegotiate = nat;
}

void QueryReporting::setBasePort(int port)
{
	basePort = port;
}

/* Returns the QR handle */
qr2_t QueryReporting::getQrec()
{
	if(qrec)
		return *qrec;
	else
		return 0;
}

/* Used to intialize Query Reporting. This must be called first. */
bool QueryReporting::init()
{
	if (qr2_init(NULL, NULL, basePort, gameName, secretKey, isPublic, natNegotiate,
		serverkey_callback, playerkey_callback, teamkey_callback,
		keylist_callback, count_callback, adderror_callback, NULL) != e_qrnoerror)
	{
		printf("Error starting query sockets\n");
		return false;
	}
	GSIACResult result;
	GSIStartAvailableCheck(_T("uscggepc"));
	while((result = GSIAvailableCheckThink()) == GSIACWaiting)
		msleep(5);
	if(qrec)
		qr2_register_natneg_callback(*qrec, natNegCB);
	else
		qr2_register_natneg_callback(0, natNegCB);
	return true;
}

/* This MUST be called in the game's update loop. Checks for messages from
   GameSpy's master server and calls the necessary callback functions. */
void QueryReporting::think()
{
	qr2_think(NULL);
}
