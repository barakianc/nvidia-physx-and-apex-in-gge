#include "StdAfx.h"
#include "ServerBrowsing.h"

/* Global Varible that should be modified only by the QR callback functions.
   The game should only have permission to read this value and not modify it.
   So declare only in cpp file and not as a static member of the SB class */
char ServerIP[16];						// IP address of the server to connect to after NAT negotiation is complete.
int port = -1;							// Server port to connect to after NAT negotiation is complete.
bool connected = false;					// Are we connected to GameSpy server?
std::vector<std::string> serverList;	// List of servers that are available for this game(only host names and IP)
std::vector<SBServer> servers;			// List of servers that are available for this game(Server Object not accessible to game-specific code)
ServerBrowser mySB;						// Server that we're connected

/* Returns the IP address of the server */
char * getServerIP()
{
	return ServerIP;
}

/* Returns the port of the server */
int getPort()
{
	return port;
}

/* Callback function that's called when NAT negotiation state changes.
   Called on both server and client. */
void natProgressCB(NegotiateState state, void *user)
{
	switch(state)
	{
		case ns_initack:
			printf("ns_initack, Init Packets Acknowledged.\n");
			break;
		case ns_connectping:
			printf("ns_connectping, Starting connection and pinging other machine.\n");
			break;
		default:
			break;
	}
}

/* Callback function that's called when NAT negotiation is complete.
   Called on both server and client. */
void natCompleteCB(NegotiateResult result, SOCKET gamesocket, sockaddr_in *remoteaddr, void *userdata)
{
	if(result != nr_success)
		printf("\nFailed!!\n");
	else
	{
		printf("Nat Negotiation Complete");
		strcpy_s(ServerIP, inet_ntoa(remoteaddr->sin_addr));
		port = ntohs(remoteaddr->sin_port);
		connected = true;
	}

}

/* Called by the game to connect to a server */
void ConnectToServer(int index)
{
	SBServer server;
	server = servers[index];
	if(SBServerHasPrivateAddress(server))
		{
			if(SBServerGetPublicInetAddress(server) == ServerBrowserGetMyPublicIPAddr(mySB))
				strcpy_s(ServerIP, SBServerGetPrivateAddress(server));
			else
			{
				int cookie = rand() % 10000;
				ServerBrowserSendNatNegotiateCookieToServer(mySB, SBServerGetPublicAddress(server), SBServerGetPublicQueryPort(server), cookie);
				NNBeginNegotiation(cookie, 0, natProgressCB, natCompleteCB, NULL);
				gsi_time start = current_time();
				while((current_time() - start <= 60000) && !connected)
				{
					NNThink();
				}
			}
		}
		else 
		{
			if(SBServerDirectConnect(server))
				strcpy_s(ServerIP, SBServerGetPublicAddress(server));
			else
			{
				int cookie = rand() % 10000;
				ServerBrowserSendNatNegotiateCookieToServer(mySB, SBServerGetPublicAddress(server), SBServerGetPublicQueryPort(server), cookie);
				NNBeginNegotiation(cookie, 0, natProgressCB, natCompleteCB, NULL);
				gsi_time start = current_time();
				while((current_time() - start <= 60000) && !connected)
				{
					NNThink();
				}
			}
		}
}

/*Returs the list of servers available */
void GetServerList(std::vector<std::string> *sl)
{
	*sl = serverList;
}

/* Called when a new server is added, removed or updated */
void SBCallback(ServerBrowser sb1, SBCallbackReason reason, SBServer server, void *instance)
{
	mySB = sb1;
	std::string serv;
	switch(reason)
	{
	case sbc_serveradded:
		serv = std::string(SBServerGetStringValue(server, "hostname", "[UNKNOWN]"));
		serv += "(";
		serv += std::string(SBServerGetPublicAddress(server));
		serv += ")\n";
		serverList.push_back(serv);
		servers.push_back(server);
		printf("\nHOSTNAME: %s\n", SBServerGetStringValue(server, "hostname", "[UNKNOWN]"));
		printf("MAPNAME: %s\n", SBServerGetStringValue(server, "mapname", "[UNKNOWN]"));
		printf("GAMETYPE: %s\n", SBServerGetStringValue(server, "gametype", "[UNKNOWN]"));
		printf("NUMPLAYERS: %d\n", SBServerGetIntValue(server, "numplayers",-1));
		printf("MAXPLAYERS: %d\n", SBServerGetIntValue(server, "maxplayers", -1));
		printf("IP ADDRESS: %s\n", SBServerGetPublicAddress(server));
		printf("PORT: %d\n", SBServerGetPublicQueryPort(server));
		break;
	case sbc_serverupdated:
		printf("\nUpdated\n");
		break;
	case sbc_queryerror:
		printf("\nError!!\n");
		break;
	}
}

ServerBrowsing::ServerBrowsing()
{
	strcpy_s(gameName, "USCGGEpc");
	strcpy_s(secretKey, "SqTXPs");
	maxConcUpdates = 20;
	lanBrowse = false;
	basicFields = new unsigned char[5];
	basicFields[0] = HOSTNAME_KEY;
	basicFields[1] = GAMETYPE_KEY;
	basicFields[2] = MAPNAME_KEY;
	basicFields[3] = NUMPLAYERS_KEY;
	basicFields[4] = MAXPLAYERS_KEY;
	numFields = 5;
	initialized = false;
}

/* Some get/set methods */
void ServerBrowsing::setGameName(char name[20])
{
	strcpy_s(gameName, name);
}

void ServerBrowsing::setSecretKey(char key[10])
{
	strcpy_s(secretKey, key);
}

void ServerBrowsing::setLanBrowse(bool lan)
{
	lanBrowse = lan;
}

void ServerBrowsing::setMaxConcUpdates(int max)
{
	maxConcUpdates = max;
}

void ServerBrowsing::setBasicFields(unsigned char *fields, int num)
{
	numFields = num;
	delete basicFields;
	basicFields = new unsigned char[numFields];
	for(int i = 0; i < numFields; i++)
		basicFields[i] = fields[i];
}

/* Needs to be called whenever we want to refresh the server list */
void ServerBrowsing::update()
{
	if(initialized)
	{
		ServerBrowserClear(sb);
		ServerBrowserUpdate(sb, SBTrue, SBTrue, basicFields, numFields, NULL);
	}
}

/* Create a new server browser object. This should be called first */
bool ServerBrowsing::newServerBrowser()
{
	//sb = ServerBrowserNew(gameName, gameName, secretKey, 0, 20, QVERSION_QR2, (SBBool)lanBrowse,SBCallback, NULL);
	GSIACResult result;
	GSIStartAvailableCheck("USCGGEpc");
	while((result = GSIAvailableCheckThink()) == GSIACWaiting)
		msleep(5);
	sb = ServerBrowserNew("USCGGEpc", "USCGGEpc", "SqTXPs", 0, 30, QVERSION_QR2, SBFalse, SBCallback, NULL);
	initialized = true;
	return true;
}

/* This MUST be called in the game's update loop. Processes incoming messages from
   GameSpy's master server and calls the appropriate callback fucntions. */
void ServerBrowsing::think()
{
	if(initialized)
		ServerBrowserThink(sb);
}
