#include <gp.h>
#include <gsAvailable.h>
#include "vector"
#include "string"
#include "map"

/* Structure used to store profile info.
   TODO: Also add fields for birthday, sex, location etc., */
struct profileInfo
{
	std::string name;
	std::string email;
};

struct newMessage
{
	std::string message;
	std::string from;
};

/* GameSpy Presence class definition. See cpp file for method descriptions. */
class GameSpyPresence
{

private:

	GPConnection connection;			// Connection handle
	char email[100];					// Email address used to connect to Presence
	char password[30];					// Password used to connect to Presence
	char nickName[30];					// Nick name of the profile
	bool initialized;					// Is presence initialized?(Don't call Process() before it is)
	static bool showOnlyOnline;			// Only display friends who are online?

public:

	GameSpyPresence();
	void SetEmail(char em[100]);
	void SetPassword(char pass[30]);
	void SetNickName(char nick[30]);
	bool PresenceInit();
	bool PresenceConnect();
	bool PresenceNewProfile(char newNick[30]);
	void PresenceDisconnect();
	bool PresenceSendBuddyRequest(char nick[31], char email[100], char f_name[40], char l_name[40]);
	bool PresenceSendBuddyMessage(int index, char message[1000]);
	void PresenceProcess();
	void PresenceAcceptBuddyRequest(int index);
	void PresenceGetBuddyRequests(std::vector<profileInfo> *list);
	void PresenceGetBuddyList(std::vector<profileInfo> *list);
	void PresenceSetStatus(bool online, char status[256]);
	void PresenceDeleteBuddy(int index);
	void PresenceDenyBuddyRequest(int index);
	static void setShowOnlyOnline(bool value);
	static bool getShowOnlyOnline();
};

void getMessage(std::vector<newMessage> *mess);
bool getLoggedIn();
bool getPendingRequests();