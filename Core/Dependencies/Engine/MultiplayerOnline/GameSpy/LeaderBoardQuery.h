#include <gsCommon.h>
#include <gsCore.h>
#include <gsAvailable.h>
#include <AuthService.h>
#include <sci.h>
#include <sc.h>
#include "GameSpyAtlas.h"
#include "atlas_atlasSamples_v2.h"

#include <vector>

// this will make our lives easier since every atlas call returns a local result
#define CHECK_SCRESULT(result) if(result != SCResult_NO_ERROR) { return gsi_false; }

gsi_bool QuerySetup();
gsi_bool QueryLeaderboard();
int QueryThink(std::vector<std::string>*);
gsi_bool QueryCleanup();