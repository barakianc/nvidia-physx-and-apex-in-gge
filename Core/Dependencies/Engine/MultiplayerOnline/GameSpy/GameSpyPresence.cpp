#include "StdAfx.h"
#include "GameSpyPresence.h"

/* Global Varible that should be modified only by the QR callback functions.
   The game should only have permission to read this value and not modify it.
   So declare only in cpp file and not as a static member of the GP class */
char name[31] = "";						// Used to store names when we recieve a message and also for buddy list
char eml[100] = "";						// Used to store email for buddy list.
std::map<GPProfile, int> inList;		// Used to keep track of buddy list(handles duplicates in buddy list when refreshing the list)
std::vector<GPProfile> profiles;		// Stores buddy list
std::vector<GPProfile> requests;		// Stores buddy requests
bool loggedIn = false;					// Are we logged in?
bool pendingRequests = false;			// Are there any pending requests?
std::vector<newMessage> messages;		// Queue up all messages.

/* Initialize static variable */
bool GameSpyPresence::showOnlyOnline = false; 

/* Returns the latest message that was recieved. Should be called in the lobby's update loop. */
void getMessage(std::vector<newMessage> *mess)
{
	*mess = messages;
	messages.erase(messages.begin(), messages.end());
}

/* Get methods for the global variables */
bool getLoggedIn()
{
	return loggedIn;
}

bool getPendingRequests()
{
	return pendingRequests;
}

/* Callback fucntion for gpGetInfo. Gets the profile info. */
void GetName(GPConnection *connection, void * arg_, void * param)
{
	GPGetInfoResponseArg * arg = (GPGetInfoResponseArg *)arg_;
	if(arg->result == GP_NO_ERROR)
	{
		strcpy_s(name, arg->nick);
		strcpy_s(eml, arg->email);
	}
	else
		strcpy_s(name,"ERROR");
}

/* Called when we recieve a buddy request. */
void RecieveBuddyRequestCB(GPConnection *connection, void * arg_, void * param)
{
	GPRecvBuddyRequestArg * arg = (GPRecvBuddyRequestArg *)arg_;
	requests.push_back(arg->profile);
	pendingRequests = true;
}

/* Called when we recieve a message */
void RecieveMessageCB(GPConnection *connection, void * arg_, void * param)
{
	char message[1000] = "";
	char from[31] = "";
	GPRecvBuddyMessageArg * arg = (GPRecvBuddyMessageArg *)arg_;
	gpGetInfo(connection, arg->profile, GP_CHECK_CACHE, GP_BLOCKING, GetName, NULL);
	strcpy_s(from, name);
	strcpy_s(message, arg->message);
	newMessage temp;
	temp.from = std::string(from);
	temp.message = std::string(message);
	messages.push_back(temp);
}

/* Callback function for gpSearchProfile. */
void SearchProfileCB(GPConnection *connection, void * arg_, void * param)
{
	GPProfileSearchResponseArg * arg = (GPProfileSearchResponseArg *)arg_;
	if(arg->numMatches > 0)
		gpSendBuddyRequest(connection, arg->matches[0].profile, "xxx");
}

/* Called when an error occurs. */
void ErrorCB(GPConnection *connection, void * arg_, void * param)
{
	GPErrorArg * arg = (GPErrorArg *) arg_;
	printf("\n%s\n", arg->errorString);
}

/* Called when a buddy's status changes */
void RecieveBuddyStatusCB(GPConnection * connection, void * arg_, void * param)
{
	GPRecvBuddyStatusArg * arg = (GPRecvBuddyStatusArg *)arg_;
	std::map<GPProfile, int>::iterator it = inList.find(arg->profile);
	GPBuddyStatus status;
	gpGetBuddyStatus(connection, arg->index, &status);
	if(it == inList.end())
	{
		if(!GameSpyPresence::getShowOnlyOnline() || (GameSpyPresence::getShowOnlyOnline() && status.status != GP_OFFLINE))
		{
			int size = profiles.size();
			profiles.push_back(arg->profile);
			inList.insert(std::pair<GPProfile, int>(arg->profile, size));
		}	
	}
	else
	{
		if(GameSpyPresence::getShowOnlyOnline() && status.status == GP_OFFLINE)
		{
			profiles.erase(profiles.begin() + (*it).second);
			inList.erase(it);
		}
	}
}

/* Called when we're connected to GameSpy Presence */
void ConnectCB(GPConnection *connection, void * arg_, void * param)
{
	printf("Connected to GameSpy Presence!");
	loggedIn = true;
}

/* Called when a new profile has been successfully created. */
void NewProfileCB(GPConnection *connection, void * arg_, void * param)
{
	printf("New Profile Created");
}

GameSpyPresence::GameSpyPresence()
{
	initialized = false;
}

/* get/set methods for showOnlyOnline */
void GameSpyPresence::setShowOnlyOnline(bool value)
{
	showOnlyOnline = value;
}

bool GameSpyPresence::getShowOnlyOnline()
{
	return showOnlyOnline;
}

/* Used to set status. If showOnlyOnline is set to true, then it's advisable to set status to online when you log in
   and set to offline when you log out. GameSpy is supposed to do this by default, but it didn't seem to work. Otherwise,
   the buddy list will not be updated when a buddy logs in/out. If showOnlyOnline is false, then it doesn't matter since
   all buddies will be displayed irrespective of status. */
void GameSpyPresence::PresenceSetStatus(bool online, char status[256])
{
	if(online)
		gpSetStatus(&connection, GP_ONLINE, status, "");
	else
		gpSetStatus(&connection, GP_OFFLINE, "", "");
}

/* Delete a buddy from list. */
void GameSpyPresence::PresenceDeleteBuddy(int index)
{
	gpDeleteBuddy(&connection, profiles[index]);
	profiles.erase(profiles.begin() + index);
}

/* Deny buddy request. */
void GameSpyPresence::PresenceDenyBuddyRequest(int index)
{
	gpDenyBuddyRequest(&connection, requests[index]);
	requests.erase(requests.begin() + index);
}

/* Some set methods. */
void GameSpyPresence::SetEmail(char em[100])
{
	strcpy_s(email, em);
}

void GameSpyPresence::SetPassword(char pass[30])
{
	strcpy_s(password, pass);
}

void GameSpyPresence::SetNickName(char nick[30])
{
	strcpy_s(nickName, nick);
}

/* Accept a buddy request. */
void GameSpyPresence::PresenceAcceptBuddyRequest(int index)
{
	gpAuthBuddyRequest(&connection, requests[index]);
	profiles.push_back(requests[index]);
	requests.erase(requests.begin() + index);
	if(requests.size() == 0)
		pendingRequests = false;
}

/* Returns buddy list. */
void GameSpyPresence::PresenceGetBuddyList(std::vector<profileInfo> *list)
{
	int size  = profiles.size();
	for(int i = 0; i < size; i++)
	{
		profileInfo info;
		gpGetInfo(&connection, profiles[i], GP_CHECK_CACHE, GP_BLOCKING, GetName, NULL);
		info.name = std::string(name);
		info.email = std::string(eml);
		(*list).push_back(info);
	}
}

/* Returns pending buddy requests. */
void GameSpyPresence::PresenceGetBuddyRequests(std::vector<profileInfo> *list)
{
	int size = requests.size();
	for(int i = 0; i < size; i++)
	{
		profileInfo info;
		gpGetInfo(&connection, requests[i], GP_CHECK_CACHE, GP_BLOCKING, GetName, NULL);
		info.name = std::string(name);
		info.email = std::string(eml);
		(*list).push_back(info);
	}
}

/* Initialize presence. Must be called first. */
bool GameSpyPresence::PresenceInit()
{
	GSIACResult result;
	GSIStartAvailableCheck(_T("uscggepc"));
	while((result = GSIAvailableCheckThink()) == GSIACWaiting)
		msleep(5);
	if(result != GSIACAvailable)
	{
		printf("Game Not Available\n");
		return false;
	}
	
	GPResult res = gpInitialize(&connection, 0, 1, 0);
	gpSetCallback(&connection, GP_ERROR,ErrorCB, NULL);
	gpSetCallback(&connection, GP_RECV_BUDDY_REQUEST,RecieveBuddyRequestCB, NULL);
	gpSetCallback(&connection, GP_RECV_BUDDY_MESSAGE,RecieveMessageCB, NULL);
	gpSetCallback(&connection, GP_RECV_BUDDY_STATUS, RecieveBuddyStatusCB, NULL);

	if( res == GP_NO_ERROR )
	{
		initialized = true;
		return true;
	}
	return false;
}

/* Connect to presence. Must be called only after Presence is initialized. */
bool GameSpyPresence::PresenceConnect()
{
	strcpy_s(GPConnectionManagerHostname, "gpcm.gamespy.com");
	// Srikkanth: nickName, email and password are currently NULL. We'll set those values when we have a UI and we can get input from the user.
	GPResult res = gpConnect(&connection, nickName, email, password, (GPEnum)0, (GPEnum)0, ConnectCB, NULL);
	if( res == GP_NO_ERROR)
	{
		gpProcess(&connection);
		return true;
	}
	return false;
}

/* Create new profile. */
bool GameSpyPresence::PresenceNewProfile(char newNick[30])
{
	GPResult res = gpNewProfile(&connection, newNick, GP_DONT_REPLACE, (GPEnum)0, NewProfileCB, NULL);
	if( res == GP_NO_ERROR )
		return true;
	return false;
}

/* Send buddy request. */
bool GameSpyPresence::PresenceSendBuddyRequest(char nick[31], char email[100], char f_name[40], char l_name[40])
{
	GPResult res = gpProfileSearch(&connection, nick, "", email, f_name, l_name, 0, GP_BLOCKING, (GPCallback)SearchProfileCB, NULL);
	if(res == GP_NO_ERROR)
		return true;
	return false;
}

/* This MUST be called in lobby update loop. Processes incoming messages from GameSpy and calls the necessary callbacks. */
void GameSpyPresence::PresenceProcess()
{
	if(initialized)
		gpProcess(&connection);
}

/*Send message to a buddy. */
bool GameSpyPresence::PresenceSendBuddyMessage(int index, char message[1000])
{
	GPResult res = gpSendBuddyMessage(&connection, profiles[index], message);
	if(res == GP_NO_ERROR)
		return true;
	return false;
}

/* Disconnect from Presence. */
void GameSpyPresence::PresenceDisconnect()
{
	gpDisconnect(&connection);
	gpDestroy(&connection);
}
