#include "qr2.h"
#include "qr2regkeys.h"
#include "natneg.h"
#include "nninternal.h"

/* Query Reporting Class definition. See cpp file for detailed description of methods. */
class QueryReporting
{
private:
	qr2_t *qrec;						// QR Handle
	int basePort;						// Base Port to use for connection to GameSpy's master server
	char gameName[20];					// The name of our game(as specified on GameSpy's server. Not the full name)
	char secretKey[10];					// Secret Key assigned to us by GameSpy
	int isPublic;						// Is this server public?
	int natNegotiate;					// Are we going to be doing NAT negotiation?(usually set to true).
	static char hostName[30];			// Name of the server
	static char gameVersion[5];			// Version of game we're running
	static int hostPort;				// Server's private port on which it's accepting connections
	static char mapName[30];			// Name of the map being played
	static char gameType[30];			// Game Type(death-match, campaign etc.,)
	static int numPlayers;				// Number of players currently in the game
	static int numTeams;				// Total number of teams in the game
	static int maxPlayers;				// Maximum number of players allowed
	static char gameMode[30];			// Game mode(depends on the game we're making)
	static bool teamPlay;				// Is it a team game?
	static int fragLimit;				// Maximum number of kills by a player after which game should end(only applicable to some games)
	
public:
	bool init();
	void think();
	void setGameName(char name[20]);
	void setSecretKey(char key[10]);
	void setIsPublic(bool pub);
	void setNatNegotiate(bool nat);
	void setBasePort(int port);
	qr2_t getQrec();
	QueryReporting();
	static void setHostName(char value[30]);
	static void setGameVersion(char value[5]);
	static void setHostPort(int value);
	static void setMapName(char value[30]);
	static void setGameType(char value[30]);
	static void setNumPlayers(int value);
	static void setNumTeams(int value);
	static void setMaxPlayers(int value);
	static void setGameMode(char value[30]);
	static void setTeamPlay(bool value);
	static void setFragLimit(int value);
	static void getHostName(char value[30]);
	static void getGameVersion(char value[5]);
	static int getHostPort();
	static void getMapName(char value[30]);
	static void getGameType(char value[30]);
	static int getNumPlayers();
	static int getNumTeams();
	static int getMaxPlayers();
	static void getGameMode(char value[30]);
	static bool getTeamPlay();
	static int getFragLimit();
};