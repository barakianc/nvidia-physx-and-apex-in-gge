//HavokWrapper.h
#ifndef _GAMEOBJECTMANAGER_H_
#define _GAMEOBJECTMANAGER_H_

#include <stdio.h>
///Include files for Engine
#include "Engine.h"

///Enumeration for creating collision layers between HavokObjects
enum CollisionLayerType
{
    COLLISION_LAYER_GROUND				= 1,//,//
    COLLISION_LAYER_MOVING_OBJECTS		= 2,//,//
    COLLISION_LAYER_STATIC_OBJECTS		= 3,//,//
    COLLISION_LAYER_KEYFRAMED_OBJECTS	= 4,//,//
	COLLISION_LAYER_CHARACTER_PLAYER	= 5,//,//
	COLLISION_LAYER_CHARACTER_NPC		= 6,//,//
    COLLISION_LAYER_PHANTOMS			= 7,//,//
    COLLISION_LAYER_VEHICLE_OBJECT		= 8 //
};

///Enumeration for the bounds for teleportation of CHARACTER object
enum HavokCharacterTeleportType
{
    TELEPORT_EQUAL	= 1,//,//
    TELEPORT_GR_Z	= 2,//,//
    TELEPORT_LS_Z	= 3,//,//
    TELEPORT_GR_Y	= 4,//,//
    TELEPORT_LS_Y	= 5,//,//
    TELEPORT_GR_X	= 6,//,//
    TELEPORT_LS_X	= 7//
};

///Include files for memory and math
#include <Common/Base/hkBase.h>
#include <Common/Base/System/hkBaseSystem.h>
#include <Common/Base/System/Error/hkDefaultError.h>
#include <Common/Base/System/Io/Istream/hkIstream.h>//STILL 
#include <Common/Base/Memory/System/Util/hkMemoryInitUtil.h>
#include <Common/Base/Monitor/hkMonitorStream.h>
#include <Common/Base/Memory/System/hkMemorySystem.h>
#include <Common/Base/Memory/Allocator/Malloc/hkMallocAllocator.h>
#include <Common/Base/Memory/Allocator/Thread/hkThreadMemory.h>//FALL2010 before it was -->Common/Base/Memory/hkThreadMemory
#include <Common/Base/Thread/Job/ThreadPool/Cpu/hkCpuJobThreadPool.h>//STILL
#include <Common/Base/Thread/Job/ThreadPool/Spu/hkSpuJobThreadPool.h>//STILL

///Include files for thread pool creation in creating the world
#include <Common/Base/Thread/JobQueue/hkJobQueue.h>//needed for creating world
#include <Common/Base/Thread/Job/ThreadPool/hkJobThreadPool.h>//needed for creating world

///Include files for Visual Debugger 
#include <Common/Visualize/hkVisualDebugger.h>//STILL 
#include <Physics/Utilities/VisualDebugger/hkpPhysicsContext.h>//STILL 		

///Include files for hkpWorld and other entities
#include <Physics/Dynamics/World/hkpWorld.h>
#include <Physics/Dynamics/Entity/hkpRigidBody.h>

///Include files for pre-defined shapes
#include <Physics/Collide/Shape/Convex/Box/hkpBoxShape.h>
#include <Physics/Collide/Shape/Convex/Sphere/hkpSphereShape.h>
#include <Physics/Collide/Shape/Convex/Triangle/hkpTriangleShape.h>
#include <Physics/Collide/Shape/Convex/ConvexVertices/hkpConvexVerticesShape.h>
#include <Physics/Collide/Shape/Compound/Tree/Mopp/hkpMoppBvTreeShape.h>
#include <Physics/Collide/Dispatch/hkpAgentRegisterUtil.h>

#include <Physics/Utilities/Dynamics/Inertia/hkpInertiaTensorComputer.h>
#include <Physics/Utilities/Actions/MouseSpring/hkpMouseSpringAction.h>

///Include files for filtering the groups of havok entities
#include <Physics/Collide/Filter/Group/hkpGroupFilter.h>
#include <Physics/Collide/Filter/Group/hkpGroupFilterSetup.h>

///Include files for loading hkx files
#include <Common/Serialize/Resource/hkResource.h>//STILL
#include <Common/Serialize/Util/hkRootLevelContainer.h>//STILL-->needed for loading in hkx files
#include <Common/Serialize/Util/hkLoader.h>//STILL-->needed for loading in hkx files
#include <Common/Serialize/Util/hkSerializeUtil.h>//STILL-->needed for loading in hkx files
#include <Common/Serialize/Packfile/hkPackfileData.h>//NEW
#include <Common/Compat/Deprecated/Packfile/hkPackfileReader.h>//FALL2010 Common/Serialize/Packfile/
#include <Physics/Utilities/Serialize/hkpPhysicsData.h>//STILL 
#include <Physics/Utilities/Serialize/hkpHavokSnapshot.h>//STILL 

///Include files for Key Framing
#include <Physics/Utilities/Constraint/Keyframe/hkpKeyFrameUtility.h>//STILL

///Include files for CollisionListeners and RayCasting
#include <Physics\Collide\Query\Collector\RayCollector\hkpClosestRayHitCollector.h>

#ifdef PHYSX
//PhysX header files
#include "PxPhysicsAPI.h"
#include "PxDefaultErrorCallback.h"
#include "PxDefaultAllocator.h"
#include "PxExtensionsAPI.h"
#include "PxCudaContextManager.h"
#ifdef PHYSX_APEX
#include "NxApex.h"
#include "destructible/public/NxModuleDestructible.h"
#include "GGEApexRenderer.h"
#include "clothing/public/NxModuleClothing.h"
#include <unordered_map>
#endif
//using namespace physx;
#endif

///Forward Declarations of Classes.
//Needed because of multiple .cpp/.h file configuration
class GameObject;
class GraphicsObject;
class AIWorldManager;

namespace GamePipe{
    class AIAgentManager;
}

//Different Collision Listener Functions For Allowing the Programmer To Write Different Types Of Collision Listener Functions
typedef	void (*Collision_Function_Type1)(GameObject* f_pOwnerGameObject,GameObject* f_pCollidedGameObject,hkVector4 &f_vCollisionPoint);	/* pointer to custom collision_function */
typedef	void (*Collision_Function_Type2)(GameObject* f_pOwnerGameObject,hkpRigidBody* f_pCollidedRigidBody,hkVector4 &f_vCollisionPoint);/* pointer to custom collision_function */

class PhysicsPrimitiveObject;
//Custom key framed function definition
typedef	void (*Custom_Keyframed_Function)(PhysicsPrimitiveObject* f_pOwnerObject,hkReal f_fDeltaTime,hkVector4 &f_pPositionToSet, hkQuaternion &f_pOrientationToSet);/* pointer to custom key framed function */

//Custom phantomAabb Collision Callback function definition
class hkpAabbPhantom;
typedef	void (*Custom_PhantomAabb_Collision_Callback_Function)(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime);/* pointer to custom phantomCollision function */

//Custom phantomShape Collision Callback function definition
class PhantomShapeObject;
typedef	void (*Custom_PhantomShape_Collision_Callback_Function)(PhantomShapeObject* f_pOwnerPhantom,hkpRigidBody* f_pCollidedRigidBody,GameObject* f_pCollidedGameObject);/* pointer to custom phantomCollision function */

//Custom characterProxy Collision Callback function definition
class CharacterProxyObject;
typedef	void (*Custom_CharacterProxy_Collision_Callback_Function)(CharacterProxyObject* f_pOwnerCharacterProxyObject,hkpRigidBody* f_pCollidedRigidBody,GameObject* f_pCollidedGameObject);/* pointer to custom CharacterProxyObjectCollision function */

/**********************************************GameObjectManagerDeclarationsSTART*********************************************/
///A manager class that handles the GameObjects
#ifdef PHYSX
class GameObjectManager : public physx::PxSimulationEventCallback
#endif
#ifdef HAVOK
class GameObjectManager
#endif
{
public:

    friend class GamePipe::GameScreen;

    ///GameObjectManager constructor.This function only initializes the memory needed by HavokPhysicsEngine
    GameObjectManager();

    ///GameObjectManager destructor
    ~GameObjectManager();

    //RDS_LOOK this needs to be created ~GameObjectManager();//De-constructor
private :
    ///AI Agent Manager
    GamePipe::AIAgentManager*       m_aiAgentManager;
	AIWorldManager*				m_recastManager;

    ///List of all the GameObjects created by the user.Needed to keep track of all GameObjects.
    std::list<GameObject*>			m_pGameObjectList;
	std::list<GameObject*>			m_pGameObjectsToDeleteList;
	std::list<GameObject*>			m_pGameObjectsToAddList;
	bool							m_bReachDeleteList;
	bool							m_bReachAddList;
	bool							m_bInitializationPeriod;

    ///HavokMemory variables needed for initialization and construction of HavokPhysics World
    ///Look into GameObjectManager constructor and CreateWorld functions for detailed information
    hkMemoryRouter*				m_pMemoryRouter;
    hkHardwareInfo				m_hardwareInfo;
    int							m_iTotalNumThreadsUsed;
    hkCpuJobThreadPoolCinfo		m_threadPoolCinfo;
    hkJobThreadPool*			m_pThreadPool;
    hkJobQueueCinfo				m_jobQueuInfo;
    hkJobQueue*					m_pJobQueue;

    //World control variables

    ///This is the pointer to GameObjectManager's Physics World.
    hkpWorld*					m_pPhysicsWorld;

    ///The info which has all the information about the PhysicsWorld - hkpWorld.
    hkpWorldCinfo				m_worldInfo;

    ///Variable for being able to change the game running speed.
    hkReal						m_fWorldStep;

    //Visual debugger variables
    ///Shows if the Visual Debugger is used or not. This variable is set at the beginning of the GameObjectManager constructor.
    bool						m_bVisualDebuggerActive;

    ///This is a Process Context array instance.For more info look at the GameObjectManager constructor.
    hkArray<hkProcessContext*>	m_arrayPhysicsContext;

    ///Pointer to the Visual Debugger which the GameObjectManager is associated with at the creation of the manager.
    hkVisualDebugger*			m_pVisualDebugger;

    ///This is a Physics context instance.For more info look at the GameObjectManager constructor.
    hkpPhysicsContext*			m_pPhysicsContext;
#ifdef PHYSX
	physx::PxProfileZoneManager*		m_PhysXZoneManager;
	physx::PxPhysics*					m_PhysXSDK;
	physx::PxCooking*					m_PhysXCooking;
	physx::PxMaterial*					m_PhysXMaterial;
	
	physx::PxScene*					m_PhysXScene;
	physx::PxSceneDesc* sceneDesc;
	bool						recordMemoryAllocations;
	
	physx::PxDefaultCpuDispatcher*		m_CpuDispatcher;
	physx::PxU32						m_NbThreads;

	bool bTrigger;

#ifdef PHYSX_APEX
	physx::apex::NxApexSDK*				m_ApexSDK;
	physx::apex::NxApexSDKDesc*			m_ApexSDKDesc;
	physx::apex::NxApexScene*			m_ApexScene;
	physx::apex::NxApexSceneDesc*		m_ApexSceneDesc;
	physx::apex::NxModuleDestructible*				m_apexDestructibleModule;
	physx::apex::NxModuleClothing*					m_apexClothModule;
	GGEApexRenderer*					m_GGEApexRender;
	bool								m_rewriteBuffers;
	//std::unordered_map<std::string,std::string> m_apexMatMap;
	//holds all our apex actors that have been created
	struct m_apexActorStruct{
		physx::apex::NxApexActor* m_apexActor;
		std::string				m_matName;
		int					m_actortype;//0 for destructible,1 for cloth
	}* m_ApexActors;

	int m_ApexActorCount;

#endif
#endif
private :

    ///This creates the Physics world for the user according to the values in the hkpWorldinfo
    void CreateWorld(bool f_bVisualDebuggerActive=false);

protected :
    ///This is called in the update loop to update the Physics simulation
    void UpdateWorld();

    ///When switching between screens the visual debugger connection needs this function to be stopped
    void PushBackManager();

    ///When switching between screens the visual debugger connection needs this function to be restarted
    void PopBackManager();

public:

	///Refreshes the game object manager list
	void DeleteGameObjects();
	void GameObjectManager::RefreshGameObjectsList(bool fromGameEngine = true);
    ///Adds a GameObject to GameObjectManager list
    void AddGameObject(GameObject* f_pNewGameObject);

    ///Deletes a GameObject from GameObjectManager list
    void RemoveGameObject(GameObject* f_pNewGameObject);

    ///Returns the number of GameObjects that has been created under the GameObjectManager
    int GetNumOfGameObjects();

    ///This function returns true if there is a GameObject with an entity that has the given name
    bool HasGameObject(Ogre::String f_sGameObjectOgrName);

    ///This function returns the GameObject with an entity that has the given name, if no such GameObject exist it returns NULL
    GameObject*	GetGameObject(Ogre::String f_sOgreUniqueName);		

    ///This Function returns the created hkpWorld
    hkpWorld* GetWorld();

    ///This functions turns the VisualDebugger on-off
    void SetVisualDebugger(bool f_bVisualDebuggerActive);

#ifdef PHYSX
	//

	physx::PxPhysics* GetPhysXWorld();

	//
	physx::PxScene* GetPhysXScene();

	//
	physx::PxCooking* GetPhysXCooking();

	physx::PxSceneDesc* GetPhysXDesc();

	physx::PxProfileZoneManager* GetPhysXZoneManager();

#ifdef PHYSX_APEX

	physx::apex::NxApexSDK* GetApexSDK();

	physx::apex::NxApexSDKDesc* GetApexSDKDesc();

	physx::apex::NxApexScene*	GetApexScene();

	physx::apex::NxApexSceneDesc*	GetApexSceneDesc();

	physx::apex::NxModuleDestructible*			GetDestructibleModule();


	GGEApexRenderer*							GetApexRender();

	void AddApexActor(physx::apex::NxApexActor* actor,std::string mat,int actortype);

#endif

	virtual void onConstraintBreak(physx::PxConstraintInfo* constraints, physx::PxU32 count);
	virtual void onWake(physx::PxActor** actors, physx::PxU32 count);
	virtual void onSleep(physx::PxActor** actors, physx::PxU32 count);
	virtual void onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs);
	virtual void onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count);

	void setTrigger(bool bt);
	bool getTrigger();
#endif

#ifdef HAVOK
	///Returns the iteration time used for physicsWorld
	hkReal GetWorldStep()
	{
		return m_fWorldStep;
	}
#endif
	AIWorldManager* getAIWorldManager();
    GamePipe::AIAgentManager* getAIAgentManager();

    ///This function casts a ray from a point to the other and sets the pointer variables according to the closest hit
    bool castRay(		Ogre::Vector3		f_vCastFrom,
						Ogre::Vector3		f_vCastTo,
						hkpRigidBody**		f_pFirstCollidedRigidBody,
						GameObject**		f_pCollidedGameObject,
						Ogre::Vector3*		f_vReturnCollisionPoint
				);
};

/*#ifdef PHYSX
class ContactCallback : public physx::PxSimulationEventCallback {
	virtual PX_INLINE void onContact (physx::PxContactPair & pair, physx::PxU32 events);
};
#endif*/
/***********************************************GameObjectManagerDeclarationsEND**********************************************/

#endif