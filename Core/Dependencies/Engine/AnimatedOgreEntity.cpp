#include "StdAfx.h"
#include "AnimatedOgreEntity.h"

using namespace AnimationManager;

AnimatedOgreEntity::AnimatedOgreEntity()
{
	////////////////////////////////////////
	// initialize animation states to "NULL"
	////////////////////////////////////////
	
	mCurrentState = NULL;
	mDestinationState = NULL;
	mTransitionState = NULL;
	mEndState = NULL;
	
	//////////////////////////////////
	// initialize strings to "None"
	//////////////////////////////////

	m_sActiveAnim = _strdup("None");
	m_sDefaultAnim = _strdup("None");

	////////////////////////
	// Set member variables
	////////////////////////
	
	mTransitionType = -1;
	mTimeleft = 0.0f;
	mDuration = 1.0f;
	mComplete = true;
	mLoop = false;
	m_pGraphicsObject = NULL;
	
}

AnimatedOgreEntity::AnimatedOgreEntity(GraphicsObject* f_pGraphicsObject, const char* f_sPath, const char* f_sName)
{
	////////////////////////////////////////
	// initialize animation states to "NULL"
	////////////////////////////////////////
	
	mCurrentState = NULL;
	mDestinationState = NULL;
	mTransitionState = NULL;
	mEndState = NULL;

	//////////////////////////////////
	// initialize strings to "None"
	//////////////////////////////////
	
	m_sActiveAnim = _strdup("None");
	m_sDefaultAnim = _strdup("None");

	////////////////////////
	// Set member variables
	////////////////////////

	mTransitionType = -1;
	mTimeleft = 0.0f;
	mDuration = 1.0f;
	mComplete = true;
	mLoop = false;
	m_pGraphicsObject = f_pGraphicsObject;
	m_sName = _strdup(f_sName);

	///////////////////////////////////////
	// add the animation names to an array
	///////////////////////////////////////

	m_aAnimationNames.clear();
	
	Ogre::AnimationStateIterator it = m_pGraphicsObject->m_pOgreEntity->getAllAnimationStates()->getAnimationStateIterator();
	while (it.hasMoreElements())
	{
		m_aAnimationNames.push_back(it.getNext()->getAnimationName().c_str());
	}
}

void AnimatedOgreEntity::Update(float f_fTimeStep)
{
	m_fDeltaTime = f_fTimeStep;

	if(mTransitionType == 0)
	{
		mCurrentState->addTime(m_fDeltaTime);
	}
	else if(mTransitionType == 1 || mTransitionType == 4)
	{
		if(mCurrentState->getTimePosition() < mCurrentState->getLength())
		{
			mCurrentState->addTime(m_fDeltaTime);
		}
		else
		{
			this->StopAnimation(m_sActiveAnim);
			
			if(mEndState != NULL)
			{
				mTransitionType = 0;
				
				m_sActiveAnim = mEndState->getAnimationName().c_str();

				mCurrentState = mEndState;
				mCurrentState->setEnabled(true);
				mCurrentState->setWeight(1);
				mCurrentState->setLoop(true);
				mCurrentState->addTime(m_fDeltaTime);

				mEndState = NULL;
			}
		}
	}
	else if(mTransitionType == 2)
	{
		if(mTimeleft > 0)
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then set its reduced weight as mTimeleft is decreasing
				mCurrentState->setWeight(Ogre::Real(mTimeleft/mDuration));
				mCurrentState->addTime(m_fDeltaTime);
			}
			
			mEndState->setWeight(Ogre::Real(1.0 - mTimeleft/mDuration));
			mEndState->addTime(m_fDeltaTime);

			mTimeleft -= m_fDeltaTime;
		}
		else
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then remove it and assign a end animation to current animation
				mCurrentState->setEnabled(false);
				mCurrentState->setLoop(false);
				mCurrentState->setWeight(0);
				mCurrentState = NULL;
			}

			mCurrentState = mEndState;
			mEndState = NULL;

			m_sActiveAnim = mCurrentState->getAnimationName().c_str();
			
			mCurrentState->setWeight(1);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 0;
		}
	}
	else if(mTransitionType == 3)
	{
		if(mTimeleft > 0)
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then set its reduced weight as mTimeleft is decreasing
				mCurrentState->setWeight(Ogre::Real(mTimeleft/mDuration));
				mCurrentState->addTime(m_fDeltaTime);
			}
			
			mEndState->setWeight(Ogre::Real(1.0 - mTimeleft/mDuration));
			mEndState->addTime(m_fDeltaTime);

			mTimeleft -= m_fDeltaTime;
		}
		else
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then remove it and assign a end animation to current animation
				mCurrentState->setEnabled(false);
				mCurrentState->setLoop(true);
				mCurrentState->setWeight(1);
				//mCurrentState = NULL;
			}

			// switching current and end
			mDestinationState = mCurrentState;
			mCurrentState = NULL;

			mCurrentState = mEndState;
			mEndState = mDestinationState;

			m_sActiveAnim = mCurrentState->getAnimationName().c_str();
			
			mCurrentState->setWeight(1);
			mCurrentState->setLoop(false);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 1;
		}
	}
	else if(mTransitionType == 5)
	{
		if(mCurrentState->getTimePosition() < mCurrentState->getLength())
		{
			mCurrentState->addTime(m_fDeltaTime);
		}
		else
		{
			this->StopAnimation(m_sActiveAnim);
			
			mTransitionType = 4;
				
			m_sActiveAnim = mDestinationState->getAnimationName().c_str();

			mCurrentState = mDestinationState;
			mCurrentState->setEnabled(true);
			mCurrentState->setWeight(1);
			mCurrentState->setLoop(false);
			mCurrentState->setTimePosition(0);
			mCurrentState->addTime(m_fDeltaTime);

			mDestinationState = NULL;
		}
	}
	else if(mTransitionType == 6)
	{
		if(mTimeleft > 0)
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then set its reduced weight as mTimeleft is decreasing
				mCurrentState->setWeight(Ogre::Real(mTimeleft/mDuration));
				mCurrentState->addTime(m_fDeltaTime);
			}
			
			mTransitionState->setWeight(Ogre::Real(1.0 - mTimeleft/mDuration));
			mTransitionState->addTime(m_fDeltaTime);

			mTimeleft -= m_fDeltaTime;
		}
		else
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then remove it and assign a end animation to current animation
				mCurrentState->setEnabled(false);
				mCurrentState->setLoop(false);
				mCurrentState = NULL;
			}

			mCurrentState = mTransitionState;

			m_sActiveAnim = mCurrentState->getAnimationName().c_str();
			
			mCurrentState->setWeight(1);
			mCurrentState->setLoop(false);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 1;
		}
	}
	else if(mTransitionType == 7)
	{
		if(mTimeleft > 0)
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then set its reduced weight as mTimeleft is decreasing
				mCurrentState->setWeight(Ogre::Real(mTimeleft/mDuration));
				mCurrentState->addTime(m_fDeltaTime);
			}
			
			mTransitionState->setWeight(Ogre::Real(1.0 - mTimeleft/mDuration));
			mTransitionState->addTime(m_fDeltaTime);

			mTimeleft -= m_fDeltaTime;
		}
		else
		{
			if(mCurrentState != NULL)
			{
				// if currently animation is playing then remove it and assign a end animation to current animation
				mCurrentState->setEnabled(false);
				mCurrentState->setLoop(false);
				mCurrentState = NULL;
			}

			mCurrentState = mTransitionState;

			m_sActiveAnim = mCurrentState->getAnimationName().c_str();
			
			mCurrentState->setWeight(1);
			mCurrentState->setLoop(false);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 5;
		}
	}
	else if(mTransitionType == 8)
	{
		mCurrentState->addTime(m_fDeltaTime);
		mEndState->addTime(m_fDeltaTime);
	}
	else if(mTransitionType == 9)
	{
		if(mEndState->getTimePosition() < mEndState->getLength())
		{
			mCurrentState->addTime(m_fDeltaTime);
			mEndState->addTime(m_fDeltaTime);
		}
		else
		{
			mEndState->setEnabled(false);
			mEndState->setLoop(false);
			mEndState = NULL;

			mCurrentState->setWeight(1);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 0;
		}
	}
	else if(mTransitionType == 10)
	{
		if(mTransitionState->getTimePosition() < mTransitionState->getLength())
		{
			mCurrentState->addTime(m_fDeltaTime);
			mTransitionState->addTime(m_fDeltaTime);
		}
		else
		{
			mTransitionState->setEnabled(false);
			mTransitionState->setLoop(false);
			mTransitionState = NULL;

			mEndState->setEnabled(true);
			mEndState->setLoop(true);
			mEndState->setWeight(0.5);

			mEndState->addTime(m_fDeltaTime);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 8;

		}
	}
	else if(mTransitionType == 11)
	{
		if(mTransitionState->getTimePosition() < mTransitionState->getLength())
		{
			mCurrentState->addTime(m_fDeltaTime);
			mTransitionState->addTime(m_fDeltaTime);
		}
		else
		{
			mTransitionState->setEnabled(false);
			mTransitionState->setLoop(false);
			mTransitionState = NULL;

			mEndState->setEnabled(true);
			mEndState->setLoop(false);
			mEndState->setWeight(0.5);

			mEndState->addTime(m_fDeltaTime);
			mCurrentState->addTime(m_fDeltaTime);

			mTransitionType = 9;
		}
	}
	else
	{
		// do nothing
	}
}

void AnimatedOgreEntity::PlayAnimation(const char* f_sDestinationAnimationName, const char* f_sTransitionAnimationName, const bool f_bLoop, const float f_fBlendDuration)
{
	//this->StopAnimation();
	// set local variables
	Ogre::AnimationState* oDestState = NULL;
	Ogre::AnimationState* oTransState = NULL;
	const char* sDestAnim = f_sDestinationAnimationName;
	const char* sTransAnim = f_sTransitionAnimationName;
	const char* sPrevAnim = m_sActiveAnim;
	bool bLoop = f_bLoop;
	float fBlendDuration = f_fBlendDuration;

	// if given animation name is not a valid animation name then do nothing and return
	if(!(this->HasAnimationName(sDestAnim)))
	{
		return;
	}
	
	oDestState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(sDestAnim);

	
	if(strcmp(sTransAnim,"") != 0)
	{
		// if the transition animation is a valid animation name then get its state
		if(this->HasAnimationName(sTransAnim))
		{
			oTransState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(sTransAnim);
		}
		else
		{
			//since transition animation is not a valid name do nothing just exit
			return;
		}
	}


	// set Transition Type

	if(strcmp(sTransAnim,"") == 0 && bLoop == true && fBlendDuration == 0.0f)
	{
		// stop all other animation if running
		this->StopAnimation();

		// set destination animation as active animation
		m_sActiveAnim = _strdup(sDestAnim);
		mCurrentState = oDestState;
		mComplete = false;
		mLoop = true;

		mCurrentState->setEnabled(true);
		mCurrentState->setWeight(1);
		mCurrentState->setLoop(true);
	
		mTransitionType = 0;
	}
	else if(strcmp(sTransAnim,"") == 0 && bLoop == false && fBlendDuration == 0.0f)
	{
		// stop all other animation if running
		this->StopAnimation();

		// set destination animation as active animation
		m_sActiveAnim = _strdup(sDestAnim);

		if(strcmp(sPrevAnim,"None") != 0)
		{
			// if there was previously playing animation present,
			// then get its state so that after playing destination animation once,
			// we can resume with the previous animation
			mEndState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(sPrevAnim);
		}

		mCurrentState = oDestState;
		mComplete = false;
		mLoop = false;

		mCurrentState->setEnabled(true);
		mCurrentState->setWeight(1);
		mCurrentState->setLoop(false);
		mCurrentState->setTimePosition(0);

		mTransitionType = 1;
	}
	else if(strcmp(sTransAnim,"") == 0 && bLoop == true && fBlendDuration != 0.0f)
	{
		// no need to check whether previously animation is playing or not,
		// that is handled in update function

		mTimeleft = f_fBlendDuration;
		mDuration = f_fBlendDuration;

		mEndState = oDestState;
		mEndState->setWeight(0);
		mEndState->setTimePosition(0);
		mEndState->setEnabled(true);
		mEndState->setLoop(true);

		mTransitionType = 2;
		
	}
	else if(strcmp(sTransAnim,"") == 0 && bLoop == false && fBlendDuration != 0.0f)
	{
		// no need to check whether previously animation is playing or not,
		// that is handled in update function

		mTimeleft = f_fBlendDuration;
		mDuration = f_fBlendDuration;

		mEndState = oDestState;
		mEndState->setWeight(0);
		mEndState->setTimePosition(0);
		mEndState->setEnabled(true);
		mEndState->setLoop(true);

		mTransitionType = 3;
	}
	else if(strcmp(sTransAnim,"") != 0 && bLoop == true && fBlendDuration == 0.0f)
	{
		// stop all other animation if running
		this->StopAnimation();

		// play transition animation once so make it active
		m_sActiveAnim = _strdup(sTransAnim);

		//After trasition animation is being played we need to play destination so store it in mEndAnimation
		mEndState = oDestState;

		mCurrentState = oTransState;
		mComplete = false;
		mLoop = false;

		mCurrentState->setEnabled(true);
		mCurrentState->setWeight(1);
		mCurrentState->setLoop(false);
		mCurrentState->setTimePosition(0);
		
		mTransitionType = 4;
	}
	else if(strcmp(sTransAnim,"") != 0 && bLoop == false && fBlendDuration == 0.0f)
	{
		// stop all other animation if running
		this->StopAnimation();

		// play the transition animation once so make it active
		m_sActiveAnim = _strdup(sTransAnim);

		//if there was previously playing animation present,
		//then store it in mEndState so that we can resume it
		if(strcmp(sPrevAnim,"None") != 0)
		{
			mEndState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(sPrevAnim);
		}

		// Stop Destination animation in mDestinationState so that we can play it once,
		// after transition animation is being played
		mDestinationState = oDestState;

		mCurrentState = oTransState;
		mComplete = false;
		mLoop = false;

		mCurrentState->setEnabled(true);
		mCurrentState->setWeight(1);
		mCurrentState->setLoop(false);
		mCurrentState->setTimePosition(0);

		mTransitionType = 5;
	}
	else if(strcmp(sTransAnim,"") != 0 && bLoop == true && fBlendDuration != 0.0f)
	{
		mTimeleft = f_fBlendDuration;
		mDuration = f_fBlendDuration;

		mTransitionState = oTransState;
		
		mTransitionState->setEnabled(true);
		mTransitionState->setLoop(true);
		mTransitionState->setWeight(0);
		mTransitionState->setTimePosition(0);

		mEndState = oDestState;
		mEndState->setWeight(1);
		mEndState->setTimePosition(0);
		mEndState->setEnabled(false);
		mEndState->setLoop(true);

		mTransitionType = 6;
	}
	else if(strcmp(sTransAnim,"") != 0 && bLoop == false && fBlendDuration != 0.0f)
	{
		mEndState = mCurrentState;
		
		mTimeleft = f_fBlendDuration;
		mDuration = f_fBlendDuration;

		mTransitionState = oTransState;
		
		mTransitionState->setEnabled(true);
		mTransitionState->setLoop(true);
		mTransitionState->setWeight(0);
		mTransitionState->setTimePosition(0);

		mDestinationState = oDestState;
		mDestinationState->setWeight(1);
		mDestinationState->setTimePosition(0);
		mDestinationState->setEnabled(false);
		mDestinationState->setLoop(false);

		mTransitionType = 7;
	}

}

void AnimatedOgreEntity::PlayAnimationOverlay(	const char* f_sOverlayAnimationName, const char* f_sTransitionAnimationName, const bool	f_bLoop, const float f_fBlendDuration)
{
	// set local variables
	Ogre::AnimationState* oOverlayState = NULL;
	Ogre::AnimationState* oTransState = NULL;
	const char* sOverlayAnim = f_sOverlayAnimationName;
	const char* sTransAnim = f_sTransitionAnimationName;
	const char* sPrevAnim = m_sActiveAnim;
	bool bLoop = f_bLoop;
	float fBlendDuration = f_fBlendDuration;

	// if animation name is not a valid name
	if(!(this->HasAnimationName(sOverlayAnim)))
	{
		return;
	}
	
	oOverlayState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(sOverlayAnim);

	if(strcmp(sTransAnim,"") != 0)
	{
		if(this->HasAnimationName(sTransAnim))
		{
			oTransState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(sTransAnim);
		}
		else 
		{
			// if animation name is not a valid name
			return;
		}
	}

	// set Transition Type

	if(strcmp(sTransAnim,"") == 0 && bLoop == true)
	{
		if(strcmp(sPrevAnim,"None") != 0)
		{
			mCurrentState->setWeight(0.5);

			// mEndState is being overlaid on the current playing animation
			mEndState = oOverlayState;
			mEndState->setEnabled(true);
			mEndState->setLoop(true);
			mEndState->setWeight(0.5);
			mTransitionType = 8;	
		}
		else
		{
			// if there is no previous played animation

			m_sActiveAnim = _strdup(sOverlayAnim);
			
			mCurrentState = oOverlayState;
			mCurrentState->setWeight(1);
			mCurrentState->setEnabled(true);
			mCurrentState->setLoop(true);

			mTransitionType = 0;
		}
	}
	else if(strcmp(sTransAnim,"") == 0 && bLoop == false)
	{
		if(strcmp(sPrevAnim,"None") != 0)
		{
			mCurrentState->setWeight(0.5);

			mEndState = oOverlayState;
			mEndState->setEnabled(true);
			mEndState->setLoop(false);
			mEndState->setTimePosition(0);
			mEndState->setWeight(0.5);
			mTransitionType = 9;	
		}
		else
		{
			m_sActiveAnim = _strdup(sOverlayAnim);
			
			mCurrentState = oOverlayState;
			mCurrentState->setWeight(1);
			mCurrentState->setEnabled(true);
			mCurrentState->setLoop(false);
			mCurrentState->setTimePosition(0);

			mTransitionType = 1;
		}
	}
	else if(strcmp(sTransAnim,"") != 0 && bLoop == true)
	{
		// store Transition animation in the mTrasitionState so that we can play it once
		mTransitionState = oTransState;

		if(strcmp(sPrevAnim,"None") != 0)
		{
			mCurrentState->setWeight(0.5);

			//store the overlay animation so that we can then play it after trasition animation is played once
			mEndState = oOverlayState;

			mTransitionState->setEnabled(true);
			mTransitionState->setLoop(false);
			mTransitionState->setTimePosition(0);
			mTransitionState->setWeight(0.5);
			mTransitionType = 10;
		}
		else
		{
			// if there was no previously played animation so make overlay animation default
			m_sActiveAnim = _strdup(sTransAnim);

			mEndState = oOverlayState;

			mCurrentState = oTransState;
			mCurrentState->setWeight(1);
			mCurrentState->setEnabled(true);
			mCurrentState->setLoop(false);
			
			mTransitionType = 1;
		}
	}
	else if(strcmp(sTransAnim,"") != 0 && bLoop == false)
	{
		// store Transition animation in the mTrasitionState so that we can play it once
		mTransitionState = oTransState;

		if(strcmp(sPrevAnim,"None") != 0)
		{
			mCurrentState->setWeight(0.5);

			//store the overlay animation so that we can then play it after trasition animation is played once
			mEndState = oOverlayState;

			mTransitionState->setEnabled(true);
			mTransitionState->setLoop(false);
			mTransitionState->setTimePosition(0);
			mTransitionState->setWeight(0.5);
			mTransitionType = 11;
		}
		else
		{
			// if there was no previously played animation so make overlay animation default
			this->PlayAnimation(sOverlayAnim,sTransAnim,false);
		}
	}
}

bool AnimatedOgreEntity::IsAnimationPlaying(const char* f_sAnimationName)
{	
	Ogre::AnimationState* CurrAnimState;

	if(this->HasAnimationName(f_sAnimationName))
	{
		CurrAnimState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(f_sAnimationName);
	
		if(CurrAnimState->getEnabled() == true)
		{
			return true;
		}
	}

	return false;
}


void AnimatedOgreEntity::StopAnimation(const char* f_sAnimationName)
{
	Ogre::AnimationState* CurrAnimState;

	// When function is called with no agrument stop all animations
	if(strcmp(f_sAnimationName,"") == 0)
	{
		// Setting Active animation to none
		m_sActiveAnim = _strdup("None");
		mCurrentState = NULL;
		mDestinationState = NULL;
		mTransitionState = NULL;
		mEndState = NULL;
		mTimeleft = 0.0f;
		mDuration = 1.0f;
		mComplete = true;
		//mLoop = false;
		mTransitionType = -1;

		for(int i = 0; i < (int)m_aAnimationNames.size(); i++)
		{
			// stoping all the animations
			CurrAnimState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(m_aAnimationNames[i]);
			CurrAnimState->setLoop(false);
			CurrAnimState->setEnabled(false);	
		}
	}
	else if(this->HasAnimationName(f_sAnimationName))	// If given animation name is valid animation name then stop it
	{
		// if stop is asked for currentAnimation
		if(strcmp(m_sActiveAnim,f_sAnimationName) == 0)
		{
			//if currentAnimation is the only animation playing
			if(mTransitionType < 8 || mTransitionType == 9 || mTransitionType == 11)
			{
				m_sActiveAnim = _strdup("None");
				mTransitionType = -1;
			}
			else if(mTransitionType == 8 || mTransitionType == 10)
			{
				// overlay is being play with two animation stop the activeAnim and make overlaid animation as active animation

				m_sActiveAnim = mEndState->getAnimationName().c_str();
				mCurrentState = mEndState;
				mCurrentState->setWeight(1);
				mEndState = NULL;
				mTransitionType = 0;
			}
		}
		else	// if stop is asked for animation other then active
		{
			
			if(mTransitionType == 8 || mTransitionType == 10) //if two animation are active
			{
				if(mEndState != NULL)
				{
					if(strcmp(mEndState->getAnimationName().c_str(),f_sAnimationName) == 0)
					{	
						// if the overlaid animation is to be stopped.
						mCurrentState->setWeight(1);
						mEndState = NULL;
						mTransitionType = 0;
					}
				}
			}
		}

		// finally stop the animation with given argument name
		CurrAnimState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(f_sAnimationName);
		//CurrAnimState->setLoop(false); //Think this will fix GGE issue
		CurrAnimState->setEnabled(false);
	}
}

const char* AnimatedOgreEntity::GetName()
{
	return m_sName;
}

const char* AnimatedOgreEntity::GetActiveAnimation()
{
	return m_sActiveAnim;
}

void AnimatedOgreEntity::SetDefaultAnimation(const char* f_sDefaultAnim)
{
	if(this->HasAnimationName(f_sDefaultAnim))
	{
		// if the given animation name is a valid animation name set it as a default

		m_sDefaultAnim = _strdup(f_sDefaultAnim);
		mDestinationState = m_pGraphicsObject->m_pOgreEntity->getAnimationState(m_sDefaultAnim);
		mDestinationState->setEnabled(false);
		mDestinationState->setLoop(false);
		mDestinationState->setWeight(1);
		mDuration = 1.0f;	
		mTimeleft = 0.0f;
		mComplete = true;
		mLoop = false;
	}
}

const char* AnimatedOgreEntity::GetDefaultAnimation()
{
	return m_sDefaultAnim;
}

std::vector<std::string> AnimatedOgreEntity::GetAnimationNames()
{
	return m_aAnimationNames;
}

void AnimatedOgreEntity::SetPosition(float x, float y, float z)
{
	m_pGraphicsObject->setPosition(x, y, z);
}