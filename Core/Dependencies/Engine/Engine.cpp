#include "StdAfx.h"
// Engine
#include "Engine.h"

// GameScreen
//#include "GameScreen.h"
//#include "Profiler.h"
//#include "Configuration.h"

// To be able to finalize physics by calling hkBaseSystem::quit
#include <Common/Base/hkBase.h>
#include <Common/Base/System/hkBaseSystem.h>

#ifdef TBB_PARALLEL_OPTION
#include "EngineUpdateTasks.h"
#endif

using namespace std;

namespace GamePipe
{
	//////////////////////////////////////////////////////////////////////////
	// Engine()
	//////////////////////////////////////////////////////////////////////////
	Engine::Engine()
	{
		this->m_hInst = NULL;
		this->m_hWnd = NULL;
		this->m_pRenderer = NULL;
		this->m_pRenderWindow = NULL;
		this->m_pLoadingScreen = NULL;
#ifdef TBB_PARALLEL_OPTION
		keepRendering = true;
		taskQueueSize = 0;
		initializing = true;
		screenTransition = false;
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// Run()
	//////////////////////////////////////////////////////////////////////////
	int Engine::Run()
	{
		GGE_PROFILER_SET_ENABLED;
		GGE_PROFILER_SET_TIMER;

		MSG msg = {0};

#ifdef TBB_PARALLEL_OPTION
		RenderingLoop loop;
		initializing = false;
		tbb::tbb_thread renderT(loop);
#endif

		while(WM_QUIT != msg.message)
		{
//Start the timer for the Engine::Run loop
#ifdef GGE_PROFILER2
			GGE_Profiler2Ptr->m_pTimer2->reset();
			GGE_Profiler2Ptr->m_fRunStartTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
#endif
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				if (!UpdateTime())
				{
					continue;
				}

				GGE_PROFILER_BEGIN_PROFILE("UPDATE");
				GGE_RESUME_PROFILING;
#ifdef TBB_PARALLEL_OPTION
				ParallelUpdate();
#else
				Update();
#endif
				GGE_PAUSE_PROFILING;
				GGE_PROFILER_END_PROFILE("UPDATE");

				Draw();
//Stop the timer for the Engine::Run loop and calculate total time spent
#ifdef GGE_PROFILER2
				GGE_Profiler2Ptr->m_fRunEndTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
				GGE_Profiler2Ptr->m_fRunTotalTime = GGE_Profiler2Ptr->m_fRunEndTime - GGE_Profiler2Ptr->m_fRunStartTime;
#endif
				//Ogre::WindowEventUtilities::messagePump();
			}
		}
#if !GGE_PROFILER3
		GGETRACELOG("Finalizing");
		if (FAILED(Finalize()))
		{
			return E_FAIL;
		}
#endif

		GGETRACELOG("Engine v2.0");
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize(int argc, char* argv[], string gFolder, std::string strDLLCoreBinPath) //strDLLCoreBinPath is only required for the DLLRelease build
	//////////////////////////////////////////////////////////////////////////
#ifndef DLLRELEASE
	int Engine::Initialize(int argc, char* argv[], std::string gFolder)
#else
	int Engine::Initialize(int argc, char* argv[], std::string gFolder, std::string strDLLCoreBinPath)
#endif
	{
		//Set the main fold for the game
		gameFolder = gFolder;

		GGETRACE("Engine v2.0");
		GGETRACE("Working Directory: ");
#ifndef DLLRELEASE
		system("cd");
#else	// if this isn't called, the current working directory is located in the browser's exe file...
		int len;
		int slength = (int)strDLLCoreBinPath.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, strDLLCoreBinPath.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, strDLLCoreBinPath.c_str(), slength, buf, len);
		std::wstring wstrPath(buf);
		delete[] buf;

		SetCurrentDirectory(wstrPath.c_str());
#endif

		if (argc >= 2)
		{
			stringstream ss;
			ss << argv[1];
			int iWindowHandle;
			ss >> iWindowHandle;
			GGETRACE("Window handle: %d", iWindowHandle);
			SetWindowHandle(reinterpret_cast<HWND>(iWindowHandle));
#ifdef GLE_EDITOR
			// Activate the editor
			m_Editor.SetEditorMode(true);
#endif
		}

		GGETRACE("Initializing Time");
		if (FAILED(InitializeTime()))
		{
			return E_FAIL;
		}

		GGETRACE("Initializing Graphics");
		if (FAILED(InitializeGraphics()))
		{
			return E_FAIL;
		}

		GGETRACE("Initializing Input");
		if (FAILED(InitializeInput()))
		{
			return E_FAIL;
		}

		GGETRACE("Initializing VideoCapture");
		if (FAILED(InitializeVideoCapture()))
		{
			return E_FAIL;
		}

		GGETRACE("Initializing Physics");
		if (FAILED(InitializePhysics()))
		{
			return E_FAIL;
		}

		GGETRACE("Initializing FlashGUI");
		if (FAILED(InitializeGUI()))
		{
			return E_FAIL;
		}

		

		GGETRACE("Initializing Audio");
		//Audio will be initialized when audio program starts
		//AudioManager::FMODInitializeAudio();

		UtilitiesPtr->Add("Copyright", new DebugText("GamePipe Game Engine v2.0", 0.5f, 0.98f, 1.5f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Center));
		UtilitiesPtr->Add("VidCap", new DebugText("Recording frames: no", 0.85f, 0.05f, 2.0f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));


//If GGEProfiler is being used, update the UI to blend in with the FPS menu overlay, otherwise continue to use the default one
#ifdef GGE_PROFILER2
		UtilitiesPtr->Add("CurrentFPS", new GamePipe::DebugText("", 0.80f, 0.02f, 2.25f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("FPS", new DebugText("", 0.9f, 0.02f, 2.0f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
#else
		UtilitiesPtr->Add("FPS", new DebugText("FPS: 30.0000", 0.9, 0.02, 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
#endif
		UtilitiesPtr->Add("ActiveCamera", new DebugText("Active Camera: Default", 0.5f, 0.02f, 2.0f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Center));

//Initialize the menus used for CPU Usage and FPS Monitoring
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->InitializeMenu();
#endif

#ifdef GLE_EDITOR
		// Initialize the engine
		UtilitiesPtr->Add("EditorMode", new DebugText("Mode: ", 0.5f, 0.05f, 2.0f, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Center));
		m_Editor.Init( this, m_pRenderer, &m_vecGameScreens );
#endif

#ifdef TBB_PARALLEL_OPTION
		tbb::task_scheduler_init tbb_init;
#endif
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitializeFlashGUI()
	//////////////////////////////////////////////////////////////////////////
	int Engine::InitializeGUI()
	{
		if (FAILED(FlashGUIPtr->Initialize(gameFolder)))
		{
			return E_FAIL;
		}

		return S_OK;
	}

#ifdef DLLRELEASE
	//////////////////////////////////////////////////////////////////////////
	// SetGameFolder(std::string gameFolder)
	//////////////////////////////////////////////////////////////////////////
	void Engine::SetGameFolder(std::string gFolder)
	{
		gameFolder = gFolder;
	}
#endif

	//////////////////////////////////////////////////////////////////////////
	// InitializeTime()
	//////////////////////////////////////////////////////////////////////////
	int Engine::InitializeTime()
	{
#ifdef TBB_PARALLEL_OPTION
		m_pTimer = new GGETimer();
#else
		m_pTimer = new Ogre::Timer();
#endif
		m_pTimer->reset();

		m_fTotalGameTime = 0;
		m_fDeltaTime = 0;

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitializeInputPtr()
	//////////////////////////////////////////////////////////////////////////
	int Engine::InitializeInput()
	{
		if (FAILED(InputPtr->Initialize()))
		{
			return E_FAIL;
		}

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitializePhysics()
	//////////////////////////////////////////////////////////////////////////
	int Engine::InitializePhysics()
	{
		//NxOgre::PhysXParams physXParams;
		//physXParams.setToDefault();
		//physXParams.mUseLog = true;
		//physXParams.mTimeController = NxOgre::PhysXParams::TC_NONE;
		//m_pPhysicsWorld = new NxOgre::World(physXParams);

		/*m_pPhysicsWorld = new NxOgre::World("FrameListener: no");
		m_pPhysicsWorld->getPhysXDriver()->createDebuggerConnection();*/
#ifdef PHYSX
		static physx::PxDefaultErrorCallback gDefaultErrorCallback;
		static physx::PxDefaultAllocator gDefaultAllocatorCallback;
	
		m_PhysXFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
		m_PVDConnection = NULL;
		if(!m_PhysXFoundation){
			printf("PxCreateFoundation failed!");
		}
#endif

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitializeGraphics()
	//////////////////////////////////////////////////////////////////////////
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		PAINTSTRUCT ps;
		HDC hdc;

		switch (message)
		{
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			EndPaint(hWnd, &ps);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		return S_OK;
	}

	int Engine::InitializeGraphics()
	{
		string ogrePluginsFile;

#if _DEBUG
		ogrePluginsFile = "plugins_d.cfg";
#else
		ogrePluginsFile = "plugins.cfg";
#endif

		// Check if the working directory has been set to ..\Core\Bin\.
		fstream OgrePlugins;
		OgrePlugins.open(ogrePluginsFile.c_str(), ios::in);
		if (!OgrePlugins.good())
		{
			string warningMessage = "Please ensure that the working directory is set to ..\\Core\\Bin\\.";
			MessageBoxA(NULL, warningMessage.c_str(), "Warning", MB_OK);
			OgrePlugins.close();
			GGEEXITNOW();
		}

		OgrePlugins.close();

		this->m_pRenderer = new Ogre::Root(ogrePluginsFile, "OgreConfig.txt", "Log.txt");

		// Load the resource locations and groups
		// Did you set the working directory to ..\Core\Bin\. ?
		Ogre::String secName, typeName, archName;
		Ogre::ConfigFile configFile;

		// making path to mediaPaths.txt
		string mediaPath;

		mediaPath += "..\\..\\";
		mediaPath += gameFolder;
		mediaPath += "\\MediaPaths.txt";

		if(ifstream(mediaPath.c_str()))
			configFile.load(mediaPath);
		else if(ifstream("..\\MediaPaths.txt"))
			configFile.load("..\\MediaPaths.txt");
		else if(ifstream("MediaPaths.txt"))
			configFile.load("MediaPaths.txt");
		else
		{
			cout << "MediaPaths.txt not found @ " << mediaPath << endl;
			return E_FAIL;
		}

		Ogre::ConfigFile::SectionIterator sectionIterator = configFile.getSectionIterator();
		while (sectionIterator.hasMoreElements())
		{
			secName = sectionIterator.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap *settingsMultiMap = sectionIterator.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator settingIterator;
			for (settingIterator = settingsMultiMap->begin(); settingIterator != settingsMultiMap->end(); ++settingIterator)
			{
				typeName = settingIterator->first;
				archName = settingIterator->second;
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
			}
		}

#if _DEBUG
		if (!m_pRenderer->showConfigDialog())
		{
			GGETRACELOG("Video configuration error.");
			return E_FAIL;
		}
#else
		if (!m_pRenderer->restoreConfig() && !m_pRenderer->showConfigDialog())
		{
			GGETRACELOG("Video configuration error.");
			return E_FAIL;
		}
#endif

		Ogre::RenderSystem* pRenderSystem = m_pRenderer->getRenderSystem();
		Ogre::ConfigOptionMap configOptionMap = pRenderSystem->getConfigOptions();

		int iWidth = 1024;
		int iHeight = 768;

		string strColorDepth = "32-bit";
		char cIgnore = '_';

		stringstream ss;

		ss << configOptionMap["Video Mode"].currentValue.c_str();
		ss >> iWidth >> cIgnore >> iHeight >> cIgnore >> strColorDepth;

		if (!GetWindowHandle())
		{
			HINSTANCE hInstance = NULL;

			WNDCLASSEX wcex;
			wcex.cbSize = sizeof(WNDCLASSEX);
			wcex.style = CS_HREDRAW | CS_VREDRAW;
			wcex.lpfnWndProc = WndProc;
			wcex.cbClsExtra = 0;
			wcex.cbWndExtra = 0;
			wcex.hInstance = hInstance;
			wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
			wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
			wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
			wcex.lpszMenuName = NULL;
			wcex.lpszClassName = L"EngineWindowClass";
			wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
			if (!RegisterClassEx(&wcex))
				return E_FAIL;

			this->m_hInst = hInstance;

			DWORD windowStyle;
			int showState;
			if (configOptionMap["Full Screen"].currentValue == "Yes")
			{
				windowStyle = WS_POPUP;
				showState = SW_SHOWMAXIMIZED;
			}
			else
			{
				windowStyle = WS_OVERLAPPEDWINDOW;
				showState = SW_SHOW;
			}

			RECT rc = { 0, 0, iWidth, iHeight };
			AdjustWindowRect(&rc, windowStyle, FALSE);
			this->m_hWnd = CreateWindow(L"EngineWindowClass", L"GamePipeGame", windowStyle,
				CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
				NULL);
			if (!this->m_hWnd)
				return E_FAIL;

			ShowWindow(this->m_hWnd, showState);
		}
		else
		{
			//GGETRACELOG("Waiting 7 seconds for debugger attach.");
			//Sleep(7000);
		}

		// If you break here, try deleting the configuration file.
			m_pRenderer->initialise(false);
			Ogre::NameValuePairList nvpl;
			nvpl["colourDepth"] = Ogre::StringConverter::toString(32);
			nvpl["FSAA"] = Ogre::StringConverter::toString(0);
			nvpl["FSAAQuality"] = Ogre::StringConverter::toString(0);
			nvpl["vsync"] = "false";
			HWND hWnd = FindWindow(0, L"GGE Live Editor");
			nvpl["externalWindowHandle"] = Ogre::StringConverter::toString(reinterpret_cast<int>(hWnd));
			nvpl["externalWindowHandle"] = Ogre::StringConverter::toString(137966);
			// If not using custom window, remember to populate window handle member variable.

			nvpl["externalWindowHandle"] = Ogre::StringConverter::toString(reinterpret_cast<int>(this->m_hWnd));

			bool fullScreen = false;
			if (configOptionMap["Full Screen"].currentValue == "Yes")
				fullScreen = true;
			else
				fullScreen = false;

			m_pRenderWindow = m_pRenderer->createRenderWindow("EngineRenderWindow", iWidth, iHeight, fullScreen, &nvpl);

		Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("Bootstrap");

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitializeVideoCapture()
	//////////////////////////////////////////////////////////////////////////
	int Engine::InitializeVideoCapture()
	{
		if(m_pRenderer->getRenderSystem()->getName() == "OpenGL Rendering Subsystem")
		{
			GGETRACELOG("Cannot perform capture in OpenGL!");
			//return E_FAIL;
		}

		else
		{
			if (FAILED(VideoCapturePtr->Initialize(m_pRenderWindow)))
			{
				return E_FAIL;
			}
			
			//return S_OK;
		}
		
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// AddGameScreen(GameScreen* pGameScreen, GameScreen* pCallingGameScreen)
	//////////////////////////////////////////////////////////////////////////
	void Engine::AddGameScreen(GameScreen* pGameScreen, GameScreen* pCallingGameScreen)
	{
		pGameScreen->SetCallingGameScreen(pCallingGameScreen);
		m_vecGameScreensToAdd.push_back(pGameScreen);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// physx::PxFoundation* GetPhysXFoundation()
	//////////////////////////////////////////////////////////////////////////
#ifdef PHYSX
	physx::PxFoundation* Engine::GetPhysXFoundation(){
		return m_PhysXFoundation;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// PVD::PvdConnection* Engine::GetPVDConnection()
	//////////////////////////////////////////////////////////////////////////
	PVD::PvdConnection* Engine::GetPVDConnection(){
		return m_PVDConnection;
	}
	
	//////////////////////////////////////////////////////////////////////////
	//void Engine::SetPVDConnection(physx::PxPhysics* phy,physx::PxVisualDebuggerConnectionFlags cf)
	//////////////////////////////////////////////////////////////////////////
	void	Engine::SetPVDConnection(physx::PxPhysics* phy,physx::PxVisualDebuggerConnectionFlags cf){
		/*if(m_PVDConnection != NULL && m_PVDConnection->i){
			m_PVDConnection->disconnect();
			m_PVDConnection->release();
		}*/
		m_PVDConnection = physx::PxVisualDebuggerExt::createConnection(phy->getPvdConnectionManager(), "127.0.0.1", 5425, 500,cf);
	}
#endif

	//////////////////////////////////////////////////////////////////////////
	// LoadPendingGameScreen()
	//////////////////////////////////////////////////////////////////////////
	void Engine::LoadPendingGameScreens()
	{
		GGE_PROFILER_RESET;
		if (m_vecGameScreensToAdd.size() > 0)
		{
#if !GGE_PROFILER3
			//TODO Fix this: Loading screen should only init once
#ifdef TBB_PARALLEL_OPTION
			GameScreenDeleteMutexType::scoped_lock lock(GameScreenLock);
			EnginePtr->initializing = true;
#endif
			if (GetLoadingScreen())
			{
				GetLoadingScreen()->LoadResources();
				GetLoadingScreen()->_BaseInitialize(true);
				GetLoadingScreen()->Show();
			}
#endif

			// Add game screens to the list
			for (size_t indexGameScreenToAdd = 0; indexGameScreenToAdd < m_vecGameScreensToAdd.size(); ++indexGameScreenToAdd)
			{
				GameScreen* gameScreenToAdd = m_vecGameScreensToAdd[indexGameScreenToAdd];
				m_vecGameScreens.push_back(gameScreenToAdd);

				gameScreenToAdd->LoadResources();
				gameScreenToAdd->_BaseInitialize();
			}

			GetRenderWindow()->update();
			Show();
			m_vecGameScreensToAdd.clear();

#if !GGE_PROFILER3
			if (GetLoadingScreen())
			{
				GetLoadingScreen()->UnloadResources();
				GetLoadingScreen()->_BaseDestroy(true);
			}
#endif
#ifdef TBB_PARALLEL_OPTION
			EnginePtr->initializing = false;
#endif
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// RemoveGameScreen(GameScreen* pGameScreen)
	//////////////////////////////////////////////////////////////////////////
	void Engine::RemoveGameScreen(GameScreen* pGameScreen)
	{
		GGE_PROFILER_RESET;
#ifdef TBB_PARALLEL_OPTION
		screenTransition = true;
#endif
		m_vecGameScreensToDelete.push_back(pGameScreen);
	}

	//////////////////////////////////////////////////////////////////////////
	// GetGameScreen(string name)
	//////////////////////////////////////////////////////////////////////////
	GameScreen* Engine::GetGameScreen(string name)
	{
		GameScreen* gameScreen = NULL;
		vector<GameScreen*>::iterator iter;

		for (iter = m_vecGameScreens.begin(); iter != m_vecGameScreens.end(); ++iter)
		{
			if ((*iter)->GetName() == name)
			{
				gameScreen = *iter;
			}
		}

		return gameScreen;
	}

	//////////////////////////////////////////////////////////////////////////
	// GetForemostGameScreen(bool includePopupScreen)
	//////////////////////////////////////////////////////////////////////////
	GameScreen* Engine::GetForemostGameScreen(bool includePopupScreen)
	{
		GameScreen* gameScreen = NULL;
		vector<GameScreen*>::iterator iter;

		for (iter = m_vecGameScreens.begin(); iter != m_vecGameScreens.end(); ++iter)
		{
			if (includePopupScreen && (*iter)->IsPopup())
			{
				continue;
			}

			gameScreen = *iter;
		}

		return gameScreen;
	}

	//////////////////////////////////////////////////////////////////////////
	// BringScreenToFront(string name)
	//////////////////////////////////////////////////////////////////////////
	void Engine::BringScreenToFront(string name)
	{
		GameScreen* screenToBringFront = NULL;

		vector<GameScreen*>::iterator iterGameScreens;
		for (iterGameScreens = m_vecGameScreens.begin(); iterGameScreens != m_vecGameScreens.end(); ++iterGameScreens)
		{
			if ((*iterGameScreens)->GetName() == name)
			{
				GameScreen* screenToBringFront = *iterGameScreens;
				m_vecGameScreens.erase(iterGameScreens);
				m_vecGameScreens.push_back(screenToBringFront);
				Show();
				break;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// SendScreenToBack(string name)
	//////////////////////////////////////////////////////////////////////////
	void Engine::SendScreenToBack(string name)
	{
		GameScreen* screenToSendBack = NULL;

		vector<GameScreen*>::iterator iterGameScreens;
		for (iterGameScreens = m_vecGameScreens.begin(); iterGameScreens != m_vecGameScreens.end(); ++iterGameScreens)
		{
			if ((*iterGameScreens)->GetName() == name)
			{
				GameScreen* screenToSendBack = *iterGameScreens;
				m_vecGameScreens.erase(iterGameScreens);
				m_vecGameScreens.insert(m_vecGameScreens.begin(), screenToSendBack);
				Show();
				break;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// ResetViewports()
	//////////////////////////////////////////////////////////////////////////
	void Engine::ResetViewports()
	{
		GetRenderWindow()->removeAllViewports();
		SetViewportIndex(0);
	}

#ifdef TBB_PARALLEL_OPTION
	//////////////////////////////////////////////////////////////////////////
	// ParallelUpdate()
	//////////////////////////////////////////////////////////////////////////

	int Engine::ParallelUpdate()
	{
#ifdef GGE_PROFILER2
		//Start the timer for the Update Loop
		GGE_Profiler2Ptr->m_fUpdateStartTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
		//Update the CPU statistics
		GGE_Profiler2Ptr->cpumeasurer->UpdatePeriodicData(GetDeltaTime());
		if (GGE_Profiler2Ptr->profilerenabled)
		{
			UtilitiesPtr->GetDebugText("CurrentFPS")->SetText("Current FPS: " + Ogre::StringConverter::toString(1 / GetDeltaTime()));
			UtilitiesPtr->GetDebugText("FPS")->SetText("");
		}
		else
		{
			UtilitiesPtr->GetDebugText("CurrentFPS")->SetText("");
			UtilitiesPtr->GetDebugText("FPS")->SetText("FPS: " + Ogre::StringConverter::toString(1 / GetDeltaTime()));
		}
#else
		//Update the default FPS text
		UtilitiesPtr->GetDebugText("FPS")->SetText("FPS: " + Ogre::StringConverter::toString(1 / GetDeltaTime()));
#endif
		if (GetForemostGameScreen())
		{
			UtilitiesPtr->GetDebugText("ActiveCamera")->SetText("Active Camera: " + GetForemostGameScreen()->GetActiveCamera()->getName());
		}
		GGE_PROFILER_BEGIN_PROFILE("Utilities");
		GGE_PROFILER_END_PROFILE("Utilities");
		tbb::task* EngineUpdateRoot = new( tbb::task::allocate_root() ) tbb::empty_task;
		tbb::task_list list;
		int size = 1;

		tbb::task *newTask = new( EngineUpdateRoot->allocate_child() ) InputUpdateChildTask ();
		list.push_back( *newTask );
		++size;
		newTask = new( EngineUpdateRoot->allocate_child() ) UtilitiesUpdateChildTask ();
		list.push_back( *newTask );
		++size;
		newTask = new( EngineUpdateRoot->allocate_child() ) FlashUpdateChildTask ();
		list.push_back( *newTask );
		++size;
		newTask = new( EngineUpdateRoot->allocate_child() ) PhysicsUpdateChildTask ();
		list.push_back( *newTask );
		++size;

		EngineUpdateRoot->set_ref_count(size);
		EngineUpdateRoot->spawn_and_wait_for_all(list);

		GGE_PROFILER_BEGIN_PROFILE("Graphics");
		UpdateGraphics();
		GGE_PROFILER_END_PROFILE("Graphics");

#ifdef GGE_PROFILER2
		//Call the Profiler's Update Loop
		GGE_Profiler2Ptr->UpdateMenu();
#endif

#ifdef GLE_EDITOR
		if(m_Editor.IsEditorMode())		UtilitiesPtr->GetDebugText("EditorMode")->SetText("Mode : Editor ");
		else							UtilitiesPtr->GetDebugText("EditorMode")->SetText("Mode : Game ");

		m_Editor.UpdateEditor();
#endif
#ifdef GGE_PROFILER2
		//Call the Profiler's Update Loop
		GGE_Profiler2Ptr->UpdateMenu();
#endif
		return S_OK;
	}
#endif

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	int Engine::Update()
	{
#ifdef GGE_PROFILER2
		//Start the timer for the Update Loop
		GGE_Profiler2Ptr->m_fUpdateStartTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
		//Update the CPU statistics
		GGE_Profiler2Ptr->cpumeasurer->UpdatePeriodicData(GetDeltaTime());
		if (GGE_Profiler2Ptr->profilerenabled)
		{
			UtilitiesPtr->GetDebugText("CurrentFPS")->SetText("Current FPS: " + Ogre::StringConverter::toString(1 / GetDeltaTime()));
			UtilitiesPtr->GetDebugText("FPS")->SetText("");
		}
		else
		{
			UtilitiesPtr->GetDebugText("CurrentFPS")->SetText("");
			UtilitiesPtr->GetDebugText("FPS")->SetText("FPS: " + Ogre::StringConverter::toString(1 / GetDeltaTime()));
		}
#else
		//Update the default FPS text
		UtilitiesPtr->GetDebugText("FPS")->SetText("FPS: " + Ogre::StringConverter::toString(1 / GetDeltaTime()));
#endif
		if (GetForemostGameScreen())
		{
			UtilitiesPtr->GetDebugText("ActiveCamera")->SetText("Active Camera: " + GetForemostGameScreen()->GetActiveCamera()->getName());
		}
		GGE_PROFILER_BEGIN_PROFILE("Utilities");
		UtilitiesPtr->_Update();
		GGE_PROFILER_END_PROFILE("Utilities");

		GGE_PROFILER_BEGIN_PROFILE("Flash");
		FlashGUIPtr->_Update();
		GGE_PROFILER_END_PROFILE("Flash");

		GGE_PROFILER_BEGIN_PROFILE("Input");
		UpdateInput();

		GGE_PROFILER_END_PROFILE("Input");

		GGE_PROFILER_BEGIN_PROFILE("Physics");
		UpdatePhysics();
		GGE_PROFILER_END_PROFILE("Physics");

		GGE_PROFILER_BEGIN_PROFILE("Graphics");
		UpdateGraphics();
		GGE_PROFILER_END_PROFILE("Graphics");

		GGE_PROFILER_BEGIN_PROFILE("Video Cap");
		UpdateVideoCapture();
		GGE_PROFILER_END_PROFILE("Video Cap");

#ifdef GLE_EDITOR
		if(m_Editor.IsEditorMode())		UtilitiesPtr->GetDebugText("EditorMode")->SetText("Mode : Editor ");
		else							UtilitiesPtr->GetDebugText("EditorMode")->SetText("Mode : Game ");

		m_Editor.UpdateEditor();
#endif

#ifdef GGE_PROFILER2
		//Call the Profiler's Update Loop
		GGE_Profiler2Ptr->UpdateMenu();
#endif
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// UpdateTime()
	//////////////////////////////////////////////////////////////////////////
	bool Engine::UpdateTime()
	{
		const unsigned long absoluteDeltaTime = m_pTimer->getMicroseconds();

		m_fDeltaTime = (float)(static_cast<float>(absoluteDeltaTime) * 1e-6);

		const float fFrameLockFPS = 60.0f;
		const float fFrameLockFPSReciprocal = 1 / fFrameLockFPS;

		bool resetTimer = false;
		if (fFrameLockFPSReciprocal > m_fDeltaTime)
		{
			resetTimer = false;
		}
		else
		{
			m_pTimer->reset();
			resetTimer = true;
		}

		m_fTotalGameTime += m_fDeltaTime;
		return resetTimer;
	}

	//////////////////////////////////////////////////////////////////////////
	// UpdateInput()
	//////////////////////////////////////////////////////////////////////////
	int Engine::UpdateInput()
	{
//Monitors the time spent updating Input
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->m_fInputStartTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
#endif
		InputPtr->Update();
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->m_fInputEndTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
		GGE_Profiler2Ptr->m_fInputTotalTime = GGE_Profiler2Ptr->m_fInputEndTime - GGE_Profiler2Ptr->m_fInputStartTime;
#endif
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// UpdatePhysics()
	//////////////////////////////////////////////////////////////////////////
	int Engine::UpdatePhysics()
	{
		//GetPhysicsWorld()->getPhysXDriver()->simulate(GetRealDeltaTime());
		//GetPhysicsWorld()->getCharacterController()->getNxControllerManager()->updateControllers();

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// UpdateGraphics()
	//////////////////////////////////////////////////////////////////////////
	int Engine::UpdateGraphics()
	{
//Monitors the time taken for the game logic
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->m_fGameLogicStartTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
#endif
		if (!m_vecGameScreens.size() && !m_vecGameScreensToAdd.size())
		{
			GGETRACELOG("That's all folks!");
			GGEEXIT();
		}

		if (m_vecGameScreensToDelete.size() > 0)
		{
			//Delete screens from screen list
#ifdef TBB_PARALLEL_OPTION
			GameScreenDeleteMutexType::scoped_lock lock(GameScreenLock);
#endif
			for (size_t iterGameScreensToDelete = 0; iterGameScreensToDelete < m_vecGameScreensToDelete.size(); ++iterGameScreensToDelete)
			{
				GameScreen* gameScreenToDelete = m_vecGameScreensToDelete[iterGameScreensToDelete];
				if (m_vecGameScreensToAdd.size() > 0)
				{
					for (size_t iterGameScreens = 0; iterGameScreens < m_vecGameScreensToAdd.size(); ++iterGameScreens)
					{
						if (m_vecGameScreensToAdd[iterGameScreens]->GetCallingGameScreen() &&
							m_vecGameScreensToAdd[iterGameScreens]->GetCallingGameScreen()->GetName() == gameScreenToDelete->GetName())
						{
							m_vecGameScreensToAdd[iterGameScreens]->SetCallingGameScreen(NULL);
						}
					}
				}

				for (size_t iterGameScreens = m_vecGameScreens.size() - 1; iterGameScreens >= 0; --iterGameScreens)
				{
					if (m_vecGameScreens[iterGameScreens]->GetName() == gameScreenToDelete->GetName())
					{
						m_vecGameScreens.erase(m_vecGameScreens.begin() + iterGameScreens);
						break;
					}
				}

				gameScreenToDelete->UnloadResources();
				gameScreenToDelete->_BaseDestroy();
				GetRenderWindow()->update();

				delete gameScreenToDelete;
				gameScreenToDelete = NULL;
#ifdef TBB_PARALLEL_OPTION
				if (!initializing)
				{
					taskQueue.clear();
					taskQueueSize=0;
				}
				screenTransition = false;
#endif
			}

			Show();
		}
		m_vecGameScreensToDelete.clear();

		LoadPendingGameScreens();

		GameScreen* foremostGameScreen = GetForemostGameScreen();
		for (size_t iterGameScreens = 0; iterGameScreens < m_vecGameScreens.size(); ++iterGameScreens)
		{
			GameScreen* gameScreen = m_vecGameScreens[iterGameScreens];
			if (gameScreen != foremostGameScreen && !gameScreen->BackgroundUpdate())
			{
				continue;
			}
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->m_fGameLogicEndTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
		GGE_Profiler2Ptr->m_fGameLogicTotalTime = GGE_Profiler2Ptr->m_fGameLogicEndTime - GGE_Profiler2Ptr->m_fGameLogicStartTime;
#endif
			//update only the foremost screen here
			gameScreen->_BaseUpdate();
		}

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// UpdateVideoCapture()
	//////////////////////////////////////////////////////////////////////////
	int Engine::UpdateVideoCapture()
	{
		VideoCapturePtr->Update();
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	int Engine::Show()
	{
		if (!m_vecGameScreens.size() && !m_vecGameScreensToAdd.size())
		{
			GGETRACELOG("No more screens to show!");
			GGEEXIT();
		}

		ResetViewports();

		GameScreen* foremostGameScreen = GetForemostGameScreen();
		vector<GameScreen*>::iterator iterGameScreens;
		//for (m_iterGameScreens = m_vecGameScreens.begin(); m_iterGameScreens != m_vecGameScreens.end(); ++m_iterGameScreens)
		for (size_t iterGameScreens = 0; iterGameScreens < m_vecGameScreens.size(); ++iterGameScreens)
		{
			//GameScreen* gameScreen = *m_iterGameScreens;
			GameScreen* gameScreen = m_vecGameScreens[iterGameScreens];
			gameScreen->_Show();
		}

#if GLE_EDITOR
		if (foremostGameScreen)
		{
			if (m_Editor.GetEditorConnectionStatus() == m_Editor.GGE_CONNECTED)
				m_Editor.Editor_GenericCommand(GGE_ActivatedScreen, foremostGameScreen->GetName());
		}
#endif

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	int Engine::Draw()
	{
		//GetPhysicsWorld()->getPhysXDriver()->render(GetRealDeltaTime());
//start monitoring the render time
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->m_fDrawStartTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
#endif
		GameScreen* foremostGameScreen = GetForemostGameScreen();

		for (size_t iterGameScreens = 0; iterGameScreens < m_vecGameScreens.size(); ++iterGameScreens)
		{
			GameScreen* gameScreen = m_vecGameScreens[iterGameScreens];
			if (gameScreen != foremostGameScreen && !gameScreen->BackgroundDraw())
			{
				continue;
			}
			gameScreen->_Draw();
		}

		// Show frame rate debug text here.
#ifndef TBB_PARALLEL_OPTION
		m_pRenderer->renderOneFrame();
#endif
#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->m_fDrawEndTime = (float)GGE_Profiler2Ptr->m_pTimer2->getMicroseconds();
		GGE_Profiler2Ptr->m_fDrawTotalTime = GGE_Profiler2Ptr->m_fDrawEndTime - GGE_Profiler2Ptr->m_fDrawStartTime;
#endif
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// Finalize()
	//////////////////////////////////////////////////////////////////////////
	int Engine::Finalize()
	{
#ifdef TBB_PARALLEL_OPTION
		keepRendering = false;
#endif
		GGETRACELOG("Finalizing Time");
		FinalizeTime();

		GGETRACELOG("Finalizing Input");
		FinalizeInput();

		GGETRACELOG("Finalizing Physics");
		FinalizePhysics();

		GGETRACELOG("Finalizing Graphics");
		FinalizeGraphics();

		GGETRACELOG("Finalizing Audio");
		FinalizeAudio();

#ifdef GLE_EDITOR
		GGETRACELOG("Finalizing Editor");
		m_Editor.FinalizeEditor();
#endif

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// FinalizeTime()
	//////////////////////////////////////////////////////////////////////////
	int Engine::FinalizeTime()
	{
		delete m_pTimer;
		m_pTimer = NULL;

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// FinalizeInputPtr()
	//////////////////////////////////////////////////////////////////////////
	int Engine::FinalizeInput()
	{
		InputPtr->Destroy();
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// FinalizePhysics()
	//////////////////////////////////////////////////////////////////////////
	int Engine::FinalizePhysics()
	{
		hkBaseSystem::quit();
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// FinalizeGraphics()
	//////////////////////////////////////////////////////////////////////////
	int Engine::FinalizeGraphics()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("Bootstrap");

		delete m_pRenderWindow;
		m_pRenderWindow = NULL;

		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// FinalizeVideoCapture()
	//////////////////////////////////////////////////////////////////////////
	int Engine::FinalizeVideoCapture()
	{
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// FinalizeAudio()
	//////////////////////////////////////////////////////////////////////////
	int Engine::FinalizeAudio()
	{
		AudioManager::UnLoadWwiseAudio();
		AudioManager::FMODUnLoadAudio("AudioScreen");

		return S_OK;
	}
}