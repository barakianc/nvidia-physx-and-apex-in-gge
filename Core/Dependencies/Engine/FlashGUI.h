#pragma once

#include "Base.h"

#include "OgreTextAreaOverlayElement.h"

// Hikari
#include "Hikari.h"

using namespace Hikari;


// Easy access
#define FlashGUIPtr (GamePipe::FlashGUI::GetInstancePointer())
#define FlashGUIRef (GamePipe::FlashGUI::GetInstanceReference())


namespace GamePipe
{
	class FlashGUI : public Singleton<FlashGUI>, public ObjectManager<Object>
	{
	protected:
		Ogre::Viewport* currentViewport;
		FlashGUI(); 
		~FlashGUI();
		
		

	public:
		HikariManager* FlashGUIManager;
		friend GamePipe::Singleton<FlashGUI>;
		int screenchange;
		/************************************************************************/
		/* Initialize                                                                     */
		/************************************************************************/
		int Initialize(Ogre::String gameFolder);
		void Update();
		void SetViewport(Ogre::Viewport* viewport);
		
		void DestroyAllControls();
		void RemoveFlashControl(FlashControl* controlToDestroy);

		FlashControl* CreateFlashMaterial(const Ogre::String& name);

		/************************************************************************/
		/* Inject Mouse                                                         */
		/************************************************************************/
	};
}