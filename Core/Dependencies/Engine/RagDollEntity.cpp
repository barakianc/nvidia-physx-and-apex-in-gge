#include "StdAfx.h"

#include "RagDollEntity.h"

RagdollObject::~RagdollObject()
{
#ifdef HAVOK
	m_physicsWorld->lock();
#endif
	if(m_ragdoll->getWorld())
	{
		m_ragdoll->removeFromWorld();
	}
	for ( int i = 0; i < m_ragdoll->m_rigidBodies.getSize(); i++ )
	{
		hkpRigidBody* rb = m_ragdoll->m_rigidBodies[ i ];

		// Explictly call to the destructor
		if ( rb != HK_NULL )
		{
			rb->~hkpRigidBody();
		}

	}
#ifdef HAVOK
	m_physicsWorld->unlock();
#endif
	delete m_ragdollRigidBodyController;

}

RagdollObject::RagdollObject(const char* hkx_Path, hkReal scale) 
{
#ifdef HAVOK
	m_physicsWorld = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetWorld();
#endif

#ifdef PHYSX

#endif

	hkLoader* m_loader = new hkLoader();

	hkRootLevelContainer* container = m_loader->load( hkx_Path);
	HK_ASSERT2(0x27343437, container != HK_NULL , "Could not load asset");

	m_ragdoll = reinterpret_cast<hkaRagdollInstance*>( container->findObjectByType( hkaRagdollInstanceClass.getName() ) );
	HK_ASSERT2(0, m_ragdoll, "Couldn't load ragdoll setup");

	// Assign the ragdoll skeleton
	m_ragdollSkeleton = const_cast<hkaSkeleton*>(m_ragdoll->getSkeleton());

	// This routine iterates through the bodies pointed to by the constraints and stabilizes their inertias.
	// This makes both ragdoll controllers lees sensitive to angular effects and hence more effective
	const hkArray<hkpConstraintInstance*>& constraints = m_ragdoll->getConstraintArray();
	hkpInertiaTensorComputer::optimizeInertiasOfConstraintTree( constraints.begin(), constraints.getSize(), m_ragdoll->getRigidBodyOfBone(0) );

	// Find the two mappings
	{
		// Iterate over all hkaSkeletonMapper objects
		void* objectFound = container->findObjectByType( hkaSkeletonMapperClass.getName() );
		while ( objectFound )
		{
			hkaSkeletonMapper* mapper = reinterpret_cast<hkaSkeletonMapper*>( objectFound );
			// Use the skeleton to determine which mapper is which
			// Assign the animation skeleton
			if ( mapper->m_mapping.m_skeletonA == m_ragdoll->getSkeleton() )
			{
				m_animationFromRagdoll = mapper;
				m_animationSkeleton = mapper->m_mapping.m_skeletonB.val();
			}
			if ( mapper->m_mapping.m_skeletonB == m_ragdoll->getSkeleton() )
			{
				m_ragdollFromAnimation = mapper;
				m_animationSkeleton = mapper->m_mapping.m_skeletonA.val();
			}
			// Find the next object of this type
			objectFound = container->findObjectByType( hkaSkeletonMapperClass.getName(), objectFound );
		}
		HK_ASSERT2( 0, m_animationFromRagdoll, "Couldn't load high-to-ragdoll mapping" );
		HK_ASSERT2( 0, m_ragdollFromAnimation, "Couldn't load ragdoll-to-high mapping" );
	}
#ifdef HAVOK
	m_ragdoll->addToWorld( m_physicsWorld, true );
#endif
	m_ragdollRigidBodyController = new hkaRagdollRigidBodyController( m_ragdoll );

	//m_ragdollRigidBodyController->driveToPose( 1.0f/60.0f, ragdollPose.accessSyncedPoseLocalSpace().begin(), hkQsTransform::IDENTITY, HK_NULL );
}