#include "StdAfx.h"
// FlashGUI
#include "FlashGUI.h"

// Engine
#include "Engine.h"

//
#include "GameScreen.h"




using namespace Hikari;

namespace GamePipe
{ 
	//////////////////////////////////////////////////////////////////////////
	// FlashGUI() Contructor
	//////////////////////////////////////////////////////////////////////////
	FlashGUI::FlashGUI() 
	{
		FlashGUIManager = NULL;   
	}

	//////////////////////////////////////////////////////////////////////////
	// ~FlashGUI()  Destructor
	//////////////////////////////////////////////////////////////////////////
	FlashGUI::~FlashGUI() 
	{
		delete FlashGUIManager;

	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	void FlashGUI::Update()     // Update fuction updates the FlashGUIManager and switches to the intended screen
	{
		FlashGUIManager->update();         

		switch (screenchange)    // screenchange denotes the screen to be added or closed
		{
			case 1 : 
			{
				EnginePtr->GetForemostGameScreen()->Close();
				screenchange=0;
				break;
			 }
			case 2 : 
			{
				screenchange=0;
				break;
			}
		}
	}
//////////////////////////////////////////////////////////////////////////
	// SetViewport()
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// SetViewport()
	//////////////////////////////////////////////////////////////////////////
	void FlashGUI::SetViewport(Ogre::Viewport* viewport)
	{
		currentViewport=viewport;     // Set the currentViewport to the active screen's viewport
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	int FlashGUI::Initialize(Ogre::String gameFolder)
	{
		FlashGUIManager = new HikariManager("..\\..\\" + gameFolder + "\\Media\\gui");  // Set the path of .swf files
		screenchange=0;
		return S_OK;
	}

	//////////////////////////////////////////////////////////////////////////
	// CreateFlashMaterial()
	//////////////////////////////////////////////////////////////////////////
	FlashControl*  FlashGUI::CreateFlashMaterial(const Ogre::String& name)
	{
		FlashControl *control = FlashGUIManager->createFlashMaterial(name, 500, 500);
		return control;

	}

	void FlashGUI::DestroyAllControls()
	{
		FlashGUIPtr->FlashGUIManager->destroyAllControls();
	}

	void FlashGUI::RemoveFlashControl(FlashControl* controlToDestroy)
	{

		FlashGUIPtr->FlashGUIManager->destroyFlashControl(controlToDestroy);

	}
}