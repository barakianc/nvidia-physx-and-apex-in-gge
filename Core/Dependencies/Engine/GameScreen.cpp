#include "StdAfx.h"
// GameScreen
#include "GameScreen.h"

// TinyXML
#include "tinyxml.h"

// Havok Height Map
#include "Physics/Collide/Shape/HeightField/StorageSampledHeightField/hkpStorageSampledHeightFieldShape.h"

#ifdef HAVOK
#ifndef _HAVOK_WRAPPER_H_
#include "HavokWrapper.h"
#endif
#endif

#include "GameObject.h"

#ifdef TBB_PARALLEL_OPTION
//#include "Threading/TaskTypes.h"
#include "TaskContainer.h"
#include "CameraUpdateTask.h"
class TaskContainer;
#endif

namespace GamePipe
{


	//////////////////////////////////////////////////////////////////////////
	// GameScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	GameScreen::GameScreen(std::string name, bool supportsTerrain /* = false */)
	{
		SetName(name);
		m_bSupportsTerrain = supportsTerrain;

		if (!EnginePtr->GetRenderer() || !EnginePtr->GetRenderer()->isInitialised())
		{
			GGETRACELOG("Expecting initialized Ogre.");
			GGEEXITNOW();
		}

		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0f));
		SetViewport(NULL);
		SetCallingGameScreen(NULL);

		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// ~GameScreen()
	//////////////////////////////////////////////////////////////////////////
	GameScreen::~GameScreen()
	{
	}

	//////////////////////////////////////////////////////////////////////////
	// CreateDefaultSceneManager()
	//////////////////////////////////////////////////////////////////////////
	Ogre::SceneManager* GameScreen::CreateDefaultSceneManager()
	{
		if(m_bSupportsTerrain)
			return EnginePtr->GetRenderer()->createSceneManager("TerrainSceneManager", GetName() + "SceneManager");
		else
			return EnginePtr->GetRenderer()->createSceneManager(Ogre::ST_GENERIC, GetName() + "SceneManager");
	}

	//////////////////////////////////////////////////////////////////////////
	// CreateDefaultSceneManager()
	//////////////////////////////////////////////////////////////////////////
	void GameScreen::CreateDefaultPhysicsScene(bool f_bVisualDebuggerActive)
	{
		m_pGameObjectManager = new GameObjectManager();
		m_pGameObjectManager->CreateWorld(f_bVisualDebuggerActive);
	}

	//////////////////////////////////////////////////////////////////////////
	// DestroyDefaultSceneManager()
	//////////////////////////////////////////////////////////////////////////
	void GameScreen::DestroyDefaultSceneManager()
	{
		SetViewport(NULL);
		GetDefaultSceneManager()->clearScene();
		//GetDefaultSceneManager()->destroyAllCameras();
		EnginePtr->GetRenderer()->destroySceneManager(GetDefaultSceneManager());
	}

	//////////////////////////////////////////////////////////////////////////
	// CreateDefaultCamera()
	//////////////////////////////////////////////////////////////////////////
	Ogre::Camera* GameScreen::CreateDefaultCamera()
	{
		std::string cameraSceneNodeName = GetName() + "CameraSceneNode";
		Ogre::SceneNode* pCameraSceneNode = NULL;
		if (GetDefaultSceneManager()->hasSceneNode(cameraSceneNodeName) == true)
		{
			pCameraSceneNode = GetDefaultSceneManager()->getSceneNode(cameraSceneNodeName);
		}
		else
		{
			pCameraSceneNode = GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode(cameraSceneNodeName);
		}

		std::string cameraName = GetName() + "Camera";
		Ogre::Camera *pCamera = NULL;
		if (GetDefaultSceneManager()->hasCamera(cameraName) == true)
		{
			pCamera = GetDefaultSceneManager()->getCamera(cameraName);
			//pCamera->detatchFromParent();
		}
		else
		{
			pCamera = GetDefaultSceneManager()->createCamera(cameraName);
		}
		pCameraSceneNode->attachObject(pCamera);

		pCamera->setNearClipDistance(0.1f);

		m_v3CameraPosition = Ogre::Vector3(0.0000, 0.0000, 200.0000);
		m_qCameraOrientation = Ogre::Quaternion::IDENTITY;
		m_fCameraMovementSpeed = 5.0;
		m_fCameraRotationSpeed = 5.0;

		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return pCamera;
	}

	//////////////////////////////////////////////////////////////////////////
	// DestroyDefaultPhysicsScene()
	//////////////////////////////////////////////////////////////////////////
	void GameScreen::DestroyDefaultPhysicsScene()
	{
		delete(m_pGameObjectManager);
		if (m_pCallingGameScreen!=NULL)
			m_pCallingGameScreen->GetGameObjectManager()->PopBackManager();
	}

	void GameScreen::SetActiveCamera(Ogre::Camera* pCamera)
	{
		m_pActiveCamera = pCamera;

		if (GetViewport())
		{
			GetViewport()->setCamera(pCamera);
		}
	}

	//////////////////////////////////////////////////////////////////////////
    // LoadTerrain(std::string cfgFileName)
    //////////////////////////////////////////////////////////////////////////
	bool GameScreen::LoadTerrain(std::string cfgFileName, std::string resourceGroupName /*= "General"*/)
	{
		assert(m_bSupportsTerrain);

		GetDefaultSceneManager()->setWorldGeometry( cfgFileName ); // creates an Ogre scene node named "Terrain"
		
		// load in the config file so we can read its properties	
		Ogre::ConfigFile terrainCfg;
		terrainCfg.load( cfgFileName, resourceGroupName, "\x09:=", true );
		std::string heightMapImgName = terrainCfg.getSetting( "Heightmap.image", Ogre::StringUtil::BLANK, "HeightmapImageNotFound" );
		
		// if we didn't find the height map image property, there's nothing more we can do
		if( heightMapImgName.compare( "HeightmapImageNotFound" ) == 0 )
			return false;

		float heightScale = (float)atof( terrainCfg.getSetting( "MaxHeight", Ogre::StringUtil::BLANK, "1.0" ).c_str() );
		float pageWorldX = (float)atof( terrainCfg.getSetting( "PageWorldX", Ogre::StringUtil::BLANK, "1.0" ).c_str() );
		float pageWorldZ = (float)atof( terrainCfg.getSetting( "PageWorldZ", Ogre::StringUtil::BLANK, "1.0" ).c_str() );

		// now we can load the image
		Ogre::Image heightMap;
		heightMap.load(heightMapImgName, resourceGroupName);

		// and reconstruct the height map to create a havok object
		const int xRes = heightMap.getWidth();
		const int zRes = heightMap.getHeight();		   
		hkArray<hkReal> heightData;
		heightData.setSize(xRes * zRes);
		hkReal minHeight = FLT_MAX, maxHeight = FLT_MIN;
		for( int x = 0; x < xRes; x++ )
		{
		   for( int z = 0; z < zRes; z++ )
		   {
			  hkReal height = heightScale * heightMap.getColourAt( x, z, 0 ).r;
			  heightData[z * zRes + x] = height;

			  if( height < minHeight )
				  minHeight = height;

			  if( height > maxHeight )
				  maxHeight = height;
		   }
		}

		// we'll need to scale the havok map according to the ratio of page size to resolution in the X and Z directions
		// as noted on http://software.intel.com/en-us/forums/showthread.php?t=68905,
		// "Remember that a 64x64 heighfield with scale 1.0 in each direction will be 63 meters on each side."
		// So we need to factor that difference into our scale size
		float xScale = pageWorldX / (xRes - 1);
		float zScale = pageWorldZ / (zRes - 1);
#ifdef HAVOK
		// construct the height field shape
		hkpSampledHeightFieldBaseCinfo sampledHeightFieldInfo;
		sampledHeightFieldInfo.m_xRes = xRes;
		sampledHeightFieldInfo.m_zRes = zRes;
		sampledHeightFieldInfo.m_minHeight = minHeight;
		sampledHeightFieldInfo.m_maxHeight = maxHeight;
		sampledHeightFieldInfo.m_scale = hkVector4(xScale, 1.0f, zScale);
		hkpStorageSampledHeightFieldShape * heightFieldShape = new hkpStorageSampledHeightFieldShape(sampledHeightFieldInfo, heightData);

		// now create the rigid body associated with this shape 
		hkpRigidBodyCinfo heightFieldRigidBodyInfo;
		// we don't want objects to penetrate the terrain; make this small
		heightFieldRigidBodyInfo.m_allowedPenetrationDepth = 0.05f;
		heightFieldRigidBodyInfo.m_shape = heightFieldShape;
		// the terrain doesn't move; fix its position in space
		heightFieldRigidBodyInfo.m_motionType = hkpMotion::MOTION_FIXED;
		hkpRigidBody * heightFieldRigidBody = new hkpRigidBody(heightFieldRigidBodyInfo);
		
		// finally, add the rigid body to the havok physics world
		GetGameObjectManager()->GetWorld()->addEntity( heightFieldRigidBody );
#endif
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// AddGameScreen(GameScreen* pGameScreen)
	//////////////////////////////////////////////////////////////////////////
	void GameScreen::AddGameScreen(GameScreen* pGameScreen)
	{
		EnginePtr->AddGameScreen(pGameScreen, this);
	}

	//////////////////////////////////////////////////////////////////////////
	// RemoveGameScreen(GameScreen* pGameScreen)
	//////////////////////////////////////////////////////////////////////////
	void GameScreen::RemoveGameScreen(GameScreen* pGameScreen)
	{
		EnginePtr->RemoveGameScreen(pGameScreen);
	}

	//////////////////////////////////////////////////////////////////////////
	// GetDefaultSceneManager()
	//////////////////////////////////////////////////////////////////////////
	Ogre::SceneManager* GameScreen::GetDefaultSceneManager()
	{
		return EnginePtr->GetRenderer()->getSceneManager(GetName() + "SceneManager");
	}

	//////////////////////////////////////////////////////////////////////////
	// GetDefaultCamera()
	//////////////////////////////////////////////////////////////////////////
	Ogre::Camera* GameScreen::GetDefaultCamera()
	{
		return GetDefaultSceneManager()->getCamera(GetName() + "Camera");
	}

	//////////////////////////////////////////////////////////////////////////
	// GetDefaultCameraSceneNode()
	//////////////////////////////////////////////////////////////////////////
	Ogre::SceneNode* GameScreen::GetDefaultCameraSceneNode()
	{
		return GetDefaultSceneManager()->getSceneNode(GetName() + "CameraSceneNode");
	}

	//////////////////////////////////////////////////////////////////////////
	// IsBackgroundScreen()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::IsBackgroundScreen()
	{
		return (EnginePtr->GetForemostGameScreen(true) != EnginePtr->GetForemostGameScreen());
	}	

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// BaseInitialize()
	// Will be called every time when Add a screen
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::_BaseInitialize(bool isLoadingScreen)
	{
		if (m_pCallingGameScreen!=NULL)
			m_pCallingGameScreen->GetGameObjectManager()->PushBackManager();
		CreateDefaultSceneManager();
		SetActiveCamera(CreateDefaultCamera());
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0f));

		if (!isLoadingScreen)
		{
			CreateDefaultPhysicsScene();

			OgreConsole::getSingletonPtr()->init(Ogre::Root::getSingletonPtr(), GetDefaultSceneManager());
			OgreConsole::getSingletonPtr()->setVisible(false);

		}

		return Initialize();
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	// Will be called every time screen got destroyed
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::_BaseDestroy(bool isLoadingScreen)
	{
		if (!isLoadingScreen) DestroyDefaultPhysicsScene();
		DestroyDefaultSceneManager();
        //OgreConsole::getSingletonPtr()->shutdown();
		return Destroy();
	}

	//////////////////////////////////////////////////////////////////////////
	// _Show()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::_Show()
	{
#ifdef GLE_EDITOR
		if (EnginePtr->GetEditor()->IsEditorMode())
		{
			Ogre::Camera* pCamera = GetActiveCamera();
			Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
			viewport->setBackgroundColour(GetBackgroundColor());
			viewport->setOverlaysEnabled(true);
			SetViewport(viewport);

			// This might potentially override the camera loaded via DotScenePlus
			pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));		

			return true;
		}
		else
#endif
		{
			//return Show();
            /*OgreConsole::getSingletonPtr()->init(Ogre::Root::getSingletonPtr(), GetDefaultSceneManager());
            OgreConsole::getSingletonPtr()->setVisible(false);*/
            return Show();
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		// This might potentially override the camera loaded via DotScenePlus
		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));		

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CameraUpdateTask* newTask = new CameraUpdateTask(pCameraSceneNode, m_v3CameraPosition, m_qCameraOrientation, Update_Pos_Rot);
			TaskContainer* container = new TaskContainer((void*)newTask,Camera_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			pCameraSceneNode->setPosition(m_v3CameraPosition);
			pCameraSceneNode->setOrientation(m_qCameraOrientation);
		}
#else
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
#endif
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// _Update()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::_BaseUpdate()
	{
		bool updateResult = true;
		#ifdef GLE_EDITOR_MODELVIEW
			updateResult = Update();
		#else
			bool shouldUpdate = true;
			#ifdef GLE_EDITOR
				if (EnginePtr->GetEditor()->IsEditorMode())
				{	
					shouldUpdate = false;
				}
			#endif
			if (shouldUpdate)
			{
				updateResult = Update();
				m_pGameObjectManager->UpdateWorld();
			}
			#ifndef TBB_PARALLEL_OPTION
				OgreConsole::getSingletonPtr()->update(EnginePtr->GetDeltaTime());
			#endif
		#endif
		return updateResult;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::Update()
	{
		if (!IsBackgroundScreen() || (IsBackgroundScreen() && BackgroundDraw()))
		{
			// If you want to write draw code here, put it in such a block.
		}

		FreeCameraMovement();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// _Draw()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::_Draw()
	{
#ifdef GLE_EDITOR
		if (EnginePtr->GetEditor()->IsEditorMode())
		{
			return true;
		}
		else
#endif
		{
			return Draw();
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::Draw()
	{
		if (!IsBackgroundScreen() || (IsBackgroundScreen() && BackgroundUpdate()))
		{
			// If you want to write update code here, put it in such a block.
		}

		GetDefaultSceneManager()->getEntity("OgreHead")->getParentSceneNode()->yaw(Ogre::Degree(100 * EnginePtr->GetDeltaTime()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CameraUpdateTask* newTask = new CameraUpdateTask(pCameraSceneNode, m_v3CameraPosition, m_qCameraOrientation, Update_Pos_Rot);
			TaskContainer* container = new TaskContainer((void*)newTask,Camera_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			pCameraSceneNode->setPosition(m_v3CameraPosition);
			pCameraSceneNode->setOrientation(m_qCameraOrientation);
		}
#else
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
#endif

		//EnginePtr->GetRenderWindow()->update();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Close()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::Close()
	{
		EnginePtr->RemoveGameScreen(this);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	// FreeCameraMovement()
	//////////////////////////////////////////////////////////////////////////
	bool GameScreen::FreeCameraMovement()
	{
		if (GetActiveCamera()->getProjectionType() == Ogre::PT_ORTHOGRAPHIC)
		{
			return false;
		}

        Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();

        if(!OgreConsole::getSingletonPtr()->isVisible()){
            if (InputPtr->IsKeyDown(GIS::KC_W))
            {
                m_v3CameraPosition += pCameraSceneNode->getOrientation() * Ogre::Vector3(0, 0, -m_fCameraMovementSpeed * EnginePtr->GetDeltaTime());
            }

            if (InputPtr->IsKeyDown(GIS::KC_S))
            {
                m_v3CameraPosition += pCameraSceneNode->getOrientation() * Ogre::Vector3(0, 0, m_fCameraMovementSpeed * EnginePtr->GetDeltaTime());
            }

            if (InputPtr->IsKeyDown(GIS::KC_A))
            {
                m_v3CameraPosition += pCameraSceneNode->getOrientation() * Ogre::Vector3(-m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0, 0);
            }

            if (InputPtr->IsKeyDown(GIS::KC_D))
            {
                m_v3CameraPosition += pCameraSceneNode->getOrientation() * Ogre::Vector3(m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0, 0);
            }

            if (InputPtr->IsKeyDown(GIS::KC_Q))
            {
                m_v3CameraPosition += pCameraSceneNode->getOrientation() * Ogre::Vector3(0, m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0);
            }

            if (InputPtr->IsKeyDown(GIS::KC_E))
            {
                m_v3CameraPosition += pCameraSceneNode->getOrientation() * Ogre::Vector3(0, -m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0);
            }
        }

#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CameraUpdateTask* newTask = new CameraUpdateTask(pCameraSceneNode, m_v3CameraPosition, m_qCameraOrientation, Update_Pos_Rot);
			TaskContainer* container = new TaskContainer((void*)newTask,Camera_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			pCameraSceneNode->setPosition(m_v3CameraPosition);
			pCameraSceneNode->setOrientation(m_qCameraOrientation);
		}
#else
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
#endif

#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->ProfilerHotkey();
#endif

		return true;
	}

	/// Default MouseMove handler so that people can attach this to the end of their handling.
	bool GameScreen::DefaultOnMouseMove(const GIS::MouseEvent& mouseEvent)
	{
		if (GetActiveCamera()->getProjectionType() == Ogre::PT_ORTHOGRAPHIC)
		{
			return false;
		}
#ifdef TBB_PARALLEL_OPTION
		m_qCameraOrientation = Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.X.rel), Ogre::Vector3(0, 1, 0))
			* m_qCameraOrientation
			* Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.Y.rel), Ogre::Vector3(1, 0, 0));
#else
		m_qCameraOrientation = Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.X.rel), Ogre::Vector3(0, 1, 0))
			* GetActiveCameraSceneNode()->getOrientation()
			* Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.Y.rel), Ogre::Vector3(1, 0, 0));
#endif
		return true;
	}

	bool GameScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		case GIS::KC_TAB:
			{
				Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
				Ogre::SceneManager::CameraIterator cameraIterator = pSceneManager->getCameraIterator();
				while (cameraIterator.hasMoreElements())
				{
					Ogre::Camera* pCamera = cameraIterator.getNext();
					if (GetActiveCamera() == pCamera)
					{
						if (cameraIterator.hasMoreElements())
						{
							pCamera = cameraIterator.getNext();
						}
						else
						{
							pCamera = pSceneManager->getCameraIterator().getNext();
						}

						SetActiveCamera(pCamera);
						m_v3CameraPosition = GetActiveCameraSceneNode()->getPosition();
						m_qCameraOrientation = GetActiveCameraSceneNode()->getOrientation();
						break;
					}
				}
				break;
			}
		case GIS::KC_SYSRQ:
			{
				//SaveScene("Game\\Media\\Screens\\" + GetName() + ".scene");
				SaveScene(GetName() + ".scene");
			}
		}

		return true;
	}

	void GameScreen::SaveScene(Ogre::String fileName)
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		DotScenePlusSaver pDotScenePlusSaver;// = new DotScenePlusSaver();
		pDotScenePlusSaver.Save(fileName, this, pSceneManager, pSceneManager->getRootSceneNode());
		//pDotScenePlusSaver.Save(fileName, this, pSceneManager, pSceneManager->getSkyBoxNode());
		//pDotScenePlusSaver.Close();
	}

	void GameScreen::LoadScene(Ogre::String fileName)
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		DotScenePlusLoader* pDotScenePlusLoader = new DotScenePlusLoader();
		pDotScenePlusLoader->Load(fileName, this, "General", pSceneManager);
	}

	bool GameScreen::OnKeyRelease(const GIS::KeyEvent& keyEvent)
	{
		return true;
	}

	bool GameScreen::OnMouseMove(const GIS::MouseEvent& mouseEvent)
	{
		return DefaultOnMouseMove(mouseEvent);
		return true;
	}

	bool GameScreen::OnMousePress(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		return true;
	}

	bool GameScreen::OnMouseRelease(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		return true;
	}

	bool GameScreen::OnWiiRemoteButtonPressed( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button )
	{
		return true;
	}

	bool GameScreen::OnWiiRemoteButtonReleased( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button )
	{
		return true;
	}

	bool GameScreen::OnWiiAnalogStickMoved( const GIS::WiiRemoteEvent &wrEvent )
	{
		return true;
	} 

	bool GameScreen::OnWiiRemoteAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent)
	{
		return true;
	}

	bool GameScreen::OnWiiNunchuckAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent)
	{
		return true;
	}

	bool GameScreen::OnWiiIRMoved( const GIS::WiiRemoteEvent &wrEvent)
	{
		return true;
	}

	bool GameScreen::OnXpadButtonPressed( const GIS::XPadEvent &xpEvent, int button)
	{
		return true;
	}

	bool GameScreen::OnXpadButtonReleased( const GIS::XPadEvent &xpEvent, int button)
	{
		return true;
	}

	bool GameScreen::OnXpadLTriggerPressed( const GIS::XPadEvent &xpEvent )
	{
		return true;
	}

	bool GameScreen::OnXpadRTriggerPressed( const GIS::XPadEvent &xpEvent )
	{
		return true;
	}

	bool GameScreen::OnXpadLThumbStickMoved( const GIS::XPadEvent &xpEvent )
	{
		return true;
	}

	bool GameScreen::OnXpadRThumbStickMoved( const GIS::XPadEvent &xpEvent )
	{
		return true;
	}

	bool GameScreen::gestureRecognized( const std::string gestureName)
	{
		return true;
	}

	bool GameScreen::speechRecognized( const std::string wordRecognized,const float wordConfidence)
	{
		return true;
	}
}