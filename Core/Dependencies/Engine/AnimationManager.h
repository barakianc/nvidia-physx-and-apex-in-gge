
#ifndef ANIMATION_MANAGER_H
#define ANIMATION_MANAGER_H

#include <string>

#include "AnimatedEntity.h"
#include "AnimatedHavokEntity.h"
#include "AnimatedOgreEntity.h"
#include "GraphicsObject.h"

namespace AnimationManager
{
	class AnimatedHavokEntity;
	class AnimatedOgreEntity;
	/*!
		@remark Manager is a Singleton
	*/
	class Manager
	{
		// typedefs for the map that holds the AnimatedEntities
		typedef std::map<const std::string, AnimatedEntity*> TStrEntMap;
		typedef TStrEntMap::iterator TStrEntIt;
		typedef std::pair<const std::string, AnimatedEntity*> TStrEntPair;

	private:
		static Manager* m_pInstance;				// Instance of Manager
		TStrEntMap m_mAnimatedEntities;		// Map of AnimatedEntities inside this Manager
		
		/*!
			@remark Animation Manager Constructor: Initializes all the existing animated entities to null
		*/
		Manager() {}; // private constructor so that no one else can call it
		/*!
			@remark Animation Manager Destructor: Destroy the manager and free the space used
		*/
		~Manager();

	public:
		/*!
			@remark Users will use this to access Manager functionality
		*/
		static Manager* GetSingleton(); // this is how clients will use the AnimationManager
		/*!
			@remark Create a Havok animated entity for a game object with its graphics object and Ogre mesh
			@param[in] f_pGraphicsObject is the graphics object of a game object
			@param[in] file is the path string of Ogre mesh, skeleton file should be under the same path
			@param[in] name is the Ogre unique name to distinguish from all other objects in the scene
		*/
		AnimatedHavokEntity* CreateHavokAnimationObject(GraphicsObject* f_pGraphicsObject, const char* file, const char* name);
		/*!
			@remark Create a Ogre animated entity for a game object with its graphics object and Ogre mesh
			@param[in] f_pGraphicsObject is the graphics object of a game object
			@param[in] file is the path string of Ogre mesh, skeleton file should be under the same path
			@param[in] name is the animated entity name, which has to be unique, to distinguish from all other objects in the scene
		*/
		AnimatedOgreEntity* CreateOgreAnimationObject(GraphicsObject* f_pGraphicsObject, const char* file, const char* name);
		/*!
			@remark Delete an animated entity given its unique name
		*/
		void DeleteAnimationObject(const char*);
	};
}

#endif // ANIMATION_MANAGER_H