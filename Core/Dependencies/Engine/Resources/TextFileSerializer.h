#ifndef __TEXTSERIALIZER_H__
#define __TEXTSERIALIZER_H__

#include <OgreSerializer.h>

class TextFile; // forward declaration

class TextFileSerializer : public Ogre::Serializer
{
public:
	TextFileSerializer();
	virtual ~TextFileSerializer();

	void exportTextFile(const TextFile *pText, const Ogre::String &fileName);
	void importTextFile(Ogre::DataStreamPtr &stream, TextFile *pDest);
};

#endif