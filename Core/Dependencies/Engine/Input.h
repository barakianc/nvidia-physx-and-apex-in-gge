#pragma once

// Base
#include "Base.h"

// Input
#include "GIS.h"


// Easy access
#define InputPtr (GamePipe::Input::GetInstancePointer())
#define InputRef (GamePipe::Input::GetInstanceReference())


namespace GamePipe
{
	class Input : public Singleton<Input>, public GIS::KeyListener, public GIS::MouseListener, public GIS::WiiRemoteListener, public GIS::XPadListener, public GIS::KinectListener, public GIS::MoveMeListener
	{
	private:
		GIS::InputManager* m_pInputManager;
		GIS::Keyboard* m_pKeyboard;
		GIS::Mouse* m_pMouse;
		GIS::WiiRemote* m_pRemote;
		GIS::XPad* m_pXPad;
		GIS::Kinect* m_pKinect;

		GIS::MoveMe* m_pMoveMe;
		GIS::MouseState m_MouseState;
		GIS::WiiRemoteState m_WiiRemoteState;
		GIS::XPadState m_XPadState;

	protected:

	public:
		bool IsKeyDown(GIS::KeyCode keyCode);
		bool IsMouseButtonDown(OIS::MouseButtonID mouseButtonId);
		bool IsWRButtonDown(GIS::WiiButtonID wrButtonId);
		bool IsXPButtonDown(GIS::XPadButtonID xpButtonId);

		GIS::MouseState GetMouseState();
		GIS::WiiRemoteState GetWiiRemoteState();
		GIS::XPadState GetXPadState();

		GIS::MoveMe* GetMoveMe();

	public:
		int Initialize();
		int Destroy();

		void Capture();
		void Update();

		void SetMouseArea(unsigned int uiWidth, unsigned int uiHeight);
		void CenterMouse();
		void SetEventCallbacks();		

		bool keyPressed(const GIS::KeyEvent& keyEvent);
		bool keyReleased(const GIS::KeyEvent& keyEvent);
		bool mouseMoved(const GIS::MouseEvent& mouseEvent);
		bool mousePressed(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId);
		bool mouseReleased(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId);

		bool buttonPressed( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button );
		bool buttonReleased( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button );
		bool analogStickMoved( const GIS::WiiRemoteEvent &wrEvent ); 
		bool remoteAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent);
		bool nunchuckAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent);
		bool irMoved( const GIS::WiiRemoteEvent &wrEvent);

		bool buttonPressed(const GIS::MoveMeEvent &mmEvent, GIS::MoveMeButtonID button);
		bool buttonReleased(const GIS::MoveMeEvent &mmEvent, GIS::MoveMeButtonID button);

		bool buttonPressed( const GIS::XPadEvent &xpEvent, int button);
		bool buttonReleased( const GIS::XPadEvent &xpEvent, int button);
		bool lTriggerPressed( const GIS::XPadEvent &xpEvent );
		bool rTriggerPressed( const GIS::XPadEvent &xpEvent );
		bool lThumbStickMoved( const GIS::XPadEvent &xpEvent );
		bool rThumbStickMoved( const GIS::XPadEvent &xpEvent );

		//for kinect
		GIS::Kinect* getKinect();
		bool gestureRecognized( const std::string gestureName);
		bool speechRecognized( const std::string wordRecognized,const float wordConfidence);

#ifdef GLE_EDITOR
		bool			m_bKeyAlt;
		bool			m_bMouseLeft;
		bool			m_bMouseMiddle;
		bool			m_bMouseMoved;		// Control the clicks, not press/release
		Ogre::Real		m_fYawValue;
		Ogre::Real		m_fPitchValue;
		Ogre::Real		m_fRadius;
		
		bool EditorMouseMoved();
		bool EditorKeyRead();
		bool EditorMouseButtonRead(int mouseButtonID, bool mouseButtonState);
		GIS::KeyCode EditorKeyCodeTranslation(int key);
		GIS::MouseButtonID EditorMouseButtonTranslation(int buttonID);
#endif

#ifdef DLLRELEASE

		GIS::KeyCode Input::DLLKeyCodeTranslation(int key);

	public: 
		GIS::Keyboard* GetKeyboard()
		{
			return m_pKeyboard;
		}

		GIS::Mouse* GetMouse()
		{
			return m_pMouse;
		}
#endif

	public:
		friend GamePipe::Singleton<Input>;

	private:
		Input();
		~Input();
	};
}

