
#ifndef ANIMATED_HAVOK_ENTITY_H
#define ANIMATED_HAVOK_ENTITY_H

#pragma once

#include "Engine.h"
#include "AnimatedEntity.h"

/// Include files for STL
#include <string>
#include <map>

/// Include files for Math and Base
#include <Common/Base/hkBase.h>
#include <Common/Base/Container/String/hkString.h>
#include <Common/Base/Container/String/hkStringBuf.h>

/// Include files for Skeletal Animation
#include <Animation/Animation/Playback/hkaAnimatedSkeleton.h>
#include <Animation/Animation/hkaAnimationContainer.h>
#include <Animation/Animation/Playback/Control/Default/hkaDefaultAnimationControl.h>

// FALL2010 Include files for loading hkx files
#include <Common/Serialize/Util/hkRootLevelContainer.h>
#include <Common/Serialize/Resource/hkResource.h>
#include <Common/Serialize/Util/hkLoader.h>
#include <Common/Serialize/Util/hkSerializeUtil.h>

// FALL2010 Include files for hkx file serialization
#include <Common/Serialize/Packfile/hkPackfileData.h>
#include <Common/Compat/Deprecated/Packfile/hkPackfileReader.h>
#include <Physics/Utilities/Serialize/hkpHavokSnapshot.h>

#include <Animation/Animation/Rig/hkaPose.h>

#include <Animation/Ragdoll/Instance/hkaRagdollInstance.h>

#include "GraphicsObject.h"
#include "RagDollEntity.h"

class GraphicsObject;
class RagdollObject;
namespace AnimationManager
{
	class AnimatedHavokEntity : public AnimatedEntity
	{
		typedef std::map<const std::string, float> TStrRampMap;
		typedef TStrRampMap::iterator TStrRampIt;
		typedef std::pair<const std::string, float> TStrRampPair;

		typedef std::map<const std::string, hkaAnimation*> TStrAnimMap;
		typedef TStrAnimMap::iterator TStrAnimIt;
		typedef std::pair<const std::string, hkaAnimation*> TStrAnimPair;

		typedef std::map<const std::string, hkaAnimationBinding*> TStrBindMap;
		typedef TStrBindMap::iterator TStrBindIt;
		typedef std::pair<const std::string, hkaAnimationBinding*> TStrBindPair;

		typedef std::map<const std::string, hkaDefaultAnimationControl*> TStrCtrlMap;
		typedef TStrCtrlMap::iterator TStrCtrlIt;
		typedef std::pair<const std::string, hkaDefaultAnimationControl*> TStrCtrlPair;

	private:
		GraphicsObject* m_pGraphicsObject;						// hook into the renderer
		
		TStrAnimMap m_hkAnimations;								// hashmap with char* anim names for keys
																// and hkaAnimation* for values
		
		TStrBindMap m_hkBindings;								// hashmap with char* anim names for keys
																// and hkaAnimationBinding* for values
		
		TStrCtrlMap m_hkControls;								// hashmap with char* anim names for keys
																// and hkaDefaultAnimationControl* for values

		hkaAnimatedSkeleton* m_hkSkeletonInstance;				// for skeleton instances

		hkLoader* m_hkLoader;									// for load object and animation

		hkaSkeleton* m_hkSkeleton;								// for loaded havok skeleton

		Ogre::Skeleton* m_ogSkeleton;							// for mapped ogre skeleton

		hkQsTransform* m_worldFromModel;						// matrix transform from model space to world space

		float m_fScaleFactor;

		RagdollObject* ragDollEntity;							// This object contorls the ragdolls

		bool transition;										// When a transition animation is passed
		char* transitionTo;										// Destination animation
		bool transitionLoop;									// loopnig
		bool overlay;

		float defaultBlendDuration;

		bool ragDollAttachedToken;								// bool indicating if there is a ragdoll
																// attached to the AnimatedEntity

		bool ragDollPoseToken;									// bool indicating whether the ragdoll is toggled on

	public:
		/// Initializes all the pointers to null if no params are passed in
		AnimatedHavokEntity();
		virtual ~AnimatedHavokEntity();

		/*!
			@remark Initializes animated havok entity using given havok rig
			@param[in] f_pGraphicsObject
			@param[in] f_sRigPath is the file path for havok rig
			@param[in] f_sName is the unique name of this entity
		*/
		AnimatedHavokEntity(GraphicsObject* f_pGraphicsObject, const char* f_sRigPath, const char* f_sName);

		virtual void Update(float f_fTimeStep);
		
		virtual void SetPosition(float x, float y, float z);

		virtual void PlayAnimation(	const char* f_sDestinationAnimationName,		//
									const char* f_sTransitionAnimationName = "",	//
									const bool	f_bLoop = true,						//
									const float f_fBlendDuration = 0.0f);				//

		virtual void PlayAnimationOverlay(	const char* f_sOverlayAnimationName,	//
									const char* f_sTransitionAnimationName = "",	//
									const bool	f_bLoop = true,						//
									const float f_fBlendDuration = 0.0f);				//

		virtual void StopAnimation(const char* f_sAnimationName = "");
		virtual void StopAllAnimation();

		virtual bool IsAnimationPlaying(const char* f_sAnimationName);

		virtual const char* GetName();
		virtual std::vector<std::string> GetAnimationNames();
		virtual const char* GetActiveAnimation();
		virtual void SetDefaultAnimation(const char* f_sDefaultAnim);
		virtual const char* GetDefaultAnimation();

		/// Toggle between ragdoll pose and animation pose
		/// This is only for AnimatedHavokEntity. User has to cast AnimatedEntity to AnimatedHavokEntity before using this function.
		void ToggleToRagDollPose(bool token);
		void AddRagDoll(const std::string f_sRagdollPath);
		void SetRagdollPosition(hkVector4& f_v4hkPos);
		//Method is exclusively used by SkeletonUpdateRootTask.h classes to get skeleton pointer from AnimatedHavokEntity
		Ogre::Skeleton* GetSkeleton() { return m_ogSkeleton; }

	};
}

#endif // ANIMATED_HAVOK_ENTITY_H
