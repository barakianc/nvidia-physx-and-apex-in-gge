
#ifndef ANIMATED_OGRE_ENTITY_H
#define ANIMATED_OGRE_ENTITY_H

#include "OgreEntity.h"
#include "AnimatedEntity.h"
#include "GraphicsObject.h"

namespace AnimationManager
{
	class AnimatedOgreEntity : public AnimatedEntity
	{
		typedef std::map<const std::string, float> TStrRampMap;
		typedef TStrRampMap::iterator TStrRampIt;
		typedef std::pair<const std::string, float> TStrRampPair;

	private:
	protected:
		GraphicsObject* m_pGraphicsObject;
		
		// currently playing animation
		Ogre::AnimationState* mCurrentState;
		// first argument of the playAnimation and playAnimationOverlay
		Ogre::AnimationState* mDestinationState;
		// second argument of the playAnimation and playAnimationOverlay
		Ogre::AnimationState* mTransitionState;
		// final animation to be played in loop after trasition/blending is over
		Ogre::AnimationState* mEndState;
		
		int mTransitionType; 
		//	mTransition type works as follows (activeAnim mean currently playing animation before any playAnimation call i.e. mCurrentState)
		//	0 for play destAnim in loop
		//	1 for play destAnim once then activeAnim in loop
		//	2 for blend from activeAnim to destAnim then destAnim in loop
		//	3 for blend from activeAnim to destAnim, play destAnim once then activeAnim in loop
		//	4 for play transAnim directly, then play destAnim in loop
		//	5 for play transAnim directly, then play destAnim once then activeAnim in loop
		//	6 for blend from activeAnim to transAnim then play destAnim in loop
		//	7 for blend from activeAnim to transAnim then play destAnim once then activeAnim in loop
		//	8 for overlay destAnim and activeAnim in loop
		//	9 for overlay destAnim and activeAnim for once then Play activeAnim in loop
		//	10 for overlay transAnim and activeAnim once then Overlay destAnim and activeAnim in loop
		//	11 for overlay transAnim and activeAnim once then Overlay destAnim and activeAnim once then Play activeAnim in loop

		float mTimeleft;
		float mDuration;
		bool mComplete;
		bool mLoop;

	public:
		AnimatedOgreEntity();
		AnimatedOgreEntity(GraphicsObject* f_pGraphicsObject, const char* f_sPath, const char* f_sName);
        virtual ~AnimatedOgreEntity(){};

		virtual void Update(float f_fTimeStep);

		// no use, just for generalization
		virtual void SetPosition(float x, float y, float z);

		virtual void PlayAnimation(	const char* f_sDestinationAnimationName,		//
									const char* f_sTransitionAnimationName = "",	//
									const bool	f_bLoop = true,						//
									const float f_fBlendDuration = 0.5f);			//

		virtual void PlayAnimationOverlay(	const char* f_sOverlayAnimationName,	//
									const char* f_sTransitionAnimationName = "",	//
									const bool	f_bLoop = true,						//
									const float f_fBlendDuration = 0.5f);			//

		virtual void StopAnimation(const char* f_sAnimationName = "");
		virtual void StopAllAnimation() { }
		virtual bool IsAnimationPlaying(const char* f_sAnimationName);

		virtual const char* GetName();
		virtual std::vector<std::string> GetAnimationNames();
		virtual const char* GetActiveAnimation();
		virtual void SetDefaultAnimation(const char* f_sDefaultAnim);
		virtual const char* GetDefaultAnimation();
	};
}

#endif // ANIMATED_OGRE_ENTITY_H