#pragma once

#include "Configuration.h"

#include <Instrumentation.h>

//#define _MASTER 1	//Forces MASTER MODE and disables the profiler
#if _MASTER
#	define GGE_PROFILER
#	define GGE_PROFILER_SET_ENABLED
#	define GGE_PROFILER_SET_TIMER
#	define GGE_PROFILER_BEGIN_PROFILE( a )
#	define GGE_PROFILER_END_PROFILE( a )
#	define GGE_PROFILER_RESET
#	define GGE_RESUME_PROFILING
#	define GGE_PAUSE_PROFILING
#else
#	if GGE_PROFILER1
#		define GGE_PROFILER GGE_Profiler PROFILER
#		define GGE_PROFILER_SET_ENABLED PROFILER.setEnabled(true)
#		define GGE_PROFILER_SET_TIMER	PROFILER.setTimer(new Ogre::Timer())
#		define GGE_PROFILER_BEGIN_PROFILE( a ) PROFILER.beginProfile( (a) )
#		define GGE_PROFILER_END_PROFILE( a ) PROFILER.endProfile( (a) )
#		define GGE_PROFILER_RESET PROFILER.reset()
#		define GGE_RESUME_PROFILING
#		define GGE_PAUSE_PROFILING
#	else
#		if GGE_PROFILER2
#			define GGE_PROFILER GGE_Profiler2 PROFILER
#			define GGE_RESUME_PROFILING PROFILER.ResumeProfiling()
#			define GGE_PAUSE_PROFILING PROFILER.PauseProfiling()
#			define GGE_PROFILER_SET_ENABLED
#			define GGE_PROFILER_SET_TIMER
#			define GGE_PROFILER_RESET
#			define GGE_PROFILER_BEGIN_PROFILE( a )
#			define GGE_PROFILER_END_PROFILE( a )
#		else
#			if GGE_PROFILER3
#				define GGE_PROFILER GGE_PROFILER_VTUNE PROFILER
#				define GGE_RESUME_PROFILING PROFILER.ResumeProfiling()
#				define GGE_PAUSE_PROFILING PROFILER.PauseProfiling()
#				define GGE_PROFILER_SET_ENABLED
#				define GGE_PROFILER_SET_TIMER
#				define GGE_PROFILER_RESET
#				define GGE_PROFILER_BEGIN_PROFILE( a )
#				define GGE_PROFILER_END_PROFILE( a )
#			else
#				define GGE_PROFILER
#				define GGE_PROFILER_SET_ENABLED
#				define GGE_PROFILER_SET_TIMER
#				define GGE_PROFILER_BEGIN_PROFILE( a )
#				define GGE_PROFILER_END_PROFILE( a )
#				define GGE_PROFILER_RESET
#				define GGE_RESUME_PROFILING
#				define GGE_PAUSE_PROFILING
#			endif
#		endif
#	endif
#endif

#include "Base.h"
#include "OgrePrerequisites.h"
#include "OgreSingleton.h"
#include "OgreString.h"
#include "OgreOverlay.h"
#include "OgreTimer.h"
#include "Input.h"
#include "OgreLineStreamOverlayElement.h"
#include <iostream>

using namespace std;

// Easy access
#define GGE_Profiler2Ptr (GamePipe::GGE_Profiler2::GetInstancePointer())
#define GGE_Profiler2Ref (GamePipe::GGE_Profiler2::GetInstanceReference())


namespace GamePipe
{
	//Modified Ogre Profiler Class
    class GGE_Profiler
	{

        public:
            GGE_Profiler();
            ~GGE_Profiler();

            /** Sets the timer for the profiler */
			void setTimer(Ogre::Timer* t);

            /** Retrieves the timer for the profiler */
			Ogre::Timer* getTimer();

			void beginProfile(const Ogre::String& profileName);
			void endProfile(const Ogre::String& profileName);
            bool getEnabled() const;
            void enableProfile(const Ogre::String& profileName);
			void setEnabled(bool enabled);
            void disableProfile(const Ogre::String& profileName);
            bool watchForMax(const Ogre::String& profileName);
            bool watchForMin(const Ogre::String& profileName);
            bool watchForLimit(const Ogre::String& profileName, Ogre::Real limit, bool greaterThan = true);
            void logResults();
            void reset();
            void setUpdateDisplayFrequency(unsigned int freq);
			unsigned int getUpdateDisplayFrequency() const;
            //static GGE_Profiler& getSingleton(void);
            //static GGE_Profiler* getSingletonPtr(void);

        protected:

            /** Initializes the profiler's GUI elements */
            void initialize();

            /** Prints the profiling results of each frame */
            void displayResults();

            /** Processes the profiler data after each frame */
            void processFrameStats();

            /** Handles a change of the profiler's enabled state*/
            void changeEnableState();

            /** An internal function to create the container which will hold our display elements*/
			Ogre::OverlayContainer* createContainer();

            /** An internal function to create a text area */
            Ogre::OverlayElement* createTextArea(const Ogre::String& name, Ogre::Real width, Ogre::Real height, Ogre::Real top, Ogre::Real left, 
                                       unsigned int fontSize, const Ogre::String& caption, bool show = true);

            /** An internal function to create a panel */
            Ogre::OverlayElement* createPanel(const Ogre::String& name, Ogre::Real width, Ogre::Real height, Ogre::Real top, Ogre::Real left, 
                                    const Ogre::String& materialName, bool show = true);

            /// Represents an individual profile call
            struct ProfileInstance {

                /// The name of the profile
                Ogre::String		name;

                /// The name of the parent, empty string if root
                Ogre::String		parent;

                /// The time this profile was started
                Ogre::ulong		currTime;

                /// Represents the total time of all child profiles to subtract
                /// from this profile
                Ogre::ulong		accum;

                /// The hierarchical level of this profile, 0 being the root profile
                unsigned int		hierarchicalLvl;
            };

            /// Represents the total timing information of a profile
            /// since profiles can be called more than once each frame
            struct ProfileFrame {
				
                /// The name of the profile
                Ogre::String	name;

                /// The total time this profile has taken this frame
                Ogre::ulong	frameTime;

                /// The number of times this profile was called this frame
                unsigned int	calls;

                /// The hierarchical level of this profile, 0 being the main loop
                unsigned int	hierarchicalLvl;

            };
			
            /// Represents a history of each profile during the duration of the app
            struct ProfileHistory {

                /// The name of the profile
                Ogre::String	name;

                /// The current percentage of frame time this profile has taken
                Ogre::Real	currentTime; // %

                /// The maximum percentage of frame time this profile has taken
                Ogre::Real	maxTime; // %

                /// The minimum percentage of frame time this profile has taken
                Ogre::Real	minTime; // %

                /// The number of times this profile has been called each frame
                unsigned int	numCallsThisFrame;

                /// The total percentage of frame time this profile has taken
                /// (used to calculate average)
                Ogre::Real	totalTime; // %

                /// The total number of times this profile was called
                /// (used to calculate average)
                Ogre::ulong	totalCalls; // %

                /// The hierarchical level of this profile, 0 being the root profile
                unsigned int	hierarchicalLvl;

			};

			
            typedef std::list<ProfileInstance> ProfileStack;
            typedef std::list<ProfileFrame> ProfileFrameList;
            typedef std::list<ProfileHistory> ProfileHistoryList;
            typedef std::map<Ogre::String, ProfileHistoryList::iterator> ProfileHistoryMap;
            typedef std::map<Ogre::String, bool> DisabledProfileMap;

            typedef std::list<Ogre::OverlayElement*> ProfileBarList;

            /// A stack for each individual profile per frame
            ProfileStack mProfiles;

            /// Accumulates the results of each profile per frame (since a profile can be called
            /// more than once a frame)
            ProfileFrameList mProfileFrame;

            /// Keeps track of the statistics of each profile
            ProfileHistoryList mProfileHistory;

            /// We use this for quick look-ups of profiles in the history list
            ProfileHistoryMap mProfileHistoryMap;

            /// Holds the names of disabled profiles
            DisabledProfileMap mDisabledProfiles;

            /// Holds the display bars for each profile results
            ProfileBarList mProfileBars;

            /// Whether the GUI elements have been initialized
            bool mInitialized;

            /// The max number of profiles we can display
            unsigned int maxProfiles;

            /// The overlay which contains our profiler results display
            Ogre::Overlay* mOverlay;

            /// The window that displays the profiler results
            Ogre::OverlayContainer* mProfileGui;

            /// The height of each bar
            Ogre::Real mBarHeight;

            /// The height of the stats window
            Ogre::Real mGuiHeight;

            /// The width of the stats window
            Ogre::Real mGuiWidth;

            /// The size of the indent for each profile display bar
            Ogre::Real mBarIndent;

            /// The width of the border between the profile window and each bar
            Ogre::Real mGuiBorderWidth;

            /// The width of the min, avg, and max lines in a profile display
            Ogre::Real mBarLineWidth;

            /// The number of frames that must elapse before the current
            /// frame display is updated
            unsigned int mUpdateDisplayFrequency;

            /// The number of elapsed frame, used with mUpdateDisplayFrequency
            unsigned int mCurrentFrame;

            /// The timer used for profiling
            Ogre::Timer* mTimer;

            /// The total time each frame takes
            Ogre::ulong mTotalFrameTime;

            /// Whether this profiler is enabled
            bool mEnabled;

            /// Keeps track of whether this profiler has
            /// received a request to be enabled/disabled
            bool mEnableStateChangePending;

            /// Keeps track of the new enabled/disabled state that the user has requested
            /// which will be applied after the frame ends
            bool mNewEnableState;

    }; // end class GGE_Profiler

	///Profiler 2 Class
	class GGE_Profiler2 : public Singleton<GGE_Profiler2>
	{
	public:
		///Profiler 2 constructor
		GGE_Profiler2()
		{
		}

		void ResumeProfiling();
		void PauseProfiling();

#ifdef TBB_PARALLEL_OPTION
		///timer used if threading is enabled as threading conflicts with Ogre default timer
		GGETimer* m_pTimer2;
#else
		///timer used for measuring data
		Ogre::Timer* m_pTimer2;
#endif
		///provides information for reporting CPU Core usage
		Instrumentation* cpumeasurer;

		///measures the start of the Engine::Draw loop
		float m_fDrawStartTime;
		///measures the end of the Engine::Draw loop
		float m_fDrawEndTime;
		///measures the total time for drawing the scene
		float m_fDrawTotalTime;
		///measures the start of the Engine::UpdateInput loop
		float m_fInputStartTime;
		///measures the end of the Engine::UpdateInput loop
		float m_fInputEndTime;
		///measures the total time taken to update input
		float m_fInputTotalTime;
		///measures the start of Engine::UpdateGraphics loop
		float m_fGameLogicStartTime;
		///measures the end of Engine::UpdateGraphics loop
		float m_fGameLogicEndTime;
		///measures the total time taken to update the game logic
		float m_fGameLogicTotalTime;
		///measures the start of the GameObjectManager::UpdateWorld loop
		float m_fPhysicsStartTime;
		///measures the end of the GameObjectManager::UpdateWorld loop
		float m_fPhysicsEndTime;
		///measures the total time taken to update the physics objects
		float m_fPhysicsTotalTime;
		///measures the start of the AnimatedHavokEntity::Update loop
		float m_fAnimationStartTime;
		///measures the end of the AnimatedHavokEntity::Update loop
		float m_fAnimationEndTime;
		///measures the total time taken to update the animation of the Havok objects
		float m_fAnimationTotalTime;
		///measures the start of the Engine::Update loop
		float m_fUpdateStartTime;
		///measures the end of the Engine::Update loop
		float m_fUpdateEndTime;
		///measures the total time taken of going through the update loop
		float m_fUpdateTotalTime;
		///measures the start of the Engine::Run loop
		float m_fRunStartTime;
		///measures the end of the Engine::Run loop
		float m_fRunEndTime;
		///measures the total time taken of the Engine::Run loop
		float m_fRunTotalTime;

		///adds an extra check to slow down the process of hitting F1 to enable/disable the profiler
		bool debugKeyReleased;
		///checks to see whether the mouse is centered on the screen when hitting F2
		bool mouseCentered;
		///calculates the offset value of X when moving a menu around
		float offsetX;
		///calculates the offset value of Y when moving a menu around
		float offsetY;
		///draws the blue rectangle for the CPU data information
		Ogre::OverlayContainer* container;
		///draws the blue rectangle for the FPS data information
		Ogre::OverlayContainer* container2;	

		///dynamically allocated array that contains CPU usage information of each core
		double* CPUPercent;
		///stores the amount of cores currently in the system
		unsigned int cpucount;

		///container used for drawing the FPS and CPU line graphs
		Ogre::Overlay* debugOverlay;

		///flag to see if the profiler is enabled, used in the UpdateMenu loop
		bool profilerenabled;
		///debug container used for drawing the CPU graph panel
		Ogre::OverlayContainer *debugLineStreamPanel;
		///debug container used for drawing the FPS graph panel
		Ogre::OverlayContainer *debugLineStreamPanel1;
		///debug container used for drawing the CPU line stream
		Ogre::OverlayContainer *debugLineStream;
		///debug container used for drawing the FPS line stream
		Ogre::OverlayContainer *debugLineStream1;

		///accessor function for setting the Animation start time
		inline void SetAnimationStartTime (float starttime) {m_fAnimationStartTime = starttime;}
		///accessor function for setting the Animation end time
		inline void SetAnimationEndTime (float endtime) {m_fAnimationEndTime = endtime;}
		///accessor function for setting the Physics start time
		inline void SetPhysicsStartTime (float starttime) {m_fPhysicsStartTime = starttime;}
		///accessor function for setting the Physics end time
		inline void SetPhysicsEndTime(float endtime) {m_fPhysicsEndTime = endtime;}

		///Initializes display information for the Profiler UI
		void InitializeMenu();
		///Updates the Profiler UI data
		void UpdateMenu();
		///Loads the blue rectangles for the FPS and CPU data panels
		void LoadGraphics();

		///variable used for drawing the actual FPS line
		Ogre::LineStreamOverlayElement* mFPSLineStream;
		///variable used for drawing the actual CPU line
		Ogre::LineStreamOverlayElement* mCPULineStream;
		///singleton for initializing the line stream element
		Ogre::OverlayElementFactory* mLineStreamFactory;

		///Can be used in the future for adding labels to the FPS and CPU line graphs
		void addOverlayLabel(Ogre::String labelName,Ogre::Real left, Ogre::Real top, Ogre::OverlayContainer* debugLineStreamPanel, Ogre::ColourValue color );
		///Creates the CPU line graph
		void createCPULineStreamOverlay(Ogre::MaterialPtr backMat, Ogre::Overlay* debugOverlay );
		///Creates the FPS line graph
		void createFPSLineStreamOverlay(Ogre::MaterialPtr backMat, Ogre::Overlay* debugOverlay );
		///Initializes the FPS and CPU line graph information
		void createOverlays();
		///Contains functionality for enabling/disabling the profiler as well as moving it around
		void ProfilerHotkey();
		///Resets the Profiler UI back to its default position
		void ResetOverlayPosition();

	private:
		///used for storing the position when dynamically creating the CPU menu
		float pos;

		///used to show/hide the CPU data panel
		Ogre::Overlay* cpudata;
		///used to show/hide the FPS data panel
		Ogre::Overlay* fpsdata;

	};//end class GGE_Profiler2

	//VTUNE PROFILER CLASS
	class GGE_PROFILER_VTUNE
	{
	public:
		GGE_PROFILER_VTUNE()
		{
			startProfile=0;
			ignoreF1=0;
			F2mode=0;
		}
		void ResumeProfiling();
		void PauseProfiling();

	private:
		int startProfile;
		int ignoreF1;								//This stops us from holding down the F1 key and profiling multiple frames
		int F2mode;
	};

} // end namespace
