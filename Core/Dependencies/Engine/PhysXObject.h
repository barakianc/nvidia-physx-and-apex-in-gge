#ifndef _PHYSX_OBJECT_H_
#define _PHYSX_OBJECT_H_

#ifdef PHYSX
#include "GameObjectManager.h"

// PhysX
#include "PxPhysicsAPI.h"
#include "PxDefaultErrorCallback.h"
#include "PxDefaultAllocator.h"
#include "PxExtensionsAPI.h"
#include "PxCudaContextManager.h"
#include "PxCapsuleController.h"
#include "PxControllerManager.h"

#ifdef PHYSX_APEX
#include "destructible/public/NxDestructibleAsset.h"
#include "destructible/public/NxDestructibleActor.h"
#include "destructible/public/NxModuleDestructible.h"
#include "NxApex.h"
#endif

class PhysicsObject 
{
public:
	virtual void				setPosition(physx::PxVec3& f_rPosition)=0;
	virtual physx::PxVec3				getPosition()=0;
	virtual physx::PxPhysics*			getPhysicsWorld()=0;
	virtual physx::PxRigidStatic*		getStaticRigidBody()=0;
	virtual physx::PxRigidDynamic*		getDynamicRigidBody()=0;
	virtual physx::PxController*		getCharacterController()=0;
	virtual bool				scale()=0;
	virtual bool				update()=0;
	virtual void				destroy()=0;
};

class CharacterObject : public PhysicsObject 
{
private:
	~CharacterObject();	
	void initializePhysicsPrimitive();

	physx::PxPhysics*				m_PhysXSDK;
	physx::PxScene*				m_PhysXScene;
	physx::PxCapsuleControllerDesc m_Desc;
	physx::PxController*			m_Controller;
	physx::PxControllerManager*	m_PxControllerManager;
	GameObjectType			m_eGameObjectType;
	GraphicsObject*			m_pGraphicsObject;
	CollisionShapeType		m_eCollisionShapeType;
	Ogre::Quaternion		m_vBaseOrientation;
	physx::PxVec3					m_PxSpeed;



public:
	CharacterObject(GraphicsObject* f_pGraphicsObject);

	physx::PxPhysics* getPhysicsWorld()
	{
		return m_PhysXSDK;
	}
	
	void setPosition(physx::PxVec3& f_rPosition) 
	{
		m_Controller->setPosition(physx::PxExtendedVec3(f_rPosition.x,f_rPosition.y,f_rPosition.z));
	}
	physx::PxVec3 getPosition() {return physx::PxVec3(0,0,0);}
	bool update();
	void destroy() {}
	physx::PxRigidStatic* getStaticRigidBody() {return NULL;}
	physx::PxRigidDynamic*	getDynamicRigidBody() {return NULL;}

	physx::PxController* getCharacterController() {
		return m_Controller;
	}

	bool scale(){return true;}
};


class PhysicsPrimitiveObject : public PhysicsObject
{
private :
	~PhysicsPrimitiveObject();	
	void initializePhysicsPrimitive();

	physx::PxPhysics*			m_PhysXSDK;
	physx::PxScene*				m_PhysXScene;
	physx::PxCooking*			m_PhysXCooking;
	physx::PxRigidDynamic*		m_DynamicActor;
	physx::PxRigidStatic*		m_StaticActor;
	GraphicsObject*				m_pGraphicsObject;
	GameObjectType				m_eGameObjectType;
	CollisionShapeType			m_eCollisionShapeType;
	Ogre::Quaternion			m_vBaseOrientation;

	Ogre::Vector3 m_vctCentroid;

#ifdef PHYSX_APEX
	physx::apex::NxApexSDK*  m_ApexSDK;
	physx::apex::NxApexScene* m_ApexScene;
	physx::apex::NxModuleDestructible*		m_DestructibleModule;
#endif

public:
	PhysicsPrimitiveObject(
								std::string				f_sOgreUniqueName,
								std::string				f_sMeshFileName,
								std::string				f_sPhysXFileName,
								GameObjectType			f_ePhysXObjectType,
								CollisionShapeType		f_eCollisionShapeType,
								GraphicsObject**		f_pGraphicsObject
		  				); 

	void addBox(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType);
	
	void addCapsule(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType);

	void addSphere(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType);

	void addCylinder(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType);

	GameObjectType getGameObjectType() {
		return m_eGameObjectType;
	}

	physx::PxPhysics* getPhysicsWorld() {
		return m_PhysXSDK;
	}
	
	physx::PxRigidStatic* getStaticRigidBody() {
		return m_StaticActor;
	}

	physx::PxRigidDynamic* getDynamicRigidBody() {
		return m_DynamicActor;
	}

	physx::PxController* getCharacterController() {
		return NULL;
	}

	physx::PxVec3 getPosition();
	
	bool isActive()
	{
		return true;
	}

	void setPosition(physx::PxVec3& f_rPosition) {
		if (m_eGameObjectType == PHYSICS_DYNAMIC) {

			f_rPosition = physx::PxVec3(f_rPosition.x - m_vctCentroid.x
				, f_rPosition.y - m_vctCentroid.y
				, f_rPosition.z - m_vctCentroid.z
				);

			if(m_DynamicActor != NULL)
				m_DynamicActor->setGlobalPose(physx::PxTransform(f_rPosition, physx::PxQuat::createIdentity()));
		}
		else if (m_eGameObjectType == PHYSICS_FIXED) {
			if (m_StaticActor != NULL)

				f_rPosition = physx::PxVec3(f_rPosition.x - m_vctCentroid.x
				, f_rPosition.y - m_vctCentroid.y
				, f_rPosition.z - m_vctCentroid.z
				);

				m_StaticActor->setGlobalPose(physx::PxTransform(f_rPosition, physx::PxQuat::createIdentity()));
		}
	}

	bool update();

	void destroy() {}

	bool scale();

	bool scaleBox();

	bool scaleSphere();

	bool scaleCapsule();

	bool scaleCylinder();
};
#endif

#endif