//Timer.h

#include <windows.h>

class GGETimer{
private:
	LARGE_INTEGER frequency;
    LARGE_INTEGER start;

public:
	GGETimer()
	{ 
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&start); 
	}

	void reset () 
	{ 
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&start); 
	}

	double getMicroseconds()
	{ 
		LARGE_INTEGER end;
		QueryPerformanceCounter(&end);
		return (end.QuadPart - start.QuadPart) * 1000000.0 / frequency.QuadPart;
	}
};