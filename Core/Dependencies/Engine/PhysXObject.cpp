#include "StdAfx.h"

#ifdef PHYSX

#include "PhysXObject.h"
#include "GameObject.h"
#include "GameObjectManager.h"
#include <Math.h>



#undef HashMap

#include <RepX.h>
#include <RepXUtility.h>
#include <RepXCoreExtensions.h>

#include "OgrePrerequisites.h"

#ifdef PHYSX_APEX
#include "destructible/public/NxDestructibleAsset.h"
#include "destructible/public/NxDestructibleActor.h"
#include "destructible/public/NxModuleDestructible.h"
#include "NxApex.h";
#endif

#ifdef TBB_PARALLEL_OPTION
#include "TaskTypes.h"
#include "TaskContainer.h"
#include "GraphicsObjectUpdateTask.h"
#endif

PhysicsPrimitiveObject::PhysicsPrimitiveObject(
								std::string				f_sOgreUniqueName,
								std::string				f_sMeshFileName,
								std::string				f_sPhysXFileName,
								GameObjectType			f_ePhysXObjectType,
								CollisionShapeType		f_eCollisionShapeType,
								GraphicsObject**		f_pGraphicsObject) : PhysicsObject()
{
	m_PhysXSDK	= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	m_PhysXScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();
	m_PhysXCooking = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXCooking();

#ifdef PHYSX_APEX
	m_ApexSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetApexSDK();
	m_ApexScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetApexScene();
	m_DestructibleModule = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetDestructibleModule();
#endif

	m_vctCentroid = Ogre::Vector3(0, 0, 0);

	m_StaticActor = NULL;
	m_DynamicActor = NULL;

	int strSearchValue = f_sPhysXFileName.find(".xml");
	if (strSearchValue == string::npos)
	strSearchValue = f_sPhysXFileName.find(".XML");


	if (strSearchValue != string::npos){
		
		m_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName, f_sMeshFileName, f_ePhysXObjectType));
		
		m_eGameObjectType = f_ePhysXObjectType;

		physx::repx::RepXExtension* extensions[64];
		int numExtensions = 0;
		physx::PxStringTable* mStringTable = NULL;
		physx::PxDefaultAllocator allocatorCallback;
		physx::PxAllocatorCallback& call = EnginePtr->GetPhysXFoundation()->getAllocator();
		//physx::PxFoundation::getAllocator();

		physx::repx::RepXIdToRepXObjectMap* idToObjMap = physx::repx::RepXIdToRepXObjectMap::create(allocatorCallback);
		
		numExtensions = physx::repx::buildExtensionList(extensions, 64, allocatorCallback);
		physx::repx::RepXCollection* collection = NULL;

		collection = physx::repx::RepXCollection::create(physx::PxDefaultFileInputData::PxDefaultFileInputData(f_sPhysXFileName.c_str()), extensions, numExtensions, allocatorCallback);
		//physx::repx::addObjectsToScene(collection, m_PhysXSDK, m_PhysXCooking, m_PhysXScene, mStringTable );
		collection->destroy();
		
	}

	else {
		m_pGraphicsObject = (GraphicsObject *)(new GraphicsObject(f_sOgreUniqueName, f_sMeshFileName, f_ePhysXObjectType));
		
		m_eGameObjectType = f_ePhysXObjectType;

		switch (f_eCollisionShapeType) {
			case COLLISION_SHAPE_SPHERE:
				addSphere(m_pGraphicsObject, f_ePhysXObjectType);
				break;
			case COLLISION_SHAPE_CAPSULE:
				addCapsule(m_pGraphicsObject, f_ePhysXObjectType);
				break;
			case COLLISION_SHAPE_CYLINDER :
				addCylinder(m_pGraphicsObject,f_ePhysXObjectType);
				break;
			case COLLISION_SHAPE_BOX :
				addBox(m_pGraphicsObject, f_ePhysXObjectType);
				break;
			case DEFAULT_COLLISION_SHAPE:
				addBox(m_pGraphicsObject, f_ePhysXObjectType);
				break;
			default :
				m_DynamicActor	= NULL;
				break;
		}
	}

	*f_pGraphicsObject = m_pGraphicsObject;
}

physx::PxVec3 PhysicsPrimitiveObject::getPosition() {
	if (m_eGameObjectType == PHYSICS_DYNAMIC) {
		physx::PxTransform pose = m_DynamicActor->getGlobalPose();
		return physx::PxVec3(pose.p);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		physx::PxTransform pose = m_StaticActor->getGlobalPose();
		return physx::PxVec3(pose.p);
	}
	return physx::PxVec3(0, 0, 0);
}


bool PhysicsPrimitiveObject::scale()
{
	bool f_bScaled = false;
	switch (m_eCollisionShapeType) {
		
		case COLLISION_SHAPE_SPHERE :
			f_bScaled = scaleSphere();
			break;
		case COLLISION_SHAPE_CAPSULE :
			f_bScaled = scaleCapsule();
			break;
		case COLLISION_SHAPE_BOX :
			f_bScaled = scaleBox();
			break;
		default :
			break;
	}
	return f_bScaled;
}

void PhysicsPrimitiveObject::addCylinder(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType) {
}

bool PhysicsPrimitiveObject::scaleCylinder() {
	return false;
}

void PhysicsPrimitiveObject::addBox(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType) {

	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	Ogre::Vector3 vctScale = f_pTempOgreSceneNode->_getDerivedScale();
	physx::PxReal xScale = vctScale.x;
	physx::PxReal yScale = vctScale.y;
	physx::PxReal zScale = vctScale.z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	

	m_vctCentroid = -f_vCenterOfEntity * vctScale;
	f_ObjectPosition -= m_vctCentroid;

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal density = 1.0f;
	physx::PxVec3 dimensions(f_rWidth * 0.5f, f_rHeight * 0.5f, f_rDepth * 0.5f);
	physx::PxBoxGeometry geometry(dimensions);

	physx::PxTransform transform(physx::PxVec3(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z), physx::PxQuat::createIdentity());

	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);

	m_eCollisionShapeType = COLLISION_SHAPE_BOX;

	if (f_ePhysXObjectType == PHYSICS_DYNAMIC) {

		m_DynamicActor = PxCreateDynamic(*m_PhysXSDK, transform, geometry, *mMaterial, density);

		m_PhysXScene->addActor(*m_DynamicActor);
	}
	else if (f_ePhysXObjectType == PHYSICS_FIXED) {
		
		m_StaticActor = PxCreateStatic(*m_PhysXSDK, transform, geometry, *mMaterial);

		m_PhysXScene->addActor(*m_StaticActor);
	}
}

bool PhysicsPrimitiveObject::scaleBox()
{
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	if (m_eGameObjectType == PHYSICS_DYNAMIC) {
		m_PhysXScene->removeActor(*m_DynamicActor);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		m_PhysXScene->removeActor(*m_StaticActor);
	}

	Ogre::Vector3 vctScale = f_pTempOgreSceneNode->_getDerivedScale();
	physx::PxReal xScale = vctScale.x;
	physx::PxReal yScale = vctScale.y;
	physx::PxReal zScale = vctScale.z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	m_vctCentroid = -f_vCenterOfEntity * vctScale;
	f_ObjectPosition -= m_vctCentroid;

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal density = 1.0f;
	physx::PxVec3 dimensions(f_rWidth * 0.5f, f_rHeight * 0.5f, f_rDepth * 0.5f);
	physx::PxBoxGeometry geometry(dimensions);

	physx::PxTransform transform(physx::PxVec3(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z), physx::PxQuat::createIdentity());

	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);
	
	if (m_eGameObjectType == PHYSICS_DYNAMIC) {

		m_DynamicActor = PxCreateDynamic(*m_PhysXSDK, transform, geometry, *mMaterial, density);

		m_PhysXScene->addActor(*m_DynamicActor);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		
		m_StaticActor = PxCreateStatic(*m_PhysXSDK, transform, geometry, *mMaterial);

		m_PhysXScene->addActor(*m_StaticActor);
	}
	return true;
}


void PhysicsPrimitiveObject::addSphere(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType) {
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	physx::PxReal xScale = f_pTempOgreSceneNode->getScale().x;
	physx::PxReal yScale = f_pTempOgreSceneNode->getScale().y;
	physx::PxReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	m_vctCentroid = -f_vCenterOfEntity * f_pTempOgreSceneNode->getScale();
	f_ObjectPosition -= m_vctCentroid;

	physx::PxReal f_rRadius = f_pTempOgreEntity->getBoundingRadius();

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal density = 1.0f;
	
	physx::PxSphereGeometry geometry(f_rRadius * f_rWidth);

	physx::PxTransform transform(physx::PxVec3(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z), physx::PxQuat::createIdentity());

	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);

	m_eCollisionShapeType = COLLISION_SHAPE_SPHERE;

	if (f_ePhysXObjectType == PHYSICS_DYNAMIC) {

		m_DynamicActor = PxCreateDynamic(*m_PhysXSDK, transform, geometry, *mMaterial, density);

#ifdef PHYSX_APEX
		physx::PxRigidDynamic * underlyingActor = m_DestructibleModule->addTwoWayRb(m_DynamicActor,*m_ApexScene);
		//m_DynamicActor = underlyingActor;
		m_PhysXScene->addActor(*m_DynamicActor);
#else
		m_PhysXScene->addActor(*m_DynamicActor);
#endif


		//m_DynamicActor->userData = (void*)4;
		/*physx::PxShape* treasureShape;
		m_DynamicActor->getShapes(&treasureShape, 1);
		treasureShape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		m_DynamicActor->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);*/
	}
	else if (f_ePhysXObjectType == PHYSICS_FIXED) {
		
		m_StaticActor = PxCreateStatic(*m_PhysXSDK, transform, geometry, *mMaterial);

		m_PhysXScene->addActor(*m_StaticActor);
	}
}

bool PhysicsPrimitiveObject::scaleSphere() {

	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	if (m_eGameObjectType == PHYSICS_DYNAMIC) {
		m_PhysXScene->removeActor(*m_DynamicActor);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		m_PhysXScene->removeActor(*m_StaticActor);
	}


	physx::PxReal xScale = f_pTempOgreSceneNode->getScale().x;
	physx::PxReal yScale = f_pTempOgreSceneNode->getScale().y;
	physx::PxReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	m_vctCentroid = -f_vCenterOfEntity * f_pTempOgreSceneNode->getScale();
	f_ObjectPosition -= m_vctCentroid;

	physx::PxReal f_rRadius = f_pTempOgreEntity->getBoundingRadius();

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal density = 1.0f;
	
	physx::PxSphereGeometry geometry((f_rWidth+f_rHeight+f_rDepth)/6.0f);

	physx::PxTransform transform(physx::PxVec3(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z), physx::PxQuat::createIdentity());

	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);
	
	if (m_eGameObjectType == PHYSICS_DYNAMIC) {

		m_DynamicActor = PxCreateDynamic(*m_PhysXSDK, transform, geometry, *mMaterial, density);

		m_PhysXScene->addActor(*m_DynamicActor);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		
		m_StaticActor = PxCreateStatic(*m_PhysXSDK, transform, geometry, *mMaterial);

		m_PhysXScene->addActor(*m_StaticActor);
	}

	return true;
}


void PhysicsPrimitiveObject::addCapsule(GraphicsObject* f_pGraphicsObject,GameObjectType f_ePhysXObjectType) {
	
	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	physx::PxReal xScale = f_pTempOgreSceneNode->getScale().x;
	physx::PxReal yScale = f_pTempOgreSceneNode->getScale().y;
	physx::PxReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	m_vctCentroid = -f_vCenterOfEntity * f_pTempOgreSceneNode->getScale();
	f_ObjectPosition -= m_vctCentroid;

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal density = 1.0f;
	physx::PxReal f_rRadius = min(min( f_rWidth,f_rHeight), f_rDepth)*0.5f;
	physx::PxReal f_rHalfHeight = max(max( f_rWidth,f_rHeight), f_rDepth)*0.5f-f_rRadius;

	
	physx::PxCapsuleGeometry geometry(f_rRadius,f_rHalfHeight);

	physx::PxTransform transform(physx::PxVec3(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z), physx::PxQuat::createIdentity());

	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);

	physx::PxTransform pose(physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0.0f,0.0f,1.0f)));

	m_eCollisionShapeType = COLLISION_SHAPE_CAPSULE;

	if (f_ePhysXObjectType == PHYSICS_DYNAMIC) {

		m_DynamicActor = PxCreateDynamic(*m_PhysXSDK, transform, geometry, *mMaterial, density, pose);

		m_PhysXScene->addActor(*m_DynamicActor);
	}
	else if (f_ePhysXObjectType == PHYSICS_FIXED) {
		
		m_StaticActor = PxCreateStatic(*m_PhysXSDK, transform, geometry, *mMaterial, pose);

		m_PhysXScene->addActor(*m_StaticActor);
	}
}


bool PhysicsPrimitiveObject::scaleCapsule() {

	Ogre::Entity* f_pTempOgreEntity = m_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = m_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();

	if (m_eGameObjectType == PHYSICS_DYNAMIC) {
		m_PhysXScene->removeActor(*m_DynamicActor);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		m_PhysXScene->removeActor(*m_StaticActor);
	}

	physx::PxReal xScale = f_pTempOgreSceneNode->getScale().x;
	physx::PxReal yScale = f_pTempOgreSceneNode->getScale().y;
	physx::PxReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	m_vctCentroid = -f_vCenterOfEntity * f_pTempOgreSceneNode->getScale();
	f_ObjectPosition -= m_vctCentroid;

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal density = 1.0f;
	physx::PxReal f_rRadius = min(min( f_rWidth,f_rHeight), f_rDepth)*0.5f;
	physx::PxReal f_rHalfHeight = max(max( f_rWidth,f_rHeight), f_rDepth)*0.5f;

	physx::PxCapsuleGeometry geometry(f_rRadius,f_rHalfHeight);

	physx::PxTransform transform(physx::PxVec3(f_ObjectPosition.x, f_ObjectPosition.y, f_ObjectPosition.z), physx::PxQuat::createIdentity());

	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);

	physx::PxTransform pose(physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0.0f,0.0f,1.0f)));

	m_eCollisionShapeType = COLLISION_SHAPE_CAPSULE;

	if (m_eGameObjectType == PHYSICS_DYNAMIC) {

		m_DynamicActor = PxCreateDynamic(*m_PhysXSDK, transform, geometry, *mMaterial, density, pose);

		m_PhysXScene->addActor(*m_DynamicActor);
	}
	else if (m_eGameObjectType == PHYSICS_FIXED) {
		
		m_StaticActor = PxCreateStatic(*m_PhysXSDK, transform, geometry, *mMaterial, pose);

		m_PhysXScene->addActor(*m_StaticActor);
	}

	return true;
}


bool PhysicsPrimitiveObject::update() {
	if (m_eGameObjectType == PHYSICS_DYNAMIC) {
		physx::PxTransform pxTrans = m_DynamicActor->getGlobalPose();

		pxTrans.p.x += m_vctCentroid.x;
		pxTrans.p.y += m_vctCentroid.y;
		pxTrans.p.z += m_vctCentroid.z;

		if (m_pGraphicsObject) m_pGraphicsObject->setPosition(pxTrans.p.x,pxTrans.p.y,pxTrans.p.z);
		m_pGraphicsObject->m_pOgreSceneNode->setOrientation(Ogre::Quaternion(pxTrans.q.w,pxTrans.q.x,pxTrans.q.y,pxTrans.q.z));
		//GetDefaultSceneManager()->getEntity(buffer)->getParentSceneNode()->setPosition(pxTrans.p.x,pxTrans.p.y,pxTrans.p.z);
		//GetDefaultSceneManager()->getEntity(buffer)->getParentSceneNode()->setOrientation(pxTrans.q.w,pxTrans.q.x,pxTrans.q.y,pxTrans.q.z);
	}
	if (m_eGameObjectType == PHYSICS_FIXED) {
		physx::PxTransform pxTrans = m_StaticActor->getGlobalPose();

		pxTrans.p.x += m_vctCentroid.x;
		pxTrans.p.y += m_vctCentroid.y;
		pxTrans.p.z += m_vctCentroid.z;

		if (m_pGraphicsObject) m_pGraphicsObject->setPosition(pxTrans.p.x,pxTrans.p.y,pxTrans.p.z);
		m_pGraphicsObject->m_pOgreSceneNode->setOrientation(Ogre::Quaternion(pxTrans.q.w,pxTrans.q.x,pxTrans.q.y,pxTrans.q.z));
		//GetDefaultSceneManager()->getEntity(buffer)->getParentSceneNode()->setPosition(pxTrans.p.x,pxTrans.p.y,pxTrans.p.z);
		//GetDefaultSceneManager()->getEntity(buffer)->getParentSceneNode()->setOrientation(pxTrans.q.w,pxTrans.q.x,pxTrans.q.y,pxTrans.q.z);
	}

	return true;
}


CharacterObject::CharacterObject(GraphicsObject* f_pGraphicsObject) : PhysicsObject(){
	Ogre::Entity* f_pTempOgreEntity = f_pGraphicsObject->m_pOgreEntity;
	Ogre::SceneNode* f_pTempOgreSceneNode = f_pGraphicsObject->m_pOgreSceneNode;
	Ogre::Vector3 f_ObjectPosition = f_pTempOgreSceneNode->getPosition();
	m_pGraphicsObject = f_pGraphicsObject;
	
	
	m_PhysXSDK	= EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	m_PhysXScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();
	physx::PxFoundation* mFoundation = &m_PhysXSDK->getFoundation();
	m_PxControllerManager = PxCreateControllerManager(*mFoundation);
	
	physx::PxMaterial* mMaterial = m_PhysXSDK->createMaterial(0.5f, 0.5f, 0.1f);
	
	physx::PxReal xScale = f_pTempOgreSceneNode->getScale().x;
	physx::PxReal yScale = f_pTempOgreSceneNode->getScale().y;
	physx::PxReal zScale = f_pTempOgreSceneNode->getScale().z;

	Ogre::AxisAlignedBox f_vEntityAabb = f_pTempOgreEntity->getBoundingBox();
	Ogre::Vector3 f_vCenterOfEntity = f_vEntityAabb.getCenter();

	physx::PxReal f_rWidth	= xScale*f_vEntityAabb.getSize().x;
	physx::PxReal f_rHeight = yScale*f_vEntityAabb.getSize().y;
	physx::PxReal f_rDepth	= zScale*f_vEntityAabb.getSize().z;

	physx::PxReal f_rRadius = min(min( f_rWidth,f_rHeight), f_rDepth)*0.5f;
	physx::PxReal f_rHalfHeight = max(max( f_rWidth,f_rHeight), f_rDepth)*0.5f;	

	m_Desc.material = mMaterial;
	m_Desc.position = physx::PxExtendedVec3(0.0f, 0.0f, 0.0f);
	m_Desc.height = f_rHalfHeight;
	m_Desc.radius = f_rRadius;
	m_Desc.slopeLimit = 0.1f;
	m_Desc.contactOffset = 0.001f;
	m_Desc.stepOffset = 1.5f;
	//m_Desc.callback = m_Desc;
	m_Desc.interactionMode = physx::PxCCTInteractionMode::eEXCLUDE;
	m_Desc.upDirection = physx::PxVec3(0,1,0);

	m_PxSpeed = physx::PxVec3(0, float(-9.81*EnginePtr->GetDeltaTime()), 0);

	m_Controller = m_PxControllerManager->createController(*m_PhysXSDK, m_PhysXScene, m_Desc);
}

bool CharacterObject::update()
{
	physx::PxU32 collisionFlags = 0;
	const physx::PxControllerFilters filter;
							
	collisionFlags = m_Controller->move(m_PxSpeed, 0.00001f, EnginePtr->GetDeltaTime(), filter);

	m_PxControllerManager->computeInteractions(EnginePtr->GetDeltaTime());

	physx::PxExtendedVec3 position = m_Controller->getPosition();

	if (m_pGraphicsObject) 
		m_pGraphicsObject->setPosition(float(position.x),float(position.y)-1.5f,float(position.z));

	return true;
}

#endif