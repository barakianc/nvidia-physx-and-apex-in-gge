#ifndef _APEX_RENDERER_H_
#define _APEX_RENDERER_H_

#ifdef PHYSX
#ifdef PHYSX_APEX

#include <NxUserRenderer.h>

class GGEApexRenderer : public physx::apex::NxUserRenderer
{
public:
	GGEApexRenderer();
	~GGEApexRenderer();
	virtual void renderResource(const physx::apex::NxApexRenderContext& context);
	std::string m_materialName;
};

#endif
#endif

#endif