#pragma once

// Core
#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif

// Winsock
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
// End Winsock
#include <stdio.h>
#include <tchar.h>
#include <Windows.h>
#include <time.h>

// Ogre
#include "Ogre.h"


// Trace
#define GGETRACE(...) \
{ \
	std::stringstream traceStringStream; \
	traceStringStream << __FILE__ << " @ " << __LINE__ << ": "; \
	printf(traceStringStream.str().c_str()); \
	printf(__VA_ARGS__); \
	printf("\n"); \
} \

#define GGETRACELOG(...) \
{ \
	char traceString[1024]; \
	sprintf_s(traceString, 1024, __VA_ARGS__); \
	std::stringstream traceStringStream; \
	traceStringStream << __FILE__ << " @ " << __LINE__ << ": " << traceString; \
	Ogre::LogManager::getSingleton().logMessage(traceStringStream.str()); \
} \

#define GGEPRINT(...) \
{ \
	char traceString[1024]; \
	sprintf_s(traceString, 1024, __VA_ARGS__); \
	std::stringstream traceStringStream; \
	traceStringStream << __FILE__ << " @ " << __LINE__ << ": " << traceString; \
	Ogre::LogManager::getSingleton().logMessage(traceStringStream.str()); \
} \


// Exit
#define GGEEXIT() { SendMessage(EnginePtr->GetWindowHandle(), WM_DESTROY, 0, 0); }
#define GGEEXITNOW() { exit(1); }

namespace GamePipe
{
	//////////////////////////////////////////////////////////////////////////
	// Object
	//////////////////////////////////////////////////////////////////////////
	class Object
	{
	private:
		static unsigned int ms_uiFreeId;

		unsigned int m_uiObjectId;
		std::string m_ObjectName;
		std::string m_ObjectType;

		bool m_bIsValid;
		bool m_bIsReadyToDestroy;

	public:
		inline static unsigned int GetNextFreeId() { return ++ms_uiFreeId; }

		virtual inline std::string GetName() { return m_ObjectName; }
		virtual inline void SetName(std::string objektName) { m_ObjectName = objektName; }
		virtual inline std::string GetType() { return m_ObjectType; }
		virtual inline void SetType(std::string objektType) { m_ObjectType = objektType; }
		virtual inline unsigned int GetId() { return m_uiObjectId; }

		virtual inline bool IsValid() { return m_bIsValid; }
		virtual inline void IsValid(bool bIsValid) { m_bIsValid = bIsValid; }
		virtual inline bool IsReadyToDestroy() { return m_bIsReadyToDestroy; }
		virtual inline void IsReadyToDestroy(bool bIsReadyToDestroy) { m_bIsReadyToDestroy = bIsReadyToDestroy; }

		virtual void Create() {}
		virtual void Destroy() {}
		virtual void Update() {}		

	public:
		Object()
		{
			m_uiObjectId = GetNextFreeId();

			SetName("Object" + Ogre::StringConverter::toString(m_uiObjectId));
			SetType("Object");

			IsValid(false);
			IsReadyToDestroy(false);

			GGETRACELOG("Created object: %s", GetName().c_str());
		}

		virtual ~Object()
		{
			IsValid(false);
		}
	};

	//////////////////////////////////////////////////////////////////////////
	// ObjektManager
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	class ObjectManager
	{
	private:

	protected:
		typedef std::map<std::string, T*> TCollection;
		typedef std::list<std::string> TDestroyCollection;

		TCollection m_mapObjects;
		TDestroyCollection m_mapDestroyObjectNames;

		typedef typename TCollection::iterator Iterator;
		typedef typename TCollection::const_iterator ConstIterator;

		TDestroyCollection& DestroyCollection()
		{
			return m_mapDestroyObjectNames;
		}

	public:
		TCollection& Collection()
		{
			return m_mapObjects;
		}

		virtual T* Get(std::string objectKey)
		{
			T* tObject = m_mapObjects[objectKey];
			//assert(objekt);
			return tObject;
		}

		virtual T* Get(size_t objectIndex)
		{
			T* tObject = NULL;
			ConstIterator constIterator = Begin();
			for (size_t i = 0; i < objectIndex; ++i) { ++constIterator; }
			tObject = (*constIterator).second;
			//assert(objekt);
			return tObject;
		}

		virtual void Add(std::string objectKey, T* objekt)
		{
			assert(!Collection()[objectKey]);
			Collection()[objectKey] = objekt;
		}

		virtual void Remove(std::string objectKey)
		{
			DestroyCollection().push_back(objectKey);
		}

		virtual void Remove(T* pObject)
		{
			for (ConstIterator constIterator = Begin(); constIterator != End(); ++constIterator)
			{
				if (((*constIterator).second) && ((*constIterator).second) == pObject)
				{
					Remove((*constIterator).first);
				}
			}
		}

		void _Update()
		{
			for (TDestroyCollection::const_iterator constIterator =  DestroyCollection().begin(); constIterator != DestroyCollection().end(); ++constIterator)
			{
				if (Collection()[(*constIterator)])
				{
					Collection()[(*constIterator)]->Destroy();
					delete Collection()[(*constIterator)];
					Collection().erase(*constIterator);
				}
			}
			DestroyCollection().clear();

			for (ConstIterator constIterator = Begin(); constIterator != End(); ++constIterator)
			{
				if ((*constIterator).second)
				{
					if (((*constIterator).second)->IsReadyToDestroy())
					{
						DestroyCollection().push_back((*constIterator).first);
					}
					else
					{
						((*constIterator).second)->Update();
					}
				}
			}

			Update();
		}

		virtual void Update() { }

		virtual void DestroyAll()
		{
			for (ConstIterator constIterator = Begin(); constIterator != End(); ++constIterator)
			{
				if ((*constIterator).second)
				{
					DestroyCollection().push_back((*constIterator).first);
				}
			}

			for (TDestroyCollection::const_iterator constIterator =  DestroyCollection().begin(); constIterator != DestroyCollection().end(); ++constIterator)
			{
				if (Collection()[(*constIterator)])
				{
					Collection()[(*constIterator)]->Destroy();
					delete Collection()[(*constIterator)];
					Collection().erase(*constIterator);
				}
			}

			DestroyCollection().clear();
			Collection().clear();
		}

		Iterator Begin()
		{
			return Collection().begin();
		}

		Iterator End()
		{
			return Collection().end();
		}

		ConstIterator Begin() const
		{
			return Collection().begin();
		}

		ConstIterator End() const
		{
			return Collection().end();
		}

	protected:
		ObjectManager() {}
		virtual ~ObjectManager()
		{
			DestroyAll();
		}
	};

	//////////////////////////////////////////////////////////////////////////
	// Singleton. Not thread safe.
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	class Singleton
	{
	private:
		static T* m_pInstance;

	public:
		static T* GetInstancePointer()
		{
			if (0 == m_pInstance)
			{
				m_pInstance = new T();
			}

			return m_pInstance;
		}

		static T& GetInstanceReference()
		{
			if (0 == m_pInstance)
			{
				m_pInstance = new T();
			}

			return *m_pInstance;
		}

		static void DestroyInstance()
		{
			delete m_pInstance;
			m_pInstance = 0;
		}

	protected:
		Singleton() {}
		virtual ~Singleton() {}

	private:
		Singleton(const Singleton& source) {}
	};

	template<typename T>
	T* Singleton<T>::m_pInstance = 0;
}