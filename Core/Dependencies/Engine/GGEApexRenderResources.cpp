#include "StdAfx.h"

#include "Ogre.h"
#include "Engine.h"

#ifdef PHYSX
#ifdef PHYSX_APEX

#include "GGEApexRenderResources.h"
#include "NxUserRenderResourceDesc.h"
#include "NxApexRenderDataFormat.h"
#include "NxUserRenderResourceManager.h"
#include "NxUserRenderIndexBufferDesc.h"

GGEApexRendererMesh::GGEApexRendererMesh(const physx::apex::NxUserRenderResourceDesc &desc, std::string meshName, std::string entityName){
	PX_ASSERT(desc.isValid());

	m_vertexBuffers     = 0;
	m_indexBuffer       = 0;
	
	m_numVertexBuffers = desc.numVertexBuffers;
	if (m_numVertexBuffers > 0)
	{
		m_vertexBuffers = new GGEApexRendererVertexBuffer*[m_numVertexBuffers];
		for (physx::PxU32 i = 0; i < m_numVertexBuffers; i++)
		{
			m_vertexBuffers[i] = static_cast<GGEApexRendererVertexBuffer*>(desc.vertexBuffers[i]);
		}
	}

	m_numVertices	 = desc.numVerts;

	m_firstIndex     = desc.firstIndex;
	m_numIndices     = desc.numIndices;
	m_indexBuffer    = static_cast<GGEApexRendererIndexBuffer*>(desc.indexBuffer);

	m_firstBone		 = desc.firstBone;
	m_numBones		 = desc.numBones;
	m_boneBuffer     = static_cast<GGEApexRendererBoneBuffer*>(desc.boneBuffer);

	m_firstInstance  = desc.firstInstance;
	m_numInstances   = desc.numInstances;
	m_instanceBuffer = static_cast<GGEApexRendererInstanceBuffer*>(desc.instanceBuffer);

	m_instanceBuffer    = 0;
	m_firstInstance		= 0;
	m_numInstances		= 0;

	m_spriteBuffer		= 0;

	m_cullMode       = desc.cullMode;
	m_meshTransform = physx::PxMat44::createIdentity();

	m_meshName = meshName;
	m_entityName = entityName;
	
	//Create manual mesh
	m_mesh = Ogre::MeshManager::getSingleton().createManual(m_meshName, "apex");

	//Create submesh
	m_subMesh = m_mesh.get()->createSubMesh();


	//Create vertex data
	m_vertexData = new Ogre::VertexData();
	m_subMesh->useSharedVertices = false;
	m_subMesh->vertexData = m_vertexData;
	m_vertexData->vertexCount = m_numVertices;

	//Create vertex declation
	m_vertexDecl = m_vertexData->vertexDeclaration;
	size_t offset = 0;
	m_vertexDecl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
	offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
	m_vertexDecl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
	m_vertexDecl->addElement(1, 0, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES);

	string manName = "testmanual" + m_entityName;
	m_testman = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->createManualObject(manName);
	EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode()->attachObject(m_testman);
	m_testman->getParentNode()->setPosition(10,0,10);
	m_testman->getParentNode()->rotate(*(new Ogre::Quaternion(1.0,0,90.0,0)));
	m_testman->estimateVertexCount(m_numVertices);
	m_testman->estimateIndexCount(m_numIndices);
	m_testman->setDynamic(true);
	//renderToOgreMesh();
}

GGEApexRendererMesh::~GGEApexRendererMesh(){

	delete [] m_vertexBuffers;
	m_testman->detachFromParent();
	delete m_testman;
}

void GGEApexRendererMesh::renderToOgreMesh(std::string matname)
{
	

	//Create vertex buffer
	//Ogre::HardwareVertexBufferSharedPtr vertexBuffer = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(m_vertexDecl->getVertexSize(0), m_numVertices, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);

	//Add data to vertex buffer
	float *vertArray;
	float *textArray;
	bool rewrite = false;
	for(int i = 0; i< m_numVertexBuffers; i++){
		if(m_vertexBuffers[i]->m_writtenTo){
			rewrite = true;
		}
		m_vertexBuffers[i]->m_writtenTo = false;
	}

	if(rewrite){
		vertArray = new float[m_numVertices * 6];
		textArray = new float[m_numVertices* 2];
		float maxX = 0, maxY = 0, maxZ = 0;
		float minX = 0, minY = 0, minZ = 0;
		minX = m_vertexBuffers[0]->vertData[0].position.x;
		minY = m_vertexBuffers[0]->vertData[0].position.y;
		minZ = m_vertexBuffers[0]->vertData[0].position.z;
		if(m_testman->getNumSections() > 0){
			m_testman->beginUpdate(0);
		}
		else{
			m_testman->begin(matname, Ogre::RenderOperation::OT_TRIANGLE_LIST);
		}
		//tore vertex data and texture coords into our 2 array to them be written out to 
		//our manual object
		int v = 0, t = 0;
		for (unsigned int i = 0; i < m_numVertexBuffers; i++ )
		{
			unsigned int nVert = m_vertexBuffers[i]->m_numVerts;
			unsigned int nUVs = m_vertexBuffers[i]->m_numUVs;
			if(nVert > 0){
				for(unsigned int j = 0, k = 0; k < nVert; k++, j+=6)
				{
					if ( m_vertexBuffers[i]->vertData[k].position.x > maxX )
						maxX = m_vertexBuffers[i]->vertData[k].position.x;
					else if ( m_vertexBuffers[i]->vertData[k].position.x < minX )
						minX = m_vertexBuffers[i]->vertData[k].position.x;
					if ( m_vertexBuffers[i]->vertData[k].position.y > maxY )
						maxY = m_vertexBuffers[i]->vertData[k].position.x;
					else if ( m_vertexBuffers[i]->vertData[k].position.y < minY )
						minY = m_vertexBuffers[i]->vertData[k].position.y;
					if ( m_vertexBuffers[i]->vertData[k].position.z > maxZ )
						maxZ = m_vertexBuffers[i]->vertData[k].position.x;
					else if ( m_vertexBuffers[i]->vertData[k].position.z < minZ )
						minZ = m_vertexBuffers[i]->vertData[k].position.z;

					/*vertArray[j]   = m_vertexBuffers[i]->vertData[k].position.x;
					vertArray[j+1] = m_vertexBuffers[i]->vertData[k].position.y;
					vertArray[j+2] = m_vertexBuffers[i]->vertData[k].position.z;
					vertArray[j+3] = m_vertexBuffers[i]->vertData[k].normal.x;
					vertArray[j+4] = m_vertexBuffers[i]->vertData[k].normal.y;
					vertArray[j+5] = m_vertexBuffers[i]->vertData[k].normal.z;*/
					if(m_vertexBuffers[i]->vertData[k].position.x > 500 || m_vertexBuffers[i]->vertData[k].position.y < -500){
						int test = 0;
					}


					//physx::PxVec4 oldVert = physx::PxVec4(m_vertexBuffers[i]->vertData[k].position.x, m_vertexBuffers[i]->vertData[k].position.y, m_vertexBuffers[i]->vertData[k].position.z, 1.0);

				/*
				physx::PxVec4 oldVert = physx::PxVec4(m_vertexBuffers[i]->vertData[k].position.x, m_vertexBuffers[i]->vertData[k].position.y, m_vertexBuffers[i]->vertData[k].position.z, 1.0);
=======
				if(m_numBones > 1 )
				{
					physx::PxVec4 oldVert = physx::PxVec4(m_vertexBuffers[i]->vertData[k].position.x, m_vertexBuffers[i]->vertData[k].position.y, m_vertexBuffers[i]->vertData[k].position.z, 1.0);
>>>>>>> .r5915

<<<<<<< .mine
=======
					physx::PxVec4 newVert = physx::PxVec4(0.0);
>>>>>>> .r5915

<<<<<<< .mine
					physx::PxVec4 newVert = physx::PxVec4(0.0);
=======
					physx::PxU32 ib = 0;
					float weight = 0.0;
>>>>>>> .r5915

<<<<<<< .mine
					physx::PxU32 ib = 0;
					float weight = 0.0;
					//if(m_numBones > 0){
						/*for ( int boneNum = 0; boneNum < 1; boneNum ++ )
						{
							//for each bone ib associated with vertex k
							ib = m_vertexBuffers[i]->vertexBoneData[k].bonesIndices[boneNum];
							weight = 1;//m_vertexBuffers[i]->vertexBoneData[k].boneWeights[boneNum];
=======
					for ( int boneNum = 0; boneNum < 4; boneNum ++ )
					{
						//for each bone ib associated with vertex k
						ib = m_vertexBuffers[i]->vertexBoneData[k].bonesIndices[boneNum];
						weight = m_vertexBuffers[i]->vertexBoneData[k].boneWeights[boneNum];
>>>>>>> .r5915
					
<<<<<<< .mine
							physx::PxMat44 transformationMat = m_boneBuffer->m_bones[ib];
=======
						ib = 1;
						weight = 0.25;
						physx::PxMat44 transformationMat = m_boneBuffer->m_bones[ib];
>>>>>>> .r5915

<<<<<<< .mine
							physx::PxVec4 tempVert = physx::PxVec4(0.0);
=======
						physx::PxVec4 tempVert = physx::PxVec4(0.0);
>>>>>>> .r5915

<<<<<<< .mine
							for ( int a = 0; a < 4; a++ )
							{
								for ( int b = 0; b < 4; b++ )
								{
									tempVert[a] += oldVert[b] * transformationMat[b][a];
								}
							}

							newVert += weight * tempVert;
=======
						for ( int a = 0; a < 4; a++ )
						{
							for ( int b = 0; b < 4; b++ )
							{
								tempVert[a] += oldVert[b] * transformationMat[b][a];
							}
>>>>>>> .r5915
						}
<<<<<<< .mine
						m_testman->position(newVert[0], newVert[1], newVert[2]);
=======

						newVert += weight * tempVert;
>>>>>>> .r5915
					}
					else{
						m_testman->position(oldVert[0], oldVert[1], oldVert[2]);
					}

				/*m_vertexBuffers[i]->vertData[k].position.x = newVert[0];
				m_vertexBuffers[i]->vertData[k].position.y = newVert[1];
				m_vertexBuffers[i]->vertData[k].position.z = newVert[2];*/
					vertArray[v] = m_vertexBuffers[i]->vertData[k].position.x;
					vertArray[v+1] = m_vertexBuffers[i]->vertData[k].position.y;
					vertArray[v+2] = m_vertexBuffers[i]->vertData[k].position.z;
					vertArray[v+3] = m_vertexBuffers[i]->vertData[k].normal.x;
					vertArray[v+4] = m_vertexBuffers[i]->vertData[k].normal.y;
					vertArray[v+5] = m_vertexBuffers[i]->vertData[k].normal.z;

					v+=6;
					/*m_testman->position(m_vertexBuffers[i]->vertData[k].position.x, m_vertexBuffers[i]->vertData[k].position.y, m_vertexBuffers[i]->vertData[k].position.z);
					m_testman->normal(m_vertexBuffers[i]->vertData[k].normal.x,m_vertexBuffers[i]->vertData[k].normal.y,m_vertexBuffers[i]->vertData[k].normal.z);
					m_testman->textureCoord(m_vertexBuffers[i]->vertData[k].texCoords0[0], m_vertexBuffers[i]->vertData[k].texCoords0[1]);*/
				}
			}
			if(nUVs > 0){
				for(unsigned int j = 0, k = 0; k < nUVs; k++, j+=6)
				{

					
					if(m_vertexBuffers[i]->vertData[k].position.x > 500 || m_vertexBuffers[i]->vertData[k].position.y < -500){
						int test = 0;
					}

					textArray[t] = m_vertexBuffers[i]->vertData[k].texCoords0[0];
					textArray[t+1] = m_vertexBuffers[i]->vertData[k].texCoords0[1];

					t += 2;
					//physx::PxVec4 oldVert = physx::PxVec4(m_vertexBuffers[i]->vertData[k].position.x, m_vertexBuffers[i]->vertData[k].position.y, m_vertexBuffers[i]->vertData[k].position.z, 1.0);

					//m_testman->textureCoord(m_vertexBuffers[i]->vertData[k].texCoords0[0], m_vertexBuffers[i]->vertData[k].texCoords0[1]);
				}
			}
		}

		for(int i =0; i < m_numVertices; i++){
			m_testman->position(vertArray[(i*6)],vertArray[(i*6)+1],vertArray[(i*6)+2]);
			m_testman->normal(vertArray[(i*6)+3],vertArray[(i*6)+4],vertArray[(i*6)+5]);
			m_testman->textureCoord(textArray[(i*2)],textArray[(i*2)+1]);
		}

		delete [] vertArray;
		delete [] textArray;
		//vertexBuffer->writeData(0, vertexBuffer->getSizeInBytes(),vertArray,true);

		//Bind vertex data
		//Ogre::VertexBufferBinding *bind = m_vertexData->vertexBufferBinding;
		//bind->setBinding(0, vertexBuffer);
	
		//Texure buffer
		//Ogre::HardwareVertexBufferSharedPtr texBuffer = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(vertexDecl->getVertexSize(1), m_numVertices, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
		//texBuffer->writeData(0, texBuffer->getSizeInBytes(),"Pass array", true);
	
		//Texture binding
		//Ogre::VertexBufferBinding *texBind = vertexData->vertexBufferBinding;
		//texBind->setBinding(1, texBuffer);
	
		//Create index buffer
		//Ogre::HardwareIndexBufferSharedPtr indexBuffer = Ogre::HardwareBufferManager::getSingleton().createIndexBuffer(Ogre::HardwareIndexBuffer::IT_16BIT, m_numIndices, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
		//Add data to index buffer
		//float *indexArray = new float[m_numIndices * 3];
		for (unsigned int i = 0, j = 0; j < (m_numIndices/3); j++, i+=3 )
		{
			//indexArray[i] = m_indexBuffer->m_indexData[j].vertexIndices[0];
			//indexArray[i+1] = m_indexBuffer->m_indexData[j].vertexIndices[1];
			//indexArray[i+2] = m_indexBuffer->m_indexData[j].vertexIndices[2];

			m_testman->index(m_indexBuffer->m_indexData[j].vertexIndices[0]);
			m_testman->index( m_indexBuffer->m_indexData[j].vertexIndices[1]);
			m_testman->index( m_indexBuffer->m_indexData[j].vertexIndices[2]);
		}
		//delete [] indexArray;
		m_testman->end();
		//indexBuffer->writeData(0, indexBuffer->getSizeInBytes(), indexArray, true);

		//Link index buffer to submesh
		//m_subMesh->indexData->indexBuffer = indexBuffer;
		//m_subMesh->indexData->indexCount = m_numIndices;
		//m_subMesh->indexData->indexStart = 0;

		//m_subMesh->setMaterialName()

		//Set bounds
		//m_mesh.get()->_setBounds(Ogre::AxisAlignedBox(minX, minY, minZ, maxX, maxY, maxZ)); 
		//m_mesh.get()->_setBoundingSphereRadius(std::max((maxX - minX), std::max(maxY - minY, maxZ - minZ))/2.0);

		//m_mesh->load(false);

		//Ogre::Entity* ourEnt = EnginePtr->GetForemostGameScreen()->GetDefaultSceneManager()->getEntity(m_entityName);
	}
}

void GGEApexRendererMesh::setVertexBufferRange(physx::PxU32 firstVertex, physx::PxU32 numVerts){

	m_firstVertex = firstVertex;
	m_numVertices = numVerts;
}

void GGEApexRendererMesh::setIndexBufferRange(physx::PxU32 firstIndex, physx::PxU32 numIndices){

	m_firstIndex = firstIndex;
	m_numIndices = numIndices;
}

void GGEApexRendererMesh::setBoneBufferRange(physx::PxU32 firstBone, physx::PxU32 numBones){

	m_firstBone = firstBone;
	m_numBones = numBones;
}

void GGEApexRendererMesh::setInstanceBufferRange(physx::PxU32 firstInstance, physx::PxU32 numInstances){

	m_firstInstance = firstInstance;
	m_numInstances = numInstances;
}

void GGEApexRendererMesh::setSpriteBufferRange(physx::PxU32 firstSprite, physx::PxU32 numSprites){

	setVertexBufferRange(firstSprite, numSprites);
}

void GGEApexRendererMesh::setMaterial(void* material){
}

physx::PxU32 GGEApexRendererMesh::getNumVertices(){
	return m_numVertices;
}

physx::PxU32 GGEApexRendererMesh::getNumIndices(){
	return m_numIndices;
}

physx::PxU32 GGEApexRendererMesh::getNumInstances(){
	return m_numInstances;
}

physx::PxU32 GGEApexRendererMesh::getNbVertexBuffers() const{
	return m_numVertexBuffers;
}

physx::NxUserRenderVertexBuffer*	GGEApexRendererMesh::getVertexBuffer(physx::PxU32 index) const{
	return m_vertexBuffers[index];
}

physx::NxUserRenderIndexBuffer*	GGEApexRendererMesh::getIndexBuffer() const{
	return m_indexBuffer;
}

physx::NxUserRenderBoneBuffer*		GGEApexRendererMesh::getBoneBuffer() const{
	return m_boneBuffer;
}

physx::NxUserRenderInstanceBuffer*	GGEApexRendererMesh::getInstanceBuffer() const{
	return m_instanceBuffer;
}

physx::NxUserRenderSpriteBuffer*	GGEApexRendererMesh::getSpriteBuffer() const{
	return m_spriteBuffer;
}


GGEApexRendererVertexBuffer::GGEApexRendererVertexBuffer(const physx::apex::NxUserRenderVertexBufferDesc& desc){
	PX_ASSERT(desc.isValid());

	if(desc.isValid()){
		vertData = new vertexData[desc.maxVerts];
		vertexBoneData = new boneData[desc.maxVerts];
	}
	m_numVerts = 0;
	m_numUVs = 0;

	m_lock = false;

	m_writtenTo = false;
}

GGEApexRendererVertexBuffer::~GGEApexRendererVertexBuffer(){
	delete [] vertData;
}

void GGEApexRendererVertexBuffer::writeBuffer(const physx::apex::NxApexRenderVertexBufferData& data, physx::PxU32 firstVertex, physx::PxU32 numVerts){
	bool verts = false;
	bool bones = false;
	bool uvs = false;
	m_writtenTo = true;
	for (physx::PxU32 i = 0; i < physx::apex::NxRenderVertexSemantic::NUM_SEMANTICS; i++)
	{
		physx::apex::NxRenderVertexSemantic::Enum apexSemantic = (physx::apex::NxRenderVertexSemantic::Enum)i;
		switch(apexSemantic)
		{
		case physx::apex::NxRenderVertexSemantic::POSITION:
			{
				if((data.getSemanticData(apexSemantic).data)){
					verts = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::NORMAL:
			{
				if((data.getSemanticData(apexSemantic).data)){
					verts = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::TEXCOORD0:
			{
				if((data.getSemanticData(apexSemantic).data)){
					uvs = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::TEXCOORD1:
			{
				if((data.getSemanticData(apexSemantic).data)){
					uvs = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::TEXCOORD2:
			{
				if((data.getSemanticData(apexSemantic).data)){
					uvs = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::TEXCOORD3:
			{
				if((data.getSemanticData(apexSemantic).data)){
					uvs = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::BONE_INDEX:
			{
				if((data.getSemanticData(apexSemantic).data)){
					bones = true;
					break;
				}
				break;
			}
		case physx::apex::NxRenderVertexSemantic::BONE_WEIGHT:
			{
				if((data.getSemanticData(apexSemantic).data)){
					bones = true;
					break;
				}
				break;
			}
		default:
			break;
		}

	}


	if(!verts && !bones){
		return;
	}

	if(verts){
		const physx::apex::NxApexRenderSemanticData& positionSemantic = data.getSemanticData(physx::apex::NxRenderVertexSemantic::POSITION);
		const physx::apex::NxApexRenderSemanticData& normalSemantic = data.getSemanticData(physx::apex::NxRenderVertexSemantic::NORMAL);
		

		const physx::PxVec3* PX_RESTRICT positionSrc = (const physx::PxVec3*)positionSemantic.data;
		const physx::PxVec3* PX_RESTRICT normalSrc = (const physx::PxVec3*)normalSemantic.data;
		

		if(!m_lock){
			m_lock = true;

			for(int i = firstVertex; i < (firstVertex+numVerts);i++){
				if(positionSrc){
					vertData[i].position = *positionSrc++;
				}
				if(normalSrc){
					vertData[i].normal = *normalSrc++;
				}
			}
			m_numVerts = numVerts;

			m_lock = false;
		}
	}

	if(uvs){
		const physx::apex::NxApexRenderSemanticData& texSemantic0 = data.getSemanticData(physx::apex::NxRenderVertexSemantic::TEXCOORD0);
		const physx::apex::NxApexRenderSemanticData& texSemantic1 = data.getSemanticData(physx::apex::NxRenderVertexSemantic::TEXCOORD1);
		const physx::apex::NxApexRenderSemanticData& texSemantic2 = data.getSemanticData(physx::apex::NxRenderVertexSemantic::TEXCOORD2);
		const physx::apex::NxApexRenderSemanticData& texSemantic3 = data.getSemanticData(physx::apex::NxRenderVertexSemantic::TEXCOORD3);

		const physx::PxVec2* texSrc0 = (const physx::PxVec2*)texSemantic0.data;
		const physx::PxVec2* texSrc1 = (const physx::PxVec2*)texSemantic1.data;
		const physx::PxVec2* texSrc2 = (const physx::PxVec2*)texSemantic2.data;
		const physx::PxVec2* texSrc3 = (const physx::PxVec2*)texSemantic3.data;

		if(!m_lock){
			m_lock = true;

			for(int i = firstVertex; i < (firstVertex+numVerts);i++){
				if(texSrc0){
					vertData[i].texCoords0 = *texSrc0++;
				}
				if(texSrc1){
					vertData[i].texCoords1 = *texSrc1++;
				}
				if(texSrc2){
					vertData[i].texCoords2 = *texSrc2++;
				}
				if(texSrc3){
					vertData[i].texCoords3 = *texSrc3++;	
				}
			}
			m_numUVs = numVerts;

			m_lock = false;
		}
	}
	/*else if(bones){
		const physx::apex::NxApexRenderSemanticData& boneIndxSemantic = data.getSemanticData(physx::apex::NxRenderVertexSemantic::BONE_INDEX);
		const physx::apex::NxApexRenderSemanticData& bonewhtSemantic = data.getSemanticData(physx::apex::NxRenderVertexSemantic::BONE_WEIGHT);
		int numindcs1, numindcs2;

		switch(boneIndxSemantic.format)
		{
		case physx::apex::NxRenderDataFormat::USHORT1: numindcs1 = 1; break;
		case physx::apex::NxRenderDataFormat::USHORT2: numindcs1 = 2; break;
		case physx::apex::NxRenderDataFormat::USHORT3: numindcs1 = 3; break;
		case physx::apex::NxRenderDataFormat::USHORT4: numindcs1 = 4; break;
		default:
			PX_ALWAYS_ASSERT();
			break;
		}

		switch(bonewhtSemantic.format)
		{
		case 0:numindcs2 = 0; break;
		case physx::apex::NxRenderDataFormat::FLOAT1: numindcs2 = 1; break;
		case physx::apex::NxRenderDataFormat::FLOAT2: numindcs2 = 2; break;
		case physx::apex::NxRenderDataFormat::FLOAT3: numindcs2 = 3; break;
		case physx::apex::NxRenderDataFormat::FLOAT4: numindcs2 = 4; break;
		default:
			PX_ALWAYS_ASSERT();
			break;
		}
		physx::PxU8* indsrc = ((physx::PxU8*)boneIndxSemantic.data);
		physx::PxF32* whtsrc = ((physx::PxF32*)bonewhtSemantic.data);
		
		for(int j = 0; j < numVerts; j++){
			
			unsigned short* boneIndices = (unsigned short*)indsrc;
			float* boneWeights = (float*)whtsrc;
			for(int i = 0; i < numindcs1; i++){
				vertexBoneData[j].bonesIndices[i] = boneIndices[i];
			}
			for(int i = numindcs1; i < 4; i++){
				vertexBoneData[j].bonesIndices[i] = 0;
			}
			for(int i = 0; i < numindcs2; i++){
				vertexBoneData[j].boneWeights[i] = boneWeights[i];
			}
			for(int i = numindcs2; i < 4; i++){
				vertexBoneData[j].boneWeights[i] = 0;
			}

			indsrc = indsrc + boneIndxSemantic.stride;
			whtsrc = whtsrc + bonewhtSemantic.stride;
		}

	}*/
}


GGEApexRendererIndexBuffer::GGEApexRendererIndexBuffer(const physx::apex::NxUserRenderIndexBufferDesc& desc){
	//Set format
	physx::apex::NxRenderDataFormat::Enum apexFormat = desc.format;

	switch ( apexFormat )
	{
		case physx::apex::NxRenderDataFormat::USHORT1:
			m_format = FORMAT_UINT16;
			break;
		case physx::apex::NxRenderDataFormat::UINT1:
			m_format = FORMAT_UINT32;
			break;
		default:
		PX_ASSERT(m_format < NUM_FORMATS);
	}

	//Set hint
	physx::apex::NxRenderBufferHint::Enum apexHint = desc.hint;

	if ( apexHint == physx::apex::NxRenderBufferHint::DYNAMIC || apexHint == physx::apex::NxRenderBufferHint::STREAMING )
	{
		m_hint = HINT_DYNAMIC;
	}
	else
	{
		m_hint = HINT_STATIC;
	}

	m_maxIndices = desc.maxIndices;

	m_indexData = new indexData[m_maxIndices/3];

}

GGEApexRendererIndexBuffer::~GGEApexRendererIndexBuffer(){
	delete(m_indexData);
}

bool GGEApexRendererIndexBuffer::getInteropResourceHandle(CUgraphicsResource& handle){
	CUgraphicsResource* tmp = &handle;
	PX_FORCE_PARAMETER_REFERENCE(tmp);

	return false;
}

void GGEApexRendererIndexBuffer::writeBuffer(const void* srcData, physx::PxU32 srcStride, physx::PxU32 firstDestElement, physx::PxU32 numElements){
	if ( numElements )
	{
		//should lock the index buffer

		//assuming primitives to be TRIANGLES
		physx::PxU32 numTriangles = numElements/3;

		if ( m_format == FORMAT_UINT16 )
		{
			const physx::PxU16* src = (const physx::PxU16*)srcData;

			for ( unsigned int i = 0; i < numTriangles; i++ )
			{
				//Sample changes src order while copying data to destination - Check if needed.
				m_indexData[i].vertexIndices[0] = (physx::PxU32)src[i * 3 + 0];
				m_indexData[i].vertexIndices[1] = (physx::PxU32)src[i * 3 + 1];
				m_indexData[i].vertexIndices[2] = (physx::PxU32)src[i * 3 + 2];
			}
		}
		else if ( m_format == FORMAT_UINT32 )
		{
			const physx::PxU32* src = (const physx::PxU32*)srcData;

			for ( unsigned int i = 0; i < numTriangles; i++ )
			{
				//Sample changes src order while copying data to destination - Check if needed.
				m_indexData[i].vertexIndices[0] = src[i * 3 + 0];
				m_indexData[i].vertexIndices[1] = src[i * 3 + 1];
				m_indexData[i].vertexIndices[2] = src[i * 3 + 2];
			}
		}
		else
		{
			std::cout<<"Index buffer format not found\n";
		}

	}
	else
	{
		std::cout<<"No elements to write to index buffer\n";
	}

	//unlock index buffer
}

GGEApexRendererBoneBuffer::GGEApexRendererBoneBuffer(const physx::apex::NxUserRenderBoneBufferDesc& desc){
	m_maxBones = desc.maxBones;
	m_bones = new physx::PxMat44[m_maxBones];
}

GGEApexRendererBoneBuffer::~GGEApexRendererBoneBuffer(){
	if (m_bones)
	{
		delete [] m_bones;
	}
}

physx::PxMat44* GGEApexRendererBoneBuffer::getBones() const{
	return m_bones;
}

void GGEApexRendererBoneBuffer::writeBuffer(const physx::apex::NxApexRenderBoneBufferData& data, physx::PxU32 firstBone, physx::PxU32 numBones){
	const physx::apex::NxApexRenderSemanticData& semanticData = data.getSemanticData(physx::apex::NxRenderBoneSemantic::POSE);
	if(numBones > 1){
		int test = 0;
	}
	if (semanticData.data && m_bones)
	{
		m_numBones = numBones;
		const void* srcData = semanticData.data;
		const physx::PxU32 srcStride = semanticData.stride;
		for (physx::PxU32 i = 0; i < numBones; i++)
		{
			//m_bones[firstBone + i] = physx::PxMat44(*(const physx::general_shared3::PxMat34Legacy*)srcData);
			m_bones[firstBone + i] = physx::PxMat44(*(const physx::PxMat44*)srcData);
			srcData = ((physx::PxU8*)srcData) + srcStride;
		}
	}
}

#endif
#endif