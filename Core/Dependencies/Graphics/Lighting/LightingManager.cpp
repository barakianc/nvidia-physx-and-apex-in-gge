#include "StdAfx.h"
#include "Ogre.h"
#include "SharedData.h"
#include "GameScreen.h"
#include "Engine.h"
#include "LightingManager.h"

template<> SharedData* Ogre::Singleton<SharedData>::msSingleton = 0;

namespace LightingManager
{
	static bool isInitialized = false;
	static DeferredShadingSystem *deferredShadingSystem;

	void Initialize(const char* directionalLightNodeName)
	{
		if (!isInitialized)
		{
			new SharedData();
			Ogre::RenderWindow* renderWindow = EnginePtr->GetRenderWindow();
			GamePipe::GameScreen *pGameScreen = EnginePtr->GetForemostGameScreen();
			if (pGameScreen)
			{
				deferredShadingSystem = new DeferredShadingSystem(renderWindow->getViewport(0), pGameScreen->GetDefaultSceneManager(), pGameScreen->GetActiveCamera());
				SharedData::getSingleton().iSystem = deferredShadingSystem;
				deferredShadingSystem->initialize();

				SharedData::getSingleton().iCamera = pGameScreen->GetActiveCamera();
				SharedData::getSingleton().iRoot = Ogre::Root::getSingletonPtr();
				SharedData::getSingleton().iWindow = renderWindow;
				SharedData::getSingleton().iActivate = true;
				SharedData::getSingleton().iGlobalActivate = true;

				if (pGameScreen->GetDefaultSceneManager()->hasSceneNode(directionalLightNodeName))
				{
					Ogre::SceneNode* sunLight = pGameScreen->GetDefaultSceneManager()->getSceneNode(directionalLightNodeName);
					SharedData::getSingleton().iMainLight = (Ogre::Light*)sunLight->getAttachedObject(0);
				}

				SharedData::getSingleton().iSystem->setActive(false);
				SharedData::getSingleton().iSystem->setSSAO(false);

				isInitialized = true;
			}
		}
	}

	void Destroy()
	{
		if (isInitialized)
		{
			delete SharedData::getSingletonPtr();
			delete deferredShadingSystem;
			isInitialized = false;
		}
	}

	void EnableDeferredShading(LightingManager::DeferredShadingState state)
	{
		assert(isInitialized);
		switch(state)
		{
		case EMISSIVE_ONLY:
			SharedData::getSingleton().iSystem->setMode(DeferredShadingSystem::DSM_SHOWEMISSIVE);
			break;
		case DIFFUSE_ONLY:
			SharedData::getSingleton().iSystem->setMode(DeferredShadingSystem::DSM_SHOWCOLOUR);
			break;
		case NORMALS_ONLY:
			SharedData::getSingleton().iSystem->setMode(DeferredShadingSystem::DSM_SHOWNORMALS);
			break;
		case DEPTH_AND_SPECULAR_ONLY:
			SharedData::getSingleton().iSystem->setMode(DeferredShadingSystem::DSM_SHOWDSP);
			break;
		case COMPOSITE:
		default:
			SharedData::getSingleton().iSystem->setMode(DeferredShadingSystem::DSM_SHOWLIT);
			break;
		}
		SharedData::getSingleton().iSystem->setActive(true);
	}

	void DisableDeferredShading()
	{
		assert(isInitialized);
		SharedData::getSingleton().iSystem->setActive(false);
	}

	void EnableSSAO()
	{
		assert(isInitialized);
		SharedData::getSingleton().iSystem->setSSAO(true);
	}

	void DisableSSAO()
	{
		assert(isInitialized);
		SharedData::getSingleton().iSystem->setSSAO(false);
	}

	bool GetDeferredShadingEnabled()
	{
		assert(isInitialized);
		return SharedData::getSingleton().iSystem->getActive();
	}

	bool GetSSAOEnabled()
	{
		assert(isInitialized);
		return SharedData::getSingleton().iSystem->getSSAO();
	}
}