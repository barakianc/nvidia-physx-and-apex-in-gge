#include "Ogre.h"
#include "SharedData.h"
#include "GameScreen.h"
#include "Engine.h"

namespace LightingManager
{
	enum DeferredShadingState
	{
		COMPOSITE,
		EMISSIVE_ONLY,
		DIFFUSE_ONLY,
		NORMALS_ONLY,
		DEPTH_AND_SPECULAR_ONLY,
	};

	/// Initializes the lighting manager.  This should be invoked once during the first Update() call in the GameScreen.
	/// Set the directionalLightNodeName input string to the name of the scene node that contains the directional light
	/// desired for use.
	void Initialize(const char* directionalLightNodeName);

	/// Destroys the lighting manager.  This should be invoked when the GameScreen is destroyed/closed.
	void Destroy();

	/// Enables deferred shading rendering.  The DeferredShadingState should be set to COMPOSITE for normal rendering, or it can
	/// be set to the other states to render specific G-buffer texture data.
	void EnableDeferredShading(DeferredShadingState state);

	/// Disables deferred shading rendering and returns to the normal forward rendering method.
	void DisableDeferredShading();

	/// Enables screen space ambient occlusion.
	void EnableSSAO();

	/// Disables screen space ambient occlusion.
	void DisableSSAO();

	/// Returns true if deferred shading is currently enabled, false otherwise.
	bool GetDeferredShadingEnabled();
	
	/// Returns true if screen space ambient occlusion is currently enabled, false otherwise.
	bool GetSSAOEnabled();
}