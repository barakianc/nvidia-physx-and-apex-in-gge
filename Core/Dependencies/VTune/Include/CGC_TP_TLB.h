

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0363 */
/* at Tue Jun 10 08:51:09 2008
 */
/* Compiler settings for D:\vtune5.0\tprofiler\src\tp_collector\CGC_TLB\CGC_TLB.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CGC_TP_TLB_h__
#define __CGC_TP_TLB_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICGCollector_FWD_DEFINED__
#define __ICGCollector_FWD_DEFINED__
typedef interface ICGCollector ICGCollector;
#endif 	/* __ICGCollector_FWD_DEFINED__ */


#ifndef __IStatusMsg_FWD_DEFINED__
#define __IStatusMsg_FWD_DEFINED__
typedef interface IStatusMsg IStatusMsg;
#endif 	/* __IStatusMsg_FWD_DEFINED__ */


#ifndef __IProgressBarCB_FWD_DEFINED__
#define __IProgressBarCB_FWD_DEFINED__
typedef interface IProgressBarCB IProgressBarCB;
#endif 	/* __IProgressBarCB_FWD_DEFINED__ */


#ifndef __IErrorMsgCB_FWD_DEFINED__
#define __IErrorMsgCB_FWD_DEFINED__
typedef interface IErrorMsgCB IErrorMsgCB;
#endif 	/* __IErrorMsgCB_FWD_DEFINED__ */


#ifndef __IBistroController_FWD_DEFINED__
#define __IBistroController_FWD_DEFINED__
typedef interface IBistroController IBistroController;
#endif 	/* __IBistroController_FWD_DEFINED__ */


#ifndef __IBistroController2_FWD_DEFINED__
#define __IBistroController2_FWD_DEFINED__
typedef interface IBistroController2 IBistroController2;
#endif 	/* __IBistroController2_FWD_DEFINED__ */


#ifndef __IBistroController3_FWD_DEFINED__
#define __IBistroController3_FWD_DEFINED__
typedef interface IBistroController3 IBistroController3;
#endif 	/* __IBistroController3_FWD_DEFINED__ */


#ifndef __IProblemDescriptor_FWD_DEFINED__
#define __IProblemDescriptor_FWD_DEFINED__
typedef interface IProblemDescriptor IProblemDescriptor;
#endif 	/* __IProblemDescriptor_FWD_DEFINED__ */


#ifndef __IStartStopConsole_FWD_DEFINED__
#define __IStartStopConsole_FWD_DEFINED__
typedef interface IStartStopConsole IStartStopConsole;
#endif 	/* __IStartStopConsole_FWD_DEFINED__ */


#ifndef __ICompletionCallBack_FWD_DEFINED__
#define __ICompletionCallBack_FWD_DEFINED__
typedef interface ICompletionCallBack ICompletionCallBack;
#endif 	/* __ICompletionCallBack_FWD_DEFINED__ */


#ifndef __IInstrumentationCallBack_FWD_DEFINED__
#define __IInstrumentationCallBack_FWD_DEFINED__
typedef interface IInstrumentationCallBack IInstrumentationCallBack;
#endif 	/* __IInstrumentationCallBack_FWD_DEFINED__ */


#ifndef __IProgressBarCallBack_FWD_DEFINED__
#define __IProgressBarCallBack_FWD_DEFINED__
typedef interface IProgressBarCallBack IProgressBarCallBack;
#endif 	/* __IProgressBarCallBack_FWD_DEFINED__ */


#ifndef __IInstrumentationStatusCallBack_FWD_DEFINED__
#define __IInstrumentationStatusCallBack_FWD_DEFINED__
typedef interface IInstrumentationStatusCallBack IInstrumentationStatusCallBack;
#endif 	/* __IInstrumentationStatusCallBack_FWD_DEFINED__ */


#ifndef __IBistroControllerInfoCallBack_FWD_DEFINED__
#define __IBistroControllerInfoCallBack_FWD_DEFINED__
typedef interface IBistroControllerInfoCallBack IBistroControllerInfoCallBack;
#endif 	/* __IBistroControllerInfoCallBack_FWD_DEFINED__ */


#ifndef __IBistroControllerErrorCallBack_FWD_DEFINED__
#define __IBistroControllerErrorCallBack_FWD_DEFINED__
typedef interface IBistroControllerErrorCallBack IBistroControllerErrorCallBack;
#endif 	/* __IBistroControllerErrorCallBack_FWD_DEFINED__ */


#ifndef __IConsoleVisualizeCallBack_FWD_DEFINED__
#define __IConsoleVisualizeCallBack_FWD_DEFINED__
typedef interface IConsoleVisualizeCallBack IConsoleVisualizeCallBack;
#endif 	/* __IConsoleVisualizeCallBack_FWD_DEFINED__ */


#ifndef __IFunctionSelection_FWD_DEFINED__
#define __IFunctionSelection_FWD_DEFINED__
typedef interface IFunctionSelection IFunctionSelection;
#endif 	/* __IFunctionSelection_FWD_DEFINED__ */


#ifndef __IFunctionData_FWD_DEFINED__
#define __IFunctionData_FWD_DEFINED__
typedef interface IFunctionData IFunctionData;
#endif 	/* __IFunctionData_FWD_DEFINED__ */


#ifndef __ICGType_FWD_DEFINED__
#define __ICGType_FWD_DEFINED__
typedef interface ICGType ICGType;
#endif 	/* __ICGType_FWD_DEFINED__ */


#ifndef __ICGExeController_FWD_DEFINED__
#define __ICGExeController_FWD_DEFINED__
typedef interface ICGExeController ICGExeController;
#endif 	/* __ICGExeController_FWD_DEFINED__ */


#ifndef __CGCollector_RLinuxNewTP_FWD_DEFINED__
#define __CGCollector_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class CGCollector_RLinuxNewTP CGCollector_RLinuxNewTP;
#else
typedef struct CGCollector_RLinuxNewTP CGCollector_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __CGCollector_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __CGCollector_TP32_FWD_DEFINED__
#define __CGCollector_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class CGCollector_TP32 CGCollector_TP32;
#else
typedef struct CGCollector_TP32 CGCollector_TP32;
#endif /* __cplusplus */

#endif 	/* __CGCollector_TP32_FWD_DEFINED__ */


#ifndef __ProgressBarCB_RLinuxNewTP_FWD_DEFINED__
#define __ProgressBarCB_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProgressBarCB_RLinuxNewTP ProgressBarCB_RLinuxNewTP;
#else
typedef struct ProgressBarCB_RLinuxNewTP ProgressBarCB_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __ProgressBarCB_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __ProgressBarCB_TP32_FWD_DEFINED__
#define __ProgressBarCB_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProgressBarCB_TP32 ProgressBarCB_TP32;
#else
typedef struct ProgressBarCB_TP32 ProgressBarCB_TP32;
#endif /* __cplusplus */

#endif 	/* __ProgressBarCB_TP32_FWD_DEFINED__ */


#ifndef __TPCollectorPropertyPageInstrumentation_RLinuxNewTP_FWD_DEFINED__
#define __TPCollectorPropertyPageInstrumentation_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class TPCollectorPropertyPageInstrumentation_RLinuxNewTP TPCollectorPropertyPageInstrumentation_RLinuxNewTP;
#else
typedef struct TPCollectorPropertyPageInstrumentation_RLinuxNewTP TPCollectorPropertyPageInstrumentation_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __TPCollectorPropertyPageInstrumentation_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __TPCollectorPropertyPageInstrumentation_TP32_FWD_DEFINED__
#define __TPCollectorPropertyPageInstrumentation_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class TPCollectorPropertyPageInstrumentation_TP32 TPCollectorPropertyPageInstrumentation_TP32;
#else
typedef struct TPCollectorPropertyPageInstrumentation_TP32 TPCollectorPropertyPageInstrumentation_TP32;
#endif /* __cplusplus */

#endif 	/* __TPCollectorPropertyPageInstrumentation_TP32_FWD_DEFINED__ */


#ifndef __StatusMsg_RLinuxNewTP_FWD_DEFINED__
#define __StatusMsg_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class StatusMsg_RLinuxNewTP StatusMsg_RLinuxNewTP;
#else
typedef struct StatusMsg_RLinuxNewTP StatusMsg_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __StatusMsg_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __StatusMsg_TP32_FWD_DEFINED__
#define __StatusMsg_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class StatusMsg_TP32 StatusMsg_TP32;
#else
typedef struct StatusMsg_TP32 StatusMsg_TP32;
#endif /* __cplusplus */

#endif 	/* __StatusMsg_TP32_FWD_DEFINED__ */


#ifndef __TPCollectorPropertyPageMiscellaneous_FWD_DEFINED__
#define __TPCollectorPropertyPageMiscellaneous_FWD_DEFINED__

#ifdef __cplusplus
typedef class TPCollectorPropertyPageMiscellaneous TPCollectorPropertyPageMiscellaneous;
#else
typedef struct TPCollectorPropertyPageMiscellaneous TPCollectorPropertyPageMiscellaneous;
#endif /* __cplusplus */

#endif 	/* __TPCollectorPropertyPageMiscellaneous_FWD_DEFINED__ */


#ifndef __TPCollectorPropertyPageCollection_FWD_DEFINED__
#define __TPCollectorPropertyPageCollection_FWD_DEFINED__

#ifdef __cplusplus
typedef class TPCollectorPropertyPageCollection TPCollectorPropertyPageCollection;
#else
typedef struct TPCollectorPropertyPageCollection TPCollectorPropertyPageCollection;
#endif /* __cplusplus */

#endif 	/* __TPCollectorPropertyPageCollection_FWD_DEFINED__ */


#ifndef __TPCollectorPropertyPageFilters_FWD_DEFINED__
#define __TPCollectorPropertyPageFilters_FWD_DEFINED__

#ifdef __cplusplus
typedef class TPCollectorPropertyPageFilters TPCollectorPropertyPageFilters;
#else
typedef struct TPCollectorPropertyPageFilters TPCollectorPropertyPageFilters;
#endif /* __cplusplus */

#endif 	/* __TPCollectorPropertyPageFilters_FWD_DEFINED__ */


#ifndef __ErrorMsgCB_RLinuxNewTP_FWD_DEFINED__
#define __ErrorMsgCB_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class ErrorMsgCB_RLinuxNewTP ErrorMsgCB_RLinuxNewTP;
#else
typedef struct ErrorMsgCB_RLinuxNewTP ErrorMsgCB_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __ErrorMsgCB_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __ErrorMsgCB_TP32_FWD_DEFINED__
#define __ErrorMsgCB_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class ErrorMsgCB_TP32 ErrorMsgCB_TP32;
#else
typedef struct ErrorMsgCB_TP32 ErrorMsgCB_TP32;
#endif /* __cplusplus */

#endif 	/* __ErrorMsgCB_TP32_FWD_DEFINED__ */


#ifndef __Controller_TP32_FWD_DEFINED__
#define __Controller_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class Controller_TP32 Controller_TP32;
#else
typedef struct Controller_TP32 Controller_TP32;
#endif /* __cplusplus */

#endif 	/* __Controller_TP32_FWD_DEFINED__ */


#ifndef __Controller_RLinuxNewTP_FWD_DEFINED__
#define __Controller_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class Controller_RLinuxNewTP Controller_RLinuxNewTP;
#else
typedef struct Controller_RLinuxNewTP Controller_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __Controller_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __Controller_TP_emt64_FWD_DEFINED__
#define __Controller_TP_emt64_FWD_DEFINED__

#ifdef __cplusplus
typedef class Controller_TP_emt64 Controller_TP_emt64;
#else
typedef struct Controller_TP_emt64 Controller_TP_emt64;
#endif /* __cplusplus */

#endif 	/* __Controller_TP_emt64_FWD_DEFINED__ */


#ifndef __ProblemDescriptor_TP32_FWD_DEFINED__
#define __ProblemDescriptor_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProblemDescriptor_TP32 ProblemDescriptor_TP32;
#else
typedef struct ProblemDescriptor_TP32 ProblemDescriptor_TP32;
#endif /* __cplusplus */

#endif 	/* __ProblemDescriptor_TP32_FWD_DEFINED__ */


#ifndef __ProblemDescriptor_RLinuxNewTP_FWD_DEFINED__
#define __ProblemDescriptor_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProblemDescriptor_RLinuxNewTP ProblemDescriptor_RLinuxNewTP;
#else
typedef struct ProblemDescriptor_RLinuxNewTP ProblemDescriptor_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __ProblemDescriptor_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __ProblemDescriptor_TP_emt64_FWD_DEFINED__
#define __ProblemDescriptor_TP_emt64_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProblemDescriptor_TP_emt64 ProblemDescriptor_TP_emt64;
#else
typedef struct ProblemDescriptor_TP_emt64 ProblemDescriptor_TP_emt64;
#endif /* __cplusplus */

#endif 	/* __ProblemDescriptor_TP_emt64_FWD_DEFINED__ */


#ifndef __StartStopConsole_TP32_FWD_DEFINED__
#define __StartStopConsole_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class StartStopConsole_TP32 StartStopConsole_TP32;
#else
typedef struct StartStopConsole_TP32 StartStopConsole_TP32;
#endif /* __cplusplus */

#endif 	/* __StartStopConsole_TP32_FWD_DEFINED__ */


#ifndef __StartStopConsole_TP_emt64_FWD_DEFINED__
#define __StartStopConsole_TP_emt64_FWD_DEFINED__

#ifdef __cplusplus
typedef class StartStopConsole_TP_emt64 StartStopConsole_TP_emt64;
#else
typedef struct StartStopConsole_TP_emt64 StartStopConsole_TP_emt64;
#endif /* __cplusplus */

#endif 	/* __StartStopConsole_TP_emt64_FWD_DEFINED__ */


#ifndef __StartStopConsole_RLinuxNewTP_FWD_DEFINED__
#define __StartStopConsole_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class StartStopConsole_RLinuxNewTP StartStopConsole_RLinuxNewTP;
#else
typedef struct StartStopConsole_RLinuxNewTP StartStopConsole_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __StartStopConsole_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __FunctionSelection_TP32_FWD_DEFINED__
#define __FunctionSelection_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class FunctionSelection_TP32 FunctionSelection_TP32;
#else
typedef struct FunctionSelection_TP32 FunctionSelection_TP32;
#endif /* __cplusplus */

#endif 	/* __FunctionSelection_TP32_FWD_DEFINED__ */


#ifndef __FunctionSelection_TP_emt64_FWD_DEFINED__
#define __FunctionSelection_TP_emt64_FWD_DEFINED__

#ifdef __cplusplus
typedef class FunctionSelection_TP_emt64 FunctionSelection_TP_emt64;
#else
typedef struct FunctionSelection_TP_emt64 FunctionSelection_TP_emt64;
#endif /* __cplusplus */

#endif 	/* __FunctionSelection_TP_emt64_FWD_DEFINED__ */


#ifndef __FunctionData_TP32_FWD_DEFINED__
#define __FunctionData_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class FunctionData_TP32 FunctionData_TP32;
#else
typedef struct FunctionData_TP32 FunctionData_TP32;
#endif /* __cplusplus */

#endif 	/* __FunctionData_TP32_FWD_DEFINED__ */


#ifndef __FunctionData_TP_emt64_FWD_DEFINED__
#define __FunctionData_TP_emt64_FWD_DEFINED__

#ifdef __cplusplus
typedef class FunctionData_TP_emt64 FunctionData_TP_emt64;
#else
typedef struct FunctionData_TP_emt64 FunctionData_TP_emt64;
#endif /* __cplusplus */

#endif 	/* __FunctionData_TP_emt64_FWD_DEFINED__ */


#ifndef __FunctionSelection_RLinuxNewTP_FWD_DEFINED__
#define __FunctionSelection_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class FunctionSelection_RLinuxNewTP FunctionSelection_RLinuxNewTP;
#else
typedef struct FunctionSelection_RLinuxNewTP FunctionSelection_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __FunctionSelection_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __FunctionData_RLinuxNewTP_FWD_DEFINED__
#define __FunctionData_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class FunctionData_RLinuxNewTP FunctionData_RLinuxNewTP;
#else
typedef struct FunctionData_RLinuxNewTP FunctionData_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __FunctionData_RLinuxNewTP_FWD_DEFINED__ */


#ifndef __ICGType_FWD_DEFINED__
#define __ICGType_FWD_DEFINED__
typedef interface ICGType ICGType;
#endif 	/* __ICGType_FWD_DEFINED__ */


#ifndef __CGExeController_TP32_FWD_DEFINED__
#define __CGExeController_TP32_FWD_DEFINED__

#ifdef __cplusplus
typedef class CGExeController_TP32 CGExeController_TP32;
#else
typedef struct CGExeController_TP32 CGExeController_TP32;
#endif /* __cplusplus */

#endif 	/* __CGExeController_TP32_FWD_DEFINED__ */


#ifndef __CGExeController_TP_emt64_FWD_DEFINED__
#define __CGExeController_TP_emt64_FWD_DEFINED__

#ifdef __cplusplus
typedef class CGExeController_TP_emt64 CGExeController_TP_emt64;
#else
typedef struct CGExeController_TP_emt64 CGExeController_TP_emt64;
#endif /* __cplusplus */

#endif 	/* __CGExeController_TP_emt64_FWD_DEFINED__ */


#ifndef __CGExeController_RLinuxNewTP_FWD_DEFINED__
#define __CGExeController_RLinuxNewTP_FWD_DEFINED__

#ifdef __cplusplus
typedef class CGExeController_RLinuxNewTP CGExeController_RLinuxNewTP;
#else
typedef struct CGExeController_RLinuxNewTP CGExeController_RLinuxNewTP;
#endif /* __cplusplus */

#endif 	/* __CGExeController_RLinuxNewTP_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_CGC_TLB_0000 */
/* [local] */ 

typedef 
enum enumSHOWRESULT
    {	SHOWERR_NONE	= 0,
	SHOWERR_CONT	= SHOWERR_NONE + 1,
	SHOWERR_HALT	= SHOWERR_CONT + 1
    } 	enumSHOWRESULT;

typedef 
enum enumVTRE_SEVERITY_CODE
    {	eVTRE_SEVERITY_FATAL	= 0,
	eVTRE_SEVERITY_ERROR	= 1,
	eVTRE_SEVERITY_WARNING	= 2,
	eVTRE_SEVERITY_INFO	= 3
    } 	enumVTRE_SEVERITY_CODE;

typedef 
enum DISPID_s
    {	DISPID_BISTRO_DIR	= 1,
	DISPID_INIT_PB	= DISPID_BISTRO_DIR + 1,
	DISPID_DONE_PB	= DISPID_INIT_PB + 1,
	DISPID_MODULE_PB	= DISPID_DONE_PB + 1,
	DISPID_INSTR_PB	= DISPID_MODULE_PB + 1,
	DISPID_INIT_STATUS	= DISPID_INSTR_PB + 1,
	DISPID_DONE_STATUS	= DISPID_INIT_STATUS + 1,
	DISPID_STATUS	= DISPID_DONE_STATUS + 1,
	DISPID_CREATE_EMPTY_PROJ	= DISPID_STATUS + 1,
	DISPID_SHOW_ERRORS	= DISPID_CREATE_EMPTY_PROJ + 1,
	DISPID_SHOW_MESSAGE	= DISPID_SHOW_ERRORS + 1,
	DISPID_SET_RESULTS_READY	= DISPID_SHOW_MESSAGE + 1,
	DISPID_EXE_DIR	= DISPID_SET_RESULTS_READY + 1,
	DISPID_SHOW_RESTORE_PROMPT	= DISPID_EXE_DIR + 1,
	DISPID_SHOW_MSGBOX	= DISPID_SHOW_RESTORE_PROMPT + 1,
	DISPID_APP_PAUSED	= DISPID_SHOW_MSGBOX + 1,
	DISPID_APP_RESUMED	= DISPID_APP_PAUSED + 1,
	DISPID_CALL_FIRE_DONE	= DISPID_APP_RESUMED + 1,
	DISPID_BINTYPE	= DISPID_CALL_FIRE_DONE + 1,
	DISPID_MSG_RCV	= DISPID_BINTYPE + 1,
	DISPID_IN_PROPERTY_PAGE	= DISPID_MSG_RCV + 1,
	DISPID_WRITE_PROP_FILE	= DISPID_IN_PROPERTY_PAGE + 1,
	DISPID_READ_PROP_FILE	= DISPID_WRITE_PROP_FILE + 1,
	DISPID_TP_FULLPATH	= DISPID_READ_PROP_FILE + 1,
	DISPID_TP_CALLSITES_TRACKED	= DISPID_TP_FULLPATH + 1,
	DISPID_TP_CALLSITES_DEPTH	= DISPID_TP_CALLSITES_TRACKED + 1,
	DISPID_TP_BCALLSITES_DEPTH	= DISPID_TP_CALLSITES_DEPTH + 1,
	DISPID_TP_ALL_EVENTS_THRESHOLD	= DISPID_TP_BCALLSITES_DEPTH + 1,
	DISPID_TP_IS_TRACK_THREAD_CPATH_TRANSITIONS_TO	= DISPID_TP_ALL_EVENTS_THRESHOLD + 1,
	DISPID_TP_TRACK_MSG_OBJS	= DISPID_TP_IS_TRACK_THREAD_CPATH_TRANSITIONS_TO + 1,
	DISPID_TP_WRAP_MSGS	= DISPID_TP_TRACK_MSG_OBJS + 1,
	DISPID_TP_TRACK_MSG_TYPE	= DISPID_TP_WRAP_MSGS + 1,
	DISPID_TP_UNCONTENDED_CS_THRESHOLD_NANOSECS	= DISPID_TP_TRACK_MSG_TYPE + 1,
	DISPID_TP_BLOCK_EVENTS_RECORD_THRESHOLD	= DISPID_TP_UNCONTENDED_CS_THRESHOLD_NANOSECS + 1,
	DISPID_TP_ADAPT_FILTER	= DISPID_TP_BLOCK_EVENTS_RECORD_THRESHOLD + 1,
	DISPID_TP_ADAPT_FILTER_WORST	= DISPID_TP_ADAPT_FILTER + 1,
	DISPID_TP_ADAPT_FILTER_ACCEPTABLE	= DISPID_TP_ADAPT_FILTER_WORST + 1,
	DISPID_TP_ADAPT_FILTER_MIN_DATA	= DISPID_TP_ADAPT_FILTER_ACCEPTABLE + 1,
	DISPID_TP_COLLECTION_MODE	= DISPID_TP_ADAPT_FILTER_MIN_DATA + 1,
	DISPID_TP_APITRACE_MODE	= DISPID_TP_COLLECTION_MODE + 1,
	DISPID_TP_APITRACE_IGNORE_CRITICAL_SECTIONS	= DISPID_TP_APITRACE_MODE + 1,
	DISPID_GET_PROP_SET	= DISPID_TP_APITRACE_IGNORE_CRITICAL_SECTIONS + 1,
	DISPID_LOAD_PROP_SET	= DISPID_GET_PROP_SET + 1,
	DISPID_INITIALIZE	= DISPID_LOAD_PROP_SET + 1,
	DISPID_NOTIFY_STARTED_RUNNING	= DISPID_INITIALIZE + 1,
	DISPID_STARTED_RUNNING	= DISPID_NOTIFY_STARTED_RUNNING + 1,
	DISPID_WORKER_PROCS	= DISPID_STARTED_RUNNING + 1,
	DISPID_TP_LAST	= DISPID_WORKER_PROCS + 1
    } 	DISPID_s;



extern RPC_IF_HANDLE __MIDL_itf_CGC_TLB_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_CGC_TLB_0000_v0_0_s_ifspec;

#ifndef __ICGCollector_INTERFACE_DEFINED__
#define __ICGCollector_INTERFACE_DEFINED__

/* interface ICGCollector */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICGCollector;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("59ABF6EE-7078-4ed0-9BCF-757422ADFEDE")
    ICGCollector : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BistroDir( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BistroDir( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InitPB( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DonePB( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ModulePB( 
            /* [in] */ long Pct,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR InstrType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InstrPB( 
            /* [in] */ long Pct,
            /* [in] */ BSTR msg) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InitStatus( 
            BSTR msg) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DoneStatus( 
            BSTR msg) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Status( 
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR NewName,
            /* [in] */ int InstrType,
            /* [in] */ long StatusCode,
            /* [in] */ VARIANT_BOOL bIgnoreCase) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateEmptyProject( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowErrors( 
            /* [in] */ VARIANT var,
            /* [in] */ VARIANT_BOOL bShowPrompt,
            /* [in] */ VARIANT_BOOL bIgnoreCase,
            /* [retval][out] */ enumSHOWRESULT *pErrors) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowMessage( 
            /* [in] */ BSTR bstrTxt) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetResultsReady( 
            /* [in] */ BSTR bstrPrfPath) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExeDir( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExeDir( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowRestorePrompt( 
            /* [in] */ VARIANT_BOOL bLocked) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowMessageBox( 
            /* [in] */ BSTR msg,
            /* [in] */ enumVTRE_SEVERITY_CODE MessageSeverity,
            /* [in] */ long flags,
            /* [retval][out] */ long *nButton) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AppPaused( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AppResumed( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallFireDone( 
            long lCancelled) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_iBinType( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_iBinType( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE MessageReceived( 
            /* [in] */ LONG MsgID) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_bInPropertyPage( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_bInPropertyPage( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WritePropertyFile( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReadPropertyFile( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TPFullPath( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TPFullPath( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_bCallsitesTracked( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_bCallsitesTracked( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_bCallsitesDepth( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_bCallsitesDepth( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallsitesDepth( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CallsitesDepth( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AllEventsThreshold( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AllEventsThreshold( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_bTrackMsgObjs( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_bTrackMsgObjs( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrackMsgType( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrackMsgType( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UncontendedCSThreshold_nanosecs( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UncontendedCSThreshold_nanosecs( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BlockEventsRecordThreshold( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BlockEventsRecordThreshold( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AdaptFilter( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AdaptFilter( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CollectionMode( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CollectionMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ApiTraceMode( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ApiTraceMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ApiTraceIgnoreCriticalSections( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ApiTraceIgnoreCriticalSections( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPropertySet( 
            long *pPropSet) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE LoadPropertySet( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Initialize( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE NotifyStartedRunning( 
            /* [in] */ BOOL canRunRealtime) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartedRunning( 
            /* [in] */ BOOL canRunRealtime) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ASPNET_Worker_Procs( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ASPNET_Worker_Procs( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICGCollectorVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICGCollector * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICGCollector * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICGCollector * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICGCollector * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICGCollector * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICGCollector * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICGCollector * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroDir )( 
            ICGCollector * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroDir )( 
            ICGCollector * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitPB )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DonePB )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModulePB )( 
            ICGCollector * This,
            /* [in] */ long Pct,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR InstrType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InstrPB )( 
            ICGCollector * This,
            /* [in] */ long Pct,
            /* [in] */ BSTR msg);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitStatus )( 
            ICGCollector * This,
            BSTR msg);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DoneStatus )( 
            ICGCollector * This,
            BSTR msg);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Status )( 
            ICGCollector * This,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR NewName,
            /* [in] */ int InstrType,
            /* [in] */ long StatusCode,
            /* [in] */ VARIANT_BOOL bIgnoreCase);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEmptyProject )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowErrors )( 
            ICGCollector * This,
            /* [in] */ VARIANT var,
            /* [in] */ VARIANT_BOOL bShowPrompt,
            /* [in] */ VARIANT_BOOL bIgnoreCase,
            /* [retval][out] */ enumSHOWRESULT *pErrors);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowMessage )( 
            ICGCollector * This,
            /* [in] */ BSTR bstrTxt);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetResultsReady )( 
            ICGCollector * This,
            /* [in] */ BSTR bstrPrfPath);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExeDir )( 
            ICGCollector * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExeDir )( 
            ICGCollector * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowRestorePrompt )( 
            ICGCollector * This,
            /* [in] */ VARIANT_BOOL bLocked);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowMessageBox )( 
            ICGCollector * This,
            /* [in] */ BSTR msg,
            /* [in] */ enumVTRE_SEVERITY_CODE MessageSeverity,
            /* [in] */ long flags,
            /* [retval][out] */ long *nButton);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AppPaused )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AppResumed )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallFireDone )( 
            ICGCollector * This,
            long lCancelled);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_iBinType )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_iBinType )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *MessageReceived )( 
            ICGCollector * This,
            /* [in] */ LONG MsgID);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_bInPropertyPage )( 
            ICGCollector * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_bInPropertyPage )( 
            ICGCollector * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WritePropertyFile )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReadPropertyFile )( 
            ICGCollector * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TPFullPath )( 
            ICGCollector * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TPFullPath )( 
            ICGCollector * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_bCallsitesTracked )( 
            ICGCollector * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_bCallsitesTracked )( 
            ICGCollector * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_bCallsitesDepth )( 
            ICGCollector * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_bCallsitesDepth )( 
            ICGCollector * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallsitesDepth )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallsitesDepth )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllEventsThreshold )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllEventsThreshold )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_bTrackMsgObjs )( 
            ICGCollector * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_bTrackMsgObjs )( 
            ICGCollector * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TrackMsgType )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TrackMsgType )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UncontendedCSThreshold_nanosecs )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UncontendedCSThreshold_nanosecs )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BlockEventsRecordThreshold )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BlockEventsRecordThreshold )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AdaptFilter )( 
            ICGCollector * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AdaptFilter )( 
            ICGCollector * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CollectionMode )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CollectionMode )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApiTraceMode )( 
            ICGCollector * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApiTraceMode )( 
            ICGCollector * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApiTraceIgnoreCriticalSections )( 
            ICGCollector * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApiTraceIgnoreCriticalSections )( 
            ICGCollector * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPropertySet )( 
            ICGCollector * This,
            long *pPropSet);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *LoadPropertySet )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Initialize )( 
            ICGCollector * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NotifyStartedRunning )( 
            ICGCollector * This,
            /* [in] */ BOOL canRunRealtime);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartedRunning )( 
            ICGCollector * This,
            /* [in] */ BOOL canRunRealtime);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ASPNET_Worker_Procs )( 
            ICGCollector * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ASPNET_Worker_Procs )( 
            ICGCollector * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ICGCollectorVtbl;

    interface ICGCollector
    {
        CONST_VTBL struct ICGCollectorVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICGCollector_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICGCollector_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICGCollector_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICGCollector_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICGCollector_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICGCollector_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICGCollector_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICGCollector_get_BistroDir(This,pVal)	\
    (This)->lpVtbl -> get_BistroDir(This,pVal)

#define ICGCollector_put_BistroDir(This,newVal)	\
    (This)->lpVtbl -> put_BistroDir(This,newVal)

#define ICGCollector_InitPB(This)	\
    (This)->lpVtbl -> InitPB(This)

#define ICGCollector_DonePB(This)	\
    (This)->lpVtbl -> DonePB(This)

#define ICGCollector_ModulePB(This,Pct,OrigName,InstrType)	\
    (This)->lpVtbl -> ModulePB(This,Pct,OrigName,InstrType)

#define ICGCollector_InstrPB(This,Pct,msg)	\
    (This)->lpVtbl -> InstrPB(This,Pct,msg)

#define ICGCollector_InitStatus(This,msg)	\
    (This)->lpVtbl -> InitStatus(This,msg)

#define ICGCollector_DoneStatus(This,msg)	\
    (This)->lpVtbl -> DoneStatus(This,msg)

#define ICGCollector_Status(This,OrigName,NewName,InstrType,StatusCode,bIgnoreCase)	\
    (This)->lpVtbl -> Status(This,OrigName,NewName,InstrType,StatusCode,bIgnoreCase)

#define ICGCollector_CreateEmptyProject(This)	\
    (This)->lpVtbl -> CreateEmptyProject(This)

#define ICGCollector_ShowErrors(This,var,bShowPrompt,bIgnoreCase,pErrors)	\
    (This)->lpVtbl -> ShowErrors(This,var,bShowPrompt,bIgnoreCase,pErrors)

#define ICGCollector_ShowMessage(This,bstrTxt)	\
    (This)->lpVtbl -> ShowMessage(This,bstrTxt)

#define ICGCollector_SetResultsReady(This,bstrPrfPath)	\
    (This)->lpVtbl -> SetResultsReady(This,bstrPrfPath)

#define ICGCollector_get_ExeDir(This,pVal)	\
    (This)->lpVtbl -> get_ExeDir(This,pVal)

#define ICGCollector_put_ExeDir(This,newVal)	\
    (This)->lpVtbl -> put_ExeDir(This,newVal)

#define ICGCollector_ShowRestorePrompt(This,bLocked)	\
    (This)->lpVtbl -> ShowRestorePrompt(This,bLocked)

#define ICGCollector_ShowMessageBox(This,msg,MessageSeverity,flags,nButton)	\
    (This)->lpVtbl -> ShowMessageBox(This,msg,MessageSeverity,flags,nButton)

#define ICGCollector_AppPaused(This)	\
    (This)->lpVtbl -> AppPaused(This)

#define ICGCollector_AppResumed(This)	\
    (This)->lpVtbl -> AppResumed(This)

#define ICGCollector_CallFireDone(This,lCancelled)	\
    (This)->lpVtbl -> CallFireDone(This,lCancelled)

#define ICGCollector_get_iBinType(This,pVal)	\
    (This)->lpVtbl -> get_iBinType(This,pVal)

#define ICGCollector_put_iBinType(This,newVal)	\
    (This)->lpVtbl -> put_iBinType(This,newVal)

#define ICGCollector_MessageReceived(This,MsgID)	\
    (This)->lpVtbl -> MessageReceived(This,MsgID)

#define ICGCollector_get_bInPropertyPage(This,pVal)	\
    (This)->lpVtbl -> get_bInPropertyPage(This,pVal)

#define ICGCollector_put_bInPropertyPage(This,newVal)	\
    (This)->lpVtbl -> put_bInPropertyPage(This,newVal)

#define ICGCollector_WritePropertyFile(This)	\
    (This)->lpVtbl -> WritePropertyFile(This)

#define ICGCollector_ReadPropertyFile(This)	\
    (This)->lpVtbl -> ReadPropertyFile(This)

#define ICGCollector_get_TPFullPath(This,pVal)	\
    (This)->lpVtbl -> get_TPFullPath(This,pVal)

#define ICGCollector_put_TPFullPath(This,newVal)	\
    (This)->lpVtbl -> put_TPFullPath(This,newVal)

#define ICGCollector_get_bCallsitesTracked(This,pVal)	\
    (This)->lpVtbl -> get_bCallsitesTracked(This,pVal)

#define ICGCollector_put_bCallsitesTracked(This,newVal)	\
    (This)->lpVtbl -> put_bCallsitesTracked(This,newVal)

#define ICGCollector_get_bCallsitesDepth(This,pVal)	\
    (This)->lpVtbl -> get_bCallsitesDepth(This,pVal)

#define ICGCollector_put_bCallsitesDepth(This,newVal)	\
    (This)->lpVtbl -> put_bCallsitesDepth(This,newVal)

#define ICGCollector_get_CallsitesDepth(This,pVal)	\
    (This)->lpVtbl -> get_CallsitesDepth(This,pVal)

#define ICGCollector_put_CallsitesDepth(This,newVal)	\
    (This)->lpVtbl -> put_CallsitesDepth(This,newVal)

#define ICGCollector_get_AllEventsThreshold(This,pVal)	\
    (This)->lpVtbl -> get_AllEventsThreshold(This,pVal)

#define ICGCollector_put_AllEventsThreshold(This,newVal)	\
    (This)->lpVtbl -> put_AllEventsThreshold(This,newVal)

#define ICGCollector_get_bTrackMsgObjs(This,pVal)	\
    (This)->lpVtbl -> get_bTrackMsgObjs(This,pVal)

#define ICGCollector_put_bTrackMsgObjs(This,newVal)	\
    (This)->lpVtbl -> put_bTrackMsgObjs(This,newVal)

#define ICGCollector_get_TrackMsgType(This,pVal)	\
    (This)->lpVtbl -> get_TrackMsgType(This,pVal)

#define ICGCollector_put_TrackMsgType(This,newVal)	\
    (This)->lpVtbl -> put_TrackMsgType(This,newVal)

#define ICGCollector_get_UncontendedCSThreshold_nanosecs(This,pVal)	\
    (This)->lpVtbl -> get_UncontendedCSThreshold_nanosecs(This,pVal)

#define ICGCollector_put_UncontendedCSThreshold_nanosecs(This,newVal)	\
    (This)->lpVtbl -> put_UncontendedCSThreshold_nanosecs(This,newVal)

#define ICGCollector_get_BlockEventsRecordThreshold(This,pVal)	\
    (This)->lpVtbl -> get_BlockEventsRecordThreshold(This,pVal)

#define ICGCollector_put_BlockEventsRecordThreshold(This,newVal)	\
    (This)->lpVtbl -> put_BlockEventsRecordThreshold(This,newVal)

#define ICGCollector_get_AdaptFilter(This,pVal)	\
    (This)->lpVtbl -> get_AdaptFilter(This,pVal)

#define ICGCollector_put_AdaptFilter(This,newVal)	\
    (This)->lpVtbl -> put_AdaptFilter(This,newVal)

#define ICGCollector_get_CollectionMode(This,pVal)	\
    (This)->lpVtbl -> get_CollectionMode(This,pVal)

#define ICGCollector_put_CollectionMode(This,newVal)	\
    (This)->lpVtbl -> put_CollectionMode(This,newVal)

#define ICGCollector_get_ApiTraceMode(This,pVal)	\
    (This)->lpVtbl -> get_ApiTraceMode(This,pVal)

#define ICGCollector_put_ApiTraceMode(This,newVal)	\
    (This)->lpVtbl -> put_ApiTraceMode(This,newVal)

#define ICGCollector_get_ApiTraceIgnoreCriticalSections(This,pVal)	\
    (This)->lpVtbl -> get_ApiTraceIgnoreCriticalSections(This,pVal)

#define ICGCollector_put_ApiTraceIgnoreCriticalSections(This,newVal)	\
    (This)->lpVtbl -> put_ApiTraceIgnoreCriticalSections(This,newVal)

#define ICGCollector_GetPropertySet(This,pPropSet)	\
    (This)->lpVtbl -> GetPropertySet(This,pPropSet)

#define ICGCollector_LoadPropertySet(This)	\
    (This)->lpVtbl -> LoadPropertySet(This)

#define ICGCollector_Initialize(This)	\
    (This)->lpVtbl -> Initialize(This)

#define ICGCollector_NotifyStartedRunning(This,canRunRealtime)	\
    (This)->lpVtbl -> NotifyStartedRunning(This,canRunRealtime)

#define ICGCollector_StartedRunning(This,canRunRealtime)	\
    (This)->lpVtbl -> StartedRunning(This,canRunRealtime)

#define ICGCollector_get_ASPNET_Worker_Procs(This,pVal)	\
    (This)->lpVtbl -> get_ASPNET_Worker_Procs(This,pVal)

#define ICGCollector_put_ASPNET_Worker_Procs(This,newVal)	\
    (This)->lpVtbl -> put_ASPNET_Worker_Procs(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_BistroDir_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICGCollector_get_BistroDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_BistroDir_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ICGCollector_put_BistroDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_InitPB_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_InitPB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_DonePB_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_DonePB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_ModulePB_Proxy( 
    ICGCollector * This,
    /* [in] */ long Pct,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR InstrType);


void __RPC_STUB ICGCollector_ModulePB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_InstrPB_Proxy( 
    ICGCollector * This,
    /* [in] */ long Pct,
    /* [in] */ BSTR msg);


void __RPC_STUB ICGCollector_InstrPB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_InitStatus_Proxy( 
    ICGCollector * This,
    BSTR msg);


void __RPC_STUB ICGCollector_InitStatus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_DoneStatus_Proxy( 
    ICGCollector * This,
    BSTR msg);


void __RPC_STUB ICGCollector_DoneStatus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_Status_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR NewName,
    /* [in] */ int InstrType,
    /* [in] */ long StatusCode,
    /* [in] */ VARIANT_BOOL bIgnoreCase);


void __RPC_STUB ICGCollector_Status_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_CreateEmptyProject_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_CreateEmptyProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_ShowErrors_Proxy( 
    ICGCollector * This,
    /* [in] */ VARIANT var,
    /* [in] */ VARIANT_BOOL bShowPrompt,
    /* [in] */ VARIANT_BOOL bIgnoreCase,
    /* [retval][out] */ enumSHOWRESULT *pErrors);


void __RPC_STUB ICGCollector_ShowErrors_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_ShowMessage_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR bstrTxt);


void __RPC_STUB ICGCollector_ShowMessage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_SetResultsReady_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR bstrPrfPath);


void __RPC_STUB ICGCollector_SetResultsReady_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_ExeDir_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICGCollector_get_ExeDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_ExeDir_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ICGCollector_put_ExeDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_ShowRestorePrompt_Proxy( 
    ICGCollector * This,
    /* [in] */ VARIANT_BOOL bLocked);


void __RPC_STUB ICGCollector_ShowRestorePrompt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_ShowMessageBox_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR msg,
    /* [in] */ enumVTRE_SEVERITY_CODE MessageSeverity,
    /* [in] */ long flags,
    /* [retval][out] */ long *nButton);


void __RPC_STUB ICGCollector_ShowMessageBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_AppPaused_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_AppPaused_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_AppResumed_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_AppResumed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_CallFireDone_Proxy( 
    ICGCollector * This,
    long lCancelled);


void __RPC_STUB ICGCollector_CallFireDone_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_iBinType_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_iBinType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_iBinType_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_iBinType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_MessageReceived_Proxy( 
    ICGCollector * This,
    /* [in] */ LONG MsgID);


void __RPC_STUB ICGCollector_MessageReceived_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_bInPropertyPage_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICGCollector_get_bInPropertyPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_bInPropertyPage_Proxy( 
    ICGCollector * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ICGCollector_put_bInPropertyPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_WritePropertyFile_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_WritePropertyFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_ReadPropertyFile_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_ReadPropertyFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_TPFullPath_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICGCollector_get_TPFullPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_TPFullPath_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ICGCollector_put_TPFullPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_bCallsitesTracked_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ICGCollector_get_bCallsitesTracked_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_bCallsitesTracked_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ICGCollector_put_bCallsitesTracked_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_bCallsitesDepth_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ICGCollector_get_bCallsitesDepth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_bCallsitesDepth_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ICGCollector_put_bCallsitesDepth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_CallsitesDepth_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_CallsitesDepth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_CallsitesDepth_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_CallsitesDepth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_AllEventsThreshold_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_AllEventsThreshold_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_AllEventsThreshold_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_AllEventsThreshold_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_bTrackMsgObjs_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ICGCollector_get_bTrackMsgObjs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_bTrackMsgObjs_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ICGCollector_put_bTrackMsgObjs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_TrackMsgType_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_TrackMsgType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_TrackMsgType_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_TrackMsgType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_UncontendedCSThreshold_nanosecs_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_UncontendedCSThreshold_nanosecs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_UncontendedCSThreshold_nanosecs_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_UncontendedCSThreshold_nanosecs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_BlockEventsRecordThreshold_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_BlockEventsRecordThreshold_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_BlockEventsRecordThreshold_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_BlockEventsRecordThreshold_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_AdaptFilter_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ICGCollector_get_AdaptFilter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_AdaptFilter_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ICGCollector_put_AdaptFilter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_CollectionMode_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_CollectionMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_CollectionMode_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_CollectionMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_ApiTraceMode_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ICGCollector_get_ApiTraceMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_ApiTraceMode_Proxy( 
    ICGCollector * This,
    /* [in] */ long newVal);


void __RPC_STUB ICGCollector_put_ApiTraceMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_ApiTraceIgnoreCriticalSections_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ICGCollector_get_ApiTraceIgnoreCriticalSections_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_ApiTraceIgnoreCriticalSections_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ICGCollector_put_ApiTraceIgnoreCriticalSections_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_GetPropertySet_Proxy( 
    ICGCollector * This,
    long *pPropSet);


void __RPC_STUB ICGCollector_GetPropertySet_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_LoadPropertySet_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_LoadPropertySet_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_Initialize_Proxy( 
    ICGCollector * This);


void __RPC_STUB ICGCollector_Initialize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_NotifyStartedRunning_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL canRunRealtime);


void __RPC_STUB ICGCollector_NotifyStartedRunning_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGCollector_StartedRunning_Proxy( 
    ICGCollector * This,
    /* [in] */ BOOL canRunRealtime);


void __RPC_STUB ICGCollector_StartedRunning_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGCollector_get_ASPNET_Worker_Procs_Proxy( 
    ICGCollector * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICGCollector_get_ASPNET_Worker_Procs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGCollector_put_ASPNET_Worker_Procs_Proxy( 
    ICGCollector * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ICGCollector_put_ASPNET_Worker_Procs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICGCollector_INTERFACE_DEFINED__ */


#ifndef __IStatusMsg_INTERFACE_DEFINED__
#define __IStatusMsg_INTERFACE_DEFINED__

/* interface IStatusMsg */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStatusMsg;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("760D39ED-395D-473e-9796-7E986021C44B")
    IStatusMsg : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IStatusMsgVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStatusMsg * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStatusMsg * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStatusMsg * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStatusMsg * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStatusMsg * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStatusMsg * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStatusMsg * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IStatusMsgVtbl;

    interface IStatusMsg
    {
        CONST_VTBL struct IStatusMsgVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStatusMsg_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IStatusMsg_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IStatusMsg_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IStatusMsg_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IStatusMsg_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IStatusMsg_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IStatusMsg_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IStatusMsg_INTERFACE_DEFINED__ */


#ifndef __IProgressBarCB_INTERFACE_DEFINED__
#define __IProgressBarCB_INTERFACE_DEFINED__

/* interface IProgressBarCB */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IProgressBarCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7F840F83-C0D9-4774-9B18-A1FE7B56A2F8")
    IProgressBarCB : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Init( 
            /* [in] */ IUnknown *pCG,
            /* [in] */ VARIANT_BOOL bDoInstrument) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProgressBarCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProgressBarCB * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProgressBarCB * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProgressBarCB * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProgressBarCB * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProgressBarCB * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProgressBarCB * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProgressBarCB * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Init )( 
            IProgressBarCB * This,
            /* [in] */ IUnknown *pCG,
            /* [in] */ VARIANT_BOOL bDoInstrument);
        
        END_INTERFACE
    } IProgressBarCBVtbl;

    interface IProgressBarCB
    {
        CONST_VTBL struct IProgressBarCBVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProgressBarCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProgressBarCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProgressBarCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProgressBarCB_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IProgressBarCB_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IProgressBarCB_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IProgressBarCB_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IProgressBarCB_Init(This,pCG,bDoInstrument)	\
    (This)->lpVtbl -> Init(This,pCG,bDoInstrument)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCB_Init_Proxy( 
    IProgressBarCB * This,
    /* [in] */ IUnknown *pCG,
    /* [in] */ VARIANT_BOOL bDoInstrument);


void __RPC_STUB IProgressBarCB_Init_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProgressBarCB_INTERFACE_DEFINED__ */


#ifndef __IErrorMsgCB_INTERFACE_DEFINED__
#define __IErrorMsgCB_INTERFACE_DEFINED__

/* interface IErrorMsgCB */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IErrorMsgCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("31BC4E74-47BA-4069-8BAC-424416BB79C6")
    IErrorMsgCB : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IErrorMsgCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IErrorMsgCB * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IErrorMsgCB * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IErrorMsgCB * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IErrorMsgCB * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IErrorMsgCB * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IErrorMsgCB * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IErrorMsgCB * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IErrorMsgCBVtbl;

    interface IErrorMsgCB
    {
        CONST_VTBL struct IErrorMsgCBVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IErrorMsgCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IErrorMsgCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IErrorMsgCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IErrorMsgCB_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IErrorMsgCB_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IErrorMsgCB_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IErrorMsgCB_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IErrorMsgCB_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_CGC_TLB_0261 */
/* [local] */ 










typedef 
enum MasterProjectType
    {	BISTRO_STANDARD	= 0,
	BISTRO_CALIBRATION	= BISTRO_STANDARD + 1,
	BISTRO_PURE_INTERPRETED	= BISTRO_CALIBRATION + 1,
	BISTRO_DOTNET_PREVIEW_PURE	= BISTRO_PURE_INTERPRETED + 1,
	BISTRO_DOTNET_PREVIEW_MIXED	= BISTRO_DOTNET_PREVIEW_PURE + 1
    } 	MasterProjectType;

typedef 
enum NewNameCreationScheme
    {	BISTRO_UNIQUE_BY_DIR	= 0,
	BISTRO_SAME_NAME	= BISTRO_UNIQUE_BY_DIR + 1,
	BISTRO_USE_SUFFIX	= BISTRO_SAME_NAME + 1
    } 	NewNameCreationScheme;

typedef 
enum NewDirCreationScheme
    {	BISTRO_USE_CACHE	= 0,
	BISTRO_SAME_DIR	= BISTRO_USE_CACHE + 1
    } 	NewDirCreationScheme;

typedef 
enum DependencyLevel
    {	BISTRO_ALL_LEVELS	= 0,
	BISTRO_ONE_LEVEL_ONLY	= BISTRO_ALL_LEVELS + 1,
	BISTRO_FIND_ONLY	= BISTRO_ONE_LEVEL_ONLY + 1
    } 	DependencyLevel;

typedef 
enum ErrorSeverity
    {	BISTRO_Error	= 1,
	BISTRO_Warning	= BISTRO_Error + 1,
	BISTRO_Info	= BISTRO_Warning + 1
    } 	ErrorSeverity;

typedef 
enum ControllerErrorStatus
    {	TREAT_AS_INFO	= 0,
	TREAT_AS_WARNING	= TREAT_AS_INFO + 1,
	TREAT_AS_ERROR	= TREAT_AS_WARNING + 1,
	RETURN_PROBLEMS_TO_CALLER_ALSO	= TREAT_AS_ERROR + 1
    } 	ControllerErrorStatus;

typedef 
enum ASSURE_Feature
    {	FEATURE_GRAPH	= 0,
	FEATURE_MEM_TRACE	= FEATURE_GRAPH + 1,
	FEATURE_REPLACEMENT	= FEATURE_MEM_TRACE + 1
    } 	ASSURE_Feature;



extern RPC_IF_HANDLE __MIDL_itf_CGC_TLB_0261_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_CGC_TLB_0261_v0_0_s_ifspec;

#ifndef __IBistroController_INTERFACE_DEFINED__
#define __IBistroController_INTERFACE_DEFINED__

/* interface IBistroController */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IBistroController;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("54FF69B0-C077-4aa5-9F1A-1DE5323DFCDA")
    IBistroController : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstallationDirectory( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MasterProject( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MasterProject( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ApplicationProject( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ApplicationProject( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CacheDir( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CacheDir( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentorCommandLine( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentorCommandLine( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultPossibleSystemExeInstrumentations( 
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultPossibleSystemDllInstrumentations( 
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultPossibleUserExeInstrumentations( 
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultPossibleUserDllInstrumentations( 
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultSystemExeInstrumentation( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultSystemExeInstrumentation( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultSystemDllInstrumentation( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultSystemDllInstrumentation( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultUserExeInstrumentation( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultUserExeInstrumentation( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultUserDllInstrumentation( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultUserDllInstrumentation( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinimalInstrumentationName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NoneInstrumentationName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentedNameScheme( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentedNameScheme( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentedNameSuffix( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentedNameSuffix( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentedPlacementScheme( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentedPlacementScheme( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultRecordFileName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultRecordFileName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultRecorderOptions( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultRecorderOptions( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InThePlaceInstrumentationAllowed( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InThePlaceInstrumentationAllowed( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AutoAddDependencies( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AutoAddDependencies( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PathPrefix( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PathPrefix( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AssumeRtLibsInPath( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AssumeRtLibsInPath( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AllowCancel( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AllowCancel( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AllowInstrumentationDegradation( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AllowInstrumentationDegradation( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LogFile( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LogFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ApplicationRunningDirectory( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ApplicationRunningDirectory( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideRunTimeInstrumentationConsole( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideRunTimeInstrumentationConsole( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseSeparateConsoleForCUI( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseSeparateConsoleForCUI( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseSeparateProcessGroup( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseSeparateProcessGroup( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StaticRunTimeInstrumentationMessages( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StaticRunTimeInstrumentationMessages( 
            /* [in] */ int newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_VerifyCacheAtRtInstrumentation( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_VerifyCacheAtRtInstrumentation( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Application( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Application( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ApplicationLauncher( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ApplicationLauncher( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RenameOriginalApplicationForLauncher( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RenameOriginalApplicationForLauncher( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ApplicationOptions( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ApplicationOptions( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddModule( 
            /* [in] */ BSTR FileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveModule( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL DependenciesAlso) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Modules( 
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NumberOfModules( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Empty( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ModuleInProject( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ModuleIsDll( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ModuleIsSystem( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DebugInfoAvailable( 
            /* [in] */ BSTR ModuleName,
            /* [out] */ BSTR *pDebugInfoFile,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DebugInfoFile( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DebugInfoFile( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ComInfoFile( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ComInfoFile( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetModuleInfo( 
            /* [in] */ BSTR ModuleName) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PossibleInstrumentationTypes( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationType( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentationType( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentedModuleName( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentedModuleName( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PathToModule( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PathToInstrumentedModule( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PathToInstrumentedModule( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AdditionalInstrumentorOptions( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AdditionalInstrumentorOptions( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationStamp( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentationStamp( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RecordFileName( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RecordFileName( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RecorderOptions( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RecorderOptions( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE get_Dependencies( 
            /* [in] */ BSTR FileName,
            /* [in] */ long level,
            /* [out] */ VARIANT *ModuleList,
            /* [out] */ VARIANT *FullNameList) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NoEnvironmentProjectName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SkipModule( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SkipModule( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CharProperty( 
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CharProperty( 
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BoolProperty( 
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BoolProperty( 
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CharModuleProperty( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CharModuleProperty( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BoolModuleProperty( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BoolModuleProperty( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseConsole( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseConsole( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InitConsole( 
            /* [in] */ int BeforeFirstConnectionTimeout,
            /* [in] */ int AfterLastConnectionTimeout,
            /* [in] */ BSTR RunTimeProjectFullName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseConsole( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WaitForConsole( 
            /* [in] */ VARIANT_BOOL AllowWindowMessagesProcessing) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConsoleSendCommand( 
            /* [in] */ int CommandId,
            /* [in] */ BSTR Buffer,
            /* [in] */ VARIANT TargetList,
            /* [in] */ int CommandSpecificFlags,
            /* [in] */ int ServerFlags) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConsoleAnnotate( 
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer,
            /* [in] */ int ServerFlags) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseRunTimeInstrumentationThread( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseRunTimeInstrumentationThread( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartRunTimeInstrumentationIhread( 
            /* [in] */ BSTR RunTimeProjectFullName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StopRunTimeInstrumentationThread( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RunTimeInstrumentorCmdLine( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RunTimeInstrumentorCmdLine( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartAllRunTimeServices( 
            /* [in] */ BSTR RunTimeProjectFullName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StopAllRunTimeServices( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Save( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Restore( 
            /* [in] */ BSTR ApplicationProjectFileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Clone( 
            /* [retval][out] */ IBistroController **ppCtrl) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Complete( 
            /* [retval][out] */ VARIANT *pPD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Instrument( 
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL ForceRebuildAll,
            /* [in] */ VARIANT_BOOL NoComplete,
            /* [in] */ VARIANT_BOOL CheckOnly,
            /* [retval][out] */ VARIANT *pPD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CancelInstrumentation( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Run( 
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NotifyAboutPtocessFinish,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WaitForApplicationFinish( 
            /* [retval][out] */ VARIANT *pPD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE KillApplication( 
            /* [in] */ VARIANT_BOOL FirstTryGracefully,
            /* [in] */ VARIANT_BOOL UseMessageBoxIfRequired) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PrepareForExternalRun( 
            /* [in] */ BSTR Suffix,
            /* [in] */ VARIANT_BOOL ReplaceOriginalExe,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearExternalRun( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UpdateFromExternalRun( 
            /* [retval][out] */ VARIANT *pPD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearCache( 
            /* [in] */ VARIANT_BOOL CurrentApplicationOnly) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CompletionCallBack( 
            /* [retval][out] */ ICompletionCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CompletionCallBack( 
            /* [in] */ ICompletionCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationCallBack( 
            /* [retval][out] */ IInstrumentationCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentationCallBack( 
            /* [in] */ IInstrumentationCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ProgressBarCallBack( 
            /* [retval][out] */ IProgressBarCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ProgressBarCallBack( 
            /* [in] */ IProgressBarCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationStatusCallBack( 
            /* [retval][out] */ IInstrumentationStatusCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentationStatusCallBack( 
            /* [in] */ IInstrumentationStatusCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BistroControllerInfoCallBack( 
            /* [retval][out] */ IBistroControllerInfoCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BistroControllerInfoCallBack( 
            /* [in] */ IBistroControllerInfoCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BistroControllerErrorCallBack( 
            /* [retval][out] */ IBistroControllerErrorCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BistroControllerErrorCallBack( 
            /* [in] */ IBistroControllerErrorCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ConsoleVisualizeCallBack( 
            /* [retval][out] */ IConsoleVisualizeCallBack **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ConsoleVisualizeCallBack( 
            /* [in] */ IConsoleVisualizeCallBack *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallBacksDll( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CallBacksDll( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMasterProject( 
            /* [in] */ long eType) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TryLong( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TryLong( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationCallBackInitString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentationCallBackInitString( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RunCallBackInitString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RunCallBackInitString( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PostProcessingCallBackInitString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PostProcessingCallBackInitString( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ConsoleTransportInitString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ConsoleTransportInitString( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CompletionCallBackInitString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CompletionCallBackInitString( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SkipAllModules( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TargetMachineName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TargetMachineName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AllowAttachDetach( 
            /* [in] */ VARIANT_BOOL bAllow) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AllowMultiuser( 
            /* [in] */ VARIANT_BOOL bAllowMultiuser) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetModuleAssureFeature( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [in] */ VARIANT_BOOL State,
            /* [retval][out] */ VARIANT_BOOL *bAccepted) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetModuleAssureFeature( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [retval][out] */ VARIANT_BOOL *State) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBistroControllerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBistroController * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBistroController * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBistroController * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBistroController * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBistroController * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBistroController * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBistroController * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstallationDirectory )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IBistroController * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MasterProject )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MasterProject )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationProject )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationProject )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CacheDir )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CacheDir )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentorCommandLine )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentorCommandLine )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleSystemExeInstrumentations )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleSystemDllInstrumentations )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleUserExeInstrumentations )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleUserDllInstrumentations )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultSystemExeInstrumentation )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultSystemExeInstrumentation )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultSystemDllInstrumentation )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultSystemDllInstrumentation )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultUserExeInstrumentation )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultUserExeInstrumentation )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultUserDllInstrumentation )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultUserDllInstrumentation )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinimalInstrumentationName )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NoneInstrumentationName )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedNameScheme )( 
            IBistroController * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedNameScheme )( 
            IBistroController * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedNameSuffix )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedNameSuffix )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedPlacementScheme )( 
            IBistroController * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedPlacementScheme )( 
            IBistroController * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultRecordFileName )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultRecordFileName )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultRecorderOptions )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultRecorderOptions )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InThePlaceInstrumentationAllowed )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InThePlaceInstrumentationAllowed )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AutoAddDependencies )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AutoAddDependencies )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathPrefix )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PathPrefix )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AssumeRtLibsInPath )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AssumeRtLibsInPath )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllowCancel )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllowCancel )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllowInstrumentationDegradation )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllowInstrumentationDegradation )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogFile )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogFile )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationRunningDirectory )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationRunningDirectory )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideRunTimeInstrumentationConsole )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideRunTimeInstrumentationConsole )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSeparateConsoleForCUI )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSeparateConsoleForCUI )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSeparateProcessGroup )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSeparateProcessGroup )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StaticRunTimeInstrumentationMessages )( 
            IBistroController * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StaticRunTimeInstrumentationMessages )( 
            IBistroController * This,
            /* [in] */ int newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_VerifyCacheAtRtInstrumentation )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_VerifyCacheAtRtInstrumentation )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Application )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationLauncher )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationLauncher )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RenameOriginalApplicationForLauncher )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RenameOriginalApplicationForLauncher )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationOptions )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationOptions )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddModule )( 
            IBistroController * This,
            /* [in] */ BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveModule )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL DependenciesAlso);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Modules )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberOfModules )( 
            IBistroController * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Empty )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleInProject )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleIsDll )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleIsSystem )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DebugInfoAvailable )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [out] */ BSTR *pDebugInfoFile,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DebugInfoFile )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DebugInfoFile )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ComInfoFile )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ComInfoFile )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetModuleInfo )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PossibleInstrumentationTypes )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationType )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationType )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedModuleName )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedModuleName )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathToModule )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathToInstrumentedModule )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PathToInstrumentedModule )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AdditionalInstrumentorOptions )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AdditionalInstrumentorOptions )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationStamp )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationStamp )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecordFileName )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecordFileName )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecorderOptions )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecorderOptions )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *get_Dependencies )( 
            IBistroController * This,
            /* [in] */ BSTR FileName,
            /* [in] */ long level,
            /* [out] */ VARIANT *ModuleList,
            /* [out] */ VARIANT *FullNameList);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NoEnvironmentProjectName )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SkipModule )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SkipModule )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharProperty )( 
            IBistroController * This,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharProperty )( 
            IBistroController * This,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BoolProperty )( 
            IBistroController * This,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BoolProperty )( 
            IBistroController * This,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharModuleProperty )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharModuleProperty )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BoolModuleProperty )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BoolModuleProperty )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseConsole )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseConsole )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitConsole )( 
            IBistroController * This,
            /* [in] */ int BeforeFirstConnectionTimeout,
            /* [in] */ int AfterLastConnectionTimeout,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseConsole )( 
            IBistroController * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForConsole )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL AllowWindowMessagesProcessing);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleSendCommand )( 
            IBistroController * This,
            /* [in] */ int CommandId,
            /* [in] */ BSTR Buffer,
            /* [in] */ VARIANT TargetList,
            /* [in] */ int CommandSpecificFlags,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleAnnotate )( 
            IBistroController * This,
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseRunTimeInstrumentationThread )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseRunTimeInstrumentationThread )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartRunTimeInstrumentationIhread )( 
            IBistroController * This,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopRunTimeInstrumentationThread )( 
            IBistroController * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RunTimeInstrumentorCmdLine )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RunTimeInstrumentorCmdLine )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartAllRunTimeServices )( 
            IBistroController * This,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopAllRunTimeServices )( 
            IBistroController * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IBistroController * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Restore )( 
            IBistroController * This,
            /* [in] */ BSTR ApplicationProjectFileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IBistroController * This,
            /* [retval][out] */ IBistroController **ppCtrl);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Complete )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Instrument )( 
            IBistroController * This,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL ForceRebuildAll,
            /* [in] */ VARIANT_BOOL NoComplete,
            /* [in] */ VARIANT_BOOL CheckOnly,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CancelInstrumentation )( 
            IBistroController * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IBistroController * This,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NotifyAboutPtocessFinish,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForApplicationFinish )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *KillApplication )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL FirstTryGracefully,
            /* [in] */ VARIANT_BOOL UseMessageBoxIfRequired);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrepareForExternalRun )( 
            IBistroController * This,
            /* [in] */ BSTR Suffix,
            /* [in] */ VARIANT_BOOL ReplaceOriginalExe,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearExternalRun )( 
            IBistroController * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateFromExternalRun )( 
            IBistroController * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearCache )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL CurrentApplicationOnly);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompletionCallBack )( 
            IBistroController * This,
            /* [retval][out] */ ICompletionCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompletionCallBack )( 
            IBistroController * This,
            /* [in] */ ICompletionCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationCallBack )( 
            IBistroController * This,
            /* [retval][out] */ IInstrumentationCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationCallBack )( 
            IBistroController * This,
            /* [in] */ IInstrumentationCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProgressBarCallBack )( 
            IBistroController * This,
            /* [retval][out] */ IProgressBarCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ProgressBarCallBack )( 
            IBistroController * This,
            /* [in] */ IProgressBarCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationStatusCallBack )( 
            IBistroController * This,
            /* [retval][out] */ IInstrumentationStatusCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationStatusCallBack )( 
            IBistroController * This,
            /* [in] */ IInstrumentationStatusCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroControllerInfoCallBack )( 
            IBistroController * This,
            /* [retval][out] */ IBistroControllerInfoCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroControllerInfoCallBack )( 
            IBistroController * This,
            /* [in] */ IBistroControllerInfoCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroControllerErrorCallBack )( 
            IBistroController * This,
            /* [retval][out] */ IBistroControllerErrorCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroControllerErrorCallBack )( 
            IBistroController * This,
            /* [in] */ IBistroControllerErrorCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConsoleVisualizeCallBack )( 
            IBistroController * This,
            /* [retval][out] */ IConsoleVisualizeCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConsoleVisualizeCallBack )( 
            IBistroController * This,
            /* [in] */ IConsoleVisualizeCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallBacksDll )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallBacksDll )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMasterProject )( 
            IBistroController * This,
            /* [in] */ long eType);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TryLong )( 
            IBistroController * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TryLong )( 
            IBistroController * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationCallBackInitString )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationCallBackInitString )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RunCallBackInitString )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RunCallBackInitString )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PostProcessingCallBackInitString )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PostProcessingCallBackInitString )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConsoleTransportInitString )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConsoleTransportInitString )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompletionCallBackInitString )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompletionCallBackInitString )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SkipAllModules )( 
            IBistroController * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TargetMachineName )( 
            IBistroController * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TargetMachineName )( 
            IBistroController * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowAttachDetach )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL bAllow);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowMultiuser )( 
            IBistroController * This,
            /* [in] */ VARIANT_BOOL bAllowMultiuser);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetModuleAssureFeature )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [in] */ VARIANT_BOOL State,
            /* [retval][out] */ VARIANT_BOOL *bAccepted);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetModuleAssureFeature )( 
            IBistroController * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [retval][out] */ VARIANT_BOOL *State);
        
        END_INTERFACE
    } IBistroControllerVtbl;

    interface IBistroController
    {
        CONST_VTBL struct IBistroControllerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBistroController_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IBistroController_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IBistroController_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IBistroController_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IBistroController_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IBistroController_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IBistroController_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IBistroController_get_InstallationDirectory(This,pVal)	\
    (This)->lpVtbl -> get_InstallationDirectory(This,pVal)

#define IBistroController_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IBistroController_get_MasterProject(This,pVal)	\
    (This)->lpVtbl -> get_MasterProject(This,pVal)

#define IBistroController_put_MasterProject(This,newVal)	\
    (This)->lpVtbl -> put_MasterProject(This,newVal)

#define IBistroController_get_ApplicationProject(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationProject(This,pVal)

#define IBistroController_put_ApplicationProject(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationProject(This,newVal)

#define IBistroController_get_CacheDir(This,pVal)	\
    (This)->lpVtbl -> get_CacheDir(This,pVal)

#define IBistroController_put_CacheDir(This,newVal)	\
    (This)->lpVtbl -> put_CacheDir(This,newVal)

#define IBistroController_get_InstrumentorCommandLine(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentorCommandLine(This,pVal)

#define IBistroController_put_InstrumentorCommandLine(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentorCommandLine(This,newVal)

#define IBistroController_get_DefaultPossibleSystemExeInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleSystemExeInstrumentations(This,pVal)

#define IBistroController_get_DefaultPossibleSystemDllInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleSystemDllInstrumentations(This,pVal)

#define IBistroController_get_DefaultPossibleUserExeInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleUserExeInstrumentations(This,pVal)

#define IBistroController_get_DefaultPossibleUserDllInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleUserDllInstrumentations(This,pVal)

#define IBistroController_get_DefaultSystemExeInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultSystemExeInstrumentation(This,pVal)

#define IBistroController_put_DefaultSystemExeInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultSystemExeInstrumentation(This,newVal)

#define IBistroController_get_DefaultSystemDllInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultSystemDllInstrumentation(This,pVal)

#define IBistroController_put_DefaultSystemDllInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultSystemDllInstrumentation(This,newVal)

#define IBistroController_get_DefaultUserExeInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultUserExeInstrumentation(This,pVal)

#define IBistroController_put_DefaultUserExeInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultUserExeInstrumentation(This,newVal)

#define IBistroController_get_DefaultUserDllInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultUserDllInstrumentation(This,pVal)

#define IBistroController_put_DefaultUserDllInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultUserDllInstrumentation(This,newVal)

#define IBistroController_get_MinimalInstrumentationName(This,pVal)	\
    (This)->lpVtbl -> get_MinimalInstrumentationName(This,pVal)

#define IBistroController_get_NoneInstrumentationName(This,pVal)	\
    (This)->lpVtbl -> get_NoneInstrumentationName(This,pVal)

#define IBistroController_get_InstrumentedNameScheme(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedNameScheme(This,pVal)

#define IBistroController_put_InstrumentedNameScheme(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedNameScheme(This,newVal)

#define IBistroController_get_InstrumentedNameSuffix(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedNameSuffix(This,pVal)

#define IBistroController_put_InstrumentedNameSuffix(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedNameSuffix(This,newVal)

#define IBistroController_get_InstrumentedPlacementScheme(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedPlacementScheme(This,pVal)

#define IBistroController_put_InstrumentedPlacementScheme(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedPlacementScheme(This,newVal)

#define IBistroController_get_DefaultRecordFileName(This,pVal)	\
    (This)->lpVtbl -> get_DefaultRecordFileName(This,pVal)

#define IBistroController_put_DefaultRecordFileName(This,newVal)	\
    (This)->lpVtbl -> put_DefaultRecordFileName(This,newVal)

#define IBistroController_get_DefaultRecorderOptions(This,pVal)	\
    (This)->lpVtbl -> get_DefaultRecorderOptions(This,pVal)

#define IBistroController_put_DefaultRecorderOptions(This,newVal)	\
    (This)->lpVtbl -> put_DefaultRecorderOptions(This,newVal)

#define IBistroController_get_InThePlaceInstrumentationAllowed(This,pVal)	\
    (This)->lpVtbl -> get_InThePlaceInstrumentationAllowed(This,pVal)

#define IBistroController_put_InThePlaceInstrumentationAllowed(This,newVal)	\
    (This)->lpVtbl -> put_InThePlaceInstrumentationAllowed(This,newVal)

#define IBistroController_get_AutoAddDependencies(This,pVal)	\
    (This)->lpVtbl -> get_AutoAddDependencies(This,pVal)

#define IBistroController_put_AutoAddDependencies(This,newVal)	\
    (This)->lpVtbl -> put_AutoAddDependencies(This,newVal)

#define IBistroController_get_PathPrefix(This,pVal)	\
    (This)->lpVtbl -> get_PathPrefix(This,pVal)

#define IBistroController_put_PathPrefix(This,newVal)	\
    (This)->lpVtbl -> put_PathPrefix(This,newVal)

#define IBistroController_get_AssumeRtLibsInPath(This,pVal)	\
    (This)->lpVtbl -> get_AssumeRtLibsInPath(This,pVal)

#define IBistroController_put_AssumeRtLibsInPath(This,newVal)	\
    (This)->lpVtbl -> put_AssumeRtLibsInPath(This,newVal)

#define IBistroController_get_AllowCancel(This,pVal)	\
    (This)->lpVtbl -> get_AllowCancel(This,pVal)

#define IBistroController_put_AllowCancel(This,newVal)	\
    (This)->lpVtbl -> put_AllowCancel(This,newVal)

#define IBistroController_get_AllowInstrumentationDegradation(This,pVal)	\
    (This)->lpVtbl -> get_AllowInstrumentationDegradation(This,pVal)

#define IBistroController_put_AllowInstrumentationDegradation(This,newVal)	\
    (This)->lpVtbl -> put_AllowInstrumentationDegradation(This,newVal)

#define IBistroController_get_LogFile(This,pVal)	\
    (This)->lpVtbl -> get_LogFile(This,pVal)

#define IBistroController_put_LogFile(This,newVal)	\
    (This)->lpVtbl -> put_LogFile(This,newVal)

#define IBistroController_get_ApplicationRunningDirectory(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationRunningDirectory(This,pVal)

#define IBistroController_put_ApplicationRunningDirectory(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationRunningDirectory(This,newVal)

#define IBistroController_get_HideRunTimeInstrumentationConsole(This,pVal)	\
    (This)->lpVtbl -> get_HideRunTimeInstrumentationConsole(This,pVal)

#define IBistroController_put_HideRunTimeInstrumentationConsole(This,newVal)	\
    (This)->lpVtbl -> put_HideRunTimeInstrumentationConsole(This,newVal)

#define IBistroController_get_UseSeparateConsoleForCUI(This,pVal)	\
    (This)->lpVtbl -> get_UseSeparateConsoleForCUI(This,pVal)

#define IBistroController_put_UseSeparateConsoleForCUI(This,newVal)	\
    (This)->lpVtbl -> put_UseSeparateConsoleForCUI(This,newVal)

#define IBistroController_get_UseSeparateProcessGroup(This,pVal)	\
    (This)->lpVtbl -> get_UseSeparateProcessGroup(This,pVal)

#define IBistroController_put_UseSeparateProcessGroup(This,newVal)	\
    (This)->lpVtbl -> put_UseSeparateProcessGroup(This,newVal)

#define IBistroController_get_StaticRunTimeInstrumentationMessages(This,pVal)	\
    (This)->lpVtbl -> get_StaticRunTimeInstrumentationMessages(This,pVal)

#define IBistroController_put_StaticRunTimeInstrumentationMessages(This,newVal)	\
    (This)->lpVtbl -> put_StaticRunTimeInstrumentationMessages(This,newVal)

#define IBistroController_get_VerifyCacheAtRtInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_VerifyCacheAtRtInstrumentation(This,pVal)

#define IBistroController_put_VerifyCacheAtRtInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_VerifyCacheAtRtInstrumentation(This,newVal)

#define IBistroController_get_Application(This,pVal)	\
    (This)->lpVtbl -> get_Application(This,pVal)

#define IBistroController_put_Application(This,newVal)	\
    (This)->lpVtbl -> put_Application(This,newVal)

#define IBistroController_get_ApplicationLauncher(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationLauncher(This,pVal)

#define IBistroController_put_ApplicationLauncher(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationLauncher(This,newVal)

#define IBistroController_get_RenameOriginalApplicationForLauncher(This,pVal)	\
    (This)->lpVtbl -> get_RenameOriginalApplicationForLauncher(This,pVal)

#define IBistroController_put_RenameOriginalApplicationForLauncher(This,newVal)	\
    (This)->lpVtbl -> put_RenameOriginalApplicationForLauncher(This,newVal)

#define IBistroController_get_ApplicationOptions(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationOptions(This,pVal)

#define IBistroController_put_ApplicationOptions(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationOptions(This,newVal)

#define IBistroController_AddModule(This,FileName)	\
    (This)->lpVtbl -> AddModule(This,FileName)

#define IBistroController_RemoveModule(This,ModuleName,DependenciesAlso)	\
    (This)->lpVtbl -> RemoveModule(This,ModuleName,DependenciesAlso)

#define IBistroController_get_Modules(This,pVal)	\
    (This)->lpVtbl -> get_Modules(This,pVal)

#define IBistroController_get_NumberOfModules(This,pVal)	\
    (This)->lpVtbl -> get_NumberOfModules(This,pVal)

#define IBistroController_Empty(This,pVal)	\
    (This)->lpVtbl -> Empty(This,pVal)

#define IBistroController_ModuleInProject(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleInProject(This,ModuleName,pVal)

#define IBistroController_ModuleIsDll(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleIsDll(This,ModuleName,pVal)

#define IBistroController_ModuleIsSystem(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleIsSystem(This,ModuleName,pVal)

#define IBistroController_DebugInfoAvailable(This,ModuleName,pDebugInfoFile,pVal)	\
    (This)->lpVtbl -> DebugInfoAvailable(This,ModuleName,pDebugInfoFile,pVal)

#define IBistroController_get_DebugInfoFile(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_DebugInfoFile(This,ModuleName,pVal)

#define IBistroController_put_DebugInfoFile(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_DebugInfoFile(This,ModuleName,newVal)

#define IBistroController_get_ComInfoFile(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_ComInfoFile(This,ModuleName,pVal)

#define IBistroController_put_ComInfoFile(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_ComInfoFile(This,ModuleName,newVal)

#define IBistroController_ResetModuleInfo(This,ModuleName)	\
    (This)->lpVtbl -> ResetModuleInfo(This,ModuleName)

#define IBistroController_get_PossibleInstrumentationTypes(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PossibleInstrumentationTypes(This,ModuleName,pVal)

#define IBistroController_get_InstrumentationType(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentationType(This,ModuleName,pVal)

#define IBistroController_put_InstrumentationType(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentationType(This,ModuleName,newVal)

#define IBistroController_get_InstrumentedModuleName(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentedModuleName(This,ModuleName,pVal)

#define IBistroController_put_InstrumentedModuleName(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentedModuleName(This,ModuleName,newVal)

#define IBistroController_get_PathToModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PathToModule(This,ModuleName,pVal)

#define IBistroController_get_PathToInstrumentedModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PathToInstrumentedModule(This,ModuleName,pVal)

#define IBistroController_put_PathToInstrumentedModule(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_PathToInstrumentedModule(This,ModuleName,newVal)

#define IBistroController_get_AdditionalInstrumentorOptions(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_AdditionalInstrumentorOptions(This,ModuleName,pVal)

#define IBistroController_put_AdditionalInstrumentorOptions(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_AdditionalInstrumentorOptions(This,ModuleName,newVal)

#define IBistroController_get_InstrumentationStamp(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentationStamp(This,ModuleName,pVal)

#define IBistroController_put_InstrumentationStamp(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentationStamp(This,ModuleName,newVal)

#define IBistroController_get_RecordFileName(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_RecordFileName(This,ModuleName,pVal)

#define IBistroController_put_RecordFileName(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_RecordFileName(This,ModuleName,newVal)

#define IBistroController_get_RecorderOptions(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_RecorderOptions(This,ModuleName,pVal)

#define IBistroController_put_RecorderOptions(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_RecorderOptions(This,ModuleName,newVal)

#define IBistroController_get_Dependencies(This,FileName,level,ModuleList,FullNameList)	\
    (This)->lpVtbl -> get_Dependencies(This,FileName,level,ModuleList,FullNameList)

#define IBistroController_get_NoEnvironmentProjectName(This,pVal)	\
    (This)->lpVtbl -> get_NoEnvironmentProjectName(This,pVal)

#define IBistroController_get_SkipModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_SkipModule(This,ModuleName,pVal)

#define IBistroController_put_SkipModule(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_SkipModule(This,ModuleName,newVal)

#define IBistroController_get_CharProperty(This,PropertyName,pVal)	\
    (This)->lpVtbl -> get_CharProperty(This,PropertyName,pVal)

#define IBistroController_put_CharProperty(This,PropertyName,newVal)	\
    (This)->lpVtbl -> put_CharProperty(This,PropertyName,newVal)

#define IBistroController_get_BoolProperty(This,PropertyName,pVal)	\
    (This)->lpVtbl -> get_BoolProperty(This,PropertyName,pVal)

#define IBistroController_put_BoolProperty(This,PropertyName,newVal)	\
    (This)->lpVtbl -> put_BoolProperty(This,PropertyName,newVal)

#define IBistroController_get_CharModuleProperty(This,ModuleName,PropertyName,pVal)	\
    (This)->lpVtbl -> get_CharModuleProperty(This,ModuleName,PropertyName,pVal)

#define IBistroController_put_CharModuleProperty(This,ModuleName,PropertyName,newVal)	\
    (This)->lpVtbl -> put_CharModuleProperty(This,ModuleName,PropertyName,newVal)

#define IBistroController_get_BoolModuleProperty(This,ModuleName,PropertyName,pVal)	\
    (This)->lpVtbl -> get_BoolModuleProperty(This,ModuleName,PropertyName,pVal)

#define IBistroController_put_BoolModuleProperty(This,ModuleName,PropertyName,newVal)	\
    (This)->lpVtbl -> put_BoolModuleProperty(This,ModuleName,PropertyName,newVal)

#define IBistroController_get_UseConsole(This,pVal)	\
    (This)->lpVtbl -> get_UseConsole(This,pVal)

#define IBistroController_put_UseConsole(This,newVal)	\
    (This)->lpVtbl -> put_UseConsole(This,newVal)

#define IBistroController_InitConsole(This,BeforeFirstConnectionTimeout,AfterLastConnectionTimeout,RunTimeProjectFullName)	\
    (This)->lpVtbl -> InitConsole(This,BeforeFirstConnectionTimeout,AfterLastConnectionTimeout,RunTimeProjectFullName)

#define IBistroController_CloseConsole(This)	\
    (This)->lpVtbl -> CloseConsole(This)

#define IBistroController_WaitForConsole(This,AllowWindowMessagesProcessing)	\
    (This)->lpVtbl -> WaitForConsole(This,AllowWindowMessagesProcessing)

#define IBistroController_ConsoleSendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)	\
    (This)->lpVtbl -> ConsoleSendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)

#define IBistroController_ConsoleAnnotate(This,AnnotationId,Buffer,ServerFlags)	\
    (This)->lpVtbl -> ConsoleAnnotate(This,AnnotationId,Buffer,ServerFlags)

#define IBistroController_get_UseRunTimeInstrumentationThread(This,pVal)	\
    (This)->lpVtbl -> get_UseRunTimeInstrumentationThread(This,pVal)

#define IBistroController_put_UseRunTimeInstrumentationThread(This,newVal)	\
    (This)->lpVtbl -> put_UseRunTimeInstrumentationThread(This,newVal)

#define IBistroController_StartRunTimeInstrumentationIhread(This,RunTimeProjectFullName)	\
    (This)->lpVtbl -> StartRunTimeInstrumentationIhread(This,RunTimeProjectFullName)

#define IBistroController_StopRunTimeInstrumentationThread(This)	\
    (This)->lpVtbl -> StopRunTimeInstrumentationThread(This)

#define IBistroController_get_RunTimeInstrumentorCmdLine(This,pVal)	\
    (This)->lpVtbl -> get_RunTimeInstrumentorCmdLine(This,pVal)

#define IBistroController_put_RunTimeInstrumentorCmdLine(This,newVal)	\
    (This)->lpVtbl -> put_RunTimeInstrumentorCmdLine(This,newVal)

#define IBistroController_StartAllRunTimeServices(This,RunTimeProjectFullName)	\
    (This)->lpVtbl -> StartAllRunTimeServices(This,RunTimeProjectFullName)

#define IBistroController_StopAllRunTimeServices(This)	\
    (This)->lpVtbl -> StopAllRunTimeServices(This)

#define IBistroController_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IBistroController_Restore(This,ApplicationProjectFileName)	\
    (This)->lpVtbl -> Restore(This,ApplicationProjectFileName)

#define IBistroController_Clone(This,ppCtrl)	\
    (This)->lpVtbl -> Clone(This,ppCtrl)

#define IBistroController_Complete(This,pPD)	\
    (This)->lpVtbl -> Complete(This,pPD)

#define IBistroController_Instrument(This,ApplicationProjectWasChanged,ForceRebuildAll,NoComplete,CheckOnly,pPD)	\
    (This)->lpVtbl -> Instrument(This,ApplicationProjectWasChanged,ForceRebuildAll,NoComplete,CheckOnly,pPD)

#define IBistroController_CancelInstrumentation(This)	\
    (This)->lpVtbl -> CancelInstrumentation(This)

#define IBistroController_Run(This,ApplicationProjectWasChanged,NotifyAboutPtocessFinish,NoInstrument,pPD)	\
    (This)->lpVtbl -> Run(This,ApplicationProjectWasChanged,NotifyAboutPtocessFinish,NoInstrument,pPD)

#define IBistroController_WaitForApplicationFinish(This,pPD)	\
    (This)->lpVtbl -> WaitForApplicationFinish(This,pPD)

#define IBistroController_KillApplication(This,FirstTryGracefully,UseMessageBoxIfRequired)	\
    (This)->lpVtbl -> KillApplication(This,FirstTryGracefully,UseMessageBoxIfRequired)

#define IBistroController_PrepareForExternalRun(This,Suffix,ReplaceOriginalExe,ApplicationProjectWasChanged,NoInstrument,pPD)	\
    (This)->lpVtbl -> PrepareForExternalRun(This,Suffix,ReplaceOriginalExe,ApplicationProjectWasChanged,NoInstrument,pPD)

#define IBistroController_ClearExternalRun(This)	\
    (This)->lpVtbl -> ClearExternalRun(This)

#define IBistroController_UpdateFromExternalRun(This,pPD)	\
    (This)->lpVtbl -> UpdateFromExternalRun(This,pPD)

#define IBistroController_ClearCache(This,CurrentApplicationOnly)	\
    (This)->lpVtbl -> ClearCache(This,CurrentApplicationOnly)

#define IBistroController_get_CompletionCallBack(This,pVal)	\
    (This)->lpVtbl -> get_CompletionCallBack(This,pVal)

#define IBistroController_put_CompletionCallBack(This,newVal)	\
    (This)->lpVtbl -> put_CompletionCallBack(This,newVal)

#define IBistroController_get_InstrumentationCallBack(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationCallBack(This,pVal)

#define IBistroController_put_InstrumentationCallBack(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationCallBack(This,newVal)

#define IBistroController_get_ProgressBarCallBack(This,pVal)	\
    (This)->lpVtbl -> get_ProgressBarCallBack(This,pVal)

#define IBistroController_put_ProgressBarCallBack(This,newVal)	\
    (This)->lpVtbl -> put_ProgressBarCallBack(This,newVal)

#define IBistroController_get_InstrumentationStatusCallBack(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationStatusCallBack(This,pVal)

#define IBistroController_put_InstrumentationStatusCallBack(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationStatusCallBack(This,newVal)

#define IBistroController_get_BistroControllerInfoCallBack(This,pVal)	\
    (This)->lpVtbl -> get_BistroControllerInfoCallBack(This,pVal)

#define IBistroController_put_BistroControllerInfoCallBack(This,newVal)	\
    (This)->lpVtbl -> put_BistroControllerInfoCallBack(This,newVal)

#define IBistroController_get_BistroControllerErrorCallBack(This,pVal)	\
    (This)->lpVtbl -> get_BistroControllerErrorCallBack(This,pVal)

#define IBistroController_put_BistroControllerErrorCallBack(This,newVal)	\
    (This)->lpVtbl -> put_BistroControllerErrorCallBack(This,newVal)

#define IBistroController_get_ConsoleVisualizeCallBack(This,pVal)	\
    (This)->lpVtbl -> get_ConsoleVisualizeCallBack(This,pVal)

#define IBistroController_put_ConsoleVisualizeCallBack(This,newVal)	\
    (This)->lpVtbl -> put_ConsoleVisualizeCallBack(This,newVal)

#define IBistroController_get_CallBacksDll(This,pVal)	\
    (This)->lpVtbl -> get_CallBacksDll(This,pVal)

#define IBistroController_put_CallBacksDll(This,newVal)	\
    (This)->lpVtbl -> put_CallBacksDll(This,newVal)

#define IBistroController_SetMasterProject(This,eType)	\
    (This)->lpVtbl -> SetMasterProject(This,eType)

#define IBistroController_get_TryLong(This,pVal)	\
    (This)->lpVtbl -> get_TryLong(This,pVal)

#define IBistroController_put_TryLong(This,newVal)	\
    (This)->lpVtbl -> put_TryLong(This,newVal)

#define IBistroController_get_InstrumentationCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationCallBackInitString(This,pVal)

#define IBistroController_put_InstrumentationCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationCallBackInitString(This,newVal)

#define IBistroController_get_RunCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_RunCallBackInitString(This,pVal)

#define IBistroController_put_RunCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_RunCallBackInitString(This,newVal)

#define IBistroController_get_PostProcessingCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_PostProcessingCallBackInitString(This,pVal)

#define IBistroController_put_PostProcessingCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_PostProcessingCallBackInitString(This,newVal)

#define IBistroController_get_ConsoleTransportInitString(This,pVal)	\
    (This)->lpVtbl -> get_ConsoleTransportInitString(This,pVal)

#define IBistroController_put_ConsoleTransportInitString(This,newVal)	\
    (This)->lpVtbl -> put_ConsoleTransportInitString(This,newVal)

#define IBistroController_get_CompletionCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_CompletionCallBackInitString(This,pVal)

#define IBistroController_put_CompletionCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_CompletionCallBackInitString(This,newVal)

#define IBistroController_put_SkipAllModules(This,newVal)	\
    (This)->lpVtbl -> put_SkipAllModules(This,newVal)

#define IBistroController_get_TargetMachineName(This,pVal)	\
    (This)->lpVtbl -> get_TargetMachineName(This,pVal)

#define IBistroController_put_TargetMachineName(This,newVal)	\
    (This)->lpVtbl -> put_TargetMachineName(This,newVal)

#define IBistroController_AllowAttachDetach(This,bAllow)	\
    (This)->lpVtbl -> AllowAttachDetach(This,bAllow)

#define IBistroController_AllowMultiuser(This,bAllowMultiuser)	\
    (This)->lpVtbl -> AllowMultiuser(This,bAllowMultiuser)

#define IBistroController_SetModuleAssureFeature(This,ModuleName,Feature,State,bAccepted)	\
    (This)->lpVtbl -> SetModuleAssureFeature(This,ModuleName,Feature,State,bAccepted)

#define IBistroController_GetModuleAssureFeature(This,ModuleName,Feature,State)	\
    (This)->lpVtbl -> GetModuleAssureFeature(This,ModuleName,Feature,State)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstallationDirectory_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstallationDirectory_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Reset_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_MasterProject_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_MasterProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_MasterProject_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_MasterProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ApplicationProject_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_ApplicationProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ApplicationProject_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_ApplicationProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_CacheDir_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_CacheDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_CacheDir_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_CacheDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentorCommandLine_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstrumentorCommandLine_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentorCommandLine_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_InstrumentorCommandLine_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultPossibleSystemExeInstrumentations_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController_get_DefaultPossibleSystemExeInstrumentations_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultPossibleSystemDllInstrumentations_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController_get_DefaultPossibleSystemDllInstrumentations_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultPossibleUserExeInstrumentations_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController_get_DefaultPossibleUserExeInstrumentations_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultPossibleUserDllInstrumentations_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController_get_DefaultPossibleUserDllInstrumentations_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultSystemExeInstrumentation_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DefaultSystemExeInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DefaultSystemExeInstrumentation_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DefaultSystemExeInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultSystemDllInstrumentation_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DefaultSystemDllInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DefaultSystemDllInstrumentation_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DefaultSystemDllInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultUserExeInstrumentation_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DefaultUserExeInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DefaultUserExeInstrumentation_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DefaultUserExeInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultUserDllInstrumentation_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DefaultUserDllInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DefaultUserDllInstrumentation_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DefaultUserDllInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_MinimalInstrumentationName_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_MinimalInstrumentationName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_NoneInstrumentationName_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_NoneInstrumentationName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentedNameScheme_Proxy( 
    IBistroController * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IBistroController_get_InstrumentedNameScheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentedNameScheme_Proxy( 
    IBistroController * This,
    /* [in] */ long newVal);


void __RPC_STUB IBistroController_put_InstrumentedNameScheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentedNameSuffix_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstrumentedNameSuffix_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentedNameSuffix_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_InstrumentedNameSuffix_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentedPlacementScheme_Proxy( 
    IBistroController * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IBistroController_get_InstrumentedPlacementScheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentedPlacementScheme_Proxy( 
    IBistroController * This,
    /* [in] */ long newVal);


void __RPC_STUB IBistroController_put_InstrumentedPlacementScheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultRecordFileName_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DefaultRecordFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DefaultRecordFileName_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DefaultRecordFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DefaultRecorderOptions_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DefaultRecorderOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DefaultRecorderOptions_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DefaultRecorderOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InThePlaceInstrumentationAllowed_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_InThePlaceInstrumentationAllowed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InThePlaceInstrumentationAllowed_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_InThePlaceInstrumentationAllowed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_AutoAddDependencies_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_AutoAddDependencies_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_AutoAddDependencies_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_AutoAddDependencies_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_PathPrefix_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_PathPrefix_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_PathPrefix_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_PathPrefix_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_AssumeRtLibsInPath_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_AssumeRtLibsInPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_AssumeRtLibsInPath_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_AssumeRtLibsInPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_AllowCancel_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_AllowCancel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_AllowCancel_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_AllowCancel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_AllowInstrumentationDegradation_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_AllowInstrumentationDegradation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_AllowInstrumentationDegradation_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_AllowInstrumentationDegradation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_LogFile_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_LogFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_LogFile_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_LogFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ApplicationRunningDirectory_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_ApplicationRunningDirectory_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ApplicationRunningDirectory_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_ApplicationRunningDirectory_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_HideRunTimeInstrumentationConsole_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_HideRunTimeInstrumentationConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_HideRunTimeInstrumentationConsole_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_HideRunTimeInstrumentationConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_UseSeparateConsoleForCUI_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_UseSeparateConsoleForCUI_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_UseSeparateConsoleForCUI_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_UseSeparateConsoleForCUI_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_UseSeparateProcessGroup_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_UseSeparateProcessGroup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_UseSeparateProcessGroup_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_UseSeparateProcessGroup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_StaticRunTimeInstrumentationMessages_Proxy( 
    IBistroController * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB IBistroController_get_StaticRunTimeInstrumentationMessages_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_StaticRunTimeInstrumentationMessages_Proxy( 
    IBistroController * This,
    /* [in] */ int newVal);


void __RPC_STUB IBistroController_put_StaticRunTimeInstrumentationMessages_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_VerifyCacheAtRtInstrumentation_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_VerifyCacheAtRtInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_VerifyCacheAtRtInstrumentation_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_VerifyCacheAtRtInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_Application_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_Application_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_Application_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_Application_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ApplicationLauncher_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_ApplicationLauncher_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ApplicationLauncher_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_ApplicationLauncher_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_RenameOriginalApplicationForLauncher_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_RenameOriginalApplicationForLauncher_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_RenameOriginalApplicationForLauncher_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_RenameOriginalApplicationForLauncher_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ApplicationOptions_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_ApplicationOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ApplicationOptions_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_ApplicationOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_AddModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR FileName);


void __RPC_STUB IBistroController_AddModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_RemoveModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ VARIANT_BOOL DependenciesAlso);


void __RPC_STUB IBistroController_RemoveModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_Modules_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController_get_Modules_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_NumberOfModules_Proxy( 
    IBistroController * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB IBistroController_get_NumberOfModules_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Empty_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_Empty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ModuleInProject_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_ModuleInProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ModuleIsDll_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_ModuleIsDll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ModuleIsSystem_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_ModuleIsSystem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_DebugInfoAvailable_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [out] */ BSTR *pDebugInfoFile,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_DebugInfoAvailable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_DebugInfoFile_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_DebugInfoFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_DebugInfoFile_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_DebugInfoFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ComInfoFile_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_ComInfoFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ComInfoFile_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_ComInfoFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ResetModuleInfo_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName);


void __RPC_STUB IBistroController_ResetModuleInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_PossibleInstrumentationTypes_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController_get_PossibleInstrumentationTypes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentationType_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstrumentationType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentationType_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_InstrumentationType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentedModuleName_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstrumentedModuleName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentedModuleName_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_InstrumentedModuleName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_PathToModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_PathToModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_PathToInstrumentedModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_PathToInstrumentedModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_PathToInstrumentedModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_PathToInstrumentedModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_AdditionalInstrumentorOptions_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_AdditionalInstrumentorOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_AdditionalInstrumentorOptions_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_AdditionalInstrumentorOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentationStamp_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstrumentationStamp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentationStamp_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_InstrumentationStamp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_RecordFileName_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_RecordFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_RecordFileName_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_RecordFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_RecorderOptions_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_RecorderOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_RecorderOptions_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_RecorderOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_get_Dependencies_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR FileName,
    /* [in] */ long level,
    /* [out] */ VARIANT *ModuleList,
    /* [out] */ VARIANT *FullNameList);


void __RPC_STUB IBistroController_get_Dependencies_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_NoEnvironmentProjectName_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_NoEnvironmentProjectName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_SkipModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB IBistroController_get_SkipModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_SkipModule_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BOOL newVal);


void __RPC_STUB IBistroController_put_SkipModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_CharProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR PropertyName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_CharProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_CharProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR PropertyName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_CharProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_BoolProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR PropertyName,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_BoolProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_BoolProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR PropertyName,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_BoolProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_CharModuleProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR PropertyName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_CharModuleProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_CharModuleProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR PropertyName,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_CharModuleProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_BoolModuleProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR PropertyName,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_BoolModuleProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_BoolModuleProperty_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ BSTR PropertyName,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_BoolModuleProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_UseConsole_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_UseConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_UseConsole_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_UseConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_InitConsole_Proxy( 
    IBistroController * This,
    /* [in] */ int BeforeFirstConnectionTimeout,
    /* [in] */ int AfterLastConnectionTimeout,
    /* [in] */ BSTR RunTimeProjectFullName);


void __RPC_STUB IBistroController_InitConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_CloseConsole_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_CloseConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_WaitForConsole_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL AllowWindowMessagesProcessing);


void __RPC_STUB IBistroController_WaitForConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ConsoleSendCommand_Proxy( 
    IBistroController * This,
    /* [in] */ int CommandId,
    /* [in] */ BSTR Buffer,
    /* [in] */ VARIANT TargetList,
    /* [in] */ int CommandSpecificFlags,
    /* [in] */ int ServerFlags);


void __RPC_STUB IBistroController_ConsoleSendCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ConsoleAnnotate_Proxy( 
    IBistroController * This,
    /* [in] */ int AnnotationId,
    /* [in] */ BSTR Buffer,
    /* [in] */ int ServerFlags);


void __RPC_STUB IBistroController_ConsoleAnnotate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_UseRunTimeInstrumentationThread_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController_get_UseRunTimeInstrumentationThread_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_UseRunTimeInstrumentationThread_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController_put_UseRunTimeInstrumentationThread_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_StartRunTimeInstrumentationIhread_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR RunTimeProjectFullName);


void __RPC_STUB IBistroController_StartRunTimeInstrumentationIhread_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_StopRunTimeInstrumentationThread_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_StopRunTimeInstrumentationThread_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_RunTimeInstrumentorCmdLine_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_RunTimeInstrumentorCmdLine_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_RunTimeInstrumentorCmdLine_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_RunTimeInstrumentorCmdLine_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_StartAllRunTimeServices_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR RunTimeProjectFullName);


void __RPC_STUB IBistroController_StartAllRunTimeServices_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_StopAllRunTimeServices_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_StopAllRunTimeServices_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Save_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_Save_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Restore_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ApplicationProjectFileName);


void __RPC_STUB IBistroController_Restore_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Clone_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IBistroController **ppCtrl);


void __RPC_STUB IBistroController_Clone_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Complete_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController_Complete_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Instrument_Proxy( 
    IBistroController * This,
    /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
    /* [in] */ VARIANT_BOOL ForceRebuildAll,
    /* [in] */ VARIANT_BOOL NoComplete,
    /* [in] */ VARIANT_BOOL CheckOnly,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController_Instrument_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_CancelInstrumentation_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_CancelInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_Run_Proxy( 
    IBistroController * This,
    /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
    /* [in] */ VARIANT_BOOL NotifyAboutPtocessFinish,
    /* [in] */ VARIANT_BOOL NoInstrument,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController_Run_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_WaitForApplicationFinish_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController_WaitForApplicationFinish_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_KillApplication_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL FirstTryGracefully,
    /* [in] */ VARIANT_BOOL UseMessageBoxIfRequired);


void __RPC_STUB IBistroController_KillApplication_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_PrepareForExternalRun_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR Suffix,
    /* [in] */ VARIANT_BOOL ReplaceOriginalExe,
    /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
    /* [in] */ VARIANT_BOOL NoInstrument,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController_PrepareForExternalRun_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ClearExternalRun_Proxy( 
    IBistroController * This);


void __RPC_STUB IBistroController_ClearExternalRun_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_UpdateFromExternalRun_Proxy( 
    IBistroController * This,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController_UpdateFromExternalRun_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_ClearCache_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL CurrentApplicationOnly);


void __RPC_STUB IBistroController_ClearCache_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_CompletionCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ ICompletionCallBack **pVal);


void __RPC_STUB IBistroController_get_CompletionCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_CompletionCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ ICompletionCallBack *newVal);


void __RPC_STUB IBistroController_put_CompletionCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentationCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IInstrumentationCallBack **pVal);


void __RPC_STUB IBistroController_get_InstrumentationCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentationCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ IInstrumentationCallBack *newVal);


void __RPC_STUB IBistroController_put_InstrumentationCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ProgressBarCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IProgressBarCallBack **pVal);


void __RPC_STUB IBistroController_get_ProgressBarCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ProgressBarCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ IProgressBarCallBack *newVal);


void __RPC_STUB IBistroController_put_ProgressBarCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentationStatusCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IInstrumentationStatusCallBack **pVal);


void __RPC_STUB IBistroController_get_InstrumentationStatusCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentationStatusCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ IInstrumentationStatusCallBack *newVal);


void __RPC_STUB IBistroController_put_InstrumentationStatusCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_BistroControllerInfoCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IBistroControllerInfoCallBack **pVal);


void __RPC_STUB IBistroController_get_BistroControllerInfoCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_BistroControllerInfoCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ IBistroControllerInfoCallBack *newVal);


void __RPC_STUB IBistroController_put_BistroControllerInfoCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_BistroControllerErrorCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IBistroControllerErrorCallBack **pVal);


void __RPC_STUB IBistroController_get_BistroControllerErrorCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_BistroControllerErrorCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ IBistroControllerErrorCallBack *newVal);


void __RPC_STUB IBistroController_put_BistroControllerErrorCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ConsoleVisualizeCallBack_Proxy( 
    IBistroController * This,
    /* [retval][out] */ IConsoleVisualizeCallBack **pVal);


void __RPC_STUB IBistroController_get_ConsoleVisualizeCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ConsoleVisualizeCallBack_Proxy( 
    IBistroController * This,
    /* [in] */ IConsoleVisualizeCallBack *newVal);


void __RPC_STUB IBistroController_put_ConsoleVisualizeCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_CallBacksDll_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_CallBacksDll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_CallBacksDll_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_CallBacksDll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_SetMasterProject_Proxy( 
    IBistroController * This,
    /* [in] */ long eType);


void __RPC_STUB IBistroController_SetMasterProject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_TryLong_Proxy( 
    IBistroController * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IBistroController_get_TryLong_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_TryLong_Proxy( 
    IBistroController * This,
    /* [in] */ long newVal);


void __RPC_STUB IBistroController_put_TryLong_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_InstrumentationCallBackInitString_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_InstrumentationCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_InstrumentationCallBackInitString_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_InstrumentationCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_RunCallBackInitString_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_RunCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_RunCallBackInitString_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_RunCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_PostProcessingCallBackInitString_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_PostProcessingCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_PostProcessingCallBackInitString_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_PostProcessingCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_ConsoleTransportInitString_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_ConsoleTransportInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_ConsoleTransportInitString_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_ConsoleTransportInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_CompletionCallBackInitString_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_CompletionCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_CompletionCallBackInitString_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_CompletionCallBackInitString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_SkipAllModules_Proxy( 
    IBistroController * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB IBistroController_put_SkipAllModules_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController_get_TargetMachineName_Proxy( 
    IBistroController * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController_get_TargetMachineName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController_put_TargetMachineName_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController_put_TargetMachineName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_AllowAttachDetach_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL bAllow);


void __RPC_STUB IBistroController_AllowAttachDetach_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_AllowMultiuser_Proxy( 
    IBistroController * This,
    /* [in] */ VARIANT_BOOL bAllowMultiuser);


void __RPC_STUB IBistroController_AllowMultiuser_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_SetModuleAssureFeature_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ enum ASSURE_Feature Feature,
    /* [in] */ VARIANT_BOOL State,
    /* [retval][out] */ VARIANT_BOOL *bAccepted);


void __RPC_STUB IBistroController_SetModuleAssureFeature_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController_GetModuleAssureFeature_Proxy( 
    IBistroController * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ enum ASSURE_Feature Feature,
    /* [retval][out] */ VARIANT_BOOL *State);


void __RPC_STUB IBistroController_GetModuleAssureFeature_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IBistroController_INTERFACE_DEFINED__ */


#ifndef __IBistroController2_INTERFACE_DEFINED__
#define __IBistroController2_INTERFACE_DEFINED__

/* interface IBistroController2 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IBistroController2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("46891FAA-8259-41dc-8F7F-948C29F93C2D")
    IBistroController2 : public IBistroController
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ForceInstrumentation( 
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ForceInstrumentation( 
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CBistroController( 
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLastError( 
            /* [retval][out] */ VARIANT *pPD) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBistroController2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBistroController2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBistroController2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBistroController2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBistroController2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBistroController2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBistroController2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBistroController2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstallationDirectory )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IBistroController2 * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MasterProject )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MasterProject )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationProject )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationProject )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CacheDir )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CacheDir )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentorCommandLine )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentorCommandLine )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleSystemExeInstrumentations )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleSystemDllInstrumentations )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleUserExeInstrumentations )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleUserDllInstrumentations )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultSystemExeInstrumentation )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultSystemExeInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultSystemDllInstrumentation )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultSystemDllInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultUserExeInstrumentation )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultUserExeInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultUserDllInstrumentation )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultUserDllInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinimalInstrumentationName )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NoneInstrumentationName )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedNameScheme )( 
            IBistroController2 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedNameScheme )( 
            IBistroController2 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedNameSuffix )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedNameSuffix )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedPlacementScheme )( 
            IBistroController2 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedPlacementScheme )( 
            IBistroController2 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultRecordFileName )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultRecordFileName )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultRecorderOptions )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultRecorderOptions )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InThePlaceInstrumentationAllowed )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InThePlaceInstrumentationAllowed )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AutoAddDependencies )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AutoAddDependencies )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathPrefix )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PathPrefix )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AssumeRtLibsInPath )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AssumeRtLibsInPath )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllowCancel )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllowCancel )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllowInstrumentationDegradation )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllowInstrumentationDegradation )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogFile )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogFile )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationRunningDirectory )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationRunningDirectory )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideRunTimeInstrumentationConsole )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideRunTimeInstrumentationConsole )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSeparateConsoleForCUI )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSeparateConsoleForCUI )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSeparateProcessGroup )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSeparateProcessGroup )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StaticRunTimeInstrumentationMessages )( 
            IBistroController2 * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StaticRunTimeInstrumentationMessages )( 
            IBistroController2 * This,
            /* [in] */ int newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_VerifyCacheAtRtInstrumentation )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_VerifyCacheAtRtInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Application )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationLauncher )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationLauncher )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RenameOriginalApplicationForLauncher )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RenameOriginalApplicationForLauncher )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationOptions )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationOptions )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL DependenciesAlso);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Modules )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberOfModules )( 
            IBistroController2 * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Empty )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleInProject )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleIsDll )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleIsSystem )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DebugInfoAvailable )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [out] */ BSTR *pDebugInfoFile,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DebugInfoFile )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DebugInfoFile )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ComInfoFile )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ComInfoFile )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetModuleInfo )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PossibleInstrumentationTypes )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationType )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationType )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedModuleName )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedModuleName )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathToModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathToInstrumentedModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PathToInstrumentedModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AdditionalInstrumentorOptions )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AdditionalInstrumentorOptions )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationStamp )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationStamp )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecordFileName )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecordFileName )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecorderOptions )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecorderOptions )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *get_Dependencies )( 
            IBistroController2 * This,
            /* [in] */ BSTR FileName,
            /* [in] */ long level,
            /* [out] */ VARIANT *ModuleList,
            /* [out] */ VARIANT *FullNameList);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NoEnvironmentProjectName )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SkipModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SkipModule )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BoolProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BoolProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharModuleProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharModuleProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BoolModuleProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BoolModuleProperty )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseConsole )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseConsole )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitConsole )( 
            IBistroController2 * This,
            /* [in] */ int BeforeFirstConnectionTimeout,
            /* [in] */ int AfterLastConnectionTimeout,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseConsole )( 
            IBistroController2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForConsole )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL AllowWindowMessagesProcessing);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleSendCommand )( 
            IBistroController2 * This,
            /* [in] */ int CommandId,
            /* [in] */ BSTR Buffer,
            /* [in] */ VARIANT TargetList,
            /* [in] */ int CommandSpecificFlags,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleAnnotate )( 
            IBistroController2 * This,
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseRunTimeInstrumentationThread )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseRunTimeInstrumentationThread )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartRunTimeInstrumentationIhread )( 
            IBistroController2 * This,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopRunTimeInstrumentationThread )( 
            IBistroController2 * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RunTimeInstrumentorCmdLine )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RunTimeInstrumentorCmdLine )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartAllRunTimeServices )( 
            IBistroController2 * This,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopAllRunTimeServices )( 
            IBistroController2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IBistroController2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Restore )( 
            IBistroController2 * This,
            /* [in] */ BSTR ApplicationProjectFileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IBistroController2 * This,
            /* [retval][out] */ IBistroController **ppCtrl);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Complete )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Instrument )( 
            IBistroController2 * This,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL ForceRebuildAll,
            /* [in] */ VARIANT_BOOL NoComplete,
            /* [in] */ VARIANT_BOOL CheckOnly,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CancelInstrumentation )( 
            IBistroController2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IBistroController2 * This,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NotifyAboutPtocessFinish,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForApplicationFinish )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *KillApplication )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL FirstTryGracefully,
            /* [in] */ VARIANT_BOOL UseMessageBoxIfRequired);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrepareForExternalRun )( 
            IBistroController2 * This,
            /* [in] */ BSTR Suffix,
            /* [in] */ VARIANT_BOOL ReplaceOriginalExe,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearExternalRun )( 
            IBistroController2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateFromExternalRun )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearCache )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL CurrentApplicationOnly);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompletionCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ ICompletionCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompletionCallBack )( 
            IBistroController2 * This,
            /* [in] */ ICompletionCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ IInstrumentationCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationCallBack )( 
            IBistroController2 * This,
            /* [in] */ IInstrumentationCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProgressBarCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ IProgressBarCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ProgressBarCallBack )( 
            IBistroController2 * This,
            /* [in] */ IProgressBarCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationStatusCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ IInstrumentationStatusCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationStatusCallBack )( 
            IBistroController2 * This,
            /* [in] */ IInstrumentationStatusCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroControllerInfoCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ IBistroControllerInfoCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroControllerInfoCallBack )( 
            IBistroController2 * This,
            /* [in] */ IBistroControllerInfoCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroControllerErrorCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ IBistroControllerErrorCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroControllerErrorCallBack )( 
            IBistroController2 * This,
            /* [in] */ IBistroControllerErrorCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConsoleVisualizeCallBack )( 
            IBistroController2 * This,
            /* [retval][out] */ IConsoleVisualizeCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConsoleVisualizeCallBack )( 
            IBistroController2 * This,
            /* [in] */ IConsoleVisualizeCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallBacksDll )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallBacksDll )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMasterProject )( 
            IBistroController2 * This,
            /* [in] */ long eType);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TryLong )( 
            IBistroController2 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TryLong )( 
            IBistroController2 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationCallBackInitString )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationCallBackInitString )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RunCallBackInitString )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RunCallBackInitString )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PostProcessingCallBackInitString )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PostProcessingCallBackInitString )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConsoleTransportInitString )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConsoleTransportInitString )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompletionCallBackInitString )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompletionCallBackInitString )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SkipAllModules )( 
            IBistroController2 * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TargetMachineName )( 
            IBistroController2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TargetMachineName )( 
            IBistroController2 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowAttachDetach )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL bAllow);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowMultiuser )( 
            IBistroController2 * This,
            /* [in] */ VARIANT_BOOL bAllowMultiuser);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetModuleAssureFeature )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [in] */ VARIANT_BOOL State,
            /* [retval][out] */ VARIANT_BOOL *bAccepted);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetModuleAssureFeature )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [retval][out] */ VARIANT_BOOL *State);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ForceInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ForceInstrumentation )( 
            IBistroController2 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CBistroController )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLastError )( 
            IBistroController2 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        END_INTERFACE
    } IBistroController2Vtbl;

    interface IBistroController2
    {
        CONST_VTBL struct IBistroController2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBistroController2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IBistroController2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IBistroController2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IBistroController2_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IBistroController2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IBistroController2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IBistroController2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IBistroController2_get_InstallationDirectory(This,pVal)	\
    (This)->lpVtbl -> get_InstallationDirectory(This,pVal)

#define IBistroController2_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IBistroController2_get_MasterProject(This,pVal)	\
    (This)->lpVtbl -> get_MasterProject(This,pVal)

#define IBistroController2_put_MasterProject(This,newVal)	\
    (This)->lpVtbl -> put_MasterProject(This,newVal)

#define IBistroController2_get_ApplicationProject(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationProject(This,pVal)

#define IBistroController2_put_ApplicationProject(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationProject(This,newVal)

#define IBistroController2_get_CacheDir(This,pVal)	\
    (This)->lpVtbl -> get_CacheDir(This,pVal)

#define IBistroController2_put_CacheDir(This,newVal)	\
    (This)->lpVtbl -> put_CacheDir(This,newVal)

#define IBistroController2_get_InstrumentorCommandLine(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentorCommandLine(This,pVal)

#define IBistroController2_put_InstrumentorCommandLine(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentorCommandLine(This,newVal)

#define IBistroController2_get_DefaultPossibleSystemExeInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleSystemExeInstrumentations(This,pVal)

#define IBistroController2_get_DefaultPossibleSystemDllInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleSystemDllInstrumentations(This,pVal)

#define IBistroController2_get_DefaultPossibleUserExeInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleUserExeInstrumentations(This,pVal)

#define IBistroController2_get_DefaultPossibleUserDllInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleUserDllInstrumentations(This,pVal)

#define IBistroController2_get_DefaultSystemExeInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultSystemExeInstrumentation(This,pVal)

#define IBistroController2_put_DefaultSystemExeInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultSystemExeInstrumentation(This,newVal)

#define IBistroController2_get_DefaultSystemDllInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultSystemDllInstrumentation(This,pVal)

#define IBistroController2_put_DefaultSystemDllInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultSystemDllInstrumentation(This,newVal)

#define IBistroController2_get_DefaultUserExeInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultUserExeInstrumentation(This,pVal)

#define IBistroController2_put_DefaultUserExeInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultUserExeInstrumentation(This,newVal)

#define IBistroController2_get_DefaultUserDllInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultUserDllInstrumentation(This,pVal)

#define IBistroController2_put_DefaultUserDllInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultUserDllInstrumentation(This,newVal)

#define IBistroController2_get_MinimalInstrumentationName(This,pVal)	\
    (This)->lpVtbl -> get_MinimalInstrumentationName(This,pVal)

#define IBistroController2_get_NoneInstrumentationName(This,pVal)	\
    (This)->lpVtbl -> get_NoneInstrumentationName(This,pVal)

#define IBistroController2_get_InstrumentedNameScheme(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedNameScheme(This,pVal)

#define IBistroController2_put_InstrumentedNameScheme(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedNameScheme(This,newVal)

#define IBistroController2_get_InstrumentedNameSuffix(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedNameSuffix(This,pVal)

#define IBistroController2_put_InstrumentedNameSuffix(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedNameSuffix(This,newVal)

#define IBistroController2_get_InstrumentedPlacementScheme(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedPlacementScheme(This,pVal)

#define IBistroController2_put_InstrumentedPlacementScheme(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedPlacementScheme(This,newVal)

#define IBistroController2_get_DefaultRecordFileName(This,pVal)	\
    (This)->lpVtbl -> get_DefaultRecordFileName(This,pVal)

#define IBistroController2_put_DefaultRecordFileName(This,newVal)	\
    (This)->lpVtbl -> put_DefaultRecordFileName(This,newVal)

#define IBistroController2_get_DefaultRecorderOptions(This,pVal)	\
    (This)->lpVtbl -> get_DefaultRecorderOptions(This,pVal)

#define IBistroController2_put_DefaultRecorderOptions(This,newVal)	\
    (This)->lpVtbl -> put_DefaultRecorderOptions(This,newVal)

#define IBistroController2_get_InThePlaceInstrumentationAllowed(This,pVal)	\
    (This)->lpVtbl -> get_InThePlaceInstrumentationAllowed(This,pVal)

#define IBistroController2_put_InThePlaceInstrumentationAllowed(This,newVal)	\
    (This)->lpVtbl -> put_InThePlaceInstrumentationAllowed(This,newVal)

#define IBistroController2_get_AutoAddDependencies(This,pVal)	\
    (This)->lpVtbl -> get_AutoAddDependencies(This,pVal)

#define IBistroController2_put_AutoAddDependencies(This,newVal)	\
    (This)->lpVtbl -> put_AutoAddDependencies(This,newVal)

#define IBistroController2_get_PathPrefix(This,pVal)	\
    (This)->lpVtbl -> get_PathPrefix(This,pVal)

#define IBistroController2_put_PathPrefix(This,newVal)	\
    (This)->lpVtbl -> put_PathPrefix(This,newVal)

#define IBistroController2_get_AssumeRtLibsInPath(This,pVal)	\
    (This)->lpVtbl -> get_AssumeRtLibsInPath(This,pVal)

#define IBistroController2_put_AssumeRtLibsInPath(This,newVal)	\
    (This)->lpVtbl -> put_AssumeRtLibsInPath(This,newVal)

#define IBistroController2_get_AllowCancel(This,pVal)	\
    (This)->lpVtbl -> get_AllowCancel(This,pVal)

#define IBistroController2_put_AllowCancel(This,newVal)	\
    (This)->lpVtbl -> put_AllowCancel(This,newVal)

#define IBistroController2_get_AllowInstrumentationDegradation(This,pVal)	\
    (This)->lpVtbl -> get_AllowInstrumentationDegradation(This,pVal)

#define IBistroController2_put_AllowInstrumentationDegradation(This,newVal)	\
    (This)->lpVtbl -> put_AllowInstrumentationDegradation(This,newVal)

#define IBistroController2_get_LogFile(This,pVal)	\
    (This)->lpVtbl -> get_LogFile(This,pVal)

#define IBistroController2_put_LogFile(This,newVal)	\
    (This)->lpVtbl -> put_LogFile(This,newVal)

#define IBistroController2_get_ApplicationRunningDirectory(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationRunningDirectory(This,pVal)

#define IBistroController2_put_ApplicationRunningDirectory(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationRunningDirectory(This,newVal)

#define IBistroController2_get_HideRunTimeInstrumentationConsole(This,pVal)	\
    (This)->lpVtbl -> get_HideRunTimeInstrumentationConsole(This,pVal)

#define IBistroController2_put_HideRunTimeInstrumentationConsole(This,newVal)	\
    (This)->lpVtbl -> put_HideRunTimeInstrumentationConsole(This,newVal)

#define IBistroController2_get_UseSeparateConsoleForCUI(This,pVal)	\
    (This)->lpVtbl -> get_UseSeparateConsoleForCUI(This,pVal)

#define IBistroController2_put_UseSeparateConsoleForCUI(This,newVal)	\
    (This)->lpVtbl -> put_UseSeparateConsoleForCUI(This,newVal)

#define IBistroController2_get_UseSeparateProcessGroup(This,pVal)	\
    (This)->lpVtbl -> get_UseSeparateProcessGroup(This,pVal)

#define IBistroController2_put_UseSeparateProcessGroup(This,newVal)	\
    (This)->lpVtbl -> put_UseSeparateProcessGroup(This,newVal)

#define IBistroController2_get_StaticRunTimeInstrumentationMessages(This,pVal)	\
    (This)->lpVtbl -> get_StaticRunTimeInstrumentationMessages(This,pVal)

#define IBistroController2_put_StaticRunTimeInstrumentationMessages(This,newVal)	\
    (This)->lpVtbl -> put_StaticRunTimeInstrumentationMessages(This,newVal)

#define IBistroController2_get_VerifyCacheAtRtInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_VerifyCacheAtRtInstrumentation(This,pVal)

#define IBistroController2_put_VerifyCacheAtRtInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_VerifyCacheAtRtInstrumentation(This,newVal)

#define IBistroController2_get_Application(This,pVal)	\
    (This)->lpVtbl -> get_Application(This,pVal)

#define IBistroController2_put_Application(This,newVal)	\
    (This)->lpVtbl -> put_Application(This,newVal)

#define IBistroController2_get_ApplicationLauncher(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationLauncher(This,pVal)

#define IBistroController2_put_ApplicationLauncher(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationLauncher(This,newVal)

#define IBistroController2_get_RenameOriginalApplicationForLauncher(This,pVal)	\
    (This)->lpVtbl -> get_RenameOriginalApplicationForLauncher(This,pVal)

#define IBistroController2_put_RenameOriginalApplicationForLauncher(This,newVal)	\
    (This)->lpVtbl -> put_RenameOriginalApplicationForLauncher(This,newVal)

#define IBistroController2_get_ApplicationOptions(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationOptions(This,pVal)

#define IBistroController2_put_ApplicationOptions(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationOptions(This,newVal)

#define IBistroController2_AddModule(This,FileName)	\
    (This)->lpVtbl -> AddModule(This,FileName)

#define IBistroController2_RemoveModule(This,ModuleName,DependenciesAlso)	\
    (This)->lpVtbl -> RemoveModule(This,ModuleName,DependenciesAlso)

#define IBistroController2_get_Modules(This,pVal)	\
    (This)->lpVtbl -> get_Modules(This,pVal)

#define IBistroController2_get_NumberOfModules(This,pVal)	\
    (This)->lpVtbl -> get_NumberOfModules(This,pVal)

#define IBistroController2_Empty(This,pVal)	\
    (This)->lpVtbl -> Empty(This,pVal)

#define IBistroController2_ModuleInProject(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleInProject(This,ModuleName,pVal)

#define IBistroController2_ModuleIsDll(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleIsDll(This,ModuleName,pVal)

#define IBistroController2_ModuleIsSystem(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleIsSystem(This,ModuleName,pVal)

#define IBistroController2_DebugInfoAvailable(This,ModuleName,pDebugInfoFile,pVal)	\
    (This)->lpVtbl -> DebugInfoAvailable(This,ModuleName,pDebugInfoFile,pVal)

#define IBistroController2_get_DebugInfoFile(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_DebugInfoFile(This,ModuleName,pVal)

#define IBistroController2_put_DebugInfoFile(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_DebugInfoFile(This,ModuleName,newVal)

#define IBistroController2_get_ComInfoFile(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_ComInfoFile(This,ModuleName,pVal)

#define IBistroController2_put_ComInfoFile(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_ComInfoFile(This,ModuleName,newVal)

#define IBistroController2_ResetModuleInfo(This,ModuleName)	\
    (This)->lpVtbl -> ResetModuleInfo(This,ModuleName)

#define IBistroController2_get_PossibleInstrumentationTypes(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PossibleInstrumentationTypes(This,ModuleName,pVal)

#define IBistroController2_get_InstrumentationType(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentationType(This,ModuleName,pVal)

#define IBistroController2_put_InstrumentationType(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentationType(This,ModuleName,newVal)

#define IBistroController2_get_InstrumentedModuleName(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentedModuleName(This,ModuleName,pVal)

#define IBistroController2_put_InstrumentedModuleName(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentedModuleName(This,ModuleName,newVal)

#define IBistroController2_get_PathToModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PathToModule(This,ModuleName,pVal)

#define IBistroController2_get_PathToInstrumentedModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PathToInstrumentedModule(This,ModuleName,pVal)

#define IBistroController2_put_PathToInstrumentedModule(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_PathToInstrumentedModule(This,ModuleName,newVal)

#define IBistroController2_get_AdditionalInstrumentorOptions(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_AdditionalInstrumentorOptions(This,ModuleName,pVal)

#define IBistroController2_put_AdditionalInstrumentorOptions(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_AdditionalInstrumentorOptions(This,ModuleName,newVal)

#define IBistroController2_get_InstrumentationStamp(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentationStamp(This,ModuleName,pVal)

#define IBistroController2_put_InstrumentationStamp(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentationStamp(This,ModuleName,newVal)

#define IBistroController2_get_RecordFileName(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_RecordFileName(This,ModuleName,pVal)

#define IBistroController2_put_RecordFileName(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_RecordFileName(This,ModuleName,newVal)

#define IBistroController2_get_RecorderOptions(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_RecorderOptions(This,ModuleName,pVal)

#define IBistroController2_put_RecorderOptions(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_RecorderOptions(This,ModuleName,newVal)

#define IBistroController2_get_Dependencies(This,FileName,level,ModuleList,FullNameList)	\
    (This)->lpVtbl -> get_Dependencies(This,FileName,level,ModuleList,FullNameList)

#define IBistroController2_get_NoEnvironmentProjectName(This,pVal)	\
    (This)->lpVtbl -> get_NoEnvironmentProjectName(This,pVal)

#define IBistroController2_get_SkipModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_SkipModule(This,ModuleName,pVal)

#define IBistroController2_put_SkipModule(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_SkipModule(This,ModuleName,newVal)

#define IBistroController2_get_CharProperty(This,PropertyName,pVal)	\
    (This)->lpVtbl -> get_CharProperty(This,PropertyName,pVal)

#define IBistroController2_put_CharProperty(This,PropertyName,newVal)	\
    (This)->lpVtbl -> put_CharProperty(This,PropertyName,newVal)

#define IBistroController2_get_BoolProperty(This,PropertyName,pVal)	\
    (This)->lpVtbl -> get_BoolProperty(This,PropertyName,pVal)

#define IBistroController2_put_BoolProperty(This,PropertyName,newVal)	\
    (This)->lpVtbl -> put_BoolProperty(This,PropertyName,newVal)

#define IBistroController2_get_CharModuleProperty(This,ModuleName,PropertyName,pVal)	\
    (This)->lpVtbl -> get_CharModuleProperty(This,ModuleName,PropertyName,pVal)

#define IBistroController2_put_CharModuleProperty(This,ModuleName,PropertyName,newVal)	\
    (This)->lpVtbl -> put_CharModuleProperty(This,ModuleName,PropertyName,newVal)

#define IBistroController2_get_BoolModuleProperty(This,ModuleName,PropertyName,pVal)	\
    (This)->lpVtbl -> get_BoolModuleProperty(This,ModuleName,PropertyName,pVal)

#define IBistroController2_put_BoolModuleProperty(This,ModuleName,PropertyName,newVal)	\
    (This)->lpVtbl -> put_BoolModuleProperty(This,ModuleName,PropertyName,newVal)

#define IBistroController2_get_UseConsole(This,pVal)	\
    (This)->lpVtbl -> get_UseConsole(This,pVal)

#define IBistroController2_put_UseConsole(This,newVal)	\
    (This)->lpVtbl -> put_UseConsole(This,newVal)

#define IBistroController2_InitConsole(This,BeforeFirstConnectionTimeout,AfterLastConnectionTimeout,RunTimeProjectFullName)	\
    (This)->lpVtbl -> InitConsole(This,BeforeFirstConnectionTimeout,AfterLastConnectionTimeout,RunTimeProjectFullName)

#define IBistroController2_CloseConsole(This)	\
    (This)->lpVtbl -> CloseConsole(This)

#define IBistroController2_WaitForConsole(This,AllowWindowMessagesProcessing)	\
    (This)->lpVtbl -> WaitForConsole(This,AllowWindowMessagesProcessing)

#define IBistroController2_ConsoleSendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)	\
    (This)->lpVtbl -> ConsoleSendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)

#define IBistroController2_ConsoleAnnotate(This,AnnotationId,Buffer,ServerFlags)	\
    (This)->lpVtbl -> ConsoleAnnotate(This,AnnotationId,Buffer,ServerFlags)

#define IBistroController2_get_UseRunTimeInstrumentationThread(This,pVal)	\
    (This)->lpVtbl -> get_UseRunTimeInstrumentationThread(This,pVal)

#define IBistroController2_put_UseRunTimeInstrumentationThread(This,newVal)	\
    (This)->lpVtbl -> put_UseRunTimeInstrumentationThread(This,newVal)

#define IBistroController2_StartRunTimeInstrumentationIhread(This,RunTimeProjectFullName)	\
    (This)->lpVtbl -> StartRunTimeInstrumentationIhread(This,RunTimeProjectFullName)

#define IBistroController2_StopRunTimeInstrumentationThread(This)	\
    (This)->lpVtbl -> StopRunTimeInstrumentationThread(This)

#define IBistroController2_get_RunTimeInstrumentorCmdLine(This,pVal)	\
    (This)->lpVtbl -> get_RunTimeInstrumentorCmdLine(This,pVal)

#define IBistroController2_put_RunTimeInstrumentorCmdLine(This,newVal)	\
    (This)->lpVtbl -> put_RunTimeInstrumentorCmdLine(This,newVal)

#define IBistroController2_StartAllRunTimeServices(This,RunTimeProjectFullName)	\
    (This)->lpVtbl -> StartAllRunTimeServices(This,RunTimeProjectFullName)

#define IBistroController2_StopAllRunTimeServices(This)	\
    (This)->lpVtbl -> StopAllRunTimeServices(This)

#define IBistroController2_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IBistroController2_Restore(This,ApplicationProjectFileName)	\
    (This)->lpVtbl -> Restore(This,ApplicationProjectFileName)

#define IBistroController2_Clone(This,ppCtrl)	\
    (This)->lpVtbl -> Clone(This,ppCtrl)

#define IBistroController2_Complete(This,pPD)	\
    (This)->lpVtbl -> Complete(This,pPD)

#define IBistroController2_Instrument(This,ApplicationProjectWasChanged,ForceRebuildAll,NoComplete,CheckOnly,pPD)	\
    (This)->lpVtbl -> Instrument(This,ApplicationProjectWasChanged,ForceRebuildAll,NoComplete,CheckOnly,pPD)

#define IBistroController2_CancelInstrumentation(This)	\
    (This)->lpVtbl -> CancelInstrumentation(This)

#define IBistroController2_Run(This,ApplicationProjectWasChanged,NotifyAboutPtocessFinish,NoInstrument,pPD)	\
    (This)->lpVtbl -> Run(This,ApplicationProjectWasChanged,NotifyAboutPtocessFinish,NoInstrument,pPD)

#define IBistroController2_WaitForApplicationFinish(This,pPD)	\
    (This)->lpVtbl -> WaitForApplicationFinish(This,pPD)

#define IBistroController2_KillApplication(This,FirstTryGracefully,UseMessageBoxIfRequired)	\
    (This)->lpVtbl -> KillApplication(This,FirstTryGracefully,UseMessageBoxIfRequired)

#define IBistroController2_PrepareForExternalRun(This,Suffix,ReplaceOriginalExe,ApplicationProjectWasChanged,NoInstrument,pPD)	\
    (This)->lpVtbl -> PrepareForExternalRun(This,Suffix,ReplaceOriginalExe,ApplicationProjectWasChanged,NoInstrument,pPD)

#define IBistroController2_ClearExternalRun(This)	\
    (This)->lpVtbl -> ClearExternalRun(This)

#define IBistroController2_UpdateFromExternalRun(This,pPD)	\
    (This)->lpVtbl -> UpdateFromExternalRun(This,pPD)

#define IBistroController2_ClearCache(This,CurrentApplicationOnly)	\
    (This)->lpVtbl -> ClearCache(This,CurrentApplicationOnly)

#define IBistroController2_get_CompletionCallBack(This,pVal)	\
    (This)->lpVtbl -> get_CompletionCallBack(This,pVal)

#define IBistroController2_put_CompletionCallBack(This,newVal)	\
    (This)->lpVtbl -> put_CompletionCallBack(This,newVal)

#define IBistroController2_get_InstrumentationCallBack(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationCallBack(This,pVal)

#define IBistroController2_put_InstrumentationCallBack(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationCallBack(This,newVal)

#define IBistroController2_get_ProgressBarCallBack(This,pVal)	\
    (This)->lpVtbl -> get_ProgressBarCallBack(This,pVal)

#define IBistroController2_put_ProgressBarCallBack(This,newVal)	\
    (This)->lpVtbl -> put_ProgressBarCallBack(This,newVal)

#define IBistroController2_get_InstrumentationStatusCallBack(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationStatusCallBack(This,pVal)

#define IBistroController2_put_InstrumentationStatusCallBack(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationStatusCallBack(This,newVal)

#define IBistroController2_get_BistroControllerInfoCallBack(This,pVal)	\
    (This)->lpVtbl -> get_BistroControllerInfoCallBack(This,pVal)

#define IBistroController2_put_BistroControllerInfoCallBack(This,newVal)	\
    (This)->lpVtbl -> put_BistroControllerInfoCallBack(This,newVal)

#define IBistroController2_get_BistroControllerErrorCallBack(This,pVal)	\
    (This)->lpVtbl -> get_BistroControllerErrorCallBack(This,pVal)

#define IBistroController2_put_BistroControllerErrorCallBack(This,newVal)	\
    (This)->lpVtbl -> put_BistroControllerErrorCallBack(This,newVal)

#define IBistroController2_get_ConsoleVisualizeCallBack(This,pVal)	\
    (This)->lpVtbl -> get_ConsoleVisualizeCallBack(This,pVal)

#define IBistroController2_put_ConsoleVisualizeCallBack(This,newVal)	\
    (This)->lpVtbl -> put_ConsoleVisualizeCallBack(This,newVal)

#define IBistroController2_get_CallBacksDll(This,pVal)	\
    (This)->lpVtbl -> get_CallBacksDll(This,pVal)

#define IBistroController2_put_CallBacksDll(This,newVal)	\
    (This)->lpVtbl -> put_CallBacksDll(This,newVal)

#define IBistroController2_SetMasterProject(This,eType)	\
    (This)->lpVtbl -> SetMasterProject(This,eType)

#define IBistroController2_get_TryLong(This,pVal)	\
    (This)->lpVtbl -> get_TryLong(This,pVal)

#define IBistroController2_put_TryLong(This,newVal)	\
    (This)->lpVtbl -> put_TryLong(This,newVal)

#define IBistroController2_get_InstrumentationCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationCallBackInitString(This,pVal)

#define IBistroController2_put_InstrumentationCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationCallBackInitString(This,newVal)

#define IBistroController2_get_RunCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_RunCallBackInitString(This,pVal)

#define IBistroController2_put_RunCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_RunCallBackInitString(This,newVal)

#define IBistroController2_get_PostProcessingCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_PostProcessingCallBackInitString(This,pVal)

#define IBistroController2_put_PostProcessingCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_PostProcessingCallBackInitString(This,newVal)

#define IBistroController2_get_ConsoleTransportInitString(This,pVal)	\
    (This)->lpVtbl -> get_ConsoleTransportInitString(This,pVal)

#define IBistroController2_put_ConsoleTransportInitString(This,newVal)	\
    (This)->lpVtbl -> put_ConsoleTransportInitString(This,newVal)

#define IBistroController2_get_CompletionCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_CompletionCallBackInitString(This,pVal)

#define IBistroController2_put_CompletionCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_CompletionCallBackInitString(This,newVal)

#define IBistroController2_put_SkipAllModules(This,newVal)	\
    (This)->lpVtbl -> put_SkipAllModules(This,newVal)

#define IBistroController2_get_TargetMachineName(This,pVal)	\
    (This)->lpVtbl -> get_TargetMachineName(This,pVal)

#define IBistroController2_put_TargetMachineName(This,newVal)	\
    (This)->lpVtbl -> put_TargetMachineName(This,newVal)

#define IBistroController2_AllowAttachDetach(This,bAllow)	\
    (This)->lpVtbl -> AllowAttachDetach(This,bAllow)

#define IBistroController2_AllowMultiuser(This,bAllowMultiuser)	\
    (This)->lpVtbl -> AllowMultiuser(This,bAllowMultiuser)

#define IBistroController2_SetModuleAssureFeature(This,ModuleName,Feature,State,bAccepted)	\
    (This)->lpVtbl -> SetModuleAssureFeature(This,ModuleName,Feature,State,bAccepted)

#define IBistroController2_GetModuleAssureFeature(This,ModuleName,Feature,State)	\
    (This)->lpVtbl -> GetModuleAssureFeature(This,ModuleName,Feature,State)


#define IBistroController2_get_ForceInstrumentation(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_ForceInstrumentation(This,ModuleName,pVal)

#define IBistroController2_put_ForceInstrumentation(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_ForceInstrumentation(This,ModuleName,newVal)

#define IBistroController2_get_CBistroController(This,pVal)	\
    (This)->lpVtbl -> get_CBistroController(This,pVal)

#define IBistroController2_GetLastError(This,pPD)	\
    (This)->lpVtbl -> GetLastError(This,pPD)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController2_get_ForceInstrumentation_Proxy( 
    IBistroController2 * This,
    /* [in] */ BSTR ModuleName,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IBistroController2_get_ForceInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController2_put_ForceInstrumentation_Proxy( 
    IBistroController2 * This,
    /* [in] */ BSTR ModuleName,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IBistroController2_put_ForceInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController2_get_CBistroController_Proxy( 
    IBistroController2 * This,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IBistroController2_get_CBistroController_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController2_GetLastError_Proxy( 
    IBistroController2 * This,
    /* [retval][out] */ VARIANT *pPD);


void __RPC_STUB IBistroController2_GetLastError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IBistroController2_INTERFACE_DEFINED__ */


#ifndef __IBistroController3_INTERFACE_DEFINED__
#define __IBistroController3_INTERFACE_DEFINED__

/* interface IBistroController3 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IBistroController3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B71F4DA2-9281-457a-BF2C-8F4F620984FE")
    IBistroController3 : public IBistroController2
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AlternatePath( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AlternatePath( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDefaultSystemDllInstrumentation( 
            /* [in] */ BSTR newVal,
            /* [in] */ BOOL forNewModulesOnly) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDefaultUserExeInstrumentation( 
            /* [in] */ BSTR newVal,
            /* [in] */ BOOL forNewModulesOnly) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDefaultUserDllInstrumentation( 
            /* [in] */ BSTR newVal,
            /* [in] */ BOOL forNewModulesOnly) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBistroController3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBistroController3 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBistroController3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBistroController3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBistroController3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBistroController3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBistroController3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBistroController3 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstallationDirectory )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IBistroController3 * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MasterProject )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MasterProject )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationProject )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationProject )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CacheDir )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CacheDir )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentorCommandLine )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentorCommandLine )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleSystemExeInstrumentations )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleSystemDllInstrumentations )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleUserExeInstrumentations )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultPossibleUserDllInstrumentations )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultSystemExeInstrumentation )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultSystemExeInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultSystemDllInstrumentation )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultSystemDllInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultUserExeInstrumentation )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultUserExeInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultUserDllInstrumentation )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultUserDllInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinimalInstrumentationName )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NoneInstrumentationName )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedNameScheme )( 
            IBistroController3 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedNameScheme )( 
            IBistroController3 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedNameSuffix )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedNameSuffix )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedPlacementScheme )( 
            IBistroController3 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedPlacementScheme )( 
            IBistroController3 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultRecordFileName )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultRecordFileName )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultRecorderOptions )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultRecorderOptions )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InThePlaceInstrumentationAllowed )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InThePlaceInstrumentationAllowed )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AutoAddDependencies )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AutoAddDependencies )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathPrefix )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PathPrefix )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AssumeRtLibsInPath )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AssumeRtLibsInPath )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllowCancel )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllowCancel )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AllowInstrumentationDegradation )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AllowInstrumentationDegradation )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogFile )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogFile )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationRunningDirectory )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationRunningDirectory )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideRunTimeInstrumentationConsole )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideRunTimeInstrumentationConsole )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSeparateConsoleForCUI )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSeparateConsoleForCUI )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSeparateProcessGroup )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSeparateProcessGroup )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StaticRunTimeInstrumentationMessages )( 
            IBistroController3 * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StaticRunTimeInstrumentationMessages )( 
            IBistroController3 * This,
            /* [in] */ int newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_VerifyCacheAtRtInstrumentation )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_VerifyCacheAtRtInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Application )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationLauncher )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationLauncher )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RenameOriginalApplicationForLauncher )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RenameOriginalApplicationForLauncher )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationOptions )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationOptions )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL DependenciesAlso);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Modules )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberOfModules )( 
            IBistroController3 * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Empty )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleInProject )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleIsDll )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModuleIsSystem )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DebugInfoAvailable )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [out] */ BSTR *pDebugInfoFile,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DebugInfoFile )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DebugInfoFile )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ComInfoFile )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ComInfoFile )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetModuleInfo )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PossibleInstrumentationTypes )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationType )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationType )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentedModuleName )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentedModuleName )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathToModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PathToInstrumentedModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PathToInstrumentedModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AdditionalInstrumentorOptions )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AdditionalInstrumentorOptions )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationStamp )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationStamp )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecordFileName )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecordFileName )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecorderOptions )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecorderOptions )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *get_Dependencies )( 
            IBistroController3 * This,
            /* [in] */ BSTR FileName,
            /* [in] */ long level,
            /* [out] */ VARIANT *ModuleList,
            /* [out] */ VARIANT *FullNameList);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NoEnvironmentProjectName )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SkipModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SkipModule )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BoolProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BoolProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharModuleProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharModuleProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BoolModuleProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BoolModuleProperty )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ BSTR PropertyName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseConsole )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseConsole )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitConsole )( 
            IBistroController3 * This,
            /* [in] */ int BeforeFirstConnectionTimeout,
            /* [in] */ int AfterLastConnectionTimeout,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseConsole )( 
            IBistroController3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForConsole )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL AllowWindowMessagesProcessing);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleSendCommand )( 
            IBistroController3 * This,
            /* [in] */ int CommandId,
            /* [in] */ BSTR Buffer,
            /* [in] */ VARIANT TargetList,
            /* [in] */ int CommandSpecificFlags,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleAnnotate )( 
            IBistroController3 * This,
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseRunTimeInstrumentationThread )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseRunTimeInstrumentationThread )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartRunTimeInstrumentationIhread )( 
            IBistroController3 * This,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopRunTimeInstrumentationThread )( 
            IBistroController3 * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RunTimeInstrumentorCmdLine )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RunTimeInstrumentorCmdLine )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartAllRunTimeServices )( 
            IBistroController3 * This,
            /* [in] */ BSTR RunTimeProjectFullName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopAllRunTimeServices )( 
            IBistroController3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IBistroController3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Restore )( 
            IBistroController3 * This,
            /* [in] */ BSTR ApplicationProjectFileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IBistroController3 * This,
            /* [retval][out] */ IBistroController **ppCtrl);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Complete )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Instrument )( 
            IBistroController3 * This,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL ForceRebuildAll,
            /* [in] */ VARIANT_BOOL NoComplete,
            /* [in] */ VARIANT_BOOL CheckOnly,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CancelInstrumentation )( 
            IBistroController3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IBistroController3 * This,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NotifyAboutPtocessFinish,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForApplicationFinish )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *KillApplication )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL FirstTryGracefully,
            /* [in] */ VARIANT_BOOL UseMessageBoxIfRequired);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrepareForExternalRun )( 
            IBistroController3 * This,
            /* [in] */ BSTR Suffix,
            /* [in] */ VARIANT_BOOL ReplaceOriginalExe,
            /* [out] */ VARIANT_BOOL *ApplicationProjectWasChanged,
            /* [in] */ VARIANT_BOOL NoInstrument,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearExternalRun )( 
            IBistroController3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateFromExternalRun )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearCache )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL CurrentApplicationOnly);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompletionCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ ICompletionCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompletionCallBack )( 
            IBistroController3 * This,
            /* [in] */ ICompletionCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ IInstrumentationCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationCallBack )( 
            IBistroController3 * This,
            /* [in] */ IInstrumentationCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProgressBarCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ IProgressBarCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ProgressBarCallBack )( 
            IBistroController3 * This,
            /* [in] */ IProgressBarCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationStatusCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ IInstrumentationStatusCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationStatusCallBack )( 
            IBistroController3 * This,
            /* [in] */ IInstrumentationStatusCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroControllerInfoCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ IBistroControllerInfoCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroControllerInfoCallBack )( 
            IBistroController3 * This,
            /* [in] */ IBistroControllerInfoCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroControllerErrorCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ IBistroControllerErrorCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroControllerErrorCallBack )( 
            IBistroController3 * This,
            /* [in] */ IBistroControllerErrorCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConsoleVisualizeCallBack )( 
            IBistroController3 * This,
            /* [retval][out] */ IConsoleVisualizeCallBack **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConsoleVisualizeCallBack )( 
            IBistroController3 * This,
            /* [in] */ IConsoleVisualizeCallBack *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallBacksDll )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallBacksDll )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMasterProject )( 
            IBistroController3 * This,
            /* [in] */ long eType);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TryLong )( 
            IBistroController3 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TryLong )( 
            IBistroController3 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationCallBackInitString )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationCallBackInitString )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RunCallBackInitString )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RunCallBackInitString )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PostProcessingCallBackInitString )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PostProcessingCallBackInitString )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConsoleTransportInitString )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConsoleTransportInitString )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompletionCallBackInitString )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompletionCallBackInitString )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SkipAllModules )( 
            IBistroController3 * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TargetMachineName )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TargetMachineName )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowAttachDetach )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL bAllow);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowMultiuser )( 
            IBistroController3 * This,
            /* [in] */ VARIANT_BOOL bAllowMultiuser);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetModuleAssureFeature )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [in] */ VARIANT_BOOL State,
            /* [retval][out] */ VARIANT_BOOL *bAccepted);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetModuleAssureFeature )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ enum ASSURE_Feature Feature,
            /* [retval][out] */ VARIANT_BOOL *State);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ForceInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ForceInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR ModuleName,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CBistroController )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLastError )( 
            IBistroController3 * This,
            /* [retval][out] */ VARIANT *pPD);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AlternatePath )( 
            IBistroController3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AlternatePath )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDefaultSystemDllInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal,
            /* [in] */ BOOL forNewModulesOnly);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDefaultUserExeInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal,
            /* [in] */ BOOL forNewModulesOnly);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDefaultUserDllInstrumentation )( 
            IBistroController3 * This,
            /* [in] */ BSTR newVal,
            /* [in] */ BOOL forNewModulesOnly);
        
        END_INTERFACE
    } IBistroController3Vtbl;

    interface IBistroController3
    {
        CONST_VTBL struct IBistroController3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBistroController3_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IBistroController3_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IBistroController3_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IBistroController3_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IBistroController3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IBistroController3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IBistroController3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IBistroController3_get_InstallationDirectory(This,pVal)	\
    (This)->lpVtbl -> get_InstallationDirectory(This,pVal)

#define IBistroController3_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IBistroController3_get_MasterProject(This,pVal)	\
    (This)->lpVtbl -> get_MasterProject(This,pVal)

#define IBistroController3_put_MasterProject(This,newVal)	\
    (This)->lpVtbl -> put_MasterProject(This,newVal)

#define IBistroController3_get_ApplicationProject(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationProject(This,pVal)

#define IBistroController3_put_ApplicationProject(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationProject(This,newVal)

#define IBistroController3_get_CacheDir(This,pVal)	\
    (This)->lpVtbl -> get_CacheDir(This,pVal)

#define IBistroController3_put_CacheDir(This,newVal)	\
    (This)->lpVtbl -> put_CacheDir(This,newVal)

#define IBistroController3_get_InstrumentorCommandLine(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentorCommandLine(This,pVal)

#define IBistroController3_put_InstrumentorCommandLine(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentorCommandLine(This,newVal)

#define IBistroController3_get_DefaultPossibleSystemExeInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleSystemExeInstrumentations(This,pVal)

#define IBistroController3_get_DefaultPossibleSystemDllInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleSystemDllInstrumentations(This,pVal)

#define IBistroController3_get_DefaultPossibleUserExeInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleUserExeInstrumentations(This,pVal)

#define IBistroController3_get_DefaultPossibleUserDllInstrumentations(This,pVal)	\
    (This)->lpVtbl -> get_DefaultPossibleUserDllInstrumentations(This,pVal)

#define IBistroController3_get_DefaultSystemExeInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultSystemExeInstrumentation(This,pVal)

#define IBistroController3_put_DefaultSystemExeInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultSystemExeInstrumentation(This,newVal)

#define IBistroController3_get_DefaultSystemDllInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultSystemDllInstrumentation(This,pVal)

#define IBistroController3_put_DefaultSystemDllInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultSystemDllInstrumentation(This,newVal)

#define IBistroController3_get_DefaultUserExeInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultUserExeInstrumentation(This,pVal)

#define IBistroController3_put_DefaultUserExeInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultUserExeInstrumentation(This,newVal)

#define IBistroController3_get_DefaultUserDllInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_DefaultUserDllInstrumentation(This,pVal)

#define IBistroController3_put_DefaultUserDllInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_DefaultUserDllInstrumentation(This,newVal)

#define IBistroController3_get_MinimalInstrumentationName(This,pVal)	\
    (This)->lpVtbl -> get_MinimalInstrumentationName(This,pVal)

#define IBistroController3_get_NoneInstrumentationName(This,pVal)	\
    (This)->lpVtbl -> get_NoneInstrumentationName(This,pVal)

#define IBistroController3_get_InstrumentedNameScheme(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedNameScheme(This,pVal)

#define IBistroController3_put_InstrumentedNameScheme(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedNameScheme(This,newVal)

#define IBistroController3_get_InstrumentedNameSuffix(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedNameSuffix(This,pVal)

#define IBistroController3_put_InstrumentedNameSuffix(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedNameSuffix(This,newVal)

#define IBistroController3_get_InstrumentedPlacementScheme(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentedPlacementScheme(This,pVal)

#define IBistroController3_put_InstrumentedPlacementScheme(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentedPlacementScheme(This,newVal)

#define IBistroController3_get_DefaultRecordFileName(This,pVal)	\
    (This)->lpVtbl -> get_DefaultRecordFileName(This,pVal)

#define IBistroController3_put_DefaultRecordFileName(This,newVal)	\
    (This)->lpVtbl -> put_DefaultRecordFileName(This,newVal)

#define IBistroController3_get_DefaultRecorderOptions(This,pVal)	\
    (This)->lpVtbl -> get_DefaultRecorderOptions(This,pVal)

#define IBistroController3_put_DefaultRecorderOptions(This,newVal)	\
    (This)->lpVtbl -> put_DefaultRecorderOptions(This,newVal)

#define IBistroController3_get_InThePlaceInstrumentationAllowed(This,pVal)	\
    (This)->lpVtbl -> get_InThePlaceInstrumentationAllowed(This,pVal)

#define IBistroController3_put_InThePlaceInstrumentationAllowed(This,newVal)	\
    (This)->lpVtbl -> put_InThePlaceInstrumentationAllowed(This,newVal)

#define IBistroController3_get_AutoAddDependencies(This,pVal)	\
    (This)->lpVtbl -> get_AutoAddDependencies(This,pVal)

#define IBistroController3_put_AutoAddDependencies(This,newVal)	\
    (This)->lpVtbl -> put_AutoAddDependencies(This,newVal)

#define IBistroController3_get_PathPrefix(This,pVal)	\
    (This)->lpVtbl -> get_PathPrefix(This,pVal)

#define IBistroController3_put_PathPrefix(This,newVal)	\
    (This)->lpVtbl -> put_PathPrefix(This,newVal)

#define IBistroController3_get_AssumeRtLibsInPath(This,pVal)	\
    (This)->lpVtbl -> get_AssumeRtLibsInPath(This,pVal)

#define IBistroController3_put_AssumeRtLibsInPath(This,newVal)	\
    (This)->lpVtbl -> put_AssumeRtLibsInPath(This,newVal)

#define IBistroController3_get_AllowCancel(This,pVal)	\
    (This)->lpVtbl -> get_AllowCancel(This,pVal)

#define IBistroController3_put_AllowCancel(This,newVal)	\
    (This)->lpVtbl -> put_AllowCancel(This,newVal)

#define IBistroController3_get_AllowInstrumentationDegradation(This,pVal)	\
    (This)->lpVtbl -> get_AllowInstrumentationDegradation(This,pVal)

#define IBistroController3_put_AllowInstrumentationDegradation(This,newVal)	\
    (This)->lpVtbl -> put_AllowInstrumentationDegradation(This,newVal)

#define IBistroController3_get_LogFile(This,pVal)	\
    (This)->lpVtbl -> get_LogFile(This,pVal)

#define IBistroController3_put_LogFile(This,newVal)	\
    (This)->lpVtbl -> put_LogFile(This,newVal)

#define IBistroController3_get_ApplicationRunningDirectory(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationRunningDirectory(This,pVal)

#define IBistroController3_put_ApplicationRunningDirectory(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationRunningDirectory(This,newVal)

#define IBistroController3_get_HideRunTimeInstrumentationConsole(This,pVal)	\
    (This)->lpVtbl -> get_HideRunTimeInstrumentationConsole(This,pVal)

#define IBistroController3_put_HideRunTimeInstrumentationConsole(This,newVal)	\
    (This)->lpVtbl -> put_HideRunTimeInstrumentationConsole(This,newVal)

#define IBistroController3_get_UseSeparateConsoleForCUI(This,pVal)	\
    (This)->lpVtbl -> get_UseSeparateConsoleForCUI(This,pVal)

#define IBistroController3_put_UseSeparateConsoleForCUI(This,newVal)	\
    (This)->lpVtbl -> put_UseSeparateConsoleForCUI(This,newVal)

#define IBistroController3_get_UseSeparateProcessGroup(This,pVal)	\
    (This)->lpVtbl -> get_UseSeparateProcessGroup(This,pVal)

#define IBistroController3_put_UseSeparateProcessGroup(This,newVal)	\
    (This)->lpVtbl -> put_UseSeparateProcessGroup(This,newVal)

#define IBistroController3_get_StaticRunTimeInstrumentationMessages(This,pVal)	\
    (This)->lpVtbl -> get_StaticRunTimeInstrumentationMessages(This,pVal)

#define IBistroController3_put_StaticRunTimeInstrumentationMessages(This,newVal)	\
    (This)->lpVtbl -> put_StaticRunTimeInstrumentationMessages(This,newVal)

#define IBistroController3_get_VerifyCacheAtRtInstrumentation(This,pVal)	\
    (This)->lpVtbl -> get_VerifyCacheAtRtInstrumentation(This,pVal)

#define IBistroController3_put_VerifyCacheAtRtInstrumentation(This,newVal)	\
    (This)->lpVtbl -> put_VerifyCacheAtRtInstrumentation(This,newVal)

#define IBistroController3_get_Application(This,pVal)	\
    (This)->lpVtbl -> get_Application(This,pVal)

#define IBistroController3_put_Application(This,newVal)	\
    (This)->lpVtbl -> put_Application(This,newVal)

#define IBistroController3_get_ApplicationLauncher(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationLauncher(This,pVal)

#define IBistroController3_put_ApplicationLauncher(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationLauncher(This,newVal)

#define IBistroController3_get_RenameOriginalApplicationForLauncher(This,pVal)	\
    (This)->lpVtbl -> get_RenameOriginalApplicationForLauncher(This,pVal)

#define IBistroController3_put_RenameOriginalApplicationForLauncher(This,newVal)	\
    (This)->lpVtbl -> put_RenameOriginalApplicationForLauncher(This,newVal)

#define IBistroController3_get_ApplicationOptions(This,pVal)	\
    (This)->lpVtbl -> get_ApplicationOptions(This,pVal)

#define IBistroController3_put_ApplicationOptions(This,newVal)	\
    (This)->lpVtbl -> put_ApplicationOptions(This,newVal)

#define IBistroController3_AddModule(This,FileName)	\
    (This)->lpVtbl -> AddModule(This,FileName)

#define IBistroController3_RemoveModule(This,ModuleName,DependenciesAlso)	\
    (This)->lpVtbl -> RemoveModule(This,ModuleName,DependenciesAlso)

#define IBistroController3_get_Modules(This,pVal)	\
    (This)->lpVtbl -> get_Modules(This,pVal)

#define IBistroController3_get_NumberOfModules(This,pVal)	\
    (This)->lpVtbl -> get_NumberOfModules(This,pVal)

#define IBistroController3_Empty(This,pVal)	\
    (This)->lpVtbl -> Empty(This,pVal)

#define IBistroController3_ModuleInProject(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleInProject(This,ModuleName,pVal)

#define IBistroController3_ModuleIsDll(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleIsDll(This,ModuleName,pVal)

#define IBistroController3_ModuleIsSystem(This,ModuleName,pVal)	\
    (This)->lpVtbl -> ModuleIsSystem(This,ModuleName,pVal)

#define IBistroController3_DebugInfoAvailable(This,ModuleName,pDebugInfoFile,pVal)	\
    (This)->lpVtbl -> DebugInfoAvailable(This,ModuleName,pDebugInfoFile,pVal)

#define IBistroController3_get_DebugInfoFile(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_DebugInfoFile(This,ModuleName,pVal)

#define IBistroController3_put_DebugInfoFile(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_DebugInfoFile(This,ModuleName,newVal)

#define IBistroController3_get_ComInfoFile(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_ComInfoFile(This,ModuleName,pVal)

#define IBistroController3_put_ComInfoFile(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_ComInfoFile(This,ModuleName,newVal)

#define IBistroController3_ResetModuleInfo(This,ModuleName)	\
    (This)->lpVtbl -> ResetModuleInfo(This,ModuleName)

#define IBistroController3_get_PossibleInstrumentationTypes(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PossibleInstrumentationTypes(This,ModuleName,pVal)

#define IBistroController3_get_InstrumentationType(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentationType(This,ModuleName,pVal)

#define IBistroController3_put_InstrumentationType(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentationType(This,ModuleName,newVal)

#define IBistroController3_get_InstrumentedModuleName(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentedModuleName(This,ModuleName,pVal)

#define IBistroController3_put_InstrumentedModuleName(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentedModuleName(This,ModuleName,newVal)

#define IBistroController3_get_PathToModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PathToModule(This,ModuleName,pVal)

#define IBistroController3_get_PathToInstrumentedModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_PathToInstrumentedModule(This,ModuleName,pVal)

#define IBistroController3_put_PathToInstrumentedModule(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_PathToInstrumentedModule(This,ModuleName,newVal)

#define IBistroController3_get_AdditionalInstrumentorOptions(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_AdditionalInstrumentorOptions(This,ModuleName,pVal)

#define IBistroController3_put_AdditionalInstrumentorOptions(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_AdditionalInstrumentorOptions(This,ModuleName,newVal)

#define IBistroController3_get_InstrumentationStamp(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_InstrumentationStamp(This,ModuleName,pVal)

#define IBistroController3_put_InstrumentationStamp(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_InstrumentationStamp(This,ModuleName,newVal)

#define IBistroController3_get_RecordFileName(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_RecordFileName(This,ModuleName,pVal)

#define IBistroController3_put_RecordFileName(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_RecordFileName(This,ModuleName,newVal)

#define IBistroController3_get_RecorderOptions(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_RecorderOptions(This,ModuleName,pVal)

#define IBistroController3_put_RecorderOptions(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_RecorderOptions(This,ModuleName,newVal)

#define IBistroController3_get_Dependencies(This,FileName,level,ModuleList,FullNameList)	\
    (This)->lpVtbl -> get_Dependencies(This,FileName,level,ModuleList,FullNameList)

#define IBistroController3_get_NoEnvironmentProjectName(This,pVal)	\
    (This)->lpVtbl -> get_NoEnvironmentProjectName(This,pVal)

#define IBistroController3_get_SkipModule(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_SkipModule(This,ModuleName,pVal)

#define IBistroController3_put_SkipModule(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_SkipModule(This,ModuleName,newVal)

#define IBistroController3_get_CharProperty(This,PropertyName,pVal)	\
    (This)->lpVtbl -> get_CharProperty(This,PropertyName,pVal)

#define IBistroController3_put_CharProperty(This,PropertyName,newVal)	\
    (This)->lpVtbl -> put_CharProperty(This,PropertyName,newVal)

#define IBistroController3_get_BoolProperty(This,PropertyName,pVal)	\
    (This)->lpVtbl -> get_BoolProperty(This,PropertyName,pVal)

#define IBistroController3_put_BoolProperty(This,PropertyName,newVal)	\
    (This)->lpVtbl -> put_BoolProperty(This,PropertyName,newVal)

#define IBistroController3_get_CharModuleProperty(This,ModuleName,PropertyName,pVal)	\
    (This)->lpVtbl -> get_CharModuleProperty(This,ModuleName,PropertyName,pVal)

#define IBistroController3_put_CharModuleProperty(This,ModuleName,PropertyName,newVal)	\
    (This)->lpVtbl -> put_CharModuleProperty(This,ModuleName,PropertyName,newVal)

#define IBistroController3_get_BoolModuleProperty(This,ModuleName,PropertyName,pVal)	\
    (This)->lpVtbl -> get_BoolModuleProperty(This,ModuleName,PropertyName,pVal)

#define IBistroController3_put_BoolModuleProperty(This,ModuleName,PropertyName,newVal)	\
    (This)->lpVtbl -> put_BoolModuleProperty(This,ModuleName,PropertyName,newVal)

#define IBistroController3_get_UseConsole(This,pVal)	\
    (This)->lpVtbl -> get_UseConsole(This,pVal)

#define IBistroController3_put_UseConsole(This,newVal)	\
    (This)->lpVtbl -> put_UseConsole(This,newVal)

#define IBistroController3_InitConsole(This,BeforeFirstConnectionTimeout,AfterLastConnectionTimeout,RunTimeProjectFullName)	\
    (This)->lpVtbl -> InitConsole(This,BeforeFirstConnectionTimeout,AfterLastConnectionTimeout,RunTimeProjectFullName)

#define IBistroController3_CloseConsole(This)	\
    (This)->lpVtbl -> CloseConsole(This)

#define IBistroController3_WaitForConsole(This,AllowWindowMessagesProcessing)	\
    (This)->lpVtbl -> WaitForConsole(This,AllowWindowMessagesProcessing)

#define IBistroController3_ConsoleSendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)	\
    (This)->lpVtbl -> ConsoleSendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)

#define IBistroController3_ConsoleAnnotate(This,AnnotationId,Buffer,ServerFlags)	\
    (This)->lpVtbl -> ConsoleAnnotate(This,AnnotationId,Buffer,ServerFlags)

#define IBistroController3_get_UseRunTimeInstrumentationThread(This,pVal)	\
    (This)->lpVtbl -> get_UseRunTimeInstrumentationThread(This,pVal)

#define IBistroController3_put_UseRunTimeInstrumentationThread(This,newVal)	\
    (This)->lpVtbl -> put_UseRunTimeInstrumentationThread(This,newVal)

#define IBistroController3_StartRunTimeInstrumentationIhread(This,RunTimeProjectFullName)	\
    (This)->lpVtbl -> StartRunTimeInstrumentationIhread(This,RunTimeProjectFullName)

#define IBistroController3_StopRunTimeInstrumentationThread(This)	\
    (This)->lpVtbl -> StopRunTimeInstrumentationThread(This)

#define IBistroController3_get_RunTimeInstrumentorCmdLine(This,pVal)	\
    (This)->lpVtbl -> get_RunTimeInstrumentorCmdLine(This,pVal)

#define IBistroController3_put_RunTimeInstrumentorCmdLine(This,newVal)	\
    (This)->lpVtbl -> put_RunTimeInstrumentorCmdLine(This,newVal)

#define IBistroController3_StartAllRunTimeServices(This,RunTimeProjectFullName)	\
    (This)->lpVtbl -> StartAllRunTimeServices(This,RunTimeProjectFullName)

#define IBistroController3_StopAllRunTimeServices(This)	\
    (This)->lpVtbl -> StopAllRunTimeServices(This)

#define IBistroController3_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IBistroController3_Restore(This,ApplicationProjectFileName)	\
    (This)->lpVtbl -> Restore(This,ApplicationProjectFileName)

#define IBistroController3_Clone(This,ppCtrl)	\
    (This)->lpVtbl -> Clone(This,ppCtrl)

#define IBistroController3_Complete(This,pPD)	\
    (This)->lpVtbl -> Complete(This,pPD)

#define IBistroController3_Instrument(This,ApplicationProjectWasChanged,ForceRebuildAll,NoComplete,CheckOnly,pPD)	\
    (This)->lpVtbl -> Instrument(This,ApplicationProjectWasChanged,ForceRebuildAll,NoComplete,CheckOnly,pPD)

#define IBistroController3_CancelInstrumentation(This)	\
    (This)->lpVtbl -> CancelInstrumentation(This)

#define IBistroController3_Run(This,ApplicationProjectWasChanged,NotifyAboutPtocessFinish,NoInstrument,pPD)	\
    (This)->lpVtbl -> Run(This,ApplicationProjectWasChanged,NotifyAboutPtocessFinish,NoInstrument,pPD)

#define IBistroController3_WaitForApplicationFinish(This,pPD)	\
    (This)->lpVtbl -> WaitForApplicationFinish(This,pPD)

#define IBistroController3_KillApplication(This,FirstTryGracefully,UseMessageBoxIfRequired)	\
    (This)->lpVtbl -> KillApplication(This,FirstTryGracefully,UseMessageBoxIfRequired)

#define IBistroController3_PrepareForExternalRun(This,Suffix,ReplaceOriginalExe,ApplicationProjectWasChanged,NoInstrument,pPD)	\
    (This)->lpVtbl -> PrepareForExternalRun(This,Suffix,ReplaceOriginalExe,ApplicationProjectWasChanged,NoInstrument,pPD)

#define IBistroController3_ClearExternalRun(This)	\
    (This)->lpVtbl -> ClearExternalRun(This)

#define IBistroController3_UpdateFromExternalRun(This,pPD)	\
    (This)->lpVtbl -> UpdateFromExternalRun(This,pPD)

#define IBistroController3_ClearCache(This,CurrentApplicationOnly)	\
    (This)->lpVtbl -> ClearCache(This,CurrentApplicationOnly)

#define IBistroController3_get_CompletionCallBack(This,pVal)	\
    (This)->lpVtbl -> get_CompletionCallBack(This,pVal)

#define IBistroController3_put_CompletionCallBack(This,newVal)	\
    (This)->lpVtbl -> put_CompletionCallBack(This,newVal)

#define IBistroController3_get_InstrumentationCallBack(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationCallBack(This,pVal)

#define IBistroController3_put_InstrumentationCallBack(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationCallBack(This,newVal)

#define IBistroController3_get_ProgressBarCallBack(This,pVal)	\
    (This)->lpVtbl -> get_ProgressBarCallBack(This,pVal)

#define IBistroController3_put_ProgressBarCallBack(This,newVal)	\
    (This)->lpVtbl -> put_ProgressBarCallBack(This,newVal)

#define IBistroController3_get_InstrumentationStatusCallBack(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationStatusCallBack(This,pVal)

#define IBistroController3_put_InstrumentationStatusCallBack(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationStatusCallBack(This,newVal)

#define IBistroController3_get_BistroControllerInfoCallBack(This,pVal)	\
    (This)->lpVtbl -> get_BistroControllerInfoCallBack(This,pVal)

#define IBistroController3_put_BistroControllerInfoCallBack(This,newVal)	\
    (This)->lpVtbl -> put_BistroControllerInfoCallBack(This,newVal)

#define IBistroController3_get_BistroControllerErrorCallBack(This,pVal)	\
    (This)->lpVtbl -> get_BistroControllerErrorCallBack(This,pVal)

#define IBistroController3_put_BistroControllerErrorCallBack(This,newVal)	\
    (This)->lpVtbl -> put_BistroControllerErrorCallBack(This,newVal)

#define IBistroController3_get_ConsoleVisualizeCallBack(This,pVal)	\
    (This)->lpVtbl -> get_ConsoleVisualizeCallBack(This,pVal)

#define IBistroController3_put_ConsoleVisualizeCallBack(This,newVal)	\
    (This)->lpVtbl -> put_ConsoleVisualizeCallBack(This,newVal)

#define IBistroController3_get_CallBacksDll(This,pVal)	\
    (This)->lpVtbl -> get_CallBacksDll(This,pVal)

#define IBistroController3_put_CallBacksDll(This,newVal)	\
    (This)->lpVtbl -> put_CallBacksDll(This,newVal)

#define IBistroController3_SetMasterProject(This,eType)	\
    (This)->lpVtbl -> SetMasterProject(This,eType)

#define IBistroController3_get_TryLong(This,pVal)	\
    (This)->lpVtbl -> get_TryLong(This,pVal)

#define IBistroController3_put_TryLong(This,newVal)	\
    (This)->lpVtbl -> put_TryLong(This,newVal)

#define IBistroController3_get_InstrumentationCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationCallBackInitString(This,pVal)

#define IBistroController3_put_InstrumentationCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationCallBackInitString(This,newVal)

#define IBistroController3_get_RunCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_RunCallBackInitString(This,pVal)

#define IBistroController3_put_RunCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_RunCallBackInitString(This,newVal)

#define IBistroController3_get_PostProcessingCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_PostProcessingCallBackInitString(This,pVal)

#define IBistroController3_put_PostProcessingCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_PostProcessingCallBackInitString(This,newVal)

#define IBistroController3_get_ConsoleTransportInitString(This,pVal)	\
    (This)->lpVtbl -> get_ConsoleTransportInitString(This,pVal)

#define IBistroController3_put_ConsoleTransportInitString(This,newVal)	\
    (This)->lpVtbl -> put_ConsoleTransportInitString(This,newVal)

#define IBistroController3_get_CompletionCallBackInitString(This,pVal)	\
    (This)->lpVtbl -> get_CompletionCallBackInitString(This,pVal)

#define IBistroController3_put_CompletionCallBackInitString(This,newVal)	\
    (This)->lpVtbl -> put_CompletionCallBackInitString(This,newVal)

#define IBistroController3_put_SkipAllModules(This,newVal)	\
    (This)->lpVtbl -> put_SkipAllModules(This,newVal)

#define IBistroController3_get_TargetMachineName(This,pVal)	\
    (This)->lpVtbl -> get_TargetMachineName(This,pVal)

#define IBistroController3_put_TargetMachineName(This,newVal)	\
    (This)->lpVtbl -> put_TargetMachineName(This,newVal)

#define IBistroController3_AllowAttachDetach(This,bAllow)	\
    (This)->lpVtbl -> AllowAttachDetach(This,bAllow)

#define IBistroController3_AllowMultiuser(This,bAllowMultiuser)	\
    (This)->lpVtbl -> AllowMultiuser(This,bAllowMultiuser)

#define IBistroController3_SetModuleAssureFeature(This,ModuleName,Feature,State,bAccepted)	\
    (This)->lpVtbl -> SetModuleAssureFeature(This,ModuleName,Feature,State,bAccepted)

#define IBistroController3_GetModuleAssureFeature(This,ModuleName,Feature,State)	\
    (This)->lpVtbl -> GetModuleAssureFeature(This,ModuleName,Feature,State)


#define IBistroController3_get_ForceInstrumentation(This,ModuleName,pVal)	\
    (This)->lpVtbl -> get_ForceInstrumentation(This,ModuleName,pVal)

#define IBistroController3_put_ForceInstrumentation(This,ModuleName,newVal)	\
    (This)->lpVtbl -> put_ForceInstrumentation(This,ModuleName,newVal)

#define IBistroController3_get_CBistroController(This,pVal)	\
    (This)->lpVtbl -> get_CBistroController(This,pVal)

#define IBistroController3_GetLastError(This,pPD)	\
    (This)->lpVtbl -> GetLastError(This,pPD)


#define IBistroController3_get_AlternatePath(This,pVal)	\
    (This)->lpVtbl -> get_AlternatePath(This,pVal)

#define IBistroController3_put_AlternatePath(This,newVal)	\
    (This)->lpVtbl -> put_AlternatePath(This,newVal)

#define IBistroController3_SetDefaultSystemDllInstrumentation(This,newVal,forNewModulesOnly)	\
    (This)->lpVtbl -> SetDefaultSystemDllInstrumentation(This,newVal,forNewModulesOnly)

#define IBistroController3_SetDefaultUserExeInstrumentation(This,newVal,forNewModulesOnly)	\
    (This)->lpVtbl -> SetDefaultUserExeInstrumentation(This,newVal,forNewModulesOnly)

#define IBistroController3_SetDefaultUserDllInstrumentation(This,newVal,forNewModulesOnly)	\
    (This)->lpVtbl -> SetDefaultUserDllInstrumentation(This,newVal,forNewModulesOnly)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IBistroController3_get_AlternatePath_Proxy( 
    IBistroController3 * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IBistroController3_get_AlternatePath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IBistroController3_put_AlternatePath_Proxy( 
    IBistroController3 * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IBistroController3_put_AlternatePath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController3_SetDefaultSystemDllInstrumentation_Proxy( 
    IBistroController3 * This,
    /* [in] */ BSTR newVal,
    /* [in] */ BOOL forNewModulesOnly);


void __RPC_STUB IBistroController3_SetDefaultSystemDllInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController3_SetDefaultUserExeInstrumentation_Proxy( 
    IBistroController3 * This,
    /* [in] */ BSTR newVal,
    /* [in] */ BOOL forNewModulesOnly);


void __RPC_STUB IBistroController3_SetDefaultUserExeInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroController3_SetDefaultUserDllInstrumentation_Proxy( 
    IBistroController3 * This,
    /* [in] */ BSTR newVal,
    /* [in] */ BOOL forNewModulesOnly);


void __RPC_STUB IBistroController3_SetDefaultUserDllInstrumentation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IBistroController3_INTERFACE_DEFINED__ */


#ifndef __IProblemDescriptor_INTERFACE_DEFINED__
#define __IProblemDescriptor_INTERFACE_DEFINED__

/* interface IProblemDescriptor */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IProblemDescriptor;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5DEE3D25-2F2B-471b-967E-9B53E6226567")
    IProblemDescriptor : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Severity( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ProblemType( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ModuleName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ProblemDescription( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsNull( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProblemDescriptorVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProblemDescriptor * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProblemDescriptor * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProblemDescriptor * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProblemDescriptor * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProblemDescriptor * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProblemDescriptor * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProblemDescriptor * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Severity )( 
            IProblemDescriptor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProblemType )( 
            IProblemDescriptor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ModuleName )( 
            IProblemDescriptor * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProblemDescription )( 
            IProblemDescriptor * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorString )( 
            IProblemDescriptor * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsNull )( 
            IProblemDescriptor * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        END_INTERFACE
    } IProblemDescriptorVtbl;

    interface IProblemDescriptor
    {
        CONST_VTBL struct IProblemDescriptorVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProblemDescriptor_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProblemDescriptor_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProblemDescriptor_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProblemDescriptor_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IProblemDescriptor_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IProblemDescriptor_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IProblemDescriptor_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IProblemDescriptor_get_Severity(This,pVal)	\
    (This)->lpVtbl -> get_Severity(This,pVal)

#define IProblemDescriptor_get_ProblemType(This,pVal)	\
    (This)->lpVtbl -> get_ProblemType(This,pVal)

#define IProblemDescriptor_get_ModuleName(This,pVal)	\
    (This)->lpVtbl -> get_ModuleName(This,pVal)

#define IProblemDescriptor_get_ProblemDescription(This,pVal)	\
    (This)->lpVtbl -> get_ProblemDescription(This,pVal)

#define IProblemDescriptor_get_ErrorString(This,pVal)	\
    (This)->lpVtbl -> get_ErrorString(This,pVal)

#define IProblemDescriptor_get_IsNull(This,pVal)	\
    (This)->lpVtbl -> get_IsNull(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProblemDescriptor_get_Severity_Proxy( 
    IProblemDescriptor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IProblemDescriptor_get_Severity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProblemDescriptor_get_ProblemType_Proxy( 
    IProblemDescriptor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IProblemDescriptor_get_ProblemType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProblemDescriptor_get_ModuleName_Proxy( 
    IProblemDescriptor * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IProblemDescriptor_get_ModuleName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProblemDescriptor_get_ProblemDescription_Proxy( 
    IProblemDescriptor * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IProblemDescriptor_get_ProblemDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProblemDescriptor_get_ErrorString_Proxy( 
    IProblemDescriptor * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IProblemDescriptor_get_ErrorString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProblemDescriptor_get_IsNull_Proxy( 
    IProblemDescriptor * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IProblemDescriptor_get_IsNull_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProblemDescriptor_INTERFACE_DEFINED__ */


#ifndef __IStartStopConsole_INTERFACE_DEFINED__
#define __IStartStopConsole_INTERFACE_DEFINED__

/* interface IStartStopConsole */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStartStopConsole;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("39591F2F-45DF-497f-A891-597B76846540")
    IStartStopConsole : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SendCommand( 
            /* [in] */ int CommandId,
            /* [in] */ BSTR Buffer,
            /* [in] */ VARIANT TargetList,
            /* [in] */ int CommandSpecificFlags,
            /* [in] */ int ServerFlags) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Annotate( 
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer,
            /* [in] */ int ServerFlags) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseConsole( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AutoFlush( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AutoFlush( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BeforeFirstConnectionTimeout( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BeforeFirstConnectionTimeout( 
            /* [in] */ int newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AfterLastConnectionTimeout( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AfterLastConnectionTimeout( 
            /* [in] */ int newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStartStopConsoleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStartStopConsole * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStartStopConsole * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStartStopConsole * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStartStopConsole * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStartStopConsole * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStartStopConsole * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStartStopConsole * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SendCommand )( 
            IStartStopConsole * This,
            /* [in] */ int CommandId,
            /* [in] */ BSTR Buffer,
            /* [in] */ VARIANT TargetList,
            /* [in] */ int CommandSpecificFlags,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Annotate )( 
            IStartStopConsole * This,
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer,
            /* [in] */ int ServerFlags);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseConsole )( 
            IStartStopConsole * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AutoFlush )( 
            IStartStopConsole * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AutoFlush )( 
            IStartStopConsole * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BeforeFirstConnectionTimeout )( 
            IStartStopConsole * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BeforeFirstConnectionTimeout )( 
            IStartStopConsole * This,
            /* [in] */ int newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AfterLastConnectionTimeout )( 
            IStartStopConsole * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AfterLastConnectionTimeout )( 
            IStartStopConsole * This,
            /* [in] */ int newVal);
        
        END_INTERFACE
    } IStartStopConsoleVtbl;

    interface IStartStopConsole
    {
        CONST_VTBL struct IStartStopConsoleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStartStopConsole_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IStartStopConsole_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IStartStopConsole_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IStartStopConsole_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IStartStopConsole_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IStartStopConsole_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IStartStopConsole_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IStartStopConsole_SendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)	\
    (This)->lpVtbl -> SendCommand(This,CommandId,Buffer,TargetList,CommandSpecificFlags,ServerFlags)

#define IStartStopConsole_Annotate(This,AnnotationId,Buffer,ServerFlags)	\
    (This)->lpVtbl -> Annotate(This,AnnotationId,Buffer,ServerFlags)

#define IStartStopConsole_CloseConsole(This)	\
    (This)->lpVtbl -> CloseConsole(This)

#define IStartStopConsole_get_AutoFlush(This,pVal)	\
    (This)->lpVtbl -> get_AutoFlush(This,pVal)

#define IStartStopConsole_put_AutoFlush(This,newVal)	\
    (This)->lpVtbl -> put_AutoFlush(This,newVal)

#define IStartStopConsole_get_BeforeFirstConnectionTimeout(This,pVal)	\
    (This)->lpVtbl -> get_BeforeFirstConnectionTimeout(This,pVal)

#define IStartStopConsole_put_BeforeFirstConnectionTimeout(This,newVal)	\
    (This)->lpVtbl -> put_BeforeFirstConnectionTimeout(This,newVal)

#define IStartStopConsole_get_AfterLastConnectionTimeout(This,pVal)	\
    (This)->lpVtbl -> get_AfterLastConnectionTimeout(This,pVal)

#define IStartStopConsole_put_AfterLastConnectionTimeout(This,newVal)	\
    (This)->lpVtbl -> put_AfterLastConnectionTimeout(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_SendCommand_Proxy( 
    IStartStopConsole * This,
    /* [in] */ int CommandId,
    /* [in] */ BSTR Buffer,
    /* [in] */ VARIANT TargetList,
    /* [in] */ int CommandSpecificFlags,
    /* [in] */ int ServerFlags);


void __RPC_STUB IStartStopConsole_SendCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_Annotate_Proxy( 
    IStartStopConsole * This,
    /* [in] */ int AnnotationId,
    /* [in] */ BSTR Buffer,
    /* [in] */ int ServerFlags);


void __RPC_STUB IStartStopConsole_Annotate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_CloseConsole_Proxy( 
    IStartStopConsole * This);


void __RPC_STUB IStartStopConsole_CloseConsole_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_get_AutoFlush_Proxy( 
    IStartStopConsole * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IStartStopConsole_get_AutoFlush_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_put_AutoFlush_Proxy( 
    IStartStopConsole * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IStartStopConsole_put_AutoFlush_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_get_BeforeFirstConnectionTimeout_Proxy( 
    IStartStopConsole * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB IStartStopConsole_get_BeforeFirstConnectionTimeout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_put_BeforeFirstConnectionTimeout_Proxy( 
    IStartStopConsole * This,
    /* [in] */ int newVal);


void __RPC_STUB IStartStopConsole_put_BeforeFirstConnectionTimeout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_get_AfterLastConnectionTimeout_Proxy( 
    IStartStopConsole * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB IStartStopConsole_get_AfterLastConnectionTimeout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStartStopConsole_put_AfterLastConnectionTimeout_Proxy( 
    IStartStopConsole * This,
    /* [in] */ int newVal);


void __RPC_STUB IStartStopConsole_put_AfterLastConnectionTimeout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IStartStopConsole_INTERFACE_DEFINED__ */


#ifndef __ICompletionCallBack_INTERFACE_DEFINED__
#define __ICompletionCallBack_INTERFACE_DEFINED__

/* interface ICompletionCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICompletionCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FF3330BD-5C4D-43a5-8921-E40547CDFDF5")
    ICompletionCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DebugInfoFile( 
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR CurrentDebugInfoFile,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Start( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Finish( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICompletionCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICompletionCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICompletionCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICompletionCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICompletionCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICompletionCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICompletionCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICompletionCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DebugInfoFile )( 
            ICompletionCallBack * This,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR CurrentDebugInfoFile,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Start )( 
            ICompletionCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Finish )( 
            ICompletionCallBack * This);
        
        END_INTERFACE
    } ICompletionCallBackVtbl;

    interface ICompletionCallBack
    {
        CONST_VTBL struct ICompletionCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICompletionCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICompletionCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICompletionCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICompletionCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICompletionCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICompletionCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICompletionCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICompletionCallBack_DebugInfoFile(This,OriginalModule,CurrentDebugInfoFile,pVal)	\
    (This)->lpVtbl -> DebugInfoFile(This,OriginalModule,CurrentDebugInfoFile,pVal)

#define ICompletionCallBack_Start(This)	\
    (This)->lpVtbl -> Start(This)

#define ICompletionCallBack_Finish(This)	\
    (This)->lpVtbl -> Finish(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICompletionCallBack_DebugInfoFile_Proxy( 
    ICompletionCallBack * This,
    /* [in] */ BSTR OriginalModule,
    /* [in] */ BSTR CurrentDebugInfoFile,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICompletionCallBack_DebugInfoFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICompletionCallBack_Start_Proxy( 
    ICompletionCallBack * This);


void __RPC_STUB ICompletionCallBack_Start_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICompletionCallBack_Finish_Proxy( 
    ICompletionCallBack * This);


void __RPC_STUB ICompletionCallBack_Finish_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICompletionCallBack_INTERFACE_DEFINED__ */


#ifndef __IInstrumentationCallBack_INTERFACE_DEFINED__
#define __IInstrumentationCallBack_INTERFACE_DEFINED__

/* interface IInstrumentationCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IInstrumentationCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A6FED611-DB05-4b29-8FFF-9B5A190A7ACB")
    IInstrumentationCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InstrumentationRequired( 
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR CacheDir,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR InstrumentationType,
            /* [in] */ BSTR CurrentInstrumentation_stamp,
            /* [out] */ BSTR *NewInstrumentationStamp,
            /* [in] */ BSTR CurrentInstrumentorOptions,
            /* [out] */ BSTR *NewInstrumentorOptions,
            /* [in] */ BSTR DebugInfoFileName,
            /* [out] */ BSTR *NewDebugInfoFileName,
            /* [in] */ BSTR ComInfoFileName,
            /* [out] */ BSTR *NewComInfoFileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE BeforeInstrument( 
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR CacheDir,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR InstrumentationType,
            /* [in] */ BSTR CurrentInstrumentorOptions,
            /* [in] */ BSTR DebugInfoFileName,
            /* [in] */ BSTR ComInfoFileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AfterInstrument( 
            /* [in] */ VARIANT_BOOL FinishedSuccesfully,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR CacheDir,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR InstrumentationType,
            /* [in] */ BSTR CurrentInstrumentorOptions,
            /* [in] */ BSTR DebugInfoFileName,
            /* [in] */ BSTR ComInfoFileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Start( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Finish( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearCache( 
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR CacheDir) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearCacheDirectory( 
            /* [in] */ BSTR CacheDir) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IInstrumentationCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IInstrumentationCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IInstrumentationCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IInstrumentationCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IInstrumentationCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IInstrumentationCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IInstrumentationCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IInstrumentationCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InstrumentationRequired )( 
            IInstrumentationCallBack * This,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR CacheDir,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR InstrumentationType,
            /* [in] */ BSTR CurrentInstrumentation_stamp,
            /* [out] */ BSTR *NewInstrumentationStamp,
            /* [in] */ BSTR CurrentInstrumentorOptions,
            /* [out] */ BSTR *NewInstrumentorOptions,
            /* [in] */ BSTR DebugInfoFileName,
            /* [out] */ BSTR *NewDebugInfoFileName,
            /* [in] */ BSTR ComInfoFileName,
            /* [out] */ BSTR *NewComInfoFileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *BeforeInstrument )( 
            IInstrumentationCallBack * This,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR CacheDir,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR InstrumentationType,
            /* [in] */ BSTR CurrentInstrumentorOptions,
            /* [in] */ BSTR DebugInfoFileName,
            /* [in] */ BSTR ComInfoFileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AfterInstrument )( 
            IInstrumentationCallBack * This,
            /* [in] */ VARIANT_BOOL FinishedSuccesfully,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR CacheDir,
            /* [in] */ BSTR OriginalModule,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR InstrumentationType,
            /* [in] */ BSTR CurrentInstrumentorOptions,
            /* [in] */ BSTR DebugInfoFileName,
            /* [in] */ BSTR ComInfoFileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Start )( 
            IInstrumentationCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Finish )( 
            IInstrumentationCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearCache )( 
            IInstrumentationCallBack * This,
            /* [in] */ BSTR NewModule,
            /* [in] */ BSTR CacheDir);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearCacheDirectory )( 
            IInstrumentationCallBack * This,
            /* [in] */ BSTR CacheDir);
        
        END_INTERFACE
    } IInstrumentationCallBackVtbl;

    interface IInstrumentationCallBack
    {
        CONST_VTBL struct IInstrumentationCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IInstrumentationCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IInstrumentationCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IInstrumentationCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IInstrumentationCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IInstrumentationCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IInstrumentationCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IInstrumentationCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IInstrumentationCallBack_InstrumentationRequired(This,OrigName,CacheDir,OriginalModule,NewModule,InstrumentationType,CurrentInstrumentation_stamp,NewInstrumentationStamp,CurrentInstrumentorOptions,NewInstrumentorOptions,DebugInfoFileName,NewDebugInfoFileName,ComInfoFileName,NewComInfoFileName)	\
    (This)->lpVtbl -> InstrumentationRequired(This,OrigName,CacheDir,OriginalModule,NewModule,InstrumentationType,CurrentInstrumentation_stamp,NewInstrumentationStamp,CurrentInstrumentorOptions,NewInstrumentorOptions,DebugInfoFileName,NewDebugInfoFileName,ComInfoFileName,NewComInfoFileName)

#define IInstrumentationCallBack_BeforeInstrument(This,OrigName,CacheDir,OriginalModule,NewModule,InstrumentationType,CurrentInstrumentorOptions,DebugInfoFileName,ComInfoFileName)	\
    (This)->lpVtbl -> BeforeInstrument(This,OrigName,CacheDir,OriginalModule,NewModule,InstrumentationType,CurrentInstrumentorOptions,DebugInfoFileName,ComInfoFileName)

#define IInstrumentationCallBack_AfterInstrument(This,FinishedSuccesfully,OrigName,CacheDir,OriginalModule,NewModule,InstrumentationType,CurrentInstrumentorOptions,DebugInfoFileName,ComInfoFileName)	\
    (This)->lpVtbl -> AfterInstrument(This,FinishedSuccesfully,OrigName,CacheDir,OriginalModule,NewModule,InstrumentationType,CurrentInstrumentorOptions,DebugInfoFileName,ComInfoFileName)

#define IInstrumentationCallBack_Start(This)	\
    (This)->lpVtbl -> Start(This)

#define IInstrumentationCallBack_Finish(This)	\
    (This)->lpVtbl -> Finish(This)

#define IInstrumentationCallBack_ClearCache(This,NewModule,CacheDir)	\
    (This)->lpVtbl -> ClearCache(This,NewModule,CacheDir)

#define IInstrumentationCallBack_ClearCacheDirectory(This,CacheDir)	\
    (This)->lpVtbl -> ClearCacheDirectory(This,CacheDir)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_InstrumentationRequired_Proxy( 
    IInstrumentationCallBack * This,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR CacheDir,
    /* [in] */ BSTR OriginalModule,
    /* [in] */ BSTR NewModule,
    /* [in] */ BSTR InstrumentationType,
    /* [in] */ BSTR CurrentInstrumentation_stamp,
    /* [out] */ BSTR *NewInstrumentationStamp,
    /* [in] */ BSTR CurrentInstrumentorOptions,
    /* [out] */ BSTR *NewInstrumentorOptions,
    /* [in] */ BSTR DebugInfoFileName,
    /* [out] */ BSTR *NewDebugInfoFileName,
    /* [in] */ BSTR ComInfoFileName,
    /* [out] */ BSTR *NewComInfoFileName);


void __RPC_STUB IInstrumentationCallBack_InstrumentationRequired_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_BeforeInstrument_Proxy( 
    IInstrumentationCallBack * This,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR CacheDir,
    /* [in] */ BSTR OriginalModule,
    /* [in] */ BSTR NewModule,
    /* [in] */ BSTR InstrumentationType,
    /* [in] */ BSTR CurrentInstrumentorOptions,
    /* [in] */ BSTR DebugInfoFileName,
    /* [in] */ BSTR ComInfoFileName);


void __RPC_STUB IInstrumentationCallBack_BeforeInstrument_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_AfterInstrument_Proxy( 
    IInstrumentationCallBack * This,
    /* [in] */ VARIANT_BOOL FinishedSuccesfully,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR CacheDir,
    /* [in] */ BSTR OriginalModule,
    /* [in] */ BSTR NewModule,
    /* [in] */ BSTR InstrumentationType,
    /* [in] */ BSTR CurrentInstrumentorOptions,
    /* [in] */ BSTR DebugInfoFileName,
    /* [in] */ BSTR ComInfoFileName);


void __RPC_STUB IInstrumentationCallBack_AfterInstrument_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_Start_Proxy( 
    IInstrumentationCallBack * This);


void __RPC_STUB IInstrumentationCallBack_Start_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_Finish_Proxy( 
    IInstrumentationCallBack * This);


void __RPC_STUB IInstrumentationCallBack_Finish_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_ClearCache_Proxy( 
    IInstrumentationCallBack * This,
    /* [in] */ BSTR NewModule,
    /* [in] */ BSTR CacheDir);


void __RPC_STUB IInstrumentationCallBack_ClearCache_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationCallBack_ClearCacheDirectory_Proxy( 
    IInstrumentationCallBack * This,
    /* [in] */ BSTR CacheDir);


void __RPC_STUB IInstrumentationCallBack_ClearCacheDirectory_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IInstrumentationCallBack_INTERFACE_DEFINED__ */


#ifndef __IProgressBarCallBack_INTERFACE_DEFINED__
#define __IProgressBarCallBack_INTERFACE_DEFINED__

/* interface IProgressBarCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IProgressBarCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C4830F86-F890-4d20-86DB-F747543163A7")
    IProgressBarCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Progress( 
            /* [in] */ int Percentage,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR InstrumentationType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallForAllModules( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Info( 
            /* [in] */ BSTR SomeInfo) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunTimeInstrumentationRequest( 
            /* [in] */ BSTR OrigName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FileProgress( 
            /* [in] */ int Percentage,
            /* [in] */ BSTR OptionalString) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PrelistModules( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PrelistModule( 
            /* [in] */ BSTR SomeInfo) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Start( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Finish( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProgressBarCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProgressBarCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProgressBarCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProgressBarCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProgressBarCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProgressBarCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProgressBarCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProgressBarCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Progress )( 
            IProgressBarCallBack * This,
            /* [in] */ int Percentage,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR InstrumentationType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallForAllModules )( 
            IProgressBarCallBack * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Info )( 
            IProgressBarCallBack * This,
            /* [in] */ BSTR SomeInfo);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunTimeInstrumentationRequest )( 
            IProgressBarCallBack * This,
            /* [in] */ BSTR OrigName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FileProgress )( 
            IProgressBarCallBack * This,
            /* [in] */ int Percentage,
            /* [in] */ BSTR OptionalString);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrelistModules )( 
            IProgressBarCallBack * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrelistModule )( 
            IProgressBarCallBack * This,
            /* [in] */ BSTR SomeInfo);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Start )( 
            IProgressBarCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Finish )( 
            IProgressBarCallBack * This);
        
        END_INTERFACE
    } IProgressBarCallBackVtbl;

    interface IProgressBarCallBack
    {
        CONST_VTBL struct IProgressBarCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProgressBarCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProgressBarCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProgressBarCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProgressBarCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IProgressBarCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IProgressBarCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IProgressBarCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IProgressBarCallBack_Progress(This,Percentage,OrigName,InstrumentationType)	\
    (This)->lpVtbl -> Progress(This,Percentage,OrigName,InstrumentationType)

#define IProgressBarCallBack_CallForAllModules(This,pVal)	\
    (This)->lpVtbl -> CallForAllModules(This,pVal)

#define IProgressBarCallBack_Info(This,SomeInfo)	\
    (This)->lpVtbl -> Info(This,SomeInfo)

#define IProgressBarCallBack_RunTimeInstrumentationRequest(This,OrigName)	\
    (This)->lpVtbl -> RunTimeInstrumentationRequest(This,OrigName)

#define IProgressBarCallBack_FileProgress(This,Percentage,OptionalString)	\
    (This)->lpVtbl -> FileProgress(This,Percentage,OptionalString)

#define IProgressBarCallBack_PrelistModules(This,pVal)	\
    (This)->lpVtbl -> PrelistModules(This,pVal)

#define IProgressBarCallBack_PrelistModule(This,SomeInfo)	\
    (This)->lpVtbl -> PrelistModule(This,SomeInfo)

#define IProgressBarCallBack_Start(This)	\
    (This)->lpVtbl -> Start(This)

#define IProgressBarCallBack_Finish(This)	\
    (This)->lpVtbl -> Finish(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_Progress_Proxy( 
    IProgressBarCallBack * This,
    /* [in] */ int Percentage,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR InstrumentationType);


void __RPC_STUB IProgressBarCallBack_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_CallForAllModules_Proxy( 
    IProgressBarCallBack * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IProgressBarCallBack_CallForAllModules_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_Info_Proxy( 
    IProgressBarCallBack * This,
    /* [in] */ BSTR SomeInfo);


void __RPC_STUB IProgressBarCallBack_Info_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_RunTimeInstrumentationRequest_Proxy( 
    IProgressBarCallBack * This,
    /* [in] */ BSTR OrigName);


void __RPC_STUB IProgressBarCallBack_RunTimeInstrumentationRequest_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_FileProgress_Proxy( 
    IProgressBarCallBack * This,
    /* [in] */ int Percentage,
    /* [in] */ BSTR OptionalString);


void __RPC_STUB IProgressBarCallBack_FileProgress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_PrelistModules_Proxy( 
    IProgressBarCallBack * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IProgressBarCallBack_PrelistModules_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_PrelistModule_Proxy( 
    IProgressBarCallBack * This,
    /* [in] */ BSTR SomeInfo);


void __RPC_STUB IProgressBarCallBack_PrelistModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_Start_Proxy( 
    IProgressBarCallBack * This);


void __RPC_STUB IProgressBarCallBack_Start_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IProgressBarCallBack_Finish_Proxy( 
    IProgressBarCallBack * This);


void __RPC_STUB IProgressBarCallBack_Finish_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProgressBarCallBack_INTERFACE_DEFINED__ */


#ifndef __IInstrumentationStatusCallBack_INTERFACE_DEFINED__
#define __IInstrumentationStatusCallBack_INTERFACE_DEFINED__

/* interface IInstrumentationStatusCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IInstrumentationStatusCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("CF07E2C2-8B99-4c4d-8579-D751DB03DE62")
    IInstrumentationStatusCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Status( 
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR NewName,
            /* [in] */ BSTR InstrumentationType,
            long Status) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Info( 
            /* [in] */ BSTR SomeInfo) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Start( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Finish( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IInstrumentationStatusCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IInstrumentationStatusCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IInstrumentationStatusCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IInstrumentationStatusCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IInstrumentationStatusCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IInstrumentationStatusCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IInstrumentationStatusCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IInstrumentationStatusCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Status )( 
            IInstrumentationStatusCallBack * This,
            /* [in] */ BSTR OrigName,
            /* [in] */ BSTR NewName,
            /* [in] */ BSTR InstrumentationType,
            long Status);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Info )( 
            IInstrumentationStatusCallBack * This,
            /* [in] */ BSTR SomeInfo);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Start )( 
            IInstrumentationStatusCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Finish )( 
            IInstrumentationStatusCallBack * This);
        
        END_INTERFACE
    } IInstrumentationStatusCallBackVtbl;

    interface IInstrumentationStatusCallBack
    {
        CONST_VTBL struct IInstrumentationStatusCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IInstrumentationStatusCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IInstrumentationStatusCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IInstrumentationStatusCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IInstrumentationStatusCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IInstrumentationStatusCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IInstrumentationStatusCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IInstrumentationStatusCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IInstrumentationStatusCallBack_Status(This,OrigName,NewName,InstrumentationType,Status)	\
    (This)->lpVtbl -> Status(This,OrigName,NewName,InstrumentationType,Status)

#define IInstrumentationStatusCallBack_Info(This,SomeInfo)	\
    (This)->lpVtbl -> Info(This,SomeInfo)

#define IInstrumentationStatusCallBack_Start(This)	\
    (This)->lpVtbl -> Start(This)

#define IInstrumentationStatusCallBack_Finish(This)	\
    (This)->lpVtbl -> Finish(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationStatusCallBack_Status_Proxy( 
    IInstrumentationStatusCallBack * This,
    /* [in] */ BSTR OrigName,
    /* [in] */ BSTR NewName,
    /* [in] */ BSTR InstrumentationType,
    long Status);


void __RPC_STUB IInstrumentationStatusCallBack_Status_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationStatusCallBack_Info_Proxy( 
    IInstrumentationStatusCallBack * This,
    /* [in] */ BSTR SomeInfo);


void __RPC_STUB IInstrumentationStatusCallBack_Info_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationStatusCallBack_Start_Proxy( 
    IInstrumentationStatusCallBack * This);


void __RPC_STUB IInstrumentationStatusCallBack_Start_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IInstrumentationStatusCallBack_Finish_Proxy( 
    IInstrumentationStatusCallBack * This);


void __RPC_STUB IInstrumentationStatusCallBack_Finish_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IInstrumentationStatusCallBack_INTERFACE_DEFINED__ */


#ifndef __IBistroControllerInfoCallBack_INTERFACE_DEFINED__
#define __IBistroControllerInfoCallBack_INTERFACE_DEFINED__

/* interface IBistroControllerInfoCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IBistroControllerInfoCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A9D8E9CD-0C5A-4496-8D6C-0D1CA8F73436")
    IBistroControllerInfoCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ControllerInfo( 
            /* [in] */ long Status) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBistroControllerInfoCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBistroControllerInfoCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBistroControllerInfoCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBistroControllerInfoCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBistroControllerInfoCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBistroControllerInfoCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBistroControllerInfoCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBistroControllerInfoCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ControllerInfo )( 
            IBistroControllerInfoCallBack * This,
            /* [in] */ long Status);
        
        END_INTERFACE
    } IBistroControllerInfoCallBackVtbl;

    interface IBistroControllerInfoCallBack
    {
        CONST_VTBL struct IBistroControllerInfoCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBistroControllerInfoCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IBistroControllerInfoCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IBistroControllerInfoCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IBistroControllerInfoCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IBistroControllerInfoCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IBistroControllerInfoCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IBistroControllerInfoCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IBistroControllerInfoCallBack_ControllerInfo(This,Status)	\
    (This)->lpVtbl -> ControllerInfo(This,Status)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroControllerInfoCallBack_ControllerInfo_Proxy( 
    IBistroControllerInfoCallBack * This,
    /* [in] */ long Status);


void __RPC_STUB IBistroControllerInfoCallBack_ControllerInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IBistroControllerInfoCallBack_INTERFACE_DEFINED__ */


#ifndef __IBistroControllerErrorCallBack_INTERFACE_DEFINED__
#define __IBistroControllerErrorCallBack_INTERFACE_DEFINED__

/* interface IBistroControllerErrorCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IBistroControllerErrorCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0AAD6E1A-15AC-470a-B461-CD45E70C35C1")
    IBistroControllerErrorCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ControllerError( 
            /* [in] */ VARIANT ProblemList,
            /* [retval][out] */ long *pStatus) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBistroControllerErrorCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBistroControllerErrorCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBistroControllerErrorCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBistroControllerErrorCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBistroControllerErrorCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBistroControllerErrorCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBistroControllerErrorCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBistroControllerErrorCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ControllerError )( 
            IBistroControllerErrorCallBack * This,
            /* [in] */ VARIANT ProblemList,
            /* [retval][out] */ long *pStatus);
        
        END_INTERFACE
    } IBistroControllerErrorCallBackVtbl;

    interface IBistroControllerErrorCallBack
    {
        CONST_VTBL struct IBistroControllerErrorCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBistroControllerErrorCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IBistroControllerErrorCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IBistroControllerErrorCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IBistroControllerErrorCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IBistroControllerErrorCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IBistroControllerErrorCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IBistroControllerErrorCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IBistroControllerErrorCallBack_ControllerError(This,ProblemList,pStatus)	\
    (This)->lpVtbl -> ControllerError(This,ProblemList,pStatus)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IBistroControllerErrorCallBack_ControllerError_Proxy( 
    IBistroControllerErrorCallBack * This,
    /* [in] */ VARIANT ProblemList,
    /* [retval][out] */ long *pStatus);


void __RPC_STUB IBistroControllerErrorCallBack_ControllerError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IBistroControllerErrorCallBack_INTERFACE_DEFINED__ */


#ifndef __IConsoleVisualizeCallBack_INTERFACE_DEFINED__
#define __IConsoleVisualizeCallBack_INTERFACE_DEFINED__

/* interface IConsoleVisualizeCallBack */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IConsoleVisualizeCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3DDB69E0-1989-406d-8182-0195C83406FB")
    IConsoleVisualizeCallBack : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConsoleStarted( 
            /* [in] */ IStartStopConsole *Console) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConsoleClosed( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConsoleKilled( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectionCreated( 
            /* [in] */ int ConnectionId,
            /* [in] */ BSTR ConnectionTitle,
            /* [in] */ int ConnectionType,
            /* [in] */ VARIANT SupportedCommandList,
            /* [in] */ VARIANT StatusPairsList) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectionClosed( 
            /* [in] */ int ConnectionId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectionUpdated( 
            /* [in] */ int ConnectionId,
            /* [in] */ VARIANT SupportedCommandList,
            /* [in] */ VARIANT StatusPairsList) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Command( 
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId,
            /* [in] */ BSTR Buffer) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Reply( 
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectionCommand( 
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId,
            /* [in] */ int ConnectionId,
            /* [in] */ BSTR Buffer) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectionReply( 
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId,
            /* [in] */ int ConnectionId,
            /* [in] */ int ReplyFlags,
            /* [in] */ int Value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Annotate( 
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Status( 
            /* [in] */ int AnnotationId,
            /* [in] */ int ConnectionId,
            /* [in] */ BSTR Buffer) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IConsoleVisualizeCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IConsoleVisualizeCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IConsoleVisualizeCallBack * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IConsoleVisualizeCallBack * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleStarted )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ IStartStopConsole *Console);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleClosed )( 
            IConsoleVisualizeCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConsoleKilled )( 
            IConsoleVisualizeCallBack * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConnectionCreated )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int ConnectionId,
            /* [in] */ BSTR ConnectionTitle,
            /* [in] */ int ConnectionType,
            /* [in] */ VARIANT SupportedCommandList,
            /* [in] */ VARIANT StatusPairsList);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConnectionClosed )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int ConnectionId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConnectionUpdated )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int ConnectionId,
            /* [in] */ VARIANT SupportedCommandList,
            /* [in] */ VARIANT StatusPairsList);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Command )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId,
            /* [in] */ BSTR Buffer);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reply )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConnectionCommand )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId,
            /* [in] */ int ConnectionId,
            /* [in] */ BSTR Buffer);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConnectionReply )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int CommandId,
            /* [in] */ int CommandReferenceId,
            /* [in] */ int ConnectionId,
            /* [in] */ int ReplyFlags,
            /* [in] */ int Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Annotate )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int AnnotationId,
            /* [in] */ BSTR Buffer);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Status )( 
            IConsoleVisualizeCallBack * This,
            /* [in] */ int AnnotationId,
            /* [in] */ int ConnectionId,
            /* [in] */ BSTR Buffer);
        
        END_INTERFACE
    } IConsoleVisualizeCallBackVtbl;

    interface IConsoleVisualizeCallBack
    {
        CONST_VTBL struct IConsoleVisualizeCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IConsoleVisualizeCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IConsoleVisualizeCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IConsoleVisualizeCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IConsoleVisualizeCallBack_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IConsoleVisualizeCallBack_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IConsoleVisualizeCallBack_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IConsoleVisualizeCallBack_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IConsoleVisualizeCallBack_ConsoleStarted(This,Console)	\
    (This)->lpVtbl -> ConsoleStarted(This,Console)

#define IConsoleVisualizeCallBack_ConsoleClosed(This)	\
    (This)->lpVtbl -> ConsoleClosed(This)

#define IConsoleVisualizeCallBack_ConsoleKilled(This)	\
    (This)->lpVtbl -> ConsoleKilled(This)

#define IConsoleVisualizeCallBack_ConnectionCreated(This,ConnectionId,ConnectionTitle,ConnectionType,SupportedCommandList,StatusPairsList)	\
    (This)->lpVtbl -> ConnectionCreated(This,ConnectionId,ConnectionTitle,ConnectionType,SupportedCommandList,StatusPairsList)

#define IConsoleVisualizeCallBack_ConnectionClosed(This,ConnectionId)	\
    (This)->lpVtbl -> ConnectionClosed(This,ConnectionId)

#define IConsoleVisualizeCallBack_ConnectionUpdated(This,ConnectionId,SupportedCommandList,StatusPairsList)	\
    (This)->lpVtbl -> ConnectionUpdated(This,ConnectionId,SupportedCommandList,StatusPairsList)

#define IConsoleVisualizeCallBack_Command(This,CommandId,CommandReferenceId,Buffer)	\
    (This)->lpVtbl -> Command(This,CommandId,CommandReferenceId,Buffer)

#define IConsoleVisualizeCallBack_Reply(This,CommandId,CommandReferenceId)	\
    (This)->lpVtbl -> Reply(This,CommandId,CommandReferenceId)

#define IConsoleVisualizeCallBack_ConnectionCommand(This,CommandId,CommandReferenceId,ConnectionId,Buffer)	\
    (This)->lpVtbl -> ConnectionCommand(This,CommandId,CommandReferenceId,ConnectionId,Buffer)

#define IConsoleVisualizeCallBack_ConnectionReply(This,CommandId,CommandReferenceId,ConnectionId,ReplyFlags,Value)	\
    (This)->lpVtbl -> ConnectionReply(This,CommandId,CommandReferenceId,ConnectionId,ReplyFlags,Value)

#define IConsoleVisualizeCallBack_Annotate(This,AnnotationId,Buffer)	\
    (This)->lpVtbl -> Annotate(This,AnnotationId,Buffer)

#define IConsoleVisualizeCallBack_Status(This,AnnotationId,ConnectionId,Buffer)	\
    (This)->lpVtbl -> Status(This,AnnotationId,ConnectionId,Buffer)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConsoleStarted_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ IStartStopConsole *Console);


void __RPC_STUB IConsoleVisualizeCallBack_ConsoleStarted_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConsoleClosed_Proxy( 
    IConsoleVisualizeCallBack * This);


void __RPC_STUB IConsoleVisualizeCallBack_ConsoleClosed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConsoleKilled_Proxy( 
    IConsoleVisualizeCallBack * This);


void __RPC_STUB IConsoleVisualizeCallBack_ConsoleKilled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConnectionCreated_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int ConnectionId,
    /* [in] */ BSTR ConnectionTitle,
    /* [in] */ int ConnectionType,
    /* [in] */ VARIANT SupportedCommandList,
    /* [in] */ VARIANT StatusPairsList);


void __RPC_STUB IConsoleVisualizeCallBack_ConnectionCreated_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConnectionClosed_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int ConnectionId);


void __RPC_STUB IConsoleVisualizeCallBack_ConnectionClosed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConnectionUpdated_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int ConnectionId,
    /* [in] */ VARIANT SupportedCommandList,
    /* [in] */ VARIANT StatusPairsList);


void __RPC_STUB IConsoleVisualizeCallBack_ConnectionUpdated_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_Command_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int CommandId,
    /* [in] */ int CommandReferenceId,
    /* [in] */ BSTR Buffer);


void __RPC_STUB IConsoleVisualizeCallBack_Command_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_Reply_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int CommandId,
    /* [in] */ int CommandReferenceId);


void __RPC_STUB IConsoleVisualizeCallBack_Reply_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConnectionCommand_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int CommandId,
    /* [in] */ int CommandReferenceId,
    /* [in] */ int ConnectionId,
    /* [in] */ BSTR Buffer);


void __RPC_STUB IConsoleVisualizeCallBack_ConnectionCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_ConnectionReply_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int CommandId,
    /* [in] */ int CommandReferenceId,
    /* [in] */ int ConnectionId,
    /* [in] */ int ReplyFlags,
    /* [in] */ int Value);


void __RPC_STUB IConsoleVisualizeCallBack_ConnectionReply_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_Annotate_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int AnnotationId,
    /* [in] */ BSTR Buffer);


void __RPC_STUB IConsoleVisualizeCallBack_Annotate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IConsoleVisualizeCallBack_Status_Proxy( 
    IConsoleVisualizeCallBack * This,
    /* [in] */ int AnnotationId,
    /* [in] */ int ConnectionId,
    /* [in] */ BSTR Buffer);


void __RPC_STUB IConsoleVisualizeCallBack_Status_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IConsoleVisualizeCallBack_INTERFACE_DEFINED__ */


#ifndef __IFunctionSelection_INTERFACE_DEFINED__
#define __IFunctionSelection_INTERFACE_DEFINED__

/* interface IFunctionSelection */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IFunctionSelection;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("536E88CF-D3F0-49b8-B9A3-1BE3CDFC5716")
    IFunctionSelection : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BistroController( 
            /* [retval][out] */ IBistroController **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BistroController( 
            /* [in] */ IBistroController *newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ActiveModule( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ActiveModule( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_COMIsActiveModule( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_COMIsActiveModule( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReadData( 
            /* [in] */ VARIANT_BOOL ReadFeedbackData) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteData( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseFunctionSelectionData( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseFunctionSelectionData( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE NextFunction( 
            /* [in] */ VARIANT_BOOL Restart,
            /* [retval][out] */ IFunctionData **ppFD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAll( 
            /* [in] */ VARIANT_BOOL PerformInstrumentation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Invert( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddFunction( 
            /* [in] */ BSTR bstrMangledName,
            /* [in] */ BSTR bstrFullName,
            /* [in] */ VARIANT_BOOL bInstrument,
            /* [in] */ long lReason,
            /* [retval][out] */ IFunctionData **ppFD) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFeedbackMessage( 
            /* [in] */ long MsgID,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFunctions( 
            /* [retval][out] */ VARIANT *pvar) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UpdateInstrumentationRequests( 
            /* [in] */ VARIANT varRequests) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DataFileName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFunctionSelectionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFunctionSelection * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFunctionSelection * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFunctionSelection * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IFunctionSelection * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IFunctionSelection * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IFunctionSelection * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IFunctionSelection * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BistroController )( 
            IFunctionSelection * This,
            /* [retval][out] */ IBistroController **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BistroController )( 
            IFunctionSelection * This,
            /* [in] */ IBistroController *newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IFunctionSelection * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ActiveModule )( 
            IFunctionSelection * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ActiveModule )( 
            IFunctionSelection * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_COMIsActiveModule )( 
            IFunctionSelection * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_COMIsActiveModule )( 
            IFunctionSelection * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReadData )( 
            IFunctionSelection * This,
            /* [in] */ VARIANT_BOOL ReadFeedbackData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteData )( 
            IFunctionSelection * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseFunctionSelectionData )( 
            IFunctionSelection * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseFunctionSelectionData )( 
            IFunctionSelection * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NextFunction )( 
            IFunctionSelection * This,
            /* [in] */ VARIANT_BOOL Restart,
            /* [retval][out] */ IFunctionData **ppFD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAll )( 
            IFunctionSelection * This,
            /* [in] */ VARIANT_BOOL PerformInstrumentation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Invert )( 
            IFunctionSelection * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddFunction )( 
            IFunctionSelection * This,
            /* [in] */ BSTR bstrMangledName,
            /* [in] */ BSTR bstrFullName,
            /* [in] */ VARIANT_BOOL bInstrument,
            /* [in] */ long lReason,
            /* [retval][out] */ IFunctionData **ppFD);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFeedbackMessage )( 
            IFunctionSelection * This,
            /* [in] */ long MsgID,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFunctions )( 
            IFunctionSelection * This,
            /* [retval][out] */ VARIANT *pvar);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateInstrumentationRequests )( 
            IFunctionSelection * This,
            /* [in] */ VARIANT varRequests);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataFileName )( 
            IFunctionSelection * This,
            /* [retval][out] */ BSTR *pVal);
        
        END_INTERFACE
    } IFunctionSelectionVtbl;

    interface IFunctionSelection
    {
        CONST_VTBL struct IFunctionSelectionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFunctionSelection_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IFunctionSelection_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IFunctionSelection_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IFunctionSelection_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IFunctionSelection_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IFunctionSelection_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IFunctionSelection_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IFunctionSelection_get_BistroController(This,pVal)	\
    (This)->lpVtbl -> get_BistroController(This,pVal)

#define IFunctionSelection_put_BistroController(This,newVal)	\
    (This)->lpVtbl -> put_BistroController(This,newVal)

#define IFunctionSelection_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IFunctionSelection_get_ActiveModule(This,pVal)	\
    (This)->lpVtbl -> get_ActiveModule(This,pVal)

#define IFunctionSelection_put_ActiveModule(This,newVal)	\
    (This)->lpVtbl -> put_ActiveModule(This,newVal)

#define IFunctionSelection_get_COMIsActiveModule(This,pVal)	\
    (This)->lpVtbl -> get_COMIsActiveModule(This,pVal)

#define IFunctionSelection_put_COMIsActiveModule(This,newVal)	\
    (This)->lpVtbl -> put_COMIsActiveModule(This,newVal)

#define IFunctionSelection_ReadData(This,ReadFeedbackData)	\
    (This)->lpVtbl -> ReadData(This,ReadFeedbackData)

#define IFunctionSelection_WriteData(This)	\
    (This)->lpVtbl -> WriteData(This)

#define IFunctionSelection_get_UseFunctionSelectionData(This,pVal)	\
    (This)->lpVtbl -> get_UseFunctionSelectionData(This,pVal)

#define IFunctionSelection_put_UseFunctionSelectionData(This,newVal)	\
    (This)->lpVtbl -> put_UseFunctionSelectionData(This,newVal)

#define IFunctionSelection_NextFunction(This,Restart,ppFD)	\
    (This)->lpVtbl -> NextFunction(This,Restart,ppFD)

#define IFunctionSelection_SetAll(This,PerformInstrumentation)	\
    (This)->lpVtbl -> SetAll(This,PerformInstrumentation)

#define IFunctionSelection_Invert(This)	\
    (This)->lpVtbl -> Invert(This)

#define IFunctionSelection_AddFunction(This,bstrMangledName,bstrFullName,bInstrument,lReason,ppFD)	\
    (This)->lpVtbl -> AddFunction(This,bstrMangledName,bstrFullName,bInstrument,lReason,ppFD)

#define IFunctionSelection_GetFeedbackMessage(This,MsgID,pVal)	\
    (This)->lpVtbl -> GetFeedbackMessage(This,MsgID,pVal)

#define IFunctionSelection_GetFunctions(This,pvar)	\
    (This)->lpVtbl -> GetFunctions(This,pvar)

#define IFunctionSelection_UpdateInstrumentationRequests(This,varRequests)	\
    (This)->lpVtbl -> UpdateInstrumentationRequests(This,varRequests)

#define IFunctionSelection_get_DataFileName(This,pVal)	\
    (This)->lpVtbl -> get_DataFileName(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_get_BistroController_Proxy( 
    IFunctionSelection * This,
    /* [retval][out] */ IBistroController **pVal);


void __RPC_STUB IFunctionSelection_get_BistroController_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_put_BistroController_Proxy( 
    IFunctionSelection * This,
    /* [in] */ IBistroController *newVal);


void __RPC_STUB IFunctionSelection_put_BistroController_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_Reset_Proxy( 
    IFunctionSelection * This);


void __RPC_STUB IFunctionSelection_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_get_ActiveModule_Proxy( 
    IFunctionSelection * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionSelection_get_ActiveModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_put_ActiveModule_Proxy( 
    IFunctionSelection * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IFunctionSelection_put_ActiveModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_get_COMIsActiveModule_Proxy( 
    IFunctionSelection * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IFunctionSelection_get_COMIsActiveModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_put_COMIsActiveModule_Proxy( 
    IFunctionSelection * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IFunctionSelection_put_COMIsActiveModule_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_ReadData_Proxy( 
    IFunctionSelection * This,
    /* [in] */ VARIANT_BOOL ReadFeedbackData);


void __RPC_STUB IFunctionSelection_ReadData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_WriteData_Proxy( 
    IFunctionSelection * This);


void __RPC_STUB IFunctionSelection_WriteData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_get_UseFunctionSelectionData_Proxy( 
    IFunctionSelection * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IFunctionSelection_get_UseFunctionSelectionData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_put_UseFunctionSelectionData_Proxy( 
    IFunctionSelection * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IFunctionSelection_put_UseFunctionSelectionData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_NextFunction_Proxy( 
    IFunctionSelection * This,
    /* [in] */ VARIANT_BOOL Restart,
    /* [retval][out] */ IFunctionData **ppFD);


void __RPC_STUB IFunctionSelection_NextFunction_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_SetAll_Proxy( 
    IFunctionSelection * This,
    /* [in] */ VARIANT_BOOL PerformInstrumentation);


void __RPC_STUB IFunctionSelection_SetAll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_Invert_Proxy( 
    IFunctionSelection * This);


void __RPC_STUB IFunctionSelection_Invert_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_AddFunction_Proxy( 
    IFunctionSelection * This,
    /* [in] */ BSTR bstrMangledName,
    /* [in] */ BSTR bstrFullName,
    /* [in] */ VARIANT_BOOL bInstrument,
    /* [in] */ long lReason,
    /* [retval][out] */ IFunctionData **ppFD);


void __RPC_STUB IFunctionSelection_AddFunction_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_GetFeedbackMessage_Proxy( 
    IFunctionSelection * This,
    /* [in] */ long MsgID,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionSelection_GetFeedbackMessage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_GetFunctions_Proxy( 
    IFunctionSelection * This,
    /* [retval][out] */ VARIANT *pvar);


void __RPC_STUB IFunctionSelection_GetFunctions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_UpdateInstrumentationRequests_Proxy( 
    IFunctionSelection * This,
    /* [in] */ VARIANT varRequests);


void __RPC_STUB IFunctionSelection_UpdateInstrumentationRequests_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionSelection_get_DataFileName_Proxy( 
    IFunctionSelection * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionSelection_get_DataFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IFunctionSelection_INTERFACE_DEFINED__ */


#ifndef __IFunctionData_INTERFACE_DEFINED__
#define __IFunctionData_INTERFACE_DEFINED__

/* interface IFunctionData */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IFunctionData;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EEBA823F-6BC6-42b6-B91F-648D175F8638")
    IFunctionData : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MangledName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FullName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ClassName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationRequested( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstrumentationRequested( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstrumentationPerformed( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Reason( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ReasonMessage( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AdditionalInfo( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFunctionDataVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFunctionData * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFunctionData * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFunctionData * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IFunctionData * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IFunctionData * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IFunctionData * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IFunctionData * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MangledName )( 
            IFunctionData * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FullName )( 
            IFunctionData * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ClassName )( 
            IFunctionData * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationRequested )( 
            IFunctionData * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstrumentationRequested )( 
            IFunctionData * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstrumentationPerformed )( 
            IFunctionData * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Reason )( 
            IFunctionData * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ReasonMessage )( 
            IFunctionData * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AdditionalInfo )( 
            IFunctionData * This,
            /* [retval][out] */ BSTR *pVal);
        
        END_INTERFACE
    } IFunctionDataVtbl;

    interface IFunctionData
    {
        CONST_VTBL struct IFunctionDataVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFunctionData_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IFunctionData_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IFunctionData_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IFunctionData_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IFunctionData_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IFunctionData_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IFunctionData_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IFunctionData_get_MangledName(This,pVal)	\
    (This)->lpVtbl -> get_MangledName(This,pVal)

#define IFunctionData_get_FullName(This,pVal)	\
    (This)->lpVtbl -> get_FullName(This,pVal)

#define IFunctionData_get_ClassName(This,pVal)	\
    (This)->lpVtbl -> get_ClassName(This,pVal)

#define IFunctionData_get_InstrumentationRequested(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationRequested(This,pVal)

#define IFunctionData_put_InstrumentationRequested(This,newVal)	\
    (This)->lpVtbl -> put_InstrumentationRequested(This,newVal)

#define IFunctionData_get_InstrumentationPerformed(This,pVal)	\
    (This)->lpVtbl -> get_InstrumentationPerformed(This,pVal)

#define IFunctionData_get_Reason(This,pVal)	\
    (This)->lpVtbl -> get_Reason(This,pVal)

#define IFunctionData_get_ReasonMessage(This,pVal)	\
    (This)->lpVtbl -> get_ReasonMessage(This,pVal)

#define IFunctionData_get_AdditionalInfo(This,pVal)	\
    (This)->lpVtbl -> get_AdditionalInfo(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_MangledName_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionData_get_MangledName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_FullName_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionData_get_FullName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_ClassName_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionData_get_ClassName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_InstrumentationRequested_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IFunctionData_get_InstrumentationRequested_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IFunctionData_put_InstrumentationRequested_Proxy( 
    IFunctionData * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IFunctionData_put_InstrumentationRequested_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_InstrumentationPerformed_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IFunctionData_get_InstrumentationPerformed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_Reason_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB IFunctionData_get_Reason_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_ReasonMessage_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionData_get_ReasonMessage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IFunctionData_get_AdditionalInfo_Proxy( 
    IFunctionData * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IFunctionData_get_AdditionalInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IFunctionData_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_CGC_TLB_0275 */
/* [local] */ 

typedef 
enum enumCGCollectorTypes
    {	UNKNOWN_TYPE	= -1,
	KSL32	= UNKNOWN_TYPE + 1,
	RLinuxKSL32	= KSL32 + 1,
	TP32	= RLinuxKSL32 + 1,
	RLinuxNewTP	= TP32 + 1
    } 	enumCGCollectorTypes;



extern RPC_IF_HANDLE __MIDL_itf_CGC_TLB_0275_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_CGC_TLB_0275_v0_0_s_ifspec;

#ifndef __ICGType_INTERFACE_DEFINED__
#define __ICGType_INTERFACE_DEFINED__

/* interface ICGType */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICGType;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("80DB6E14-2715-4d6f-A186-06881095454B")
    ICGType : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CGTYPE( 
            /* [retval][out] */ enumCGCollectorTypes *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CGTYPE( 
            /* [in] */ enumCGCollectorTypes newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICGTypeVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICGType * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICGType * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICGType * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICGType * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICGType * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICGType * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICGType * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CGTYPE )( 
            ICGType * This,
            /* [retval][out] */ enumCGCollectorTypes *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CGTYPE )( 
            ICGType * This,
            /* [in] */ enumCGCollectorTypes newVal);
        
        END_INTERFACE
    } ICGTypeVtbl;

    interface ICGType
    {
        CONST_VTBL struct ICGTypeVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICGType_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICGType_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICGType_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICGType_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICGType_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICGType_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICGType_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICGType_get_CGTYPE(This,pVal)	\
    (This)->lpVtbl -> get_CGTYPE(This,pVal)

#define ICGType_put_CGTYPE(This,newVal)	\
    (This)->lpVtbl -> put_CGTYPE(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGType_get_CGTYPE_Proxy( 
    ICGType * This,
    /* [retval][out] */ enumCGCollectorTypes *pVal);


void __RPC_STUB ICGType_get_CGTYPE_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ICGType_put_CGTYPE_Proxy( 
    ICGType * This,
    /* [in] */ enumCGCollectorTypes newVal);


void __RPC_STUB ICGType_put_CGTYPE_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICGType_INTERFACE_DEFINED__ */


#ifndef __ICGExeController_INTERFACE_DEFINED__
#define __ICGExeController_INTERFACE_DEFINED__

/* interface ICGExeController */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICGExeController;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7CA11744-38ED-4a5b-9C2F-2900ECF59E7D")
    ICGExeController : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBistroDir( 
            BSTR bstrDir,
            BOOL bCalibration) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCollector( 
            IUnknown *pUnk) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallFireDone( 
            long lCancelled) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallFirePostRunExe( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AppWasLaunched( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSnapshotStatus( 
            /* [out] */ VARIANT *pProcs,
            /* [out] */ VARIANT *pPIDs,
            /* [out] */ VARIANT *pStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetSnapshotReply( 
            /* [in] */ long lReply) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICGExeControllerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICGExeController * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICGExeController * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICGExeController * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICGExeController * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICGExeController * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICGExeController * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICGExeController * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBistroDir )( 
            ICGExeController * This,
            BSTR bstrDir,
            BOOL bCalibration);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCollector )( 
            ICGExeController * This,
            IUnknown *pUnk);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallFireDone )( 
            ICGExeController * This,
            long lCancelled);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallFirePostRunExe )( 
            ICGExeController * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AppWasLaunched )( 
            ICGExeController * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSnapshotStatus )( 
            ICGExeController * This,
            /* [out] */ VARIANT *pProcs,
            /* [out] */ VARIANT *pPIDs,
            /* [out] */ VARIANT *pStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetSnapshotReply )( 
            ICGExeController * This,
            /* [in] */ long lReply);
        
        END_INTERFACE
    } ICGExeControllerVtbl;

    interface ICGExeController
    {
        CONST_VTBL struct ICGExeControllerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICGExeController_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICGExeController_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICGExeController_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICGExeController_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICGExeController_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICGExeController_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICGExeController_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICGExeController_SetBistroDir(This,bstrDir,bCalibration)	\
    (This)->lpVtbl -> SetBistroDir(This,bstrDir,bCalibration)

#define ICGExeController_SetCollector(This,pUnk)	\
    (This)->lpVtbl -> SetCollector(This,pUnk)

#define ICGExeController_CallFireDone(This,lCancelled)	\
    (This)->lpVtbl -> CallFireDone(This,lCancelled)

#define ICGExeController_CallFirePostRunExe(This)	\
    (This)->lpVtbl -> CallFirePostRunExe(This)

#define ICGExeController_get_AppWasLaunched(This,pVal)	\
    (This)->lpVtbl -> get_AppWasLaunched(This,pVal)

#define ICGExeController_GetSnapshotStatus(This,pProcs,pPIDs,pStatus)	\
    (This)->lpVtbl -> GetSnapshotStatus(This,pProcs,pPIDs,pStatus)

#define ICGExeController_SetSnapshotReply(This,lReply)	\
    (This)->lpVtbl -> SetSnapshotReply(This,lReply)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGExeController_SetBistroDir_Proxy( 
    ICGExeController * This,
    BSTR bstrDir,
    BOOL bCalibration);


void __RPC_STUB ICGExeController_SetBistroDir_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGExeController_SetCollector_Proxy( 
    ICGExeController * This,
    IUnknown *pUnk);


void __RPC_STUB ICGExeController_SetCollector_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGExeController_CallFireDone_Proxy( 
    ICGExeController * This,
    long lCancelled);


void __RPC_STUB ICGExeController_CallFireDone_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGExeController_CallFirePostRunExe_Proxy( 
    ICGExeController * This);


void __RPC_STUB ICGExeController_CallFirePostRunExe_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICGExeController_get_AppWasLaunched_Proxy( 
    ICGExeController * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICGExeController_get_AppWasLaunched_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGExeController_GetSnapshotStatus_Proxy( 
    ICGExeController * This,
    /* [out] */ VARIANT *pProcs,
    /* [out] */ VARIANT *pPIDs,
    /* [out] */ VARIANT *pStatus);


void __RPC_STUB ICGExeController_GetSnapshotStatus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICGExeController_SetSnapshotReply_Proxy( 
    ICGExeController * This,
    /* [in] */ long lReply);


void __RPC_STUB ICGExeController_SetSnapshotReply_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICGExeController_INTERFACE_DEFINED__ */



#ifndef __CGCTPLib_LIBRARY_DEFINED__
#define __CGCTPLib_LIBRARY_DEFINED__

/* library CGCTPLib */
/* [helpstring][version][uuid] */ 

typedef 
enum enumMessages
    {	MSG_SNAPSHOT_STARTED	= 0,
	MSG_SNAPSHOT_INPROGRESS	= MSG_SNAPSHOT_STARTED + 1,
	MSG_SNAPSHOT_FINISHED	= MSG_SNAPSHOT_INPROGRESS + 1
    } 	enumMessages;


typedef 
enum enumSnapshotReply
    {	SNAPSHOT_RETRY	= 0,
	SNAPSHOT_STOP	= SNAPSHOT_RETRY + 1,
	SNAPSHOT_COMPLETE_STOP	= SNAPSHOT_STOP + 1
    } 	enumSnapshotReply;


EXTERN_C const IID LIBID_CGCTPLib;

EXTERN_C const CLSID CLSID_CGCollector_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("0284F663-A22C-4035-87AD-E055659D0A7F")
CGCollector_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_CGCollector_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("AB1A835A-4A4B-412a-BD59-98ED3C9E5734")
CGCollector_TP32;
#endif

EXTERN_C const CLSID CLSID_ProgressBarCB_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("59C873A2-3237-465a-9374-C00C58A894EE")
ProgressBarCB_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_ProgressBarCB_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("CC560FC3-A9A0-4f0b-93C1-BE695BA34E9F")
ProgressBarCB_TP32;
#endif

EXTERN_C const CLSID CLSID_TPCollectorPropertyPageInstrumentation_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("153B65B8-4671-4b28-9D84-1911430FA2D7")
TPCollectorPropertyPageInstrumentation_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_TPCollectorPropertyPageInstrumentation_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("3DC687A6-7CDA-4582-B86E-E10C6EE6A75D")
TPCollectorPropertyPageInstrumentation_TP32;
#endif

EXTERN_C const CLSID CLSID_StatusMsg_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("EACA6A84-DA90-4644-8C06-7DB1936D0F96")
StatusMsg_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_StatusMsg_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("7AF5C8B6-0D60-4224-9313-7D2E64DC8214")
StatusMsg_TP32;
#endif

EXTERN_C const CLSID CLSID_TPCollectorPropertyPageMiscellaneous;

#ifdef __cplusplus

class DECLSPEC_UUID("D2AF853A-37C4-42d5-8D18-D37849B3D470")
TPCollectorPropertyPageMiscellaneous;
#endif

EXTERN_C const CLSID CLSID_TPCollectorPropertyPageCollection;

#ifdef __cplusplus

class DECLSPEC_UUID("6E956BA7-4646-4966-B2B2-3C220F5CA523")
TPCollectorPropertyPageCollection;
#endif

EXTERN_C const CLSID CLSID_TPCollectorPropertyPageFilters;

#ifdef __cplusplus

class DECLSPEC_UUID("BACDAFF5-9837-4CCC-BE2E-1C7A0B05661E")
TPCollectorPropertyPageFilters;
#endif

EXTERN_C const CLSID CLSID_ErrorMsgCB_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("FEF33CAE-8F21-478e-9D2C-BAAA188097E8")
ErrorMsgCB_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_ErrorMsgCB_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("932CBBDD-8CFE-4277-AE66-1CD17D28E027")
ErrorMsgCB_TP32;
#endif

EXTERN_C const CLSID CLSID_Controller_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("99F68D6B-A87E-420c-93F7-0FE7FE4F84FB")
Controller_TP32;
#endif

EXTERN_C const CLSID CLSID_Controller_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("638E65A3-5E81-491b-B682-50A3EE21F8CE")
Controller_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_Controller_TP_emt64;

#ifdef __cplusplus

class DECLSPEC_UUID("157C99D6-A2F1-4caf-8638-33DD28D455E2")
Controller_TP_emt64;
#endif

EXTERN_C const CLSID CLSID_ProblemDescriptor_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("A2D9BBCA-F1B2-4a10-88A9-A1F036B661EE")
ProblemDescriptor_TP32;
#endif

EXTERN_C const CLSID CLSID_ProblemDescriptor_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("62B257F2-F182-42ce-ADC7-51178278DB2B")
ProblemDescriptor_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_ProblemDescriptor_TP_emt64;

#ifdef __cplusplus

class DECLSPEC_UUID("2EF8B282-B820-4db3-87AA-0B567DB8CDC2")
ProblemDescriptor_TP_emt64;
#endif

EXTERN_C const CLSID CLSID_StartStopConsole_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("9B3DEF17-3F18-4a16-8FCE-2AB4348166EC")
StartStopConsole_TP32;
#endif

EXTERN_C const CLSID CLSID_StartStopConsole_TP_emt64;

#ifdef __cplusplus

class DECLSPEC_UUID("CB4FDF1D-098F-4938-B40E-48118DAA5810")
StartStopConsole_TP_emt64;
#endif

EXTERN_C const CLSID CLSID_StartStopConsole_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("D3FD9EB2-00DA-4430-B968-210C5F881D29")
StartStopConsole_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_FunctionSelection_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("E095A4A5-B23E-4208-94AF-CF38D5B6B8DC")
FunctionSelection_TP32;
#endif

EXTERN_C const CLSID CLSID_FunctionSelection_TP_emt64;

#ifdef __cplusplus

class DECLSPEC_UUID("17CF0B0D-0F88-423b-8144-95AAABC34311")
FunctionSelection_TP_emt64;
#endif

EXTERN_C const CLSID CLSID_FunctionData_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("A04FC5F3-E5B6-47c5-BDB7-E4074A83C4D2")
FunctionData_TP32;
#endif

EXTERN_C const CLSID CLSID_FunctionData_TP_emt64;

#ifdef __cplusplus

class DECLSPEC_UUID("E6DEA045-9B41-440e-9C33-898724BC8797")
FunctionData_TP_emt64;
#endif

EXTERN_C const CLSID CLSID_FunctionSelection_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("08F25B5D-91EC-46a0-A01E-5E8F8838BBCD")
FunctionSelection_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_FunctionData_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("08187B0C-902B-4b67-9AE5-28E2B18B8715")
FunctionData_RLinuxNewTP;
#endif

EXTERN_C const CLSID CLSID_CGExeController_TP32;

#ifdef __cplusplus

class DECLSPEC_UUID("0F1705DA-A849-40a1-93F1-9434DD60FEF3")
CGExeController_TP32;
#endif

EXTERN_C const CLSID CLSID_CGExeController_TP_emt64;

#ifdef __cplusplus

class DECLSPEC_UUID("74838CE0-5B36-4a7c-A48F-7F53F83FB826")
CGExeController_TP_emt64;
#endif

EXTERN_C const CLSID CLSID_CGExeController_RLinuxNewTP;

#ifdef __cplusplus

class DECLSPEC_UUID("101C9D5C-AA0A-4600-8697-5875338ED30E")
CGExeController_RLinuxNewTP;
#endif
#endif /* __CGCTPLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


