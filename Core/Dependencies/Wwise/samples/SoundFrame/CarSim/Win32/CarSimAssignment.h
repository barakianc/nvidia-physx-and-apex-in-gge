//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <AK/SoundFrame/SF.h>

using namespace AK;
using namespace SoundFrame;

class CarSimAssignment
{
public:
	// Default constructor ...
	CarSimAssignment();
	~CarSimAssignment();

	void Clear();
	void Assign( const CarSimAssignment& in_rAssignement );

	void RemoveObject( AkUniqueID in_objectID );

	IGameParameter* m_pRPMParam;
	IGameParameter* m_pLoadParam;
	IGameParameter* m_pSpeedParam;

	IEvent*			m_pEnginePlayEvent;
	IEvent*			m_pEngineStopEvent;
	IEvent*			m_pUpShiftEvent;
	IEvent*			m_pDownShiftEvent;

	IAuxBus*		m_pAuxBus;

private:
	CarSimAssignment(const CarSimAssignment&);
	CarSimAssignment& operator=( const CarSimAssignment& );
};