//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "CarSimHelpers.h"

class CarSimEngine;

class CarSimSimulationSerializer
{
public:
	CarSimSimulationSerializer();
	~CarSimSimulationSerializer();

	void Init( CarSimEngine* in_pEngine );

	void ClearFileName();

	bool Save( const CString& in_rFileName );
	bool Load( const CString& in_rFileName );

	const CString& GetCurrentFilePath() const;

	// Return the file name with no extension
	CString GetCurrentFileNameOnly() const;

	const CString& GetFileExtension() const;

private:

	void EnsureFileExtension( CString& io_rFileName );

	template<class TObject>
	bool LoadAssignment( ReadBytesInFile& in_rReader, TObject*& in_rpObject );

	template<class TObject>
	bool SaveAssignment( WriteBytesInFile& in_rWriter, TObject*& in_rpObject );

	CString m_csLastFile;

	CarSimEngine* m_pCarSimEngine;
};