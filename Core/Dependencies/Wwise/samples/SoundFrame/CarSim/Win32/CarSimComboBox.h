//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "afxwin.h"
#include "resource.h"

template<class TObject, class TList>
class CarSimAssignmentComboBox : public CComboBox
{
public:
	void ClearCombo()
	{
		int cObjects = GetCount();
		for ( int i = 0; i < cObjects; i++ )
		{
			TObject * pObject = (TObject *) GetItemDataPtr( i );
			if ( pObject )
				pObject->Release();
		}

		ResetContent();
	}

	TObject* GetSelectedObject()
	{
		int idx = GetCurSel();

		if( idx == CB_ERR )
			return NULL;

		return (TObject*) GetItemDataPtr( idx );
	}

	void AddObjectAndDisable( TObject* in_pObject )
	{
		if( in_pObject )
		{
			int idx = AddObject( in_pObject );
			SetCurSel( idx );
		}

		EnableWindow( FALSE );
	}

	int AddObject( TObject* in_pObject )
	{
		int idx = AddString( in_pObject->GetName() );
		SetItemDataPtr( idx, in_pObject );

		in_pObject->AddRef();

		return idx;
	}

	void FillFromList( TList * in_pObjectList, TObject* in_pObjectToSelect )
	{
		ClearCombo();

		CString csNone;
		csNone.LoadString( IDS_ASSIGNMENT_NONE );
		
		AddString( csNone );
		SetItemDataPtr( 0, NULL );

		if( in_pObjectList )
		{
			while ( TObject * pObject = in_pObjectList->Next() )
			{
				AddObject( pObject );
			}
		}

		if( ! in_pObjectToSelect || ! SelectObject( in_pObjectToSelect->GetID() ) )
			SetCurSel( 0 );

		if( in_pObjectList )
			in_pObjectList->Reset();
	}

	bool SelectObject( AkUniqueID in_ObjectID )
	{
		int cObjects = GetCount();
		for ( int i = 0; i < cObjects; i++ )
		{
			TObject * pObject = (TObject *) GetItemDataPtr( i );
			if ( pObject && pObject->GetID() == in_ObjectID )
			{
				SetCurSel( i );
				return true;
			}
		}

		return false;
	}

	// Return true if the object was in the list
	bool RemoveObject( AkUniqueID in_ObjectID )
	{
		int cObjects = GetCount();
		int iSel = GetCurSel();
		for ( int i = (cObjects - 1); i >= 0; --i )
		{
			TObject * pObject = (TObject *) GetItemDataPtr( i );
			if ( pObject && pObject->GetID() == in_ObjectID )
			{
				pObject->Release();
				DeleteString( i );

				if( iSel == i )
					SetCurSel( 0 );

				return true;
			}
		}

		return false;
	}
};