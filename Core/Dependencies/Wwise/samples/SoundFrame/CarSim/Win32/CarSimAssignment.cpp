//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CarSimAssignment.h"
#include "CarSimHelpers.h"

CarSimAssignment::CarSimAssignment()
	: m_pRPMParam( NULL )
	, m_pLoadParam( NULL )
	, m_pSpeedParam( NULL )
	, m_pEnginePlayEvent( NULL )
	, m_pEngineStopEvent( NULL )
	, m_pUpShiftEvent( NULL )
	, m_pDownShiftEvent( NULL )
	, m_pAuxBus( NULL )
{}

CarSimAssignment::~CarSimAssignment()
{
	Clear();
}

void CarSimAssignment::Clear()
{
	ReleaseClear( m_pRPMParam );
	ReleaseClear( m_pLoadParam );
	ReleaseClear( m_pSpeedParam );
	ReleaseClear( m_pEnginePlayEvent );
	ReleaseClear( m_pEngineStopEvent );
	ReleaseClear( m_pUpShiftEvent );
	ReleaseClear( m_pDownShiftEvent );
	ReleaseClear( m_pAuxBus );
}
	
void CarSimAssignment::Assign( const CarSimAssignment& in_rAssignement )
{
	Clear();

	m_pRPMParam = in_rAssignement.m_pRPMParam;
	m_pLoadParam = in_rAssignement.m_pLoadParam;
	m_pSpeedParam = in_rAssignement.m_pSpeedParam;
	m_pEnginePlayEvent = in_rAssignement.m_pEnginePlayEvent;
	m_pEngineStopEvent = in_rAssignement.m_pEngineStopEvent;
	m_pUpShiftEvent = in_rAssignement.m_pUpShiftEvent;
	m_pDownShiftEvent = in_rAssignement.m_pDownShiftEvent;
	m_pAuxBus = in_rAssignement.m_pAuxBus;

	SafeAddRef( m_pRPMParam );
	SafeAddRef( m_pLoadParam );
	SafeAddRef( m_pSpeedParam );
	SafeAddRef( m_pEnginePlayEvent );
	SafeAddRef( m_pEngineStopEvent );
	SafeAddRef( m_pUpShiftEvent );
	SafeAddRef( m_pDownShiftEvent );
	SafeAddRef( m_pAuxBus );
}

void CarSimAssignment::RemoveObject( AkUniqueID in_objectID )
{
	if( m_pRPMParam && m_pRPMParam->GetID() == in_objectID )
		ReleaseClear( m_pRPMParam );
	else if( m_pLoadParam && m_pLoadParam->GetID() == in_objectID )
		ReleaseClear( m_pLoadParam );
	else if( m_pSpeedParam && m_pSpeedParam->GetID() == in_objectID )
		ReleaseClear( m_pSpeedParam );
	else if( m_pEnginePlayEvent && m_pEnginePlayEvent->GetID() == in_objectID )
		ReleaseClear( m_pEnginePlayEvent );
	else if( m_pEngineStopEvent && m_pEngineStopEvent->GetID() == in_objectID )
		ReleaseClear( m_pEngineStopEvent );
	else if( m_pUpShiftEvent && m_pUpShiftEvent->GetID() == in_objectID )
		ReleaseClear( m_pUpShiftEvent );
	else if( m_pDownShiftEvent && m_pDownShiftEvent->GetID() == in_objectID )
		ReleaseClear( m_pDownShiftEvent );
	else if( m_pAuxBus && m_pAuxBus->GetID() == in_objectID )
		ReleaseClear( m_pAuxBus );
}