//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "CarSimSimulationSerializer.h"
#include "CarSimEngine.h"

namespace
{
	static const ULONG s_uiVersionNumber = 2;
	static const CString s_csFileExtension( L".carsim" );
}

CarSimSimulationSerializer::CarSimSimulationSerializer()
:	m_pCarSimEngine( NULL )
{}
	
CarSimSimulationSerializer::~CarSimSimulationSerializer()
{}

void CarSimSimulationSerializer::Init( CarSimEngine* in_pEngine )
{
	m_pCarSimEngine = in_pEngine;
}

void CarSimSimulationSerializer::ClearFileName()
{
	m_csLastFile = L"";
}

bool CarSimSimulationSerializer::Save( const CString& in_rFileName )
{
	if( !m_pCarSimEngine )
		return false;

	CString csFileName( in_rFileName );
	EnsureFileExtension( csFileName );

	WriteBytesInFile fileWriteBytes;
	if( ! fileWriteBytes.Open( csFileName ) )
		return false;

	if( ! fileWriteBytes.Write<ULONG>( s_uiVersionNumber )
		|| ! fileWriteBytes.WriteString( m_pCarSimEngine->GetGameObjectName() )
		|| ! fileWriteBytes.WriteString( m_pCarSimEngine->GetCarParamSerializer().GetCurrentFilePath() ) )
	{
		return false;
	}

	CarSimAssignment carAssignment;
	carAssignment.Assign( m_pCarSimEngine->GetAssignement() );

	if( ! SaveAssignment( fileWriteBytes, carAssignment.m_pRPMParam ) 
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pLoadParam )
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pSpeedParam )
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pEnginePlayEvent )
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pEngineStopEvent )
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pUpShiftEvent )
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pDownShiftEvent )
		|| ! SaveAssignment( fileWriteBytes, carAssignment.m_pAuxBus ) )
	{
		return false;
	}

	double dblDryVolume = 0.0;
	double dblWetVolume = 0.0;
	bool bUseAuxBus = m_pCarSimEngine->GetAuxBusSettings( dblDryVolume, dblWetVolume );
	
	if( ! fileWriteBytes.Write<bool>( bUseAuxBus ) )
	{
		return false;
	}

	if( ! fileWriteBytes.Write<ULONG>( m_pCarSimEngine->GetFrameInterval() ) )
		return false;

	m_csLastFile = csFileName;

	return true;
}
	
bool CarSimSimulationSerializer::Load( const CString& in_rFileName )
{
	if( !m_pCarSimEngine )
		return false;

	CString csGameObjectName, csCarSettingsFile;

	CString csFileName( in_rFileName );
	EnsureFileExtension( csFileName );

	ReadBytesInFile fileReadBytes;
	if( ! fileReadBytes.Open( csFileName ) )
		return false;

	UINT version = fileReadBytes.Read<UINT>();
	if( version != s_uiVersionNumber )
		return false;

	WCHAR szBuffer[_MAX_PATH] = {0};
	if( ! fileReadBytes.ReadString( szBuffer, _MAX_PATH ) )
		return false;

	csGameObjectName = szBuffer;
	
	if( ! fileReadBytes.ReadString( szBuffer, _MAX_PATH ) )
		return false;

	csCarSettingsFile = szBuffer;

	CarSimAssignment carAssignment;
	if( ! LoadAssignment( fileReadBytes, carAssignment.m_pRPMParam )
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pLoadParam ) 
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pSpeedParam ) 
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pEnginePlayEvent ) 
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pEngineStopEvent ) 
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pUpShiftEvent ) 
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pDownShiftEvent ) 
		|| ! LoadAssignment( fileReadBytes, carAssignment.m_pAuxBus ) )
	{
		return false;
	}

	bool bUseAuxBus = fileReadBytes.Read<bool>();
	double dblDryVolume = 1.0;
	double dblWetVolume = 0.0;

	ULONG ulTimeInterval =  fileReadBytes.Read<ULONG>();

	m_pCarSimEngine->ChangeGameObject( csGameObjectName );
	m_pCarSimEngine->SetAssignement( carAssignment );
	m_pCarSimEngine->SetAuxBusSettings( bUseAuxBus, dblDryVolume, dblWetVolume );
	m_pCarSimEngine->SetFrameInterval( ulTimeInterval );

	bool bSetDefault = true;
	if( ! csCarSettingsFile.IsEmpty() )
	{
		bSetDefault = ! m_pCarSimEngine->GetCarParamSerializer().Load( csCarSettingsFile );
		if( bSetDefault )
		{
			CString csError;
			csError.Format( IDS_OPEN_ERROR, csCarSettingsFile );
			MessageBox( NULL, csError, NULL, MB_ICONERROR | MB_OK );
		}
	}
	
	if( bSetDefault )
	{
		m_pCarSimEngine->GetCarParamSerializer().ClearFileName();
		m_pCarSimEngine->GetCar().SetDefaultCarParam();
	}

	m_csLastFile = csFileName;

	return true;
}

const CString& CarSimSimulationSerializer::GetCurrentFilePath() const
{
	return m_csLastFile;
}

CString CarSimSimulationSerializer::GetCurrentFileNameOnly() const
{
	return CarSimHelper::GetFileNameOnly( m_csLastFile );
}
	
const CString& CarSimSimulationSerializer::GetFileExtension() const
{
	return s_csFileExtension;
}

void CarSimSimulationSerializer::EnsureFileExtension( CString& io_rFileName )
{
	CarSimHelper::EnsureFileExtension( io_rFileName, s_csFileExtension );
}

template<class TObject>
bool CarSimSimulationSerializer::LoadAssignment( ReadBytesInFile& in_rReader, TObject*& in_rpObject )
{
	bool retVal = true;

	bool bHaveObject = in_rReader.Read<bool>();

	if( bHaveObject )
	{
		in_rpObject = TObject::From( &in_rReader );

		retVal = in_rpObject != NULL;
	}

	return retVal;
}

template<class TObject>
bool CarSimSimulationSerializer::SaveAssignment( WriteBytesInFile& in_rWriter, TObject*& in_rpObject )
{
	bool retVal = in_rWriter.Write<bool>( in_rpObject == NULL ? false : true );

	if( in_rpObject )
	{
		retVal = in_rpObject->To( &in_rWriter );
	}

	return retVal;
}
