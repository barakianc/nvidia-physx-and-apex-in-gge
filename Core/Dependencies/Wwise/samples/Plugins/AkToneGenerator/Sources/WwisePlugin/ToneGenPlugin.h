//////////////////////////////////////////////////////////////////////
//
// ToneGenPlugin.h
//
// Tone generator Wwise plugin implementation.
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <AK/Wwise/AudioPlugin.h>
 
// Tone generator property names
static LPCWSTR szStartFreq = L"StartFreq";
static LPCWSTR szStartFreqRandMin = L"StartFreqRandMin";
static LPCWSTR szStartFreqRandMax = L"StartFreqRandMax";
static LPCWSTR szSweepFreq = L"SweepFreq";
static LPCWSTR szSweepFreqType = L"SweepFreqType";
static LPCWSTR szStopFreq = L"StopFreq";
static LPCWSTR szStopFreqRandMin = L"StopFreqRandMin";
static LPCWSTR szStopFreqRandMax = L"StopFreqRandMax";
static LPCWSTR szWaveType = L"WaveType";
static LPCWSTR szWaveGain = L"WaveGain";
static LPCWSTR szDurMode = L"DurMode";
static LPCWSTR szFixDur = L"FixDur";
static LPCWSTR szAttackTime = L"AttackTime";
static LPCWSTR szDecayTime = L"DecayTime";
static LPCWSTR szSustainTime = L"SustainTime";
static LPCWSTR szSustainLevel = L"SustainLevel";
static LPCWSTR szReleaseTime = L"ReleaseTime";
static LPCWSTR szChannelMask = L"ChannelMask";

class ToneGenPlugin : public AK::Wwise::IAudioPlugin
{
public:
	ToneGenPlugin(AkUInt16 in_idPlugin);
	~ToneGenPlugin();

	// AK::Wwise::IPluginBase
	// Implement the destruction of the Wwise source plugin.
	virtual void Destroy();

	// AK::Wwise::IAudioPlugin

	// Wwise application must be able to know if the plugin is ready to be played or not
	virtual bool IsPlayable() const;

	virtual void InitToDefault() {}

	virtual void Delete(){}

	/// Load file 
	virtual bool Load( AK::IXmlTextReader* in_pReader );
	/// Save file
	virtual bool Save( AK::IXmlTextWriter* in_pWriter );

	virtual bool CopyInto( IAudioPlugin* io_pWObject ) const { return true; }

	// Set internal values of the property set (allow persistence)
	virtual void SetPluginPropertySet( AK::Wwise::IPluginPropertySet * in_pPSet );
	// Set plugin object store, that allows to create and manage inner objects
	virtual void SetPluginObjectStore( AK::Wwise::IPluginObjectStore * in_pObjectStore ){}
	// Set plugin object data, that allows to create and manage Data files
	virtual void SetPluginObjectMedia( AK::Wwise::IPluginObjectMedia * in_pObjectStore ){}
	// Necessary actions to take on platform change.
	virtual void NotifyCurrentPlatformChanged( const GUID & in_guidCurrentPlatform );
	// Take necessary action on property changes. 
	// Note: not currently implemented, user also has the option of 
	// catching appropriate message in WindowProc function.
	virtual void NotifyPropertyChanged( const GUID & in_guidPlatform, LPCWSTR in_szPropertyName );
	// Notify when a inner object property changes (not used for this plugin)
	virtual void NotifyInnerObjectPropertyChanged( AK::Wwise::IPluginPropertySet* in_pPSet, const GUID & in_guidPlatform, LPCWSTR in_pszPropertyName ){}
	// Notify when a inner object is added or removed (not used for this plugin)
	virtual void NotifyInnerObjectAddedRemoved( AK::Wwise::IPluginPropertySet* in_pPSet, unsigned int in_uiIndex, AK::Wwise::IAudioPlugin::NotifyInnerObjectOperation in_eOperation	){}
	// Get access to UI resource handle.
	virtual HINSTANCE GetResourceHandle() const;
	// Standard window function, user can intercept what ever message that is of interest to him to implement UI behavior.
	virtual bool GetDialog( eDialog in_eDialog, UINT & out_uiDialogID, AK::Wwise::PopulateTableItem *& out_pTable ) const;
	// Store current plugin settings into banks when asked to.
	virtual bool WindowProc( eDialog in_eDialog, HWND in_hWnd, UINT in_message, WPARAM in_wParam, LPARAM in_lParam, LRESULT & out_lResult );
	// Store current plugin settings into banks when asked to.
    virtual bool GetBankParameters( const GUID & in_guidPlatform, AK::Wwise::IWriteData* in_pDataWriter ) const;
	// Used to get plugin-defined data to pass to the sound engine when Wwise is connected to the game.
	virtual bool GetPluginData( const GUID & in_guidPlatform, AkPluginParamID in_idParam, AK::Wwise::IWriteData* in_pDataWriter ) const;
	// Allow Wwise to retrieve a user friendly name for that property's value (e.g. Undo etc.).
	virtual bool DisplayNameForProp( LPCWSTR in_szPropertyName, LPWSTR out_szDisplayName, UINT in_unCharCount ) const;
	// Allow Wwise to retrieve a user friendly name for that property's value (e.g. RTPC etc.).
	virtual bool DisplayNamesForPropValues( LPCWSTR in_szPropertyName, LPWSTR out_szValuesName, UINT in_unCharCount ) const;
	// Implement online help when the user clicks on the "?" icon .
	virtual bool Help( HWND in_hWnd, eDialog in_eDialog ) const;

	virtual void NotifyMonitorData( void * in_pData, unsigned int in_uDataSize, bool in_bNeedsByteSwap ) {}

	virtual AK::Wwise::IPluginMediaConverter* GetPluginMediaConverterInterface();

	static const short CompanyID;
	static const short PluginID;

	//If the same class is used to generate motion data, it needs a separate ID
	static const short MotionPluginID;

private:

	// Window action controls
	// Enable/disable frequency sweeping controls
	void EnableFrequencySweeping( HWND in_hWnd, eDialog in_eDialog, bool in_bEnable );
	// Enable/disable envelope controls
	void EnableEnvelopeControls( HWND in_hWnd, eDialog in_eDialog, bool in_bEnable );
	// Enable/disable all frequency controls
	void EnableFrequencyControls( HWND in_hWnd, eDialog in_eDialog );

	// Enable a single dialog item
	static void EnableDlgItem( HWND in_hwndDialog, UINT in_uiDlgItem, BOOL in_bEnable );

	AK::Wwise::IPluginPropertySet * m_pPSet;

	HWND m_hwndPropView;
	HWND m_hwndObjPane;

	AkUInt16 m_idDialogBig;
	AkUInt16 m_idDialogSmall;
};
