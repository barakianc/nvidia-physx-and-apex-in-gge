//////////////////////////////////////////////////////////////////////
//
// SinePlugin.h
//
// Sine tone Wwise plugin implementation.
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <AK/Wwise/AudioPlugin.h>

class SinePlugin
	: public AK::Wwise::IAudioPlugin
{
public:
	SinePlugin();
	~SinePlugin();

	// AK::Wwise::IPluginBase
	virtual void Destroy();

	// AK::Wwise::IAudioPlugin

	virtual void InitToDefault() {}

	virtual void Delete(){}

	/// Load file 
	virtual bool Load( AK::IXmlTextReader* in_pReader );
	/// Save file
	virtual bool Save( AK::IXmlTextWriter* in_pWriter );

	virtual bool CopyInto( IAudioPlugin* io_pWObject ) const { return true; }

	virtual bool IsPlayable() const;
	virtual void SetPluginPropertySet( AK::Wwise::IPluginPropertySet * in_pPSet );
	virtual void SetPluginObjectStore( AK::Wwise::IPluginObjectStore * in_pObjectStore ){}
	virtual void SetPluginObjectMedia( AK::Wwise::IPluginObjectMedia * in_pObjectStore ){}

	virtual void NotifyCurrentPlatformChanged( const GUID & in_guidCurrentPlatform );
	virtual void NotifyPropertyChanged( const GUID & in_guidPlatform, LPCWSTR in_szPropertyName );
	virtual void NotifyInnerObjectPropertyChanged( AK::Wwise::IPluginPropertySet* in_pPSet, const GUID & in_guidPlatform, LPCWSTR in_pszPropertyName ){}
	virtual void NotifyInnerObjectAddedRemoved( AK::Wwise::IPluginPropertySet* in_pPSet, unsigned int in_uiIndex, AK::Wwise::IAudioPlugin::NotifyInnerObjectOperation in_eOperation	){}

	virtual HINSTANCE GetResourceHandle() const;
	virtual bool GetDialog( eDialog in_eDialog, UINT & out_uiDialogID, AK::Wwise::PopulateTableItem *& out_pTable ) const;
	virtual bool WindowProc( eDialog in_eDialog, HWND in_hWnd, UINT in_message, WPARAM in_wParam, LPARAM in_lParam, LRESULT & out_lResult );

    virtual bool GetBankParameters( const GUID & in_guidPlatform, AK::Wwise::IWriteData* in_pDataWriter ) const;
	virtual bool GetPluginData( const GUID & in_guidPlatform, AkPluginParamID in_idParam, AK::Wwise::IWriteData* in_pDataWriter ) const;

	virtual bool DisplayNameForProp( LPCWSTR in_szPropertyName, LPWSTR out_szDisplayName, UINT in_unCharCount ) const;
	virtual bool DisplayNamesForPropValues( LPCWSTR in_szPropertyName, LPWSTR out_szValuesName, UINT in_unCharCount ) const;

	virtual bool Help( HWND in_hWnd, eDialog in_eDialog ) const;

	virtual void NotifyMonitorData( void * in_pData, unsigned int in_uDataSize, bool in_bNeedsByteSwap ) {}

	virtual AK::Wwise::IPluginMediaConverter* GetPluginMediaConverterInterface();

	static const short CompanyID;
	static const short PluginID;

private:
	AK::Wwise::IPluginPropertySet * m_pPSet;
};
