# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

"""Various utility functions that are likely to be used in more than one
source file"""

from __future__ import with_statement
import os
import re
import sys
import cPickle
import string


HK_REFLECTION_RE = re.compile(r"^\s*HK_DECLARE_REFLECTION\s*\(\s*\)\s*;", re.M)

def hasReflectionDeclaration(txt):
    return HK_REFLECTION_RE.search(txt)

def hasExcludeMarker(txt):
    return txt.find("//HK_REFLECTION_PARSER_EXCLUDE_FILE") != -1

def hasAssemblyExcludeMarker(txt):
    return txt.find("//HK_HAVOK_ASSEMBLY_EXCLUDE_FILE") != -1

def mergeTags(tags1, tags2):
    tagslist1 = tags1.replace("+"," ").split()
    tagslist2 = tags2.replace("+"," ").split()
    if "ALL" in tagslist1 or "ALL" in tagslist2:
        return "ALL"
    tags = tagslist1
    for t2 in tagslist2:
        if not t2 in tags:
            tags.append(t2)
    return "+".join(tags)

HK_TKBMS_HEADER_RE = re.compile("// TKBMS v1.0.+?TKBMS v1.0", re.MULTILINE | re.DOTALL)
HK_TKBMS_TAGS_RE = re.compile("(\w[\w\d_]*)\s*:\s*(.+)")
def extract_tkbms(txt):
    try:
        tkbms = HK_TKBMS_HEADER_RE.search(txt).group(0)
    except AttributeError:
        return {}
    else:
        tags = {}
        for line in tkbms.split("\n"):
            match = HK_TKBMS_TAGS_RE.search(line)
            if match:
                (var,val) = match.group(1,2)
                tags[var.lower()] = val.strip()
        return tags

HK_TKBMS_HEADER_START_RE = re.compile("// TKBMS v1.0")
HK_TKBMS_HEADER_END_RE = re.compile("//\s+\-+TKBMS v1.0")

def extract_tkbms_file(txt):
    if len(txt) < 20:
        # The file (probably) can't have valid headers
        return extract_tkbms(string.join(txt))
    else:
        return extract_tkbms(string.join(txt[:20]))

# Parse a possible template definition and return the various
# parts. Return type is a list, [0] is the template name,
# [1]..[n] are the n template arguments. Extra code after the
# closing > is ignored. If the definition is not a template,
# an empty list is returned.
#
# ValueError is raised if the bracket nesting is not matched
# e.g. hkArray< hkUint32[6 >

bracketMatch = {"<": ">", "[": "]", "<": ">", "(": ")"}

def parse_template(txt):
    ret = []
    bracketList = []
    currentToken = ""
    while len(txt):
        #char = txt.pop(0)
        char = txt[0]
#        sys.stderr.write("%s[%d]" % (char, len(bracketList)))

        txt = txt[1:]
        if len(bracketList) == 0 and char == "<":
            ret.append(currentToken.strip())
            currentToken = ""
            bracketList.append(char)
        elif len(bracketList) == 1 and char == ">":
            popped = bracketList.pop()
            if bracketMatch[popped] != char:
                raise ValueError
#                sys.stderr.write("TOKEN %s=" % currentToken.strip())
            ret.append(currentToken.strip())
            currentToken = ""
        elif char in bracketMatch.keys():
            bracketList.append(char)
            currentToken += char
        elif char in bracketMatch.values():
            if len(bracketList) == 0:
                raise ValueError
            popped = bracketList.pop()
            if bracketMatch[popped] != char:
                raise ValueError
            currentToken += char
        elif len(bracketList) == 1 and char == ",":
#            sys.stderr.write("TOKEN %s=" % currentToken.strip())
            ret.append(currentToken.strip())
            currentToken = ""
        else:
            currentToken += char

    return ret


def writeIfDifferent(text, fname, force_output=False, verbose=False):
    """Write to the output file if it has changed, or force_output"""
    try:
        if open(fname).read() == text:
            if not force_output:
                if verbose:
                    print "UNCHANGED", fname
                return
    except IOError:
        if verbose:
            print "CREATE", fname
    else:
        if verbose:
            print "UPDATE", fname
    open(fname,"w").write(text)


# These need to be out of the module that they are saving
# or else cPickle can not load them properly. Standard pickle
# will work but it is an order of magnitude slower

# Add any import statements required to the modules below
def readDatabase(reflections_db):
    import reflectionDatabase.reflectionDatabase
    try:
        with open(reflections_db, "rb") as dbFile:
            tempObject = cPickle.load(dbFile)
    except Exception, e:
        #print e
        raise cPickle.PickleError
    tempObject.new = False
    return tempObject

def writeDatabase(DBObject, reflections_db):
    import reflectionDatabase.reflectionDatabase
    with open(reflections_db, "wb") as dbFile:

        cPickle.dump(DBObject, dbFile, cPickle.HIGHEST_PROTOCOL)

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
