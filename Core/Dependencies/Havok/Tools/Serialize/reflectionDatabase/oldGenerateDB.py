#
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2010 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
#

#!/usr/bin/env python

# Generate a project database using the deprecated header file parser. This should not be called directly.
# generateDB is the external interface. This examines the project database corresponding
# to the specified project, creating or updating it if necessary and returning the
# resulting (up-to-date) database object
#

from __future__ import with_statement
import reflectionDatabase
from havokDomClass import headerToDom
import string
import sys
import os
import re
import util
# Ths object is not pickled from this file but it needs to catch any errors
from pickle import PickleError

BASE_DIR = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir)


## Translate from settings.build to reflections.db
def getProjectDBFile(project_dir):
    return os.path.join(project_dir, "reflections.db")


def filterProjectFiles(filename):
    if not filename.endswith("h"):
        return None
    fileText = open(filename).read()
    if not util.hasReflectionDeclaration(fileText):
        return None
    filehash = reflectionDatabase.hashFileContents(fileText)
    return (filename, filehash)

## Find all of the files with reflection declarations from a given starting
## point
def findProjectFiles(project_dir):
    fileList = set()
    for dirname, subdirs, files in os.walk(project_dir):
        headers = filter(None, map(filterProjectFiles, [reflectionDatabase.standardFileName(os.path.join(dirname,f)) for f in files]))
        fileList.update(headers)

    return fileList

env_re = re.compile(r'\$\(([\w\d_]+)\)')
## Resolve any environment variables in a given string
## Any substring of the form $(NAME) is replaced with the
## environment variable NAME
def resolveEnv(path):
    ret = env_re.search(path)
    while ret:
        try:
            path = path.replace('$(' + ret.groups()[0] + ')', os.environ[ret.groups()[0]])
#                print "REPLACE $(%s) -> %s" % (ret.groups()[0], path)
        except KeyError:
#                print "NOT FOUND $(%s)" % ret.groups()[0]
            path = path.replace('$(' + ret.groups()[0] + ')', "")
        ret = env_re.search(path)
    return path     

def createDB(project_dir, options):    
    reflections_db = getProjectDBFile(project_dir)
    DBObject = reflectionDatabase.createReflectionDatabase()
    projectFiles = findProjectFiles(project_dir)

    # If there are no reflected files, just return an empty object
    if projectFiles:
        DBObject.project_dir = project_dir
        settings_build = os.path.join(project_dir, "settings.build")

        if os.path.exists(settings_build) and os.path.isfile(settings_build):

            sys.path.append(os.path.join(BASE_DIR, "Build", "ReleaseSystem"))
            import BuildSystem.Modules
            
            with open(settings_build, "r") as project:
                settingsGlobals = {}

                globalSettingsFile = os.path.join(BASE_DIR, "Build", "ReleaseSystem", "global_settings.build")
                if os.path.exists(globalSettingsFile):
                    for line in open(globalSettingsFile).readlines():
                        line=line.strip()
                        if line!='':
                            (key, value) = map(string.strip, line.split('='))
                            value = resolveEnv(value)
                            settingsGlobals[key] = value

                DBObject.settings = BuildSystem.Modules.evalSettings(settings_build, "win32", "msvc", "9", "ai+animation+behavior+cloth+destruction+physics", "", False, "win32", settingsGlobals)


        for (headerfile, filehash) in projectFiles:
            try:
                projectClass = headerToDom.headerToDom(headerfile)
                projectClass.origfilename = reflectionDatabase.standardFileName(headerfile)
                DBObject.Documents[projectClass.origfilename] = projectClass
                DBObject.contentsHash[projectClass.origfilename] = filehash
            except (RuntimeError, IOError, OSError), error:
                print "%s: error creating reflection file : %s" % (headerfile, error)
                pass

    try:
        reflectionDatabase.writeReflectionDatabase(DBObject, reflections_db)
    except:
        # Remove aborted db file and re-raise error
        if os.path.exists(reflections_db):
            os.remove(reflections_db)
        raise

    return DBObject

def projectDependencyFailed(project_dir, reflections_db, DBObject):
    try:
        projectFileList = findProjectFiles(project_dir)
        
        # If any of the files in the DB no longer exist or are no longer in the database, regenerate it
        if any(map(lambda x: x.origfilename not in zip(*projectFileList)[0], DBObject.getDocuments())):
            return True

        # Check the file hash for each included file. Note this is based on the file contents NOT the mtime
        return any(map(lambda (fname, fhash): DBObject.contentsHash.get(fname, "") != fhash, projectFileList)) if projectFileList else False
    except OSError:
        # If any file does not exist or is not readable, fail the dependency
        return True

def generateDB(project_dir, options):
    """Create if necessary and return a project information database"""
    project_dir = reflectionDatabase.standardFileName(project_dir)
    if not os.path.exists(os.path.join(project_dir, "settings.build")):
        # This is a customer build
        setattr(options, "customer_build", True)
        
    reflections_db = getProjectDBFile(project_dir)
    try:
        DBObject = reflectionDatabase.readReflectionDatabase(reflections_db)
    except PickleError:
        DBObject = None
    # Else the exception will be passed on 

    # Check that the objects look the same
    localDB = reflectionDatabase.reflectionDatabase()
    if not DBObject or sorted(localDB.__dict__.keys()) != sorted(DBObject.__dict__.keys()) or localDB.version != DBObject.version:
        DBObject = None
  
    if (not DBObject) or (options and options.force_rebuild) or projectDependencyFailed(project_dir, reflections_db, DBObject):
        return createDB(project_dir, options)
    return DBObject


#
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20100811)
# 
# Confidential Information of Havok.  (C) Copyright 1999-2010
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
# rights, and intellectual property rights in the Havok software remain in
# Havok and/or its suppliers.
# 
# Use of this software for evaluation purposes is subject to and indicates
# acceptance of the End User licence Agreement for this product. A copy of
# the license is included with this software and is also available at www.havok.com/tryhavok.
# 
#
