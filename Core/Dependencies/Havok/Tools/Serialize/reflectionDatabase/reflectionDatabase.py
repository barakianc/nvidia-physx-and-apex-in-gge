# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

# This is the database interface used by the output plugins
# getDocuments() will return a list of reflected dom objects
# (the same as the old headerToDom classes) contained in
# the project. getDocuments(filename) will return only the
# dom object for that file.

# 'project_dir' is the directory containing the project
#
# 'settings' are the project settings, taken from settings.build and global_settings.build
# originally and saved to reflectionSettings.cache for processing and distribution.
#
# 'new' is True if the databasefile is newly created, False if it has been loaded from disk.
# This allows plugins to only generate output if the database has changed

"""Virtual class, generated reflection databases implement this interface"""

from __future__ import with_statement
import os
import hashlib

GLOBAL_VERSION = "2011.2-a"

# Absolute path, with drive letter converted to lowercase
def standardFileName(filename):
    filename = os.path.abspath(filename).replace("\\", "/")
    fname = filename.split(":/")
    if len(fname) > 1:
        fname[0] = fname[0].lower()
        return ":/".join(fname)

    return filename

def hashFile(filename):
    m = hashlib.md5()
    try:
        m.update("%s %s" % (GLOBAL_VERSION, os.path.getmtime(filename)))
    except OSError:
        # Hash in a DB will never be none so always fail if file doesn't exist
        return None
    
    return m.hexdigest()

class reflectionDatabase(object):
    """Virtual class, objects in the database are of this type"""
    def __init__(self):
        object.__init__(self)
        # The directory used to create the DB.
        self.project_dir = ""
        # hkcToDom objects, indexed by standardFileName(filename)
        self.Documents = {}
        # Hash of file, indexed by standardFileName
        # This is guaranteed to be equal to hashFile() of the file
        # at the time the DB is generated
        self.contentsHash = {} 
        # Dict of build settings, taken from reflectionSettings.cache.
        # Empty if not a havok build.
        self.settings = {}
        # True if the DB has been freshly generated (False = loaded from cache)
        self.new = True
        # Version of the database. Checked in code should use
        # "Release-Sub", where Release is the last released version,
        # and Sub is incremented every time a database rebuild is needed
        self.version = GLOBAL_VERSION

    def getDocuments(self, filename = None):
        """Get all document objects in the given file, or all files"""
        if(filename):
            return self.Documents[self.standardFileName(filename)]
        else:
            return self.Documents.values()

def createReflectionDatabase():
    obj = reflectionDatabase()
    return obj

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
