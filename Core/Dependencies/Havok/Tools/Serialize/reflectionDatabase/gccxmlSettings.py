# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

##
## Edit this file to customize the behaviour of gccxml
## Most settings can remain at their defaults
##
## See comments for more information
##

##
## Search path
##
## Add any paths to the search path used for python libraries
## This variable is a list of paths, all will be added
##

HK_PYGCCXML_PATHADD = []
#HK_PYGCCXML_PATHADD = ["/path/to/libraries", "/more/path/to/libraries"]

##
## GCCXML Installation
##
## Specify the path to the gccxml executable
## Leave at None to search in the directories specified
## by the environment variables GCCXML_ROOT and HAVOK_SDKS_DIR
##
# HK_GCCXML_PATH="/path/to/gccxml.exe"
HK_GCCXML_PATH=None


##
## C compiler
##
## gccxml requires an installed C compiler to find a set of
## system libraries. Normally this is autodetected, however
## if more than one compiler is installed, you may need to
## specify which one to use
##

## Visual Studio .NET 9:
# HK_GCCXML_COMPILER="msvc9"

## Visual Studio .NET 8:
# HK_GCCXML_COMPILER="msvc8"

## GCC
# HK_GCCXML_COMPILER="gcc"

## Default - autodetect
HK_GCCXML_COMPILER="msvc9"

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
