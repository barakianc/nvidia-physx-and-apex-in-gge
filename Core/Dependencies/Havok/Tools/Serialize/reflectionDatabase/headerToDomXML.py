# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

# Parse the output of gccxml to create a dom class
# This should not be called directly -- regenerateProjectDB.py knows when
# it needs to be called

import exceptions
import string
import types
import util
import sys
import os
import re

from havokDomClass import hkcToDom


try:
    sys.path.append(os.path.join(os.environ['HAVOK_SDKS_DIR'], "win32", "gccXML", "pygccxml-1.0.0"))
except KeyError:
    pass

def handleErrorIfThisIsRelease(msg):
    if 'HAVOK_SDKS_DIR' not in os.environ:
        import checkPrerequisites
        print msg
        checkPrerequisites.checkPrerequisites()
        sys.exit(-1)

try:
	from pygccxml import parser as pygccxmlParser, declarations as pygccxmlDecls
except:
    handleErrorIfThisIsRelease('Error locating PyGCCXML, launching prerequisites checker...\n')
    raise RuntimeError, """Unable to load pygccxml module. Ensure that it is visible to
the default python installation.See Common Havok Components > Serialization for more information"""


# Comparison function to allow sorting of objects by the order
# they appear in source files
def sortByFileLine(vala, valb):
    return cmp(vala.location.line, valb.location.line)

class hkXMLParser:
    # Various REs applied to source elements
    re_hkAllocator = re.compile(r"^(class|struct)\s+[:]*\w+Allocator$") # Memory allocator, domToClass doesn't expect to see these
    re_CommentLine = re.compile(r"^\s*//.*$") # Match empty lines, and lines containing only whitespace and comments
    re_refQuals = re.compile(r"[\*&]|const|\[\d+\]") # Qualifiers applied to types TODO use pygccxml types instead
    re_wSpace = re.compile(r"\s+") # Qualifiers applied to types TODO use pygccxml types instead
    re_fnPtr = re.compile(r"[\w\s\*\&:]*\([^\*]*\s*\*\s*\)\s*\([^\)]*\)") # Function Pointer
    re_Array = re.compile(r"\w\s*\[(?P<arraySize>\d+)\]") # Standard C Array
    re_Alignment = re.compile(r"(__declspec\(align\((?P<alignmenta>\d+)\)\)|HK_ALIGN(?P<alignmentb>\d+)\(|HK_ALIGN\(.*,\s*(?P<alignmentc>\d+)\);)") # Alignment specifiers
    re_Plus = re.compile(r"\+\w") # Plus followed by word character ( +x ) and not (x + y)
    re_Override = re.compile("(hkArray|hkSimpleArray|hkEnum|hkFlags)\s*<\s*([^>,]+)\s*(,[^>]+)?\s*>") # Used by attribute override
    
    # These types are treated specially by the dom compiler -- don't translate
    # them into their basic representation -- mostly taken from domToClass.py
    # Normally types are converted into their basic representation (collapsing typedefs, explicitly referencing classes/structs etc.)
    specialTypes = ('hkUint8', 'hkUint16', 'hkUint32', 'hkUint64', 'hkVector4', 'hkTransform', 'hkHalf', 'hkClass', 'hkArray', 'hkEnum',
                    'hkQuaternion', 'hkMatrix3', 'hkRotation', 'hkQsTransform', 'hkMatrix4', 'hkStringPtr', 'hkBool', 'hkUFloat8', 'hkVariant',
                    'hkChar', 'hkRelArray')

    # Template types that are supported by domToClass
    supportedTemplateTypes = ["hkArray","hkEnum","hkFlags","hkSimpleArray","hkHomogenousArray", "hkInplaceArray", "hkPointerMap", "hkPointerMultiMap",
                              "hkStringMap", "hkStorageStringMap", "hkCachedHashMap", "hkRelArray"]

    # Convert native types to internal hk types
    hkTypeNames = (('hkUint8', 'unsigned char'), ('hkInt8','signed char'), ('hkUint16','short unsigned int'), ('hkInt16','short int'), ('hkReal','float'), ('hkUlong','long unsigned int'),
                  ('hkLong','long int'), ('hkUint32','unsigned int'), ('hkInt32','int'), ('hkUint64','long long unsigned int'),( 'hkInt64','long long int'), ('hkChar','signed char'))

    # Thrown to jump out of some loops
    class FinishedException(exceptions.Exception):
        pass

    # Parse errors don't need to kill the whole compilation, only the current file
    # so treat them specially
    class ParseErrorException(exceptions.Exception):
        def __init__(self, args = None, arglist = None):
            if arglist:
                self.filename = args
                self.msg = arglist
            else:
                self.filename = args[0]
                self.msg = args[1]    
        pass


    def __init__(self):
        #self.debug = lambda x : sys.stderr.write(x + "\n")
        self.debug = lambda x: None

    # Strip global namespace qualifier, if present
    def stripGlobal(self, val):
        if val.startswith("::"):
            return val[2:]
        return val

    # standardForm converts a type name to a standard form, removing extra spaces, const qualifiers and any
    # unnecessary namespace specifiers
    def standardForm(self, val, namespace):
        val = val.replace('const','')
        val = self.re_wSpace.sub(" ", val)
        val = val.replace(" *","*")
        val = self.stripGlobal(val).strip()

        # namespace should have "::" but not "::::"
        if(namespace.startswith("::::")):
            namespace = namespace[2:]
            
        if val.find("<") > -1 or val.find(",") > -1:
            # Template or arg list, handled specially elsewhere so just ignore it here
            pass
        else:           
            namespace = namespace.split("::")
            for nsIndex in range(0, len(namespace)):
                namespaceName = "::".join(namespace[-(nsIndex+1):])
                if(val.startswith(namespaceName + "::")):
                    val = val[len(namespaceName + "::"):]

        return val

    # Strip off pointer, reference, const, array to get the base type
    def stripQuals(self, hkType):
        return self.re_refQuals.sub("", hkType).strip()


    # Helper class since we pass this information
    # around a good bit. Stores methods to qualify
    # types
    class memberQuals:
        def __init__(self, isConst = False, isReference = False, isPointer = False, isPointerPointer = False, arraySize = 0):
            self.isConst = isConst
            self.isReference = isReference
            self.isPointer = isPointer
            self.arraySize = arraySize
            self.isPointerPointer = isPointerPointer

        def qualify(self, typeString):
            retStr = ""
            if self.isConst:
                retStr = retStr + "const "

            retStr = retStr + typeString
            
            if self.isPointer:
                retStr = retStr + "*"
            if self.isPointerPointer:
                retStr = retStr + "*"
            if self.arraySize:
               retStr = "%s[%d]" % (retStr, self.arraySize)
            if self.isReference:
                retStr = retStr+ "&"

            return retStr

        pass


    # Given a qualified typename as a string, find the python
    # object corresponding to that type. This is quite slow and
    # should only be used where necessary (e.g. parsing template
    # arguments)

    # We can't use the per-file dictionaries since these
    # types could be defined anywhere

    # This will search classes, typedefs and enums in that order
    
    def searchNamespace(self, typeName, filename):
        typeName = self.stripGlobal(typeName)
        nameList = typeName.split("::")

        cTypes = self.global_ns
        for cName in nameList:
            self.debug("CNAME %s" % cName)
            try:
                if filename:
                    cTypes = cTypes.class_(name=cName, header_file = filename)
                else:
                    cTypes = cTypes.class_(name=cName)
            except:
                try:
                    cTypes = cTypes.typedef(name = cName)
                except:
                    try:
                        cTypes = cTypes.enum(name = cName)
                    except:
                        return None
        return cTypes
    

    # Unfortunately, template types are represented as strings in pygccxml
    # This causes problems with complex templates, and causes expensive
    # string -> namespace lookups to resolve the sub-types into primitive
    # types again
    #
    # correctTemplate converts from a template instantiation string into the
    # format expected by domToClass. This includes converting types and
    # carrying out qualification
    
    def correctTemplate(self, typeName, namespace, qual, filename, declDict):
        try:
            templateArgs = util.parse_template(typeName)
            templateName = templateArgs.pop(0)
        except ValueError,IndexError:
            raise self.ParseErrorException((filename, "Template Parse Error: %s" % typeName))

        
        #self.debug("IS A TEMPLATE %s: << %s >>" % (templateName, "; ".join(templateArgs)))

        if templateName == "hkRefPtr" and len(templateArgs) == 1:
            # Special case hkRefPtr<T> -> T*
            retStr = qual.qualify("%s *" % self.correctType(templateArgs[0], namespace, declDict, filename))
        elif templateName == "hkPadSpu":
            # Take out hkPadSpu
            retStr = self.correctType(templateArgs[0], namespace, declDict, filename)
        else:
            # Ordinary Template -- spaces are REQUIRED to prevent nested templates producing C99 digraphs or >> token
            retStr = qual.qualify(
                "%s< %s >" % (templateName,", ".join(filter(lambda x: not self.re_hkAllocator.match(x), map(lambda x: self.correctType(x, namespace, declDict, filename),templateArgs)))))
        return retStr

    # Check if an object is located at a given position in the class hierarchy
    def hierarchyMatch(self, hierarchy, obj):
        if obj.parent.name == hierarchy[-1]:
            if len(hierarchy) == 1:
                return True
            else:
                return self.hierarchyMatch(hierarchy[:-1], obj.parent)                
        else:
            return False

    # Extract the total size of an array and the fundemantal type
    # This will collapse multi-dimensional arrays
    def parseArray(self, arrayType):
        arraySize = 0
        while(pygccxmlDecls.is_array(arrayType)):
            if arraySize:
                arraySize *= pygccxmlDecls.array_size(arrayType)
            else:
                arraySize = pygccxmlDecls.array_size(arrayType)
                
            arrayType = pygccxmlDecls.array_item_type(arrayType)
        return (arrayType, arraySize)

    # Translate from pygccType to hkcDom type
    # This carries out all of the mapping needed to keep
    # hkcDom happy

    # namespace is the (string) name of the currently active namespace. This is only
    # significant for subclasses
    # declDict contains a list of dictionaries to use for string -> namespace lookups
    # filename is the file that is currently being parsed (used for error messages)
    def correctType(self, pygccType, namespace, declDict = None, filename = None):
        if not pygccType:
            raise self.ParseErrorException((filename, "Invalid type: None"))
        

        # typeName is guaranteed to be a string -- pygccType may be either a string or an object
        typeName = pygccType
    
        if hasattr(typeName, "decl_string"):
            typeName = typeName.decl_string
        typeName = self.standardForm(typeName, namespace)

        # Function pointers can not be safely serialized anyway
        # so it's easier to just treat them as a void *
        if pygccxmlDecls.is_calldef_pointer(typeName):
            return "void *"

        if re.search("\(\s*\*\s*\)", typeName):
            typeName = re.sub("(^|[,\(<])([^,\(<]+\(\s*\*\s*\)\s*\((\([^\)]*\)|[^\)])*\)\s*)","\g<1>void *", typeName)

        # Special types are passed straight through to domToClass
        if self.stripQuals(typeName) in self.specialTypes:
            # Convert multi-dimensional arrays to a flat array
            (pygccType, arraySize) = self.parseArray(pygccType)

            qual = self.memberQuals(isConst = pygccxmlDecls.is_const(pygccType), isReference = pygccxmlDecls.is_reference(pygccType),
                                    isPointer = pygccxmlDecls.is_pointer(pygccType), isPointerPointer = pygccxmlDecls.is_pointer(pygccxmlDecls.remove_pointer(pygccType)), arraySize = arraySize)

            return qual.qualify(self.stripQuals(typeName))


        # For some reason short and short int are distinct types according to gccxml
        if typeName.find('short') > -1 and typeName.find('short int') == -1 and typeName.find('short unsigned int') == -1:
            typeName = typeName.replace('short','short int')
        if typeName.find('unsigned short int') > -1:
            typeName = typeName.replace('unsigned short int', 'short unsigned int')
        # Similiarly long long and long long int
        if typeName.find('long long') > -1 and typeName.find('long long int') == -1 and typeName.find('long long unsigned int') == -1:
            typeName = typeName.replace('long long','long long int')
        if typeName.find('unsigned long long int') > -1:
            typeName = typeName.replace('unsigned long long int', 'long long unsigned int')
        
        # Handle the various hk Types first -- processing them as templates will confuse everything
        for hkBase, hkType in self.hkTypeNames:
            strippedTypeName = self.stripQuals(typeName)
            if strippedTypeName == hkBase or strippedTypeName == hkType:

                #self.debug("CONVERT DEBUG %s %s %s" % (strippedTypeName, hkBase, hkType))
                (pygccType, arraySize) = self.parseArray(pygccType)
                
                qual = self.memberQuals(isConst = pygccxmlDecls.is_const(pygccType), isReference = pygccxmlDecls.is_reference(pygccType),
                                        isPointer = pygccxmlDecls.is_pointer(pygccType), isPointerPointer = pygccxmlDecls.is_pointer(pygccxmlDecls.remove_pointer(pygccType)), arraySize = arraySize)

                return qual.qualify(hkBase)

        # Special case
        if typeName == "hkRefVariant":
            retStr = "hkReferencedObject*"
            return retStr

        # If pygccType is already a string, find the corresponding object type
        # This allows typedefs/classes/enums etc. to be resolved correctly

        # Extract the various qualifiers here so that they can be reassembled later on
        if not hasattr(pygccType,"decl_string"):
            pygccType = pygccType.strip()

            qual = self.memberQuals(isConst = pygccType.find("const") > -1, isReference = (pygccType.find("&") > -1),
                                        isPointer = (pygccType.find("*") > -1), isPointerPointer = (pygccType.replace("*", "", 1).find("*") > -1))

            arrayMatch = self.re_Array.search(pygccType)
            if arrayMatch and arrayMatch.groups('arraySize'):
                qual.arraySize = arrayMatch.groups('arraySize')
                pygccType = self.stripQuals(pygccType)

            if pygccxmlDecls.templates.is_instantiation(typeName):  # Template instantiation
                retStr = self.correctTemplate(typeName, namespace, qual, filename, declDict)
    
                return retStr

            pygccType = self.stripQuals(pygccType)

            # Only use searchNameSpace if we do not have a local dictionary to search
            if declDict:
                # Walk through the hierarchy
                # Only one global search if we search backwards
                retType = None
                hierarchy = pygccType.lstrip(":").split("::")
                try:
                    # Search multiple objects as there may be multiple types
                    # with the same name in different scopes
                    for d in declDict[hierarchy[-1]]:
                        if len(hierarchy) == 1 or self.hierarchyMatch(hierarchy[:-1], d):
                            retType = d
                            break
                except KeyError:
                    pass

            else:
                retType = self.searchNamespace(pygccType, filename)
            
            if retType:
                pygccType = retType
                
        else:
            # Note this is a plain C array (not hkArray)
            (pygccType, arraySize) = self.parseArray(pygccType)

            qual = self.memberQuals(isConst = pygccxmlDecls.is_const(pygccType), isReference = pygccxmlDecls.is_reference(pygccType),
                                        isPointer = pygccxmlDecls.is_pointer(pygccType), isPointerPointer = pygccxmlDecls.is_pointer(pygccxmlDecls.remove_pointer(pygccType)), arraySize = arraySize)
        retStr = ""

        # Find the basic type corresponding to the declaration
        baseType = pygccxmlDecls.base_type(pygccType)
        if not baseType:
            raise self.ParseErrorException((filename, "Type conversion failed for %s" % pygccType))
        
        # If it is a fundamental (C++ native) type
        # domToClass expects these to be converted back to hkXXX types
        # (e.g. hkUint32 instead of unsigned int)
        if pygccxmlDecls.is_fundamental(baseType):
            try:
                for hkBase, hkType in self.hkTypeNames:
                    if hasattr(baseType, 'type'):
                        if baseType.type.decl_string == hkType:
                            baseType = hkBase
                            raise self.FinishedException
                    if baseType.decl_string == hkType:
                        baseType = hkBase
                        raise self.FinishedException
            except self.FinishedException:
                pass

            try:
                baseType = baseType.declaration
            except:
                pass

            if hasattr(baseType, "decl_string"):
                baseType = baseType.decl_string
        
            retStr = qual.qualify(self.stripGlobal(baseType))

            return retStr
        elif pygccxmlDecls.templates.is_instantiation(typeName):  # Template instantiation
            retStr = self.correctTemplate(typeName, namespace, qual, filename, declDict)

            return retStr
        
        elif pygccxmlDecls.is_class(baseType) or pygccxmlDecls.is_class_declaration(baseType):
            className = baseType.decl_string

            # Use a global name if the class is not in the current namespace

            if className == namespace:
                className = "::" + namespace;
            else:
                if not className.startswith("::"):
                    className = "::" + className

            # If the base type is a struct, call it struct
            # If it's anything else, it's a class

            try:
                if baseType.class_type == 'struct':
                    retStr = qual.qualify("struct %s" % className)
                    return retStr
            except AttributeError:
                pass

            try:
                if baseType._declaration.class_type == 'struct':
                    retStr = qual.qualify("struct %s" % className)
                    return retStr
            except AttributeError:
                pass

            retStr = qual.qualify("class %s" % className)

            return retStr
        elif pygccxmlDecls.is_enum(baseType):
            className = self.standardForm(baseType.decl_string, namespace)
            if className.find(namespace) > -1:
                className = className.replace(namespace,"")

            retStr = qual.qualify("enum %s" % className)
#            self.debug("-> %s" % retStr)

            return retStr
        else:
            # typeName still has qualifiers
            #self.debug("UNKNOWN TYPE %s" % typeName)
            retStr = self.standardForm(typeName, namespace)

            return retStr

    def typeTracked(self, pygccType, namespace, declDict):
        # Easy cases -- function pointer, const value always no
        if pygccxmlDecls.is_calldef_pointer(pygccType) or pygccxmlDecls.is_const(pygccType):
            return False
        # Simple pointers and references, always yes

        while(pygccxmlDecls.is_array(pygccType)):
#            print "%s is an array" % pygccType
            pygccType = pygccxmlDecls.array_item_type(pygccType)
#            print "Changed to %s" % pygccType

        if pygccxmlDecls.is_pointer(pygccType): # or pygccxmlDecls.is_reference(pygccType):
#            print "--SIMPLE TYPE %s IS TRACKED" % pygccType
            return True
        # Can't be a declaration if it is being instantiated here
        pygccType = pygccxmlDecls.remove_declarated(pygccType)
        # If it is a class (includes struct), we need to look at the members
        # If any member is tracked, the class is tracked. If not, need to
        # look at parent classes (bases) as well
        if  pygccxmlDecls.is_class(pygccType):
#            print "--CLASS %s" % pygccType
            for cMember in list(pygccType.variables(allow_empty = True)):
#                print cMember
                try:
                    if cMember.name == "Members"  or cMember.type_qualifiers.has_static:
                        continue
                except AttributeError:
                    pass
                if self.typeTracked(cMember.type, namespace, declDict):
#                    print "--MEMBER %s IS TRACKED" % cMember
                    return True
#                else:
#                    print "--MEMBER %s not tracked" % cMember
            if pygccType.bases:
#                print "--PARENT %s" % pygccType.bases[0].related_class
                return self.typeTracked(pygccType.bases[0].related_class, namespace, declDict)
        return False

    # Find the first occurence of searchStr in quotedString, excluding
    # any quoted strings inside the string. Return the index of the first
    # match, or -1 if not found
    def findInQuoted(self, quotedString, searchStr):
        quoteMatch = re.search(r'(?<!\\)"(\\"|[^"])*(?<!\\)"', quotedString)
        quoteIndex = quoteMatch.start() if quoteMatch else -1
        firstSearchIndex = quotedString.find(searchStr)
        offsetIndex = 0
        while quoteIndex > 0 and quoteIndex < firstSearchIndex:
            offsetIndex += quoteMatch.end()
            quoteMatch = re.search(r'(?<!\\)"(\\"|[^"])*(?<!\\)"', quotedString[offsetIndex:])
            quoteIndex = quoteMatch.end() if quoteMatch else -1
            firstSearchIndex = quotedString[offsetIndex:].find(searchStr)
        return firstSearchIndex + offsetIndex if firstSearchIndex > -1 else -1
    
    # Scan a line for attribute declarations
    # These are of the form "// +name(val)
    # TODO: Need to keep scanning until next statement
    def getOldstyleAttributes(self, declStr):
        ret = []
        
        alignMatch = self.re_Alignment.search(declStr)

        if alignMatch:
            for name in ['alignmenta', 'alignmentb', 'alignmentc']:
                if alignMatch.group(name):
                    ret.append(("align", alignMatch.group(name)))
            
        startIndex = declStr.find("//")
        declStr = declStr[startIndex+len("//"):]

        plusMatch = self.re_Plus.search(declStr)
        if plusMatch:
            startIndex = plusMatch.start()
        else:
            startIndex = -1
            
        while startIndex > -1:
            declStr = declStr[startIndex+1:]
            nameIndex = declStr.find("(")
            spaceIndex = declStr.find(" ")

            # if an opening '(' is not found or there is a space before it, check for single name attributes
            if nameIndex == -1 or (nameIndex > -1 and spaceIndex < nameIndex):
                if declStr.startswith("nosave"):
                    declStr = declStr[len("nosave"):]
                    startIndex = declStr.find("+")
                    ret.append(("nosaveattr", True))
                    continue
            
            attributeIndex = self.findInQuoted(declStr, ")")
            if nameIndex > -1 and attributeIndex > -1 and attributeIndex > nameIndex:
                ret.append((declStr[:nameIndex],declStr[nameIndex+1:attributeIndex]))
                declStr = declStr[attributeIndex:]
            match = self.re_Plus.search(declStr)
            if match:
                startIndex = match.start()
            else:
                startIndex = -1

        return ret


    # Apply (old or new) attributes to a class or class member
    def applyAttributes(self, obj, attributes):
        for key, val in attributes:
            try:
                # Try and guess the type for basic native types
                # If none of these work, just leave it as a string
                curval = getattr(obj, key)
                if isinstance(curval, types.IntType):
                    try:
                        if(val.lower() == 'true'):
                            val = True
                        elif(val.lower() == 'false'):
                            val = False
                    except:
                        pass
                    else:                       
                        val = int(val)
                elif isinstance(curval, types.FloatType):
                    val = float(val)
                # If it can't be handled numerically it is assumed to be a string
                setattr(obj, key, val)
            except AttributeError:
                # If there is an attribute with that name, set it
                # If not, dump it in the extra attribute list
                obj.attributes[key] = val


    def applyNewAttributeList(self, target, attributes):
        matches = []

        in_string = False
        nested_brackets = 0
        index = 0
        lastChar = " "
        currentName = ""
        currentMatch = ""

        while index < len(attributes):
            if nested_brackets != 0 or attributes[index] != " ":
                currentMatch += attributes[index] # Skip spaces between identifiers
            if in_string:
                if attributes[index] == "\"" and lastChar != "\\":
                    in_string = False
            else:
                if attributes[index] == "(":
                    nested_brackets += 1
                    if nested_brackets == 2:
                        currentName = currentMatch[:-1]
                        currentMatch = ""
                elif attributes[index] == ")":
                    nested_brackets -= 1
                    if nested_brackets == 0:
                        # Emit a value if it matches
                        if currentName.startswith("gccxml("):
                            # gccxml(name(value)) -> (name, value)
                            matches.append((currentName[7:].strip(), currentMatch[:-2].strip()))
                            currentMatch = ""
            lastChar = attributes[index]
            index += 1

        for (key, val) in matches:
            if key == "nosave":
                matches.append(("nosaveattr", True))
#                matches.append(("reflected", False))
                matches.remove((key, val))
                break
#        print matches

        self.applyAttributes(target, matches)
        return matches

    # Scan forward from the current line until we reach a new statement
    # Apply all attributes found to the target object -- including new
    # style ATTRIBUTE definitions

    # line = line number of declaration to start searching for old-style attributes
    #    None -> don't search
    # attributes = new-style attribute string
    #    None -> don't use new attributes
    def searchAttributes(self, target, line = None, attributes = None):
        if attributes:
            attributesMatches = self.applyNewAttributeList(target, attributes)

        originalLine = line
        while(line and line < len(self.fileLines)):
            memberDecl = self.fileLines[line]
            # Special case -- access specs may come in the middle of some attributes (particularly class version)
            # Anything else other than whitespace or comments will terminate the search
            memberDecl = memberDecl.strip()
            if line == originalLine:
                pass
            elif memberDecl == "public:" or memberDecl == "private:" or memberDecl == "protected:":
                pass
            elif memberDecl.strip() == "" or self.re_CommentLine.match(memberDecl):
                pass
            else:
                return

            decls = self.getOldstyleAttributes(memberDecl)

            # If we find a comment that is not an attribute, we're finished
            if self.re_CommentLine.match(memberDecl) and not decls:
                return
            
            self.applyAttributes(target, decls)
            
            line += 1
        


    # Generate the enum list for a class or namespace
    def listEnums(self, classEnums, namespace):
        ret = []
        for cEnum in classEnums:

            # Skip anonymous enum
            if(cEnum.name == ""):
                continue

            enum = hkcToDom.Enum()

            enum.name = cEnum.name
            # This handles namespaces correctly if the processing script
            # is aware that scope may be different to the declaration
            # hierarchy
            enum.scope = self.getScope(cEnum.parent)#namespace

            self.searchAttributes(enum, cEnum.location.line, cEnum.attributes)        
            for name,val in cEnum.values:
                item = hkcToDom.EnumItem()
                item.value = val
                item.name = self.standardForm(name, cEnum.parent.name)
                enum.item.append(item)

            ret.append(enum)
        return ret

    def getScope(self, cClass):
        if cClass.parent and cClass.parent.name != "::":
            return "%s::%s" % (self.getScope(cClass.parent), cClass.name)
        else:
            return "%s" % cClass.name

    def recodeMember(self, cMember, ret, namespace, declDict):
        member = hkcToDom.Member()

        if cMember.parent.name == ret.name and not cMember.name.startswith(ret.memberprefix):
            if ret.reflected:
                raise self.ParseErrorException((cMember.location.file_name, "Member %s does not start with %s" % (cMember.name, ret.memberprefix)))
            else:
                member.has_memberprefix = False
            member.name = cMember.name[:]
        else:                        
            member.name = cMember.name[len(ret.memberprefix):]
        member.type = self.correctType(cMember.type, namespace, declDict)
        member.memory_tracked = self.typeTracked(cMember.type, namespace, declDict)

        # Do SimpleArray etc. here as we can apply attributes to the correct type
        if(len(ret.member) > 0):
            lastMember = ret.member[-1]
            if member.name.lower().startswith("num") and lastMember.name.lower() == member.name.lower()[3:]:
                if(len(ret.member) > 1 and ret.member[-2].name.lower().startswith(lastMember.name.lower()) and ret.member[-2].name.lower().endswith("class")):
                    secondLastMember = ret.member[-2]
                    member.name = lastMember.name
                    member.type = "hkHomogeneousArray"
                    ret.member.remove(lastMember)
                    ret.member.remove(secondLastMember)
                elif lastMember.type.endswith("*"):
                    member.type = "hkSimpleArray< %s >" % lastMember.type.rsplit("*",1)[0].rstrip()
                    member.name = lastMember.name
                    ret.member.remove(lastMember)
            elif member.name.lower().endswith("class") and lastMember.name.lower() == member.name.lower()[:-5] and member.type.lower() == "hkclass *" and lastMember.type.lower() == "void *":
                member.type = "hkVariant"
                member.name = lastMember.name
                ret.member.remove(lastMember)

        # Find any attributes (old or new) corresponding to this member
        self.searchAttributes(member, cMember.location.line - 1, cMember.attributes)

        # Fix up enums -- some non-reflected classes use C++ enums, but their underlying type
        # (and size) is implementation-defined. For now we assume they are 4 bytes in size
        # (MSVC default) however this is not guaranteed
        if member.type.find("hkEnum") == -1 and member.type.find("hkFlags") == -1:
            enumret = re.subn(r"enum\s+([\w\d_:]+)",r"int", member.type)
            member.type = enumret[0]
            if enumret[1] and ret.reflected:
                print "%s(%d) : warning: Converting unsized enum %s, assuming int storage type\n" % (filename, cMember.location.line, member.name)


        # Copied in from old file -- nosave and reflected(false) are treated slightly
        # differently
        if member.attributes.has_key('nosaveattr'):
            member.serialized = False
            t = member.overridetype or member.type
            if t.endswith("*"):
                member.overridetype = "void*"
            else:
                partial = self.re_Override.match(t)
                if partial:
                    stem, c, sz = partial.groups()
                    member.overridetype = stem + ("<void*" if c.strip().endswith("*") else "<void") + (sz or "") + ">"
            del member.attributes['nosaveattr']


        # If we already have an override type, leave it alone
        if member.type.find("<") > -1 and not member.overridetype:
            templateInfo = util.parse_template(member.type)
            if templateInfo[0] not in self.supportedTemplateTypes:
#                        self.debug("Unsupported type %s -> hkUnsupportedContainer (%s)" % (member.type, member.name))
                member.overridetype = None
                if not member.type.startswith("class ") or member.type.startswith("struct "):
                    member.type = "class " + member.type

        member.visibility = cMember.access_type
        member.sourcecode = "%s %s;" % (member.type, cMember.name)
        return member
    
    # Convert from gccxml class to hkcDom class
    # cClass is the gccxml class object
    # namespace is a string representing the current level in the declaration hierarchy
    # filename is the current filename (used for error messages and to speed up searches)
    # Normal behaviour is only classes with DECLARE_REFLECTION(...), i.e. with a staticClass() method are reflected by default
    def recodeClass(self, cClass, namespace, filename, declDict = None):
        # Catch parse errors so that only the current class is aborted
        try:
            # rtti inserts these, we don't need them
            if(cClass.name.find("__vmi_class_type_info") > -1):
                return None

            ret = hkcToDom.Class()

            ret.name = cClass.name
            ret.location = os.path.normpath(cClass.location.file_name), cClass.location.line
            # This handles namespaces correctly if the processing script
            # is aware that scope may be different to the declaration
            # hierarchy
            ret.scope = self.getScope(cClass.parent)#namespace
            ret.abstract = cClass.is_abstract
            ret.fromheader = True

            if(namespace == "::"):
                namespace = "::" + ret.name
            else:
                namespace = namespace + "::" + ret.name

#            self.debug("CLASS %s[%s] -- NAMESPACE %s" % (cClass, cClass.name, namespace))
            
            # By parent it really means base class
            classParents = list(cClass.bases)

            # Constructors and destructors are also methods and are expected to be mentioned
            classMethods = list(cClass.member_functions(allow_empty = True))
            classMethods.extend(list(cClass.constructors(allow_empty = True)))
            classMethods.extend(list(filter(lambda x: x.name == ("~%s" % cClass.name),cClass.get_members())))
            classMethods = filter(lambda x: x and x.parent.name == ret.name, classMethods)
            classMethods.sort(sortByFileLine)

            classMembers = list(cClass.variables(allow_empty = True))
            classMembers.sort(sortByFileLine)

            classBaseMembers = []
            classBase = cClass.bases[0].related_class if cClass.bases else None
            while(classBase):
                classBaseMembers.extend([c for c in classBase.variables(allow_empty = True) if c.parent.name == classBase.name])
                classBase = classBase.bases[0].related_class if classBase.bases else None

            classEnums = list(cClass.enumerations(allow_empty = True))
            classEnums = filter(lambda x: x and x.parent.name == ret.name, classEnums)
            classEnums.sort(sortByFileLine)

            # Leave out anonymous subclasses
            classClasses = [c for c in cClass.classes(allow_empty = True) if c.name and (c.class_type == 'struct' or c.class_type == 'class') and c.parent.name == ret.name]
            classClasses.sort(sortByFileLine)

##            print cClass.name
##            for d in cClass.decls(allow_empty = True):
##                print "  %s/%s" % (d.name, d.parent.name)

##            trackerFound = any([d.parent.name == ret.name for d in cClass.decls("TrackerStruct", allow_empty = True)])
##            ## Need to find class declaration
##            if not trackerFound and cClass.name.find("<") == -1 and (cClass.class_type == 'struct' or cClass.class_type == 'class'):
##                print "\"%s\" %s" % (os.path.abspath(cClass.location.file_name).lower().replace('\\','/'), cClass.location.line)

            
            # The DECLARE_REFLECTION macro has already been expanded but we test for
            # a staticClass() method which it will have created
            if any(map(lambda x: x.name == "staticClass", classMethods)):
                ret.reflected = True

            if ret.reflected and namespace.count("::") > 2:
                raise ValueError, "Classes can not be nested more than two levels deep : %s" % (namespace.lstrip(":"))

            # Apply attributes to the entire class
            self.searchAttributes(ret, cClass.location.line, cClass.attributes)

            # Base classes and interfaces
            # First base class is parent, the rest are interfaces (by convention)
            if(len(classParents)):
                classParents[0] = classParents[0].related_class
                ret.parent = classParents[0].name
                classParents[0] = classParents[0].parent
                while classParents[0] and classParents[0].name != "::":
                    ret.parent = classParents[0].name + "::" + ret.parent
                    classParents[0] = classParents[0].parent
                
            for interf in classParents[1:]:
                # interfaces
                ic = hkcToDom.Interface()
                interf = interf.related_class
                ic.name = interf.name
                interf = interf.parent
                while interf and interf.name != "::":
                    ic.name = interf.name + "::" + ic.name
                    interf = interf.parent
                ret.interface.append(ic)

            # Class methods (member functions) -- these aren't really used by the reflection but are needed
            # to find out if a class is virtual
            for cMethod in classMethods:
                method = hkcToDom.Method()
                method.description = cMethod.decl_string
                method.name = cMethod.name
                method.visibility = cMethod.access_type
                method.static = cMethod.has_static
                method.const = cMethod.has_const
                method.virtual = (cMethod.virtuality == 'virtual') or (cMethod.virtuality == 'pure virtual')
                method.purevirtual = (cMethod.virtuality == 'pure virtual')

                # Constructors don't return anything so can't assume there is a return_type
                if(cMethod.return_type):
                    method.returns = self.correctType(cMethod.return_type, namespace, declDict, filename)

                # Method parameters
                for cParam in cMethod.arguments:
                    param = hkcToDom.Parameter()
                    param.name = cParam.name
                    param.type = self.correctType(cParam.type, namespace, declDict, filename)
                    param.default = cParam.default_value
                    param.description = "%s %s" % (param.name, param.type)
                    if(param.default):
                        param.description += ("= %s" % param.default)
                    method.parameter.append(param)
                    
                # If any methods are virtual, the class is
                if(method.virtual or method.purevirtual):
                    ret.vtable = True
                try:
                    self.debug("METHOD %s %s %s [ %s ]" % (cMethod.access_type , method.returns if method.returns else "", method.name, ", ".join(map(lambda x: x.type + " " + x.name, method.parameter))))
                except:
                    pass

                ret.method.append(method)

            # Class members (variables)
            for cMember in classMembers:
                # Skip generated members and static members
                if cMember.name == "Members" or cMember.parent.name != ret.name or cMember.type_qualifiers.has_static:
                    continue

                member = self.recodeMember(cMember, ret, namespace, declDict)

                self.debug("MEMBER %s %s" % (member.type, member.name))

                ret.member.append(member)
                ret.tracked_member.append(member)

                #print "%s MEMBER %s (%s) %s" % (ret.name, member.name, member.type, "TRACKED" if member.memory_tracked else "")

            for cMember in classBaseMembers:
                # Skip generated members and static members
                if cMember.name == "Members" or cMember.type_qualifiers.has_static:
                    continue

                member = self.recodeMember(cMember, ret, namespace, declDict)

                self.debug("MEMBER %s %s" % (member.type, member.name))

                if member.visibility == 'public' or member.visibility == 'protected':
                    ret.tracked_member.append(member)
                    #print "%s MEMBER %s (%s) %s" % (ret.name, member.name, member.type, "TRACKED" if member.memory_tracked else "")

            # Enum types
            for cEnum in self.listEnums(classEnums, ret.name):
                ret.enum.append(cEnum)

            # Subclasses
            for subClass in classClasses:
                ret._class.append(self.recodeClass(subClass, namespace, filename, declDict))


            # Try to get the memory_declaration right, managed classes expect this

            def is_refobject(top):
                todo = [top]
                done = {}
                is_placement_allocator = False
                is_real_allocator = False

                # check for placement new and a real allocator definition
                for op in top.operators(name="new", allow_empty=True):
                    if not op.is_artificial and op.parent.name == top.name:
                        is_real_allocator = True

                while todo:
                    c = todo.pop()
                    for op in c.operators(name="new", allow_empty=True):
                        if op.attributes and op.attributes.find("gccxml(placement)") != -1 and op.parent.name == c.name:
                            is_placement_allocator = 2
                    if c.name == "hkReferencedObject":
                        return True, is_placement_allocator, is_real_allocator
                    done[c.name] = 1
                    todo += [ c.related_class for c in c.bases if done.get(c.related_class.name)==None ]
                return False, is_placement_allocator, is_real_allocator

            is_ref_object, is_placement_allocator, is_real_allocator = is_refobject(cClass)
            
            if is_placement_allocator == False:
                mem = None
                for op in cClass.member_operators(name="new", allow_empty=True):
                    if op.access_type=="public" and op.parent==cClass and not op.is_artificial:
                        mem = (op.access_type, True, is_ref_object, is_real_allocator)
                        break
                ret.memory_declaration = mem if mem else ("protected", False, is_ref_object, is_real_allocator) # default to a safe value
            else:
                ret.memory_declaration = ("public", is_placement_allocator, is_ref_object, is_real_allocator) # default to a safe value

            return ret
        except self.ParseErrorException, e:
            print "%s: error: class %s: %s\n" % (e.filename, cClass.name, e.msg)
            #return None
            raise
        except AttributeError, e:
            # Most misconfigurations will result in an attribute error
            # as an invalid type or NoneType is accessed
            print "%s: warning: Parse Error, class %s: %s\n" % (filename, cClass.name, e)
            #return None
            raise
        

    # Parse a header file into the hkcDom structure. This contains generic functionality that
    # does not depend on on whether the header file is cached or output directly from gccxml
    def parseFileGeneric(self, filename, ns, globalDicts = None):
        try:
#            print filename
            ret = hkcToDom.Document(os.path.abspath(filename))

            # Some information is encoded in comments -- need to use the original file
            # to extract these as gccxml will strip comments
            txtfile = open(filename)
            if not txtfile:
                raise self.ParseErrorException((filename, "File not found"))
            
            self.fileLines = txtfile.readlines()
            ret.file = hkcToDom.File()
            tkbms_dict = util.extract_tkbms_file(self.fileLines)
            if tkbms_dict:
                for key, value in tkbms_dict.items():
                    setattr(ret.file,key,value)
            else:
                setattr(ret.file, "product", "ALL")
                setattr(ret.file, "platform", "ALL")
                setattr(ret.file, "visibility", "CUSTOMER")

            txtfile.close()

            # Extract global namespace
            self.global_ns = ns
            if not ns:
                raise self.ParseErrorException((filename, "Unable to locate global namespace"))
            
            fileDict = {}
            fileList = []

            fileFilter = os.path.normpath(os.path.abspath(filename)).lower()
            fnameCache = {}

            fileDict = globalDicts[""]
            try:
                fileList = globalDicts[fileFilter]
            except KeyError:
                fileList = []
                    
            sortedClassList = list()

            sortedClassList.extend(filter(lambda x: isinstance(x, pygccxmlDecls.class_t) and isinstance(x.parent, pygccxmlDecls.namespace_t) and x.parent.name == "::" and (x.class_type == 'struct' or x.class_type == 'class'), fileList))
            sortedClassList.sort(sortByFileLine)


            for cClass in filter(None, map(lambda x: self.recodeClass(x, x.parent.name, filename, fileDict), sortedClassList)): 
               ret.file._class.append(cClass)
                    
            
            # File can have enums as well
            ret.file.enum.extend(self.listEnums((filter(lambda x: isinstance(x, pygccxmlDecls.enumeration_t) and isinstance(x.parent, pygccxmlDecls.namespace_t), fileList)), "::"))

##            if len(ret.file._class) == 0 and len(ret.file.enum) == 0:
##                print "%s: warning : Empty reflected file generated\n" % filename

            return ret

        except RuntimeError, e:
            # RuntimeErrors abort the overall process
            sys.stderr.write("PYGCCXML Error %s\n" % e)
            raise


def usage():
    import os
    print 'This file should not be called directly'
    print 'Options:'
    print '  -h --help    print this help text'

def main():
    usage()
    sys.exit(1)

if __name__=="__main__":
    main()


# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
