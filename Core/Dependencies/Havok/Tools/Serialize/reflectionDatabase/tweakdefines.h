// The xml compiler is actually gcc, but we need to use all of the includes / headers of msvc
// This gives gcc syntax while still keeping the msvc options/includes etc.

#ifndef _TWEAKDEFINES_H
#define _TWEAKDEFINES_H

#ifdef _MSC_VER
#undef _MSC_VER
#endif

#ifndef __GNUC__
#define __GNUC__ 4
#endif

#ifndef __GNUC_MINOR__
#define __GNUC_MINOR__ 2
#endif

#ifndef __i386__
#define __i386__
#endif

#define HK_CONFIG_THREAD HK_CONFIG_SINGLE_THREADED

#define HK_CONFIG_SIMD HK_CONFIG_SIMD_DISABLED

// For compatability, eventually this will be removed
#ifndef __GCCXML__
#define __GCCXML__
#endif

#define __HAVOK_PARSER__

#endif // _TWEAKDEFINES_H
