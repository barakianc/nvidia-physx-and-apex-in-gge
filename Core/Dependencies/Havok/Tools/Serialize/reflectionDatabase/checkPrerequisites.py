# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

import os, tempfile, warnings

# Disable Deprecation warnings to allow the use of popen2 without errors in Python 2.6
warnings.simplefilter("ignore", DeprecationWarning)
import popen2

env_but_not_found = """\t* Environment variable GCCXML_ROOT is set to %s, but no gccxml exe was found
\t\t- Expected exe to be at %s based on environment variable GCCXML_ROOT"""
no_env = """\t* Environment variable GCCXML_ROOT is not set
\t\t- Please ensure that GCCXML is installed and that the environment variable GCCXML_ROOT
\t\t  points to GccXmlInstallDir/share/gccxml-0.9
\t\t\t-e.g. GCCXML_ROOT=C:\Program Files\gccxml 0.9\share\gccxml-0.9"""
quotes_in_env_var = """\t* Environment variable GCCXML_ROOT is set to "%s".
\t\t- GCCXML_ROOT cannot contain quote characters, please remove them."""
gccxml_notworking = """\t* GCCXML test failed with output:\n\t\t%s
\t\t- Have you set GCCXML_ROOT to GccXmlInstallDir/share/gccxml-0.9?
\t\t\te.g. GCCXML_ROOT=C:\Program Files\gccxml 0.9\share\gccxml-0.9"""
no_pygcc = """\t* Unable to find PYGCCXML, please install it from http://sourceforge.net/projects/pygccxml/files"""
to_docs = """\nFor more information please consult the Quickstart Guide"""

def runCmd(cmd):
    stdout,stdin = popen2.popen4(cmd)
    result = stdout.read()
    stdout.close()
    stdin.close()
    return result

def find_gccxml():
    # is gccxml already in the path?
    gccxml_exe = 'gccxml'
    exe_extention = {'nt': '.exe',
                     'posix': ''}
    gccxml_exe += exe_extention[os.name]
    for path in os.environ['PATH'].split(os.path.pathsep):
        possible_gccxml_exe = os.path.join(path, gccxml_exe)
        if os.path.exists(possible_gccxml_exe):
            return possible_gccxml_exe

    #check for the GCCXML_ROOT env var
    gccxml_root_env_var = 'GCCXML_ROOT'
    if gccxml_root_env_var in os.environ:
        gccxml_root = os.environ[gccxml_root_env_var]
        if '"' in gccxml_root or "'" in gccxml_root:
            print quotes_in_env_var % gccxml_root
            return False
        gccxml_exe = os.path.normpath(os.path.join(gccxml_root, '..', '..', 'bin', gccxml_exe))
        if os.path.exists(gccxml_exe):
            return gccxml_exe
        else:
            print env_but_not_found % (gccxml_root, gccxml_exe)
    else:
        print no_env

    return False

def check_pygccxml():
    try:
        import pygccxml
        return True
    except ImportError:
        print no_pygcc
        return False

def try_gccxml(gccxml):
    worked = False
    try:
        tempdir = tempfile.mkdtemp(prefix='HavokReflection')
        test_header = os.path.join(tempdir, 'test.h')
        test_xml = os.path.join(tempdir, 'test.xml')
        open(test_header, 'w').write("#include <stdio.h>")
        output = runCmd('"%s" %s -fxml=%s' % (gccxml, test_header, test_xml))
        worked = os.path.exists(test_xml)
        if not worked:
            print gccxml_notworking % '\n\t\t'.join(output.split('\n'))
        os.remove(test_header)
        os.remove(test_xml)
        os.rmdir(tempdir)
    except:
        pass
    return worked

def checkPrerequisites():
    print 'Havok Reflection Prerequisites Checker\n'
    print 'Testing GCCXML'
    gccxml = find_gccxml()
    print "\t- GCCXML found: %s" % gccxml
    gccxml_working = try_gccxml(gccxml)
    print '\t- GCCXML working: ', gccxml_working
    print '\nTesting PyGCCXML'
    pygcc = check_pygccxml()
    print "\t- PyGCCXML working: ", pygcc
    print to_docs

if __name__ == "__main__":
    checkPrerequisites()

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
