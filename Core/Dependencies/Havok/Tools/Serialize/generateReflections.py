# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#! /usr/bin/env python

"""This is the top-level interface to the reflection/serialization system"""

# This should be run on a directory to generate all of the serialized class information
# The file timestamp is used to determine whether or not is is
# necessary to run the output scripts and files are only updated if changed
#
# An internal cache is used (file reflections.db) to store processed class information, commandline
# option --force-output forces a rebuild of this cache and all output files. If not present or
# invalid, this is automatically rebuilt
#
# The option --force-output will not rebuild database files unless they are out of date but it
# will always run the output plugins. These will generally only update an output file if it has
# changed
#
# --force-rebuild will force all files to be regenerated
#

import re
import os

def processDir(arg, options):
    # First search DOWN
    projectList = set(findProjectsDown([arg]))
    if len(projectList) > 0:
        return projectList
    
    # Now search UP (directory or project) -- Need abspath or else it will only search up to the
    # root of the specified directory
    project = findProjectUp(os.path.abspath(arg))
    if project:
        return set([project])

    return set([arg])


def findProjectsDown(where):
    if not isinstance(where, list):
        where = [where]
    for w in where:
        for dirname, subdirs, files in os.walk(w):
            if 'settings.build' in files or 'reflectionSettings.cache' in files:
                yield dirname

reDrive = re.compile(r'\w:[\\/]$')
def findProjectUp(where):
    for tryFile in ['settings.build', 'reflectionSettings.cache']:
        if os.path.isfile( os.path.join(where, tryFile) ):
            return where
    (where, discard) = os.path.split(where)
    return findProjectUp(where) if where and not reDrive.match(where) else None

USAGE="""
generateReflections.py [DIRECTORY = .] generates all of the required reflection files for the specified directory
"""
def main():
    import optparse
    parser = optparse.OptionParser(usage=USAGE)
    parser.add_option("-v", "--verbose", action="store_true", default=False, help="Verbose execution")
    parser.add_option("-q", "--quiet", action="store_true", default=False, help="Quiet execution")
    parser.add_option("-f", "--force-rebuild", action="store_true", default=False, help="Generate fresh database files and write output files even if not changed")
    parser.add_option("-a", "--force-output", action="store_true", default=False, help="Always run output plugins (will not output unless the output file has changed)")
    parser.add_option("-c", "--customer-build", action="store_true", default=False, help="The build is not an internal havok build (autodetected)")
    parser.add_option("-p", "--havok-products", action="store", default="", help="The Havok products being built (e.g. 'PHYSICS+ANIMATION'. For internal use.")
    parser.add_option("-d", "--output-dir", action="store", default=None, help="Override the default directory for output files")
    # For compatability only, these are ignored:
    parser.add_option("-o", "--old-parser", action="store_true", default=False, help="(for compatability only)")
    parser.add_option("", "--compiler-msvc8", action="store_true", default=False, help="(for compatability only)")
    parser.add_option("", "--compiler-msvc9", action="store_true", default=False, help="(for compatability only)")
    parser.add_option("", "--compiler-gcc", action="store_true", default=False, help="(for compatability only)")

    options, args = parser.parse_args()

    projectList = set()
    
    for arg in args:
        if os.path.isdir(arg):
            projectList.update(processDir(arg, options))
        elif os.path.isfile(arg):
            projectList.add(arg)
        else:
            print "'%s' is neither a file nor directory, skipping." % arg
    if len(args) == 0:
        parser.error("No input")

    import reflectionDatabase
    import reflectionDatabase.reflectionDatabase as refDB
    import outputPlugins

    for project_dir in projectList:
        standard_project_dir = refDB.standardFileName(project_dir)
        DB = reflectionDatabase.createDatabase(standard_project_dir, options)
        if DB and (options.force_output or options.force_rebuild or DB.new):
            outputPlugins.processDatabase(DB, standard_project_dir, options)

if __name__ == "__main__":
    main()

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
