

What are these files?
=====================

This directory contains code to parse havok header files and generate
reflection data from them.

You will need python 2.5 or 2.6 to run these scripts. Get it from www.python.org.
See also http://www.python.org/topics/learn/ for an overview.

See the userguide for more details.


Scripts Overview
===============

generateReflections.py
    Recursively search a directory for class metadata to update.
    Use this script if you have changed a class definition.
	This is usually the only script that will have to be run manually.
	Use the --force-output option and specify a directory to force
	regeneration of all reflected files

reflectionDatabase/*
	A database is generated containing a python reflection of the
	reflected class data (hkcToDom classes)
	
outputPlugins/*
	Plugins that are run for the reflection database for every
	project in the build. These generate all of the necessary
	files for the reflection system
	
havokDomClass/*
	This defines the internal layout of the havok dom classes
