# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

import util
import os
import re
import string
import reflectionDatabase
# use calculateOutputFiles from reflectedClasses.py
import reflectedClasses

_tkbms = """// TKBMS v1.0 -----------------------------------------------------
//
// PLATFORM   : %s
// PRODUCT    : %s
// VISIBILITY : %s
//
// ------------------------------------------------------TKBMS v1.0
//HK_REFLECTION_PARSER_EXCLUDE_FILE
"""

_boilerPlateStart = """/* 
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2010 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 * 
 */
 //HK_REFLECTION_PARSER_EXCLUDE_FILE
"""

_boilerPlateEnd = """/*
* Havok SDK - Level 1 file, Client Regenerated
* 
* Confidential Information of Havok.  (C) Copyright 1999-2010
* Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
* Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
* rights, and intellectual property rights in the Havok software remain in
* Havok and/or its suppliers.
* 
* Use of this software for evaluation purposes is subject to and indicates
* acceptance of the End User licence Agreement for this product. A copy of
* the license is included with this software and is also available from salesteam@havok.com.
* 
*/
"""

dontFollow = set([
    "hkInt64", "hkInt32", "hkInt16", "hkInt8", "hkUint64", "hkUint32", "hkUint16",
    "hkUint8", "hkReal", "int", "char", "short", "long", "hk_size_t", "hkTransform",
    "hkVector4", "hkRotation", "hkMatrix3", "hkMatrix4", "hkMatrix6", "hkHalf", "hkVector8",
    "float", "hkBool", "hkUFloat8", "hkUlong", "hkLong", "bool", "hkUchar", "hkObjectIndex", "hkTime"])

# Fix up a filename from an absolute or relative path, to a Source/ relative path
# suitable for inclusion in a source file
def fixFileName(filename):
    filename = reflectionDatabase.reflectionDatabase.standardFileName(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir, os.pardir, "source", filename))).replace("\\", "/")
    filereplace = reflectionDatabase.reflectionDatabase.standardFileName(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir, os.pardir))).replace("\\", "/")

    filename = filename.replace(os.path.commonprefix([filereplace, filename]), "")
    filename = filename.lstrip("/")
    if filename.startswith("source/") or filename.startswith("Source/"):
        filename = filename[7:]
    if filename.startswith("demo/") or filename.startswith("Demo/"):
        filename = filename[5:]
    return filename

# Find the file prefix from a project
def getPrefix(projectDB, project_dir):
    try:
        return projectDB.settings['setPrefix'][0] + os.path.basename(project_dir)
    except (KeyError, TypeError):
        return os.path.basename(project_dir)

# Determine which class members should be tracked
def findTrackedMembers(scope, clsMembers):
    return [m for m in clsMembers if m.name and m.memory_tracked]
    #print "%s :: %s - [%s]" % (scope, m.name, m.memory_tracked)


def cpp_scope(scope, name = None):
    names = []
    for cls in scope:
        names.append(cls.name)
    if name:
        names += [name]
    return "::".join(names)

def isTypeClassEnum(scope, type):
    for cls in scope:
        for enum in cls.enum:
            if enum.name == type:
                return True

def trackerFileName(projectDB, project_dir, options, extra = ""):
    prefix = getPrefix(projectDB, project_dir)

    if options.output_dir:
        dirname = options.output_dir
    else:
        dirname = os.path.join(project_dir, "Classes")

    if not os.path.exists(dirname):
        os.mkdir(dirname)

    return os.path.join(dirname, prefix + extra + "TrackerClasses.cpp")

def allowPlatform(platformString):
    platforms = set(platformString.split(' '))

    if 'NONE' in platforms:
        return False

    if 'ALL' in platforms:
        return True

    if 'SPU' in platforms:
        return False

    return False

def allowProduct(product):
    return product != 'NONE'

def allowFilename(filename):
    return not re.search(r'\bunittest\b', filename, re.I)

def isSimpleTrackable(c):
    if c.vtable!=0:
        return False
#    return not any([m.memory_tracked for m in c.member])
    if c.name.find("<") != -1:
        return False
    if c.parent and c.parent.find("<") != -1:
        return False
    classAttrs = c.attributes.get("hk.MemoryTracker", "")
    if re.search(r"ignore\s*=\s*true", classAttrs, re.I):
        return False
    return len(c._class)==0 and len(findTrackedMembers("", c.member))==0

def isSimpleChildTrackable(c):
#    return not any([m.memory_tracked for m in c.member])
    if c.name.find("<") != -1:
        return False
    if c.parent and c.parent.find("<") != -1:
        return False
    classAttrs = c.attributes.get("hk.MemoryTracker", "")
    if re.search(r"ignore\s*=\s*true", classAttrs, re.I):
        return False
    return True

def createTypeString(typeName, scope):
    # There is probably a better way to do this
    typeName = typeName.replace("class ::", "")
    typeName = typeName.replace("struct ::", "")
    typeName = typeName.replace("enum ::", "")

    typeName = typeName.replace("const ", "")

    typeName = typeName.replace("class ", "")
    typeName = typeName.replace("struct ", "")
    typeName = typeName.replace("enum ", "")
    
    typeName = re.sub(r"<\s*([^<])", r"<\1", typeName)
    typeName = re.sub(r"([^>])\s*>", r"\1>", typeName)
    typeName = re.sub(r"\s+", r" ", typeName)
    typeName = typeName.lstrip().rstrip()
    return "\"%s\"" % typeName   

def generateSimple(c, scope, scopedName):
    if c.abstract:
        ret = "HK_TRACKER_CLASS_MEMBERS_BEGIN(%s)\n" % scopedName
        ret += "HK_TRACKER_CLASS_MEMBERS_END()\n"
        if c.parent:
            ret += "HK_TRACKER_IMPLEMENT_CLASS_ABSTRACT(%s, s_libraryName, %s)\n\n" % (scopedName, c.parent)
        else:
            ret += "HK_TRACKER_IMPLEMENT_CLASS_ABSTRACT_BASE(%s, s_libraryName)\n\n" % scopedName
        return ret
    else:                        
        if scopedName != cpp_scope(scope, c.name):
            return "HK_TRACKER_IMPLEMENT_NAMESPACE_SIMPLE(%s, s_libraryName, %s)\n\n" % (scopedName, scopedName.replace("::", "_"))
        else:
            if c.scope and c.scope != "::":
                return "HK_TRACKER_IMPLEMENT_CHILD_SIMPLE(%s, %s, s_libraryName)\n\n" % (c.scope, c.name)
            else:
                return "HK_TRACKER_IMPLEMENT_SIMPLE(%s, s_libraryName)\n\n" % scopedName

def generateScan(c, scope, scopedName):
    ret = "\nHK_TRACKER_DECLARE_CLASS_BEGIN(%s)\n" % scopedName
    ret += "HK_TRACKER_DECLARE_CLASS_END\n"
    ret += "HK_TRACKER_IMPLEMENT_SCAN_CLASS(%s, s_libraryName)\n\n" % scopedName
    return ret

def generateClassChildren(c, scope, scopedName):
    ret = "\nHK_TRACKER_DECLARE_CLASS_BEGIN(%s)\n" % scopedName
    for childClass in [childClass for childClass in c._class if childClass.name and isSimpleChildTrackable(childClass)]:
        ret += "    HK_TRACKER_DECLARE_CHILD_SIMPLE(%s)\n" % childClass.name
    for childEnum in c.enum:
        ret += "    HK_TRACKER_DECLARE_CHILD_SIMPLE(%s)\n" % childEnum.name
    ret += "HK_TRACKER_DECLARE_CLASS_END\n\n"
    return ret

def generateWithMembers(c, scope, scopedName, members):
    ret = "HK_TRACKER_CLASS_MEMBERS_BEGIN(%s)\n" % scopedName
    for m in members:
        memberFlags = "0"
        memberAttrs = m.attributes.get("hk.MemoryTracker", "")

        if re.match("\s*backPointer\s*=\s*true\s*", memberAttrs, re.I):
            memberFlags += "|hkMemoryTracker::Member::FLAG_BACK_POINTER"

        ret += "    HK_TRACKER_MEMBER(%s, %s%s, %s, %s) // %s\n" % (scopedName, c.memberprefix if m.has_memberprefix else "", m.name, memberFlags, createTypeString(m.type, generateScopedName(c)), m.type) # TODO: flags
    ret += "HK_TRACKER_CLASS_MEMBERS_END()\n"
    if c.parent:
        ret += "HK_TRACKER_IMPLEMENT_CLASS%s(%s, s_libraryName, %s)\n\n" % ("_ABSTRACT" if c.abstract else "", scopedName, c.parent)
    else:
        ret += "HK_TRACKER_IMPLEMENT_CLASS%s_BASE(%s, s_libraryName)\n\n" % ("_ABSTRACT" if c.abstract else "", scopedName)
    return ret

def printClass(scope, c):

    # early outs
    if c.name.find("<") != -1:
        return " // Skipping Class %s as it is a template\n\n" % c.name
    elif c.parent and c.parent.find("<") != -1:
        return "// Skipping Class %s as it is derived from a template\n\n" % c.name

    # Find any hk.MemoryTracker attributes    
    classAttrs = c.attributes.get("hk.MemoryTracker", "")
    
    if re.search(r"ignore\s*=\s*true", classAttrs, re.I):
        return "// hk.MemoryTracker ignore %s\n" % c.name
    scanThis = re.search(r"\sscan\s*=\s*true\s", classAttrs, re.I)
    if c.name == "hkpAgentNnSector" or c.name == "hkpAgent1nSector":
        scanThis = True

    ret = "\n"
    ret += "// %s %s\n" % (c.name, c.scope)
    fully_qualified_name = generateScopedName(c)

    # Find locations that should have memory declarations but don't
    allocator_declared = c.memory_declaration[1]
    # If we have a new allocator declaration that checks for
    # a proper allocator, use it
    if len(c.memory_declaration)>3:
        allocator_declared = c.memory_declaration[3]
        
    if c.memory_declaration[1]==2:
        return "// Skipping Class %s as it is has a placement allocator\n" % c.name
    elif allocator_declared==False and not isSimpleTrackable(c):
        print "%s(%s) : warning : Class %s is not a simple type but it does not have an allocator declared. Add HK_DECLARE_CLASS_ALLOCATOR(%s), HK_DECLARE_NONVIRTUAL_CLASS_ALLOCATOR or specify another allocator" % (c.location[0], c.location[1], fully_qualified_name, fully_qualified_name)
        return "// Skipping Class %s as it does not have an allocator declared\n" % fully_qualified_name
        
    scopedName = generateScopedName(c)

    if scanThis:
        ret += generateScan(c, scope, scopedName)
    elif isSimpleTrackable(c):
        ret += generateSimple(c, scope, scopedName)
    else:
        # If the class has children
        ret += generateClassChildren(c, scope, scopedName)

        members = findTrackedMembers(scope, c.member)

        if members or c.parent or c.vtable != 0:
            ret += generateWithMembers(c, scope, scopedName, members)
        else:
            ret += generateSimple(c, scope, scopedName)

        # Do simple types
        for childEnum in c.enum:
            ret += "HK_TRACKER_IMPLEMENT_CHILD_SIMPLE(%s, %s, s_libraryName)\n\n" % (childEnum.scope, childEnum.name)

        # Child classes
        for n in [subClass for subClass in c._class if subClass.name and subClass.name.find("<") == -1]:
            ret += printClass(scope + [c], n)

    return ret


def generateScopedName(cClass):
    if cClass.scope and cClass.scope != "::":
        return "%s::%s" % (cClass.scope, cClass.name)
    return cClass.name


def generateTracker(dom):
    ret = ""

    for c in dom.file._class:
        ret += printClass([], c)

    for e in dom.file.enum:
        ret += "// %s %s\n" % (e.scope, e.name)
        if e.scope and e.scope != "::":
            ret += "HK_TRACKER_IMPLEMENT_NAMESPACE_SIMPLE(%s, s_libraryName, %s)\n" % (generateScopedName(e), generateScopedName(e).replace("::", "_"))
        else:
            ret += "HK_TRACKER_IMPLEMENT_SIMPLE(%s, s_libraryName)\n" % e.name


    return ret

def fileListPlugin(DBObject, project_dir, options):
    if re.search(r'\b(source|demos)\b', project_dir.lower(), re.I):
        outputFilesDict = reflectedClasses.calculateOutputFiles(DBObject, options)
        return [trackerFileName(DBObject, project_dir, options, outputFile) for outputFile in outputFilesDict.keys()]
    return []

def findVisibility(docs):
    visList = [d.file.visibility for d in docs]
    if "INTERNAL" in visList:
        return "INTERNAL"
    if "CLIENT" in visList:
        return "CLIENT"
    if "CUSTOMER" in visList:
        return "CUSTOMER"
    return "PUBLIC"

def findPlatform(docs):
    # All the classes to be stored in a single file MUST have the same platform combo.
    # Assert if they don't.
    errorStr = """ERROR: Trying to put files with a mix of TKBMS platforms into a single file.
                   Force them to go in different files by adding //+hk.ReflectedFile("<location>") to the class declarations.
                   Platforms are [%s]"""
    platformsInDocs = []
    for d in docs:
        # Ignore !Platform for now
        platList = [p for p in d.file.platform.upper().split(' ') if p not in ['SPU', 'SIMSPU', 'LRB'] and p.find("!") == -1]
        normalisedPlatform = string.join( sorted(platList), ' ' )
        if normalisedPlatform not in platformsInDocs:
            platformsInDocs.append( normalisedPlatform )
    assert len(platformsInDocs) == 1, errorStr % string.join( platformsInDocs, '], [' )
    return platformsInDocs[0]

def findProductsInDocs(docs):
    productsInDocs = []
    # Imported templates should not be considered for generating the product list
    for d in [doc for doc in docs if ((not doc.file._class) or (not all([c.override_name for c in doc.file._class])))]:
        normalisedProduct = string.join( sorted(d.file.product.split('+')), '+' ).upper()
        if normalisedProduct not in productsInDocs:
            productsInDocs.append( normalisedProduct )
    return productsInDocs

def genOutputText(fileContentsDict, options, prefix):

    outputText = ""
    if not options.customer_build:

        allDocs = []
        map( lambda x: allDocs.extend(x), fileContentsDict.values() )

        # If the classes to be stored in the file all have the same product combo use that, otherwise use 'ALL'.
        product = 'ALL'
        allProductCombos = findProductsInDocs(allDocs)
        if len(allProductCombos) == 1:
            product = allProductCombos[0]

        # Write out the TKBMS header.
        vis = findVisibility(allDocs)
        if vis == "CUSTOMER":
            outputText += _boilerPlateStart
        else:
            outputText += _tkbms % (findPlatform(allDocs), product, vis)


    wrapClassesInHavokProductDefines = (not options.customer_build) and len(fileContentsDict.keys()) > 1
    if not options.customer_build:
        outputText += "\n#include <%s>\n" % (allDocs[0].pchfile if allDocs[0].pchfile else "Common/Base/hkBase.h")
        outputText += """static const char s_libraryName[] = "%s";\n""" % (prefix)
        if wrapClassesInHavokProductDefines:
            outputText += "#include <Common/Base/KeyCode.h>\n"

    # Add the class info, wrapping it in #defines where necessary.
    for havokProductGroup in sorted(fileContentsDict.keys()):

        if wrapClassesInHavokProductDefines and havokProductGroup != '_NONE':
            outputText += '\n\n#if defined(HK_FEATURE_PRODUCT_%s)\n' % string.join(havokProductGroup.split('+'), ') && defined(HK_FEATURE_PRODUCT_')

        def getFileName(d):
            return d.origfilename

        docsList = sorted([doc for doc in fileContentsDict[havokProductGroup] if allowPlatform(doc.file.platform) and allowProduct(doc.file.product) and allowFilename(doc.origfilename)], key=getFileName)

        if docsList:
            usePrefix = prefix
            if len(fileContentsDict.keys()) > 1 and havokProductGroup != '_NONE':
                usePrefix += "".join(havokProductGroup.split("+"))
            outputText += """#include <Common/Base/Memory/Tracker/hkTrackerClassDefinition.h>

void HK_CALL %sRegister() {}

""" % (usePrefix)

            for doc in docsList:

                fileText = generateTracker(doc)
                if fileText:
                    outputText += "#include <%s>\n\n" % fixFileName(doc.origfilename)
                    outputText += fileText

        if wrapClassesInHavokProductDefines and havokProductGroup != '_NONE':
            outputText += '\n#endif // HK_FEATURE_PRODUCT_%s\n' % string.join(havokProductGroup.split('+'), ' && HK_FEATURE_PRODUCT_')

    if not options.customer_build:
        vis = findVisibility(allDocs)
        if vis == "CUSTOMER":
            outputText += _boilerPlateEnd
    
    return outputText

# Main interface
def outputPlugin(DBObject, project_dir, options):
    # This should only run on projects within the "Source" and "Demo" directories

    if not re.search(r'\b(source|demos)\b', project_dir.lower(), re.I):
        return
    if re.search(r'\bcontenttools\b', project_dir.lower()):
        return

    outputFilesDict = reflectedClasses.calculateOutputFiles(DBObject, options)
    for(outputFile, fileContentsDict) in outputFilesDict.items():
        filename = trackerFileName(DBObject, project_dir, options, outputFile)
        prefix = getPrefix(DBObject, project_dir)
        if len(outputFilesDict.keys()) > 1:
            prefix += outputFile
        util.writeIfDifferent(genOutputText(fileContentsDict, options, prefix), filename, options.force_rebuild, options.verbose)

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
