# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

# Plugin to generate class reflection files. This should not be called directly. 
#
# A ReflectedClasses.cpp file is generated for every project containing reflected
# files. This contains the hkClass files corresponding to that project. If any unit tests
# contain serialized classes, these are placed in a seperate ReflectedClassesUnitTests.cpp
# to allow them to be stripped correctly.
#
# The output file is only written if it has changed, to reduce compilation

from havokDomClass import domToClass
import re
import util
import os
import string

_tkbms = """// TKBMS v1.0 -----------------------------------------------------
//
// PLATFORM   : %s
// PRODUCT    : %s
// VISIBILITY : %s
//
// ------------------------------------------------------TKBMS v1.0
//HK_REFLECTION_PARSER_EXCLUDE_FILE
"""

_boilerPlateStart = """/* 
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2010 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 * 
 */
 //HK_REFLECTION_PARSER_EXCLUDE_FILE
"""

_boilerPlateEnd = """/*
* Havok SDK - Level 1 file, Client Regenerated
* 
* Confidential Information of Havok.  (C) Copyright 1999-2010
* Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
* Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
* rights, and intellectual property rights in the Havok software remain in
* Havok and/or its suppliers.
* 
* Use of this software for evaluation purposes is subject to and indicates
* acceptance of the End User licence Agreement for this product. A copy of
* the license is included with this software and is also available from salesteam@havok.com.
* 
*/
"""
# Generate the filename for a set of reflected class definitions
# extra is an extra string to insert, used to distinguish between different
# visibilities etc.
def reflectedClassFileName(projectDB, project_dir, options, extra):
    prefix = os.path.basename(project_dir)

    try:
        prefix = projectDB.settings['setPrefix'][0] + prefix
    except (KeyError, TypeError):
        pass

    if options.output_dir:
        dirname = options.output_dir
    else:
        dirname = os.path.join(project_dir, "Classes")

    if not os.path.exists(dirname):
        os.mkdir(dirname)

    return os.path.join(dirname, prefix + extra + "Reflections.cpp")

def findClasses(classList):
    ret = classList[:]
    for c in [c for c in classList if c._class]:
        ret.extend(findClasses(c._class))
    return ret

def genOutputText(fileContentsDict, options):

    outputText = ''

    # Figure out what header to use
    if not options.customer_build:

        allDocs = []
        map( lambda x: allDocs.extend(x), fileContentsDict.values() )

        # If the classes to be stored in the file all have the same product combo use that, otherwise use 'ALL'.
        product = 'ALL'
        allProductCombos = findProductsInDocs(allDocs)
        if len(allProductCombos) == 1:
            product = allProductCombos[0]

        # Write out the header.
        vis = findVisibility(allDocs)
        if vis == "CUSTOMER":
            outputText += _boilerPlateStart
        else:
            outputText += _tkbms % (findPlatform(allDocs), product, vis)
            

    # Include the precompiled header and, if required, KeyCode.h.
    wrapClassesInHavokProductDefines = (not options.customer_build) and len(fileContentsDict.keys()) > 1
    if not options.customer_build:
        outputText += "\n#include <%s>\n" % (allDocs[0].pchfile if allDocs[0].pchfile else "Common/Base/hkBase.h")
        if wrapClassesInHavokProductDefines:
            outputText += "#include <Common/Base/KeyCode.h>\n"

    # Add the class info, wrapping it in #defines where necessary.
    for havokProductGroup in sorted(fileContentsDict.keys()):

        if wrapClassesInHavokProductDefines and havokProductGroup != '_NONE':
            outputText += '\n\n#if defined(HK_FEATURE_PRODUCT_%s)\n' % string.join(havokProductGroup.split('+'), ') && defined(HK_FEATURE_PRODUCT_')

        def getFileName(d):
            return d.origfilename
        
        docsList = sorted(fileContentsDict[havokProductGroup], key=getFileName)
        generated = "\n\n".join( domToClass.domToClass(d) for d in docsList )
        headers = [] # hack to put all #includes at top - TODO handle multi-product combinations
        generated = re.sub("#include[^\n]+", lambda x : headers.append(x.group(0)), generated)
        outputText += "\n".join(headers) + generated

        if wrapClassesInHavokProductDefines and havokProductGroup != '_NONE':
            outputText += '\n#endif // HK_FEATURE_PRODUCT_%s\n' % string.join(havokProductGroup.split('+'), ' && HK_FEATURE_PRODUCT_')

    if not options.customer_build:
        vis = findVisibility(allDocs)
        if vis == "CUSTOMER":
            outputText += _boilerPlateEnd

    return outputText

def fileListPlugin(DBObject, project_dir, options):
    outputFilesDict = calculateOutputFiles(DBObject, options, reflectionsOnly = True)
    return [reflectedClassFileName(DBObject, project_dir, options, outputFile) for outputFile in outputFilesDict.keys()]

def findProductsInDocs(docs):
    productsInDocs = []
    # Imported templates should not be considered for generating the product list
    for d in [doc for doc in docs if (doc.file._class and (not all([c.override_name for c in doc.file._class])))]:
        normalisedProduct = string.join( sorted(d.file.product.split('+')), '+' ).upper()
        if normalisedProduct not in productsInDocs:
            productsInDocs.append( normalisedProduct )
    return productsInDocs

def findVisibility(docs):
    visList = [d.file.visibility for d in docs]
    if "INTERNAL" in visList:
        return "INTERNAL"
    if "CLIENT" in visList:
        return "CLIENT"
    if "CUSTOMER" in visList:
        return "CUSTOMER"
    return "PUBLIC"

def findPlatform(docs):
    # All the classes to be stored in a single file MUST have the same platform combo.
    # Assert if they don't.
    errorStr = """ERROR: Trying to put files with a mix of TKBMS platforms into a single file.
                   Force them to go in different files by adding //+hk.ReflectedFile("<location>") to the class declarations.
                   Platforms are [%s]"""
    platformsInDocs = []
    for d in docs:
        platList = [p for p in d.file.platform.upper().split(' ') if p not in ['SPU', 'SIMSPU', 'LRB']]
        normalisedPlatform = string.join( sorted(platList), ' ' )
        if normalisedPlatform not in platformsInDocs:
            platformsInDocs.append( normalisedPlatform )
    assert len(platformsInDocs) == 1, errorStr % string.join( platformsInDocs, '], [' )
    return platformsInDocs[0]

def filterFile(doc, reflectionsOnly):
    if doc.file.platform == 'NONE' or doc.file.product == 'NONE':
        return None
    
    if reflectionsOnly:
        fileText = open(doc.origfilename).read()
        if not util.hasReflectionDeclaration(fileText):
            return None
    
    return True

def calculateOutputFiles(DBObject, options, reflectionsOnly = False):
    ret = {}

    # Record which locations contain public files and which contain client.
    locationsContainingPublicFiles = []
    locationsContainingClientFiles = []

    foundHavokFile = False

    for doc in [d for d in DBObject.getDocuments() if filterFile(d, reflectionsOnly)]:
        # Either every file or no file should have TKBMS headers
        if doc.file.visibility != "CUSTOMER":
            foundHavokFile = True
        else:
            assert not foundHavokFile, "Project contains files with TKBMS headers, however %s does not" % (doc.origfilename)

        # Look for //+hk.ReflectedFile("<location>") in the file.
        moveList = [c.attributes.get("hk.ReflectedFile", None) for c in findClasses(doc.file._class)]
        moveSet = set([a for a in moveList if a])
        location = moveSet.pop() if len(moveSet) else ""
        assert len(moveSet) == 0, "ERROR: Found multiple hk.ReflectedFile attributes in %s." % doc.origfilename

        havokProductGroup = '_NONE' # The initial '_' is to put this group at the start when sorting later.

        # Trim quotes.
        if location.startswith('"') and location.endswith('"'):
            location = location[1:-1]

        fileIsArtificial = doc.file._class and all([c.override_name for c in doc.file._class])

        # Do some extra automated processing for Havok classes (only if hk.ReflectedFile hasn't been used to specify where the file belongs).
        if not location and not options.customer_build:

            # Classify the doc according to its Havok product.
            # If imported file is parsed first, productgroup stays at _NONE => assert
            if doc.file.product.find("ALL") == -1 and not fileIsArtificial:
                havokProductGroup = "+".join(sorted(doc.file.product.split("+")))

            # Serialize unit test classes separately.
            if re.search(r'\bUnitTest\b', doc.origfilename, re.I):
                location += "UnitTest"

            # Create a separate file for Internal classes (for packaging reasons).
            if doc.file.visibility == "INTERNAL":
                location += "Internal"
            if doc.file.visibility == "CLIENT":
                location += "Client"

        # Note what locations found public and client files are in.
        if doc.file.visibility == "PUBLIC" or doc.file.visibility == "CUSTOMER" and location not in locationsContainingPublicFiles:
            locationsContainingPublicFiles.append(location)
        elif doc.file.visibility == "CLIENT" and location not in locationsContainingClientFiles:
            locationsContainingClientFiles.append(location)

        if fileIsArtificial:
            havokProductGroup = "_ARTIFICIAL"

        # Build up the dictionary to return.
        if location not in ret:
            ret[location] = {}
        if havokProductGroup not in ret[location]:
            ret[location][havokProductGroup] = [doc]
        else:
            ret[location][havokProductGroup].append(doc)

    # Assert if the generated file will contain a mix of PUBLIC and CLIENT files.
    publicClientIntersection = [x for x in locationsContainingPublicFiles if x in locationsContainingClientFiles]
    if publicClientIntersection:
        print publicClientIntersection
        assert False, "ERROR: File contents for 'location' %s contain classes that are a mix of PUBLIC and CLIENT. This isn't allowed for packaging reasons." % publicClientIntersection

    # Do some post-processing to put generated template classes in the right place
    for loc in ret.keys():
        docList = ret[loc].get("_ARTIFICIAL", [])
        if docList:
            try:
                nonArtificialKey = [d for d in ret[loc].keys() if d != "_ARTIFICIAL"][0]
            except IndexError:
                # This will never happen for any project that is outputting files
                # but we need to handle it anyway
                nonArtificialKey = "_NONE"
                ret[loc][nonArtificialKey] = []
                
            ret[loc][nonArtificialKey].extend(docList)
            del ret[loc]["_ARTIFICIAL"]
    return ret

def outputPlugin(DBObject, project_dir, options):
    outputFilesDict = calculateOutputFiles(DBObject, options, reflectionsOnly = True)
    for (outputFile, fileContentsDict) in outputFilesDict.items():
        filename = reflectedClassFileName(DBObject, project_dir, options, outputFile)
        util.writeIfDifferent(genOutputText(fileContentsDict, options), filename, options.force_rebuild, options.verbose)

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
