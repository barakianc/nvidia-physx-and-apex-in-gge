# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

#
# Plugin to clean the Classes/ directory of a project. This means that
# a complete clean and rebuild is not needed when upgrading
#
# There are three steps:
#
# 1 - Any *Class.cpp file with a corresponding header file with a reflection definition is moved
# 2 - A list of files that should be in the "Classes/" directory is made
# 3 - Any file not on that list is moved
#     
import re
import os
import string
import reflectedClasses
import outputPlugins

def cleanDir(project_dir, allowedFiles, options):

    # Windows file system is not case-sensitive so we must assume files are not
    allowedFiles = [os.path.abspath(f).lower().replace("\\", "/") for f in allowedFiles]

    if options.output_dir:
        dirname = options.output_dir
    else:
        dirname = os.path.join(project_dir, "Classes")

    try:
        for filename in os.listdir(dirname):
            if filename.startswith("."):
                continue # skip hidden or special files
            oldFileName = os.path.abspath(os.path.join(dirname, filename))
            oldFileNameString = oldFileName.lower().replace("\\", "/")
            # If the filename ends in .\d+, it's already been renamed
            if not re.search(r"\.\d+$", oldFileNameString) and not oldFileNameString in allowedFiles:
                for i in range(0,100):
                        try:
                            backup = "%s.%i" % (oldFileName,i)
                            open( backup )
                        except IOError:
                            print "Renaming old reflection file %s -> %s" % (oldFileName,backup)
                            try:
                                os.rename(oldFileName, backup)
                            except IOError:
                                pass
                            break

    except OSError:
        pass

MAGIC_DESTINATION_MARK = re.compile(r'^\s*HK_REFLECTION_CLASSFILE_HEADER\s*\(\s*"([^"]*)"\s*\)', re.M)
        
def outputPlugin(DBObject, project_dir, options):
    try:
        prefix = DBObject.settings['setPrefix'][0] + os.path.basename(project_dir)
    except (KeyError, TypeError):
        prefix = os.path.basename(project_dir)
    
    # Remove old reflection class files -- they will confuse the build
    for dirname, subdirs, files in os.walk(project_dir):
        for f in [ os.path.join(dirname,f) for f in files if (f.endswith("Class.cpp") and f != "hkClass.cpp") ]:
            if not MAGIC_DESTINATION_MARK.search(open(f).read()):
                for i in range(0,100):
                    try:
                        backup = "%s.%i" % (f,i)
                        open( backup )
                    except IOError:
                        print "Renaming old reflection file %s -> %s" % (f,backup)
                        try:
                            os.rename(f, backup)
                        except IOError:
                            pass
                        break

    allowedFiles = outputPlugins.generatedFileList(DBObject, project_dir, options)

    cleanDir(project_dir, allowedFiles, options)

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
