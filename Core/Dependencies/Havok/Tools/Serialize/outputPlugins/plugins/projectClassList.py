# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

# Plugin to generate the project class list files. This should not be
# called directly.

# Classes/XXXClasses.h and Classes/XXXClasses.cxx are generated.
# The prefix for these files is taken from the reflectionSettings.cache
# file (Havok code) or the directory name (customer code).

import re
import os
import util
import glob
import string

import reflectedClasses
import reflectionDatabase


TKBMS = \
"""// TKBMS v1.0 -----------------------------------------------------
//
// PLATFORM       : ALL
// PRODUCT        : %s
// VISIBILITY : %s
//
// ------------------------------------------------------TKBMS v1.0

// Autogenerated by generateReflections.py
//HK_REFLECTION_PARSER_EXCLUDE_FILE

"""

_boilerPlateStart = """/* 
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2010 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 * 
 */

//HK_REFLECTION_PARSER_EXCLUDE_FILE
// Autogenerated by generateReflections.py
"""

_boilerPlateEnd = """/*
* Havok SDK - Level 1 file, Client Regenerated
* 
* Confidential Information of Havok.  (C) Copyright 1999-2010
* Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
* Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
* rights, and intellectual property rights in the Havok software remain in
* Havok and/or its suppliers.
* 
* Use of this software for evaluation purposes is subject to and indicates
* acceptance of the End User licence Agreement for this product. A copy of
* the license is included with this software and is also available from salesteam@havok.com.
* 
*/
"""


def getClassFiles(projectDB, project_dir, options):
    try:
        prefix = projectDB.settings['setPrefix'][0] + os.path.basename(project_dir)
    except (KeyError, TypeError):
        prefix = os.path.basename(project_dir)

    if options.output_dir:
        directory = options.output_dir
    else:
        directory = os.path.join(project_dir, "Classes")

    if not os.path.exists(directory):
        os.mkdir(directory)

    return (os.path.join(directory, prefix + "Classes.h"), os.path.join(directory, prefix + "Classes.cxx"))

def completeName(c):
    ret = c.name
    try:
        ret = c.scope + ret
    except:
        try:
            ret = c.scope.name + ret
        except:
            pass
    return ret.replace("::", "")

def classDef(c):
    if c.abstract:
        return "HK_ABSTRACT_CLASS(%s)" % completeName(c)
    elif c.vtable:
        return "HK_CLASS(%s)" % completeName(c)
    else:
        return "HK_STRUCT(%s)" % completeName(c)

def allClasses(c):
    if c.reflected:
        retList = [c]
    else:
        retList = []
    for sc in c._class:
        retList.extend(allClasses(sc))
    return retList


def fixFileName(filename):
    filename = reflectionDatabase.reflectionDatabase.standardFileName(filename)
    filereplace = reflectionDatabase.reflectionDatabase.standardFileName(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir, os.pardir))
    filename = filename.replace(os.path.commonprefix([filereplace, filename]), "")
    filename = filename.lstrip("/")
    if filename.startswith("Source/"):
        filename = filename[7:]
    if filename.startswith("Demo/"):
        filename = filename[5:]
    return filename

def projectHasClassList(project_dir):
    # This only applies to Havok projects
    projects = [ string.lower(os.path.abspath(d)) for d in glob.glob(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir, os.pardir, "Source/*/*/reflectionSettings.cache"))]
    return string.lower(os.path.abspath(os.path.join(project_dir, "reflectionSettings.cache"))) in projects

def createClassList(DBObject, fileList = None):
    classList = []

    if fileList:
        completeFileList = fileList
    else:
        completeFileList = DBObject.getDocuments()

    for doc in [doc for doc in completeFileList if not re.search(r'\bUnitTest\b', doc.origfilename, re.I) and doc.file.product != "NONE" and doc.file.platform != "NONE"]:
        if not fileList or doc in fileList:
            thisFileList = []
            for c in doc.file._class:
                thisFileList.extend(allClasses(c))
            classList.extend(thisFileList)

    return classList

def findClasses(classList):
    ret = classList[:]
    for c in [c for c in classList if c._class]:
        ret.extend(findClasses(c._class))
    return ret

def fileListPlugin(DBObject, project_dir, options):
    if not options.customer_build and projectHasClassList(project_dir):
        classList = createClassList(DBObject)
        if classList:
            (headerFile, cxxFile) = getClassFiles(DBObject, project_dir, options)
            return [headerFile, cxxFile]
    return []

def stripQuotes(c):
    if c.startswith('"') and c.endswith('"'):
        return c[1:-1]
    return c

def outputPlugin(DBObject, project_dir, options):
    # These files are only needed by the Havok sdk.
    if options.customer_build or not projectHasClassList(project_dir):
        return

    classList = createClassList(DBObject)
    if classList:
        directory = os.path.abspath(project_dir)

        try:
            prefix = DBObject.settings['setPrefix'][0] + os.path.basename(directory)
        except (KeyError, TypeError):
            prefix = os.path.basename(directory)

        headerText = ""
        cxxText = ""

        outputFilesDict = reflectedClasses.calculateOutputFiles(DBObject, options, reflectionsOnly = True)

        for (outputFile, fileContentsDict) in sorted(outputFilesDict.items()):

            # Need to add support for #ifdef by Havok product if we start generating class lists that contain files belonging to different product sets (e.g. in Demos).
            assert len(fileContentsDict.keys()) == 1, "ERROR: Class lists for files belonging to more than one Havok product combination aren't currently supported."
            curDocsList = fileContentsDict[fileContentsDict.keys()[0]]

            # Re-evaluate the files' location, using only //+hk.ReflectedFile("<location>") this time.
            # If it's not the same as the location provided don't write out any class info for the files as they're in a UnitTest dir, Internal or both.
            origOutputfile = str(outputFile) if str(outputFile) != "Internal" else ""

            moveSet = set()
            for doc in curDocsList:
                for c in findClasses(doc.file._class):
                    if c.attributes.get("hk.ReflectedFile", None):
                        moveSet.add(c.attributes.get("hk.ReflectedFile", None))
            outputFile = stripQuotes(moveSet.pop()) if len(moveSet) else ""
            assert len(moveSet) == 0, "ERROR: Found multiple hk.ReflectedFile attributes in group of files that should share a single one."

            if outputFile == origOutputfile:

                if outputFile:
                    headerText += "#ifndef HK_EXCLUDE_FEATURE_%s\n" % outputFile
                    cxxText += "#ifndef HK_EXCLUDE_FEATURE_%s\n" % outputFile

                curClassesList = createClassList(DBObject, curDocsList)
                headerText += "%s\n" % ("\n".join(sorted([classDef(c) for c in curClassesList])))
                cxxText += "#include <%s>\n" % (fixFileName(reflectedClasses.reflectedClassFileName(DBObject, project_dir, options, outputFile)))

                if outputFile:
                    headerText += "#endif\n"
                    cxxText += "#endif\n"

        allFilesList = []
        for location in outputFilesDict:
            for havokProductGroup in outputFilesDict[location]:
                allFilesList.extend( outputFilesDict[location][havokProductGroup] )

        (headerFile, cxxFile) = getClassFiles(DBObject, project_dir, options)

        productsList = reflectedClasses.findProductsInDocs(allFilesList)
        assert len(productsList) == 1, 'ERROR: Found different product combinations used in serialized classes when generating %s, %s.' % (headerFile, cxxFile)
        if(productsList):
            product = productsList[0]

            docList = []
            for d in outputFilesDict.values():
            	for l in d.values():
            	    docList.extend(l)

            # We now allow serialized internal classes. The classlist should
            # always be PUBLIC as it must be seen to register the classes
            visibility = reflectedClasses.findVisibility(docList)

            if visibility == "CUSTOMER":
                cxxText = _boilerPlateStart + cxxText + _boilerPlateEnd
                headerText = _boilerPlateStart + headerText + _boilerPlateEnd
            else:
                cxxText = (TKBMS % (product, visibility)) + "\n" + cxxText
                headerText = (TKBMS % (product, "PUBLIC")) + headerText

            # Write out the files.
            util.writeIfDifferent(headerText, headerFile, options.force_rebuild)
            util.writeIfDifferent(cxxText, cxxFile, options.force_rebuild)


# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
