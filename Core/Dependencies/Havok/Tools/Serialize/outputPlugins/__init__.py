# 
# Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's 
# prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok. 
# Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2011 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement. 
#

#!/usr/bin/env python

import os
import sys
pluginList = []

def generatedFileList(DBObject, project_dir, options):
    fileList = set()
    for p in pluginList:
        if hasattr(p, "fileListPlugin"):
            fileList.update(p.fileListPlugin(DBObject, project_dir, options))
    return fileList

def processDatabase(DBObject, project_dir, options):
    for p in pluginList:
        if hasattr(p, "outputPlugin"):
            p.outputPlugin(DBObject, project_dir, options)

## Plugins directory is read at startup only
old_sys_path = sys.path[:]
sys.path.append(os.path.join(os.path.dirname(__file__), "plugins"))
for filename in os.listdir(os.path.join(os.path.dirname(__file__), "plugins")):
    if filename.endswith(".py"):
        filename = filename[:-3]
        pluginList.append(__import__(filename))

sys.path = old_sys_path[:]

# 
# Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20110914) 
#  
# Confidential Information of Havok.  (C) Copyright 1999-2011 
# Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok 
# Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership 
# rights, and intellectual property rights in the Havok software remain in 
# Havok and/or its suppliers. 
#  
# Use of this software for evaluation purposes is subject to and indicates 
# acceptance of the End User licence Agreement for this product. A copy of 
# the license is included with this software and is also available at www.havok.com/tryhavok. 
#  
#
