/* 
 * 
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Level 2 and Level 3 source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2010 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 * 
 */
#ifndef HK_MX_VECTOR4_H
#define HK_MX_VECTOR4_H


#define hkMxVector4Parameter const hkMxVector4&
#define hkMxRealParameter const hkMxReal&
#define hkMxSingleParameter const hkMxSingle&
#define hkMxMask4Parameter const hkMxMask4&
#define hkMxQuaternionParameter const hkMxQuaternion&
#define hkMxUNROLL( X ) { {	const int hkMxI = 0; X; } { const int hkMxI = 1; X; } { const int hkMxI = 2; X; } { const int hkMxI = 3; X; } }	

class hkMxVector4;
class hkMxQuaternion;
class hkMxSingle;
class hkMxMask4;

#include <Common/Base/Math/Vector/hkVector4MxVector4.h>

class hkMxVector4Util;
#define hkMxPackedReal hkVector4 

// 4x4 vectors 
class hkMxVector4
{
public:

	HK_DECLARE_NONVIRTUAL_CLASS_ALLOCATOR(HK_MEMORY_CLASS_MATH, hkMxVector4);
//	HK_DECLARE_POD_TYPE();

//#ifndef HK_DISABLE_VECTOR4_CONSTRUCTORS
	/// Empty constructor.
	HK_FORCE_INLINE hkMxVector4() { }
//#endif

	enum Permutation {
		SHIFT_RIGHT_CYCLIC,
		SHIFT_LEFT_CYCLIC,
		REVERSE
	};

	HK_FORCE_INLINE explicit hkMxVector4(hkVector4Parameter v); // replicate abcd abcd abcd abcd
	HK_FORCE_INLINE explicit hkMxVector4(hkReal v);             // broadcast aaaa aaaa aaaa aaaa

	HK_FORCE_INLINE void load(const hkVector4* v);     // read linear as abcd efgh ijkl mnop
	HK_FORCE_INLINE void store(hkVector4* vOut) const; // vOut =  abcd efgh ijkl mnop

	HK_FORCE_INLINE void gather(const hkVector4* base, hkUint32 byteAddressIncrement); // read non-linear as abcd efgh ijkl mnop
	HK_FORCE_INLINE void gather(const hkVector4* base, hkUint32 byteAddressIncrement, const hkUint16* indices);
	HK_FORCE_INLINE void gather(const hkVector4* base, hkUint32 byteAddressIncrement, const hkInt32* indices);

	HK_FORCE_INLINE void scatter(hkVector4* base, hkUint32 byteAddressIncrement) const; // write non-linear as abcd efgh ijkl mnop
	HK_FORCE_INLINE void scatter(hkVector4* base, hkUint32 byteAddressIncrement, const hkUint16* indices) const;
	HK_FORCE_INLINE void scatter(hkVector4* base, hkUint32 byteAddressIncrement, const hkInt32* indices) const;

		/// Transposes the hkMxVector4 and stores it, only works for 4 elements
	HK_FORCE_INLINE void storeTransposed4(hkVector4* matrix4) const;
	

	HK_FORCE_INLINE void set(hkMxVector4Parameter v);
	HK_FORCE_INLINE void setXYZ(hkMxVector4Parameter v);
	HK_FORCE_INLINE void setW(hkMxVector4Parameter v);
	HK_FORCE_INLINE void setW(hkMxSingleParameter s);
	HK_FORCE_INLINE void setXYZW(hkMxVector4Parameter v0, hkMxVector4Parameter v1);

	HK_FORCE_INLINE void setZero();

	HK_FORCE_INLINE void setCross(hkMxVector4Parameter v0, hkMxVector4Parameter v1);
	HK_FORCE_INLINE void setCross(hkMxSingleParameter s, hkMxVector4Parameter v);
	HK_FORCE_INLINE void setCross(hkMxVector4Parameter v, hkMxSingleParameter s);

	HK_FORCE_INLINE void setAdd(hkMxVector4Parameter v0, hkMxVector4Parameter v1);
	HK_FORCE_INLINE void setAddMul(hkMxVector4Parameter v0, hkMxVector4Parameter v1, hkMxVector4Parameter v2);
	HK_FORCE_INLINE void setAddMul(hkMxVector4Parameter v0, hkMxVector4Parameter v1, const hkMxPackedReal& v2);

	HK_FORCE_INLINE void setSub(hkMxVector4Parameter v0, hkMxVector4Parameter v1);
	HK_FORCE_INLINE void setSub(hkMxVector4Parameter v, hkMxSingleParameter s);
	HK_FORCE_INLINE void setSubMul(hkMxVector4Parameter v0, hkMxVector4Parameter v1, hkMxVector4Parameter v2);
	HK_FORCE_INLINE void setSubMul(hkMxVector4Parameter v0, hkMxVector4Parameter v1, hkMxSingleParameter s);

	HK_FORCE_INLINE void setDiv(hkMxVector4Parameter v0, hkMxVector4Parameter v1);
	HK_FORCE_INLINE void setDivFast(hkMxVector4Parameter v0, hkMxVector4Parameter v1);

	HK_FORCE_INLINE void setSqrtInverse( hkMxVector4Parameter m );

	HK_FORCE_INLINE void setMul(hkMxVector4Parameter v0, hkMxVector4Parameter v1);
	HK_FORCE_INLINE void setMul(hkMxVector4Parameter v, hkMxSingleParameter s);

	template <Permutation P> HK_FORCE_INLINE void setPermutation(hkMxVector4Parameter m);
	template <int I> HK_FORCE_INLINE void setBroadcast(hkMxVector4Parameter v); // take the i component from every vector and broadcast within this vector only

	HK_FORCE_INLINE void add(hkMxVector4Parameter m);
	HK_FORCE_INLINE void addMul(hkMxVector4Parameter v0, hkMxVector4Parameter v1);
	HK_FORCE_INLINE void addMul(hkMxVector4Parameter v, hkMxSingleParameter s);

	HK_FORCE_INLINE void sub(hkMxVector4Parameter m);

	HK_FORCE_INLINE void mul(hkMxVector4Parameter m);
	HK_FORCE_INLINE void mul(hkMxSingleParameter s);

	// these are for storage
	template <int I> HK_FORCE_INLINE void getVector(hkVector4& vOut) const; // vOut = [i=0]abcd [i=1]efgh [i=2]ijkl [i=3]mnop
	template <int I> HK_FORCE_INLINE hkVector4 getVector() const;				// return [i=0]abcd [i=1]efgh [i=2]ijkl [i=3]mnop
	template <int I> HK_FORCE_INLINE void setVector(hkVector4Parameter v);

	// this is for calc and should actual become getVector
	template <int I> HK_FORCE_INLINE void getSingle(hkMxSingle& vOut) const; 

	HK_FORCE_INLINE void setVectorSLOW(int i, hkVector4Parameter v); // temp, needs to go
	HK_FORCE_INLINE void getVectorSLOW(int i, hkVector4& vOut) const;

	HK_FORCE_INLINE void compareGreaterThan(hkMxVector4Parameter v, hkMxMask4& mask) const;
	HK_FORCE_INLINE void compareGreaterThan(hkMxSingleParameter s, hkMxMask4& mask) const;
	HK_FORCE_INLINE void compareLessThan(hkMxVector4Parameter v, hkMxMask4& mask) const;
	HK_FORCE_INLINE void compareLessThan(hkMxSingleParameter s, hkMxMask4& mask) const;
	HK_FORCE_INLINE void compareLessThanEqual(hkMxVector4Parameter v, hkMxMask4& mask) const;
	HK_FORCE_INLINE void compareEqual(hkMxVector4Parameter v, hkMxMask4& mask) const;
	HK_FORCE_INLINE void compareEqual(hkMxSingleParameter s, hkMxMask4& mask) const;

	HK_FORCE_INLINE void select(hkMxVector4Parameter v0, hkMxVector4Parameter v1, const hkMxMask4& mask);
	HK_FORCE_INLINE void select(hkMxSingleParameter s, hkMxVector4Parameter v, const hkMxMask4& mask);
	HK_FORCE_INLINE void select(hkMxVector4Parameter v, hkMxSingleParameter s, const hkMxMask4& mask);

	// change to storage pipeline
	HK_FORCE_INLINE void horizontalMin4Packed( hkVector4& minsOut ) const; // minsOut = xyzw with x=min(abcd) y=min(efgh) z=min(ijkl) w=min(mnop)
	HK_FORCE_INLINE void horizontalMax4Packed( hkVector4& maxsOut ) const;
	HK_FORCE_INLINE void horizontalAdd4( hkVector4& addsOut ) const;
	HK_FORCE_INLINE void horizontalAdd3( hkVector4& addsOut ) const;

	// stay in calc pipeline
	HK_FORCE_INLINE void horizontalMin4Packed( hkMxReal& minsOut ) const; // minsOut = xyzw with x=min(abcd) y=min(efgh) z=min(ijkl) w=min(mnop)
	HK_FORCE_INLINE void horizontalMax4Packed( hkMxReal& maxsOut ) const;

	HK_FORCE_INLINE void reduceAdd( hkVector4& addOut ) const;

	HK_FORCE_INLINE void normalize4(); // normalize every component
	HK_FORCE_INLINE void length4( hkMxReal& lensOut ) const;
	HK_FORCE_INLINE void lengthInverse4( hkMxReal& lensOut ) const;
	HK_FORCE_INLINE void dot4(hkMxVector4Parameter v, hkMxReal& lensOut ) const;

	HK_FORCE_INLINE void dot3(hkMxVector4Parameter v, hkMxReal& lensOut ) const;

	HK_FORCE_INLINE void dot3(hkMxVector4Parameter v, hkMxSingle& lensOut ) const;

	HK_FORCE_INLINE void setFastNormalize3NonZero(hkMxVector4Parameter m);


private:
	void operator=( const hkMxVector4& rhs);		// results in bad compiler output, use load instead
	hkMxVector4( const hkMxVector4& other);	// results in bad compiler output, use load instead


protected:

	hkQuadVec m_vec;
	friend class hkMxReal;
	friend class hkMxHalf8;
	friend class hkMxQuaternion;
	friend class hkMxVector4Util;
};


class hkMxVector4Util
{
	public:
		HK_FORCE_INLINE static void HK_CALL transformPosition(hkMxVector4Parameter mat, hkMxVector4Parameter vIn, hkMxVector4& vOut);
		HK_FORCE_INLINE static void HK_CALL transformTransposePosition(hkMxVector4Parameter mat, hkMxVector4Parameter vIn, hkMxVector4& vOut);

		HK_FORCE_INLINE static void HK_CALL rotateDir( hkMxVector4Parameter mat, hkMxVector4Parameter vIn, hkMxVector4& vOut);
		HK_FORCE_INLINE static void HK_CALL rotateInverseDir( hkMxVector4Parameter mat, hkMxVector4Parameter vIn, hkMxVector4& vOut);

		// these two need to go - vector register spill - unroll instead
		HK_FORCE_INLINE static void HK_CALL transformPositions(hkMxVector4Parameter mat0, 
			hkMxVector4Parameter mat1, 
			hkMxVector4Parameter mat2, 
			hkMxVector4Parameter mat3,
			hkMxVector4Parameter vIn,
			hkMxVector4& vOut);

		HK_FORCE_INLINE static void HK_CALL rotateDirs(hkMxVector4Parameter mat0, 
			hkMxVector4Parameter mat1, 
			hkMxVector4Parameter mat2, 
			hkMxVector4Parameter mat3,
			hkMxVector4Parameter vIn,
			hkMxVector4& vOut);

		HK_FORCE_INLINE static void HK_CALL gatherUnpackFirst(const hkHalf8* base, hkUint32 byteAddressIncrement, hkMxVector4& vOut);
		HK_FORCE_INLINE static void HK_CALL gatherUnpackFirst(const hkHalf8* base, hkUint32 byteAddressIncrement, const hkInt32* indices, hkMxVector4& vOut);
		HK_FORCE_INLINE static void HK_CALL gatherUnpackSecond(const hkHalf8* base, hkUint32 byteAddressIncrement, hkMxVector4& vOut);
		HK_FORCE_INLINE static void HK_CALL gatherUnpackSecond(const hkHalf8* base, hkUint32 byteAddressIncrement, const hkInt32* indices, hkMxVector4& vOut);
};


class hkMxQuaternion
{
public:

	HK_DECLARE_NONVIRTUAL_CLASS_ALLOCATOR(HK_MEMORY_CLASS_MATH, hkMxQuaternion);
	//	HK_DECLARE_POD_TYPE();

//#ifndef HK_DISABLE_VECTOR4_CONSTRUCTORS
	/// Empty constructor.
	HK_FORCE_INLINE hkMxQuaternion() { }
//#endif

	HK_FORCE_INLINE operator const hkMxVector4&() const;

	HK_FORCE_INLINE void load(const hkVector4* v);     // read as abcd efgh ijkl mnop
	HK_FORCE_INLINE void store(hkVector4* vOut) const; // vOut =  abcd efgh ijkl mnop

	HK_FORCE_INLINE void gather(const hkQuaternion* base, hkUint32 byteAddressIncrement); // read non-linear as abcd efgh ijkl mnop
	HK_FORCE_INLINE void scatter(hkQuaternion* base, hkUint32 byteAddressIncrement) const; // write non-linear as abcd efgh ijkl mnop

	HK_FORCE_INLINE void setMul(hkMxVector4Parameter p, hkMxVector4Parameter q);
	HK_FORCE_INLINE void setMul(hkMxVector4Parameter p, hkMxSingleParameter s);

	HK_FORCE_INLINE void setMulAsVector(hkMxVector4Parameter p, hkMxSingleParameter s);

	

	HK_FORCE_INLINE void setXYZ(hkMxVector4Parameter v);
	HK_FORCE_INLINE void setW(hkMxVector4Parameter v);
	HK_FORCE_INLINE void setW(hkMxSingleParameter s);
	HK_FORCE_INLINE void setXYZW(hkMxVector4Parameter xyz, hkMxVector4Parameter w);

	template <int I> HK_FORCE_INLINE void setVector(hkVector4Parameter v);  // reinterpret
	template <int I> HK_FORCE_INLINE void getVector(hkVector4& v) const;

	template <int I> HK_FORCE_INLINE void setVector(const hkQuaternion& q);
	template <int I> HK_FORCE_INLINE void getVector(hkQuaternion& q) const;

	HK_FORCE_INLINE void setVectorSLOW(int i, hkVector4Parameter v); // temp, needs to go
	HK_FORCE_INLINE void getVectorSLOW(int i, hkVector4& v) const;

	HK_FORCE_INLINE void normalize();

private:

	hkQuadVec m_quat;
};


#	include <Common/Base/Math/Vector/hkVector4MxVector4.inl>

#endif //HK_MX_VECTOR4_H


/*
* Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20100811)
* 
* Confidential Information of Havok.  (C) Copyright 1999-2010
* Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
* Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
* rights, and intellectual property rights in the Havok software remain in
* Havok and/or its suppliers.
* 
* Use of this software for evaluation purposes is subject to and indicates
* acceptance of the End User licence Agreement for this product. A copy of
* the license is included with this software and is also available at www.havok.com/tryhavok.
* 
*/
