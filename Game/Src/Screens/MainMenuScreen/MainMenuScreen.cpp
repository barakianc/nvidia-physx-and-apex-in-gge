#include "StdAfx.h"
// MainMenuScreen
#include "MainMenuScreen.h"

// Engine
#include "Engine.h"

// Physics
#ifdef HAVOK
#include "HavokWrapper.h"
#endif

namespace GamePipeGame
{
	//GameObjectManager* havokManager;

	//////////////////////////////////////////////////////////////////////////
	// MainMenuScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	MainMenuScreen::MainMenuScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Initialize()
	{
		GetGameObjectManager()->SetVisualDebugger(true);

		SetBackgroundColor(Ogre::ColourValue(0,0,0,0));

		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000f, 0.5000f, 0.5000f, 0.5000f));

		
		SetBackgroundColor(Ogre::ColourValue(0, 0, 0, 0));

		pointLight = pSceneManager->createLight("pointLight");
		pointLight->setType(Ogre::Light::LT_POINT);
		pointLight->setPosition(Ogre::Vector3(0, 0, -250));
		pointLight->setDiffuseColour(1.0, 1.0, 1.0);
		i=0;
		LoadScene("MainMenuScreen.scene");
		m_Text = new SimpleDebugText(18, Ogre::String("Hello"));
		m_Text->setText("Test");
		m_Text->setPosition(10, 100);

		quat_Text = new SimpleDebugText(18, Ogre::String("Hello"));
		quat_Text->setPosition(10, 150);


		//TODO-SV: delete GameObject when an Entity is deleted in GLE cant be done yet
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Destroy()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Update()
	{
		// Rotate Ogre Head
		
		pointLight->setPosition(Ogre::Vector3(0, 0, -250+i));
		i=i+0.1f;
		// Update Camera
		FreeCameraMovement();

			serverData = (GamePipe::Input::GetInstancePointer())->GetMoveMe()->getMoveServerPacket();

			char buffer [50];
			sprintf_s(buffer, "Position (x, y, z): %1.2f, %1.2f, %1.2f", serverData.state[0].pos[0], serverData.state[0].pos[1], serverData.state[0].pos[2]);

			m_Text->setText(Ogre::String(buffer));
		
			char buffer2 [100];

			sprintf_s(buffer2, "Quaternion (x, y, z, w): %1.2f, %1.2f, %1.2f, %1.2f", serverData.state[0].quat[0], serverData.state[0].quat[1], serverData.state[0].quat[2], serverData.state[0].quat[3]);

			quat_Text->setText(Ogre::String(buffer2));


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Draw()
	{
		return true;
	}

	bool MainMenuScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}

		case GIS::KC_SYSRQ:
			{
				SaveScene("..\\..\\Game\\Media\\scenes\\saved.scene");
				break;
			}
		}

		return true;
	}
}