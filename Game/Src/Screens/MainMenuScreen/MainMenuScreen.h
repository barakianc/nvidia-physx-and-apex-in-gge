#pragma once

// GameScreen
#include "GameScreen.h"

#include "SimpleDebugText.h"
//#include "moveclient.h"
#include <bitset>
#include "GIS.h"

namespace GamePipeGame
{
	class MainMenuScreen : public GamePipe::GameScreen
	{
	private:
		Ogre::Light* pointLight;
		float i;

		GIS::MovePadData padData;
		//MoveClient moveClient;
		GIS::MoveServerPacket serverData;

	protected:
		SimpleDebugText *m_Text;
		SimpleDebugText *quat_Text;
		
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		

	public:
		MainMenuScreen(std::string name);
		~MainMenuScreen() {}
	};
}