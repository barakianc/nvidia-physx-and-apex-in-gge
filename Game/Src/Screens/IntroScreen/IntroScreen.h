#pragma once

// GameScreen
#include "GameScreen.h"


namespace GamePipeGame
{
	class IntroScreen : public GamePipe::GameScreen
	{
	private:

	protected:

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		bool OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);
	public:
		IntroScreen(std::string name);
		~IntroScreen() {}
	};
}