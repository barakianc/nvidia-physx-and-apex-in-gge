#pragma once

// GameScreen
#include "GameScreen.h"

// LoadingScreen
#include "ExampleLoadingBar.h"


namespace GamePipeGame
{
	class LoadingScreen : public GamePipe::GameScreen
	{
	private:
		ExampleLoadingBar* m_pLoadingBar;

	protected:

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();
		bool Draw();

		bool Initialize();
		bool Destroy();

		bool Update();

		void Progress(float fProgressBarInc);
		void SetInitProportion(float initProportion);

	public:
		LoadingScreen(std::string name);
		~LoadingScreen();
	};
}