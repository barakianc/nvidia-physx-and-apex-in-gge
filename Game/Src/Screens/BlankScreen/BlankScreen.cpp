#include "StdAfx.h"
// Includes
#include "BlankScreen.h"
// Engine
#include "Engine.h"

namespace GamePipeGame
{
	/*! Constructor
	*	@param[in]	name	Name of the screen
	*/
	BlankScreen::BlankScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	/*! Load the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("ParticleUniverse");
		return true;
	}

	/*! Unload the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("ParticleUniverse");
		return true;
	}

	/*! Initializes the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());
		//CreateDefaultPhysicsScene(GetDefaultSceneManager());

		SetBackgroundColor(Ogre::ColourValue(0.5, 0.5, 0.5, 1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));

		// Create default grid
		CreateGrid();

// To play without GLE
#ifdef GLE_EDITOR
		// Initialize cameras
		EnginePtr->GetEditor()->InitEditorCameras(pSceneManager, true);
		SetActiveCamera(GetDefaultSceneManager()->getCamera("Editor_CameraPerspective"));
		EnginePtr->GetEditor()->SetEditorMode(true);
#endif

		return true;
	}
	/*! Create a basic grid for the model viewer
	*/
	void BlankScreen::CreateGrid()
	{
		int iCount				= 0 ;
		int iGridMaxX			= 50;
		int iGridMaxY			= 50;
		int iGridMaxZ			= 50;
		int iGridSeparation		= 1;

		// Create the grid
		Ogre::ManualObject * mGrid = GetDefaultSceneManager()->createManualObject("Editor_Grid");
		mGrid->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

		// extra grids are for the orthogonal cameras
		Ogre::ManualObject * mGridSide = GetDefaultSceneManager()->createManualObject("Editor_Grid_Side");
		mGridSide->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

		Ogre::ManualObject * mGridFront = GetDefaultSceneManager()->createManualObject("Editor_Grid_Front");
		mGridFront->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

		for( int i = -iGridMaxZ ; i <= iGridMaxZ ; i += iGridSeparation)
		{
			mGrid->position(-Ogre::Real(iGridMaxX),	0.0f,	Ogre::Real(i));
			mGrid->colour(0.7f,0.7f,0.7f);
			if( i == 0 )	mGrid->colour(1.0f,1.0f,1.0f);
			mGrid->position(Ogre::Real(iGridMaxX),	0.0f,	Ogre::Real(i));
			//
			mGridSide->position(0.0f,	-Ogre::Real(iGridMaxY),	Ogre::Real(i));
			mGridSide->colour(0.7f,0.7f,0.7f);
			if( i == 0 )	mGridSide->colour(1,1,1);
			mGridSide->position(0.0f,	Ogre::Real(iGridMaxY),	Ogre::Real(i));
			//
			mGridFront->position(-Ogre::Real(iGridMaxX),	Ogre::Real(i),	0.0f);
			mGridFront->colour(0.7f,0.7f,0.7f);
			if( i == 0 )	mGridFront->colour(1.0f,1.0f,1.0f);
			mGridFront->position(Ogre::Real(iGridMaxX),	Ogre::Real(i),	0.0f);
			//

			iCount++;
		}
		iCount = 0;
		for( int i = -iGridMaxX ; i <= iGridMaxX ; i += iGridSeparation)
		{
			mGrid->position(Ogre::Real(i),	0.0f, -Ogre::Real(iGridMaxZ)	);
			mGrid->colour(0.7f,0.7f,0.7f);
			if( i == 0 )	mGrid->colour(1.0f,1.0f,1.0f);
			mGrid->position(Ogre::Real(i),  0.0f,	Ogre::Real(iGridMaxZ)	);
			//
			mGridSide->position(0,	Ogre::Real(i), -Ogre::Real(iGridMaxZ)	);
			mGridSide->colour(0.7f,0.7f,0.7f);
			if( i == 0 )	mGridSide->colour(1.0f,1.0f,1.0f);
			mGridSide->position(0,  Ogre::Real(i),	Ogre::Real(iGridMaxZ)	);
			//
			mGridFront->position(Ogre::Real(i),	-Ogre::Real(iGridMaxY),	0.0f);
			mGridFront->colour(0.7f,0.7f,0.7f);
			if( i == 0 )	mGridFront->colour(1.0f,1.0f,1.0f);
			mGridFront->position(Ogre::Real(i),	Ogre::Real(iGridMaxY),	0.0f);
			//

			iCount++;
		}
		mGrid->end();
		mGridSide->end();
		mGridFront->end();
		// Add it to a new scene node
		GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode")->attachObject(mGrid);
		GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode_Side")->attachObject(mGridSide);
		GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode_Front")->attachObject(mGridFront);

		GetDefaultSceneManager()->getManualObject("Editor_Grid_Side")->setVisible(false);
		GetDefaultSceneManager()->getManualObject("Editor_Grid_Front")->setVisible(false);
	}

	/*! Create the axes for the model viewer (disabled)
	*/
	void BlankScreen::CreateAxes()
	{
		// Create the Axes
		Ogre::Entity* mAxes = GetDefaultSceneManager()->createEntity("axes","axes.mesh");
		// if there has been a problem we don't want to continue
		if(mAxes==NULL) return;
		mAxes->setRenderQueueGroup( Ogre::RENDER_QUEUE_OVERLAY );
		Ogre::SceneNode * p = GetDefaultSceneManager()->createSceneNode("axesscenenode");
		p->attachObject( mAxes );
		p->translate(  -2.5f,  -2.5f, -8.0f );
		p->setScale(0.05f, 0.05f, 0.05f);
		Ogre::Overlay *  m_pOverlay  = Ogre::OverlayManager::getSingleton( ).create( Ogre::String("AxesOverlay") );
		m_pOverlay->setZOrder(540);
		m_pOverlay->add3D( p );
		m_pOverlay->show();
	}

	/*! Called when the screen is being destroyed
	*	@return	True if everything went well
	*/
	bool BlankScreen::Destroy()
	{
		//DestroyDefaultPhysicsScene();

		return true;
	}

	/*! Show the screen
	*	@return	True if everything went well
	*/
	bool BlankScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	/*! Update the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::Update()
	{
		GameObject* currentlySelectedObject = EnginePtr->GetEditor()->CurrentGameObject();
		if (currentlySelectedObject  != NULL)
		{
			currentlySelectedObject->update();
		}
		// Update the axes - Feature disabled
		//if(GetActiveCamera() == m_pCameraPerspective)
		//{
		//	if(GetDefaultSceneManager()->hasSceneNode("axesscenenode"))
		//		GetDefaultSceneManager()->getSceneNode("axesscenenode")->setOrientation( m_pCameraPerspective->getOrientation());
		//}

		return true;
	}

	/*! Draw the scene
	*	@return	True if everything went well
	*/
	bool BlankScreen::Draw()
	{
		return true;
	}
}