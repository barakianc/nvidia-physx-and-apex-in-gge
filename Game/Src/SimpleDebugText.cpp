#include "StdAfx.h"
#include "SimpleDebugText.h"

#include "OgreFontManager.h"

namespace GamePipe{

	int SimpleDebugText::ID = 0;

	SimpleDebugText::SimpleDebugText(int fontSize, Ogre::String t){

		ID++;

		//create overlay
		
		text = Ogre::String(t);
		overlay = Ogre::OverlayManager::getSingleton().create(Ogre::String("SimpleText_" + ID));
		
		container = (Ogre::OverlayContainer*)Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", "container_" + Ogre::String("SimpleText_" + ID));
		overlay->add2D(container);

		textArea = Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea","text_area_" + Ogre::String("SimpleText_" + ID));
		textArea->setDimensions(0.8f, 0.8f);
		textArea->setMetricsMode(Ogre::GMM_PIXELS);
		textArea->setPosition(0.1f, 0.1f);

		Ogre::FontManager::getSingleton().load("EngineFont", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		font = (Ogre::Font*)Ogre::FontManager::getSingleton().getByName("EngineFont").getPointer();
		textArea->setParameter("font_name", "EngineFont");
		//textArea->setParameter("char_height", font->getParameter("size"));
		
		
		std::stringstream ss;
		ss << fontSize;

		textArea->setParameter("char_height", ss.str());
		textArea->setParameter("horz_align", "left");
		//textArea->setColour(color);
		textArea->setCaption(text);
		//textArea->setPosition(0, 100);
		container->addChild(textArea);
		overlay->show();

		initialized = true;
	
	}
	
	void SimpleDebugText::setText(Ogre::String t){
		if(initialized){
			text = Ogre::String(t);
			textArea->setCaption(text);

		}
	}

	void SimpleDebugText::setPosition(Ogre::Real left, Ogre::Real top){
		textArea->setPosition(left, top);
	}
	
	SimpleDebugText::~SimpleDebugText(void)
	{

	}

}
