#pragma once

#include "Debug/DebugText2d.h"

namespace GamePipe
{
	class SimpleDebugText
	{
	
	private:

		int fontSize;
		bool initialized;
		static int ID;
	protected:
		Ogre::Overlay *overlay;
        Ogre::OverlayElement *textArea;
        Ogre::OverlayContainer* container;
        Ogre::Font *font;
        Ogre::String text;

		
	
	public:
		SimpleDebugText(int fontSize, Ogre::String text);
		void setText(Ogre::String text);
		void setPosition(Ogre::Real left, Ogre::Real top);
		~SimpleDebugText(void);
	};
}

