# Resource locations to be added to the 'boostrap' path
# This also contains the minimum you need to use the Ogre example framework
[Bootstrap]
Zip=../../Game/Media/packs/OgreCore.zip
FileSystem=../../Game/Media/fonts


# Resource locations to be added to the default path
[General]
FileSystem=../../Game/Media
FileSystem=../../Game/Media/materials/programs
FileSystem=../../Game/Media/materials/scripts
FileSystem=../../Game/Media/materials/textures
FileSystem=../../Game/Media/models
FileSystem=../../Game/Media/overlays
FileSystem=../../Game/Media/particle
FileSystem=../../Game/Media/gui
FileSystem=../../Game/Media/Havok
FileSystem=../../Game/Media/scenes
FileSystem=../../Game/Media/Collada

[ParticleUniverse]
FileSystem=../../Game/Media/ParticleUniverse
FileSystem=../../Game/Media/ParticleUniverse/material
FileSystem=../../Game/Media/ParticleUniverse/scripts
FileSystem=../../Game/Media/ParticleUniverse/models
FileSystem=../../Game/Media/ParticleUniverse/gui
FileSystem=../../Game/Media/ParticleUniverse/textures

[IntroScreen]
FileSystem=../../Game/Media/Screens/Intro

[LoadingScreen]
FileSystem=../../Game/Media/Screens/Loading

[SkyX]
FileSystem=../../Game/Media/SkyX

[Hydrax]
FileSystem=../../Game/Media/Hydrax