package
{
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.events.*;
	import flash.net.URLRequest;
	import flash.display.Bitmap;
	public class MenuBtn extends MovieClip
	{
		public var menuIndex:int;
		var imageLoader:Loader;
		public function MenuBtn()
		{
			super();
		}
		public function loadImage(theURL:String):void
		{
			imageLoader =new Loader();
			var imageRequest = new URLRequest(theURL);
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			imageLoader.load(imageRequest);
		}
		private function onComplete(evt:Event) {
			var snapshot = imageLoader.content;
			//snapshot.setRegistrationPoint( snapshot, snapshot.width/2, snapshot.height/2, true); 
			snapshot.x -= snapshot.width/2;
			snapshot.y -= snapshot.height/2;
			addChild(snapshot);
		}
	}
}