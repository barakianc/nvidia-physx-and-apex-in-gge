fragment_program passthrough_fp_hlsl hlsl
{
	source passthrough.hlsl
    entry_point mainPS
	target ps_3_0
}

vertex_program passthrough_vp_hlsl hlsl
{
	source passthrough.hlsl
	entry_point mainVS
	target vs_3_0
}

vertex_program blinn_vp_hlsl hlsl
{
	source blinn.fx
    entry_point std_VS
	target vs_3_0
}

fragment_program blinn_fp_hlsl hlsl
{
	source blinn.fx
    entry_point std_PS
	target ps_3_0
}

vertex_program carpaint_vp_hlsl hlsl
{
	source carpaint.fx
	entry_point carPaintVS
	target vs_3_0
}

fragment_program carpaint_fp_hlsl hlsl
{
	source carpaint.fx
	entry_point litPS
	target ps_3_0
}

vertex_program velvet_vp_hlsl hlsl
{
	source velvet.fx
	entry_point std_VS
	target vs_3_0
}

fragment_program velvet_fp_hlsl hlsl
{
	source velvet.fx
	entry_point velvetPS
	target ps_3_0
}

vertex_program pulse_vp_hlsl hlsl
{
	source pulse.fx
	entry_point std_VS
	target vs_3_0
}

fragment_program pulse_fp_hlsl hlsl
{
	source pulse.fx
	entry_point pulsetrainPS
	target ps_3_0
}

vertex_program ocean_vp_hlsl hlsl
{
	source ocean.fx
	entry_point mainVS
	target vs_3_0
}

fragment_program ocean_fp_hlsl hlsl
{
	source ocean.fx
	entry_point mainPS
	target ps_3_0
}

material passthrough
{
	technique
	{
		pass
		{
			vertex_program_ref passthrough_vp_hlsl
			{
				param_named_auto WorldViewProj worldviewproj_matrix
			}

			fragment_program_ref passthrough_fp_hlsl
			{
				
			}
		}
	}
}

material blinners 
{
	technique
	{
		pass
		{
			vertex_program_ref blinn_vp_hlsl
			{
				param_named_auto WorldITXf inverse_transpose_world_matrix
				param_named_auto WvpXf worldviewproj_matrix
				param_named_auto WorldXf world_matrix
				param_named_auto ViewIXf inverse_view_matrix
				
				param_named_auto Lamp0Pos light_position 0
				
			}

			fragment_program_ref blinn_fp_hlsl
			{
				param_named_auto AmbiColor ambient_light_colour
				param_named_auto Lamp0Color light_diffuse_colour 0
				param_named Ks float 0.4
				param_named Eccentricity float 0.3
			}

			texture_unit 
			{
				texture splendor.jpg
			}
		}
	}
}

material carpaint 
{
	technique
	{
		pass
		{
			vertex_program_ref carpaint_vp_hlsl
			{
				param_named_auto WvpXf worldviewproj_matrix
				param_named_auto WorldITXf inverse_transpose_world_matrix
				param_named_auto WorldXf world_matrix
				param_named_auto ViewIXf inverse_view_matrix

				param_named_auto LampPos light_position 0
			}

			fragment_program_ref carpaint_fp_hlsl
			{
				param_named_auto LampColor light_diffuse_colour 0
				param_named_auto AmbiColor ambient_light_colour
				param_named SpecExpon float 55
			}

			texture_unit 
			{
				texture carpaint_mystique.dds
			}
		}
	}
}

material velvet 
{
	technique
	{
		pass
		{
			vertex_program_ref velvet_vp_hlsl
			{
				param_named_auto WvpXf worldviewproj_matrix
				param_named_auto WorldITXf inverse_transpose_world_matrix
				param_named_auto WorldXf world_matrix
				param_named_auto ViewIXf inverse_view_matrix

				param_named_auto LampPos light_position 0

			}

			fragment_program_ref velvet_fp_hlsl
			{
				param_named_auto Lamp0Color light_diffuse_colour 0

				param_named FuzzySpecColor	float4 0.7 0.7 0.75 1.0
				param_named SubColor		float4 0.2 0.2 1.0 1.0
				param_named RollOff			float 0.3
				param_named SurfaceColor	float4 0.5 0.5 0.5 1.0
			}
		}
	}
}

material pulse 
{
	technique
	{
		pass
		{
			vertex_program_ref pulse_vp_hlsl
			{
				param_named_auto WvpXf worldviewproj_matrix
				param_named_auto WorldITXf inverse_transpose_world_matrix
				param_named_auto WorldXf world_matrix
				param_named_auto ViewIXf inverse_view_matrix

				param_named_auto LampPos light_position 0
			}

			fragment_program_ref pulse_fp_hlsl
			{
				param_named_auto Timer			time
				param_named_auto LampColor		light_diffuse_colour 0
				param_named_auto AmbiColor		ambient_light_colour
				
				param_named LampIntensity	float 1000.5
				param_named SurfaceColor	float3 1.0 1.0 1.0
				param_named Ks				float 0.4
				param_named SpecExpon		float 50.0
				param_named Speed			float 5.0
				param_named Oversample		float 1.0
				param_named Period			float 1.0
				param_named Balance			float 0.5
				param_named WaveFreq		float 10.5
				param_named WaveGain		float 0.4
			}
		}
	}
}

material ocean 
{
	technique
	{
		pass
		{
			vertex_program_ref ocean_vp_hlsl
			{
				param_named_auto WorldViewProj	worldviewproj_matrix
				param_named_auto seconds		time

				param_named		timescale			float	1.0

				param_named		Wave0Direction		float2	0.2
				param_named		Wave0Amp			float	3.01
				param_named		Wave0Speed			float	2.0
				param_named		Wave0Freq			float	0.15

				param_named		Wave1Direction		float2	-0.2 1.0
				param_named		Wave1Amp			float	2.01		
				param_named		Wave1Speed			float	0.9
				param_named		Wave1Freq			float	0.07

				param_named		Wave2Direction		float2	0.0 -1.3
				param_named		Wave2Amp			float	5.55	
				param_named		Wave2Speed			float	3.0
				param_named		Wave2Freq			float	0.01

				param_named		Wave3Direction		float2	-0.5 -0.1
				param_named		Wave3Amp			float	0.5		
				param_named		Wave3Speed			float	3.2
				param_named		Wave3Freq			float	0.56
			}

			fragment_program_ref ocean_fp_hlsl
			{
				param_named_auto Lamp0Color		light_diffuse_colour 0
				param_named_auto Lamp0Pos		light_position 0
				param_named_auto seconds		TIME
				param_named_auto EyePos			camera_position

				param_named Steepness		float	1
				param_named fresnelPower	float	0.5
				param_named diffuseColor	float3	0.2 0.25 0.2
				param_named reflectionTint	float3	0.5 0.5 0.5

				param_named specularColor	float3	0.9 0.9 1.0
				param_named specularPower	float	150.0

				param_named ambientColor	float3	0.05 0.08 0.08

				param_named Bump			float	0.5

				param_named		Wave0Direction		float2	0.2
				param_named		Wave0Amp			float	3.01
				param_named		Wave0Speed			float	2.0
				param_named		Wave0Freq			float	0.15

				param_named		Wave1Direction		float2	-0.2 1.0
				param_named		Wave1Amp			float	2.01		
				param_named		Wave1Speed			float	0.9
				param_named		Wave1Freq			float	0.07

				param_named		Wave2Direction		float2	0.0 -1.3
				param_named		Wave2Amp			float	5.55	
				param_named		Wave2Speed			float	3.0
				param_named		Wave2Freq			float	0.01

				param_named		Wave3Direction		float2	-0.5 -0.1
				param_named		Wave3Amp			float	0.5		
				param_named		Wave3Speed			float	3.2
				param_named		Wave3Freq			float	0.56

				param_named 	waterNorm0Dir		float2	1.0 0.0
				param_named		waterNorm0Scale		float2	15.0 15.0
				param_named 	waterNorm0Speed		float	10.0

				param_named 	waterNorm1Dir		float2	0.2 0.1
				param_named		waterNorm1Scale		float2	15.0 5.0
				param_named 	waterNorm1Speed		float	15.0

				param_named 	waterNorm2Dir		float2	-0.2 0.3
				param_named		waterNorm2Scale		float2	15.0 15.0
				param_named		waterNorm2Speed		float	3.0

				param_named timescale				float	1.0
				param_named UVWidth					float	250.0
			}

			texture_unit 
			{
				texture CGSkies_0282_free.dds
			}

			texture_unit 
			{
				texture 723-normal.jpg
			}
		}
	}
}