
/*

% Description of my shader.
% Second line of description for my shader.

keywords: material classic

date: YYMMDD

*/

struct VertexOutput {
	float4 position : POSITION0;
	float3 worldPos : TEXCOORD0;
};

struct VertexInput {
	float4 aPos : POSITION0;
};

//
// Autos
//

float4x4 	WorldViewProj	: 	WorldViewProjection;
float3		EyePos			:	CameraPosition;

//
//Tweakables
//

/// Point Lamp 0 ////////////
float3 Lamp0Pos : Direction <
    string Object = "PointLight0";
    string UIName =  "Lamp 0 Position";
    string Space = "World";
> = {-0.5f,2.0f,1.25f};
float3 Lamp0Color : Specular <
    string UIName =  "Lamp 0";
    string Object = "Pointlight0";
    string UIWidget = "Color";
> = {1.0f,1.0f,1.0f};

//
// Textures
//

texture EnvTexture : ENVIRONMENT <
    string ResourceName = "default_reflection.dds";
    string UIName =  "Environment";
    string ResourceType = "Cube";
>;

samplerCUBE EnvSampler = sampler_state {
    Texture = <EnvTexture>;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
    AddressW = Clamp;
};

//Were going to overlap this on itself many times
//at many scales

texture WaterNormalTexture <
    string ResourceName = "default_bump_normal.dds";
    string UIName =  "Normal-Map Texture";
    string ResourceType = "2D";
>;

sampler2D NormalSampler = sampler_state {
	Texture = <WaterNormalTexture>;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

float Steepness;
float fresnelPower;
float3 diffuseColor;
float3 reflectionTint;

float3 	specularColor;
float 	specularPower;

float3 	ambientColor;

float 	Bump;

float2 	waterNorm0Dir;
float2	waterNorm0Scale;
float 	waterNorm0Speed;

float2 	waterNorm1Dir;
float2	waterNorm1Scale;
float 	waterNorm1Speed;

float2 	waterNorm2Dir;
float2	waterNorm2Scale;
float	waterNorm2Speed;

float seconds : TIME;
float timescale;
float UVWidth;

//Wave1
float2 Wave0Direction;	//Normalized 2D vector
float Wave0Amp;			
float Wave0Speed;		//stored as phase: speed / freq
float Wave0Freq;		//2 * PI / Wavelength

//Wave2
float2 Wave1Direction;
float Wave1Amp;
float Wave1Speed;
float Wave1Freq;

//Wave3
float2 Wave2Direction;
float Wave2Amp;
float Wave2Speed;
float Wave2Freq;

//Wave4
float2 Wave3Direction;
float Wave3Amp;
float Wave3Speed;
float Wave3Freq;

//Fix me


//Ocean-Wide Params

VertexOutput mainVS(VertexInput vsInput) {
	VertexOutput output;
	
	//TODO - take out this adjustment, its unessesary
	static float t = seconds * timescale;
	
	float3 WaveOffset = float3( 0, 0, 0 );
	float ActualSteepness = Steepness / (Wave0Freq * Wave0Amp * 4.0);
	
	float2 aWave0Direction = normalize( Wave0Direction );
	float2 aWave1Direction = normalize( Wave1Direction );
	float2 aWave2Direction = normalize( Wave2Direction );
	float2 aWave3Direction = normalize( Wave3Direction );
	
	float h;
	float4 wac;
	float4 was;
	
	h = dot( Wave0Freq * aWave0Direction, vsInput.aPos.xz ) + (t * Wave0Speed);
	wac.x = cos( h );
	was.x = sin( h );
	
	h = Wave1Freq * dot( aWave1Direction, vsInput.aPos.xz ) + (t * Wave1Speed);
	wac.y = cos( h );
	was.y = sin( h );
	
	h = Wave2Freq * dot( aWave2Direction, vsInput.aPos.xz ) + (t * Wave2Speed);
	wac.z = cos( h );
	was.z = sin( h );
	
	h = Wave3Freq * dot( aWave3Direction, vsInput.aPos.xz ) + (t * Wave3Speed);
	wac.w = cos( h );
	was.w = sin( h );
	
	//adjust positions now
	float3 offset = float3( 0, 0, 0 );
	
	float tmp;
	tmp = Wave0Amp * ActualSteepness;
	offset.x += tmp * aWave0Direction.x * wac.x;
	offset.z += tmp * aWave0Direction.y * wac.x;
	offset.y += Wave0Amp * was.x;
	
	tmp = Wave1Amp * ActualSteepness;
	offset.x += tmp * aWave0Direction.x * wac.y;
	offset.z += tmp * aWave0Direction.y * wac.y;
	offset.y += Wave1Amp * was.y;
	
	tmp = Wave2Amp * ActualSteepness;
	offset.x += tmp * aWave0Direction.x * wac.z;
	offset.z += tmp * aWave0Direction.y * wac.z;
	offset.y += Wave2Amp * was.z;
	
	tmp = Wave3Amp * ActualSteepness;
	offset.x += tmp * aWave0Direction.x * wac.w;
	offset.z += tmp * aWave0Direction.y * wac.w;
	offset.y += Wave3Amp * was.w;
	
	//Finish computation of WAS and WAC terms
	//Which are passed to be LERPd in the pixel shader
	float WA;
	
	WA = Wave0Freq * Wave0Amp;
	was.x *= WA;
	wac.x *= WA;
	
	WA = Wave1Freq * Wave1Amp;
	was.y *= WA;
	wac.y *= WA;
	
	WA = Wave2Freq * Wave2Amp;
	was.z *= WA;
	wac.z *= WA;
	
	WA = Wave3Freq * Wave3Amp;
	was.w *= WA;
	wac.w *= WA;
	
	//All done
	vsInput.aPos.xyz += offset;
	output.worldPos.xyz = vsInput.aPos.xyz;
	output.position = mul(WorldViewProj, float4(vsInput.aPos.xyz, 1.0));
	return output;
}

void AdjustTangentSpace( 
	in float freq, in float amp, in float2 dir, in float2 pos, in float speed,
	inout float3 n, inout float3 t, inout float3 bt )
{
	static float time = seconds * timescale;
	
	float ActualSteepness = Steepness / (freq * amp * 4.0);
	float h = freq * dot( dir, pos ) + (time * speed);
	float wac = cos( h );
	float was = sin( h );
	
	was *= freq * amp;
	wac *= freq * amp;
	
	n.x -= dir.x * wac;
	n.z -= dir.y * wac;
	n.y -= ActualSteepness * was;
	
	t.x -= ActualSteepness * dir.x * dir.y * was;
	t.z -= ActualSteepness * dir.y * dir.y * was;
	t.y += dir.y * wac;
	
	bt.x -= ActualSteepness * dir.x * dir.x * was;
	bt.z -= ActualSteepness * dir.x * dir.y * was;
	bt.y += dir.x * wac;
}

float4 mainPS( VertexOutput input ) : COLOR {
	
	//TODO - take out this adjustment, its unessesary
	static float adjustedTime = seconds * timescale;
	
	//Compute wave state
	float ActualSteepness = Steepness / (Wave0Freq * Wave0Amp * 4.0);
	
	float2 aWave0Direction = normalize( Wave0Direction );
	float2 aWave1Direction = normalize( Wave1Direction );
	float2 aWave2Direction = normalize( Wave2Direction );
	float2 aWave3Direction = normalize( Wave3Direction );
	
	//Per-pixel tangent space lighting
	
	float3 normal = float3( 0, 0, 0 );
	float3 tangent = float3( 0, 0, 0 );
	float3 binormal = float3( 0, 0, 0 );
	
	float3x3 tp;
	
	//tangent space basis for gerstner waves
	
	AdjustTangentSpace( 
		Wave0Freq, Wave0Amp, aWave0Direction, input.worldPos.xz, Wave0Speed,
		normal, tangent, binormal );
	
	AdjustTangentSpace(
		Wave1Freq, Wave1Amp, aWave1Direction, input.worldPos.xz, Wave1Speed,
		normal, tangent, binormal );
	
	AdjustTangentSpace(
		Wave2Freq, Wave2Amp, aWave2Direction, input.worldPos.xz, Wave2Speed,
		normal, tangent, binormal );
		
	AdjustTangentSpace(
		Wave3Freq, Wave3Amp, aWave3Direction, input.worldPos.xz, Wave3Speed,
		normal, tangent, binormal );
	
	//finish calc
	normal.y += 1.0;
	normal = normalize( normal );
	
	tangent.z += 1.0;
	tangent = normalize( tangent );
	
	binormal.x += 1.0;
	binormal = normalize( binormal );
	
	//Add normals from all contributing maps
	float3 normal2;
	float3 bump;
	float2 aUV;
	
	//wave 1
	aUV = float2( input.worldPos.x / UVWidth, input.worldPos.z / UVWidth);
	aUV += adjustedTime * waterNorm0Dir * waterNorm0Speed/1000.0;
	aUV *= waterNorm0Scale;
	
	bump = Bump * (tex2D(NormalSampler, aUV).rgb - float3(0.5,0.5,0.5));
	
	//wave 2
	
	aUV = float2( input.worldPos.x / UVWidth, (input.worldPos.z + 6) / UVWidth );
	aUV += adjustedTime * waterNorm1Dir * waterNorm1Speed/1000.0;
	aUV *= waterNorm1Scale;
	
	bump += Bump * (tex2D(NormalSampler, aUV).rgb - float3(0.5,0.5,0.5));
	
	//wave 3
	aUV = float2( input.worldPos.x / UVWidth, input.worldPos.z / UVWidth);
	aUV += adjustedTime * waterNorm2Dir * waterNorm2Speed/1000.0;
	aUV *= waterNorm2Scale;
	
	bump += Bump * (tex2D(NormalSampler, aUV).rgb - float3(0.5,0.5,0.5));
	
	//All done
	normal2 = bump.x * tangent + bump.y * binormal;
	normal += normal2;
	
	normal = normalize( normal );
	float3 aLampDir = normalize(Lamp0Pos);
	float ndl = dot( normal, aLampDir );
	
	float3 r;
	
	//Fresnel Reflection
	float3 eye = normalize( input.worldPos - EyePos );
	float fresnel = 1 - pow( dot( normal, -eye ), fresnelPower);	//TODO - tex1D lookup
	r = reflect( eye, normal );
		
	float3 baseColor = diffuseColor * ndl * Lamp0Color;
	float3 reflectionColor = texCUBE(EnvSampler, r) * reflectionTint;
	
	//Specularity
	r = reflect( aLampDir, normal );
	float rdotv = dot( r, eye );
	float3 specularTerm = specularColor * min( max( pow( rdotv, specularPower ), 0 ), 1); 
	
	float3 finalColor = float3( 0, 0, 0 );
	finalColor = lerp( baseColor, reflectionColor, fresnel );
	finalColor += ambientColor + specularTerm;
	return float4( finalColor, 1.0 );
}

technique simple {
    pass p0 {
		VertexShader = compile vs_3_0 mainVS();
        PixelShader = compile ps_3_0 mainPS();        
    }
}