/*********************************************************************NVMH3****
*******************************************************************************
$Revision$

Copyright NVIDIA Corporation 2008
TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED
*AS IS* AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL NVIDIA OR ITS SUPPLIERS
BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES
WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS,
BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER PECUNIARY
LOSS) ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF
NVIDIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

% Blast from the past (Cg 1 Maya sample shader).
% Analytic anti-aliasing against an arbitrary function -- in this case
% pulsing 3D sine waves.

keywords: pattern material animation
date: 070711



To learn more about shading, shaders, and to bounce ideas off other shader
    authors and users, visit the NVIDIA Shader Library Forums at:

    http://developer.nvidia.com/forums/

*******************************************************************************
******************************************************************************/

/*****************************************************************/
/*** HOST APPLICATION IDENTIFIERS ********************************/
/*** Potentially predefined by varying host environments *********/
/*****************************************************************/

// #define _XSI_		/* predefined when running in XSI */

#ifndef FXCOMPOSER_VERSION	/* for very old versions */
#define FXCOMPOSER_VERSION 180
#endif /* FXCOMPOSER_VERSION */

// #define FLIP_TEXTURE_Y	/* Different in OpenGL & DirectX */

/*****************************************************************/
/*** EFFECT-SPECIFIC CODE BEGINS HERE ****************************/
/*****************************************************************/

/******* Lighting Macros *******/
/** To use "Object-Space" lighting definitions, change these two macros: **/
#define LIGHT_COORDS "World"
// #define OBJECT_SPACE_LIGHTS /* Define if LIGHT_COORDS is "Object" */
/** Define the macro USE_SHARED_SHADOW to permit the import and use of "shared
		surface "shadow maps created by COLLADA-FX. **/
// #define USE_SHARED_SHADOW

/**** UNTWEAKABLES: Hidden & Automatically-Tracked Parameters **********/

// transform object vertices to world-space:
float4x4 gWorldXf : World < string UIWidget="None"; >;
// transform object normals, tangents, & binormals to world-space:
float4x4 gWorldITXf : WorldInverseTranspose < string UIWidget="None"; >;
// transform object vertices to view space and project them in perspective:
float4x4 gWvpXf : WorldViewProjection < string UIWidget="None"; >;
// provide tranform from "view" or "eye" coords back to world-space:
float4x4 gViewIXf : ViewInverse < string UIWidget="None"; >;

float gTimer : TIME < string UIWidget = "None"; >;

/*****************************************/
/************* TWEAKABLES ****************/
/*****************************************/

// (Quadratic-Falloff) Lamp 0 /////////////
float3 gLamp0Pos : POSITION <
    string Object = "PointLight0";
    string UIName =  "Lamp 0 Position";
    string Space = (LIGHT_COORDS);
> = {-0.5f,2.0f,1.25f};
float3 gLamp0Color <
    string UIName =  "Lamp 0";
    string Object = "Pointlight0";
    string UIWidget = "Color";
> = {1.0f,1.0f,1.0f};
float gLamp0Intensity <
    string UIWidget = "slider";
    float UIMin = 1.0;
    float UIMax = 10000.0f;
    float UIStep = 0.1;
    string UIName =  "Lamp 0 Quadratic Intensity";
> = 1.0f;


// Ambient Light
float3 gAmbiColor <
    string UIName =  "Ambient Light";
    string UIWidget = "Color";
> = {0.07f,0.07f,0.07f};

// surface color
float3 gSurfaceColor <
    string UIName =  "Surface";
    string UIWidget = "Color";
> = {1,1,1};

float gKs <
    string UIWidget = "slider";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
    string UIName =  "Specular";
> = 0.4;

float gSpecExpon <
    string UIWidget = "slider";
    float UIMin = 1.0;
    float UIMax = 128.0;
    float UIStep = 1.0;
    string UIName =  "Specular Exponent";
> = 30.0;

float gOversample <
    string UIWidget = "slider";
    float uimin = 0.0;
    float uimax = 20.0;
    float uistep = 0.0001;
    string UIName = "AA Softness";
 > = 1.0;

float gPeriod <
    string UIWidget = "slider";
    float uimin = 0.0;
    float uimax = 0.4;
    float uistep = 0.001;
    string UIName = "Stripe Period";
> = 0.5;

float gBalance <
    string UIWidget = "slider";
    float uimin = 0.01;
    float uimax = 0.99;
    float uistep = 0.01;
    string UIName = "Clip Balance";
 > = 0.5;

float gWaveFreq <
    string UIWidget = "slider";
    float uimin = 0.0;
    float uimax = 40.0;
    float uistep = 0.01;
    string UIName = "Wave Period";
 > = 7.0;

float gWaveGain <
    string UIWidget = "slider";
    float uimin = 0.0;
    float uimax = 1.0;
    float uistep = 0.01;
    string UIName = "Wobbliness";
 > = 0.2;

float gSpeed <
    string UIWidget = "slider";
    float uimin = 0.0;
    float uimax = 10.0;
    float uistep = 0.01;
    string UIName = "Wave Speed";
> = 2.5;

#ifdef USE_SHARED_SHADOW
#define MAX_SHADOW_BIAS 0.01
#define MIN_SHADOW_BIAS 0.00005
#include "include/shadowMap.cgh"
DECLARE_SHADOW_XFORMS("SpotLight0",gLampViewXf,gLampProjXf,gShadowViewProjXf)
DECLARE_SHADOW_BIAS
DECLARE_SHADOW_MAPS(ColorShadTarget,ColorShadSampler,DepthShadTarget,DepthShadSampler)

float ShadDens <
    string UIWidget = "slider";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
    string UIName =  "Shadow Density";
> = 0.7;
#endif /* USE_SHARED_SHADOW */


/************* DATA STRUCTS **************/

/* data from application vertex buffer */
struct appdata {
    float3 Position	: POSITION;
    float4 UV		: TEXCOORD0;
    float4 Normal	: NORMAL;
    float4 Tangent	: TANGENT0;
    float4 Binormal	: BINORMAL0;
};

/* data passed from vertex shader to pixel shader */
struct vertexOutput {
    float4 HPosition	: POSITION;
    float2 UV		: TEXCOORD0;
    // The following values are passed in "World" coordinates since
    //   it tends to be the most flexible and easy for handling
    //   reflections, sky lighting, and other "global" effects.
    float3 LightVec	: TEXCOORD1;
    float3 WorldNormal	: TEXCOORD2;
    float3 WorldTangent	: TEXCOORD3;
    float3 WorldBinormal : TEXCOORD4;
    float3 WorldView	: TEXCOORD5;
#ifdef USE_SHARED_SHADOW
    // This optional value expresses the current location in "light"
    //   coordinates for use with shadow mapping.
    float4 LProj	: LPROJ_COORD;
#endif /* USE_SHARED_SHADOW */
};

/*********** vertex shader ******/

/*********** Generic Vertex Shader ******/

vertexOutput std_VS(appdata IN,
	uniform float4x4 WorldITXf, // our four standard "untweakable" xforms
	uniform float4x4 WorldXf,
	uniform float4x4 ViewIXf,
	uniform float4x4 WvpXf,
#ifdef USE_SHARED_SHADOW
	uniform float4x4 ShadowViewProjXf,
	uniform float ShadBias,
#endif /* USE_SHARED_SHADOW */
	uniform float3 LampPos) {
    vertexOutput OUT = (vertexOutput)0;
    OUT.WorldNormal = mul(WorldITXf,IN.Normal).xyz;
    OUT.WorldTangent = mul(WorldITXf,IN.Tangent).xyz;
    OUT.WorldBinormal = mul(WorldITXf,IN.Binormal).xyz;
    float4 Po = float4(IN.Position.xyz,1); // homogeneous location
    float4 Pw = mul(WorldXf,Po);	// convert to "world" space
	//float4 Pw = mul(Po,WorldXf);	// convert to "world" space
#ifdef OBJECT_SPACE_LIGHTS
    float4 Lo = float4(LampPos.xyz,1.0); // homogeneous coordinates
    float4 Lw = mul(WorldXf,Lo);	// convert to "world" space
    OUT.LightVec = (Lw.xyz - Pw.xyz);
	OUT.LightVec = float3(0,0,0);
#else /* !OBJECT_SPACE_LIGHTS -- standard world-space lights */
    OUT.LightVec = (LampPos - Pw.xyz);
#endif /* !OBJECT_SPACE_LIGHTS */
#ifdef FLIP_TEXTURE_Y
    OUT.UV = float2(IN.UV.x,(1.0-IN.UV.y));
#else /* !FLIP_TEXTURE_Y */
    OUT.UV = IN.UV.xy;
#endif /* !FLIP_TEXTURE_Y */
#ifdef USE_SHARED_SHADOW
    float4 Pl = mul(ShadowViewProjXf,Pw);  // "P" in light coords
    float4x4 BiasXf = make_bias_mat(ShadBias);
    OUT.LProj = mul(BiasXf,Pl);		// bias to make texcoord
#endif /* USE_SHARED_SHADOW */
    OUT.WorldView = normalize(float3(ViewIXf[0].w,ViewIXf[1].w,ViewIXf[2].w) - Pw.xyz);
    OUT.HPosition = mul(WvpXf,Po);
    return OUT;
}

/********* pixel shader ********/

// PS with box-filtered step function
float4 pulsetrainPS(vertexOutput IN,
		    uniform float3 SurfaceColor,
		    uniform float Ks,
		    uniform float SpecExpon,
		    uniform float3 LampColor,
		    uniform float LampIntensity,
		    uniform float3 AmbiColor,
		    uniform float Timer,
		    uniform float Speed,
		    uniform float Oversample,
		    uniform float Period,
		    uniform float Balance,
		    uniform float WaveFreq,
		    uniform float WaveGain
) : COLOR {
    float3 Ln = normalize(IN.LightVec);
    float falloff = LampIntensity/dot(IN.LightVec,IN.LightVec);
    float3 Nn = normalize(IN.WorldNormal);
    float wavy = WaveGain*sin(IN.UV.y*WaveFreq+(Speed*Timer));
    float balanced = (wavy + Balance);
    balanced = min(1.0,max(0.0,balanced));
    float edge = Period*balanced;
    float width = abs(ddx(IN.UV.x)) + abs(ddy(IN.UV.x));
    float w = width*Oversample/Period;
    float x0 = IN.UV.x/Period - (w/2.0);
    float x1 = x0 + w;
    float nedge = edge/Period;
    float i0 = (1.0-nedge)*floor(x0) + max(0.0, frac(x0)-nedge);
    float i1 = (1.0-nedge)*floor(x1) + max(0.0, frac(x1)-nedge);
    float s = (i1 - i0)/w;
    s = min(1.0,max(0.0,s));
    float3 Vn = normalize(IN.WorldView);
    float3 Hn = normalize(Vn + Ln);
    float hdn = pow(max(0,dot(Hn,Nn)),SpecExpon);
    float ldn = dot(Ln,Nn);
#ifdef USE_SHARED_SHADOW
    float shadowed = tex2Dproj(DepthShadSampler,IN.LProj).x;
	float faded = 1.0-(ShadDens*(1.0-shadowed));
	ldn *= faded;
	hdn *= shadowed;
#endif /* USE_SHARED_SHADOW */
    float diffComp = falloff * max(0,ldn);
    float3 diffC = (s * SurfaceColor) * ((diffComp*LampColor) + AmbiColor);
    float3 specC = falloff * hdn * LampColor;
    float3 result = diffC + ((1.0-s)*Ks*specC);
    
	return float4(result.rgb,1.0);
	
}

/*************/

///////////////////////////////////////
/// TECHNIQUES ////////////////////////
///////////////////////////////////////

technique Main <
	string Script = "Pass=p0;";
> {
    pass p0 <
	string Script = "Draw=geometry;";
    > {
        VertexProgram = compile vs_3_0 std_VS(gWorldITXf,gWorldXf,
				gViewIXf,gWvpXf,
#ifdef USE_SHARED_SHADOW
				gShadowViewProjXf, ShadBias,
#endif /* USE_SHARED_SHADOW */
				gLamp0Pos);
		DepthTestEnable = true;
		DepthMask = true;
		CullFaceEnable = false;
		BlendEnable = false;
		DepthFunc = LEqual;
        FragmentProgram = compile ps_3_0 pulsetrainPS(gSurfaceColor,
		gKs,gSpecExpon,gLamp0Color,gLamp0Intensity,gAmbiColor,
		gTimer,gSpeed,
		gOversample,gPeriod,gBalance,gWaveFreq,gWaveGain);
    }
}

/***************************** eof ***/
