void gloBalloon_VS(
	float4 position			: POSITION,
	float4 normal			: NORMAL,

	out float4 vsOutPos		: POSITION,
	out float3 vsOutWorldNorm	: TEXCOORD0,
	out float3 vsOutWorldView	: TEXCOORD1,
	
    uniform float Inflate,
    uniform float4x4 WorldITXf, // our four standard "untweakable" xforms
	uniform float4x4 WorldXf,
	uniform float4x4 ViewIXf,
	uniform float4x4 WvpXf
) {
    vsOutWorldNorm = mul(WorldITXf,normal).xyz;
    float4 Po = float4(position.xyz,1); 
	Po += (Inflate*normalize(float4(normal.xyz,0))); // the balloon effect
    float4 Pw = mul(WorldXf,Po);
    vsOutWorldView = normalize(ViewIXf[3].xyz - Pw.xyz);
    vsOutPos = mul(WvpXf,Po);
}

void gloBalloon_PS(
	float3 vsOutWorldNorm	: TEXCOORD0,
	float3 vsOutWorldView		: TEXCOORD1,
	
	out float4 fsOutColor	: COLOR,
	
    uniform float3 GlowColor,
    uniform float GlowExpon
) {
    float3 Nn = normalize(vsOutWorldNorm);
    float3 Vn = normalize(vsOutWorldView);
    float edge = 1.0 - dot(Nn,Vn);
    edge = pow(edge,GlowExpon);
    float3 result = edge * GlowColor.rgb;
    fsOutColor =  float4(result,edge);
}