void mainVS(
	float4 position			: POSITION,
	float3 normal			: NORMAL,
	float2 UV    			: TEXCOORD0,
	
	out float4 vsOutPos		: POSITION,
	out float4 vsOutColor	: COLOR,
	out float2 vsOutUV		: TEXCOORD0,

	uniform float4x4 worldViewProj,
	uniform float4x4 world,
	uniform float4 	 cameraPos,
	uniform float4 	 lightPos,
	uniform float4	 ambientColor,
	uniform float4 	 diffColor,
	uniform float4 	 specColor,
	uniform float	 shiny
	)
{
	vsOutPos = mul(worldViewProj, position);
	vsOutUV = UV;
	
	float3 worldpos = mul(position, world);
 
	float3 	lightDir= normalize( lightPos - worldpos );
	float3 	viewDir	= normalize( cameraPos - worldpos );
 
	float 	dotNL 	= dot(normal,lightDir);
	float 	diff 	= saturate(dotNL);
 
	float3 	ref 	=  (normal * 2 * dotNL) - lightDir;
	float	dotRV	= dot(ref, viewDir);
	float 	spec 	= pow(saturate(dotRV), shiny); 
 
	vsOutColor = saturate(diff * diffColor + spec * specColor + ambientColor);
}

void mainFS(
	float4 vsOutColor		: COLOR,
	float2 vsOutUV			: TEXCOORD0,
	out float4 fsOutColor	: COLOR,
	uniform sampler2D image : register(s0)
	)
{
	fsOutColor = saturate(0.85 * vsOutColor + 0.15 * tex2D(image, vsOutUV));
}