-- init function called when the AI Agent is initialized
function init()
	setOnBehaviors("seek")
	setOnBehaviors("cohesion")
end

function firstState()
	x,y,z = getPosition()
	if (math.abs(z) <= 2) then
		state = "secondState"
	end
	setSeekTarget(30,0,0)
	
end

function secondState()
	x,y,z = getPosition()
	if (math.abs(x) <= 2) then
		state = "thirdState"
	end
	setSeekTarget(0,0,0)
	
end

function thirdState()
	x,y,z = getPosition()
	if (math.abs(z - 40) <= 2) then
		state = "fourthState"
	end
	setSeekTarget(0,0,40)
	
end

function fourthState()
	x,y,z = getPosition()
	if (math.abs(x - 20) <= 2) then
		state = "fifthState"
	end
	setSeekTarget(20,0,20)
	
end

function fifthState()
	x,y,z = getPosition()
	if (math.abs(x) <= 2) then
		state = "sixthState"
	end
	setSeekTarget(0,0,0)
	
end

function sixthState()
	x,y,z = getPosition()
	if (math.abs(x - 20) <= 2) then
		state = "firstState"
	end
	setSeekTarget(20,0,20)
	
end

--States of the FSM
states = {"initialState","finalState"}
--Current state
state = "firstState"