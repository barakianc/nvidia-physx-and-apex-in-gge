#pragma once

// Engine
#include "Engine.h"


// Screens
#include "BlankScreen\BlankScreen.h"
#include "IntroScreen\IntroScreen.h"
#include "LoadingScreen\LoadingScreen.h"
#include "MainMenuScreen\MainMenuScreen.h"
#include "RendererDemo\RendererDemo.h"
#include "FlashGuiDemo2\FlashGuiDemo2.h"
#include "FlashGuiDemo1\FlashGuiDemo1.h"
#include "ColladaScreen\ColladaScreen.h"
#include "AudioScreen\AudioScreen.h"
#include "VideoScreen\VideoScreen.h"
#include "GISDemo\GISScreen.h"
#include "HavokDemo\HavokDemo.h"
#include "AnimationDemo\AnimationDemo.h"
#include "MultiplayerOnlineDemo\Screens\HomeScreen\HomeScreen.h"
#include "InternalLightingScreen\InternalLightingScreen.h"
#include "ExternalLightingScreen\ExternalLightingScreen.h"
//#include "AIScreen\AIScreen.h"
#include "AIDemoScreen\AIDemoScreen.h"
#include "PhysXDemo\PhysXDemo.h"
#include "CloudClientScreen\CloudClientScreen.h"
