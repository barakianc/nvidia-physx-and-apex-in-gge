#include "StdAfx.h"
#include "Main.h"

#include "LoadingScreen\LoadingScreen.h"
#include "MainMenuScreen\MainMenuScreen.h"

#ifndef DLLRELEASE
#ifndef GLE_EDITOR_MODELVIEW

//////////////////////////////////////////////////////////////////////////
// A normal game
//////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	// the third prama is a string. It is the name of the folder your MediaPaths.txt is in.
	EnginePtr->Initialize(argc, argv, "Demos");
	EnginePtr->SetLoadingScreen(new GamePipeGame::LoadingScreen("LoadingScreen"));
	EnginePtr->AddGameScreen(new GamePipeGame::MainMenuScreen("MainMenuScreen"));
	int returnValue = EnginePtr->Run();

	return returnValue;
}

#else /* GLE_EDITOR_MODELVIEW */

//////////////////////////////////////////////////////////////////////////
// ModelViewer
//////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	EnginePtr->Initialize(argc, argv, "Demos");
	EnginePtr->SetLoadingScreen(new GamePipeGame::LoadingScreen("LoadingScreen"));
	EnginePtr->AddGameScreen(new GamePipeGame::BlankScreen("BlankScreen"));
	int returnValue = EnginePtr->Run();

	return returnValue;
}

#endif /* GLE_EDITOR_MODELVIEW */
#endif /* DLLRELEASE */