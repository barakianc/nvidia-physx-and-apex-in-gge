#include "StdAfx.h"
// AIScreen
#include "AIScreen.h"

// Engine
#include "Engine.h"
#include "TextFileManager.h"

// Physics
#include "HavokWrapper.h"
#include "AIAgent.h"

#include "GraphWorld.h"
#include <OgreVector2.h>
#include <vector>

#include "GraphNode.h"
#include "GraphEdge.h"
#include "GenerateScene.h"
#include "SteeringBehaviors.h"
#include "AIAgentManager.h"

#include "OgreConsole.h"

namespace GamePipeGame
{
    /////////////////////////////////////////////////////////////////
	// AIScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	AIScreen::AIScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));
        pSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

		LoadScene("aiscreen.scene");

        EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->SetVisualDebugger(true);

        ball = new GameObject("crate4m","crate4m.mesh","crate4m.hkx",PHYSICS_DYNAMIC);
        ball->setPosition(-4.0f,0.0f,-4.0f);
        ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
        //ball->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);

        ball = new GameObject("crate2m","crate2m.mesh","crate2m.hkx",PHYSICS_DYNAMIC);
        ball->setPosition(3.0f,0.0f,5.0f);
        ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
        //ball->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);

        ball = new GameObject("crate1m","crate1m.mesh","crate1m.hkx",PHYSICS_DYNAMIC);
        ball->setPosition(5.5f,0.0f,-5.5f);
        ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);
        //ball->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);

        ball = new GameObject("desert","desert.mesh","desert.hkx",PHYSICS_DYNAMIC);
        ball->setPosition(0,-20,0);
        //ball->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
        
        gWorld = new GraphWorld();

        gWorld->addNode(new GraphNode(Ogre::Vector3(0,-5.0f,0)));
        gWorld->addNode(new GraphNode(Ogre::Vector3(30,-10.0f,0)));

        gWorld->addNode(new GraphNode(Ogre::Vector3(10,-10.0f,20)));

        gWorld->addNode(new GraphNode(Ogre::Vector3(10,-7.0f,50)));

        GetGameObjectManager()->getAIAgentManager()->setWorld(gWorld);

        gWorld->updateGraphWorld();
        gWorld->initializeDebug(pSceneManager);
        gWorld->setDebug(true);

        //follow
        GameObject* leader = GenerateScene::createCharacter(1,"sceenOneSeek.lua");
        GameObject* pursuer1 = GenerateScene::createCharacter(1,"empty.lua");
        GameObject* pursuer2 = GenerateScene::createCharacter(1,"empty.lua");
        GameObject* runner = GenerateScene::createCharacter(2,"empty.lua");
        GameObject* pursuerFlee = GenerateScene::createCharacter(2,"empty.lua");

        OpenSteer::AVGroup *neighbors = new OpenSteer::AVGroup();

        neighbors->push_back(leader->getAIAgent());
        neighbors->push_back(pursuer1->getAIAgent());
        neighbors->push_back(pursuer2->getAIAgent());
        neighbors->push_back(runner->getAIAgent());
        neighbors->push_back(pursuerFlee->getAIAgent());

        leader->getAIAgent()->setDebugText("Lead Runner");
        leader->setPosition(1.0f,0.0f,0.0f);
        leader->getAIAgent()->m_steeringBehavior->WanderOn();
        //leader->getAIAgent()->m_steeringBehavior->ObstacleAvoidanceOn();
        //leader->getAIAgent()->obstacles = obstacles;
        
        pursuer1->getAIAgent()->m_steeringBehavior->PursuitOn(leader->getAIAgent());
        pursuer1->getAIAgent()->m_steeringBehavior->WanderOn();
        pursuer1->setPosition(2.0f,0.0f,0.0f);
        pursuer1->getAIAgent()->m_steeringBehavior->ObstacleAvoidanceOn();
        pursuer1->getAIAgent()->neighbors = neighbors;

        pursuer2->getAIAgent()->m_steeringBehavior->PursuitOn(leader->getAIAgent());
        pursuer2->getAIAgent()->m_steeringBehavior->WanderOn();
        pursuer2->setPosition(3.0f,0.0f,0.0f);
        pursuer2->getAIAgent()->m_steeringBehavior->ObstacleAvoidanceOn();
        pursuer2->getAIAgent()->neighbors = neighbors;

        //flee and persue
        runner->getAIAgent()->m_steeringBehavior->EvadeOn(pursuerFlee->getAIAgent());
        runner->getAIAgent()->m_steeringBehavior->PursuitOn(leader->getAIAgent());
        runner->getAIAgent()->m_steeringBehavior->WanderOn();
        runner->getAIAgent()->m_steeringBehavior->ObstacleAvoidanceOn();
        runner->setPosition(0,0,50);
        ((OpenSteer::SimpleVehicle*)runner->getAIAgent())->setPosition(OpenSteer::Vec3(0,0,50));
        runner->getAIAgent()->setDebugText("Flee");
        runner->getAIAgent()->neighbors = neighbors;

        pursuerFlee->getAIAgent()->m_steeringBehavior->PursuitOn(runner->getAIAgent());
        pursuerFlee->setPosition(50,0,50);
        ((OpenSteer::SimpleVehicle*)pursuer1->getAIAgent())->setPosition(OpenSteer::Vec3(50,0,50));
        pursuerFlee->getAIAgent()->m_steeringBehavior->WanderOn();
        pursuerFlee->getAIAgent()->m_steeringBehavior->ObstacleAvoidanceOn();
        pursuerFlee->getAIAgent()->setDebugText("Pursue Flee");
        pursuerFlee->getAIAgent()->neighbors = neighbors;

        ball = new GameObject("tower","tower.mesh","tower.hkx",PHYSICS_DYNAMIC);
        ball->setPosition(40.0f,-10.0f,-10.0f);
        ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);

        ball = new GameObject("tent","tent.mesh","tent.hkx",PHYSICS_DYNAMIC);
        ball->setPosition(80.0f,-11.0f,-10.0f);
        ball->m_pGraphicsObject->m_pOgreEntity->setCastShadows(true);

/*        GameObject *wander;

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                wander = GenerateScene::createCharacter(1,"empty.lua");
                wander->getAIAgent()->setOnBehaviors("wander");
                wander->setPosition(i + -150.0f,0,j);
                wander->getAIAgent()->setDebugText("Wander");
            }
        }
/*

  /**/      
		//TODO-SV: delete GameObject when an Entity is deleted in GLE cant be done yet
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::Destroy()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
        viewport->setBackgroundColour(Ogre::ColourValue(0.0f,0.0f,0.0f));
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::Update()
	{
		// Update Camera
        FreeCameraMovement();

        gWorld->updateGraphWorld();
        gWorld->setDebug(false);
        gWorld->initializeDebug(GetDefaultSceneManager());
        gWorld->updateDebug(0);
        gWorld->setDebug(true);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool AIScreen::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();

		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	bool AIScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		}

		return true;
	}
}
