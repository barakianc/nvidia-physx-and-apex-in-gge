#pragma once

// GameScreen
#include "GameScreen.h"

//
#include "GameObject.h"
#include "GameObjectManager.h"
#include "AIAgent.h"

#include "OgreConsole.h"

namespace GamePipe{
    class GraphWorld;
}

using namespace GamePipe;

namespace GamePipeGame
{
	class AIScreen : public GamePipe::GameScreen
	{
	private:
	protected:

		GameObject *ball;
		AIAgent *ballAI;	

		GameObject *people;
		AIAgent *peopleAI;

        GraphWorld *gWorld;

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);		

	public:
		AIScreen(std::string name);
		~AIScreen() {}
	};
}