#include "StdAfx.h"
// IntroScreen
#include "IntroScreen.h"

// Main
#include "Main.h"


namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// IntroScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	IntroScreen::IntroScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool IntroScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("IntroScreen");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool IntroScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("IntroScreen");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool IntroScreen::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());

		Ogre::OverlayManager::getSingleton().getByName("IntroOverlay")->show();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool IntroScreen::Destroy()
	{
		Ogre::OverlayManager::getSingleton().getByName("IntroOverlay")->hide();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool IntroScreen::Update()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool IntroScreen::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		EnginePtr->GetRenderWindow()->update();
		return true;
	}

	bool IntroScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				Close();
				return true;
			}
		default:
			{
				Close();
				AddGameScreen(new GamePipeGame::MainMenuScreen("MainMenuScreenFromIntro"));
				return true;
			}
		}

		return true;
	}

	bool IntroScreen::OnMousePress(const GIS::MouseEvent& mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		Close();
		AddGameScreen(new GamePipeGame::MainMenuScreen("MainMenuScreenFromIntro"));
		return true;
	}
}
