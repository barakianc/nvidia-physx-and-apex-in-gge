#include "StdAfx.h"
// LoadingScreen
#include "LoadingScreen.h"

// Main
#include "Main.h"


namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// LoadingScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	LoadingScreen::LoadingScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// ~LoadingScreen()
	//////////////////////////////////////////////////////////////////////////
	LoadingScreen::~LoadingScreen()
	{
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("Bootstrap");
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("LoadingScreen");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("LoadingScreen");
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("Bootstrap");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());		
		//CreateDefaultPhysicsScene(GetDefaultSceneManager());

		SetBackgroundColor(Ogre::ColourValue(0.0000, 0.0000, 0.0000, 1.0000));

		Ogre::OverlayManager::getSingleton().getByName("LoadingOverlay")->show();
		m_pLoadingBar = new ExampleLoadingBar();
		m_pLoadingBar->start(EnginePtr->GetRenderWindow(), 1, 1, 1.0);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::Destroy()
	{
		Ogre::OverlayManager::getSingleton().getByName("LoadingOverlay")->hide();
		m_pLoadingBar->finish();

		delete m_pLoadingBar;
		m_pLoadingBar = NULL;

		//DestroyDefaultPhysicsScene();
		//DestroyDefaultSceneManager();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex(), 0.0000, 0.0000, 1.0000, 1.0000);
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::Update()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool LoadingScreen::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		EnginePtr->GetRenderWindow()->update();
		return true;
	}

	void LoadingScreen::Progress(float progress)
	{
		m_pLoadingBar->manualProgression(progress);
	}

	void LoadingScreen::SetInitProportion(Real initProportion)
	{
		m_pLoadingBar->mInitProportion = initProportion;
	}
}