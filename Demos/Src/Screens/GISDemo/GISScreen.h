#pragma once


//#include "input.h"

// GameScreen
#include "GameScreen.h"
#include "Hikari.h"


// Debug Stuff
//#include "Utilities.h"

namespace GamePipeGame
{
	
	class GISScreen : public GamePipe::GameScreen
	{
	private:

		int CENTER_X;
		int CENTER_Y;
		int CENTER_Z;
	protected:
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		virtual bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		virtual bool OnKeyRelease(const GIS::KeyEvent &keyEvent);
		virtual bool OnMouseMove(const GIS::MouseEvent &mouseEvent);
		virtual bool OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);
		virtual bool OnMouseRelease(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);

		virtual bool OnWiiRemoteButtonPressed( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button );
		virtual bool OnWiiRemoteButtonReleased( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button );
		virtual bool OnWiiAnalogStickMoved( const GIS::WiiRemoteEvent &wrEvent ); 
		virtual bool OnWiiRemoteAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent);
		virtual bool OnWiiNunchuckAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent);
		virtual bool OnWiiIRMoved( const GIS::WiiRemoteEvent &wrEvent);

		virtual bool OnXpadButtonPressed( const GIS::XPadEvent &xpEvent, int button);
		virtual bool OnXpadButtonReleased( const GIS::XPadEvent &xpEvent, int button);
		virtual bool OnXpadLTriggerPressed( const GIS::XPadEvent &xpEvent );
		virtual bool OnXpadRTriggerPressed( const GIS::XPadEvent &xpEvent );
		virtual bool OnXpadLThumbStickMoved( const GIS::XPadEvent &xpEvent );
		virtual bool OnXpadRThumbStickMoved( const GIS::XPadEvent &xpEvent );
		
		bool gestureRecognized( const std::string gestureName);
		bool speechRecognized( const std::string wordRecognized,const float wordConfidence);
		int activeKinect;

		//for mapping animation 
		Ogre::Skeleton* skeleton;
		Ogre::Entity* character;
		Ogre::Entity* floor;
		void setTPose();

		//for recording gesture
		float globalThreshold,firstThreshold;
		float minRecoTime;
		std::string gestureName;
		bool isSkeletonSelected[NUI_SKELETON_POSITION_COUNT];
		Hikari::FlashControl *control;//Flash GUI for recording gesture
		bool record;
		float initialDelay;
		float recordTime;


		bool showCamera;
		bool showSkeleton;
		bool showGestureRecorder;

		void getParameter();
		void initGUI();
		void switchActiveKinect();
		Hikari::FlashValue OnCheckBoxClick(Hikari::FlashControl* caller, const Hikari::Arguments& args);
		Hikari::FlashValue OnButtonClick(Hikari::FlashControl* caller, const Hikari::Arguments& args);

	public:
		GISScreen(std::string name);
		~GISScreen() {}
	};
}