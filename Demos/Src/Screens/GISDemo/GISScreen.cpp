#include "StdAfx.h"
// GISScreen
#include "GISScreen.h"

// Main
#include "Main.h"
#include "input.h"



// For sprintf_s
#include <stdio.h>




GIS::JOINTS_MAP jointMap[] =
{
    { GIS::INT_ROOT,           "Root", },
    { GIS::INT_SHOULDER_LEFT,  "Humerus.R", },
    { GIS::INT_ELBOW_LEFT,     "Ulna.R", },
    { GIS::INT_WRIST_LEFT,     "Hand.R", },
	{ GIS::INT_SHOULDER_RIGHT, "Humerus.L", },
    { GIS::INT_ELBOW_RIGHT,    "Ulna.L",},
    { GIS::INT_WRIST_RIGHT,    "Hand.L", },
    { GIS::INT_HIP_LEFT,       "Thigh.R", },
    { GIS::INT_KNEE_LEFT,      "Calf.R", },
    { GIS::INT_HIP_RIGHT,      "Thigh.L", },
    { GIS::INT_KNEE_RIGHT,     "Calf.L", },
};

namespace GamePipeGame
{

	 GIS::JOINTS_MAP jointMap[] =
  {
    { GIS::INT_ROOT,           "Root", },
    { GIS::INT_SHOULDER_LEFT,  "Humerus.R", },
    { GIS::INT_ELBOW_LEFT,     "Ulna.R", },
    { GIS::INT_WRIST_LEFT,     "Hand.R", },
	{ GIS::INT_SHOULDER_RIGHT, "Humerus.L", },
    { GIS::INT_ELBOW_RIGHT,    "Ulna.L",},
    { GIS::INT_WRIST_RIGHT,    "Hand.L", },
    { GIS::INT_HIP_LEFT,       "Thigh.R", },
    { GIS::INT_KNEE_LEFT,      "Calf.R", },
    { GIS::INT_HIP_RIGHT,      "Thigh.L", },
    { GIS::INT_KNEE_RIGHT,     "Calf.L", },
  };

	//////////////////////////////////////////////////////////////////////////
	// GISScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	GISScreen::GISScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());
		
		activeKinect=0;

		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f, 1.0f));

		m_v3CameraPosition = GetActiveCameraSceneNode()->getPosition();
		m_qCameraOrientation = GetActiveCameraSceneNode()->getOrientation();

		// Text
		UtilitiesPtr->Add("TEXT", new GamePipe::DebugText("Hello!", (Ogre::Real)0.5, (Ogre::Real)0.8, (Ogre::Real)6, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
		UtilitiesPtr->Add("Hint", new GamePipe::DebugText("1: Turn On/off Camera Display\n2: Turn On/Off Skeleton Tracking\nTAB: Switch between GIS Demo \n    and Gesture Recorder (Only\n    works when connecting\n    Kinect)", (Ogre::Real)0.7, (Ogre::Real)0.3, (Ogre::Real)3, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
		record=false;

		setTPose();

		showCamera=true;
		showSkeleton=true;
		showGestureRecorder=false;//Initially show the GIS demo
		
		//Ogre::ParticleSystem::setDefaultNonVisibleUpdateTimeout(5);  // set nonvisible timeout
		//Ogre::ParticleSystem* ps;
		//ps = pSceneManager->createParticleSystem("Fireworks", "Examples/Fireworks");
		//pSceneManager->getRootSceneNode()->attachObject(ps);
		//pSceneManager->getParticleSystem("Fireworks")->setVisible(false);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::Destroy()
	{
		//DestroyDefaultPhysicsScene();
		//DestroyDefaultSceneManager();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
		initGUI();

		//display Skeleton, Video and Depth
		InputPtr->getKinect()->displaySkeleton(GetDefaultSceneManager());
		InputPtr->getKinect()->setVideoVisible(true);
		InputPtr->getKinect()->setDepthVisible(true);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::Update()
	{
		//FreeCameraMovement();
		InputPtr->getKinect()->mapSkeleton(character,skeleton,jointMap);
		if(record)
		{
			//Initial Delay Countdown
			if(initialDelay>0)
			{
				UtilitiesPtr->GetDebugText("TEXT")->SetText("Start recording in "+Ogre::StringConverter::toString(initialDelay)+" s");
				initialDelay-=EnginePtr->GetDeltaTime();
			}
			else
			{
				//Record Time Countdown
				recordTime=recordTime-EnginePtr->GetDeltaTime();
				UtilitiesPtr->GetDebugText("TEXT")->SetText("Recording Time Left: "+Ogre::StringConverter::toString(recordTime)+" s");
				if(recordTime<0)
				{
					record=false;
					UtilitiesPtr->GetDebugText("TEXT")->SetText("Recording Finished");
					InputPtr->getKinect()->stopRecordGesture();
				}
			}
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool GISScreen::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	// Forward declarations for easy string production.
	std::string XPadButtonString(int buttons);
	std::string WiiRemoteButtonString(int buttons);
	std::string AccelerometerStatusString(GIS::Vector3 vec);
	std::string IRStatusString(GIS::Vector2 lc, GIS::Vector2 rc);
	std::string AnalogStatusString( GIS::Vector2 as );
	std::string AxisString(GIS::Axis ax);

	bool GISScreen::OnKeyPress(const GIS::KeyEvent &keyEvent){
		switch(keyEvent.key)
		{
			case GIS::KC_ESCAPE:
			{
				InputPtr->getKinect()->setVideoVisible(false);
				InputPtr->getKinect()->setDepthVisible(false);
				UtilitiesPtr->Remove("TEXT");
				UtilitiesPtr->Remove("Hint");
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(control);
				Close();
				break;
			}
			case GIS::KC_1:
				{
					showCamera=!showCamera;
					InputPtr->getKinect()->setVideoVisible(showCamera);
					InputPtr->getKinect()->setDepthVisible(showCamera);
					break;
				}

			case GIS::KC_2:
			{
				showSkeleton=!showSkeleton;
				InputPtr->getKinect()->setSkeletonVisible(showSkeleton);
				break;
			}
			case GIS::KC_S:
			{
				switchActiveKinect();
				break;
			}
			case GIS::KC_TAB:
			{
				if( ( InputPtr->getKinect()->getCurrentKinect() == NULL ) || ( InputPtr->getKinect()->getCurrentKinect() == (INuiSensor*)0xcdcdcdcd ) ){
					//if Kinect doesn't connect with computer, do nothing.
				}
				else{
					//switch between GIS Demo and Gesture Recorder
					if(!showGestureRecorder)
					{
						InputPtr->getKinect()->setVideoVisible(false);
						InputPtr->getKinect()->setDepthVisible(false);
						control->show();//Show GUI for Geture Recorder
						UtilitiesPtr->GetDebugText("TEXT")->SetPosition((Ogre::Real)0.20, (Ogre::Real)0.8);
						//floor->setVisible(false);
						//character->setVisible(false);
						InputPtr->getKinect()->enableGestureReco(false);
						showGestureRecorder=!showGestureRecorder;
					}
					else
					{
						InputPtr->getKinect()->setVideoVisible(true);
						InputPtr->getKinect()->setDepthVisible(true);
						control->hide();//Hide GUI for Gesture Recorder
						UtilitiesPtr->GetDebugText("TEXT")->SetPosition((Ogre::Real)0.0, (Ogre::Real)0.8);
						//floor->setVisible(true);
						//character->setVisible(true);
						InputPtr->getKinect()->enableGestureReco(true);
						showGestureRecorder=!showGestureRecorder;
					}
				}
			}
			default:
			{
				std::string output;
				output = "Key Pressed {" + Ogre::StringConverter::toString(keyEvent.key) + ", " + ((GIS::Keyboard*)(keyEvent.device))->getAsString(keyEvent.key) + "}";
				output += "\nCharacter (";
				output += (char)keyEvent.text;
				output += ")";
				UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
				break;
			}
		}
		
		return true;
	}

	bool GISScreen::OnKeyRelease(const GIS::KeyEvent &keyEvent){
		std::string output;
		output = "Key Released {" + Ogre::StringConverter::toString(keyEvent.key) + ", " + ((GIS::Keyboard*)(keyEvent.device))->getAsString(keyEvent.key) + "}";
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		return true;
	}
	bool GISScreen::OnMouseMove(const GIS::MouseEvent &mouseEvent){
		std::string output="Mouse Moved";
		output+="\nAbs(" + Ogre::StringConverter::toString(mouseEvent.state.X.abs) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Y.abs) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Z.abs) + ")";
		output+="\nRel(" + Ogre::StringConverter::toString(mouseEvent.state.X.rel) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Y.rel) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Z.abs) + ")";
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		return true;
	}
	bool GISScreen::OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId){
		std::string output = "Mouse Pressed: ";

		switch(mouseButtonId)
		{
		case GIS::MB_Left:
			output+="Left";
			break;

		case GIS::MB_Right:
			output+="Right";
			break;
		case GIS::MB_Middle:
			output+="Center";
			break;
		default:
			output+="Non-Standard";
			break;
		}
		output+="\nAbs(" + Ogre::StringConverter::toString(mouseEvent.state.X.abs) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Y.abs) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Z.abs) + ")";
		output+="\nRel(" + Ogre::StringConverter::toString(mouseEvent.state.X.rel) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Y.rel) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Z.abs) + ")";
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		return true;
	}

	bool GISScreen::OnMouseRelease(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId){
		std::string output = "Mouse Released: ";
		switch(mouseButtonId)
		{
		case GIS::MB_Left:
			output+="Left";
			break;
		case GIS::MB_Right:
			output+="Right";
			break;
		case GIS::MB_Middle:
			output+="Center";
			break;
		default:
			output+="Non-Standard";
			break;
		}
		output+="\nAbs(" + Ogre::StringConverter::toString(mouseEvent.state.X.abs) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Y.abs) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Z.abs) + ")";
		output+="\nRel(" + Ogre::StringConverter::toString(mouseEvent.state.X.rel) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Y.rel) + ", " + Ogre::StringConverter::toString(mouseEvent.state.Z.abs) + ")";
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		return true;
	}

	bool GISScreen::OnWiiRemoteButtonPressed( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button ){
		UtilitiesPtr->GetDebugText("TEXT")->SetText("WR Button Press! Current Buttons:" + WiiRemoteButtonString(wrEvent.state.buttons));
		return true;
	}
	bool GISScreen::OnWiiRemoteButtonReleased( const GIS::WiiRemoteEvent &wrEvent, GIS::WiiButtonID button ){
		UtilitiesPtr->GetDebugText("TEXT")->SetText("WR Button Release! Current Buttons:" + WiiRemoteButtonString(wrEvent.state.buttons));
		return true;
	}
	bool GISScreen::OnWiiAnalogStickMoved( const GIS::WiiRemoteEvent &wrEvent ){
		if (wrEvent.state.wrButtonsDown(GIS::WR_MINUS)){
			UtilitiesPtr->GetDebugText("TEXT")->SetText("WR Analog Stick moved!");
		}
		return true;
	} 
	bool GISScreen::OnWiiRemoteAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent){
		if (wrEvent.state.wrButtonsDown(GIS::WR_A)){
			UtilitiesPtr->GetDebugText("TEXT")->SetText("WR Accelerometers: " + AccelerometerStatusString(wrEvent.state.remote));
		}
		return true;
	}
	bool GISScreen::OnWiiNunchuckAccelerometersMoved( const GIS::WiiRemoteEvent &wrEvent){
		if (wrEvent.state.wrButtonsDown(GIS::WR_PLUS)){
			UtilitiesPtr->GetDebugText("TEXT")->SetText("WN Accelerometers: " + AccelerometerStatusString(wrEvent.state.nunchuck));
		}
		return true;
	}
	bool GISScreen::OnWiiIRMoved( const GIS::WiiRemoteEvent &wrEvent){
		if (wrEvent.state.wrButtonsDown(GIS::WR_B)){
			UtilitiesPtr->GetDebugText("TEXT")->SetText("Wii IR moved!");
		}
		return true;
	}
	bool GISScreen::OnXpadButtonPressed( const GIS::XPadEvent &xpEvent, int button){
		UtilitiesPtr->GetDebugText("TEXT")->SetText("XPad Button Press! Current Buttons:" + XPadButtonString(xpEvent.state.buttons));
		return true;
	}
	bool GISScreen::OnXpadButtonReleased( const GIS::XPadEvent &xpEvent, int button){
		UtilitiesPtr->GetDebugText("TEXT")->SetText("XPad Button Release! Current Buttons:" + XPadButtonString(xpEvent.state.buttons));
		return true;
	}
	bool GISScreen::OnXpadLTriggerPressed( const GIS::XPadEvent &xpEvent ){
		//if (xpEvent.state.xpButtonsDown(GIS::XP_X)){
		std::string output="Xpad Left Trigger Pressed: ";
		output+=Ogre::StringConverter::toString(xpEvent.state.XP_lTrigger);
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);                                                                                                                               
		//}
		return true;
	}
	bool GISScreen::OnXpadRTriggerPressed( const GIS::XPadEvent &xpEvent ){
		//if (xpEvent.state.xpButtonsDown(GIS::XP_B)){
		std::string output="Xpad Right Trigger Pressed: ";
		output+=Ogre::StringConverter::toString(xpEvent.state.XP_rTrigger);
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		//}
		return true;
	}
	bool GISScreen::OnXpadLThumbStickMoved( const GIS::XPadEvent &xpEvent ){
		if (xpEvent.state.xpButtonsDown(GIS::XP_LSHOULDER)){
			std::string output="Xpad Left Stick Moved";
			output+="\nPosition(" + Ogre::StringConverter::toString(xpEvent.state.XP_lThumbX) + ", " + Ogre::StringConverter::toString(xpEvent.state.XP_lThumbY) + ")";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		return true;
	}
	bool GISScreen::OnXpadRThumbStickMoved( const GIS::XPadEvent &xpEvent ){
		if (xpEvent.state.xpButtonsDown(GIS::XP_RSHOULDER)){
			std::string output="Xpad Right Stick Moved";
			output+="\nPosition(" + Ogre::StringConverter::toString(xpEvent.state.XP_rThumbX) + ", " + Ogre::StringConverter::toString(xpEvent.state.XP_rThumbY) + ")";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		return true;
	}
	

	std::string XPadButtonString(int buttons){
		int bID;
		std::string retVal = "";
		bID = GIS::XP_UP;
		if ((buttons & bID) == bID) retVal+=" U";
		bID = GIS::XP_DOWN;
		if ((buttons & bID) == bID) retVal+=" D";
		bID = GIS::XP_LEFT;
		if ((buttons & bID) == bID) retVal+=" L";
		bID = GIS::XP_RIGHT;
		if ((buttons & bID) == bID) retVal+=" R";
		bID = GIS::XP_START;
		if ((buttons & bID) == bID) retVal+=" START";
		bID = GIS::XP_BACK;
		if ((buttons & bID) == bID) retVal+=" BACK";
		bID = GIS::XP_LTHUMB;
		if ((buttons & bID) == bID) retVal+=" LTH";
		bID = GIS::XP_RTHUMB;
		if ((buttons & bID) == bID) retVal+=" RTH";
		bID = GIS::XP_LSHOULDER;
		if ((buttons & bID) == bID) retVal+=" LSH";
		bID = GIS::XP_RSHOULDER;
		if ((buttons & bID) == bID) retVal+=" RSH";
		bID = GIS::XP_A;
		if ((buttons & bID) == bID) retVal+=" A";
		bID = GIS::XP_B;
		if ((buttons & bID) == bID) retVal+=" B";
		bID = GIS::XP_X;
		if ((buttons & bID) == bID) retVal+=" X";
		bID = GIS::XP_Y;
		if ((buttons & bID) == bID) retVal+=" Y";
		return retVal;
	}

	std::string WiiRemoteButtonString(int buttons){
		int bID;
		std::string retVal = "";
		bID = GIS::WR_A;
		if ((buttons & bID) == bID) retVal+=" A";
		bID = GIS::WR_B;
		if ((buttons & bID) == bID) retVal+=" B";
		bID = GIS::WR_ONE;
		if ((buttons & bID) == bID) retVal+=" ONE";
		bID = GIS::WR_TWO;
		if ((buttons & bID) == bID) retVal+=" TWO";
		bID = GIS::WR_PLUS;
		if ((buttons & bID) == bID) retVal+=" +";
		bID = GIS::WR_MINUS;
		if ((buttons & bID) == bID) retVal+=" -";
		bID = GIS::WR_HOME;
		if ((buttons & bID) == bID) retVal+=" HOME";
		bID = GIS::WR_LEFT;
		if ((buttons & bID) == bID) retVal+=" L";
		bID = GIS::WR_RIGHT;
		if ((buttons & bID) == bID) retVal+=" R";
		bID = GIS::WR_UP;
		if ((buttons & bID) == bID) retVal+=" U";
		bID = GIS::WR_DOWN;
		if ((buttons & bID) == bID) retVal+=" D";
		bID = GIS::NC_C;
		if ((buttons & bID) == bID) retVal+=" C";
		bID = GIS::NC_Z;
		if ((buttons & bID) == bID) retVal+=" Z";	
		return retVal;
	}

	std::string AccelerometerStatusString(GIS::Vector3 vec){
		char output[100];
		sprintf_s(output, "X=%f,Y=%f,Z=%f", vec.x, vec.y, vec.z);
		return output;
	}

	std::string IRStatusString(GIS::Vector2 lc, GIS::Vector2 rc){
		char output[100];
		sprintf_s(output, "LeftIR: (%f,%f)\nRightIR: (%f,%f)", lc.x, lc.y, rc.x, rc.y);
		return output;
	}

	std::string AnalogStatusString( GIS::Vector2 as ){
		char output[100];
		sprintf_s(output, "Analog Stick: (%f, %f)", as.x, as.y);
		return output;
	}

	std::string AxisString( GIS::Axis ax ){
		char output[100];
		sprintf_s(output, "Axis (abs,rel): (%i, %i)", ax.abs, ax.rel);
		return output;
	}

	bool GISScreen::gestureRecognized( const std::string gestureName)
	{
		//printf_s("Hello\n");
		std::string output="Gesture Recognized: ";
		output+=gestureName;
		UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		if(gestureName.compare("RightWave")==0)
		{
			//GetDefaultSceneManager()->getParticleSystem("Fireworks")->setVisible(true);
		}
		if(gestureName.compare("LeftWave")==0)
		{
			//GetDefaultSceneManager()->getParticleSystem("Fireworks")->setVisible(false);
		}
		return true;
	}

	bool GISScreen::speechRecognized( const std::string wordRecognized, const float wordConfidence)
	{
		if(wordRecognized.compare("Next")==0)
		{
			std::string output="Speech Recognized: ";
			output+="Next";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Previous")==0)
		{
			std::string output="Speech Recognized: ";
			output+="Previous";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Switch")==0)
		{
			std::string output="Speech Recognized: ";
			output+="Switch";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
			switchActiveKinect();
		}
		if(wordRecognized.compare("Near")==0)
		{
			std::string output="Speech Recognized: ";
			output+="Near";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
			InputPtr->getKinect()->toggleNear();
		}
		if(wordRecognized.compare("Exit")==0||wordRecognized.compare("Back")==0||wordRecognized.compare("Close")==0)
		{
			InputPtr->getKinect()->setVideoVisible(false);
			InputPtr->getKinect()->setDepthVisible(false);
			UtilitiesPtr->Remove("TEXT");
			UtilitiesPtr->Remove("Hint");
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(control);
			Close();
		}
		if(wordRecognized.compare("One")==0)
		{
			std::string output="Speech Recognized: ";
			output+="1";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Two")==0)
		{
			std::string output="Speech Recognized: ";
			output+="2";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Three")==0)
		{
			std::string output="Speech Recognized: ";
			output+="3";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Four")==0)
		{
			std::string output="Speech Recognized: ";
			output+="4";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Five")==0)
		{
			std::string output="Speech Recognized: ";
			output+="5";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Six")==0)
		{
			std::string output="Speech Recognized: ";
			output+="6";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Seven")==0)
		{
			std::string output="Speech Recognized: ";
			output+="7";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Eight")==0)
		{
			std::string output="Speech Recognized: ";
			output+="8";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Nine")==0)
		{
			std::string output="Speech Recognized: ";
			output+="9";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		if(wordRecognized.compare("Ten")==0)
		{
			std::string output="Speech Recognized: ";
			output+="10";
			UtilitiesPtr->GetDebugText("TEXT")->SetText(output);
		}
		return true;
	}

	void GISScreen::getParameter()
	{
		Hikari::FlashValue fv=control->callFunction("getGestureNameInput");
		gestureName=fv.getString();
		fv=control->callFunction("getGlobalInput");
		globalThreshold=fv.getNumber();
		fv=control->callFunction("getFirstInput");
		firstThreshold=fv.getNumber();
		fv=control->callFunction("getInitialDelayInput");
		initialDelay=fv.getNumber();
		fv=control->callFunction("getRecordTimeInput");
		recordTime=fv.getNumber();
		fv=control->callFunction("getMinTimeInput");
		minRecoTime=fv.getNumber();
	}

	void GISScreen::initGUI()
	{

		control=FlashGUIPtr->FlashGUIManager->createFlashOverlay("SkeletonRecorderUI", GetViewport(), (int)400, (int)600, Position(0,0));
		control->load("SkeletonRecorderUI.swf");
		control->setTransparent(true, true);
		control->bind("checkboxChanged", FlashDelegate(this, &GISScreen::OnCheckBoxClick));
		control->bind("buttonClick", FlashDelegate(this, &GISScreen::OnButtonClick));
		control->hide();


		for(int i=0;i<NUI_SKELETON_POSITION_COUNT;i++)
		{
			isSkeletonSelected[i]=false;
		}
	}

	void GISScreen::switchActiveKinect()
	{
		for(int i = 0; i < 3; i++)
		{
			activeKinect = (activeKinect + 1) % 4;
			if(InputPtr->getKinect()->switchKinects(activeKinect+1))
			{
				InputPtr->getKinect()->initializeSkeleton(GetDefaultSceneManager(),GamePipe::Engine::GetInstancePointer()->gameFolder);
				InputPtr->getKinect()->initializeDepth(0.0f,0.0f,0.3f,0.3f);
				InputPtr->getKinect()->initializeVideo(0.0f,0.3f,0.3f,0.3f);
				InputPtr->getKinect()->displaySkeleton(GetDefaultSceneManager());
				InputPtr->getKinect()->setVideoVisible(true);
				InputPtr->getKinect()->setDepthVisible(true);
				return;
			}else{
				printf("Kinect %d not available\n", activeKinect+1);
			}
		}
		//if not switched roll back around
		activeKinect = (activeKinect + 1) % 4;
	}

	FlashValue GISScreen::OnCheckBoxClick(FlashControl* caller, const Arguments& args)
	{
		Ogre::String sn = (Ogre::DisplayString)(args.at(0).getString());
		std::vector<std::string> SkeletonName=InputPtr->getKinect()->getSkeletonName();
		int i,size=SkeletonName.size();
		for(i=0;i<size;i++)
		{
			if(sn == SkeletonName.at(i))
				break;
		}
		isSkeletonSelected[i]=!isSkeletonSelected[i];
		return FLASH_VOID;
	}

	FlashValue GISScreen::OnButtonClick(FlashControl* caller, const Arguments& args)
	{
		getParameter();

		if(( InputPtr->getKinect()->getCurrentKinect() == NULL ) || ( InputPtr->getKinect()->getCurrentKinect() == (INuiSensor*)0xcdcdcdcd )){
			//UtilitiesPtr->GetDebugText("TEXT")->SetText("Please connect Kinect to Computer for recording gesture.");
		}
		else{
			record=true;
			InputPtr->getKinect()->startRecordGesture(gestureName, globalThreshold, firstThreshold,minRecoTime ,isSkeletonSelected);
			//UtilitiesPtr->GetDebugText("TEXT")->SetText("Start recording in"+Ogre::StringConverter::toString(initialDelay)+" s");
		}
		return FLASH_VOID;
	}


	//Initialize character and set it to T-pose
	void GISScreen::setTPose()
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		Ogre::MeshManager::getSingleton().createPlane("floor", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		 Ogre::Plane(Ogre::Vector3::UNIT_Y, -52), 500, 250, 25, 25, true, 1, 15, 15, Ogre::Vector3::UNIT_Z);

		// add a floor to our scene using the floor mesh we created
		floor = pSceneManager->createEntity("Floor", "floor");
		floor->setMaterialName("Examples/Rockwall");
		floor->setCastShadows(false);
		pSceneManager->getRootSceneNode()->attachObject(floor);

		character=pSceneManager->createEntity("character","Sinbad.mesh");
		
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(character);
		pSceneManager->getEntity("character")->getParentNode()->setScale(10,10,10);
		skeleton=character->getSkeleton();
		int boneCount=skeleton->getNumBones();
		for(int i = 0; i < boneCount; i++)
		{
			skeleton->getBone(i)->setManuallyControlled(true);
			
		}


		Ogre::SkeletonAnimationBlendMode mode=character->getSkeleton()->getBlendMode();
		Ogre::Quaternion q = Ogre::Quaternion::IDENTITY;
		Ogre::Quaternion q2,q3; 
		Ogre::Vector3 xAxis,yAxis,zAxis;
		q.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,0,-1));
		q.ToAxes(xAxis,yAxis,zAxis);
		q2.FromAngleAxis(Ogre::Degree(90),xAxis);
		InputPtr->getKinect()->setupBone(character,"Humerus.L",q*q2);

		q.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,0,1));
		q.ToAxes(xAxis,yAxis,zAxis);
		q2.FromAngleAxis(Ogre::Degree(90),xAxis);
		InputPtr->getKinect()->setupBone(character,"Humerus.R",q*q2);
		
		q.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,0,-1));	 
		q2.FromAngleAxis(Ogre::Degree(45),Ogre::Vector3(0,-1,0));
		InputPtr->getKinect()->setupBone(character,"Ulna.L",q*q2);

		q.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,0,1));	
		InputPtr->getKinect()->setupBone(character,"Ulna.R",q*q2.Inverse());
		
		q.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,0,-1));
		q2.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,-1,0));
		InputPtr->getKinect()->setupBone(character,"Hand.L",q*q2);

		q.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,0,1));
		q2.FromAngleAxis(Ogre::Degree(90),Ogre::Vector3(0,-1,0));
		InputPtr->getKinect()->setupBone(character,"Hand.R",q*q2.Inverse());
		
		q.FromAngleAxis(Ogre::Degree(180),Ogre::Vector3(0,1,0));
		InputPtr->getKinect()->setupBone(character,"Chest",q);
		InputPtr->getKinect()->setupBone(character,"Stomach",q);
		q.FromAngleAxis(Ogre::Degree(180),Ogre::Vector3(1,0,0));	 	
		q2.FromAngleAxis(Ogre::Degree(180),Ogre::Vector3(0,1,0));
		InputPtr->getKinect()->setupBone(character,"Thigh.L",q*q2);
		InputPtr->getKinect()->setupBone(character,"Thigh.R",q*q2);
		InputPtr->getKinect()->setupBone(character,"Calf.L",q*q2);
		InputPtr->getKinect()->setupBone(character,"Calf.R",q*q2);
		q.FromAngleAxis(Ogre::Degree(120),Ogre::Vector3(1,0,0));	 	
		q2.FromAngleAxis(Ogre::Degree(180),Ogre::Vector3(0,1,0));
		InputPtr->getKinect()->setupBone(character,"Foot.L",q*q2);
		InputPtr->getKinect()->setupBone(character,"Foot.R",q*q2);
		InputPtr->getKinect()->setupBone(character,"Root",Ogre::Degree(0),Ogre::Degree(0),Ogre::Degree(0));
	}
}