#pragma once

// GameScreen
#include "GameScreen.h"

#ifdef PHYSX
#include "PhysXObject.h"//added
#endif

namespace GamePipeGame
{

	class PhysXDemo : public GamePipe::GameScreen
	{
#ifdef PHYSX
			
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		//bool OnKeyRelease(const GIS::KeyEvent &keyEvent);

		//bool OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);

		PhysXDemo(std::string name);
		~PhysXDemo() {}
#endif
	};
}