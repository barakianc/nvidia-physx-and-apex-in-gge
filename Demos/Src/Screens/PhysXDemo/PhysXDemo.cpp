#include "StdAfx.h"
// PhysXDemo
#include "PhysXDemo.h"
#include "PxCloth.h"



#ifdef PHYSX
#ifdef PHYSX_APEX
#include "destructible/public/NxDestructibleAsset.h"
#include "destructible/public/NxDestructibleActor.h"
#include "destructible/public/NxModuleDestructible.h"
#include "clothing/public/NxClothingAsset.h"
#include "clothing/public/NxClothingActor.h"
#include "clothing/public/NxModuleClothing.h"
#include "NxApex.h"
#include "NxParameterized.h"
#include <NxParamUtils.h>
#include "GGEApexRenderer.h"
#endif
using namespace std;

void setupHouse() {
	
	/*GameObject* house1 = new GameObject("house1", "stove.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	house1->scale(4.0f, 4.0f, 4.0f);
	house1->setPosition(40.0f, 1.0f, 0.0f);

	GameObject* house2 = new GameObject("house2", "Refrigerator.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	house2->scale(4.0f, 4.0f, 4.0f);
	house2->setPosition(50.0f, 1.0f, 0.0f);*/
}

void setupUnbreakFixedJoint() {
	
	physx::PxPhysics* mSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	physx::PxScene* mScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();

	GameObject* base1 = new GameObject("fix", "brick.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	base1->scale(2.0f,10.0f,0.4f);
	base1->setPosition(20.0f, 10.0f, 20.0f);

	GameObject* base2 = new GameObject("fix0", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	base2->scale(2.0f,10.0f,0.4f);
	base2->setPosition(22.04f, 10.0f, 20.0f);

	physx::PxFixedJoint *joint = physx::PxFixedJointCreate(*mSDK,
		base1->m_pPhysicsObject->getStaticRigidBody(), physx::PxTransform(physx::PxVec3(1.0f,0,0)),
		base2->m_pPhysicsObject->getDynamicRigidBody(), physx::PxTransform(physx::PxVec3(-1.0f,0,0)));
}


void setupFixedJoint() {
	
	physx::PxPhysics* mSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	physx::PxScene* mScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();

	GameObject* base1 = new GameObject("fix1", "brick.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	base1->scale(2.0f,10.0f,0.4f);
	base1->setPosition(30.0f, 10.0f, 20.0f);

	GameObject* base2 = new GameObject("fix2", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	base2->scale(2.0f,10.0f,0.4f);
	base2->setPosition(32.04f, 10.0f, 20.0f);

	physx::PxFixedJoint *joint = physx::PxFixedJointCreate(*mSDK,
		base1->m_pPhysicsObject->getStaticRigidBody(), physx::PxTransform(physx::PxVec3(1.0f,0,0)),
		base2->m_pPhysicsObject->getDynamicRigidBody(), physx::PxTransform(physx::PxVec3(-1.0f,0,0)));

	joint->setBreakForce(100.0f, 100.0f);
	joint->setConstraintFlag(physx::PxConstraintFlag::eBROKEN, true);
}


void setupRevJoint() {
	
	physx::PxPhysics* mSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	physx::PxScene* mScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();

	GameObject* base1 = new GameObject("fix3", "brick.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	base1->scale(2.0f,10.0f,0.4f);
	base1->setPosition(40.0f, 10.0f, 20.0f);

	GameObject* base2 = new GameObject("fix4", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	base2->scale(2.0f,10.0f,0.4f);
	base2->setPosition(42.04f, 10.0f, 20.0f);

	physx::PxRevoluteJoint *joint = physx::PxRevoluteJointCreate(*mSDK,
		base1->m_pPhysicsObject->getStaticRigidBody(), physx::PxTransform(physx::PxVec3(1.0f,0,0),physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0,0,1))),
		base2->m_pPhysicsObject->getDynamicRigidBody(), physx::PxTransform(physx::PxVec3(-1.0f,0,0),physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0,0,1))));

	//joint->setBreakForce(100.0f, 100.0f);
	//joint->setConstraintFlag(physx::PxConstraintFlag::eBROKEN, true);
}


void setupLimitRevJoint() {
	
	physx::PxPhysics* mSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	physx::PxScene* mScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();

	GameObject* base1 = new GameObject("fix5", "brick.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	base1->scale(2.0f,10.0f,0.4f);
	base1->setPosition(50.0f, 10.0f, 20.0f);

	GameObject* base2 = new GameObject("fix6", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	base2->scale(2.0f,10.0f,0.4f);
	base2->setPosition(52.04f, 10.0f, 20.0f);

	physx::PxRevoluteJoint *joint = physx::PxRevoluteJointCreate(*mSDK,
		base1->m_pPhysicsObject->getStaticRigidBody(), physx::PxTransform(physx::PxVec3(1.0f,0,0),physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0,0,1))),
		base2->m_pPhysicsObject->getDynamicRigidBody(), physx::PxTransform(physx::PxVec3(-1.0f,0,0),physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0,0,1))));

	joint->setLimit(physx::PxJointLimitPair(-physx::PxPi/4, physx::PxPi/4, 0.1f)); // upper, lower, tolerance
	joint->setRevoluteJointFlag(physx::PxRevoluteJointFlag::eLIMIT_ENABLED, true);
}


void setupSphJoint() {
	
	physx::PxPhysics* mSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
	physx::PxScene* mScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();

	GameObject* base1 = new GameObject("fix7", "brick.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	base1->scale(2.0f,10.0f,0.4f);
	base1->setPosition(60.0f, 10.0f, 20.0f);

	GameObject* base2 = new GameObject("fix8", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	base2->scale(2.0f,10.0f,0.4f);
	base2->setPosition(62.04f, 10.0f, 20.0f);

	physx::PxSphericalJoint *joint = physx::PxSphericalJointCreate(*mSDK,
		base1->m_pPhysicsObject->getStaticRigidBody(), physx::PxTransform(physx::PxVec3(1.0f,0,0),physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0,0,1))),
		base2->m_pPhysicsObject->getDynamicRigidBody(), physx::PxTransform(physx::PxVec3(-1.0f,0,0),physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0,0,1))));

	//joint->setLimit(physx::PxJointLimitPair(-physx::PxPi/4, physx::PxPi/4, 0.1f)); // upper, lower, tolerance
	//joint->setRevoluteJointFlag(physx::PxRevoluteJointFlag::eLIMIT_ENABLED, true);
}


void setupSleepBox() {
	

	GameObject* box = new GameObject("box3", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	box->scale(2.0,2.0,2.0);
	box->setPosition(80.0f, 30.0f, 0.0f);
	//box->m_pPhysicsObject->getDynamicRigidBody()->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
	//box->m_pPhysicsObject->getDynamicRigidBody()->setRigidDynamicFlag(physx::PxRigidDynamicFlag::eKINEMATIC, true);
	box->m_pPhysicsObject->getDynamicRigidBody()->putToSleep();
}

void setBoxes() {
	GameObject* box1 = new GameObject("box1", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	box1->scale(2.0f, 4.0f, 2.0f);
	box1->setPosition(60.0f, 2.0f, 0.0f);
	box1->m_pPhysicsObject->getDynamicRigidBody()->setMass(box1->m_pPhysicsObject->getDynamicRigidBody()->getMass()*20);

	GameObject* box2 = new GameObject("box2", "brick.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
	box2->scale(2.0f, 4.0f, 2.0f);
	box2->setPosition(70.0f, 2.0f, 0.0f);

	box2->m_pPhysicsObject->getDynamicRigidBody()->setMass(box2->m_pPhysicsObject->getDynamicRigidBody()->getMass()/40);
}


void setupSteps() {
	for(int j=0; j<10; j++){
		char buffer[30];
		itoa(2000+j, buffer, 10);
		GameObject* brick = new GameObject(buffer, "step.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
		brick->scale(2.0f, 2.0f, 2.0f);
		brick->setPosition(0.0f, 1.0f+2*j, 0.0f+j*4);
	}
	for(int j=0; j<20; j++){
		char buffer[30];
		itoa(2500+j, buffer, 10);
		GameObject* brick = new GameObject(buffer, "step.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
		brick->setPosition(10.0f, 0.5f+j, 0.0f+j*2);
	}

	GameObject* flat = new GameObject("f1", "step.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	flat->scale(2.0f, 0.1f, 40.0f);
	flat->m_pPhysicsObject->getStaticRigidBody()->setGlobalPose(physx::PxTransform(physx::PxVec3(-10.0f,0.0f,0),physx::PxQuat(physx::PxHalfPi/2, physx::PxVec3(-1,0,0))));

	GameObject* flat2 = new GameObject("f2", "step.mesh", "", PHYSICS_FIXED, COLLISION_SHAPE_BOX, "", false);
	flat2->scale(2.0f, 0.1f, 40.0f);
	flat2->m_pPhysicsObject->getStaticRigidBody()->setGlobalPose(physx::PxTransform(physx::PxVec3(-20.0f,0.0f,0),physx::PxQuat(physx::PxHalfPi/3, physx::PxVec3(-1,0,0))));

}

namespace GamePipeGame
{
	int boxNum = 0;
	bool bStartTrigger = false;
	bool bAfterTrigger = false;
	int nTriggerNum = 0;

	GameObject* Character;
	GameObject* Character1;
	GameObject* Character2;
	GameObject* Character3;
	GameObject* box3;
	bool walking=false;
	
	Ogre::AnimationState *goblinwalk;
	Ogre::AnimationState *goblinwalk1;
	Ogre::AnimationState *goblinwalk2;
	Ogre::AnimationState *goblinwalk3;

#ifdef PHYSX_APEX
	physx::apex::NxDestructibleActor* destructibleActor;
	physx::apex::NxClothingActor* clothingActor;
#endif


	void setTrigger()
	{
		GameObject* trigger = new GameObject("triggerBox", "triggerBox.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_BOX, "", false);
		trigger->scale(2.0f, 2.0f, 2.0f);
		trigger->setPosition(90.0f, 2.0f, 0.0f);

		physx::PxShape* treasureShape;
		trigger->m_pPhysicsObject->getDynamicRigidBody()->getShapes(&treasureShape, 1);
		treasureShape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		trigger->m_pPhysicsObject->getDynamicRigidBody()->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);

		EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->setTrigger(false);
		bStartTrigger = true;
	}

	PhysXDemo::PhysXDemo(std::string name) :GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool PhysXDemo::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
#ifdef PHYSX_APEX
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("apex");
#endif
		return true;
	}
	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool PhysXDemo::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
#ifdef PHYSX_APEX
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("apex");
#endif
		return true;
	}

	bool PhysXDemo::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;


		Ogre::Light* directionalLight = GetDefaultSceneManager()->createLight("directionalLight");
		directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
		directionalLight->setDiffuseColour(Ogre::ColourValue(.5, .5, .5));
		directionalLight->setSpecularColour(Ogre::ColourValue(.25, .25, 0));
	 
		directionalLight->setDirection(Ogre::Vector3( 0, -1, -1 )); 

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000f, 0.5000f, 0.5000f, 0.5000f));


		Ogre::Light* light = pSceneManager->createLight("light10");
		light->setType(Ogre::Light::LT_POINT);
		light->setPosition(Ogre::Vector3(0, 150, 250));
		light->setDiffuseColour(1.0, 1.0, 1.0);
		light->setSpecularColour(1.0, 1.0, 1.0);

		Ogre::Light* light2 = pSceneManager->createLight("light11");
		light2->setType(Ogre::Light::LT_POINT);
		light2->setPosition(Ogre::Vector3(0, 150, -250));
		light2->setDiffuseColour(1.0, 1.0, 1.0);
		light2->setSpecularColour(1.0, 1.0, 1.0);
		
		Ogre::Vector3 camPos(0,20,-60);
		m_v3CameraPosition = camPos;
		Ogre::Quaternion camQuat(0.0f, 0.0f, 1.0f, 0.0f);
		m_qCameraOrientation = camQuat;

		physx::PxReal d = 0.0f;
		physx::PxU32 axis = 1;
		physx::PxTransform pose;
		
		LoadScene("PhysXDemoScreen.scene");

		///////////////////////////

		if(axis == 0)
				pose = physx::PxTransform(physx::PxVec3(d, 0.0f, 0.0f));
		else if(axis == 1)
				pose = physx::PxTransform(physx::PxVec3(0.0f, d, 0.0f), physx::PxQuat(physx::PxHalfPi, physx::PxVec3(0.0f, 0.0f, 1.0f)));
		else if(axis == 2)        
				pose = physx::PxTransform(physx::PxVec3(0.0f, 0.0f, d), physx::PxQuat(-physx::PxHalfPi, physx::PxVec3(0.0f, 1.0f, 0.0f)));
		///////////////////////////
		

		physx::PxPhysics* mSDK = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXWorld();
		physx::PxScene* mScene = EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->GetPhysXScene();

		physx::PxMaterial* mMaterial = mSDK->createMaterial(0.5f, 0.5f, 0.1f);

		physx::PxRigidStatic* plane = mSDK->createRigidStatic(pose);
		
		if (!plane)
			printf("create plane failed!");
		physx::PxShape* shape = plane->createShape(physx::PxPlaneGeometry(), *mMaterial);
		if (!shape)
			printf("create shape failed!");
		mScene->addActor(*plane);

		physx::PxVec3 dimensionsfloor(100.0f, 20.0f, 100.0f);
		physx::PxBoxGeometry geometryfloor(dimensionsfloor);
		physx::PxTransform transformfloor(physx::PxVec3(0.0f, -20.0f, 0.0f), physx::PxQuat::createIdentity());
		physx::PxRigidStatic* floor = PxCreateStatic(*mSDK, transformfloor, geometryfloor, *mMaterial);

		mScene->addActor(*floor);

		  //---------------------------------------Command on Screen-----------------------------------//
               Ogre::String text;

               Ogre::String s_position;
               Ogre::stringstream out1;
               out1 << GetActiveCameraSceneNode()->getPosition();
            
               text = "NVIDIA PhysX:\n";
			   text = text + "    Demo the static object, dynamic object, keyframe object(sleeping).\n";
			   text = text + "         character control.\n";
			   text = text + "         breakable joint, fixed joint, revolute joint, spherical joint, limited revolute joint.\n";
			   text = text + "         trigger.\n";
			   text = text + "    B -> Create objects in the scene.\n";
			   text = text + "    SPACE -> Shoot balls.\n";
               text = text + "    1 -> Control the first character to move.\n";
               text = text + "    2 -> Control the second character to move.\n";
               text = text + "    3 -> Control the third character to move.\n";
               text = text + "    4 -> Control the fourth character to move.\n";
			   text = text + "    6 -> Change the second character's offstep attribute.\n";

               UtilitiesPtr->Add("Commands", new GamePipe::DebugText(text, (Ogre::Real)0.05, (Ogre::Real)0.1, (Ogre::Real)3, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
               //---------------------------------------Command on Screen-----------------------------------//

#ifdef PHYSX_APEX
			   
			   fstream filetest;
			   filetest.open("test.txt");
			   physx::PxFileBuf* stream = GetGameObjectManager()->GetApexSDK()->createStream("Bunny.apb",physx::PxFileBuf::OPEN_READ_ONLY );
			   physx::PxU32 openMode = stream->getOpenMode();
			   if(filetest.is_open()){
				   int test = 0;
			   }
			   char peekData[32];
			   stream->peek(peekData, 32);
			   NxParameterized::Serializer::SerializeType iSerType = GetGameObjectManager()->GetApexSDK()->getSerializeType(peekData, 32);
			   NxParameterized::Serializer* ser = GetGameObjectManager()->GetApexSDK()->createSerializer(iSerType);
			   NxParameterized::Serializer::DeserializedData data;
			   NxParameterized::Serializer::ErrorType serError = ser->deserialize(*stream, data);
			   NxParameterized::Interface *params = data[0];
			   physx::apex::NxApexAsset* asset = GetGameObjectManager()->GetApexSDK()->createAsset( params, "wall1" );
			   physx::apex::NxDestructibleAsset* destructibleAsset = static_cast<physx::apex::NxDestructibleAsset*>( asset );
			   //now create the destructible actor
			   NxParameterized::Interface* descParams = destructibleAsset->getDefaultActorDesc();
			   physx::PxMat44 pose1;
			   pose1 = physx::PxMat44::createIdentity();
			   pose1.setPosition(*(new physx::PxVec3(10,0,10)));
			   pose1.rotate(*(new physx::PxVec3(0,90,0)));
			   //pose1.setPosition(*(new physx::PxVec3(1,1,1)));

			   

			   //physx::apex::NxApexActor * actor = asset->create(*descParams,*(GetGameObjectManager()->GetApexScene()));
			   NxParameterized::setParamMat44(*descParams,"globalPose",pose1);
			   destructibleActor = destructibleAsset->createDestructibleActor(descParams,*(GetGameObjectManager()->GetApexScene()));//static_cast< physx::apex::NxDestructibleActor*>( actor );

			   GetGameObjectManager()->AddApexActor(destructibleActor,"lambert2",0);
			   //destructibleActor->setInitialGlobalPose(pose1);

			 //create a cloth object
			   physx::apex::NxApexAsset* clothasset = NULL;
			   NxParameterized::Serializer::DeserializedData deserData;

			   physx::PxFileBuf* clothBuff = GetGameObjectManager()->GetApexSDK()->createStream("ctdm_trenchcoat_high.apb",physx::PxFileBuf::OPEN_READ_ONLY);

			   if(clothBuff != NULL){
				   if(clothBuff->isOpen()){
						char peekDataloth[32];
						clothBuff->peek(peekDataloth,32);

						NxParameterized::Serializer::SerializeType serType = GetGameObjectManager()->GetApexSDK()->getSerializeType(peekDataloth,32);
						NxParameterized::Serializer* clothserializer = GetGameObjectManager()->GetApexSDK()->createSerializer(serType);

						if(clothserializer != NULL){
							NxParameterized::Serializer::ErrorType clothserError = NxParameterized::Serializer::ERROR_NONE;
							clothserError = clothserializer->deserialize(*clothBuff,deserData);

							if(clothserError != NxParameterized::Serializer::ERROR_NONE){
								int test = 0;
							}

							clothserializer->release();
						}

				   }
				   clothBuff->release();
			   }


			   NxParameterized::Interface* clothdata = deserData[0];
			   clothasset = GetGameObjectManager()->GetApexSDK()->createAsset(clothdata,"cloth Asset");

			   NxParameterized::Interface* clothdesc = clothasset->getDefaultActorDesc();
			   PX_ASSERT(clothdesc != NULL);

			   physx::PxMat44 pose2;
			   pose2 = physx::PxMat44::createZero();
			   pose2.rotate(*(new physx::PxVec3(0,0,90)));
			   //NxParameterized::Handle clothactorHandler(*clothdesc);
			   //clothactorHandler
			   NxParameterized::setParamMat44(*clothdesc,"globalPose",pose2);

			   physx::apex::NxApexActor* clothActor = clothasset->createApexActor(*clothdesc,*(GetGameObjectManager()->GetApexScene()));
			   clothingActor = static_cast<physx::apex::NxClothingActor*>(clothActor);
			   GetGameObjectManager()->AddApexActor(clothingActor,"WoodPallet",1);
			  /* NxParameterized::Interface* windDesc = clothingActor->getActorDesc();

			   NxParameterized::setParamVec3(*windDesc,"windParams.Velocity",*(new physx::PxVec3(5,0.0,0.0)));
			   NxParameterized::setParamF32(*windDesc,"windParams.Adaption", 1.0f);*/

#endif
		return true;
	}

	/// Function to destroy the screen
	bool PhysXDemo::Destroy()
	{
		return true;
	}

	/// Function to setup camera viewports and show the screen
	bool PhysXDemo::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex(), 0, 0, 1.0, 1.0);
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));
		
		/*
		Character = new GameObject("character", "samurai_spear.ma.mesh", "", PHYSICS_CHARACTER_RIGIDBODY, COLLISION_SHAPE_CAPSULE, "", false);
		Character->setPosition(10.0f, 5.0f, -10.0f);
		goblinwalk = GetDefaultSceneManager()->getEntity("character")->getAnimationState("Forward");
		goblinwalk->setLoop(true);
		goblinwalk->setEnabled(false);

		Character1 = new GameObject("character1", "samurai_spear.ma.mesh", "", PHYSICS_CHARACTER_RIGIDBODY, COLLISION_SHAPE_CAPSULE, "", false);
		Character1->setPosition(0.0f, 5.0f, -10.0f);
		goblinwalk1 = GetDefaultSceneManager()->getEntity("character1")->getAnimationState("Forward");
		goblinwalk1->setLoop(true);
		goblinwalk1->setEnabled(false);


		Character2 = new GameObject("character2", "samurai_spear.ma.mesh", "", PHYSICS_CHARACTER_RIGIDBODY, COLLISION_SHAPE_CAPSULE, "", false);
		Character2->setPosition(-10.0f, 5.0f, -10.0f);
		goblinwalk2 = GetDefaultSceneManager()->getEntity("character2")->getAnimationState("Forward");
		goblinwalk2->setLoop(true);
		goblinwalk2->setEnabled(false);

		Character3 = new GameObject("character3", "samurai_spear.ma.mesh", "", PHYSICS_CHARACTER_RIGIDBODY, COLLISION_SHAPE_CAPSULE, "", false);
		Character3->setPosition(-20.0f, 5.0f, -10.0f);
		goblinwalk3 = GetDefaultSceneManager()->getEntity("character3")->getAnimationState("Forward");
		goblinwalk3->setLoop(true);
		goblinwalk3->setEnabled(false);
		*/
		
		//setupHouse();
		//setupSteps();
		

		return true;
	}

	///Update
	bool PhysXDemo::Update() 
	{
		FreeCameraMovement();

		if(bStartTrigger == true && EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getTrigger() == true && bAfterTrigger == true)
		{
			if(nTriggerNum == 0)
			{
				GetDefaultSceneManager()->getEntity("triggerBox")->setMaterialName("PhysXRed");
				nTriggerNum++;
				EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->setTrigger(false);
				bAfterTrigger = false;
			}
			else if(nTriggerNum == 1)
			{
				GetDefaultSceneManager()->getEntity("triggerBox")->setMaterialName("PhysXYellow");
				nTriggerNum++;
				EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->setTrigger(false);
				bAfterTrigger = false;
			}
			else if(nTriggerNum == 2)
			{
				GetDefaultSceneManager()->getEntity("triggerBox")->setMaterialName("PhysXGreen");
				nTriggerNum++;
				EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->setTrigger(false);
				bAfterTrigger = false;
			}
			else if(nTriggerNum == 3)
			{
				GetDefaultSceneManager()->getEntity("triggerBox")->setMaterialName("PhysXBlack");
				nTriggerNum = 0;
				EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->setTrigger(false);
				bAfterTrigger = false;
			}
		}


		/*goblinwalk->addTime(EnginePtr->GetDeltaTime());
		goblinwalk1->addTime(EnginePtr->GetDeltaTime());
		goblinwalk2->addTime(EnginePtr->GetDeltaTime());
		goblinwalk3->addTime(EnginePtr->GetDeltaTime());*/
		

		/*if(InputPtr->IsKeyDown(GIS::KC_1))
		{
				
				physx::PxU32 uiCollisionFlags = 0;
				
				const physx::PxControllerFilters filter;
				uiCollisionFlags = Character->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 0.00001f, EnginePtr->GetDeltaTime(), filter);
				//Character->m_pGraphicsObject->m_pOgreEntity->mMesh->
				if (walking = true) {
				goblinwalk->setEnabled(true);
				walking = false;
				}
		}
		else if(InputPtr->IsKeyDown(GIS::KC_2))
		{
				
				physx::PxU32 uiCollisionFlags = 0;

				const physx::PxControllerFilters filter;
				uiCollisionFlags = Character1->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 0.00001f, EnginePtr->GetDeltaTime(), filter);
				
				//Character1->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 2, 0.00001f, uiCollisionFlags);
				if (walking = true) {
				goblinwalk1->setEnabled(true);
				walking = false;
				}
		}
		else if(InputPtr->IsKeyDown(GIS::KC_3))
		{
				
				physx::PxU32 uiCollisionFlags = 0;

				const physx::PxControllerFilters filter;
				uiCollisionFlags = Character2->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 0.00001f, EnginePtr->GetDeltaTime(), filter);
				
				//Character2->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 2, 0.00001f, uiCollisionFlags);
				if (walking = true) {
				goblinwalk2->setEnabled(true);
				walking = false;
				}
		}
		else if(InputPtr->IsKeyDown(GIS::KC_4))
		{
				
				physx::PxU32 uiCollisionFlags = 0;

				const physx::PxControllerFilters filter;
				uiCollisionFlags = Character3->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 0.00001f, EnginePtr->GetDeltaTime() , filter);
				
				//Character3->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(0,0,5 * EnginePtr->GetDeltaTime()), 2, 0.00001f, uiCollisionFlags);
				if (walking = true) {
				goblinwalk3->setEnabled(true);
				walking = false;
				}
		}
		else{
			goblinwalk->setEnabled(false);
			goblinwalk1->setEnabled(false);
			goblinwalk2->setEnabled(false);
			goblinwalk3->setEnabled(false);

		}*/

		//destructibleActor->updateRenderResources();
		//destructibleActor->dispatchRenderResources(*(GetGameObjectManager()->GetApexRender()));
		//GetActiveCamera()

		//GetGameObjectManager()->GetApexScene()->setProjMatrix(projMatrix,0);
		//GetGameObjectManager()->GetApexScene()->setProjParams(0.1f, 10000.0f, 45.0f,EnginePtr->GetRenderWindow()->getWidth(),EnginePtr->GetRenderWindow()->getHeight());
		//GetGameObjectManager()->GetApexScene()->setViewMatrix(viewMat,0);
		//GetGameObjectManager()->GetApexScene()->setUseViewProjMatrix(0,0);

		/*GGEApexRenderer render1;
		//render1.m_materialName = "lambert2";
		destructibleActor->lockRenderResources();
		destructibleActor->updateRenderResources();
		destructibleActor->dispatchRenderResources(render1);
		destructibleActor->unlockRenderResources();

		physx::PxMat44 pose2;
		pose2 = physx::PxMat44::createZero();
		pose2.rotate(*(new physx::PxVec3(0,0,90)));
		clothingActor->updateState(pose2,NULL,0,0,physx::apex::ClothingTeleportMode::Continuous);

		GGEApexRenderer render2;
		//render2.m_materialName = "lambert";
		clothingActor->lockRenderResources();
		clothingActor->updateRenderResources();
		clothingActor->dispatchRenderResources(render2);
		clothingActor->unlockRenderResources();*/

		
		return true;
	}

	/// Function to draw all screen elements
	bool PhysXDemo::Draw()
	{
		//box3->m_pPhysicsObject->getDynamicRigidBody()->setLinearVelocity(physx::PxVec3(5.0f, 0.0f, 0.0f), true);
		//box3->m_pPhysicsObject->getDynamicRigidBody()->addForce(physx::PxVec3(0.0f, 0.0f, 5.0f),physx::PxForceMode::eVELOCITY_CHANGE,true);
		
		//box3->m_pPhysicsObject->getDynamicRigidBody()->setAngularVelocity(physx::PxVec3(5.0f, 0.0f, 0.0f), true);
		return true;
	}

	bool PhysXDemo::OnKeyPress(const GIS::KeyEvent &keyEvent) 
	{
		switch(keyEvent.key)
		{
			case GIS::KC_ESCAPE:
			{
				UtilitiesPtr->GetDebugText("Commands")->SetText("");
				Close();
				break;
			}

			case GIS::KC_SPACE: 
			{
				char buffer[30];
				itoa(boxNum, buffer, 10);
				GameObject* sphere = new GameObject(buffer, "star.mesh", "", PHYSICS_DYNAMIC, COLLISION_SHAPE_SPHERE, "", false);
				Ogre::Vector3 vectPos = GetActiveCameraSceneNode()->getPosition();
				//printf("%f, %f, %f", vectPos.x, vectPos.y, vectPos.z);
				sphere->setPosition(vectPos.x, vectPos.y, vectPos.z);
				//sphere->setPosition(0, 200, 200);
				
				printf("%f, %f, %f", sphere->m_pPhysicsObject->getDynamicRigidBody()->getGlobalPose().p.x, 
					sphere->m_pPhysicsObject->getDynamicRigidBody()->getGlobalPose().p.y, 
					sphere->m_pPhysicsObject->getDynamicRigidBody()->getGlobalPose().p.z);
				//sphere->m_pPhysicsObject->getDynamicRigidBody()->setGlobalPose(physx::PxTransform(physx::PxVec3(vectPos.x,vectPos.y,vectPos.z), physx::PxQuat::createIdentity()));
				//sphere->scale(1.0f, 1.0f, 1.0f);
				
				sphere->m_pPhysicsObject->getDynamicRigidBody()->setLinearVelocity(
					physx::PxVec3(40.0f*GetActiveCamera()->getDerivedDirection().x,
								40.0f*GetActiveCamera()->getDerivedDirection().y,
								40.0f*GetActiveCamera()->getDerivedDirection().z),true);
				
				boxNum++;
			    bAfterTrigger = true;
				EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->setTrigger(false);
				break;
			}
			case GIS::KC_B:
			{	
				setupUnbreakFixedJoint();
				setBoxes();
				setupFixedJoint();
				setupRevJoint();
				setupLimitRevJoint();
				setupSphJoint();
				setupSleepBox();

				GetDefaultSceneManager()->getEntity("fix")->setMaterialName("PhysXRed");
				GetDefaultSceneManager()->getEntity("fix0")->setMaterialName("PhysXBlue");
				GetDefaultSceneManager()->getEntity("fix1")->setMaterialName("PhysXRed");
				GetDefaultSceneManager()->getEntity("fix2")->setMaterialName("PhysXBlue");
				GetDefaultSceneManager()->getEntity("fix3")->setMaterialName("PhysXRed");
				GetDefaultSceneManager()->getEntity("fix4")->setMaterialName("PhysXBlue");
				GetDefaultSceneManager()->getEntity("fix5")->setMaterialName("PhysXRed");
				GetDefaultSceneManager()->getEntity("fix6")->setMaterialName("PhysXBlue");
				GetDefaultSceneManager()->getEntity("fix7")->setMaterialName("PhysXRed");
				GetDefaultSceneManager()->getEntity("fix8")->setMaterialName("PhysXBlue");
				GetDefaultSceneManager()->getEntity("box1")->setMaterialName("PhysXYellow");
				GetDefaultSceneManager()->getEntity("box2")->setMaterialName("PhysXBlue");
				GetDefaultSceneManager()->getEntity("box3")->setMaterialName("PhysXRed");
				//GetDefaultSceneManager()->getEntity("f1")->setMaterialName("PhysXYellow");
				//GetDefaultSceneManager()->getEntity("f2")->setMaterialName("PhysXGreen");

				/*for(int j=0; j<10; j++){
					char buffer[30];
					itoa(2000+j, buffer, 10);
					GetDefaultSceneManager()->getEntity(buffer)->setMaterialName("PhysXPurple");
				}
				for(int j=0; j<20; j++){
					char buffer[30];
					itoa(2500+j, buffer, 10);
					GetDefaultSceneManager()->getEntity(buffer)->setMaterialName("PhysXBlack");
				}*/

				setTrigger();
				GetDefaultSceneManager()->getEntity("triggerBox")->setMaterialName("PhysXBlue");

				break;
			}
			case GIS::KC_5:
			{
				break;
			}

			case GIS::KC_6:
			{
				
				Character1->m_pPhysicsObject->getCharacterController()->setStepOffset(physx::PxF32(4.0f));
				break;
			}

			case GIS::KC_UP:
			{
				physx::PxU32 uiCollisionFlags = 0;

				const physx::PxControllerFilters filter;
				uiCollisionFlags = Character->m_pPhysicsObject->getCharacterController()->move(physx::PxVec3(5 * EnginePtr->GetDeltaTime(),0,0), 0.00001f, EnginePtr->GetDeltaTime(), filter );
				break;
			}
			default:
				break;
		}

		return true;
	}

	
}
#endif