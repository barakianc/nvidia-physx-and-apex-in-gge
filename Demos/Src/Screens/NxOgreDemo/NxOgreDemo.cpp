// NxOgreDemo
#include "NxOgreDemo.h"

// Main
#include "Main.h"

// Engine
#include "Engine.h"


namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// NxOgreDemo(std::string name)
	//////////////////////////////////////////////////////////////////////////
	NxOgreDemo::NxOgreDemo(std::string name) : GamePipe::GameScreen(name)
	{
		m_pWorld = NULL;
		m_pScene = NULL;

		m_v3CameraPosition = Ogre::Vector3(0, 0, 50);

		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::LoadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("NxOgre");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::UnloadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().clearResourceGroup("NxOgre");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::Initialize()
	{
		CreateDefaultSceneManager();
		SetActiveCamera(CreateDefaultCamera());		
		CreateDefaultPhysicsScene(GetDefaultSceneManager());

		SetBackgroundColor(Ogre::ColourValue(0.0000, 1.0000, 0.0000, 1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		m_pWorld = EnginePtr->GetPhysicsWorld();
		m_pScene = GetPhysicsScene();

		// Create Static Geometry: Floor; we are just adding a floor mesh where the floor is
		Ogre::StaticGeometry* pStaticGeometry;
		pStaticGeometry = pSceneManager->createStaticGeometry("Grid");
		pStaticGeometry->addEntity(pSceneManager->createEntity("nx.floor", "nx.floor.mesh"), Ogre::Vector3(0, -0.05f, 0), Ogre::Quaternion(1, 0, 0, 0), Ogre::Vector3(20, 0, 20));
		pStaticGeometry->build();

		// Create Left Column and push the bodies to the Nxmodels vector
		NxOgre::Body* m_body_left1 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(-6, 3.5, 0), "Mass: 10");
		NxOgre::Body* m_body_left2 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(-6, 6.5, 0), "Mass: 10");
		NxOgre::Body* m_body_left3 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(-6, 9.5, 0), "Mass: 10");

		// Create Middle Column
		NxOgre::Body* m_body_middle1 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(0, 3.5, 0), "Mass: 10");
		NxOgre::Body* m_body_middle2 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(0, 6.5, 0), "Mass: 10");
		NxOgre::Body* m_body_middle3 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(0, 9.5, 0), "Mass: 10");
		NxOgre::Body* m_body_middle4 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(0, 12.5, 0), "Mass: 10");

		// Create Right Column
		NxOgre::Body* m_body_right1 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(6, 3.5, 0), "Mass: 10");
		NxOgre::Body* m_body_right2 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(6, 3.5, 1), "Mass: 10");
		NxOgre::Body* m_body_right3 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(7, 3.5, 0), "Mass: 10");
		NxOgre::Body* m_body_right4 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(7, 3.5, 1), "Mass: 10");
		NxOgre::Body* m_body_right5 = m_pScene->createBody("crate.mesh", new NxOgre::CubeShape(1, 1, 1), Ogre::Vector3(6, 5.5, 0), "Mass: 10");

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::Destroy()
	{
		GetDefaultSceneManager()->destroyEntity("nx.floor");
		GetDefaultSceneManager()->destroyStaticGeometry("Grid");
		
		DestroyDefaultPhysicsScene();
		DestroyDefaultSceneManager();
		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::Update()
	{
		FreeCameraMovement();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool NxOgreDemo::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}
}