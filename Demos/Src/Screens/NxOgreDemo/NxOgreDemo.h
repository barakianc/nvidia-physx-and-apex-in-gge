#pragma once

// GameScreen
#include "GameScreen.h"

// Engine
#include "Engine.h"


namespace GamePipeGame
{
	class NxOgreDemo : public GamePipe::GameScreen
	{
	private:
		NxOgre::World* m_pWorld;
		NxOgre::Scene* m_pScene;

	protected:

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

	public:
		NxOgreDemo(std::string name);
		~NxOgreDemo() {}
	};
}