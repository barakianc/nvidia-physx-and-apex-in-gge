#pragma once

// GameScreen
#include "GameScreen.h"

// For Deferred Shading
#include "DeferredShading.h"

namespace GamePipeGame
{
	class ExternalLightingScreen : public GamePipe::GameScreen
	{
	private:
	protected:
		bool doDeferredShading;
		bool doDeferredShadingSSAO;
		DeferredShadingSystem *mSystem;

		bool startup;
		bool programExit;

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		

	public:
		ExternalLightingScreen(std::string name);
		~ExternalLightingScreen() {}
	};
}