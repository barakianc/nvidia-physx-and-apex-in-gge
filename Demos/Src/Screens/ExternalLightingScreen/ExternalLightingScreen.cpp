#include "StdAfx.h"
// Lighting screens
#include "ExternalLightingScreen.h"
#include "..\InternalLightingScreen\InternalLightingScreen.h"
#include "..\MainMenuScreen\MainMenuScreen.h"

// Engine
#include "Engine.h"

// Graphics
#include "LightingManager.h"

// Physics
#include "HavokWrapper.h"

namespace GamePipeGame
{
	//GameObjectManager* havokManager;

	//////////////////////////////////////////////////////////////////////////
	// ExternalLightingScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	ExternalLightingScreen::ExternalLightingScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("Lighting");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("Lighting");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::Initialize()
	{
		GetGameObjectManager()->SetVisualDebugger(true);

		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 7.0;
		m_qCameraOrientation = Ogre::Quaternion(Ogre::Degree(90.0), Ogre::Vector3(0.0, 1.0, 0.0));
		m_v3CameraPosition = Ogre::Vector3(49,30,55);

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.0f, 0.0f, 0.0f));

		pSceneManager->setSkyBox(true, "Skybox/ViolentSkyBox", 500);

		// Initialize variables
		doDeferredShading = false;
		doDeferredShadingSSAO = false;
		startup = true;
		programExit = false;

		// Initialize rendering mode text
		if (!InternalLightingScreen::textInitialized)
		{
			const std::string instructions0 = "3 = Toggle Deferred Shading (DS)";
			UtilitiesPtr->Add("Instructions0", new GamePipe::DebugText(instructions0, 0.05f, 0.1f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions0")->GetOverlay()->show();
			const std::string instructions1 = "4 = Toggle SSAO";
			UtilitiesPtr->Add("Instructions1", new GamePipe::DebugText(instructions1, 0.05f, 0.12f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions1")->GetOverlay()->show();
			const std::string instructions2 = "5 = Toggle DS Shadows";
			UtilitiesPtr->Add("Instructions2", new GamePipe::DebugText(instructions2, 0.05f, 0.14f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions2")->GetOverlay()->show();
			const std::string instructions4 = "7 = DS Diffuse";
			UtilitiesPtr->Add("Instructions4", new GamePipe::DebugText(instructions4, 0.05f, 0.16f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions4")->GetOverlay()->show();
			const std::string instructions5 = "8 = DS Normals";
			UtilitiesPtr->Add("Instructions5", new GamePipe::DebugText(instructions5, 0.05f, 0.18f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions5")->GetOverlay()->show();
			const std::string instructions6 = "9 = DS Depth and Specular";
			UtilitiesPtr->Add("Instructions6", new GamePipe::DebugText(instructions6, 0.05f, 0.20f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions6")->GetOverlay()->show();
			const std::string instructions7 = "0 = DS Composite";
			UtilitiesPtr->Add("Instructions7", new GamePipe::DebugText(instructions7, 0.05f, 0.22f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions7")->GetOverlay()->show();

			const std::string renderingTextFR = "Forward Rendering";
			UtilitiesPtr->Add("RenderingTextFR", new GamePipe::DebugText(renderingTextFR, 0.05f, 0.4f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
			const std::string renderingTextDS = "Deferred Shading";
			UtilitiesPtr->Add("RenderingTextDS", new GamePipe::DebugText(renderingTextDS, 0.05f, 0.4f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			const std::string renderingTextDSComposite = "Composite";
			UtilitiesPtr->Add("RenderingTextDSComposite", new GamePipe::DebugText(renderingTextDSComposite, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			const std::string renderingTextDSEmissive = "Emissive Color";
			UtilitiesPtr->Add("RenderingTextDSEmissive", new GamePipe::DebugText(renderingTextDSEmissive, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
			const std::string renderingTextDSDiffuse = "Diffuse Color";
			UtilitiesPtr->Add("RenderingTextDSDiffuse", new GamePipe::DebugText(renderingTextDSDiffuse, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
			const std::string renderingTextDSNormals = "Normals";
			UtilitiesPtr->Add("RenderingTextDSNormals", new GamePipe::DebugText(renderingTextDSNormals, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
			const std::string renderingTextDSDepthSpec = "Depth and Specular";
			UtilitiesPtr->Add("RenderingTextDSDepthSpec", new GamePipe::DebugText(renderingTextDSDepthSpec, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
			const std::string renderingTextSSAO = "Screen-space Ambient Occlusion";
			UtilitiesPtr->Add("RenderingTextSSAO", new GamePipe::DebugText(renderingTextSSAO, 0.05f, 0.6f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextSSAO")->GetOverlay()->hide();
			InternalLightingScreen::textInitialized = true;
		}

		Ogre::Entity* entity;
		Ogre::SceneNode* node;

		entity = pSceneManager->createEntity("the_ground","the_ground.mesh");
		entity->setMaterialName("havok_demo/stonyMaterial");
		node = pSceneManager->getRootSceneNode()->createChildSceneNode();
		node->attachObject(entity);
		node->setPosition(0,-1,0);

		entity = pSceneManager->createEntity("ExcellentBridge","ExcellentBridge.mesh");
		node = pSceneManager->getRootSceneNode()->createChildSceneNode();
		node->attachObject(entity);
		node->setPosition(100,-1,-55);

		entity = pSceneManager->createEntity("dubai_house","dubai_house.mesh");
		node = pSceneManager->getRootSceneNode()->createChildSceneNode();
		node->attachObject(entity);
		node->setPosition(-10,-1,0);

		entity = pSceneManager->createEntity("middle_east_house","middle_east_house.mesh");
		node = pSceneManager->getRootSceneNode()->createChildSceneNode();
		node->attachObject(entity);
		node->setPosition(35,-1,0);
		
		entity = pSceneManager->createEntity("the_ramp","the_ramp.mesh");
		node = pSceneManager->getRootSceneNode()->createChildSceneNode();
		node->attachObject(entity);
		node->setPosition(5,1,20);
	
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::Destroy()
	{
		LightingManager::Destroy();

		if (programExit)
		{
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextFR"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDS"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSComposite"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSNormals"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextSSAO"));

			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions0"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions1"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions2"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions4"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions5"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions6"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions7"));
			InternalLightingScreen::textInitialized = false;
		}
		else
			// Switch screens only if we're not trying to exit the program.
			EnginePtr->AddGameScreen(new GamePipeGame::InternalLightingScreen("InternalLightingScreen"));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::Update()
	{
		//static bool isSSAOAndDSInitialized = false;
		//if (!isSSAOAndDSInitialized)
		//{
		//	LightingManager::Initialize("SunLight_Node");
		//	// TODO - this has to be enabled at least once during the Update thread before it can
		//	// enabled/disabled without crashing when changed in any other thread later.  This needs
		//	// to be fixed!!
		//	LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
		//	isSSAOAndDSInitialized = true;
		//}

		if (startup)
		{
			Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

			// Create main, static light
			Ogre::Light* l1 = pSceneManager->createLight();
			l1->setType(Ogre::Light::LT_DIRECTIONAL);
			l1->setDiffuseColour(0.7f, 0.65f, 0.3f);
			l1->setDirection(1.0f, -0.5f, -0.2f);
			l1->setShadowFarClipDistance(250);
			l1->setShadowFarDistance(75);
			//Turn this on to have the directional light cast shadows
			l1->setCastShadows(false);
			Ogre::SceneNode* node = pSceneManager->getRootSceneNode()->createChildSceneNode();
			node->attachObject(l1);
			LightingManager::Initialize(node->getName().c_str());

			//// Init main, static light
			//Ogre::Light* mainLight = GetDefaultSceneManager()->getLight("main_light");
			//mainLight->setDirection(1, -0.5, -0.2);
			//mainLight->setShadowFarClipDistance(250);
			//mainLight->setShadowFarDistance(75);
			////Turn this on to have the directional light cast shadows
			//mainLight->setCastShadows(false);

			//LightingManager::Initialize("SunLight_Node");

			// TODO - this has to be enabled at least once during the Update thread before it can
			// enabled/disabled without crashing when changed in any other thread later.  This needs
			// to be fixed!!
			LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
			//isLightingInitialized = true;

			//initLights();

			startup = false;
		}
	
		// Rotate Ogre Head
		if (GetDefaultSceneManager()->hasEntity("OgreHead"))
		{
			GetDefaultSceneManager()->getEntity("OgreHead")->getParentSceneNode()->yaw(Ogre::Degree(100 * EnginePtr->GetDeltaTime()));
		}

		// Update Camera
		FreeCameraMovement();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool ExternalLightingScreen::Draw()
	{
		return true;
	}

	bool ExternalLightingScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		/*case GIS::KC_BACK:
		{
			EnginePtr->RemoveGameScreen(this);
			break;
		}*/
		case GIS::KC_ESCAPE:
			{
				EnginePtr->AddGameScreen(new GamePipeGame::MainMenuScreen("MainMenuScreen"));
				Close();
				programExit = true;
				break;
			}
			
		case GIS::KC_SYSRQ:
			{
				SaveScene("..\\..\\Game\\Media\\scenes\\saved.scene");
				break;
			}
				case GIS::KC_3:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					if (!LightingManager::GetDeferredShadingEnabled())
					{
						LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
						UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();

						UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->show();
						UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
					}
					else
					{
						LightingManager::DisableDeferredShading();
						UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->show();

						UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
					}
				}
#endif
				break;
			}
		case GIS::KC_4:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					if (LightingManager::GetSSAOEnabled())
					{
						UtilitiesPtr->GetDebugText("RenderingTextSSAO")->GetOverlay()->hide();
						LightingManager::DisableSSAO();
					}
					else
					{
						UtilitiesPtr->GetDebugText("RenderingTextSSAO")->GetOverlay()->show();
						LightingManager::EnableSSAO();
					}
				}
#endif
				break;
			}
		case GIS::KC_7:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::DIFFUSE_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		case GIS::KC_8:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::NORMALS_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		case GIS::KC_9:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::DEPTH_AND_SPECULAR_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->show();
				}
#endif
				break;
			}
		case GIS::KC_0:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		}

		return true;
	}



	
}