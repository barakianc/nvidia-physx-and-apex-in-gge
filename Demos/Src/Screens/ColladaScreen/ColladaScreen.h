#pragma once

// GameScreen
#include "GameScreen.h"

// Utilities
//#include "Utilities.h"

// Main
#include "Main.h"

// OgreCollada
#include <OgreCollada.h>
#include <vector>
#include <string>

namespace GamePipeGame
{
	class ColladaScreen : public GamePipe::GameScreen
	{
	private:
		std::vector<string> materialNames;
		int mMaterialIndex;
		Ogre::Entity* mOgreHead;

	protected:

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();
		
		bool HandleInput();
		bool OnKeyPress(const GIS::KeyEvent& keyEvent);
		bool OnKeyRelease(const GIS::KeyEvent &keyEvent);

		void CycleMaterial();
	
	public:
		ColladaScreen(std::string name);
		~ColladaScreen() {}
	};
}