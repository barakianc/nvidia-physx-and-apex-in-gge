#include "StdAfx.h"
// Screen1
#include "ColladaScreen.h"




namespace GamePipeGame
{
	////////////////////////////////////////////////////////////////////////
	// Screen1(std::string name)
	////////////////////////////////////////////////////////////////////////
	ColladaScreen::ColladaScreen(std::string name) : 
		GamePipe::GameScreen(name),
		mMaterialIndex(0)
	{
		m_v3CameraPosition = Ogre::Vector3(0, 50, 0);
		m_fCameraMovementSpeed = 120.0;
		m_fCameraRotationSpeed = 15.0;
		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::LoadResources()
	{
		try
		{
			GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
			GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("ColladaDemo");
		}
		catch (Ogre::InternalErrorException e)
		{
			Ogre::String errorMessage;
			errorMessage = e.getFullDescription();
			return true;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::Initialize()
	{
		Ogre::SceneManager* mSceneMgr = GetDefaultSceneManager();

		GamePipe::DebugText* info = new GamePipe::DebugText(
			"Spacebar - Cycle Materials\nKeys - Rotate Ogre Head", 
			Ogre::Real(0.005f), 
			Ogre::Real(0.035f), 
			2, 
			Ogre::ColourValue(1, 1, 1, 0.75),
			Ogre::TextAreaOverlayElement::Left);
		UtilitiesPtr->Add("demoinfo", info);

		Ogre::MaterialManager& lMaterialManager = Ogre::MaterialManager::getSingleton();
		materialNames.push_back("carpaint");
		materialNames.push_back("blinners");
		materialNames.push_back("velvet");
		materialNames.push_back("pulse");
		materialNames.push_back("bumpGloss");
		materialNames.push_back("testGlowBalloon");

		string curMaterialName = materialNames[mMaterialIndex];

		mOgreHead = mSceneMgr->createEntity("Head", "ogrehead.mesh");
		mOgreHead->getSubEntity(0)->setMaterialName(curMaterialName);
		mOgreHead->getSubEntity(1)->setMaterialName(curMaterialName);
		mOgreHead->getSubEntity(2)->setMaterialName(curMaterialName);

		Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
		Ogre::MeshManager::getSingleton().createPlane("ground",
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
			500,500,200,200,true,1,5,5,Ogre::Vector3::UNIT_Z);
		Ogre::Entity *ent = mSceneMgr->createEntity("GroundEntity", "ground");
			mSceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(0,-5,0))->attachObject(ent);

		ent->setMaterialName("ocean");


		Ogre::SceneNode* headNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		headNode->attachObject( mOgreHead );

		mSceneMgr->setAmbientLight(Ogre::ColourValue(0.2f, 0.2f, 0.2f));

		Ogre::Light* pointLight;
  
		pointLight = mSceneMgr->createLight("pointLight1");
		pointLight->setType(Ogre::Light::LT_POINT);
		pointLight->setDiffuseColour( Ogre::ColourValue( 0.6f, 0.7f, 0.8f, 1.0f ) );
		pointLight->setSpecularColour( Ogre::ColourValue( 1.0f, 1.0f, 1.0f, 1.0f ) );
		pointLight->setPosition(Ogre::Vector3(10, 10, 50));

		pointLight = mSceneMgr->createLight("pointLight2");
		pointLight->setType(Ogre::Light::LT_POINT);
		pointLight->setDiffuseColour( Ogre::ColourValue( 0.2f, 0.2f, 0.6f, 1.0f ) );
		pointLight->setSpecularColour( Ogre::ColourValue( 1.0f, 1.0f, 1.0f, 1.0f ) );
		pointLight->setPosition(Ogre::Vector3(500, 10, 0));
	
		pointLight = mSceneMgr->createLight("pointLight3");
		pointLight->setType(Ogre::Light::LT_POINT);
		pointLight->setDiffuseColour( Ogre::ColourValue( 0.6f, 0.2f, 0.2f, 1.0f ) );
		pointLight->setSpecularColour( Ogre::ColourValue( 1.0f, 1.0f, 1.0f, 1.0f ) );
		pointLight->setPosition(Ogre::Vector3(-500, 10, 0));
	
		mSceneMgr->setSkyBox(true,"Examples/StormySkyBox",1000);
		//mSceneMgr->getCamera("ColladaScreenCamera")->setFarClipDistance(50000);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::Destroy()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::Update()
	{
		FreeCameraMovement();
		HandleInput();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool ColladaScreen::Draw()
	{
		return true;
	}

	bool ColladaScreen::HandleInput()
	{
		Ogre::SceneNode* node = mOgreHead->getParentSceneNode();
		if(InputPtr->IsKeyDown(GIS::KC_LEFT))
		{
			node->yaw(Ogre::Radian(0.03f));
		}
		else if(InputPtr->IsKeyDown(GIS::KC_RIGHT))
		{
			node->yaw(Ogre::Radian(-0.03f));
		}

		if(InputPtr->IsKeyDown(GIS::KC_UP))
		{
			node->roll(Ogre::Radian(0.03f));
		}
		else if(InputPtr->IsKeyDown(GIS::KC_DOWN))
		{
			node->roll(Ogre::Radian(-0.03f));
		}
		return true;
	}

	bool ColladaScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{		
		switch(keyEvent.key)
		{
			case GIS::KC_SPACE:
			{
				if(mOgreHead != 0)
				{
					if(++mMaterialIndex >= (int)materialNames.size())
						mMaterialIndex = 0;

					mOgreHead->setMaterialName(materialNames[mMaterialIndex]);
				}
			}
			break;

			case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		}

		return true;
	}

	bool ColladaScreen::OnKeyRelease(const GIS::KeyEvent& keyEvent )
	{
		return true;
	}
}