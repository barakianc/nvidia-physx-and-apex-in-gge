#pragma once

// GameScreen
#include "GameScreen.h"
#include "GUIManager/DemoGuiManager.h"
#include "Hikari.h"

namespace GamePipeGame
{
	class MainMenuScreen : public GamePipe::GameScreen
	{
	private:
        void ToggleSelectionText();
		//DemoGuiManager* m_guiMgr;
		FlashControl* m_menu;
		int enterCount;
		bool readyToExit;
		int selectedIndex;
	protected:
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

		bool gestureRecognized( const std::string gestureName);
		bool speechRecognized( const std::string wordRecognized, const float wordConfidence);


		FlashValue LoadButtonClick(FlashControl* caller, const Arguments& args);

	public:
		MainMenuScreen(std::string name);
		~MainMenuScreen() {}
	};
}