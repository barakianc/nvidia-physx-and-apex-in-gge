#include "StdAfx.h"
// MainMenuScreen
#include "MainMenuScreen.h"

// Main
#include "Main.h"

namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// MainMenuScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	MainMenuScreen::MainMenuScreen(std::string name) : GamePipe::GameScreen(name)
	{
		//FlashGUIPtr->FlashGUIManager-> = new DemoGuiManager();
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Initialize()
	{
		GetGameObjectManager()->SetVisualDebugger(true);
		SetBackgroundColor(Ogre::ColourValue(1.0f, 0.8f, 0.0f, 1.0f));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f, 0.5f));
		//pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pSceneManager->createEntity("OgreHead", "ogrehead.mesh"));        
		
		//const std::string menuText = "1: Audio\n2: Renderer\n3: Collada\n4: GIS\n5: Havok\n6: Multiplayer Online\n7: FlashGui 1\n8: FlashGui 2\n8: Video\n0: FlashGui 3\nL: Lighting\n9: Animation";        
        const std::string menuText = "";
		UtilitiesPtr->Add("MainMenuScreenText", new GamePipe::DebugText(menuText, (Ogre::Real)0.05, (Ogre::Real)0.1, (Ogre::Real)2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));        

		enterCount = 0;
		readyToExit = false;
		m_menu = NULL;

		//Initializing the Kinect Speech Recognition
		InputPtr->getKinect()->initializeSkeleton(pSceneManager,GamePipe::Engine::GetInstancePointer()->gameFolder);
		InputPtr->getKinect()->initializeDepth(0.0f,0.0f,0.3f,0.3f);
		InputPtr->getKinect()->initializeVideo(0.0f,0.3f,0.3f,0.3f);
		InputPtr->getKinect()->setVideoVisible(false);
		InputPtr->getKinect()->setDepthVisible(false);
		InputPtr->getKinect()->enableGestureReco(true);
		bool speechinit = InputPtr->getKinect()->InitSpeechRecog(true);
		if(speechinit)
		{
			InputPtr->getKinect()->AddGrammar(L"Audio");
			InputPtr->getKinect()->AddGrammar(L"Camera");
			InputPtr->getKinect()->AddGrammar(L"Collada");
			InputPtr->getKinect()->AddGrammar(L"Input");
			InputPtr->getKinect()->AddGrammar(L"Switch");
			InputPtr->getKinect()->AddGrammar(L"Near");
			InputPtr->getKinect()->AddGrammar(L"Havoc");
			InputPtr->getKinect()->AddGrammar(L"RakNet");
			InputPtr->getKinect()->AddGrammar(L"Flash One");
			InputPtr->getKinect()->AddGrammar(L"Flash Two");
			InputPtr->getKinect()->AddGrammar(L"Video");
			InputPtr->getKinect()->AddGrammar(L"Animation");
			InputPtr->getKinect()->AddGrammar(L"Internal Lighting");
			InputPtr->getKinect()->AddGrammar(L"External Lighting");
			InputPtr->getKinect()->AddGrammar(L"A I");
			InputPtr->getKinect()->AddGrammar(L"Cloud Client");
			InputPtr->getKinect()->AddGrammar(L"Cloud Server");
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Destroy()
	{
		UtilitiesPtr->Remove("MainMenuScreenText");  
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Show()
	{
#ifdef HAVOK
		if (!MultiplayerOnlineHomeScreen::DemoIsRunning())
		{
			Ogre::Camera* pCamera = GetActiveCamera();
			Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
			viewport->setBackgroundColour(GetBackgroundColor());
			viewport->setOverlaysEnabled(true);
			SetViewport(viewport);

			pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

			if (FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu") == 0 && enterCount%2 == 0)
			{
				int screenW = viewport->getActualWidth();
				int screenH = viewport->getActualHeight();
				m_menu = FlashGUIPtr->FlashGUIManager->createFlashOverlay("startmenu",viewport, screenW,screenH, Position(0, 0));
				m_menu->load("StartMenu.swf");
				m_menu->setTransparent(true, true);
				m_menu->bind("mouseClick",FlashDelegate(this,&MainMenuScreen::LoadButtonClick));
				m_menu->setDraggable(false);
				ToggleSelectionText();
				//m_menu = FlashGUIPtr->FlashGUIManager->AddStartMenuButton("startmenu"+enterCount,0,0,"STARTMENU",FlashDelegate(this,&MainMenuScreen::LoadButtonClick),viewport);
			}
			
			enterCount++;
			InputPtr->getKinect()->displaySkeleton(GetDefaultSceneManager());
		}
		else
		{
			enterCount = 0;
		}
#endif
			
#ifdef PHYSX
			Ogre::Camera* pCamera = GetActiveCamera();
			Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
			viewport->setBackgroundColour(GetBackgroundColor());
			viewport->setOverlaysEnabled(true);
			SetViewport(viewport);

			pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

			if (FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu") == 0 && enterCount%2 == 0)
			{
				int screenW = viewport->getActualWidth();
				int screenH = viewport->getActualHeight();
				m_menu = FlashGUIPtr->FlashGUIManager->createFlashOverlay("startmenu",viewport, screenW,screenH, Position(0, 0));
				m_menu->load("StartMenu.swf");
				m_menu->setTransparent(true, true);
				m_menu->bind("mouseClick",FlashDelegate(this,&MainMenuScreen::LoadButtonClick));
				m_menu->setDraggable(false);
				ToggleSelectionText();
				//m_menu = FlashGUIPtr->FlashGUIManager->AddStartMenuButton("startmenu"+enterCount,0,0,"STARTMENU",FlashDelegate(this,&MainMenuScreen::LoadButtonClick),viewport);
			}
			
			enterCount++;
			InputPtr->getKinect()->displaySkeleton(GetDefaultSceneManager());
#endif


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Update()
	{
        //
 		//FreeCameraMovement();
		if (readyToExit)
		{
			if (FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu") == 0)
			{
				readyToExit = false;
				
				switch(selectedIndex) {
					case 0:
						{
							AddGameScreen(new AudioScreen("AudioScreen"));
							
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 1:
						{
							AddGameScreen(new RendererDemo("RendererDemo"));

							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 2:
						{
							AddGameScreen(new ColladaScreen("ColladaScreen"));
	
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 3:
						{
							AddGameScreen(new GISScreen("GISScreen"));
		
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
#ifdef HAVOK
					case 4:
						{
							AddGameScreen(new HavokDemo("HavokDemo"));

							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
#endif
					case 5:
						{
#ifdef HAVOK
							AddGameScreen(new MultiplayerOnlineHomeScreen("MultiplayerOnlineDemo"));
#endif
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 6:
						{
							AddGameScreen(new FlashGuiDemo1("FlashGuiDemo1"));
					
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 7:
						{
#ifdef PHYSX
							AddGameScreen(new PhysXDemo("PhysXDemo"));
		
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
#endif
						}
					case 8:
						{
							AddGameScreen(new VideoScreen("VideoScreen"));
		
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 9:
						{
							AddGameScreen(new AnimationDemo("AnimationDemo"));
			
							ToggleSelectionText();
							break;
						}
					case 10:
						{
							AddGameScreen(new InternalLightingScreen("InternalLightingScreen"));
							Close(); // This is required to get the internal lighting screen to work properly
			
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
					case 11:
						{
							AddGameScreen(new ExternalLightingScreen("ExternalLightingScreen"));
							Close(); // This is required to get the external lighting screen to work properly
			
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
                    case 12:
                        {
                            AddGameScreen(new AIDemoScreen("Artificial Intelligence"));
                            
                            UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
                            break;
                        }
						
					case 13:
						{
							//This will require a change in the .SWF & .FLA file to add a 13th slot 
#ifdef HAVOK
							AddGameScreen(new CloudClientScreen("Cloud Client"));
#endif
							UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
							break;
						}
						
					case -99:
					{
						Close();
						break;
					}
				}
			}
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool MainMenuScreen::Draw()
	{
		//GetDefaultSceneManager()->getEntity("OgreHead")->getParentSceneNode()->yaw(Ogre::Degree(100 * EnginePtr->GetDeltaTime()));
		
		return true;
	}

	bool MainMenuScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{        
	    switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				readyToExit = true;
				selectedIndex = -99;
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				break;
			}
		case GIS::KC_1:
			{

				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 1;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_2:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 2;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_3:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 3;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_4:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 4;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
        case GIS::KC_5:
            {
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
                selectedIndex = 5;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
                break;
            }
        case GIS::KC_6:
            {
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
               selectedIndex = 6;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
                break;
            }
		case GIS::KC_7:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 7;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_8:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 8;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_9:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 9;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_0:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 0;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_A:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 10;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_X:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 8;
				UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
		case GIS::KC_L:
			{
				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
				selectedIndex = 0;
				//UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
				readyToExit = true;
				break;
			}
        case GIS::KC_ADD:
            {
            }
        case GIS::KC_MINUS:
            {
                FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
                selectedIndex = 12;
                UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
                readyToExit = true;
                break;
            }
			//Cloud Client
		case GIS::KC_C:
			{

				FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
#ifdef HAVOK
				AddGameScreen(new CloudClientScreen("Cloud Client"));
				//readyToExit = true;
#endif
				break;
			}
        }    

		return true;
	}

    void MainMenuScreen::ToggleSelectionText()
    {
        if (EnginePtr->GetForemostGameScreen() != this)
        {
            UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
        }
        else
        {
            UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->show();
        }
    }

	FlashValue MainMenuScreen::LoadButtonClick(FlashControl* caller, const Arguments& args)
	{
		selectedIndex =  (int)args.at(0).getNumber();
		switch(selectedIndex) {
			case 0:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 0;
					readyToExit = true;
					break;
				}
			case 1:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 1;
					readyToExit = true;
					break;
				}
			case 2:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 2;
					readyToExit = true;
					break;
				}
			case 3:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 3;
					readyToExit = true;
					break;
				}
			case 4:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 4;
					readyToExit = true;
					break;
				}
			case 5:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 5;
					readyToExit = true;
					break;
				}
			case 6:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 6;
					readyToExit = true;
					
					break;
				}
			case 7:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 7;
					readyToExit = true;
					break;
				}
			case 8:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 8;
					readyToExit = true;
					break;
				}
			case 9:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 9;
					readyToExit = true;
					break;
				}
			case 10:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 10;
					readyToExit = true;
					break;
				}
			case 11:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 11;
					readyToExit = true;
					break;
				}
            case 12:
                {
                    FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
                    selectedIndex = 12;
                    readyToExit = true;
                    break;
                }
			case 13:
				{
					FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
					selectedIndex = 13;
					readyToExit = true;
					break;
				}
		}
		//FlashGUIPtr->FlashGUIManager->DestroyAllControls();
		UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		return FLASH_VOID;
	}


	//Use Speech recognition to navigate demos
	bool MainMenuScreen::speechRecognized( const std::string wordRecognized, const float wordConfidence)
	{
		if(wordRecognized.compare("Next")==0)
		{
			if(FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")!=NULL)
			FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")->callFunction("nextDemo");
		}
		if(wordRecognized.compare("Previous")==0)
		{
			if(FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")!=NULL)
				FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")->callFunction("previousDemo");
		}
		if(wordRecognized.compare("Load")==0)
		{
			if(FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")!=NULL)
				FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")->callFunction("loadDemo");
		}
		if(wordRecognized.compare("Audio")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new AudioScreen("AudioScreen"));
							
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("Renderer")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new RendererDemo("RendererDemo"));

			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("Collada")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new ColladaScreen("ColladaScreen"));
	
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("Input")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new GISScreen("GISScreen"));
		
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		/*if(wordRecognized.compare("Havoc")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
#ifdef HAVOK
			AddGameScreen(new HavokDemo("HavokDemo"));
#endif
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}*/
#ifdef HAVOK
		if(wordRecognized.compare("RakNet")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new MultiplayerOnlineHomeScreen("MultiplayerOnlineDemo"));
				
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
#endif
		if(wordRecognized.compare("Flash One")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new FlashGuiDemo1("FlashGuiDemo1"));
					
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("Flash Two")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
#ifdef PHYSX
			AddGameScreen(new PhysXDemo("PhysXDemo"));
#endif		
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("Video")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new VideoScreen("VideoScreen"));
		
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("Animation")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new AnimationDemo("AnimationDemo"));
			
			ToggleSelectionText();
		}
		if(wordRecognized.compare("Internal Lighting")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new InternalLightingScreen("InternalLightingScreen"));
			Close(); // This is required to get the internal lighting screen to work properly
			
		    UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("External Lighting")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new ExternalLightingScreen("ExternalLightingScreen"));
			Close(); // This is required to get the external lighting screen to work properly
			
			UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		if(wordRecognized.compare("A I")==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(m_menu);
			AddGameScreen(new AIDemoScreen("Artificial Intelligence"));
                            
            UtilitiesPtr->GetDebugText("MainMenuScreenText")->GetOverlay()->hide();
		}
		return true;
	}


	//Use gesture to navigate demo
	bool MainMenuScreen::gestureRecognized( const std::string gestureName)
	{
		if(gestureName.compare("RightMove")==0)
		{
			if(FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")!=NULL)
				FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")->callFunction("nextDemo");
		}
		if(gestureName.compare("LeftMove")==0)
		{
			if(FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")!=NULL)
				FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")->callFunction("previousDemo");
		}
		if(gestureName.compare("Push")==0)
		{
			if(FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")!=NULL)
				FlashGUIPtr->FlashGUIManager->getFlashControl("startmenu")->callFunction("loadDemo");
		}
		return true;
	}
	
}