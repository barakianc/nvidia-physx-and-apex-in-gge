#include "StdAfx.h"
// AudioScreen
#include "AudioScreen.h"

// Main
#include "Main.h"

namespace GamePipeGame
{
	float AudioScreen::val = 0;
	float AudioScreen::rpm = 3000;
	//const AkGameObjectID GAME_OBJECT_ID_CAR = 200;
	bool playCar = false;
	GameObject* Car;
	////////////////////////////////////////////////////////////////////////
	// AudioScreen(std::string name)
	////////////////////////////////////////////////////////////////////////
	AudioScreen::AudioScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		GamePipe::AudioManager::FMODInitializeAudio();
		GamePipe::AudioManager::FMODLoadAudio("AudioScreen");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		GamePipe::AudioManager::FMODUnLoadAudio("AudioScreen");
		GamePipe::AudioManager::UnLoadWwiseAudio();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::Initialize()
	{		
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));
		pSceneManager->setSkyBox(true, "MySkyBox", 1000);
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pSceneManager->createEntity("CAR", "corvette.mesh"));
		pSceneManager->getEntity("CAR")->getParentSceneNode()->scale(Ogre::Vector3(30,30,30));
		
		Car = new GameObject("CAR","corvette.mesh","",DEFAULT_GAME_OBJECT_TYPE);
		Car->setPosition(0,0,0);
		Car->scale(30, 30, 30);	
		Car->setAkGameObjectID(200);

#ifdef HAVOK
		GameObject* Ground = new GameObject("the_ground","the_ground.mesh","the_ground.hkx",PHYSICS_FIXED);
		Ground->setPosition(0,-1,0);
		Ground->m_pPhysicsObject->getRigidBody()->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_GROUND));
		Ground->m_pGraphicsObject->m_pOgreEntity->setMaterialName("havok_demo/stonyMaterial");
#endif
		m_fCameraMovementSpeed *= 5;

		GamePipe::AudioManager::FMODSetEventSound("examples/AdvancedTechniques/Car", "carEvent");		
		//GamePipe::AudioManager::FMODSetEventSound("sample/group1/song1", "musicEvent");	
		GamePipe::AudioManager::FMODSetEventSound("thriller/MusicEvent/thriller", "musicEvent");
		GamePipe::AudioManager::FMODSetEventSound("test/music/event", "music2Event");
		GamePipe::AudioManager::FMODSetEventSound("examples/FeatureDemonstration/3D Events/3DSoundEmit", "3DSoundEmit");
		FMOD_VECTOR pos = { 0.0f, 0.0f, 0.0f };
		FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };
		GamePipe::AudioManager::FMODSet3DAttributes("3DSoundEmit", pos, vel);

		//Initialize the Wwise sound engine
		GamePipe::AudioManager::InitializeWwise();

		//Load the appropriate sound bank, sound banks contain events and audio
		GamePipe::AudioManager::LoadWwiseBank("queen.bnk");
		GamePipe::AudioManager::LoadWwiseBank("V_K.bnk");
		GamePipe::AudioManager::LoadWwiseBank("Car_Engine.bnk");

		AK::SoundEngine::RegisterGameObj( Car->getAkGameObjectID(), "Car_Engine" );
		GamePipe::AudioManager::SetWwiseGameObjectPosition(Car->getAkGameObjectID(), pSceneManager->getEntity("CAR")->getParentSceneNode()->getPosition(), pSceneManager->getEntity("CAR")->getParentSceneNode()->getOrientation());

		//---------------------------------------Command on Screen-----------------------------------//
		Ogre::String text;
		text = "1/2/3 -> FMOD_Muisc1 Play/pause/stop\n";
		text = text + "J/K/L -> FMOD_Muisc2 Play/pause/stop\n";
		text = text + "G/H -> FMOD_3D_sound -> Play/Stop\n";
		text = text + "N/M -> FMOD_car_sound -> Play/Stop\nUp/Down -> Set RPM of FMOD_car_sound\n";		
		text = text + "C -> Add_FMOD_DSP_Flange\nX -> Add_FMOD_DSP_Distortion\nZ -> Add_FMOD_DSP_Echo\nF -> Remove_FMOD_DSP\n";
		text = text + "5/6/7 -> Wwise_Music1 Play/pause/stop\n";
		text = text + "8/9/0 -> Wwise_Music2 Play/pause/stop\n";
		text = text + "V/B -> Wwise_car_sound Play/Stop\nRight/Left -> Set RPM of Wwise_car_sound\nESC -> Quit\n\n\n";
		text = text + "Audio Status:\n";
		text = text + "FMOD MUSIC1 Hey Jude is " + GamePipe::AudioManager::FMODGetState("music2Event") + "\n";
		text = text + "FMOD MUSIC2 Thriller is " + GamePipe::AudioManager::FMODGetState("musicEvent") + "\n";
		Ogre::String s_val;
		Ogre::stringstream out1;	
		out1 << AudioScreen::val;
		s_val = out1.str();
		text = text + "FMOD Car Sound is " + GamePipe::AudioManager::FMODGetState("carEvent") + ", RPM = " + s_val +"\n";
		text = text + "FMOD 3D Sound is " + GamePipe::AudioManager::FMODGetState("3DSoundEmit") + "\n";
		text = text + "Wwise MUSIC1 We Will Rock You is " + GamePipe::AudioManager::getWwiseState("We_Will_Rock_You") + "\n";
		text = text + "Wwise MUSIC2 is " +GamePipe::AudioManager::getWwiseState("V_K") + "\n";
		Ogre::String s_rpm;
		Ogre::stringstream out2;
		out2.clear();
		out2 << AudioScreen::rpm;
		s_rpm = out2.str();
		text = text + "Wwise Car 3D Sound is " + GamePipe::AudioManager::getWwiseState("Car_Engine") + ", RPM = " + s_rpm +"\n";
		UtilitiesPtr->Add("Commands", new GamePipe::DebugText(text, (Ogre::Real)0.05, (Ogre::Real)0.1, (Ogre::Real)3, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
		//---------------------------------------Command on Screen-----------------------------------//
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::Destroy()
	{	
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::Update()
	{	
		// Update Camera
		FreeCameraMovement();

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		////////////////////////////////////////////////////////////////////////////////
		//Calls for updating FMOD 3D Sound
		//
		//
		//Update the listener sound
		GamePipe::AudioManager::FMODUpdateListener(m_v3CameraPosition, m_qCameraOrientation);     // update 'ears'
		GamePipe::AudioManager::FMODeventSystem->update();   // needed to update 3d engine, once per frame.
		
		////////////////////////////////////////////////////////////////////////////////
		// Calls to function for updating Wwise 3D Sound
		//
		// Update AKObjectID representing the car		
		GamePipe::AudioManager::SetWwiseGameObjectPosition(Car->getAkGameObjectID(), pSceneManager->getEntity("CAR")->getParentSceneNode()->getPosition(), pSceneManager->getEntity("CAR")->getParentSceneNode()->getOrientation());
		//
		// Update AkObjectID representing the camera
		GamePipe::AudioManager::SetWwiseListenerPosition(m_v3CameraPosition, m_qCameraOrientation);
		//
		// check to see if the car sound is supposed to be playing if it is, make sure to stop the sound before we update it with the new rpm value
		if(playCar)
		{
			GamePipe::AudioManager::StopWwiseSound("Car_Engine");
			GamePipe::AudioManager::PlayWwiseSound("Car_Engine", Car->getAkGameObjectID(), AudioScreen::rpm);
		}

		Ogre::String text;
		text = "1/2/3 -> FMOD_Muisc1 Play/pause/stop\n";
		text = text + "J/K/L -> FMOD_Muisc2 Play/pause/stop\n";
		text = text + "N/M -> FMOD_car_sound -> Play/Stop\nUp/Down -> Set RPM of FMOD_car_sound\n";	
		text = text + "G/H -> FMOD_3D_sound -> Play/Stop\n";	
		text = text + "C -> Add_FMOD_DSP_Flange\nX -> Add_FMOD_DSP_Distortion\nZ -> Add_FMOD_DSP_Echo\nF -> Remove_FMOD_DSP\n";
		text = text + "5/6/7 -> Wwise_Music1 Play/pause/stop\n";
		text = text + "8/9/0 -> Wwise_Music2 Play/pause/stop\n";
		text = text + "V/B -> Wwise_car_sound Play/Stop\nRight/Left -> Set RPM of Wwise_car_sound\nESC -> Quit\n\n\n";
		text = text + "Audio Status:\n";
		text = text + "FMOD MUSIC1 Hey Jude is " + GamePipe::AudioManager::FMODGetState("music2Event") + "\n";
		text = text + "FMOD MUSIC2 Thriller is " + GamePipe::AudioManager::FMODGetState("musicEvent") + "\n";
		Ogre::String s_val;
		Ogre::stringstream out1;	
		out1 << AudioScreen::val;
		s_val = out1.str();
		text = text + "FMOD Car Sound is " + GamePipe::AudioManager::FMODGetState("carEvent") + ", RPM = " + s_val +"\n";
		text = text + "FMOD 3D Sound is " + GamePipe::AudioManager::FMODGetState("3DSoundEmit") + "\n";
		text = text + "Wwise MUSIC1 We Will Rock You is " + GamePipe::AudioManager::getWwiseState("We_Will_Rock_You") + "\n";
		text = text + "Wwise MUSIC2 is " +GamePipe::AudioManager::getWwiseState("V_K") + "\n";
		Ogre::String s_rpm;
		Ogre::stringstream out2;
		out2.clear();
		out2 << AudioScreen::rpm;
		s_rpm = out2.str();
		text = text + "Wwise Car 3D Sound is " + GamePipe::AudioManager::getWwiseState("Car_Engine") + ", RPM = " + s_rpm +"\n";
		UtilitiesPtr->GetDebugText("Commands")->SetText(text);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool AudioScreen::Draw()
	{
		return true;
	}

	bool AudioScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
			case GIS::KC_ESCAPE:
			{
				UtilitiesPtr->Remove("Commands");
				Close();
				break;
			}		
			case GIS::KC_UP:
			{			
				if(AudioScreen::val<0) AudioScreen::val=0;
				if(AudioScreen::val>8800) AudioScreen::val=8800;
				if(AudioScreen::val<8800)
				{
					AudioScreen::val = AudioScreen::val + 100;
					GamePipe::AudioManager::FMODSetEventRPMVolume("carEvent", AudioScreen::val);									
				}				
				break;
			}
			case GIS::KC_DOWN:
			{
				if(AudioScreen::val<0) AudioScreen::val=0;
				if(AudioScreen::val>8800) AudioScreen::val=8800;
				if(AudioScreen::val>0)
				{
 					AudioScreen::val = AudioScreen::val - 100;
					GamePipe::AudioManager::FMODSetEventRPMVolume("carEvent", AudioScreen::val);					
				}				
				break;
			}
			case GIS::KC_1:
			{
				GamePipe::AudioManager::FMODEventPlay("music2Event");		
				break;
			}		
			case GIS::KC_2:
			{
				GamePipe::AudioManager::FMODEventPaused("music2Event");
				break;
			}
			case GIS::KC_3:
			{
				GamePipe::AudioManager::FMODEventStop("music2Event");	
				break;
			}
			case GIS::KC_J:
			{
				GamePipe::AudioManager::FMODEventPlay("musicEvent");		
				break;
			}		
			case GIS::KC_K:
			{
				GamePipe::AudioManager::FMODEventPaused("musicEvent");
				break;
			}
			case GIS::KC_L:
			{
				GamePipe::AudioManager::FMODEventStop("musicEvent");	
				break;
			}
			case GIS::KC_N:
			{
				//GamePipe::AudioManager::FMODPlayPauseEvents();
				GamePipe::AudioManager::FMODEventPlay("carEvent");
				//GamePipe::AudioManager::FMODSetEventMute("musicEvent");
				break;
			}
			case GIS::KC_M:
			{				
				GamePipe::AudioManager::FMODEventStop("carEvent");				
				break;
			}
			case GIS::KC_G:
			{
				GamePipe::AudioManager::FMODEventPlay("3DSoundEmit");		
				break;
			}		
			case GIS::KC_H:
			{
				GamePipe::AudioManager::FMODEventStop("3DSoundEmit");	
				break;
			}
			case GIS::KC_C:
			{	
				GamePipe::AudioManager::FMODaddDsp(FMOD_DSP_TYPE_FLANGE);
				break;
			}		
			case GIS::KC_X:
			{			
				GamePipe::AudioManager::FMODaddDsp(FMOD_DSP_TYPE_DISTORTION);
				break;
			}
			case GIS::KC_Z:
			{	
				GamePipe::AudioManager::FMODaddDsp(FMOD_DSP_TYPE_ECHO);
				break;
			}
			case GIS::KC_F:
			{				
				GamePipe::AudioManager::FMODremoveDsp();
				break;
			}
			case GIS::KC_5:
			{
				GamePipe::AudioManager::PlayWwiseMusic("We_Will_Rock_You");		
				break;
			}		
			case GIS::KC_6:
			{
				GamePipe::AudioManager::PauseWwiseMusic("We_Will_Rock_You");	
				break;
			}
			case GIS::KC_7:
			{
				GamePipe::AudioManager::StopWwiseMusic("We_Will_Rock_You");		
				break;
			}
			case GIS::KC_8:
			{
				GamePipe::AudioManager::PlayWwiseMusic("V_K");		
				break;
			}		
			case GIS::KC_9:
			{
				GamePipe::AudioManager::PauseWwiseMusic("V_K");	
				break;
			}
			case GIS::KC_0:
			{
				GamePipe::AudioManager::StopWwiseMusic("V_K");		
				break;
			}
			case GIS::KC_V:
			{
				playCar = true;
				//GamePipe::AudioManager::PlayWwiseSound("Car", GAME_OBJECT_ID_CAR, AudioScreen::rpm);		
				break;
			}		
			case GIS::KC_B:
			{
				playCar = false;
				GamePipe::AudioManager::StopWwiseSound("Car_Engine");		
				break;
			}
			case GIS::KC_RIGHT:
			{			
				if(AudioScreen::rpm < 1000) AudioScreen::rpm = 1000;
				if(AudioScreen::rpm > 10000) AudioScreen::rpm = 10000;
				if(AudioScreen::rpm < 10000)
				{
					AudioScreen::rpm = AudioScreen::rpm + 50;						
					//GamePipe::AudioManager::setWwiseRPM("Car_Engine", AudioScreen::rpm);
				}				
				break;
			}
			case GIS::KC_LEFT:
			{
				if(AudioScreen::rpm < 1000) AudioScreen::rpm = 1000;
				if(AudioScreen::rpm > 10000) AudioScreen::rpm = 10000;
				if(AudioScreen::rpm > 1000)
				{
 					AudioScreen::rpm = AudioScreen::rpm - 50;
					//GamePipe::AudioManager::setWwiseRPM("Car_Engine", AudioScreen::rpm);
				}				
				break;
			}
		}
		return true;
	}	
	

}