#pragma once

// GameScreen
#include "GameScreen.h"
#include "AudioManager.h"

namespace GamePipeGame
{
	class AudioScreen : public GamePipe::GameScreen
	{
	private:
	protected:
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent& keyEvent);

		static float val;
		static float rpm;
		FMOD::Event* carEvent;
		FMOD::Event* musicEvent;
		FMOD::EventParameter* param;

	
	public:
		AudioScreen(std::string name);
		~AudioScreen() {}
	};
}