#include "StdAfx.h"
// CloudClientScreen
#include "CloudClientScreen.h"

// Engine
#include "Engine.h"

// Physics
#ifdef HAVOK
#include "HavokWrapper.h"

// Cloud gaming

#include "CloudClientGlobal.h"
#include "RakNetObjects.h"
#include "CloudMessageIdentifiers.h"
#include "Bitstream.h"


/*	
	Note that this currently works in release only in the demos section
	because of a strange error in engine.cpp.
	Yet, it works properly in debug and release if you place the cloud 
	client project folder from "ogreaddons/Cloud gaming" in the same
	folder as the core, and run it as an independent project.
*/	
namespace GamePipeGame
{
	//GameObjectManager* havokManager;

	//////////////////////////////////////////////////////////////////////////
	// CloudClientScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	CloudClientScreen::CloudClientScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}


	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::Initialize()
	{
		GetGameObjectManager()->SetVisualDebugger(true);

		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;

		pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000f, 0.5000f, 0.5000f, 0.5000f));


		//Initializing flags for managing packets flow
		f1=0;
		f2=0;

		/***************************  CLOUD MESSGAGES	********************************/	
		//Initializing the connection, or connection failed message. Displays the server IP as well.
		UtilitiesPtr->Add("Status", 
						   new GamePipe::DebugText("", 
												   (Ogre::Real)0.05, 
												   (Ogre::Real)0.1, 
												   (Ogre::Real)2, 
												   Ogre::ColourValue(1, 1, 1, 0.75), 
												   Ogre::TextAreaOverlayElement::Left));   

		UtilitiesPtr->Add("ObjectsInfo", 
						   new GamePipe::DebugText("", 
												   (Ogre::Real)0.05, 
												   (Ogre::Real)0.1, 
												   (Ogre::Real)2, 
												   Ogre::ColourValue(1, 1, 1, 0.75), 
												   Ogre::TextAreaOverlayElement::Left)); 

		UtilitiesPtr->Add("ObjectsInfo1", 
						   new GamePipe::DebugText("", 
												   (Ogre::Real)0.05, 
												   (Ogre::Real)0.1, 
												   (Ogre::Real)2, 
												   Ogre::ColourValue(1, 1, 1, 0.75), 
												   Ogre::TextAreaOverlayElement::Left)); 

		/************************** CONNECTING TO CLOUD  ****************************/	

		clientMsg = "\n\nCONNECTING TO CLOUD. . . . . . . . .";
		clientMsg += "\n";
		UtilitiesPtr->GetDebugText("Status")->SetText(clientMsg);		

		/*
			Connecting to the cloud
			Observe that this is the location of the IP address string.
			You need to change this number here and below when you are connecting to 
			a new server. This should eventually be replaced by a global variable
			popated with user input
		*/
		cloudClientManager = new GamePipe::CloudClientManager();
		cloudClientManager->serverAddress = "207.151.235.195";
		int x = 0;
		x = cloudClientManager->CreateCloudClient();		

		/***************************	POINT LIGHT	********************************/	

		pointLight1 = pSceneManager->createLight("PointLight");
		pointLight1->setType(Ogre::Light::LT_POINT);
		pointLight1->setPosition(Ogre::Vector3(50,150,50));
		pointLight1->setDiffuseColour((Ogre::Real)0.3, (Ogre::Real)0.7, (Ogre::Real)0.9);
		pointLight1->setSpecularColour((Ogre::Real)0.3, (Ogre::Real)0.7, (Ogre::Real)0.9);
		
		pointLight2 = pSceneManager->createLight("PointLight1");
		pointLight2->setType(Ogre::Light::LT_POINT);
		pointLight2->setPosition(Ogre::Vector3(0, 20, 30));
		pointLight2->setDiffuseColour((Ogre::Real)0.3, (Ogre::Real)0.7, (Ogre::Real)0.9);
		pointLight2->setSpecularColour((Ogre::Real)0.3, (Ogre::Real)0.7, (Ogre::Real)0.9);
		
		/*****************************	SKYBOX	***********************************/	
		
    	pSceneManager->setSkyBox(true, "MySkyBox", 1000);	

		/*****************************	CAMERA	***********************************/	

		Ogre::Camera *minCam1 = pSceneManager->createCamera("minCam");
	
		//ID_GGE_GAME_OBJECT
		//ID_CONNECTION_REQUEST_ACCEPTED
		//TODO-SV: delete GameObject when an Entity is deleted in GLE cant be done yet
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::Destroy()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);
			
		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		//--------------------------------------------------------

		minCam = pSceneManager->getCamera("minCam");
		minCam->setPosition(Ogre::Vector3(0, -20, 80));
		minCam->lookAt(Ogre::Vector3(0, -30, 50));
		minCam->setNearClipDistance(15);

		viewport1 = EnginePtr->GetRenderWindow()->addViewport(minCam, EnginePtr->GetFreeViewportIndex(), (float)0.66, (float)0.66, (float)0.33, (float)0.33);
		viewport1->setBackgroundColour(GetBackgroundColor());
		viewport1->setOverlaysEnabled(true);
		SetViewport(viewport1);
		minCam->setAspectRatio(static_cast<Ogre::Real>(viewport1->getActualWidth()) / static_cast<Ogre::Real>(viewport1->getActualHeight()));


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::Update()
	{

		/***************************	UPDATE CAMERA	********************************/	
		FreeCameraMovement();


		/*************************** GAME OBJECTS FROM CLOUD ********************************/
		
		//Grab cloud game objects		
		if(cloudClientManager->CloudUpdateClient() == 0)
		{
			Close();
		}
		//Create a list of all the game objects that are going to be passed to your client
		std::list<GameObject*>::iterator go_it;
		
		std::string tempMsg = "\n\n\n\n";
		std::string tempMsg1 = "\n\n\n\n\n\n";

		if(objectDataList.size() > 0)
		{

			for(std::list<ObjectData>::iterator it = objectDataList.begin(); it != objectDataList.end(); it++)
			{

				clientMsg = "Loading Object ";
				clientMsg += it->name;
				clientMsg += ".........\n";
				tempMsg += clientMsg;
				UtilitiesPtr->GetDebugText("ObjectsInfo")->SetText(tempMsg);

				if(!it->isCreated)
				{

					GameObject *go;
					
					if(it->name == "TheGroundtest")
					{
						go = new GameObject(it->name, it->meshName, "the_ground.hkx");
					}
					else
					{
						go = new GameObject(it->name, it->meshName, "", it->objectType);
					}
					go->setPosition(it->posX, it->posY, it->posZ);
					gameObjects.push_back(go);
					gameObjects2[it->name] = go;
					it->setIsCreated(true);

				}	
				else
				{					
					std::list<GameObject*>::iterator go_it;
					if(it->isUpdated)
					{
						for(go_it= gameObjects.begin(); go_it != gameObjects.end(); go_it++)
						{
							if(gameObjects2[it->name] == (*go_it)) {
								(*go_it)->m_pGraphicsObject->m_pOgreEntity->getParentSceneNode()->setPosition(
								it->posX, 
								it->posY, 
								it->posZ);	
								/*
								(*go_it)->m_pGraphicsObject->m_pOgreSceneNode->setOrientation(
								Ogre::Quaternion(Ogre::Radian(it->roll), Ogre::Vector3::UNIT_X) + 
								Ogre::Quaternion(Ogre::Radian(it->pitch), Ogre::Vector3::UNIT_Y) + 
								Ogre::Quaternion(Ogre::Radian(it->yaw), Ogre::Vector3::UNIT_Z));
								*/
							}
						}
						it->setIsUpdated(false);
					}
					/*
						There are 5 ogre heads
						It is set to to where the
						leftmost and rightmost ogre heads
						are told by the server to raise
						up at the beginning.
					*/

					if(it->name == "OgreHead0")
					{
						if(it->posY < 10)
						{

								bs = NULL;
								bs.Write((RakNet::MessageID)ID_GGE_ENV_OBJECTS);
								cloudClientManager->rakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, cloudClientManager->serverAddressObject, false); 		

						}
						else if (it->posY == 10)
						{
							it->setIsUpdated(false);
						}
					}
				

					if(it->name == "OgreHead4")
					{
						if(it->posY < 10)
						{

								bs = NULL;
								bs.Write((RakNet::MessageID)ID_GGE_ENV_OBJECTS);						
								//bs.Write(4);						
								cloudClientManager->rakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, cloudClientManager->serverAddressObject, false); 		

						}
						else if (it->posY == 10)
						{
							it->setIsUpdated(false);
						}
					}

				}			
			}
		}
		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool CloudClientScreen::Draw()
	{
		//You need this iterator to go through all of the game objects
		std::list<ObjectData>::iterator it;
		if(objectDataList.size() > 0)
		{
			for(it = objectDataList.begin(); it != objectDataList.end(); it++)
			{
				if(it->name == "OgreHead2")
				{
					break;
				}
			}
		}

		/***************************	PLAYER INPUT	********************************/
		/*
			Originally the program was designed
			to take user input. If the user
			presses t, then the center ogre head raises.
			But this disrupted our packet stream, so it
			is inactive.
		*/
		if(InputPtr->IsKeyDown(GIS::KC_T))
		{
						int key = 24;
						//string ObjectId = "OgreHead2";
						//int x=0, y=0, z=0;
						RakNet::BitStream bs;
						bs.Write((RakNet::MessageID)ID_KEY_INPUT);
				
						bs.Write(key);
						//bs.Write(ObjectId);
						
						bs.Write(it->posX);
						bs.Write(it->posY);
						bs.Write(it->posZ);

						cloudClientManager->rakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, cloudClientManager->serverAddressObject, false); 		
		}

		return true;
	}

	bool CloudClientScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		//Press escape to exit
		case GIS::KC_ESCAPE:
			{
				UtilitiesPtr->Remove("Status");
				UtilitiesPtr->Remove("ObjectsInfo");
				UtilitiesPtr->Remove("ObjectsInfo1");
				RakNet::BitStream bs;
				bs.Write((const unsigned char)ID_DISCONNECTION_NOTIFICATION);
				cloudClientManager->rakPeer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, cloudClientManager->serverAddressObject, false);
				Close();

				break;
			}
		//This is never really used
		case GIS::KC_SYSRQ:
			{
				SaveScene("..\\..\\CloudGaming_Client\\Media\\scenes\\saved.scene");
				break;
			}
		}

		return true;
	}
}

#endif