#pragma once

// GameScreen
#include "GameScreen.h"

//cloud client
#include "CloudClientManager.h"
#include "ObjectData.h"


namespace GamePipeGame
{
	class CloudClientScreen : public GamePipe::GameScreen
	{
	private:
		//GGE Game Objects
		GamePipe::CloudClientManager* cloudClientManager;
		std::list<GameObject*> gameObjects;
		//Maps are crucial for managing objects from the server
		std::map<std::string, GameObject*> gameObjects2;
    	std::map<std::string, ObjectData> mObjectData;	
		ObjectData od;

		Ogre::Entity* ajCube;
		Ogre::SceneNode* ajCubeNode;
		
		Ogre::Light* pointLight1;
		Ogre::Light* pointLight2;
	
		std::string clientMsg;
		
		Ogre::SceneManager* pSceneManager;
		Ogre::Camera* minCam;
		Ogre::Viewport* viewport1;

		// ---- RAKPEER -----
		RakNet::RakPeerInterface *rakPeer;
		RakNet::Packet *packet;

		RakNet::BitStream bs;

		int f, f1, f2;

	protected:
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		

	public:
		CloudClientScreen(std::string name);
		~CloudClientScreen() {}
	};
}