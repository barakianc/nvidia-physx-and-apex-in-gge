#include "StdAfx.h"
#include "StdAfx.h"
#include "DemoGuiManager.h"
#include "Input.h"

using namespace Hikari;
using namespace Ogre;

namespace GamePipeGame
{
	/************************************************************************/
	/* Create Controls                                                                     */
	/************************************************************************/
	void DemoGuiManager::MakeMainMenu()  // Default Mainmenu with four buttons
	{
		MainMenuText("Main Menu Text",100,10);
		AddMenuButton("Choose Screen",200,200,"Choose Screen");
		AddMenuButton("Options",200,230,"Options");
		AddMenuButton("Credits",200,260,"Credits");
		AddMenuButton("Exit",200,290,"Exit");
	}

	void DemoGuiManager::CreatePauseMenu() // Default Pausemenu with four buttons
	{
		//FlashGUIPtr->FlashGUIManager->destroyAllControls();
		CompositorManager::getSingleton().addCompositor(currentViewport,"Bloom");
		CompositorManager::getSingleton().setCompositorEnabled(currentViewport,"Bloom",true);
		/*PauseMenuText("pause text",50,10);
		AddMenuButton("Resume",200,200,"Resume");
		AddMenuButton("Control",200,230,"Control");
		AddMenuButton("Options1",200,260,"Options");
		AddMenuButton("PExit",200,290,"Exit"); */
		pause=true;
	}

	void DemoGuiManager::ResumePauseMenu()
	{
		resume=true;        // indicate resume
		CompositorManager::getSingleton().removeCompositor(currentViewport,"Bloom");
		//DestroyAllControls();
		pause=false;
		mainMenuCreated = false;
	}

	FlashControl* DemoGuiManager::AddMenuButton( const String& name,int x, int y,const String& label )
	{
		FlashControl *control;
		control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 160,30, Position(x,y));
		control->load("button.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));         // Calls the actionscript function to set the text
		control->bind("mouseClick",FlashDelegate(this, &DemoGuiManager::MenuButtonClick));
		return control;
	}

	FlashControl* DemoGuiManager::AddCircularButton( const String& name,int x, int y,const String& label,const FlashDelegate& callback )
	{
		FlashControl *control;
		control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,currentViewport, 80,80, Position(x,y));
		control->load("circularbutton.swf");
		control->setTransparent(true, true);
		control->callFunction("setText",Args(label));
		control->bind("mouseClick",callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddSlider( const String& name,int x, int y,const String& label,const FlashDelegate& callback )
	{
		FlashControl *control;
		control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 160,90, Position(x,y));
		control->load("slider.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		control->bind("sliderChange", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddCheckBox( const String& name, int x, int y,const String& label,const FlashDelegate& callback )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 160, 30, Position(x, y));
		control->load("checkbox.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		control->bind("checkboxClick", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddToggleButton( const String& name,int x, int y,const String& label,const FlashDelegate& callback )
	{
		FlashControl *control= FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 160, 30, Position(x, y));
		control->load("togglebutton.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		control->bind("mouseClick", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddComboBox( const String& name,int x, int y,const FlashDelegate& callback )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport,160, 500, Position(x, y));
		control->load("combo.swf");
		control->setTransparent(true, true);
		control->bind("comboChange", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddDoubleButton( const String& name,int x, int y,const String& label,const FlashDelegate& callback )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 200, 30, Position(x, y));
		control->load("doublebutton.swf");
		control->setTransparent(true, true);
		control->bind("mouseClick", callback);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddMessageBox( const String& name, int x, int y,const String& label )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 300, 300, Position(x, y));
		control->load("transparenttext.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddOverlayText( const String& name, int x, int y,const String& label )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 200, 400, Position(x, y));
		control->load("overlaytext.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddLabelText( const String& name, int x, int y,const String& label )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 160, 30, Position(x, y));
		control->load("labeltext.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddBar( const String& name,int x, int y,int maxValue )
	{
		FlashControl *control= FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 200, 30, Position(x, y));
		control->load("bar.swf");
		control->setTransparent(true, true);
		control->callFunction("setMax",Args(maxValue));
		return control;
	}

	FlashControl* DemoGuiManager::AddTimer( const String& name,int x, int y,int seconds )
	{
		FlashControl *control= FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 100, 40, Position(x, y));
		control->load("timer.swf");
		control->setTransparent(true, true);
		control->callFunction("setTimer", Args(seconds));
		return control;
	}

	FlashControl* DemoGuiManager::AddAlertText( const String& name,int x, int y )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 160, 30, Position(x, y));
		control->load("alert.swf");
		control->setTransparent(true, true);
		return control;
	}

	FlashControl* DemoGuiManager::MainMenuText( const String& name,int x, int y )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 500, 120, Position(x, y));
		control->load("maintext.swf");
		control->setTransparent(true, true);
		return control;
	}

	FlashControl* DemoGuiManager::PauseMenuText( const String& name,int x, int y )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 500, 120, Position(x, y));
		control->load("pausetext.swf");
		control->setTransparent(true, true);
		return control;
	}

	FlashControl* DemoGuiManager::AddYouTubeVideo( const String& name,int x, int y,const String& file )
	{
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 200, 200, Position(x, y));
		control->load(file);
		control->setTransparent(true, true);
		return control;
	}

	//////////////////
	FlashControl* DemoGuiManager::AddMenuButton( const String& name,float x, float y,const String& label,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();

		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*160,(int)multiplerY*30, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*160,(int)multiplerY*30, Position(screenX,screenY));
		}

		control->load("button.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));         // Calls the actionscript function to set the text
		control->bind("mouseClick",FlashDelegate(this, &DemoGuiManager::MenuButtonClick));
		return control;
	}

	FlashControl* DemoGuiManager::AddRectButton( const String& name,float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;

		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,currentViewport, 200,50, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,renderViewport, 200,50, Position(screenX,screenY));
		}
		control->load("menuItem_blur.swf");
		control->setTransparent(true, true);
		control->callFunction("setText",Args(label));
		control->bind("mouseClick",callback);
		return control;
	}

    FlashControl* DemoGuiManager::AddCircularButton( const String& name,float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
{
	int screenW = currentViewport->getActualWidth();
	int screenH = currentViewport->getActualHeight();
	float multiplerX = screenW/800.0f;
	float multiplerY = screenH/600.0f;
	int screenX = (int)(screenW*x);
	int screenY = (int)(screenH*y);
	FlashControl *control;

	if (renderViewport == NULL)
	{
		control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,currentViewport, (int)(multiplerX*80),(int)(multiplerY*80), Position(screenX,screenY));
	}
	else
	{
		control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,renderViewport, (int)(multiplerX*80),(int)(multiplerY*80), Position(screenX,screenY));
	}
	control->load("circularbutton.swf");
	control->setTransparent(true, true);
	control->callFunction("setText",Args(label));
	control->bind("mouseClick",callback);
	return control;
}
	FlashControl* DemoGuiManager::AddStartMenuButton( const String& name,float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = renderViewport->getActualWidth();
		int screenH = renderViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;

		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,currentViewport, (int)(multiplerX*800),(int)(multiplerY*600), Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name,renderViewport, (int)(multiplerX*800),(int)(multiplerY*600), Position(screenX,screenY));
		}
		control->load("StartMenu.swf");
		control->setTransparent(true, true);
		control->bind("mouseClick",callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddSlider( const String& name,float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;

		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*160,(int)multiplerY*90, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*160,(int)multiplerY*90, Position(screenX,screenY));
		}
		control->load("slider.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		control->bind("sliderChange", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddCheckBox( const String& name, float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		control->load("checkbox.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		control->bind("checkboxClick", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddToggleButton( const String& name,float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		control->load("togglebutton.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		control->bind("mouseClick", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddComboBox( const String& name,float x, float y,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport,(int)multiplerX*160, (int)multiplerY*500, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport,(int)multiplerX*160, (int)multiplerY*500, Position(screenX,screenY));
		}
		control->load("combo.swf");
		control->setTransparent(true, true);
		control->bind("comboChange", callback);
		return control;
	}

	FlashControl* DemoGuiManager::AddDoubleButton( const String& name,float x, float y,const String& label,const FlashDelegate& callback,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)(multiplerX*200), (int)(multiplerY*30), Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)(multiplerX*200), (int)(multiplerY*30), Position(screenX,screenY));
		}
		control->load("doublebutton.swf");
		control->setTransparent(true, true);
		control->bind("mouseClick", callback);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddMessageBox( const String& name, float x, float y,const String& label,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)(multiplerX*300), (int)(multiplerY*300), Position(screenX,screenY));
		}
		else
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)(multiplerX*300), (int)(multiplerY*300), Position(screenX,screenY));
		}
		control->load("transparenttext.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddOverlayText( const String& name, float x, float y,const String& label,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)(multiplerX*200), (int)(multiplerY*400), Position(screenX,screenY));
		}
		else
		{
			control   = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)(multiplerX*200), (int)(multiplerY*400), Position(screenX,screenY));
		}
		control->load("overlaytext.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddLabelText( const String& name, float x, float y,const String& label,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		else
		{
			control   = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		control->load("labeltext.swf");
		control->setTransparent(true, true);
		control->callFunction("setText", Args(label));
		return control;
	}

	FlashControl* DemoGuiManager::AddBar( const String& name,float x, float y,int maxValue,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control= FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*200, (int)multiplerY*30, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*200, (int)multiplerY*30, Position(screenX,screenY));
		}
		control->load("bar.swf");
		control->setTransparent(true, true);
		control->callFunction("setMax",Args(maxValue));
		return control;
	}

	FlashControl* DemoGuiManager::AddTimer( const String& name,float x, float y,int seconds,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control= FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*100, (int)multiplerY*40, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*100, (int)multiplerY*40, Position(screenX,screenY));
		}
		control->load("timer.swf");
		control->setTransparent(true, true);
		control->callFunction("setTimer", Args(seconds));
		return control;
	}

	FlashControl* DemoGuiManager::AddAlertText( const String& name,float x, float y,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control;
		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*160, (int)multiplerY*30, Position(screenX,screenY));
		}
		control->load("alert.swf");
		control->setTransparent(true, true);
		return control;
	}

	FlashControl* DemoGuiManager::MainMenuText( const String& name,float x, float y,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control ;
		if (renderViewport == NULL)
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*500, (int)multiplerY*120, Position(screenX,screenY));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, (int)multiplerX*500, (int)multiplerY*120, Position(screenX,screenY));
		}
		control->load("mafloatext.swf");
		control->setTransparent(true, true);
		return control;
	}

	FlashControl* DemoGuiManager::PauseMenuText( const String& name,float x, float y,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*500, (int)multiplerY*120, Position(screenX,screenY));
		control->load("pausetext.swf");
		control->setTransparent(true, true);
		return control;
	}

	FlashControl* DemoGuiManager::AddYouTubeVideo( const String& name,float x, float y,const String& file,Ogre::Viewport* renderViewport )
	{
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		int screenX = (int)(screenW*x);
		int screenY = (int)(screenH*y);
		FlashControl *control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, (int)multiplerX*200, (int)multiplerY*200, Position(screenX,screenY));
		control->load(file);
		control->setTransparent(true, true);
		return control;
	}

	/************************************************************************/
	//3D Overlay
	/************************************************************************/
	FlashControl* DemoGuiManager::Add3DItem(const String& name,Ogre::SceneManager* pSceneManager, Ogre::Camera* pCamera,Ogre::Vector3 pos,const String& flashName,
		const FlashDelegate& callback1, const FlashDelegate& callback2, Ogre::Viewport* renderViewport){
		OverlayManager& overlayManager = OverlayManager::getSingleton();
		Ogre::Overlay* overlay;
		overlay = overlayManager.create(name+"3DOverlay");

		FlashControl *control;
		if (renderViewport == NULL)
		{
			control  = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, currentViewport, 153,153, Position(0,0));
		}
		else
		{
			control = FlashGUIPtr->FlashGUIManager->createFlashOverlay(name, renderViewport, 153, 153, Position(0,0));
		}
		control->load(flashName);
		control->setTransparent(true, true);
		//control->callFunction("setID", Args(name));
		control->bind("mouseOver", callback1);
		control->bind("mouseOut", callback2);
		control->callFunction("setAlpha",Args(Ogre::Real(0.7)));

		// the 3D stuff!!! *****************************************************************************************************************************/
//		OverlayManager& overlayManager = OverlayManager::getSingleton();

		//Get the overlay
		//Ogre::Overlay* overlay = overlayManager.getByName(name + "Overlay");
		//Get the overlayElement
		Ogre::OverlayElement* panel = overlayManager.getOverlayElement(name+"Panel");

		// Create SceneNode
		//Ogre::SceneNode* pOverlayNode = new Ogre::SceneNode(pSceneManager,name+"OverlayNode");
		Ogre::SceneNode* pOverlayNode = pSceneManager->createSceneNode(name+"OverlayNode");
		Ogre::Entity* pOverlayEntity = pSceneManager->createEntity(name+"OverlayEntity", "cube.mesh");
	    pOverlayNode ->setScale(0.2f,0.2f,0.0f);
		pOverlayEntity ->setMaterialName(control->getMaterialName());
		pOverlayNode ->attachObject(pOverlayEntity);
		pOverlayNode ->setPosition(pos);
		//pOverlayNode ->showBoundingBox(true);

		overlays.push_back(name);
		overlay->add3D(pOverlayNode);
		overlay->show();

		//UtilitiesPtr->Add("cameraPos", new GamePipe::DebugText("Loading", 0.03, 0.70, 3, Ogre::ColourValue(0, 0, 1, 1), Ogre::TextAreaOverlayElement::Left));
        //UtilitiesPtr->Add("nodePos", new GamePipe::DebugText("Loading", 0.03, 0.72, 3, Ogre::ColourValue(0, 0, 1, 1), Ogre::TextAreaOverlayElement::Left));
		updatePosition(name, pCamera, pSceneManager);

		//Hide the 2D panel and show the 3D node
		//panel->hide();

		return control;
	}
	/////////////////////////////////////////////////////////////////////////
	//updatePosition
	/////////////////////////////////////////////////////////////////////////
	bool DemoGuiManager::updatePosition(const Ogre::String& name,Ogre::Camera* pCamera, Ogre::SceneManager* pSceneManager)
	{
		OverlayManager& overlayManager = OverlayManager::getSingleton();
		Ogre::OverlayElement* panel = overlayManager.getOverlayElement(name+"Panel");

		Ogre::Entity* pOverlayEntity = pSceneManager->getEntity(name+"OverlayEntity");
		Ogre::SceneNode* pOverlayNode = pOverlayEntity->getParentSceneNode();

		// Reset the Position
		Ogre::Vector3 pos = pOverlayNode->getPosition();
		int screenW = currentViewport->getActualWidth();
		int screenH = currentViewport->getActualHeight();
		float multiplerX = screenW/800.0f;
		float multiplerY = screenH/600.0f;
		Ogre::Real x=0.0f,y=0.0f;
		//getProjectPos(pCamera,pos,x,y);

		panel->setMetricsMode(Ogre::GMM_PIXELS);

		// Reset the Dimension
		Ogre::Real leftX,rightX,topY,bottomY;
		Ogre::Real width,height;
		Ogre::Real screenX;
		Ogre::Real screenY;
	/*	leftX = pOverlayEntity->getWorldBoundingBox(true).getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP).x;
		rightX = pOverlayEntity->getWorldBoundingBox(true).getCorner(Ogre::AxisAlignedBox::NEAR_RIGHT_BOTTOM).x;
		topY= pOverlayEntity->getWorldBoundingBox(true).getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP).y;
		bottomY = pOverlayEntity->getWorldBoundingBox(true).getCorner(Ogre::AxisAlignedBox::NEAR_RIGHT_BOTTOM).y;
*/
		Ogre::Vector3 posLT = pOverlayEntity->getWorldBoundingBox(true).getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP);
		Ogre::Vector3 posRB = pOverlayEntity->getWorldBoundingBox(true).getCorner(Ogre::AxisAlignedBox::NEAR_RIGHT_BOTTOM);
		getProjectPos(pCamera,posLT,leftX,topY);
		getProjectPos(pCamera,posRB,rightX,bottomY);

		width = screenW * ((rightX - leftX)/2.0f) ;
		height = screenH * ((topY - bottomY)/2.0f);

		screenX = screenW*((leftX+1.0f)/2.0f);
		screenY = screenH * ((-topY+1.0f)/2.0f);
		panel->setDimensions(width, height);
		panel->setPosition(screenX,screenY);
		panel->hide();

		//UtilitiesPtr->GetDebugText("cameraPos")->SetText(ss.str());
		//UtilitiesPtr->GetDebugText("nodePos")->SetText(ss1.str());
		return true;
	}
	/////////////////////////////////////////////////////////////////////////
	//update
	/////////////////////////////////////////////////////////////////////////
	bool DemoGuiManager::update(Ogre::Camera *pCamera, Ogre::SceneManager *pSceneManager)
	{
		int overlaySize = overlays.size();
		for(int i =0;i<overlaySize;i++)
		{
			updatePosition(overlays[i], pCamera, pSceneManager);
		}

		return true;
	}
	/////////////////////////////////////////////////////////////////////////
	///  getProjectPos()
	/// returns true if in front of the cam, and fills x,y with clamped screencoords in [-1;1]
    // cam->getProjectionMatrix() is cached inside ogre
	/////////////////////////////////////////////////////////////////////////
	bool    DemoGuiManager::getProjectPos(Ogre::Camera* cam,const Ogre::Vector3& pos,Ogre::Real& x,Ogre::Real& y) {
    Vector3 eyeSpacePos = cam->getViewMatrix(true) * pos;
    // z < 0 means in front of cam
    if (eyeSpacePos.z < 0) {
        // calculate projected pos
        Vector3 screenSpacePos = cam->getProjectionMatrix() * eyeSpacePos;
        x = screenSpacePos.x;
        y = screenSpacePos.y;
        return true;
    } else {
        x = (-eyeSpacePos.x > 0) ? -1.0f : 1.0f;
        y = (-eyeSpacePos.y > 0) ? -1.0f : 1.0f;
        return false;
    }
}
	/************************************************************************/
	/* Event Listeners                                                      */
	/************************************************************************/
	FlashValue DemoGuiManager::OnSliderChange(FlashControl* caller, const Arguments& args)
	{
		Real value = args.at(0).getNumber();
		return FLASH_VOID;
	}

	FlashValue DemoGuiManager::MenuButtonClick( FlashControl* caller, const Arguments& args )
	{
		clickedButtonName = caller->getName();      // clickedButtonName gets the name of the clicked  button
		// and does the appropriate task

		if((clickedButtonName.compare("Choose Screen"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			MainMenuText("Main MenuText",100,10);
			AddMenuButton("Screen 1",200,200,"Screen 4");
			AddMenuButton("Screen 2",200,230,"Screen 2");
			AddMenuButton("Screen 3",200,260,"BlankScreen");
			AddMenuButton("Back",200,290,"Back");
		}
		else if((clickedButtonName.compare("Back"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			MakeMainMenu();
		}
		else if((clickedButtonName.compare("Options"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			MainMenuText("MainMenu Text",100,10);
			AddMenuButton("Sound",200,200,"Sound");
			AddMenuButton("Music",200,230,"Music");
			AddMenuButton("Music Vol",200,260,"Music Vol");
			AddMenuButton("Back",200,290,"Back");
		}
		else if((clickedButtonName.compare("Credits"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			AddMenuButton("Back",200,290,"Back");
		}
		else if((clickedButtonName.compare("Music Vol"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			MainMenuText("Main Menu Text",100,10);
			AddSlider ("Slider",200,200,"Volume",FlashDelegate(this, &DemoGuiManager::OnSliderChange));
			AddMenuButton("OPBack",200,290,"Back");
		}
		else if((clickedButtonName.compare("OPBack"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			MainMenuText("Main MenuText",50,10);
			AddMenuButton("Sound",200,200,"Sound");
			AddMenuButton("Music",200,230,"Music");
			AddMenuButton("Music Vol",200,260,"Music Vol");
			AddMenuButton("Back",200,290,"Back");
		}

		else if((clickedButtonName.compare("Exit"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			screenchange=1;		  // Indicates to the update function to exit the screen on next update.
		}
		else if((clickedButtonName.compare("Resume"))==0)
		{
			ResumePauseMenu();
		}
		else if((clickedButtonName.compare("PExit"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			screenchange=1;		  // Indicates to the update function to exit the screen on next update.
			pause=false;
			CompositorManager::getSingleton().removeCompositor(currentViewport,"Bloom");
		}
		else if((clickedButtonName.compare("Options1"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			PauseMenuText("pausetext",50,10);
			AddMenuButton("Sound",200,200,"Sound");
			AddMenuButton("Music",200,230,"Music");
			AddMenuButton("PMusic Vol",200,260,"Music Vol");
			AddMenuButton("PBack",200,290,"Back");
		}
		else if((clickedButtonName.compare("PBack"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			PauseMenuText("paustext",50,10);
			AddMenuButton("Resume",200,200,"Resume");
			AddMenuButton("Control",200,230,"Control");
			AddMenuButton("Options1",200,260,"Options");
			AddMenuButton("PExit",200,290,"Exit");
		}
		else if((clickedButtonName.compare("PMusic Vol"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			PauseMenuText("pause text",50,10);
			AddSlider ("Slider",200,200,"Volume",FlashDelegate(this, &DemoGuiManager::OnSliderChange));
			AddMenuButton("QBack",200,290,"Back");
		}
		else if((clickedButtonName.compare("QBack"))==0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			AddMenuButton("Sound",200,200,"Sound");
			AddMenuButton("Music",200,230,"Music");
			AddMenuButton("PMusic Vol",200,260,"Music Vol");
			AddMenuButton("PBack",200,290,"Back");
		}
		return FLASH_VOID;
	}

	void DemoGuiManager::ScreenChange()
	{
		screenchange = 1;
	}

	bool DemoGuiManager::isPaused()
	{
		return pause;
	}

	bool DemoGuiManager::isResume()
	{
		return resume;
	}

	void DemoGuiManager::ResumeGame()
	{
		resume=false;
	}

	bool DemoGuiManager::isMainMenuCreated()
	{
		return mainMenuCreated;
	}

	void DemoGuiManager::setMainMenuCreated( bool status )
	{
		mainMenuCreated = status;
	}

	DemoGuiManager::~DemoGuiManager()
	{
		DestroyAllControls();
	}

	DemoGuiManager::DemoGuiManager()
	{
		clickedButtonName="";
		close=0;
		play=0;
		pause=false;
		resume=false;
		toggleYoutube=false;
		screenchange = 0;
	}

	FlashControl* DemoGuiManager::CreateFlashMaterial( const Ogre::String& name )
	{
		return FlashGUIPtr->CreateFlashMaterial(name);
	}

	void DemoGuiManager::SetViewport(Ogre::Viewport* pviewPort)
	{
		FlashGUIPtr->SetViewport(pviewPort);
		currentViewport = pviewPort;
	}

	void DemoGuiManager::DestroyAllControls()
	{
		FlashGUIPtr->DestroyAllControls();
	}

	void DemoGuiManager::RemoveFlashControl( FlashControl* controlToDestroy )
	{
		FlashGUIPtr->RemoveFlashControl(controlToDestroy);
	}
	}