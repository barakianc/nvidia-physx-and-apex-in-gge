#pragma once

//#include "GameScreen.h"
#include "Engine.h"
#include "FlashGUI.h"
#include "Hikari.h"
#include <vector>

using namespace Hikari;

namespace GamePipeGame
{
	class DemoGuiManager //: public GamePipe::FlashGUI
	{
	private:
		bool pause,resume,toggleYoutube;
		int close,play;
		GameScreen *callingScreen;
		bool mainMenuCreated;
		Ogre::String clickedButtonName;
		Ogre::Viewport* currentViewport;
		std::vector<Ogre::String> overlays;
		
	protected:

	public:
		DemoGuiManager();
		~DemoGuiManager();
		int screenchange;
		void SetViewport(Ogre::Viewport* pviewPort);
		void DestroyAllControls();
		void RemoveFlashControl(FlashControl* controlToDestroy);
		FlashControl* CreateFlashMaterial(const Ogre::String& name);
		/************************************************************************/
		/* Create components                                                    */
		/************************************************************************/
		void MakeMainMenu(); /* Default Mainmenu with four buttons */
		void CreatePauseMenu();
		void ResumePauseMenu();

		FlashControl* AddMenuButton(const Ogre::String& name,int x, int y,const Ogre::String& label);
		FlashControl* AddCircularButton(const Ogre::String& name,int x, int y,const Ogre::String& label,const FlashDelegate& callback);	

		FlashControl* AddSlider(const Ogre::String& name,int x, int y,const Ogre::String& label,const FlashDelegate& callback);
		FlashControl* AddCheckBox(const Ogre::String& name, int x, int y,const Ogre::String& label,const FlashDelegate& callback);
		FlashControl* AddToggleButton(const Ogre::String& name,int x, int y,const Ogre::String& label,const FlashDelegate& callback);
		FlashControl* AddComboBox(const Ogre::String& name,int x, int y,const FlashDelegate& callback);
		FlashControl* AddDoubleButton(const Ogre::String& name,int x, int y,const Ogre::String& label,const FlashDelegate& callback);	
		FlashControl* AddMessageBox(const Ogre::String& name, int x, int y,const Ogre::String& label);
		FlashControl* AddOverlayText(const Ogre::String& name, int x, int y,const Ogre::String& label);
		FlashControl* AddLabelText(const Ogre::String& name, int x, int y,const Ogre::String& label);	
		FlashControl* AddBar(const Ogre::String& name,int x, int y,int maxValue);
		FlashControl* AddTimer(const Ogre::String& name,int x, int y,int seconds);	
		FlashControl* AddAlertText(const Ogre::String& name,int x, int y);
		FlashControl* MainMenuText(const Ogre::String& name,int x, int y) ;
		FlashControl* PauseMenuText(const Ogre::String& name,int x, int y);
		FlashControl* AddYouTubeVideo(const Ogre::String& name,int x, int y,const Ogre::String& file);

		FlashControl* AddMenuButton(const Ogre::String& name,float x, float y,const Ogre::String& label, Ogre::Viewport* renderViewport);
		FlashControl* AddCircularButton(const Ogre::String& name,float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);	
		FlashControl* AddStartMenuButton(const Ogre::String& name,float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);	
		FlashControl* AddRectButton(const Ogre::String& name,float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);	
		FlashControl* AddSlider(const Ogre::String& name,float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);
		FlashControl* AddCheckBox(const Ogre::String& name, float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);
		FlashControl* AddToggleButton(const Ogre::String& name,float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);
		FlashControl* AddComboBox(const Ogre::String& name,float x, float y,const FlashDelegate& callback, Ogre::Viewport* renderViewport);
		FlashControl* AddDoubleButton(const Ogre::String& name,float x, float y,const Ogre::String& label,const FlashDelegate& callback, Ogre::Viewport* renderViewport);	
		FlashControl* AddMessageBox(const Ogre::String& name, float x, float y,const Ogre::String& label,Ogre::Viewport* renderViewport);
		FlashControl* AddOverlayText(const Ogre::String& name, float x, float y,const Ogre::String& label,Ogre::Viewport* renderViewport);
		FlashControl* AddLabelText(const Ogre::String& name, float x, float y,const Ogre::String& label,Ogre::Viewport* renderViewport);	
		FlashControl* AddBar(const Ogre::String& name,float x, float y,int maxValue,Ogre::Viewport* renderViewport);
		FlashControl* AddTimer(const Ogre::String& name,float x, float y,int seconds,Ogre::Viewport* renderViewport);	
		FlashControl* AddAlertText(const Ogre::String& name,float x, float y,Ogre::Viewport* renderViewport);
		FlashControl* MainMenuText(const Ogre::String& name,float x, float y,Ogre::Viewport* renderViewport) ;
		FlashControl* PauseMenuText(const Ogre::String& name,float x, float y,Ogre::Viewport* renderViewport);
		FlashControl* AddYouTubeVideo(const Ogre::String& name,float x, float y,const Ogre::String& file,Ogre::Viewport* renderViewport);

		/************************************************************************/
	    //3D Overlay
	    /************************************************************************/
		FlashControl* Add3DItem(const Ogre::String& name,Ogre::SceneManager* pSceneManager, Ogre::Camera* pCamera,Ogre::Vector3 pos,const Ogre::String& flashName,
			const FlashDelegate& callback1,const FlashDelegate& callback2, Ogre::Viewport* renderViewport);
        bool getProjectPos(Ogre::Camera* cam,const Ogre::Vector3& pos,Ogre::Real& x,Ogre::Real& y);
		bool updatePosition(const Ogre::String& name,Ogre::Camera* pCamera,Ogre::SceneManager* pSceneManager);
		bool update(Ogre::Camera* pCamera,Ogre::SceneManager* pSceneManager);

		void ScreenChange();
		bool isPaused();
		bool isResume();
		
		bool isMainMenuCreated();
		void setMainMenuCreated(bool status);

		void ResumeGame();

		//Event listener
		FlashValue MenuButtonClick(FlashControl* caller, const Arguments& args);
		FlashValue OnSliderChange(FlashControl* caller, const Arguments& args);

	};
}