// RendererDemo
#include "StdAfx.h"
#include "RendererDemo.h"

// Engine
#include "Engine.h"

// Physics
#include "HavokWrapper.h"

namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// RendererDemo(std::string name)
	//////////////////////////////////////////////////////////////////////////
	RendererDemo::RendererDemo(std::string name) : GamePipe::GameScreen(name), m_bobBoat(NULL)
	{
		fireWorksTimer = 0.0f;
		isFireworks = false;
		m_autoCamTarget = YACHT;
		m_isFreeCamMain = true;
		m_isSubCamReq = false;
		m_isUp = false;
		m_isDown = false;
		mSkyXOption = NONESK;
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);

		m_idCamFocus = 1;
		m_tmToAutoCam = 0;
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("SkyX");
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("Hydrax");
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("RendererDemo");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::UnloadResources()
	{
		delete mSkyX;
		mSkyX = NULL;
		delete mHydraX;
		mHydraX = NULL;
		Ogre::OverlayManager::getSingleton().getByName("RenderDemoHUD")->hide();

		Ogre::SceneManager* pSceneManager = m_mainScene;
		ParticleUniverse::ParticleSystemManager *pManager = ParticleUniverse::ParticleSystemManager::getSingletonPtr();

		pManager->destroyParticleSystem("Rain", pSceneManager);
		pManager->destroyParticleSystem("Fire", pSceneManager);
		pManager->destroyParticleSystem("Smoke", pSceneManager);
		pManager->destroyParticleSystem("Fireworks", pSceneManager);
		pManager->destroyParticleSystem("Fireworks2", pSceneManager);

		/*pSceneManager->destroyLight("Light0");
		pSceneManager->destroyLight("Light1");
		pSceneManager->destroyCamera("Cam2");
		pSceneManager->destroySceneNode("Cam2node");
		pSceneManager->destroySceneNode("Particle");
		//pSceneManager->destroySceneNode("FlareFireWoodSceneNode");
		//pSceneManager->destroySceneNode("SteamerSmokeSceneNode");
		pSceneManager->destroySceneNode("fireWorksNode1");
		pSceneManager->destroySceneNode("fireWorksNode2");*/

		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("SkyX");
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("Hydrax");
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("RendererDemo");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(1, 1, 1, 1));

		m_fCameraMovementSpeed = 100.0;
		m_fCameraRotationSpeed = 5.0;

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		m_mainScene = pSceneManager;
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000f, 0.5000f, 0.5000f, 0.5000f));

		Ogre::Light *mLight0 = pSceneManager->createLight("Light0");
		mLight0->setDiffuseColour(1, 1, 1);
		mLight0->setCastShadows(false);

		// Shadow caster
		Ogre::Light *mLight1 = pSceneManager->createLight("Light1");
		mLight1->setType(Ogre::Light::LT_DIRECTIONAL);

		LoadScene("RendererDemo.scene");

		Ogre::SceneNode *nodeYacht = pSceneManager->getSceneNode("MegaYachtSceneNode");

		m_bobBoat = Bobber(nodeYacht);

		Ogre::SceneNode *nodeRootYacht = pSceneManager->getSceneNode("MegaYachtBobSceneNode");
		m_YachtGuider.setTarget(nodeRootYacht);
		m_YachtGuider.setEnabled(true);

		Ogre::SceneNode *nodeFloater = pSceneManager->getSceneNode("FloaterSceneNode");
		Ogre::SceneNode *nodeFloater1 = pSceneManager->getSceneNode("Floater1SceneNode");
		Ogre::SceneNode *nodeFloater2 = pSceneManager->getSceneNode("Floater2SceneNode");
		Ogre::SceneNode *nodeFloater3 = pSceneManager->getSceneNode("Floater3SceneNode");
		Ogre::SceneNode *nodeFloater4 = pSceneManager->getSceneNode("Floater4SceneNode");
		Ogre::SceneNode *nodeFloater5 = pSceneManager->getSceneNode("Floater5SceneNode");

		Ogre::Vector3 arrPathYacht[8] = 
		{
			nodeFloater->getPosition()
			, nodeFloater1->getPosition()
			, nodeFloater2->getPosition()
			, nodeFloater3->getPosition()

			, nodeFloater3->getPosition()
			, nodeFloater4->getPosition()
			, nodeFloater5->getPosition()
			, nodeFloater->getPosition()
			};
		augmentBezier(&arrPathYacht[6], &arrPathYacht[7], &arrPathYacht[0], &arrPathYacht[1]);
		augmentBezier(&arrPathYacht[2], &arrPathYacht[3], &arrPathYacht[4], &arrPathYacht[5]);
		m_YachtGuider.setPaths(arrPathYacht, 2, 20);

		ParticleUniverse::ParticleSystemManager *pManager = ParticleUniverse::ParticleSystemManager::getSingletonPtr();
		mPSys = pManager->createParticleSystem("Rain", "rainSystem_2", pSceneManager);
		Ogre::SceneNode *nodePart = pSceneManager->getRootSceneNode()->createChildSceneNode("Particle");
		nodePart->attachObject(mPSys);
		mPSys->prepare();
		mPSys->start();

		fireParticle = pManager->createParticleSystem("Fire", "customFire", pSceneManager);
		Ogre::SceneNode *fireNode = pSceneManager->getSceneNode("FlareFireWoodSceneNode");
		fireNode->attachObject(fireParticle);
		fireParticle->prepare();

		smokePart = pManager->createParticleSystem("Smoke", "customSmoke", pSceneManager);
		Ogre::SceneNode *smokeNode = pSceneManager->getSceneNode("SteamerSmokeSceneNode");
		smokeNode->attachObject(smokePart);
		smokePart->prepare();
		smokePart->start();

		fireWorks = pManager->createParticleSystem("Fireworks", "explosionYellow", pSceneManager);
		Ogre::SceneNode *fireworksNode1 = pSceneManager->getRootSceneNode()->createChildSceneNode("fireWorksNode1");
		fireworksNode1->translate(0,1000,0);
		fireworksNode1->attachObject(fireWorks);
		fireWorks->prepare();
		fireWorks1On = false;

		fireWorks2 = pManager->createParticleSystem("Fireworks2", "explosionBlue", pSceneManager);
		Ogre::SceneNode *fireworksNode2 = pSceneManager->getRootSceneNode()->createChildSceneNode("fireWorksNode2");
		fireworksNode2->translate(100,1000,0);
		fireworksNode2->attachObject(fireWorks2);
		fireWorks2->prepare();
		fireWorks2On = false;

		//TODO-SV: delete GameObject when an Entity is deleted in GLE cant be done yet
		return true;
	}

	void RendererDemo::updateEnvironmentLighting(Ogre::Camera *camera, bool isForceDisableShadows)
	{
		Ogre::Vector3 lightDir = mSkyX->getAtmosphereManager()->getSunDirection();

		bool preForceDisableShadows = isForceDisableShadows;
		isForceDisableShadows = (lightDir.y > 0.15f) ? true : false;

		Ogre::SceneManager *sceneMgr = camera->getSceneManager();

		Ogre::Vector3 sunPos = camera->getDerivedPosition() - lightDir*mSkyX->getMeshManager()->getSkydomeRadius()*0.1f;
		mHydraX->setSunPosition(sunPos);

		Ogre::Light *Light0 = sceneMgr->getLight("Light0"),
					*Light1 = sceneMgr->getLight("Light1");

		Light0->setPosition(camera->getDerivedPosition() - lightDir*mSkyX->getMeshManager()->getSkydomeRadius()*0.02f);
		Light1->setDirection(lightDir);
	}

	void RendererDemo::updateHUD()
	{
		std::stringstream ss;
		string strHUD = "";

		//Controls
		strHUD += "TAB: Toggle Text Visibility\n";

		//SkyX HUD
		Ogre::Real skyXVal = 0;
		Ogre::Radian angle(0);

		//WINDSPEED
		strHUD += "SkyX Wind Speed (NumPad 1): ";
		getSkyXValue(WINDSPEED, &skyXVal, &angle);
		ss << skyXVal;
		if(mSkyX->getVCloudsManager()->isCreated())
			strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		//WINDDIRECTION
		strHUD += "SkyX Wind Direction (NumPad 2): ";
		getSkyXValue(WINDDIRECTION, &skyXVal, &angle);
		ss << angle.valueDegrees();
		if(mSkyX->getVCloudsManager()->isCreated())
			strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		//RADIUSIN
		strHUD += "SkyX Radius In (NumPad 3): ";
		getSkyXValue(RADIUSIN, &skyXVal, &angle);
		ss << skyXVal;
		strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		//RADIUSOUT
		strHUD += "SkyX Radius Out (NumPad 4): ";
		getSkyXValue(RADIUSOUT, &skyXVal, &angle);
		ss << skyXVal;
		strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		//EXPOSURE
		strHUD += "SkyX Exposure (NumPad 5): ";
		getSkyXValue(EXPOSURE, &skyXVal, &angle);
		ss << skyXVal;
		strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		//NOISESCALE
		strHUD += "SkyX Noise Scale (NumPad 6): ";
		getSkyXValue(NOISESCALE, &skyXVal, &angle);
		ss << skyXVal;
		if(mSkyX->getVCloudsManager()->isCreated())
			strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		//TIMEMULTIPLIER
		strHUD += "SkyX Time Multiplier (NumPad 7): ";
		getSkyXValue(TIMEMULTIPLIER, &skyXVal, &angle);
		ss << skyXVal;
		strHUD += ss.str();
		strHUD += "\n";
		ss.str(std::string());

		strHUD += "Num Key and Up / Down to increase / decrease\n";

		//HydraX HUD
		float hydraXVal = 0;

		//WAVESTRENGTH
		strHUD += "Wave Strength: ";
		getHydraxValue(WAVESTRENGTH, &hydraXVal);
		ss << hydraXVal;
		strHUD += ss.str();
		strHUD += "\nPeriod / Slash: Increase / Decrease Wave Strength\n";
		ss.str(std::string());

		strHUD += "C: Toggle clouds and rain\n";
		strHUD += "P: Toggle fireworks\n";
		strHUD += "R / T: Toggle / fullscreen extra viewport (boat underside)\n";
		strHUD += "L: Switch autocam location\n";
		strHUD += "Idle: autocam (halted by mouse movement)\n";
		strHUD += "ESC: Exit\n";

		Ogre::OverlayManager *omgr = Ogre::OverlayManager::getSingletonPtr();
		Ogre::OverlayElement *txtHelp = omgr->getOverlayElement("RenderDemoHUD/TxtHelp");
		txtHelp->setCaption(strHUD);

	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::Destroy()
	{
		//Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		

		//mSkyX->getVCloudsManager()->remove();
		//mSkyX->remove();
		/*delete mSkyX;
		mSkyX = NULL;*/

		//mHydraX->remove();
		//mHydraX->remove();
		/*delete mHydraX;
		mHydraX = NULL;*/

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		m_camFreeCam = pCamera;
		pCamera->setFOVy(Ogre::Degree(60));
		pCamera->setFarClipDistance(20000);
		pCamera->setNearClipDistance(1);
		Ogre::SceneManager *pSceneManager = GetDefaultSceneManager();

		m_nodeActiveCam = pCamera->getParentSceneNode();
		m_nodeActiveCam->setPosition(0, 110, 0);

		m_camPlayer = pSceneManager->createCamera("Cam2");
		m_camPlayer->setFOVy(Ogre::Degree(60));
		m_camPlayer->setFarClipDistance(20000);
		m_camPlayer->setNearClipDistance(1);
		m_nodePlayerCam = pSceneManager->createSceneNode("Cam2node");
		m_nodePlayerCam->attachObject(m_camPlayer);

		m_nodeAutoYacht = pSceneManager->getSceneNode("MegaYachtBobSceneNode");
		m_nodeAutoYacht->addChild(m_nodePlayerCam);
		
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		

		Ogre::Vector3 arrPathCam[8] = {Ogre::Vector3(-8.834202f, 320.585114f, -516.341003f)
				, Ogre::Vector3(-273.166809f, 203.031601f, -313.540833f)
				, Ogre::Vector3(-232.503601f, 186.227600f, 185.869644f)
				, Ogre::Vector3(-23.725925f, 150.756119f, 286.112061f)
			
				, Ogre::Vector3(-23.725925f, 150.756119f, 286.112061f)
				, Ogre::Vector3(176.634018f, 154.129410f, 10.895500f)
				, Ogre::Vector3(201.090256f, 241.217422f, -385.620056f)
				, Ogre::Vector3(-8.834202f, 320.585114f, -516.341003f)
			};
		augmentBezier(&arrPathCam[6], &arrPathCam[7], &arrPathCam[0], &arrPathCam[1]);
		augmentBezier(&arrPathCam[2], &arrPathCam[3], &arrPathCam[4], &arrPathCam[5]);
		m_pathGuider.setTarget(m_nodeActiveCam);
		m_pathGuider.setEnabled(true);
		m_pathGuider.setPaths(arrPathCam, 2, 10);

		updateAutoCamTarget();

		mSkyX = new SkyX::SkyX(pSceneManager, pCamera);
		mSkyX->create();

		mSkyX->getVCloudsManager()->create();
		SkyX::AtmosphereManager::Options atOpt = mSkyX->getAtmosphereManager()->getOptions();
		atOpt.RayleighMultiplier = 0.0045f;
		mSkyX->getAtmosphereManager()->setOptions(atOpt);

		this->setSkyXValue(TIMEMULTIPLIER, 0, Ogre::Degree(0));

		mHydraX = new Hydrax::Hydrax(pSceneManager, pCamera, viewport);
		Hydrax::Module::ProjectedGrid *mModule = new Hydrax::Module::ProjectedGrid(// Hydrax parent pointer
                                                                                mHydraX,
                                                                                // Noise module
                                                                                new Hydrax::Noise::Perlin(),
                                                                                // Base plane
                                                                                Ogre::Plane(Ogre::Vector3(0,1,0), Ogre::Vector3(0,0,0)),
                                                                                // Normal mode
                                                                                Hydrax::MaterialManager::NM_VERTEX,
                                                                                // Projected grid options
                                                                                Hydrax::Module::ProjectedGrid::Options());
 
		// Set our module
		mHydraX->setModule(static_cast<Hydrax::Module::Module*>(mModule));
 
		// Load all parameters from config file
		// Remarks: The config file must be in Hydrax resource group.
		// All parameters can be set/updated directly by code(Like previous versions),
		// but due to the high number of customizable parameters, since 0.4 version, Hydrax allows save/load config files.
		mHydraX->loadCfg("HydraxDemo.hdx");
 
		// Create water
		mHydraX->create();

		initViewPort(m_isFreeCamMain, m_isSubCamReq);

		Ogre::OverlayManager *omgr = Ogre::OverlayManager::getSingletonPtr();
		Ogre::OverlayManager::getSingleton().getByName("RenderDemoHUD")->show();
		updateHUD(); //Update debug text
		return true;
	}

	void RendererDemo::getSkyXValue(ESkyXOption skyxOption, Ogre::Real *pval,  Ogre::Radian *pAngle)
	{
		switch(skyxOption)
		{
		case RADIUSIN:
			*pval = mSkyX->getAtmosphereManager()->getOptions().InnerRadius;
			break;
		case RADIUSOUT:
			*pval = mSkyX->getAtmosphereManager()->getOptions().OuterRadius;
			break;
		case TIMEMULTIPLIER:
			*pval = mSkyX->getTimeMultiplier();
			break;
		case EXPOSURE:
			*pval = mSkyX->getAtmosphereManager()->getOptions().Exposure;
			break;
		case WINDSPEED:
			*pval = mSkyX->getVCloudsManager()->getVClouds()->getWindSpeed();
			break;
		case WINDDIRECTION:
			*pAngle = mSkyX->getVCloudsManager()->getVClouds()->getWindDirection();
			break;
		case NOISESCALE:
			*pval= mSkyX->getVCloudsManager()->getVClouds()->getNoiseScale();
			break;
		};
	}

	void RendererDemo::setSkyXValue(ESkyXOption skyxOption, Ogre::Real val,  Ogre::Radian angle)
	{
		SkyX::AtmosphereManager::Options options =  mSkyX->getAtmosphereManager()->getOptions();
		switch(skyxOption)
		{
		case RADIUSIN:
			options.InnerRadius = val;
			break;
		case RADIUSOUT:
			options.OuterRadius = val;
			break;
		case TIMEMULTIPLIER:
			mSkyX->setTimeMultiplier(val);
			break;
		case EXPOSURE:
			options.Exposure = val;
			break;
		case WINDSPEED:
			mSkyX->getVCloudsManager()->getVClouds()->setWindSpeed(val);
			break;
		case WINDDIRECTION:
			mSkyX->getVCloudsManager()->getVClouds()->setWindDirection(angle);
			break;
		case NOISESCALE:
			mSkyX->getVCloudsManager()->getVClouds()->setNoiseScale(val);
			break;
		};

		mSkyX->getAtmosphereManager()->setOptions(options);
	}

	void RendererDemo::getHydraxValue(EHydraXOption hydraxOption, float *pval)
	{
		Hydrax::Module::ProjectedGrid* module = static_cast<Hydrax::Module::ProjectedGrid*>(mHydraX->getModule());
		Hydrax::Module::ProjectedGrid::Options options = module->getOptions();

		switch(hydraxOption)
		{
		case WAVESTRENGTH:
			*pval = options.Strength;
			break;
		};
	}

	void RendererDemo::setHydraxValue(EHydraXOption hydraxOption, float val)
	{
		Hydrax::Module::ProjectedGrid* module = static_cast<Hydrax::Module::ProjectedGrid*>(mHydraX->getModule());
		Hydrax::Module::ProjectedGrid::Options options = module->getOptions();

		switch(hydraxOption)
		{
		case WAVESTRENGTH:
			options.Strength = val;
			break;
		};

		module->setOptions(options);
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::Update()
	{
		float delTime = EnginePtr->GetDeltaTime();

		Ogre::SceneManager *pSceneManager = GetDefaultSceneManager();

		m_tmToAutoCam -= delTime;
		if(m_tmToAutoCam <= 0)
		{
			m_tmToAutoCam = 0;
			m_pathGuider.setEnabled(true);
		}

		m_YachtGuider.update(delTime);

		Ogre::SceneNode *nodeYacht = pSceneManager->getSceneNode("MegaYachtBobSceneNode");
		
		Ogre::Vector3 vctTangent = m_YachtGuider.getPathTangent();
		Ogre::Real dotProd = vctTangent.dotProduct(Ogre::Vector3(0, 0, -1.0)) / vctTangent.length();
		Ogre::Vector3 vctCross = vctTangent.crossProduct(Ogre::Vector3(0, 0, -1.0));
		Ogre::Degree angle = Ogre::Math::ACos(dotProd);
		if(vctCross.y > 0)
			angle = Ogre::Degree(180 + 180 - angle.valueDegrees());

		Ogre::Quaternion qtrYacht(angle, Ogre::Vector3(0, 1, 0));
		nodeYacht->setOrientation(qtrYacht);

		m_bobBoat.update(delTime);
		m_pathGuider.update(delTime);

		Ogre::Camera* pCamera = GetActiveCamera();
		
		Ogre::SceneManager *pSceneMgr = GetDefaultSceneManager();

		myFreeCameraKeyMovement(pCamera);

		fireWorksTimer += delTime;

		if(isFireworks)
		{
			if(fireWorksTimer >= 6)
			{
				if(fireWorks1On)
				{
					fireWorks->stop();
					fireWorks1On = false;
					fireWorks2->start();
					fireWorks2On = true;
					fireWorksTimer = 0.0f;
				}
				else if(fireWorks2On)
				{
					fireWorks2->stop();
					fireWorks2On = false;
					fireWorks->start();
					fireWorks1On = true;
					fireWorksTimer = 0.0f;
				}
			}
		}

		if(m_tmToAutoCam <= 0)
		{
			pCamera->lookAt(m_nodeActiveCam->getParentSceneNode()->_getDerivedPosition());
		}

		updateEnvironmentLighting(pCamera);

		if(!m_isUp && !m_isDown)
		{
		}
		else
		{
			Ogre::Real oldVal;
			Ogre::Radian oldAngle;
			Ogre::Real multiplier = 1;
			switch(mSkyXOption)
			{
			case RADIUSIN:
				multiplier = 0.25f;
				break;
			case RADIUSOUT:
				multiplier = 0.25f;
				break;
			case TIMEMULTIPLIER:
				multiplier = 1;
				break;
			case EXPOSURE:
				multiplier = 0.5f;
				break;
			case WINDSPEED:
				multiplier = 100.0f;
				break;
			case WINDDIRECTION:
				multiplier = 5.0f;
				break;
			case NOISESCALE:
				multiplier = 2.5f;
				break;
			};
			getSkyXValue(mSkyXOption, &oldVal, &oldAngle);
			oldVal += ((m_isUp ? 1 : -1) * multiplier * EnginePtr->GetDeltaTime());
			oldAngle = Ogre::Radian(oldAngle.valueRadians() + ((m_isUp ? 1 : -1) * multiplier * EnginePtr->GetDeltaTime()));
			setSkyXValue(mSkyXOption, oldVal, oldAngle);

			updateHUD();
		}

		//updateHUD(); //Update debug text
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool RendererDemo::Draw()
	{
		float delTime = EnginePtr->GetDeltaTime();
		if(mSkyX != NULL)
			mSkyX->update(EnginePtr->GetDeltaTime());
		if(mHydraX != NULL)
			mHydraX->update(EnginePtr->GetDeltaTime());
		return true;
	}

	bool RendererDemo::OnKeyRelease(const GIS::KeyEvent &keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_UP:
			m_isUp = false;
			break;
		case GIS::KC_DOWN:
			m_isDown = false;
			break;
		case GIS::KC_TAB:
			break;
		};
		return true;
	}

	bool RendererDemo::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		Ogre::SceneManager *pSceneManager = GetDefaultSceneManager();
		Ogre::Camera *pCamera = GetActiveCamera();
		static bool isPs = true;
		static bool isClouds = true;
		static bool isHUD = true;
		switch(keyEvent.key)
		{
		case GIS::KC_1:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = WINDSPEED;
				break;
			}
		case GIS::KC_2:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = WINDDIRECTION;
				break;
			}
		case GIS::KC_3:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = RADIUSIN;
				break;
			}
		case GIS::KC_4:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = RADIUSOUT;
				break;
			}
		case GIS::KC_5:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = EXPOSURE;
				break;
			}
		case GIS::KC_6:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = NOISESCALE;
				break;
			}
		case GIS::KC_7:
			{
				mHydraXOption = NONEHY;
				mSkyXOption = TIMEMULTIPLIER;
				break;
			}
		case GIS::KC_L:
			{
				if(m_autoCamTarget == YACHT)
					m_autoCamTarget = LIGHTHOUSE;
				else if(m_autoCamTarget == LIGHTHOUSE)
					m_autoCamTarget = STEAMER;
				else if(m_autoCamTarget == STEAMER)
					m_autoCamTarget = YACHT;

				updateAutoCamTarget();
			}
			break;
		case GIS::KC_P:
			{
				if(!isFireworks)
				{
					fireWorks->start();
					isFireworks = true;
					fireWorks1On = true;
					fireWorksTimer = 0.0;
				}
				else
				{
					fireWorks->stop();
					fireWorks2->stop();
					isFireworks = false;
				}
			}
			break;
		case GIS::KC_PERIOD:
			{
				mSkyXOption = NONESK;
				mHydraXOption = WAVESTRENGTH;
				float strength;
				getHydraxValue(mHydraXOption, &strength);
				++strength;
				setHydraxValue(mHydraXOption, strength);
				break;
			}
		case GIS::KC_SLASH:
			{
				mSkyXOption = NONESK;
				mHydraXOption = WAVESTRENGTH;
				float strength;
				getHydraxValue(mHydraXOption, &strength);
				--strength;
				setHydraxValue(mHydraXOption, strength);
				break;
			}
		case GIS::KC_TAB:
			{
				isHUD = !isHUD;
				if(isHUD)
					Ogre::OverlayManager::getSingleton().getByName("RenderDemoHUD")->show(); 
				else
					Ogre::OverlayManager::getSingleton().getByName("RenderDemoHUD")->hide(); 
				break;
			}
		case GIS::KC_UP:
			{
				m_isUp = true;
				break;
			}
		case GIS::KC_DOWN:
			{
				m_isDown = true;
				break;
			}
		case GIS::KC_C:
			{
				isClouds = !isClouds;
				if(isClouds)
				{
					mPSys->start();
					fireParticle->stop();
					mSkyX->getVCloudsManager()->create();
				}
				else
				{
					mPSys->stop();
					fireParticle->start();
					mSkyX->getVCloudsManager()->remove();
				}
				break;
			}
		case GIS::KC_R:
			{
				m_isSubCamReq = !m_isSubCamReq;
				initViewPort(m_isFreeCamMain, m_isSubCamReq);
				break;
			}
		case GIS::KC_T:
			{
				m_isFreeCamMain = !m_isFreeCamMain;
				initViewPort(m_isFreeCamMain, m_isSubCamReq);
				break;
			}
		case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		case GIS::KC_SYSRQ:
			{
				SaveScene("..\\..\\Game\\Media\\scenes\\saved.scene");
				break;
			}
		}
		return true;
	}

	bool RendererDemo::OnMouseMove(const GIS::MouseEvent& mouseEvent)
	{
		Ogre::Camera *pCamera = GetActiveCamera();
		Ogre::SceneManager *pSceneMgr = GetDefaultSceneManager();
		myFreeCameraMouseMovement(mouseEvent, pCamera);

		return true;
	}

	bool RendererDemo::myFreeCameraMouseMovement(const GIS::MouseEvent& mouseEvent, Ogre::Camera *pcamera)
	{
		m_tmToAutoCam = 15;
		m_pathGuider.setEnabled(false);
		m_nodeActiveCam->getParentSceneNode()->removeChild(m_nodeActiveCam);
		GetDefaultSceneManager()->getRootSceneNode()->addChild(m_nodeActiveCam);
		if (pcamera->getProjectionType() == Ogre::PT_ORTHOGRAPHIC)
		{
			return false;
		}

		Ogre::SceneNode *pCamSceneNode = pcamera->getParentSceneNode();
		Ogre::Quaternion pCamSceneOrientation = pcamera->getOrientation();

#ifdef TBB_PARALLEL_OPTION
		pCamSceneOrientation = Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.X.rel), Ogre::Vector3(0, 1, 0))
			* pCamSceneOrientation
			* Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.Y.rel), Ogre::Vector3(1, 0, 0));
#else
		pCamSceneOrientation = Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.X.rel), Ogre::Vector3(0, 1, 0))
			* pCamSceneOrientation
			* Ogre::Quaternion(Ogre::Degree(-m_fCameraRotationSpeed * EnginePtr->GetDeltaTime() * mouseEvent.state.Y.rel), Ogre::Vector3(1, 0, 0));
#endif

		pcamera->setOrientation(pCamSceneOrientation);

		return true;
	}

	bool RendererDemo::myFreeCameraKeyMovement(Ogre::Camera *camera)
	{
		if (camera->getProjectionType() == Ogre::PT_ORTHOGRAPHIC)
		{
			return false;
		}

		Ogre::SceneNode* pCameraSceneNode = camera->getParentSceneNode();

		Ogre::Vector3 v3CameraPosition = pCameraSceneNode->getPosition();
		Ogre::Vector3 v3OldCamPos = v3CameraPosition;
		Ogre::Quaternion qtrOrientation = camera->getOrientation();

		if(!GamePipe::OgreConsole::getSingletonPtr()->isVisible()){
            if (InputPtr->IsKeyDown(GIS::KC_W))
            {
                v3CameraPosition += qtrOrientation * Ogre::Vector3(0, 0, -m_fCameraMovementSpeed * EnginePtr->GetDeltaTime());
            }

            if (InputPtr->IsKeyDown(GIS::KC_S))
            {
                v3CameraPosition += qtrOrientation * Ogre::Vector3(0, 0, m_fCameraMovementSpeed * EnginePtr->GetDeltaTime());
            }

            if (InputPtr->IsKeyDown(GIS::KC_A))
            {
                v3CameraPosition += qtrOrientation * Ogre::Vector3(-m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0, 0);
            }

            if (InputPtr->IsKeyDown(GIS::KC_D))
            {
                v3CameraPosition += qtrOrientation * Ogre::Vector3(m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0, 0);
            }

            if (InputPtr->IsKeyDown(GIS::KC_Q))
            {
                v3CameraPosition += qtrOrientation * Ogre::Vector3(0, m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0);
            }

            if (InputPtr->IsKeyDown(GIS::KC_E))
            {
                v3CameraPosition += qtrOrientation * Ogre::Vector3(0, -m_fCameraMovementSpeed * EnginePtr->GetDeltaTime(), 0);
            }

			if(v3OldCamPos != v3CameraPosition)
			{
				GGETRACE("\nCam Position: (%f, %f, %f)", v3CameraPosition.x, v3CameraPosition.y, v3CameraPosition.z);
				pCameraSceneNode->setPosition(v3CameraPosition);
			}
        }

#ifdef TBB_PARALLEL_OPTION
		if(!EnginePtr->initializing)
		{
			CameraUpdateTask* newTask = new CameraUpdateTask(pCameraSceneNode, m_v3CameraPosition, m_qCameraOrientation, Update_Pos_Rot);
			TaskContainer* container = new TaskContainer((void*)newTask,Camera_Update_Task);
			EnginePtr->taskQueue.push(container);
			EnginePtr->taskQueueSize++;
		}
		else
		{
			pCameraSceneNode->setPosition(m_v3CameraPosition);
			pCameraSceneNode->setOrientation(m_qCameraOrientation);
		}
#endif

#ifdef GGE_PROFILER2
		GGE_Profiler2Ptr->ProfilerHotkey();
#endif

		return true;
	}
}