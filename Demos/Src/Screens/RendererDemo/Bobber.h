#pragma once

#include "OgreSceneNode.h"

class Bobber
{
	Ogre::SceneNode *m_nodeBob;

	Ogre::Real m_delElevation;
	Ogre::Radian m_delAngleX, m_delAngleY, m_delAngleZ;

	Ogre::Real m_currElevation, m_targetElevation;
	Ogre::Radian m_currAngleX, m_currAngleY, m_currAngleZ;
	Ogre::Radian m_targetAngleX, m_targetAngleY, m_targetAngleZ;

	Ogre::Real m_tmDelElevation, m_tmDelAngleX, m_tmDelAngleY;

public:
	Bobber(Ogre::SceneNode *nodeBob)
	{
		m_nodeBob = nodeBob;

		m_delElevation = 1.0f;
		m_delAngleX = Ogre::Degree(1);
		m_delAngleY = Ogre::Degree(1);
		m_delAngleZ = Ogre::Degree(1);

		m_currElevation = 0;
		m_targetElevation = -m_delElevation;

		m_currAngleX = Ogre::Degree(0);
		m_currAngleY = Ogre::Degree(0);
		m_currAngleZ = Ogre::Degree(0);
		m_targetAngleX = -m_delAngleX;
		m_targetAngleY = -m_delAngleY;
		m_targetAngleZ = -m_delAngleZ;

		m_tmDelElevation = 1;
		m_tmDelAngleX = 3;
		m_tmDelAngleY = 6;
	}

	void reset()
	{
		m_nodeBob->setPosition(m_nodeBob->getPosition() + Ogre::Vector3(0, -m_currElevation, 0));

		//Ogre::Quaternion qtrAdjust(Ogre::Matrix3::FromEulerAnglesXYZ(
		
	}

	void update(Ogre::Real delTime)
	{
		Ogre::Real newElevation = m_currElevation;
		if(m_currElevation < m_targetElevation)
		{
			newElevation = newElevation + (delTime * (m_delElevation));
			if(newElevation > m_targetElevation)
				newElevation = m_targetElevation;
		}
		else
		{
			newElevation = newElevation - (delTime * (m_delElevation));
			if(newElevation < m_targetElevation)
				newElevation = m_targetElevation;
		}

		if(newElevation == m_targetElevation)
			m_targetElevation = -m_targetElevation;

		m_currElevation = newElevation;
		m_nodeBob->setPosition(0, m_currElevation, 0);

		Ogre::Radian newAngleX = m_currAngleX;
		if(m_currAngleX < m_targetAngleX)
		{
			newAngleX = newAngleX + (delTime * m_delAngleX / m_tmDelAngleX);
			if(newAngleX > m_targetAngleX) 
				newAngleX = m_targetAngleX;
		}
		else
		{
			newAngleX = newAngleX - (delTime * m_delAngleX / m_tmDelAngleX);
			if(newAngleX < m_targetAngleX) 
				newAngleX = m_targetAngleX;
		}

		if(newAngleX == m_targetAngleX)
			m_targetAngleX = -m_targetAngleX;
		/*if(rand() % 2 == 0)
			m_targetAngleX = -m_targetAngleX;*/

		m_currAngleX = newAngleX;

		Ogre::Radian newAngleY = m_currAngleY;
		if(m_currAngleY < m_targetAngleY)
		{
			newAngleY = newAngleY + (delTime * m_delAngleY / m_tmDelAngleY);
			if(newAngleY > m_targetAngleY) 
				newAngleY = m_targetAngleY;
		}
		else
		{
			newAngleY = newAngleY - (delTime * m_delAngleY / m_tmDelAngleY);
			if(newAngleY < m_targetAngleY) 
				newAngleY = m_targetAngleY;
		}

		if(newAngleY == m_targetAngleY)
			m_targetAngleY = -m_targetAngleY;
		/*if(rand() % 2 == 0)
			m_targetAngleY = -m_targetAngleY;*/

		m_currAngleY = newAngleY;

		Ogre::Quaternion qtrX(m_currAngleX, Ogre::Vector3::UNIT_X);
		Ogre::Quaternion qtrY(m_currAngleY, Ogre::Vector3::UNIT_Y);
		m_nodeBob->setOrientation(qtrX * qtrY);
	}
};