#include "OgreCamera.h"

class AutoCam
{
	Ogre::Camera *m_camera;
	Ogre::SceneNode *m_nodeCamera;

	Ogre::Vector3 m_vctOldPosition, m_vctP1, m_vctP2, m_vctNewPosition;
	Ogre::Real m_lerpPosition, m_tmLerpPosition, m_currTmLerpPosition;
	bool m_isChangePosition;
	bool m_isSpline;

	Ogre::Quaternion m_qtrOldDirection, m_qtrNewDirection;
	Ogre::Real m_lerpDirection, m_tmLerpDirection, m_currTmLerpDirection;
	bool m_isChangeDirection;

public:
	AutoCam(Ogre::Camera *camera)
	{
		m_camera = camera;
		if(m_camera != NULL)
			m_nodeCamera = m_camera->getParentSceneNode();

		m_isChangeDirection = false;
		m_isChangePosition = false;
		m_lerpDirection = 1;
	}

	Ogre::Camera *getCamera()
	{
		return m_camera;
	}

	bool isChangePosition()
	{
		return m_isChangePosition;
	}

	bool isChangeDirection()
	{
		return m_isChangeDirection;
	}

	void setIsChangePosition(bool isChangePosition)
	{
		m_isChangePosition = isChangePosition;
	}

	void setIsChangeDirection(bool isChangeDirection)
	{
		m_isChangeDirection = isChangeDirection;
	}

	void setNewDirection(Ogre::Vector3 vctNewDirection)
	{
		Ogre::Quaternion currDirection = m_camera->getOrientation();
		m_camera->lookAt(m_camera->getPosition() + vctNewDirection);
		m_qtrNewDirection = m_camera->getOrientation();
		m_camera->setOrientation(currDirection);
	}

	void changePosition(Ogre::Vector3 vctNewPosition, Ogre::Real tmLerpPosition)
	{
		if(tmLerpPosition <= 0)
		{
			m_nodeCamera->setPosition(vctNewPosition);
			return;
		}

		m_vctOldPosition = m_nodeCamera->getPosition();
		m_vctNewPosition = vctNewPosition;
		m_currTmLerpPosition = 0;
		m_tmLerpPosition = tmLerpPosition;
		m_lerpPosition = 0;
		m_isChangePosition = true;
		m_isSpline = false;
	}

	void changePosition(Ogre::Vector3 vctP1, Ogre::Vector3 vctP2, Ogre::Vector3 vctP3, Ogre::Vector3 vctP4, Ogre::Real tmLerpPosition)
	{
		if(tmLerpPosition <= 0)
		{
			m_nodeCamera->setPosition(vctP4);
			return;
		}

		m_vctOldPosition = vctP1;
		m_vctP1 = vctP2;
		m_vctP2 = vctP3;
		m_vctNewPosition = vctP4;

		m_currTmLerpPosition = 0;
		m_tmLerpPosition = tmLerpPosition;
		m_lerpPosition = 0;
		m_isChangePosition = true;
		m_isSpline = true;
	}

	void changeDirection(Ogre::Vector3 vctNewDirection, Ogre::Real tmLerpDirection)
	{
		m_currTmLerpDirection = 0;
		m_tmLerpDirection = tmLerpDirection;
		/*m_vctOldDirection = m_camera->getOrientation() * Ogre::Vector3(0, 0, -1);
		m_vctNewDirection = vctNewDirection;*/
		m_qtrOldDirection = m_camera->getOrientation();
		m_camera->lookAt(m_camera->getPosition() + vctNewDirection);

		if(tmLerpDirection <= 0)
			return;

		m_qtrNewDirection = m_camera->getOrientation();
		m_camera->setOrientation(m_qtrOldDirection);

		m_lerpDirection = 0;
		m_isChangeDirection = true;
	}

	void update(Ogre::Real delTime)
	{
		if(m_isChangeDirection)
		{
			m_currTmLerpDirection += delTime;
			if(m_currTmLerpDirection > m_tmLerpDirection)
				m_currTmLerpDirection = m_tmLerpDirection;

			m_lerpDirection = m_currTmLerpDirection / m_tmLerpDirection;

			Ogre::Quaternion qtrDir = (m_lerpDirection * m_qtrNewDirection + (1.0f - m_lerpDirection) * m_qtrOldDirection);
			m_camera->setOrientation(qtrDir);

			//Ogre::Vector3 vctDir = (m_lerpDirection * m_vctNewDirection + (1.0f - m_lerpDirection) * m_vctOldDirection);
			//m_camera->setDirection(vctDir);
			//m_camera->lookAt(m_camera->getPosition() + vctDir);

			if(m_currTmLerpDirection == m_tmLerpDirection)
			{
				m_isChangeDirection = false;
			}
		}

		if(m_isChangePosition)
		{
			m_currTmLerpPosition += delTime;
			if(m_currTmLerpPosition > m_tmLerpPosition)
				m_currTmLerpPosition = m_tmLerpPosition;

			m_lerpPosition = m_currTmLerpPosition / m_tmLerpPosition;

			if(m_isSpline)
			{
				Ogre::Vector3 vctPos;
				CubicBezierCurve(&m_vctOldPosition, &m_vctP1, &m_vctP2, &m_vctNewPosition, m_lerpPosition, &vctPos);
				//printf("\nvctPos: (%f, %f, %f)", vctPos.x, vctPos.y, vctPos.z);
				m_nodeCamera->setPosition(vctPos);
			}
			else
			{
				Ogre::Vector3 vctPos = (m_lerpPosition * m_vctNewPosition + (1.0f - m_lerpPosition) * m_vctOldPosition);
				m_nodeCamera->setPosition(vctPos);
			}

			if(m_currTmLerpPosition == m_tmLerpPosition)
			{
				m_isChangePosition = false;
			}
		}
	}

	inline static void CubicBezierCurve(
		Ogre::Vector3 *vct_p1, 
		Ogre::Vector3 *vct_p2, 
		Ogre::Vector3 *vct_p3, 
		Ogre::Vector3 *vct_p4,
		Ogre::Real s,
		Ogre::Vector3 *vct_otp
		)
	{
		FLOAT a = (1 - s) * (1 - s) * (1 - s);
		FLOAT b = 3 * s * (1 - s) * (1 - s);
		FLOAT c = 3 * s * s * (1 - s);
		FLOAT d = s * s * s;

		vct_otp->x = a * vct_p1->x + b * vct_p2->x + c * vct_p3->x + d * vct_p4->x;
		vct_otp->y = a * vct_p1->y + b * vct_p2->y + c * vct_p3->y + d * vct_p4->y;
		vct_otp->z = a * vct_p1->z + b * vct_p2->z + c * vct_p3->z + d * vct_p4->z;
	}
};