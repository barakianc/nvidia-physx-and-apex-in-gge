#pragma once

// GameScreen
#include "GameScreen.h"
#include "DebugText2D.h"
#include "GameCamera.h"

#include "SkyX/SkyX.h"
#include "HydraX/HydraX.h"
#include "HydraX/Modules/ProjectedGrid/ProjectedGrid.h"
#include "ParticleUniverseSystemManager.h"

#include "Bobber.h"
#include "AutoCam.h"

namespace GamePipeGame
{
	class PathGuider
	{
	private:
		Ogre::Vector3 m_arrVctBez[10][4];
		int m_numSplines;
		int m_indxBez;
		Ogre::Real m_tmLerpPosition, m_currTmLerpPosition;

		Ogre::SceneNode *m_targetNode;
		bool m_isEnabled;

	public:
		PathGuider()
		{
			m_targetNode = NULL;
			m_isEnabled = false;
		}

		void setTarget(Ogre::SceneNode *targetNode)
		{
			m_targetNode = targetNode;
		}

		//Max Splines 10
		void setPaths(Ogre::Vector3 *arrSplines, int numSplines, Ogre::Real tmLerpPosition)
		{
			m_tmLerpPosition = tmLerpPosition;
			m_currTmLerpPosition = 0;
			m_indxBez = 0;
			m_numSplines = numSplines;
			for(int itr = 0; itr < m_numSplines; itr++)
			{
				for(int itrSp = 0; itrSp < 4;itrSp++)
				{
					m_arrVctBez[itr][itrSp] = arrSplines[4 * itr + itrSp];
				}
			}
		}

		void setEnabled(bool isEnabled)
		{
			m_isEnabled = isEnabled;
		}

		void update(Ogre::Real delTime)
		{
			if(m_isEnabled)
			{
				Ogre::Real lerpPosition = m_currTmLerpPosition / m_tmLerpPosition;

				int indxBez = m_indxBez;

				Ogre::Vector3 vctPos;
				CubicBezierCurve(&m_arrVctBez[indxBez][0], &m_arrVctBez[indxBez][1], &m_arrVctBez[indxBez][2], &m_arrVctBez[indxBez][3], lerpPosition, &vctPos);
				m_targetNode->setPosition(vctPos);
				
				if(lerpPosition == 1)
				{
					m_currTmLerpPosition = 0;
					lerpPosition = 0;
					m_indxBez = (m_indxBez + 1) % m_numSplines;
				}
				else
				{
					m_currTmLerpPosition += delTime;
					if(m_currTmLerpPosition > m_tmLerpPosition)
						m_currTmLerpPosition = m_tmLerpPosition;
				}
			}
		}

		Ogre::Vector3 getPathTangent()
		{
			Ogre::Real lerpPosition = m_currTmLerpPosition / m_tmLerpPosition;
			int indxBez = m_indxBez;

			Ogre::Vector3 vctPos;
			CubicBezierCurveDbyDs(&m_arrVctBez[indxBez][0], &m_arrVctBez[indxBez][1], &m_arrVctBez[indxBez][2], &m_arrVctBez[indxBez][3], lerpPosition, &vctPos);
			return vctPos;
		}

		inline static void CubicBezierCurve(
		Ogre::Vector3 *vct_p1, 
		Ogre::Vector3 *vct_p2, 
		Ogre::Vector3 *vct_p3, 
		Ogre::Vector3 *vct_p4,
		Ogre::Real s,
		Ogre::Vector3 *vct_otp
		)
		{
			FLOAT a = (1 - s) * (1 - s) * (1 - s);
			FLOAT b = 3 * s * (1 - s) * (1 - s);
			FLOAT c = 3 * s * s * (1 - s);
			FLOAT d = s * s * s;

			vct_otp->x = a * vct_p1->x + b * vct_p2->x + c * vct_p3->x + d * vct_p4->x;
			vct_otp->y = a * vct_p1->y + b * vct_p2->y + c * vct_p3->y + d * vct_p4->y;
			vct_otp->z = a * vct_p1->z + b * vct_p2->z + c * vct_p3->z + d * vct_p4->z;
		}

		
	inline static void CubicBezierCurveDbyDs(
		Ogre::Vector3 *vct_p1, 
		Ogre::Vector3 *vct_p2, 
		Ogre::Vector3 *vct_p3, 
		Ogre::Vector3 *vct_p4, 
		FLOAT s,
		Ogre::Vector3 *vct_otp
		)
	{
		FLOAT a = -3.0f * (1 - s) * (1 - s);
		FLOAT b = 3.0f * (1 - s) * (1 - 3.0f * s);
		FLOAT c = 3 * s * (2.0f - 3.0f * s);
		FLOAT d = 3.0f * s * s;

		vct_otp->x = a * vct_p1->x + b * vct_p2->x + c * vct_p3->x + d * vct_p4->x;
		vct_otp->y = a * vct_p1->y + b * vct_p2->y + c * vct_p3->y + d * vct_p4->y;
		vct_otp->z = a * vct_p1->z + b * vct_p2->z + c * vct_p3->z + d * vct_p4->z;
	}
	};

	class RendererDemo : public GamePipe::GameScreen
	{
	private:

		enum EAutoCamTarget {YACHT, LIGHTHOUSE, STEAMER};
		EAutoCamTarget m_autoCamTarget;
		Ogre::Viewport *m_viewPortFreeCam;
		Ogre::Viewport *m_viewPortPlayer;
		Ogre::Camera *m_camFreeCam;
		Ogre::Camera *m_camPlayer;

		Ogre::SceneNode *m_nodeActiveCam;
		Ogre::SceneNode *m_nodePlayerCam;

		Ogre::SceneNode *m_nodePlayerCamParent;
		Ogre::SceneNode *m_nodeAutoYacht;

		Ogre::SceneManager *m_mainScene;

		bool m_isFreeCamMain, m_isSubCamReq;

		Ogre::Real m_tmToAutoCam;
		PathGuider m_pathGuider;
		PathGuider m_YachtGuider;
		int m_idCamFocus; 

		Bobber m_bobBoat;

		GameCamera *mCam;
		CameraType mCamType;

		SkyX::SkyX *mSkyX;
		Hydrax::Hydrax *mHydraX;

		enum ESkyXOption {RADIUSIN, RADIUSOUT, TIMEMULTIPLIER, EXPOSURE, WINDSPEED, WINDDIRECTION, NOISESCALE, NONESK};
		ESkyXOption mSkyXOption;

		enum EHydraXOption {WAVESTRENGTH, NONEHY};
		EHydraXOption mHydraXOption;

		bool m_isUp, m_isDown;

		bool RendererDemo::myFreeCameraMouseMovement(const GIS::MouseEvent& mouseEvent, Ogre::Camera *pcamera);
		bool RendererDemo::myFreeCameraKeyMovement(Ogre::Camera *camera);

		void updateAutoCamTarget()
		{
			if(m_autoCamTarget == YACHT)
			{
				m_nodeActiveCam->getParentSceneNode()->removeChild(m_nodeActiveCam);
				Ogre::SceneNode *nodeYacht = GetDefaultSceneManager()->getSceneNode("MegaYachtBobSceneNode");
				nodeYacht->addChild(m_nodeActiveCam);
			}
			else if(m_autoCamTarget == LIGHTHOUSE)
			{
				m_nodeActiveCam->getParentSceneNode()->removeChild(m_nodeActiveCam);
				Ogre::SceneNode *nodeYacht = GetDefaultSceneManager()->getSceneNode("TopPIslandSceneNode");
				nodeYacht->addChild(m_nodeActiveCam);
			}
			else if(m_autoCamTarget == STEAMER)
			{
				m_nodeActiveCam->getParentSceneNode()->removeChild(m_nodeActiveCam);
				Ogre::SceneNode *nodeYacht = GetDefaultSceneManager()->getSceneNode("YelloSteamPSceneNode");
				nodeYacht->addChild(m_nodeActiveCam);
			}
		}

		ParticleUniverse::ParticleSystem *mPSys;
		ParticleUniverse::ParticleSystem *fireParticle;

		ParticleUniverse::ParticleSystem *fireWorks;
		ParticleUniverse::ParticleSystem *fireWorks2;

		ParticleUniverse::ParticleSystem *smokePart;
		bool fireWorks1On;
		bool fireWorks2On;
		float fireWorksTimer;
		bool isFireworks;
	protected:
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		bool OnKeyRelease(const GIS::KeyEvent &keyEvent);
		bool OnMouseMove(const GIS::MouseEvent& mouseEvent);

		void getSkyXValue(ESkyXOption skyxOption, Ogre::Real *pval = NULL, Ogre::Radian *pAngle = NULL);
		void setSkyXValue(ESkyXOption skyxOption, Ogre::Real pval, Ogre::Radian pAngle);

		void getHydraxValue(EHydraXOption hydraxOption, float *pval);
		void setHydraxValue(EHydraXOption hydraxOption, float val);
		
		void updateHUD();
		void RendererDemo::updateEnvironmentLighting(Ogre::Camera *camera, bool isForceDisableShadows = false);

		void initViewPort(bool isFreeCamMain, bool isSubCamRequired)
		{
			EnginePtr->GetRenderWindow()->removeAllViewports();

			if(isFreeCamMain)
			{
				m_viewPortFreeCam = EnginePtr->GetRenderWindow()->addViewport(m_camFreeCam, 0, 0, 0, 1.0f, 1.0f);
				if(isSubCamRequired)
				{
					m_viewPortPlayer = EnginePtr->GetRenderWindow()->addViewport(m_camPlayer, 1, 0, 0.5f, 0.5f, 0.5f);
				}
				else
				{
					m_viewPortPlayer = NULL;
				}
			}
			else
			{
				m_viewPortPlayer = EnginePtr->GetRenderWindow()->addViewport(m_camPlayer, 0, 0, 0, 1.0f, 1.0f);
				if(isSubCamRequired)
				{
					m_viewPortFreeCam = EnginePtr->GetRenderWindow()->addViewport(m_camFreeCam, 1, 0, 0.5f, 0.5f, 0.5f);
				}
				else
				{
					m_viewPortFreeCam = NULL;
				}
			}

			if(m_viewPortFreeCam != NULL)
				m_camFreeCam->setAspectRatio(static_cast<Ogre::Real>(m_viewPortFreeCam->getActualWidth()) / static_cast<Ogre::Real>(m_viewPortFreeCam->getActualHeight()));
			if(m_viewPortPlayer != NULL)
				m_camPlayer->setAspectRatio(static_cast<Ogre::Real>(m_viewPortPlayer->getActualWidth()) / static_cast<Ogre::Real>(m_viewPortPlayer->getActualHeight()));
		}

		void augmentBezier(Ogre::Vector3 *vctX1, Ogre::Vector3 *vctP1, Ogre::Vector3 *vctP2, Ogre::Vector3 *vctX2)
		{
			Ogre::Vector3 vctP = (*vctP1 + *vctP2) / 2;
			*vctP1 = vctP;
			*vctP2 = vctP;
			Ogre::Vector3 vctX1D = vctP + (vctP - *vctX1);
			Ogre::Vector3 vctX2D = vctP + (vctP - *vctX2);
			*vctX1 = (*vctX1 + vctX2D) / 2;
			*vctX2 = (*vctX2 + vctX1D) / 2;
		}

	public:
		RendererDemo(std::string name);
		~RendererDemo() {}
	};
}