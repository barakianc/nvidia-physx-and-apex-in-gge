#include "StdAfx.h"
// FlashGuiDemo1
#include "FlashGuiDemo1.h"

// GameScreen
#include "GameScreen.h"
// Main
#include "Main.h"
// Engine
//#include "Engine.h"


namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// FlashGuiDemo1(std::string name)
	//////////////////////////////////////////////////////////////////////////

	FlashGuiDemo1::FlashGuiDemo1(std::string name) : GamePipe::GameScreen(name)
	{
		m_v3CameraPosition = Ogre::Vector3(0, 0, 50);
		m_guiMgr = new DemoGuiManager();

		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("NxOgre");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("NxOgre");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.0000, 1.0000, 0.0000, 1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		// Create Static Geometry: Floor; we are just adding a floor mesh where the floor is
		Ogre::StaticGeometry* pStaticGeometry;
		pStaticGeometry = pSceneManager->createStaticGeometry("Grid");
		pStaticGeometry->addEntity(pSceneManager->createEntity("nx.floor", "nx.floor.mesh"), Ogre::Vector3(0, -0.05f, 0), Ogre::Quaternion(1, 0, 0, 0), Ogre::Vector3(20, 0, 20));
		pStaticGeometry->build();

		ogreHead=pSceneManager->createEntity("OgreHead", "ogrehead.mesh");
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(ogreHead);
		ogreHead->getParentNode()->setPosition(0,0,-50);

		readyToExit = 0;
		wireframe=false;
		ammuCount=10;
		maxAmmu=10;
		totalSeconds=300;
		text=0;
		choice=0;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::Destroy()
	{

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		m_guiMgr->SetViewport(viewport);
		CreateHUDElements();

		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	// CreateHUDElements()
	//////////////////////////////////////////////////////////////////////////
	void FlashGuiDemo1::CreateHUDElements()
	{
		// Add a slider for zooming
		m_guiMgr->AddSlider("Zoom",10/800.0f,10/600.0f,"Zoom",FlashDelegate(this, &FlashGuiDemo1::OnSliderChange), NULL);
		// Add a checkbox for toggling wireframe mode
		m_guiMgr->AddCheckBox("WireframeMode",500/800.f ,10/600.0f,"Wireframe",FlashDelegate(this, &FlashGuiDemo1::OnCheckBoxClick), NULL);
		// Adding a bar to indicate ammunition
		// maxAmmu denoted the total amount to be indicated
		ammu=m_guiMgr->AddBar("Ammunition",200/800.f,60/600.0f,maxAmmu, NULL);
		// Add timer, start it with totalSeconds
		m_guiMgr->AddTimer("timer",10/800.0f,550/600.0f,totalSeconds, NULL);	
		// Add a back button, and link it with GoBack funtion
		m_guiMgr->AddCircularButton("back",700/800.0f,520/600.0f,"BACK",FlashDelegate(this, &FlashGuiDemo1::GoBack), NULL);
		// Add Toggle Pause button, and link it with Pause funtion 
		m_guiMgr->AddToggleButton("pause",350/800.0f,550/600.0f,"PAUSE",FlashDelegate(this, &FlashGuiDemo1::Pause), NULL);
		// Add a small message box
		m_guiMgr->AddMessageBox("Message",300/800.0f,300/600.0f," Press U for Updates", NULL);
		// Add a update text
		overlayText=m_guiMgr->AddOverlayText("Updates",550/800.0f,20/600.0f,"Press O to FIRE", NULL);
		// Add simple label text 
		m_guiMgr->AddLabelText("General Label" , 0/800.f , 500/600.0f , "LEVEL 1", NULL);
		// Add a Double button to change the camera view.
		cameraView=m_guiMgr->AddDoubleButton("Camera View",0/800.0f,150/600.0f,"NORMAL VIEW",FlashDelegate(this, &FlashGuiDemo1::DoubleButtonclick), NULL);
		// Add a combo box
		// and add items to it.
		combo=m_guiMgr->AddComboBox("Combo box",0/800.0f,150/600.0f,FlashDelegate(this, &FlashGuiDemo1::ComboBoxChange), NULL);
		m_guiMgr->AddYouTubeVideo("youtubevideo",0/800.0f,200/600.0f,"untitled.swf", NULL);
		combo->callFunction("additems", Args("newmenu.swf"));
		combo->callFunction("additems", Args("flashmaterial.swf"));
		combo->callFunction("additems", Args("circularbutton.swf"));
		combo->callFunction("additems", Args("alertmaterial.swf"));
		combo->callFunction("additems", Args("untitled.swf"));
		combo->callFunction("additems", Args("untitled2.swf"));
		//Create a .swf material
		flashMaterial = m_guiMgr->CreateFlashMaterial("Flash Material");
		flashMaterial->load("alertmaterial.swf");
		// Set flash material
		ogreHead->setMaterialName(flashMaterial->getMaterialName());


	}


	//////////////////////////////////////////////////////////////////////////
	// ComboBoxChange()
	//////////////////////////////////////////////////////////////////////////
	FlashValue FlashGuiDemo1::ComboBoxChange(FlashControl* caller, const Arguments& args)
	{
		// On combo boxchange, change the ogre material
		flashMaterial->load(args.at(0).getString());
		return FLASH_VOID;
	}

	//////////////////////////////////////////////////////////////////////////
	// DoubleButtonclick()
	//////////////////////////////////////////////////////////////////////////
	FlashValue FlashGuiDemo1::DoubleButtonclick(FlashControl* caller, const Arguments& args)
	{
		// change the camera view on double button click
		int value = int(args.at(0).getNumber());

		if(value==1)
			choice++;
		else
			choice--;

		if(choice<0)
			choice=2;

		choice%=3;

		switch(choice)

		{
		case 0 : {
			cameraView->callFunction("setText", Args("Eagle's View"));
			m_v3CameraPosition=Ogre::Vector3(0,50,160);
			break;}
		case 1 : {
			cameraView->callFunction("setText", Args("Normal view"));
			m_v3CameraPosition= Ogre::Vector3(0.0000, 0.0000, 160.0000);

			break;}
		case 2 : {
			cameraView->callFunction("setText", Args("Point of interest"));

			m_v3CameraPosition=Ogre::Vector3(0,5,20);
			break;}
		}

		return FLASH_VOID;
	}

	//////////////////////////////////////////////////////////////////////////
	// Pause()
	//////////////////////////////////////////////////////////////////////////
	FlashValue FlashGuiDemo1::Pause(FlashControl* caller, const Arguments& args)
	{

		// Pause/resume
		if(!m_guiMgr->isPaused())
		{
			m_guiMgr->CreatePauseMenu();
		}
		else
		{
			m_guiMgr->ResumePauseMenu();
		}

		return FLASH_VOID;
	}

	//////////////////////////////////////////////////////////////////////////
	// GoBack()
	//////////////////////////////////////////////////////////////////////////
	FlashValue FlashGuiDemo1::GoBack(FlashControl* caller, const Arguments& args)
	{

		// To exit the screen
		readyToExit = 1;
		return FLASH_VOID;
	}



	//////////////////////////////////////////////////////////////////////////
	// OnCheckBoxClick()
	//////////////////////////////////////////////////////////////////////////
	FlashValue FlashGuiDemo1::OnCheckBoxClick(FlashControl* caller, const Arguments& args)
	{
		if(!wireframe)
		{ 
			GetActiveCamera()->setPolygonMode(Ogre::PM_WIREFRAME);
			wireframe=true;
		}
		else
		{
			GetActiveCamera()->setPolygonMode(Ogre::PM_SOLID);
			wireframe=false;
		}


		return FLASH_VOID;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnSliderChange()
	//////////////////////////////////////////////////////////////////////////
	FlashValue FlashGuiDemo1::OnSliderChange(FlashControl* caller, const Arguments& args)
	{
		Real value = args.at(0).getNumber();
		m_v3CameraPosition = Ogre::Vector3(m_v3CameraPosition.x,m_v3CameraPosition.y,200-value*2);
		return FLASH_VOID;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::Update()
	{
		FreeCameraMovement();
		if (readyToExit > 0)
		{
			FlashGUIPtr->FlashGUIManager->destroyAllControls();
			FlashGUIPtr->screenchange = 1;
			readyToExit = 0;
		}
		

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress()
	//////////////////////////////////////////////////////////////////////////
	bool  FlashGuiDemo1::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_O:
			{
				if(ammuCount>1)
				{
					ammuCount--;
					ammu->callFunction("update", Args(ammuCount));
				}
				else
				{
					ammuCount--;
					if(ammuCount==0)
					{
						alert=m_guiMgr->AddAlertText("alert",220,100, NULL);	
						ammu->callFunction("update", Args(ammuCount));
					}	  
				}

				break;
			}
		case GIS::KC_R:
			{       // To reload
				if(ammuCount<1)
				{
					m_guiMgr->RemoveFlashControl(alert);}
				ammuCount=maxAmmu;
				ammu->callFunction("update", Args(ammuCount));

				break;
			}
		case GIS::KC_ESCAPE:
			{

				readyToExit = 1;
				break;
			}

		case GIS::KC_U:
			{   // Change update text
				text++;
				text%=4;
				if(text==0)
					overlayText->callFunction("setText", Args("Random Text, Move slider to ZOOM in and out"));
				else if(text==1)
					overlayText->callFunction("setText", Args("Tip: Press R to RELOAD"));
				else if(text==2)
					overlayText->callFunction("setText", Args("TIP: Use Checkbox to toggle wire frame mode, xxx ,xxx ,xxx xxx xxx XXX OOO XXX OOO XXX OOO XXX OOO"));
				else if(text==3)
					overlayText->callFunction("setText", Args("Press O to FIRE"));
				break;

			}

		case GIS::KC_P:
			{
				// Pause /Resume						
				if(!m_guiMgr->isPaused())
					m_guiMgr->CreatePauseMenu();
				else
					m_guiMgr->ResumePauseMenu();
				break;
			}
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Overriding OnMouseMove()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo1::OnMouseMove(const GIS::MouseEvent& mouseEvent)
	{
		return true;
	}
}