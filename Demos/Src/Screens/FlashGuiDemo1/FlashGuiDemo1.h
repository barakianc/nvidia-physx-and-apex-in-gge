#pragma once



// Engine
//#include "Engine.h"
#include "GUIManager/DemoGuiManager.h"
#include "Hikari.h"

using namespace Hikari;

namespace GamePipeGame
{
	class FlashGuiDemo1 : public GamePipe::GameScreen
	{
	private:
		DemoGuiManager* m_guiMgr;
		int readyToExit;
	protected:

	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

	public:
		int ammuCount;
		int maxAmmu;
		int totalSeconds;
		int text;
		int choice;
		Ogre::Entity* ogreHead;
		FlashControl * ammu;
		FlashControl * alert;
		FlashControl * overlayText;
		FlashControl * cameraView;
		FlashControl * combo;
		FlashControl * flashMaterial;
		bool wireframe;
		void CreateHUDElements();
		FlashValue DoubleButtonclick(FlashControl* caller, const Arguments& args);
		FlashValue OnSliderChange(FlashControl* caller, const Arguments& args);
		FlashValue OnCheckBoxClick(FlashControl* caller, const Arguments& args);
		FlashValue GoBack(FlashControl* caller, const Arguments& args);
		FlashValue Pause(FlashControl* caller, const Arguments& args);
		FlashValue ComboBoxChange(FlashControl* caller, const Arguments& args);
		bool OnKeyPress(const GIS::KeyEvent& keyEvent);
		bool OnMouseMove(const GIS::MouseEvent& mouseEvent);
		FlashGuiDemo1(std::string name);
		~FlashGuiDemo1() {}

	};
}