#pragma once
// GameScreen
#include "GameScreen.h"

// GameObject
#include "GameObject.h"
#include "GameObjectManager.h"

// AI
#include "AIWorldManager.h"

namespace GamePipeGame
{
	class AIDemoScreen:public GamePipe::GameScreen
	{
	protected:
		Ogre::ManualObject* mManualObj;
		AIWorldManager* mAIWorldManager;
		GameObject* ground;
		GameObject* mDynamicChest;
		char* mGameObjectType;
		Ogre::String mBehavior;

		int mCurrTargetID[4];
		float mPathFollowTargets[12][3];
		GameObject* mPathFollowingAgent[4];
		GameObject* mPursuitAgent;
		GameObject* mWanderingAgents[5];
		GameObject* mPathFindingAgent;

		GamePipe::Input* m_prevInputPtr;
		dtCrowd* mPathFollowingCrowd;
		dtCrowd* mWanderingCrowd;
		dtCrowd* mPathFindingCrowd;

		bool m_mouseLeftReleased, m_mouseRightReleased;
		int mDebugState;

		Ogre::SceneNode* mActiveSign;

	public:
		AIDemoScreen(std::string name);
		~AIDemoScreen(void);

		bool Initialize();
		bool Destroy();
		bool Show();
		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		bool OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);
		bool OnMouseRelease(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);
		bool HandleKeyboardEvents();

		bool LoadResources();
		bool UnloadResources();

		bool AddPreDefinedAgents();

	};
}

