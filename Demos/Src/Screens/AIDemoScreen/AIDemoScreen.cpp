#include "StdAfx.h"
#include "AIDemoScreen.h"

// Engine
#include "Engine.h"

void detourDrawCross( Ogre::ManualObject* dd, const float x, const float y, const float z, const float size, Ogre::ColourValue col, const float lineWidth, Ogre::String materialName);


namespace GamePipeGame
{
	AIDemoScreen::AIDemoScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
		mGameObjectType = "GRAPHICS_OBJECT";
		mBehavior = "none";
		mCurrTargetID[0] = 1;
		mCurrTargetID[1] = 2;
		mCurrTargetID[2] = 4;
		mCurrTargetID[3] = 6;
		mDebugState = 0;
	}


	AIDemoScreen::~AIDemoScreen(void)
	{
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::Initialize()
	{
		AIWorldManager::useRecast = true;

		//Set background color
		Ogre::ColourValue backColor(0.9f, 0.9f, 0.86f, 1.0000f);
		SetBackgroundColor(backColor);

		// Set up camera speed
		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;

		// Set shadow type
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(1.f, 1.f, 1.f, 1.f));
		pSceneManager->setFog(Ogre::FOG_LINEAR, backColor, 0.0f, 60, 200);
		//pSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

		// Create lights
		Ogre::Light* directionalLight_1 = pSceneManager->createLight("directionalLight_1");
		directionalLight_1->setType(Ogre::Light::LT_DIRECTIONAL);
		directionalLight_1->setDiffuseColour(Ogre::ColourValue(0.9f, 0.9f, 0.8f));
		directionalLight_1->setSpecularColour(Ogre::ColourValue(0.3f, 0.3f, 0.25f));
		directionalLight_1->setDirection(Ogre::Vector3( 0, -1, 0.5));

		Ogre::Light* directionalLight_2 = pSceneManager->createLight("directionalLight_2");
		directionalLight_2->setType(Ogre::Light::LT_DIRECTIONAL);
		directionalLight_2->setDiffuseColour(Ogre::ColourValue(0.65f, 0.65f, 0.65f));
		directionalLight_2->setSpecularColour(Ogre::ColourValue(0.25f, 0.25f, 0.25f));
		directionalLight_2->setDirection(Ogre::Vector3( 0, 1, -0.5));

		// Create terrain
		ground = new GameObject("islands","island.mesh","island.hkx", PHYSICS_FIXED, COLLISION_SHAPE_BOX);

		// Build navigation mesh
		mAIWorldManager =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->getAIWorldManager();
		mAIWorldManager->setNaviParameters(0.25f, 0.3f, 2.0f, 0.6f, 0.9f, 45.0f, 8.0f, 20.0f);
		mAIWorldManager->setDrawNaviMesh(false);
		mAIWorldManager->CreateStaticWorld();

		// Add palm tree tops
		Ogre::Entity* treeTop = GetDefaultSceneManager()->createEntity("TreeTop_Entity", "island_palms.mesh"); 
		Ogre::SceneNode* treeTopNode = GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode( "TreeTop_Node", Ogre::Vector3(0, 0, 0)); 
		treeTopNode->attachObject(treeTop);

		// Add active sign
		Ogre::Entity* activeSignEntity = GetDefaultSceneManager()->createEntity("ActiveSign_Entity", "ActiveSign.mesh"); 
		activeSignEntity->setMaterialName("CyanMaterial");
		mActiveSign = GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode( "ActiveSign_Node", Ogre::Vector3(0, 0, 0)); 
		mActiveSign->attachObject(activeSignEntity);
		mActiveSign->scale(0.16f, 0.2f, 0.16f);
		mAIWorldManager->setActiveSign(mActiveSign);
	
		// Add a dynamic obstacle
		mDynamicChest = new GameObject("chest","chest.mesh","", GRAPHICS_OBJECT, COLLISION_SHAPE_BOX);
		mDynamicChest->scale(0.2f, 0.2f, 0.2f);
		mDynamicChest->setPosition(33, 21, -20);

		// Initialize input data
		this->m_prevInputPtr = NULL;
		this->m_mouseLeftReleased = false;
		this->m_mouseRightReleased = false;
		
		// Add a path-following agent
		AddPreDefinedAgents();

		// Set instructions
		Ogre::String text("AI Demo");
		text += "\nReset target: right mouse button";
		text += "\nAdd new agent: left mouse button";
		text += "\nSwitch active agent: space bar";
		//text += "\n\nSwitch between GameObject type:\n\tGRAPHICS_OBJECT\n\tANIMATED_CHARACTER_RIGIDBODY\n";

		UtilitiesPtr->Add("AIDemo", new GamePipe::DebugText(text, (Ogre::Real)0.005, (Ogre::Real)0.005, 
			(Ogre::Real)3.0f, Ogre::ColourValue(0.0f, 0.0f, 0.0f, 1.00f), Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("AgentsCount", new GamePipe::DebugText("Agent Count", (Ogre::Real)0.95, (Ogre::Real)0.05, (Ogre::Real)3, Ogre::ColourValue(0.0f, 0.0f, 0.8f, 1.00f), Ogre::TextAreaOverlayElement::Right));
		//UtilitiesPtr->Add("ActiveCrowd", new GamePipe::DebugText("Active Crowd", (Ogre::Real)0.95, (Ogre::Real)0.15, (Ogre::Real)3, Ogre::ColourValue(1.0f, 1.0f, 0.0f, 1.00f), Ogre::TextAreaOverlayElement::Right));
		//UtilitiesPtr->Add("DebugInfo_wander", new GamePipe::DebugText("DebugInfo", (Ogre::Real)0.005, (Ogre::Real)0.90, (Ogre::Real)3, Ogre::ColourValue(1.0f, 0.0f, 0.0f, 1.00f), Ogre::TextAreaOverlayElement::Left));
		//UtilitiesPtr->Add("AgentInfo_wander", new GamePipe::DebugText("AgentInfo", (Ogre::Real)0.95, (Ogre::Real)0.90, (Ogre::Real)3, Ogre::ColourValue(1.0f, 0.0f, 0.0f, 1.00f), Ogre::TextAreaOverlayElement::Right));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::Show()
	{
		// Set Camera
		Ogre::Camera* defCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(defCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);
		defCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		// Set initial mouse position
		unsigned int width, height, depth;
		int left, top;
		EnginePtr->GetRenderWindow()->getMetrics(width, height, depth, left, top);
		SetCursorPos((int) (0.5f*width + left), (int)(0.5f*height + top));


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::Destroy()
	{		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::Update()
	{
		// Update Camera
		FreeCameraMovement();

		// Handle keyboard events
		HandleKeyboardEvents();

		// Update sample simulation
		if (mAIWorldManager)
			mAIWorldManager->UpdateWorld(EnginePtr->GetDeltaTime());
		/*

		for ( int i = 0; i < 4; i++ )
		{
			if (mPathFollowingAgent[i]->getdtAIAgent()->isNearGoal(4.0f))
			{
				if ( mCurrTargetID[i] == 7 )
					mCurrTargetID[i] = 0;
				else
					mCurrTargetID[i]++;

				//int currActiveCrowdID = mAIWorldManager->getActiveCrowdId();
				//mAIWorldManager->setActiveCrowd(mPathFollowingCrowd->getCrowdID());
				//mAIWorldManager->addNewTarget(mPathFollowTargets[mCurrTargetID[i]], "arrive");
				//mAIWorldManager->setActiveCrowd(currActiveCrowdID);
				mPathFollowingAgent[i]->getdtAIAgent()->
					setSeekAndArriveSteeringBehavior(mPathFollowTargets[mCurrTargetID[i]]);
			}
		}
		*/

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();

		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		if (mAIWorldManager)
		{
			mAIWorldManager->RenderWorld();

			if ( mAIWorldManager->getDrawNaviMesh() == true )
			{
			}
			// Draw all the targets
			/*for ( int i = 0; i < 8; i++ )
			{
				detourDrawCross(mAIWorldManager->getManualObject(), mPathFollowTargets[i][0], 
					mPathFollowTargets[i][1] + 0.5f, mPathFollowTargets[i][2], 1.0, Ogre::ColourValue(1.0f,1.0f,1.0f,0.75f), 
					3.0f, "MagentaMaterial");
			}*/

		}

		// Output debug information
		Ogre::String text = Ogre::String();
		text = "Agent Count: " + Ogre::StringConverter::toString(dtCrowd::getAgentCountAllCrowds());
		text += "\nActive Crowd: " + Ogre::StringConverter::toString(this->mAIWorldManager->getActiveCrowdId());
		//text += "\nActive Agent: " + Ogre::StringConverter::toString(this->mAIWorldManager->getActiveAgentId());
		UtilitiesPtr->GetDebugText("AgentsCount")->SetText(text);

		//if ( mGameObjectType == "GRAPHICS_OBJECT" )
		//	text = "\nGRAPHICS_OBJECT";
		//else if ( mGameObjectType == "ANIMATED_CHARACTER_RIGIDBODY" )
		//	text = "\nANIMATED_CHARACTER_RIGIDBODY";
		//text += "\nSteering Behavior: " + mBehavior;
		//UtilitiesPtr->GetDebugText("ActiveCrowd")->SetText(text);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::OnKeyPress( const GIS::KeyEvent &keyEvent )
	{
		switch(keyEvent.key)
		{
		case GIS::KC_SPACE:
			{
				if ( mAIWorldManager->getActiveAgentId() != -1 )
				{
					mAIWorldManager->getActiveAgent()->setDebug(false, false);
				}
				
				int agentCount = mAIWorldManager->getCrowd( mAIWorldManager->getActiveCrowdId() )->getActiveAgentsCount();
				int activeAgentId = mAIWorldManager->getActiveAgentId();
				if ( activeAgentId < (agentCount - 1) )
				{
					mAIWorldManager->setActiveAgent( activeAgentId + 1 );
				}
				else
				{
					mAIWorldManager->setActiveAgent(0);
				}

				break;
			}
		case GIS::KC_TAB:
			{
				if ( mGameObjectType == "GRAPHICS_OBJECT" )		
				{
					mGameObjectType = "ANIMATED_CHARACTER_RIGIDBODY";
				}
				else if ( mGameObjectType == "ANIMATED_CHARACTER_RIGIDBODY" )
				{
					mGameObjectType = "GRAPHICS_OBJECT";
				}
				break;
			}
		case GIS::KC_ESCAPE:
			{
				// Delete the world and all the crowds when exist
				UtilitiesPtr->Remove("AIDemo");
				UtilitiesPtr->Remove("AgentsCount");
				mAIWorldManager->deleteWorld();
				Close();
				break;
			}
		case GIS::KC_UP:
			{
				Ogre::Vector3 pos;
				pos = mDynamicChest->getPosition();
				pos.z -= 2;
				mDynamicChest->setPosition(pos.x, pos.y, pos.z);
				break;
			}
		case GIS::KC_DOWN:
			{
				Ogre::Vector3 pos;
				pos = mDynamicChest->getPosition();
				pos.z += 2;
				mDynamicChest->setPosition(pos.x, pos.y, pos.z);
				break;
			}
		case GIS::KC_LEFT:
			{
				Ogre::Vector3 pos;
				pos = mDynamicChest->getPosition();
				pos.x -= 2;
				mDynamicChest->setPosition(pos.x, pos.y, pos.z);
				break;
			}
		case GIS::KC_RIGHT:
			{
				Ogre::Vector3 pos;
				pos = mDynamicChest->getPosition();
				pos.x += 2;
				mDynamicChest->setPosition(pos.x, pos.y, pos.z);
				break;
			}
		case GIS::KC_M:
			{
				Ogre::Vector3 pos;
				pos = mDynamicChest->getPosition();
				pos.y -= 0.5f;
				mDynamicChest->setPosition(pos.x, pos.y, pos.z);
				break;
			}
		case GIS::KC_N:
			{
				Ogre::Vector3 pos;
				pos = mDynamicChest->getPosition();
				pos.y += 0.5f;
				mDynamicChest->setPosition(pos.x, pos.y, pos.z);
				break;
			}
		case GIS::KC_P:
			{			

				if ( mDebugState == 1 )
				{
					int crowdCount = mAIWorldManager->getCrowdsCount();
					for ( int i = 0; i < crowdCount; i++ )
					{
						int agentCount = mAIWorldManager->getCrowd(i)->getActiveAgentsCount();
						for ( int j = 0; j < agentCount; j++ )
						{
							mAIWorldManager->getCrowd(i)->getAgent(j)->toggleDebugInfo(true);
						}
					}
					mAIWorldManager->setDebug(true);

				}
				else if( mDebugState == 2 )
				{
					//if ( mAIWorldManager->getDrawNaviMesh() == true )
					//	mAIWorldManager->setDrawNaviMesh(false);
					//else
					mAIWorldManager->setDrawNaviMesh(true);
				}
				else if ( mDebugState == 0 )
				{
					mAIWorldManager->setDrawNaviMesh(false);
					int crowdCount = mAIWorldManager->getCrowdsCount();
					for ( int i = 0; i < crowdCount; i++ )
					{
						int agentCount = mAIWorldManager->getCrowd(i)->getActiveAgentsCount();
						for ( int j = 0; j < agentCount; j++ )
						{
							mAIWorldManager->getCrowd(i)->getAgent(j)->toggleDebugInfo(false);
						}
					}			
					mAIWorldManager->setDebug(false);
				}
				
				mDebugState = (mDebugState + 1)%3;

				break;
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnMousePress()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::OnMousePress( const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId )
	{
		// get window height and width
		Ogre::Real screenWidth = (Ogre::Real) EnginePtr->GetRenderWindow()->getWidth();
		Ogre::Real screenHeight = (Ogre::Real) EnginePtr->GetRenderWindow()->getHeight();

		// convert to 0-1 offset
		Ogre::Real offsetX = InputPtr->GetMouseState().X.abs / screenWidth;
		Ogre::Real offsetY = InputPtr->GetMouseState().Y.abs / screenHeight;

		// Get ray from camera
		float pHitPointDistance;			
		Ogre::Ray pRay = GetActiveCamera()->getCameraToViewportRay(offsetX, offsetY);
		bool processHitTestShift = InputPtr->IsKeyDown(GIS::KC_RSHIFT) || InputPtr->IsKeyDown(GIS::KC_LSHIFT);

		if (mAIWorldManager->getInputGeom()->raycastMesh(pRay, pHitPointDistance, true))
		{
			float pHitPoint[3];                                                                                                                                                                                                                                                                                          
			pHitPoint[0] = pRay.getOrigin().x + pRay.getDirection().x * pHitPointDistance;
			pHitPoint[1] = pRay.getOrigin().y + pRay.getDirection().y * pHitPointDistance;
			pHitPoint[2] = pRay.getOrigin().z + pRay.getDirection().z * pHitPointDistance;


			if (mouseButtonId == GIS::MB_Right)
			{
				// Reset target for the path finding crowd
				int currActiveCrowdID = mAIWorldManager->getActiveCrowdId();
				mAIWorldManager->setActiveCrowd(mPathFindingCrowd->getCrowdID());
				mAIWorldManager->addNewTarget(pHitPoint, "arrive");
				mAIWorldManager->setActiveCrowd(currActiveCrowdID);
			}
			else if (mouseButtonId == GIS::MB_Left)
			{
				// Add new agent to the active crowd				
				std::string name = mAIWorldManager->getNextAgentName("Sinbad");
				GameObject* agentObj = new GameObject( name, "Sinbad.mesh", "", GRAPHICS_OBJECT, DEFAULT_COLLISION_SHAPE, "", true);
				agentObj->getdtAIAgent()->setAgentPositionAbsolute(pHitPoint);
				agentObj->getdtAIAgent()->scaleAgent(0.2f, 0.2f, 0.2f);
				agentObj->getdtAIAgent()->setYOffset(1.15f);
				agentObj->getdtAIAgent()->setDebugBehavior("path finding");

				if ( mDebugState == 1 || mDebugState == 2 )
				{
					agentObj->getdtAIAgent()->toggleDebugInfo(true);
				}
				else if ( mDebugState == 0 )
				{
					agentObj->getdtAIAgent()->toggleDebugInfo(false);
				}
			}			

			//mAIWorldManager->setActiveCrowd(mWanderingCrowd->getCrowdID());
			//dtAIAgent* wanderingAgent = mAIWorldManager->addNewAgent(pHitPoint, "Sinbad.mesh", "", "Sinbad", "", "wander", "GRAPHICS_OBJECT");
			//wanderingAgent->scaleAgent(0.2f, 0.2f, 0.2f);
			//wanderingAgent->setYOffset(1.15f);
			//wanderingAgent->setSinbadMaterial("Sinbad/Clothes_orange");
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// AddPreDefinedAgents()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::AddPreDefinedAgents()
	{
		// Initialize crowds
		mPathFollowingCrowd = mAIWorldManager->addNewCrowd("MagentaMaterial");
		
		
		// Set target list for path following
		mPathFollowTargets[0][0] = -21;
		mPathFollowTargets[0][1] = 33;
		mPathFollowTargets[0][2] = 25;
		/*
		mPathFollowTargets[1][0] = -22;
		mPathFollowTargets[1][1] = 29.7f;
		mPathFollowTargets[1][2] = -18;
		*/
		mPathFollowTargets[1][0] = -25;
		mPathFollowTargets[1][1] = 33;
		mPathFollowTargets[1][2] = 5;
		
		mPathFollowTargets[2][0] = -15;
		mPathFollowTargets[2][1] = 29;
		mPathFollowTargets[2][2] = -20;

		mPathFollowTargets[3][0] = 22;
		mPathFollowTargets[3][1] = 22;
		mPathFollowTargets[3][2] = -19;

		mPathFollowTargets[4][0] = 5.5f;
		mPathFollowTargets[4][1] = 17;
		mPathFollowTargets[4][2] = -1.0f;

		mPathFollowTargets[5][0] = 1.3f;
		mPathFollowTargets[5][1] = 12;
		mPathFollowTargets[5][2] = 31;

		mPathFollowTargets[6][0] = 30;
		mPathFollowTargets[6][1] = 24.5f;
		mPathFollowTargets[6][2] = 21.0f;

		mPathFollowTargets[7][0] = 46.5f;
		mPathFollowTargets[7][1] = 24.7f;
		mPathFollowTargets[7][2] = 15.5f;

		mPathFollowTargets[8][0] = 35;
		mPathFollowTargets[8][1] = 24.7f;
		mPathFollowTargets[8][2] = 5;

		mPathFollowTargets[9][0] = 14.5;
		mPathFollowTargets[9][1] = 26.0f;
		mPathFollowTargets[9][2] = 16.0;
		
		mPathFollowTargets[10][0] = -8;
		mPathFollowTargets[10][1] = 33.0f;
		mPathFollowTargets[10][2] = 20;

		for ( int i = 0; i < 4; i++ )
		{
			std::string name = mAIWorldManager->getNextAgentName("Sinbad");
			mPathFollowingAgent[i] = new GameObject( name, "Sinbad.mesh", "", GRAPHICS_OBJECT, DEFAULT_COLLISION_SHAPE, "", true);
			mPathFollowingAgent[i]->getdtAIAgent()->scaleAgent(0.2f, 0.2f, 0.2f);
			mPathFollowingAgent[i]->getdtAIAgent()->setYOffset(1.15f);
			mPathFollowingAgent[i]->getdtAIAgent()->setSinbadMaterial("Sinbad/Clothes_magenta");
		}
		
		dtTarget* dtTargets[11];
		for (int i = 0 ; i < 11 ; i++)
		{
			dtTargets[i] = this->mAIWorldManager->addNewTarget(mPathFollowTargets[i], "follow_path");
		}

		mPathFollowingAgent[0]->getdtAIAgent()->setAgentPositionRelative(mPathFollowTargets[0]);
		mPathFollowingAgent[1]->getdtAIAgent()->setAgentPositionRelative(mPathFollowTargets[1]);
		mPathFollowingAgent[2]->getdtAIAgent()->setAgentPositionRelative(mPathFollowTargets[3]);
		mPathFollowingAgent[3]->getdtAIAgent()->setAgentPositionRelative(mPathFollowTargets[5]);
		
		//dtTarget* target0 = mAIWorldManager->addNewTarget(mPathFollowTargets[1], "arrive");
		mPathFollowingAgent[0]->getdtAIAgent()->setPathFollowingSteeringBehavior(dtTargets[1]);
		//dtTarget* target1 = mAIWorldManager->addNewTarget(mPathFollowTargets[2], "arrive");
		mPathFollowingAgent[1]->getdtAIAgent()->setPathFollowingSteeringBehavior(dtTargets[2]);
		//dtTarget* target2 = mAIWorldManager->addNewTarget(mPathFollowTargets[4], "arrive");
		mPathFollowingAgent[2]->getdtAIAgent()->setPathFollowingSteeringBehavior(dtTargets[4]);
		//dtTarget* target3 = mAIWorldManager->addNewTarget(mPathFollowTargets[6], "arrive");
		mPathFollowingAgent[3]->getdtAIAgent()->setPathFollowingSteeringBehavior(dtTargets[6]);
			
		for ( int i = 0; i < 4; i++ )
		{
			mPathFollowingAgent[i]->getdtAIAgent()->setDebugBehavior("Follow Path");
			//mPathFollowingAgent[i]->getdtAIAgent()->setDebug(true, true);
		}
		

		float wanderAgentPos[5][3];
		wanderAgentPos[0][0] = 30;
		wanderAgentPos[0][1] = 21.2f;
		wanderAgentPos[0][2] = -28;

		wanderAgentPos[1][0] = -4.6f;
		wanderAgentPos[1][1] = 29.2f;
		wanderAgentPos[1][2] = 16;
		
		wanderAgentPos[2][0] = -21.7f;//-1.2f;
		wanderAgentPos[2][1] = 31.1f;//11.2f;
		wanderAgentPos[2][2] = 18.3f;//37;
		
		wanderAgentPos[3][0] = 18;
		wanderAgentPos[3][1] = 21.5f;
		wanderAgentPos[3][2] = -32;
		
		wanderAgentPos[4][0] = 22;
		wanderAgentPos[4][1] = 21.5f;
		wanderAgentPos[4][2] = -17;

		// Add wandering crowd
		mWanderingCrowd = mAIWorldManager->addNewCrowd("CyanMaterial");
		for ( int i = 0; i < 5; i++ )
		{
			std::string name = mAIWorldManager->getNextAgentName("Sinbad");
			mWanderingAgents[i] = new GameObject( name, "Sinbad.mesh", "", GRAPHICS_OBJECT, DEFAULT_COLLISION_SHAPE, "", true);
			mWanderingAgents[i]->getdtAIAgent()->setAgentPositionAbsolute(wanderAgentPos[i]);
			mWanderingAgents[i]->getdtAIAgent()->scaleAgent(0.2f, 0.2f, 0.2f);
			mWanderingAgents[i]->getdtAIAgent()->setYOffset(1.15f);
			mWanderingAgents[i]->getdtAIAgent()->setSinbadMaterial("Sinbad/Clothes_cyan");
			mWanderingAgents[i]->getdtAIAgent()->setWanderSteeringBehavior();
			mWanderingAgents[i]->getdtAIAgent()->setDebugBehavior("wander");
			mWanderingAgents[i]->getdtAIAgent()->setMaxSpeed(2);
		}
		

		// Add path finding crowd
		float posPF[3];
		posPF[0] = 0;
		posPF[1] = 28;
		posPF[2] = 16;

		mPathFindingCrowd = mAIWorldManager->addNewCrowd("OrangeMaterial");
		std::string name = mAIWorldManager->getNextAgentName("Sinbad");
		mPathFindingAgent = new GameObject( name, "Sinbad.mesh", "", GRAPHICS_OBJECT, DEFAULT_COLLISION_SHAPE, "", true);
		mPathFindingAgent->getdtAIAgent()->setAgentPositionRelative(dtTargets[4]->getTargetPosF());
		mPathFindingAgent->getdtAIAgent()->scaleAgent(0.2f, 0.2f, 0.2f);
		mPathFindingAgent->getdtAIAgent()->setYOffset(1.15f);
		mPathFindingAgent->getdtAIAgent()->setSinbadMaterial("Sinbad/Clothes_orange");
		dtTarget* t = mAIWorldManager->addNewTarget(posPF, "arrive");
		mPathFindingAgent->getdtAIAgent()->setSeekAndArriveSteeringBehavior(t);
		mPathFindingAgent->getdtAIAgent()->setDebugBehavior("path finding");

		this->mAIWorldManager->setActiveCrowd(2);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	// HandleKeyboardEvents()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::HandleKeyboardEvents()
	{	
		if ((InputPtr->IsKeyDown(GIS::KC_RCONTROL) || InputPtr->IsKeyDown(GIS::KC_LCONTROL)))
		{
			// Select active crowd
			if (InputPtr->IsKeyDown(GIS::KC_1))
			{
				if ( mAIWorldManager->getActiveAgentId() != -1 )
					mAIWorldManager->getActiveAgent()->setDebug(false, false);
				if ( mAIWorldManager->getCrowdsCount() >= 1 )
					mAIWorldManager->setActiveCrowd(0);
			}
			else if (InputPtr->IsKeyDown(GIS::KC_2))
			{
				if ( mAIWorldManager->getActiveAgentId() != -1 )
					mAIWorldManager->getActiveAgent()->setDebug(false, false);
				if ( mAIWorldManager->getCrowdsCount() >= 2 )
					mAIWorldManager->setActiveCrowd(1);
			}
			else if (InputPtr->IsKeyDown(GIS::KC_3))
			{
				if ( mAIWorldManager->getActiveAgentId() != -1 )
					mAIWorldManager->getActiveAgent()->setDebug(false, false);
				if ( mAIWorldManager->getCrowdsCount() >= 3 )
					mAIWorldManager->setActiveCrowd(2);
			}
			else if (InputPtr->IsKeyDown(GIS::KC_4))
			{
				if ( mAIWorldManager->getActiveAgentId() != -1 )
					mAIWorldManager->getActiveAgent()->setDebug(false, false);
				if ( mAIWorldManager->getCrowdsCount() >= 4 )
					mAIWorldManager->setActiveCrowd(3);
			}
			else if (InputPtr->IsKeyDown(GIS::KC_5))
			{
				if ( mAIWorldManager->getActiveAgentId() != -1 )
					mAIWorldManager->getActiveAgent()->setDebug(false, false);
				if ( mAIWorldManager->getCrowdsCount() >= 5 )
					mAIWorldManager->setActiveCrowd(4);
			}

			if (InputPtr->IsKeyDown(GIS::KC_F2))
				mBehavior = "none";
			else if (InputPtr->IsKeyDown(GIS::KC_F3))
				mBehavior = "arrive";
			else if (InputPtr->IsKeyDown(GIS::KC_F4))
				mBehavior = "follow_path";
			else if (InputPtr->IsKeyDown(GIS::KC_F5))
				mBehavior = "wander";
			else if (InputPtr->IsKeyDown(GIS::KC_F6))
				mBehavior = "pursuit";
			else if (InputPtr->IsKeyDown(GIS::KC_F7))
				mBehavior = "evade";
			else if (InputPtr->IsKeyDown(GIS::KC_F8))
				mBehavior = "flee";
			else if (InputPtr->IsKeyDown(GIS::KC_F9))
				mBehavior = "flock";
		}

		this->m_prevInputPtr = InputPtr;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	// OnMouseRelease()
	//////////////////////////////////////////////////////////////////////////
	bool AIDemoScreen::OnMouseRelease(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		if (mouseButtonId == GIS::MB_Left)
			this->m_mouseLeftReleased = true;
		else if (mouseButtonId == GIS::MB_Right)
			this->m_mouseRightReleased = true;

		return true;
	}

}
