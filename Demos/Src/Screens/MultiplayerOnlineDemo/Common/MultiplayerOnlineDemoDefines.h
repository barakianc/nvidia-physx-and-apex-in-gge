#pragma once

namespace GamePipeGame
{
	class MultiplayerOnlineDemoDefines
	{
	public:
		static const float MAX_LOG_MSG_AGE;
		static const int DEFAULT_SERVER_PORT = 60000;
		static const int DEFAULT_MAX_CONNECTIONS = 32;
		static const int DEFAULT_SLEEP_INTERVAL = 100;
	};
}