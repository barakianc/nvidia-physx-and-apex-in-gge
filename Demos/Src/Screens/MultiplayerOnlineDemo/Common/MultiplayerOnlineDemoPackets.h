#pragma once

// RakNet message identifiers
#include "MessageIdentifiers.h"

// For RakNet types (RakNet::Packet, etc.)
#include "NetManager.h"

namespace GamePipeGame
{
	enum CustomPacketID
	{
		//-----------------------------
		// Client -> Server
		//-----------------------------
		CREATE_BALL = ID_USER_PACKET_ENUM,
		CREATE_ANIMATED_CHARACTER_PROXY,//C
		CREATE_GRAPHICS_OBJECT,//G
		CREATE_ANIMATED_CHRACTER_RIGIDBODY,//R
		STORMTROOPER_ATTITUDE_WALK,//1
		STORMTROOPER_GUARD_N_WAIT,//2
		STORMTROOPER_TALK,//3
		STORMTROOPER_CHALLENGE_OPP,//4
		STORMTROOPER_SPOT_N_SHOOT,//5
		STORMTROOPER_DANCE,//6
		STORMTROOPER_IDLE_TO_SHOOT,//7
		STORMTROOPER_IDLE,//8
		STORMTROOPER_IDLE1,//9
		STORMTROOPER_SHOOT_MOVE_FWD,//0
		STORMTROOPER_SHOOT_N_DIE,//P
		STORMTROOPER_ATTITUDE_WALK_LEFT,//O
		STORMTROOPER_ATTITUDE_WALK_RIGHT,//I
		STORMTROOPER_WALK_2STEPS,//U
		WALK_ROBOT,//Y
		GOBLIN_WALK,//J
		GOBLIN_IDLE,//K
		GOBLIN_ATTACK,//L
		STORMTROOPER_STOP_ANIMATIONS,
		GOBLIN_STOP_ANIMATIONS,
		STOP_ROBOT,
		//-----------------------------
		// Server -> Client
		//-----------------------------
		CREATE_STATIC_OBJECT,
		ATLAS_SESSION_ID
	};


	//-------------------------------------------------------------------------
	// Custom Packet base class
	//-------------------------------------------------------------------------
	class CustomPacket
	{
	public:
		CustomPacket(CustomPacketID id);
		bool WriteToBitStream(RakNet::BitStream& bitStream) const;
		virtual bool LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket) = 0;		
		
	protected:
		CustomPacketID m_id;
		std::string m_message;
		virtual void ConstructMessage() = 0;
	};

	//-------------------------------------------------------------------------
	// Client -> Server Packets
	//-------------------------------------------------------------------------
	class CreateBallPacket : public CustomPacket
	{
	public:
		CreateBallPacket();
		CreateBallPacket(Ogre::Vector3 position);
		virtual bool LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket);
		Ogre::Vector3 GetPosition() const { return m_position; }

	private:
		Ogre::Vector3 m_position;
		virtual void ConstructMessage();
	};

	//-------------------------------------------------------------------------
	// Server -> Client Packets
	//-------------------------------------------------------------------------
	class CreateStaticObjectPacket : public CustomPacket
	{
	public:
		CreateStaticObjectPacket();
		CreateStaticObjectPacket(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type);
		virtual bool LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket);
		const char* GetObjectName() const { return m_objectName.c_str(); }
		const char* GetMeshName() const { return m_meshName.c_str(); }
		const char* GetHKXFileName() const { return m_hkxFileName.c_str(); }
		Ogre::Vector3 GetPosition() const { return m_position; }
		GameObjectType GetObjectType() const { return m_type; }
		
	private:
		std::string m_objectName;
		std::string m_meshName;
		std::string m_hkxFileName;
		Ogre::Vector3 m_position;
		GameObjectType m_type;
		virtual void ConstructMessage();
	};

	class AtlasReportSessionPacket : public CustomPacket
	{
	public:
		AtlasReportSessionPacket();
		AtlasReportSessionPacket(char* sessionId);
		virtual bool LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket);
		char* GetSessionId() { return m_sessionId; }

	private:
		char m_sessionId[40];
		virtual void ConstructMessage();
	};
}