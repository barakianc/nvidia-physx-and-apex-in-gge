#include "StdAfx.h"
#include "MultiplayerOnlineDemoPackets.h"

#include <string>

namespace GamePipeGame
{
	///////////////////////////////////////////////////////////////////////////
	// CustomPacket
	///////////////////////////////////////////////////////////////////////////
	CustomPacket::CustomPacket(CustomPacketID id) :
		m_id(id),
		m_message("")
	{
	}

	//-------------------------------------------------------------------------
	bool CustomPacket::WriteToBitStream(RakNet::BitStream& bitStream) const
	{
		bitStream.Write((unsigned char)m_id);
		if (m_message.length() > 0)
			bitStream.Write(m_message.c_str());
		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// CreateStaticObjects Packet
	///////////////////////////////////////////////////////////////////////////
	//-------------------------------------------------------------------------
	CreateBallPacket::CreateBallPacket() :
		CustomPacket(CREATE_BALL)
	{
	}

	//-------------------------------------------------------------------------
	CreateBallPacket::CreateBallPacket(Ogre::Vector3 position) :
		CustomPacket(CREATE_BALL),
		m_position(position)
	{
		ConstructMessage();
	}

	//-------------------------------------------------------------------------
	void CreateBallPacket::ConstructMessage()
	{
		char buffer[1024];
		sprintf_s(buffer, "%f %f %f", m_position.x, m_position.y, m_position.z);
		// null-terminate the buffer just in case
		buffer[sizeof(buffer) - 1] = 0;
		m_message = buffer;
	}

	//-------------------------------------------------------------------------
	bool CreateBallPacket::LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket)
	{
		// if the packet only has one byte in it, it only contains the mesasge type.
		// we can't parse that.
		if (1 == rakNetPacket->length)
			return false;

		// we are not interested in the first 3 bytes of data we received.
		// ignore them.
		unsigned char buffer[1024];
		memcpy(buffer, &rakNetPacket->data[3], rakNetPacket->length - 3);
		buffer[rakNetPacket->length - 3] = 0;
		std::string msg = std::string(reinterpret_cast<const char*>(buffer));

		//-----------------------------------
		// Parse object position
		//-----------------------------------
		// x coord
		size_t pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string xCoord = msg.substr(0, pos);
		m_position.x = (float)atof(xCoord.c_str());
		msg = msg.substr(pos + 1);
		
		// y coord
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string yCoord = msg.substr(0, pos);
		m_position.y = (float)atof(yCoord.c_str());
		msg = msg.substr(pos + 1);

		// z coord
		// there should be no more spaces at this point
		pos = msg.find(" ");
		if (string::npos != pos)
			return false;
		std::string zCoord = msg;
		m_position.z = (float)atof(zCoord.c_str());

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// CreateStaticObjects Packet
	///////////////////////////////////////////////////////////////////////////
	//-------------------------------------------------------------------------
	CreateStaticObjectPacket::CreateStaticObjectPacket() :
		CustomPacket(CREATE_STATIC_OBJECT)
	{
	}

	//-------------------------------------------------------------------------
	CreateStaticObjectPacket::CreateStaticObjectPacket(std::string objectName, std::string meshName, std::string hkxFileName, Ogre::Vector3 position, GameObjectType type) :
		CustomPacket(CREATE_STATIC_OBJECT),
		m_objectName(objectName),
		m_meshName(meshName),
		m_hkxFileName(hkxFileName),
		m_position(position),
		m_type(type)
	{
		ConstructMessage();
	}

	//-------------------------------------------------------------------------
	void CreateStaticObjectPacket::ConstructMessage()
	{
		char buffer[1024];
		sprintf_s(buffer, "'%s' '%s' '%s' %d %f %f %f", m_objectName.c_str(), m_meshName.c_str(), m_hkxFileName.c_str(), m_type, m_position.x, m_position.y, m_position.z);
		// null-terminate the buffer just in case
		buffer[sizeof(buffer) - 1] = 0;
		m_message = buffer;
	}

	//-------------------------------------------------------------------------
	bool CreateStaticObjectPacket::LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket)
	{
		// if the packet only has one byte in it, it only contains the mesasge type.
		// we can't parse that.
		if (1 == rakNetPacket->length)
			return false;

		// we are not interested in the first 3 bytes of data we received.
		// ignore them.
		unsigned char buffer[1024];
		memcpy(buffer, &rakNetPacket->data[3], rakNetPacket->length - 3);
		buffer[rakNetPacket->length - 3] = 0;
		std::string msg = std::string(reinterpret_cast<const char*>(buffer));

		//-----------------------------------
		// Parse object name
		//-----------------------------------
		size_t pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_objectName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);

		//-----------------------------------
		// Parse mesh name
		//-----------------------------------
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_meshName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);

		//-----------------------------------
		// Parse hkx file name
		//-----------------------------------
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		m_hkxFileName = msg.substr(0, pos);
		msg = msg.substr(pos + 1);

		// the hkx file name is followed by a space. get rid of it.
		if (msg[0] == ' ')
			msg = msg.substr(1);

		//-----------------------------------
		// Parse object type
		//-----------------------------------
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string objType = msg.substr(0, pos);
		m_type = (GameObjectType)atoi(objType.c_str());
		msg = msg.substr(pos + 1);

		//-----------------------------------
		// Parse object position
		//-----------------------------------
		// x coord
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string xCoord = msg.substr(0, pos);
		m_position.x = (float)atof(xCoord.c_str());
		msg = msg.substr(pos + 1);
		
		// y coord
		pos = msg.find(" ");
		if (string::npos == pos)
			return false;
		std::string yCoord = msg.substr(0, pos);
		m_position.y = (float)atof(yCoord.c_str());
		msg = msg.substr(pos + 1);

		// z coord
		// there should be no more spaces at this point
		pos = msg.find(" ");
		if (string::npos != pos)
			return false;
		std::string zCoord = msg;
		m_position.z = (float)atof(zCoord.c_str());

		return true;
	}

	 AtlasReportSessionPacket::AtlasReportSessionPacket() : CustomPacket(CREATE_STATIC_OBJECT)
	{
	}

	AtlasReportSessionPacket::AtlasReportSessionPacket(char* sessionId) : CustomPacket(ATLAS_SESSION_ID)
	{
		strcpy_s(m_sessionId, sessionId);
		ConstructMessage();
	}

	void AtlasReportSessionPacket::ConstructMessage()
	{
		char buffer[1024];
		sprintf_s(buffer, "'%s'", m_sessionId);
		// null-terminate the buffer just in case
		buffer[sizeof(buffer) - 1] = 0;
		m_message = buffer;
	}

	bool AtlasReportSessionPacket::LoadFromRakNetPacket(const RakNet::Packet* const rakNetPacket)
	{
		// if the packet only has one byte in it, it only contains the mesasge type.
		// we can't parse that.
		if (1 == rakNetPacket->length)
			return false;

		// we are not interested in the first 3 bytes of data we received.
		// ignore them.
		unsigned char buffer[1024];
		memcpy(buffer, &rakNetPacket->data[3], rakNetPacket->length - 3);
		buffer[rakNetPacket->length - 3] = 0;
		std::string msg = std::string(reinterpret_cast<const char*>(buffer));

		//-----------------------------------
		// Parse object name
		//-----------------------------------
		size_t pos = msg.find("'");
		if (string::npos == pos)
			return false;

		msg = msg.substr(pos + 1);
		pos = msg.find("'");
		if (string::npos == pos)
			return false;

		strcpy_s(m_sessionId, msg.substr(0, pos).c_str());
		return true;
	}
}