#pragma once

#ifdef HAVOK

#include "GameScreen.h"

namespace GamePipeGame
{
	class PresenceScreen:public GamePipe::GameScreen
	{
		public:
			static PresenceScreen* s_instance;
			static PresenceScreen* Get();

			bool LoadResources();
			bool UnloadResources();

			bool Show();

			bool Initialize();
			bool Destroy();

			bool Update();
			bool Draw();

			bool OnKeyPress(const GIS::KeyEvent &keyEvent);

			PresenceScreen(std::string name);
			~PresenceScreen(){};
	};
}

#endif