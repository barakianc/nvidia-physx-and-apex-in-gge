#include "StdAfx.h"

#ifdef HAVOK


#include "PresenceScreen.h"
#include "GameSpyPresence.h"
#include "MultiplayerOnlineDemo\Screens\HomeScreen\HomeScreen.h"
#include "MultiplayerOnlineDemo\Screens\ClientScreen\ClientScreen.h"

#include <string>
#include <vector>

// Engine
#include "Engine.h"


namespace GamePipeGame
{
	PresenceScreen* PresenceScreen::s_instance = 0;

	const char* PRESENCE_BUDDY_LIST = "PresenceBuddyList";

	const char* PRESENCE_BUDDY_REQUESTS = "PresenceBuddyRequests";

	const char* LOADING = "PresenceScreenLoading";

	const char* CMD = "PresenceScreenCommands";

	const char* p_availableCommands[] = {
		"V -> View Buddy List",
		"R -> View Buddy Requests",
		"Buddy # -> Message Buddy(Only used while Viewing Buddy List)",
		"S -> Send Buddy Request",
		"ESC->Quit"
	};

	bool viewingBuddyList = false;
	bool viewingRequests = false;
	bool statusSet = false;
	bool connected = false;

	GameSpyPresence gp;

	PresenceScreen* PresenceScreen::Get()
	{
		if (!s_instance)
			s_instance = new PresenceScreen("PresenceScreen");

		return s_instance;
	}

	//////////////////////////////////////////////////////////////////////////
	// Server Browser Screen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	PresenceScreen::PresenceScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		//------------------------------------------------
		// Add text for available commands
		//------------------------------------------------
		std::string commandText;
		int numCommands = sizeof(p_availableCommands)/sizeof(p_availableCommands[0]);
		for (int idx = 0; idx < numCommands; idx++)
		{
			commandText += p_availableCommands[idx];
			commandText += "\n";
		}
		UtilitiesPtr->Add(CMD, new GamePipe::DebugText(commandText.c_str(), 0.25f, 0.25f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
		char email[100], nick[30], pass[30];
		printf("Enter Email: ");
		scanf_s("%s", email);
		printf("Enter Nick: ");
		scanf_s("%s", nick);
		printf("Enter Password: ");
		scanf_s("%s", pass);
		gp.SetEmail(email);
		gp.SetNickName(nick);
		gp.SetPassword(pass);
		gp.PresenceInit();
		gp.PresenceConnect();
		GameSpyPresence::setShowOnlyOnline(false);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::Update()
	{
		if(!statusSet && getLoggedIn())
		{
			statusSet = true;
			gp.PresenceSetStatus(true, "Online");
		}
		gp.PresenceProcess();
		std::vector<newMessage> mess;
		getMessage(&mess);
		int size = mess.size();
		for(int i = 0; i < size; i++)
		{
			printf("%s says: %s\n", mess[i].from.c_str(), mess[i].message.c_str());
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::Destroy()
	{
		UtilitiesPtr->Remove(CMD);
		if(viewingBuddyList)
			UtilitiesPtr->Remove(PRESENCE_BUDDY_LIST);
		if(viewingRequests)
			UtilitiesPtr->Remove(PRESENCE_BUDDY_REQUESTS);
		viewingRequests = viewingBuddyList = false;
		s_instance = 0;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool PresenceScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
			case GIS::KC_ESCAPE:
			{
				gp.PresenceSetStatus(false, "");
				gp.PresenceDisconnect();
				EnginePtr->AddGameScreen(new GamePipeGame::MultiplayerOnlineHomeScreen("HomeScreen"));
				Close();
				break;
			}

			case GIS::KC_V:
				{
					if(viewingRequests)
					{
						UtilitiesPtr->Remove(PRESENCE_BUDDY_REQUESTS);
						viewingRequests = false;
					}
					if(!viewingBuddyList)
					{
						std::vector<profileInfo> list;
						string disp = std::string("\nBUDDY LIST\n");
						gp.PresenceGetBuddyList(&list);
						int size = list.size();
						for(int i = 0; i < size; i++)
						{
							disp += list[i].name;
							disp += "(";
							disp += list[i].email;
							disp += ")\n";
						}
						UtilitiesPtr->Add(PRESENCE_BUDDY_LIST, new GamePipe::DebugText(disp.c_str(), 0.0f, 0.5f, 4.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
						viewingBuddyList = true;
					}
					break;
				}
			case GIS::KC_R:
				{
					if(viewingBuddyList)
					{
						UtilitiesPtr->Remove(PRESENCE_BUDDY_LIST);
						viewingBuddyList = false;
					}
					if(!viewingRequests)
					{
						std::vector<profileInfo> list;
						string disp = std::string("\nREQUESTS\n");
						gp.PresenceGetBuddyRequests(&list);
						int size = list.size();
						for(int i = 0; i < size; i++)
						{
							disp += list[i].name;
							disp += "(";
							disp += list[i].email;
							disp += ")\n";
						}
						UtilitiesPtr->Add(PRESENCE_BUDDY_REQUESTS, new GamePipe::DebugText(disp.c_str(), 0.0f, 0.5f, 4.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
						viewingRequests = true;
					}
					break;
				}
			case GIS::KC_S:
				{
					char email[100], nick[30], fname[30], lname[30];
					printf("\nHit enter if you want to leave any of the empty. Atleast one field must be specified.\n");
					printf("Enter Email: ");
					fflush(stdin);
					fgets(email, 100, stdin);
					email[strlen(email) - 1] = '\0';
					printf("Enter Nick: ");
					fgets(nick, 30, stdin);
					nick[strlen(nick) - 1] = '\0';
					printf("Enter First Name: ");
					fgets(fname, 30, stdin);
					fname[strlen(fname) - 1] = '\0';
					printf("Enter Last Name: ");
					fgets(lname, 30, stdin);
					lname[strlen(lname) - 1] = '\0';
					if(gp.PresenceSendBuddyRequest(nick, email, fname, lname))
						printf("Request Sent!\n");
					else
						printf("Could not find profile!\n");
					break;
				}
			default:
			{
				if(keyEvent.key >= 2 && keyEvent.key <= 11)
				{
					if(viewingRequests)
					{
						std::vector<profileInfo> list;
						gp.PresenceGetBuddyRequests(&list);
						int size = list.size();
						int temp = keyEvent.key - 2;
						int index;
						if(temp < 9)
							index = temp + 1;
						else
							index = 0;
						if(index < size)
						{
							gp.PresenceAcceptBuddyRequest(index);
						}
					}
					else if(viewingBuddyList)
					{
						std::vector<profileInfo> list;
						gp.PresenceGetBuddyList(&list);
						int size = list.size();
						int temp = keyEvent.key - 2;
						int index;
						if(temp < 9)
							index = temp + 1;
						else
							index = 0;
						if(index < size)
						{
							char message[1000];
							printf("Enter Message: ");
							fflush(stdin);
							fgets(message, 1000, stdin);
							message[strlen(message) - 1] = '\0';
							gp.PresenceSendBuddyMessage(index, message);
						}
					}
				}
				break;
			}
		}
		return true;
	}
}

#endif