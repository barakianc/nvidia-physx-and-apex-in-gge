#pragma once

#ifdef HAVOK
// GameScreen
#include "GameScreen.h"

// RakNet
#include "MultiplayerOnline\RakNet\HavokNetManager.h"

// Custom (game-specific) packets
#include "MultiplayerOnlineDemo\Common\MultiplayerOnlineDemoPackets.h"

namespace GamePipeGame
{
	class ServerScreen : public GamePipe::GameScreen
	{
	private:
		GameObjectReplica* addedBall1;
		HavokNetManager* m_pHavokNetMgr;
		std::deque<LogMessage *> m_messageLog;
		Ogre::ConfigFile m_configFile;
		EntityMapType m_entityMap;
		std::vector<CreateStaticObjectPacket*> m_staticObjectPackets;
		AtlasReportSessionPacket* m_atlasReportSessionPacket;

		bool LoadConfigFile(const char* configFileName);
		bool InitHavokNetMgr();

		void SetLogMessage(std::string newMessage, Ogre::ColourValue &newColor);
		void AddLogMessage(std::string newMessage, int newPriority = 0);
		void ProcessLog(float deltaTime);

		void CreateStaticObjects();
		void SendStaticObjectPackets();

		void SendAtlasReportPacket();

		void HandleCreateBallPacket(const CreateBallPacket& packet);

		ServerScreen(std::string name, const char* configFileName = "multiplayer_online_demo.config");
		static ServerScreen* s_instance;

	public:
		static ServerScreen* Get();
		~ServerScreen() {}

		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

		bool HandleCustomPacket(const RakNet::Packet* const rakNetPacket);
	};
}

#endif