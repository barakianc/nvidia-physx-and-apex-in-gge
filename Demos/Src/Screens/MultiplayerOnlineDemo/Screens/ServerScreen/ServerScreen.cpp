#include "StdAfx.h"
#ifdef HAVOK
// ServerScreen
#include "ServerScreen.h"

// HomeScreen
#include "MultiplayerOnlineDemo\Screens\HomeScreen\HomeScreen.h"

// Engine
#include "Engine.h"

#include "QueryReporting.h"

// Common constants for demo
#include "MultiplayerOnlineDemo\Common\MultiplayerOnlineDemoDefines.h"

#include "GameReport.h"

static const char* LOG_NAME = "ServerScreenLog";
static const char* AVAILABLE_CMDS_NAME = "ServerScreenCommands";
static const char* s_availableCommandsText[] = {
	"SERVER RUNNING",
	"ESC->Shut Down Server"
};
QueryReporting qr;

namespace GamePipeGame
{
	extern ofstream outputLog; // This is defined in GameObjectReplica.

	ServerScreen* ServerScreen::s_instance = 0;

	//////////////////////////////////////////////////////////////////////////
	// HandleCustomPacketCB(RakNet::Packet* rackNetPacket) -- static callback
	//////////////////////////////////////////////////////////////////////////
	static bool HandleCustomPacketCB(const RakNet::Packet* const rakNetPacket)
	{
		return ServerScreen::Get()->HandleCustomPacket(rakNetPacket);
	}

	//////////////////////////////////////////////////////////////////////////
	// Get()
	// The server screen uses the singleton pattern so that it can handle
	// custom game packets via the callback above.
	//////////////////////////////////////////////////////////////////////////
	ServerScreen* ServerScreen::Get()
	{
		if (!s_instance)
			s_instance = new ServerScreen("ServerScreen");

		return s_instance;
	}

	//////////////////////////////////////////////////////////////////////////
	// ServerScreen(std::string name, const char* configFileName)
	//////////////////////////////////////////////////////////////////////////
	ServerScreen::ServerScreen(std::string name, const char* configFileName /*= "networkedhavokdemo.config"*/) : 
	GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);

		if (!LoadConfigFile(configFileName))
		{
			cout << "[ServerScreen] There were errors processing the server configuration file '" << configFileName << "'." << endl;
			return;
		}

		if (!InitHavokNetMgr())
		{
			cout << "[ServerScreen] Error intializing Havok Net Manager on the server!\n" << endl;
			return;
		}
		qr.init();
		QueryReporting::setHostName("Srik");

	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::Initialize()
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		//------------------------------------------------
		// Add text for available commands
		//------------------------------------------------
		std::string commandText;
		int numCommands = sizeof(s_availableCommandsText)/sizeof(s_availableCommandsText[0]);
		for (int idx = 0; idx < numCommands; idx++)
		{
			commandText += s_availableCommandsText[idx];
			commandText += "\n";
		}
		UtilitiesPtr->Add(AVAILABLE_CMDS_NAME, new GamePipe::DebugText(commandText.c_str(), 0.25f, 0.25f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));

		//------------------------------------------------
		// Create visual for log messages
		//------------------------------------------------
		m_messageLog.clear();
		UtilitiesPtr->Add(LOG_NAME, new GamePipe::DebugText("", (Ogre::Real)0.01, (Ogre::Real)0.97, (Ogre::Real)3));
		AddLogMessage("CS522 Fall 2011, Multiplayer Online Demo Server");

		//------------------------------------------------
		// Create all static objects in our world
		//------------------------------------------------
		CreateStaticObjects();

		// setup atlas report, get report session id
		char sessionId[SC_SESSION_GUID_SIZE];
		ReportServerSetup(sessionId, sizeof(sessionId));
		m_atlasReportSessionPacket = new AtlasReportSessionPacket(sessionId);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::Destroy()
	{
		outputLog.close();
		m_pHavokNetMgr->NetShutdown();

		// RMF: cannot delete m_pNetHavokMgr here; parts of it are already
		// deleted elsewhere. I believe this is a flaw in the way the previous
		// networking team destructed net manager objects.

		//------------------------------------------------
		// Destroy text for available commands
		//------------------------------------------------
		UtilitiesPtr->Remove(AVAILABLE_CMDS_NAME);

		//------------------------------------------------
		// Destroy text for log messages
		//------------------------------------------------
		UtilitiesPtr->Remove(LOG_NAME);

		//------------------------------------------------
		// Delete all our static object packets
		//------------------------------------------------
		for (unsigned idx = 0; idx < m_staticObjectPackets.size(); idx++)
		{
			delete m_staticObjectPackets[idx];
		}

		// 'this' server will be deleted elsewhere. 
		// need to simulate deleting the server singleton by setting ptr to 0.
		s_instance = 0;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::Update()
	{
		//-------------------------------------
		// Do Havok Net Manager Updates
		//-------------------------------------
		if (!m_pHavokNetMgr->NotStarted())
		{
			m_pHavokNetMgr->ProcessMessages();
			m_pHavokNetMgr->UpdateNetworkObjectsInSceneWithHavok(GetDefaultSceneManager(), m_entityMap, m_pGameObjectManager);
			m_pHavokNetMgr->UpdateNetworkObjectsAfterHavokStep();
		}

		qr.think();
		//-------------------------------------
		// Update log message display as needed
		//-------------------------------------
		ProcessLog(EnginePtr->GetDeltaTime());

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				PlayFakeMatch();
				ReportStats(0);
				ReportCleanup(0);
				EnginePtr->AddGameScreen(new GamePipeGame::MultiplayerOnlineHomeScreen("HomeScreen"));
				Close();
				break;
			}

		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitHavokNetMgr()
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::InitHavokNetMgr()
	{
		m_pHavokNetMgr = new HavokNetManager(&HandleCustomPacketCB);
		if (!m_pHavokNetMgr)
			return false;

		m_pHavokNetMgr->SetMessageLog(&m_messageLog);
		m_pHavokNetMgr->SetConfigFile(&m_configFile);

		m_pHavokNetMgr->SetTopology(SERVER);
		m_pHavokNetMgr->CreateNetworkIDManager(true);

		Ogre::String configStrLocalSocketPort = m_configFile.getSetting("server.localport");
		Ogre::String configStrMaxConnections = m_configFile.getSetting("server.maxconnections");
		Ogre::String configStrSleepInterval = m_configFile.getSetting("server.sleepinterval");

		unsigned short configLocalSocketPort = (configStrLocalSocketPort.empty()) ? MultiplayerOnlineDemoDefines::DEFAULT_SERVER_PORT : ((unsigned short)Ogre::StringConverter::parseInt(configStrLocalSocketPort));
		int configMaxConnections = (configStrMaxConnections.empty()) ? MultiplayerOnlineDemoDefines::DEFAULT_MAX_CONNECTIONS : Ogre::StringConverter::parseInt(configStrMaxConnections);
		int configSleepInterval = (configStrSleepInterval.empty()) ? MultiplayerOnlineDemoDefines::DEFAULT_SLEEP_INTERVAL : Ogre::StringConverter::parseInt(configStrSleepInterval);

		m_pHavokNetMgr->SetLocalSocketPort(configLocalSocketPort);
		m_pHavokNetMgr->SetMaxConnections(configMaxConnections);
		m_pHavokNetMgr->SetSleepInterval(configSleepInterval);

		AddLogMessage("Starting server on port: " + Ogre::StringConverter::toString(configLocalSocketPort));
		m_pHavokNetMgr->Initialize();

		AddLogMessage("Enabling plug-in: RakNet/ReplicaManager3");
		m_pHavokNetMgr->EnableRakNetPlugin("ReplicaManager3");

		AddLogMessage("Server is listening for incoming connections.");

		outputLog.open("MultiplayerOnlineDemo_Server.txt");

		outputLog << "server socket port: " << configLocalSocketPort << endl;
		outputLog << "server max connections: " << configMaxConnections << endl;
		outputLog << "server sleep interval: " << configSleepInterval << endl;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// AddLogMessage(std::string message, int priority)
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::AddLogMessage(std::string message, int priority /*= 0*/)
	{
		if (!m_messageLog.empty())
			m_messageLog.front()->messageAge += 1;

		m_messageLog.push_back(new LogMessage(message, priority));
	}

	//////////////////////////////////////////////////////////////////////////
	// SetLogMessage(std::string newMessage, Ogre::ColourValue &newColor)
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::SetLogMessage(std::string newMessage, Ogre::ColourValue &newColor)
	{
		UtilitiesPtr->GetDebugText(LOG_NAME)->SetText(newMessage);
		UtilitiesPtr->GetDebugText(LOG_NAME)->SetColor(newColor);
	}

	//////////////////////////////////////////////////////////////////////////
	// ProcessLog(float deltaTime)
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::ProcessLog(float deltaTime)
	{
		if (!m_messageLog.empty())
		{
			LogMessage *first = m_messageLog.front();
			if(first->messageAge > MultiplayerOnlineDemoDefines::MAX_LOG_MSG_AGE)
			{
				m_messageLog.pop_front();
				delete first;
			}
		}

		Ogre::ColourValue textColor = Ogre::ColourValue(1,1,1,1);
		if(!m_messageLog.empty())
		{
			LogMessage* first = m_messageLog.front();

			first->messageAge += deltaTime;
			float remainingTime = MultiplayerOnlineDemoDefines::MAX_LOG_MSG_AGE - first->messageAge;

			textColor.a = (remainingTime < 1) ? remainingTime : 1;
			if (textColor.a < 0)	
				textColor.a = 0;
			SetLogMessage(first->messageText, textColor);
		}
		else
		{
			SetLogMessage("", Ogre::ColourValue(1,1,1,1));
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// LoadConfigFile(const char* configFileName)
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::LoadConfigFile(const char* configFileName)
	{
		// NOTE: config file is expected to be in the same file as MediaPaths.txt!

		std::string path = "";
		path.append("../../");
		path.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		path.append("/");
		path.append(configFileName);

		ifstream ifs (path.c_str(), ifstream::in);

		if (ifs.good())
		{
			m_configFile.load(path.c_str(), "\x09:=", true);
		}
		else
		{
			cout << "[ServerScreen::LoadConfigFile] Could not find configuration file: '" << configFileName << "'" << endl;
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// CreateBall()
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::HandleCreateBallPacket(const CreateBallPacket& packet)
	{
		GameObjectReplica* addedBall = new GameObjectReplica("ball");
		addedBall->SetHavokObjectFileInfo("star.hkx", "star.mesh");
		addedBall->SetHavokObjectType(PHYSICS_DYNAMIC);
		addedBall->SetPosition(packet.GetPosition());
		addedBall->SetRealPosition(packet.GetPosition());
		addedBall->SetVelocity(Ogre::Vector3::ZERO);
		addedBall->SetRealVelocity(Ogre::Vector3::ZERO);
		addedBall->SetUseServerPhysicsOnly(true);

		m_pHavokNetMgr->AddObject(addedBall);
		AddLogMessage("New ball added.");
	}

	//////////////////////////////////////////////////////////////////////////
	// CreateStaticObjects()
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::CreateStaticObjects()
	{
		CreateStaticObjectPacket* packet;
		GameObject* gameObj;
		Ogre::Vector3 position;

		//---------------------------------------
		// create the ground
		//---------------------------------------
		gameObj = new GameObject("the_ground","the_ground.mesh","the_ground.hkx", PHYSICS_FIXED);
		position = Ogre::Vector3(0.0f, -1.0f, 0.0f);
		gameObj->setPosition(position.x, position.y, position.z);
		packet = new CreateStaticObjectPacket("the_ground", "the_ground.mesh", "the_ground.hkx", position, PHYSICS_FIXED);
		m_staticObjectPackets.push_back(packet);

		//---------------------------------------
		// create our dubai houses
		//---------------------------------------
		// house 1
		gameObj = new GameObject("dubai_house1","dubai_house.mesh","dubai_house.hkx", PHYSICS_FIXED);
		position = Ogre::Vector3(0.0f, -1.0f, -8.0f);
		gameObj->setPosition(position.x, position.y, position.z);
		packet = new CreateStaticObjectPacket("dubai_house1", "dubai_house.mesh", "dubai_house.hkx", position, PHYSICS_FIXED);
		m_staticObjectPackets.push_back(packet);
		// house 2
		gameObj = new GameObject("dubai_house2","dubai_house.mesh","dubai_house.hkx", PHYSICS_FIXED);
		position = Ogre::Vector3(0.0f, -1.0f, 10.0f);
		gameObj->setPosition(position.x, position.y, position.z);
		packet = new CreateStaticObjectPacket("dubai_house2", "dubai_house.mesh", "dubai_house.hkx", position, PHYSICS_FIXED);
		m_staticObjectPackets.push_back(packet);

		//---------------------------------------
		// create our towers
		//---------------------------------------
		gameObj = new GameObject("tower1","tower.mesh","tower.hkx", PHYSICS_FIXED);
		position = Ogre::Vector3(10.0f, -1.0f, 0.0f);
		gameObj->setPosition(position.x, position.y, position.z);
		packet = new CreateStaticObjectPacket("tower1", "tower.mesh", "tower.hkx", position, PHYSICS_FIXED);
		m_staticObjectPackets.push_back(packet);
	}

	//////////////////////////////////////////////////////////////////////////
	// SendStaticObjectPackets()
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::SendStaticObjectPackets()
	{
		RakNet::BitStream bitStream;
		for (unsigned idx = 0; idx < m_staticObjectPackets.size(); idx++)
		{
			if (m_staticObjectPackets[idx]->WriteToBitStream(bitStream))
			{
				m_pHavokNetMgr->SendPacket(bitStream);
				bitStream.Reset();
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// SendAtlasReportPackets()
	//////////////////////////////////////////////////////////////////////////
	void ServerScreen::SendAtlasReportPacket()
	{
		RakNet::BitStream bitStream;
		if (m_atlasReportSessionPacket->WriteToBitStream(bitStream))
		{
			m_pHavokNetMgr->SendPacket(bitStream);
			bitStream.Reset();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// HandleCustomPacket(RakNet::Packet* rakNetPacket)
	//////////////////////////////////////////////////////////////////////////
	bool ServerScreen::HandleCustomPacket(const RakNet::Packet* const rakNetPacket)
	{
		switch (rakNetPacket->data[0])
		{
		case ID_NEW_INCOMING_CONNECTION:
			// we have a new client connection! we need to send out our
			// static objects so the new client can create them.
			SendStaticObjectPackets();
			// share atlas report session id - Jeff
			SendAtlasReportPacket();
			break;
		case CREATE_BALL:
			{
				CreateBallPacket packet;
				if (packet.LoadFromRakNetPacket(rakNetPacket))
					HandleCreateBallPacket(packet);
				break;
			}
		case CREATE_ANIMATED_CHARACTER_PROXY:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index==objectList.size())
				{
					//create a new object
					RakNet::BitStream myBitStream(rakNetPacket->data, rakNetPacket->length, false);
					unsigned char messageType;
					myBitStream.Read(messageType); //must be this case
					Ogre::Vector3 position;
					myBitStream.Read(position.x);
					myBitStream.Read(position.y);
					myBitStream.Read(position.z);
					GameObjectReplica *trooper = new GameObjectReplica("trooper");
					trooper->SetHavokObjectFileInfo("storm_trooper_ragdoll_complete.hkx", "stormtrooper_withgun_bound_LOD.mesh");
					trooper->SetHavokObjectType(ANIMATED_CHARACTER_PROXY);
					trooper->SetPosition(position);
					trooper->SetRealPosition(position);
					trooper->SetVelocity(300.0f*GetActiveCamera()->getDerivedDirection().x,300.0f*GetActiveCamera()->getDerivedDirection().y,300.0f*GetActiveCamera()->getDerivedDirection().z);
					trooper->SetRealVelocity(300.0f*GetActiveCamera()->getDerivedDirection().x,300.0f*GetActiveCamera()->getDerivedDirection().y,300.0f*GetActiveCamera()->getDerivedDirection().z);
					trooper->SetUseServerPhysicsOnly(true);
					m_pHavokNetMgr->AddObject(trooper,rakNetPacket->guid);
					AddLogMessage("New trooper added.");
				}

				break;
			}
		case CREATE_GRAPHICS_OBJECT:
			{
				//create any number of graphics object per client... all animations will be same(in the case of demo only)
				RakNet::BitStream myBitStream(rakNetPacket->data, rakNetPacket->length, false);
				unsigned char messageType;
				myBitStream.Read(messageType); //must be this case
				Ogre::Vector3 position;
				myBitStream.Read(position.x);
				myBitStream.Read(position.y);
				myBitStream.Read(position.z);
				GameObjectReplica *goblin = new GameObjectReplica("goblin");
				goblin->SetHavokObjectFileInfo("", "Goblin.mesh");
				goblin->SetHavokObjectType(GRAPHICS_OBJECT);
				goblin->SetPosition(position);
				goblin->SetRealPosition(position);

				goblin->SetUseServerPhysicsOnly(true);
				m_pHavokNetMgr->AddObject(goblin,rakNetPacket->guid);
				AddLogMessage("New goblin added.");
				break;
			}
		case CREATE_ANIMATED_CHRACTER_RIGIDBODY:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_RIGIDBODY)
					{
						break;
					}
				}
				if(index==objectList.size())
				{
					//create a new object
					RakNet::BitStream myBitStream(rakNetPacket->data, rakNetPacket->length, false);
					unsigned char messageType;
					myBitStream.Read(messageType); //must be this case
					Ogre::Vector3 position;
					myBitStream.Read(position.x);
					myBitStream.Read(position.y);
					myBitStream.Read(position.z);
					GameObjectReplica *robot = new GameObjectReplica("robot");
					robot->SetHavokObjectFileInfo("", "robot.mesh");
					robot->SetHavokObjectType(ANIMATED_CHARACTER_RIGIDBODY);
					robot->SetPosition(position);
					robot->SetRealPosition(position);
					robot->SetVelocity(300.0f*GetActiveCamera()->getDerivedDirection().x,300.0f*GetActiveCamera()->getDerivedDirection().y,300.0f*GetActiveCamera()->getDerivedDirection().z);
					robot->SetRealVelocity(300.0f*GetActiveCamera()->getDerivedDirection().x,300.0f*GetActiveCamera()->getDerivedDirection().y,300.0f*GetActiveCamera()->getDerivedDirection().z);
					robot->SetUseServerPhysicsOnly(true);
					m_pHavokNetMgr->AddObject(robot,rakNetPacket->guid);
					AddLogMessage("New robot added.");
				}
				break;
			}
		case STORMTROOPER_ATTITUDE_WALK:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_attitude_walk");
				break;
			}
		case STORMTROOPER_STOP_ANIMATIONS:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->stopAnimation();
				break;
			}
		case STORMTROOPER_GUARD_N_WAIT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_guard_n_wait");
				break;
			}
		case STORMTROOPER_TALK:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_talk");
				break;
			}
		case STORMTROOPER_CHALLENGE_OPP:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_challenge_opp");
				break;
			}
		case STORMTROOPER_SPOT_N_SHOOT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_spot_n_shoot");
				break;
			}
		case STORMTROOPER_DANCE:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_dance");
				break;
			}
		case STORMTROOPER_IDLE_TO_SHOOT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_idle_to_shoot");
				break;
			}
		case STORMTROOPER_IDLE:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_idle");
				break;
			}
		case STORMTROOPER_IDLE1:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_idle1");
				break;
			}
		case STORMTROOPER_SHOOT_MOVE_FWD:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_shoot_move_fwd");
				break;
			}
		case STORMTROOPER_SHOOT_N_DIE:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_shoot_n_die");
				break;
			}
		case STORMTROOPER_ATTITUDE_WALK_LEFT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_attitude_walk_left");
				break;
			}
		case STORMTROOPER_ATTITUDE_WALK_RIGHT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_attitude_walk_right");
				break;
			}
		case STORMTROOPER_WALK_2STEPS:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_PROXY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("stormtrooper_walk_2steps");
				break;
			}
		case WALK_ROBOT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_RIGIDBODY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->playAnimation("Walk");
				break;
			}
			case STOP_ROBOT:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==ANIMATED_CHARACTER_RIGIDBODY)
					{
						break;
					}
				}
				if(index < (int)objectList.size())
				objectList[index]->stopAnimation();
				break;
			}
			case GOBLIN_WALK:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==GRAPHICS_OBJECT)
					{
						objectList[index]->playAnimation("GoblinWalk");
					}
				}
				break;
			}
			case GOBLIN_STOP_ANIMATIONS:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==GRAPHICS_OBJECT)
					{
						objectList[index]->stopAnimation();
					}
				}
				break;
			}
			case GOBLIN_IDLE:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==GRAPHICS_OBJECT)
					{
						objectList[index]->playAnimation("GoblinIdle");
					}
				}
				break;
			}
			case GOBLIN_ATTACK:
			{
				vector<GameObjectReplica*> objectList;
				m_pHavokNetMgr->QueryObjectsByGUID(objectList,rakNetPacket->guid);
				int index;
				for(index = 0;index < (int)objectList.size();index++)
				{
					if(objectList[index]->GetHavokObjectType()==GRAPHICS_OBJECT)
					{
						objectList[index]->playAnimation("GoblinAttack");
					}
				}
				break;
			}


		default:
			outputLog << "[ServerScreen::HandleCustomPacket] Don't know how to handle packet with ID " << (int)rakNetPacket->data[0] << endl;
			break;
		}

		return true;
	}
}

#endif