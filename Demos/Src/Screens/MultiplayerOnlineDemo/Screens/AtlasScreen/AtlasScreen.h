#pragma once

// GameScreen
#include "GameScreen.h"

#ifdef HAVOK

namespace GamePipeGame
{
	class AtlasScreen:public GamePipe::GameScreen
	{
		private:
		protected:
		public:
			static AtlasScreen* a_instance;
			static AtlasScreen* Get();

			bool LoadResources();
			bool UnloadResources();

			bool Show();

			bool Initialize();
			bool Destroy();

			bool Update();
			bool Draw();

			bool OnKeyPress(const GIS::KeyEvent &keyEvent);

			AtlasScreen(std::string name);
			~AtlasScreen(){};
	};
}


#endif
