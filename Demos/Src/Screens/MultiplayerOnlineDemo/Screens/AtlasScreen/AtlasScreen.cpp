#include "StdAfx.h"

#ifdef HAVOK
#include "AtlasScreen.h"
#include "MultiplayerOnlineDemo\Screens\HomeScreen\HomeScreen.h"

#include <string>
#include <vector>

// Engine
#include "Engine.h"

// AtlasQuery
#include "LeaderBoardQuery.h"

namespace GamePipeGame
{
	bool isLeaderBoardDisplayed = false;
	AtlasScreen* AtlasScreen::a_instance = 0;

	std::vector<std::string>  leaderBoardData;
	const char* LEADER_BOARD_TEXT_NAME = "AtlasScreenLeaderBoard";

	const char* LOADING_TEXT_NAME = "AtlasScreenLoading";

	const char* CMD_TEXT_NAME = "AtlasScreenCommands";
	const char* s_availableCommandsText[] = {
		"Game Leader Board",
		"ESC->Quit"
	};
	AtlasScreen* AtlasScreen::Get()
	{
		if (!a_instance)
			a_instance = new AtlasScreen("AtlasScreen");

		return a_instance;
	}

	//////////////////////////////////////////////////////////////////////////
	// Atlas Screen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	AtlasScreen::AtlasScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		//------------------------------------------------
		// Add text for available commands
		//------------------------------------------------
		std::string commandText;
		int numCommands = sizeof(s_availableCommandsText)/sizeof(s_availableCommandsText[0]);
		for (int idx = 0; idx < numCommands; idx++)
		{
			commandText += s_availableCommandsText[idx];
			commandText += "\n";
		}
		UtilitiesPtr->Add(CMD_TEXT_NAME, new GamePipe::DebugText(commandText.c_str(), 0.25f, 0.25f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
		std::string loadingText = "Loading Leader Board...";
		UtilitiesPtr->Add(LOADING_TEXT_NAME, new GamePipe::DebugText(loadingText.c_str(), 0.25f, 0.5f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
		// first do the prerequisite initialization and such so we'll be ready to authenticate a player
		printf("Doing the prerequisite setup and initialization\n");
		if (!QuerySetup())
			return false;

		// the beef of the sample app - you can also reference this sample code on the developer portal 'Docs'
		printf("Retrieve the first page of the player stats leaderboard\n");
		if (!QueryLeaderboard())
			return false;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::Update()
	{
		if (!isLeaderBoardDisplayed)
		{
			std::string boardText;
			int result = QueryThink(&leaderBoardData);
			if (result == 1)
			{
			
				for (unsigned int i = 0; i < leaderBoardData.size(); i++)
				{
					boardText += leaderBoardData[i];
					boardText += "\n";
				}
			}
			else if (result == -1)
			{
				boardText = "Load Leader Board Failed...";
			}
			if (result != 0)
			{
				UtilitiesPtr->Remove(LOADING_TEXT_NAME);
				UtilitiesPtr->Add(LEADER_BOARD_TEXT_NAME, new GamePipe::DebugText(boardText.c_str(), 0.0f, 0.5f, 4.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
				isLeaderBoardDisplayed = true;
			}
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::Destroy()
	{
		//------------------------------------------------
		// Destroy text for available commands
		//------------------------------------------------
		UtilitiesPtr->Remove(CMD_TEXT_NAME);
		UtilitiesPtr->Remove(LEADER_BOARD_TEXT_NAME);

		// shutdown the Core object, then we'll wait for user input before killing the app
		printf("Cleaning up after ourselves\n");
		if (!QueryCleanup())
			return false;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool AtlasScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
			case GIS::KC_ESCAPE:
			{
				EnginePtr->AddGameScreen(new GamePipeGame::MultiplayerOnlineHomeScreen("HomeScreen"));
				Close();
				break;
			}
		}
		return true;
	}
}

#endif