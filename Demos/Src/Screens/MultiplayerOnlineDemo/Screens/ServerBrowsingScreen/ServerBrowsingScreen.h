#pragma once

#ifdef HAVOK

#include "GameScreen.h"

namespace GamePipeGame
{
	class ServerBrowsingScreen:public GamePipe::GameScreen
	{
		public:
			static ServerBrowsingScreen* s_instance;
			static ServerBrowsingScreen* Get();

			bool LoadResources();
			bool UnloadResources();

			bool Show();

			bool Initialize();
			bool Destroy();

			bool Update();
			bool Draw();

			bool OnKeyPress(const GIS::KeyEvent &keyEvent);

			ServerBrowsingScreen(std::string name);
			~ServerBrowsingScreen(){};
	};
}

#endif