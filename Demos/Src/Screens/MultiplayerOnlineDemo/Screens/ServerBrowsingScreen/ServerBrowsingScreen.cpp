#include "StdAfx.h"

#ifdef HAVOK

#include "ServerBrowsingScreen.h"
#include "ServerBrowsing.h"
#include "MultiplayerOnlineDemo\Screens\HomeScreen\HomeScreen.h"
#include "MultiplayerOnlineDemo\Screens\ClientScreen\ClientScreen.h"

#include <string>
#include <vector>

// Engine
#include "Engine.h"


namespace GamePipeGame
{
	ServerBrowsingScreen* ServerBrowsingScreen::s_instance = 0;

	const char* SERVER_SCREEN_TEXT_NAME = "ServerList";

	const char* LOADING_TEXT = "ServerBrowsingScreenLoading";

	const char* CMD_TEXT = "ServerBrowsingScreenCommands";

	const char* s_availableCommands[] = {
		"Server #->Connect",
		"N->Next Screen",
		"ESC->Quit"
	};
	bool isServerListDisplayed = false;
	int ctr = 0;
	std::vector<std::string> servers;

	ServerBrowsingScreen* ServerBrowsingScreen::Get()
	{
		if (!s_instance)
			s_instance = new ServerBrowsingScreen("ServerBrowserScreen");

		return s_instance;
	}

	//////////////////////////////////////////////////////////////////////////
	// Server Browser Screen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	ServerBrowsingScreen::ServerBrowsingScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		//------------------------------------------------
		// Add text for available commands
		//------------------------------------------------
		std::string commandText;
		int numCommands = sizeof(s_availableCommands)/sizeof(s_availableCommands[0]);
		for (int idx = 0; idx < numCommands; idx++)
		{
			commandText += s_availableCommands[idx];
			commandText += "\n";
		}
		UtilitiesPtr->Add(CMD_TEXT, new GamePipe::DebugText(commandText.c_str(), 0.25f, 0.25f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
		std::string loadingText = "Loading Server List...";
		UtilitiesPtr->Add(LOADING_TEXT, new GamePipe::DebugText(loadingText.c_str(), 0.25f, 0.5f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::Update()
	{
		if(!isServerListDisplayed)
		{
			GetServerList(&servers);
			UtilitiesPtr->Remove(LOADING_TEXT);
			string list;
			int size = servers.size();
			for(int i = ctr * 10; i < size && i < (ctr * 10 + 10); i++)
			{
				list += servers[i];
			}
			UtilitiesPtr->Add(SERVER_SCREEN_TEXT_NAME, new GamePipe::DebugText(list.c_str(), 0.0f, 0.5f, 4.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
			isServerListDisplayed = true;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::Destroy()
	{
		UtilitiesPtr->Remove(CMD_TEXT);
		UtilitiesPtr->Remove(SERVER_SCREEN_TEXT_NAME);
		s_instance = 0;
		if(!isServerListDisplayed)
			UtilitiesPtr->Remove(LOADING_TEXT);
		isServerListDisplayed = false;
		ctr = 0;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool ServerBrowsingScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
			case GIS::KC_ESCAPE:
			{
				EnginePtr->AddGameScreen(new GamePipeGame::MultiplayerOnlineHomeScreen("HomeScreen"));
				Close();
				break;
			}
			
			case GIS::KC_N:
			{
				ctr++;
				isServerListDisplayed = false;
				break;
			}

			default:
			{
				if(keyEvent.key >= 2 && keyEvent.key <= 11)
				{
					int size = servers.size();
					int temp = keyEvent.key - 2;
					int index;
					if(temp < 9)
						index = ctr * 10 + temp + 1;
					else
						index = ctr * 10;
					if(index < size)
					{
						ConnectToServer(index);
						EnginePtr->AddGameScreen(GamePipeGame::ClientScreen::Get(getServerIP(), getPort()));
						Close();
					}
				}
			}
		}
		return true;
	}
}

#endif