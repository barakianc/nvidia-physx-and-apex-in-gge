#include "StdAfx.h"

#ifdef HAVOK
// HomeScreen
#include "HomeScreen.h"

// Engine
#include "Engine.h"

// Client & Server
#include "MultiplayerOnlineDemo\Screens\ServerScreen\ServerScreen.h"
#include "MultiplayerOnlineDemo\Screens\ClientScreen\ClientScreen.h"
#include "MultiplayerOnlineDemo\Screens\AtlasScreen\AtlasScreen.h"
#include "MultiplayerOnlineDemo\Screens\ServerBrowsingScreen\ServerBrowsingScreen.h"
#include "MultiplayerOnlineDemo\Screens\PresenceScreen\PresenceScreen.h"
#include "GameSpyPresence.h"
#include "ServerBrowsing.h"

#include "gsAvailable.h"
#include "natneg.h"
#include "nninternal.h"

ServerBrowsing sb1;

static const char* AVAILABLE_CMDS_NAME = "StartupScreenCommands";
static const char* s_availableCommandsText[] = {
	"MULTIPLAYER DEMO OPTIONS",
	"S->Start Server",
	"C->Start Client",
	"2U, then C->Init Server Browsing",
	"L->Leader Board",
	"G->GameSpy Presence Init",
	"ESC->Quit"
};

namespace GamePipeGame
{
	// hack to get screens to show up correctly since MainMenuScreen doesn't know how
	// to deal with a demo that uses more than 1 screen. sigh.
	bool MultiplayerOnlineHomeScreen::s_demoIsRunning = false;
	bool MultiplayerOnlineHomeScreen::DemoIsRunning()
	{
		return s_demoIsRunning;
	}

	//////////////////////////////////////////////////////////////////////////
	// MultiplayerOnlineHomeScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	MultiplayerOnlineHomeScreen::MultiplayerOnlineHomeScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);
		s_demoIsRunning = true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		//------------------------------------------------
		// Add text for available commands
		//------------------------------------------------
		std::string commandText;
		int numCommands = sizeof(s_availableCommandsText)/sizeof(s_availableCommandsText[0]);
		for (int idx = 0; idx < numCommands; idx++)
		{
			commandText += s_availableCommandsText[idx];
			commandText += "\n";
		}
		UtilitiesPtr->Add(AVAILABLE_CMDS_NAME, new GamePipe::DebugText(commandText.c_str(), 0.25f, 0.25f, 5.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Center));
		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::Destroy()
	{
		//------------------------------------------------
		// Destroy text for available commands
		//------------------------------------------------
		UtilitiesPtr->Remove(AVAILABLE_CMDS_NAME);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::Update()
	{
		sb1.think();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool MultiplayerOnlineHomeScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				s_demoIsRunning = false;
				Close();
				break;
			}
		case GIS::KC_S: // Start Server
			{				
				EnginePtr->AddGameScreen(GamePipeGame::ServerScreen::Get());
				Close();
				break;
			}
		case GIS::KC_L:
			{
				EnginePtr->AddGameScreen(GamePipeGame::AtlasScreen::Get());
				Close();
				break;
			}
		case GIS::KC_G:
			{
				// Srikkanth: These calls are just for testing purposes. We'll have a proper UI for making calls to these functions once we've confirmed they work
				EnginePtr->AddGameScreen(GamePipeGame::PresenceScreen::Get());
				Close();
				break;
			}
		case GIS::KC_U:
			{
				sb1.update();
				break;
			}
		case GIS::KC_C:
			{
				EnginePtr->AddGameScreen(GamePipeGame::ServerBrowsingScreen::Get());
				Close();
				break;
			}
		case GIS::KC_2:
			{
				sb1.newServerBrowser();
				break;
			}
		}

		return true;
	}
}

#endif