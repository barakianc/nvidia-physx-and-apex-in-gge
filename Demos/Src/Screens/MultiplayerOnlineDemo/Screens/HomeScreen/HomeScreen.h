#pragma once

#ifdef HAVOK

// GameScreen
#include "GameScreen.h"

namespace GamePipeGame
{
	class MultiplayerOnlineHomeScreen : public GamePipe::GameScreen
	{
	private:
		static bool s_demoIsRunning;

	protected:
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

		static bool DemoIsRunning();
	
	public:
		MultiplayerOnlineHomeScreen(std::string name);
		~MultiplayerOnlineHomeScreen() {}
	};
}

#endif