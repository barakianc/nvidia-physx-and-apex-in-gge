#pragma once

#ifdef HAVOK

// GameScreen
#include "GameScreen.h"

// RakNet
#include "MultiplayerOnline\RakNet\HavokNetManager.h"

// Custom (game-specific) packets
#include "MultiplayerOnlineDemo\Common\MultiplayerOnlineDemoPackets.h"

namespace GamePipeGame
{
	class ClientScreen : public GamePipe::GameScreen
	{
	private:
		static const int DEFAULT_PORT = 0;
		static const char* DEFAULT_REMOTE_HOST;

		HavokNetManager* m_pHavokNetMgr;
		std::deque<LogMessage *> m_messageLog;
		Ogre::ConfigFile m_configFile;
		EntityMapType m_entityMap;
		std::vector<std::string> m_staticObjectNames;

		bool LoadConfigFile(const char* configFileName);
		bool InitHavokNetMgr(const char* serverIP, int port);

		void SetLogMessage(std::string newMessage, Ogre::ColourValue &newColor);
		void AddLogMessage(std::string newMessage, int newPriority = 0);
		void ProcessLog(float deltaTime);

		void HandleCreateStaticObjectPacket(CreateStaticObjectPacket packet);

		ClientScreen(std::string name, const char* serverIP = 0, int port = -1, const char* configFileName = "multiplayer_online_demo.config");
		static ClientScreen* s_instance;

	protected:

	public:
		static ClientScreen* Get(const char* serverIP = 0, int port = -1);
		~ClientScreen() {}

		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

		bool HandleCustomPacket(const RakNet::Packet* const rakNetPacket);
		int sendMessageToServer(CustomPacketID messageID);
	};
}

#endif