#include "StdAfx.h"

#ifdef HAVOK
// ClientScreen
#include "ClientScreen.h"

// HomeScreen
#include "MultiplayerOnlineDemo\Screens\HomeScreen\HomeScreen.h"

// Engine
#include "Engine.h"

// Physics
#include "HavokWrapper.h"

// Common constants for demo
#include "MultiplayerOnlineDemo\Common\MultiplayerOnlineDemoDefines.h"

#include "GameReport.h"

static const char* LOG_NAME = "ClientScreenLog";
static const char* AVAILABLE_CMDS_NAME = "ClientScreenCommands";
static const char* s_availableCommandsText[] = {
	"CLIENT OPTIONS",
	"C: Add Trooper(ANIMATED_CHARACTER_PROXY)",
	"G: Add Goblin(GRAPHICS_OBJECT)",
	"R: Add Robot(ANIMATED_CHARACTER_RIGIDBODY)",
	"",
	"ANIMATIONS",
	"0-9,P,O,I,U: Trooper Animations",
	"Y: Robot Walk",
	"J,K,L: Goblin Animations",
	"SPACE: Stop Animation of trooper",
	"H: Stop Goblin Animation",
	"V: Stop Robot"
};

namespace GamePipeGame
{
	extern ofstream outputLog; // This is defined in GameObjectReplica.
	
	ClientScreen* ClientScreen::s_instance = 0;
	const char* ClientScreen::DEFAULT_REMOTE_HOST = "127.0.0.1";

	//////////////////////////////////////////////////////////////////////////
	// HandleCustomPacketCB(RakNet::Packet* rackNetPacket) -- static callback
	//////////////////////////////////////////////////////////////////////////
	static bool HandleCustomPacketCB(const RakNet::Packet* const rakNetPacket)
	{
		return ClientScreen::Get()->HandleCustomPacket(rakNetPacket);
	}

	//////////////////////////////////////////////////////////////////////////
	// Get()
	// The server screen uses the singleton pattern so that it can handle
	// custom game packets via the callback above.
	//////////////////////////////////////////////////////////////////////////
	ClientScreen* ClientScreen::Get(const char* serverIP /* = 0*/, int port /* = -1*/)
	{
		if (!s_instance)
			s_instance = new ClientScreen("ClientScreen", serverIP);

		return s_instance;
	}

	//////////////////////////////////////////////////////////////////////////
	// ClientScreen(std::string name, const char* configFileName)
	//////////////////////////////////////////////////////////////////////////
	ClientScreen::ClientScreen(std::string name, 
		                       const char* serverIP /*= 0*/, 
		                       int port /*= -1*/, 
							   const char* configFileName /*= "networkedhavokdemo.config"*/) : 
		GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(false);
		SetIsPopup(false);

		if (!LoadConfigFile(configFileName))
		{
			cout << "[ClientScreen] There were errors processing the server configuration file '" << configFileName << "'." << endl;
			return;
		}

		if (!InitHavokNetMgr(serverIP, port))
		{
			cout << "[ClientScreen] Error intializing Havok Net Manager on the client!\n" << endl;
			return;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().LoadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstanceRef().UnloadResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::Initialize()
	{
		GetGameObjectManager()->SetVisualDebugger(true);
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		//------------------------------------------------
		// Manual/hard-coded portion of scene setup
		//------------------------------------------------
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;

		//------------------------------------------------
		// Set up camera
		//------------------------------------------------
		m_v3CameraPosition = Ogre::Vector3(0.0f, 0.0f, 50.0f);

		//------------------------------------------------
		// Add Ambient Lighting
		//------------------------------------------------
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000f, 0.5000f, 0.5000f, 0.5000f));

		//------------------------------------------------
		// Add point lights
		//------------------------------------------------
		Ogre::Light* pointLight1 = pSceneManager->createLight("pointLight1");
		pointLight1->setType(Ogre::Light::LT_POINT);
		pointLight1->setPosition(Ogre::Vector3(0, 150, 250));
		pointLight1->setDiffuseColour(1.0, 1.0, 1.0);
        pointLight1->setSpecularColour(1.0, 1.0, 1.0);

		Ogre::Light* pointLight2 = pSceneManager->createLight("pointLight2");
		pointLight2->setType(Ogre::Light::LT_POINT);
		pointLight2->setPosition(Ogre::Vector3(0, 150, -250));
		pointLight2->setDiffuseColour(1.0, 1.0, 1.0);
        pointLight2->setSpecularColour(1.0, 1.0, 1.0);

		Ogre::Light* pointLight3 = pSceneManager->createLight("pointLight3");
		pointLight3->setType(Ogre::Light::LT_POINT);
		pointLight3->setPosition(Ogre::Vector3(250, 150, 250));
		pointLight3->setDiffuseColour(1.0, 1.0, 1.0);
        pointLight3->setSpecularColour(1.0, 1.0, 1.0);

		Ogre::Light* pointLight4 = pSceneManager->createLight("pointLight4");
		pointLight4->setType(Ogre::Light::LT_POINT);
		pointLight4->setPosition(Ogre::Vector3(-250, 150, 0));
		pointLight4->setDiffuseColour(1.0, 1.0, 1.0);
        pointLight4->setSpecularColour(1.0, 1.0, 1.0);

		//------------------------------------------------
		// Create visual for log messages
		//------------------------------------------------
		m_messageLog.clear();
		UtilitiesPtr->Add(LOG_NAME, new GamePipe::DebugText("", (Ogre::Real)0.01, (Ogre::Real)0.97, (Ogre::Real)3));
		AddLogMessage("CS522 Fall 2011, Multiplayer Online Demo Client");

		//------------------------------------------------
		// Add text for available commands
		//------------------------------------------------
		std::string commandText;
		int numCommands = sizeof(s_availableCommandsText)/sizeof(s_availableCommandsText[0]);
		for (int idx = 0; idx < numCommands; idx++)
		{
			commandText += s_availableCommandsText[idx];
			commandText += "\n";
		}
		UtilitiesPtr->Add(AVAILABLE_CMDS_NAME, new GamePipe::DebugText(commandText.c_str(), 0.05f, 0.05f, 3.0f, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));

		//TODO-SV: delete GameObject when an Entity is deleted in GLE cant be done yet
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::Destroy()
	{
		outputLog.close();
		m_pHavokNetMgr->NetShutdown();

		// RMF: cannot delete m_pNetHavokMgr here; parts of it are already
		// deleted elsewhere. I believe this is a flaw in the way the previous
		// networking team destructed net manager objects.

		//------------------------------------------------
		// Destroy text for log messages
		//------------------------------------------------
		UtilitiesPtr->Remove(LOG_NAME);

		//------------------------------------------------
		// Destroy text for available commands
		//------------------------------------------------
		UtilitiesPtr->Remove(AVAILABLE_CMDS_NAME);

		// 'this' server will be deleted elsewhere. 
		// need to simulate deleting the client singleton by setting ptr to 0.
		s_instance = 0;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		//--------------------------------------------------------
		// Update Camera Position
		//--------------------------------------------------------
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::Update()
	{
		//-------------------------------------
		// Do Havok Net Manager Updates
		//-------------------------------------
		if (!m_pHavokNetMgr->NotStarted())
		{
			m_pHavokNetMgr->ProcessMessages();
			m_pHavokNetMgr->UpdateNetworkObjectsInSceneWithHavok(GetDefaultSceneManager(), m_entityMap, m_pGameObjectManager);
			m_pHavokNetMgr->UpdateNetworkObjectsAfterHavokStep();
		}

		//-------------------------------------
		// Update log message display as needed
		//-------------------------------------
		ProcessLog(EnginePtr->GetDeltaTime());

		//-------------------------------------
		// Update client visuals
		//-------------------------------------
		// TODO

		// Update Camera
		FreeCameraMovement();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::Draw()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// OnKeyPress(const GIS::KeyEvent& keyEvent)
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				PlayFakeMatch();
				ReportStats(1);
				ReportCleanup(1);
				EnginePtr->AddGameScreen(new GamePipeGame::MultiplayerOnlineHomeScreen("HomeScreen"));
				Close();
				break;
			}
		case GIS::KC_B:
			{
				// make a request to the server to create a ball
				CreateBallPacket packet(GetActiveCameraSceneNode()->getPosition());
				RakNet::BitStream bitStream;
				packet.WriteToBitStream(bitStream);
				m_pHavokNetMgr->SendPacket(bitStream);
				break;
			}
		case GIS::KC_C:
			{
				sendMessageToServer(CREATE_ANIMATED_CHARACTER_PROXY);
				break;
			}
			
			case GIS::KC_G:
			{
				sendMessageToServer(CREATE_GRAPHICS_OBJECT);
				break;
			}
			case GIS::KC_R:
			{
				sendMessageToServer(CREATE_ANIMATED_CHRACTER_RIGIDBODY);
				break;
			}
			case GIS::KC_Y:
			{
				sendMessageToServer(WALK_ROBOT);
				break;
			}
			case GIS::KC_J:
			{
				sendMessageToServer(GOBLIN_WALK);
				break;
			}
			case GIS::KC_K:
			{
				sendMessageToServer(GOBLIN_IDLE);
				break;
			}
			case GIS::KC_L:
			{
				sendMessageToServer(GOBLIN_ATTACK);
				break;
			}
			case GIS::KC_1:
			{
				sendMessageToServer(STORMTROOPER_ATTITUDE_WALK);
				break;
			}
			case GIS::KC_2:
			{
				sendMessageToServer(STORMTROOPER_GUARD_N_WAIT);
				break;
			}
			case GIS::KC_3:
			{
				sendMessageToServer(STORMTROOPER_TALK);
				break;
			}
			case GIS::KC_4:
			{
				sendMessageToServer(STORMTROOPER_CHALLENGE_OPP);
				break;
			}
			case GIS::KC_5:
			{
				sendMessageToServer(STORMTROOPER_SPOT_N_SHOOT);
				break;
			}
			case GIS::KC_6:
			{
				sendMessageToServer(STORMTROOPER_DANCE);
				break;
			}
			case GIS::KC_7:
			{
				sendMessageToServer(STORMTROOPER_IDLE_TO_SHOOT);
				break;
			}
			case GIS::KC_8:
			{
				sendMessageToServer(STORMTROOPER_IDLE);
				break;
			}
			case GIS::KC_9:
			{
				sendMessageToServer(STORMTROOPER_IDLE1);
				break;
			}
			case GIS::KC_0:
			{
				sendMessageToServer(STORMTROOPER_SHOOT_MOVE_FWD);
				break;
			}
			case GIS::KC_P:
			{
				sendMessageToServer(STORMTROOPER_SHOOT_N_DIE);
				break;
			}
			case GIS::KC_O:
			{
				sendMessageToServer(STORMTROOPER_ATTITUDE_WALK_LEFT);
				break;
			}
			case GIS::KC_I:
			{
				sendMessageToServer(STORMTROOPER_ATTITUDE_WALK_RIGHT);
				break;
			}
			case GIS::KC_U:
			{
				sendMessageToServer(STORMTROOPER_WALK_2STEPS);
				break;
			}
			
			case GIS::KC_SPACE:
				{
					sendMessageToServer(STORMTROOPER_STOP_ANIMATIONS);
					break;
				}
			case GIS::KC_H:
			{
				sendMessageToServer(GOBLIN_STOP_ANIMATIONS);
				break;
			}
			case GIS::KC_V:
			{
				sendMessageToServer(STOP_ROBOT);
				break;
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// InitHavokNetMgr()
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::InitHavokNetMgr(const char* serverIP, int port)
	{
		m_pHavokNetMgr = new HavokNetManager(&HandleCustomPacketCB);
		if (!m_pHavokNetMgr)
			return false;

		m_pHavokNetMgr->SetMessageLog(&m_messageLog);
		m_pHavokNetMgr->SetConfigFile(&m_configFile);

		m_pHavokNetMgr->SetTopology(CLIENT);
		m_pHavokNetMgr->CreateNetworkIDManager(false);

		Ogre::String configStrLocalSocketPort = m_configFile.getSetting("client.localport");
		Ogre::String configStrMaxConnections = m_configFile.getSetting("client.maxconnections");
		Ogre::String configStrSleepInterval = m_configFile.getSetting("client.sleepinterval");
		Ogre::String configStrRemoteHost = m_configFile.getSetting("client.remotehost");
		Ogre::String configStrRemotePort = m_configFile.getSetting("client.remoteport");
		Ogre::String configStrPassword = m_configFile.getSetting("client.password");

		unsigned short configLocalSocketPort = (configStrLocalSocketPort.empty())? DEFAULT_PORT : ((unsigned short)Ogre::StringConverter::parseInt(configStrLocalSocketPort));
		int configMaxConnections = (configStrMaxConnections.empty())? MultiplayerOnlineDemoDefines::DEFAULT_MAX_CONNECTIONS : Ogre::StringConverter::parseInt(configStrMaxConnections);
		int configSleepInterval = (configStrSleepInterval.empty())? MultiplayerOnlineDemoDefines::DEFAULT_SLEEP_INTERVAL : Ogre::StringConverter::parseInt(configStrSleepInterval);
		Ogre::String configRemoteHost = (configStrRemoteHost.empty())? DEFAULT_REMOTE_HOST : configStrRemoteHost;
		int configRemotePort = (configStrRemotePort.empty())? MultiplayerOnlineDemoDefines::DEFAULT_SERVER_PORT : Ogre::StringConverter::parseInt(configStrRemotePort);
		Ogre::String configPassword = configStrPassword;

		m_pHavokNetMgr->SetLocalSocketPort(configLocalSocketPort);
		m_pHavokNetMgr->SetMaxConnections(configMaxConnections);
		m_pHavokNetMgr->SetSleepInterval(configSleepInterval);
		// use the explicitly provided serverIP if we have one. otherwise just use the default from the config file.
		std::string remoteHost = serverIP ? serverIP : configRemoteHost;
		m_pHavokNetMgr->SetRemoteHost(remoteHost.c_str());
		// use the explicitly provided port if we have one. otherwise just use the default from the config file.
		m_pHavokNetMgr->SetRemotePort((-1 != port) ? port : configRemotePort);
		m_pHavokNetMgr->SetPasswordOptions(configStrPassword.c_str(), configStrPassword.length());

		AddLogMessage("Starting client on port " + Ogre::StringConverter::toString(configLocalSocketPort));
		m_pHavokNetMgr->Initialize();

		AddLogMessage("Enabling plug-in: RakNet/ReplicaManager3");
		m_pHavokNetMgr->EnableRakNetPlugin("ReplicaManager3");

		AddLogMessage("Attempting to connect to: " + configRemoteHost + ":" + Ogre::StringConverter::toString(configRemotePort));
		if (NETMANAGER_SUCCESS == m_pHavokNetMgr->Connect())
			AddLogMessage("Connection to server succeeded!");
		else
			AddLogMessage("Connection to server failed!");

		outputLog.open("MultiplayerOnlineDemo_Client.txt");

		outputLog << "client socket port: " << configLocalSocketPort << endl;
		outputLog << "client max connections: " << configMaxConnections << endl;
		outputLog << "client sleep interval: " << configSleepInterval << endl;
		outputLog << "client remote host: " << configRemoteHost << endl;
		outputLog << "client remote port: " << configRemotePort << endl;
		outputLog << "client password: " << configPassword << endl;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// AddLogMessage(std::string message, int priority)
	//////////////////////////////////////////////////////////////////////////
	void ClientScreen::AddLogMessage(std::string message, int priority /*= 0*/)
	{
		if (!m_messageLog.empty())
			m_messageLog.front()->messageAge += 1;

		m_messageLog.push_back(new LogMessage(message, priority));
	}

	//////////////////////////////////////////////////////////////////////////
	// SetLogMessage(std::string newMessage, Ogre::ColourValue &newColor)
	//////////////////////////////////////////////////////////////////////////
	void ClientScreen::SetLogMessage(std::string newMessage, Ogre::ColourValue &newColor)
	{
		UtilitiesPtr->GetDebugText(LOG_NAME)->SetText(newMessage);
		UtilitiesPtr->GetDebugText(LOG_NAME)->SetColor(newColor);
	}

	//////////////////////////////////////////////////////////////////////////
	// ProcessLog(float deltaTime)
	//////////////////////////////////////////////////////////////////////////
	void ClientScreen::ProcessLog(float deltaTime)
	{
		if (!m_messageLog.empty())
		{
			LogMessage *first = m_messageLog.front();
			if(first->messageAge > MultiplayerOnlineDemoDefines::MAX_LOG_MSG_AGE)
			{
				m_messageLog.pop_front();
				delete first;
			}
		}

		Ogre::ColourValue textColor = Ogre::ColourValue(1,1,1,1);
		if(!m_messageLog.empty())
		{
			LogMessage* first = m_messageLog.front();

			first->messageAge += deltaTime;
			float remainingTime = MultiplayerOnlineDemoDefines::MAX_LOG_MSG_AGE - first->messageAge;

			textColor.a = (remainingTime < 1) ? remainingTime : 1;
			if (textColor.a < 0)	
				textColor.a = 0;
			SetLogMessage(first->messageText, textColor);
		}
		else
		{
			SetLogMessage("", Ogre::ColourValue(1,1,1,1));
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// LoadConfigFile(const char* configFileName)
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::LoadConfigFile(const char* configFileName)
	{
		// NOTE: config file is expected to be in the same file as MediaPaths.txt!

		std::string path = "";
		path.append("../../");
		path.append(GamePipe::Engine::GetInstancePointer()->gameFolder.c_str());
		path.append("/");
		path.append(configFileName);

		ifstream ifs (path.c_str(), ifstream::in);

		if (ifs.good())
		{
			m_configFile.load(path.c_str(), "\x09:=", true);
		}
		else
		{
			cout << "[ClientScreen::LoadConfigFile] Could not find configuration file: '" << configFileName << "'" << endl;
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// HandleCreateStaticObjectPacket(CreateStaticObjectPacket packet)
	//////////////////////////////////////////////////////////////////////////
	void ClientScreen::HandleCreateStaticObjectPacket(CreateStaticObjectPacket packet)
	{
		// if we've already created this static object, don't do it again
		for (unsigned idx = 0; idx < m_staticObjectNames.size(); idx++)
		{
			if (!strcmp(packet.GetObjectName(), m_staticObjectNames[idx].c_str()))
				return;
		}

		GameObject* newObject = new GameObject(packet.GetObjectName(), packet.GetMeshName(), packet.GetHKXFileName(), packet.GetObjectType());
		newObject->setPosition(packet.GetPosition().x, packet.GetPosition().y, packet.GetPosition().z);
		m_staticObjectNames.push_back(std::string(packet.GetObjectName()));
	}

	//////////////////////////////////////////////////////////////////////////
	// HandleCustomPacket(RakNet::Packet* rakNetPacket)
	//////////////////////////////////////////////////////////////////////////
	bool ClientScreen::HandleCustomPacket(const RakNet::Packet* const rakNetPacket)
	{
		switch (rakNetPacket->data[0])
		{
		case CREATE_STATIC_OBJECT:
			{
			CreateStaticObjectPacket packet;
			if (!packet.LoadFromRakNetPacket(rakNetPacket))
				return false;
			HandleCreateStaticObjectPacket(packet);
			}
			break;
		case ATLAS_SESSION_ID:
			{
				AtlasReportSessionPacket atlasPacket;
				if (!atlasPacket.LoadFromRakNetPacket(rakNetPacket))
					return false;
				std::printf("Get Session Id: %s\n", atlasPacket.GetSessionId());
				ReportClientSetup(1, atlasPacket.GetSessionId());
				break;
			}
		default:
			outputLog << "[ClientScreen::HandleCustomPacket] Don't know how to handle packet with ID " << (int)rakNetPacket->data[0] << endl;
			break;
		}

		return true;
	}
	int ClientScreen::sendMessageToServer(CustomPacketID messageID)
{
				RakNet::BitStream bitStream;
				bitStream.Write((unsigned char)messageID);
				Ogre::Vector3 position = GetActiveCameraSceneNode()->getPosition();
				position.z -=10;
				bitStream.Write(position.x);
				bitStream.Write(position.y);
				bitStream.Write(position.z);
				m_pHavokNetMgr->SendPacket(bitStream);
				return 0;
}
}

#endif
