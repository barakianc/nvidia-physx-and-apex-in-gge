#include "StdAfx.h"
// Includes
#include "AnimationDemo.h"
#include "Main.h"


namespace GamePipeGame
{

	/*! Constructor
	*	@param[in]	name	Name of the screen
	*/
	AnimationDemo::AnimationDemo(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);

		// initialize booleans
		DEBUG_MODE_ENABLED = false;
		RAGDOLL_ENABLED = false;
		screenIndex = 0;

		// remove the main menu screen's text
		UtilitiesPtr->Remove("MainMenuScreenText");

		cameraLocations.push_back(Ogre::Vector3(130, 55, -30));
		cameraLocations.push_back(Ogre::Vector3(-90, 45, -200));
		cameraLocations.push_back(Ogre::Vector3(-28, 50, -30));
		cameraLocations.push_back(Ogre::Vector3(-80, 50, 180));

		positionIndex = 0;
		ogrePositionIndex = 3;
		movementDirection = -1;
		TRANSITION = false;
		OVERLAY = false;
		OVERLAYTRANSITION = false;
		animationIndex = 0;
	}

	/*! Load the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool AnimationDemo::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		return true;
	}

	/*! Unload the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool AnimationDemo::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}

	/*! Initializes the Model Viewer
	*	@return	True if everything went well
	*/
	bool AnimationDemo::Initialize()
	{
		
		// initialize debug text
		debugText = new GamePipe::DebugText("", Ogre::Real(0.005f), Ogre::Real(0.005f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
		UtilitiesPtr->Add("DebugText", debugText);
		animPlaying = new GamePipe::DebugText("Animations are Playing: ", Ogre::Real(0.005f), Ogre::Real(0.035f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
		UtilitiesPtr->Add("AnimationPlaying", animPlaying);
		defaultAnim = new GamePipe::DebugText("Default Animations: ", Ogre::Real(0.005f), Ogre::Real(0.155f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
		UtilitiesPtr->Add("DefaultAnimations", defaultAnim);
		ragdollState = new GamePipe::DebugText("StormTrooper1: RagdollPose: ", Ogre::Real(0.005f), Ogre::Real(0.305f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
		UtilitiesPtr->Add("RagdollState", ragdollState);

		//--------------------------------------------------------
		// Set Movement Speeds
		//--------------------------------------------------------
		m_fCameraMovementSpeed = 50.0;
		m_fCameraRotationSpeed = 5.0;

		//--------------------------------------------------------
		// Lighting
		//--------------------------------------------------------
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(1.0, 1.0, 1.0));

		//--------------------------------------------------------
		// Shadows
		//--------------------------------------------------------
		pSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

		//--------------------------------------------------------
		// Add Directional Lighting
		//--------------------------------------------------------
		Ogre::Light* directionalLight = pSceneManager->createLight("directionalLight");
		directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
		directionalLight->setDiffuseColour(Ogre::ColourValue(1.0, 1.0, 1.0));
		directionalLight->setSpecularColour(Ogre::ColourValue(1.0, 1.0, 1.0));
		directionalLight->setDirection(Ogre::Vector3( 0, -1, 1 ));

		//--------------------------------------------------------
		// Skybox
		//--------------------------------------------------------
		pSceneManager->setSkyBox(true, "MySkyBox", 1000);

		EnginePtr->GetForemostGameScreen()->GetGameObjectManager()->SetVisualDebugger(true);

		//-------------------------------------------------------------
		// Save default camera as havok camera, and create ogre camera
		//-------------------------------------------------------------

		havokCamera = GetActiveCamera();
		
		Ogre::Entity* box = pSceneManager->createEntity("box","box5.mesh");
		Ogre::SceneNode* pOgreCameraScene = pSceneManager->getRootSceneNode()->createChildSceneNode();
		
		pOgreCameraScene->attachObject(box);
		pOgreCameraScene->setPosition(0,300,50);
		pOgreCameraScene->setOrientation(0,0,1,0);
		
		ogreCamera = pSceneManager->createCamera("ogreCamera");
		pOgreCameraScene->attachObject(ogreCamera);

		ogreCamera->setNearClipDistance(0.1f);
		ogreCamera->setPosition(0,53,10);
		ogreCamera->lookAt(0,300,300);

		//---------------------------------------------
		// havok animation intitialization
		//---------------------------------------------
#ifdef HAVOK
		m_pStormtrooper1 = new GameObject(	"stormtrooper1",
									"stormtrooper_withgun_bound_LOD.mesh",
									"storm_trooper_ragdoll_complete.hkx",
									ANIMATED_CHARACTER_RIGIDBODY);
		m_pStormtrooper1->scale(20,20,20);
		m_pStormtrooper1->setPosition(-150, 0, 150);
		((CharacterRigidBodyObject*)m_pStormtrooper1->m_pPhysicsObject)->setInputs(0,0,0,90);
		m_pStormtrooper1->m_pGraphicsObject->m_pOgreEntity->setCastShadows(false);

		m_pStormtrooper1->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_guard_n_wait");

		allStormTrooperAnimations = m_pStormtrooper1->m_pAnimatedEntity->GetAnimationNames();

		m_pStormtrooper2 = new GameObject(	"stormtrooper2",
									"stormtrooper_withgun_bound_LOD.mesh",
									"storm_trooper_ragdoll_complete.hkx",
									ANIMATED_CHARACTER_RIGIDBODY);
		//static_cast<AnimationManager::AnimatedHavokEntity*>(m_pStormtrooper2->m_pAnimatedEntity)->AddRagDoll("Ragdoll_storm_trooper_ragdoll_complete.hkx");
		m_pStormtrooper2->scale(20,20,20);
		m_pStormtrooper2->setPosition(150, 0, 150);
		((CharacterRigidBodyObject*)m_pStormtrooper2->m_pPhysicsObject)->setInputs(0,0,0,90);
		m_pStormtrooper2->m_pGraphicsObject->m_pOgreEntity->setCastShadows(false);

		m_pStormtrooper2->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_challenge_opp");

		m_pStormtrooper3 = new GameObject(	"stormtrooper3",
									"stormtrooper_withgun_bound_LOD.mesh",
									"storm_trooper_ragdoll_complete.hkx",
									ANIMATED_CHARACTER_RIGIDBODY);
		m_pStormtrooper3->scale(20,20,20);
		m_pStormtrooper3->setPosition(150, 0, -150);
		m_pStormtrooper3->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_idle");
		((CharacterRigidBodyObject*)m_pStormtrooper3->m_pPhysicsObject)->setInputs(0,0,0,90);
		m_pStormtrooper3->m_pGraphicsObject->m_pOgreEntity->setCastShadows(false);


		m_pStormtrooper4 = new GameObject(	"stormtrooper4",
									"stormtrooper_withgun_bound_LOD.mesh",
									"storm_trooper_ragdoll_complete.hkx",
									ANIMATED_CHARACTER_RIGIDBODY);
		m_pStormtrooper4->scale(20,20,20);
		m_pStormtrooper4->setPosition(-150, 0, -150);
		((CharacterRigidBodyObject*)m_pStormtrooper4->m_pPhysicsObject)->setInputs(0,0,0,90);
		m_pStormtrooper4->m_pGraphicsObject->m_pOgreEntity->setCastShadows(false);
		m_pStormtrooper4->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_talk");

	//m_pStormtrooper1 = new GameObject(	"stormtrooper1",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper1->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper1->scale(20,20,20);
	//	m_pStormtrooper1->setPosition(-150, 0, 150);
	//	((CharacterRigidBodyObject*)m_pStormtrooper1->m_pPhysicsObject)->setInputs(0,0,0,90);
	//	m_pStormtrooper1->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_idle1");

	//	//std::vector<std::string> animationList1 = m_pStormtrooper1->m_pAnimatedEntity->GetAnimationNames();

	//	m_pStormtrooper2 = new GameObject(	"stormtrooper2",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper2->scale(20,20,20);
	//	m_pStormtrooper2->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper2->setPosition(0, 0, 150);
	//	((CharacterRigidBodyObject*)m_pStormtrooper2->m_pPhysicsObject)->setInputs(0,0,0,90);
	//	m_pStormtrooper2->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_idle1");

	//	m_pStormtrooper3 = new GameObject(	"stormtrooper3",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper3->scale(20,20,20);
	//	m_pStormtrooper3->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper3->setPosition(150, 0, 150);
	//	((CharacterRigidBodyObject*)m_pStormtrooper3->m_pPhysicsObject)->setInputs(0,0,0,90);
	//	m_pStormtrooper3->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_walk");
	//	//m_pStormtrooper3->m_pAnimatedEntity->StopAnimation();	// T-pose

	//	m_pStormtrooper4 = new GameObject(	"stormtrooper4",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper4->scale(20,20,20);
	//	m_pStormtrooper4->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper4->setPosition(150, 0, 0);
	//	((CharacterRigidBodyObject*)m_pStormtrooper4->m_pPhysicsObject)->setInputs(0,0,0,90);
	//	m_pStormtrooper4->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_walk");

	//	m_pStormtrooper5 = new GameObject(	"stormtrooper5",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper5->scale(20,20,20);
	//	m_pStormtrooper5->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper5->setPosition(150, 0, -150);
	//	m_pStormtrooper5->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_walk");

	//	m_pStormtrooper6 = new GameObject(	"stormtrooper6",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper6->scale(20,20,20);
	//	m_pStormtrooper6->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper6->setPosition(0,0, -150);
	//	m_pStormtrooper6->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_idle1");

	//	m_pStormtrooper7 = new GameObject(	"stormtrooper7",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper7->scale(20,20,20);
	//	m_pStormtrooper7->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper7->setPosition(-150, 0, -150);
	//	m_pStormtrooper7->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_walk");

	//	m_pStormtrooper8 = new GameObject(	"stormtrooper8",
	//								"stormtrooper.mesh",
	//								"stormtrooper.hkx",
	//								ANIMATED_CHARACTER_RIGIDBODY);
	//	m_pStormtrooper8->scale(20,20,20);
	//	m_pStormtrooper8->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
	//	m_pStormtrooper8->setPosition(-150, 0, 0);
	//	m_pStormtrooper8->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_walk");

		m_pStormtrooper9 = new GameObject("stormtrooper9",
									"stormtrooper_withgun_bound_LOD.mesh",
									"storm_trooper_ragdoll_complete.hkx",
									ANIMATED_CHARACTER_RIGIDBODY);
		static_cast<AnimationManager::AnimatedHavokEntity*>(m_pStormtrooper9->m_pAnimatedEntity)->AddRagDoll("Ragdoll_storm_trooper_ragdoll_complete.hkx");
		m_pStormtrooper9->scale(20,20,20);
		//m_pStormtrooper9->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
		m_pStormtrooper9->setPosition(0, 0, 0);
		m_pStormtrooper9->m_pAnimatedEntity->SetDefaultAnimation("stormtrooper_idle");

		/// Doesnot work fine because the havok rig and ogre skeleton doesnot match
		//m_pHavokGirl = new GameObject("havokgirl",
		//							"havokGirl3.mesh",
		//							"HavokGirl.hkx",
		//							ANIMATED_CHARACTER_RIGIDBODY);
		//m_pHavokGirl->scale(20,20,20);
		//m_pHavokGirl->setPosition(0, 200, 0);
		//((CharacterRigidBodyObject*)m_pHavokGirl->m_pPhysicsObject)->setInputs(0,0,0,135);
		//m_pHavokGirl->m_pGraphicsObject->m_pOgreEntity->setCastShadows(false);
		//m_pHavokGirl->m_pAnimatedEntity->SetDefaultAnimation("HavokGirl_RunLoop");

		m_pGround = new GameObject("s_ground","the_ground.mesh","the_ground.hkx");
		m_pGround->setPosition(0,-1,0);

		
#endif	
		
		//////////////////////////////////////
		//		Ogre Initialization			//
		//////////////////////////////////////
		
		ogre1pos = Ogre::Vector3(7,292,300);
		ogre2pos = Ogre::Vector3(30,264,320);
		ogre3pos = Ogre::Vector3(-20,322,280);
		ogre4pos = Ogre::Vector3(-5,312,240);
		
		// Ogre 1 : check point 1 at (0,0,-130) ; check point 2 at (80,0,0) ; check point 3 at (-40,0,50) ; check point 4 at (-100,0,0) ;
		// Ogre 2 : check point 1 at (0,0,-130); check point 2 at (80,0,0) ; check point 3 at (-40,0,50) ; check point 4 at (-100,0,0) ;
		// Ogre 3 : check point 1 at (0,0,-130); check point 2 at (80,0,0) ; check point 3 at (-40,0,50) ; check point 4 at (-100,0,0) ;
		// Ogre 4 : check point 1 at (0,0,-130) ; check point 2 at (55,0,-50) ; check point 3 at (-40,0,30) ; check point 4 at (-60,0,-50) ;

#ifdef HAVOK
		m_pOgreAnimation1 = new GameObject( "ogreAnim1",
											"robot.mesh",
											"",
											ANIMATED_CHARACTER_PROXY);
		
		
		m_pOgreAnimation1->m_pGraphicsObject->setPosition(ogre1pos.x + 80, ogre1pos.y, ogre1pos.z);
		m_pOgreAnimation1->scale(0.55f,0.55f,0.55f);
		((CharacterProxyObject*)m_pOgreAnimation1->m_pPhysicsObject)->setInputs(0,0,0,1.5);
		
		m_pOgreAnimation2 = new GameObject( "ogreAnim2",
											"ninja.mesh",
											"",
											ANIMATED_CHARACTER_PROXY);
		
		m_pOgreAnimation2->m_pGraphicsObject->setPosition(ogre2pos.x - 40, ogre2pos.y, ogre2pos.z + 50);
		m_pOgreAnimation2->scale(0.22f,0.22f,0.22f);
		//((CharacterProxyObject*)m_pOgreAnimation2->m_pPhysicsObject)->setInputs(0,0,0,1);
		
		m_pOgreAnimation3 = new GameObject( "ogreAnim3",
											"Sinbad.mesh",
											"",
											ANIMATED_CHARACTER_PROXY);
		
		
		m_pOgreAnimation3->m_pGraphicsObject->setPosition(ogre3pos.x, ogre3pos.y, ogre3pos.z - 130);
		m_pOgreAnimation3->scale(4.8f,4.8f,4.8f);
		((CharacterProxyObject*)m_pOgreAnimation3->m_pPhysicsObject)->setInputs(0,0,0,3);
		
		m_pOgreAnimation4 = new GameObject( "ogreAnim4",
											"orc_archer.mesh",
											"",
											ANIMATED_CHARACTER_PROXY);
		
		m_pOgreAnimation4->m_pGraphicsObject->setPosition(ogre4pos.x - 60, ogre4pos.y, ogre4pos.z - 50);
		//m_pOgreAnimation4->m_pGraphicsObject->m_pOgreSceneNode->showBoundingBox(true);
		m_pOgreAnimation4->scale(1.53f,1.53f,1.53f);
		((CharacterProxyObject*)m_pOgreAnimation4->m_pPhysicsObject)->setInputs(0,0,0,3);
		m_pOgreAnimation4->m_pGraphicsObject->m_pOgreEntity->setCastShadows(false);
		

		m_pGround2 = new GameObject("s_ground2","the_ground.mesh","the_ground.hkx");
		m_pGround2->setPosition(0,300,300);
		

		m_pGround2->m_pGraphicsObject->m_pOgreEntity->setMaterialName("the_ground2/lambert1");
#endif
		m_v3CameraPosition = cameraLocations.at(0);

		std::string havokTextString = "HAVOK ANIMATIONS:\nTab -> Switch to Ogre Animation\n1 - >Play Animation No Transition\n2 -> Play Animation with Transition\n3 -> Play Overlay Animation No Transition\n4 -> Play Overlay Animation with Transistion\nR -> Toggle Ragdoll\n0 -> Stop Everything\nESC -> Quit";
		UtilitiesPtr->Add("HavokText", new GamePipe::DebugText(havokTextString, Ogre::Real(0.55f), Ogre::Real(0.1f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
				
		return true;
	}

	/*! Called when the screen is being destroyed
	*	@return	True if everything went well
	*/
	bool AnimationDemo::Destroy()
	{
		return true;
	}

	/*! Show the screen
	*	@return	True if everything went well
	*/
	bool AnimationDemo::Show()
	{
		//--------------------------------------------------------
		// Initialize Camera
		//--------------------------------------------------------
		Ogre::Camera* pCamera = GetActiveCamera();

		//--------------------------------------------------------
		// Initialize Main Viewport
		//--------------------------------------------------------
		mainViewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		mainViewport->setBackgroundColour(GetBackgroundColor());
		mainViewport->setOverlaysEnabled(true);
		SetViewport(mainViewport);
		pCamera->setAspectRatio(static_cast<Ogre::Real>(mainViewport->getActualWidth()) / static_cast<Ogre::Real>(mainViewport->getActualHeight()));
		
		//--------------------------------------------------------
		// Update Camera Position
		//--------------------------------------------------------
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
		return true;
	}

	/* 
	*	@return	True if everything went well
	*/
	bool AnimationDemo::Update()
	{
		if(screenIndex == 0)
		{
			//////////////////////////////////////
			// Update when havok demo is active	//
			//////////////////////////////////////


			FreeCameraMovement();

			//defaultAnim->SetText(m_pStormtrooper2->m_pAnimatedEntity->GetActiveAnimation());

			//--------------------------------------------------------
			// Add Debug Text (camera position and rotation)
			//--------------------------------------------------------
			if (DEBUG_MODE_ENABLED) {

				// initialize the string
				Ogre::String text = Ogre::String();
				text.append("Camera Position (x, y, z): ");

				// convert position and rotation to strings
				text.append(RealToString(m_v3CameraPosition.x));
				text.append(", ");
				text.append(RealToString(m_v3CameraPosition.y));
				text.append(", ");
				text.append(RealToString(m_v3CameraPosition.z));
				text.append("\nCamera Rotation (r, y, p): ");
				text.append(RealToString(m_qCameraOrientation.getRoll().valueDegrees()));
				text.append(", ");
				text.append(RealToString(m_qCameraOrientation.getYaw().valueDegrees()));
				text.append(", ");
				text.append(RealToString(m_qCameraOrientation.getPitch().valueDegrees()));

				// set the text
				debugText->SetText(text);
			}

			std::string animationsPlaying = "Animations Playing:";
			animationsPlaying = animationsPlaying.append("\nStormTrooper1: ");
			animationsPlaying = animationsPlaying.append(m_pStormtrooper1->m_pAnimatedEntity->GetActiveAnimation());
			animationsPlaying = animationsPlaying.append("\nStormTrooper2: ");
			animationsPlaying = animationsPlaying.append(m_pStormtrooper2->m_pAnimatedEntity->GetActiveAnimation());
			animationsPlaying = animationsPlaying.append("\nStormTrooper3: ");
			animationsPlaying = animationsPlaying.append(m_pStormtrooper3->m_pAnimatedEntity->GetActiveAnimation());
			animationsPlaying = animationsPlaying.append("\nStormTrooper4: ");
			animationsPlaying = animationsPlaying.append(m_pStormtrooper4->m_pAnimatedEntity->GetActiveAnimation());
			//animationsPlaying = animationsPlaying.append("\nHavokGirl: ");
			//animationsPlaying = animationsPlaying.append(m_pHavokGirl->m_pAnimatedEntity->GetActiveAnimation());
			animPlaying->SetText(animationsPlaying);

			std::string defaultAnimations = "Default Animations:";
			defaultAnimations = defaultAnimations.append("\nStormTrooper1: ");
			defaultAnimations = defaultAnimations.append(m_pStormtrooper1->m_pAnimatedEntity->GetDefaultAnimation());
			defaultAnimations = defaultAnimations.append("\nStormTrooper2: ");
			defaultAnimations = defaultAnimations.append(m_pStormtrooper2->m_pAnimatedEntity->GetDefaultAnimation());
			defaultAnimations = defaultAnimations.append("\nStormTrooper3: ");
			defaultAnimations = defaultAnimations.append(m_pStormtrooper3->m_pAnimatedEntity->GetDefaultAnimation());
			defaultAnimations = defaultAnimations.append("\nStormTrooper4: ");
			defaultAnimations = defaultAnimations.append(m_pStormtrooper4->m_pAnimatedEntity->GetDefaultAnimation());
			//defaultAnimations = defaultAnimations.append("\nHavokGirl: ");
			//defaultAnimations = defaultAnimations.append(m_pHavokGirl->m_pAnimatedEntity->GetDefaultAnimation());
			defaultAnim->SetText(defaultAnimations);

			if (RAGDOLL_ENABLED)
			{
				ragdollState->SetText("StormTrooper2: Ragdoll Enabled");
			}
			else
			{
				ragdollState->SetText("StormTrooper2: Ragdoll Disabled");
			}

		}
		else
		{
			//////////////////////////////////////
			// Update when ogre demo is active	//
			//////////////////////////////////////
		
			if(m_pOgreAnimation3->m_pAnimatedEntity->IsAnimationPlaying("JumpLoop"))
			{
#ifdef HAVOK
				((CharacterProxyObject*)m_pOgreAnimation3->m_pPhysicsObject)->setInputs(0,0,true,0);
#endif
			}

			//changeOgrePosition();
		}
		return true;
	}

	/*! Draw the scene
	*	@return	True if everything went well
	*/
	bool AnimationDemo::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		
		// update the camera position and rotation
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	bool AnimationDemo::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
				
		switch(keyEvent.key)
		{
			//-------------------------------------
			// Quit
			//-------------------------------------
			case GIS::KC_ESCAPE:
				{
					if(screenIndex == 0)
					{
						// remove Havok text as current Viewport is Havok
						UtilitiesPtr->Remove("HavokText");
						UtilitiesPtr->Remove("DebugText");
						UtilitiesPtr->Remove("AnimationPlaying");
						UtilitiesPtr->Remove("DefaultAnimations");
						UtilitiesPtr->Remove("RagdollState");
					}
					else
					{
						//remove Ogre text as current Viewport is Ogre
						UtilitiesPtr->Remove("OgreText1");
						UtilitiesPtr->Remove("OgreText2");
						UtilitiesPtr->Remove("OgreControl");
						UtilitiesPtr->Remove("OgreTips");
					}

					//Add main menu text again NOTE: every time menu changes in mainMenuScreen.cpp, we need to update it here
					const std::string menuText = "";//1: Audio\n2: Camera\n3: Collada\n4: GIS\n5: Havok\n6: Multiplayer Online\n7: FlashGui 1\n8: FlashGui 2\n8: Video\n0: FlashGui 3\nL: Lighting\n9: Animation";        
					UtilitiesPtr->Add("MainMenuScreenText", new GamePipe::DebugText(menuText, Ogre::Real(0.05f), Ogre::Real(0.1f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
					Close();
					break;
				}
			//-------------------------------------
			// Stormtrooper 2 toggles ragdoll pose
			// play once or loop
			//-------------------------------------
			case GIS::KC_0:
				{
					if (screenIndex == 0)
					{
						//m_pStormtrooper2->m_pAnimatedEntity->PlayAnimation("stormtrooper_dance", "", true, 0.5f);
						m_pStormtrooper1->m_pAnimatedEntity->StopAllAnimation();
						m_pStormtrooper2->m_pAnimatedEntity->StopAllAnimation();
						m_pStormtrooper3->m_pAnimatedEntity->StopAllAnimation();
						m_pStormtrooper4->m_pAnimatedEntity->StopAllAnimation();
						m_pStormtrooper9->m_pAnimatedEntity->StopAllAnimation();
					}
					break;
				}
			case GIS::KC_1:
				{
					if (screenIndex == 0)
					{
						m_pStormtrooper1->m_pAnimatedEntity->PlayAnimation("stormtrooper_dance", "", true, 0.0f);
					}
					else
					{

						//---------------------------------------------------------------
						// simple play animation for ogre and direct switch between them
						//---------------------------------------------------------------
						
						Ogre::String currentAnimName = m_pOgreAnimation1->m_pAnimatedEntity->GetAnimationNames().at(positionIndex%5);
						m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation(currentAnimName.c_str());
						
						currentAnimName = m_pOgreAnimation2->m_pAnimatedEntity->GetAnimationNames().at(positionIndex%20);
						m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation(currentAnimName.c_str());
						
						currentAnimName = m_pOgreAnimation3->m_pAnimatedEntity->GetAnimationNames().at(positionIndex%13);
						m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation(currentAnimName.c_str());
						
						currentAnimName = m_pOgreAnimation4->m_pAnimatedEntity->GetAnimationNames().at(positionIndex%5);
						m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation(currentAnimName.c_str());
						positionIndex++;
					}
					break;
				}

			case GIS::KC_2:
				{

					if (screenIndex == 0) {
						m_pStormtrooper2->m_pAnimatedEntity->PlayAnimation("stormtrooper_shoot_n_die", "stormtrooper_spot_n_shoot", true, 0.0f);
					} else
					if (screenIndex == 1)
					{

						//---------------------------------------------------------------
						// simple play animation for ogre and blending between them
						//---------------------------------------------------------------
						
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle3");

							// we can also write as follows, all of them are same
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("Dance","",true);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_walk","",true,0.0f);
							positionIndex++;
						}
						else if(positionIndex == 1)
						{
							//-------------------------------------------------------
							// Play ogre animation blending from previous animation
							//-------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Slump","",true,7.5f);
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Spin","",true,1.5f);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop","",true,7.5f);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_run","",true,7.5f);
							positionIndex++;	
						}
						else if(positionIndex == 2)
						{
							// stop all animations
							m_pOgreAnimation1->m_pAnimatedEntity->StopAnimation();
							m_pOgreAnimation2->m_pAnimatedEntity->StopAnimation();
							m_pOgreAnimation3->m_pAnimatedEntity->StopAnimation();
							m_pOgreAnimation4->m_pAnimatedEntity->StopAnimation();
							positionIndex++;
						}
						else if(positionIndex == 3)
						{
							//-------------------------------------------------------
							// Play ogre animation blending from NO animation
							//-------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk","",true,7.5f);
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle3","",true,7.5f);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("Dance","",true,7.5f);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_walk","",true,7.5f);
							positionIndex = 1;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_3:
				{
					if (screenIndex == 0) {
						m_pStormtrooper3->m_pAnimatedEntity->PlayAnimationOverlay("stormtrooper_attitude_walk", "", true, 0.0f);
					} else
					if(screenIndex == 1)
					{
						
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle3");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_idle");
							positionIndex++;
						}
						else if(positionIndex < 5)
						{
							//------------------------------------------------------
							// Play ogre animation once then continue with previous
							//------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Shoot","",false);
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Spin","",false);

							// We can also write as follows, the bending duration by default is zero.
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("Dance","",false,0.0f);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_swat","",false,0.0f);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}
				
			case GIS::KC_4:
				{
					if (screenIndex == 0) {
						m_pStormtrooper4->m_pAnimatedEntity->PlayAnimationOverlay("stormtrooper_shoot_n_die", "stormtrooper_challenge_opp", true, 0.0f);
					} else

					if(screenIndex == 1)
					{
						
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle3");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_idle");
							positionIndex++;
						}
						else if(positionIndex < 5)
						{
							//---------------------------------------------------------------------------------------
							// Play ogre animation once blending from previous animation then continue with previous
							//---------------------------------------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Shoot","",false,7.5f);
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Spin","",false,1.5f);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("Dance","",false,7.5f);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_swat","",false,7.5f);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_5:
				{
					if(screenIndex == 1)
					{
						
						if(positionIndex == 0 || positionIndex == 2 ||positionIndex == 4)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Idle");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle1");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_idle");
							positionIndex++;
						}
						else if(positionIndex == 1 || positionIndex == 3 || positionIndex == 5)
						{
							//---------------------------------------------------------------------------------------
							// Play ogre animation once from previous animation then continue with third animation
							//---------------------------------------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk","Shoot",true,0.0f);

							// We can also write as follows, as blending duration is by default 0.0f
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle2","Attack2",true);

							// this animation works even if we don't write true. Third argument is by default true
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleBase","RunBase");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_run","orc_walk");
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_6:
				{
					if(screenIndex == 1)
					{
						
						if(positionIndex == 0 || positionIndex == 2 ||positionIndex == 4)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Idle");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle1");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_walk");
							positionIndex++;
						}
						else if(positionIndex == 1 || positionIndex == 3 || positionIndex == 5)
						{
							//---------------------------------------------------------------------------------------
							// BLEND ogre animation once from previous animation then continue with third animation
							//---------------------------------------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk","Shoot",true,7.5f);
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle2","HighJump",true,1.5f);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleBase","RunBase",true,7.5f);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_idle","orc_run",true,7.5f);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_7:
				{
					if(screenIndex == 1)
					{
						
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Idle");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle1");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_idle");
							positionIndex++;
						}
						else if(positionIndex < 5)
						{
							//--------------------------------------------------------------------
							// Play ogre transition animation once from previous animation then 
							// play next animation once then continue with original animation
							//--------------------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Die","Shoot",false,0.0f);

							// we can eliminate last agrument as blending duration is by default zero.
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("HighJump","Attack2",false);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("JumpEnd","JumpLoop",false);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_swat","orc_walk",false);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_8:
				{
					if(screenIndex == 1)
					{
						
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Idle");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Idle1");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("IdleTop");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_idle");
							positionIndex++;
						}
						else if(positionIndex < 5)
						{
							//--------------------------------------------------------------------
							// Blend ogre transition animation once from previous animation then 
							// play next animation once then continue with original animation
							//--------------------------------------------------------------------

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Die","Shoot",false,7.5f);

							// we can eliminate last agrument as blending duration is by default zero.
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("HighJump","Attack2",false,2.5f);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("Dance","IdleBase",false,3.5f);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_swat","orc_walk",false,7.5f);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_R:
				{
					if (screenIndex == 0)
					{
						if (RAGDOLL_ENABLED)
						{
							//static_cast<AnimationManager::AnimatedHavokEntity*>(m_pHavokGirl->m_pAnimatedEntity)->ToggleToRagDollPose(false);
							static_cast<AnimationManager::AnimatedHavokEntity*>(m_pStormtrooper9->m_pAnimatedEntity)->ToggleToRagDollPose(false);
							RAGDOLL_ENABLED = false;
						}
						else
						{
							//m_pHavokGirl->m_pAnimatedEntity->StopAnimation();
							//static_cast<AnimationManager::AnimatedHavokEntity*>(m_pHavokGirl->m_pAnimatedEntity)->ToggleToRagDollPose(false);
							m_pStormtrooper9->m_pAnimatedEntity->StopAnimation();
							static_cast<AnimationManager::AnimatedHavokEntity*>(m_pStormtrooper9->m_pAnimatedEntity)->ToggleToRagDollPose(true);
							RAGDOLL_ENABLED = true;
						}
					}
					break;
				}

			//-------------------------------------
			// Stormtrooper 3 transition between
			// two different animations
			//-------------------------------------
		/*	case GIS::KC_T:
				{
					if (screenIndex == 0)
					{
						if (TRANSITION)
						{
							m_pStormtrooper3->m_pAnimatedEntity->PlayAnimation("stormtrooper_walk_2steps", "", false, 0.5f);
							TRANSITION = true;
						}
						else
						{
							m_pStormtrooper3->m_pAnimatedEntity->PlayAnimation("stormtrooper_shoot_n_die", "stormtrooper_idle_to_shoot", false, 0.5f);
							TRANSITION = false;
						}
					}
					break;
				}*/

			//-------------------------------------------
			// Overlay function for both havok and ogre
			//-------------------------------------------
			case GIS::KC_O:
				{
					if (screenIndex == 0)
					{
						//-------------------------------------
						// StormTrooper4 overlay animation
						//-------------------------------------
	/*					if (OVERLAYTRANSITION)
						{
							m_pStormtrooper4->m_pAnimatedEntity->PlayAnimationOverlay("stormtrooper_guard_n_wait", "", false, 0.5f);
							OVERLAYTRANSITION = false;
						}
						else
						{
							m_pStormtrooper4->m_pAnimatedEntity->PlayAnimationOverlay("stormtrooper_spot_n_shoot", "stormtrooper_idle_to_shoot", false, 0.5f);
							OVERLAYTRANSITION = true;
						}*/
					}
					else
					{
						if(positionIndex == 0)
						{

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("RunBase");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_walk");
							positionIndex++;
						}
						else if(positionIndex == 1)
						{
							//-------------------------------------------------------------
							// play overlay with previous animation in loop
							//-------------------------------------------------------------
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimationOverlay("Shoot");

							// above function can be called in following different ways
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimationOverlay("Attack3","",true);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimationOverlay("RunTop","",true);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimationOverlay("orc_idle","",true);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_J:
				{
					if(screenIndex == 1)
					{
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("RunBase");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_run");
							positionIndex++;
						}
						else if(positionIndex == 1)
						{
							//-------------------------------------------------------------------
							// play overlay transition once with previous animation
							// then play next animation overlaid with the first animtion in loop
							//-------------------------------------------------------------------
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimationOverlay("Idle","Shoot");

							// above function can be called in following different ways
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimationOverlay("Attack3","Attack1",true);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimationOverlay("RunTop","SliceHorizontal",true);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimationOverlay("orc_idle","orc_walk",true);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_P:
				{
					if(screenIndex == 1)
					{
						if(positionIndex == 0)
						{

							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("RunBase");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_walk");
							positionIndex++;
						}
						else if(positionIndex == 1)
						{
							//-------------------------------------------------------------
							// play overlay once with previous animation
							// then play previous animation in loop
							//-------------------------------------------------------------
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimationOverlay("Shoot","",false);
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimationOverlay("HighJump","",false);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimationOverlay("JumpLoop","",false);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimationOverlay("orc_run","",false);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			case GIS::KC_K:
				{
					if(screenIndex == 1)
					{
						if(positionIndex == 0)
						{
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimation("Walk");
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimation("RunBase");
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimation("orc_run");
							positionIndex++;
						}
						else if(positionIndex == 1)
						{
							//-------------------------------------------------------------------
							// play overlay transition once with previous animation
							// then play next animation overlaid with the previous animtion once
							// then play previous animation in loop
							//-------------------------------------------------------------------
							m_pOgreAnimation1->m_pAnimatedEntity->PlayAnimationOverlay("Idle","Shoot",false);

							// above function can be called in following different ways
							m_pOgreAnimation2->m_pAnimatedEntity->PlayAnimationOverlay("HighJump","Attack1",false);
							m_pOgreAnimation3->m_pAnimatedEntity->PlayAnimationOverlay("IdleTop","SliceHorizontal",false);
							m_pOgreAnimation4->m_pAnimatedEntity->PlayAnimationOverlay("orc_idle","orc_walk",false);
							positionIndex++;
						}
						else
						{
							// stop all animations
							stopAllAnimations();
						}
					}
					break;
				}

			//-------------------------------------
			// Stormtrooper1 scrolls through
			// playing different animations
			//-------------------------------------
			case GIS::KC_UP:
				{
					if (screenIndex == 0)
					{	
						m_pStormtrooper1->m_pAnimatedEntity->PlayAnimation(allStormTrooperAnimations.at(++animationIndex % 14).c_str());
					}
					break;
				}

			//-------------------------------------
			// Toggle through camera positions
			// to look at each characgter
			//-------------------------------------
			case GIS::KC_RIGHT:
				{
					if(screenIndex == 0)
					{
						++positionIndex;
					}
					else
					{
						ogrePositionIndex++;
						ogrePositionIndex %= 4;

						movementDirection = 0;	
						changeOgrePosition();	
					}
					break;
				}

			case GIS::KC_LEFT:
				{
					if(screenIndex == 0)
					{
						// for havok
					}
					else
					{
						ogrePositionIndex--;
						ogrePositionIndex += 4;

						ogrePositionIndex %= 4;
					
						movementDirection = 1;
						changeOgrePosition();
					}
					break;
				}

			

			//-----------------------------------------------
			//	Stop all animations
			//-----------------------------------------------
			case GIS::KC_S:
				{
					if(screenIndex == 1)
					{
						stopAllAnimations();
					}
					break;
				}

			//----------------------------------------------------
			// Toggle between ogre animation and havok animation
			//----------------------------------------------------
			case GIS::KC_TAB:
				{
					//Change the viewport to view 
					if(screenIndex)
					{
						screenIndex = 0;

						//remove ogre text from screen
						UtilitiesPtr->Remove("OgreText1");
						UtilitiesPtr->Remove("OgreText2");
						UtilitiesPtr->Remove("OgreControl");
						UtilitiesPtr->Remove("OgreTips");

						//set havok text on screen
						//std::string havokTextString = "HAVOK ANIMATIONS:\nTab -> Switch to Ogre Animation\nUpArrow ->Play StormTrooper1's Animations Sequentially\nR -> StormTrooper2: Toggle RagDoll\n	0: Loop 1: Once\nT -> StormTrooper3: TransitionAnimation\nO -> StormTrooper4: OverlayAnimation\n	Toggle between with and without Transition\nESC -> Quit";
						std::string havokTextString = "HAVOK ANIMATIONS:\nTab -> Switch to Ogre Animation\n1 - >Play Animation No Transition\n2 -> Play Animation with Transition\n3 -> Play Overlay Animation No Transition\n4 -> Play Overlay Animation with Transistion\nR -> Toggle Ragdoll\n0 -> Stop Everything\nESC -> Quit";
						UtilitiesPtr->Add("HavokText", new GamePipe::DebugText(havokTextString, Ogre::Real(0.55f), Ogre::Real(0.1f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
						
						debugText = new GamePipe::DebugText("", Ogre::Real(0.005f), Ogre::Real(0.005f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
						UtilitiesPtr->Add("DebugText", debugText);
						
						animPlaying = new GamePipe::DebugText("Animations are Playing: ", Ogre::Real(0.005f), Ogre::Real(0.035f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
						UtilitiesPtr->Add("AnimationPlaying", animPlaying);
						
						defaultAnim = new GamePipe::DebugText("Default Animations: ", Ogre::Real(0.005f), Ogre::Real(0.155f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
						UtilitiesPtr->Add("DefaultAnimations", defaultAnim);
						
						ragdollState = new GamePipe::DebugText("StormTrooper1: RagdollPose: ", Ogre::Real(0.005f), Ogre::Real(0.305f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left);
						UtilitiesPtr->Add("RagdollState", ragdollState);

						//set havok camera on viewport
						mainViewport->setCamera(havokCamera);
					
					}
					else
					{
						screenIndex = 1;

						// remove Havok text from the screen
						UtilitiesPtr->Remove("HavokText");
						UtilitiesPtr->Remove("DebugText");
						UtilitiesPtr->Remove("AnimationPlaying");
						UtilitiesPtr->Remove("DefaultAnimations");
						UtilitiesPtr->Remove("RagdollState");


						// set ogre text on the  screen
						std::string ogreControl = "OGRE ANIMATIONS:\n\nTAB > Switch back to Havok animation\nESC > Switch back to Main menu\n\nDemo Controls:\nRIGHT/LEFT > Change animated object at front";
						std::string ogreText1 = "Play Animation:\n1 > Switch animation and play in loop\n2 > Switch animation with blending\n3 > Play animation once stopping current animation\n4 > Play animation once with blending\n5 > Play transition animation once then play destination in loop\n6 > Blend transition animation from current animation then play destination in loop\n7 > Play transition animation once, then destination animation once\n8 > Blend transition animation from current animation, then play destination animation once";
						std::string ogreText2 = "Stop Animations:\nS > stop all animations\n\nPlay Animation Overlay:\nO > overlay destination animation on current animation in loop\nP > overlay destination animation on current once\nJ > overlay transition animation with previous animation,\n    then overlay destination with previous in loop\nK > overlay transition with previous once,\n    then overlay destination with previous once";
						std::string ogreTips = "Tips:\n> Press 'S' to stop all animations before selecting any\n  option from play animation or play animation overlay.\n> Pressing any option key once, sets an animation on\n  which blending or overlay will be performed, So press\n  same option key again to see the blending or overlay.\n> Press RIGHT/LEFT to change the animation on front.\n> For most of the cases, GREEN OGRE shows best\n  results as it has clear animations.";
						UtilitiesPtr->Add("OgreControl", new GamePipe::DebugText(ogreControl, Ogre::Real(0.71f), Ogre::Real(0.04f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
						UtilitiesPtr->Add("OgreText1", new GamePipe::DebugText(ogreText1, Ogre::Real(0.01f), Ogre::Real(0.01f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
						UtilitiesPtr->Add("OgreText2", new GamePipe::DebugText(ogreText2, Ogre::Real(0.01f), Ogre::Real(0.78f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
						UtilitiesPtr->Add("OgreTips", new GamePipe::DebugText(ogreTips, Ogre::Real(0.67f), Ogre::Real(0.8f), 2, Ogre::ColourValue(1, 1, 1, 0.75), Ogre::TextAreaOverlayElement::Left));
						//set ogre camera on viewport
						mainViewport->setCamera(ogreCamera);
					}	
					break;
				}

		}
		
		return true;
	}

	bool AnimationDemo::OnKeyRelease(const GIS::KeyEvent& keyEvent)
	{
				
		switch(keyEvent.key)
		{
		case GIS::KC_P:
			{
				break;
			}
		}	
		
		return true;
	}

	std::string AnimationDemo::RealToString(float number)
	{
		// use ostringstream to write float to std::string
		std::ostringstream buff;
		buff << number;     
		return buff.str();
	}

	void AnimationDemo::stopAllAnimations()
	{
		m_pOgreAnimation1->m_pAnimatedEntity->StopAnimation();
		//since no argument is passed it will stop all current playing animations

		m_pOgreAnimation2->m_pAnimatedEntity->StopAnimation();
		m_pOgreAnimation3->m_pAnimatedEntity->StopAnimation();
		m_pOgreAnimation4->m_pAnimatedEntity->StopAnimation();

		positionIndex = 0;
	}

	void AnimationDemo::changeOgrePosition()
	{
		float fVelocity = 10.0f;
		Vector3 anim1pos = m_pOgreAnimation1->getPosition();
		Vector3 anim2pos = m_pOgreAnimation2->m_pGraphicsObject->getPosition();
		Vector3 anim3pos = m_pOgreAnimation3->m_pGraphicsObject->getPosition();
		Vector3 anim4pos = m_pOgreAnimation4->m_pGraphicsObject->getPosition();

		if(ogrePositionIndex == 0)
		{
			m_pOgreAnimation1->setPosition(ogre1pos.x - 18, ogre1pos.y + 22, ogre1pos.z - 155);
			m_pOgreAnimation2->setPosition(ogre2pos.x + 43, ogre2pos.y + 47.5f, ogre2pos.z - 45);
			m_pOgreAnimation3->setPosition(ogre3pos.x - 92, ogre3pos.y, ogre3pos.z - 13);
			m_pOgreAnimation4->setPosition(ogre4pos.x - 30, ogre4pos.y - 11.9f, ogre4pos.z + 30);	
		}
		else if(ogrePositionIndex == 1)
		{
			m_pOgreAnimation1->setPosition(ogre1pos.x - 106, ogre1pos.y + 22, ogre1pos.z - 36);
			m_pOgreAnimation2->setPosition(ogre2pos.x - 38, ogre2pos.y + 47.5f, ogre2pos.z - 175);
			m_pOgreAnimation3->setPosition(ogre3pos.x - 20, ogre3pos.y, ogre3pos.z + 90);
			m_pOgreAnimation4->setPosition(ogre4pos.x + 47, ogre4pos.y - 11.9f, ogre4pos.z - 50);
		}
		else if(ogrePositionIndex == 2)
		{
			m_pOgreAnimation1->setPosition(ogre1pos.x - 40, ogre1pos.y + 22, ogre1pos.z + 90);
			m_pOgreAnimation2->setPosition(ogre2pos.x - 126, ogre2pos.y + 47.5f, ogre2pos.z - 56);
			m_pOgreAnimation3->setPosition(ogre3pos.x + 75, ogre3pos.y, ogre3pos.z);
			m_pOgreAnimation4->setPosition(ogre4pos.x - 9, ogre4pos.y - 11.9f, ogre4pos.z - 145);	
		}
		else if(ogrePositionIndex == 3)
		{
			m_pOgreAnimation1->setPosition(ogre1pos.x + 62, ogre1pos.y + 22, ogre1pos.z - 23);
			m_pOgreAnimation2->setPosition(ogre2pos.x - 60, ogre2pos.y + 47.5f, ogre2pos.z + 50);
			m_pOgreAnimation3->setPosition(ogre3pos.x - 4, ogre3pos.y, ogre3pos.z - 132);
			m_pOgreAnimation4->setPosition(ogre4pos.x - 70, ogre4pos.y - 11.9f, ogre4pos.z - 60);	
		}
	}
}