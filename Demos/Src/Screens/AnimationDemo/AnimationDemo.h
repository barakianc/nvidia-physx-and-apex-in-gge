#pragma once

// GameScreen Header
#include "GameScreen.h"

// Animation Entity Header
#include "AnimationManager.h"

namespace GamePipeGame
{
	
	class AnimationDemo : public GamePipe::GameScreen
	{
	private:
		std::vector<Ogre::Vector3> cameraLocations;

		std::vector<std::string> allStormTrooperAnimations;

		int animationIndex;

		bool RAGDOLL_ENABLED;
		bool TRANSITION;
		bool OVERLAY;
		bool OVERLAYTRANSITION;
		int positionIndex;
		int screenIndex;
		int ogrePositionIndex;
		int movementDirection;

		Ogre::Viewport* mainViewport;
		
		Ogre::Camera* ogreCamera;
		Ogre::Camera* havokCamera;

		GameObject* m_pStormtrooper1;
		GameObject* m_pStormtrooper2;
		GameObject* m_pStormtrooper3;
		GameObject* m_pStormtrooper4;
		GameObject* m_pStormtrooper5;
		GameObject* m_pStormtrooper6;
		GameObject* m_pStormtrooper7;
		GameObject* m_pStormtrooper8;
		GameObject* m_pStormtrooper9;
		GameObject* m_pHavokGirl;

		GameObject* m_pOgreAnimation1;
		GameObject* m_pOgreAnimation2;
		GameObject* m_pOgreAnimation3;
		GameObject* m_pOgreAnimation4;
		
		GameObject* m_pGround;
		GameObject* m_pGround2;

		//-------------------------------------
		// Debug Mode
		//-------------------------------------
		bool DEBUG_MODE_ENABLED;
		GamePipe::DebugText* debugText;
		Ogre::SceneNode* debugGunNode;
		Ogre::Entity* debugGun;

		GamePipe::DebugText* animPlaying;
		GamePipe::DebugText* defaultAnim;
		GamePipe::DebugText* ragdollState;

		//-------------------------------------
		// Utilities
		//-------------------------------------
		std::string RealToString(Ogre::Real);
		void changeOgrePosition();
		void stopAllAnimations();

		//-------------------------------------
		// Position vectors
		//-------------------------------------
		Ogre::Vector3 ogre1pos;
		Ogre::Vector3 ogre2pos;
		Ogre::Vector3 ogre3pos;
		Ogre::Vector3 ogre4pos;
		
	public:

		AnimationDemo(std::string name);
		~AnimationDemo() {}

		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();
		
		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent& keyEvent);
		bool OnKeyRelease(const GIS::KeyEvent &keyEvent);
	};
}