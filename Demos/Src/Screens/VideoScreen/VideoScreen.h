#pragma once

/*! Includes */
#include "GameScreen.h"
//#include "Utilities.h"


/*! For Video Texture */
#include "wmvideotexture.h"
#include "OgreExternalTextureSourceManager.h"

namespace GamePipeGame
{
	
	class VideoScreen : public GamePipe::GameScreen
	{
	private:
		
	public:
		Ogre::CWMVideoTexture	*mVTexture;
		
		bool VIDEO_PLAYING;
		bool VIDEO_FULLSCREEN;
		bool VIDEO_MUTE;
		Ogre::Rectangle2D* rect;

		VideoScreen(std::string name);
		~VideoScreen() {}

		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();
		bool OnKeyPress(const GIS::KeyEvent& keyEvent);
		bool Update();
		bool Draw();
	};
}