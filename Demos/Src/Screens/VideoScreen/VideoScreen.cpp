#include "StdAfx.h"
// Includes
#include "VideoScreen.h"
#include "Main.h"


namespace GamePipeGame
{

	/*! Constructor
	*	@param[in]	name	Name of the screen
	*/
	VideoScreen::VideoScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	/*! Load the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool VideoScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		return true;
	}

	/*! Unload the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool VideoScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}

	/*! Initializes the Model Viewer
	*	@return	True if everything went well
	*/
	bool VideoScreen::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());

		SetBackgroundColor(Ogre::ColourValue(0.2f, 0.2f, 0.7f, 1.0f));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f, 0.5f));

		//Initializing Video on Cube
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pSceneManager->createEntity("Cube", "cube.mesh"));
		pSceneManager->getEntity("Cube")->getParentSceneNode()->setPosition(0,10,-200);
		pSceneManager->getEntity("Cube")->getParentSceneNode()->scale(Ogre::Vector3(2,2,2));
		pSceneManager->getEntity("Cube")->setMaterialName("VideoTexture");
		
		//Intializing Video for Full Screen
		rect = new Rectangle2D(true);
		rect->setCorners(-1.0, 1.0, 1.0, -1.0);
		rect->setMaterial("VideoTexture");
		rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);
		AxisAlignedBox aabInf;
		aabInf.setInfinite();
		rect->setBoundingBox(aabInf);
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(rect);
		
		//Video Plays on Cube initially
		rect->setVisible(false);
		VIDEO_FULLSCREEN = false;

		//Setting Video in Pause mode
		mVTexture = (Ogre::CWMVideoTexture *)Ogre::ExternalTextureSourceManager::getSingleton().getExternalTextureSource("wmvideo");
		mVTexture->setPlayMode(TextureEffectPlay_Looping);
		VIDEO_PLAYING = false;
		VIDEO_MUTE = false;

		UtilitiesPtr->Add("Commands", new GamePipe::DebugText("P->Video Play/Pause\nF->Toggle FullScreen\nR->Restart Video\nESC->Quit", (Ogre::Real)0.005, (Ogre::Real)0.85, (Ogre::Real)2.5, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
	
		return true;
	}
	
	/*! Called when the screen is being destroyed
	*	@return	True if everything went well
	*/
	bool VideoScreen::Destroy()
	{
		return true;
	}

	/*! Show the screen
	*	@return	True if everything went well
	*/
	bool VideoScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	/* 
	*	@return	True if everything went well
	*/
	bool VideoScreen::Update()
	{
		FreeCameraMovement();
		return true;
	}

	/*! Draw the scene
	*	@return	True if everything went well
	*/
	bool VideoScreen::Draw()
	{
		GetDefaultSceneManager()->getEntity("Cube")->getParentSceneNode()->yaw(Ogre::Degree(50 * EnginePtr->GetDeltaTime()));

		return true;
	}

	bool VideoScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
				
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				UtilitiesPtr->Remove("Commands");
				mVTexture->stop("VideoTexture");
				Close();
				break;
			}
		case GIS::KC_P:
			{
				if(VIDEO_PLAYING)
				{
					VIDEO_PLAYING = !VIDEO_PLAYING;
					mVTexture->pause("VideoTexture");
				}
				else
				{
					VIDEO_PLAYING = !VIDEO_PLAYING;
					mVTexture->start("VideoTexture");
				}
				break;
			}
		case GIS::KC_R:
			{
				mVTexture->restart("VideoTexture");
				mVTexture->start("VideoTexture");
				VIDEO_PLAYING = true;
				break;
			}
			
		case GIS::KC_F:
			{
				VIDEO_FULLSCREEN = !VIDEO_FULLSCREEN;
				rect->setVisible(!rect->getVisible());
				GetDefaultSceneManager()->getEntity("Cube")->setVisible(!GetDefaultSceneManager()->getEntity("Cube")->getVisible());
				break;
			}
		}	
		
		return true;
	}

}