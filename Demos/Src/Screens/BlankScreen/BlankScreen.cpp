#include "StdAfx.h"
// Includes
#include "BlankScreen.h"



#include "Main.h"

namespace GamePipeGame
{

	/*! Constructor
	*	@param[in]	name	Name of the screen
	*/
	BlankScreen::BlankScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(false);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	/*! Load the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("ParticleUniverse");
		return true;
	}

	/*! Unload the resources used by the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("ParticleUniverse");
		return true;
	}

	/*! Initializes the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());	

		SetBackgroundColor(Ogre::ColourValue(0.5f, 0.5f, 0.5f, 1.0f));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f, 0.5f));

		// Create default grid
		CreateGrid();

// To play without GLE
#ifdef GLE_EDITOR
		// Initialize cameras
		EnginePtr->GetEditor()->InitEditorCameras(pSceneManager, true);
		SetActiveCamera(GetDefaultSceneManager()->getCamera("Editor_CameraPerspective"));
		EnginePtr->GetEditor()->SetEditorMode(true);
#endif

		return true;
	}
	/*! Create a basic grid for the model viewer
	*/
	void BlankScreen::CreateGrid()
	{
		int iCount				= 0 ;
		int iGridMaxX			= 50;
		int iGridMaxZ			= 50;
		int iGridSeparation		= 1;

		// Create the grid
		Ogre::ManualObject * mGrid = GetDefaultSceneManager()->createManualObject("Editor_Grid");
		mGrid->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );
		for( int i = -iGridMaxZ ; i <= iGridMaxZ ; i += iGridSeparation)
		{
			mGrid->position((Ogre::Real)(-iGridMaxX), (Ogre::Real)0.0f, (Ogre::Real)i);
			mGrid->colour((Ogre::Real)0.7f, (Ogre::Real)0.7f, (Ogre::Real)0.7f);
			if( i == 0 )	mGrid->colour(1.0f,1.0f,1.0f);
			mGrid->position((Ogre::Real)iGridMaxX,	(Ogre::Real)0.0f, (Ogre::Real)i);
			iCount++;
		}
		iCount = 0;
		for( int i = -iGridMaxX ; i <= iGridMaxX ; i += iGridSeparation)
		{
			mGrid->position(Ogre::Real(i),	0.0f, -Ogre::Real(iGridMaxZ)	);
			mGrid->colour((Ogre::Real)0.7f, (Ogre::Real)0.7f, (Ogre::Real)0.7f);
			if( i == 0 )	mGrid->colour((Ogre::Real)1.0f, (Ogre::Real)1.0f, (Ogre::Real)1.0f);
			mGrid->position((Ogre::Real)i,  (Ogre::Real)0.0f, (Ogre::Real)iGridMaxZ);
			iCount++;
		}
		mGrid->end(); 
		// Add it to a new scene node
		GetDefaultSceneManager()->getRootSceneNode()->createChildSceneNode("Editor_GridSceneNode")->attachObject(mGrid);					
	}

	/*! Create the axes for the model viewer (disabled)
	*/
	void BlankScreen::CreateAxes()
	{
		// Create the Axes
		Ogre::Entity* mAxes = GetDefaultSceneManager()->createEntity("axes","axes.mesh");
		// if there has been a problem we don't want to continue
		if(mAxes==NULL) return;
		mAxes->setRenderQueueGroup( Ogre::RENDER_QUEUE_OVERLAY ); 
		Ogre::SceneNode * p = GetDefaultSceneManager()->createSceneNode("axesscenenode");
		p->attachObject( mAxes );
		p->translate((Ogre::Real) -2.5f, (Ogre::Real)-2.5f, (Ogre::Real)-8.0f );
		p->setScale((Ogre::Real)0.05, (Ogre::Real)0.05, (Ogre::Real)0.05);
		Ogre::Overlay *  m_pOverlay  = Ogre::OverlayManager::getSingleton( ).create( String("AxesOverlay") );
		m_pOverlay->setZOrder(540);   
		m_pOverlay->add3D( p );
		m_pOverlay->show();
	}

	/*! Called when the screen is being destroyed
	*	@return	True if everything went well
	*/
	bool BlankScreen::Destroy()
	{
		return true;
	}

	/*! Show the screen
	*	@return	True if everything went well
	*/
	bool BlankScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	/*! Update the Model Viewer
	*	@return	True if everything went well
	*/
	bool BlankScreen::Update()
	{
		// Update the axes - Feature disabled
		//if(GetActiveCamera() == m_pCameraPerspective)
		//{
		//	if(GetDefaultSceneManager()->hasSceneNode("axesscenenode"))
		//		GetDefaultSceneManager()->getSceneNode("axesscenenode")->setOrientation( m_pCameraPerspective->getOrientation());
		//}
		return true;
	}

	/*! Draw the scene
	*	@return	True if everything went well
	*/
	bool BlankScreen::Draw()
	{
		return true;
	}
}