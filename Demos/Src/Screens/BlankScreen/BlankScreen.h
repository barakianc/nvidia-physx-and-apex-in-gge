#pragma once

/*! Includes */
#include "GameScreen.h"

namespace GamePipeGame
{
	/*! Screen used by the Model Viewer */
	class BlankScreen : public GamePipe::GameScreen
	{
	private:
		void					CreateAxes();
		void					CreateGrid();

	public:
		BlankScreen(std::string name);
		~BlankScreen() {}

		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();
	};
}