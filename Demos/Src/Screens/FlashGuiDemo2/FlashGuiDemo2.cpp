#include "StdAfx.h"
// FlashGuiDemo2
#include "FlashGuiDemo2.h"

namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// FlashGuiDemo2(std::string name)
	//////////////////////////////////////////////////////////////////////////
	FlashGuiDemo2::FlashGuiDemo2(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		//GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("NxOgre");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		//GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("NxOgre");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::Initialize()
	{
		//CreateDefaultSceneManager();
		//SetActiveCamera(CreateDefaultCamera());

	// init the scence stuff
		SetBackgroundColor(Ogre::ColourValue(0,1,1,1.0000));

		pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(1,1,1,1));	

		// Create Static Geometry: Floor; we are just adding a floor mesh where the floor is
		Ogre::StaticGeometry* pStaticGeometry;
		pStaticGeometry = pSceneManager->createStaticGeometry("Grid");
		pStaticGeometry->addEntity(pSceneManager->createEntity("nx.floor", "nx.floor2.mesh"), Ogre::Vector3(0, -20.05f, 0), Ogre::Quaternion(1, 0, 0, 0), Ogre::Vector3(20, 0, 20));
		pStaticGeometry->build();
		
	readyToExit = 0;
	 //init values
		nsStr = 5;
		nsInt = 5;
		nsCon = 5;
		nsCha = 5;
		nsWis = 5;
		nsDex = 5;
	
	// set gender
		gender = "Male";
	// set appearance
		strAppearance = "1";

	// set Height Width
		rHeight = 1.0f;
		rWidth = 1.0f;
		rZoom = 0.0f;
		
		difficulty = "Normal";
		gore = "Off";
	// init the nodes
		// lady first
		pWomanNode = pSceneManager->getRootSceneNode()->createChildSceneNode();
		pWomanEntity = pSceneManager->createEntity("Woman_gengeric", "woman_generic.mesh");
		pWomanEntity->setMaterialName("woman_generic"+strAppearance);
		pWomanNode->attachObject(pWomanEntity);
		pWomanNode->scale(45*rWidth,45*rHeight,45);
		pWomanNode->setPosition(-10,-20,0);
		//pWomanNode->setAutoTracking(true);
		pWomanNode->setVisible(false);
		// man
		pManNode = pSceneManager->getRootSceneNode()->createChildSceneNode();
		pManEntity = pSceneManager->createEntity("Man_gengeric", "man_generic.mesh");
		pManEntity->setMaterialName("man_generic"+strAppearance);
		pManNode->attachObject(pManEntity);
		pManNode->scale(25*rWidth,25*rHeight,25);
		pManNode->setPosition(-10,-20,0);
		//pManNode->setAutoTracking(true);
		pManNode->setVisible(false);

		pCurrentNode = pManNode;
		SetMesh();

		return true;
	}

	bool FlashGuiDemo2::SetMesh()
	{
		pCurrentNode->setVisible(false);
		if(gender == "Male")
		{

			pManEntity->setMaterialName("man_generic"+strAppearance);
			pManNode->resetToInitialState();
			pManNode->setPosition(-10*(1-rZoom/150),-20*(1-rZoom/300),0);
			pManNode->scale(25*rWidth,25*rHeight,25);

			pCurrentNode = pManNode;
		}
		else if(gender == "Female")
		{
			pWomanEntity->setMaterialName("woman_generic"+strAppearance);
			pWomanNode->resetToInitialState();
			pWomanNode->setPosition(-10*(1-rZoom/150),-20*(1-rZoom/300),0);
			pWomanNode->scale(45*rWidth,45*rHeight,45);
			
			pCurrentNode = pWomanNode;
		}

		pCurrentNode->setVisible(true);
		return true;
	}


	void FlashGuiDemo2::RemoveHUDElements()
	{
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_LeftPanel);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_RightPanel);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_ZoomSlider);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_RotateLeft);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_RotateRight);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_ZoomLabel);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_ViewToggle);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_Difficulty);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_GoreCheckBox);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_NameTextBox);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_NameLabel);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_CountDown);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_CountDownLabel);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_Reset);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_Finish);
		FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_Random);
		if (FlashGUIPtr->FlashGUIManager->getFlashControl("fc_infoPanel"))
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_infoPanel);
		}
		if (FlashGUIPtr->FlashGUIManager->getFlashControl("fc_infoPanelBtn"))
		{
			FlashGUIPtr->FlashGUIManager->destroyFlashControl(fc_infoPanelBtn);
		}
		

	}

	void FlashGuiDemo2::CreateHUDElements(Ogre::Viewport* viewport)
	{
		int screenW = viewport->getActualWidth();
		int screenH = viewport->getActualHeight();
		float controlWidth; //relative float value of the flash control's width as to the viewport's width
		float controlHeight; //relative float value of the flash control's height as to the viewport's height
		float posX;  //relative float value of the flash control's x position as to the viewport's width
		float posY; //relative float value of the flash control's y position as to the viewport's height

		//left panel
		controlWidth = 320/1024.0f;
		controlHeight = 600/768.0f;
		posX = 0.0f;
		posY = 0.05f;

		fc_LeftPanel = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_LeftPanel",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_LeftPanel->load("leftPanel.swf");
		fc_LeftPanel->setTransparent(true, true);
		fc_LeftPanel->bind("newStepperValue",FlashDelegate(this, &FlashGuiDemo2::newStepperValue));
		fc_LeftPanel->bind("genderSelected",FlashDelegate(this, &FlashGuiDemo2::genderSelected));
		fc_LeftPanel->bind("newSliderValue",FlashDelegate(this, &FlashGuiDemo2::newSliderValue));
		fc_LeftPanel->setDraggable(false);

		//Right panel
		controlWidth = 460/1024.0f;
		controlHeight = 600/768.0f;
		posX = 0.55f;
		posY = 0.05f;

		fc_RightPanel = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_RightPanel",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_RightPanel->load("rightPanel.swf");
		fc_RightPanel->setTransparent(true, true);
		fc_RightPanel->bind("classSelected",FlashDelegate(this, &FlashGuiDemo2::classSelected));
		fc_RightPanel->setDraggable(false);

		//Zoom slider
		controlWidth = 180/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.39f;
		posY = 0.80f;
		fc_ZoomSlider = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_ZoomSlider",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_ZoomSlider->load("slider2.swf");
		fc_ZoomSlider->setTransparent(true, true);
		fc_ZoomSlider->callFunction("initSlider",Args("slZoom"));
		fc_ZoomSlider->bind("newSliderValue",FlashDelegate(this, &FlashGuiDemo2::newSliderValue));
		fc_ZoomSlider->setDraggable(false);

		//Zoom label
		controlWidth = 300/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.30f;
		posY = 0.80f;
		fc_ZoomLabel = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_ZoomLabel",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_ZoomLabel->load("textLabel.swf");
		fc_ZoomLabel->setTransparent(true, true);
		fc_ZoomLabel->callFunction("setText",Args("Zoom:"));
		fc_ZoomLabel->setDraggable(false);

		//rotate left
		controlWidth = 50/1024.0f;
		controlHeight = 50/768.0f;
		posX = 0.36f;
		posY = 0.72f;
		fc_RotateLeft = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_RotateLeft",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_RotateLeft->load("squarebutton.swf");
		fc_RotateLeft->setTransparent(true, true);
		fc_RotateLeft->callFunction("initSquareBtn",Args("rotateL")("Textures\\RotateL.png"));
		fc_RotateLeft->bind("squareBtnPressed",FlashDelegate(this, &FlashGuiDemo2::squareButtonPressed));
		fc_RotateLeft->setDraggable(false);

		//rotate right
		controlWidth = 50/1024.0f;
		controlHeight = 50/768.0f;
		posX = 0.46f;
		posY = 0.72f;
		fc_RotateRight = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_RotateRight",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_RotateRight->load("squarebutton.swf");
		fc_RotateRight->setTransparent(true, true);
		fc_RotateRight->callFunction("initSquareBtn",Args("rotateR")("Textures\\RotateR.png"));
		fc_RotateRight->bind("squareBtnPressed",FlashDelegate(this, &FlashGuiDemo2::squareButtonPressed));
		fc_RotateRight->setDraggable(false);


		//view toggle
		controlWidth = 240/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.32f;
		posY = 0.09f;
		fc_ViewToggle = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_ViewToggle",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_ViewToggle->load("toggleMenu.swf");
		fc_ViewToggle->setTransparent(true, true);
		fc_ViewToggle->callFunction("setLabelName",Args("View:"));
		fc_ViewToggle->callFunction("addToggleBtn",Args("Body"));
		fc_ViewToggle->callFunction("addToggleBtn",Args("Head"));
		fc_ViewToggle->callFunction("addToggleBtn",Args("TopDown"));
		fc_ViewToggle->callFunction("setBtnLabelName",Args("Body"));
		fc_ViewToggle->bind("toggleBtnChanged",FlashDelegate(this, &FlashGuiDemo2::toggleBtnChanged));
		fc_ViewToggle->setDraggable(false);

		//difficulty dropdown
		controlWidth = 180/1024.0f;
		controlHeight = 250/768.0f;
		posX = 0.30f;
		posY = 0.87f;
		fc_Difficulty = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_Difficulty",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_Difficulty->load("comboBox.swf");
		fc_Difficulty->setTransparent(true, true);
		fc_Difficulty->bind("difficultySelected",FlashDelegate(this, &FlashGuiDemo2::difficultySelected));
		fc_Difficulty->setDraggable(false);

		//gore checkbox
		controlWidth = 200/1024.0f;
		controlHeight = 40/768.0f;
		posX = 0.48f;
		posY = 0.87f;
		fc_GoreCheckBox = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_GoreCheckBox",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_GoreCheckBox->load("labeledCheckBox.swf");
		fc_GoreCheckBox->callFunction("initCheckBox",Args("Gore:")("left"));
		fc_GoreCheckBox->setTransparent(true, true);
		fc_GoreCheckBox->bind("checkboxChanged",FlashDelegate(this, &FlashGuiDemo2::checkboxChanged));
		fc_GoreCheckBox->setDraggable(false);

		//Name Label
		controlWidth = 300/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.32f;
		posY = 0.02f;
		fc_NameLabel = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_NameLabel",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_NameLabel->load("textLabel.swf");
		fc_NameLabel->setTransparent(true, true);
		fc_NameLabel->callFunction("setText",Args("Name:"));
		fc_NameLabel->setDraggable(false);

		//Name TextBox
		controlWidth = 200/1024.0f;
		controlHeight = 40/768.0f;
		posX = 0.39f;
		posY = 0.01f;
		fc_NameTextBox = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_NameTextBox",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_NameTextBox->load("textbox.swf");
		fc_NameTextBox->callFunction("setTextBox",Args("Enter name..."));
		fc_NameTextBox->setTransparent(true, true);
		FlashValue fv = fc_NameTextBox->callFunction("getTextBox");
		playerName = fv.getString();
		fc_NameTextBox->setDraggable(false);

		//CountDown Label
		controlWidth = 300/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.72f;
		posY = 0.84f;
		fc_CountDownLabel = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_CountDownLabel",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_CountDownLabel->load("textLabel.swf");
		fc_CountDownLabel->setTransparent(true, true);
		fc_CountDownLabel->callFunction("setText",Args("Time to start:"));
		fc_CountDownLabel->setDraggable(false);

		//CountDown Timer
		controlWidth = 300/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.77f;
		posY = 0.84f;
		fc_CountDown = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_CountDown",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_CountDown->load("timer2.swf");
		fc_CountDown->setTransparent(true, true);
		fc_CountDown->callFunction("setTimer",Args("countdown")(180)(60));
		fc_CountDown->bind("timerFinished",FlashDelegate(this, &FlashGuiDemo2::timerFinished));
		fc_CountDown->setDraggable(false);

		//reset
		controlWidth = 120/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.16f;
		posY = 0.9f;
		fc_Reset = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_Reset",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_Reset->load("rectbutton.swf");
		fc_Reset->setTransparent(true, true);
		fc_Reset->callFunction("initRectBtn",Args("Reset"));
		fc_Reset->bind("rectBtnPressed",FlashDelegate(this, &FlashGuiDemo2::rectBtnPressed));
		fc_Reset->setDraggable(false);

		//fc_Finish
		controlWidth = 160/1024.0f;
		controlHeight = 40/768.0f;
		posX = 0.81f;
		posY = 0.9f;
		fc_Finish = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_Finish",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_Finish->load("rectbutton.swf");
		fc_Finish->setTransparent(true, true);
		fc_Finish->callFunction("initRectBtn",Args("Finish"));
		fc_Finish->bind("rectBtnPressed",FlashDelegate(this, &FlashGuiDemo2::rectBtnPressed));
		fc_Finish->setDraggable(false);

		//fc_Random
		controlWidth = 120/1024.0f;
		controlHeight = 30/768.0f;
		posX = 0.02f;
		posY = 0.9f;
		fc_Random = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_Random",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_Random->load("rectbutton.swf");
		fc_Random->setTransparent(true, true);
		fc_Random->callFunction("initRectBtn",Args("Random"));
		fc_Random->bind("rectBtnPressed",FlashDelegate(this, &FlashGuiDemo2::rectBtnPressed));
		fc_Random->setDraggable(false);

		
	}




	void FlashGuiDemo2::AddInfoPanel(Ogre::Viewport* viewport){
		fc_LeftPanel->hide();
		fc_RightPanel->hide();
		fc_ZoomSlider->hide();
		fc_RotateLeft->hide();
		fc_RotateRight->hide();
		fc_ZoomLabel->hide();
		fc_ViewToggle->hide();
		fc_Difficulty->hide();
		fc_GoreCheckBox->hide();
		fc_NameTextBox->hide();
		fc_NameLabel->hide();
		fc_CountDown->hide();
		fc_CountDownLabel->hide();
		fc_Reset->hide();
		fc_Finish->hide();
		fc_Random->hide();
		//fc_infoPanel
		FlashValue fv = fc_NameTextBox->callFunction("getTextBox");
		playerName = fv.getString();
		int screenW = viewport->getActualWidth();
		int screenH = viewport->getActualHeight();
		float controlWidth = 400/1024.0f;
		float controlHeight = 600/768.0f;
		float posX = 0.29f;
		float posY = 0.05f;
		fc_infoPanel = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_infoPanel",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_infoPanel->load("infoPanel.swf");
		fc_infoPanel->setTransparent(true, true);
		Ogre::String infoText = Ogre::String("Character Summary\n\n")
			+Ogre::String("Name:          ")+playerName+Ogre::String("\n")
			+Ogre::String("Gender:          ")+gender+Ogre::String("\n")
			+Ogre::String("Class:          ")+characterClass +Ogre::String("\n")
			+Ogre::String("Attribute:")+Ogre::String("\n")
			+Ogre::String("Strength:      ")+Ogre::StringConverter::toString(nsStr)+Ogre::String("\n")
			+Ogre::String("Dexterity:      ")+Ogre::StringConverter::toString(nsDex)+Ogre::String("\n")
			+Ogre::String("Constitution:      ")+Ogre::StringConverter::toString(nsCon)+Ogre::String("\n")
			+Ogre::String("Intelligence:      ")+Ogre::StringConverter::toString(nsInt)+Ogre::String("\n")
			+Ogre::String("Wisdom:     ")+Ogre::StringConverter::toString(nsWis)+Ogre::String("\n")
			+Ogre::String("Charisma:     ")+Ogre::StringConverter::toString(nsCha)+Ogre::String("\n\n")
			+Ogre::String("Game Options")+Ogre::String("\n")
			+Ogre::String("Difficulty:          ")+difficulty+Ogre::String("\n")
			+Ogre::String("Excessive Gore:          ")+gore+Ogre::String("\n");

		fc_infoPanel->callFunction("setText",Args(infoText));

		//fc_infoPanelBtn
		controlWidth = 160/1024.0f;
		controlHeight = 40/768.0f;
		posX = 0.40f;
		posY = 0.85f;
		fc_infoPanelBtn = FlashGUIPtr->FlashGUIManager->createFlashOverlay("fc_infoPanelBtn",viewport, (int)(controlWidth*screenW),(int)(controlHeight*screenH), Position((short)(posX*screenW),(short)(posY*screenH)));
		fc_infoPanelBtn->load("rectbutton.swf");
		fc_infoPanelBtn->setTransparent(true, true);
		fc_infoPanelBtn->callFunction("initRectBtn",Args("OK"));
		fc_infoPanelBtn->bind("rectBtnPressed",FlashDelegate(this, &FlashGuiDemo2::rectBtnPressed));

	}


	////////////////////////////////////////////////////////
	//call backs
	////////////////////////////////////////////////////////

	FlashValue FlashGuiDemo2::classSelected(FlashControl* caller, const Arguments& args){
		characterClass = (Ogre::DisplayString)(args.at(0).getString());
		return FLASH_VOID;
	}
	FlashValue FlashGuiDemo2::genderSelected(FlashControl* caller, const Arguments& args){
		gender = (Ogre::DisplayString)(args.at(0).getString());
		SetMesh();
		return FLASH_VOID;
	}

	FlashValue FlashGuiDemo2::newStepperValue(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "nsStr")
		{
			int stepperValue = (int)(args.at(1).getNumber());
			nsStr = stepperValue;
		} 
		else if (name == "nsDex")
		{
			int stepperValue = (int)(args.at(1).getNumber());
			nsDex = stepperValue;
		}
		else if (name == "nsCon")
		{
			int stepperValue = (int)(args.at(1).getNumber());
			nsCon = stepperValue;
		}
		else if (name == "nsInt")
		{
			int stepperValue = (int)(args.at(1).getNumber());
			nsInt = stepperValue;
		} 
		else if (name == "nsWis")
		{
			int stepperValue = (int)(args.at(1).getNumber());
			nsWis = stepperValue;
		}
		else if (name == "nsCha")
		{
			int stepperValue = (int)(args.at(1).getNumber());
			nsCha = stepperValue;
		}
		return FLASH_VOID;
	}
	FlashValue FlashGuiDemo2::newSliderValue(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "slHeight")
		{
			//TODO:call ur function
			int sliderValue = (int)(args.at(1).getNumber());
			rHeight = (Ogre::Real)(1.0f+sliderValue*0.1/100.0f);
			
		} 
		else if (name == "slWeight")
		{
			//TODO:call ur function
			int sliderValue = (int)(args.at(1).getNumber());
			rWidth = (Ogre::Real)(1.0+sliderValue*0.2/100.0f);
			
		}
		else if (name == "slCloth")
		{
			//TODO:call ur function
			int sliderValue = (int)(args.at(1).getNumber());
			if(sliderValue==0)
				strAppearance = "1";
			else if(sliderValue==10)
				strAppearance = "2";
			else if(sliderValue==20)
				strAppearance = "3";
			
		}
		else if (name == "slZoom")
		{
			//TODO:call ur function
			int sliderValue = (int)(args.at(1).getNumber());
			Ogre::Camera* pCamera = GetActiveCamera();
			pCamera->setPosition(Ogre::Vector3(camPos.x,camPos.y,camPos.z-sliderValue*0.8f));
		}

		SetMesh();
		return FLASH_VOID;
	}

	FlashValue FlashGuiDemo2::squareButtonPressed(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "rotateL")
		{
			//TODO:call ur function
			pCurrentNode->yaw(Ogre::Degree(-1000 * EnginePtr->GetDeltaTime()));
		} 
		else if (name == "rotateR")
		{
			//TODO:call ur function
			pCurrentNode->yaw(Ogre::Degree(1000 * EnginePtr->GetDeltaTime()));
		}
		return FLASH_VOID;
	}

	FlashValue FlashGuiDemo2::toggleBtnChanged(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		Ogre::Camera* pCamera = GetActiveCamera();
		if (name == "Body")
		{
			//TODO:call ur function
			pCamera->setPosition(Ogre::Vector3(0.0000, 0.0000, 0.0000));
			pCamera->lookAt(0,0,-100);
		} 
		else if (name == "Head")
		{
			//TODO:call ur function
			pCamera->setPosition(Ogre::Vector3(-10.0000, 20.0000, -150));
			pCamera->lookAt(0,20,-150);
		}
		else if (name == "TopDown")
		{
			//TODO:call ur function
			pCamera->setPosition(Ogre::Vector3(0,300,0));
			pCamera->lookAt(0,0,20);
		}
		camPos= pCamera->getPosition();
		return FLASH_VOID;
	}
	FlashValue FlashGuiDemo2::difficultySelected(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "Easy")
		{
			difficulty = name;
		} 
		else if (name == "Normal")
		{
			difficulty = name;
		}
		else if (name == "Hard")
		{
			difficulty = name;
		}
		return FLASH_VOID;
	}
	FlashValue FlashGuiDemo2::checkboxChanged(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "Gore:")
		{
			bool isGore = (bool)(args.at(1).getBool());
			if (isGore)
			{
				gore = "On";
			} 
			else
			{
				gore = "Off";
			}
		} 

		return FLASH_VOID;
	}
	FlashValue FlashGuiDemo2::timerFinished(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "countdown")
		{
			//force to start game
			AddInfoPanel(guiViewport);
		} 

		return FLASH_VOID;
	}
	FlashValue FlashGuiDemo2::rectBtnPressed(FlashControl* caller, const Arguments& args){
		Ogre::DisplayString name = (Ogre::DisplayString)(args.at(0).getString());
		if (name == "Reset")
		{
			fc_LeftPanel->callFunction("resetControl");
		} 
		if (name == "Finish")
		{
			//finish the creation 
			AddInfoPanel(guiViewport);
		} 
		if (name == "Random")
		{
			//random selection
			fc_LeftPanel->callFunction("randomControl");
		} 
		if (name == "OK")
		{
			//exit demo
			//RemoveHUDElements();
			readyToExit = 1;

		} 
	
		return FLASH_VOID;
	}


	
	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::Destroy()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		camPos= pCamera->getPosition();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);
		guiViewport = viewport;
		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		CreateHUDElements(viewport);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::Update()
	{

		

		if (readyToExit > 0)
		{
			if (FlashGUIPtr->FlashGUIManager->isAnyFocused())
			{
				FlashGUIPtr->FlashGUIManager->defocusAll();
			}
			RemoveHUDElements();
			FlashGUIPtr->screenchange = 1;
			readyToExit = 0;
		}
		return true;
	}

	

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool FlashGuiDemo2::Draw()
	{		
		return true;
	}

	bool FlashGuiDemo2::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				
				readyToExit = 1;
				
				break;
			}


		}

		return true;
	}
}