#pragma once


// Main
//#include "Main.h"

// GameScreen
#include "GameScreen.h"
#include "GUIManager/DemoGuiManager.h"
namespace GamePipeGame
{
	class FlashGuiDemo2 : public GamePipe::GameScreen
	{
	private:
		Ogre::SceneManager* pSceneManager;
		Ogre::SceneNode* pManNode;
		Ogre::SceneNode* pWomanNode;
		Ogre::SceneNode* pCurrentNode;
		Ogre::Entity* pManEntity;
		Ogre::Entity* pWomanEntity;
		int readyToExit;
		//////flash controls//////
		Ogre::Viewport* guiViewport;
		FlashControl* fc_LeftPanel;
		FlashControl* fc_RightPanel;
		FlashControl* fc_ZoomSlider;
		FlashControl* fc_RotateLeft;
		FlashControl* fc_RotateRight;
		FlashControl* fc_ZoomLabel;
		FlashControl* fc_ViewToggle;
		FlashControl* fc_Difficulty;
		FlashControl* fc_GoreCheckBox;
		FlashControl* fc_NameTextBox;
		FlashControl* fc_NameLabel;
		FlashControl* fc_CountDown;
		FlashControl* fc_CountDownLabel;
		FlashControl* fc_Reset;
		FlashControl* fc_Finish;
		FlashControl* fc_Random;
		FlashControl* fc_infoPanel;
		FlashControl* fc_infoPanelBtn;
		Ogre::String gender;
		Ogre::String strAppearance;// "1" to "3"
		Ogre::Real rHeight;
		Ogre::Real rWidth;
		Ogre::Real rZoom;
		Ogre::Vector3 camPos;
		
		void CreateHUDElements(Ogre::Viewport* viewport);
		void AddInfoPanel(Ogre::Viewport* viewport);
		void RemoveHUDElements();

		//character variables
		Ogre::DisplayString characterClass;
		int nsStr,nsDex,nsCon,nsInt,nsWis,nsCha;
		Ogre::String difficulty;
		Ogre::String gore;
		Ogre::String playerName;
		

	protected:
	public:
		
		bool LoadResources();
		bool UnloadResources();
		bool Show();
		bool Initialize();
		bool Destroy();
		bool Update();
		bool Draw();
		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

		//
		bool SetMesh();
		bool UpdateControlPos();

		//call backs
		FlashValue genderSelected(FlashControl* caller, const Arguments& args);
		FlashValue classSelected(FlashControl* caller, const Arguments& args);
		FlashValue newStepperValue(FlashControl* caller, const Arguments& args);
		FlashValue newSliderValue(FlashControl* caller, const Arguments& args);
		FlashValue squareButtonPressed(FlashControl* caller, const Arguments& args);
		FlashValue toggleBtnChanged(FlashControl* caller, const Arguments& args);
		FlashValue difficultySelected(FlashControl* caller, const Arguments& args);
		FlashValue checkboxChanged(FlashControl* caller, const Arguments& args);
		FlashValue timerFinished(FlashControl* caller, const Arguments& args);
		FlashValue rectBtnPressed(FlashControl* caller, const Arguments& args);


	public:
		FlashGuiDemo2(std::string name);
		~FlashGuiDemo2() {}
		
	};
}