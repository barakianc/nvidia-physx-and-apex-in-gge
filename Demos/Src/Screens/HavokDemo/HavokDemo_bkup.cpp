// HavokDemo
#include "HavokDemo.h"

// Main
#include "Main.h"
#include "Input.h"


#define USE_VISUAL_DEBUGGER
#include "HavokWrapper.h"

// Gfx
#include "RenderTargetTexture.h"
	
#define USE_VISUAL_DEBUGGER


// custom function to calculate successive positions for keyframed physics objects
void getKeyframePositionAndRotation(hkReal t,hkVector4 &pos,hkQuaternion &rot)
{
	t *= 2 * HK_REAL_PI * 0.2;
	pos.set(50* hkMath::cos(t), +2.0f, -50 * hkMath::sin(t));
	hkVector4 axis(0,1,0);
	rot.setAxisAngle(axis, t);
}

namespace GamePipeGame
{
	GameObjectManager* havokManager;
	GameObject* Chris; // characterObject is global since we need to use the setInputs method to move the character around
	int fb1 = 0, lr1 = 0;
	bool jump1 = false;
	GameObject* bigBox; // keyFramed objects need to be global
	hkTime keyFrameTime = 0; // time for manipulating next keyframe position

	//////////////////////////////////////////////////////////////////////////
	// HavokDemo(std::string name)
	//////////////////////////////////////////////////////////////////////////
	HavokDemo::HavokDemo(std::string name) 
	:	GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::LoadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::UnloadResources()
	{
		//Ogre::ResourceGroupManager::getSingleton().clearResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::Initialize()
	{
		CreateDefaultSceneManager();
		SetActiveCamera(CreateDefaultCamera());	
		SetBackgroundColor(Ogre::ColourValue(0.5000, 0.6000, .7000, 1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));
		Light* light = pSceneManager->createLight("l1");
		light->setType(Light::LT_POINT);
		light->setPosition(Vector3(0, 150, 250));
		
		light->setDiffuseColour(1.0, 1.0, 1.0);
        light->setSpecularColour(1.0, 1.0, 1.0);
		Vector3 camPos(0,0,50);
		m_v3CameraPosition = camPos;

		havokManager = initHavokMemory();
		
		havokManager->CreateWorld();
		
		GameObject* Ground = new GameObject("the_ground.hkx","|the_ground","the_ground","the_ground.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_FIXED);
	
		hkVector4 groundPosition(0,-1,0);
		Ground->m_HavokObj->GetRigidBody()->setPosition(groundPosition);
		
		
		GameObject* dubai_house_Mesh = new GameObject("dubai_house.hkx","dubai_house","dubai_house","dubai_house.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_FIXED);
		hkVector4 house1MPosition(5,-1,0);
		dubai_house_Mesh->m_HavokObj->GetRigidBody()->setPosition(house1MPosition);

		GameObject* dubai_house_Hull = new GameObject("dubai_house_hull.hkx","dubai_house_hull","dubai_house_hull","dubai_house.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_FIXED);
		hkVector4 house1HPosition(-10,-1,0);
		dubai_house_Hull->m_HavokObj->GetRigidBody()->setPosition(house1HPosition);
		
		GameObject* dubai_house_Box = new GameObject("dubai_house_box.hkx","dubai_house_box","dubai_house_box","dubai_house.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_FIXED);
		hkVector4 house1BPosition(20,-1,0);
		dubai_house_Box->m_HavokObj->GetRigidBody()->setPosition(house1BPosition);

		GameObject* middle_east_house = new GameObject("middle_east_house.hkx","middle_eastern_house","middle_east_house","middle_east_house.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_FIXED);
		hkVector4 middleEhouse(35,-1,0);
		middle_east_house->m_HavokObj->GetRigidBody()->setPosition(middleEhouse);
		
		GameObject* Ramp = new GameObject("the_ramp.hkx","|the_ramp","the_ramp","the_ramp.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		Ramp->m_HavokObj->GetRigidBody()->setMass(300);
		hkVector4 rampPosition(10,1,20);
		Ramp->m_HavokObj->GetRigidBody()->setPosition(rampPosition);
		
		GameObject* starBall1 = new GameObject("star.hkx","star1","star1","star.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 starBall1Position(10,3,10);
		starBall1->m_HavokObj->GetRigidBody()->setPosition(starBall1Position);

		GameObject* starBall2 = new GameObject("star.hkx","star1","star2","star.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 starBall2Position(2,3,20);
		starBall2->m_HavokObj->GetRigidBody()->setPosition(starBall2Position);

		GameObject* starBall3 = new GameObject("star.hkx","star1","star3","star.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 starBall3Position(3,3,20);
		starBall3->m_HavokObj->GetRigidBody()->setPosition(starBall3Position);

		
		GameObject* starBall4 = new GameObject("star.hkx","star1","star4","star.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 starBall4Position(26,3,27);
		starBall4->m_HavokObj->GetRigidBody()->setPosition(starBall4Position);
		
		GameObject* Wall = new GameObject("wall5x5x1.hkx","wall1","wall1","wall5x5x1.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 wallPosition(5,2,35);
		Wall->m_HavokObj->GetRigidBody()->setPosition(wallPosition);

		GameObject* stackBox1 = new GameObject("box5x5x5.hkx","|box5x5x5","stackbox1","box5x5x5.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 stackBox1Position(-6,2,38);
		stackBox1->m_HavokObj->GetRigidBody()->setPosition(stackBox1Position);

		GameObject* stackBox2 = new GameObject("box5x5x5.hkx","|box5x5x5","stackbox2","box5x5x5.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 stackBox2Position(-6,8,38);
		stackBox2->m_HavokObj->GetRigidBody()->setPosition(stackBox2Position);

		GameObject* stackBox3 = new GameObject("box5x5x5.hkx","|box5x5x5","stackbox3","box5x5x5.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_MOVING);
		hkVector4 stackBox3Position(-6,14,38);
		stackBox3->m_HavokObj->GetRigidBody()->setPosition(stackBox3Position);

		Chris =  new GameObject(NULL,"chris1","chris1","character.mesh",havokManager,pSceneManager,CHARACTER,CHARACTER_PROXY);
		
		bigBox = new GameObject("box5x5x5.hkx","|box5x5x5","box5x5x5","box5x5x5.mesh",havokManager,pSceneManager,PHYSICS,PHYSICS_KEYFRAMED);
		GameObject* phantomShape = new GameObject(NULL,NULL,NULL,NULL,havokManager,pSceneManager,PHANTOM,PHANTOM_SHAPE);
		GameObject* phantomAabb = new GameObject(NULL,NULL,NULL,NULL,havokManager,pSceneManager,PHANTOM,PHANTOM_AABB);
		

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::Destroy()
	{
		havokManager->DestroyWorld();
		DestroyDefaultSceneManager();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex(), 0, 0, 1.0, 1.0);
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::Update()
	{
		hkVector4 pos;                                           //
		hkQuaternion rot;                                        // done solely 
		getKeyframePositionAndRotation(keyFrameTime,pos,rot);    // for key framed objects;
		bigBox->m_HavokObj->setKeyFrame(pos,rot);                //

		havokManager->UpdateWorld();
		keyFrameTime+=.002; // update to next position, keyFrameTime is equivalent to movement speed

		FreeCameraMovement();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::Draw()
	{
		havokManager->UpdateGraphicsObjects();
		
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
		return true;
	}

	bool HavokDemo::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
				
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		case GIS::KC_0:
			{
				//EnginePtr->BringScreenToFront(GetCallingGameScreen()->GetName());
				break;
			}

			case GIS::KC_U:
			{
				lr1 = 1;
				break;
			}
			case GIS::KC_J:
			{
				lr1 = -1;
				break;
			}
			case GIS::KC_H:
			{
				fb1 = 1;
				break;
			}
			case GIS::KC_K:
			{
				fb1 = -1;
				break;
			}
			case GIS::KC_P:
			{
				jump1 = true;
				break;
			}
		}
		Chris->m_HavokObj->setInputs(lr1,fb1,jump1,0);
		return true;
	}

	bool HavokDemo::OnKeyRelease(const GIS::KeyEvent &keyEvent)
	{
		lr1 = 0;
		fb1 = 0;
		jump1 = false;
		Chris->m_HavokObj->setInputs(lr1,fb1,jump1,0);
		return true;
	}

	bool HavokDemo::OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		return true;
	}


}