#include "StdAfx.h"
// HavokDemo
#include "HavokDemo.h"

using namespace std;

#ifdef HAVOK
int vehicleDamage2=0;

void carCollide(GameObject* f_pOwnerGameObject,hkpRigidBody* f_pCollidedRB,hkVector4* f_vCollisionPoint)
{	
	std::string f_sOwnerName = f_pOwnerGameObject->m_pGraphicsObject->m_sOgreUniqueName;//just for debugging purposes
	if (f_pCollidedRB!=NULL)
	{
		if (f_pCollidedRB->getCollisionFilterInfo()==hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_GROUND))
			vehicleDamage2 = vehicleDamage2 - 1;
	}
	vehicleDamage2 = vehicleDamage2 + 1;
}

hkReal totalKeyframeTime=0;
bool isBreak=false;

void customPhantomBox1Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime);
void customPhantomBox2Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime);
void customPhantomBox3Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime);
void customPhantomBox4Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime);

void customKeyframeFunction(PhysicsPrimitiveObject* f_pOwnerObject,hkReal f_fDeltaTime,hkVector4 &f_pPositionToSet, hkQuaternion &f_pOrientationToSet)
{
	totalKeyframeTime += f_fDeltaTime*0.1f;
	if (totalKeyframeTime>360) totalKeyframeTime-=360;

	hkReal time = hkReal(totalKeyframeTime * 2 * HK_REAL_PI * 0.2);
	f_pPositionToSet.set(50* hkMath::cos(time), +2.0f, -50 * hkMath::sin(time));
	hkVector4 axis(0,1,0);
	f_pOrientationToSet.setAxisAngle(axis, time);
}


//function for breakable glass
void customPhantomBGFunction(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime)
{
		isBreak=true;
		
}

void customPhantomAabbFunction(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime)
{
	const hkReal f_fRadius = 6.0f;
	const hkReal phantomSide = 2;

	int intToMinusFromTotal = (int)((int)f_fTotalTime%(10));
	hkReal totTim = f_fTotalTime - intToMinusFromTotal;
	
	f_pOwnerAabbPhantom->ensureDeterministicOrder();
	for (int i = 0; i < f_pOwnerAabbPhantom->getOverlappingCollidables().getSize(); i++ )
	{
		try 
		{
			hkpRigidBody* f_pRigidBody = static_cast<hkpRigidBody*>(f_pOwnerAabbPhantom->getOverlappingCollidables()[i]->getOwner());
			if (f_pRigidBody!=NULL)
			{
				if (f_pRigidBody->getMotionType()==hkpMotion::MOTION_SPHERE_INERTIA)
				{
					f_pRigidBody->activate();
					hkReal		f_fHeight = f_pRigidBody->getPosition()(1);//RDS_LOOK shouldn't 1 be i
					hkVector4	f_vForce(0.0f,2.0f*(10.0f - f_fHeight),0.0f );

					f_pRigidBody->applyForce( totTim*f_fRadius/100, f_vForce );
					//f_pRigidBody->applyForce( f_fTimeStep,hkVector4(0.0f,-1.0f,0.0f) );
					//f_pRigidBody->setLinearVelocity( hkVector4(0.0f,-1.0f,0.0f) );
				}
			}
		}
		catch (...)
		{
			//do nothing
		}
	}
}

void customPhantomEnterShapeFunction(PhantomShapeObject* f_pOwnerPhantom,hkpRigidBody* f_pCollidedRigidBody,GameObject* f_pCollidedGameObject)
{
	hkVector4 m_vPhantomEnterLinearVelocityEffect = hkVector4(0,-1,0,0);
	switch (f_pCollidedRigidBody->getMotionType())
	{
		case hkpMotion::MOTION_FIXED:
		case hkpMotion::MOTION_KEYFRAMED:
			break;
		default:
			f_pCollidedRigidBody->setLinearVelocity(m_vPhantomEnterLinearVelocityEffect);
			break;
	}
}
void customPhantomLeaveShapeFunction(PhantomShapeObject* f_pOwnerPhantom,hkpRigidBody* f_pCollidedRigidBody,GameObject* f_pCollidedGameObject)
{
	hkVector4 m_vPhantomLeaveLinearVelocityEffect = hkVector4(0,2,0,0);
	switch (f_pCollidedRigidBody->getMotionType())
	{
		case hkpMotion::MOTION_FIXED:
		case hkpMotion::MOTION_KEYFRAMED:
			break;
		default:
			f_pCollidedRigidBody->setLinearVelocity(m_vPhantomLeaveLinearVelocityEffect);
			break;
	}
}
namespace GamePipeGame
{
	/// Previously HavokDemo(std::string name), is the constructor for the Previous Year's Havok Demo. 
	/// The string parameter name is used to give the screen's title
	HavokDemo::HavokDemo(std::string name) :GamePipe::GameScreen(name){}
	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		return true;
	}
	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool HavokDemo::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		return true;
	}
	/// Function to initialize all objects. Returns true if successful
	bool HavokDemo::Initialize()
	{
		checkPosition = hkVector4(12,20,-13);//m_pHussiCharProxy pos 10,15,2
		teleportPosition = hkVector4(10,20,2);//10,20,2
		bound = 2;
		m_bAnimateAlexStormTropper=true;
		m_iForwardMinus_BackPlus = 0.0f;
		m_iLeftPlus_RightMinus = 0.0f;
		m_fRotAngle_LeftPlus_RightMinus = 0.0f;
		m_bJUMP = false;
		ragdollToken=false;
		ToggleViewportToken=false;
		m_iBallCount=0;
		vehicleDamage2 = 0;
		toggle_v_c=3;
		toggle = 0;
		controlToggle = 0;
		phantomToggle = true;
		m_bRayCastActive=false;

		m_fCameraMovementSpeed*=10;
		GetGameObjectManager()->SetVisualDebugger(true);
		SetBackgroundColor(Ogre::ColourValue(0.5f, 0.6f, 0.7f, 1.0f));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(1.0f, 1.0f, 1.0f, 1.0f));
		pSceneManager->setSkyBox(true, "MySkyBox", 1000);
		//pSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
		
		Ogre::Light* light = pSceneManager->createLight("light10");
		light->setType(Ogre::Light::LT_POINT);
		light->setPosition(Ogre::Vector3(0, 150, 250));
		light->setDiffuseColour(1.0, 1.0, 1.0);
		light->setSpecularColour(1.0, 1.0, 1.0);

		Ogre::Light* light2 = pSceneManager->createLight("light11");
		light2->setType(Ogre::Light::LT_POINT);
		light2->setPosition(Ogre::Vector3(0, 150, -250));
		light2->setDiffuseColour(1.0, 1.0, 1.0);
		light2->setSpecularColour(1.0, 1.0, 1.0);
		
		Ogre::Vector3 camPos(0,0,50);
		m_v3CameraPosition = camPos;

		Ogre::Camera* pCamera = pSceneManager->createCamera("Camera1");
		// set its position, direction  
        pCamera->setPosition(Ogre::Vector3(49,30,55));
        pCamera->lookAt(Ogre::Vector3(49,3,42));
		pCamera->setNearClipDistance(5);

		GameObject* Ground = new GameObject("the_ground","the_ground.mesh","the_ground.hkx",PHYSICS_FIXED);
		Ground->setPosition(0,-1,0);
		Ground->m_pPhysicsObject->getRigidBody()->setCollisionFilterInfo(hkpGroupFilter::calcFilterInfo(COLLISION_LAYER_GROUND));
		Ground->m_pGraphicsObject->m_pOgreEntity->setMaterialName("havok_demo/grassMaterial");

		//Physics System
		GameObject* myFixedRopeBridge = new GameObject("myBrekRopeBridgeFIXED","ExcellentBridge.mesh","ExcellentBridge.hkx",PHYSICS_FIXED);
		myFixedRopeBridge->setPosition(100,-1,-55);

		myBrekRopeBridge = new GameObject("myBrekRopeBridgeBREAKING","ExcellentBridge.mesh","ExcellentBridge.hkx",PHYSICS_SYSTEM);
		myBrekRopeBridge->setPosition(-20,-1,-55);

		GameObject* myUnbreakRopeBridge = new GameObject("myUnbreakRopeBridge","ExcellentBridge.mesh","UnbreakableBridge.hkx");
		myUnbreakRopeBridge->setPosition(5,-1,-55);

		GameObject* dubai_house_Hull = new GameObject("dubai_house_hull","dubai_house.mesh","",PHYSICS_FIXED);
		dubai_house_Hull->setPosition(-10,-1,0);
		
		GameObject* dubai_house_Box = new GameObject("dubai_house_box","dubai_house.mesh","dubai_house_box.hkx",PHYSICS_FIXED);
		dubai_house_Box->setPosition(20,-1,0);

		GameObject* middle_east_house = new GameObject("middle_east_house","middle_east_house.mesh","middle_east_house.hkx");
		middle_east_house->setPosition(35,-1,0);
		
		GameObject* Ramp = new GameObject("the_ramp","the_ramp.mesh","the_ramp.hkx");
		Ramp->setPosition(5,1,20);
		
		//create the balls
		createNonsenseHavokBalls();
		
		GameObject* Wall = new GameObject("wall1","wall5x5x1.mesh","wall5x5x1.hkx");
		Wall->setPosition(5,2,35);

		GameObject* stackBox1 = new GameObject("stackbox1","box5x5x5.mesh","box5x5x5.hkx");
		stackBox1->setPosition(-6,2,38);

		GameObject* stackBox2 = new GameObject("stackbox2","box5x5x5.mesh","box5x5x5.hkx");
		stackBox2->setPosition(-6,8,38);

		CarObject = new GameObject("car1","bugatti_veyron.ma.mesh","", VEHICLE_CAR);
		CarObject->setPosition(-40.0, 5, 5.0);
		CarObject->addCollisionListener();
		CarObject->setOverridableCollisionFuction(COLLISION_LISTENER_TYPE2,carCollide);
		float a[]={1.75f,0.25f,1.1f,1.9f,0.15f,1.0f,0.4f,-1.0f,0.7f,0.7f,0.4f,-1.0f,0.25f,1.2f};
		((VehicleObject *)CarObject->m_pPhysicsObject)->setDimensions(a);
		KeyFramedBox = new GameObject("box5x5x5","box5x5x5.mesh","box5x5x5.hkx", PHYSICS_KEYFRAMED);
		KeyFramedBox->setPosition(40,2,0);//the first position will be calculated like this
		//not to move the key framed object too fast we have to calculate the first position for the key framed object
		((PhysicsPrimitiveObject *)KeyFramedBox->m_pPhysicsObject)->setCustomKeyframedFunction(customKeyframeFunction,true);

		phantomBG = new GameObject("PhantomBG","breakable_glass.mesh","",PHANTOM_AABB);
		phantomBG->setVisible(true);
		phantomBG->scale(1,1,2);
		phantomBG->setPosition(15,2,38);
		((PhantomAabbObject *)phantomBG->m_pPhysicsObject)->setCustom_PhantomAabb_Collision_Callback_Function(customPhantomBGFunction);
		
		boundingBox= new GameObject("BoundingBox","bounding_box.mesh","bounding_box.hkx",PHYSICS_SYSTEM);
		boundingBox->setPosition(140,-1,-55);

		boundingMesh= new GameObject("BoundingMesh","bounding_mesh.mesh","bounding_mesh.hkx",PHYSICS_SYSTEM);
		boundingMesh->setPosition(170,-1,-55);

		boundingHull= new GameObject("BoundingHull","bounding_hull.mesh","bounding_hull.hkx",PHYSICS_SYSTEM);
		boundingHull->setPosition(200,-1,-55);

		newBridge= new GameObject("bridge","bridge.mesh","bridge.hkx",PHYSICS_SYSTEM);
		newBridge->setPosition(140,-1,-20);

		newBreakableBridge= new GameObject("BreakableBridge","bridge_breakable.mesh","bridge_breakable.hkx",PHYSICS_SYSTEM);
		newBreakableBridge->setPosition(160,-1,-20);

		pendulumMoved= new GameObject("pendulumMoved","pendulum_moved.mesh","pendulum_moved.hkx",PHYSICS_SYSTEM);
		pendulumMoved->setPosition(160,-1,20);

		pendulum= new GameObject("pendulum","pendulum.mesh","pendulum.hkx",PHYSICS_SYSTEM);
		pendulum->setPosition(130,-1,20);

		//
		constraint3= new GameObject("BallChain2","pendulum_spring.mesh","pendulum_spring.hkx",PHYSICS_SYSTEM);
		constraint3->setPosition(100,-1,20);

		/*********CHARACTER RIGIDBODIES AND PROXIES ************/
		m_pRAHCharRigBod = new GameObject("homelessRigidBody", "MAGICIAN.ma.mesh", "", PHYSICS_CHARACTER_RIGIDBODY);
		m_pRAHCharRigBod->scale(0.5f, 0.5f, 0.5f);
		m_pRAHCharRigBod->setPosition(-20, 0, 20);

		m_pRDSRoboCharAnimRigBody = new GameObject("robot","robot.mesh","", ANIMATED_CHARACTER_RIGIDBODY);
		m_pRDSRoboCharAnimRigBody->scale(0.1f,0.1f,0.1f);
		m_pRDSRoboCharAnimRigBody->setPosition(-10,0,20);
		m_pRDSRoboCharAnimRigBody->m_pAnimatedEntity->PlayAnimation("Walk");

		m_pHussiCharProxy =  new GameObject("HussiProxy","character.mesh","", PHYSICS_CHARACTER_PROXY);
		m_pHussiCharProxy->setPosition(-5, 0, 20);

		m_pAlexStormCharAnimProxy = new GameObject("stormTrooperProxy", "stormtrooper_withgun_bound_LOD.mesh", "storm_trooper_ragdoll_complete.hkx", ANIMATED_CHARACTER_PROXY);
		m_pAlexStormCharAnimProxy->m_pGraphicsObject->setVisible(true);
		m_pAlexStormCharAnimProxy->setPosition(0,0,20);
		((CharacterProxyObject *)m_pAlexStormCharAnimProxy->m_pPhysicsObject)->setBaseLookingDirection(70);

		((CharacterRigidBodyObject *)m_pRAHCharRigBod->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		((CharacterRigidBodyObject *)m_pRDSRoboCharAnimRigBody->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		((CharacterProxyObject *)m_pHussiCharProxy->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		((CharacterProxyObject *)m_pAlexStormCharAnimProxy->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);

		//RDS_LOOK
		m_pPhantomShape = new GameObject("s_PhantomShape","","",PHANTOM_SHAPE,COLLISION_SHAPE_SPHERE);
		m_pPhantomShape->setPosition(10, 0, 10);
		m_pPhantomShape->setVisible(false);
		((PhantomShapeObject *)m_pPhantomShape->m_pPhysicsObject)->setCustom_phantomEnterEvent_Function(customPhantomEnterShapeFunction);
		((PhantomShapeObject *)m_pPhantomShape->m_pPhysicsObject)->setCustom_phantomLeaveEvent_Function(customPhantomLeaveShapeFunction);

		m_pPhantomAabb = new GameObject("s_PhantomAAbb","the_ramp.mesh","",PHANTOM_AABB);
		m_pPhantomAabb->setVisible(false);
		m_pPhantomAabb->scale(5,0.5f,1);
		m_pPhantomAabb->setPosition(50.0f,0.5f, 43.0f);
		((PhantomAabbObject *)m_pPhantomAabb->m_pPhysicsObject)->setCustom_PhantomAabb_Collision_Callback_Function(customPhantomAabbFunction);

		m_pPhantomBox1 = new GameObject("m_pPhantomBox1","box5x5x5.mesh","",PHANTOM_AABB);
		m_pPhantomBox1->setVisible(false);
		m_pPhantomBox1->setPosition(20.0f, 3.0f, -35.0f);
		((PhantomAabbObject *)m_pPhantomBox1->m_pPhysicsObject)->setCustom_PhantomAabb_Collision_Callback_Function(customPhantomBox1Function);

		m_pPhantomBox2 = new GameObject("m_pPhantomBox2","box5x5x5.mesh","",PHANTOM_AABB);
		m_pPhantomBox2->setVisible(false);
		m_pPhantomBox2->setPosition(50.0f, 3.0f, -35.0f);
		((PhantomAabbObject *)m_pPhantomBox2->m_pPhysicsObject)->setCustom_PhantomAabb_Collision_Callback_Function(customPhantomBox2Function);

		m_pPhantomBox3 = new GameObject("m_pPhantomBox3","box5x5x5.mesh","",PHANTOM_AABB);
		m_pPhantomBox3->setVisible(false);
		m_pPhantomBox3->setPosition(50.0f, 3.0f, -70.0f);
		((PhantomAabbObject *)m_pPhantomBox3->m_pPhysicsObject)->setCustom_PhantomAabb_Collision_Callback_Function(customPhantomBox3Function);

		m_pPhantomBox4 = new GameObject("m_pPhantomBox4","box5x5x5.mesh","",PHANTOM_AABB);
		m_pPhantomBox4->setVisible(false);
		m_pPhantomBox4->setPosition(20.0f, 3.0f, -70.0f);
		((PhantomAabbObject *)m_pPhantomBox4->m_pPhysicsObject)->setCustom_PhantomAabb_Collision_Callback_Function(customPhantomBox4Function);

		UtilitiesPtr->Add("Formal_Elements", new GamePipe::DebugText(
			"Havok Physics\n\n\
			Camera Controls: W/S/A/D\n\n\
			Main Controls:\n\
			F1 -> Flycheat ON/OFF\n\
			F2 -> Activate Vehicle\n\
			F3 -> Activate Character Proxy\n\
			F4 -> Activate Animated Character Proxy\n\
			F5 -> Activate Character RigidBody\n\
			F6 -> Activate Animated Character Rigid Body \n\n\
			Character Controls:\n\
			Move - Left/Right/Up/Down Arrow Keys\n\
			Rotate - J/K\n\
			JUMP - Left Control\n\
			Scale - Comma(,)/Period(.)\n\
			ESC - Quit\n",
			(Ogre::Real) 0.001, 
			(Ogre::Real) 0.001, 
			(Ogre::Real) 2.5, 
			Ogre::ColourValue(1.0f, 1.0f, 1.0f, 1.0f), 
			Ogre::TextAreaOverlayElement::Left));
		UtilitiesPtr->Add("Formal_Elements1", new GamePipe::DebugText(
			"Adhoc Controls\n\n\
			Spacebar -> Shoot\n\
			0 ->Stop StormTrooper Animation\n\
			1-6 Change StormTrooper Animations\n\
			7 -> Reset Havok Balls\n\
			L -> ViewPort\n\
			P -> Toggle Phantom\n\
			B -> Create/Destroy Bridge\n", 
			(Ogre::Real) 0.68, 
			(Ogre::Real) 0.001, 
			(Ogre::Real) 2.5, 
			Ogre::ColourValue(1, 1, 1, 1), 
			Ogre::TextAreaOverlayElement::Left));
		sprintf_s(cardamageText,"Car Damage = %d",vehicleDamage2);


		UtilitiesPtr->Add("CarDamageText", new GamePipe::DebugText(cardamageText, (Ogre::Real)0.2, (Ogre::Real)0.005, (Ogre::Real)2.5, Ogre::ColourValue(1.0f, 0.0f, 0.0f, 1.0f), Ogre::TextAreaOverlayElement::Left));
		
		// We disable collisions between different layers to determine what behavior we want

		//hkpGroupFilter* groupFilter = new hkpGroupFilter();
		//groupFilter->disableCollisionsBetween(HavokDemo::LAYER_RAGDOLL_PENETRATING, HavokDemo::LAYER_LANDSCAPE);
		//groupFilter->disableCollisionsBetween(HavokDemo::LAYER_GROUND, HavokDemo::LAYER_LANDSCAPE);
		//prevHavokManager->GetWorld()->setCollisionFilter( groupFilter, true);
		//groupFilter->removeReference();

		return true;
	}

	/// Function to destroy the screen
	bool HavokDemo::Destroy()
	{
		return true;
	}

	/// Function to setup camera viewports and show the screen
	bool HavokDemo::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex(), 0, 0, 1.0, 1.0);
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		return true;
	}

	/// Function to setup a different viewport
	void HavokDemo::ViewportToggle(bool value)
	{
		if(value)
		{
			Ogre::Camera* defCamera;
			Ogre::Viewport* viewport;
			//Camera 1
			defCamera = GetDefaultSceneManager()->getCamera("Camera1");
			viewport = EnginePtr->GetRenderWindow()->addViewport(defCamera, EnginePtr->GetFreeViewportIndex(), 0.8f, 0.8f, 0.2f, 0.2f);
			
			//viewport->setBackgroundColour(Ogre::ColourValue(0,0,0,1));
			viewport->setOverlaysEnabled(true);
			SetViewport(viewport);

			defCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));
		}
		else
		{
			EnginePtr->GetRenderWindow()->removeAllViewports();
			Show();
		}
	}

	/// Screen Update
	bool HavokDemo::Update()
	{

		if(isBreak==true)
		{
			collWall = new GameObject("collWall","breakable_glass.mesh","breakable_glass.hkx",PHYSICS_SYSTEM);
			collWall->setPosition(15,2,38);
			isBreak=false;
			phantomBG->removeThis();

		}

		if((controlToggle == 1)&&(InputPtr->IsKeyDown(GIS::KC_DOWN)))
		{
			if(CarObject != NULL)
				((VehicleObject *)CarObject->m_pPhysicsObject)->reverse();
		}

		if((controlToggle == 1)&&(InputPtr->IsKeyDown(GIS::KC_UP)))
		{
			if(CarObject != NULL)
				((VehicleObject *)CarObject->m_pPhysicsObject)->accelerate();
		}

		if((controlToggle == 1)&&(InputPtr->IsKeyDown(GIS::KC_LEFT)))
		{
			if(CarObject != NULL)
				((VehicleObject *)CarObject->m_pPhysicsObject)->steer_left();
		}
		else if((controlToggle == 1) && (!InputPtr->IsKeyDown(GIS::KC_RIGHT)))
		{
			if(CarObject != NULL)
				((VehicleObject *)CarObject->m_pPhysicsObject)->go_straight();
		}

		if((controlToggle == 1)&& (InputPtr->IsKeyDown(GIS::KC_RIGHT)))
		{
			if(CarObject != NULL)
				((VehicleObject *)CarObject->m_pPhysicsObject)->steer_right();
		}
		else if((controlToggle == 1) && (!InputPtr->IsKeyDown(GIS::KC_LEFT)))
		{
			if(CarObject != NULL)
				((VehicleObject *)CarObject->m_pPhysicsObject)->go_straight();
		}


		if((controlToggle == 2 || controlToggle == 3 || controlToggle == 4 || controlToggle ==5)&&(InputPtr->IsKeyDown(GIS::KC_UP)))
		{
			m_iForwardMinus_BackPlus = -1.0f;
		}

		if((controlToggle == 2 || controlToggle == 3 || controlToggle == 4 || controlToggle ==5)&&(InputPtr->IsKeyDown(GIS::KC_DOWN)))
		{
			m_iForwardMinus_BackPlus = 1.0f;
		}

		if((controlToggle == 2 || controlToggle == 3 || controlToggle == 4 || controlToggle ==5)&&(InputPtr->IsKeyDown(GIS::KC_LEFT)))
		{
			m_iLeftPlus_RightMinus = 1.0f;
		}

		if((controlToggle == 2 || controlToggle == 3 || controlToggle == 4 || controlToggle ==5)&&(InputPtr->IsKeyDown(GIS::KC_RIGHT)))
		{
			m_iLeftPlus_RightMinus = -1.0f;
		}

		switch(controlToggle){

			case 2: ((CharacterProxyObject *)m_pHussiCharProxy->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
					break;

			case 3:	((CharacterProxyObject *)m_pAlexStormCharAnimProxy->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
					break;

			case 4: ((CharacterRigidBodyObject *)m_pRAHCharRigBod->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
					break;

			case 5:	((CharacterRigidBodyObject *)m_pRDSRoboCharAnimRigBody->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
					break;	 
		}
		
		sprintf_s(cardamageText,"Car Damage = %d",vehicleDamage2);
		UtilitiesPtr->GetDebugText("CarDamageText")->SetText(cardamageText);

		FreeCameraMovement();
		return true;
	}

	/// Function to draw all screen elements
	bool HavokDemo::Draw()
	{
		return true;
	}

	bool HavokDemo::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{		
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				UtilitiesPtr->Remove("Formal_Elements");
				UtilitiesPtr->Remove("Formal_Elements1");
				UtilitiesPtr->Remove("CarDamageText");
				Close();
				break;
			}
		case GIS::KC_F2:	//	Activate Vehicle
			{
				//toggle_v_c = 0;
				controlToggle = 1;
				break;
			}
		case GIS::KC_F3:	//Activate Character Proxy Hussi
			{
				//toggle_v_c = 1;
				controlToggle = 2;
				break;
			}

		case GIS::KC_F4:	//Activate Character Proxy With Animation (Robot) RDS
			{
				controlToggle = 3;
				break;
			}
		case GIS::KC_F5:	//Activate Character Rigid Body (stormTrooper) RAH
			{

				controlToggle = 4;
				break;
			}
		case GIS::KC_F6:	//Activate Character Rigid Body With Animation (Alex)
			{

				controlToggle = 5;
				break;
			}


		case GIS::KC_F1:
			{
				if(toggle == 0)
				{
					//Call the flycheat
					((CharacterProxyObject *)m_pHussiCharProxy->m_pPhysicsObject)->flyCheat();
					((CharacterProxyObject *)m_pAlexStormCharAnimProxy->m_pPhysicsObject)->flyCheat();
					toggle = 1;
				}
				else
				{
					//Disable the flycheat
					((CharacterProxyObject *)m_pHussiCharProxy->m_pPhysicsObject)->removeFlyCheat();
					((CharacterProxyObject *)m_pAlexStormCharAnimProxy->m_pPhysicsObject)->removeFlyCheat();
					m_bJUMP = true;
					toggle = 0;
				}
				break;
			}

		case GIS::KC_0:
			{

				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAnimation();
				
				break;
			}
		case GIS::KC_1:
			{
				//m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAllAnimations();
				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->PlayAnimation("stormtrooper_attitude_walk");
				break;
			}
		case GIS::KC_2:
			{
				//m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAllAnimations();
				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->PlayAnimation("stormtrooper_guard_n_wait");
				break;
			}
		case GIS::KC_3:
			{
				//m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAllAnimations();
				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->PlayAnimation("stormtrooper_talk");
				break;
			}
		case GIS::KC_4:
			{
				//m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAllAnimations();
				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->PlayAnimation("stormtrooper_challenge_opp");
				break;
			}
		case GIS::KC_5:
			{
				//m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAllAnimations();
				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->PlayAnimation("stormtrooper_spot_n_shoot");
				break;
			}
		case GIS::KC_6:
			{
				//m_pAlexStormCharAnimProxy->m_pAnimatedEntity->StopAllAnimations();
				m_pAlexStormCharAnimProxy->m_pAnimatedEntity->PlayAnimation("stormtrooper_dance");
				break;
			}
		case GIS::KC_7:
			{
				resetNonsenseHavokBalls();
				break;
			}
		case GIS::KC_P:
			{
				if(!phantomToggle){
					phantomToggle = true;
				}
				else{
					phantomToggle = false;
				}

				if(phantomToggle){
					m_pPhantomAabb->setVisible(false);
					m_pPhantomShape->setVisible(false);
					m_pPhantomBox1->setVisible(false);
					m_pPhantomBox2->setVisible(false);
					m_pPhantomBox3->setVisible(false);
					m_pPhantomBox4->setVisible(false);

				}
				else{
					m_pPhantomAabb->setVisible(true);
					m_pPhantomShape->setVisible(true);
					m_pPhantomBox1->setVisible(true);
					m_pPhantomBox2->setVisible(true);
					m_pPhantomBox3->setVisible(true);
					m_pPhantomBox4->setVisible(true);
				}
				break;
			}
		case GIS::KC_LCONTROL:
			{
				m_bJUMP = true;
				break;

			}
		case GIS::KC_J:
			{
				m_fRotAngle_LeftPlus_RightMinus = (hkReal)0.2;
				break;
			}

		case GIS::KC_K:
			{
				m_fRotAngle_LeftPlus_RightMinus = -((hkReal)0.2);
				break;
			}
		case GIS::KC_L:
			{
				ToggleViewportToken=!ToggleViewportToken;
				ViewportToggle(ToggleViewportToken);
				break;
			}
		case GIS::KC_SPACE:
			{
				m_iBallCount++;
				string bulletNameString = "s_bullet_";
				bulletNameString.append(Ogre::StringConverter::toString(m_iBallCount).c_str());
				char* bulletName = new char[30];
				bulletName = (char*)bulletNameString.c_str();

				GameObject*	m_pBulletObject = new GameObject(bulletName,"star.mesh","star.hkx",PHYSICS_DYNAMIC);

				m_pBulletObject->m_pPhysicsObject->getRigidBody()->setQualityType(HK_COLLIDABLE_QUALITY_BULLET);
				m_pBulletObject->m_pPhysicsObject->getRigidBody()->setMotionType(hkpMotion::MOTION_BOX_INERTIA);
				m_pBulletObject->m_pPhysicsObject->getRigidBody()->setFriction(0.0f);
				m_pBulletObject->m_pPhysicsObject->getRigidBody()->setRestitution(1.0f);

				Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
				Ogre::Quaternion quat=pCameraSceneNode->getOrientation();

				m_pBulletObject->m_pPhysicsObject->getRigidBody()->setLinearVelocity(hkVector4(200.0f*GetActiveCamera()->getDerivedDirection().x,200.0f*GetActiveCamera()->getDerivedDirection().y,200.0f*GetActiveCamera()->getDerivedDirection().z));
				Ogre::Vector3 vectPos=GetActiveCameraSceneNode()->getPosition();
				m_pBulletObject->setPosition(vectPos.x,vectPos.y,vectPos.z);
				break;
			}
		case GIS::KC_R :
			{
				//m_pPhantomBox1->setPosition(20.0f, 3.0f, -35.0f);
				//m_pPhantomBox2->setPosition(50.0f, 3.0f, -35.0f);
				//m_pPhantomBox3->setPosition(50.0f, 3.0f, -70.0f);
				//m_pPhantomBox4->setPosition(20.0f, 3.0f, -70.0f);
				GameObjectManager* myGOM =  EnginePtr->GetForemostGameScreen()->GetGameObjectManager();
				Ogre::Vector3		f_vCastFrom = Ogre::Vector3(35,3,-32);
				Ogre::Vector3		f_vCastTo = Ogre::Vector3(35,3,-73);
				hkpRigidBody*		f_pFirstCollidedRigidBody = NULL;
				GameObject*			f_pCollidedGameObject = NULL;
				Ogre::Vector3		f_vReturnCollisionPoint;
				myGOM->castRay(f_vCastFrom,f_vCastTo,&f_pFirstCollidedRigidBody,&f_pCollidedGameObject,&f_vReturnCollisionPoint);
				if (f_pFirstCollidedRigidBody!=NULL)
					f_pFirstCollidedRigidBody->setLinearVelocity(hkVector4(0,-1,0));
				if (f_pCollidedGameObject!=NULL)
					f_pCollidedGameObject->scale(0.5,0.5,0.5);
				f_vCastFrom = Ogre::Vector3(17,3,-47.5);
				f_vCastTo = Ogre::Vector3(53,3,-47.5);
				myGOM->castRay(f_vCastFrom,f_vCastTo,&f_pFirstCollidedRigidBody,&f_pCollidedGameObject,&f_vReturnCollisionPoint);
				if (f_pFirstCollidedRigidBody!=NULL)
					f_pFirstCollidedRigidBody->setLinearVelocity(hkVector4(0,-1,0));
				if (f_pCollidedGameObject!=NULL)
					f_pCollidedGameObject->scale(0.5,0.5,0.5);


				break;
			}
		case GIS::KC_COMMA :
			{
				switch(controlToggle)
				{
					case 2: m_pHussiCharProxy->scale(1.1f, 1.1f, 1.1f);
						break;

					case 3:	m_pAlexStormCharAnimProxy->scale(1.1f, 1.1f, 1.1f);
						break;

					case 4: m_pRAHCharRigBod->scale(1.1f, 1.1f, 1.1f);
						break;

					case 5:	m_pRDSRoboCharAnimRigBody->scale(1.1f, 1.1f, 1.1f);
						break;	 
				}
				break;
			}
		case GIS::KC_PERIOD :
			{
				switch(controlToggle)
				{
				case 2: m_pHussiCharProxy->scale(0.9f, 0.9f, 0.9f);
					break;

				case 3:	m_pAlexStormCharAnimProxy->scale(0.9f, 0.9f, 0.9f);
					break;

				case 4: m_pRAHCharRigBod->scale(0.9f, 0.9f, 0.9f);
					break;

				case 5:	m_pRDSRoboCharAnimRigBody->scale(0.9f, 0.9f, 0.9f);
					break;	 
				}
				break;
			}
		case GIS::KC_B :
			{
				if (myBrekRopeBridge==NULL)
				{
					myBrekRopeBridge = new GameObject("myBrekRopeBridgeBREAKING","ExcellentBridge.mesh","ExcellentBridge.hkx",PHYSICS_SYSTEM);
					myBrekRopeBridge->setPosition(-20,-1,-55);
				}
				else
				{
					myBrekRopeBridge->removeThis();
					myBrekRopeBridge=NULL;
				}
				break;
			}
		case GIS::KC_V :
			{
				//if (CarObject==NULL)
				//{
				//	CarObject = new GameObject("car1","bugatti_veyron.ma.mesh","", VEHICLE_CAR);
				//	CarObject->setPosition(-40.0, 5, 5.0);
				//	CarObject->addCollisionListener();
				//	CarObject->setOverridableCollisionFuction(COLLISION_LISTENER_TYPE2,carCollide);
				//}
				//else
				//{
				//	CarObject->removeThis();
				//	CarObject=NULL;
				//}
				break;
			}
		}
		return true;
	}

	bool HavokDemo::OnKeyRelease(const GIS::KeyEvent &keyEvent)
	{
		m_iLeftPlus_RightMinus = 0.0f;
		m_iForwardMinus_BackPlus = 0.0f;
		m_fRotAngle_LeftPlus_RightMinus = 0.0f;
		m_bJUMP = false;
		if(CarObject != NULL)
		{
			((VehicleObject *)CarObject->m_pPhysicsObject)->slow();
			((VehicleObject *)CarObject->m_pPhysicsObject)->go_straight();
		}
		((CharacterRigidBodyObject *)m_pRAHCharRigBod->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		((CharacterRigidBodyObject *)m_pRDSRoboCharAnimRigBody->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		((CharacterProxyObject *)m_pHussiCharProxy->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		((CharacterProxyObject *)m_pAlexStormCharAnimProxy->m_pPhysicsObject)->setInputs(m_iLeftPlus_RightMinus,m_iForwardMinus_BackPlus,m_bJUMP,m_fRotAngle_LeftPlus_RightMinus);
		return true;
	}

	bool HavokDemo::OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId)
	{
		return true;
	}

	void HavokDemo::resetNonsenseHavokBalls()
	{
		for (int i=0;i<49;i++)
			starBall[i]->m_pPhysicsObject->getRigidBody()->setLinearVelocity(hkVector4(0.0f,0.0f,0.0f));

		starBall[0]->setPosition(10,3,10);
		starBall[1]->setPosition(-10,30,0);
		starBall[2]->setPosition(40,3,40);
		starBall[3]->setPosition(40,3,41);
		starBall[4]->setPosition(40,3,43);
		starBall[5]->setPosition(40,3,44);
		starBall[6]->setPosition(40,3,42);
		starBall[7]->setPosition(41,3,42);
		starBall[8]->setPosition(42,3,42);
		starBall[9]->setPosition(43,3,42);
		starBall[10]->setPosition(43,3,40);
		starBall[11]->setPosition(43,3,41);
		starBall[12]->setPosition(43,3,43);
		starBall[13]->setPosition(43,3,44);
		starBall[14]->setPosition(45,3,44);
		starBall[15]->setPosition(45,3,43);
		starBall[16]->setPosition(45,3,42);
		starBall[17]->setPosition(45,3,41);
		starBall[18]->setPosition(46,3,41);
		starBall[19]->setPosition(47,3,41);
		starBall[20]->setPosition(47,3,42);
		starBall[21]->setPosition(46,3,43);
		starBall[22]->setPosition(47,3,43);
		starBall[23]->setPosition(47,3,44);
		starBall[24]->setPosition(50,3,44);
		starBall[25]->setPosition(49.5f,3,43);
		starBall[26]->setPosition(49,3,42);
		starBall[27]->setPosition(49,3,41);
		starBall[28]->setPosition(50.5,3,43);
		starBall[29]->setPosition(51,3,42);
		starBall[30]->setPosition(51,3,41);
		starBall[31]->setPosition(53,3,41);
		starBall[32]->setPosition(53,3,42);
		starBall[33]->setPosition(53,3,43);
		starBall[34]->setPosition(53,3,44);
		starBall[35]->setPosition(54,3,44);
		starBall[36]->setPosition(55,3,44);
		starBall[37]->setPosition(55,3,43);
		starBall[38]->setPosition(55,3,42);
		starBall[39]->setPosition(55,3,41);
		starBall[40]->setPosition(54,3,41);
		starBall[41]->setPosition(57,3,41);
		starBall[42]->setPosition(57,3,42);
		starBall[43]->setPosition(57,3,43);
		starBall[44]->setPosition(57,3,44);
		starBall[45]->setPosition(58,3,42);
		starBall[46]->setPosition(59,3,41);
		starBall[47]->setPosition(58,3,43);
		starBall[48]->setPosition(59,3,44);
	}

	void HavokDemo::createNonsenseHavokBalls()
	{
		for (int i=0;i<49;i++)
		{
			string bulletNameString = "sABal_";
			bulletNameString.append(Ogre::StringConverter::toString(i).c_str());
			char* bulletName = new char[30];
			bulletName = (char*)bulletNameString.c_str();
			starBall[i] = new GameObject(bulletName,"star.mesh","star.hkx",PHYSICS_DYNAMIC);
		}
		resetNonsenseHavokBalls();
	}
}

void customPhantomBox1Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime)
{
	hkVector4 boxPos1 = hkVector4(20.0f + 5.0f, 3.0f, -35.0f);
	for (int i = 0; i < f_pOwnerAabbPhantom->getOverlappingCollidables().getSize(); i++ )
	{
		try 
		{
			hkpRigidBody* f_pRigidBody = static_cast<hkpRigidBody*>(f_pOwnerAabbPhantom->getOverlappingCollidables()[i]->getOwner());
			if (f_pRigidBody!=NULL)
			{
				f_pRigidBody->activate();
				f_pRigidBody->setPosition(boxPos1);
				hkVector4	f_vForce(2000.0f, 0.0f, 0.0f);
				f_pRigidBody->applyForce( f_fTotalTime, f_vForce );
			}
		}
		catch (...)
		{
		}
	}
}

void customPhantomBox2Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime)
{
	hkVector4 boxPos2 = hkVector4(50.0f, 3.0f, -35.0f - 5.0f);
	for (int i = 0; i < f_pOwnerAabbPhantom->getOverlappingCollidables().getSize(); i++ )
	{
		try 
		{
			hkpRigidBody* f_pRigidBody = static_cast<hkpRigidBody*>(f_pOwnerAabbPhantom->getOverlappingCollidables()[i]->getOwner());
			if (f_pRigidBody!=NULL)
			{
				f_pRigidBody->activate();
				f_pRigidBody->setPosition(boxPos2);
				hkVector4	f_vForce(0.0f, 0.0f, -2000.0f);
				f_pRigidBody->applyForce( f_fTotalTime, f_vForce );
			}
		}
		catch (...)
		{
		}
	}
}

void customPhantomBox3Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime)
{
	hkVector4 boxPos3 = hkVector4(50.0f - 5.0f, 3.0f, -70.0f);
	for (int i = 0; i < f_pOwnerAabbPhantom->getOverlappingCollidables().getSize(); i++ )
	{
		try 
		{
			hkpRigidBody* f_pRigidBody = static_cast<hkpRigidBody*>(f_pOwnerAabbPhantom->getOverlappingCollidables()[i]->getOwner());
			if (f_pRigidBody!=NULL)
			{
				f_pRigidBody->activate();
				f_pRigidBody->setPosition(boxPos3);
				hkVector4	f_vForce(-2000.0f, 0.0f, 0.0f );
				f_pRigidBody->applyForce( f_fTotalTime, f_vForce );
			}
		}
		catch (...)
		{
		}
	}
}

void customPhantomBox4Function(hkpAabbPhantom* f_pOwnerAabbPhantom,hkReal f_fDeltaTime,hkReal f_fTotalTime)
{
	hkVector4 boxPos4 = hkVector4(20.0f, 3.0f, -70.0f + 5.0f);
	for (int i = 0; i < f_pOwnerAabbPhantom->getOverlappingCollidables().getSize(); i++ )
	{
		try 
		{
			hkpRigidBody* f_pRigidBody = static_cast<hkpRigidBody*>(f_pOwnerAabbPhantom->getOverlappingCollidables()[i]->getOwner());
			if (f_pRigidBody!=NULL)
			{
				f_pRigidBody->activate();
				f_pRigidBody->setPosition(boxPos4);
				hkVector4	f_vForce(0.0f, 0.0f, 2000.0f);
				f_pRigidBody->applyForce( f_fTotalTime, f_vForce );
			}
		}
		catch (...)
		{
		}
	}
}
#endif