#pragma once

// GameScreen
#include "GameScreen.h"
#ifdef HAVOK
#include "HavokWrapper.h"//added
#endif

namespace GamePipeGame
{

	class HavokDemo : public GamePipe::GameScreen
	{
#ifdef HAVOK
		hkVector4 checkPosition;
		hkVector4 teleportPosition;
		int bound;

		//Character Inout Variables
		bool m_bAnimateAlexStormTropper;
		hkReal m_iForwardMinus_BackPlus, m_iLeftPlus_RightMinus,m_fRotAngle_LeftPlus_RightMinus;
		bool m_bJUMP;

		//BulletCount
		int m_iBallCount;

		//Debug Txt Variables
		char cardamageText[50];

		//Keyboard and Screen Handling Variables
		int toggle_v_c;
		int toggle;
		int controlToggle;

		//Keyframed Objects
		GameObject* KeyFramedBox;

		//CharacterProxy and RigidBodies
		GameObject* m_pHussiCharProxy;
		GameObject* m_pRAHCharRigBod;
		GameObject* m_pRDSRoboCharAnimRigBody;
		GameObject* m_pAlexStormCharAnimProxy;
		GameObject* newBridge;
		GameObject* boundingBox;
		GameObject* boundingHull;
		GameObject* boundingMesh;
		GameObject* constraint3;
		GameObject* newBreakableBridge;
		GameObject* pendulumMoved;
		GameObject* pendulum;
		GameObject* collWall;
		GameObject* phantomBG;

		//Vehicle
		GameObject* CarObject;

		//Bridge
		GameObject* myBrekRopeBridge;

		//Phantoms
		GameObject* m_pPhantomAabb;
		GameObject* m_pPhantomShape;

		GameObject* m_pPhantomBox1;
		GameObject* m_pPhantomBox2;
		GameObject* m_pPhantomBox3;
		GameObject* m_pPhantomBox4;

		bool ragdollToken;
		bool ToggleViewportToken;
		bool phantomToggle;
		bool m_bRayCastActive;

		GameObject* starBall[50];
		void createNonsenseHavokBalls();
		void resetNonsenseHavokBalls();
			
	public:
		void ViewportToggle(bool value);
		void CreateRagDollsOnFly();
		
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		bool OnKeyRelease(const GIS::KeyEvent &keyEvent);

		bool OnMousePress(const GIS::MouseEvent &mouseEvent, GIS::MouseButtonID mouseButtonId);

		HavokDemo(std::string name);
		~HavokDemo() {}
#endif
	};
}