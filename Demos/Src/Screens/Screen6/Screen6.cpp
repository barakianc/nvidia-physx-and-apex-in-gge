// Screen6 , Input Screen
#include "Screen6.h"

// Main
#include "Main.h"
#include "Utilities.h"
#include "GameScreen.h"


namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// Screen6(std::string name)
	//////////////////////////////////////////////////////////////////////////
	Screen6::Screen6(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);

		key_code = GIS::KC_CALCULATOR;
		key_text = " ";
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::LoadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::UnloadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().clearResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::Initialize()
	{
		CreateDefaultSceneManager();
		SetActiveCamera(CreateDefaultCamera());		
		CreateDefaultPhysicsScene(GetDefaultSceneManager());

		SetBackgroundColor(Ogre::ColourValue(0.3921, 0.5843, 0.9294, 1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));
		pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pSceneManager->createEntity("OgreHead", "ogrehead.mesh"));


		UtilitiesPtr->Add("JST", new GamePipe::DebugText("KEY: --- ", 0.5, 0.5, 5, Ogre::ColourValue(0, 0, 0, 0.75), Ogre::TextAreaOverlayElement::Center));
		UtilitiesPtr->Add("KEY", new GamePipe::DebugText("Key: --- ", 0.5, 0.7, 5, Ogre::ColourValue(0, 0, 0, 0.75), Ogre::TextAreaOverlayElement::Center));
		UtilitiesPtr->Add("POV" , new GamePipe::DebugText("POV: --- ", 0.5, 0.6, 4, Ogre::ColourValue(0, 0, 0, 0.75), Ogre::TextAreaOverlayElement::Center));
		UtilitiesPtr->Add("AXIS" , new GamePipe::DebugText("AXIS: --- ", 0.5, 0.8, 4, Ogre::ColourValue(0, 0, 0, 0.75), Ogre::TextAreaOverlayElement::Center));		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::Destroy()
	{
		UtilitiesPtr->Remove("JST"); 
		UtilitiesPtr->Remove("KEY"); 
		UtilitiesPtr->Remove("POV");
		UtilitiesPtr->Remove("AXIS");
		
		DestroyDefaultPhysicsScene();
		DestroyDefaultSceneManager();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::Update()
	{
		FreeCameraMovement();
		

		if (InputPtr->IsKeyDown(key_code))
		{
		UtilitiesPtr->GetDebugText("KEY")->SetText("Keyboard:"+ key_text);
		UtilitiesPtr->GetDebugText("KEY")->SetColor(Ogre::ColourValue (1, 1, 1, 0.75));
		}


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool Screen6::Draw()
	{
		GetDefaultSceneManager()->getEntity("OgreHead")->getParentSceneNode()->yaw(Ogre::Degree(100 * EnginePtr->GetRealDeltaTime()));
		
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	bool Screen6::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{

		key_code = keyEvent.key;
		key_text = keyEvent.text;

		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		}

		return true;
	}

	bool Screen6::OnKeyRelease(const GIS::KeyEvent& keyEvent)
	{
		UtilitiesPtr->GetDebugText("KEY")->SetText("KEY: -- ");
		UtilitiesPtr->GetDebugText("KEY")->SetColor(Ogre::ColourValue (0, 0, 0, 0.75));
		return true;
	}

}