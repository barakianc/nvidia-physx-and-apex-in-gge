#pragma once

// GameScreen
#include "GameScreen.h"


namespace GamePipeGame
{
	class Screen6 : public GamePipe::GameScreen
	{
	private:
	protected:
	public:
		std::string controller;

		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);
		bool OnKeyRelease(const GIS::KeyEvent& keyEvent);

		GIS::KeyCode key_code;
		Ogre::String key_text;

	public:
		Screen6(std::string name);
		~Screen6() {}
	};
}