#pragma once

// GameScreen
#include "GameScreen.h"


namespace GamePipeGame
{
	class Screen5 : public GamePipe::GameScreen
	{
	private:
	protected:
	public:
	
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

	public:
		Screen5(std::string name);
		~Screen5() {}
	};
}