// Screen5
#include "Screen5.h"

// Main
#include "Main.h"


namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// Screen5(std::string name)
	//////////////////////////////////////////////////////////////////////////
	Screen5::Screen5(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::LoadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::UnloadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().clearResourceGroup("General");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::Initialize()
	{
		CreateDefaultSceneManager();
		SetActiveCamera(CreateDefaultCamera());		
		CreateDefaultPhysicsScene(GetDefaultSceneManager());

		SetBackgroundColor(Ogre::ColourValue(1,1,1,1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));
		//pSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(pSceneManager->createEntity("OgreHead", "ogrehead.mesh"));
		FlashGUIPtr->firstTime=true;  // To indicate the creation of mainmenu only on the first call of show. 

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::Destroy()
	{
		DestroyDefaultPhysicsScene();
		DestroyDefaultSceneManager();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);
		if(FlashGUIPtr->firstTime)
		{
		FlashGUIPtr->SetViewport(viewport);       // Setting the viewport to make flashoverlays
	  	FlashGUIPtr->MakeMainMenu();              // Setting up main menu
		FlashGUIPtr->firstTime=false;
		}
       
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::Update()
	{
		FreeCameraMovement();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool Screen5::Draw()
	{
		//GetDefaultSceneManager()->getEntity("OgreHead")->getParentSceneNode()->yaw(Ogre::Degree(100 * EnginePtr->GetRealDeltaTime()));
		
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	bool Screen5::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case GIS::KC_ESCAPE:
			{
				FlashGUIPtr->FlashGUIManager->destroyAllControls();
		        FlashGUIPtr->screenchange=1;		
		        FlashGUIPtr->firstTime=true;
				break;
			}
		case GIS::KC_1:
			{
				//AddGameScreen(new Screen1("Screen1"));
				break;
			}
		case GIS::KC_2:
			{
				AddGameScreen(new NxOgreDemo("NxOgreDemo"));
				break;
			}
		//case GIS::KC_3:
		//	{
		//		if (EnginePtr->GetGameScreen("Screen3"))
		//		{
					//EnginePtr->BringScreenToFront("Screen3");
		//		}
		//		else
		//		{
					//AddGameScreen(new Screen3("Screen3"));
		//		}
				
		//		break;
		//	}
		//case GIS::KC_0:
		//	{
		//		AddGameScreen(new DefenseScreen("Defense"));
		//		break;
		//	}
		}

		return true;
	}
}