// Screen3
#include "Screen3.h"

// Main
#include "Main.h"
#include "Input.h"

// Gfx
#include "RenderTargetTexture.h"

namespace GamePipeGame
{
	//////////////////////////////////////////////////////////////////////////
	// Screen3(std::string name)
	//////////////////////////////////////////////////////////////////////////
	Screen3::Screen3(std::string name) 
	:	GamePipe::GameScreen(name),
		m_bShowRenderTargetMaterial( true ),
		m_bRadialBlur( false ),
		m_bUpdateBullet( true ),
		m_bSunMaterial( true ),
		m_pFireBillboard( NULL ),
		m_pHeatShimmerBillboard( NULL ),
		m_pSunBillboard( NULL )
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::LoadResources()
	{
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("General");
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("ParticleUniverse");
		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::UnloadResources()
	{
		//Ogre::ResourceGroupManager::getSingleton().clearResourceGroup("General");
		//Ogre::ResourceGroupManager::getSingleton().clearResourceGroup("ParticleUniverse");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::Initialize()
	{
		CreateDefaultSceneManager();
		CreateDefaultPhysicsScene(GetDefaultSceneManager());

		SetActiveCamera(CreateDefaultCamera());	

		SetBackgroundColor(Ogre::ColourValue(0.0000, 0.0000, 1.0000, 1.0000));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.5000, 0.5000, 0.5000, 0.5000));
		
		Ogre::SceneNode *pRootNode = pSceneManager->getRootSceneNode();

		ParticleUniverse::ParticleSystem *mySys = ParticleUniverse::ParticleSystemManager::getSingleton().createParticleSystem("mySystem", "example_003", pSceneManager);
		Ogre::SceneNode* particleNode = pRootNode->createChildSceneNode();
		particleNode->attachObject(pSceneManager->createEntity("OgreHead", "ogrehead.mesh"));
		particleNode->attachObject(mySys);
		particleNode->setPosition(Ogre::Vector3(0, 0, 0));
		mySys->prepare();
		mySys->start();

	
		//---- Gfx stuff begin -----------------------------------------------
		GamePipe::RenderTargetTexture::Get().AddRenderTarget( GetActiveCamera(), "RenderTarget1" );

		//add another rendertarget for demo purpose
		Ogre::Camera *pAnotherCam = pSceneManager->createCamera("AnotherCamera");
		pAnotherCam->setAspectRatio(16.0f/9.0f);
		pAnotherCam->setPosition( Ogre::Vector3(0.0f,-200.0f,0.0f) );

		Ogre::Quaternion camQuat;
		camQuat.FromAxes(	Ogre::Vector3::UNIT_X,
							Ogre::Vector3::UNIT_Z,
							-Ogre::Vector3::UNIT_Y );

		pAnotherCam->setOrientation( camQuat );
	

		GamePipe::RenderTargetTexture::Get().AddRenderTarget( pAnotherCam, "RenderTarget2" );


		//must initialize this resource group after rendertargettexture is initialized
		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Gfx");
		
		m_BulletTracerManager.AttachToNode(	pSceneManager->getRootSceneNode() );
		m_BulletTracerManager.SetMaterial( "Material_BulletTracer" );

		CreateFire( pRootNode );

		CreateHeatShimmer( pRootNode );

		CreateSun( pRootNode );

		//---- Gfx stuff end -------------------------------------------------

		//create the skybox
		pSceneManager->setSkyBox(	true,							//enable
									"Examples/StormySkyBox",		//materialName
									5000.0f,						//distance
									true,							//drawFirst
									Ogre::Quaternion::IDENTITY  );	//orientation

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::Destroy()
	{
		if ( m_bRadialBlur ) ToggleRadialBlur();

		ParticleUniverse::ParticleSystemManager::getSingleton().destroyParticleSystem("mySystem", GetDefaultSceneManager());
	
		DestroyDefaultPhysicsScene();
		DestroyDefaultSceneManager();

		delete m_pFireBillboard;
		delete m_pHeatShimmerBillboard;
		delete m_pSunBillboard;

		m_pFireBillboard = NULL;
		m_pHeatShimmerBillboard = NULL;
		m_pSunBillboard = NULL;

		//GamePipe::RenderTargetTexture::Get().ClearRenderTarget();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex(), 0, 0, 1.0, 1.0);
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::Update()
	{
		FreeCameraMovement();

		if (InputPtr->IsKeyDown(OIS::KC_SPACE))
		{
			char str[3];

			static int randomNum = 0;
			randomNum++;
			itoa(randomNum,str,10);
			
			Ogre::String nodename = "bd";
			nodename = nodename + str;

			Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
			
			ParticleUniverse::ParticleSystem *mySys = ParticleUniverse::ParticleSystemManager::getSingleton().createParticleSystem(nodename, "example_003", pSceneManager);
			Ogre::SceneNode* particleNode = pSceneManager->getRootSceneNode()->createChildSceneNode();
			particleNode->attachObject(mySys);
			particleNode->setPosition(Ogre::Vector3(0, 0, 0));
			mySys->prepare();
			mySys->start();
		}

		CreateBulletTracer( Ogre::Vector3(0.0f, 0.0f, 50.0f) );

		if ( m_bUpdateBullet )
		{
			//---- Gfx stuff begin -----------------------------------------------
			m_BulletTracerManager.Update();
			//---- Gfx stuff end -------------------------------------------------
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool Screen3::Draw()
	{
		GetDefaultSceneManager()->getEntity("OgreHead")->getParentSceneNode()->yaw(Ogre::Degree(300 * EnginePtr->GetRealDeltaTime()));
		
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		//---- Gfx stuff begin -----------------------------------------------
		GamePipe::RenderTargetTexture::Get().PreRender();
		//---- Gfx stuff end -------------------------------------------------

		return true;
	}

	bool Screen3::OnKeyPress(const OIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		case OIS::KC_ESCAPE:
			{
				Close();
				break;
			}
		case OIS::KC_0:
			{
				EnginePtr->BringScreenToFront(GetCallingGameScreen()->GetName());
			}
		}

		return true;
	}

	bool Screen3::OnKeyRelease(const OIS::KeyEvent &keyEvent)
	{
		switch(keyEvent.key)
		{
		case OIS::KC_U:
			ToggleMaterial();
			break;
		case OIS::KC_B:
			ToggleRadialBlur();
			break;
		case OIS::KC_P:
			ToggleBulletUpdate();
			break;
		case OIS::KC_T:
			ToggleSunMaterial();
			break;
		}

		return true;
	}

	bool Screen3::OnMousePress(const OIS::MouseEvent &mouseEvent, OIS::MouseButtonID mouseButtonId)
	{
		switch ( mouseButtonId )
		{
		case OIS::MB_Left:
			{
				Ogre::Camera *pCamera = GetActiveCamera();

				const Quaternion &camQuat = pCamera->getDerivedOrientation();

				Ogre::Vector3 vCameraZ = camQuat.zAxis();
	
				Ogre::Vector3 vBegin = m_v3CameraPosition + Ogre::Vector3(0.0f, -2.0f, 0.0f);
				Ogre::Vector3 vEnd = m_v3CameraPosition - vCameraZ * 500.0f;

				static float s_fSpeed = 100.0f;
				static float s_fWidth = .4f;
				static float s_fFade = 0.25f;

				m_BulletTracerManager.CreateBulletTracer(	vBegin,
															vEnd,
															s_fWidth,
															s_fSpeed,
															s_fFade );
			}
			break;
		}
		return true;
	}

	void Screen3::CreateFire( Ogre::SceneNode *pNode )
	{
		m_pFireBillboard = new GamePipe::BillboardChain("Fire");
		m_pFireBillboard->setMaterialName("Material_ProceduralFire");

		m_pFireBillboard->setMaxChainElements(2);
		m_pFireBillboard->setTextureCoordDirection(BillboardChain::TCD_V);

		pNode->attachObject( m_pFireBillboard );

		GamePipe::BillboardChain::Element element;
		element.position = Ogre::Vector3(0.0f, -50.0f, 0.0f);
		element.width = 50.0f;
		element.texCoord = 0.95f;
		element.colour = ColourValue::White;

		m_pFireBillboard->addChainElement( 0, element );

		element.position[1] += 50.0f;
		element.texCoord = 0.0f;

		m_pFireBillboard->addChainElement( 0, element );
	}

	void Screen3::CreateHeatShimmer( Ogre::SceneNode *pNode )
	{
		m_pHeatShimmerBillboard = new GamePipe::BillboardSet("HeatShimmer");
		m_pHeatShimmerBillboard->setMaterialName("Material_HeatShimmer");

		pNode->attachObject( m_pHeatShimmerBillboard );

		Ogre::Billboard *pBillboard = m_pHeatShimmerBillboard->createBillboard( Ogre::Vector3(50.0f, 0.0f, 0.0f) );

		pBillboard->setDimensions(20.0f, 20.0f);
	}

	void Screen3::CreateSun( Ogre::SceneNode *pNode )
	{
		m_pSunBillboard = new GamePipe::BillboardSet("Sun");
		m_pSunBillboard->setMaterialName("Material_ProceduralSun");

		pNode->attachObject( m_pSunBillboard );

		Ogre::Billboard *pBillboard = m_pSunBillboard->createBillboard( Ogre::Vector3(50.0f, 50.0f, 0.0f) );

		pBillboard->setDimensions(100.0f, 100.0f);
	}

	void Screen3::CreateBulletTracer(const Ogre::Vector3 &center)
	{
		const float width = 50.0f;
		const float height = 50.0f;
		const float fromX = 50.0f;
		const float toX = -50.0f;

		float randomFloat1 = float( rand() ) / RAND_MAX;
		float randomFloat2 = float( rand() ) / RAND_MAX;
		float randomFloat3 = float( rand() ) / RAND_MAX;
		float randomFloat4 = float( rand() ) / RAND_MAX;

		Ogre::Vector3 from( fromX, randomFloat1 * height, (randomFloat2 - .5f) * width );
		Ogre::Vector3 to( toX, randomFloat3 * height, (randomFloat4 - .5f) * width ); 

		
		static float s_fSpeed = 1000.0f;
		static float s_fWidth = .40f;
		static float s_fFade = 0.5f;

		m_BulletTracerManager.CreateBulletTracer(	center + from,
													center + to,
													s_fWidth,
													s_fSpeed,
													s_fFade );
		
	}

	void Screen3::ToggleMaterial()
	{
		m_bShowRenderTargetMaterial = !m_bShowRenderTargetMaterial;

		if ( m_bShowRenderTargetMaterial )
		{
			m_pHeatShimmerBillboard->setMaterialName("Material_HeatShimmer");
			m_BulletTracerManager.SetMaterial("Material_BulletTracer");
		}
		else
		{
			m_pHeatShimmerBillboard->setMaterialName("BaseWhite");
			m_BulletTracerManager.SetMaterial("BaseWhite");
		}
	}

	void Screen3::ToggleRadialBlur()
	{
		m_bRadialBlur = !m_bRadialBlur;

		Ogre::CompositorManager &compositeMan = Ogre::CompositorManager::getSingleton();
		
		Ogre::Camera *pCam = GetActiveCamera();
		Ogre::Viewport *pViewport = pCam->getViewport();

		if ( m_bRadialBlur )
		{
			compositeMan.addCompositor( pViewport, "Compositor_RadialBlur" );
			compositeMan.setCompositorEnabled( pViewport, "Compositor_RadialBlur", true);
		}
		else
		{
			compositeMan.removeCompositor( pViewport, "Compositor_RadialBlur" );
		}
	}

	void Screen3::ToggleBulletUpdate()
	{
		m_bUpdateBullet = !m_bUpdateBullet;
	}

	void Screen3::ToggleSunMaterial()
	{
		m_bSunMaterial = !m_bSunMaterial;

		if ( m_bSunMaterial )
		{
			m_pSunBillboard->setMaterialName("Material_ProceduralSun");
		}
		else
		{
			m_pSunBillboard->setMaterialName("Material_RTExample");
		}
	}
}