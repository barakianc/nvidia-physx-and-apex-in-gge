#pragma once

// GameScreen
#include "GameScreen.h"

// Gfx
#include "BillboardChain.h"
#include "BillboardSet.h"
#include "BulletTracerManager.h"

namespace GamePipeGame
{
	class Screen3 : public GamePipe::GameScreen
	{
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		bool Update();
		bool Draw();

		bool OnKeyPress(const OIS::KeyEvent &keyEvent);
		bool OnKeyRelease(const OIS::KeyEvent &keyEvent);

		bool OnMousePress(const OIS::MouseEvent &mouseEvent, OIS::MouseButtonID mouseButtonId);

		Screen3(std::string name);
		~Screen3() {}
	protected:

	private:
		bool									m_bShowRenderTargetMaterial;
		bool									m_bRadialBlur;
		bool									m_bUpdateBullet;
		bool									m_bSunMaterial;

		GamePipe::BulletTracerManager			m_BulletTracerManager;

		GamePipe::BillboardChain *				m_pFireBillboard;

		GamePipe::BillboardSet *				m_pHeatShimmerBillboard;

		GamePipe::BillboardSet *				m_pSunBillboard;

		void CreateFire( Ogre::SceneNode *pNode );
		void CreateHeatShimmer( Ogre::SceneNode *pNode );
		void CreateSun( Ogre::SceneNode *pNode );
		void CreateBulletTracer( const Ogre::Vector3 &center );

		void ToggleMaterial();
		void ToggleRadialBlur();
		void ToggleBulletUpdate();
		void ToggleSunMaterial();
	};
}