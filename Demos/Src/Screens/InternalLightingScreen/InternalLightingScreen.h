#pragma once

// GameScreen
#include "GameScreen.h"
#include "GameCamera.h"
#include "Utilities.h"

// For Deferred Shading
#include "DeferredShading.h"

namespace GamePipeGame
{
	class InternalLightingScreen : public GamePipe::GameScreen
	{
	private:
	protected:
		bool doDeferredShading;
		bool doDeferredShadingSSAO;
		DeferredShadingSystem *mSystem;

		bool startup;
		bool programExit;
	
	public:
		bool LoadResources();
		bool UnloadResources();

		bool Show();

		bool Initialize();
		bool Destroy();

		void createAtheneScene(Ogre::SceneNode* rootNode);
		void createKnotScene(Ogre::SceneNode* rootNode);
		void createObjects(Ogre::SceneNode* rootNode);
		void setEntityHeight(Ogre::Entity* ent, Ogre::Real newHeight);
		void createSampleLights();
		void initLights();

		bool Update();
		bool Draw();

		bool OnKeyPress(const GIS::KeyEvent &keyEvent);

		static bool textInitialized;

	public:
		InternalLightingScreen(std::string name);
		~InternalLightingScreen() {}
	};
}