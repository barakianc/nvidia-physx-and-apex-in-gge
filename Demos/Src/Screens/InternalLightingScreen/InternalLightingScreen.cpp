#include "StdAfx.h"
// Lighting screens
#include "InternalLightingScreen.h"
#include "..\ExternalLightingScreen\ExternalLightingScreen.h"
#include "..\MainMenuScreen\MainMenuScreen.h"

// Engine
#include "Engine.h"

// Graphics
#include "LightingManager.h"

#include "GeomUtils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

namespace GamePipeGame
{
	bool InternalLightingScreen::textInitialized = false;

	const Ogre::ColourValue SAMPLE_COLORS[] = 
    {   Ogre::ColourValue::Red, Ogre::ColourValue::Green, Ogre::ColourValue::Blue, 
    Ogre::ColourValue::White, Ogre::ColourValue(1,1,0,1), Ogre::ColourValue(1,0,1,1) };

	//////////////////////////////////////////////////////////////////////////
	// InternalLightingScreen(std::string name)
	//////////////////////////////////////////////////////////////////////////
	InternalLightingScreen::InternalLightingScreen(std::string name) : GamePipe::GameScreen(name)
	{
		SetBackgroundUpdate(true);
		SetBackgroundDraw(true);
		SetIsPopup(false);
	}

	//////////////////////////////////////////////////////////////////////////
	// Load()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::LoadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("General");
		GamePipe::ResourceManager::GetInstancePtr()->LoadResourceGroup("Lighting");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unload()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::UnloadResources()
	{
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("General");
		GamePipe::ResourceManager::GetInstancePtr()->UnloadResourceGroup("Lighting");
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Initialize()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::Initialize()
	{
		SetBackgroundColor(Ogre::ColourValue(0.3921f, 0.5843f, 0.9294f, 1.0000f));

		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		// Setup shadows first
		pSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

		m_fCameraMovementSpeed = 20.0;
		m_fCameraRotationSpeed = 7.0;
		m_qCameraOrientation = Ogre::Quaternion(Ogre::Degree(90.0), Ogre::Vector3(0.0, 1.0, 0.0));
		m_v3CameraPosition = Ogre::Vector3(25, 5, 0);

        // Set ambient light
		pSceneManager->setAmbientLight(Ogre::ColourValue(0.15f, 0.00f, 0.00f));
        // Skybox
		pSceneManager->setSkyBox(true, "Skybox/ViolentSkyBox", 500);

		// Initialize variables
		doDeferredShading = false;
		doDeferredShadingSSAO = false;
		startup = true;
		programExit = false;

		// Initialize rendering mode text
		if (!textInitialized)
		{
			const std::string instructions0 = "3 = Toggle Deferred Shading (DS)";
			UtilitiesPtr->Add("Instructions0", new GamePipe::DebugText(instructions0, 0.05f, 0.1f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions0")->GetOverlay()->show();
			const std::string instructions1 = "4 = Toggle SSAO";
			UtilitiesPtr->Add("Instructions1", new GamePipe::DebugText(instructions1, 0.05f, 0.12f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions1")->GetOverlay()->show();
			const std::string instructions2 = "5 = Toggle DS Shadows";
			UtilitiesPtr->Add("Instructions2", new GamePipe::DebugText(instructions2, 0.05f, 0.14f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions2")->GetOverlay()->show();
			const std::string instructions3 = "6 = DS Emissive";
			UtilitiesPtr->Add("Instructions3", new GamePipe::DebugText(instructions3, 0.05f, 0.16f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions3")->GetOverlay()->show();
			const std::string instructions4 = "7 = DS Diffuse";
			UtilitiesPtr->Add("Instructions4", new GamePipe::DebugText(instructions4, 0.05f, 0.18f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions4")->GetOverlay()->show();
			const std::string instructions5 = "8 = DS Normals";
			UtilitiesPtr->Add("Instructions5", new GamePipe::DebugText(instructions5, 0.05f, 0.20f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions5")->GetOverlay()->show();
			const std::string instructions6 = "9 = DS Depth and Specular";
			UtilitiesPtr->Add("Instructions6", new GamePipe::DebugText(instructions6, 0.05f, 0.22f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions6")->GetOverlay()->show();
			const std::string instructions7 = "0 = DS Composite";
			UtilitiesPtr->Add("Instructions7", new GamePipe::DebugText(instructions7, 0.05f, 0.24f, 2, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("Instructions7")->GetOverlay()->show();

			const std::string renderingTextFR = "Forward Rendering";
			UtilitiesPtr->Add("RenderingTextFR", new GamePipe::DebugText(renderingTextFR, 0.05f, 0.4f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
			const std::string renderingTextDS = "Deferred Shading";
			UtilitiesPtr->Add("RenderingTextDS", new GamePipe::DebugText(renderingTextDS, 0.05f, 0.4f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			const std::string renderingTextDSComposite = "Composite";
			UtilitiesPtr->Add("RenderingTextDSComposite", new GamePipe::DebugText(renderingTextDSComposite, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			const std::string renderingTextDSEmissive = "Emissive Color";
			UtilitiesPtr->Add("RenderingTextDSEmissive", new GamePipe::DebugText(renderingTextDSEmissive, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
			const std::string renderingTextDSDiffuse = "Diffuse Color";
			UtilitiesPtr->Add("RenderingTextDSDiffuse", new GamePipe::DebugText(renderingTextDSDiffuse, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
			const std::string renderingTextDSNormals = "Normals";
			UtilitiesPtr->Add("RenderingTextDSNormals", new GamePipe::DebugText(renderingTextDSNormals, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
			const std::string renderingTextDSDepthSpec = "Depth and Specular";
			UtilitiesPtr->Add("RenderingTextDSDepthSpec", new GamePipe::DebugText(renderingTextDSDepthSpec, 0.05f, 0.5f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
			const std::string renderingTextSSAO = "Screen-space Ambient Occlusion";
			UtilitiesPtr->Add("RenderingTextSSAO", new GamePipe::DebugText(renderingTextSSAO, 0.05f, 0.6f, 3, Ogre::ColourValue(0.3f, 1.0f, 0.3f, 0.75f), Ogre::TextAreaOverlayElement::Left));
			UtilitiesPtr->GetDebugText("RenderingTextSSAO")->GetOverlay()->hide();
			textInitialized = true;
		}

		// Load the internal scene.
		LoadScene("InternalLightingScreen.scene");

		return true;
	}

	// Initializes the lights in the scene with whatever the .scene file is incapable of storing.
	void InternalLightingScreen::initLights()
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		Ogre::vector<Ogre::Light*>::type lights;
		Ogre::vector<Ogre::Node*>::type nodes;

		Ogre::String postfix[6] = { "a", "b", "c", "d", "e", "f" };
		//Ogre::Real pos[6][3] = { { 0,0,1 }, { 1,0,0 }, { 0,0,-1 }, { -1,0,0 }, { 1,0,1 }, { -1,0,-1 } };
		//Ogre::Real lightRadius = 25;

		for (int i = 0; i < 6; ++i)
		{
			Ogre::Light* light = pSceneManager->getLight("athene_light_" + postfix[i]);
			Ogre::SceneNode* node = light->getParentSceneNode();
			//node->setPosition(pos[i][0] * lightRadius, pos[i][1] * lightRadius, pos[i][2] * lightRadius);
			lights.push_back(light);
			nodes.push_back(node);
		}

		// Create marker meshes to show user where the lights are
		Ogre::Entity *ent;
		GeomUtils::createSphere("PointLightMesh", 0.05f, 5, 5, true, true);
		for(Ogre::vector<Ogre::Light*>::type::iterator i=lights.begin(); i!=lights.end(); ++i)
		{
			Ogre::Light* light = *i;
			ent = pSceneManager->createEntity(light->getName()+"v", "PointLightMesh");
			Ogre::String matname = light->getName()+"m";
			// Create coloured material
			Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create(matname,
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
            Ogre::Pass* pass = mat->getTechnique(0)->getPass(0);
            pass->setDiffuse(0.0f,0.0f,0.0f,1.0f);
			pass->setAmbient(0.0f,0.0f,0.0f);
			pass->setSelfIllumination(light->getDiffuseColour());

			ent->setMaterialName(matname);
			//ent->setRenderQueueGroup(light->getRenderQueueGroup());
			ent->setRenderQueueGroup(DeferredShadingSystem::POST_GBUFFER_RENDER_QUEUE);
			static_cast<Ogre::SceneNode*>(light->getParentNode())->attachObject(ent);
			ent->setVisible(true);
		}		

		// Store nodes for hiding/showing
		SharedData::getSingleton().mLightNodes = nodes;

		// Do some animation for node a-f
		// Generate helix structure
		float seconds_per_station = 1.0f;
		float r = 1.0;
		//Vector3 base(0,-30,0);
		//Ogre::Vector3 base(-8.75, 3.5, 0);
		Ogre::Vector3 base(-0.25, -1, 0);

		float h=3;
		const size_t s_to_top = 16;
		const size_t stations = s_to_top*2-1;
		float ascend = h/((float)s_to_top);
		float stations_per_revolution = 3.5f;
		size_t skip = 2; // stations between lights
		Ogre::Vector3 station_pos[stations];
		for(size_t x=0; x<s_to_top; ++x)
		{
			float theta = ((float)x/stations_per_revolution)*2.0f*Ogre::Math::PI;
			station_pos[x] = base+Ogre::Vector3(Ogre::Math::Sin(theta)*r, ascend*x, Ogre::Math::Cos(theta)*r);
		}
		for(size_t x=s_to_top; x<stations; ++x)
		{
			float theta = ((float)x/stations_per_revolution)*2.0f*Ogre::Math::PI;
			station_pos[x] = base+Ogre::Vector3(Ogre::Math::Sin(theta)*r, h-ascend*(x-s_to_top), Ogre::Math::Cos(theta)*r);
		}
		// Create a track for the light swarm
		Ogre::Animation* anim = pSceneManager->createAnimation("LightSwarmTrack", stations*seconds_per_station);
		// Spline it for nice curves
		anim->setInterpolationMode(Ogre::Animation::IM_SPLINE);
		for(unsigned int x=0; x<nodes.size(); ++x)
		{
			// Create a track to animate the camera's node
			Ogre::NodeAnimationTrack* track = anim->createNodeTrack(x, nodes[x]);
			for(size_t y=0; y<=stations; ++y)
			{
				// Setup keyframes
				Ogre::TransformKeyFrame* key = track->createNodeKeyFrame(y*seconds_per_station); // A start position
				key->setTranslate(station_pos[(x*skip+y)%stations]);
				// Make sure size of light doesn't change
				key->setScale(nodes[x]->getScale());
			}
		}
		// Create a new animation state to track this
		SharedData::getSingleton().mMLAnimState = pSceneManager->createAnimationState("LightSwarmTrack");
		SharedData::getSingleton().mMLAnimState->setEnabled(true);

		// Init the lights over the knots.
		for (int i=0; i < 5; i++)
        {
			char lightName[16];
            sprintf_s(lightName, "KnotLight%d", i);
			Ogre::Light* knotLight = pSceneManager->getLight(lightName);
            knotLight->setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);
            knotLight->setSpotlightRange(Ogre::Degree(25), Ogre::Degree(45), 1);
            knotLight->setAttenuation(6.0f, 1.0f, 0.2f, 0.0f);
        }
	}

//*************************************************************************************************
// These following functions created the original scene. Keeping around just in case.
//*************************************************************************************************

	void InternalLightingScreen::createAtheneScene(Ogre::SceneNode* rootNode)
	{
		// Prepare athene mesh for normalmapping
		Ogre::MeshPtr pAthene = Ogre::MeshManager::getSingleton().load("athene.mesh", 
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        unsigned short src, dest;
        if (!pAthene->suggestTangentVectorBuildParams(Ogre::VES_TANGENT, src, dest))
            pAthene->buildTangentVectors(Ogre::VES_TANGENT, src, dest);

		//Create an athena statue
        Ogre::Entity* athena = GetDefaultSceneManager()->createEntity("Athena", "athene.mesh");
		athena->setMaterialName("DeferredDemo/DeferredAthena");
		Ogre::SceneNode *aNode = rootNode->createChildSceneNode();
		aNode->attachObject( athena );
		aNode->setPosition(-8.5, 4.5, 0);
        setEntityHeight(athena, 4.0);
        aNode->yaw(Ogre::Degree(90));
		// Create some happy little lights to decorate the athena statue
		createSampleLights();
	}

	//Utility function to help set scene up
    void InternalLightingScreen::setEntityHeight(Ogre::Entity* ent, Ogre::Real newHeight)
    {
        Ogre::Real curHeight = ent->getMesh()->getBounds().getSize().y;
        Ogre::Real scaleFactor = newHeight / curHeight;

        Ogre::SceneNode* parentNode = ent->getParentSceneNode();
        parentNode->setScale(scaleFactor, scaleFactor, scaleFactor);
    }

	void InternalLightingScreen::createSampleLights()
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		// Create some lights		
		Ogre::vector<Ogre::Light*>::type lights;
		Ogre::SceneNode *parentNode = pSceneManager->getRootSceneNode()->createChildSceneNode("LightsParent");
		// Create light nodes
		Ogre::vector<Ogre::Node*>::type nodes;

        Ogre::Vector4 attParams = Ogre::Vector4(4,1,0,7);
        Ogre::Real lightRadius = 25;

		Ogre::Light *a = pSceneManager->createLight("athene_light_a");
		Ogre::SceneNode *an = parentNode->createChildSceneNode();
		an->attachObject(a);
		a->setAttenuation(attParams.x, attParams.y, attParams.z, attParams.w);
		//a->setAttenuation(1.0f, 0.000f, 0.000f);
		an->setPosition(0,0,lightRadius);
		a->setDiffuseColour(1,0,0);
		//a->setSpecularColour(0.5,0,0);
		lights.push_back(a);
		nodes.push_back(an);

		Ogre::Light *b = pSceneManager->createLight("athene_light_b");
		Ogre::SceneNode *bn = parentNode->createChildSceneNode();
		bn->attachObject(b);
		b->setAttenuation(attParams.x, attParams.y, attParams.z, attParams.w);
		bn->setPosition(lightRadius,0,0);
		b->setDiffuseColour(1,1,0);
		//b->setSpecularColour(0.5,0.5,0);
		lights.push_back(b);
		nodes.push_back(bn);

		Ogre::Light *c = pSceneManager->createLight("athene_light_c");
		Ogre::SceneNode *cn = parentNode->createChildSceneNode();
		cn->attachObject(c);
		c->setAttenuation(attParams.x, attParams.y, attParams.z, attParams.w);
		cn->setPosition(0,0,-lightRadius);
		c->setDiffuseColour(0,1,1);
		c->setSpecularColour(0.25,1.0,1.0); // Cyan light has specular component
		lights.push_back(c);
		nodes.push_back(cn);

		Ogre::Light *d = pSceneManager->createLight("athene_light_d");
		Ogre::SceneNode *dn = parentNode->createChildSceneNode();
		dn->attachObject(d);
		d->setAttenuation(attParams.x, attParams.y, attParams.z, attParams.w);
		dn->setPosition(-lightRadius,0,0);
		d->setDiffuseColour(1,0,1);
		d->setSpecularColour(0.0,0,0.0);
		lights.push_back(d);
		nodes.push_back(dn);

		Ogre::Light *e = pSceneManager->createLight("athene_light_e");
		Ogre::SceneNode *en = parentNode->createChildSceneNode();
		en->attachObject(e);
		e->setAttenuation(attParams.x, attParams.y, attParams.z, attParams.w);
		en->setPosition(lightRadius,0,lightRadius);
		e->setDiffuseColour(0,0,1);
		e->setSpecularColour(0,0,0);
		lights.push_back(e);
		nodes.push_back(en);
		
		Ogre::Light *f = pSceneManager->createLight("athene_light_f");
		Ogre::SceneNode *fn = parentNode->createChildSceneNode();
		fn->attachObject(f);
		f->setAttenuation(attParams.x, attParams.y, attParams.z, attParams.w);
		fn->setPosition(-lightRadius,0,-lightRadius);
		f->setDiffuseColour(0,1,0);
		f->setSpecularColour(0,0.0,0.0);
		lights.push_back(f);
		nodes.push_back(fn);

		// Create marker meshes to show user where the lights are
		Ogre::Entity *ent;
		GeomUtils::createSphere("PointLightMesh", 0.05f, 5, 5, true, true);
		for(Ogre::vector<Ogre::Light*>::type::iterator i=lights.begin(); i!=lights.end(); ++i)
		{
			Ogre::Light* light = *i;
			ent = pSceneManager->createEntity(light->getName()+"v", "PointLightMesh");
			Ogre::String matname = light->getName()+"m";
			// Create coloured material
			Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create(matname,
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
            Ogre::Pass* pass = mat->getTechnique(0)->getPass(0);
            pass->setDiffuse(0.0f,0.0f,0.0f,1.0f);
			pass->setAmbient(0.0f,0.0f,0.0f);
			pass->setSelfIllumination(light->getDiffuseColour());

			ent->setMaterialName(matname);
			//ent->setRenderQueueGroup(light->getRenderQueueGroup());
			ent->setRenderQueueGroup(DeferredShadingSystem::POST_GBUFFER_RENDER_QUEUE);
			static_cast<Ogre::SceneNode*>(light->getParentNode())->attachObject(ent);
			ent->setVisible(true);
		}		

		// Store nodes for hiding/showing
		SharedData::getSingleton().mLightNodes = nodes;

		// Do some animation for node a-f
		// Generate helix structure
		float seconds_per_station = 1.0f;
		float r = 1.0;
		//Vector3 base(0,-30,0);
		Ogre::Vector3 base(-8.75, 3.5, 0);

		float h=3;
		const size_t s_to_top = 16;
		const size_t stations = s_to_top*2-1;
		float ascend = h/((float)s_to_top);
		float stations_per_revolution = 3.5f;
		size_t skip = 2; // stations between lights
		Ogre::Vector3 station_pos[stations];
		for(size_t x=0; x<s_to_top; ++x)
		{
			float theta = ((float)x/stations_per_revolution)*2.0f*Ogre::Math::PI;
			station_pos[x] = base+Ogre::Vector3(Ogre::Math::Sin(theta)*r, ascend*x, Ogre::Math::Cos(theta)*r);
		}
		for(size_t x=s_to_top; x<stations; ++x)
		{
			float theta = ((float)x/stations_per_revolution)*2.0f*Ogre::Math::PI;
			station_pos[x] = base+Ogre::Vector3(Ogre::Math::Sin(theta)*r, h-ascend*(x-s_to_top), Ogre::Math::Cos(theta)*r);
		}
		// Create a track for the light swarm
		Ogre::Animation* anim = pSceneManager->createAnimation("LightSwarmTrack", stations*seconds_per_station);
		// Spline it for nice curves
		anim->setInterpolationMode(Ogre::Animation::IM_SPLINE);
		for(unsigned int x=0; x<nodes.size(); ++x)
		{
			// Create a track to animate the camera's node
			Ogre::NodeAnimationTrack* track = anim->createNodeTrack(x, nodes[x]);
			for(size_t y=0; y<=stations; ++y)
			{
				// Setup keyframes
				Ogre::TransformKeyFrame* key = track->createNodeKeyFrame(y*seconds_per_station); // A start position
				key->setTranslate(station_pos[(x*skip+y)%stations]);
				// Make sure size of light doesn't change
				key->setScale(nodes[x]->getScale());
			}
		}
		// Create a new animation state to track this
		SharedData::getSingleton().mMLAnimState = pSceneManager->createAnimationState("LightSwarmTrack");
		SharedData::getSingleton().mMLAnimState->setEnabled(true);

		/*Ogre::Light* spotLight = pSceneManager->createLight("Spotlight1");
		spotLight->setType(Ogre::Light::LT_SPOTLIGHT);
		spotLight->setAttenuation(200, 1.0f, 0, 0);
		spotLight->setSpotlightRange(Ogre::Degree(30.0), Ogre::Degree(45.0), 0.8);
		spotLight->setPosition(0,120,0);
		spotLight->setDirection(0, -1, 0);
		spotLight->setDiffuseColour(1,1,1);
		spotLight->setSpecularColour(1,1,1);*/
	}

	void InternalLightingScreen::createKnotScene(Ogre::SceneNode* rootNode)
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		// Prepare knot mesh for normal mapping
		Ogre::MeshPtr pKnot = Ogre::MeshManager::getSingleton().load("knot.mesh", 
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		unsigned short src, dest;
        if (!pKnot->suggestTangentVectorBuildParams(Ogre::VES_TANGENT, src, dest))
            pKnot->buildTangentVectors(Ogre::VES_TANGENT, src, dest);

		// Create a bunch of knots with spotlights hanging from above
		Ogre::Entity* knotEnt = pSceneManager->createEntity("Knot", "knot.mesh");
		knotEnt->setMaterialName("DeferredDemo/RockWall");
		//knotEnt->setMeshLodBias(0.25f);
        Ogre::Vector3 knotStartPos(25.5f, 2.0f, 5.5f);
        Ogre::Vector3 knotDiff(-3.7f, 0.0f, 0.0f);
        for (int i=0; i < 5; i++)
        {
            char cloneName[16];
			sprintf_s(cloneName, "Knot%d", i);
            Ogre::Entity* cloneKnot = knotEnt->clone(cloneName);
            Ogre::Vector3 clonePos = knotStartPos + knotDiff*(Ogre::Real)i;
			Ogre::SceneNode* cloneNode = rootNode->createChildSceneNode(clonePos);
            cloneNode->attachObject(cloneKnot);
            setEntityHeight(cloneKnot, 3);
            cloneNode->yaw(Ogre::Degree((Ogre::Real)i*17));
            cloneNode->roll(Ogre::Degree((Ogre::Real)i*31));

            sprintf_s(cloneName, "KnotLight%d", i);
            Ogre::Light* knotLight = pSceneManager->createLight(cloneName);
			Ogre::SceneNode* lightNode = rootNode->createChildSceneNode(clonePos + Ogre::Vector3(0,3,0));
			lightNode->attachObject(knotLight);
            knotLight->setType(Ogre::Light::LT_SPOTLIGHT);
            knotLight->setDiffuseColour(SAMPLE_COLORS[i]);
            knotLight->setSpecularColour(Ogre::ColourValue::White);
            //knotLight->setPosition(Ogre::Vector3(0,3,0));
            knotLight->setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);
            knotLight->setSpotlightRange(Ogre::Degree(25), Ogre::Degree(45), 1);
            knotLight->setAttenuation(6.0f, 1.0f, 0.2f, 0.0f);
        }
	}

	void InternalLightingScreen::createObjects(Ogre::SceneNode* rootNode)
	{
		Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

		// Create ogre heads to decorate the wall
		Ogre::Entity* ogreHead = pSceneManager->createEntity("Head", "ogrehead.mesh");
		//rootNode->createChildSceneNode( "Head" )->attachObject( ogreHead );
        Ogre::Vector3 headStartPos[2] = { Ogre::Vector3(25.25,11,3), Ogre::Vector3(25.25,11,-3) };
        Ogre::Vector3 headDiff(-3.7f,0.0f,0.0f);
        for (int i=0; i < 12; i++) 
        {
            char cloneName[16];
			sprintf_s(cloneName, "OgreHead%d", i);
            Ogre::Entity* cloneHead = ogreHead->clone(cloneName);
            Ogre::Vector3 clonePos = headStartPos[i%2] + headDiff*(Ogre::Real)(i/2);
            if ((i/2) >= 4) clonePos.x -= 0.75;
			Ogre::SceneNode* cloneNode = rootNode->createChildSceneNode(clonePos);
            cloneNode->attachObject(cloneHead);
            setEntityHeight(cloneHead, 1.5);
            if (i % 2 == 0)
            {
                cloneNode->yaw(Ogre::Degree(180));
            }
        }

		// Create a pile of wood pallets
        Ogre::Entity* woodPallet = pSceneManager->createEntity("Pallet", "WoodPallet.mesh");
        Ogre::Vector3 woodStartPos(10, 0.5, -5.5);
        Ogre::Vector3 woodDiff(0.0f, 0.3f, 0.0f);
        for (int i=0; i < 5; i++)
        {
            char cloneName[16];
			sprintf_s(cloneName, "WoodPallet%d", i);
            Ogre::Entity* clonePallet = woodPallet->clone(cloneName);
            Ogre::Vector3 clonePos = woodStartPos + woodDiff*(Ogre::Real)i;
			Ogre::SceneNode* cloneNode = rootNode->createChildSceneNode(clonePos);
            cloneNode->attachObject(clonePallet);
            setEntityHeight(clonePallet, 0.3f);
            cloneNode->yaw(Ogre::Degree((Ogre::Real)i*20));
        }

	}

	//////////////////////////////////////////////////////////////////////////
	// Destroy()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::Destroy()
	{
		LightingManager::Destroy();

		if (programExit)
		{
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextFR"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDS"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSComposite"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSEmissive"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSNormals"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("RenderingTextSSAO"));

			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions0"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions1"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions2"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions3"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions4"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions5"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions6"));
			UtilitiesPtr->Remove(UtilitiesPtr->GetDebugText("Instructions7"));
			textInitialized = false;
		}
		else
			// Switch screens only if we're not trying to exit the program.
			EnginePtr->AddGameScreen(new GamePipeGame::ExternalLightingScreen("ExternalLightingScreen"));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Show()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::Show()
	{
		Ogre::Camera* pCamera = GetActiveCamera();
		Ogre::Viewport* viewport = EnginePtr->GetRenderWindow()->addViewport(pCamera, EnginePtr->GetFreeViewportIndex());
		viewport->setBackgroundColour(GetBackgroundColor());
		viewport->setOverlaysEnabled(true);
		SetViewport(viewport);

		pCamera->setAspectRatio(static_cast<Ogre::Real>(viewport->getActualWidth()) / static_cast<Ogre::Real>(viewport->getActualHeight()));

		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Update()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::Update()
	{
		if (startup)
		{
			//Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();

			//// Create main, static light
			//Ogre::Light* l1 = pSceneManager->createLight();
			//l1->setType(Ogre::Light::LT_DIRECTIONAL);
			//l1->setDiffuseColour(0.5f, 0.45f, 0.1f);
			//l1->setDirection(1, -0.5, -0.2);
			//l1->setShadowFarClipDistance(250);
			//l1->setShadowFarDistance(75);
			////Turn this on to have the directional light cast shadows
			//l1->setCastShadows(false);
			//LightingManager::Initialize(l1->getName().c_str());

			// Init main, static light
			Ogre::Light* mainLight = GetDefaultSceneManager()->getLight("main_light");
			mainLight->setDirection(1.0f, -0.5f, -0.2f);
			mainLight->setShadowFarClipDistance(250);
			mainLight->setShadowFarDistance(75);
			//Turn this on to have the directional light cast shadows
			mainLight->setCastShadows(false);

			LightingManager::Initialize("main_light_Node");

			// TODO - this has to be enabled at least once during the Update thread before it can
			// enabled/disabled without crashing when changed in any other thread later.  This needs
			// to be fixed!!
			LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
			//isLightingInitialized = true;

			initLights();

			startup = false;
		}

		FreeCameraMovement();

		//static bool firsttime = true;
		//if (firsttime)
		//{
		//	//// Create "root" node
		//	//Ogre::SceneManager* pSceneManager = GetDefaultSceneManager();
		//	//Ogre::SceneNode* rootNode = pSceneManager->getRootSceneNode()->createChildSceneNode();

		//	//// Create the cathedral - this will be the static scene
		//	//Ogre::Entity* cathedralEnt = pSceneManager->createEntity("Cathedral", "sibenik.mesh");
		//	//Ogre::SceneNode* cathedralNode = rootNode->createChildSceneNode();
		//	//cathedralNode->attachObject(cathedralEnt);
		//	//
		//	//createAtheneScene(rootNode);
		//	//createKnotScene(rootNode);
		//	//createObjects(rootNode);
		//	initLights();

		//	//setupControls();

		//	firsttime = false;
		//}

		if (SharedData::getSingleton().mMLAnimState)
			SharedData::getSingleton().mMLAnimState->addTime(EnginePtr->GetDeltaTime());

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw()
	//////////////////////////////////////////////////////////////////////////
	bool InternalLightingScreen::Draw()
	{
		Ogre::SceneNode* pCameraSceneNode = GetActiveCameraSceneNode();
		
		pCameraSceneNode->setPosition(m_v3CameraPosition);
		pCameraSceneNode->setOrientation(m_qCameraOrientation);

		return true;
	}

	bool InternalLightingScreen::OnKeyPress(const GIS::KeyEvent& keyEvent)
	{
		switch(keyEvent.key)
		{
		/*case GIS::KC_BACK:
		{
			EnginePtr->RemoveGameScreen(this);
			break;
		}*/
		case GIS::KC_ESCAPE:
			{
				EnginePtr->AddGameScreen(new GamePipeGame::MainMenuScreen("MainMenuScreen"));
				Close();
				programExit = true;
				break;
			}
		case GIS::KC_TAB:
			{
				GameScreen::OnKeyPress(keyEvent);
				break;
			}
		case GIS::KC_SYSRQ:
			{
				GameScreen::OnKeyPress(keyEvent);
				break;
			}
		
		case GIS::KC_3:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					if (!LightingManager::GetDeferredShadingEnabled())
					{
						LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
						UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();

						UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->show();
						UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
					}
					else
					{
						LightingManager::DisableDeferredShading();
						UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->show();

						UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
						UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
					}
				}
#endif
				break;
			}
		case GIS::KC_4:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					if (LightingManager::GetSSAOEnabled())
					{
						UtilitiesPtr->GetDebugText("RenderingTextSSAO")->GetOverlay()->hide();
						LightingManager::DisableSSAO();
					}
					else
					{
						UtilitiesPtr->GetDebugText("RenderingTextSSAO")->GetOverlay()->show();
						LightingManager::EnableSSAO();
					}
				}
#endif
				break;
			}
		case GIS::KC_5:
			{
#ifdef GLE_EDITOR
				static bool enableKnotShadows = true;
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					//UtilitiesPtr->GetDebugText("EnableShadows")->GetOverlay()->hide();
					// Enable/disable the shadows over the torus knots
					for (int i=0; i < 5; i++)
					{
						char lightName[16];
						sprintf_s(lightName, "KnotLight%d", i);
						Ogre::Light* knotLight = GetDefaultSceneManager()->getLight(lightName);
						knotLight->setCastShadows(!enableKnotShadows);
					}
					enableKnotShadows = !enableKnotShadows;
				}
#endif
				break;
			}
			case GIS::KC_6:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::EMISSIVE_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		case GIS::KC_7:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::DIFFUSE_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		case GIS::KC_8:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::NORMALS_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		case GIS::KC_9:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::DEPTH_AND_SPECULAR_ONLY);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->show();
				}
#endif
				break;
			}
		case GIS::KC_0:
			{
#ifdef GLE_EDITOR
				if (!EnginePtr->GetEditor()->IsEditorMode())
				{
					LightingManager::EnableDeferredShading(LightingManager::COMPOSITE);
					UtilitiesPtr->GetDebugText("RenderingTextFR")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDS")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSComposite")->GetOverlay()->show();
					UtilitiesPtr->GetDebugText("RenderingTextDSEmissive")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDiffuse")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSNormals")->GetOverlay()->hide();
					UtilitiesPtr->GetDebugText("RenderingTextDSDepthSpec")->GetOverlay()->hide();
				}
#endif
				break;
			}
		}

		return true;
	}
}
